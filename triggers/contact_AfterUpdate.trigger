trigger contact_AfterUpdate on Contact ( after update, before insert, before update) 
{
    Map<string, string> requestMap = new Map<string, string>();
    Boolean run= MaskSensitiveDataSettings__c.getInstance().MaskAllData__c;

    if (!run) 
    {
        for (Contact con : Trigger.new) 
        {     
            if (Trigger.isBefore){
                if (con.Phone_Index__c != con.Phone_Clean__c) con.Phone_Index__c = con.Phone_Clean__c;
                if (String.isNotEmpty(con.MailingState)) {
                    if ( 'AL,MO,MN,MS,ND,SD,TX,NE,KS,OK,IA,AR,LA,WI,IL,TN'.contains(con.MailingState.toUpperCase())) con.Time_Zone__c = 'Central Time' ;
                    else if ( 'GA,MI,NC,OH,DE,NJ,FL,SC,KY,IN,WV,VA,PA,NY,DC,MD,DE,CT,RI,MA,NH,ME,VT'.contains(con.MailingState.toUpperCase())) con.Time_Zone__c = 'Eastern Time' ;
                    else if ( 'UT,AZ,MT,NM,ID,WY,CO'.contains(con.MailingState.toUpperCase())) con.Time_Zone__c = 'Mountain Time' ;
                    else if ( 'AK'.contains(con.MailingState.toUpperCase())) con.Time_Zone__c = 'Yukon Time' ;
                    else if ('WA,CA,OR,NV'.contains(con.MailingState.toUpperCase())) con.Time_Zone__c = 'Pacific Time' ;
                }
                if (con.DOB__c != null && con.Birthdate == null) con.Birthdate = con.DOB__c;
            }
                
            if (Trigger.isBefore && Trigger.IsInsert) {
                con.UnemploymentRate__c = SalesForceUtil.getUnemploymentRate(con);
                //SSN Population before insert 
                if (con.ints__Social_Security_Number__c != null ) con.SSN__c = con.ints__Social_Security_Number__c.replace('-',''); 
            }
            
            if (Trigger.isBefore && Trigger.isUpdate) {
                if((con.MailingState <> Trigger.oldMap.get(con.id).MailingState)||(con.MailingPostalCode <> Trigger.oldMap.get(con.id).MailingPostalCode)){
                    con.UnemploymentRate__c = SalesForceUtil.getUnemploymentRate(con);
                }
                //SSN Population before update 
                if (con.ints__Social_Security_Number__c != null ) con.SSN__c = con.ints__Social_Security_Number__c.replace('-',''); 
                if(con.Do_Not_Contact__c <> Trigger.oldMap.get(con.id).Do_Not_Contact__c ){
                    if (con.Do_Not_Contact__c) con.Do_Not_Contact_Request_Date__c = DateTime.now();
                    con.DoNotCall = con.Do_Not_Contact__c;
                }
                if(con.Employment_Type__c <> Trigger.oldMap.get(con.id).Employment_Type__c && String.isNotEmpty(con.Employment_Type__c) && con.Employment_Type__c.toUpperCase() == 'MILITARY'){
                    con.Military__c = true;
                }
            }
            
            if (Trigger.isAfter && Trigger.isUpdate) 
            {
                if (con.Email <> Trigger.oldMap.get(con.id).Email) 
                {
                    requestMap.put('contactId', con.id);
                    requestMap.put('email', con.email);
                    string jsonRequest = Json.SerializePretty(requestMap);
                    system.debug('=======jsonRequest ======' + jsonRequest );
                    updateEmailInCPDatabaseUtility.tokenAndEmailUpdateMethod(jsonRequest);
                }
            }
        }
    }

}