trigger paymentSuccesEmail on loan__Loan_Payment_Transaction__c (after update) {  
    System.debug('Email loan__Loan_Payment_Transaction__c  STARTED');        
    String mesg, htmlBody, subject = ''; //ID conId; 
    try{ 
        List<String> toSent = New List<String>();          
        List<Note> no = New List<Note>();  
        EmailTemplate eTemplate = [Select id,subject,Htmlvalue from EmailTemplate where Name=:'Payment Received' limit 1];  
        for (loan__Loan_Payment_Transaction__c a : Trigger.new){
           //system.debug('a ' + a);
            if (a.loan__Cleared__c == true && a.Payment_Pending__c == 'Cleared'){
                toSent.add(a.Customer_Name__c);
                subject = string.isnotEmpty(eTemplate.subject) ? eTemplate.subject : '';
                htmlBody = string.isempty(eTemplate.HtmlValue) ?  '' : eTemplate.HtmlValue;                
                htmlBody =  htmlBody.replace(' {!Contact.FirstName}' , (a.Customer_Name__c != null) ? [select firstname from contact where id =:a.Customer_Name__c].firstname :'');
                                
                htmlBody =  htmlBody.replace('{!loan__Loan_Payment_Transaction__c.loan__Transaction_Amount__c}' , (a.loan__Transaction_Amount__c != null) ? string.ValueOF(a.loan__Transaction_Amount__c) : string.ValueOf(0.00));
                htmlBody =  htmlBody.replace('{!loan__Loan_Payment_Transaction__c.Name}' , (a.Name!= null) ? a.Name :'');
              
                Note conNote = New Note(ParentId =a.Customer_Name__c,Body='Payment Received',Title='Thank you! Your payment has been received.');
                no.add(conNote);
            }
            else mesg = 'payment not processed' ;
        }
        
         if(toSent!=null && toSent.size() > 0){        
            Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
            mail.setToAddresses(toSent);
            mail.subject = subject;
            mail.setHtmlBody(htmlBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage [] { mail });
            insert no;
            system.debug('Email sent sucessfully and notes also');
        }
       System.debug('Email loan__Loan_Payment_Transaction__c  Ended');     
    }
    catch(Exception e){
        system.debug(e+'  '+mesg);
    }
   
}