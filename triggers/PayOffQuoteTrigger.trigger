trigger PayOffQuoteTrigger on loan__Payoff_Quote__c (before insert) {
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    boolean bAddOneDayInterest = cuSettings != null && ( cuSettings.Add_Day_of_Interest__c || cuSettings.Add_Day_Interest_to_Pay_Off__c);

    for (loan__Payoff_Quote__c p : trigger.new ) {
        if (bAddOneDayInterest) p.loan__Poq_Total_Payoff_Amount__c +=  p.loan__Poq_Interest_Per_Diem__c;	
       
    }
    
}