trigger CaseAfterCreate on Case (after insert, after update) {

    private List<Case> caseListToUpsert = new List<Case>();
    private Map<id, Opportunity> currentAppListToUpsert = new Map<Id, Opportunity>();
    private Map<Id, Opportunity> currAppToLeadAssign = new Map<id, Opportunity>();
    private List<loan__Loan_Account__c> loansToUpdate = new List<loan__Loan_Account__c>();
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    private Map<id, User> aUsers = new Map<Id, User>([Select Id, Name from User where  IsActive = true]);

    String DECLINED = 'Declined (or Unqualified)',
           ADDITIONAL_ITEMS_REQUESTED = 'Additional Items Requested',
           FUNDED = 'Funded',
           REFINANCE_UNQUALIFIED = 'Refinance Unqualified',
           AGED_CANCELATION = 'Aged / Cancellation',
           DOCUMENTS_REQUESTED = 'Documents Requested',
           DOCUMENTS_RECEIVED = 'Documents Received',
           INCOMPLETE_PACKAGE = 'Incomplete Package',
           CREDIT_QUALIFIED = 'Credit Qualified',
           OFFER_ACCEPTED = 'Offer Accepted',
           CONTRACT_PACKAGE_SENT = 'Contract Pkg Sent',
           SIGNED_CONTRACT = 'A signed Contract has been received for',
           SIGNED_WORK_AGREEMENT = 'A signed Work Agreement has been received for',
           STATEMENTS_AVAILABLE = 'Statements Available in ACH Information Tab',
           CREDIT_APPROVED_NO_CONTACT = 'Credit Approved - No Contact Allowed',
           APPROVED_PENDING_FUNDING = 'Approved, Pending Funding',
           DOCUMENTS_IN_REVIEW = 'Documents In Review',
           CREDIT_REVIEW = 'Credit Review',
           CONTRACT_PACKAGE_APPROVED = 'Contract Pkg Approved';

    Set<String> statusesNoChange = new Set<String> {APPROVED_PENDING_FUNDING, CONTRACT_PACKAGE_APPROVED, CREDIT_REVIEW, DOCUMENTS_IN_REVIEW, FUNDED};
    Set<String> statusesToDocsReceived = new Set<String> {CREDIT_APPROVED_NO_CONTACT, CREDIT_QUALIFIED, DOCUMENTS_REQUESTED, OFFER_ACCEPTED};

    if(Trigger.isInsert) {
        try {
            for (Case cs : Trigger.new) {
                for (Case currentCase : [SELECT Id, Status, RecordTypeId, Overdue_Days__c, OwnerId, Description, ContactId, Automatic_Assignment__c, Opportunity__c, Opportunity__r.Name, Subject, SuppliedEmail, CreatedDate, Origin, CL_Contract__c, Case_Assignment_Date__c
                                        FROM Case
                                        WHERE Id = : cs.Id]) {

                    if (currentCase.RecordTypeId != null && currentCase.RecordTypeId == Label.RecordType_Collections_Id) {
    
                    } else {

                        Boolean isCaseAdded = false, sameOwner = false, defaultOwner = false, dataEntryAssigned = false;
                        String description = '', mail = '', subject = '';

                        Opportunity currentApp = new Opportunity();

                        if (String.isNotEmpty(currentCase.Subject)) subject = currentCase.subject;
                        //Findng Contact.email to assigning current case to currentContact and currentApp
                        //Looking by Description contains emailContact
                        if (String.isNotEmpty(currentCase.Description)) {
                            description = currentCase.Description.toLowerCase().replace('@lendingpoint.com', ' ').replace('@decisionlogic.net', '');
                            String regex = '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))';
                            Pattern descriptionPattern = Pattern.compile(regex );
                            Matcher mailMatcher = descriptionPattern.matcher(description);
                            while (mailMatcher.find() && !isCaseAdded) {
                                mail = mailMatcher.group();

                                if (String.isNotEmpty(mail)) {
                                    for (Opportunity app : [SELECT Id
                                                            FROM Opportunity
                                                            WHERE Contact__r.Email = : mail AND
                                                                    Name LIKE 'APP%'
                                                                    ORDER By CreatedDate DESC LIMIT 1]) {
                                        currentApp = app;
                                        isCaseAdded = true;
                                    }
                                }

                            }

                        }
                        
                        //if @mail still empty, take a look into Subject and valorate if contains app.Name
                        if (!isCaseAdded && String.isNotEmpty(subject) && subject.contains('APP-')) {
                            String subjectAppName = subject.subString(subject.indexOf('APP-'),
                                                    subject.indexOf('APP-') + 14);
                            if (String.isNotEmpty(subjectAppName)) {
                                for (Opportunity app : [SELECT Id
                                                        FROM Opportunity
                                                        WHERE Name = : subjectAppName AND Name LIKE 'APP%' LIMIT 1]) {
                                    currentApp = app;
                                    isCaseAdded = true;
                                }
                            }
                        }



                        if (!isCaseAdded && String.isNotEmpty(subject) && subject.contains('Request Complete') && subject.contains('003')) {
                            String contactId = subject.subString(subject.indexOf('003'), subject.indexOf('003') + 18);
                            for (Opportunity app : [SELECT Id
                                                    FROM Opportunity
                                                    WHERE Contact__c = : contactId AND
                                                            Name LIKE 'APP%'
                                                            order by CreatedDate DESC LIMIT 1]) {
                                currentApp = app;
                                isCaseAdded = true;

                            }
                        }

                        if (isCaseAdded) {
                            currentApp = [SELECT Id, Name, Contact__c, OwnerId, Contact__r.Email, DE_Assigned_To_Doc__c, Status__c, Status_Modified_Date_Time__c
                                        FROM Opportunity where Id = : currentApp.Id];
                            currentCase.ContactId = currentApp.Contact__c;
                            currentCase.Opportunity__c = currentApp.Id;
                        }
                        ////search end, the case must be assigned at this point

                        //Assinging cases to Default_Case_User
                        if (String.isNotEmpty(subject) && (subject.contains(STATEMENTS_AVAILABLE) ||
                                                        subject.contains(SIGNED_CONTRACT) ||
                                                        subject.contains(SIGNED_WORK_AGREEMENT))) {

                            currentCase.OwnerId = Label.Default_Case_User;
                            currentCase.Automatic_Assignment__c = true;
                            currentCase.Status = 'Closed';
                            defaultOwner = true;
                            if (currentApp.Id != null) currAppToLeadAssign.put(currentApp.Id, currentApp);
                        }

                        //Assigning LPMS Welcome Cases
                        if (String.isNotEmpty(subject) && (subject.contains('LPMS Welcome Call'))) {
                            currentCase.OwnerId = CaseAutomaticAssignmentUtils.getNextLPMSUserForCase(currentCase).userId;
                            currentCase.Automatic_Assignment__c = true;
                            defaultOwner = true;
                        }

                        //Assigning Owner by DE already selected on drop-down
                        if (!defaultOwner && isCaseAdded && String.isNotEmpty(currentApp.DE_Assigned_To_Doc__c)) {
                            for (User u :  [SELECT Id
                                            From User
                                            WHERE Name = : currentApp.DE_Assigned_To_Doc__c
                                                        LIMIT 1]) {
                                if (aUsers.containsKey(u.Id) && (CaseAutomaticAssignmentUtils.checkOwnerExistanceIntoGroupById(u.Id)) ) {
                                    currentCase.OwnerId = u.Id;
                                    currentCase.Automatic_Assignment__c = true;
                                    dataEntryAssigned = true;
                                }
                                if (currentApp.Id != null) {
                                    currAppToLeadAssign.put(currentApp.Id, currentApp);
                                    currentApp.Status__c =  validateAppStatus(currentCase, currentApp, description);

                                    if (!currentAppListToUpsert.containsKey(currentApp.id)) currentAppListToUpsert.put(currentApp.id, currentApp);
                                }
                            }
                        }

                        //Assigning to the sameOwner when already exists an assigned case for the currentApp
                        if (isCaseAdded && !defaultOwner && !dataEntryAssigned) {
                            for (Case caseExists : [SELECT Id, OwnerId
                                                    FROM Case
                                                    WHERE ((Opportunity__c != null AND Opportunity__c = : currentApp.Id)
                                                        OR (ContactId != null AND ContactId = : currentApp.Contact__c))
                                                    AND OwnerId <> : Label.Default_Case_User
                                                    ORDER BY createdDate desc
                                                    limit 1]) {

                                if (String.isNotEmpty(caseExists.OwnerId) && aUsers.containsKey(caseExists.OwnerId) && (CaseAutomaticAssignmentUtils.checkOwnerExistanceIntoGroupById(caseExists.OwnerId))) {
                                    currentCase.OwnerId = caseExists.OwnerId;
                                    currentCase.Automatic_Assignment__c = true;
                                    sameOwner = true;
                                    currentApp.Status__c = validateAppStatus(currentCase, currentApp, description);
                                    currentApp.DE_Assigned_To_Doc__c = aUsers.get(currentCase.OwnerId).name;
                                    if (!currentAppListToUpsert.containsKey(currentApp.id)) currentAppListToUpsert.put(currentApp.id, currentApp);
                                }
                            }
                        }

                        //added case to be upserted
                        if ((isCaseAdded || sameOwner || defaultOwner || dataEntryAssigned)  && !caseListToUpsert.contains(currentCase)) {
                            caseListToUpsert.add(currentCase);
                        }
                        //Assignin a new ownerId DataEntry/CustomerService
                        if ( ((String.isNotEmpty(description)) || (String.isNotEmpty(subject) &&
                                !subject.contains('Signature Request') &&
                                !subject.contains('Corporate eFax')) )
                                && (!sameOwner && !defaultOwner && !dataEntryAssigned) ) {

                            DealAutomaticUserGroup user = null;
                            //Cases for Data Entry
                            if ( (String.isNotEmpty(description) && (currentCase.Description.contains('docs@lendingpoint.com') ||
                                    currentCase.Description.contains('customersuccess@lendingpoint.com')))
                                    || (String.isNotEmpty(currentCase.SuppliedEmail) && (currentCase.SuppliedEmail.contains('docs@lendingpoint.com') ||
                                            currentCase.SuppliedEmail.contains('customersuccess@lendingpoint.com')))
                                    || (String.isNotEmpty(subject) && (subject.contains('Documents Have Been Uploaded from Web') ||
                                            subject.contains('Documents uploaded via the Web for App') )) ) {

                                if (currentApp != null && currentApp.Id != null) {
                                    currentApp.Status__c = validateAppStatus(currentCase, currentApp, description);
                                    if (!currentAppListToUpsert.containsKey(currentApp.id)) currentAppListToUpsert.put(currentApp.id, currentApp);
                                }
                                
                                // if (currentApp != null && currentApp.Id != null) currAppToLeadAssign.put(currentApp.Id, currentApp);
                            }
                                    
                            // Cases for Customer Service
                            if ( (String.isNotEmpty(description) && (currentCase.Description.contains('info@lendingpoint.com') ||
                                    currentCase.Description.contains('customerservice@lendingpoint.com') ||
                                    currentCase.Description.contains('payments@lendingpoint.com')))
                                    || (String.isNotEmpty(currentCase.SuppliedEmail) && (currentCase.SuppliedEmail.contains('info@lendingpoint.com') ||
                                            currentCase.SuppliedEmail.contains('customerservice@lendingpoint.com') ||
                                            currentCase.SuppliedEmail.contains('payments@lendingpoint.com')))
                                    || (String.isNotEmpty(subject) && subject.containsIgnoreCase('Payment Request')) ) {

                                user = CaseAutomaticAssignmentUtils.getNextCustomerServiceUserForCase(currentCase);
                            }

                            if (user != null && user.userId != null) {
                                currentCase.OwnerId = user.userId;
                                currentCase.Automatic_Assignment__c = true;

                                if (!isCaseAdded && !caseListToUpsert.contains(currentCase))
                                    caseListToUpsert.add(currentCase);
                            }
                        }
                        //Cases for active FundingSpecialist 
                        if (subject.contains(('Credit Review - Opportunity # ' + currentCase.Opportunity__r.name))) {
                            DealAutomaticUserGroup user = CaseAutomaticAssignmentUtils.getNextEzVerifyFSUserForCase(currentCase);
                            if ( user != null ) { 
                                currentCase.OwnerId = user.userId;
                                currentCase.Automatic_Assignment__c = true;
                                Boolean addCaseToUpsertList = true;
                                for (Case aCase : caseListToUpsert) {
                                    if (aCase.Id == currentCase.Id) {
                                        addCaseToUpsertList = false;
                                    }
                                }
                                if (addCaseToUpsertList && !caseListToUpsert.contains(currentCase)) {
                                    caseListToUpsert.add(currentCase);
                                }
                            }
                        }
                        if (subject.contains('Request Complete for') && subject.contains('Request Code:') && subject.contains('003') && currentCase.Description.contains('LOGIN NOT VERIFIED') ){
                            currentCase.Status = 'Closed';
                            if (!caseListToUpsert.contains(currentCase))
                                caseListToUpsert.add(currentCase);   
                        }
                    }
                }
            }
            for (Opportunity tApp : currentAppListToUpsert.values()) {
                if (tApp.Status__c == 'Credit Approved - No Contact Allowed' &&  currAppToLeadAssign.containsKey(tApp.Id) )  { tApp.Status__c = 'Offer Accepted';}
            }

            if (caseListToUpsert.size() > 0) upsert caseListToUpsert;
            if (currentAppListToUpsert.size() > 0) upsert currentAppListToUpsert.values();
            if (loansToUpdate.size() > 0 ) upsert loansToUpdate;

            for (Opportunity tApp : currentAppListToUpsert.values()) {
                if (tApp.Status__c == 'Offer Accepted' && tApp.OwnerId == cuSettings.WEB_API_User_Id__c &&  currAppToLeadAssign.containsKey(tApp.Id) )
                    DealAutomaticAssignmentManager.assignExistingAppFromWeb(tApp.Id);
            }


        } catch (Exception e) {
            System.debug('CaseAfterCreate Exception = ' + e);
            WebToSFDC.notifyDev('CaseAfterCreate trigger ERROR', e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isAfter) {
            for (Case currentCase : Trigger.new) {
                String recordIdRegularCases = [SELECT Id FROM RecordType where Name = 'Regular Cases'][0].Id;
                if (currentCase.RecordTypeId == recordIdRegularCases && currentCase.status == 'Closed') {
                    List<Attachment> emailLinkedAttachments = [SELECT Id, Name FROM Attachment WHERE ParentId IN (SELECT Id FROM EmailMessage WHERE ParentId = :currentCase.Id)];
                    List<Attachment> attachmentList = [SELECT Id, ParentId FROM Attachment WHERE ParentId = :currentCase.Id];
                    if (emailLinkedAttachments != null && emailLinkedAttachments.size() > 0) attachmentList.addAll(emailLinkedAttachments);
                    if (attachmentList != null && attachmentList.size() > 0) delete attachmentList;
                    List<ContentDocument> contentDocumentList = [SELECT Id FROM ContentDocument WHERE Id IN(SELECT ContentDocumentId FROM ContentVersion WHERE FirstPublishLocationId = : currentCase.id)];
                    if (contentDocumentList != null && contentDocumentList.size() > 0) delete contentDocumentList;
                }
            }
        }
    }

    public String validateAppStatus(Case currentCase, Opportunity currentApp, String description) {
        Boolean mainCondToUpdToDocReceived = ((statusesToDocsReceived.contains(currentApp.Status__c)) && (String.isNotEmpty(currentCase.SuppliedEmail) && 
                                              currentCase.SuppliedEmail.contains('docs@lendingpoint.com')) &&
                                              currentCase.Opportunity__c != null && (currentCase.CreatedDate > currentApp.Status_Modified_Date_Time__c)) ;

        Boolean isValidSubjectOnNewCase = !(String.isNotEmpty(currentCase.subject) && (currentCase.subject.contains(STATEMENTS_AVAILABLE) || currentCase.subject.contains(SIGNED_CONTRACT) ||
                                            currentCase.subject.contains(SIGNED_WORK_AGREEMENT))) ;

         
        if ((mainCondToUpdToDocReceived && isValidSubjectOnNewCase) && (!statusesNoChange.contains(currentApp.Status__c)))  
            return DOCUMENTS_RECEIVED;
        else if (currentApp.Status__c == INCOMPLETE_PACKAGE) {
            return DOCUMENTS_RECEIVED;
        }
        else 
            return  currentApp.Status__c;
    }
}