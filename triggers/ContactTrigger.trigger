/**
** Re-Grade Logic is implemented in this trigger
**/
trigger ContactTrigger on Contact (before update, before insert) {
    
    Set<Id> conIds = new Set<Id>();
    Set<Id> oppIds = new Set<Id>();
    Map<Id, Integer> oppCountMap = new Map<Id, Integer>();
    Map<Id, Integer> offerCountMap = new Map<Id, Integer>();
    
    if(Trigger.IsUpdate){
        AggregateResult[] offrResult = [SELECT Contact__c, Count(Id) FROM Offer__c WHERE Contact__c IN: Trigger.newMap.keySet() GROUP BY Contact__c];
        
        if(offrResult.size() > 0) {
            for(AggregateResult offr : offrResult) {
                offerCountMap.put((Id) offr.get('Contact__c'), (Integer) offr.get('expr0'));
            }
        }
    }
    
    for(Contact con : Trigger.new) {
        if(Trigger.IsUpdate){
            // Access the "old" record by its ID in Trigger.oldMap
            Contact oldCon = Trigger.oldMap.get(con.Id);
        
            if(offerCountMap.containsKey(con.Id)) {
                Boolean need = false;
                String reasons = ''; 
                
                if (oldCon.Annual_Income__c != con.Annual_Income__c){
                    con.Regrade_Reason_Income__c = 'Prior Income: ' + oldCon.Annual_Income__c + ', New Income: ' + con.Annual_Income__c + '.';
                    need = true;
                }
                if (string.isNotEmpty(oldCon.employment_Type__c) && oldCon.employment_Type__c != con.employment_Type__c){
                    con.Regrade_Reason_Employment__c = 'Prior Employment: ' + oldCon.employment_Type__c + ', New Employment: ' + con.employment_Type__c + '.';
                    need = true;
                }
                if (oldCon.mailingState != con.mailingState){
                    con.Regrade_Reason_State__c = 'Prior State: ' + oldCon.mailingState + ', New State: ' + con.mailingState + '.';
                    need = true;
                }
                
                if (need) con.Show_Regrade_Warning__c = true;
            }
        }
        if (!String.Isblank(con.Lead_Sub_Source__c) && con.Lead_Sub_Source__c.Contains('Credit') && (con.Lead_Sub_Source__c.Contains('Karma'))) con.Credit_Karma_Lead_Source_Tracking__c = true;
    }       
}