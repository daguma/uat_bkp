trigger CustomOtherTransactionsTrigger on loan__Other_Transaction__c (after insert) {
	 String RESCHEDULE = 'Reschedule',
           PRINCIPAL_ADJUSTMENT_ADD = 'PrincipalAdjustment-Add',
           PRINCIPAL_ADJUSTMENT_SUBTRACT = 'PrincipalAdjustment-Subtract',
           PRINCIPAL_ADJUSTMENT_ADD_REVERSAL = 'PrincipalAdjustment-Add Reversal',
           PRINCIPAL_ADJUSTMENT_SUBTRACT_REVERSAL = 'PrincipalAdjustment-Subtract Reversal',
           RATE_CHANGE = 'Rate Change',
           PAYMENT_TOLERANCE_CHANGE = 'Payment Tolerance Change',
           DUE_DATE_CHANGE = 'Due Date Change',
           PAYMENT_PLAN = 'Delinquent Pmt Plan',
           PAYMENT_CHANGE = 'Payment Change',
           PAYMENT_CHANGE_REVERSAL = 'Payment Change Reversal';
    Set<String> transactionsToGenerateAlert = new Set<String> { RESCHEDULE, PRINCIPAL_ADJUSTMENT_ADD, PRINCIPAL_ADJUSTMENT_SUBTRACT, PRINCIPAL_ADJUSTMENT_ADD_REVERSAL, PRINCIPAL_ADJUSTMENT_SUBTRACT_REVERSAL, RATE_CHANGE, PAYMENT_TOLERANCE_CHANGE, DUE_DATE_CHANGE, PAYMENT_PLAN, PAYMENT_CHANGE, PAYMENT_CHANGE_REVERSAL };

    if (Trigger.IsInsert) {
        String emailBody = '';

        for(loan__Other_Transaction__c newTransaction: trigger.new) {
            if(transactionsToGenerateAlert.contains(newTransaction.loan__Transaction_Type__c)){
              String createdByName = [SELECT Name FROM User WHERE id =: newTransaction.CreatedById LIMIT 1].Name;
              loan__Loan_Account__c contract = [SELECT Name, Card_Next_Debit_Date__c, Is_Debit_Card_On__c FROM loan__Loan_Account__c WHERE id =: newTransaction.loan__Loan_Account__c LIMIT 1];
              insert new OtherTransactionLogs__c(Other_Loan_Transaction__c = newTransaction.Id, CL_Contract__c = contract.Id, Created_By_Name__c = createdByName, Modification_Type__c = newTransaction.loan__Transaction_Type__c);
               
              if(contract.Is_Debit_Card_On__c){
                  if(newTransaction.loan__Transaction_Type__c.contains(RESCHEDULE)){
                      contract.Card_Next_Debit_Date__c = newTransaction.loan__Repayment_Start_Date__c;
                      update contract;
                  }else if(newTransaction.loan__Transaction_Type__c.contains(DUE_DATE_CHANGE)){
                      contract.Card_Next_Debit_Date__c = newTransaction.loan__Due_Date__c;
                      update contract;
                  }
              }
            }
        }
    }
}