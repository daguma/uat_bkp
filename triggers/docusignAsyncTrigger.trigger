trigger docusignAsyncTrigger on Account (after update, after insert) {
    
    if(Label.AccountTrigger == 'True'){
    System.debug('Account trigger executed..>> ');  
        
        //not bulk supported
        if (trigger.newMap.keySet().size() > 1)
            return;
            
        list<Recordtype> accRecordType = [select Name from Recordtype where id=: [select recordTypeid from Account where id IN: trigger.newmap.keyset()].recordTypeid ];    

        for (Integer i = 0; i < trigger.newMap.keySet().size(); i++) {
            system.debug('===============typeid======'+accRecordType);        
            
             if(accRecordType.size() > 0 && accRecordType[0].Name.contains('LMS')){
                if (Trigger.isUpdate) {
                    if (trigger.new[i].Onboarding_Stage__c == Docusign_Integration_Settings__c.getInstance().Trigger_Status__c 
                        && trigger.old[i].Onboarding_Stage__c != Docusign_Integration_Settings__c.getInstance().Trigger_Status__c 
                        && String.isBlank(trigger.new[i].Merchant_Agreement_Envelope_Id__c)) {
                            DocusignProcessor docusignJob = new DocusignProcessor(trigger.new[i].Id);
                            ID jobID = System.enqueueJob(docusignJob);
                            //DocusignProcessor.SubmitContractRequest(trigger.new[i].Id);        
                    }
                }
                else {
                    if (trigger.new[i].Onboarding_Stage__c == Docusign_Integration_Settings__c.getInstance().Trigger_Status__c && String.isBlank(trigger.new[i].Merchant_Agreement_Envelope_Id__c)) {
                        DocusignProcessor docusignJob = new DocusignProcessor(trigger.new[i].Id);
                        ID jobID = System.enqueueJob(docusignJob);
                        //DocusignProcessor.SubmitContractRequest(trigger.new[i].Id);        
                    }
                }
            }
        }
           //to insert and update merchant in mule dynamo database
        for(Account acc: trigger.new){
        system.debug('acc=='+acc);
            if(acc.type <> null && acc.type =='Merchant' && acc.Onboarding_Stage__c =='Approved' && acc.RMC_Email__c != null && acc.Merchant_User_ID__c == null){
                if ((Trigger.isUpdate && Trigger.oldMap.get(acc.Id).Onboarding_Stage__c != acc.Onboarding_Stage__c) || (Trigger.isInsert)) {
                    userNamePasswordUtilityClass.hitMerchantCreateAPI(acc.id);
                }
            }
            if(acc.type == 'Merchant'){
                string email, isActive;
                if (Trigger.isUpdate && Trigger.oldMap.get(acc.Id).RMC_Email__c!= acc.RMC_Email__c&& acc.RMC_Email__c<> null){
                    email= acc.RMC_Email__c;
                }
                if(Trigger.isUpdate && Trigger.oldMap.get(acc.Id).Active_Partner__c!= acc.Active_Partner__c){
                    isActive=string.valueOf(acc.Active_Partner__c);
                }
                if((email!=null || isActive !=null) && acc.Merchant_User_ID__c <> null)
                    userNamePasswordUtilityClass.updateMerchantTomySql(acc.id, email, isActive,integer.valueOf(acc.Merchant_User_ID__c), acc.name);
           
            }
            
        }
        
    }
    
}