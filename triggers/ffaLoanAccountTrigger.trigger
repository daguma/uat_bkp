trigger ffaLoanAccountTrigger on loan__loan_Account__c (before insert, before update, after update) {
    string llcCompanyName = 'Lendingpoint LLC', speCompanyName = 'Lendingpoint SPE LLC', revTemplateName = 'Loan Account-% Complete';
    boolean bAssetSalesChanged = false;
    System.debug('Loan Trigger');
    Map<Id, loan__Loan_Account__c> mAcc = new Map<Id, loan__Loan_Account__c>();
    Map<String, id> revTemplateMap = new Map<String, id> (), companyMap = new Map<String, id> ();
    Set<Id> mWriteOffList = new Set<Id>();

    List<Opportunity> applist = new List<Opportunity>();

    LoanTriggerHandler lth = new LoanTriggerHandler();
    System.debug('Test trigger ffaLoanAccountTrigger');
    if (Trigger.isUpdate) {
        
        for(loan__loan_Account__c newAcc: trigger.new){
            loan__loan_Account__c oldLoan = Trigger.oldMap.get(newAcc.Id);
            // This following code is important! NEVER remove. It will restore temporary frequency so that the mtro 2 file generates correctly for 28 Day loans
            if (oldLoan.Asset_Sale_Date__c <> newAcc.Asset_Sale_Date__c) {
                bAssetSalesChanged = true;
            }
            if (oldLoan.loan__Charged_Off_Date__c != null &&
                newAcc.loan__Charged_Off_Date__c != null && 
                oldLoan.loan__Charged_Off_Date__c  <> newAcc.loan__Charged_Off_Date__c ) {
                
                newAcc.addError('Once the Charged off date is set, it cannot be modified');
            }

            //if (Trigger.isAfter && !string.IsBlank(newAcc.DMC__c) && oldLoan.DMC__c <> newAcc.DMC__c)
            //       CaseAutomaticAssignmentUtils.assignUserByOverduedaysBucket(newAcc.Id, 60);
        }
        lth.onUpdate(Trigger.New, Trigger.oldmap, Trigger.isBefore);
    }
    if (Trigger.isBefore && (Trigger.IsInsert || bAssetSalesChanged)) {
        //**Start of ffaLoanAccountTrigger Trigger**

        for(c2g__codaCompany__c company:[Select name, id from c2g__codaCompany__c])
        { 
            companyMap.put(company.name, company.id);
        }

        for(ffrr__Template__c template:[Select name, id from ffrr__Template__c])
        { 
            revTemplateMap.put(template.name, template.id);
        }
        System.Debug('********** ffaLoanAccountTrigger - setCompany - CompanyMap'+ companyMap);
            
        for(loan__loan_Account__c newAcc: trigger.new) {
                
            //**Start of ffaLoanAccountTrigger Trigger**
            newAcc.ffrrTemplate__c = revTemplateMap.containsKey(revTemplateName) ? revTemplateMap.get(revTemplateName)  :  null;
            system.debug('Company map Contains SPE' + companyMap.containsKey(speCompanyName));

            if(companyMap.containsKey(llcCompanyName)) newAcc.Company__c = companyMap.get(llcCompanyName);
            if(newAcc.Asset_Sale_Date__c != null)
            {
                system.debug('*** Asset_Sale Date not null');
                if(companyMap.containsKey(speCompanyName)) newAcc.Company__c = companyMap.get(speCompanyName);
            }
            //**End of ffaLoanAccountTrigger Trigger**
        }
        
             //**start of CLContractAfterCreation_Trigger Trigger**
        if (Trigger.isInsert ) {
            lth.onInsert(Trigger.New, Trigger.oldmap, Trigger.isBefore);
        }
        
    }

   

}