//now v2
trigger OppTriggers on Opportunity(after update, before insert, before update) {
    
    // if (Trigger.isBefore && Trigger.isUpdate) TriggerFactory.createandexecuteHandler(OpportunityHandler.class);
    List<Id> apps = new List<Id>();
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    Set<String> FICOIds = new Set<String>();
    Map<String, Opportunity> finalAppUpdMap = new Map<String, Opportunity>();    
    Map<String, String> ApplMap = New Map<String, String>(), appMap = new Map<String, String>();
    Map <String, list<Opportunity> > appFS = new Map <String, list<Opportunity> >();
    Set<ID> creditReportIDs = new Set<ID>(), showHardPullIDs = new Set<ID>();    
    set<ID> productIdSet = new set<ID>(), AccSet = new set<ID>(), contId = new Set<Id>();
    map<id, Lending_Product__c> productMap;
    map<id, Account> accMap;
    RecordType rType = [Select Id from RecordType where SObjectType = 'Opportunity' and DeveloperName = 'LP'];
    Map<Id, Contact> allContacts = new Map<Id, Contact>();    
    
    if(!checkRecursive.isRecursive){    
        if(Trigger.IsUpdate && Trigger.IsBefore) { 
            for (Opportunity app : trigger.new){
                
                productIdSet.add(app.Lending_Product__c);
                AccSet.add(app.Partner_Account__c);
                if (app.AccountId == null && app.Contact__c != null && !contID.contains(app.Contact__c)) contId.add(app.Contact__c);
            }
            allContacts = new Map<id, Contact>([select id, AccountId from Contact where Id in: contId ]);
            productMap = new map<id,Lending_Product__c>([Select Id, Name from Lending_Product__c Where Id IN: productIdSet]);       
            if(productIdSet.contains(Label.ProdIDPoN) || productIdSet.contains(Label.ProdIDSP)) accMap = new map<id,Account>([select id,Use_Promotion_Fields__c,Promotion_Duration__c from Account where id In: AccSet]);
            
        }
        if(Trigger.IsInsert && Trigger.IsBefore) { 
            for (Opportunity app : trigger.new){
                if ( app.Contact__c != null && !contID.contains(app.Contact__c)) contId.add(app.Contact__c);
            }
            allContacts = new Map<id, Contact>([select id, AccountId, Promotion_Active__c, Promotion_Bust_Date__c, Promotion_Expiration_Date__c, Promotion_Grace_Days__c, 
                                                Promotion_Grace_Expiration_Date__c, Promotion_Rate__c, Promotion_Term_In_Months__c, Promotion_Type__c 
                                                from Contact where Id in: contId ]);
            
        }
        //4673 | Possible Product Issue - Start
        
        for (Opportunity app : trigger.new) {
            if (Trigger.IsBefore){
                if (Trigger.IsInsert) {            
                    if(rType != null && app.RecordTypeId==null) app.RecordTypeId = rType.id;
                    app.AssignmentDate__c = (app.AssignmentDate__c == null)?DateTime.now():app.AssignmentDate__c;                        
                    app.Analytic_Random_Number__c = (app.Analytic_Random_Number__c == null)?(math.random() * 99).intValue():app.Analytic_Random_Number__c;
                    if (String.isNotBlank(app.Contact__c) && String.isempty(app.Offer_Code__c)) { //API-313
                        Contact conInfo = allContacts.get(app.Contact__c);
                        // added condition to avoid opportunity fields override after prequal
                        if (conInfo != null && conInfo.Promotion_Active__c) {
                            app.Promotion_Active__c = conInfo.Promotion_Active__c;
                            app.Promotion_Bust_Date__c = conInfo.Promotion_Bust_Date__c;
                            app.Promotion_Expiration_Date__c = conInfo.Promotion_Expiration_Date__c;
                            app.Promotion_Grace_Days__c = conInfo.Promotion_Grace_Days__c;
                            app.Promotion_Grace_Expiration_Date__c = conInfo.Promotion_Grace_Expiration_Date__c;
                            app.Promotion_Rate__c = conInfo.Promotion_Rate__c;
                            app.Promotion_Term_In_Months__c = conInfo.Promotion_Term_In_Months__c;
                            app.Promotion_Type__c = conInfo.Promotion_Type__c;
                        }
                        
                    }
                    if (rType.Id == app.RecordTypeId){
                        if (String.isEmpty(app.Name) || !app.name.contains('APP-')) app.Name = 'APP-' + DateTime.Now();
                        if (app.Type == 'Refinance' && app.First_Refi_Qualified_Date__c == null) app.First_Refi_Qualified_Date__c = Date.today();
                        app.StageName = 'Qualification';
                    }
                    
                }
            }
                        
            if (Trigger.IsUpdate) {            
                Opportunity oldApp = Trigger.oldMap.get(app.Id);
                
                if(Trigger.IsBefore){
                    
                    if (app.Amount != null && app.Amount > oldApp.Amount) { app.Contract_Signed_Date__c = null;} //MER-495

                    if (app.Is_Resumptive_Flow__c <> oldApp.Is_Resumptive_Flow__c && app.Is_Resumptive_Flow__c  ) {
                        app.Resumed_Date__c = Date.today();
                    }
                    if (app.FS_Assigned_To_Val__C  != null && app.FS_Assigned_To_Val__C <> oldApp.FS_Assigned_To_Val__C) {
                        app.Funding_Specialist_Assigned_Date__c = Date.today();
                    }
                    if (app.Preferred_Payment_date__c  != null && app.Preferred_Payment_date__c <> oldApp.Preferred_Payment_date__c) {
                        app.Expected_First_Payment_Date__c = app.Preferred_Payment_date__c;
                    }
                    
                    if (app.Selected_Offer_Term__c <> null) {
                        date frstPaymentDate    = app.Expected_First_Payment_Date__c;
                        if (frstPaymentDate <> oldApp.Expected_First_Payment_Date__c) {
                            app.Effective_APR__c = OfferCtrlUtil.calculateEffectiveAPR_GovComp(app.Expected_Start_Date__c, app.Expected_First_Payment_Date__c, app.Amount, app.Payment_Amount__c, app.Term__c, (app.Payment_Frequency__c == 'WEEKLY' && app.Payment_Frequency_Multiplier__c != null && app.Payment_Frequency_Multiplier__c == 4) ? '28 Days' : app.Payment_Frequency__c  );                  
                            app.Expected_Close_Date__c = (frstPaymentDate <> null) ? (frstPaymentDate.addMonths(Integer.valueOf(app.Selected_Offer_Term__c) - 1)) : frstPaymentDate;
                        }
                    }
                    
                    if (app.AccountId == null && allContacts.containsKey(app.Contact__c)) app.AccountId = allContacts.get(app.Contact__c).AccountId;
                    if(oldApp.Lending_Product__c != app.Lending_Product__c && app.Lending_Product__c != null) 
                        app.ProductName__c = productMap.get(app.Lending_Product__c).Name ;                     
                    if (rType.Id == app.RecordTypeId){
                        if (!String.isEmpty(app.status__c) && app.status__c != oldApp.status__c ) {
                            app.Status_Modified_Date__c = Date.today();
                            app.Status_Modified_Date_Time__c = DateTime.now();
                            if (app.status__c == 'Credit Qualified' && app.Type == 'Refinance') app.Status__c = 'Refinance Qualified';
                            if (app.status__c == 'Declined (or Unqualified)' && app.Type == 'Refinance') app.Status__c = 'Refinance Unqualified';
                            if (app.status__c == 'Credit Qualified' && cuSettings.WEB_API_User_Id__c == UserInfo.getUserId() ) app.OwnerId = cuSettings.WEB_API_User_Id__c;
                            
                            if ('Credit Qualified|Refinance Qualified|Credit Approved - No Contact Allowed'.contains(app.status__c)){
                                app.Last_CQ_Date__c = Datetime.now();
                                app.Dialer_Status__c = null;
                                app.StageName = 'Qualification';
                            }
                            if (!'Aged / Cancellation|Declined (or Unqualified)|Refinance Unqualified'.contains(app.status__c)){
                                app.Decline_Note__c = null;
                            } 
                            if (String.isNotEmpty(app.FICO__c) && app.FICO__c.isNumeric() && String.isNotEmpty(app.Status__c) &&
                                !app.Is_Send_Pricing_Notice__c && app.Type == 'New' && 
                                (String.isEmpty(app.Lead_Sub_Source__c) || app.Lead_Sub_Source__c != 'LoanHero' ) ){
                                    app.Is_Send_Pricing_Notice__c = true;
                                }
                            
                        }
                        if((app.Status__c == 'Refinance Qualified' || (app.Type == 'Multiloan' && app.Status__c == 'Credit Qualified')) && 
                               (oldApp.Last_CQ_Date__c == null || oldApp.Last_CQ_Date__c != app.Last_CQ_Date__c ) ){
                                   app.Qualification_Count__c = (app.Qualification_Count__c != null ? app.Qualification_Count__c : 0) + 1;
                                   
                               }
                        if (String.isNotBlank(app.Opportunity_Number__c) && app.Name != app.Opportunity_Number__c && string.isempty(app.ApplicationId__c)) app.Name = app.Opportunity_Number__c;
                    }
                    
                    app.Encrypted_AppId_AES__c = ( string.isEmpty(app.Encrypted_AppId_AES__c))?ThirdPartyDetails.encrypt(app.id):app.Encrypted_AppId_AES__c;                           
                    if(app.Merchant_Discount_Pct__c != null && app.Amount != null && app.Amount != oldApp.Amount) {
                        app.Merchant_Disbursal_Amount__c = app.Amount * (1 - (app.Merchant_Discount_Pct__c/100));
                        app.Merchant_Discount_Amount__c = app.Amount * (app.Merchant_Discount_Pct__c/100);
                    }
                    if ((generalPurposeWebServices.isPointOfNeed(app.ProductName__c) || generalPurposeWebServices.isSelfPayProduct(app.ProductName__c)) &&
                        (app.Partner_Account__c == null || (accMap <> null && accMap.containskey(app.Partner_Account__c) &&
                                                            (!accMap.get(app.Partner_Account__c).Use_Promotion_Fields__c || 
                                                             accMap.get(app.Partner_Account__c).Promotion_Duration__c > 0))))  {
                                                                 system.debug('---Expected_Start_Date__c ---'+app.Expected_Start_Date__c);
                                                                 if(app.Expected_Start_Date__c != null && app.Expected_Start_Date__c != oldApp.Expected_Start_Date__c && String.isempty(app.Offer_Code__c)) { //API-313
                                                                     // ezVerify_Settings__c EZVerifySettings = CustomSettings.getEZVerifyCustom();
                                                                     decimal noOfDays = (accMap <> null && accMap.containskey(app.Partner_Account__c)) ? accMap.get(app.Partner_Account__c).Promotion_Duration__c : 0;
                                                                     if(noOfDays == null) noOfDays=0;
                                                                     app.Promotion_Expiration_Date__c = app.Expected_Start_Date__c.addDays(integer.valueOf(noOfDays)); //MAINT-914
                                                                 }
                                                             }
                    
                    
                    
                }//4673 | Possible Product Issue - End
                
                
                if (app.Automatically_Assigned__c  && !oldApp.Automatically_Assigned__c ) continue;
                
                if (Trigger.IsAfter) {                
                    
                    if (app.status__c == 'Funded' &&  !String.isEmpty(oldApp.status__c) &&
                        app.status__c != oldApp.status__c) {
                            if (app.Lead_Sub_Source__c == cuSettings.Super_Money_Sub_Source__c)  apps.add(app.Id);
                            if(!System.isFuture() && !System.isBatch()) {
                                userNamePasswordUtilityClass.sendUserToCustomerPortal(app.id);
                            }else{
                                userNamePasswordUtilityClass.sendUserToCP(app.id);
                            }
                        }
                    
                    
                    // #2847 - Refinance & Customer Retention - Credit Review Rules:
                    if (oldApp.status__c != app.status__c && app.status__c == 'Documents In Review' && !app.Fico_Reviewed__c) {
                        if (app.FICO__c <> null && Integer.valueOf(app.FICO__c) >= 720 && (app.Type == 'New'))
                            FICOIds.add(app.ID);
                        appMap.put(app.Id, app.Contact__c);                    
                    }
                    
                    //OFAC Call when Status change (Only if the last record is older than 30 days). 
                    if (app.status__c != oldApp.status__c && (app.status__c == 'Offer Accepted' || 
                                                              app.status__c == 'Documents In Review' || 
                                                              app.status__c == 'Credit Qualified') ) { 
                                                                  
                                                                  BridgerSFLookup.callBridgerWhenChangeStatus(app.Contact__c);   
                                                              }
                    
                    if(app.Status__c == 'Refinance Qualified' && app.status__c != oldApp.status__c){
                        Integer refiOppsCnt = 1;
                        Set<Id> Ids = new Set<Id>();
                        Contact con = [SELECT Id, Refinance_Count__c FROM Contact WHERE Id = :app.Contact__c];                        
                        List<Opportunity> refiOpps = [SELECT Id FROM Opportunity WHERE Contact__c = :con.Id AND Type = 'Refinance'];
                        if(refiOpps.size() >0) {
                            for(Opportunity refiOpp : refiOpps){
                                Ids.add(refiOpp.Id);
                            }
                            
                            for (Id eachId : Ids){
                                List<OpportunityFieldHistory> oppfields = [Select OpportunityID, NewValue From OpportunityFieldHistory Where Field in ('Status__c') and OpportunityId = :eachId];
                                for(OpportunityFieldHistory oppfield: oppfields){
                                    if( oppfield.NewValue=='Refinance Qualified' && app.Id <> oppfield.OpportunityId){
                                        refiOppsCnt++;
                                        break;
                                    }
                                }                    
                                
                            }  
                            con.Refinance_Count__c = refiOppsCnt;
                            update con;
                        }
                    }

                    if ( String.isNotEmpty(oldApp.status__c) &&
                         (app.status__c != oldApp.status__c) &&
                          (app.status__c == 'Funded' ||
                            app.status__c == 'Declined (or Unqualified)' ||
                            app.status__c == 'Refinance Unqualified' ||
                            app.status__c == 'Contract Package Sent' ||
                            app.status__c == 'Approved, Pending Funding') ) {

                        SalesForceUtil.lookForCasesToClose(app.Id, app.Contact__c);
                    }
                }                      
            }
        }
        //OF-631
        if (Trigger.IsBefore && Trigger.IsUpdate) {        
            map<string,opportunity> mapfinalAppUpd = RefinanceHandler.refinaceCopyRec(Trigger.New, Trigger.OldMap);
            if(!mapfinalAppUpd.isEmpty()) finalAppUpdMap.putAll(mapfinalAppUpd);     
        }
        
        if (Trigger.IsAfter && Trigger.IsUpdate) {        
            
            if (appMap <> null && appMap.values().size() > 0) {
                
                Map<String, String> lg_map = new Map<String, String>(), ido_map = new Map<String, String>();
                Integer scoreLimit = Integer.valueOf(Label.KountScoreLimit);
                
                //For getting the latest Kount Score.
                String score;
                for (Logging__c lc : [Select id, CreatedDate, name, api_response__c, Webservice__c, contact__c
                                      FROM Logging__c
                                      WHERE (contact__c in: appMap.values() AND Webservice__c = 'Kount')
                                      ORDER BY CreatedDate DESC
                                      LIMIT 50000]) {
                                          if (!(lg_map.containsKey(lc.contact__c)) && lc.api_response__c != null) {
                                              score = '';
                                              
                                              for (String st : lc.api_response__c.split(' ')) {
                                                  if (st.contains('SCOR') && st.contains('=')) {
                                                      score = st.split('=')[1];
                                                      break;
                                                  }
                                              }
                                              
                                              
                                              lg_map.put(lc.contact__c, score);
                                          }
                                      }
                
                
                System.debug('lg_map value ' + lg_map);
                
                //For getting the latest Qualifiers
                for (IDology_Request__c idr : [Select id , CreatedDate, name, Qualifiers__c, contact__c
                                               FROM IDology_Request__c
                                               WHERE contact__c in: appMap.values()
                                               
                                               ORDER BY CreatedDate DESC
                                               LIMIT 50000]) {
                                                   System.debug('IDO ' + idr.contact__c + ' Qualifiers ' + idr.Qualifiers__c);
                                                   System.debug('IDO If ' + (!(ido_map.containsKey(idr.contact__c)) && String.isNotEmpty(idr.Qualifiers__c)));
                                                   if (!(ido_map.containsKey(idr.contact__c)) && String.isNotEmpty(idr.Qualifiers__c))
                                                       ido_map.put(idr.contact__c , idr.Qualifiers__c.replaceAll('<li>', '').replaceAll('</li>', ''));                   
                                               }
                
                
                System.debug('IDO_MAP value' + ido_map);
                String qualif;
                Integer kount_scr;
                Opportunity gapp;
                
                for (String appId : appMap.keyset()) {
                    qualif = '';
                    if (lg_map.containsKey(appMap.get(appId)) && ido_map.containsKey(appMap.get(appId))) {
                        qualif  = ido_map.get(appMap.get(appId));
                        System.debug('Kount score from the API ' + lg_map.get(appMap.get(appId)).trim());
                        kount_scr = Integer.valueof(lg_map.get(appMap.get(appId)).trim());
                        
                        if (kount_scr > scoreLimit && (qualif.contains('Address Location Alert') || qualif.contains('Address Velocity Alert') ||
                                                       qualif.contains('Address Longevity Alert') || qualif.contains('Newer Record Found') ||
                                                       qualif.contains('Input Address is a PO Box') || qualif.contains('SSN Not Available') ||
                                                       qualif.contains('Data Strength Alert') || qualif.contains('Bankruptcy Found') ||
                                                       qualif.contains('Warm Address Alert') ||  qualif.contains('Address Stability Alert') ||
                                                       qualif.contains('DOB/YOB Not Available'))) {
                                                           
                                                           gapp = new Opportunity (ID = appId, Review_Reason__c = 'Identify/KYC',
                                                                                   status__c = 'Credit Review');
                                                           
                                                           If(!finalAppUpdMap.containsKey(appId)) {
                                                               system.debug('finalAppUpdMap kount.Ido ===========' + finalAppUpdMap);
                                                               finalAppUpdMap.put(appId, gapp);
                                                               system.debug('finalAppUpdMap kount.Ido after put ===========' + finalAppUpdMap);
                                                           }
                                                       }
                        
                        
                    }
                    System.debug('kount_scr ' + kount_scr + ' Qualifiers ' + qualif  + ' for contact ' + appMap.get(appId));
                }
            }
            
            //        if (apps.size() > 0 ) {
            //            MelisaCtrl.notifySuperMoney(apps);
            //        }
            
            if (!FICOIds.isempty()) {
                Opportunity gapp;
                String existingRevReason;
                for (String apid : FICOIds) {
                    gapp = new Opportunity (ID = apid, status__c = 'Credit Review', Review_Reason__c = 'Credit Profile');
                    
                    If(finalAppUpdMap.containsKey(apid)) {
                        existingRevReason = finalAppUpdMap.get(apid).Review_Reason__c;
                        gapp.Review_Reason__c = existingRevReason + ',' + gapp.Review_Reason__c;
                        gapp.status__c = finalAppUpdMap.get(apid).status__c;
                        finalAppUpdMap.remove(apid);
                        system.debug('finalAppUpdMap contains apid ===========' + finalAppUpdMap);
                    }
                    finalAppUpdMap.put(apid, gapp);
                    
                }
            }
            
            
            system.debug('finalAppUpdMap ===========' + finalAppUpdMap);
            try {
                If(!finalAppUpdMap.isEmpty()){
                    checkRecursive.isRecursive = true;
                    update finalAppUpdMap.values();
                }     
            } catch (Exception e) {
                system.debug('Exception while update ====' + e.getMessage());
            }
            
        }
        //**End of Application_AfterUpdate trigger*
        
        if (trigger.IsInsert && trigger.IsAfter){
            for (Opportunity app : trigger.new) {
                if ( app.status__c == 'Funded' ||
                      app.status__c == 'Declined (or Unqualified)' ||
                       app.status__c == 'Refinance Unqualified' ||
                        app.status__c == 'Contract Package Sent' ||
                         app.status__c == 'Approved, Pending Funding') {

                    SalesForceUtil.lookForCasesToClose(app.Id, app.Contact__c);
                }    
            }   
        }
    } 
}