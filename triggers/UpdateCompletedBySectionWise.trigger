trigger UpdateCompletedBySectionWise on Employment_Details__c (before insert, before update) {

  system.debug('==========trigger.new============'+trigger.new);
   if(Trigger.IsInsert){
       EmploymentDetailhandler.onInsert(trigger.new);
   }
   
   
   if(Trigger.IsUpdate){    
       EmploymentDetailhandler.onUpdate(trigger.oldMap,trigger.new);
   }
   
   }