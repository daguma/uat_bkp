trigger ConditionalSpreadLPT on loan__Loan_Payment_Transaction__c (before insert) {

    if(trigger.isBefore)
    {
        if(trigger.isInsert)
        {
            conditionalSpreadLPTTriggerHandler triggerHandler = new conditionalSpreadLPTTriggerHandler();
            triggerHandler.decideSpread(trigger.new);
        }
    }
    
}