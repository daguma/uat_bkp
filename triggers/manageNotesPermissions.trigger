trigger manageNotesPermissions on Note (before update, before delete) {
    
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    
    if (trigger.isDelete){
        for (Note noteRow : trigger.old) {
            boolean bCanNotDeleteNotes = (cuSettings.Delete_Note_Restriction_By_ProfileId__c != null && cuSettings.Delete_Note_Restriction_By_ProfileId__c.contains(UserInfo.getProfileId()));

            if ( bCanNotDeleteNotes ) { 
                noteRow.addError('Insufficient Privileges To Delete Note Records');
            }
            else {}
        }
    }
    
    boolean bCanBeOverriden =  (cuSettings.Offer_Selection_Override_Profiles__c != null && cuSettings.Offer_Selection_Override_Profiles__c.contains(UserInfo.getProfileId()));
    if (!bCanBeOverriden) {
        if (Trigger.isUpdate) {
            for (Note noteRow : trigger.new) {
                boolean bCanEditNotes = (cuSettings.Edit_Note_Override_Profiles__c != null && cuSettings.Edit_Note_Override_Profiles__c.contains(UserInfo.getProfileId()));
                
                String NewParentId = String.valueof(noteRow.parentid);
                String OldParentId  = trigger.oldmap.get(noteRow.id).parentid;
                String OldParentObject =  Id.valueOf(OldParentId).getSObjectType().getDescribe().getName();
                String NewParentObject =  Id.valueOf(NewParentId).getSObjectType().getDescribe().getName();

                if ( (bCanEditNotes) || (NewParentId != OldParentId && OldParentObject == 'Lead' && NewParentObject == 'Contact') ) { }
                else {
                    noteRow.addError('Insufficient privileges to edit/delete Note records');
                }
            }
        }
    }
}