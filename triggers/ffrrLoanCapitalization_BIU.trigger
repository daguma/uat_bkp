/****************************************************************************************
Name            : ffaUtilities
Author          : CLD
Created Date    : 3/7/17
Description     : Trigger to set the Rev Rec Template on the Loan Capitalization record(s)
*****************************************************************************************/

trigger ffrrLoanCapitalization_BIU on Loan_Capitalization__c (before insert) 
{
	ffrrRevMgmtHandler.setRevRecTemplates(Trigger.New); 
}