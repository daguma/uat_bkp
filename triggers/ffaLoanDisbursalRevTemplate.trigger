trigger ffaLoanDisbursalRevTemplate on loan__Loan_Disbursal_Transaction__c (before insert, before update) {
	
	string revTemplateName = 'Loan Disbursal-One Time Entry';
	
	Map<String, id> revTemplateMap = new Map<String, id> ();
	for(ffrr__Template__c template:[Select name, id from ffrr__Template__c])
	{ 
		revTemplateMap.put(template.name, template.id);
	}
	

	for(loan__Loan_Disbursal_Transaction__c loanDisbursal: trigger.new)
	{
		
		loanDisbursal.ffrrTemplate__c = revTemplateMap.containsKey(revTemplateName) ? revTemplateMap.get(revTemplateName)  :  null;
		
}
}