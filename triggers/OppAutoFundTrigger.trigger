trigger OppAutoFundTrigger on Opportunity(after update) {
    list<Opportunity> gAppToUpdate = new list<Opportunity>();
    List<Id> appIds = new List<Id>();
    string retMsg;
    try{
        System.debug('Funding Automation! Apps to process ' + appIds.size());
        for(Opportunity gApp : trigger.new){

            System.debug('Current Status = ' + gApp.Status__c + ' New Status = ' + trigger.oldmap.get(gApp.ID).Status__c);
            if(gApp.Status__c == 'Funded' && gApp.Lending_Account__c==NULL &&
                    trigger.oldmap.get(gApp.ID).Status__c <> gApp.Status__c
                    ){
                system.debug('CONDITIONS ME********T');

                gApp.Expected_Disbursal_Date__c= system.today();
                CallContract.ContractCreation(gApp.ID, string.valueOf(gApp.Expected_Disbursal_Date__c));

            } else
                    system.debug('CONDITIONS NOT MET************');

        }



    }
    catch(exception e)
    {
        system.debug('Exception1->'+e.getLineNumber());
        system.debug('Exception2->' + e.getMessage());
    }
    // if(gAppToUpdate != null)
    //    update gAppToUpdate;


}