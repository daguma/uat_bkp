trigger PaymentTransactionTrigger on loan__Loan_Payment_Transaction__c (before insert, before update) {
    System.debug('loan_Payment_Transaction_trigger STARTED');
   	if (Trigger.isBefore && Trigger.isInsert) {
       PaymentHandler.onInsert(Trigger.New);
    }
   	if (Trigger.isUpdate) { 
        PaymentHandler.onUpdate(Trigger.New, Trigger.Old);
    }
    System.debug('loan_Payment_Transaction_trigger ENDED');
}