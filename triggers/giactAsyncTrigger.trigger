trigger giactAsyncTrigger on GIACT__c (after insert) {
    //not bulk supported
    if (trigger.newMap.keySet().size() > 1)
        return;

    for(GIACT__c item : Trigger.new){
        GiactProcessor.SubmitGiact(item.Id);
    }     
}