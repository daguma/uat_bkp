trigger TaskAfterCreate on Task (after insert, before delete) {
    try {
        if (trigger.isInsert){
        	for (Task t : Trigger.new) {
                if (t.Subject.contains('Call') && t.AccountId != null) {
                    for (Contact c : [SELECT AccountId, TotalCallsMade__c, LastCallMade__c, TotalCallsMadeSinceLastStatusChange__c FROM Contact WHERE AccountId = : t.AccountId LIMIT 1]) {
                        c.TotalCallsMade__c = 0;
                        c.LastCallMade__c = Datetime.now();
                        for (AggregateResult total : [SELECT Count(Id) total  FROM Task WHERE Subject LIKE '%Call%' AND AccountId  = : c.AccountId  AND CreatedDate = LAST_N_DAYS:30 GROUP BY AccountId]) {
                            c.TotalCallsMade__c = (Integer)total.get('total');
                        }
                        //SM13 increment the TotalCallsMadeSinceLastStatusChange__c by one everytime a "Call" type task is created
                        c.TotalCallsMadeSinceLastStatusChange__c = c.TotalCallsMadeSinceLastStatusChange__c == null ? 1 : c.TotalCallsMadeSinceLastStatusChange__c + 1;
                        update c;
                    }
                }
            }
    	}
        if (trigger.isDelete){
            LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
            
            for (Task taskRow : trigger.old) {
                boolean bCanNotDeleteTasks = (cuSettings.Delete_Restrictions__c != null && cuSettings.Delete_Restrictions__c.contains(UserInfo.getProfileId()));
                if ( bCanNotDeleteTasks ) { 
                    taskRow.addError('Insufficient Privileges To Delete Task Records');
                }
            }
        }
        
        //Update_Test_Ob_dummy.updateTestBO_dummy();
    } catch (Exception e) {
        System.debug('TaskAfterCreate Exception = ' + e);
    }
    
}