trigger AttachmentTrigger on Attachment (before insert) {
    
    //VARIABLE DECLARATION
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    Set<Id> oppIds = new Set<Id>();
    Map<Id, List<String>> oppIdDocTypeListMap = new Map<Id, List<String>>();
    List<Id> deleteAttIds = new List<Id>();
    set<id> merchantInvoiceSet= new set<id>();
    
    //HANDLING ATTACHMENTS WHICH HAS BEEN UPLOADED BY WEB INTERFACE USING API
    if(Trigger.isInsert && Trigger.isBefore) {
        for(Attachment att : Trigger.new) {
            String Descrp = att.Description;
            
            if(String.isNotBlank(Descrp) && Descrp.containsIgnoreCase('File from APIautoworkflow')) {

                if(Descrp.containsIgnoreCase('Merchant Invoice')) {
                    merchantInvoiceSet.add(att.ParentId);
                }
                //IN BELOW CONDITION "DESCRIPTION" WILL CONTAIN AN ID OF ATTACHMENT WHICH NEEDS TO BE DELETED
                if(Descrp.containsIgnoreCase('Delete AttachmentId:')) {
                    String deleteAttachmentId = Descrp.substringAfter('Delete AttachmentId:');
                    
                    if(!deleteAttIds.contains(deleteAttachmentId.trim()))
                        deleteAttIds.add(deleteAttachmentId.trim());
                       
                    att.Description = Descrp.substringBefore('Delete AttachmentId:').trim();
                }
                
                String docType = Descrp.substringAfter('File from APIautoworkflow:').trim();
                
                if(String.isNotBlank(docType)) {
                    if(!oppIdDocTypeListMap.containsKey(att.ParentId))
                        oppIdDocTypeListMap.put(att.ParentId, new List<String>());
                    
                    List<String> docTypeList = oppIdDocTypeListMap.get(att.ParentId);
                    docTypeList.add(docType);
                    
                    oppIdDocTypeListMap.put(att.ParentId, docTypeList);
                }
                oppIds.add(att.ParentId);
            }
            
            for(EmailMessage email : [SELECT ParentId FROM EmailMessage WHERE Id =: att.ParentId AND ToAddress = 'docs@lendingpoint.com' LIMIT 1]) {
                for(Case caseFromEmail : [SELECT Opportunity__c FROM Case WHERE Id =: email.ParentId AND Opportunity__c != NULL LIMIT 1]) {
                    att.ParentId = caseFromEmail.Opportunity__c;
                }
            } 
        }
        
        if(oppIds.size() > 0) {
            Set<Id> reassignAppIds = new Set<Id>();
            List<Opportunity> oppList = [SELECT Id, Income_Source__c, Status__c, OwnerId, Name, Contact__c, 
                                         Contact__r.AccountId, Contact__r.Employment_Type__c,Contact__r.Second_Income_Type__c, 
                                         All_Applicants_XML__c, Employed_Applicants_1_XML__c, Employed_Applicants_2_XML__c, 
                                         Retired_Applicants_XML__c, Self_Employed_Applicants_1_XML__c, Merchant_Invoice_Received__c,
                                         Self_Employed_Applicants_2_XML__c, Miscellaneous_Applicants_XML__c, Applicants_Doc_Code__c  
                                         FROM Opportunity WHERE Id IN : oppIds];
            
            for(Opportunity opp : oppList) {
                
                switch on opp.Status__c {
                    when 'Offer Accepted' {
                        opp.Status__c = label.Document_App_Status;
                    }
                    when 'Credit Approved - No Contact Allowed' {
                        opp.Status__c = 'Offer Accepted';
                        if(opp.OwnerId == cuSettings.WEB_API_User_Id__c)
                            reassignAppIds.add(opp.Id);
                    }
                }
                
                if(oppIdDocTypeListMap.containsKey(opp.Id)) {
                    for(String docTypeStr : oppIdDocTypeListMap.get(opp.Id)) {                    
                        if(docTypeStr.containsIgnoreCase('Signed Contract'))
                            DocusignService.NoteOnNewContractAfterRegrade(opp.Id);
                        system.debug('---docTypeStr ----'+docTypeStr );
                        //REMOVE DISPOSITION LOGIC
                        opp = applyDashboardService.removeDispositionsOnReUpload(docTypeStr, opp);                        
                    }
                }
                
                if(merchantInvoiceSet.contains(opp.id)){
                    opp.Merchant_Invoice_Received__c = true;
                }
            }
            
            update oppList;
            
            //DEAL AUTOMATIC ASSIGNMENT LOGIC
            if(reassignAppIds.size() > 0) {
                for(Id oppId : reassignAppIds) {
                    DealAutomaticAssignmentManager.assignExistingAppFromWeb(oppId);
                }
            }
            
            if(deleteAttIds.size() > 0) {
                Database.delete(deleteAttIds);
            }
        }
    }
    
}