trigger AttachmentFromDocusignBeforeTrigger on Attachment (after insert) {
   
    List<Attachment>  newAtts = new List<Attachment>(), tDet = new List<Attachment>();
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    Map<Id, Id> mDocs = new Map<Id, Id>();
    set<id> delAttIds = new set<id>();

    for (Attachment att : Trigger.New){
        if (att.ParentId.getSobjectType() == dsfs__DocuSign_Status__c.SobjectType && !mDocs.containsKey(att.ParentId)) mDocs.put(att.ParentId, null);
    }
    
    System.debug('Docusign Documents Inserted = ' + mDocs.Size());
    
    if (mDocs.size() > 0) {
        List<dsfs__DocuSign_Recipient_Status__c> docs = [Select Name, LastActivityDate, dsfs__Contact__c, dsfs__Recipient_Status__c, dsfs__Account__c, Id, dsfs__Parent_Status_Record__c  from dsfs__DocuSign_Recipient_Status__c where dsfs__Parent_Status_Record__c in: mDocs.keySet()];
        System.debug('Recipients Found ' + docs.size());
        for (dsfs__DocuSign_Recipient_Status__c doc : docs) {
            if (mDocs.containsKey(doc.dsfs__Parent_Status_Record__c )) mDocs.remove(doc.dsfs__Parent_Status_Record__c);
            if (doc.dsfs__Contact__c != null) mDocs.put(doc.dsfs__Parent_Status_Record__c , doc.dsfs__Contact__c);
            System.debug('Put ContactID ' + doc.dsfs__Contact__c + ' for Parent = ' + doc.dsfs__Parent_Status_Record__c);
        }    
        
        for (Attachment att : Trigger.New){
            // Find Docusign Inserts
            if (mDocs.containsKey(att.ParentID)) {
                System.debug('Found a Docusign Doc for this. ContactID = ' + mDocs.get(att.ParentID));
                List<Opportunity> apps = [Select Id, Name, Contact__r.LastName, Contact__r.FirstName from Opportunity where Contact__c =: mDocs.get(att.ParentID) AND Name LIKE 'APP%' Order by CreatedDate desc LIMIT 1];
                List<loan__Loan_Account__C> loans = [Select Id, Signed_Agreement_Received__c, Agreement_Modified_Date__c, Contract_Modification_Reason__c, Name, loan__Contact__r.LastName, Loan__Contact__r.FirstName from loan__Loan_Account__C where loan__Contact__c =: mDocs.get(att.ParentID) Order by CreatedDate desc LIMIT 1 ];
                if (apps.size() > 0) {
                    boolean isLoanMod =  loans.size() > 0 && att.name.toLowerCase().contains('modification');
                    boolean isWorkOutDoc =  loans.size() > 0 && (att.name.toLowerCase().deleteWhitespace().contains('workoutagreement'));
                    Attachment newAtt = new Attachment();
                    newAtt.ParentId = (isLoanMod || isWorkOutDoc) ? loans[0].id : apps[0].Id;
                    newAtt.Body = att.Body;
                    newAtt.ContentType = att.ContentType;
                    newAtt.Description = att.Description;
                    newAtt.OwnerId = att.OwnerId;
                    newAtt.Name = (isLoanMod ? 'Loan Modification.' :(isWorkOutDoc)?'LendingPoint Loan Workout Agreement – Change in Number of Months, APR, & Monthly Payment Amount.': 'Signed Contract.') + att.name.substringAfterLast('.');
                    newAtts.add(newAtt);
                   // tDet.add(att);
                    delAttIds.add(att.id);
                    System.debug('Apps/Loans found ' + newAtt.ParentId);
                    System.debug('isWorkOutDoc==================== ' + isWorkOutDoc);
                    if (!isLoanMod && !isWorkOutDoc) {
                        DocusignService.NoteOnNewContractAfterRegrade(apps[0].id);
                        Opportunity appln = new Opportunity(id= apps[0].Id,Contract_Signed_Date__c=date.today(),Contract_Received_Through__c='Docusign');
                        update appln;
                        WebToSFDC.notifyDev('A signed Contract has been received for ' + apps[0].Name + ' - ' + apps[0].Contact__r.FirstName + ' ' + apps[0].Contact__r.LastName, ' A customer(' + apps[0].Contact__r.FirstName + ' ' + apps[0].Contact__r.LastName + ') has just signed their contract. \n\n Please follow this link to reach the app: \n \n https://' + System.Url.getSalesforceBaseUrl().getHost() + '/' + apps[0].Id , cuSettings.Docusign_Notification_Email__c);
                    
                    } else if (isWorkOutDoc && loans[0].Signed_Agreement_Received__c == false) {
                        loans[0].Signed_Agreement_Received__c = true;
                        update loans[0];
                        WebToSFDC.notifyDev('A signed Work Agreement has been received for ' + loans[0].Name + ' - ' + loans[0].loan__Contact__r.FirstName + ' ' + loans[0].loan__Contact__r.LastName, ' A customer(' + loans[0].loan__Contact__r.FirstName + ' ' + loans[0].loan__Contact__r.LastName + ') has just signed their Workout Agreement. \n\n Please follow this link to reach the Contract: \n \n https://' + System.Url.getSalesforceBaseUrl().getHost() + '/' + loans[0].Id , cuSettings.Docusign_Notification_Email__c);
                    
                    }else if(isLoanMod){
                        WebToSFDC.notifyDev('A signed Loan Modification has been received for ' + loans[0].Name + ' - ' + loans[0].loan__Contact__r.FirstName + ' ' + loans[0].loan__Contact__r.LastName, ' A customer(' + loans[0].loan__Contact__r.FirstName + ' ' + loans[0].loan__Contact__r.LastName + ') has just signed their Loan Modification. \n\n Please follow this link to reach the Contract: \n \n https://' + System.Url.getSalesforceBaseUrl().getHost() + '/' + loans[0].Id , cuSettings.Docusign_Notification_Email__c);
                    }                
                }            
            } 
        }
        system.debug(newAtts);
        
        tDet = [select id from attachment where id IN: delAttIds];
        if (newAtts.size() > 0) insert newAtts;
        try {if(tDet.size() > 0) delete tDet;}  catch (Exception e) {}        
    }
}