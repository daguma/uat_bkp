trigger UpdateSumOnOpportunity on Provider_Payments__c (after insert, after update, after delete) {

    Set<id> oppIds = new Set<id>();
    List<Opportunity> oppsToUpdate = new List<Opportunity>();

try{
    if (!Trigger.isDelete){
        for (Provider_Payments__c paym : Trigger.new){
            if(Trigger.isInsert || (Trigger.isUpdate && paym.Payment_Amount__c <> Trigger.oldMap.get(paym.id).Payment_Amount__c))
                oppIds.add(paym.Opportunity__c);
        }
    
    }else{   
        for (Provider_Payments__c paym : Trigger.old)
            oppIds.add(paym.Opportunity__c);
    }
    
    Map<id,Opportunity> OppMap = new Map<id,Opportunity>([select id, Provider_Payment_s_Sum__c from Opportunity where id IN :oppIds]);    
    
    for (AggregateResult pay : [select SUM(Payment_Amount__c)paySum, Opportunity__c op from Provider_Payments__c where Opportunity__c IN :oppIds group by Opportunity__c]) {
        OppMap.get(string.valueOf(pay.get('op'))).Provider_Payment_s_Sum__c = (pay.get('paySum') <> null)?decimal.valueOf(string.valueOf(pay.get('paySum'))):0 ;               
        oppsToUpdate.add(OppMap.get(string.valueOf(pay.get('op'))));
    }

    if (Trigger.isDelete){
        for (Opportunity noPay : [select id,Provider_Payment_s_Sum__c  from  Opportunity where id IN :oppIds AND id NOT IN (select Opportunity__c from Provider_Payments__c)]) {
            noPay.Provider_Payment_s_Sum__c  = 0;
            oppsToUpdate.add(noPay);
        }
    }
    //==MER-609==
    if(Trigger.isInsert){
        List<id> OppList= new List<id>();
        for(Provider_Payments__c prov : [select id, Opportunity__c, Opportunity__r.status__c, Provider_Name__c, Opportunity__r.Partner_Account__c, Opportunity__r.amount, Opportunity__r.Partner_Account__r.Automate_Split_Pay_Funding__c, Opportunity__r.Partner_Account__r.Enable_Split_Pay__c from Provider_Payments__c where id IN: trigger.new] ){
        system.debug('==Provider Payment=='+ prov);
            system.debug('==prov.Opportunity__r.Partner_Account__r.Automate_Split_Pay_Funding__c=='+prov.Opportunity__r.Partner_Account__r.Automate_Split_Pay_Funding__c);
            system.debug('==Provider Name=='+prov.Provider_Name__c);
            system.debug('==prov.Opportunity__r.Partner_Account__c=='+prov.Opportunity__r.Partner_Account__c);
            system.debug('==Opportunity=='+prov.Opportunity__c);
            system.debug('==prov.Opportunity__r.Partner_Account__r.Enable_Split_Pay__c=='+prov.Opportunity__r.Partner_Account__r.Enable_Split_Pay__c);
            if(prov.Opportunity__c <> null && prov.Opportunity__r.Partner_Account__c <> null && prov.Opportunity__r.status__c == 'Approved, Pending Funding' && prov.Opportunity__r.Partner_Account__r.Automate_Split_Pay_Funding__c && prov.Opportunity__r.Partner_Account__r.Enable_Split_Pay__c && prov.Provider_Name__c <> prov.Opportunity__r.Partner_Account__c){ 
               OppList.add(prov.Opportunity__c);   
            }
        }
        if(!OppList.isEmpty()){
            generalPurposeWebServices.fundAppAsync(OppList);
        }
    }
    
    update oppsToUpdate;

 }catch(Exception e){
     system.debug('====excep ================'+e.getmessage());
 }

}