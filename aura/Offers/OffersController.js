//OffersController
({
    doInit : function(component, event, helper) {
        helper.initialize(component);
        helper.fetchPickListVal(component, 'Fee_Handling__c');
        helper.fetchPickListVal(component, 'RepaymentFrequency__c'); 
    },
    finwiseAction: function (component, event, helper) {
        var opp = component.get("v.urOfferEntity.application");
        var recordId = component.get("v.recordId");
        var state = opp.State__c;
        
        var action = component.get("c.callFinwise");
        action.setParams({
            "recordId": recordId,
            "appState": state
        });
        
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(response.getReturnValue());
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var warnings = component.get("v.warnings");
                warnings.push(result);
                component.set("v.warnings", warnings);
                     $A.get('e.force:refreshView').fire();
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                component.set("v.hasErrorMsg", true);
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    processingQuestions: function(component, event, helper) {
        var action = component.get("c.processQuestions"); 
        
        action.setParams({
            'EntityStr': JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();  
            console.log(response.getReturnValue());
            if (state === "SUCCESS") {
                console.log('Questions successfully processed!');
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
            } else if(state === 'ERROR') {
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            }
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    reGrade: function(component, event, helper) {
        if(confirm('Are you sure you want to re-grade this application ? If so, please remember to confirm we have the correct Income Type option and employment start date details (for Employed applicants) before completing the re-grade.')) {
            component.set("v.showSpinner", true);
            var actionReGrade = component.get("c.GetGrade");
            
            actionReGrade.setParams({
                'EntityStr': JSON.stringify(component.get("v.urOfferEntity"))
            });
            
            actionReGrade.setCallback(this, function(response) {
                component.set("v.showSpinner", false);
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    component.set("v.urOfferEntity", response.getReturnValue());
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    console.log("Failed with state: " + state);
                }
                component.set("v.showSpinner", false);
            });
            
            // Send action off to be executed
            $A.enqueueAction(actionReGrade);
        }
    },
    reloadUroEntity: function(component, event, helper) {
        var jsonEntityString = event.getParam("uroEntityString"); 
        var jsonEntityObject = JSON.parse(jsonEntityString);
        component.set("v.urOfferEntity", jsonEntityObject);
        component.set("v.warnings", jsonEntityObject.warningMsgs); 
        component.set("v.infos", jsonEntityObject.infoMsgs); 
    },
    selectOfferAction: function(component, event, helper) {
        var offer = event.getParam("offerEntity"); 
        var action = component.get("c.selectOffer");
        
        action.setParams({
            "offer": offer,
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {       
                var result = response.getReturnValue();
                
                if(!$A.util.isEmpty(result.popupMessage)) {
                    var message = result.popupMessage;
                    if(message.includes("WARNING")) {
                        message = message.replace("WARNING :", "").trim();
                        helper.showToastAlert("Warning!", message, "warning", 10000);
                    }else {
                        message = message.replace("ERROR : ", "").trim();
                        helper.showToastAlert("Error!", message, "error", 10000);
                    }
                }
                
                result.popupMessage = '';
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
                
                $A.get('e.force:refreshView').fire();
            }else {
                helper.parseError(response);
            }
            component.set("v.showSpinner", false);
        })     
        $A.enqueueAction(action);
    },
    deselectOfferAction : function(component, event, helper) {
        var offer = event.getParam("offerEntity"); 
        var action = component.get("c.deselectOffer");
        
        action.setParams({
            "offer": offer,
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {       
                var result = response.getReturnValue()
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
                     $A.get('e.force:refreshView').fire();
            }else {
                helper.parseError(response);
            }
        })     
        $A.enqueueAction(action);
    },
    deleteOfferAction : function(component, event, helper) {
        var offer = event.getParam("offerEntity"); 
        var action = component.get("c.deleteOffer");
        
        action.setParams({
            "offer": offer,
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                this.showToastAlert("Success!", 'Offer deleted successfully', "success", 1000);
                var result = response.getReturnValue()
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
                     $A.get('e.force:refreshView').fire();
            }else {
                helper.parseError(response);
            }
        })     
        $A.enqueueAction(action);
    }
})