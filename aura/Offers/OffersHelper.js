//OffersHelper
({
    initialize : function(component) {
        component.set("v.showSpinner", true);
        var action = component.get("c.getUnderWriterOfferEntity");
        action.setParams({
            "appId": component.get("v.recordId")
        });
action.setBackground();
        var opts = [];
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.feeHandlingVal", result.application.Fee_Handling__c);
                component.set("v.repaymentFreqVal", result.application.RepaymentFrequency__c);
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
                component.set("v.infos", result.infoMsgs);
                //Disqualifier_Scorecard component data
                var disqualifierScorecardEntity = new Object();
                disqualifierScorecardEntity.decision = result.decisionString;
                if(!$A.util.isEmpty(result.score)){
                    disqualifierScorecardEntity.acceptanceSP = result.score.Acceptance__c;
                    disqualifierScorecardEntity.acceptanceHP = result.score.Acceptance_Hard_Pull__c;
                    disqualifierScorecardEntity.grade = result.score.Grade__c;
                    disqualifierScorecardEntity.DTI = result.score.DTI__c;
                    disqualifierScorecardEntity.FICO = result.score.FICO_Score__c;
                    disqualifierScorecardEntity.RevUtilization = result.score.Revolving_Utilization__c;
                }
                if(!$A.util.isEmpty(result.application)){
                    disqualifierScorecardEntity.permissionToHP = result.application.Permission_to_Get_Hard_Pull__c == true ? 'T' : 'F'; // MER-728
                    disqualifierScorecardEntity.salesStrategy = result.application.StrategyType__c;
                }
                component.set("v.disqualifierScorecardEntity", disqualifierScorecardEntity);

            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                component.set("v.hasErrorMsg", true);
            }
            component.set("v.showSpinner", false);
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    fetchPickListVal: function(component, fieldName) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": { 'sobjectType': 'Opportunity' },
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                if(fieldName == "Fee_Handling__c"){
					component.set("v.options", opts);
				}else{
					component.set("v.repOptions", opts);
				}
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error in fetching values');
            }
        });
        $A.enqueueAction(action);
    },
    showToastAlert: function(title, message, type, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type,
            "mode": "dismissible",
            "duration": time
        });
        toastEvent.fire();
    },
    parseError: function(response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                var message = errors[0].message;
                if(message.includes("WARNING")) {
                    message = message.replace("WARNING :", "").trim();
                	this.showToastAlert("Warning!", message, "warning", 10000);
                }else {
                    message = message.replace("ERROR : ", "").trim();
                    this.showToastAlert("Error!", message, "error", 10000);
                }
            }
        } else {
            this.showToastAlert("", "Unkown Error", "error", 2000);
        }
    },
    validateContact: function(component) {
        var validSSNPattern = new RegExp("^([0-9]{9})$");
        var contact = component.get("v.urOfferEntity.cont");
        var todaysDate = this.todaysDate();  //Date format Used: [yyyy-mm-dd]      
        
        if (!this.checkEmpty(contact.ints__Social_Security_Number__c) && !validSSNPattern.test(contact.ints__Social_Security_Number__c)) {          
            this.showToastAlert("Error!", "SSN is not in the correct format. Please input 9 digits only", "error", 2000); 
            return false;
        }
        
        if (contact.Use_of_funds__c == 'Others' && this.checkEmpty(contact.Other_Use_Of_Funds__c)) {
            this.showToastAlert("Error!", "Please input value in Other field as Use of Funds is Others", "error", 2000);
            return false;
        }
        
        if (contact.MailingState == '--None--' || this.checkEmpty(contact.MailingState)) {
            this.showToastAlert("Error!", "Please select state", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.MobilePhone) && this.checkEmpty(contact.Phone)) {
            this.showToastAlert("Error!", "Please provide phone number", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.MailingPostalCode)) {
            this.showToastAlert("Error!", "Please enter Postal Code", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.MailingCity)) {
            this.showToastAlert("Error!", "Please enter Mailing City", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.MailingStreet)) {
            this.showToastAlert("Error!", "Please enter Mailing Street", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.FirstName)) {
            this.showToastAlert("Error!", "Please enter Fistname", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.LastName)) {
            this.showToastAlert("Error!", "Please enter Lastname", "error", 2000);
            return false;
        }
        
        if (contact.Birthdate >= todaysDate) {
            this.showToastAlert("Error!", "DOB cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        if (contact.Time_at_Current_Address__c >= todaysDate) {
            this.showToastAlert("Error!", "Time In Current Address cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        if (contact.Employment_Start_Date__c >= todaysDate) {
            this.showToastAlert("Error!", "Employment Start Date cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        if (contact.Previous_Employment_Start_Date__c >= todaysDate) {
            this.showToastAlert("Error!", "Prior Employment Started cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        if (contact.Previous_Employment_End_Date__c >= todaysDate) {
            this.showToastAlert("Error!", "Prior Employment Ended cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        return true;
    },
    payfoneandrecoveringQuestions: function(component, event) {
        component.set("v.showSpinner", true);
        var action = component.get("c.payfoneRequest");

        action.setParams({
            'EntityStr': JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();  
            
            if (state === "SUCCESS") {
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
                this.recoveringQuestions(component, event, result);
            } else if(state === 'ERROR') {
                component.set("v.showSpinner", false);
                this.parseError(response);
                console.log("Payfone Error");
            } else {
                component.set("v.showSpinner", false);
                console.log("Payfone Unknown Error");
            }
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    recoveringQuestions: function(component, event, result) {
        var action = component.get("c.recoverQuestions");
        
        action.setParams({
            'EntityStr': JSON.stringify(result)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();  
            if (state === "SUCCESS") {
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
            } else if(state === 'ERROR') {
                this.parseError(response);
            } else {
                console.log("Recovering question request Failed!");
            }   
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    processingQuestions: function(component, event) {
        var action = component.get("c.processQuestions"); 
        
        action.setParams({
            'EntityStr': JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();                
            
            if (state === "SUCCESS") {
                console.log('Questions successfully processed!');
                component.set("v.urOfferEntity.questions", []);
                this.showToastAlert("", "Questions processed successfully", "success", 1000);
            } else if(state === 'ERROR') {
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            } 
        });
        
        $A.enqueueAction(action);
    },
    todaysDate: function(component) {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        
        return [year, month, day].join('-');
        
    },
    checkEmpty : function(data) {
        if(typeof(data) == 'number' || typeof(data) == 'boolean')
        { 
            return false; 
        }
        if(typeof(data) == 'undefined' || data === null)
        {
            return true; 
        }
        if(typeof(data.length) != 'undefined')
        {
            return data.length == 0;
        }
        var count = 0;
        for(var i in data)
        {
            if(data.hasOwnProperty(i))
            {
                count ++;
            }
        }
        return count == 0;
    }
})