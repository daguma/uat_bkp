//Button_DocumentChecklistController
({
    showDocumentChecklist : function(component, event, helper) {
        component.set("v.body", []);
        $A.createComponent(
            "c:documentChecklist",
            {
                "oppId": component.get("v.recordId")
            },
            function(documentChecklistcmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(documentChecklistcmp);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
            }
        );
    }
})