//ApplicationDetailsHelper
({

    loadSelectValues: function (component) {
        var opportunityRec = component.get("v.applicationDetailsEntity");
        component.set("v.paymentMethodValue", opportunityRec.Payment_Method__c);
        component.set("v.incmDenominatorValue", opportunityRec.Income_Denominator__c);
        component.set("v.bankInfoSourceValue", opportunityRec.Bank_Info_Source__c);
    },

    loadPicklists: function (component) {
        var action = component.get("c.getPicklistValues");
        var opportunityRec = component.get("v.applicationDetailsEntity");
        action.setParams({
            opp: opportunityRec
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result.hasError) {
                    this.showAlert("error", result.errorMessage, "Failed!");
                } else {
                    component.set("v.paymentMethodOptions", result.paymentMethods);
                    component.set("v.incmDenominatorOptions", result.incomeDenominators);
                    component.set("v.bankInfoSourceOptions", result.bankInfoSources);
                }
            } else if (state == "ERROR") {
                this.showAlert("error", "Error at DocumentsHelper.getDocsEnt()", "Failed!");
            }
        });
        $A.enqueueAction(action);
    },

    updateAsItemSelected: function (component, event) {
        var whichPicklist = event.getSource();
        if (!$A.util.isUndefinedOrNull(whichPicklist)) {
            var value = whichPicklist.get("v.value") === "--None--" ? "" : whichPicklist.get("v.value");
            switch (whichPicklist.getLocalId()) {
                case "paymentMethod":
                    component.set("v.paymentMethodValue", value);
                    break;
                case "incmDenominator":
                    component.set("v.incmDenominatorValue", value);
                    break;
                case "bankInfoSource":
                    component.set("v.bankInfoSourceValue", value);
            }
        }
    },

    showAlert: function (alertType, alertMsg, alertTitle, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "dismissible",
            "duration": time
        });
        toastEvent.fire();
    },

    saveAllAppFields: function (component) {
        var opportunity = new Object();

        opportunity.Payment_Method__c = component.get("v.paymentMethodValue");
        opportunity.Income_Denominator__c = component.get("v.incmDenominatorValue");
        opportunity.Bank_Info_Source__c = component.get("v.bankInfoSourceValue");

        var bankName = component.find("bankName").get("v.value");
        if ($A.util.isUndefinedOrNull(bankName) || $A.util.isEmpty(bankName)) {
            opportunity.ACH_Bank_Name__c = "";
        } else {
            opportunity.ACH_Bank_Name__c = bankName;
        }

        return opportunity;
    }
})