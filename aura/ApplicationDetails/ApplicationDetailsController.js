//ApplicationDetailsController
({
    loadPicklistValues : function(component, event, helper){
        helper.loadSelectValues(component);
        helper.loadPicklists(component);
    },

    onItemSelected : function (component, event, helper) {
        helper.updateAsItemSelected(component, event);
    },
    
    callQuickSaveApp : function (component, event, helper) {
        var compEvent = component.getEvent("ApplicationDetails_SaveEvt");
        compEvent.fire();
    },

    saveDetails : function (component, event, helper) {
        return helper.saveAllAppFields(component);
    }
})