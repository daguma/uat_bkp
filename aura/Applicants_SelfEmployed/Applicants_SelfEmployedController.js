//Applicants_SelfEmployedController
({
    onTitleButtonClick: function (component, event, helper) {
        var cmp1 = component.find("parentDiv");
        $A.util.toggleClass(cmp1, "slds-is-open");
    },

    onTitleButtonClick2: function (component, event, helper) {
        var cmp1 = component.find("parentDiv2");
        $A.util.toggleClass(cmp1, "slds-is-open");
    },

    callQuickSaveApp: function (component, event, helper) {
        var compEvent = component.getEvent("ApplicationDetails_SaveEvt");
        compEvent.fire();
    },

    saveDetails: function (component, event, helper) {
        var result = new Object();

        var appSelfEmployedTable1 = component.find("applicantsSelfEmployedTable1");
        var appSelfEmployedTableList = appSelfEmployedTable1.saveDetails();
        result.one = appSelfEmployedTableList;

        var appSelfEmployedTable2 = component.find("applicantsSelfEmployedTable2");
        if ( ! $A.util.isUndefined(appSelfEmployedTable2) ) {
            var appSelfEmployedTableList2 = appSelfEmployedTable2.saveDetails();
            result.two = appSelfEmployedTableList2;
        }

        result.Is_No_POI_Required__c = component.get("v.noPOIRequired");

        return result;
    }
})