({
    handleEditRecord : function(component, event) {
        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.id;
        
        if(!$A.util.isEmpty(recordId)) {
    		var editRecordEvent = $A.get("e.force:editRecord");
            editRecordEvent.setParams({
                 "recordId": recordId
            });
            editRecordEvent.fire();
        }
    },
	handleViewRecord : function(component, event) {
        var selectedItem = event.currentTarget;
        var recordId = selectedItem.dataset.id;
        
        if(!$A.util.isEmpty(recordId)) {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId,
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
	},
    handleDeleteRecord: function(component, event, helper) {
        component.find("recordHandler").deleteRecord($A.getCallback(function(deleteResult) {
            if (deleteResult.state === "SUCCESS" || deleteResult.state === "DRAFT") {
                console.log("Record is deleted.");
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Deleted",
                    "message": "The record was deleted."
                });
                resultsToast.fire();
                
                component.getEvent("DeleteRowEvt").setParams({
                    "indexVar" : component.get("v.rowIndex")
                }).fire();
            }
            else if (deleteResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            }
            else if (deleteResult.state === "ERROR") {
            	console.log('Problem deleting record, error: ' +
                JSON.stringify(deleteResult.error));
            }
            else {
            	console.log('Unknown problem, state: ' + deleteResult.state +
                                    ', error: ' + JSON.stringify(deleteResult.error));
            }
        }));
    }
})