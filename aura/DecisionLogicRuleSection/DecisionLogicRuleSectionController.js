({
    init : function(component, event, helper) {
        helper.getDecisionLogicRules(component, helper);
        helper.getDecisionLogicRulesObject(component, helper);
        console.log('DL Rules Section hideManualBoton: ' + component.get("v.hideManualBoton"));
    },
    
    loadAddNoteModal : function(component, event, helper) {
        helper.loadAddNoteModal(component, helper, 'Decision Logic Rules Note');
    },
    
    checkIfDealIsReadyToFund : function(component, event, helper) {   
        helper.validateDecisionLogicMeetTolerance(component,helper, event);
    },
    getManualCallDecisionLogicRulesListFromProxyAPI : function(component, event, helper) {
                
        if (component.get("v.validationEntity.app.Status__c") != 'Funded') {
            helper.getManualCallDecisionLogicRulesFromProxyAPI(component, helper);
        }
    }
})