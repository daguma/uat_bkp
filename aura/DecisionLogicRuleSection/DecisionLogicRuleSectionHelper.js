({
    getDecisionLogicRulesObject : function(component, helper) {
        var action = component.get("c.getDecisionLogicRulesObject");
        action.setParams({ 
            opportunityId : component.get("v.recordId") 
        });
        //action.setBackground();
        action.setCallback(this, function(response){
            var status = response.getState();
            if (status === "SUCCESS") {
                 //console.log('getDecisionLogicRulesObject: , Status: SUCCESS');
                var result = response.getReturnValue();
                if (result !== null) {
                    component.set("v.DLRulesObject", result);   
                    component.set("v.lastCallDate", component.get("v.DLRulesObject.LastCallDate__c"));
                    component.set("v.hideManualBoton",helper.validateManualBoton(component, helper));
                    //component.set("v.ShowNotifications", helper.validateDecisionLogicAccountNumber(component));
                }
               
            }
            if (status === "INCOMPLETE") {
                console.log('DecisionLogicRuleSection, function:getDecisionLogicRulesObject: , Status: INCOMPLETE');
            }
            if (status === "ERROR") {
                console.log('DecisionLogicRuleSection, function:getDecisionLogicRulesObject, Status: ERROR');
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log(actionName + "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getDecisionLogicRules : function(component, helper) {
        
        var action = component.get("c.getDecisionLogicRulesList");
        
        action.setParams({ 
            opportunityId : component.get("v.recordId") 
        });
        
        action.setCallback(this, function(response) {
            var status = response.getState();
            if (status === "SUCCESS") {
                 //console.log('DecisionLogicRuleSection, function : getDecisionLogicRulesList, Status: SUCCESS');
                var result = response.getReturnValue();
                if (result !== null) {
                    console.log('** Rules Object: ' + result);
                    component.set("v.GDS_DLrules", result);
                    
                }         
            }
            if (status === "INCOMPLETE") {
                console.log('DecisionLogicRuleSection, function : getDecisionLogicRulesList, Status: INCOMPLETE');
            }
            if (status === "ERROR") {
                console.log('DecisionLogicRuleSection, function : getDecisionLogicRulesList, Status: ERROR');
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log("Unknown error");
                }
            }
        });
    
        $A.enqueueAction(action); 
    },
    
    validateDecisionLogicAccountNumber : function(component){
        var DLAccountNumberFound =  component.get("v.DLRulesObject.Opportunity__r.DL_Account_Number_Found__c");
        var oppStatus = component.get("v.DLRulesObject.Opportunity__r.Status__c");
        
        if (oppStatus === 'Funded') {
            return false
        }
        return (DLAccountNumberFound == null || DLAccountNumberFound == '' || DLAccountNumberFound == '--None--' || DLAccountNumberFound == undefined);
    },
    
    validateManualBoton : function(component,helper) {  
        var manualCall = component.get("v.DLRulesObject.ManualCall__c");
        var oppStatus = component.get("v.DLRulesObject.Opportunity__r.Status__c");
        var DLAccountNumberFound =  component.get("v.DLRulesObject.Opportunity__r.DL_Account_Number_Found__c"); 
        var validAccountNumber = (DLAccountNumberFound != null && DLAccountNumberFound != '' && DLAccountNumberFound != '--None--' && DLAccountNumberFound != undefined);
        var disableBoton = (validAccountNumber && oppStatus != 'Funded' && manualCall == false) ? "false" : "true";
        return (validAccountNumber && oppStatus != 'Funded' && manualCall == false) ? "false" : "true";
    },
 
    validateDecisionLogicMeetTolerance : function(component,helper, event) {
       
        var compEvent = component.getEvent("checkIfDealIsReadyToFundEvent");
        
        /*if (component.get("v.GDS_DLrules") === null) {
            component.set("v.validationEntity.app.DecisionLogicMeetsTolerance__c", true);
            console.log('** GDS_DLrules = NULL, DecisionLogicMeetsTolerance: ' + component.get("v.validationEntity.app.DecisionLogicMeetsTolerance__c"));
            compEvent.fire();
            return;
        }*/
        
        var action = component.get("c.validateDecisionLogicMeetTolerance");
        console.log('DecisionLogicRuleSection, function : validateDecisionLogicMeetTolerance, recordId: ' +  component.get("v.recordId"));
        console.log('DecisionLogicRuleSection, function : validateDecisionLogicMeetTolerance, jsonGDSResponse: ' +  JSON.stringify(component.get("v.GDS_DLrules")));
        action.setParams({ 
            'opportunityId' :  component.get("v.recordId"),
            'jsonGDSResponse' : JSON.stringify(component.get("v.GDS_DLrules"))
        });
        
        action.setCallback(this, function(response) {
            var status = response.getState();
            if (status === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('DecisionLogicRuleSection, function : validateDecisionLogicMeetTolerance, Status: SUCCESS, DecisionLogicMeetsTolerance: ' + result );
                component.set("v.validationEntity.app.DecisionLogicMeetsTolerance__c", result);  
                compEvent.fire();         
            }
            if (status === "INCOMPLETE") {
                console.log('DecisionLogicRuleSection, function : validateDecisionLogicMeetTolerance, Status: INCOMPLETE');
            }
            if (status === "ERROR") {
                console.log('DecisionLogicRuleSection, function : validateDecisionLogicMeetTolerance, Status: ERROR');
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log("Unknown error");
                }
            }
        });
    
        $A.enqueueAction(action); 
    },
    
    getManualCallDecisionLogicRulesFromProxyAPI : function(component, helper) {
        var today = new Date();
        var action = component.get("c.getManualCallDecisionLogicRulesFromProxyAPI");
        
        action.setParams({ 
            opportunityId : component.get("v.recordId") 
        });
        
        component.set("v.showSpinner", true);
        
        action.setCallback(this, function(response){
            var status = response.getState();
            if (status === "SUCCESS") {
                //console.log('DecisionLogicRuleSection, function:getManualCallDecisionLogicRulesFromProxyAPI: , Status: SUCCESS');
                var result = response.getReturnValue();
                if (result !== null) {
                    component.set("v.GDS_DLrules", result);
                    component.set("v.hideManualBoton","true");
                    component.set("v.lastCallDate",today);
                    $A.get('e.force:refreshView').fire();
                }
            }
            if (status === "INCOMPLETE") {
                console.log('DecisionLogicRuleSection, function:getManualCallDecisionLogicRulesFromProxyAPI: , Status: INCOMPLETE');
            }
            if (status === "ERROR") {
                console.log('DecisionLogicRuleSection, function:getManualCallDecisionLogicRulesFromProxyAPI, Status: ERROR');
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                } else {
                    console.log(actionName + "Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },
    
    loadAddNoteModal : function(component,helper, title) {
        component.set("v.body", []);
        $A.createComponent(
            "c:AttachNote",
            {
                "noteEntity": { 
                    'sobjectType': 'Note',
                    'ParentId': component.get("v.recordId"),
                    'Title': title     
                }
            },
            function(customerProfileCmp, status, errorMessage){
                
                if (status === "SUCCESS") {
                    
                    //console.log('decisionLogicRuleSection, function : loadAddNoteModal, Status: SUCCESS');
                    
                    var body = component.get("v.body");
                    body.push(customerProfileCmp);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE")   
                    console.log("decisionLogicRuleSection, function : loadAddNoteModal, Status: INCOMPLETE : No response from server or client is offline.")
                else if (status === "ERROR")            
                    console.log('decisionLogicRuleSection, function : loadAddNoteModal, Status: ERROR: ' + errorMessage);
            }
        );
    }
})