//ApplicationRulesController
({
    init: function (cmp, event, helper) {
        var existingCreditReportList = cmp.get("v.creditReportList");
        var existingRecordId = cmp.get("v.recordId");
        if (existingCreditReportList === null && existingRecordId !== null) {
            helper.getCrReport(cmp);
        }
    },

    showcreditReportDetails: function(cmp, event, helper) {

        var recordId = cmp.get("v.oppRecordId");
        var reportId = event.getSource().get("v.title");
        event.preventDefault();
        cmp.set("v.body", []);
        
        $A.createComponent(
            "c:CreditReportDetails",
            {
                recordId : recordId,
                reportId : reportId
            },
            function(newCmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(newCmp);
                    cmp.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    }
})