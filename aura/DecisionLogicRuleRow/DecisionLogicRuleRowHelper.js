({
    overrideRule : function(component) {  
        if(component.get("v.dlRule.overridden")){
            component.set("v.dlRule.overridden_by",component.get("v.validationEntity.currentUserName"));
            component.set("v.dlRule.deRuleStatus","Pass");
            component.set("v.dlRule.override_required",false);
        }
        else{
            component.set("v.dlRule.overridden_by","");
            component.set("v.dlRule.deRuleStatus","Fail");
            component.set("v.dlRule.override_required",true);
        }
    },
    
    updateDecisionLogicRulesObject : function(component) {
        
        //console.log('ruleList Object: ' + JSON.stringify(component.get("v.GDS_DLrules")) );
        
        //console.log('decisionLogicRuleRow, function : updateDecisionLogicRuleObject, Opportunity Id: ' + component.get("v.recordId"));
        
        var action = component.get("c.upsertDecisionLogicRuleObject");
        
        action.setParams({ 
            opportunityId : component.get("v.recordId"),
            jsonGDSResponse : JSON.stringify(component.get("v.GDS_DLrules"))
        });
        
        action.setBackground();
        
        action.setCallback(this, function(response) {
            
            var status = response.getState();
            
            if (status === "SUCCESS") {
                
                var result = response.getReturnValue();
                //console.log('decisionLogicRuleRow, function : updateDecisionLogicRulesObject, Status: SUCCESS, APEX Successful: MeetTolerance: ' + result);
                component.set("v.validationEntity.app.DecisionLogicMeetsTolerance__c", result)
                
            }else if (status === "INCOMPLETE") {
                console.log('decisionLogicRuleRow, function : updateDecisionLogicRulesObject , Status: INCOMPLETE');
            }else if (status === "ERROR") {
                
                console.log('decisionLogicRuleRow, function : updateDecisionLogicRulesObject , Status: ERROR');
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {              
                    console.log("Error message: " + errors[0].message);            
                } else {
                    console.log("Unknown error");
                }
            }
        });     
        $A.enqueueAction(action); 
    }
    
    
})