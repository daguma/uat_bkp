//AchInformationController.js
({
    init: function (cmp, event, helper) { 
        //Remove context menu
        document.addEventListener('contextmenu', event => event.preventDefault());
        //Remove paste option
        document.addEventListener('paste', event => event.preventDefault());

        //Load AchInfo from server if not loaded yet and if the Opp id is present
        var existingAchInfoEntity = cmp.get("v.achInformationEntity");
        var existingRecordId = cmp.get("v.recordId");
        if (existingAchInfoEntity === null && existingRecordId !== null) {
            helper.getAchInfo(cmp);
        }
    },

    updateStmt: function (component, event, helper) {
        var selectedStmt = event.getParam("bankStatement");
        helper.setStmtValues(component, selectedStmt);
    },

    saveOpp: function (component, event, helper) {
        var achInfoValidity = helper.validateAchInfo(component);
        component.find("accountNumber").set("v.value",helper.maskValue("v.accountNumber"));
        component.find("routingNumber").set("v.value",helper.maskValue("v.routingNumber"));
        if(achInfoValidity.isAllValid){
            var opportunity = new Object();
            opportunity.Id = component.get("v.recordId");
            var anh = component.get("v.accountNumberHidden");
            opportunity.ACH_Account_Number__c = anh;
            opportunity.ACH_Member_Account_Number__c = component.find("memberAcNumber").get("v.value");
            opportunity.ACH_Routing_Number__c = component.get("v.routingNumberHidden");
            opportunity.ACH_Bank_Name__c = component.find("bankName").get("v.value");
            opportunity.Others__c = component.find("others").get("v.value");

            var verifiedSelected = component.find("verChecklist").get("v.value");
            if ($A.util.isUndefinedOrNull(verifiedSelected) || $A.util.isEmpty(verifiedSelected) ) {
                var verCheclist = component.get("v.achInformationEntity.verifiedChecklist");
                for (var i = 0, len = verCheclist.length; i < len; i++) {
                    var verOption = verCheclist[i];
                    if( verOption.isSelected === true ){
                        verifiedSelected = verOption.value;
                    }
                }
            }
            if ($A.util.isUndefined(verifiedSelected) || $A.util.isEmpty(verifiedSelected)) {
                verifiedSelected = "Other";
            }
            opportunity.Verified_Checklist__c = verifiedSelected;

            var acctType = component.find("accountType").get("v.value");
            if ($A.util.isUndefinedOrNull(acctType) || $A.util.isEmpty(acctType) ) {
                var acctTypelist = component.get("v.achInformationEntity.achAccountTypesList");
                for (var i = 0, len = acctTypelist.length; i < len; i++) {
                    var verOption = acctTypelist[i];
                    if( verOption.isSelected === true ){
                        acctType = verOption.value;
                    }
                }
            }

            if ($A.util.isUndefined(acctType) || $A.util.isEmpty(acctType)) {
                acctType = "Checking";
            }

            opportunity.ACH_Account_Type__c = acctType;

            helper.saveOppFields(component, opportunity);
            
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": achInfoValidity.errorMessage,
                "mode": "sticky",
                "type": "error"
            });
            toastEvent.fire();
        }
    },

    onAcctNumBlur: function (component, event, helper) {
        //Process account number typed
        var accountNumber = component.find("accountNumber").get("v.value");

        if (!$A.util.isUndefined(accountNumber)) {
            accountNumber = accountNumber.replace("*", "");

            var validity = component.find("accountNumber").get("v.validity");
            if(validity.valid){

                //If account is valid, set the hidden account number
                component.set("v.accountNumberHidden", accountNumber);

                //Mask the account number typed
                //var maskedAcct = helper.maskValue(accountNumber);
                //component.find("accountNumber").set("v.value",helper.maskValue(accountNumber));

                //Load the statements
                var parametersEvent = $A.get("e.c:AchStatement_BankAccount");
                parametersEvent.setParams({
                    "accountTyped": accountNumber,
                    "displaySummaryFieldsWarning": false
                });
                // Fire the event
                parametersEvent.fire();

            }else{
                component.find("accountNumber").set("v.value","");
                component.set("v.accountNumberHidden", "");
            }

        }

        //Clear account confirmation field
        component.find("confAcctNumber").set("v.value","");
        component.set("v.confAccountNumberHidden", "");

    },

    onConfAcctNumBlur : function(component, event, helper){
        var confAcctComponent = component.find("confAcctNumber");
        var validity = confAcctComponent.get("v.validity");
        if(validity.valid){
            var confAcct = component.find("confAcctNumber").get("v.value");
            var accountTyped = component.get("v.accountNumberHidden");
            if(accountTyped === confAcct){
                $A.util.removeClass(confAcctComponent, "slds-has-error"); // remove red border
                component.set("v.confAccountNumberHidden", confAcct);
                component.find("confAcctNumber").set("v.value",helper.maskValue(confAcct));
            }else{
                component.set("v.confAccountNumberHidden", "");
                component.find("confAcctNumber").set("v.value","");
                //component.find("confAcctNumber").set('v.validity', {valid:false, typeMismatch :true});
                $A.util.addClass(confAcctComponent, "slds-has-error"); // add red border
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Account number does not match.",
                    "mode": "sticky",
                    "type": "error"
                });
                toastEvent.fire();
            }
        }else{
            component.find("confAcctNumber").set("v.value","");
        }
    },

    onRoutingNumBlur : function (component, event, helper) {
        //Process routing number typed
        var routingNumber = component.find("routingNumber").get("v.value");

        if (!$A.util.isUndefinedOrNull(routingNumber)) {
            routingNumber = routingNumber.replace("*", "");

            var validity = component.find("routingNumber").get("v.validity");
            if(validity.valid){

                //If routing num is valid, set the hidden routing number
                component.set("v.routingNumberHidden", routingNumber);

                //Mask the account number typed
                //component.find("routingNumber").set("v.value",helper.maskValue(routingNumber));

            }else{
                component.find("routingNumber").set("v.value","");
                component.set("v.routingNumberHidden", "");
            }

        }

        //Clear account confirmation field
        component.find("confRoutNumber").set("v.value","");
        component.set("v.confAccountNumberHidden", "");
    },

    onConfRoutingNumBlur : function (component, event, helper) {
        var confRoutNumberComp = component.find("confRoutNumber");
        var validity = confRoutNumberComp.get("v.validity");
        if(validity.valid){
            var confRoutNumber = component.find("confRoutNumber").get("v.value");
            var routingNumTyped = component.get("v.routingNumberHidden");
            if(routingNumTyped === confRoutNumber){
                $A.util.removeClass(confRoutNumberComp, "slds-has-error"); // remove red border
                component.set("v.confRoutingNumberHidden", confRoutNumber);
                component.find("confRoutNumber").set("v.value",helper.maskValue(confRoutNumber));
            }else{
                component.set("v.confRoutingNumberHidden", "");
                component.find("confRoutNumber").set("v.value","");
                //component.find("confAcctNumber").set('v.validity', {valid:false, typeMismatch :true});
                $A.util.addClass(confRoutNumberComp, "slds-has-error"); // add red border
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Routing number does not match.",
                    "mode": "sticky",
                    "type": "error"
                });
                toastEvent.fire();
            }
        }else{
            component.find("confRoutNumber").set("v.value","");
        }
    },

    onAcctNumFocus : function (component, event) {
        //Load the statements
        var parametersEvent = $A.get("e.c:AchStatement_BankAccount");
        parametersEvent.setParams({
            "accountTyped": '',
            "displaySummaryFieldsWarning": true
        });
        // Fire the event
        parametersEvent.fire();
        
        var acctHidden = component.get("v.accountNumberHidden");
        if (!$A.util.isUndefined(acctHidden) && !$A.util.isEmpty(acctHidden)) {
            component.find("accountNumber").set("v.value",acctHidden);
        }
    },

    onRoutingNumFocus: function (component) {
        var routingNumHidden = component.get("v.routingNumberHidden");
        if (!$A.util.isUndefined(routingNumHidden) && !$A.util.isEmpty(routingNumHidden)) {
            component.find("routingNumber").set("v.value",routingNumHidden);
        }
    },

    onConfAcctNumFocus : function(component ){
        var acctHidden = component.get("v.confAccountNumberHidden");
        if (!$A.util.isUndefined(acctHidden) && !$A.util.isEmpty(acctHidden)) {
            component.find("confAcctNumber").set("v.value",acctHidden);
        }
    },

    onConfRoutingNumFocus : function(component){
        var routingNumHidden = component.get("v.confRoutingNumberHidden");
        if (!$A.util.isUndefined(routingNumHidden) && !$A.util.isEmpty(routingNumHidden)) {
            component.find("confRoutNumber").set("v.value",routingNumHidden);
        }
    },

    onBankNameBlur : function (component) {
        var bankName = component.find("bankName").get("v.value");
        component.set("v.bankName",bankName);
    },
    
    onInvitationLinkClick : function (component, event, helper) {
        var myUrl = event.getSource().get("v.label");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": myUrl
        });
        urlEvent.fire();
    }
})