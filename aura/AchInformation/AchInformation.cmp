<aura:component description="AchInformation"
                implements="flexipage:availableForAllPageTypes,force:hasRecordId"
                controller="AchInformationCtrl">

    <aura:attribute name="recordId" type="Id"/>
    <aura:attribute name="achInformationEntity" type="Object"/>
    <aura:attribute name="accountNumber" type="String"/>
    <aura:attribute name="accountNumberHidden" type="String"/>
    <aura:attribute name="confAccountNumberHidden" type="String"/>
    <aura:attribute name="routingNumber" type="String"/>
    <aura:attribute name="routingNumberHidden" type="String"/>
    <aura:attribute name="confRoutingNumberHidden" type="String"/>
    <aura:attribute name="bankName" type="String"/>
    <aura:attribute name="showSpinner" type="Boolean" default="true"/> 
    <aura:attribute name="firstTimeTabLoading" type="Boolean" default="true"/>

    <aura:handler name="init" value="{! this }" action="{! c.init }"/>
    <aura:handler event="force:refreshView" action="{!c.init}" />
    <aura:handler event="c:Event_App_AchSave" action="{!c.saveOpp}"/>
    <aura:handler event="c:Event_App_UpdateStmt" action="{!c.updateStmt}"/>

    <!--Registering of the event. This will be hadled in BankStatement component -->
    <aura:registerEvent name="AchStatement_BankAccountEvt" type="c:AchStatement_BankAccount"/>

    <div class="slds-border_left slds-border_bottom slds-border_right slds-border_top slds-p-top_medium slds-p-right_medium slds-p-bottom_medium slds-p-left_medium">

        <aura:if isTrue="{!v.showSpinner}">
            <div class="slds-spinner_container">
                <lightning:spinner aura:id="spinner" variant="brand" size="small"/>
            </div>
        </aura:if>

        <ui:outputText class="slds-output slds-text-heading_small" value="ACH Information"/>
        <br/><br/><br/>

        <div>
            <c:Button_InvitationEmail recordId="{!v.recordId}" accountNumber="{!v.accountNumberHidden}"
                                      routingNumber="{!v.routingNumberHidden}"
                                      invitationLink="{!v.achInformationEntity.opportunity.Invitation_Link__c}"/>
            <c:Button_DLAvailability recordId="{!v.recordId}" routingNumber="{!v.routingNumberHidden}"
                                     bankName="{!v.bankName}"/>
            <c:Button_IncomeValidation recordId="{!v.recordId}" accountNumber="{!v.accountNumberHidden}"/>
        </div>
        <br/>

        <div class="slds-grid slds-p-vertical_x-small">

            <div class="slds-p-horizontal_small slds-size_1-of-2 slds-medium-size_1-of-1 slds-large-size_1-of-2">
                <div class="slds-form-element__control">
                    <lightning:input type="text" label="Account Number" aura:id="accountNumber" name="accountNumber"
                                     value="{!v.accountNumber}"
                                     pattern="^[\d|\*]*"
                                     messageWhenPatternMismatch="The Account Number input must be numeric."
                                     onblur="{!c.onAcctNumBlur}" onfocus="{!c.onAcctNumFocus}"/>
                </div>
                <div class="slds-form-element__control">
                    <ui:inputSelect label="Account Type" aura:id="accountType">
                        <aura:iteration items="{!v.achInformationEntity.achAccountTypesList}" var="achAccountType">
                            <ui:inputSelectOption text="{!achAccountType.value}"
                                                  label="{!achAccountType.label}"
                                                  value="{!achAccountType.isSelected}"/>
                        </aura:iteration>
                    </ui:inputSelect>
                </div>
                <div class="slds-form-element__control">
                    <lightning:input type="text" label="Routing Number" aura:id="routingNumber" name="routingNumber"
                                     value="{!v.routingNumber}"
                                     pattern="^[\d|\*]*" maxlength="9" minlength="9"
                                     messageWhenPatternMismatch="The ACH Routing Number must be exactly 9 digits."
                                     messageWhenBadInput="The ACH Routing Number must be exactly 9 digits."
                                     onblur="{!c.onRoutingNumBlur}" onfocus="{!c.onRoutingNumFocus}"/>
                </div>
                <div class="slds-form-element__control">
                    <lightning:input type="text" label="Bank Name" aura:id="bankName" name="bankName"
                                     value="{!v.achInformationEntity.opportunity.ACH_Bank_Name__c}"
                                     onblur="{!c.onBankNameBlur}"/>
                </div>
                <div class="slds-form-element__control">
                    <ui:inputSelect label="Verified Checklist" aura:id="verChecklist">
                        <aura:iteration items="{!v.achInformationEntity.verifiedChecklist}" var="verifiedChecklistItem">
                            <ui:inputSelectOption text="{!verifiedChecklistItem.value}"
                                                  label="{!verifiedChecklistItem.label}"
                                                  value="{!verifiedChecklistItem.isSelected}"/>
                        </aura:iteration>
                    </ui:inputSelect>
                </div>

                <div class="slds-form-element__control">
                    <label>Invitation Link</label> <br/>
                    <ui:outputURL label="{!v.achInformationEntity.opportunity.Invitation_Link__c}"
                                  value="javascript:void(0)" title=""
                                  click="{!c.onInvitationLinkClick}"/>
                </div>

            </div>

            <div class="slds-p-horizontal_small slds-size_1-of-2 slds-medium-size_1-of-1 slds-large-size_1-of-2">

                <div class="slds-form-element__control">
                    <lightning:input type="text" label="Confirm Account Number" aura:id="confAcctNumber"
                                     name="confAcctNumber"
                                     pattern="^[\d|\*]*"
                                     messageWhenPatternMismatch="The Account Number input must be numeric."
                                     onblur="{!c.onConfAcctNumBlur}" onfocus="{!c.onConfAcctNumFocus}"
                    />
                </div>
                <div class="slds-form-element__control">
                    <lightning:input type="text" label="Member Account Number" name="memberAcNumber"
                                     aura:id="memberAcNumber"
                                     value="{!v.achInformationEntity.opportunity.ACH_Member_Account_Number__c}"/>
                </div>
                <div class="slds-form-element__control">
                    <lightning:input aura:id="confRoutNumber" type="text" label="Confirm Routing Number"
                                     name="confRoutNumber" pattern="^[\d|\*]*" maxlength="9" minlength="9"
                                     messageWhenBadInput="The ACH Routing Number must be exactly 9 digits."
                                     messageWhenPatternMismatch="The ACH Routing Number must be exactly 9 digits."
                                     onblur="{!c.onConfRoutingNumBlur}" onfocus="{!c.onConfRoutingNumFocus}"/>
                </div>
                <div class="slds-form-element__control">
                    <lightning:input type="text" label="Others" name="others" aura:id="others"
                                     value="{!v.achInformationEntity.opportunity.Others__c}"/>
                </div>
                <div class="slds-form-element__control">
                    <!--<label class="slds-form-element__label slds-p-top_medium"></label>-->
                    <!--<ui:outputText class="slds-output slds-text-heading_small"-->
                                   <!--value="{!v.achInformationEntity.opportunity.Data_Entry_Assigned_To__c}"/>-->
                    <ui:inputSelect label="Data Entry Assigned To" aura:id="dataEntryList">
                        <aura:iteration items="{!v.achInformationEntity.dataEntryAssignedList}" var="dataEntryAssignedListItem">
                            <ui:inputSelectOption text="{!dataEntryAssignedListItem.value}"
                                                  label="{!dataEntryAssignedListItem.label}"
                                                  value="{!dataEntryAssignedListItem.isSelected}"/>
                        </aura:iteration>
                    </ui:inputSelect>
                </div>
            </div>
        </div>
    </div>

</aura:component>