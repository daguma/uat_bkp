//AchInformationHelper
({
    getAchInfo: function (component) {
        var opp = component.get("c.getAchInformationEntity");
        opp.setParams({
            "oppId": component.get("v.recordId")
        });
        opp.setBackground();
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();

                var bankName = result.opportunity.ACH_Bank_Name__c;
                component.set("v.bankName", bankName);

                var acctNum = result.opportunity.ACH_Account_Number__c;
                this.manageAccount(component, acctNum);

                var routingNum = result.opportunity.ACH_Routing_Number__c;
                this.manageRouting(component, routingNum);
                component.set("v.achInformationEntity", result);
            }
            else {
                console.log("Failed with state: " + state);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(opp);

    },

    setStmtValues: function (component, statement) {

        var isFirstTimeTabLoading = component.get("v.firstTimeTabLoading");
        if( isFirstTimeTabLoading === true){

            if (!$A.util.isUndefinedOrNull(statement.institutionName) && !$A.util.isEmpty(statement.institutionName)) {
                this.setValuesFromStatement(component, statement);
            }
            component.set("v.firstTimeTabLoading",false);

        }else{
            this.setValuesFromStatement(component, statement);
        }

    },
    
    setValuesFromStatement: function (component, statement) {
        var bankName = statement.institutionName;
        component.set("v.bankName", bankName);
        component.find("bankName").set("v.value", bankName);

        var ops = [];
        var acctType = statement.accountType;

        if (!$A.util.isUndefinedOrNull(acctType) && !$A.util.isEmpty(acctType)) {
            if ('SAVINGS' === acctType.toUpperCase()) {
                ops.push({class: "optionClass", label: "Checking", value: "Checking"});
                ops.push({class: "optionClass", label: "Savings", value: "Savings", selected: "true"});
            } else if ('CHECKING' === acctType.toUpperCase()) {
                ops.push({class: "optionClass", label: "Checking", value: "Checking", selected: "true"});
                ops.push({class: "optionClass", label: "Savings", value: "Savings"});
            }
        }
        component.find("accountType").set("v.options", ops);

        var ops = [];
        ops.push({class: "optionClass", label: "Call the bank", value: "Call the bank"});
        if (statement == null || statement == '') {
            ops.push({class: "optionClass", label: "DecisionLogic", value: "DecisionLogic"});
            ops.push({class: "optionClass", label: "Other", value: "Other", selected: "true"});
        } else {
            ops.push({class: "optionClass", label: "DecisionLogic", value: "DecisionLogic", selected: "true"});
            ops.push({class: "optionClass", label: "Other", value: "Other"});
        }
        ops.push({class: "optionClass", label: "Yodlee", value: "Yodlee"});
        component.find("verChecklist").set("v.options", ops);

    },

    manageRouting: function (component, routingNum) {
        component.set("v.routingNumberHidden", routingNum);
        component.set("v.confRoutingNumberHidden", routingNum);

        var maskedRoutingNum = '';
        if (!$A.util.isUndefinedOrNull(routingNum) && !$A.util.isEmpty(routingNum)) {
            var allMasked = true, hasAsterisk = false;
            for (var i = 0, len = routingNum.length; i < len; i++) {
                if (routingNum.charAt(i) != '*') {
                    allMasked = false;
                }
                if (routingNum.charAt(i) === '*') {
                    hasAsterisk = true;
                }
            }
            if (!allMasked && !hasAsterisk) {
                if (routingNum != null) {
                    for (var i = 0, len = routingNum.length; i < len; i++) {
                        maskedRoutingNum = maskedRoutingNum + '*';
                    }
                }
            }
        } else {
            maskedRoutingNum = '';
        }
        component.set("v.routingNumber", maskedRoutingNum);
        component.find("confRoutNumber").set("v.value", maskedRoutingNum);
    },

    manageAccount: function (component, acctNum) {
        component.set("v.accountNumberHidden", acctNum);
        component.set("v.confAccountNumberHidden", acctNum);
        var maskedAcct = '';
        if (!$A.util.isUndefinedOrNull(acctNum) && !$A.util.isEmpty(acctNum)) {
            var allMasked = true, hasAsterisk = false;
            for (var i = 0, len = acctNum.length; i < len; i++) {
                if (acctNum.charAt(i) != '*') {
                    allMasked = false;
                }
                if (acctNum.charAt(i) === '*') {
                    hasAsterisk = true;
                }
            }
            if (!allMasked && !hasAsterisk) {
                for (var i = 0, len = acctNum.length; i < len; i++) {
                    maskedAcct = maskedAcct + '*';
                }
            }
        } else {
            maskedAcct = '';
        }
        component.set("v.accountNumber", maskedAcct);
        component.find("confAcctNumber").set("v.value", maskedAcct);
    },

    maskAccount: function (component) {
        var selected = component.find("accountNumber").get("v.value");
        component.set("v.accountNumberHidden", selected);
        if (!$A.util.isUndefinedOrNull(selected) && !$A.util.isEmpty(selected)) {
            var allMasked = true, hasAsterisk = false;
            for (var i = 0, len = selected.length; i < len; i++) {
                if (selected.charAt(i) !== '*') {
                    allMasked = false;
                }
                if (selected.charAt(i) === '*') {
                    hasAsterisk = true;
                }
            }
            if (!allMasked && !hasAsterisk) {
                var maskedValue = '';
                for (var i = 0, len = selected.length; i < len; i++) {
                    maskedValue = maskedValue + '*';
                }
                component.find("accountNumber").set("v.value", maskedValue);
            }
        }
    },

    maskRoutingNum: function (component) {
        var selected = component.find("routingNumber").get("v.value");
        if (!$A.util.isUndefinedOrNull(selected) && !$A.util.isEmpty(selected)) {
            var allMasked = true, hasAsterisk = false;
            for (var i = 0, len = selected.length; i < len; i++) {
                if (selected.charAt(i) !== '*') {
                    allMasked = false;
                }
                if (selected.charAt(i) === '*') {
                    hasAsterisk = true;
                }
            }
            if (!allMasked && !hasAsterisk) {
                component.set("v.routingNumberHidden", selected);
                var maskedValue = '';
                for (var i = 0, len = selected.length; i < len; i++) {
                    maskedValue = maskedValue + '*';
                }
                component.find("routingNumber").set("v.value", maskedValue);
            }
        }
    },

    maskValue: function (valueToMask) {
        var maskedValue = '';
        if (!$A.util.isUndefinedOrNull(valueToMask) && !$A.util.isEmpty(valueToMask)) {
            var allMasked = true, hasAsterisk = false;
            for (var i = 0, len = valueToMask.length; i < len; i++) {
                if (valueToMask.charAt(i) !== '*') {
                    allMasked = false;
                }
                if (valueToMask.charAt(i) === '*') {
                    hasAsterisk = true;
                }
            }
            if (!allMasked && !hasAsterisk) {
                for (var i = 0, len = valueToMask.length; i < len; i++) {
                    maskedValue = maskedValue + '*';
                }
            }
        }
        return maskedValue;
    },

    validateAchInfo: function (component) {

        var achInfoValidity = new Object();
        achInfoValidity.isAllValid = true;
        achInfoValidity.errorMessage = "";

        var fields = ["accountNumber", "routingNumber", "confAcctNumber", "confRoutNumber"];
        var field = "";

        for (var i = 0; i < fields.length; i++) {
            field = component.find(fields[i]);
            if ($A.util.hasClass(field, "slds-has-error")) {
                achInfoValidity.isAllValid = false;
                achInfoValidity.errorMessage = "Please correct field value errors before saving the Opportunity";
                return achInfoValidity;
            }
        }

        var verifiedSelected = component.find("verChecklist").get("v.value");
        var otherSpecified = component.find("others").get("v.value");
        if (!$A.util.isUndefinedOrNull(verifiedSelected)
            && verifiedSelected === "Other"
            && ($A.util.isUndefinedOrNull(otherSpecified) || $A.util.isEmpty(otherSpecified))) {
            achInfoValidity.isAllValid = false;
            achInfoValidity.errorMessage = "Error : Please input value in Other field as Verified Checklist is Others";
        }

        return achInfoValidity;
    },

    saveOppFields: function (component, opportunity) {
        var opp = component.get("c.saveOpportunity");
        opp.setParams({
            "opportunity": opportunity
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var saveResult = response.getReturnValue();

                if (saveResult.hasError) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": saveResult.error,
                        "mode": "sticky",
                        "type": "error"
                    });
                    toastEvent.fire();
                } else {
                    if (saveResult.hasWarning) {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Warning!",
                            "message": saveResult.warning,
                            "mode": "sticky",
                            "type": "warning"
                        });
                        toastEvent.fire();
                    }

                    //Saves the opportunity even with warnings
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Opportunity saved",
                        "mode": "sticky",
                        "type": "success"
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();

                }
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "AchInformationHelper was not able to save the Opportunity. " + response.getReturnValue(),
                    "mode": "sticky",
                    "type": "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(opp);

    }

})