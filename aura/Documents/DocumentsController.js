//DocumentsController
({
    doInit : function (component, event, helper) {
        helper.getDocEntity(component);
    },

    saveOpp : function (component, event, helper) {
        helper.saveAllAppFields(component);
    },

    buttonAction : function (component, event) {
        var target = event.getSource();
        alert(target.get("v.label"));
    },

    attachMultiple : function (component, event) {
        var recordId = component.get("v.recordId");
        var url = '/apex/EasyUploadAttachment?parentId=' + recordId;
        window.open(url, '_self');
    },

    reportIssue : function (component, event) {
        var recordId = component.get("v.recordId");
        var url = '/apex/ShowAttachments?appId=' + recordId;
        window.open(url, '_self');
    },

    uploadInvite : function (component, event, helper) {
        helper.docUploadInvite(component);
    },

    attachFile : function (component, event, helper) {
        component.set("v.showAttachFileBtn",false);
        component.find("dummyDocUploadSection").getElement().scrollIntoView(true);
    },

    cancelFileAttachAction : function (component, event, helper) {
        component.set("v.showAttachFileBtn", true);
    },

    generateContract : function (component, event, helper) {
        helper.genContractDocuments(component);
    }
})