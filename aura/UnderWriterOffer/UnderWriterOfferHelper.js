({
    initialize : function(component) {
        // Get a reference to the getEmploymentEntity() function defined in the Apex controller
        var action = component.get("c.getUnderWriterOfferEntity");
        
        action.setParams({
            "appId": component.get("v.recordId")
        });
        action.setBackground();
        var opts = [];        
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                if(result !== null) {
                    component.set("v.feeHandlingVal", result.application.Fee_Handling__c);
                    component.set("v.urOfferEntity", result);
                    component.set("v.errorMsg", result.errorMessage);
                    component.set("v.hasErrorMsg", result.hasErrorMessage);
                    component.set("v.warnings", result.warningMsgs); 
                }else {
                    console.log("Failed to fetch information 'getUnderWriterOfferEntity'");
                }
            }else if(state === "ERROR") {
                this.parseError(response);
        	}else {
                component.set("v.hasErrorMsg", true);
                console.log('OH, I AM NOT IN');
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    fetchPickListVal: function(component, fieldName) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": { 'sobjectType': 'Opportunity' },
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            console.log(response);
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
		if(fieldName == "Fee_Handling__c"){
			component.set("v.options", opts);
		}else{
			component.set("v.repOptions", opts);
		}
                
            }else if(state === "ERROR") {
                this.parseError(response);
        	}else {
                console.log('Error in fetching values');
            }
        });
        $A.enqueueAction(action);
    },
    saveApplication: function(component) {
        var action = component.get("c.saveApp");
        
        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                if($A.util.isEmpty(result.popupMessage)) {
                    result.popupMessage = '';
                    component.set("v.urOfferEntity", result);
                    // this.showToastAlert("", "Reason saved successfully.", "success", 1000);
                     component.set("v.infoMsg", "Reason saved successfully.");
                    component.set("v.hasInfoMsg", true);
                     $A.get('e.force:refreshView').fire();

                }else {
//                    this.showToastAlert("Error!", result.popupMessage, "error", 2000);
                    component.set("v.errorMsg", result.popupMessage);
                    component.set("v.hasErrorMsg", true);

                }                
            }else if(state === "ERROR") {
                this.parseError(response);
        	}else { 
            	console.log('Error Rejection Block');
//                this.showToastAlert("Error!", result.errorMessage, "error", 2000);
                    component.set("v.errorMsg", result.errorMessage);
                    component.set("v.hasErrorMsg", true);

            }
        });
        $A.enqueueAction(action);
    },
    validateField: function(component, fieldId) {
        var isValid = true;
        var fieldCmp = component.find(fieldId);
        if(!fieldCmp.get("v.value")) {
        	fieldCmp.set("v.errors", [{message:"Field is required"}]); // Set Error
            isValid = false;
        }else fieldCmp.set("v.errors", null); // Clear Error
        return isValid;
    },
    validateFeeHandling: function(component) {
        var app = component.get("v.urOfferEntity");
        var feeHandleOldVal = component.get("v.feeHandlingVal");
        var field = component.find("feeHandling");
        if(app.application.Is_Selected_Offer__c) {
            this.showToastAlert("Warning!", "You cannot change the fee handling if an offer is selected.", "warning", 1000);
            return false;
        }else {
            if(confirm("Are you sure you want to change this Origination Fee?\nThis action will re-generates the offers and cannot be undone.")) {
                component.set("v.feeHandlingVal", field.get("v.value"));
                return true;
            }else {
                field.set("v.value", feeHandleOldVal);
                return false;
            }
        }

    },
    regenerateRepaymentFrequency: function(component) {
        var action = component.get("c.rePaymentFrequencyGeneration");
        
        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {     
                this.showToastAlert("","Repayment frequency updated.","success",100);
                component.set("v.urOfferEntity", result);
                component.find("existingOffers").offersReload();
            }else if (response.getState() === "ERROR") {
                this.parseError(response);
            }
            else {
                console.log("Repayment frequency Incomplete");
            }
        })     
        $A.enqueueAction(action);
    },
    regeneratingOffers: function(component) {       
        var action = component.get("c.reGenerateOffers");
        
        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                if(!result.hasErrorMessage) {
                    console.log('Regenerated Offers');
                    component.set("v.urOfferEntity", result);
                    component.find("existingOffers").offersReload();
                }else {
                    console.log('Failed to Regenerate Offers');
                    this.showToastAlert("Error!", result.errorMessage, "error", 2000);
                }                
            }else if(state === "ERROR") {
                this.parseError(response);
        	}else { 
            	console.log('Error Rejection Block');
                this.showToastAlert("Error!", result.errorMessage, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    changingPaymentorPrincipal: function(component, methodName) {
        var action = component.get(methodName);
        
        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                
                if(!$A.util.isEmpty(result)) {
                    if(result.includes("WARNING")) {
                        result = result.replace("WARNING :", "").trim();
                        this.showToastAlert("Warning!", result, "warning", 10000);
                    }else {
                        result = result.replace("ERROR : ", "").trim();
                        this.showToastAlert("Error!", result, "error", 10000);
                    }
                }else {
                    this.showToastAlert("Success!", 'Recalculation of offers done successfully', "success", 2000);
                    component.find("existingOffers").offersReload();
                }
            }else if(response.getState() == "ERROR"){ 
            	this.parseError(response);
            }
        });
        $A.enqueueAction(action);
    },
    showToastAlert: function(title, message, type, time) {
        var toastEvent = $A.get("e.force:showToast");
        	toastEvent.setParams({
            	"title": title,
                "message": message,
                "type": type,
                "mode": "dismissible",
                "duration": time
            });
            toastEvent.fire();
    },
    parseError: function(response) {
        var errors = response.getError();
        if (errors) {
        	if (errors[0] && errors[0].message) {
//            	this.showToastAlert("Error!", errors[0].message, "error", 2000); 
                    component.set("v.errorMsg", errors[0].message);
                    component.set("v.hasErrorMsg", true);

            }
        } else {
//       		this.showToastAlert("", "Unkown Error", "error", 2000); 
                    component.set("v.errorMsg", "Unknown Error");
                    component.set("v.hasErrorMsg", true);
        }
    }
})