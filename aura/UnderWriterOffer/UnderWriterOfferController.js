({
    doInit : function(component, event, helper) {
        helper.initialize(component);
        helper.fetchPickListVal(component, 'Fee_Handling__c');
        helper.fetchPickListVal(component, 'RepaymentFrequency__c');
    },
    saveApplication : function(component, event, helper) {
        if(helper.validateField(component, 'DecReason'))
            helper.saveApplication(component);
    },
    regeneratingOffers : function(component, event, helper) {
        if(helper.validateFeeHandling(component))
            helper.regeneratingOffers(component);
    },
    regeneratingRepaymentFreq : function(component, event, helper) {
        //var rePayment = event.getSource().get("v.value");
        //alert(rePayment);
        //if(!$A.util.isEmpty(rePayment) && rePayment != "--None--"){
        	helper.regenerateRepaymentFrequency(component);
        //}
        //else {
            //alert(rePayment);
        //}
    },
    AONregeneratingOffers : function(component, event, helper) {
        var checked = event.getSource().get("v.checked");
        if(confirm('Are you sure you want to change the AON Enrollment?\nThis action will re-generate the offers and cannot be undone'))
            helper.regeneratingOffers(component);
        else 
            component.set("v.urOfferEntity.application.AON_Enrollment__c", !checked);
    },
    changingPayment : function(component, event, helper) {
        if(confirm('Are you sure you want to proceed?\nThis action will delete all the custom offer(s) and cannot be undone'))
            var fee_percent = component.get("v.urOfferEntity.GivenFeePerc");        
        	if(!fee_percent)
            	component.set("v.urOfferEntity.GivenFeePerc", null);  
        	helper.changingPaymentorPrincipal(component, "c.changePayment");
    },
    changingPrincipal : function(component, event, helper) {
        if(confirm('Are you sure you want to proceed?\nThis action will delete all the custom offer(s) and cannot be undone'))
        	var fee_percent = component.get("v.urOfferEntity.GivenFeePerc");        
        	if(!fee_percent)
            	component.set("v.urOfferEntity.GivenFeePerc", null); 
            helper.changingPaymentorPrincipal(component, "c.changePrincipal");
    },
    reloadUroEntity: function(component, event, helper) {
        var jsonEntityString = event.getParam("uroEntityString"); 
        var jsonEntityObject = JSON.parse(jsonEntityString);
        component.set("v.urOfferEntity", jsonEntityObject);
        component.set("v.errorMsg", jsonEntityObject.errorMessage);
        component.set("v.hasErrorMsg", jsonEntityObject.hasErrorMessage);
        component.set("v.warnings", jsonEntityObject.warningMsgs); 
        helper.fetchPickListVal(component, 'Fee_Handling__c', 'feeHandling');
        helper.fetchPickListVal(component, 'RepaymentFrequency__c', 'repaymentFrequency');
    },
    selectOfferAction: function(component, event, helper) {
        var offer = event.getParam("offerEntity"); 
        var action = component.get("c.selectOffer");
        
        action.setParams({
            "offer": offer,
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {       
                var result = response.getReturnValue();
                
                if(!$A.util.isEmpty(result.popupMessage)) {
                    var message = result.popupMessage;
                    if(message.includes("WARNING")) {
                        message = message.replace("WARNING :", "").trim();
                        helper.showToastAlert("Warning!", message, "warning", 10000);
                    }else {
                        message = message.replace("ERROR : ", "").trim();
                        helper.showToastAlert("Error!", message, "error", 10000);
                    }
                }
                
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
                $A.get('e.force:refreshView').fire();
            }else {
                helper.parseError(response);
            }
        })     
        $A.enqueueAction(action);
    },
    deselectOfferAction : function(component, event, helper) {
        var offer = event.getParam("offerEntity"); 
        var action = component.get("c.deselectOffer");
        
        action.setParams({
            "offer": offer,
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {       
                var result = response.getReturnValue()
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
                $A.get('e.force:refreshView').fire();
            }else {
                helper.parseError(response);
            }
        })     
        $A.enqueueAction(action);
    },
    deleteOfferAction : function(component, event, helper) {
        var offer = event.getParam("offerEntity"); 
        var action = component.get("c.deleteOffer");
        
        action.setParams({
            "offer": offer,
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity"))
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                helper.showToastAlert("Success!", 'Offer deleted successfully', "success", 1000);
                var result = response.getReturnValue()
                component.set("v.urOfferEntity", result);
                component.set("v.errorMsg", result.errorMessage);
                component.set("v.hasErrorMsg", result.hasErrorMessage);
                component.set("v.warnings", result.warningMsgs);
                $A.get('e.force:refreshView').fire();
            }else {
                helper.parseError(response);
            }
        })     
        $A.enqueueAction(action);
    }
})