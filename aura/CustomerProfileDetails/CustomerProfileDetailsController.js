({
	getCustomerProfile : function(component, event, helper) {
        helper.applyCSS(component);
		// #1 Get Opportunity Details
        var actionCustProf = component.get("c.initializeCustomerProfile");
    	actionCustProf.setParams({
            "appId": component.get("v.recordId")
        });
        
        // Add callback behavior for when response is received
        actionCustProf.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.custProfile", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        // Send action off to be executed
        $A.enqueueAction(actionCustProf);
	},
    closeModal : function(component, event, helper) {
        component.set("v.showPopUp", false);
        helper.revertCssChange(component);
    },
    showcreditReportDetails : function(cmp, event, helper) {
		cmp.set("v.showPopUp", false);
        var recordId = cmp.get("v.recordId");
        var reportId = cmp.get("v.custProfile.crId");

        event.preventDefault();
        cmp.set("v.body", []);
        
        $A.createComponent(
            "c:CreditReportDetails",
            {
                recordId : recordId,
                reportId : reportId
            },
            function(newCmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(newCmp);
                    cmp.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    }
})