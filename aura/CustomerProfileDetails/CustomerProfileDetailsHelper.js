({
	applyCSS: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:0} .forceStyle.desktop .viewport{overflow:hidden}</style>");
    },
    revertCssChange: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:5} .forceStyle.desktop .viewport{overflow:visible}</style>");
    }
})