//Banner2018HolidayPricingController.js
({
    getHolidayPricingAppsList : function (component, event, helper) {
        if( _holidayPricing.isHolidayPricingEligible(component.get("v.recordId")) ){
            component.set("v.is2018HolidayPricingEligible",true);
        }else{
            component.set("v.is2018HolidayPricingEligible",false);
        }
    }
})