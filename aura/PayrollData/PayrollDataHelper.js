//PayrollDataHelper
({
    getPDEntity : function (component) {
        var opp = component.get("c.getPayrollDataEntity");
        opp.setParams({
            "oppId": component.get("v.recordId")
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.hasError === true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": result.errorMessage,
                        "mode": "sticky",
                        "type": "error"
                    });
                    toastEvent.fire();
                }else{
                    component.set("v.payrollDataEntity", result);
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(opp);
    },

    validateNextPayrollDate : function (component) {
        var nextPayroll = component.get("v.payrollDataEntity.opportunity.Next_Payroll_Date__c");
        var nextPayrollDate = new Date(nextPayroll);
        nextPayrollDate.setDate(nextPayrollDate.getDate() +1);

        var today = new Date();

        if (nextPayrollDate < today) {
            alert("The Next Payroll date should not be prior to today.");
            return false;
        }

        return true;

    },

    savePayroll : function (component) {
        var opp = component.get("c.savePayrollData");
        opp.setParams({
            "opp": component.get("v.payrollDataEntity.opportunity")
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.hasError === true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": result.errorMessage,
                        "mode": "sticky",
                        "type": "error"
                    });
                    toastEvent.fire();
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success",
                        "message": "The Payroll Data has been saved!",
                        "mode": "sticky",
                        "type": "success"
                    });
                    toastEvent.fire();
					$A.get('e.force:refreshView').fire();
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(opp);
    }
})