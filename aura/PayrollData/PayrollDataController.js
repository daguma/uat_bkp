//PayrollDataController
({
    doInit: function (component, event, helper) {
        //Load PayrollDataEntity from server if not loaded yet and if the Opp id is present
        var rec = component.get("v.recordId");
        if (rec !== null) {
            helper.getPDEntity(component);
        }
    },

    savePayrollAction : function (component, event, helper) {
        if(helper.validateNextPayrollDate(component)){
            component.set("v.payrollDataEntity.opportunity.Payroll_Frequency__c",component.find("salaryCycle").get("v.value"));
            component.set("v.payrollDataEntity.opportunity.Payment_Method__c",component.find("paymentMethod").get("v.value"));
            helper.savePayroll(component);
        }
    }
})