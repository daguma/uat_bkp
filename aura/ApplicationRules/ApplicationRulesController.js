//ApplicationRulesController
({
    init: function (cmp, event, helper) {
        var existingAppRules = cmp.get("v.applicationRules");
        var existingRecordId = cmp.get("v.recordId");
        if( existingAppRules === null && existingRecordId !== null){
            helper.getAppRules(cmp);
        }
    }
})