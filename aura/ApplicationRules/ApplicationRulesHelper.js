//ApplicationRulesHelper
({
    getAppRules : function(cmp) {
        var action = cmp.get('c.getCRApplicationRules');
        action.setParams({
            "oppId": cmp.get("v.recordId")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                cmp.set('v.applicationRules', response.getReturnValue());

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    }
})