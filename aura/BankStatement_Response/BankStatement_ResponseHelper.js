//BankStatement_ResponseHelper
({
    saveOpportunityDetails: function (component) {
        var i, option, accountNumberFound, len = 0;

        var acctNumFoundSelected = component.find("accountNumbersFound").get("v.value");
        if ($A.util.isUndefinedOrNull(acctNumFoundSelected) || $A.util.isEmpty(acctNumFoundSelected)) {
            var acctNumFoundOptions = component.get("v.accountsFound");
            if (!$A.util.isUndefinedOrNull(acctNumFoundOptions)) {
                len = acctNumFoundOptions.length;
            }

            for (i = 0; i < len; ++i) {
                option = acctNumFoundOptions[i];
                if (option.isSelected === true) {
                    accountNumberFound = option.value;
                }
            }
            if ($A.util.isUndefinedOrNull(accountNumberFound)) {
                accountNumberFound = "";
            }
        } else {
            accountNumberFound = acctNumFoundSelected;
        }

        var oppId = component.get("v.recordId");
        var opp = component.get("c.saveOpportunity");
        opp.setParams({
            "oppId": oppId,
            "dl_Account_Number_Found": accountNumberFound
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "BankStatement_ResponseHelper was not able to save the Opportunity. " + response.getReturnValue(),
                    "mode": "sticky",
                    "type": "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(opp);
    }
})