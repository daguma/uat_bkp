//BankStatement_ResponseController
({
    onSelectChange : function(component, event, helper) {
        var selected = component.find("accountNumbersFound").get("v.value");
        var parametersEvent = component.getEvent("BankStatement_ChangeStatement");
        parametersEvent.setParams({
            "statementSelected" : selected
        });
        // Fire the event
        parametersEvent.fire();
    },

    saveOpp : function (component, event, helper) {
        helper.saveOpportunityDetails(component);
    }
})