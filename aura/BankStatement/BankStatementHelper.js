//BankStatementHelper
({
    getBankStmt : function(component, selectedStmt, firstTime) {
        var opp = component.get("c.getBankStatementEntity");
        opp.setParams({
            "oppId": component.get("v.recordId"),
            "statementToSearch": selectedStmt,
            "isFirstTimeLoading" : firstTime
        });
        opp.setBackground();
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.bankStatementEntity", response.getReturnValue());

                //Fire Event_App_UpdateStmt so other components use this same statement data
                var statement = response.getReturnValue().statement;
                if (!$A.util.isUndefined(statement) && !$A.util.isEmpty(statement)) {
                    if(statement.requestCode !== null){ //Assuming the requestCode is always present in the statement

                        if(!$A.util.isUndefined(statement.asOfDateSimple)){
                            if( statement.asOfDateSimple.substring(0,4) === "1900" ){
                                statement.asOfDateSimple = "";
                            }
                        }

                        if( statement.negativeDays == 0 ){
                            statement.negativeDays = "";
                        }

                        component.set("v.bankStatementEntity.statement", statement);
                        component.set("v.dlAccountsFound", response.getReturnValue().accountsFound);

                        this.triggerUpdateStmt(statement);
                    }
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(opp);

    },

    triggerUpdateStmt : function(statement){
        var updateStmtEvent = $A.get("e.c:Event_App_UpdateStmt");
        updateStmtEvent.setParams({
            "bankStatement": statement
        });
        updateStmtEvent.fire();
    },
    validateSummaryFields:function(component) {
       var availableBalance = component.get("v.bankStatementEntity.statement.availableBalance");
       var currentBalance = component.get("v.bankStatementEntity.statement.currentBalance");
       var totalCredits = component.get("v.bankStatementEntity.statement.totalCredits");
       var totalDebits = component.get("v.bankStatementEntity.statement.totalDebits");
       var asOfDateSimple = component.get("v.bankStatementEntity.statement.asOfDateSimple");
       var averageBalance = component.get("v.bankStatementEntity.statement.averageBalance");
       var averageBalanceRecent = component.get("v.bankStatementEntity.statement.averageBalanceRecent");
       var negativeDays = component.get("v.bankStatementEntity.statement.negativeDays");
        
       return ((availableBalance === undefined || availableBalance === null) &&
                (currentBalance === undefined || currentBalance !== null) &&
                (totalCredits === undefined || totalCredits === null) &&
                (totalDebits === undefined || totalDebits === null) &&
                (asOfDateSimple === undefined || asOfDateSimple === null) &&
                (averageBalance === undefined || averageBalance === null) &&
                (averageBalanceRecent === undefined || averageBalanceRecent === null) &&
                (negativeDays === undefined || negativeDays === null));
    }
})