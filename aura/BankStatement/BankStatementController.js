//BankStatementController.js
({
    init: function (cmp, event, helper) {
        var existingBankStmt = cmp.get("v.bankStatementEntity");
        var existingRecordId = cmp.get("v.recordId");
        if( (existingBankStmt === null || existingBankStmt == '') && existingRecordId !== null){
            helper.getBankStmt(cmp, null, true);
        }
    },

    processNewStatement : function(component, event, helper) {
        var selectedStmt = event.getParam("statementSelected");
        if( selectedStmt === null || selectedStmt == '' || selectedStmt == '--None--'){
            component.set("v.bankStatementEntity", new Object() );
            helper.triggerUpdateStmt('');
        } else{
            helper.getBankStmt(component, selectedStmt, false);
        }
    },

    processNewAccountTyped : function(component, event, helper) {
        var currentBalance = component.get("v.bankStatementEntity.statement.currentBalance");
        var accountTyped = event.getParam("accountTyped");
        var accountNumbersValues = JSON.parse(JSON.stringify(component.get("v.dlAccountsFound")));
        var matchStatement = false;
        console.log('component.get("v.dlAccountsFound") '+ JSON.stringify(component.get("v.dlAccountsFound")));
        if(event.getParam("displaySummaryFieldsWarning"))
        if((Object.keys(component.get("v.dlAccountsFound")).length > 1) && (!helper.validateSummaryFields(component))){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title" : "Warning",
                "message": "Please verify that the account summary information corresponds to the account and routing numbers.",
                "duration": "1000",
                "type": "warning",
                "mode": "dismissible"
            });
            toastEvent.fire();
        }
        
        if((accountTyped === null || accountTyped == '') && (helper.validateSummaryFields(component))){
            component.set("v.bankStatementEntity", null );
        } else{   
            for(var i=0;i< accountNumbersValues.length; i++){
                if(accountTyped.includes((accountNumbersValues[i].value.split('*').join('')).replace(/^0+/, '')))
                matchStatement = true;
            }
            if(matchStatement){
                helper.getBankStmt(component, accountTyped, false);
            }
        }

    }
})