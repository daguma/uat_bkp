//Applicants_MiscellaneousController
({
    onTitleButtonClick: function (component, event, helper) {
        var cmp1 = component.find("parentDiv");
        $A.util.toggleClass(cmp1, "slds-is-open");
    },

    callQuickSaveApp: function (component, event, helper) {
        var compEvent = component.getEvent("ApplicationDetails_SaveEvt");
        compEvent.fire();
    },

    saveDetails: function (component, event, helper) {
        var miscAppList = component.get("v.miscEmployedList");

        var arrayLength = miscAppList.length;
        var fullList = [];
        for (var i = 0; i < arrayLength; i++) {
            var item = miscAppList[i];
			// MAINT-351
            item.received = item.isReceived === true ? "true" : "false";
            item.approved = item.isApproved === true ? "true" : "false";
            item.required = item.isRequired === true ? "true" : "false";
            item.income = $A.util.isUndefined(item.incomeVal) ? "" : "" + item.incomeVal;

            if ($A.util.isUndefined(item.receivedDateVal)) {
                item.receivedDate = item.receivedDateVal;
            } else {
                if( item.receivedDateVal.length === 10 ){
                    var temp = item.receivedDateVal.replace(/-/g, "/")
                    var dd = temp.substr(temp.lastIndexOf("/"), temp.length);
                    var mm = temp.substr(temp.indexOf("/") + 1, temp.lastIndexOf("/"));
                    var mm = mm.substr(0, mm.indexOf("/"));
                    var yyyy = "/" + temp.substr(0, temp.indexOf("/"));
                    item.receivedDate = mm + dd + yyyy;
                }else{
                    delete item["receivedDateVal"];
                    delete item["receivedDate"];
                }
            }

            item.dispositions = item.dispositions === "--None--" ? "" : item.dispositions;

            fullList.push(item);
        }
        return fullList;
    },

    onCompletedByClick : function (component, event) {
        var userName = event.getSource().get("v.label");
        var index = event.getSource().get("v.title");
        var cmp = component.find("completedByInput");
        cmp[index].set("v.value",userName);
    }
})