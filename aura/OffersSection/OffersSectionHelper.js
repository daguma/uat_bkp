//OffersSectionHelper
({
    updateAonMilitarySelectedOptions: function(component) {

        var selectedOptions = component.get("v.selectedAonMilitaryOptions");

        if( !$A.util.isEmpty(selectedOptions) ){
            var reasons = String(selectedOptions).split(",");
            selectedOptions = reasons[0];
            for(var i = 1; i < reasons.length; i++){
                selectedOptions += ";" + reasons[i];
            }
        }else{
            selectedOptions = "";
        }

        component.set("v.urOfferEntity.application.AON_Military_Options__c", selectedOptions);

        var action = component.get("c.updateAonMilitaryOptions");
        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setBackground();
        
        action.setCallback(this, function(response) {
            
            var status = response.getState();
            
            if (status === "SUCCESS") {
                console.log('OffersSectionHelper, function : updateAonMilitarySelectedOptions , Status: SUCCESS');
                
            }else if (status === "INCOMPLETE") {
                console.log('OffersSectionHelper, function : updateAonMilitarySelectedOptions , Status: INCOMPLETE');
            }else if (status === "ERROR") {
                
                console.log('OffersSectionHelper, function : updateAonMilitarySelectedOptions , Status: ERROR');
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {              
                    console.log("Error message: " + errors[0].message);            
                } else {
                    console.log("Unknown error");
                }
            }
        });     
        $A.enqueueAction(action); 
    },

    validateIsAonBundle: function(component) {
        var isBundle;
        var action = component.get("c.isAonBundle");
        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });
        action.setBackground();
        action.setCallback(this, function(response) {
            
            var status = response.getState();
            
            if (status === "SUCCESS") {
                
                var result = response.getReturnValue();
                console.log('OffersSectionHelper, function : validateIsAonBundle , Status: SUCCESS');
                component.set("v.urOfferEntity.isAonBundle",result);
                component.set("v.urOfferEntity.application.AON_Enrollment__c", result);
                component.set("v.urOfferEntity.application.AON_isBundle__c",result);
                
            }else if (status === "INCOMPLETE") {
                console.log('OffersSectionHelper, function : validateIsAonBundle , Status: INCOMPLETE');
            }else if (status === "ERROR") {
                
                console.log('OffersSectionHelper, function : validateIsAonBundle , Status: ERROR');
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {              
                    console.log("Error message: " + errors[0].message);            
                } else {
                    console.log("Unknown error");
                }
            }
        });     
        $A.enqueueAction(action); 
    },

    isDisableAONOptionalProduct: function(component) {
        var isBundle;
        var action = component.get("c.isDisableAONOptionalProduct");
        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });
        action.setBackground();
        action.setCallback(this, function(response) {
            
            var status = response.getState();
            
            if (status === "SUCCESS") {
                
                var result = response.getReturnValue();
                console.log('OffersSectionHelper, function : isDisableAONOptionalProduct , Status: SUCCESS');
                
                component.set("v.urOfferEntity.isDisableAONOptionalProduct",result);
                
            }else if (status === "INCOMPLETE") {
                console.log('OffersSectionHelper, function : isDisableAONOptionalProduct , Status: INCOMPLETE');
            }else if (status === "ERROR") {
                
                console.log('OffersSectionHelper, function : isDisableAONOptionalProduct , Status: ERROR');
                var errors = response.getError();
                
                if (errors && errors[0] && errors[0].message) {              
                    console.log("Error message: " + errors[0].message);            
                } else {
                    console.log("Unknown error");
                }
            }
        });     
        $A.enqueueAction(action); 
    },

    regeneratingOffersAon: function(component,helper,checked) {
        var action = component.get("c.reGenerateOffers");

        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {

            var result = response.getReturnValue();
            var status = response.getState();

            if (status === "SUCCESS"){ 
                if(!result.hasErrorMessage) {

                    console.log('Regenerated Offers AON');
                    console.log('OffersSectionHelper, function : regeneratingOffersAon , Status: SUCCESS');

                    component.set("v.urOfferEntity", result); //Offers generated, DB field updated correctly

                    helper.validateIsAonBundle(component);

                    component.set("v.urOfferEntity.application.AON_Enrollment__c", component.get("v.urOfferEntity.isAonBundle") ? true : checked);
            
                    helper.updateAonMilitarySelectedOptions(component,event);

                    helper.isDisableAONOptionalProduct(component);

                    component.find("existingOffers").offersReload();

                }else {
                    console.log('Failed to Regenerate Offers Aon');
                    this.showToastAlert("Error!", result.errorMessage, "error", 2000);
                }
            }else if(status === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error Rejection Block');
                this.showToastAlert("Error!", result.errorMessage, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },

    aonMilitaryHandle: function(component, event, helper) {

        var checked = event.getSource().get("v.checked");
        var AonAditionalPaymentAmount = component.get("v.urOfferEntity.AONAditionalPaymentAmount");
        var aonErrollment = component.get("v.urOfferEntity.application.AON_Enrollment__c");

        if(confirm('By selecting this option, you are specifying that the contact is a direct relative to a Military employee. This will result in the AON insurance costs being covered by LendingPoint. Due to this, a recalculation of the offers will also occur. Please make sure this is correct before proceeding.')){

            component.set("v.urOfferEntity.application.AON_Military__c", checked);
            component.set("v.urOfferEntity.application.AON_Enrollment__c", false);
            helper.regeneratingOffersAon(component,helper,checked);

        }else {
            component.set("v.urOfferEntity.application.AON_Military__c", !checked);
        }
    },

    saveApplication: function(component) {
        var action = component.get("c.saveApp");

        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                if(!result.hasErrorMessage) {
                    component.set("v.urOfferEntity", result);
                    this.showToastAlert("", "Reason saved successfully.", "success", 1000);
                    $A.get('e.force:refreshView').fire();
                }else {
                    this.showToastAlert("Error!", result.errorMessage, "error", 2000);
                }
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error Rejection Block');
                this.showToastAlert("Error!", result.errorMessage, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },

    validateField: function(component, fieldId) {
        var isValid = true;
        var fieldCmp = component.find(fieldId);
        if(!fieldCmp.get("v.value")) {
            fieldCmp.set("v.errors", [{message:"Field is required"}]); // Set Error
            isValid = false;
        }else fieldCmp.set("v.errors", null); // Clear Error
        return isValid;
    },

    validateFeeHandling: function(component) {
        var app = component.get("v.urOfferEntity");
        var feeHandleOldVal = component.get("v.feeHandlingVal");
        var field = component.find("feeHandling");
        if(app.application.Is_Selected_Offer__c) {
            this.showToastAlert("Warning!", "You cannot change the fee handling if an offer is selected.", "warning", 1000);
            return false;
        }else {
            if(confirm("Are you sure you want to change this Origination Fee?\nThis action will re-generates the offers and cannot be undone.")) {
                component.set("v.feeHandlingVal", field.get("v.value"));
                return true;
            }else {
                field.set("v.value", feeHandleOldVal);
                return false;
            }
        }

    },

    regeneratingOffers: function(component) {
        var action = component.get("c.reGenerateOffers");

        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                if(!result.hasErrorMessage) {
                    console.log('Regenerated Offers');
                    component.set("v.urOfferEntity", result);
                    component.find("existingOffers").offersReload();
                }else {
                    console.log('Failed to Regenerate Offers');
                    this.showToastAlert("Error!", result.errorMessage, "error", 2000);
                }
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error Rejection Block');
                this.showToastAlert("Error!", result.errorMessage, "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },
    regenerateRepaymentFrequency: function(component) {
        var action = component.get("c.rePaymentFrequencyGeneration");
        
        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {     
                this.showToastAlert("","Repayment frequency updated.","success",100);
                component.set("v.urOfferEntity", result);
                component.find("existingOffers").offersReload();
            }else if (response.getState() === "ERROR") {
                this.parseError(response);
            }
            else {
                console.log("Repayment frequency Incomplete");
            }
        })     
        $A.enqueueAction(action);
    },
    changingPaymentorPrincipal: function(component, methodName) {
        var action = component.get(methodName);

        action.setParams({
            "uroEntityString": JSON.stringify(component.get("v.urOfferEntity")),
        });

        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                console.log('RECALCULATION :- ' + result);
                if(!$A.util.isEmpty(result)) {                 
                    if(result.includes("WARNING")) {
                        result = result.replace("WARNING :", "").trim();
                        this.showToastAlert("Warning!", result, "warning", 10000);
                    }else {
                        result = result.replace("ERROR : ", "").trim();
                        this.showToastAlert("Error!", result, "error", 10000);
                    }
                }else {
                    this.showToastAlert("Success!", 'Recalculation of offers done successfully', "success", 2000);
                    component.find("existingOffers").offersReload();
                }
            }else if(response.getState() == "ERROR"){ 
                this.parseError(response);
                return false;
            }
        });
        $A.enqueueAction(action);
    },

    parseError: function(response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                this.showToastAlert("Error!", errors[0].message, "error", 2000);
            }
        } else {
            this.showToastAlert("", "Unkown Error", "error", 2000);
        }
    },
    
    showToastAlert: function(title, message, type, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type,
            "mode": "dismissible",
            "duration": time
        });
        toastEvent.fire();
    },

    loadIncompletePackage : function (component) {
        var action = component.get("c.loadIncPkgReasons");
        action.setParams({
            "oppId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                    component.set("v.incPakReasonsList", result.incPackageReasonList);
                    component.set("v.selectedIncPakReasons", result.selectedIncPakReasons);
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error Rejection Block');
                this.showToastAlert("Error!", "Error at OffersSectionHelper.loadIncPkgReasons()", "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },

    getAonMilitaryOptions : function (component) {
        var action = component.get("c.loadAonMilitaryOptions");
        action.setParams({
            "oppId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                result = JSON.stringify(result);
                result = JSON.parse(result);
                console.log('** getAonMilitaryOptions SUCCESS, result: ' + result);
                console.log('** get optionList:  ' + JSON.stringify(result.optionsList));
                //console.log('** get optionList:  ' + result.optionsList[0].label);
                component.set("v.aonMilitaryOptionsList", result.optionsList);
                component.set("v.selectedAonMilitaryOptions", result.selectedOptions);
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error Rejection Block');
                this.showToastAlert("Error!", "Error at OffersSectionHelper.getAonMilitaryOptions()", "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },

    saveStatusAndReasons : function (component) {

        var urOfferEntity = component.get("v.urOfferEntity");
        urOfferEntity.itemRequested = component.get("v.itemRequested");
        urOfferEntity.prevAppStatus = component.get("v.prevAppStatus");
        urOfferEntity.application = this.getOppDetails(component);

        var action = component.get("c.saveApp");
        action.setParams({
            "uroEntityString": JSON.stringify(urOfferEntity)
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            if (response.getState() == "SUCCESS") {
                if(result.hasErrorMessage){
                    this.showToastAlert("Error!", result.errorMessage, "error", 2000);
                }else{
                    this.showToastAlert("Saved!", "Application saved", "success", 2000);
                    $A.get('e.force:refreshView').fire();
                }
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error Rejection Block');
                this.showToastAlert("Error!", "Error at OffersSectionHelper.saveStatusAndReasons()", "error", 2000);
            }
        });
        $A.enqueueAction(action);
    },

    getOppDetails : function (component) {
        var opp = component.get("v.urOfferEntity.application");
        opp.Status__c = $A.util.isEmpty(component.find("oppStatus").get("v.value")) ? opp.Status__c : component.find("oppStatus").get("v.value");
        opp.Reject_Offer_Reason__c = $A.util.isEmpty(component.find("agedCancelReason").get("v.value")) ? opp.Reject_Offer_Reason__c : component.find("agedCancelReason").get("v.value");
        opp.Reason_of_Opportunity_Status__c = $A.util.isEmpty(component.find("declinedReason_offersTab").get("v.value")) ? opp.Reason_of_Opportunity_Status__c : component.find("declinedReason_offersTab").get("v.value");
        opp.Items_Requested__c = $A.util.isEmpty(component.find("itemsRequested").get("v.value")) ? opp.Items_Requested__c : component.find("itemsRequested").get("v.value");
        opp.Incomplete_Package_Reason__c = component.get("v.selectedIncPakReasons");
        if( !$A.util.isEmpty(opp.Incomplete_Package_Reason__c) ){
            var reasons = String(opp.Incomplete_Package_Reason__c).split(",");
            opp.Incomplete_Package_Reason__c = reasons[0];
            var i;
            for(i = 1; i < reasons.length; i++){
                opp.Incomplete_Package_Reason__c += ";" + reasons[i];
            }
        }else{
            opp.Incomplete_Package_Reason__c = "";
        }
        opp.Decline_Note__c = component.find("declinedNote").get("v.value");
        opp.Insufficient_Documentation__c = $A.util.isEmpty(component.find("insuffDoc").get("v.value")) ? opp.Insufficient_Documentation__c : component.find("insuffDoc").get("v.value");
        opp.Info_Docs_Unable_to_Verify__c = $A.util.isEmpty(component.find("docsUnableToVerify").get("v.value")) ? opp.Info_Docs_Unable_to_Verify__c : component.find("docsUnableToVerify").get("v.value");
        return opp;
    }
})