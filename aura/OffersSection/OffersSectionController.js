//OffersSectionController
({
    initialize: function (component, event, helper) {
        helper.loadIncompletePackage(component);
        helper.getAonMilitaryOptions(component);
    },
    saveApplication: function (component, event, helper) {
        if (helper.validateField(component, 'DecReason'))
            helper.saveApplication(component);
    },
    regeneratingOffers: function (component, event, helper) {
        if (helper.validateFeeHandling(component))
            helper.regeneratingOffers(component);
    },
    regeneratingRepaymentFreq : function(component, event, helper) {
        helper.regenerateRepaymentFrequency(component);
    },
    AONregeneratingOffers : function(component, event, helper) {
        var checked = event.getSource().get("v.checked");
        if(confirm('Are you sure you want to change the AON Enrollment?\nThis action will re-generate the offers and cannot be undone'))
            helper.regeneratingOffers(component);
        else {
        	component.set("v.urOfferEntity.application.AON_Enrollment__c", !checked);
            //component.set("v.urOfferEntity.application.AON_Military__c",!checked);
        }
    },
    changingPayment : function(component, event, helper) {
        if(confirm('Are you sure you want to proceed?\nThis action will delete all the custom offer(s) and cannot be undone'))
            helper.changingPaymentorPrincipal(component, "c.changePayment");
    },
    changingPrincipal : function(component, event, helper) {
        if(confirm('Are you sure you want to proceed?\nThis action will delete all the custom offer(s) and cannot be undone'))
            helper.changingPaymentorPrincipal(component, "c.changePrincipal");
    },
    handleIncPakReasonsChange: function (component, event, helper) {
        // get the updated/changed values
        component.set("v.selectedIncPakReasons", event.getParam("value"));
    },
    saveStatusReasons : function (component, event, helper) {
        helper.saveStatusAndReasons(component);
    },
    handleMilitaryOptionsChange: function (component, event, helper) {
        // get the updated/changed values
        component.set("v.selectedAonMilitaryOptions", event.getParam("value"));
        //console.log('handleMilitaryOptionsChange values: ' + component.get("v.selectedAonMilitaryOptions"));
        helper.updateAonMilitarySelectedOptions(component,event);
    },

    AonBundle : function (component, event, helper) {
        var checked = event.getSource().get("v.checked");
        helper.aonMilitaryHandle(component,event,helper);
    }
})