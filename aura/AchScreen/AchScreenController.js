//AchScreenController
({
    init : function(component, event, helper) {
        var opp = component.get("c.getAchScreenEntity");
        opp.setParams({
            "oppId": component.get("v.recordId")
        });
        opp.setBackground();

        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.achTabEntity", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(opp);
    }
})