({
    getBridger : function(cmp) {
        var action = cmp.get('c.getBridgerDetails');
        action.setParams({
            "oppId": cmp.get("v.recordId")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				console.log(response.getReturnValue());
                cmp.set('v.BridgerList', response.getReturnValue());

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    }
})