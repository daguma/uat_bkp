({
    activeTab : function(component, event, helper) {
        var clicked = event.target.id;
        var tab2, tab3, tab4;
        
        if(clicked === 'cr-detail') {
            tab2 = 'cr-criteria';
            tab3 = 'cr-other';
            tab4 = 'cr-attachment';
        }
        if(clicked === 'cr-criteria') {
            tab2 = 'cr-detail';
            tab3 = 'cr-other';
            tab4 = 'cr-attachment';
        }
        if(clicked === 'cr-other') {
            tab2 = 'cr-criteria';
            tab3 = 'cr-detail';
            tab4 = 'cr-attachment';
        }
        if(clicked === 'cr-attachment') {
            tab2 = 'cr-criteria';
            tab3 = 'cr-detail';
            tab4 = 'cr-other';
        }
        
        var tabId = component.find(clicked + '-tab');
        var tabData = component.find(clicked + '-data');
        //show and Active fruits tab
        $A.util.addClass(tabId, 'slds-is-active');
        $A.util.addClass(tabData, 'slds-show');
        $A.util.removeClass(tabData, 'slds-hide');
        
        // Hide and deactivate others tab
        var tabTwo = component.find(tab2 + '-tab');
        var TabTwodata = component.find(tab2 + '-data');
        $A.util.removeClass(tabTwo, 'slds-is-active');
        $A.util.removeClass(TabTwodata, 'slds-show');
        $A.util.addClass(TabTwodata, 'slds-hide');
        
        var tabThree = component.find(tab3 + '-tab');
        var TabThreeData = component.find(tab3 + '-data');
        $A.util.removeClass(tabThree, 'slds-is-active');
        $A.util.removeClass(TabThreeData, 'slds-show');
        $A.util.addClass(TabThreeData, 'slds-hide');
        
        var tabFour = component.find(tab4 + '-tab');
        var TabFourdata = component.find(tab4 + '-data');
        $A.util.removeClass(tabFour, 'slds-is-active');
        $A.util.removeClass(TabFourdata, 'slds-show');
        $A.util.addClass(TabFourdata, 'slds-hide');
    },
    showTooltip : function(component, event, helper) {
        var labelId = event.currentTarget.id;
        var element = document.getElementById(labelId + '-tooltip');
        element.classList.remove("slds-fall-into-ground");
        element.classList.add("slds-rise-from-ground");
    },
    hideTooltip : function(component, event, helper) {
        var labelId = event.currentTarget.id;
        var element = document.getElementById(labelId + '-tooltip');
        element.classList.remove("slds-rise-from-ground");
        element.classList.add("slds-fall-into-ground");
    },
    showRecord : function(component, event, helper) {
        event.preventDefault();
        var recordId = event.currentTarget.id;
        if(recordId == 'showAttachment')
            component.set("v.showReportTxt", true);
        else {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId,
                "slideDevName": "detail",
                "isredirect": true
            });
            navEvt.fire();
        }
    }
})