({
    doInit : function(component, event, helper) {
        
        var totalCnt = component.get("c.getTotalCount");
        totalCnt.setParams({
            "parentId": component.get("v.recordId")
        });
        totalCnt.setCallback(this, function(a) {
            component.set("v.totalNumberOfRows", a.getReturnValue());
        });
        $A.enqueueAction(totalCnt);       
        
        var actions = [
            { label: 'View', name: 'view' },
            { label: 'Delete', name: 'delete' }
        ];
        
        component.set('v.columns', [
            { type: 'action', typeAttributes: { rowActions: actions } },
            {label: 'Name', fieldName: 'Name', type: 'text',sortable:true },
            {label: 'Owner', fieldName: 'OwnerName', type: 'text',sortable:true },
            {label: 'Last Modified', fieldName: 'LastModifiedDate', type: 'datetime',sortable:true }
        ]);
        helper.getData(component);
    },
    // Client-side controller called by the onsort event handler
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'view':
                var attId = row.Id;               
                var url = "/servlet/servlet.FileDownload?file=" + attId;
                window.open(url, '_blank');
                break;
            case 'delete':
                var rows = cmp.get('v.data');
                var rowIndex = rows.indexOf(row);
                var deleteAct = cmp.get("c.deleteAttachments");
                deleteAct.setParams({ ids : rows[rowIndex].Id });
                $A.enqueueAction(deleteAct);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The attachment has been delete successfully.",
                    "type": "success"
                });
                toastEvent.fire();
                rows.splice(rowIndex, 1);
                cmp.set('v.totalNumberOfRows', rows.length);
                cmp.set('v.data', rows);
                break;    
        }
    },
})