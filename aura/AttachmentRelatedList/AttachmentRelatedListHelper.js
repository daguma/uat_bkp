({
    getData : function(component) {
        
        var action = component.get("c.getAttachmentList");
        action.setParams({
            "parentId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            var rows = a.getReturnValue();
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                if (row.Owner.Name) row.OwnerName = row.Owner.Name;
                if(row.LastModifiedDate) row.LastModifiedDate = $A.localizationService.formatDate(row.LastModifiedDate, "MM/DD/YYYY hh:mm a");
            }
            component.set("v.data", rows);
            component.set("v.currentCount", component.get("v.initialRows"));
            
        });
        $A.enqueueAction(action);
    },
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.data");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.data", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
    
})