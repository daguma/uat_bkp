//Applicants_SelfEmployedTableController
({
    callQuickSaveApp: function (component, event, helper) {
        var compEvent = component.getEvent("ApplicationDetails_SaveEvt");
        compEvent.fire();
    },

    saveDetails: function (component, event, helper) {
        var selfEmpAppList = component.get("v.selfEmployedList");

        var arrayLength = selfEmpAppList.length;
        var fullList = [];
        for (var i = 0; i < arrayLength; i++) {
            var item = selfEmpAppList[i];

            item.income = $A.util.isUndefined(item.incomeVal) ? "" : "" + item.incomeVal;
            item.netIncome = $A.util.isUndefined(item.netIncomeVal) ? "" : "" + item.netIncomeVal;
            //MAINT-351
            item.received = item.isReceived === true ? "true" : "false";
            item.approved = item.isApproved === true ? "true" : "false";
            item.required = item.isRequired === true ? "true" : "false";

            if ($A.util.isUndefined(item.calendarValueVal)) {
                //item.calendarValue = "";
            } else {
                if (item.calendarValueVal.length > 4) {
                    var temp = item.calendarValueVal.replace(/-/g, "/");
                    var dd = temp.substr(temp.lastIndexOf("/"), temp.length);
                    var mm = temp.substr(temp.indexOf("/") + 1, temp.lastIndexOf("/"));
                    var mm = mm.substr(0, mm.indexOf("/"));
                    var yyyy = "/" + temp.substr(0, temp.indexOf("/"));
                    item.calendarValue = mm + dd + yyyy;
                }else{
                    delete item["calendarValue"];
                }
            }

            if ($A.util.isUndefined(item.receivedDateVal)) {
                item.receivedDate = item.receivedDateVal;
            } else {
                if( item.receivedDateVal.length === 10){
                    var temp = item.receivedDateVal.replace(/-/g, "/")
                    var dd = temp.substr(temp.lastIndexOf("/"), temp.length);
                    var mm = temp.substr(temp.indexOf("/") + 1, temp.lastIndexOf("/"));
                    var mm = mm.substr(0, mm.indexOf("/"));
                    var yyyy = "/" + temp.substr(0, temp.indexOf("/"));
                    item.receivedDate = mm + dd + yyyy;
                }else{
                    delete item["receivedDate"];
                    delete item["receivedDateVal"];
                }
            }

            item.dispositions = item.dispositions === "--None--" ? "" : item.dispositions;
            fullList.push(item);
        }
        return fullList;
    },

    onCompletedByClick : function (component, event) {
        var userName = event.getSource().get("v.label");
        var index = event.getSource().get("v.title");
        var cmp = component.find("completedByInput");
        cmp[index].set("v.value",userName);
    }
})