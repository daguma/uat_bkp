({
	doInit : function(component, event, helper) {
		helper.applyCSS(component);
	},
    closeModal : function(component, event, helper) {
        helper.revertCssChange(component);
        document.getElementById("attachNoteContainer").style.display = "none";
    },
    savingNote : function(component, event, helper) {
        // #1 Get Opportunity Details
        var actionSaveNote = component.get("c.saveNote");
        
    	actionSaveNote.setParams({
            "n": component.get("v.noteEntity")
        });
        
        // Add callback behavior for when response is received
        actionSaveNote.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result) {
                	var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    	"title": "Success!",
                        "message": "Note saved successfully!",
                        "duration": 2000,
                        "mode": "dismissible",
                        "type": "success"
                    });
                    toastEvent.fire();
                    //Close Note Modal
                    helper.revertCssChange(component);
        			document.getElementById("attachNoteContainer").style.display = "none";
                }else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    	"title": "Failed!",
                        "message": "Note failed to save!",
                        "duration": 2000,
                        "mode": "dismissible",
                        "type": "error"
                    });
                    toastEvent.fire();
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        // Send action off to be executed
        $A.enqueueAction(actionSaveNote);
    }
})