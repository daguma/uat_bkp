//RefinanceAlertHelper
({
    getRefiAppByContactSuccess : function (component,response) {
        if (!$A.util.isUndefinedOrNull(response) && !$A.util.isEmpty(response)) {
            if(response.hasError){
                component.set("v.associatedRefiAppName","")
                component.set("v.hasRefinanceApp",false);
                component.set("v.hasError", true);
                console.log(response.errorMessage);
            }else{
                if (!$A.util.isUndefinedOrNull(response.refiAppsCSV) && !$A.util.isEmpty(response.refiAppsCSV)) {
                    component.set("v.associatedRefiAppName",response.refiAppsCSV)
                    component.set("v.hasRefinanceApp",true);
                }
            }
        }else{
            component.set("v.hasRefinanceApp",false);
        }
    }
})