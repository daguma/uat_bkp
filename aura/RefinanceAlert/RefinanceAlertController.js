//RefinanceAlertController
({
    doInit : function (component, event, helper) {

        // Get server action service
        const server = component.find('server');

        var parametersMap = {};
        parametersMap["contactId"] = component.get("v.recordId");

        // Call server-side action with parameters and callback
        server.callServer(
            component.get('c.getRefiAppByContact'), // Server-side action
            {"contactId": component.get("v.recordId")}, // Action parameters
            false, // Disable cache
            $A.getCallback(response => { // Custom success callback
                // Handle response
                helper.getRefiAppByContactSuccess(component,response);
            }),
            $A.getCallback(errors => { // Custom error callback
                // Handle errors
            }),
            false, // Disable built-in error notification
            false, // Disable background
            false // Not abortable
        );

    }
})