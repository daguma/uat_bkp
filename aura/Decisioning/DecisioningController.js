//DecisioningController.js
({
    init: function(component, event, helper) {
        component.set("v.showSpinner", true);
        var decEntityAction = component.get("c.getDecisioningEntity");
        decEntityAction.setParams({
            "oppId": component.get("v.recordId")
        });
        decEntityAction.setBackground();
        decEntityAction.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.decisioningEntity", response.getReturnValue() );
                //console.log(response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(decEntityAction);
    },
    overridingDisqualifiers : function(component, event, helper) {
        component.set("v.showSpinner", true);
        var decEntity = component.get("v.decisioningEntity");
        var overrideAction = component.get("c.overrideDisqualifier");
        
        overrideAction.setParams({
            "opp": decEntity.opportunity,
            "reportId": decEntity.crId,
            "displayOldView": decEntity.isPreBRMS
        });
        
        overrideAction.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Override Disqualifier Success');
                $A.get('e.force:refreshView').fire();
            }
            else {
                console.log("Failed with state: " + state);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(overrideAction);
    },
    softCreditPull : function(component, event, helper) {
        component.set("v.showSpinner", true);
		var decEntity = component.get("v.decisioningEntity");
        var softPullAction = component.get("c.fetchNewSoftCreditPull");
        
        softPullAction.setParams({
            "opp": decEntity.opportunity
        });
        
        softPullAction.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('SOFT PULL SUCCESS');
                $A.get('e.force:refreshView').fire();
            }
            else {
                console.log("SOFT PULL FAILED:  " + state);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(softPullAction);        
    },
    hardCreditPull : function(component, event, helper) {
        component.set("v.showSpinner", true);
        var decEntity = component.get("v.decisioningEntity");
        var hardPullAction = component.get("c.fetchNewHardCreditPull");
        hardPullAction.setParams({
            "oppId": decEntity.opportunity.Id
        });
        hardPullAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('HARD PULL SUCCESS');
                var result = response.getReturnValue();
                if (result.hasError) {
                	var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    	"title": "Failed!",
                        "message": result.errorDetails ,
                        "duration": 2000,
                        "mode": "dismissible",
                        "type": "error"
                    });
                    toastEvent.fire();
                } else {
                    $A.get('e.force:refreshView').fire();    
                }
            }
            else {
                console.log("HARD PULL FAILED:  " + state);
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(hardPullAction);
    },
    verifyEmailIP : function(component, event, helper) {
        component.set("v.showSpinner", true);
        var decEntity = component.get("v.decisioningEntity");
        var verifyEmailIPAction = component.get("c.fetchNewEmailAgeResult");
        
        verifyEmailIPAction.setParams({
            "opp": decEntity.opportunity,
            "reportId": decEntity.crId,
            "displayOldView": decEntity.isPreBRMS
        });
        
        verifyEmailIPAction.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('VERIFY EMAIL/IP SUCCESS');
            }
            else {
                console.log("VERIFY EMAIL/IP FAILED:  " + state);
            }
            component.set("v.showSpinner", false);
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(verifyEmailIPAction); 
    },

    closeModel : function (component) {
        component.set('v.showErrorDetails', "false");
    },

    displayErrDetails : function (component) {
        component.set('v.showErrorDetails', "true");
    },

    openFundingApproval: function(component, event,helper) {
         
        component.set('v.isButtonFundingApprovalOpened', "true");
    },
    closeModel: function(component, event, helper) {
         
        helper.closeTheModel(component);
    },
    sendRequestFundingApproval: function(component, event,helper){
        helper.sendRequestFundingApproval(component);

    }

})