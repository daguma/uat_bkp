({
	helperMethod : function() {
		
	},
    closeTheModel : function (component) {
    	
        component.set("v.isButtonFundingApprovalOpened", false);
    },
    sendRequestFundingApproval: function(component){
        var action = component.get("c.createFundingApproval");
        
        action.setParams({
            "oppId": component.get("v.decisioningEntity.opportunity.Id"),
            "oppName": component.get("v.decisioningEntity.opportunity.Name"),
            "description": component.find("pBody").get("v.value")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The new case was created.",
                    "type": "success",
                    "mode": "dismissible"
                });
                resultsToast.fire();
            } else if (state === "ERROR") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Error",
                    "message": "The case was not created.",
                    "type": "error",
                    "mode": "dismissible"
                });
                resultsToast.fire();
            }
        }));
        $A.enqueueAction(action);

        //close the model by calling the cancel function
        this.closeTheModel(component);
    }
})