({
    getUniversalNotes : function(component, event, helper) {
        component.set("v.body", []);
        var recordId = component.get("v.recordId");
        
        if(!$A.util.isEmpty(recordId)) {
            helper.loadUniversalNotes(component, recordId);
        }else {
            this.showToastAlert("Error", "No record Id found", "error", 1000)
        }
    }
})