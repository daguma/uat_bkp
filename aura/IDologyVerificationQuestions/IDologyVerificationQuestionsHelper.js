({
    getQuestions : function() {
        //your logic to fetch questions
    },
    removeSkip: function(questions) {
        var i;
        for(i = 0; i < questions.length; i++) {
            var index = questions[i].AnswersList.indexOf('Skip Question');
            if (index !== -1) {
                questions[i].listAnswers.splice(index, 1);
            }
        }
        return questions;
    },
    showToastAlert : function(alertTitle, alertMsg, alertType, alertTime) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "dismissible",
            "duration": alertTime
        });
        toastEvent.fire();
    },
    processIdologyQuestions: function(component) {
        component.set("v.showSpinner", true);
        component.getEvent("processQuestions").setParams({
            "process" : "Yes"
        }).fire();    
    },
    validateIdologyQuestions: function(component) {
        var answered = 0;
        var questions = component.get("v.fourQuestions");
        for(var i = 0; i < questions.length; i++) {
            var quest = questions[i];
            if(!$A.util.isEmpty(quest.selectedAnswer) &&
               quest.selectedAnswer != 'Select Answer' && 
               quest.selectedAnswer != 'Skip Question')
                answered++;
        }
        
        if(answered >= 4) {
            return true;
        } else {
            this.showToastAlert("Error", "Please answer atleast 4 questions", "error", true);
            return false;
        }
        return true;     
    }
})