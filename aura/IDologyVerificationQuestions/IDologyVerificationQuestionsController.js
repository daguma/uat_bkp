({
    doInit : function(component, event, helper) {
        var questions = component.get("v.questions");
        var Noofquestions = questions.length;
        
        if(Noofquestions > 4) {
            var lastQuestion = questions[4];
            lastQuestion.skipped = 'yes';
            lastQuestion.selectedAnswer = 'Skip Question';
            questions[4] = lastQuestion;
        }
        
        component.set("v.fourQuestions", questions);
    },
    processIdologyQuestions: function(component, event, helper) {
        if(helper.validateIdologyQuestions(component)) {
            component.set("v.questions", component.get("v.fourQuestions"));
            helper.processIdologyQuestions(component);
        }        	
    },
    cancelprocessIdologyQuestions : function(component, event, helper) {
        component.set("v.showSpinner", true);
        component.getEvent("processQuestions").setParams({
            "process" : "No"
        }).fire();       	
    },
    skipQuestion: function(component, event, helper) {
        var rowNum = event.getSource().get("v.value");
        
        var questions = component.get("v.fourQuestions");
        
        if(questions.length > 4) {
            var lastQuestion = questions[4];
            lastQuestion.skipped = 'no';
            lastQuestion.selectedAnswer = '';
            questions[4] = lastQuestion;
        }
        
        var skipQuest = questions[rowNum];
        skipQuest.selectedAnswer = 'Skip Question';
        skipQuest.skipped = 'yes';
        questions[rowNum] = skipQuest;
        
        component.set("v.skipDisabled", true);       
        component.set("v.fourQuestions", questions);

    }
})