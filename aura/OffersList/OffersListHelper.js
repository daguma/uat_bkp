//OffersListHelper
({
    getOffersEntity: function (component) {
        var opp = component.get("c.getOffersList");
        opp.setParams({
            "oppId": component.get("v.recordId")
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if (result.hasError === true) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": result.errorMessage,
                        "mode": "sticky",
                        "type": "error"
                    });
                    toastEvent.fire();
                } else {
                    component.set("v.offersListEntity", result);
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(opp);
    },

    onRpymntFrqChange : function (component, event) {
        var cmpTriggeredEvt = event.getSource();
        alert( cmpTriggeredEvt.get("v.value") );
    }
})