//OffersListController
({
    doInit: function (component, event, helper) {
        //Load OffersListEntity from server if not loaded yet and if the Opp id is present
        var rec = component.get("v.recordId");
        if (rec !== null) {
            helper.getOffersEntity(component);
        }
    },

    onPreferredPaymtBlur: function (component, event, helper) {
        var cmpTriggeredEvent = event.getSource();
        var rowNumber = cmpTriggeredEvent.get("v.name");
        var offersListEntities = component.get("v.offersListEntity.offerEntities");
        var i = 0;
        while (i < offersListEntities.length) {
            if (i === rowNumber) {
                offersListEntities[i].offer.Preferred_Payment_Date_String__c = cmpTriggeredEvent.get("v.value");
                i = offersListEntities.length;
            } else {
                i++;
            }
        }
        component.set("v.offersListEntity.offerEntities", offersListEntities);
    },

    onRepaymentFreqChange: function (component, event, helper) {
        helper.onRpymntFrqChange(component, event);
    }
})