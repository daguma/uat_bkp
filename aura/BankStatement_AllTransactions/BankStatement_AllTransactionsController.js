//BankStatement_AllTransactionsController
({
    init : function (cmp, event, helper) {
        var isContactIdOk = !($A.util.isUndefined(cmp.get("v.contactId")) || $A.util.isEmpty(cmp.get("v.contactId")));
        var isOpportunityIdOk = !($A.util.isUndefined(cmp.get("v.opportunityId")) || $A.util.isEmpty(cmp.get("v.opportunityId")));
        var isAccountNumberOk = !($A.util.isUndefined(cmp.get("v.accountNumber")) || $A.util.isEmpty(cmp.get("v.accountNumber")));
        var isTransactionsTypeCodeOk = !($A.util.isUndefined(cmp.get("v.transactionsTypeCode"))); //TypeCode is correctly allowed to be empty!!!
        
        if (isContactIdOk === true
            && isOpportunityIdOk === true
            && isAccountNumberOk === true
            && isTransactionsTypeCodeOk === true) {
            helper.getTxnsList(cmp);
        }
        else{
            var obj = new Object();
            obj.hasError = "true";
            obj.errorMessage = "Contact, Opportunity, Account Number or Type Code not ok!";
            cmp.set("v.allTransactionsEntity", obj);
            console.log("Contact, Opportunity, Account Number or Type Code not ok!");
        }
    }
})