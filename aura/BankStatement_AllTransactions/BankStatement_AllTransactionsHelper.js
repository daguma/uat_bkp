//BankStatement_AllTransactionsHelper
({
    getTxnsList : function(cmp) {
        var action = cmp.get('c.getTransactionsList');
        action.setParams({
            "contactId": cmp.get("v.contactId"),
            "oppId": cmp.get("v.opportunityId"),
            "accountNumber": cmp.get("v.accountNumber"),
            "typeCode": cmp.get("v.transactionsTypeCode")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.allTransactionsEntity', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    }
})