({
    showAlert : function(alertType, alertMsg, alertTitle, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "dismissible",
            "duration": time
        });
        toastEvent.fire();
    },
    CalcYearForJob : function(cmp, checboxID, hireDateVal) {
        if(checboxID == 'emp_hire_date')
            checboxID = "emp_less_than_year";
        if(checboxID == 'emp_SJ_hire_date')
            checboxID = "emp_SJ_less_than_year";
        if(checboxID == 'emp_TJ_hire_date')
            checboxID = "emp_TJ_less_than_year";
        
        var lessthenYearCheck = cmp.find(checboxID);
        var hireDate = new Date(hireDateVal);  
        var todayDate = new Date();
        todayDate.setDate(todayDate.getDate()-365);
        
        if(hireDate > todayDate) {
            lessthenYearCheck.set("v.checked", true);
        }else { 
            lessthenYearCheck.set("v.checked", false);
        }
    },
    EmployerRenderInfo : function(cmp, fieldId, isCheck) {
        if(fieldId == 'does_check_1')
            fieldId = 'emp_name_1';
        if(fieldId == 'does_check_2')
            fieldId = 'emp_name_2';
        if(fieldId == 'does_check_3')
            fieldId = 'emp_name_3';
        
        var empEntity = cmp.get("v.employmentEntity");
        var employerName = "";
        var empName = cmp.find(fieldId);
        
        if(isCheck)
            if(!$A.util.isUndefined(empEntity.opportunity.Contact__r.Employer_Name__c))
                employerName = empEntity.opportunity.Contact__r.Employer_Name__c;
        empName.set("v.value", employerName);
    },
    setFieldError : function(cmp, fieldId) {
        var field = cmp.find(fieldId);
        $A.createComponent(
            "ui:message",
            {
                "title" : "Cannot be greater than today",
                "severity" : "error",
            },
            function(component, status, errorMessage) {
                if (status === "SUCCESS") {
                    
                    var errorCmp = div1.get("v.errorComponent");
                    errorCmp.push(component);
                    field.set("v.errorComponent", errorCmp)
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
            }
        );
    },
    clearFieldError : function(cmp, fieldId) {
        var field = cmp.find(fieldId);
        field.set("v.errorComponent", null);
    },
    SaveEmpDetail : function(cmp, helper) { //Upsert employment details
        var empDetail = cmp.get("v.employmentEntity.empDetail");
        console.log(empDetail);
        var action = cmp.get("c.saveApp");
        
        action.setParams({
            EmpDetail : empDetail
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (!cmp.get("v.FSchanged")) cmp.set("v.showSpinner", false);
            if(state == "SUCCESS"){
                var result = response.getReturnValue();
                
                if(result.isSuccess ){
                    this.showAlert('success', result.message, 'Success!', 1000);
                    $A.get('e.force:refreshView').fire();
                } else {
                    this.showAlert('error', result.message, 'Failed!', 1000);
                }
                
            } else if(state == "ERROR"){
                this.parseError(response);
            }
        });
        
        $A.enqueueAction(action);
        
        // If FundingSpecialist changes, we need to save it into opp.FS_Assigned
        if (cmp.get("v.FSchanged")){
            helper.updateFS(cmp, cmp.get("v.recordId"), cmp.find('FSassigned').get('v.value')); 
        }
    },
    updateFS: function(cmp, recordId, FSassigned) {
        var secondAction = cmp.get("c.saveFSassigned");
        secondAction.setParams({
            "oppId": recordId,
            "newFS": FSassigned
        });
        
        secondAction.setCallback(this, function(resp) {
            cmp.set("v.showSpinner", false)
            var state = resp.getState();
            if(state == "SUCCESS"){
            } else if(state == "ERROR"){
                alert('Error : ' + resp.getReturnValue());
            }
        });
        
        $A.enqueueAction(secondAction);  
    },
    parseError: function(response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                this.showAlert("error", errors[0].message, "Error!", 2000); 
            }
        } else {
            this.showAlert("", "Unkown Error", "error", 2000); 
        }
    },
    verifyingWorkEmail: function(cmp) {
        var empEntity = cmp.get("v.employmentEntity");
        
        var appId = cmp.get("v.recordId");
        var conId = empEntity.opportunity.Contact__r.Id;
        
        if(conId === null || conId === '') {
            this.showAlert('error', 'No contact found', '', 1000);
        }else {
            var action = cmp.get("c.verifyWorkEmail");
            
            action.setParams({
                conId : conId,
                appId : appId
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                cmp.set("v.showSpinner", false);
                if(state == "SUCCESS"){
                    var result = response.getReturnValue();                
                    if(!$A.util.isEmpty(result)) {
                        if(result.includes('Success')){
                            this.showAlert('success', result, '', 1000);
                        } else {
                            this.showAlert('warning', result, '', 1000);
                        }
                    }else {
                        this.showAlert('error', 'Failed to verify work email. ', '', 1000);
                    }
                    
                } else if(state == "ERROR"){
                    this.parseError(response);
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    formatPhoneNumber: function(phone) {
        var s2 = (""+phone).replace(/\D/g, '');
        var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
        return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
    }
})