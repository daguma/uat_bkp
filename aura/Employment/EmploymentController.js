({
    doInit : function(component, event, helper) {
        // Get a reference to the getEmploymentEntity() function defined in the Apex controller
        var action = component.get("c.getEmploymentEntity");
        action.setParams({
            "appId": component.get("v.recordId")
        });
        action.setBackground();
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.employmentEntity", response.getReturnValue());
                var empEntity = response.getReturnValue().empDetail;
                
                //Phone Number formatting
                var phone = empEntity.Employer_Phone_Number__c;
                if(!$A.util.isEmpty(phone)) {
                    phone = helper.formatPhoneNumber(phone);
                    empEntity.Employer_Phone_Number__c = phone;
                }
                //get the opportunity to show FS Assigned.
                component.set("v.oppEntity", response.getReturnValue().opportunity);
                
                component.set("v.empDetail", empEntity);
                component.set("v.warnings", response.getReturnValue().warningMsgs);

            }else {
                helper.showAlert('error', 'Failed to load the information.', 'Error!');
                console.log("Failed with state: " + state);
            }
            component.set("v.showSpinner", false);
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    onCheck : function(cmp, event, helper) {
        var isCheck = event.getSource().get("v.checked");
        if(!isCheck) {
            var thirdCheck = cmp.find("thirdJob");
            thirdCheck.set("v.checked",false);
        }
    },
    SaveEmpDetail : function(cmp, event, helper) { //Upsert employment details
        cmp.set("v.showSpinner", true);
        helper.SaveEmpDetail(cmp, helper);
    },
    CalcYear : function(cmp, event, helper) { //To calculate experience from hire date
    	var hireDateVal = event.getSource().get("v.value");
        var checboxID = event.getSource().getLocalId();
        var todayDate = new Date();
        if(hireDateVal > todayDate) {
            helper.setFieldError(cmp, checboxID);
        }else {        
            helper.CalcYearForJob(cmp, checboxID, hireDateVal);
        }
    },
    EmployerRender : function(cmp, event, helper) { //To populate Employer Name from Contact details
        var IsCheck = event.getSource().get("v.checked");
        var fieldId = event.getSource().getLocalId();           
        helper.EmployerRenderInfo(cmp, fieldId, IsCheck);
    },
    verifyingWorkEmail : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.verifyingWorkEmail(component);
    },
    changeFS : function (cmp){
        cmp.set("v.FSchanged", true);
    }
})