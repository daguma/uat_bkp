({
    fetchPickListVal: function(component, fieldName, elementId) {
        
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.offer"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.options", opts);
            }else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },
    deletingOffer: function(component) {
        var action = component.get("c.deleteOffer");
        
        action.setParams({
            "offer": component.get("v.offer")
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.getEvent("DeleteRowEvt").setParams({
                    "indexVar" : component.get("v.rowIndex")
                }).fire();
                this.showToastAlert("Success!", 'Offer deleted successfully', "success", 1000);
				component.getEvent("UROEntityEvent").setParams({
                    "uroEntityString" : JSON.stringify(response.getReturnValue())
                }).fire();                
            }else {
                this.parseError(response);
            }
        })     
        $A.enqueueAction(action);
    },
    deSelectingOffer: function(component) {
        var action = component.get("c.deselectOffer");
        
        action.setParams({
            "offer": component.get("v.offer")
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {               
                var result = response.getReturnValue();
                component.getEvent("UROEntityEvent").setParams({
                    "uroEntityString" : JSON.stringify(result)
                }).fire();
            }else {
                this.parseError(response);
            }
        })     
        $A.enqueueAction(action);
    },
    SelectingOffer: function(component) {
        var action = component.get("c.selectOffer");
        
        action.setParams({
            "offer": component.get("v.offer")
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {               
                var result = response.getReturnValue();
                component.getEvent("UROEntityEvent").setParams({
                    "uroEntityString" : JSON.stringify(result)
                }).fire();
            }else {
                this.parseError(response);
                component.set("v.showSpinner", false);
            }
        })     
        $A.enqueueAction(action);
    },
    calculatePayAmount: function(component) {
        var action = component.get("c.CalcPaymentAmt");
        
        action.setParams({
            "offer": component.get("v.offer")
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {     
                this.showToastAlert("","Repayment frequency updated.","success",100);
                component.getEvent("LoadOfferEvt").fire();
            }else if (response.getState() === "ERROR") {
                this.parseError(response);
            }
            else {
                console.log("Repayment frequency Incomplete");
            }
        })     
        $A.enqueueAction(action);
    },
    showToastAlert: function(title, message, type, time) {
        var toastEvent = $A.get("e.force:showToast");
        	toastEvent.setParams({
            	"title": title,
                "message": message,
                "type": type,
                "mode": "dismissible",
                "duration": time
            });
            toastEvent.fire();
    },
    parseError: function(response) {
        var errors = response.getError();
        if (errors) {
        	if (errors[0] && errors[0].message) {
            	this.showToastAlert("Error!", errors[0].message, "error", 2000); 
            }
        } else {
       		this.showToastAlert("", "Unkown Error", "error", 2000); 
        }
    },

    preferredPaymentField : function (component) {

        var isOfferSelected = component.get("v.offer.IsSelected__c");

        var selectedFrequency = component.find("repayFreq").get("v.value");
        var rowIndex = component.get("v.rowIndex");

        $("#datepickerId-" + rowIndex).each( function () {
            
            var currentObj = $(this);
            
            currentObj.datepicker("destroy");

            if (selectedFrequency.trim() == '28 Days') {

                $(this).datepicker({
                    minDate: '+28d',
                    maxDate: '+43d',
                    dateformat: 'MM/DD/YYYY',
                });
                
            } else if (selectedFrequency.trim() == 'Monthly') {

                $(this).datepicker({
                    minDate: '+30d',
                    maxDate: '+45d',
                    dateformat: 'MM/DD/YYYY',
                });

            } else if (selectedFrequency.trim() == 'Semi-Monthly') {

                $(this).datepicker({
                    minDate: '+15d',
                    maxDate: '+30d',
                    dateformat: 'MM/DD/YYYY',
                });
                
            } else {

                $(this).datepicker({
                    minDate: '+14d',
                    maxDate: '+30d',
                    dateformat: 'MM/DD/YYYY',
                });
            }
        });
        
        $("#datepickerId-" + rowIndex).show();
    },
    
    formatDate: function(date, output) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        
        if(output === "SOQL") return [year, month, day].join('-');
        if(output === "LNG") return [month, day, year].join('/');
	},
    
    checkValues: function(cmp) {
        var action = cmp.get("c.checkValuesOfferSelection");

        action.setParams({
            "offer": cmp.get("v.offer"),
            "state": cmp.get("v.opportunity.State__c"),
            "product": cmp.get("v.opportunity.Lending_Product__r.Name"),
            "isFinwise": cmp.get("v.opportunity.Is_Finwise__c"),
            "isFEB": cmp.get("v.opportunity.Is_FEB__c")
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                if(!response.getReturnValue()){
                    this.showToastAlert('Error!', 'The rate on the offer selected is not permitted for this state. Please check the offer and correct it.', 'error', 5000);
                    
                }
                else{
                    //alert('No Entro');
                    var validDate = true;  
                    var DateStrVal = '';
                    var amountExceeded = false; 
                    var canContinue = false; 
                    var message='';

                    if((cmp.get("v.opportunity.Campaign_Id__c") != null && cmp.get("v.opportunity.Campaign_Id__c") != undefined && !cmp.get("v.opportunity.Campaign_Id__c").includes('RiskRefinanceOpp')) || cmp.get("v.opportunity.Campaign_Id__c") === undefined){
                        if(cmp.get("v.offer.Payment_Amount__c") > cmp.get("v.opportunity.Maximum_Payment_Amount__c")){
                            amountExceeded=true;
                            message='The selected Payment amount is higher than the max permitted payment amount.';   
                        }
                        if(cmp.get("v.offer.Loan_Amount__c") > cmp.get("v.opportunity.Maximum_Loan_Amount__c")){
                            amountExceeded=true;
                            message='The selected Loan amount is higher than the max permitted Loan amount.';
                        }
                        if(amountExceeded)
                            canContinue = confirm(message + "\nAre you sure?");   
                    } 
                    
                    if(canContinue || (!amountExceeded)){
                        alert("Please remember to confirm we have the correct Income Type option and employment start date details (for Employed applicants) before selecting an offer");

                        if( $("#datepickerId-" + cmp.get("v.rowIndex")).val() != null || $("#datepickerId-" + cmp.get("v.rowIndex")).val() != "") {
                            
                            var preferredPaymentDate = new Date($("#datepickerId-" + cmp.get("v.rowIndex")).val());
                            
                            var soqlDateFormat = this.formatDate(preferredPaymentDate,"SOQL");
                            
                            cmp.set("v.preferredDate", soqlDateFormat);
                            
                            cmp.set("v.offer.Preferred_Payment_Date_String__c", soqlDateFormat); 
                                                                         
                            DateStrVal = $("#datepickerId-" + cmp.get("v.rowIndex")).val();
                            var preferredPaymentDate = new Date(DateStrVal);
                            
                            if(preferredPaymentDate != 'Invalid Date') {
                                var soqlDateFormat = this.formatDate(preferredPaymentDate,"SOQL");
                               
                                cmp.set("v.preferredDate", soqlDateFormat);
                                
                                cmp.set("v.offer.Preferred_Payment_Date_String__c", soqlDateFormat);
                            }else {
                                validDate = false;
                                this.showToastAlert('Error!', 'Invalid Date ' + DateStrVal, 'error', 2000);
                            }
                        }
                        
                        if(validDate) {
                            cmp.set("v.showSpinner", true);
                            cmp.getEvent("selectOfferEvt").setParams({
                                "offerEntity" : cmp.get("v.offer")
                            }).fire();
                        }
                    }                    
                }
            }
            else {
                alert('The function helper.checkValues fails.');
            }
        })

        $A.enqueueAction(action);
    }
})