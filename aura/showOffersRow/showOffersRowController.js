({
    init: function (cmp, event, helper) {
        var offr = cmp.get("v.offer");
        if(!offr.IsSelected__c) {
    		helper.fetchPickListVal(cmp, 'Repayment_Frequency__c', 'repayFreq');
        }
    },
    deletingOffer: function (cmp, event, helper) {
    	cmp.set("v.showSpinner", true);
        cmp.getEvent("deleteOfferEvt").setParams({
        	"offerEntity" : cmp.get("v.offer")
        }).fire();        
    },
    deSelectingOffer: function (cmp, event, helper) {
        cmp.set("v.showSpinner", true);
        cmp.getEvent("deselectOfferEvt").setParams({
        	"offerEntity" : cmp.get("v.offer")
        }).fire();        
    },
	
	//PAR-10
	openModel: function(component, event, helper) {		
		var ofr = component.get("v.offer");
        if(ofr.Offer_Code__c != null && ofr.Offer_Code__c != undefined) {
			component.set("v.isOpen", true);
        }
		else{
			var a = component.get('c.SelectingOffer');
			$A.enqueueAction(a);
		}	  
   },
   
   closeModel: function(component, event, helper) {  
      component.set("v.isOpen", false);
   },
    
	SelectingOffer: function (cmp, event, helper) {
        helper.checkValues(cmp);
    },
    
    calculatePayAmount: function (cmp, event, helper) {
        helper.calculatePayAmount(cmp);
   
    },
    
    preferredPaymentSchedule: function(cmp,event,helper) {
        cmp.set("v.showField",false);
        helper.preferredPaymentField(cmp); 
    }

});