({
	init : function(component, event, helper) {
        helper.applyCSS(component);
        var recordId = component.get("v.recordId");
        if(recordId !== null)
			helper.getCreditReports(component, recordId);        
	},
    closeModal : function(component, event, helper) {
        helper.revertCssChange(component);
        document.getElementById("allCreditReportsContainer").style.display = "none";
    }
})