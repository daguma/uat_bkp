({
	processMultiloanResponse : function(component,response) {
        if (!$A.util.isUndefinedOrNull(response) && !$A.util.isEmpty(response)) {
            if(response.hasError){
               
                console.log(response.errorMessage);
            }else{
                if (!$A.util.isUndefinedOrNull(response.isMultiloan) && !$A.util.isEmpty(response.isMultiloan)) {
                    component.set("v.isMultiloan",response.isMultiloan);
                  
                }
            }
        }else{
            component.set("v.isMultiloan",false);
        }
		
	}
})