//Button_HardCreditPullController
({
    openModel: function(component, event, helper) {
        component.set("v.isButtonOpened", true);
    },
    closeModel: function(component, event, helper) {
        component.set("v.isButtonOpened", false);
    }
})