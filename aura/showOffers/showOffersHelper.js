({
    getOffers : function(cmp) {
        var action = cmp.get("c.loadOffers");
        action.setParams({
            "appId": cmp.get("v.recordId")
        });
        action.setBackground();

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            console.log(response);
            var result = response.getReturnValue();
            if (state === "SUCCESS") {
                if(!$A.util.isUndefinedOrNull(result)) {
                    if(result.length == 0) {
                        console.log('No Offers found');
                        this.showToastAlert("Warning", "No offers found.", "warning", 2000);
                    }else {
                        console.log('Offers Set');
                        cmp.set("v.offers", result.offers);
                        cmp.set("v.sortAsc", true);
                        this.sortBy(cmp, "APR_percent__c");
                    }
                }else {
                    this.showToastAlert("Error", "Something went wrong while loading offers.", "error", 2000);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.log('Error ' + errors);
            }
            cmp.set("v.showSpinner", false);
        }));
        $A.enqueueAction(action);
    },
     insertOffer : function(component) {
        component.set("v.showSpinner", true);
        
        var reLoadOffers = false;        
        var action = component.get("c.InsertCustOffer");
        
        // Set Param of Server side function "InsertCustOffer"
        action.setParams({
            "CustomOffer" : component.get("v.CustomOffer"),
            "appId": component.get("v.recordId")
        });        
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                var result = response.getReturnValue();
                
                if(result.includes('=>') || result.includes('SUCCESS')) {
                    var msgs = result.split("=>");
                    for(var l = 0; l < msgs.length; l++) {
                        var message = msgs[l];
                        
                        if(message.includes('SUCCESS')) {
                    		message = message.replace("SUCCESS:", "").trim();
                    		component.set("v.CustomOffer", { 'sobjectType': 'Offer__c' }); 
                    		this.showToastAlert("Success", message, "success", 10000);
                            reLoadOffers = true;
                            $A.get('e.force:refreshView').fire();

                        }
                        if(message.includes('INFO')) {
                            message = message.replace("INFO:", "").trim();
                            this.showToastAlert("Info", message, "info", 10000);
                        }
                        if(message.includes('WARNING')) {
                            message = message.replace("WARNING:", "").trim();
                            this.showToastAlert("Warning", message, "warning", 10000);
                        }
                    }
                }
                
                if(result.includes('ERROR')) {
                    result = result.replace("ERROR:", "").trim();
                    this.showToastAlert("Error", result, "error", 10000);
                }   
                    
                if(reLoadOffers)
                    this.getOffers(component);
            }else {
                console.log('OH, I AM NOT IN');
            }
            component.set("v.showSpinner", false);
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    validateOffer: function(component) {
        var isValid = true;
        var fields = ["co_loanAmt", "co_term", "co_apr", "co_fee"];
        var fieldLabel = ["Disbursal Amount", "Term", "APR", "Fee"];
        var leadsource = component.get("v.leadsource"),
            leadsubsource = component.get("v.leadsubsource");         
        for(var i = 0; i < fields.length; i++) {
            var fieldCmp = component.find(fields[i]);           
            if ((leadsource == "EZVerify" || leadsubsource == "LoanHero") && fields[i] == "co_fee"){               
                continue;
            }
            else {
                if(!fieldCmp.get("v.value") && fields[i] !== 'co_fee') {
                    this.showToastAlert("Error", fieldLabel[i] + " field is required", "error", 2000);
                    return false;
                }else{
                    if(fields[i] == 'co_fee' && fieldCmp.get("v.value") < 0){
                        this.showToastAlert("Error", fieldLabel[i] + " cannot be lower than $0.00", "error", 2000);
                        return false;
                    }
                }
            }
        }
        
        return true;
    },
    showToastAlert: function(title, message, type, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type,
            "mode": "dismissible",
            "duration": time
        });
        toastEvent.fire();
    },
    sortBy: function(component, field) {
        //debugger;
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.offers");
        
        sortAsc = (field === sortField) ? !sortAsc : true;
        
        if(sortAsc)
        	records = this.bubbleSortAsc(records, field);
        else
            records = this.bubbleSortDesc(records, field);
        
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.offers", records);
    },
    bubbleSortAsc : function(items, field) {
        var length = items.length;
        
        for (var i = (length - 1); i >= 0; i--) {
            //Number of passes
            for (var j = (length - i - 1); j > 0; j--) { 
                var prevVal = items[j][field];
                var nextVal = items[j - 1][field];
                
                //Handle Date Fields
                if(field === 'FirstPaymentDate__c' || field === 'CreatedDate' || field === 'Selected_Date__c') {
                    prevVal = prevVal ? new Date(prevVal) : prevVal;
                    nextVal = nextVal ? new Date(nextVal) : nextVal;
                }
                
                //Compare the adjacent positions
                if(prevVal && nextVal) {
                    if (prevVal < nextVal) {
                        //Swap the numbers
                        var tmp = items[j];
                        items[j] = items[j - 1];
                        items[j - 1] = tmp;
                    }
                }else if(prevVal == undefined) {
                    //Swap the numbers
                    var tmp = items[j];
                    items[j] = items[j - 1];
                    items[j - 1] = tmp;
                }
            };
        }
        
        return items;
    },
    bubbleSortDesc : function(items, field) {
        var length = items.length;
        
        for (var i = (length - 1); i >= 0; i--) {
            //Number of passes
            for (var j = (length - i - 1); j > 0; j--) { 
                var prevVal = items[j][field];
                var nextVal = items[j - 1][field];
                
                //Handle Date Fields
                if(field === 'FirstPaymentDate__c' || field === 'CreatedDate' || field === 'Selected_Date__c') {
                    prevVal = prevVal ? new Date(prevVal) : prevVal;
                    nextVal = nextVal ? new Date(nextVal) : nextVal;
                }
                
                //Compare the adjacent positions
                if(prevVal && nextVal) {
                    if (prevVal > nextVal) {
                        //Swap the numbers
                        var tmp = items[j];
                        items[j] = items[j - 1];
                        items[j - 1] = tmp;
                    }
                }else if(nextVal == undefined) {
                    //Swap the numbers
                    var tmp = items[j];
                    items[j] = items[j - 1];
                    items[j - 1] = tmp;
                }
            };
        }
        
        return items;
    }
})