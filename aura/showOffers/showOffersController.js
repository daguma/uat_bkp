({
    doinIt: function (cmp, event, helper) {
        cmp.set("v.showSpinner", true);
        helper.getOffers(cmp);
    },
    insertOffer : function(cmp, event, helper) {
        if(helper.validateOffer(cmp))
            helper.insertOffer(cmp)
            },   
    removeDeletedRow: function(cmp, event, helper) { 
        var index = event.getParam("indexVar"); 
        var offersList = cmp.get("v.offers");
        
        offersList.splice(index, 1);
        cmp.set("v.offers", offersList);
    },
    reloadOffers: function(cmp, event, helper) {
        console.log('Offers Reloading....');
        cmp.set("v.showSpinner", true);
        helper.getOffers(cmp);           
    },
    sortByIR: function(component, event, helper) {
        helper.sortBy(component, "APR_percent__c");
    },
    sortByTerm: function(component, event, helper) {
        helper.sortBy(component, "Term__c");
    },
    sortByRF: function(component, event, helper) {
        helper.sortBy(component, "Repayment_Frequency__c");
    },
    sortByInstallment: function(component, event, helper) {
        helper.sortBy(component, "Installments__c");
    },
    sortByPA: function(component, event, helper) {
        helper.sortBy(component, "Payment_Amount__c");
    },
    sortByDA: function(component, event, helper) {
        helper.sortBy(component, "Loan_Amount__c");
    },
    sortByFees: function(component, event, helper) {
        helper.sortBy(component, "Fee__c");
    },
    sortByTLA: function(component, event, helper) {
        helper.sortBy(component, "Total_Loan_Amount__c");
    },
    sortByPartner: function(component, event, helper) {
        helper.sortBy(component, "Lead_Sub_Source__c");
    },
    sortByCDA: function(component, event, helper) {
        helper.sortBy(component, "Customer_Disbursal__c");
    },
    sortByEAPR: function(component, event, helper) {
        helper.sortBy(component, "Effective_APR__c");
    },
    sortBycDate: function(component, event, helper) {
        helper.sortBy(component, "CreatedDate");
    },
    sortByFPD: function(component, event, helper) {
        helper.sortBy(component, "FirstPaymentDate__c");
    },
    sortByPPD: function(component, event, helper) {
        helper.sortBy(component, "Preferred_Payment_Date_String__c");
    },
    sortByIC: function(component, event, helper) {
        helper.sortBy(component, "IsCustom__c");
    },
    sortByIS: function(component, event, helper) {
        helper.sortBy(component, "IsSelected__c");
    },
    sortBySD: function(component, event, helper) {
        helper.sortBy(component, "Selected_Date__c");
    },
    sortByScope: function(component, event, helper) {
        helper.sortBy(component, "Scope__c");
    }
})