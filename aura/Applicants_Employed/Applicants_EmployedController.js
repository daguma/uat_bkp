//Applicants_EmployedController
({
    onTitleButtonClick: function (component, event, helper) {
        var cmp1 = component.find("parentDiv");
        $A.util.toggleClass(cmp1, "slds-is-open");
    },

    on2ndJobBtnClick : function (component, event, helper) {
        var cmp2 = component.find("parentDiv2");
        $A.util.toggleClass(cmp2, "slds-is-open");
    },

    callQuickSaveApp: function (component, event, helper) {
        var compEvent = component.getEvent("ApplicationDetails_SaveEvt");
        compEvent.fire();
    },

    saveDetails: function (component, event, helper) {
        var result = new Object();
        var applicantsEmployedTable1 = component.find("applicantsEmployedTable1");
        var appEmployedTableList = applicantsEmployedTable1.saveDetails();
        result.one = appEmployedTableList;

        var applicantsEmployedTable2 = component.find("applicantsEmployedTable2");
        if ( ! $A.util.isUndefined(applicantsEmployedTable2) ) {
            var appEmployedTableList2 = applicantsEmployedTable2.saveDetails();
            result.two = appEmployedTableList2;
        }

        return result;
    }
})