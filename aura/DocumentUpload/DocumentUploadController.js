//DocumentUploadController
({
    doSave: function(component, event, helper) {
        if (component.find("fileId").get("v.files").length > 0) {
            helper.uploadHelper(component, event);
            component.set("v.fileName","No File Selected...");
        } else {
            alert('Please Select a Valid File');
        }
    },

    doCancel : function (component, event, helper) {

        // Get the component event by using the
        // name value from aura:registerEvent
        var cmpEvent = component.getEvent("cancelFileAttach");
        cmpEvent.fire();

    },

    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },
})