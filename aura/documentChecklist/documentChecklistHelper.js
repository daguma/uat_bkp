({
    getDocEntity: function (cmp) {
        var action = cmp.get("c.getDocumentsEntity");
        var opportunityId = cmp.get("v.oppId");
        action.setParams({
            oppId: opportunityId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result.hasError) {
                    this.showAlert("error", result.errorMsg, "Failed!");
                } else {
                    cmp.set("v.documentsEntity", result);
                }
            } else if (state == "ERROR") {
                this.showAlert("error", "Error at DocumentsHelper.getDocsEnt()", "Failed!");
            }
        });
        $A.enqueueAction(action);
    },
    
    saveAllAppFields: function (component) {
        
        var oppToSave = new Object();
        oppToSave.opportunity = component.get("v.documentsEntity.newDocumentCtrl.application");
        /* 
        //Contract Generation component
        var conGenerationCmp = component.find("contractGeneration");
        var conGenerationData = conGenerationCmp.saveDetails();
        if (typeof conGenerationData !== 'string' && !(conGenerationData instanceof String)) 
			oppToSave.opportunity.Contract_Document__c = conGenerationData.Contract_Document__c;
            oppToSave.opportunity.Contract_Received_Through__c = conGenerationData.Contract_Received_Through__c;
            oppToSave.opportunity.Credit_Form__c = conGenerationData.Credit_Form__c;
            oppToSave.opportunity.Contract_Signed_Date__c = conGenerationData.Contract_Signed_Date__c;
		*/
        //All applicants component
        var allAppsCmp = component.find("allApplicants");
        var allAppsList = allAppsCmp.saveDetails();
        oppToSave.allApplicantsList = JSON.stringify(allAppsList);
        
        //Employed applicants component
        var resultEmpAppList = component.find("employedApplicants").saveDetails();
        oppToSave.employedApplicants1List = JSON.stringify(resultEmpAppList.one);
        var resultEmpAppTwo = resultEmpAppList.two;
        if (!$A.util.isUndefined(resultEmpAppTwo)) {
            oppToSave.employedApplicants2List = JSON.stringify(resultEmpAppList.two);
        } else {
            oppToSave.employedApplicants2List = null;
        }
        
        //Retired applicants component
        var retiredAppsCmp = component.find("retiredApplicants");
        var retiredAppsList = retiredAppsCmp.saveDetails();
        oppToSave.retiredApplicantsList = JSON.stringify(retiredAppsList);
        
        //Self-Employed applicants component
        var resultSelfEmpAppList = component.find("selfEmployedApplicants").saveDetails();
        oppToSave.selfEmployedApplicants1List = JSON.stringify(resultSelfEmpAppList.one);
        var resultSelfEmpAppTwo = resultSelfEmpAppList.two;
        if (!$A.util.isUndefined(resultSelfEmpAppTwo)) {
            oppToSave.selfEmployedApplicants2List = JSON.stringify(resultSelfEmpAppList.two);
        } else {
            oppToSave.selfEmployedApplicants2List = null;
        }
        oppToSave.opportunity.Is_No_POI_Required__c = resultSelfEmpAppList.Is_No_POI_Required__c === true ? true : false;
        
        //Miscellaneous applicants component
        var miscAppsCmp = component.find("miscApplicants");
        var miscAppsList = miscAppsCmp.saveDetails();
        oppToSave.miscellaneousApplicantsList = JSON.stringify(miscAppsList);
        /*
            //Add Documents component info to be saved (Opportunity details)
            var dea = component.find("deAtD").get("v.value");
            if ($A.util.isUndefinedOrNull(dea) || $A.util.isEmpty(dea)) {
                //do nothing
            }
            else if (dea === "--None--") {
                oppToSave.opportunity.DE_Assigned_To_Doc__c = "";
            } else {
                oppToSave.opportunity.DE_Assigned_To_Doc__c = dea;
            }

            var fsAtD = component.find("fsAtD").get("v.value");
            if ($A.util.isUndefinedOrNull(fsAtD) || $A.util.isEmpty(fsAtD)) {
                //do nothing
            }
            else if (fsAtD === "--None--") {
                oppToSave.opportunity.FS_Assigned_To_Doc__c = "";
            } else {
                oppToSave.opportunity.FS_Assigned_To_Doc__c = fsAtD;
            }

            var govIdStateValue = component.find("govIdState").get("v.value");
            if ($A.util.isUndefinedOrNull(govIdStateValue) || $A.util.isEmpty(govIdStateValue)) {
                //do nothing
            }
            else if (govIdStateValue === "--None--") {
                oppToSave.opportunity.Borrower_ID_State__c = "";
            } else {
                oppToSave.opportunity.Borrower_ID_State__c = govIdStateValue;
            }

            var govIdNum = component.find("govId").get("v.value");
            oppToSave.opportunity.Borrower_ID_Number__c = $A.util.isUndefinedOrNull(govIdNum) ? "" : govIdNum;

            //Application Details component
            var appDetailsCmp = component.find("applicationDetails");
            var appDetailsData = appDetailsCmp.saveDetails();
            oppToSave.opportunity.Payment_Method__c = appDetailsData.Payment_Method__c;
            oppToSave.opportunity.Income_Denominator__c = appDetailsData.Income_Denominator__c;
            oppToSave.opportunity.ACH_Bank_Name__c = appDetailsData.ACH_Bank_Name__c;
            oppToSave.opportunity.Bank_Info_Source__c = appDetailsData.Bank_Info_Source__c;
*/
        var action = component.get("c.quickSaveApp");
        action.setParams({
            pOpportunity: oppToSave.opportunity,
            pAllApplicantsList: oppToSave.allApplicantsList,
            pEmployedApplicants1List: oppToSave.employedApplicants1List,
            pEmployedApplicants2List: oppToSave.employedApplicants2List,
            pRetiredApplicantsList: oppToSave.retiredApplicantsList,
            pSelfEmployedApplicants1List: oppToSave.selfEmployedApplicants1List,
            pSelfEmployedApplicants2List: oppToSave.selfEmployedApplicants2List,
            pMiscellaneousApplicantsList: oppToSave.miscellaneousApplicantsList
            
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result.qsHasError) {
                    this.showAlert("error", result.errorMsg, "Failed!");
                } else {
                    if (result.qsHasWarning) {
                        for (var i = 0; i < result.qsWarningMessage.length; i++) {
                            this.showAlert("warning", result.qsWarningMessage[i], "warning!", "sticky");
                        }
                    }
                    if (result.isSuccess) {
                        this.showAlert("success", result.successMessage, "success!");
                    }
                }
            } else if (state == "ERROR") {
                this.showAlert("error", "Error at DocumentsHelper.saveAllAppFields()", "Failed!");
            }
        });
        $A.enqueueAction(action);
    },
    showAlert: function (alertType, alertMsg, alertTitle, mode, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": mode,
            "duration": time
        });
        toastEvent.fire();
    },
    applyCSS: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:0} .forceStyle.desktop .viewport{overflow:hidden}</style>");
    },
    revertCssChange: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:5} .forceStyle.desktop .viewport{overflow:visible}</style>");
    }
})