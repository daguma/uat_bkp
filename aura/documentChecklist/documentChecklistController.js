({
	 doInit : function (component, event, helper) {
        helper.applyCSS(component);
        helper.getDocEntity(component);
     },
    attachFile : function (component, event, helper) {
        component.set("v.showAttachFileBtn",false);
        component.find("dummyDocUploadSection").getElement().scrollIntoView(true);
    },
    attachMultiple : function (component, event) {
        var recordId = component.get("v.oppId");
        var url = '/apex/EasyUploadAttachment?parentId=' + recordId;
        window.open(url, '_self');
    },
    cancelFileAttachAction : function (component, event, helper) {
        component.set("v.showAttachFileBtn", true);
    },
    saveOpp : function (component, event, helper) {
        helper.saveAllAppFields(component);
    },
    closeModal : function(component, event, helper) {
        component.set("v.showPopUp", false);
        helper.revertCssChange(component);
    },
})