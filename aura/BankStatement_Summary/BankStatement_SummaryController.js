//BankStatement_SummaryController.js
({
    closeModel : function(component, event, helper) {
        component.set('v.showTransactions', "false");
    },

    onKeyPressedEsc : function(component, event, helper) {
        if (event.keyCode == 27) { // escape key maps to keycode `27`
            component.set('v.showTransactions', "false");
        }
    },

    onTxnDescriptionClick : function (component, event, helper) {
        component.set('v.txnTypeCodeName', event.getSource().get("v.label"));
        component.set('v.txnTypeCode', event.getSource().get("v.title"));
        component.set('v.showTransactions', "true");
    },

    saveOpp: function (component, event, helper) {
        var achInfoValidity = helper.validateSummary(component);
        if(achInfoValidity.isAllValid){
            var opportunity = new Object();
            opportunity.Id = component.get("v.opportunityId");
            opportunity.DL_Available_Balance__c = component.find("availableBalance").get("v.value");
            opportunity.DL_As_of_Date__c = component.find("asOfDate").get("v.value");
            opportunity.DL_Current_Balance__c = component.find("currBalance").get("v.value");
            opportunity.DL_Average_Balance__c = component.find("avgBal").get("v.value");
            opportunity.DL_Deposits_Credits__c = component.find("depCredits").get("v.value");
            opportunity.DL_Avg_Bal_Latest_Month__c = component.find("latestAvgBal").get("v.value");
            opportunity.DL_Withdrawals_Debits__c = component.find("withDebits").get("v.value");
            opportunity.Number_of_Negative_Days_Number__c = component.find("negDays").get("v.value");
            if($A.util.isEmpty(opportunity.Number_of_Negative_Days_Number__c)){
                opportunity.Number_of_Negative_Days_Number__c = 0.00;
            }

            //Fields needed at Validation Tab stage
            opportunity.average_Daily_Balance_Amount__c = opportunity.DL_Average_Balance__c;
            opportunity.current_Balance_Amount__c = opportunity.DL_Current_Balance__c;
            opportunity.total_Deposits_Amount__c = opportunity.DL_Deposits_Credits__c;

            helper.saveOppFields(component, opportunity);
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": achInfoValidity.errorMessage,
                "mode": "sticky",
                "type": "error"
            });
            toastEvent.fire();
        }
    },

    limitTwoDecimals : function (component,event) {
        var comp = event.getSource();
        var num = comp.get("v.value")
        if (!$A.util.isUndefined(num) && !$A.util.isEmpty(num)) {
            var sign = num >= 0 ? 1 : -1;
            num = (Math.round((num*Math.pow(10,2)) + (sign*0.001)) / Math.pow(10,2)).toFixed(2);
            comp.set("v.value",num);
        }
    }
})