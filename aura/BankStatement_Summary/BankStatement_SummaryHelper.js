//BankStatement_SummaryHelper.js
({
    validateSummary : function (component) {
        var summaryValidity = new Object();
        summaryValidity.isAllValid = true;
        summaryValidity.errorMessage = "";
        return summaryValidity;
    },

    saveOppFields : function (component, opportunity) {
        var opp = component.get("c.saveOpportunity");
        opp.setParams({
            "opportunity": opportunity
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Successfully saved opportunity summary fields.")
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "BankStatement_SummaryHelper was not able to save the Opportunity. " + response.getReturnValue(),
                    "mode": "sticky",
                    "type": "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(opp);

    }
})