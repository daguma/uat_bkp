({
    enableNotifications : function(component, event, helper) {
        event.getSource().set("v.disabled", true);

        if (!confirm("Are you sure you want to continue enabling the SMS Notifications?"))
        	return;

        var action = component.get("c.startSmsNotificationsForCampaign");
        action.setParams({campaignId : component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
                return;
            }

            alert(`${state}- There was a problem processing your request: ${response.getReturnValue()}`);
            event.getSource().set("v.disabled", true);
        });
        
        $A.enqueueAction(action);

    },
	stopNotifications : function(component, event, helper) {
        event.getSource().set("v.disabled", true);

        if (!confirm("Are you sure you want to stop sending the SMS Notifications?"))
        	return;

        var action = component.get("c.stopSmsNotificationsForCampaign");
        action.setParams({campaignId : component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
                return;
            }

            alert(`${state}- There was a problem processing your request: ${response.getReturnValue()}`);
            event.getSource().set("v.disabled", true);
        });
        
        $A.enqueueAction(action);
    },
	 sendSmsCreditCard : function(component, event, helper) {
        event.getSource().set("v.disabled", true);

        if (!confirm("Are you sure you want to send the SMS Notifications?"))
        	return;
        
        var action = component.get("c.sendSmsNotificationsForCampaign");
        action.setParams({ campaignId : component.get("v.recordId") });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                alert("Your request has been completed.");
                $A.get('e.force:refreshView').fire();
                return;
            }
            else if (state === "INCOMPLETE") {}
            else if (state === "ERROR") {};

            alert(`${state}- There was a problem processing your request: ${response.getReturnValue()}`);
            event.getSource().set("v.disabled", false);
        });
        
        $A.enqueueAction(action);
    }
})