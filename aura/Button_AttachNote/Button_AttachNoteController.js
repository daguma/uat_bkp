//Button_AttachNoteController
({
    openModel: function(component, event, helper) {
        helper.openTheModel(component);
    },
    closeModel: function(component, event, helper) {
        helper.closeTheModel(component);
    },
    saveNote : function (component, event, helper) {
        helper.saveTheNote(component);
    }
})