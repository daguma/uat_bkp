//Button_AttachNoteHelper
({
    openTheModel : function (component) {
        component.set("v.isButtonOpened", true);
    },
    closeTheModel : function (component) {
        component.set("v.isButtonOpened", false);
    },
    saveTheNote : function (component) {
        var action = component.get("c.save");
        action.setParams({
            "pParentId": component.get("v.recordId"),
            "pTitle": component.find("pTitle").get("v.value"),
            "pBody": component.find("pBody").get("v.value")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The note was saved.",
                    "type": "success",
                    "mode": "dismissible"
                });
                resultsToast.fire();
            } else if (state === "ERROR") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Error",
                    "message": "The note was not saved.",
                    "type": "error",
                    "mode": "dismissible"
                });
                resultsToast.fire();
            }
        }));
        $A.enqueueAction(action);

        //close the model by calling the cancel function
        this.closeTheModel(component);
    }
})