//Button_IncomeValidationHelper.js
({
    valIncome : function (component, recordId) {
        var opp = component.get("c.validateIncome");
        opp.setParams({
            "oppId": recordId
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.hasError){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": result.errorMessage,
                        "type": "error"
                    });
                    toastEvent.fire();
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": result.successMessage,
                        "type": "success"
                    });
                    toastEvent.fire();

                    //Update BankStatement_IncomeVal fields
                    var parametersEvent = $A.get("e.c:Event_App_IncmValUpdate");
                    parametersEvent.fire();
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(opp);
    }
})