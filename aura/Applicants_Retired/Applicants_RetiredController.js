//Applicants_RetiredController
({
    onTitleButtonClick: function (component, event, helper) {
        var cmp1 = component.find("parentDiv");
        $A.util.toggleClass(cmp1, "slds-is-open");
    },

    callQuickSaveApp: function (component, event, helper) {
        var compEvent = component.getEvent("ApplicationDetails_SaveEvt");
        compEvent.fire();
    },

    saveDetails: function (component, event, helper) {
        var retiredAppList = component.get("v.retiredApplicantsList");

        var arrayLength = retiredAppList.length;
        var fullList = [];
        for (var i = 0; i < arrayLength; i++) {
            var item = retiredAppList[i];

            item.form1099Income = $A.util.isUndefined(item.form1099IncomeVal) ? "" : "" + item.form1099IncomeVal;
            item.benefitLetterIncome = $A.util.isUndefined(item.benefitLetterIncomeVal) ? "" : "" + item.benefitLetterIncomeVal;
            //MAINT-351
            item.received = item.isReceived === true ? "true" : "false";
            item.approved = item.isApproved === true ? "true" : "false";
            item.required = item.isRequired === true ? "true" : "false";

            if ($A.util.isUndefined(item.form1099ReceivedDateVal)) {
                item.form1099ReceivedDate = "";
            } else {
                if (item.form1099ReceivedDateVal.length === 10) {
                    var temp = item.form1099ReceivedDateVal.replace(/-/g, "/");
                    var dd = temp.substr(temp.lastIndexOf("/"), temp.length);
                    var mm = temp.substr(temp.indexOf("/") + 1, temp.lastIndexOf("/"));
                    var mm = mm.substr(0, mm.indexOf("/"));
                    var yyyy = "/" + temp.substr(0, temp.indexOf("/"));
                    item.form1099ReceivedDate = mm + dd + yyyy;
                } else {
                    delete item["form1099ReceivedDate"];
                    delete item["form1099ReceivedDateVal"];
                }

            }

            if ($A.util.isUndefined(item.benefitLetterReceivedDateVal)) {
                item.benefitLetterReceivedDate = "";
            } else {
                if (item.benefitLetterReceivedDateVal.length === 10) {
                    var temp = item.benefitLetterReceivedDateVal.replace(/-/g, "/");
                    var dd = temp.substr(temp.lastIndexOf("/"), temp.length);
                    var mm = temp.substr(temp.indexOf("/") + 1, temp.lastIndexOf("/"));
                    var mm = mm.substr(0, mm.indexOf("/"));
                    var yyyy = "/" + temp.substr(0, temp.indexOf("/"));
                    item.benefitLetterReceivedDate = mm + dd + yyyy;
                } else {
                    delete item["benefitLetterReceivedDate"];
                    delete item["benefitLetterReceivedDateVal"];
                }
            }

            item.dispositions = item.dispositions === "--None--" ? "" : item.dispositions;

            fullList.push(item);
        }
        return fullList;
    },

    onCompletedByClick : function (component, event) {
        var userName = event.getSource().get("v.label");
        var index = event.getSource().get("v.title");
        var cmp = component.find("completedByInput");
        cmp[index].set("v.value",userName);
    },

    onCompletedBy2Click : function (component, event) {
        var userName = event.getSource().get("v.label");
        var index = event.getSource().get("v.title");
        var cmp = component.find("completedBy2Input");
        cmp[index].set("v.value",userName);
    }

})