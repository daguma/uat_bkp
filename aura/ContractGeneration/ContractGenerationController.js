//ContractGenerationController
({
    loadPicklistValues: function (component, event, helper) {
        helper.loadSelectValues(component);
        helper.loadPicklists(component);
    },

    onItemSelected: function (component, event, helper) {
        helper.updateAsItemSelected(component, event);
    },

    saveDetails: function (component, event, helper) {
        var returnedMessage = new Object();
        if (component.get("v.isContractReturnedDateRequired") === true) {
            var signedDate = component.find("contractRetDate").get("v.value");
            if ($A.util.isUndefinedOrNull(signedDate) || $A.util.isEmpty(signedDate)) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Contract Returned Date needed",
                    "message": "Contract Returned Date is missing.",
                    "type": "error",
                    "mode": "sticky"
                });
                toastEvent.fire();
                returnedMessage = "Contract Returned Date is missing";
            } else {
                returnedMessage = helper.saveAllAppFields(component);
            }
        } else {
            returnedMessage = helper.saveAllAppFields(component);
        }
        return returnedMessage;
    },

    onContractReturnedDateChange: function (component) {
        var conRecThrough = component.get("v.contractReceivedValue");
        component.set("v.isContractReturnedDateRequired", (conRecThrough === "DocuSign" || conRecThrough === "Fax or Scan"));
    }
})