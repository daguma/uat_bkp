//ContractGenerationHelper
({

    loadSelectValues: function (component) {
        var opportunityRec = component.get("v.contractGenerationEntity");
        component.set("v.contractDocumentValue", opportunityRec.Contract_Document__c);
        var doc = opportunityRec.Contract_Received_Through__c;
        component.set("v.contractReceivedValue", doc);
        component.set("v.isContractReturnedDateRequired", (doc === "DocuSign" || doc === "Fax or Scan"));
        component.set("v.creditFormValue", opportunityRec.Credit_Form__c);
    },

    loadPicklists: function (component) {
        var action = component.get("c.getPicklistValues");
        var opportunityRec = component.get("v.contractGenerationEntity");
        action.setParams({
            opp: opportunityRec
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var result = response.getReturnValue();
                if (result.hasError) {
                    this.showAlert("error", result.errorMessage, "Failed!");
                } else {
                    component.set("v.contractDocumentOptions", result.contractDocumentOptions);
                    component.set("v.contractReceivedOptions", result.contractReceivedOptions);
                    component.set("v.creditFormOptions", result.creditFormOptions);
                }
            } else if (state == "ERROR") {
                this.showAlert("error", "Error at ContractGenerationHelper.loadPicklist()", "Failed!");
            }
        });
        $A.enqueueAction(action);
    },

    updateAsItemSelected: function (component, event) {
        var whichPicklist = event.getSource();
        if (!$A.util.isUndefinedOrNull(whichPicklist)) {
            var value = whichPicklist.get("v.value") === "--None--" ? "" : whichPicklist.get("v.value");
            switch (whichPicklist.getLocalId()) {
                case "contractDocument":
                    component.set("v.contractDocumentValue", value);
                    break;
                case "contractReceived":
                    component.set("v.contractReceivedValue", value);
                    component.set("v.isContractReturnedDateRequired", (value === "DocuSign" || value === "Fax or Scan"));
                    break;
                case "creditForm":
                    component.set("v.creditFormValue", value);
            }
        }
    },

    showAlert: function (alertType, alertMsg, alertTitle, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "dismissible",
            "duration": time
        });
        toastEvent.fire();
    },

    saveAllAppFields: function (component) {
        var opportunity = new Object();
        opportunity.Contract_Document__c = component.get("v.contractDocumentValue");
        opportunity.Contract_Received_Through__c = component.get("v.contractReceivedValue");
        opportunity.Credit_Form__c = component.get("v.creditFormValue");
        var signedDate = component.find("contractRetDate").get("v.value");
        if ($A.util.isUndefinedOrNull(signedDate) || $A.util.isEmpty(signedDate)) {
            opportunity.Contract_Signed_Date__c = "";
        } else {
            opportunity.Contract_Signed_Date__c = signedDate;
        }
        return opportunity;
    }
})