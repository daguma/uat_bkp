//Button_InvitationEmailHelper
({
    sendEmail: function (cmp) {
        var action = cmp.get('c.sendInvitationEmail');
        action.setParams({
            "oppId": cmp.get("v.recordId"),
            "acctNum": cmp.get("v.accountNumber"),
            "routingNum": cmp.get("v.routingNumber")
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if( result.hasError === true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": result.errorMessage,
                        "type": "error"
                    });
                    toastEvent.fire();
                }else{
                    cmp.set("v.invitationLink",result.invitationLink);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": result.successMessage,
                        "type": "success"
                    });
                    toastEvent.fire();
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    }
})