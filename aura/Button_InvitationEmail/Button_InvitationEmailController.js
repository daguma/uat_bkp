//BankStatement_AllTransactionsController
({
    confirmSendEmail: function (component, event, helper) {
        var sendEmail = confirm("The Invitation E-mail will be sent.\nAre you sure?");
        if (sendEmail === true) {
            helper.sendEmail(component);
        }
    }
})