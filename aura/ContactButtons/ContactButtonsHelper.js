({
    getIDologyEntity : function(component) {
        var recordId = component.get("v.recordId");
        
        if(recordId != '' || recordId != null) {
            var action = component.get("c.getContactEntity"); 
            
            action.setParams({
                'recordId': recordId,
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();                 
                component.set("v.showSpinner", false);
                
                if (state === "SUCCESS") {                   
                    component.set("v.contactEntity", result);  
                    console.log(result);
                } else if(state === 'ERROR') {
                    console.log('Error Error Error');
                    this.parseError(response);
                } else {
                    console.log("Something went wrong!");
                } 
            });
            
            $A.enqueueAction(action);
        }
    },
    validateSSN: function(component) {
        var contact = component.get("v.contactEntity.cont");
        var SSN = contact.ints__Social_Security_Number__c
        
        if($A.util.isEmpty(contact.MailingPostalCode)) {
            this.showToastAlert("Error!", "Please enter Postal Code", "error", 2000); 
            return false;
        }
        
        if($A.util.isEmpty(SSN) || (SSN.length < 4)) {
            this.showToastAlert("Error!", "Please enter valid SSN", "error", 2000); 
            return false;
        }        
        return true;        
    },
    populateSSN: function(component) {
        
        var action = component.get("c.SSNPopulate"); 
        
        action.setParams({
            'cont': component.get("v.contactEntity.cont")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {
                this.showToastAlert("", "SSN populated successfully", "success", 1000); 
                $A.get('e.force:refreshView').fire();
            } else if(state === 'ERROR') {
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            } 
        });
        
        $A.enqueueAction(action);
    },
    validateContact: function(component) {
        var validSSNPattern = new RegExp("^([0-9]{9})$");
        var contact = component.get("v.contactEntity.cont");
        var todaysDate = this.todaysDate();  //Date format Used: [yyyy-mm-dd]      
        
        if (!this.checkEmpty(contact.ints__Social_Security_Number__c) && !validSSNPattern.test(contact.ints__Social_Security_Number__c)) {          
            this.showToastAlert("Error!", "SSN is not in the correct format. Please input 9 digits only", "error", 2000); 
            return false;
        }
        
        if (contact.Use_of_funds__c == 'Others' && this.checkEmpty(contact.Other_Use_Of_Funds__c)) {
            this.showToastAlert("Error!", "Please input value in Other field as Use of Funds is Others", "error", 2000);
            return false;
        }
        
        if (contact.MailingState == '--None--' || this.checkEmpty(contact.MailingState)) {
            this.showToastAlert("Error!", "Please select state", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.MobilePhone) && this.checkEmpty(contact.Phone)) {
            this.showToastAlert("Error!", "Please provide phone number", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.MailingPostalCode)) {
            this.showToastAlert("Error!", "Please enter Postal Code", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.MailingCity)) {
            this.showToastAlert("Error!", "Please enter Mailing City", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.MailingStreet)) {
            this.showToastAlert("Error!", "Please enter Mailing Street", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.FirstName)) {
            this.showToastAlert("Error!", "Please enter Fistname", "error", 2000);
            return false;
        }
        
        if (this.checkEmpty(contact.LastName)) {
            this.showToastAlert("Error!", "Please enter Lastname", "error", 2000);
            return false;
        }
        
        if (contact.Birthdate >= todaysDate) {
            this.showToastAlert("Error!", "DOB cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        if (contact.Time_at_Current_Address__c >= todaysDate) {
            this.showToastAlert("Error!", "Time In Current Address cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        if (contact.Employment_Start_Date__c >= todaysDate) {
            this.showToastAlert("Error!", "Employment Start Date cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        if (contact.Previous_Employment_Start_Date__c >= todaysDate) {
            this.showToastAlert("Error!", "Prior Employment Started cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        if (contact.Previous_Employment_End_Date__c >= todaysDate) {
            this.showToastAlert("Error!", "Prior Employment Ended cannot be equal to nor greater than today", "error", 2000);
            return false;
        }
        
        return true;
    },
    payfoneandrecoveringQuestions: function(component, event) {       
        var action = component.get("c.payfoneRequest");
        
        action.setParams({
            'EntityStr': JSON.stringify(component.get("v.contactEntity"))
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();  
            
            if (state === "SUCCESS") {  
                if(!$A.util.isEmpty(result.popupMessage)) {
                    var message = result.popupMessage;
                    result.popupMessage = '';
                    if(message.includes("WARNING")) {
                        message = message.replace("WARNING:", "").trim();
                        this.showToastAlert("Warning", 'PAYFONE - ' + message, "warning", 3000);
                    }else if(message.includes("ERROR")) {
                        message = message.replace("ERROR:", "").trim();
                        this.showToastAlert("Error", 'PAYFONE - ' + message, "error", 3000);
                    }else if(message.includes("INFO")) {
                        message = message.replace("INFO:", "").trim();
                        this.showToastAlert("Info", 'PAYFONE - ' + message, "info", 3000);
                    }                   
                }                
            } else if(state === 'ERROR') {
                component.set("v.showSpinner", false);
                this.parseError(response);
                console.log("Payfone Error");
            } else {
                component.set("v.showSpinner", false);
                console.log("Payfone Unknown Error");
            }
            
            //IDOLOGY CALLOUT
            this.recoveringQuestions(component, event, result);
        });
        
        $A.enqueueAction(action);
    },
    recoveringQuestions: function(component, event, result) {
        var action = component.get("c.recoverQuestions");
        
        action.setParams({
            'EntityStr': JSON.stringify(result)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();  
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {
                console.log("Questions Recovered");
                if($A.util.isEmpty(result.popupMessage)) {
                    console.log("Payfone Succes");
                    component.set("v.contactEntity", result);
                }else {
                    var message = result.popupMessage;
                    result.popupMessage = '';
                    if(message.includes("WARNING")) {
                        message = message.replace("WARNING:", "").trim();
                        this.showToastAlert("Warning", message, "warning", 10000);
                    }else if(message.includes("ERROR")) {
                        message = message.replace("ERROR:", "").trim();
                        this.showToastAlert("Error", message, "error", 10000);
                    }else if(message.includes("INFO")) {
                        message = message.replace("INFO:", "").trim();
                        this.showToastAlert("Info", message, "info", 10000);
                    }
                }
                if(!result.questionsRecovered) 
                    $A.get('e.force:refreshView').fire(); //IDology quick fix
            } else if(state === 'ERROR') {
                this.parseError(response);
            } else {
                console.log("Recovering question request Failed!");
            }       
        });
        
        $A.enqueueAction(action);
    },
    processingQuestions: function(component, event) {
        var action = component.get("c.processQuestions"); 
        
        action.setParams({
            'EntityStr': JSON.stringify(component.get("v.contactEntity"))
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();  
            console.log(response.getReturnValue());
            if (state === "SUCCESS") {
                var message = result;
                if($A.util.isEmpty(message) || message.includes("SUCCESS")) {
                    component.set("v.contactEntity.questions", []);
                    if(!$A.util.isEmpty(message)) {
                        message = message.replace("SUCCESS:", "").trim();
                        this.showToastAlert("Success", message, "success", 10000);
                    }
                    $A.get('e.force:refreshView').fire();
                }else {
                    if(message.includes("WARNING")) {
                        message = message.replace("WARNING:", "").trim();
                        this.showToastAlert("Warning", message, "warning", 10000);
                    }else if(message.includes("ERROR")) {
                        message = message.replace("ERROR:", "").trim();
                        this.showToastAlert("Error", message, "error", 10000);
                    }else if(message.includes("INFO")) {
                        message = message.replace("INFO:", "").trim();
                        this.showToastAlert("Info", message, "info", 10000);
                    }
                }
            } else if(state === 'ERROR') {
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            }
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    CancelprocessingQuestions: function(component, event) {
        var action = component.get("c.cancelQuestionsProcess"); 
        
        action.setParams({
            'EntityStr': JSON.stringify(component.get("v.contactEntity"))
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
            } else if(state === 'ERROR') {
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            }
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    todaysDate: function(component) {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        
        return [year, month, day].join('-');
        
    },
    parseError: function(response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                this.showToastAlert("Error!", errors[0].message, "error", 2000); 
            }
        } else {
            this.showToastAlert("", "Unkown Error", "error", 2000); 
        }
    },
    showToastAlert : function(alertTitle, alertMsg, alertType, alertTime) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "sticky",
            "duration": alertTime
        });
        toastEvent.fire();
    },
    setError : function(component, errorMessage) {
        component.set("v.errorMessage", errorMessage);
        component.set("v.hasError", true);
    },
    clearError : function(component, errorMessage) {
        component.set("v.errorMessage", "");
        component.set("v.hasError", false);
    },
    checkEmpty : function(data) {
        if(typeof(data) == 'number' || typeof(data) == 'boolean')
        { 
            return false; 
        }
        if(typeof(data) == 'undefined' || data === null)
        {
            return true; 
        }
        if(typeof(data.length) != 'undefined')
        {
            return data.length == 0;
        }
        var count = 0;
        for(var i in data)
        {
            if(data.hasOwnProperty(i))
            {
                count ++;
            }
        }
        return count == 0;
    },
    submittingApplication : function(component) {
        var contact = component.get("v.contactEntity.cont");
        
        if(!$A.util.isEmpty(contact.Birthdate)) {
            var action = component.get("c.submitApplicaiton");
            action.setParams({
                contactID : component.get("v.recordId"),
                BirthDate : contact.Birthdate
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                component.set("v.showSpinner", false);
                if (state === "SUCCESS") {      
                    console.log(result);
                    var result = JSON.parse(response.getReturnValue());
                    
                    if(result.hasError) {
                        var msg = result.message;
                        if(msg.includes("=>")) {
                            var msgs = msg.split("=>");
                            for(var s = 0; s < msgs.length; s++) {
                                this.showToastAlert("Error!", msgs[s], "error", 5000);
                            }
                        }else
                            this.showToastAlert("Error!", result.message, "error", 5000);
                    }else {
                        this.callingSoftPull(component, result.message);
                    }
                } else if(state === 'ERROR') {
                    console.log('Error Error Error');
                    this.parseError(response);
                } else {
                    console.log("Something went wrong!");
                } 
                
            });
            
            $A.enqueueAction(action);
        }else {
            component.set("v.showSpinner", false);
            this.showToastAlert("Error!","Date of Birth is missing", "error", 1000);
        }       
    },
    callingSoftPull : function(component, applicationId) {
        component.set("v.showSpinner", true);
        var contact = component.get("v.contactEntity.cont");
        
        var action = component.get("c.callSoftPull");
        action.setParams({
            contactID : component.get("v.recordId"),
            appID : applicationId
        });
        
        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();           
            if (state === "SUCCESS") {      
                this.showToastAlert("Success", "Application created successfully", "success", 1000);
                $A.get('e.force:refreshView').fire();
            } else if(state === 'ERROR') {
                console.log('Error Error Error');
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            } 
            
        });
        
        $A.enqueueAction(action);    
    },
    IncomeNotSufficient : function(component) {
        var action = component.get("c.markIncomeNotEnough");
        var recordId = component.get("v.recordId");
        
        action.setParams({
            "contactID" : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {                   
                var result = response.getReturnValue();
                if(result != null) {
                    if(result.indexOf("marked as Not Enough") >= 0) {
                        console.log(result);
                        this.showToastAlert("Success", result, "success", 1000);
                    }else {
                        this.showToastAlert("Error!", result, "error", 1000);
                    }                    
                    $A.get('e.force:refreshView').fire();
                }else {
                    this.showToastAlert("Error!", "Something went wrong.", "error", 1000);
                }
            } else if(state === 'ERROR') {
                console.log('Error Error Error');
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            } 
            
        });
        
        $A.enqueueAction(action);
    },
    RefreshingFICOInfo : function(component) {
        var action = component.get("c.refreshFICOInfo");
        var recordId = component.get("v.recordId");
        
        action.setParams({
            "contactID" : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
            } else if(state === 'ERROR') {
                console.log('Error Error Error');
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            } 
            
        });
        
        $A.enqueueAction(action);
    },
    ResetingAuthSSNCount : function(component) {
        var action = component.get("c.resetAuthSSNCount");
        var recordId = component.get("v.recordId");
        
        action.setParams({
            "contactID" : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {    
                var result = response.getReturnValue();
                console.log(result);
                if(result != null) {
                    if(result.indexOf("successfully") >= 0) {
                        this.showToastAlert("Success", result, "success", 1000);
                    }else {
                        this.showToastAlert("Error!", result, "error", 1000);
                    }                
                    $A.get('e.force:refreshView').fire();
                }else {
                    this.showToastAlert("Error!", "Something went wrong.", "error", 1000);
                }
            } else if(state === 'ERROR') {
                console.log('Error Error Error');
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            }            
        });
        
        $A.enqueueAction(action);
    },
    CreatingUserforCustomerPortal : function(component) {
        var action = component.get("c.CreateUserforCustomerPortal");
        var recordId = component.get("v.recordId");
        
        action.setParams({
            "contactID" : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {    
                var result = response.getReturnValue();
                console.log(result);
                if(result != null) {
                    if(result.indexOf("processed") >= 0) {
                        this.showToastAlert("Success", result, "success", 1000);
                    }else {
                        this.showToastAlert("Error!", result, "error", 1000);
                    }                
                    $A.get('e.force:refreshView').fire();
                }else {
                    this.showToastAlert("Error!", "Something went wrong.", "error", 1000);
                }
            } else if(state === 'ERROR') {
                console.log('Error Error Error');
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            }            
        });
        
        $A.enqueueAction(action);
    },
    verifyingEmail : function(component) {
        var action = component.get("c.verifyEmail");
        var recordId = component.get("v.recordId");
        
        action.setParams({
            "contactID" : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {    
                console.log('Verify Email Result : ' + response.getReturnValue());
                var result = JSON.parse(response.getReturnValue());
                if(result.message === 'success') {
                    this.showToastAlert("", "Email Verified Successfully!", "success", 2000);
                    $A.get('e.force:refreshView').fire();
                }else
                    this.showToastAlert("", result.message, "error", 2000);
            } else if(state === 'ERROR') {
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            }            
        });
        
        $A.enqueueAction(action);
    },
    cancelVerification : function(component) {
        var action = component.get("c.callCancelVerification");
        var recordId = component.get("v.recordId");
        
        action.setParams({
            "entityId" : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {  
                console.log('Cancel Verification Result : ' + response.getReturnValue());
                var result = JSON.parse(response.getReturnValue());
                if(result.status === '200') {
                    if((result.message).includes('Error Code'))
                     this.showToastAlert("", result.message, "error", 1000);                    
                    else
                     this.showToastAlert("", result.message, "success", 1000);                    
                    //$A.get('e.force:refreshView').fire();
                }else
                    this.showToastAlert("", result.message, "error", 2000);
                //this.showToastAlert("Success", "Verification Order cancelled successfully", "success", 1000);
                $A.get('e.force:refreshView').fire();
            } else if(state === 'ERROR') {
                console.log('Error Error Error');
                this.parseError(response);
            } else {
                console.log("Something went wrong!");
            } 
            
        });
        
        $A.enqueueAction(action);
        
    },
    applyCSS: function(component){
        component.set("v.cssStyle", "<style>.toastMessage.forceActionsText{ white-space : pre-line !important; }</style>");
    }
})