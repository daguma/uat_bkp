({
    doInit : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.applyCSS(component);
        helper.getIDologyEntity(component);
    },
    populateSSN : function(component, event, helper) {
        if(helper.validateSSN(component)) {
            component.set("v.showSpinner", true);
            helper.populateSSN(component);
        }
    },
    recoverIDologyQuestions: function(component, event, helper) {   
        if(helper.validateContact(component)) {
            component.set("v.showSpinner", true);
        	helper.payfoneandrecoveringQuestions(component, event);
        }
    },
    processingQuestions: function(component, event, helper) {
        var process = event.getParam("process");

        if(process === 'Yes')
    		helper.processingQuestions(component, event);
        if(process === 'No')
    		helper.CancelprocessingQuestions(component, event);
    },
    submittingApplication: function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.submittingApplication(component);
    },
    IncomeNotSufficient : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.IncomeNotSufficient(component);
    },
    RefreshingFICOInfo : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.RefreshingFICOInfo(component);
    },
    ResetingAuthSSNCount : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.ResetingAuthSSNCount(component);
    },
    CreatingUserforCustomerPortal : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.CreatingUserforCustomerPortal(component);
    },
    showAllCreditReports : function(component, event, helper) {
        component.set("v.body", []);
        $A.createComponent(
            "c:AllCreditReports",
            {
                "recordId": component.get("v.recordId")
            },
            function(allreportsCmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(allreportsCmp);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    verifyingEmail : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.verifyingEmail(component);
    },
    createNewCase : function(component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Case",
            "defaultFieldValues": {
                'ContactId' : component.get("v.recordId")
            }
        });
        createRecordEvent.fire();
    },
    showeOscarSummary : function(component, event, helper) {
        component.set("v.body", []);
        $A.createComponent(
            "c:eOscarSummary",
            {
                "recordId": component.get("v.recordId")
            },
            function(eOscarcmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(eOscarcmp);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    cancelVerification : function(component, event, helper) {
        var r=confirm("Are you sure to cancel the Verification?");
        //var r=helper.alertWithoutNotice("Are you sure to cancel the Verification?");        
        if (r==true)
        {
          component.set("v.showSpinner", true);
          helper.cancelVerification(component);
        }
    }
})