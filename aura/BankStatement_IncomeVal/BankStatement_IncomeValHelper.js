//BankStatement_IncomeValHelper
({
    updateFieldsFromCtrl : function (component,oppId) {
        var opp = component.get("c.updateFields");
        opp.setParams({
            "oppId": oppId
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert("Success: " + response.getReturnValue());
                var result = response.getReturnValue();
                if(result.hasError){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": result.errorMessage,
                        "type": "error"
                    });
                    toastEvent.fire();
                }else{
                    component.set('v.opportunity', result.opportunity);
                }
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Unsuccessfull process",
                    "type": "error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(opp);
    }
})