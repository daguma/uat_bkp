//BankStatement_IncomeValController
({
    updateAllFields : function (component, event, helper) {
        var existingRecordId = component.get("v.recordId");
        if (!$A.util.isUndefined(existingRecordId) && !$A.util.isEmpty(existingRecordId)) {
            helper.updateFieldsFromCtrl(component,existingRecordId);
        }
    }
})