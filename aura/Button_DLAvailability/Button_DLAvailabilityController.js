//Button_DLAvailabilityController
({
    checkDLAvailability: function (component, event, helper) {
        var ACHRoutingNumber = component.get("v.routingNumber");
        var ACHBankname = component.get("v.bankName");

        var ACHRoutingNumberIsNotEmpty = false;
        var ACHRoutingNumberIsValid = false;
        if(!$A.util.isUndefinedOrNull( ACHRoutingNumber )){
            ACHRoutingNumberIsNotEmpty = (ACHRoutingNumber.trim() != null && ACHRoutingNumber.trim().length != 0 && ACHRoutingNumber.trim() != '');
            ACHRoutingNumberIsValid = (ACHRoutingNumber.trim() != null && ACHRoutingNumber.trim().length != 0 && ACHRoutingNumber.trim().length == 9);
        }

        var ACHBanknameIsNotEmpty = false;
        if(!$A.util.isUndefinedOrNull( ACHBankname )){
            ACHBanknameIsNotEmpty = (ACHBankname.trim() != null && ACHBankname.trim().length != 0 && ACHBankname.trim() != '');
        }

        var goAhead = false;
        if (ACHRoutingNumberIsNotEmpty) {
            if (!ACHRoutingNumberIsValid) {
                alert('The ACH Routing Number must be exactly 9 digits.');
            } else {
                goAhead = true;
            }
        } else if (ACHBanknameIsNotEmpty) {
            goAhead = true;
        } else {
            alert('Please fill the Routing Number or Bank Name field.');
        }

        if (goAhead === true) {
            helper.checkAvailability(component, ACHRoutingNumber, ACHBankname, component.get("v.recordId"));
        }
    }
})