//Button_DLAvailabilityHelper.js
({
    checkAvailability : function (component, aCHRoutingNumber, aCHBankname, oppId) {
        var opp = component.get("c.checkForDLAvailable");
        opp.setParams({
            "aCHRoutingNumber": aCHRoutingNumber,
            "aCHBankname": aCHBankname,
            "oppId": component.get("v.recordId")
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.hasError){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": result.errorMessage,
                        "type": "error"
                    });
                    toastEvent.fire();
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": result.successMessage,
                        "type": "success"
                    });
                    toastEvent.fire();
                }
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(opp);
    }
})