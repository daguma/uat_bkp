({ 
	init : function(component, event, helper ) {
        var recordId = component.get( 'v.recordId' );
        
        if(!$A.util.isEmpty(recordId)) {
            helper.fetchAttachments(component, recordId);
        }
    }, 

    onSelectAttachment : function(component, event, helper ) {
		let params = event.getParam("value").split(',');
		let attachmentId = params[0];
		let action = params[1];
		let attacmentIndex = params[2];

	    switch (action) {
	      case "View": 
	      helper.showAttachment(component, attachmentId);
	      break;
	      case "Delete": 
	      component.set("v.showSpinner", true)
	      helper.deleteAttachment(component, attachmentId, attacmentIndex);
	      break;
	    }
    }
})