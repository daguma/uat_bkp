({
    fetchAttachments: function (cmp, recordId) {console.log('recordId '+ recordId);
        var helper = this;
        var action = cmp.get('c.getOpportunityAttachments');
        action.setParams({ "recordId" : recordId });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue(); console.log(result);
                if(result.length > 0) {
                   
                    helper.applyFileTypeIconNames(cmp, result)
                    cmp.set("v.attachments", result);
                }else { 
                    cmp.set("v.attachments", []);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
                cmp.set("v.noResults", "An error occurred: No records to display");
            }
            cmp.set("v.showSpinner", false)
        }));
        $A.enqueueAction(action);
    },

    applyFileTypeIconNames : function(cmp, files ) {

        for ( var i = 0; i < files.length; i++ ) {

            var iconName = 'doctype:attachment';
            var file = files[i];
            var fileName = file.Name.toLowerCase();

            if (fileName.includes('.pdf')) {
                iconName = 'doctype:pdf';
            }
            else if (fileName.includes('.png') || fileName.includes('.jpg')) {
                iconName = 'doctype:image';
            }
            else if (fileName.includes('.html')) {
                iconName = 'doctype:html';
            }
            else if (fileName.includes('.xlsx')) {
                iconName = 'doctype:excel';
            }
            else if (fileName.includes('.doc') || fileName.includes('.docx')) {
                iconName = 'doctype:word';
            }

            file.FileTypeIconName = iconName;

        }
 
    },

    showAttachment: function(cmp, attachmentId) {
        window.open('/servlet/servlet.FileDownload?file='+attachmentId);
    },

    deleteAttachment: function(cmp, attachmentId, attacmentIndex) {
        let attachments = cmp.get("v.attachments");
        var action = cmp.get('c.deleteAttachment');
        action.setParams({ "attachmentId" : attachmentId });

        action.setCallback(this, $A.getCallback(function (response) { console.log(response);
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === "SUCCESS" && result === true) {
                this.showToastAlert("Attachment deleted", "The attachment was removed from the opportunity", "success", 1000);
                attachments.splice(attacmentIndex, 1);
                cmp.set("v.attachments", attachments);
            } else if (state === "ERROR" || result === false) {
                var errors = response.getError();
                console.error(errors);
                this.showToastAlert("Error", "An error has occurred when you tried to delete the attachment", "error", 1000);
            }
            cmp.set("v.showSpinner", false)
        }));
        $A.enqueueAction(action);
    },

    showToastAlert : function(alertTitle, alertMsg, alertType, alertTime) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "sticky",
            "duration": alertTime
        });
        toastEvent.fire();
    },

})