({
    geteOscarSummaryData : function(component, recordId) {
		component.set("v.showSpinner", false);
        var action = component.get("c.geteOscarSummary");
        
        action.setParams({ 
            "contactId" : recordId
        });

        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.eOscarEntity", result);                    
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
	},
	applyCSS: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:0} .forceStyle.desktop .viewport{overflow:hidden}</style>");
    },
    revertCssChange: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:5} .forceStyle.desktop .viewport{overflow:visible}</style>");
    }
})