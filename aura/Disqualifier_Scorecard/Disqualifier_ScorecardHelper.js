//Disqualifier_ScorecardHelper
({
    getDSEntity: function (component) {
        var opp = component.get("c.getDisqualifierScorecardEntity");
        opp.setParams({
            "oppId": component.get("v.recordId")
        });
        opp.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.disqualifierScorecardEntity", result);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(opp);
    }
})