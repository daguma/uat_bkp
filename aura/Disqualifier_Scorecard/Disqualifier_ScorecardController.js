//Disqualifier_ScorecardController
({
    init: function (cmp, event, helper) {

        //Load DisqualifierScorecardEntity from server if not loaded yet and if the Opp id is present
        var existingEntity = cmp.get("v.disqualifierScorecardEntity");
        var existingRecordId = cmp.get("v.recordId");
        if (existingEntity === null && existingRecordId !== null) {
            helper.getDSEntity(cmp);
        }
    }
})