({
    // Load expenses from Salesforce
	doInit: function(component, event, helper) {
        
		//Get Fraud Summary Entity
        var action = component.get("c.getFraudSummaryEntity");
    	action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setBackground();
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(result);
            if (state === "SUCCESS") {
            	component.set("v.fraudEntity", result);
            }
            else {
                console.log("Failed with state: " + state);
            }
            component.set("v.showSpinner", false);
        });
        
        // Send action off to be executed
        $A.enqueueAction(action);
	},   
    updateOpp: function(component, event, helper) {
        component.set("v.showSpinner", true);
        var fraudEntity = component.get("v.fraudEntity");
        
        var actionUpdOpp = component.get("c.updateOpportunity");
        
        actionUpdOpp.setParams({
            "opp": fraudEntity.application
        });
        
        // Add callback behavior for when response is received
        actionUpdOpp.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log(result);
            if (state === "SUCCESS") {
            	$A.get('e.force:refreshView').fire();
            }
            else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                	"title": "Error!",
                    "message": "Application update failed.",
                    "type": "error",
                    "duration": 2000,
                    "mode": "dismissible"
                });
                toastEvent.fire();
                console.log("Failed with state: " + state);
                
            }
            component.set("v.showSpinner", false);
        });
        
        // Send action off to be executed
        $A.enqueueAction(actionUpdOpp);
    }
})