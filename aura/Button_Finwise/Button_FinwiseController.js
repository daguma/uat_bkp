({
	init : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        
        var action = component.get("c.disableFinwise");
        action.setParams({
            "recordId": recordId
        });
        
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.isfinwiseDisabled", result);
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error something unexpected happened.')
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
	},
    callFinwise : function(component, event, helper) {
        component.set("v.showSpinner", true);
		var recordId = component.get("v.recordId");
        var appState = component.get("v.appState");
        
        var action = component.get("c.submitToFinwise");
        action.setParams({
            "recordId": recordId,
            "applicationState": appState
        });
        
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Info!",
                    "message": result,
                    "type": "info",
                    "mode": "dismissible",
                    "duration": 4000
                });
                toastEvent.fire();
            }else if(state === "ERROR") {
                this.parseError(response);
            }else {
                console.log('Error something unexpected happened.')
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
	},
})