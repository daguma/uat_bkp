({
    loadUniversalNotes : function(component, recordId) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = component.get("c.getRelatedNotes");
        action.setParams({ "recordId" : recordId });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            console.log("UniversalNotesHelper loadUniversalNotes state: " + state);
            
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                console.log("UniversalNotesHelper loadUniversalNotes result: " + JSON.stringify(result));
                
                component.set("v.showSpinner", false);
                if(result.length > 0) {
                    component.set("v.notes", result);
                }else {
                    component.set("v.notes", []);
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    showToastAlert : function(alertTitle, alertMsg, alertType, alertTime) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "dismissible",
            "duration": alertTime
        });
        toastEvent.fire();
    },
    applyCSS: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:0} .forceStyle.desktop .viewport{overflow:hidden}</style>");
    },
    revertCssChange: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:5} .forceStyle.desktop .viewport{overflow:visible}</style>");
    }
})