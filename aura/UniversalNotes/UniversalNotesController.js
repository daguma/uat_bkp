({
	doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        helper.applyCSS(component);
        if(!$A.util.isEmpty(recordId)) {
            console.log("UniversalNotesController doInit SUCCESS");
            helper.loadUniversalNotes(component, recordId);
        }
	},
    closeModal : function(component, event, helper) {
        helper.revertCssChange(component);
        document.getElementById("universalNote-container").style.display = "none";
    },
    removeDeletedRow : function(component, event, helper) {
        var index = event.getParam("indexVar"); 
        var notesList = component.get("v.notes");
        
        notesList.splice(index, 1);
        component.set("v.notes", notesList);
    }
})