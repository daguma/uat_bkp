//AppDetailsHelper
({
    loadPicklists : function (component) {
        var action = component.get("c.getPicklistValues");
        var opportunityRec = component.get("v.applicationDetailsEntity");
        action.setParams({
            opp : opportunityRec
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var result = response.getReturnValue();
                if(result.hasError){
                    this.showAlert("error", result.errorMessage, "Failed!");
                }else{
                    component.set("v.paymentMethodOptions",result.paymentMethods);
                    component.set("v.incmDenominatorOptions",result.incomeDenominators);
                    component.set("v.bankInfoSourceOptions",result.bankInfoSources);
                }
            } else if(state == "ERROR"){
                this.showAlert("error", "Error at DocumentsHelper.getDocsEnt()", "Failed!");
            }
        });
        $A.enqueueAction(action);
    },

    showAlert : function(alertType, alertMsg, alertTitle, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "dismissible",
            "duration": time
        });
        toastEvent.fire();
    },

    save : function (component) {
        var opportunity = new Object();
        opportunity.id = component.get("v.recordId");
        opportunity.ACH_Bank_Name__c = component.find("bankName").get("v.value");
        alert(JSON.stringify(opportunity));
    }
})