//AppDetailsController
({
    doInit : function (component, event, helper) {
        helper.loadPicklists(component);
    },

    callQuickSaveApp : function (component, event, helper) {
        var compEvent = component.getEvent("ApplicationDetails_SaveEvt");
        // Optional: set some data for the event (also known as event shape)
        // A parameter’s name must match the name attribute
        // of one of the event’s <aura:attribute> tags
        // compEvent.setParams({"myParam" : myValue });
        compEvent.fire();
    },

    saveAppDetails : function (component, event, helper) {
        helper.save(component);
        var message = "saveAppDetails";
        return message;
    },

    dummyMethod : function (component, event) {
        var dummy = "dummy";
    }
})