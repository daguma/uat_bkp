//CreditReportDetailsController.js
({
	doInit : function(component, event, helper) {
        helper.applyCSS(component);
		var action = component.get("c.getCreditReportDetail");
        action.setParams({
            entityId : String(component.get("v.recordId")),
            reportId : String(component.get("v.reportId")),
            viewattach : false
        });

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.crDetailEntity", response.getReturnValue());
                console.log(component.get("v.crDetailEntity.CreditReportText"));
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });

        $A.enqueueAction(action);
	},
    closeModal : function(component, event, helper) {
        component.set("v.showCreditReport", false);
        helper.revertCssChange(component);
    },
    activeTab : function(component, event, helper) {
		var clicked = event.target.id;
        var clickedTab = parseInt(clicked.substring(clicked.length, clicked.length - 1));
        helper.activateDeactivateTab(component, clickedTab);
	}
})