({
	applyCSS: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:0} .forceStyle.desktop .viewport{overflow:hidden}</style>");
    },
    revertCssChange: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:5} .forceStyle.desktop .viewport{overflow:visible}</style>");
    },
    activateDeactivateTab: function(component, active) {      
        //Active Tab
        var AC_tabId = component.find(active + '-tab');
        var AC_tabData = component.find('tab-scoped-' + active);
        $A.util.addClass(AC_tabId, 'slds-is-active');
        $A.util.addClass(AC_tabData, 'slds-show');
        $A.util.removeClass(AC_tabData, 'slds-hide');
        
        //Deactive Tab
        for(var i = 1; i < 6; i++) {
            if(i !== active) {
                var DE_tabId = component.find(i + '-tab');
                var DE_tabData = component.find('tab-scoped-' + i);
                $A.util.removeClass(DE_tabId, 'slds-is-active');
                $A.util.removeClass(DE_tabData, 'slds-show');
                $A.util.addClass(DE_tabData, 'slds-hide');
            }
        }
    }
})