({
	goToRefinanceNotes : function(component, event, helper) {
        var createRefinanceNoteEvent = $A.get("e.force:createRecord");
        createRefinanceNoteEvent.setParams({
            "entityApiName": "Refinance_Notes__c",
            "defaultFieldValues": {
                'Opportunity__c' : component.get("v.recordId")
            }
        });
        createRefinanceNoteEvent.fire();
	}
})