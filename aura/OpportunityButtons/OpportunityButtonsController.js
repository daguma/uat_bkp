({
	getUniversalNotes : function(component, event, helper) {
        component.set("v.body", []);
        var recordId = component.get("v.recordId");
        
        if(!$A.util.isEmpty(recordId)) {
            console.log("OpportunityButtons getUniversalNotes SUCCESS");
            helper.loadUniversalNotes(component, recordId);
        }else {
            this.showToastAlert("Error", "No record Id found", "error", 1000)
        }
    },
    goToRefinanceNotes : function(component, event, helper) {
        var createRefinanceNoteEvent = $A.get("e.force:createRecord");
        createRefinanceNoteEvent.setParams({
            "entityApiName": "Refinance_Notes__c",
            "defaultFieldValues": {
                'Opportunity__c' : component.get("v.recordId")
            }
        });
        createRefinanceNoteEvent.fire();
	},
    showCustomerProfile : function(component, event, helper) {
        component.set("v.body", []);
        $A.createComponent(
            "c:CustomerProfileDetails",
            {
                "recordId": component.get("v.recordId")
            },
            function(customerProfileCmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(customerProfileCmp);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    }
})