({
    loadUniversalNotes : function(component, recordId) {
        $A.createComponent(
            "c:UniversalNotes",
            {
                "recordId": recordId
            },
            function(newCmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    console.log("OpportunityButtons loadUniversalNotes SUCCESS");

                    var body = component.get("v.body");
                    body.push(newCmp);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                	console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
     revertCssChange: function(component){
        component.set("v.cssStyle", "<style>.forceStyle .viewport .oneHeader.slds-global-header_container {z-index:5} .forceStyle.desktop .viewport{overflow:visible}</style>");
    }
})