({
    getpickListOptions : function(component, fieldName) {
        var action = component.get("c.getselectOptions");
        action.setParams({ 
            "objObject": { 'sobjectType': 'Opportunity' },
            "fld": fieldName
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue();
                var opts = [];
                opts.push({
                    label: '--None--',
                    value: ''
                });
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                if(fieldName === 'FS_Assigned_To_Val__c')
                    component.set("v.fsoptions", opts);
                else
                    component.set("v.uwoptions", opts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    //LOAD ATTACH NOTE MODAL COMPONENT
    loadAddNoteModal : function(component, title) {
        component.set("v.body", []);
        $A.createComponent(
            "c:AttachNote",
            {
                "noteEntity": { 
                    'sobjectType': 'Note',
                    'ParentId': component.get("v.recordId"),
                    'Title': title     
                }
            },
            function(customerProfileCmp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(customerProfileCmp);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
            }
        );
    },
    populateOverriddenBy : function(component, checkField, OverridenField, MeetsToleranceField) {
        var currentUserName = component.get("v.validationEntity.currentUserName");
        var check = component.get("v.validationEntity.app." + checkField);
        
        if(check)
            component.set("v.validationEntity.app." + OverridenField, currentUserName);
        else
            component.set("v.validationEntity.app." + OverridenField, '');
        
        if(MeetsToleranceField != '')
        	component.set("v.validationEntity.app." + MeetsToleranceField, check);
    },
    
    validateDecisionLogicAccountNumber : function(component){
        var DLAccountNumberFound =  component.get("v.validationEntity.app.DL_Account_Number_Found__c"); 
        var oppStatus = component.get("v.validationEntity.app.Status__c");
        var showDLNotifications = false;
        
        if (oppStatus === 'Funded') {
            showDLNotifications = false
        }
         showDLNotifications = (DLAccountNumberFound == null || DLAccountNumberFound == '' || DLAccountNumberFound == '--None--' || DLAccountNumberFound == undefined);
        
        component.set("v.showDLNotifications", showDLNotifications);
    },
    
    validateDLManualBoton : function(component) {  
  
        var oppStatus = component.get("v.validationEntity.app.Status__c");
        var DLAccountNumberFound =  component.get("v.validationEntity.app.DL_Account_Number_Found__c"); 
        var validAccountNumber = (DLAccountNumberFound != null && DLAccountNumberFound != '' && DLAccountNumberFound != '--None--' && DLAccountNumberFound != undefined);
        var disableBoton = (validAccountNumber && oppStatus != 'Funded') ? "false" : "true";
        //console.log('disable DL manual Boton: ' + disableBoton);
        component.set("v.hideDLManualBoton", disableBoton);
    },

    checkWhyDealIsNotReady : function(component) {
        var msg = '\n';
        var entity = component.get("v.validationEntity");

        if (!entity.app.InqCurrAddOnFile_Meet_Tolerance__c) msg += '-InqCurrAddOnFile Tolerance.\n';         
        if (!entity.app.Is_SSNRepMoreFreqAnother_Tolerance_Met__c) msg += '-SSNRepMoreFreqAnother Tolerance.\n';         
        if (!entity.app.Is_PTI_Tolerance_Met__c) msg += '-PTI Tolerance.\n';         
        if (!entity.app.Is_Overdrafts_Tolerance_Met__c) msg += '-Overdrafts Tolerance.\n';         
        if (!entity.app.Is_YTD_NSFs_Tolerance_Met__c) msg += '-YTD NSFs Tolerance.\n';         
        if (!entity.app.Employment_Time_Meets_Tolerance__c) msg += '-Employment Time Tolerance.\n';         
        if (!entity.app.Fee_Meets_Tolerance__c) msg += '-Fee Tolerance.\n';         
        if (!entity.app.Estimated_Grand_Total_Meets_Tolerance__c) msg += '-Estimated Grand Total Tolerance.\n';         
        if (!entity.app.DTI_Meets_Tolerance__c) msg += '-DTI Tolerance.\n';         
        if (!entity.app.Is_Maximum_Payment_Meets_Tolerance__c) msg += '-Maximum Payment Tolerance.\n';         
        if (!entity.app.Is_Offer_Payment_Amount_Tolerance_Met__c) msg += '-Offer Payment Amount Tolerance.\n';         
        if (!entity.app.Is_Scorecard_Grade_Tolerance_Met__c) msg += '-Scorecard Grade Tolerance.\n';         
        if (!entity.app.Manual_Grade_Meets_Tolerance__c) msg += '-Manual Grade Tolerance.\n';         
        if (!entity.app.Employment_Verification_Meets_Tolerance__c) msg += '-Employment Verification Tolerance.\n';         
        if (!entity.app.DecisionLogicMeetsTolerance__c) msg += '-DL Tolerance.\n';         
        if (!entity.app.DecisionLogicAccepted__c) msg += '-DecisionLogicAccepted.\n';         
        if (!entity.app.EWS_Call_Meets_Tolerance__c) msg += '-EWS Call.\n';  
        if (!entity.app.Is_Signed_Contract_Tolerance_Met__c) msg += '-Signed Contract Met.\n';         
        if (!entity.app.DL_Time_Meets_Tolerance__c) msg += '-DL Time.\n';         
        if (!entity.app.BRMS_Rules_Meets_Tolerance__c) msg += '-BRMS Rules.\n';      
        if (!entity.app.CSI_Watch_List_Approved__c) msg += '-CSI Watch List.\n';         
        if (!entity.app.Fraud_Alert_Accepted__c) msg += '-Fraud Alert.\n';     
        if (!entity.app.Is_Totals_Accepted__c) msg += '-Is Totals Unaccepted.\n';
        if (! (entity.app.Is_Miscellanous_RentalScheduleE_Accepted__c || !entity.showsMiscellaneousApplicants) ) msg += '-Miscellanous RentalScheduleE or showsMiscellaneousApplicants.\n';    
        if (! (entity.app.Is_Miscellaneous_Child_Support_Accepted__c || !entity.showsMiscellaneousApplicants) ) msg += 'Miscellaneous_Child_Support or showsMiscellaneousApplicants.\n';
        if (! (entity.app.Profit_Loss_Statement_Accepted__c || !entity.showsSelfEmployedApplicants) ) msg += '-Profit Loss Statement or showsSelfEmployedApplicants.\n'; 
        if (! (entity.app.Form1099_Accepted__c || !entity.showsSelfEmployedApplicants) ) msg += '-Form1099 or showsSelfEmployedApplicants.\n';
        if (! (entity.app.Is_YTD_Gross_Paystub_Accepted__c || !entity.showsEmployedApplicants ) ) msg += '-YTD Gross Paystub or showsEmployedApplicants.\n';
        if (! (entity.app.Is_Signed_Contract_Accepted__c || entity.app.Contact__r.MailingState == 'WI') ) msg += '-Signed Contract or MailingState.\n';     
        if (!entity.dl_Accepted_Variable) msg += '-DL Unaccepted.\n';
        if (!entity.reviewed_and_Cleared_Variable) msg += '-Reviewed and Cleared.\n';
        if (!entity.employment_Time_at_Job_Accepted_Variable) msg += '-Employment Time at Job.\n';
        if (!entity.app.Idology_Accepted__c) msg += '-Idology Unaccepted.\n';
        if (entity.isHardPullPending) msg += '-Hard Pull.\n';         
        if (!entity.bridgerMeetsTolerance) msg += '-Bridger.\n';   
        
        var SelfEmpNoPOIAccepted = true;
        if (entity.SelfEmpNoPOIReq  && !entity.app.Is_Self_Employment_Bank_Data_Overridden__c) {  
            var paymentAmt = (entity.app.Payment_Amount__c == null ? 0 : entity.app.Payment_Amount__c);
           
            SelfEmpNoPOIAccepted = ( (entity.app.Total_Deposits_Amount__c > (5 * paymentAmt)) && 
                                     (entity.app.Scorecard_Grade__c != 'D' && entity.app.Scorecard_Grade__c != 'F') &&
                                     (entity.app.ACH_Account_Type__c == 'Checking') &&
                                     (entity.currentAvgDailyBal > (3 * paymentAmt)) 
                                    ); 

            if (entity.app.NSFs_Number__c != null && entity.app.Overdrafts_Number__c != null && entity.app.YTD_NSFs_Number__c != null && entity.app.YTD_Overdrafts_Number__c != null)
                SelfEmpNoPOIAccepted = (SelfEmpNoPOIAccepted && ((entity.app.NSFs_Number__c + entity.app.Overdrafts_Number__c) <= 1) && ((entity.app.YTD_NSFs_Number__c + entity.app.YTD_Overdrafts_Number__c) <= 3));

            SelfEmpNoPOIAccepted = (SelfEmpNoPOIAccepted && entity.hasLast3MonthsBankDepositsPopulated && entity.hasLast3MonthsBankStatementsPopulated);
        }

        if (!SelfEmpNoPOIAccepted) msg += 'SelfEmpNoPOIAccepted'; 
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Actions:",
            "message": msg,
            "type": "info",
            "mode": "dismissible",//sticky
            "duration":10000
        });
        toastEvent.fire();
    } 
})