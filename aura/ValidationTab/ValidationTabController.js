({
    //ON LOAD OF VALIDATION TAB
    doInit : function(component, event, helper) {
        //Loading Funding Specialist Assigned Picklist Values
        helper.getpickListOptions(component, 'UW_Assigned_To_Val__c');
        //Loading UnderWriter Assigned Picklist Values
        helper.getpickListOptions(component, 'FS_Assigned_To_Val__c');
        
        var action = component.get("c.getValidationEntity");
        action.setParams({ 
            recordId : component.get("v.recordId") 
        });
        action.setBackground();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log(response.getReturnValue());
                var result = response.getReturnValue();
                component.set("v.validationEntity", result);
                helper.validateDecisionLogicAccountNumber(component);
                helper.validateDLManualBoton(component);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    //SUBMIT FINWISE
	finwiseAction: function (component, event, helper) {
        
        component.set("v.showSpinner", true);
        
        var opp = component.get("v.validationEntity.app");
        var recordId = component.get("v.recordId");
        var state = opp.State__c;
        
        var action = component.get("c.submitToFinwise");
        action.setParams({
            "recordId": recordId,
            "appState": state
        });
        action.setBackground();
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            //console.log(response.getReturnValue());
            if (state === "SUCCESS") {
                var result = response.getReturnValue();   
                    var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Info!",
                                "message": result,
                                "type": "info",
                                "mode": "dismissible"
                            });
                    toastEvent.fire();
                     $A.get('e.force:refreshView').fire();

            }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }else {
                //
            }
            component.set("v.showSpinner", false);
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    //SAVE APPLICATION
    savingApplication : function(component, event, helper) {
        
        component.set("v.showSpinner", true);       
        var action = component.get("c.saveApplication");
        
        action.setParams({ 
            'EntityStr' :  JSON.stringify(component.get("v.validationEntity"))
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if($A.util.isEmpty(result)) {
                	$A.get('e.force:refreshView').fire();
                }else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    	"title": "Warning!",
                        "message": result,
                        "type": "warning",
                        "mode": "dismissible"
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    //CHECK IF DEAL IS READY
    checkingDealIsReady : function(component, event, helper) {
        console.log('Validation Tab Parent | function : checkingDealIsReady ');
        component.set("v.showSpinner", true);       
        var action = component.get("c.checkIftheDealIsReady");
        
        action.setParams({ 
            'EntityStr' :  JSON.stringify(component.get("v.validationEntity"))
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.validationEntity", result);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            component.set("v.showSpinner", false);
        });
        
        $A.enqueueAction(action);
    },
    //RE-GRADE APPLICATION
    reGradeApplication : function(component, event, helper) {
        if(confirm("Are you sure you want to re-grade this application? If so, please remember to confirm we have the correct Income Type option and employment start date details (for Employed applicants) before completing the re-grade.")) {
            component.set("v.showSpinner", true);       
            var action = component.get("c.regradeApp");
            
            action.setParams({ 
                'EntityStr' :  JSON.stringify(component.get("v.validationEntity"))
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    component.set("v.validationEntity", result);
                    console.log('Refresh Validation Screen');
                    $A.get('e.force:refreshView').fire();
                }
                else if (state === "INCOMPLETE") {
                    // do somethin
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                component.set("v.showSpinner", false);
            });
            
            $A.enqueueAction(action);
        }
    },
    //GENERATE A CONTRACT
    genrateAContract : function(component, event, helper) {
        if(confirm('Are you sure you want to fund this application and turn it into a CL Contract right now ?')) {
            component.set("v.showSpinner", true);       
            var action = component.get("c.generateContract");
            
            action.setParams({ 
                'EntityStr' :  JSON.stringify(component.get("v.validationEntity"))
            });
			action.setBackground();            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    component.set("v.validationEntity", result);
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                component.set("v.showSpinner", false);
            });
            
            $A.enqueueAction(action);
        }
    },
    IdologyValidationNote : function(component, event, helper) {
        helper.loadAddNoteModal(component, 'Idology Validation Note');
    },
    FraudValidationNote : function(component, event, helper) {
        helper.loadAddNoteModal(component, 'Fraud Alert Validation Note');
    },
    BankValidationNote : function(component, event, helper) {
        helper.loadAddNoteModal(component, 'Bank Statements Validation Note');
    },
    EmploymentValidationNote : function(component, event, helper) {
        helper.loadAddNoteModal(component, 'Employment Validation Note');
    },
    DocumentsValidationNote : function(component, event, helper) {
        helper.loadAddNoteModal(component, 'Documents Validation Note');
    },
    TotalsValidationNote : function(component, event, helper) {
        helper.loadAddNoteModal(component, 'Totals Validation Note');
    },
    InquiryCurrentAddressFileOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'InqCurrAddOnFile_Overridden__c', 'InqCurrAddOnFile_Overridden_By__c', 'InqCurrAddOnFile_Meet_Tolerance__c');        
    },
    SSNRepMoreFreqforAnotherOverridden : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Is_SSNRepMoreFreqforAnother_Overridden__c', 'SSNRepMoreFreqforAnother_Overridden_By__c', 'Is_SSNRepMoreFreqAnother_Tolerance_Met__c');        
    },
    EWSOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'EWS_Call_Overidden__c', 'EWS_Call_Overridden_By__c', 'EWS_Call_Meets_Tolerance__c');        
    },
    EmploymentVerificationOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Employment_Verification_Overidden__c', 'Employment_Verification_Overridden_By__c', 'Employment_Verification_Meets_Tolerance__c');        
    },
    EmploymentOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Employment_Overidden__c', 'Employment_Overridden_By__c', 'Employment_Time_Meets_Tolerance__c');        
    },
    TotalOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Is_Totals_Overridden__c', 'Totals_Overridden_By__c', 'Estimated_Grand_Total_Meets_Tolerance__c');        
    },
    ManualGradeOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Is_Manual_Grade_Overridden__c', 'Manual_Grade_Overridden_By__c', 'Manual_Grade_Meets_Tolerance__c');        
    },
    ScorecardGradeOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Scorecard_Grade_Overridden__c', 'Scorecard_Grade_Overridden_by__c', 'Is_Scorecard_Grade_Tolerance_Met__c');        
    },
    OfferPaymentAmountOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Offer_Payment_Amount_Overridden__c', 'Offer_Payment_Amount_Overridden_by__c', 'Is_Offer_Payment_Amount_Tolerance_Met__c');        
    },
    AverageDailyBalanceOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Average_Daily_Balance_Overridden__c', 'Average_Daily_Balance_Overridden_by__c', 'Average_Daily_Balance_Meets_Tolerance__c');        
    },
    OverdraftsOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Overdrafts_Overridden__c', 'Overdrafts_Overridden_by__c', 'Is_Overdrafts_Tolerance_Met__c');        
    },
    YTDOverdraftsOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'YTD_Overdrafts_Overridden__c', 'YTD_Overdrafts_Overridden_by__c', 'Is_YTD_Overdrafts_Tolerance_Met__c');        
    },
    DLOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'DL_Overidden__c', 'DL_Overridden_By__c', 'DL_Time_Meets_Tolerance__c');        
    },
    DTIOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'DTI_Overridden__c', 'DTI_Overridden_By__c', 'DTI_Meets_Tolerance__c');        
    },
    PTIOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'PTI_Overridden__c', 'PTI_Overridden_By__c', 'Is_PTI_Tolerance_Met__c');        
    },
    FeeOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Fee_Overridden__c', 'Fee_Overridden_By__c', 'Fee_Meets_Tolerance__c');        
    },
    NSFOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'NSFs_Overridden__c', 'NSFs_Overridden_by__c', 'Is_NSFs_Tolerance_Met__c');        
    },
    YTDNSFOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'YTD_NSFs_Overridden__c', 'YTD_NSFs_Overridden_by__c', 'Is_YTD_NSFs_Tolerance_Met__c');        
    },
    TotDepAmOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Is_Total_Deposits_Amount_Overridden__c', 'Total_Deposits_Amount_Overridden_By__c', 'Is_Total_Deposits_Amount_Tolerance_Met__c');        
    },
    CurBalOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Current_Balance_Overridden__c', 'Current_Balance_Overridden_by__c', 'Current_Balance_Meets_Tolerance__c');        
    },
    OverdraftsPlusNSFOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'OverdraftsPlusNSF_Overridden__c', 'OverdraftsPlusNSF_Overridden_by__c', 'Is_Overdrafts_Plus_NSFs_Tolerance_Met__c');        
    },
    MaximumPaymentOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Is_Maximum_Payment_Overridden__c', 'Maximum_Payment_Overridden_By__c', 'Is_Maximum_Payment_Meets_Tolerance__c');        
    },
    NumNDOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Is_No_Of_Negative_Days_Overridden__c', 'Number_Of_Negative_Days_Overridden_By__c', 'Is_No_Of_Negative_Days_Tolerance_Met__c');        
    },
    SelfEmpOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Is_Self_Employment_Bank_Data_Overridden__c', 'Self_Employment_Bank_Data_Overridden_By__c', '');        
    },
    SignedContractOverride : function(component, event, helper) {
        helper.populateOverriddenBy(component, 'Is_Signed_Contract_Overridden__c', 'Signed_Contract_Overridden_By__c', 'Is_Signed_Contract_Tolerance_Met__c', 'SignedContract_Overidden_Required__c');        
    },
    whyDealIsNotReady : function(component, event, helper) {
        helper.checkWhyDealIsNotReady(component);
    } 
})