({ 
	loadFile : function(component, file) {
    	var reader = new FileReader();
        
        reader.onload = function() {
            var fileContents = reader.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            
            fileContents = fileContents.substring(dataStart);
            
            component.set('v.fileData', fileContents);
            component.set('v.fileObject', file);
            component.set('v.fileName', file.name);
        };
        reader.readAsDataURL(file);
	},

	insertAttachment : function(component, file, fileContents){
   		var action = component.get("c.saveAttachment"); 
		var attachments = component.get("v.oppAttachments");
		
		action.setParams({
		     caseId: component.get("v.caseId"),
		     fileName: file.name,
		     base64Data: encodeURIComponent(fileContents), 
		     contentType: file.type
		});

		action.setCallback(this, $A.getCallback(function(response) {
		    var state = response.getState();		    
            if (state === "SUCCESS") {
    			var result = response.getReturnValue();
    			if(result !== null){
    				this.applyFileTypeIconNames(component, result);
    				attachments.splice(0, 0, result);
    				component.set("v.oppAttachments", attachments);
					this.showToastAlert("Success", "The attachment has been saved.", "success", 1000);
    			}else{
    				this.showToastAlert("Error", "The attachment couldn't be saved.", "error", 1000);
    			}
            }
            component.set('v.showSpinner', false);
		}));
         
		$A.enqueueAction(action); 
		component.set("v.isModalOpen", false);
	},

	showToastAlert : function(alertTitle, alertMsg, alertType, alertTime) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": alertTitle,
            "message": alertMsg,
            "type": alertType,
            "mode": "sticky",
            "duration": alertTime
        });
        toastEvent.fire();
    },

    applyFileTypeIconNames : function(cmp, file ) {

        var iconName = 'doctype:attachment';
        var fileName = file.Name.toLowerCase();

        if (fileName.includes('.pdf')) {
            iconName = 'doctype:pdf';
        }
        else if (fileName.includes('.png') || fileName.includes('.jpg')) {
            iconName = 'doctype:image';
        }
        else if (fileName.includes('.html')) {
            iconName = 'doctype:html';
        }
        else if (fileName.includes('.xlsx')) {
            iconName = 'doctype:excel';
        }
        else if (fileName.includes('.doc') || fileName.includes('.docx')) {
            iconName = 'doctype:word';
        }

        file.FileTypeIconName = iconName;
    },
})