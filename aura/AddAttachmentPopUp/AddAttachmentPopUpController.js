({
   openModel: function(component, event, helper) {
      component.set('v.fileName', 'No File Selected..');
      component.set("v.isModalOpen", true);

   },
  
   closeModel: function(component, event, helper) {
      component.set("v.isModalOpen", false);
   },

   handleFilesChange: function(component, event, helper) {
      let fileInput = component.find("attachmentInput").getElement();
      let file = fileInput.files[0];

      if((file !== null && file !== undefined) && file.size > 0)
         helper.loadFile(component, file);
 },

   handleSave: function(component, event, helper) {
      var file =  component.get('v.fileObject');
      var fileContents = component.get('v.fileData');

      component.set('v.showSpinner', true);
      helper.insertAttachment(component, file, fileContents);
   },
})