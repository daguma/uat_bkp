//Applicants_AllController
({
    onTitleButtonClick: function (component, event, helper) {
        var cmp1 = component.find("parentDiv");
        $A.util.toggleClass(cmp1, "slds-is-open");
    },

    callQuickSaveApp: function (component, event, helper) {
        var compEvent = component.getEvent("ApplicationDetails_SaveEvt");
        compEvent.fire();
    },

    saveDetails: function (component, event, helper) {

        var allAppList = component.get("v.allApplicantsList");

        var arrayLength = allAppList.length;
        var fullList = [];
        for (var i = 0; i < arrayLength; i++) {
            var item = allAppList[i];
            item.approved = item.isApproved === true ? "true" : "false";
            // MAINT-351
            item.received = item.isReceived === true ? "true" : "false";
            item.required = item.isRequired === true ? "true" : "false";
            item.yodleeFlag = item.isYodleeFlag === true ? "true" : "false";

            if( $A.util.isUndefined(item.receivedDateVal) ){
                item.receivedDate = item.receivedDateVal;
            }else{
               if( item.receivedDateVal.length === 10 ){
                    var temp = item.receivedDateVal.replace(/-/g, "/")
                    var dd = temp.substr(temp.lastIndexOf("/"),temp.length);
                    var mm = temp.substr(temp.indexOf("/")+1, temp.lastIndexOf("/"));
                    var mm = mm.substr(0,mm.indexOf("/"));
                    var yyyy = "/" + temp.substr(0, temp.indexOf("/"));
                    item.receivedDate = mm+dd+yyyy;
               }else{
                   delete item["receivedDate"];
                   delete item["receivedDateVal"];
               }
            }

            item.dispositions = item.dispositions === "--None--" ? "" : item.dispositions;
            fullList.push(item);
        }

        return fullList;
    },

    onCompletedByClick : function (component, event) {
        var userName = event.getSource().get("v.label");
        var index = event.getSource().get("v.title");
        var cmp = component.find("completedByInput");
        cmp[index].set("v.value",userName);
    }
})