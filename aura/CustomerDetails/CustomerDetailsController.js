//CustomerDetailsController
({
    init: function (cmp, event, helper) {
        var existingCustomerDetails = cmp.get("v.customerDetails");
        var existingRecordId = cmp.get("v.recordId");
        if( existingCustomerDetails === null && existingRecordId !== null){
            helper.getCustDetails(cmp);
        }
    }
})