({
	processMultiloanResponse : function(component,response) {
        if (!$A.util.isUndefinedOrNull(response) && !$A.util.isEmpty(response)) {
            if(response.hasError){
               
                console.log(response.errorMessage);
            }else{
                if (!$A.util.isUndefinedOrNull(response.isMultiloan) && !$A.util.isEmpty(response.isMultiloan)) {
                    component.set("v.isMultiloan",response.isMultiloan);
                  
                }
            }
        }else{
            component.set("v.isMultiloan",false);
        }
    },
    
    validateFuneralBanner : function(component) {
        var action = component.get('c.getUseOfFunds');
        action.setParams({ "recordId" :  component.get("v.recordId") });
        console.log("action "+action);
        action.setCallback(this, $A.getCallback(function (response) {
            console.log("response=================== "+response);
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.showFuneralBanner", response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error('Error on validateFuneralBanner '+ errors);
            }
        }))
        $A.enqueueAction(action);
    }
})