({
	doInit : function(component, event, helper) {
        // Get server action service
        const server = component.find('server');
        if(component.get('v.isContact') === true){
            var parametersMap = {};
            //parametersMap["contactId"] = component.get("v.recordId");
    
            // Call server-side action with parameters and callback
            server.callServer(
                component.get('c.isMultiloan'), // Server-side action
                {"contactId": component.get("v.recordId")}, // Action parameters
                false, // Disable cache
                $A.getCallback(response => { // Custom success callback
                    // Handle response
                    helper.processMultiloanResponse(component,response);
                }),
                $A.getCallback(errors => { // Custom error callback
                    // Handle errors
                }),
                false, // Disable built-in error notification
                false, // Disable background
                false // Not abortable
            );
            
        }else{
            if(component.get('v.isOpportunity') === true){
                helper.validateFuneralBanner(component);
            }
        }

	}
})