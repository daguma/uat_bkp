({
    handleSave : function(component, event, helper) {
        var isValid = component.find("noteTitle").get('v.validity').valid;
        
        if(isValid) {
            var noteObject = component.get("v.noteObj");
            noteObject.ParentId = component.get("v.recordId");
    		console.log(noteObject);
            var action = component.get("c.saveNote");
            action.setParams({ 
                noteEntity : noteObject
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
    
            $A.enqueueAction(action);
        }
    }
})