//Button_VerifyEmailIpController
({
    openModal: function(component, event, helper) {
        component.set("v.isButtonOpened", true);
    },
    closeModal: function(component, event, helper) {
        component.set("v.isButtonOpened", false);
    }
})