({
	showToastAlert: function(title, message, type, time) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type,
            "mode": "dismissible",
            "duration": time
        });
        toastEvent.fire();
    }
})