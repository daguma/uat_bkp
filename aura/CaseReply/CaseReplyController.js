({
    sendReply : function(component, event, helper) {
        var action = component.get("c.submitReply");
        
        action.setParams({
            "recordId": component.get("v.recordId"),
            "description": component.get("v.descriptionTxt")
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var message = response.getReturnValue();
                if(message.includes("SUCCESS")) {
                    message = message.replace("SUCCESS :", "").trim();
                	helper.showToastAlert("Success!", message, "success", 10000);
                    $A.get('e.force:refreshView').fire();
                    component.set("v.descriptionTxt", "");
                }else {
                    message = message.replace("ERROR :", "").trim();
                    helper.showToastAlert("Error!", message, "error", 10000);
                }
            }else if(state === "ERROR") {
                console.log('Error in fetching values');
            }else {
                console.log('Error in fetching values');
            }
        });
        $A.enqueueAction(action);
    }
})