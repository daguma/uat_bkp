//Button_SaveController
({
    openModel: function(component, event, helper) {
        component.set("v.isButtonOpened", true);
    },
    closeModel: function(component, event, helper) {
        component.set("v.isButtonOpened", false);
    },
    saveAction: function(component, event, helper) {
        var parametersEvent = $A.get("e.c:Event_App_AchSave");
        parametersEvent.fire();
    }
})