({
    recordUpdate : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        var result = component.get("v.simpleRecord");
        var accId = component.get("v.recordId");

        var changeType = event.getParams().changeType;
        
        if (changeType === "LOADED") {
            
            if(result.Source__c != 'EzVerify') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Warning!",
                    "message": "The Source is not valid.",
                    "type": "warning"
                });
                toastEvent.fire();
            }else {
                var urlEvent = $A.get("e.force:navigateToURL");
                
                urlEvent.setParams({
                    "url": '/apex/loop__looplus?eid=' + accId + '&contactId=' + result.contact__c
                });
                urlEvent.fire();
            }
        }
        
    }
})