@isTest public class RefinanceJobTest {
    @isTest static void RefinanceJob_EligibilityCheck_Test(){
        
        Refinance_Settings__c sett = New Refinance_Settings__c();
        sett.Name = 'settings';
        sett.Days_To_First_Check__c = 10;
        sett.Eligibility_Days__c = 20;
        sett.FICO_Approved__c = 585;
        sett.Max_Originated_Loan_Amount_LG__c = 5000;
        insert sett;
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Loan_Status__c='Active - Good Standing';
        update contract;
        
        contract = [SELECT Id, loan__Loan_Status__C, loan__Contact__r.MailingState FROM loan__Loan_Account__c WHERE Id =:contract.Id];
        
        loan_Refinance_Params__c params = new loan_Refinance_Params__c();
        params.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-90);
        params.contract__c = contract.Id;
        params.ManualEligibilityCheck__c = true;
        insert params;
        
        LP_Custom__c lp = [SELECT Allowed_Refinance_States__c FROM LP_Custom__c];
        lp.Allowed_Refinance_States__c = 'AL,AK,AR,AZ,CA,DC,DE,FL,GA,ID,IL,IN,KS,KY,MI,MS,MO,MN,MT,NE,NC,NJ,NM,OH,OK,OR,PA,SD,TN,TX,VA,UT,WA,HI';
        update lp;
         
        RefinanceJob job = new RefinanceJob('EligibilityCheck', 5);
        database.executebatch(job,1);
    }
    
    @isTest static void RefinanceJob_EligibilityCheckGetCR_Test(){
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Loan_Status__c='Active - Good Standing';
        update contract;
        
        contract = [SELECT Id, loan__Loan_Status__C, loan__Contact__r.MailingState, 
                    loan__Contact__c, loan__Principal_Remaining__c, loan__Fees_Remaining__c, 
                    loan__Interest_Accrued_Not_Due__c, Opportunity__c FROM loan__Loan_Account__c 
                    WHERE Id =:contract.Id];
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';

        Decimal loanAmount = (((contract.loan__Principal_Remaining__c +
                                contract.loan__Fees_Remaining__c +
                                contract.loan__Interest_Accrued_Not_Due__c) / 100)
                              .round(System.RoundingMode.CEILING)) * 100;
        newApp.Amount = loanAmount > 3500 ? loanAmount : 3500;
        update newApp;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'A1';
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;
        
        loan_Refinance_Params__c params = new loan_Refinance_Params__c();
        params.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-90);
        params.contract__c = contract.Id;
        params.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id;
        params.Eligible_For_Soft_Pull__c = true;
        insert params;

        RefinanceJob job = new RefinanceJob('EligibilityCheckGetCR', 0);
        database.executebatch(job,1);
    }
}