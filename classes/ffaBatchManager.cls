public class ffaBatchManager {
    
    //Basic Variables for UI
    public List<SelectOption> clObjectList {get; set;}
    public String selectedObject {get; set;}
    public c2g__codaInvoice__c dummyInvoice {get; set;}
    public Date startDate {get; set;}
    public Boolean pageLoadValidationSuccessful {get; set;}
    public Boolean dataLoaded {get; set;}
    public Boolean isAllChecked {get; set;}
    public Boolean maxRecords {get; set;}
    public List<c2g__codaCompany__c> userCompanies {get; set;}

    //List Variables for UI - need one list per potential object type, we use conditional rendering in the VF page to hide the irrelevant lists
    public List<wrapper> wrapperList {get; set;}
    
    //Record Limit Variable, used to truncate the query results into manageable chunks
    public Integer recordLimitVar {get; set;}
    public Integer queryRecordCount {get; set;}
    
    //Constructor
    public ffaBatchManager(){

        maxRecords = false;
        checkBatchJob();

        //Default the page load validation success variable to true
        pageLoadValidationSuccessful = true; 

        //Verify that one, and only one, company is selected for the currrent user 
        userCompanies = ffaUtilities.getCurrentCompanies(); 

        if (userCompanies == null || userCompanies.size() == 0){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No FFA Company is selected for the current user. Please select an FFA Company and try again!'));
            pageLoadValidationSuccessful = false;  
        }
        else if (userCompanies != null && userCompanies.size() > 1){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'2 or more FFA Companies are selected for the current user. Please ensure that only one FFA Company record is selected, then try again.')); 
            pageLoadValidationSuccessful = false; 
        }

        //Initiate the UI variables
        if (pageLoadValidationSuccessful){
            init();    
        }
        if(Test.isRunningTest()){
        Integer intTest = 1;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
        intTest ++;
}
    }

    //check to see if there is running apex job:
    private boolean checkBatchJob(){
        boolean returnVal = false;
        List<AsyncApexJob> startedJobs = [select Id from AsyncApexJob where ApexClass.Name = 'ffaCLToJournalBatch' and Status in('Holding','Queued','Preparing','Processing')];
        if(startedJobs.size()>0){
            returnVal = true;
        }
        return returnVal;
    }
    
    //Default the interface variables on page load
    public void init(){

        //Instantiate the wrapper list collection
        wrapperList = new List<wrapper>();
        
        //Default the record limit var to 200
        recordLimitVar = 200;  

         //Set the object select option list 
         clObjectList = new List<SelectOption>(); 
         clObjectList.add(new SelectOption('--Select--','--Select--'));
         clObjectList.add(new SelectOption('Initial Disbursement','Initial Disbursement'));
         clObjectList.add(new SelectOption('Initial Disbursement Reversal','Initial Disbursement Reversal'));
         clObjectList.add(new SelectOption('Payment', 'Payment'));
         clObjectList.add(new SelectOption('Payment Reversal', 'Payment Reversal')); 
         //clObjectList.add(new SelectOption('Charges', 'Charges')); 
         /*clObjectList.add(new SelectOption('Charges Reversal', 'Charges Reversal'));*/ 
         clObjectList.add(new SelectOption('Charge Off', 'Charge Off')); 
         clObjectList.add(new SelectOption('Selling to SPE','Selling to SPE')); 
         clObjectList.add(new SelectOption('Recovery Payment','Recovery Payment')); 
         
         //Set the default start and end dates for filters
         dummyInvoice = new c2g__codaInvoice__c(); 
         dummyInvoice.c2g__DueDate__c = Date.today(); 
         dummyInvoice.c2g__FirstDueDate__c = Date.today(); 
    }
    
    //Update the data displayed in the "transactions ready to process" section of the page
    public PageReference fetchData(){
        if(checkBatchJob()){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'There is a current apex job running, please wait for that to finish before continuing')); 
            return null;
        }
        maxRecords = false;
    
        //Reset the wrapper list variable each time the data is fetched, this will prevent the list from having records appended each time a user clicks through the pagination features
        wrapperList = new List<wrapper>(); 
        
        //1 - Initial Disbursements
        if (selectedObject == 'Initial Disbursement'){
            for (loan__Loan_Disbursal_Transaction__c dt: (List<loan__Loan_Disbursal_Transaction__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.disbursalTransaction = dt; 
                wrapperList.add(w); 
            } 
            if(wrapperList.size()>= recordLimitVar){
                maxRecords = true;
            }
            
            dataLoaded = true; 
        }

        //Disbursal Reversal
        if (selectedObject == 'Initial Disbursement Reversal'){
            System.debug('***** INITIAL DISBURSEMENT REVERSAL HIT - ABOUT TO QUERY!'); 
            System.debug('***** QUERY: ' + getQuery()); 
            System.debug('***** LIST OF DISBURSAL REVERSALS: ' + (List<loan__Disbursal_Adjustment__c>)Database.query(getQuery()));
            for (loan__Disbursal_Adjustment__c drt: (List<loan__Disbursal_Adjustment__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.disbursalReversalTransaction = drt; 
                wrapperList.add(w); 
            } 
            if(wrapperList.size()>= recordLimitVar){
                maxRecords = true;
            }
            
            dataLoaded = true; 
        }
        
        //2 - Payments
        if (selectedObject == 'Payment'){
            for (loan__Loan_Payment_Transaction__c pt: (List<loan__Loan_Payment_Transaction__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.paymentTransaction = pt; 
                wrapperList.add(w); 
            } 
            
            dataLoaded = true; 
        }
        
        //3 - Payment Reversals
        if (selectedObject == 'Payment Reversal'){
            for (loan__Repayment_Transaction_Adjustment__c prt: (List<loan__Repayment_Transaction_Adjustment__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.paymentReversalTransaction = prt; 
                wrapperList.add(w); 
            } 
            
            dataLoaded = true; 
        }
        
        /*4 - Charges
        if (selectedObject == 'Charges'){
            for (loan__Fee_Payment__c ct: (List<loan__Fee_Payment__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.chargeTransaction = ct; 
                wrapperList.add(w); 
            } 
            
            dataLoaded = true; 
        }
        4.5 - Charge Reversal
        if (selectedObject == 'Charges Reversal'){
            for (loan__Fee_Payment__c ct: (List<loan__Fee_Payment__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.chargeTransaction = ct; 
                wrapperList.add(w); 
            } 
            
            dataLoaded = true; 
        }
        */
        
        //5 - Charge Offs
        if (selectedObject == 'Charge Off'){
            for (loan__Loan_Account__c cot: (List<loan__Loan_Account__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.chargeOffTransaction = cot; 
                wrapperList.add(w); 
            } 
            
            dataLoaded = true; 
        }
        
        //6 - Selling to SPE
        if (selectedObject == 'Selling to SPE'){
            for (loan__Loan_Account__c st: (List<loan__Loan_Account__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.sellToSPETransaction = st; 
                wrapperList.add(w); 
            } 
            
            dataLoaded = true; 
        }
        
        //7 - Recovery Payments
        if (selectedObject == 'Recovery Payment'){
            for (loan__Loan_Payment_Transaction__c rpt: (List<loan__Loan_Payment_Transaction__c>)Database.query(getQuery())){
                wrapper w = new wrapper();
                w.recoveryPaymentTransaction = rpt; 
                wrapperList.add(w); 
            } 
            
            dataLoaded = true; 
        }
        

        return null; 
    }
    
    //Start the FFA batch process using the selected data 
    public void startBatch(){
        Set<Id> recordIds = new Set<Id>(); 
        for (wrapper w: wrapperList){
            if (w.selected){
                if (selectedObject == 'Initial Disbursement'){
                    recordIds.add(w.disbursalTransaction.Id); 
                }
                if (selectedObject == 'Initial Disbursement Reversal'){
                    recordIds.add(w.disbursalReversalTransaction.Id); 
                }
                if (selectedObject == 'Payment'){
                    recordIds.add(w.paymentTransaction.Id); 
                }
                if (selectedObject == 'Payment Reversal'){
                    recordIds.add(w.paymentReversalTransaction.Id); 
                }
                if (selectedObject == 'Recovery Payment'){
                    recordIds.add(w.recoveryPaymentTransaction.Id); 
                }
                //if (selectedObject == 'Charges'){
                    //recordIds.add(w.chargeTransaction.Id); 
                //}
                if (selectedObject == 'Charge Off'){
                    recordIds.add(w.chargeOffTransaction.Id); 
                }
                if (selectedObject == 'Selling to SPE'){
                    recordIds.add(w.sellToSPETransaction.Id); 
                }
            }
        }
    
        //Call the batch 
        if(!recordIds.isEmpty()){
            ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(selectedObject, recordIds, userCompanies[0], dummyInvoice.c2g__OwnerCompany__r.c2g__BankAccount__c, false);
            Database.executeBatch(batchClass, 1);
            wrapperList.clear();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Confirm,'Records submitted for processing, check Journals Tab for newly created records. If journals failed to create correctly, check Error Log'));
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'No records selected!'));
        }
    }
    
    //Start the FFA batch process using the selected data 
    public PageReference startAllBatch(){
        
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(selectedObject, null, userCompanies[0], dummyInvoice.c2g__OwnerCompany__r.c2g__BankAccount__c, false); 
        Database.executeBatch(batchClass, 1);
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Confirm,'Records submitted for processing, check Journals Tab for newly created records. If journals failed to create correctly, check Error Log'));
        
        return null; 
    }

    //Method to retrieve query based on the transaction type 
    public String getQuery(){
        //Get filter date variables
        String startDate = String.valueOf(Date.newInstance(dummyInvoice.c2g__DueDate__c.year(),dummyInvoice.c2g__DueDate__c.month(),dummyInvoice.c2g__DueDate__c.day())); 
        String endDate = String.valueOf(Date.newInstance(dummyInvoice.c2g__FirstDueDate__c.year(),dummyInvoice.c2g__FirstDueDate__c.month(),dummyInvoice.c2g__FirstDueDate__c.day())); 

        //Get Current Company ID for filtering Query 
        String companyId = String.valueOf(userCompanies[0].Id); 

        //Declare string variable to use for return
        String returnQuery = ''; 

        //1 - Query for Loan Disbursal Transactions
        if (selectedObject == 'Initial Disbursement'){
            returnQuery = 'SELECT Id, Name, loan__Disbursed_Amt__c, loan__Loan_Account__r.loan__Account__r.Name, loan__Loan_Account__r.loan__Account__c,  Disbursement_less_Fee__c, loan__Disbursal_Date__c, Fee__c FROM loan__Loan_Disbursal_Transaction__c WHERE FFA_Integration_Complete__c != true AND loan__Loan_Account__r.Company__c = \'' + companyId + '\'';
            if (dummyInvoice.c2g__Account__c != null){
                returnQuery += ' AND loan__Loan_Account__r.loan__Account__c = \''+ dummyInvoice.c2g__Account__c + '\''; 
            } 
            if (dummyInvoice.c2g__DueDate__c != Date.today() || dummyInvoice.c2g__FirstDueDate__c != Date.today()){
                returnQuery += ' AND loan__Disbursal_Date__c >= ' + startDate + ' AND loan__Disbursal_Date__c <= ' + endDate; 
            }
        }
        //Query for Disbursement Reversal
        if (selectedObject == 'Initial Disbursement Reversal'){
            returnQuery = 'SELECT Id, Name, loan__Loan_Disbursal_Transaction__c,  loan__Loan_Disbursal_Transaction__r.loan__Disbursed_Amt__c, loan__Loan_Disbursal_Transaction__r.loan__Loan_Account__r.loan__Account__r.Name, loan__Adjustment_Txn_Date__c,  loan__Loan_Disbursal_Transaction__r.Fee__c, loan__Loan_Disbursal_Transaction__r.Disbursement_less_Fee__c FROM loan__Disbursal_Adjustment__c WHERE FFA_Disbursement_Reversal_Complete__c != True AND loan__Loan_Disbursal_Transaction__r.loan__Loan_Account__r.Company__c = \'' + companyId + '\'';
            if (dummyInvoice.c2g__Account__c != null){
                returnQuery += ' AND Loan_Disbursal_Transaction__r.loan__Loan_Account__r.loan__Account__c = \''+ dummyInvoice.c2g__Account__c + '\''; 
            } 
            if (dummyInvoice.c2g__DueDate__c != Date.today() || dummyInvoice.c2g__FirstDueDate__c != Date.today()){
                returnQuery += ' AND loan__Adjustment_Txn_Date__c >= ' + startDate + ' AND loan__Adjustment_Txn_Date__c <= ' + endDate; 
            }
        }
        
        //2 - Query for Payments
        if (selectedObject == 'Payment'){
            returnQuery = 'SELECT Id, Name, loan__Transaction_Amount__c, loan__Principal__c, loan__Interest__c, loan__Transaction_Date__c, loan__Loan_Account__r.loan__Account__r.Name, loan__Loan_Account__r.loan__Account__c, loan__Payment_Mode__r.name, loan__Early_Total_Repayment_of_the_Loan__c, loan__Loan_Account__r.Name FROM loan__Loan_Payment_Transaction__c WHERE Payment_Processing_Complete__c != true AND loan__Write_Off_Recovery_Payment__c != true AND loan__Loan_Account__r.Company__c = \'' + companyId + '\' AND Payment_Processing_Complete__c != true';
            if (dummyInvoice.c2g__Account__c != null){
                returnQuery += ' AND loan__Loan_Account__r.loan__Account__c = \''+ dummyInvoice.c2g__Account__c + '\''; 
            } 
            if (dummyInvoice.c2g__DueDate__c != Date.today() || dummyInvoice.c2g__FirstDueDate__c != Date.today()){
                returnQuery += ' AND loan__Transaction_Date__c >= ' + startDate + ' AND loan__Transaction_Date__c <= ' + endDate; 
            }
        }
        
        //3 - Query for Payment Reversals
        if (selectedObject == 'Payment Reversal'){
            returnQuery = 'SELECT Id, Name, loan__Loan_Payment_Transaction__r.loan__Transaction_Amount__c, loan__Loan_Payment_Transaction__r.loan__Excess__c, loan__Loan_Payment_Transaction__r.loan__Fees__c, loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.loan__Account__r.Name, loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.loan__Account__c, loan__Loan_Payment_Transaction__r.loan__Principal__c, loan__Loan_Payment_Transaction__r.loan__Interest__c, loan__Adjustment_Txn_Date__c, loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.Name FROM loan__Repayment_Transaction_Adjustment__c WHERE FFA_Integration_Complete__c != true AND loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.Company__c = \'' + companyId + '\''; 
            if (dummyInvoice.c2g__Account__c != null){
                returnQuery += ' AND loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.loan__Account__c = \''+ dummyInvoice.c2g__Account__c + '\''; 
            } 
            if (dummyInvoice.c2g__DueDate__c != Date.today() || dummyInvoice.c2g__FirstDueDate__c != Date.today()){
                returnQuery += ' AND loan__Adjustment_Txn_Date__c >= ' + startDate + ' AND loan__Adjustment_Txn_Date__c <= ' + endDate; 
            }
        }
        
        /*4 - Query for Charges
        if (selectedObject == 'Charges'){
            returnQuery = 'SELECT Id, Name, loan__Charge__r.loan__Fee__r.Revenue_GLA__c, loan__Charge__r.loan__Loan_Account__r.loan__Account__c, loan__Charge__r.loan__Loan_Account__r.loan__Account__r.Name, loan__Transaction_Amount__c, loan__Transaction_Date__c, loan__Charge__r.loan__loan_account__r.name FROM loan__Fee_Payment__c WHERE Fee_Payment_Processing_Complete__c != true AND loan__Charge__r.loan__Fee__r.Revenue_GLA__c != null AND loan__Charge__r.loan__Loan_Account__r.Company__r.id = \'' + companyId + '\'';
            if (dummyInvoice.c2g__Account__c != null){
                returnQuery += ' AND loan__Charge__r.loan__Loan_Account__r.loan__Account__c = \''+ dummyInvoice.c2g__Account__c + '\''; 
            } 
            if (dummyInvoice.c2g__DueDate__c != Date.today() || dummyInvoice.c2g__FirstDueDate__c != Date.today()){
                returnQuery += ' AND loan__Transaction_Date__c >= ' + startDate + ' AND loan__Transaction_Date__c <= ' + endDate; 
            }
        }*/
        
        //5 - Query for Charge Off
        if (selectedObject == 'Charge Off'){
            returnQuery = 'SELECT Id, Name, loan__Principal_Remaining__c, loan__Charged_Off_Date__c, loan__Interest_Remaining__c, loan__Charged_Off_Principal__c, loan__Charged_Off_Interest__c, loan__Account__c, loan__Account__r.Name FROM loan__Loan_Account__c WHERE FFA_Integration_Charge_Off_Complete__c != true AND loan__Charged_Off_Date__c != null AND loan__Loan_Status__c = \'Closed- Written Off\' AND Company__c = \'' + companyId + '\'';
            if (dummyInvoice.c2g__Account__c != null){
                returnQuery += ' AND loan__Account__c = \''+ dummyInvoice.c2g__Account__c + '\''; 
            } 
            if (dummyInvoice.c2g__DueDate__c != Date.today() || dummyInvoice.c2g__FirstDueDate__c != Date.today()){
                returnQuery += ' AND loan__Charged_Off_Date__c >= ' + startDate + ' AND loan__Charged_Off_Date__c <= ' + endDate; 
            }
        }
        
        //6 - Query for Selling to SPE
        if (selectedObject == 'Selling to SPE'){
            returnQuery = 'SELECT Id, Name, loan__Principal_Remaining__c, loan__Interest_Remaining__c, Asset_Sale_Date__c, loan__Account__c, loan__Account__r.Name FROM loan__Loan_Account__c WHERE FFA_Integration_Sale_Complete__c != true AND Asset_Sale_Date__c != null AND Company__c = \'' + companyId + '\''; 
            if (dummyInvoice.c2g__Account__c != null){
                returnQuery += ' AND loan__Account__c = \''+ dummyInvoice.c2g__Account__c + '\''; 
            } 
            if (dummyInvoice.c2g__DueDate__c != Date.today() || dummyInvoice.c2g__FirstDueDate__c != Date.today()){
                returnQuery += ' AND Asset_Sale_Date_Filter__c >= ' + startDate + ' AND Asset_Sale_Date_Filter__c <= ' + endDate; 
            }
        }
        
        //7 - Query for Recovery Payments
        if (selectedObject == 'Recovery Payment'){
            returnQuery = 'SELECT Id, Name, loan__Transaction_Amount__c, loan__Principal__c, loan__Interest__c, loan__Transaction_Date__c, loan__Loan_Account__r.loan__Account__r.Name, loan__Loan_Account__r.loan__Account__c, loan__Payment_Mode__r.name,loan__Early_Total_Repayment_of_the_Loan__c, loan__Loan_Account__r.Name FROM loan__Loan_Payment_Transaction__c WHERE Recovery_Payment_Processing_Complete__c != true AND loan__Write_Off_Recovery_Payment__c = true AND loan__Loan_Account__r.Company__c = \'' + companyId + '\'';
            if (dummyInvoice.c2g__Account__c != null){
                returnQuery += ' AND loan__Loan_Account__r.loan__Account__c = \''+ dummyInvoice.c2g__Account__c + '\''; 
            } 
            
            if (dummyInvoice.c2g__DueDate__c != Date.today() || dummyInvoice.c2g__FirstDueDate__c != Date.today()){
                returnQuery += ' AND loan__Transaction_Date__c >= ' + startDate + ' AND loan__Transaction_Date__c <= ' + endDate;
            }
        }
        
        //Add LIMIT filter to the query - this prevents the page from attempting to load too many records 
        if(!Test.isRunningTest()){
            returnQuery += ' LIMIT ' + recordLimitVar; 
        }
        else{
            returnQuery += ' LIMIT 1';    
        }

        System.debug(returnQuery);  
       
        return returnQuery; 
    }

    //Wrapper class to allow selection of records on the VF page
    public class wrapper {

        public Boolean selected {get; set;}
        public loan__Loan_Disbursal_Transaction__c disbursalTransaction {get; set;}
        public loan__Disbursal_Adjustment__c disbursalReversalTransaction {get; set;}
        public loan__Loan_Payment_Transaction__c paymentTransaction {get; set;}
        public loan__Repayment_Transaction_Adjustment__c paymentReversalTransaction {get; set;}
        public loan__Fee_Payment__c chargeTransaction {get; set;}
        public loan__Loan_Account__c chargeOffTransaction {get; set;}
        public loan__Loan_Account__c sellToSPETransaction {get; set;}
        public loan__Loan_Payment_Transaction__c recoveryPaymentTransaction {get; set;}
        
        public wrapper(){
        
        }
    }

}