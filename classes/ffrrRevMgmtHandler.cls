/****************************************************************************************
Name            : ffrrRevMgmtHandler
Author          : CLD
Created Date    : 3/7/17
Description     : Helper class to set Revenue Template(s) in a generic fashion.
*****************************************************************************************/

public with sharing class ffrrRevMgmtHandler 
{
	//Given a set of SObject records, set the appropriate Revenue Recognition (and Forecasting) template value(s) on the records
	public static void setRevRecTemplates(List<SObject> records)
	{
		//First grab all possible template mapping records 
		List<ffrr__TemplateMapping__c> allMapRecords = [SELECT ffrr__ObjectName__c, ffrr__Level__c, 
														ffrr__Template__c, ffrr__CriteriaField__c, 
														ffrr__CriteriaValue__c, ffrftemplate__c
														FROM ffrr__TemplateMapping__c
														WHERE ffrr__Template__c != null
														AND ffrr__CriteriaValue__c != null
														AND ffrr__CriteriaField__c != null
														AND ffrr__ObjectName__c != null];

		//Build a map of of object API names to list of template mapping records. We'll reference this map when iterating through the SObjects to update 
		Map<String, List<ffrr__TemplateMapping__c>> templateMap = new Map<String, List<ffrr__TemplateMapping__c>>(); 
		for (ffrr__TemplateMapping__c tm: allMapRecords)
		{
			if (!templateMap.containsKey(tm.ffrr__ObjectName__c))
			{
				templateMap.put(tm.ffrr__ObjectName__c, new List<ffrr__TemplateMapping__c>()); 
			}
			templateMap.get(tm.ffrr__ObjectName__c).add(tm); 
		}
		System.debug('templateMap: ' + templateMap); 

		//Now that we have a map that can be accessed using the SObject name, iterate over each passed-in object and determine which mapping record applies
		for (SObject so: records)
		{
			String objectName = so.getSObjectType().getDescribe().getName();
			for (String key: templateMap.keySet())
			{
				//Iterate over each template, determine which is appropriate based on the criteria field and criteria value 
				for (ffrr__TemplateMapping__c t: templateMap.get(key))
				{
					System.debug('template criteria field: ' + t.ffrr__CriteriaField__c); 
					System.debug('template criteria value: ' + t.ffrr__CriteriaValue__c); 

					String sObjCriteriaValue = (String)so.get(t.ffrr__CriteriaField__c); 

					System.debug('sObjCriteriaValue: ' + sObjCriteriaValue); 

					if ((t.ffrr__ObjectName__c == objectName && t.ffrr__CriteriaValue__c == sObjCriteriaValue) || (t.ffrr__ObjectName__c == objectName && t.ffrr__CriteriaValue__c == null && t.ffrr__CriteriaField__c == null))
					{
						so.put(ffrr__RevenueRecognitionSettings__c.getOrgDefaults().ffrr__ActualsTemplate__c, t.ffrr__Template__c);
						so.put(ffrr__RevenueRecognitionSettings__c.getOrgDefaults().ffrr__ForecastingTemplate__c, t.ffrftemplate__c);  
					}
				}
			}  
		}
	}
}