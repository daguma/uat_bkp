@istest
public class ProcessFailedTransactionsJobTest {
    
    static testmethod void InsertLoggingsTestMethod() {
        
        //INSERT PAYMENT MODE
        loan__Payment_Mode__c lpm = new loan__Payment_Mode__c();
        lpm.Name = 'Debit Card';
        insert lpm;
        
        //INSERT CONTRACT
        loan__Loan_Account__c loanAcc = LibraryTest.createContractTH();

        List<Logging__c> loggingList = new List<Logging__c>();
        
        //INSERT LOGGING's  (FAILED TRANSACTIONS)
        for(Integer i = 0; i < 10; i++) {
            Decimal transactionAmount = 0.010 + i;
            String dateVal = math.mod(i, 2) == 0 ? String.valueOf(System.today()) : String.valueOf(System.today().addDays(5));
            
            Logging__c log = new Logging__c();
            log.Webservice__c = 'CustomerPortalWSDLUtil.addPayment Error';
            log.API_Request__c = '{ "attributes" : { "type" : "loan__Loan_Payment_Transaction__c" },' +
                				 '"loan__Loan_Account__c" : "' + loanAcc.Id + '", "loan__Transaction_Amount__c" : ' + 
                				 transactionAmount + ', "loan__Payment_Mode__c" : "' + lpm.Id + '",' +  
                				 '"loan__Receipt_Date__c" : "' + dateVal + '", ' + 
                				 '"loan__Transaction_Date__c" : "' + dateVal + '" }';
            log.CL_Contract__c = loanAcc.Id;
            loggingList.add(log);
        }
        
        insert loggingList;
        
        //VERIFY LOGGING LIST
        Integer loggingCount = [SELECT count() FROM Logging__c];
        System.assertEquals(10, loggingCount);
    }
    
    static testmethod void batchExecutionTesting() {
        InsertLoggingsTestMethod();        
        ProcessFailedTransactionsJob failedTransactionBatch = new ProcessFailedTransactionsJob();
        Id batchId = Database.executeBatch(failedTransactionBatch, 200);               
    }
    
    static testmethod void batchScheduledExecutionTesting() {
        ProcessFailedTransactionsJob failedTransactionScheduledBatch = new ProcessFailedTransactionsJob();
        SchedulableContext SC;
        failedTransactionScheduledBatch.execute(SC);                
    }
}