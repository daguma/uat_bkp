public with sharing class ATTUSNewLookupCtrlContact {
public class MyException extends Exception{}
public string errMsg {get;set;}
public Boolean isError {get;set;}
public boolean autolookup {get;set;}
private final ATTUSWDSFLookup__c tmpData;
private final Contact cont;
private boolean isTest = false;
private boolean isErrorTest = false;
private list<ATTUSWDSFLookup__c> rslt;
      public ATTUSNewLookupCtrlContact(ApexPages.StandardController stdController) 
      {
          tmpData= (ATTUSWDSFLookup__c) stdController.getRecord();
          
          cont = [select id, FirstName, LastName, mailingstreet, mailingcity, mailingstate, mailingpostalcode, mailingcountry,
          otherstreet, othercity, otherstate, otherpostalcode, othercountry
          from Contact where id = :tmpData.Contact__c];

           autolookup = true;     
           errMsg = '';
           isError = false;
           rslt = new list<ATTUSWDSFLookup__c>();
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please wait while the ATTUS Watchlist Check is performed.....');
           ApexPages.addMessage(myMsg);           
      }
      
      private boolean addressMatch(Contact cont)
      {
          boolean retVal = true; //default to matching
          if(cont.mailingstreet != cont.otherstreet)
          {
              retval = false;
          }
          if(cont.mailingcity != cont.othercity)
          {
              retval = false;
          }
          if(cont.mailingstate != cont.otherstate)
          {
              retval = false;
          }
          if(cont.mailingpostalcode != cont.otherpostalcode)
          {
              retval = false;
          }
          if(cont.mailingcountry != cont.othercountry)
          {
              retval = false;
          }
          
          return retVal;
      }

      public ATTUSNewLookupCtrlContact(ApexPages.StandardController stdController, boolean testRun, boolean testErrorRun) 
      {
          //getconfig();
          tmpData= (ATTUSWDSFLookup__c) stdController.getRecord();
          
          cont = [select id, FirstName, LastName, mailingstreet, mailingcity, mailingstate, mailingpostalcode, mailingcountry,
          otherstreet, othercity, otherstate, otherpostalcode, othercountry
          from Contact where id = :tmpData.Contact__c];
           autolookup = true;     
           errMsg = '';
           isError = false;
           isTest = testRun;
           isErrorTest = testErrorRun;
           rslt = new list<ATTUSWDSFLookup__c>();
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please wait while the ATTUS Watchlist Check is performed.....');
           ApexPages.addMessage(myMsg);
      }


      public pageReference checkList()
      {
      PageReference pageRef = null;
      string results = '';
      boolean gotMailing = false;
      boolean gotOther = false;
      boolean sentOTher = false;
      httpRequest req;
      autolookup = false;
      try
      {
        ATTUSWatchDOGSFLookup lu = null;
        
        //setup address parameters
        if(cont.mailingstreet != null || cont.mailingcity != null || cont.mailingstate != null || cont.mailingpostalcode != null || cont.mailingcountry != null)
        {
            gotMailing = true;
        }
        if(cont.otherstreet != null || cont.othercity != null || cont.otherstate != null || cont.otherpostalcode != null || cont.othercountry != null)
        {
            gotOther = true;
        }
        
        //if(cont.mailingstreet != null || cont.mailingcity != null || cont.mailingstate != null || cont.mailingpostalcode != null || cont.mailingcountry != null)
        //{
        lu = new ATTUSWatchDOGSFLookup();
        if(gotMailing == true)
        {
        if(cont.mailingstreet != null )
            {
            if(cont.mailingstreet.length() <= 100)
                {
                lu.street = cont.mailingstreet;
                }
            else
                {
                lu.street = cont.mailingstreet.substring(0,100);
                }
            }
        if(cont.mailingcity != null )
            {
            lu.city = cont.mailingcity;
            }
        if(cont.mailingstate != null )
            {
            lu.state = cont.mailingstate;
            }
        if(cont.mailingpostalcode != null && (cont.mailingcountry != null && cont.mailingcountry != '')) //if country is blank, do not send zip code
            {
            //if us country, send only left 5 of zip
            if(cont.mailingcountry == 'US' || cont.mailingcountry == 'USA' || cont.mailingcountry == 'U.S.' || cont.mailingcountry == 'U.S.A.' || cont.mailingcountry.contains('United States'))
                {
                if(cont.mailingpostalcode.length() <= 5)
                    {
                    lu.postalcode = cont.mailingpostalcode;
                    }
                else
                    {
                    lu.postalcode = cont.mailingpostalcode.substring(0,5);
                    }
                }
            else
                {
                lu.postalcode = cont.mailingpostalcode;
                }
            }
        if(cont.mailingcountry != null )
            {
            lu.country = cont.mailingcountry;
            }
        }
        else if(gotMailing == false && gotOther == true)
        {
        if(cont.otherstreet != null )
            {
            if(cont.otherstreet.length() <= 100)
                {
                lu.street = cont.otherstreet;
                }
            else
                {
                lu.street = cont.otherstreet.substring(0,100);
                }
            }
        if(cont.othercity != null )
            {
            lu.city = cont.othercity;
            }
        if(cont.otherstate != null )
            {
            lu.state = cont.otherstate;
            }
        if(cont.otherpostalcode != null && (cont.othercountry != null && cont.othercountry != '')) //if country is blank, do not send zip code
            {
            //if us country, send only left 5 of zip
            if(cont.othercountry == 'US' || cont.othercountry == 'USA' || cont.othercountry == 'U.S.' || cont.othercountry == 'U.S.A.' || cont.othercountry.contains('United States'))
                {
                if(cont.otherpostalcode.length() <= 5)
                    {
                    lu.postalcode = cont.otherpostalcode;
                    }
                else
                    {
                    lu.postalcode = cont.otherpostalcode.substring(0,5);
                    }
                }
            else
                {
                lu.postalcode = cont.otherpostalcode;
                }
            }
        if(cont.othercountry != null )
            {
            lu.country = cont.othercountry;
            }
        sentOther = true;
        }
        
        req = lu.createRequestInd(cont.FirstName, cont.LastName);
        if(isTest == false)
            {
            results = lu.checkList(req);
            }
         else
             {
                 if(isErrorTest == false)
                 {
                 results = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ATTUSWATCHDOGRESPONSE tranid="317302009" trandate="2011-04-01 08:15:21"><BILLINGCODE partnerid="0f066555-caee-439f-88d7-7ce72767592b " billingid="0f066555-caee-439f-88d7-7ce72767592b" clientdefined1="SF Testing" clientdefined2="" clientdefined3="" /><LISTS><LIST code="OFAC" /></LISTS><QUERY><ITEM operator="" threshold=".85" surname="bin Ladin" givenname="Usama" unparsedname="" othername="" allentities="" id="" state="" city="" postalcode="" country="" blockedcountryname="" /></QUERY><SEARCHCRITERIA text="search criteria text"><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-03-31 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-03-31" /><ENTITIES><ENTITY entityid="755" entitynum="6365" thisentitysmaxscore="1.000"><LASTNAME>BIN LADIN</LASTNAME><FIRSTNAME>Usama bin Muhammad bin Awad</FIRSTNAME><OTHERNAME /><WHOLENAME /><ISALIASHIT>1</ISALIASHIT><SCORE>1.000</SCORE><TYPE>individual</TYPE><PROGRAMS>SDGT(Specially Designated Global Terrorist); SDT(Specially Designated Terrorist)</PROGRAMS><REMARKS>Modified On or Before: 10/26/2001; DOB 30 Jul 1957; alt. DOB 1958; POB Jeddah, Saudi Arabia; alt. POB Yemen.</REMARKS><ALIASES><ALIAS name="aka, BIN LADEN, Osama" type="individual" score="0.865" /><ALIAS name="aka, BIN LADEN, Usama" type="individual" score="0.942" /><ALIAS name="aka, BIN LADIN, Osama" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Osama bin Muhammad bin Awad" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Usama" type="individual" score="1.000" /></ALIASES><ADDRESSES /></ENTITY></ENTITIES></SEARCHCRITERIA></ATTUSWATCHDOGRESPONSE></soap:Body></soap:Envelope>';
                 }
                 else
                 {
                 results = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><soap:Fault><faultcode>soap:Client</faultcode><faultstring>Server did not recognize the value of HTTP Header SOAPAction: ProcessNameListMessage_An.</faultstring><detail /></soap:Fault></soap:Body></soap:Envelope>';
                 }
             }
        lu.parseDOC(results);
        if(lu.retErrMsg.size() > 0)
        {
            string errMsgs = '';
              for(string s: lu.retErrMsg)
              {
               if(s<> '')
                 {
                     if(s.contains('ERR-67001') || s.contains('ERR-67007') || s.contains('ERR-67008')  || s.contains('ERR-67010')  || s.contains('ERR-67012') || s.contains('ERR-67013') || s.contains('ERR-67014') || s.contains('ERR-67021') || s.contains('ERR-67023') || s.contains('ERR-67024') || s.contains('ERR-67301'))
                     {
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'The watch list check could not be completed – ATTUS Error:  Client is unauthorized.  Please have your Salesforce administrator contact ATTUS Support at 888-494-8449 option 3.');
                     ApexPages.addMessage(myMsg);
                     }
                     else
                     {
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,s);
                     ApexPages.addMessage(myMsg);
                     }
                 }
              }
              isError = true;
              errMsg = errMsg + ' : ' + errMsgs;   
        }
        
        //create Lookupobject and save to database.
        
        for (Integer x = 0; x < lu.retXmlData.size() ; x++ ) 
        {
            ATTUSWDSFLookup__c layout = new ATTUSWDSFLookup__c();
            layout.Contact__c = cont.id;
            layout.List_Date__c = Date.valueOf(lu.retListDate[x]);
            layout.Number_of_Hits__c = lu.retHitCount[x];
            layout.List_Description__c = lu.retListName[x];
            layout.Result_Node__c = lu.retXmlData[x];
            layout.name_searched__c = cont.firstname + ' ' + cont.lastname;
            if(gotMailing == true)
            {
            layout.address_type__c = 'Mailing';
            if(cont.mailingstreet != null)
            {
                if(cont.mailingstreet.length() <= 100)
                    {
                    layout.search_street__c = cont.mailingstreet;            
                    }
                else
                    {
                    layout.search_street__c = cont.mailingstreet.substring(0,100);            
                    }
            }
            else
            {
             layout.search_street__c = '';
            }
            layout.search_city__c = cont.mailingcity;
            layout.search_state__c = cont.mailingstate;
            layout.search_postal_code__c = cont.mailingpostalcode;
            layout.search_country__c = cont.mailingcountry;
            }
            else if(gotMailing == false && gotOther == true)
            {
            layout.address_type__c = 'Other';
            if(cont.otherstreet != null)
            {
                if(cont.otherstreet.length() <= 100)
                    {
                    layout.search_street__c = cont.otherstreet;            
                    }
                else
                    {
                    layout.search_street__c = cont.otherstreet.substring(0,100);            
                    }
            }
            else
            {
             layout.search_street__c = '';
            }
            layout.search_city__c = cont.othercity;
            layout.search_state__c = cont.otherstate;
            layout.search_postal_code__c = cont.otherpostalcode;
            layout.search_country__c = cont.othercountry;
            }
            
            layout.lookup_by__c = userinfo.getname() + ' ' + datetime.now().formatlong();
            if(layout.number_of_hits__c > 0)
                {
                layout.review_status__c = 'Not Reviewed';
                }
            else
                {
                layout.review_status__c = 'No match';
                }            
            rslt.add(layout);
        }
        //} //end of mailing address check
        
  
        if(gotOther == true && sentOther == false && addressMatch(cont) == false)
        {
        lu = new ATTUSWatchDOGSFLookup();
        
        if(cont.otherstreet != null )
            {
            if(cont.otherstreet.length() <= 100)
                {
                lu.street = cont.otherstreet;
                }
            else
                {
                lu.street = cont.otherstreet.substring(0,100);
                }
            }
        if(cont.othercity != null )
            {
            lu.city = cont.othercity;
            }
        if(cont.otherstate != null )
            {
            lu.state = cont.otherstate;
            }
        if(cont.otherpostalcode != null && (cont.othercountry != null && cont.othercountry != '')) //if country is blank, do not send zip code
            {
            //if us country, send only left 5 of zip
            if(cont.othercountry == 'US' || cont.othercountry == 'USA' || cont.othercountry == 'U.S.' || cont.othercountry == 'U.S.A.' || cont.othercountry.contains('United States'))
                {
                if(cont.otherpostalcode.length() <= 5)
                    {
                    lu.postalcode = cont.otherpostalcode;
                    }
                else
                    {
                    lu.postalcode = cont.otherpostalcode.substring(0,5);
                    }
                }
            else
                {
                lu.postalcode = cont.otherpostalcode;
                }
            }
        if(cont.othercountry != null )
            {
            lu.country = cont.othercountry;
            }
      
        req = lu.createRequestInd(cont.FirstName, cont.LastName);
        if(isTest == false)
            {
            results = lu.checkList(req);
            }
         else
             {
                 if(isErrorTest == false)
                 {
                 results = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ATTUSWATCHDOGRESPONSE tranid="317302009" trandate="2011-04-01 08:15:21"><BILLINGCODE partnerid="0f066555-caee-439f-88d7-7ce72767592b " billingid="0f066555-caee-439f-88d7-7ce72767592b" clientdefined1="SF Testing" clientdefined2="" clientdefined3="" /><LISTS><LIST code="OFAC" /></LISTS><QUERY><ITEM operator="" threshold=".85" surname="bin Ladin" givenname="Usama" unparsedname="" othername="" allentities="" id="" state="" city="" postalcode="" country="" blockedcountryname="" /></QUERY><SEARCHCRITERIA text="search criteria text"><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-03-31 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-03-31" /><ENTITIES><ENTITY entityid="755" entitynum="6365" thisentitysmaxscore="1.000"><LASTNAME>BIN LADIN</LASTNAME><FIRSTNAME>Usama bin Muhammad bin Awad</FIRSTNAME><OTHERNAME /><WHOLENAME /><ISALIASHIT>1</ISALIASHIT><SCORE>1.000</SCORE><TYPE>individual</TYPE><PROGRAMS>SDGT(Specially Designated Global Terrorist); SDT(Specially Designated Terrorist)</PROGRAMS><REMARKS>Modified On or Before: 10/26/2001; DOB 30 Jul 1957; alt. DOB 1958; POB Jeddah, Saudi Arabia; alt. POB Yemen.</REMARKS><ALIASES><ALIAS name="aka, BIN LADEN, Osama" type="individual" score="0.865" /><ALIAS name="aka, BIN LADEN, Usama" type="individual" score="0.942" /><ALIAS name="aka, BIN LADIN, Osama" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Osama bin Muhammad bin Awad" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Usama" type="individual" score="1.000" /></ALIASES><ADDRESSES /></ENTITY></ENTITIES></SEARCHCRITERIA></ATTUSWATCHDOGRESPONSE></soap:Body></soap:Envelope>';
                 }
                 else
                 {
                 results = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><soap:Fault><faultcode>soap:Client</faultcode><faultstring>Server did not recognize the value of HTTP Header SOAPAction: ProcessNameListMessage_An.</faultstring><detail /></soap:Fault></soap:Body></soap:Envelope>';
                 }
             }
        lu.parseDOC(results);
        if(lu.retErrMsg.size() > 0)
        {
            string errMsgs = '';
              for(string s: lu.retErrMsg)
              {
               if(s<> '')
                 {
                     if(s.contains('ERR-67001') || s.contains('ERR-67007') || s.contains('ERR-67008')  || s.contains('ERR-67010')  || s.contains('ERR-67012') || s.contains('ERR-67013') || s.contains('ERR-67014') || s.contains('ERR-67021') || s.contains('ERR-67023') || s.contains('ERR-67024') || s.contains('ERR-67301'))
                     {
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'The watch list check could not be completed – ATTUS Error:  Client is unauthorized.  Please have your Salesforce administrator contact ATTUS Support at 888-494-8449 option 3.');
                     ApexPages.addMessage(myMsg);
                     }
                     else
                     {
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,s);
                     ApexPages.addMessage(myMsg);
                     }
                 }
              }
              isError = true;
              errMsg = errMsg + ' : ' + errMsgs;   
        }
        
        //create Lookupobject and save to database.
        
        for (Integer x = 0; x < lu.retXmlData.size() ; x++ ) 
        {
            ATTUSWDSFLookup__c layout = new ATTUSWDSFLookup__c();
            layout.Contact__c = cont.id;
            layout.List_Date__c = Date.valueOf(lu.retListDate[x]);
            layout.Number_of_Hits__c = lu.retHitCount[x];
            layout.List_Description__c = lu.retListName[x];
            layout.Result_Node__c = lu.retXmlData[x];
            layout.name_searched__c = cont.firstname + ' ' + cont.lastname;
            layout.address_type__c = 'Other';
            if(cont.otherstreet != null)
            {
                if(cont.otherstreet.length() <= 100)
                    {
                    layout.search_street__c = cont.otherstreet;            
                    }
                else
                    {
                    layout.search_street__c = cont.otherstreet.substring(0,100);            
                    }
            }
            else
            {
             layout.search_street__c = '';
            }
            layout.search_city__c = cont.othercity;
            layout.search_state__c = cont.otherstate;
            layout.search_postal_code__c = cont.otherpostalcode;
            layout.search_country__c = cont.othercountry;
            layout.lookup_by__c = userinfo.getname() + ' ' + datetime.now().formatlong();
            if(layout.number_of_hits__c > 0)
                {
                layout.review_status__c = 'Not Reviewed';
                }
            else
                {
                layout.review_status__c = 'No match';
                }                        
            rslt.add(layout);
        }
        } //end of otheraddress check
        
        
        if(isError == true)
            {
                throw new MyException('');
            }
        }
        catch (MyException ex)
        {        
         return null;
        }
        finally
        {
        pageRef = new PageReference('/' + cont.id);  
        pageRef.setRedirect(true);
        if(rslt.size() > 0)
            {
            insert rslt;  
            }
        }
        return pageRef;
}

      public PageReference goBack()
      {
        PageReference pageRef = new PageReference('/' + cont.id);  
        pageRef.setRedirect(true);
        return pageRef;  
      }
}