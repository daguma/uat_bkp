@isTest
public class TestUpdateRescheduleOptionJob {

    @isTest
    private static void testRescheduleOptionUpdate() {
        Test.startTest();
        UpdateRescheduleOptionJob job = new UpdateRescheduleOptionJob(null, null);
        Database.executebatch(job, job.batchsize);
        Test.stopTest();
    }

    @isTest
    private static void testRescheduleOptionUpdate2() {
        Test.startTest();
         loan.TestHelper.systemDate = Date.newInstance(2015, 3, 01);
        loan.TestHelper.createSeedDataForTesting();
        
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Set the Fiscal Year days
        loan.TestHelper.createDayProcessForFullYear(loan.TestHelper.SystemDate);
        
        //Client__c dummyClient = TestHelper.createClient(dummyOffice);
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();        
        
        loan__Payment_Mode__c pMode = [SELECT Id 
                                       FROM loan__Payment_Mode__c 
                                       LIMIT 1
                                      ];
        
        Contact dummyClient = new Contact();
        dummyClient.LastName = 'TestContact';
        insert dummyClient;

        loan__Loan_Account__c loanAccount1 = loan.TestHelper.createLoanAccountForContactObj(dummyLP,
                                                    dummyClient,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
        loanAccount1.loan__Reschedule_Option_On_Excess_Payment__c = 'Change Payment Amount';
        update loanAccount1;
        //Set Org parameters
        loan__Org_Parameters__c orgParams = loan.CustomSettingsUtil.getOrgParameters();
        orgParams.loan__Disable_Triggers__c = false;
        upsert orgParams;
        system.debug('inside test, loanAccount1 :' + loanAccount1);

        List<loan__Loan_Account__c> loanAccountList = [SELECT ID,
                                                              Name,
                                                              loan__Reschedule_Option_On_Excess_Payment__c,
                                                              loan__Loan_Product_Name__c,
                                                              loan__Loan_Product_Name__r.loan__Excess_Threshold_For_Reschedule__c,
                                                              loan__Loan_Product_Name__r.loan__Reschedule_Option_On_Excess_Payment__c
                                                       FROM loan__Loan_Account__c
                                                       //WHERE loan__Loan_Product_Name__r.loan__Excess_Threshold_For_Reschedule__c > 0
                                                       LIMIT 1
                                                      ];
        system.debug('loanAccountList : ' + loanAccountList);
        loan__Loan_Account__c loanAccount = loanAccountList.get(0);
        loanAccount.loan__Reschedule_Option_On_Excess_Payment__c = null;
        loanAccount.loan__Loan_Product_Name__r.loan__Excess_Threshold_For_Reschedule__c = 10;
        loanAccount.loan__Loan_Product_Name__r.loan__Reschedule_Option_On_Excess_Payment__c = null;
        UpdateRescheduleOptionJob job = new UpdateRescheduleOptionJob();
        job.updateLoanAccounts(loanAccountList);

        system.debug('test, loanAccountList after processing : ' + loanAccountList);
        system.assertEquals('Keep Same Payment Amount', loanAccountList.get(0).loan__Reschedule_Option_On_Excess_Payment__c);
        dummyLP.loan__Reschedule_Option_On_Excess_Payment__c = 'Change Payment Amount';
        List<loan__Loan_Product__c> loanProductList = new List<loan__Loan_Product__c>();
        loanProductList.add(dummyLP);
        job.updateLoanProducts(loanProductList);
        system.debug('loanProductList : ' + loanProductList);
        system.assertEquals('Keep Same Payment Amount', loanProductList.get(0).loan__Reschedule_Option_On_Excess_Payment__c);
        Test.stopTest();
    }

}