global class PrepayRebateImplementationBatch implements Database.Batchable<sObject>, Schedulable {

    global final List<loan__Loan_Account__c> loanToUpdate = new List<loan__Loan_Account__c>();

    global PrepayRebateImplementationBatch() {

    }

    global void execute(SchedulableContext sc) {

         PrepayRebateImplementationBatch b = new PrepayRebateImplementationBatch();
            database.executebatch(b);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query = 'SELECT Id , loan__Pay_Off_Amount_As_Of_Today__c, ' + 
                'Remaining_Fee_Amount__c, RebatePayoffUpdated__c ' + 
                'FROM loan__Loan_Account__c ' +
                'WHERE loan__Loan_Status__c not in (\'Closed- Written Off\', \'Closed - Obligations met\', \'Canceled\') '+ 
                'AND RebatePayoffUpdated__c <> TODAY AND Remaining_Fee_Amount__c > 0 ORDER BY CreatedDate DESC';
        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<loan__Loan_Account__c> scope) {

        for (loan__Loan_Account__c loan : scope) {
            
            if (loan.Remaining_Fee_Amount__c > 0) {
                loan.loan__Pay_Off_Amount_As_Of_Today__c  = loan.loan__Pay_Off_Amount_As_Of_Today__c - loan.Remaining_Fee_Amount__c;
                loan.RebatePayoffUpdated__c = date.today();
                loanToUpdate.add(loan);
            }
        }
        if (loanToUpdate.size() > 0) Database.update(loanToUpdate, false);
    }

    global void finish(Database.BatchableContext BC) {

    }

}