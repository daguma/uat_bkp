@isTest public class ProxyApiBRMSUtilTest {

    @isTest static void validates_before_call_api_false() {
        ProxyApiBRMSUtil.settings = null;

        Boolean validate = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        System.assertEquals(false, validate);
    }

    @isTest static void validates_before_call_api_true() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        Boolean validate = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        System.assertEquals(true, validate);
    }

    @isTest static void gets_new_token() {
        Proxy_Api__c settings = LibraryTest.fakeSettings();
        settings.Token_New__c = '';
        settings.Token_Expires_Time_New__c = Datetime.now().addDays(-1).getTime();

        ProxyApiBRMSUtil.settings = settings;

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'TestCookie');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Boolean result = ProxyApiBRMSUtil.ExistsApiToken();

        Test.stopTest();

        System.assertEquals(true, result);
        System.assertEquals('tokentest2', ProxyApiBRMSUtil.settings.Token_New__c);
    }

    @isTest static void gets_new_token_with_cookie() {
        Proxy_Api__c settings = LibraryTest.fakeSettings();
        settings.Token_New__c = '';
        settings.Token_Expires_Time_New__c = Datetime.now().addDays(-1).getTime();

        ProxyApiBRMSUtil.settings = settings;

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT; path=/; domain=.lendingpoint.com; HttpOnly');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Boolean result = ProxyApiBRMSUtil.ExistsApiToken();

        Test.stopTest();

        System.assertEquals(true, result);
        System.assertEquals('tokentest2', ProxyApiBRMSUtil.settings.Token_New__c);
    }

    @isTest static void gets_api_result_401() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(401,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiBRMSUtil.ApiGetResult('http://somethingtotest.com');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiBRMSUtil.settings.Token_New__c);
    }

    @isTest static void gets_api_result_201() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiBRMSUtil.ApiGetResult('http://somethingtotest.com');

        System.assertEquals('tokentest', ProxyApiBRMSUtil.settings.Token_New__c);
    }

    @isTest static void gets_api_result_200() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiBRMSUtil.ApiGetResult('http://somethingtotest.com');
        System.assertEquals('[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]', jsonResult);
    }

    @isTest static void gets_api_result_417() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(417,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiBRMSUtil.ApiGetResult('http://somethingtotest.com');
        System.assertEquals('[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]', jsonResult);
    }

    @isTest static void gets_api_result_401_with_body() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(401,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiBRMSUtil.ApiGetResult('http://somethingtotest.com', 'testbody');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiBRMSUtil.settings.Token_New__c);
    }

    @isTest static void gets_api_result_201_with_body() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiBRMSUtil.ApiGetResult('http://somethingtotest.com', 'testbody');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiBRMSUtil.settings.Token_New__c);
    }

    @isTest static void gets_api_result_200_with_body() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiBRMSUtil.ApiGetResult('http://somethingtotest.com', 'testbody');
        System.assertEquals('[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]', jsonResult);
    }

    @isTest static void gets_api_result_417_with_body() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(417,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiBRMSUtil.ApiGetResult('http://somethingtotest.com', 'testbody');
        System.assertEquals('[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]', jsonResult);
    }

    @isTest static void gets_binary_result_200() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Blob jsonResult = ProxyApiBRMSUtil.ApiGetBinaryResult('http://somethingtotest.com', 'testbody');
        System.assertNotEquals(null, jsonResult);
    }

    @isTest static void gets_binary_result_401() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(401,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Blob jsonResult = ProxyApiBRMSUtil.ApiGetBinaryResult('http://somethingtotest.com', 'testbody');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiBRMSUtil.settings.Token_New__c);
    }

    @isTest static void gets_binary_result_0() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(0,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Blob jsonResult = ProxyApiBRMSUtil.ApiGetBinaryResult('http://somethingtotest.com', 'testbody');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_api_bank_info_mail() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        ProxyApiBankInformation jsonResult = ProxyApiBRMSUtil.ApiGetBankInformationByEmail('test2@testmail2.com');
        System.assertNotEquals(null, jsonResult);
    }

    @isTest static void gets_null_api_bank_info_mail() {
        ProxyApiBRMSUtil.settings = null;

        ProxyApiBankInformation jsonResult = ProxyApiBRMSUtil.ApiGetBankInformationByEmail('test2@testmail2.com');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_api_bank_info_empty_mail() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        ProxyApiBankInformation jsonResult = ProxyApiBRMSUtil.ApiGetBankInformationByEmail('');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_api_bank_info_pdf() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Blob jsonResult = ProxyApiBRMSUtil.ApiGetBankInformationPdfByEmail('test2@testmail2.com');
        System.assertNotEquals(null, jsonResult);

        ProxyApiBRMSUtil.settings = null;
        jsonResult = ProxyApiBRMSUtil.ApiGetBankInformationPdfByEmail('test2@testmail2.com');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_null_api_bank_info_pdf() {
        ProxyApiBRMSUtil.settings = null;
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Blob jsonResult = ProxyApiBRMSUtil.ApiGetBankInformationPdfByEmail('test2@testmail2.com');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void api_get_null_offers_by_loan_amount() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        List<ProxyApiOfferCatalog> offerList = ProxyApiBRMSUtil.ApiGetOffersByLoanAmount('C2', 250000, 15000, 'CA', 1);
        System.assertEquals(null, offerList);
    }

    @isTest static void api_get_offers_by_loan_amount() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        List<ProxyApiOfferCatalog> offerList = ProxyApiBRMSUtil.ApiGetOffersByLoanAmount('C2', 250000, 15000, 'CA');
        System.assertEquals(true, offerList.size() > 0);
    }

    @isTest static void api_get_offers_by_loan_amount_not_validated() {
        ProxyApiBRMSUtil.settings = null;

        List<ProxyApiOfferCatalog> offerList = ProxyApiBRMSUtil.ApiGetOffersByLoanAmount('C2', 250000, 15000, 'CA', 0);
        System.assertEquals(null, offerList);
    }

    @isTest static void api_get_null_offers_by_payment_amount() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        List<ProxyApiOfferCatalog> offerList = ProxyApiBRMSUtil.ApiGetOffersByPaymentAmount('C2', 250000, 15000, 'CA', 1);
        System.assertEquals(null, offerList);
    }

    @isTest static void api_get_offers_by_payment_amount() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        List<ProxyApiOfferCatalog> offerList = ProxyApiBRMSUtil.ApiGetOffersByPaymentAmount('C2', 250000, 15000, 'CA', 0);
        System.assertEquals(true, offerList.size() > 0);
    }

    @isTest static void api_get_null_offers_by_payment_amount_not_validated() {
        ProxyApiBRMSUtil.settings = null;

        List<ProxyApiOfferCatalog> offerList = ProxyApiBRMSUtil.ApiGetOffersByPaymentAmount('C2', 250000, 15000, 'CA', 0);
        System.assertEquals(null, offerList);
    }

}