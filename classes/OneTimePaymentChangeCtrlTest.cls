@isTest public class OneTimePaymentChangeCtrlTest {
    
    @isTest Static void PaymentChange_ContractIsCheck(){
        
        Contact con = LibraryTest.createContactTH();
        con.Checking_Account_Number__c = '1123565454';
        con.Checking_Routing_Number__c = '098767656';
        con.Account_Type__c = 'Test Type';
        con.Bank_Name__c = 'Test Fargo Bank';
        upsert con;
        
        Account acc = new Account();
        acc.name = 'Credit Account Test';
        acc.Discount_Fee__c = 12.80;
        insert acc;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.status__c = 'Funded';
        opp.Partner_Account__c = acc.Id;
        opp.Offer_Selected_Date__c = Date.today().addDays(-15);
        
        update opp;
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.loan__Contact__c = con.Id;
        cnt.Opportunity__c = opp.Id;
        cnt.loan__ACH_Next_Debit_Date__c = Date.today().addDays(15);
        cnt.loan__OT_ACH_Debit_Date__c = Date.today().addDays(17);
        cnt.loan__Next_Due_Generation_Date__c = Date.today().addDays(15);
        cnt.loan__Disbursal_Date__c = Date.today().addDays(-15);
        cnt.loan__First_Installment_Date__c = Date.today().addDays(15);
        cnt.loan__ACH_On__c = False;
        cnt.Is_Debit_Card_On__c = False;
        cnt.loan__Pmt_Amt_Cur__c = 129.90;
        cnt.loan__Frequency_of_Loan_Payment__c = 'Semi-Monthly';
        upsert cnt;
        
        
        loan__loan_Account__c contract = [SELECT Id
                                          ,Name
                                          ,Opportunity__c
                                          ,loan__ACH_Next_Debit_Date__c
                                          ,loan__OT_ACH_Debit_Date__c
                                          ,loan__ACH_On__c
                                          ,loan__ACH_Routing_Number__c
                                          ,loan__ACH_Account_Number__c
                                          ,loan__ACH_Debit_Amount__c
                                          ,loan__Pmt_Amt_Cur__c
                                          ,loan__ACH_Relationship_Type__c
                                          ,loan__ACH_Bank_Name__c
                                          ,loan__ACH_Drawer_Name__c
                                          ,loan__ACH_Account_Type__c
                                          FROM loan__Loan_Account__c where Id =: cnt.Id];
        
                
		
        LP_Custom__c custom = [SELECT Id, PaymentDaysOutAllowed__c, PaymentChangeDaysButtonDisabled__c, OneTimeDueDateChange_PON__c, OneTimeDueDateChange_LP__c FROM LP_Custom__c LIMIT 1];
        custom.PaymentDaysOutAllowed__c = 15;
        custom.PaymentChangeDaysButtonDisabled__c = 90;
        custom.OneTimeDueDateChange_LP__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        custom.OneTimeDueDateChange_PON__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        update custom;
        
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        OneTimePaymentChangeController OneTimePayment = New OneTimePaymentChangeController(controller);
        
        OneTimePayment.save();
        
    }
    
    @isTest Static void PaymentChange_Contract_isACH(){
        
        Account acc = new Account();
        acc.name = 'Credit Account Test';
        acc.Discount_Fee__c = 12.80;
        insert acc;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.status__c = 'Funded';
        opp.Partner_Account__c = acc.Id;
        opp.Offer_Selected_Date__c = Date.today().addDays(-15);
        update opp;
        
  
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.opportunity__c = opp.Id;
        cnt.loan__ACH_Next_Debit_Date__c = Date.today().addDays(15);
        cnt.loan__OT_ACH_Debit_Date__c = Date.today().addDays(17);
        cnt.loan__Next_Due_Generation_Date__c = Date.today().addDays(15);
        cnt.loan__Disbursal_Date__c = Date.today().addDays(-15);
        cnt.loan__First_Installment_Date__c = Date.today().addDays(17);
        cnt.ACH_Flag_Reason__c = 'Customer Authorized';
        cnt.loan__ACH_Account_Type__c = 'Checking';
        cnt.loan__ACH_Routing_Number__c = '1000012303';
        cnt.loan__ACH_Account_Number__c = '0987676576';
        cnt.loan__ACH_Debit_Amount__c = 12.90;
        cnt.loan__Pmt_Amt_Cur__c = 129.90;
        cnt.loan__ACH_Relationship_Type__c = 'Primary';
        cnt.loan__ACH_Bank_Name__c = 'Teat American Bank';
        cnt.loan__ACH_Drawer_Name__c = 'Test';
        cnt.loan__ACH_On__c = True;
        cnt.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        upsert cnt;
        
        loan__loan_Account__c contract = [SELECT Id
                                          ,Name
                                          ,loan__Contact__c
                                          ,loan__ACH_Next_Debit_Date__c
                                          ,loan__OT_ACH_Debit_Date__c
                                          ,loan__ACH_On__c
                                          ,loan__ACH_Routing_Number__c
                                          ,loan__ACH_Account_Number__c
                                          ,loan__ACH_Debit_Amount__c
                                          ,loan__Pmt_Amt_Cur__c
                                          ,loan__ACH_Relationship_Type__c
                                          ,loan__ACH_Bank_Name__c
                                          ,loan__ACH_Drawer_Name__c
                                          ,loan__ACH_Account_Type__c
                                          FROM loan__Loan_Account__c where Id =: cnt.Id];
        
        loan__Repayment_Schedule__c repayment = New loan__Repayment_Schedule__c();
        repayment.loan__Due_Date__c = Date.today().addDays(300);
        repayment.loan__Loan_Account__c = contract.Id;
        repayment.loan__Past_Due_Date__c = false;
        insert repayment;
        
        LP_Custom__c custom = [SELECT Id, PaymentDaysOutAllowed__c, PaymentChangeDaysButtonDisabled__c, OneTimeDueDateChange_PON__c, OneTimeDueDateChange_LP__c FROM LP_Custom__c LIMIT 1];
        custom.PaymentDaysOutAllowed__c = 15;
        custom.PaymentChangeDaysButtonDisabled__c = 90;
        custom.OneTimeDueDateChange_LP__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        custom.OneTimeDueDateChange_PON__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        update custom;

        
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        OneTimePaymentChangeController OneTimePayment = New OneTimePaymentChangeController(controller);
        
        OneTimePayment.save();
        
    }
    
    @isTest Static void PaymentChange_ContractIsDebitCard(){
        
        Contact con = LibraryTest.createContactTH();
        con.Checking_Account_Number__c = '1123565454';
        con.Checking_Routing_Number__c = '098767656';
        con.Account_Type__c = 'Test Type';
        con.Bank_Name__c = 'Test Fargo Bank';
        upsert con;
        
        Account acc = new Account();
        acc.name = 'Credit Account Test';
        acc.Discount_Fee__c = 12.80;
        insert acc;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.status__c = 'Funded';
        opp.Partner_Account__c = acc.Id;
        opp.Offer_Selected_Date__c = Date.today().addDays(-15);
        
        update opp;
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.loan__Contact__c = con.Id;
        cnt.Opportunity__c = opp.Id;
        cnt.loan__ACH_Next_Debit_Date__c = Date.today().addDays(15);
        cnt.loan__OT_ACH_Debit_Date__c = Date.today().addDays(17);
        cnt.loan__Next_Due_Generation_Date__c = Date.today().addDays(15);
        cnt.loan__Disbursal_Date__c = Date.today().addDays(-15);
        cnt.loan__First_Installment_Date__c = Date.today().addDays(15);
        cnt.loan__Frequency_of_Loan_Payment__c = '28 Days';//Bi-Weekly
        cnt.loan__ACH_On__c = False;
        cnt.Is_Debit_Card_On__c = True;
        cnt.loan__Pmt_Amt_Cur__c = 129.90;
        upsert cnt;
        
        
        loan__loan_Account__c contract = [SELECT Id
                                          ,Name
                                          ,Opportunity__c
                                          ,loan__ACH_Next_Debit_Date__c
                                          ,loan__OT_ACH_Debit_Date__c
                                          ,loan__ACH_On__c
                                          ,loan__ACH_Routing_Number__c
                                          ,loan__ACH_Account_Number__c
                                          ,loan__ACH_Debit_Amount__c
                                          ,loan__Pmt_Amt_Cur__c
                                          ,loan__ACH_Relationship_Type__c
                                          ,loan__ACH_Bank_Name__c
                                          ,loan__ACH_Drawer_Name__c
                                          ,loan__ACH_Account_Type__c
                                          FROM loan__Loan_Account__c where Id =: cnt.Id];
        
                
		
        LP_Custom__c custom = [SELECT Id, PaymentDaysOutAllowed__c, PaymentChangeDaysButtonDisabled__c, OneTimeDueDateChange_PON__c, OneTimeDueDateChange_LP__c FROM LP_Custom__c LIMIT 1];
        custom.PaymentDaysOutAllowed__c = 15;
        custom.PaymentChangeDaysButtonDisabled__c = 90;
        custom.OneTimeDueDateChange_LP__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        custom.OneTimeDueDateChange_PON__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        update custom;
        
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        OneTimePaymentChangeController OneTimePayment = New OneTimePaymentChangeController(controller);
        
        OneTimePayment.save();
        
    }
    
    @isTest Static void PaymentChange_DueDate_Change(){
        
        Contact con = LibraryTest.createContactTH();
        con.Checking_Account_Number__c = '1123565454';
        con.Checking_Routing_Number__c = '098767656';
        con.Account_Type__c = 'Test Type';
        con.Bank_Name__c = 'Test Fargo Bank';
        upsert con;
        
        Account acc = new Account();
        acc.name = 'Credit Account Test';
        acc.Discount_Fee__c = 12.80;
        insert acc;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.status__c = 'Funded';
        opp.Partner_Account__c = acc.Id;
        opp.Offer_Selected_Date__c = Date.today().addDays(-15);
        update opp;
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.loan__Contact__c = con.Id;
        cnt.Opportunity__c = opp.Id;
        cnt.loan__ACH_Next_Debit_Date__c = Date.today().addDays(15);
        cnt.loan__OT_ACH_Debit_Date__c = Date.today().addDays(17);
        cnt.loan__Next_Due_Generation_Date__c = Date.today().addDays(15);
        cnt.loan__Disbursal_Date__c = Date.today().addDays(-15);
        cnt.loan__ACH_On__c = False;
        cnt.loan__Frequency_of_Loan_Payment__c = 'Bi-Weekly';
        cnt.LastDateDueDateChanged__c = Date.today();
        upsert cnt;
        
        
        loan__loan_Account__c contract = [SELECT Id
                                          ,Name
                                          ,Opportunity__c
                                          ,loan__ACH_Next_Debit_Date__c
                                          ,loan__OT_ACH_Debit_Date__c
                                          ,loan__ACH_On__c
                                          ,loan__ACH_Routing_Number__c
                                          ,loan__ACH_Account_Number__c
                                          ,loan__ACH_Debit_Amount__c
                                          ,loan__Pmt_Amt_Cur__c
                                          ,loan__ACH_Relationship_Type__c
                                          ,loan__ACH_Bank_Name__c
                                          ,loan__ACH_Drawer_Name__c
                                          ,loan__ACH_Account_Type__c
                                          ,LastDateDueDateChanged__c
                                          FROM loan__Loan_Account__c where Id =: cnt.Id];
        
                
		
        LP_Custom__c custom = [SELECT Id, PaymentDaysOutAllowed__c, PaymentChangeDaysButtonDisabled__c, OneTimeDueDateChange_PON__c, OneTimeDueDateChange_LP__c FROM LP_Custom__c LIMIT 1];
        custom.PaymentDaysOutAllowed__c = 15;
        custom.PaymentChangeDaysButtonDisabled__c = 90;
        custom.OneTimeDueDateChange_LP__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        custom.OneTimeDueDateChange_PON__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        update custom;
        
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        OneTimePaymentChangeController OneTimePayment = New OneTimePaymentChangeController(controller);
        
        OneTimePayment.save();
        
    }
    
    @isTest Static void PaymentChange_ErrorButtondisable(){
        
        Contact con = LibraryTest.createContactTH();
        con.Checking_Account_Number__c = '1123565454';
        con.Checking_Routing_Number__c = '098767656';
        con.Account_Type__c = 'Test Type';
        con.Bank_Name__c = 'Test Fargo Bank';
        upsert con;
        
        Account acc = new Account();
        acc.name = 'Credit Account Test';
        acc.Discount_Fee__c = 12.80;
        insert acc;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.status__c = 'Funded';
        opp.Partner_Account__c = acc.Id;
        opp.Offer_Selected_Date__c = Date.today().addDays(-15);
        update opp;
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.loan__Contact__c = con.Id;
        cnt.Opportunity__c = opp.Id;
        cnt.loan__ACH_Next_Debit_Date__c = Date.today().addDays(15);
        cnt.loan__OT_ACH_Debit_Date__c = Date.today().addDays(17);
        cnt.loan__Next_Due_Generation_Date__c = Date.today().addDays(15);
        cnt.loan__Disbursal_Date__c = Date.today().addDays(-15);
        cnt.loan__ACH_On__c = False;
        cnt.LastDateDueDateChanged__c = Date.today().addDays(-1);
        upsert cnt;
        
        
        loan__loan_Account__c contract = [SELECT Id
                                          ,Name
                                          ,Opportunity__c
                                          ,loan__ACH_Next_Debit_Date__c
                                          ,loan__OT_ACH_Debit_Date__c
                                          ,loan__ACH_On__c
                                          ,loan__ACH_Routing_Number__c
                                          ,loan__ACH_Account_Number__c
                                          ,loan__ACH_Debit_Amount__c
                                          ,loan__Pmt_Amt_Cur__c
                                          ,loan__ACH_Relationship_Type__c
                                          ,loan__ACH_Bank_Name__c
                                          ,loan__ACH_Drawer_Name__c
                                          ,loan__ACH_Account_Type__c
                                          ,LastDateDueDateChanged__c
                                          FROM loan__Loan_Account__c where Id =: cnt.Id];
        
                
		
        LP_Custom__c custom = [SELECT Id, PaymentDaysOutAllowed__c, PaymentChangeDaysButtonDisabled__c, OneTimeDueDateChange_PON__c, OneTimeDueDateChange_LP__c FROM LP_Custom__c LIMIT 1];
        custom.PaymentDaysOutAllowed__c = 15;
        custom.PaymentChangeDaysButtonDisabled__c = 90;
        custom.OneTimeDueDateChange_LP__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        custom.OneTimeDueDateChange_PON__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        update custom;
        
        String currentUser;
        List<user> us = new List<user>();   // falata asignarle el user
        
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        OneTimePaymentChangeController OneTimePayment = New OneTimePaymentChangeController(controller);
        
        
        OneTimePayment.save();
        
    }
    
    @isTest Static void PaymentChange_Date_limit(){
        //line 78
        Contact con = LibraryTest.createContactTH();
        con.Checking_Account_Number__c = '1123565454';
        con.Checking_Routing_Number__c = '098767656';
        con.Account_Type__c = 'Test Type';
        con.Bank_Name__c = 'Test Fargo Bank';
        upsert con;
        
        Account acc = new Account();
        acc.name = 'Credit Account Test';
        acc.Discount_Fee__c = 12.80;
        insert acc;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.status__c = 'Funded';
        opp.Partner_Account__c = acc.Id;
        opp.Offer_Selected_Date__c = Date.today().addDays(-15);
        update opp;
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.loan__Contact__c = con.Id;
        cnt.Opportunity__c = opp.Id;
        cnt.loan__ACH_Next_Debit_Date__c = Date.today().addDays(2);
        cnt.loan__OT_ACH_Debit_Date__c = Date.today().addDays(30);
        cnt.loan__Next_Due_Generation_Date__c = Date.today().addDays(2);
        cnt.loan__Disbursal_Date__c = Date.today().addDays(-15);
        cnt.loan__ACH_On__c = False;
        upsert cnt;
        
        
        loan__loan_Account__c contract = [SELECT Id
                                          ,Name
                                          ,Opportunity__c
                                          ,loan__ACH_Next_Debit_Date__c
                                          ,loan__OT_ACH_Debit_Date__c
                                          ,loan__ACH_On__c
                                          ,loan__ACH_Routing_Number__c
                                          ,loan__ACH_Account_Number__c
                                          ,loan__ACH_Debit_Amount__c
                                          ,loan__Pmt_Amt_Cur__c
                                          ,loan__ACH_Relationship_Type__c
                                          ,loan__ACH_Bank_Name__c
                                          ,loan__ACH_Drawer_Name__c
                                          ,loan__ACH_Account_Type__c
                                          ,LastDateDueDateChanged__c
                                          FROM loan__Loan_Account__c where Id =: cnt.Id];
        
                
		
        LP_Custom__c custom = [SELECT Id, PaymentDaysOutAllowed__c, PaymentChangeDaysButtonDisabled__c, OneTimeDueDateChange_PON__c, OneTimeDueDateChange_LP__c FROM LP_Custom__c LIMIT 1];
        custom.PaymentDaysOutAllowed__c = 15;
        custom.PaymentChangeDaysButtonDisabled__c = 90;
        custom.OneTimeDueDateChange_LP__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        custom.OneTimeDueDateChange_PON__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        update custom;

        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        OneTimePaymentChangeController OneTimePayment = New OneTimePaymentChangeController(controller);
        
        
        OneTimePayment.save();
        
    }
    
    @isTest Static void due_date_before_two_days_Message(){
        //line 78
        Contact con = LibraryTest.createContactTH();
        con.Checking_Account_Number__c = '1123565454';
        con.Checking_Routing_Number__c = '098767656';
        con.Account_Type__c = 'Test Type';
        con.Bank_Name__c = 'Test Fargo Bank';
        upsert con;
        
        Account acc = new Account();
        acc.name = 'Credit Account Test';
        acc.Discount_Fee__c = 12.80;
        insert acc;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.status__c = 'Funded';
        opp.Partner_Account__c = acc.Id;
        opp.Offer_Selected_Date__c = Date.today().addDays(-15);
        update opp;
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.loan__Contact__c = con.Id;
        cnt.Opportunity__c = opp.Id;
        cnt.loan__ACH_Next_Debit_Date__c = Date.today().addDays(2);
        cnt.loan__OT_ACH_Debit_Date__c = Date.today().addDays(1);
        cnt.loan__Next_Due_Generation_Date__c = Date.today().addDays(2);
        cnt.loan__Disbursal_Date__c = Date.today().addDays(-15);
        cnt.loan__ACH_On__c = False;
        cnt.LastDateDueDateChanged__c = Date.today().addDays(-1);
        upsert cnt;
        
        
        loan__loan_Account__c contract = [SELECT Id
                                          ,Name
                                          ,Opportunity__c
                                          ,loan__ACH_Next_Debit_Date__c
                                          ,loan__OT_ACH_Debit_Date__c
                                          ,loan__ACH_On__c
                                          ,loan__ACH_Routing_Number__c
                                          ,loan__ACH_Account_Number__c
                                          ,loan__ACH_Debit_Amount__c
                                          ,loan__Pmt_Amt_Cur__c
                                          ,loan__ACH_Relationship_Type__c
                                          ,loan__ACH_Bank_Name__c
                                          ,loan__ACH_Drawer_Name__c
                                          ,loan__ACH_Account_Type__c
                                          ,LastDateDueDateChanged__c
                                          FROM loan__Loan_Account__c where Id =: cnt.Id];
        
                
		
        LP_Custom__c custom = [SELECT Id, PaymentDaysOutAllowed__c, PaymentChangeDaysButtonDisabled__c, OneTimeDueDateChange_PON__c, OneTimeDueDateChange_LP__c FROM LP_Custom__c LIMIT 1];
        custom.PaymentDaysOutAllowed__c = 15;
        custom.PaymentChangeDaysButtonDisabled__c = 90;
        custom.OneTimeDueDateChange_LP__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        custom.OneTimeDueDateChange_PON__c = 'Monthly:45,28Days:43,Bi-Weekly:30,Semi-Monthly:30';
        update custom;
        

        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        OneTimePaymentChangeController OneTimePayment = New OneTimePaymentChangeController(controller);
        
        
        OneTimePayment.save();
        
        Apexpages.StandardController controller2 = New Apexpages.StandardController(contract);
        OneTimePaymentChangeController OneTimePayment2 = New OneTimePaymentChangeController(controller);
        
        OneTimePayment.save();
        
    }
    
    

}