global class BatchFactorTrustPaymentReporting implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts{
     
     public void execute(SchedulableContext sc) { 

       BatchFactorTrustPaymentReporting batchapex = new BatchFactorTrustPaymentReporting();
       Factor_Trust_Configuration__c n = Factor_Trust_Configuration__c.getOrgDefaults();
       id batchprocessid = Database.executebatch(batchapex, n.Batch_Payment_Records__c.intValue());
       system.debug('Process ID: ' + batchprocessid);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){  
         String query = 'select id ,loan__Loan_Account__c,loan__Rejected__c,FT_Payment_Reporting__c,FT_Bounce_Reporting__c,loan__Transaction_Date__c, loan__Balance__c, loan__Cleared__c, loan__Transaction_Amount__c from loan__Loan_Payment_Transaction__c where  loan__Loan_Account__r.loan__Invalid_Data__c <> true AND ((loan__Cleared__c = true AND FT_Payment_Reporting__c = false)  OR (loan__Rejected__c = true AND FT_Bounce_Reporting__c = false)) ';                                                                                                                                                                                                                                                                                                      
         return Database.getQueryLocator(query);
    }
   
    
    global void execute(Database.BatchableContext BC, List<loan__Loan_Payment_Transaction__c> scope){ 
        if (scope == null) return;
        Set<Id> cs = new Set<Id>();
        for (loan__Loan_Payment_Transaction__c pt : scope) {
            cs.add(pt.loan__Loan_Account__c);
        }

        Map<Id, loan__Loan_Account__c> acc = new Map<Id, loan__Loan_Account__c>([ select Id, loan__Contact__r.FirstName, loan__Contact__r.LastName, loan__Contact__r.MailingStreet,
                                                                                        loan__Contact__r.MailingCity, loan__Contact__r.MailingState, loan__Contact__r.MailingPostalCode,
                                                                                        loan__Contact__r.MailingCountry, loan__Contact__r.BirthDate, loan__Contact__r.Phone_Clean__c,
                                                                                        loan__Contact__r.ints__Social_Security_Number__c, loan__Loan_Effective_Date__c, loan__Next_Installment_Date__c,
                                                                                        loan__ACH_Account_Number__c, Opportunity__c, (select Id from loan__Loan_Payment_Transactions__r) from loan__Loan_Account__c where Id in : cs ]);

        list<Logging__c> logList = new list<Logging__c>();       
        date DOB;
        string fName, lName,street, city, state, Zip, country, phone, PaymntId; 
        Integer i = 0;
        try{
            for(loan__Loan_Payment_Transaction__c pt : scope){
                 loan__Loan_Account__c la = acc.get(pt.loan__Loan_Account__c);
                 Contact c = la.loan__Contact__r;
                 PaymntId = pt.id; 
                 fName  = c.FirstName;
                 lName  = c.LastName;
                 street = c.MailingStreet;
                 city   = c.MailingCity;
                 state  = c.MailingState;
                 Zip    = c.MailingPostalCode;
                 country= c.MailingCountry;
                 DOB    = c.BirthDate; 
                 phone  = c.Phone_Clean__c;
                 i = 0;
                 for (loan__Loan_Payment_Transaction__c p : la.loan__Loan_Payment_Transactions__r) {
                     i++;
                 }
                 
                 FTReportingUtil.FactorTrustReporting('PT',PaymntId,'Loan RollOver Report','RO', c.ints__Social_Security_Number__c,String.ValueOf(la.loan__Loan_Effective_Date__c),String.ValueOf(la.loan__Next_Installment_Date__c),String.ValueOf(pt.loan__Transaction_Amount__c),String.ValueOf(pt.loan__Balance__c),la.loan__ACH_Account_Number__c,string.ValueOf(pt.loan__Transaction_Date__c),pt.loan__Loan_Account__c,la.Opportunity__c,fName,lname,DOB,city,street,state,Zip,country,phone, String.valueOf(i));
                                                      
                 if(pt.loan__Cleared__c  && !pt.FT_Payment_Reporting__c ){
                     logList.add(FTReportingUtil.FactorTrustReporting('PT',PaymntId,'Loan Cleared Report','PM', c.ints__Social_Security_Number__c,String.ValueOf(la.loan__Loan_Effective_Date__c),String.ValueOf(la.loan__Next_Installment_Date__c),String.ValueOf(pt.loan__Transaction_Amount__c),String.ValueOf(pt.loan__Balance__c),la.loan__ACH_Account_Number__c,string.ValueOf(pt.loan__Transaction_Date__c),pt.loan__Loan_Account__c,la.Opportunity__c,fName,lname,DOB,city,street,state,Zip,country,phone, null));
                     pt.FT_Payment_Reporting__c = true;
                 } 
                 
                 if(pt.loan__Rejected__c  && !pt.FT_Bounce_Reporting__c ){
                     logList.add(FTReportingUtil.FactorTrustReporting('PT',PaymntId,'Loan Rejected Report','RI', c.ints__Social_Security_Number__c,String.ValueOf(la.loan__Loan_Effective_Date__c),String.ValueOf(la.loan__Next_Installment_Date__c),String.ValueOf(pt.loan__Transaction_Amount__c),String.ValueOf(pt.loan__Balance__c),la.loan__ACH_Account_Number__c,string.ValueOf(pt.loan__Transaction_Date__c),pt.loan__Loan_Account__c,la.Opportunity__c,fName,lname,DOB,city,street,state,Zip,country,phone, null));
                     pt.FT_Bounce_Reporting__c = true;
                 }        
             }
                      
             update scope; 
             insert logList;
         }catch(Exception e){
            insert logList;          
            WebToSFDC.notifyDev('Error while submitting Factor Trust report for Payment Transaction - '+PaymntId, e.getMessage() + ' line ' + (e.getLineNumber()));                 
         }           
    }   
    
    
    global void finish(Database.BatchableContext BC) {       
        WebToSFDC.notifyDev('Factor Trust Payment Batch Completed', 'Factor Trust Payment Reporting completed for Today.');               
    }
}