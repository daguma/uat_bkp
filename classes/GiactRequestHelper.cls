public class GiactRequestHelper {
    public static string InquiryRequestStringFormat = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Header><AuthenticationHeader xmlns="http://api.giact.com/verificationservices/v5"><ApiUsername>{0}</ApiUsername><ApiPassword>{1}</ApiPassword></AuthenticationHeader></soap:Header><soap:Body><PostInquiry xmlns="http://api.giact.com/verificationservices/v5"><Inquiry><UniqueId>{7}</UniqueId><Check><RoutingNumber>{2}</RoutingNumber><AccountNumber>{3}</AccountNumber><CheckAmount>{4}</CheckAmount><AccountType>{5}</AccountType></Check><Customer><EntityType>Business</EntityType><BusinessName>{6}</BusinessName><DateOfBirth xsi:nil="true" /><MobileConsentRecordId xsi:nil="true" /><AltIdType xsi:nil="true" /></Customer><GVerifyEnabled>true</GVerifyEnabled><FundsConfirmationEnabled>{8}</FundsConfirmationEnabled><GAuthenticateEnabled>true</GAuthenticateEnabled><VoidedCheckImageEnabled>false</VoidedCheckImageEnabled><GIdentifyEnabled>false</GIdentifyEnabled><GIdentifyKbaEnabled>false</GIdentifyKbaEnabled><GIdentifyEsiEnabled>false</GIdentifyEsiEnabled><CustomerIdEnabled>false</CustomerIdEnabled><OfacScanEnabled>false</OfacScanEnabled><IpAddressInformationEnabled>false</IpAddressInformationEnabled><DomainWhoisEnabled>false</DomainWhoisEnabled><MobileVerifyEnabled>false</MobileVerifyEnabled><MobileIdentifyEnabled>false</MobileIdentifyEnabled><MobileLocationEnabled>false</MobileLocationEnabled></Inquiry></PostInquiry></soap:Body></soap:Envelope>';
    
    public static string[] GenerateStringArrayFromGiact(GIACT__c record, boolean dba) {
        GIACT_Integration_Settings__c settings = GIACT_Integration_Settings__c.getInstance();
        string fundsConfirmationEnabled = 'true';
        string selectedAccount = !dba ? record.MerchantAccount__r.Name : record.MerchantAccount__r.DBA_Doing_Business_As__c;
        if (!string.IsBlank(selectedAccount))
            selectedAccount = selectedAccount.replace('&','&amp;');

        return new string[] { settings.ApiUsername__c,
                               settings.ApiPassword__c,
                               record.RoutingNumber__c,
                               record.AccountNumber__c,
                               String.valueOf(record.CheckAmount__c),
                               'Checking',
                               selectedAccount,
                               record.Id,
                               fundsConfirmationEnabled };
    }
    
    public static string FundsConfirmationRequestStringFormat = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Header><AuthenticationHeader xmlns="http://api.giact.com/verificationservices/v5"><ApiUsername>{0}</ApiUsername><ApiPassword>{1}</ApiPassword></AuthenticationHeader></soap:Header><soap:Body><GetFundsConfirmationResults xmlns="http://api.giact.com/verificationservices/v5"><ResultsDate>{2}</ResultsDate></GetFundsConfirmationResults></soap:Body></soap:Envelope>';

    public static string GenerateFundsConfirmationRequest() {
        GIACT_Integration_Settings__c settings = GIACT_Integration_Settings__c.getInstance();
        return string.format(GiactRequestHelper.FundsConfirmationRequestStringFormat, new string[] { settings.ApiUsername__c, settings.ApiPassword__c, DateTime.Now().addDays(1).format('yyyy-MM-dd') + 'T00:00:00+00:00' });
    }
}