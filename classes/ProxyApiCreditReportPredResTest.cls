@isTest 
public class ProxyApiCreditReportPredResTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ProxyApiCreditReportPredictionResult cpr = new ProxyApiCreditReportPredictionResult ();
        Test.stopTest();
        System.assertEquals(null, cpr.labelIndex);
        System.assertEquals(null, cpr.label);
        System.assertEquals(null, cpr.classProbabilities);
        System.assertEquals(null, cpr.predictionErrorMessage);
        System.assertEquals(null, cpr.predictionErrorDetails);
    }
    
     static testmethod void validate_assign() {
        Test.startTest();
        ProxyApiCreditReportPredictionResult  cpr = new ProxyApiCreditReportPredictionResult ();
        ProxyApiCreditReportClassProbabilities cp1 = new ProxyApiCreditReportClassProbabilities();
        ProxyApiCreditReportClassProbabilities cp2 = new ProxyApiCreditReportClassProbabilities();
        List<ProxyApiCreditReportClassProbabilities> cpList = new List<ProxyApiCreditReportClassProbabilities>();
        Test.stopTest();
        cpr.labelIndex = 'Test index';
        cpr.label = 'Test label';
        
        cp1.innerValue = 'Test value1';
        cpList.add(cp1);
        cp2.innerValue = 'Test value2';
        cpList.add(cp1);
        
        cpr.classProbabilities = cpList;
        cpr.predictionErrorMessage = 'Test errorMessage';
        cpr.predictionErrorDetails = 'Test errorDetails';
        
        System.assertEquals('Test index', cpr.labelIndex);
        System.assertEquals('Test label', cpr.label);
        System.assertEquals(cpList, cpr.classProbabilities);
        System.assertEquals('Test errorMessage', cpr.predictionErrorMessage);
        System.assertEquals('Test errorDetails', cpr.predictionErrorDetails);
        
     }
}