@isTest private class ClearCourtResultsTest {
	@isTest static void validates() {
        ClearCourtResults ccr = new ClearCourtResults();
        
        ccr.openTaxLiens = 0;
        ccr.closedTaxLiens = 0;
        ccr.openJudgements = 0;
        ccr.closedJudgements = 0;
        ccr.totalLiensJudgements = 0;
        ccr.noteBody = 'The note body';
        ClearCourtReportData data = new ClearCourtReportData();
        ccr.clearCourtReportData = new List<ClearCourtReportData>();
        ccr.clearCourtReportData.add(data);

        System.assert(ccr.ClearCourtReportData.size() > 0);
        System.assertEquals(0, ccr.openTaxLiens);
        System.assertEquals(0, ccr.closedTaxLiens);
        System.assertEquals(0, ccr.openJudgements);
        System.assertEquals(0, ccr.closedJudgements);
        System.assertEquals(0, ccr.totalLiensJudgements);
        System.assertEquals('The note body', ccr.noteBody);
    }
}