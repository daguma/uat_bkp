@isTest class ContractGenerationCtrlTest {
    static testMethod void test() {
    	Contact con = new Contact(LastName='TestContact',Firstname = 'David',Email = 'test@gmail2.com',MailingState = 'GA');
        insert con;

        Opportunity app = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id);
        insert app;

    	ContractGenerationCtrl.getPicklistValues(app);
    }
}