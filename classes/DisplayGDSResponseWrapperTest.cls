@isTest public class DisplayGDSResponseWrapperTest {
    @isTest static void validate() {
        
        DisplayGDSResponseWrapper disWrapper = new DisplayGDSResponseWrapper ();
        
        disWrapper.ruleNumber = 'test data';
        system.assertEquals('test data', disWrapper.ruleNumber);
        
        disWrapper.ruleName = 'test data';
        system.assertEquals('test data', disWrapper.ruleName);
        
        disWrapper.isSoftPass = true;
        system.assertEquals(true, disWrapper.isSoftPass);
        
        disWrapper.isHardPass = true;
        system.assertEquals(true, disWrapper.isHardPass);
        
        disWrapper.ruleCategory = 'test data';
        system.assertEquals('test data', disWrapper.ruleCategory);
        
        disWrapper.description = 'test data';
        system.assertEquals('test data', disWrapper.description );
        
        disWrapper.actionInstructions = 'test data';
        system.assertEquals('test data', disWrapper.actionInstructions );
        
        disWrapper.rulePriority = 333;
        system.assertEquals(333, disWrapper.rulePriority );
        
        disWrapper.applicationRisk = 4;
        system.assertEquals(4, disWrapper.applicationRisk );
        
        disWrapper.showSoftPullEmptyBox = true;
        system.assertEquals(true, disWrapper.showSoftPullEmptyBox );
        
        disWrapper.showHardPullEmptyBox = true;
        system.assertEquals(true, disWrapper.showHardPullEmptyBox );
        
        disWrapper.contractStatus = 'test data';
        system.assertEquals('test data', disWrapper.contractStatus );
        
        disWrapper.sequenceCategory = 'test data';
        system.assertEquals('test data', disWrapper.sequenceCategory );
        
        disWrapper.approvedBy = 'test data';
        system.assertEquals('test data', disWrapper.approvedBy );
        
        disWrapper.colorCode = 'test data';
        system.assertEquals('test data', disWrapper.colorCode );
        
        disWrapper.deQueueAssignment = 'test data';
        system.assertEquals('test data', disWrapper.deQueueAssignment);
        
        string approvedByName = disWrapper.approvedByDisplayText;
        system.assertEquals('test data', approvedByName );
        
        disWrapper.softPullRuleValue = 'test data';
        system.assertEquals('test data', disWrapper.softPullRuleValue);
        
        disWrapper.hardPullRuleValue = 'test data';
        system.assertEquals('test data', disWrapper.softPullRuleValue);
        
    }
}