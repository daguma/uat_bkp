@isTest public class BatchOnDelayedOnboardingProgressTest {
    
    @isTest static Void testDelayedLongForm(){
        Lead l = new Lead();
        l.FirstName = 'Sample fName';
        l.LastName = 'Sample lName';
        l.Company = 'Test Company';
        l.Phone = '9876545674';
        l.email = 'test.test@gmail.com';
        l.Step_1_Information__c = true;
        l.Step_2_Information__c = false;
        l.LeadSource = 'Invisalign';
        l.createdDate = system.now().addHours(-1).addMinutes(-10);
        insert l;
        
        Test.startTest();
        
        BatchOnDelayedOnboardingProgress job = new BatchOnDelayedOnboardingProgress();
        database.executebatch(job);
        
        Test.stopTest();
    }
}