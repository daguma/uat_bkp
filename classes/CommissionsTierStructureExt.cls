public with sharing class CommissionsTierStructureExt {
    // Declarations
    public string selectedParentName{get;set;}
    public string selectedChildName{get;set;}
    public Commissions_Structure__c cs{get;set;}
    public string disSchValue{get;set;}
    public String parentComm{get;set;}
    public String childComm{get;set;}
    public Commissions_Structure__c clonedCommStr;
    public List<Tier_Structure__c> tiersList{get;set;}
    public Integer rowNum{get;set;}
    public List<Tier_Structure__c> existingTiersList{get;set;}
    public static String nullStr = 'none';
    public static string actType = 'Partner';
    public String Cloneyes;
    public Map<String,String> actMap = new Map<String,String>();
    public String oldParent;
    public String oldChild;
    
    
    public CommissionsTierStructureExt(ApexPages.StandardController controller) {
        // get current record
        cs = (Commissions_Structure__c)controller.getRecord();
        // get record id from url
        disSchValue = '0';
        selectedParentName = nullStr;
        selectedChildName = nullStr;
        parentComm = '0';
        childComm = '0';
        // list to collect tier structure records
        tiersList = new List<Tier_Structure__c>();
        existingTiersList = new List<Tier_Structure__c>();
        If(cs.Id <> null && String.isNotBlank(cs.Id)){
            cs = [Select Id,Parent_Partner__c,Child_Partner__c,Non_Split__c,Parent_Partner_Commission_Value__c,Child_Partner_Commission_Value__c,Name,Commission_Structure_Status__c,
                  Commission_Structure_Start_Date__c, Commission_Structure_End_Date__c, Commission_Payment_Frequency__c, Commission_Scheme_Indicator__c, Commission_Tier_Indicator__c,
                  Commission_Cost_Base__c,Commission_Scheme_Value__c   ,Parent_Partner_Name__c, Tier_Range_Indicator__c,Child_Partner_Name__c
                  from Commissions_Structure__c 
                  where id =: cs.Id];
            system.debug('cs =============' +cs);
            /*If(cs.Commission_Tier_Indicator__c == true)
                disSchValue = '0';
            else
                disSchValue = String.valueOf(cs.Commission_Scheme_Value__c);
            */   
            // set page parameters with existing record values
            If(cs.Non_Split__c == false){
                If(String.isNotBlank(cs.Parent_Partner__c))
                    selectedParentName = cs.Parent_Partner__c;
                If(String.isNotBlank(cs.Child_Partner__c))
                    selectedChildName = cs.Child_Partner__c;
                If(cs.Parent_Partner_Commission_Value__c <> null)
                    parentComm = String.valueOf(cs.Parent_Partner_Commission_Value__c);
                If(cs.Child_Partner_Commission_Value__c <> null)
                    childComm = String.valueOf(cs.Child_Partner_Commission_Value__c);
            } 
            If(Cloneyes == null){
                existingTiersList = [Select Id, Name,Commissions_Structure__c,Commission_Scheme_Value__c, Maximum_Range_Value__c, Minimum_Range_Value__c
                                     from Tier_Structure__c where Commissions_Structure__c =: cs.Id order by Minimum_Range_Value__c asc];
                If(!existingTiersList.isEmpty()){
                    tiersList = existingTiersList;
                }
            }
        }
    }
    
    public pageReference updateCommStr(){
        system.debug('selectedChildName on action ===' +selectedChildName);
        List<Account> childPartnerIns = [Select Id from Account 
                                        where Id =: selectedChildName AND (Is_Child_Partner__c = false OR Type <>: actType OR Account__c <>: selectedParentName)  
                                        LIMIT 1];
        system.debug('selectedParentName on action ===' +selectedParentName);
        List<Account> parentPartnerIns = [Select Id from Account 
                                        where Id =: selectedParentName AND (Is_Parent_Partner__c = false OR Type <>: actType)  
                                        LIMIT 1];
        If(!childPartnerIns.isEmpty() || !parentPartnerIns.isEmpty()){
            cs.Parent_Partner__c = null;
            cs.Child_Partner__c = null;
            cs.Parent_Partner_Commission_Value__c = null;
            cs.Child_Partner_Commission_Value__c = null;
            cs.Non_Split__c = true;
            selectedParentName = nullStr;
            parentComm = null;
            childcomm = null;
            update cs;
        }
        return null;
    }
    
    
    // Method to add a row for tier structure
    public void AddRow(){    
        Tier_Structure__c tierIns = new Tier_Structure__c();
        tiersList.add(tierIns);
    }
    
    // Method to delete a row for tier structure
    public void delRow(){   
        rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
        tiersList.remove(rowNum);  
        String teirId = apexpages.currentpage().getparameters().get('tierId');
        If(String.isNotBlank(teirId) ){
            
            try{
                If(String.isNotBlank(teirId)){
                    Tier_Structure__c tierToDel = [Select Id from Tier_Structure__c where id =: teirId];
                    If(tierToDel <> null)
                    delete tierToDel;
                }
            }catch(Exception e){
                system.debug('Exception in Delete Operation for tiers ===' +e.getMessage());
            }
        }
    }
    
    // Method called on click of Cancel Button
    public pageReference cancelAndRedirectToListView(){
        If(tiersList.isEmpty() && cs.Commission_Tier_Indicator__c){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please add Tier structure or uncheck Commission Tier Indicator');
            ApexPages.addMessage(msg);
            return null;
        }
        Schema.DescribeSObjectResult r = Commissions_Structure__c.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix();
        //System.debug('Printing --'+keyPrefix );
        pageReference pg = new pageReference('/'+keyPrefix+'/o');
        return pg;
    }
    
    // Method to show parent partners on VF
    public List<SelectOption> getParentPartners(){
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new selectOption(nullStr, 'Select Parent Partner')); 
        List<Account> parentActs = [Select Id,Name from Account where Is_Parent_Partner__c = true AND Type =: actType];
        for(Account parent : parentActs)        
        {    
            options.add(new selectOption(parent.Id,parent.Name));      
            actMap.put(parent.Id,parent.Name);              
        }
        return Options; 
    }
    
    // Method to show child partners on VF
    public List<SelectOption> getChildPartners(){
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new selectOption(nullStr, 'Select Child Partner')); 
        If(String.isNotBlank(selectedParentName) || selectedParentName <> nullStr){
            List<Account> childActs = [Select Id,Name from Account 
                                       where Is_Child_Partner__c = true AND Account__c <> null AND Account__c =: selectedParentName AND Type =: actType];
            for (Account child : childActs) { 
                options.add(new selectOption(child.Id, child.Name)); 
                actMap.put(child.Id,child.Name);
            }
        }
        return options; 
    }
    
    // Method called on click of Save Button
    public pageReference saveCommStr(){
        pageReference pg;
        If(ApexPages.currentPage().getParameters().get('clone') <> null && clonedCommStr <> null){
            clonedCommStr.Name = cs.Name;
            
            List<Commissions_Structure__c> existingComm = [Select id,name from Commissions_Structure__c where name =: clonedCommStr.name];
            If(!existingComm.isEmpty()){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Commission Structure with the same name already exist. Please enter a different name for commission structure');
                ApexPages.addMessage(msg);
                return null;
            }else{
            try{
                insert clonedCommStr;
            }catch(Exception e){
                system.debug('Exception in Clone =====' +e.getMessage());
            }
            }
            List<Tier_Structure__c> clonedTiers = new List<Tier_Structure__c>();
            If(!existingTiersList.isEmpty()){
                for(Tier_Structure__c tier : existingTiersList){
                     Tier_Structure__c tierIns = new Tier_Structure__c();
                     tierIns.Name = tier.Name;
                     tierIns.Commission_Scheme_Value__c = tier.Commission_Scheme_Value__c;
                     tierIns.Maximum_Range_Value__c = tier.Maximum_Range_Value__c;
                     tierIns.Minimum_Range_Value__c = tier.Minimum_Range_Value__c;
                     tierIns.Commissions_Structure__c = clonedCommStr.Id;
                     clonedTiers.add(tierIns);
                }
                try{
                    insert clonedTiers;
                }catch(Exception e){
                    system.debug('Exception in Cloned Tiers =====' +e.getMessage());
            }
            }
            pg = new pageReference('/'+clonedCommStr.Id);
            pg.setRedirect(true);
        }
        else{        
            If(cs.Commission_Structure_End_Date__c  <= cs.Commission_Structure_Start_Date__c){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Commission Structure End Date & Time should be greater than its Start Date & Time');
                ApexPages.addMessage(msg);
                return null;
            }
            system.debug('cs.Non_Split__c =======' +cs.Non_Split__c);
            If(!cs.Non_Split__c){
                If(selectedParentName == nullStr){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please select Parent partner');
                    ApexPages.addMessage(msg);
                    return null;
                }else{
                    cs.Parent_Partner__c = selectedParentName;
                    If(actMap.containsKey(selectedParentName) )
                        cs.Parent_Partner_Name__c = actMap.get(selectedParentName);
                    
                }
                If(selectedChildName == nullStr){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please select Child partner');
                    ApexPages.addMessage(msg);
                    return null;
                }else{
                    cs.Child_Partner__c =  selectedChildName; 
                    If(actMap.containsKey(selectedChildName) )
                        cs.Child_Partner_Name__c = actMap.get(selectedChildName);
                }
                
                If(cs.Parent_Partner_Commission_Value__c == null || cs.Parent_Partner_Commission_Value__c <=0){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please set correct percentage of commission for Parent partner');
                    ApexPages.addMessage(msg);
                    return null;
                }
                If(cs.Child_Partner_Commission_Value__c == null || cs.Child_Partner_Commission_Value__c <= 0){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please set correct percentage of commission for Child partner');
                    ApexPages.addMessage(msg);
                    return null;
                }
                
                If(cs.Parent_Partner_Commission_Value__c > 0 && cs.Child_Partner_Commission_Value__c > 0 && cs.Child_Partner_Commission_Value__c+cs.Parent_Partner_Commission_Value__c <> 100){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Sum of Split Commission Indicator for Parent partner & child partner should be 100%');
                    ApexPages.addMessage(msg);
                    return null;
                }
                
            }else{
                 cs.Parent_Partner_Commission_Value__c = null;
                 cs.Parent_Partner_Name__c = null;
                 cs.Child_Partner_Name__c = null;
                 cs.Child_Partner_Commission_Value__c = null;
                 cs.Parent_Partner__c = null;
                 cs.Child_Partner__c = null;
            }
            If(cs.Commission_Tier_Indicator__c){
                
                If(tiersList.isEmpty() ){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please add Tier structure or uncheck Commission Tier Indicator');
                    ApexPages.addMessage(msg);
                    return null;
                }
                else if(cs.Commission_Cost_Base__c!='' && cs.Commission_Cost_Base__c!='Cost per Funding'){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Commission Tier Structure for Commission Cost base: '+cs.Commission_Cost_Base__c+' is only allowed');
                    ApexPages.addMessage(msg);
                    return null;                
                }
                else{
                    Map<String,Tier_Structure__c> rangeMap = new Map<String,Tier_Structure__c>();
                    for(Tier_Structure__c tier : tiersList){
                        If(tier.Commission_Scheme_Value__c <= 0){
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify Commission Tier Structure : Incorrect Commission Scheme Value for Tier '+tier.Name+'. It should be greater than zero');
                            ApexPages.addMessage(msg);
                            return null;
                        }

                        If(tier.Minimum_Range_Value__c <=0){
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify Commission Tier Structure : Incorrect Minimum Range Value for Tier '+tier.Name+'.  It should not be greater than zero');
                            ApexPages.addMessage(msg);
                            return null;
                        }

                        /* 4613 change - comment these lines
                        
                        string minPoints=String.Valueof(tier.Minimum_Range_Value__c).substring(String.Valueof(tier.Minimum_Range_Value__c).length()-2);
                        string maxPoints=String.Valueof(tier.Maximum_Range_Value__c).substring(String.Valueof(tier.Maximum_Range_Value__c).length()-2);
                    
                        If(cs.Tier_Range_Indicator__c=='Count' && (Integer.ValueOf(maxPoints)!=0)){
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify Commission Tier Structure :Incorrect  Maximum Range Value for Tier: '+tier.Name+'.  It should not contain decimal values for Commission Tier Indicator: '+cs.Tier_Range_Indicator__c);
                            ApexPages.addMessage(msg);
                            return null;
                        }
                        If(cs.Tier_Range_Indicator__c=='Count' && (Integer.ValueOf(minPoints) !=0)){
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify Commission Tier Structure : Incorrect Minimum Range Value for Tier: '+tier.Name+'.  It should not contain decimal values for Commission Tier Indicator: '+cs.Tier_Range_Indicator__c);
                            ApexPages.addMessage(msg);
                            return null;
                        } */
                        system.debug('=Commission_Scheme_Indicator__c =' + cs.Commission_Scheme_Indicator__c );
                        If(cs.Commission_Scheme_Indicator__c == 'Percentage' && tier.Commission_Scheme_Value__c > 100){
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify Commission Tier Structure : Incorrect Commission Scheme Value for Tier '+tier.Name+'.  It should not be greater than 100%');
                            ApexPages.addMessage(msg);
                            return null;
                        }
                        If(tier.Minimum_Range_Value__c >= tier.Maximum_Range_Value__c){
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify Commission Tier Structure : Incorrect minimum and maximum range values for Tier '+tier.Name+'. Maximum Range Value should be greater than Minimum Range Value');
                            ApexPages.addMessage(msg);
                            return null;
                        }
                        String rangeStr = tier.Minimum_Range_Value__c+','+tier.Maximum_Range_Value__c;
                        If(rangeMap.isEmpty() ){
                            rangeMap.put(rangeStr,tier);
                        }else{
                            If(rangeMap.containsKey(rangeStr)){
                                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify Commission Tier Structure: Duplicate Range ');
                                ApexPages.addMessage(msg); 
                                return null;
                            }else{
                                rangeMap.put(rangeStr,tier);
                            }
                        }
                    }
                    Set<String> tierNameSet = new Set<String>();
                    for(String range : rangeMap.keySet()){
                        If(tierNameSet.isEmpty()){
                            tierNameSet.add(rangeMap.get(range).Name);
                        }else{
                            If(tierNameSet.contains(rangeMap.get(range).Name)){
                                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify Commission Tier Structure: Duplicate Tier Names');
                                ApexPages.addMessage(msg); 
                                return null;
                            }else{
                                tierNameSet.add(rangeMap.get(range).Name);
                            }
                        }
                    }
                    system.debug('tiersList =====' +tiersList);
                    system.debug('rangeMap =====' +rangeMap);
                    for(Tier_Structure__c tier : tiersList){
                        String rangeStr = tier.Minimum_Range_Value__c+','+tier.Maximum_Range_Value__c;
                        system.debug('rangeStr =====' +rangeStr);
                        for(String range : rangeMap.keySet()){
                            system.debug('range keyset=====' +range);
                            If(rangeStr <> range){
                                List<String> parts = range.split('\\,');
                                decimal min = decimal.valueOf(parts[0]);
                                decimal max = decimal.valueOf(parts[1]);

                                If(min >= tier.Minimum_Range_Value__c && min <= tier.Maximum_Range_Value__c){
                                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify commission Tier Structure: Incorrect Range for Tier '+rangeMap.get(range).Name);
                                    ApexPages.addMessage(msg); 
                                    return null;
                                }



                                system.debug('min ====' +min);
                                system.debug('tier.Minimum_Range_Value__c =========' +tier.Minimum_Range_Value__c);
                                If(min < tier.Minimum_Range_Value__c){
                                    If(rangeMap.get(range).Commission_Scheme_Value__c >= tier.Commission_Scheme_Value__c){
                                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify commission Tier Structure: Incorrect Commission Scheme Value for Tier '+rangeMap.get(range).Name);
                                        ApexPages.addMessage(msg); 
                                        return null;
                                    }
                                }else{
                                    If(rangeMap.get(range).Commission_Scheme_Value__c <= tier.Commission_Scheme_Value__c){
                                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Verify commission Tier Structure: Incorrect Commission Scheme Value for Tier '+rangeMap.get(range).Name);
                                        ApexPages.addMessage(msg); 
                                        return null;
                                    }
                                }
                            }
                        }
                    }
                }
                cs.Commission_Scheme_Value__c = 0;
            }
            else{

                system.debug('=else Commission_Scheme_Indicator__c =' + cs.Commission_Scheme_Indicator__c );
                If(cs.Commission_Scheme_Indicator__c == 'Percentage' && (cs.Commission_Cost_Base__c=='Cost per Credit Approval' || cs.Commission_Cost_Base__c=='Cost per Lead')){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Incorrect Commission Scheme Value. The Commission Scheme Indicator as Percentage for Commission Cost Base: '+cs.Commission_Cost_Base__c+' is not allowed');
                    ApexPages.addMessage(msg);
                    return null;  
                } 
                If(cs.Commission_Scheme_Value__c <= 0){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Incorrect Commission Scheme Value. It should be greater than zero');
                    ApexPages.addMessage(msg);
                    return null;
                }

                If(cs.Commission_Scheme_Indicator__c == 'Percentage' && cs.Commission_Scheme_Value__c > 100){

                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Incorrect Commission Scheme Value. It should not be greater than 100%');
                    ApexPages.addMessage(msg);

                    return null;
                }
                
                system.debug(cs.Commission_Scheme_Indicator__c);system.debug(cs.Commission_Scheme_Value__c );
                If(cs.Commission_Scheme_Indicator__c == null ) {
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Commission Scheme Indicator is required fields.');
                    ApexPages.addMessage(msg);
                    return null;
                }
                
                If(cs.Commission_Scheme_Value__c == null) {
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Commission Scheme Value is required fields.');
                    ApexPages.addMessage(msg);
                    return null;
                }
            }
            
            try {
                List<Commissions_Structure__c> existingComm ;
                If(cs.Id == null && String.isBlank(cs.Id)){
                    existingComm = [Select id,name from Commissions_Structure__c where name =: cs.name];
                }else{
                     existingComm = [Select id,name from Commissions_Structure__c where name =: cs.name AND id <>: cs.Id];
                }
                If(!existingComm.isEmpty()){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Commission Structure with the same name already exist. Please enter a different name for commission structure');
                    ApexPages.addMessage(msg);
                    return null;
                }
                
                upsert cs;
                If(!tiersList.isEmpty() && cs.Commission_Tier_Indicator__c){
                    for(Tier_Structure__c tier : tiersList){
                        If(tier.Commissions_Structure__c == null)
                           tier.Commissions_Structure__c = cs.Id; 
                    }
                    upsert tiersList;
                }
                system.debug('tiersList INSERT ================' +tiersList);
            }catch(Exception e){
                system.debug('Exception In Upsert ============'+e.getMessage());
            }
            pg = new pageReference('/'+cs.Id);
            pg.setRedirect(true);
        }
        return pg;
    }
    
    // Method to clear split values incase of non-split
    public pageReference clearSplitCommIndicator(){
        
        If(cs.Non_Split__c == true){
            oldParent = selectedParentName;
            oldChild = selectedChildName;
            selectedParentName = nullStr;
            selectedChildName = nullStr;
            parentComm = null;
            childComm = null;
        }else{
            selectedParentName = oldParent ;
            selectedChildName = oldChild ;
        }
        return null;
    }
    
    // Method to clone current commission structure
    public pageReference cloneCommStr(){
        Cloneyes = ApexPages.currentPage().getParameters().get('clone');
        pageReference pg;
        if(Cloneyes !=null){
            clonedCommStr = new Commissions_Structure__c();
            clonedCommStr.name = cs.Name;
            clonedCommStr.Commission_Structure_Status__c = cs.Commission_Structure_Status__c;
            clonedCommStr.Commission_Structure_Start_Date__c = cs.Commission_Structure_Start_Date__c;
            clonedCommStr.Commission_Structure_End_Date__c = cs.Commission_Structure_End_Date__c;
            clonedCommStr.Commission_Payment_Frequency__c = cs.Commission_Payment_Frequency__c;
            clonedCommStr.Commission_Scheme_Indicator__c = cs.Commission_Scheme_Indicator__c;
            clonedCommStr.Commission_Scheme_Value__c = cs.Commission_Scheme_Value__c;
            clonedCommStr.Commission_Cost_Base__c = cs.Commission_Cost_Base__c;
            clonedCommStr.Parent_Partner__c = cs.Parent_Partner__c;
            clonedCommStr.Parent_Partner_Name__c = cs.Parent_Partner_Name__c;
            clonedCommStr.Parent_Partner_Commission_Value__c = cs.Parent_Partner_Commission_Value__c;
            clonedCommStr.Child_Partner__c = cs.Child_Partner__c;
            clonedCommStr.Child_Partner_Name__c = cs.Child_Partner_Name__c;
            clonedCommStr.Child_Partner_Commission_Value__c = cs.Child_Partner_Commission_Value__c;
            clonedCommStr.Non_Split__c = cs.Non_Split__c;
            clonedCommStr.Commission_Tier_Indicator__c = cs.Commission_Tier_Indicator__c;
            clonedCommStr.Tier_Range_Indicator__c = cs.Tier_Range_Indicator__c;
        }
        return null;
    }
}