@isTest public class ProxyApiCreditReportEntityLiteTest {

    @isTest static void assigns_variables() {

        ProxyApiCreditReportEntityLite lite = new ProxyApiCreditReportEntityLite();
    
        DateTime myDateTime = DateTime.now();
    
        lite.id = 123;
        lite.appId = 'appId';
        lite.appName = 'APP-name';
        lite.isSoftPull = true;
        lite.datasourceName = 'datasource';
        lite.createdDate = myDateTime.getTime();
        lite.errorMessage = 'No Error Message';
        lite.decision = true;
        lite.sequenceName = 'Account Management';
        lite.sequenceId = '10';
    
        System.assertEquals('CR-00000123', lite.name);
    
    
        lite.id = null;
    
        System.assertEquals(' - ', lite.name);
    
        System.assertEquals('Yes', lite.automatedDecision);
    
        lite.decision = false;
    
        System.assertEquals('No', lite.automatedDecision);
            
        lite.sequenceId = null;
        
        System.assertEquals(' - ', lite.sequenceName);
        
        
        lite.sequenceId = 'test';
        
        System.assertEquals('test', lite.sequenceId);
            
    
        System.assertEquals(Datetime.newInstance(lite.createdDate), lite.creationDate);
        System.assertEquals(Datetime.newInstance(lite.createdDate), lite.getCreatedDate());
        System.assertEquals(true, lite.isCreateDateOnLast30Days());
    
        lite.createdDate = null;
    
        System.assertEquals(null, lite.creationDate);
        System.assertEquals(null, lite.getCreatedDate());
        System.assertEquals(false, lite.isCreateDateOnLast30Days());
    
        System.assertEquals(true, lite.errorResponse);
    
        lite.errorMessage = null;
    
        System.assertEquals(false, lite.errorResponse);
    
        System.assertNotEquals(null, lite.appUrl);
    
        myDateTime = DateTime.now().addDays(-35);
    
        lite.createdDate = myDateTime.getTime();
    
        System.assertEquals(false, lite.isCreateDateOnLast30Days());

    }
}