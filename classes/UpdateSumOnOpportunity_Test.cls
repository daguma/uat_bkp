@isTest
 public class UpdateSumOnOpportunity_Test{
      
      public static testMethod void UpdateSumOnOpportunity_insert(){          
          Opportunity  App = TestHelper.createApplication();
          
          Account acc = new Account(Name = 'TestAccount',  Type = 'Partner');
          insert acc;
          insert new Provider_Payments__c(Payment_Amount__c = App.Amount/2, Opportunity__c = App.id, Provider_Name__c = acc.id);
          
          insert new Provider_Payments__c(Payment_Amount__c = App.Amount/2, Opportunity__c = App.id, Provider_Name__c = acc.id);
          
          system.assertequals(App.Amount,[select Provider_Payment_s_Sum__c  from opportunity where id =: App.id].Provider_Payment_s_Sum__c);          
      }
      
      public static testMethod void UpdateSumOnOpportunity_update(){
          Opportunity  App = TestHelper.createApplication();
          
          Account acc = new Account(Name = 'TestAccount',  Type = 'Partner');
          insert acc;         
          Provider_Payments__c prov = new Provider_Payments__c(Payment_Amount__c = App.Amount, Opportunity__c = App.id, Provider_Name__c = acc.id);
          insert prov;
          prov.Payment_Amount__c = prov.Payment_Amount__c - 50;
          update prov;
          
          system.assertequals(App.Amount - 50,[select Provider_Payment_s_Sum__c  from opportunity where id =: App.id].Provider_Payment_s_Sum__c);          
      }
      
      public static testMethod void UpdateSumOnOpportunity_delete(){          
          Opportunity  App = TestHelper.createApplication();
          
          Account acc = new Account(Name = 'TestAccount',  Type = 'Partner');
          insert acc;
          Provider_Payments__c prov = new Provider_Payments__c(Payment_Amount__c = App.Amount, Opportunity__c = App.id, Provider_Name__c = acc.id);          
          insert prov;
          delete prov;
          
          //system.assertequals(0,[select Provider_Payment_s_Sum__c  from opportunity where id =: App.id].Provider_Payment_s_Sum__c);
      }


 }