global class ProxyApiCreditReportScoringModelEntity {

    public String modelName{get;set;}
    public String grade{get;set;}
    public List<ProxyApiCreditReportCoefficientEntity> coefficientsList{get;set;}
    public String dollarRateBadProb{get;set;}
    public String unitBadProb {get;set;}
    public String score {get;set;}
}