@isTest
public class PayOffQuoteTriggerTest {

    @isTest static void testPayOffGeneration() {

        loan__Loan_Account__c cl = TestHelper.createContract();
        cl.loan__Disbursal_Date__c = System.today();
        cl.loan__Loan_Status__c = 'Active - Good Standing';
        cl.loan__Pmt_Amt_Cur__c = 200;
       // cl.loan__Interest_Rate__c = 26.20;
        cl.Next_Payment_is_Additional_Payment__c = true;
        update cl;
        TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.createSeedDataForTesting();

        loan__Payment_Mode__c pMode = [select Id from loan__Payment_Mode__c where Name = 'ACH' order by CreatedDate desc limit 1];

		loan__Payoff_Quote__c pq = new loan__Payoff_Quote__c();
        pq.loan__Loan_Account__c = cl.Id; 
        pq.loan__Poq_Total_Payoff_Amount__c = 100;
        pq.loan__Poq_Interest_Per_Diem__c = 5;
        pq.loan__Poq_valid_till_Dt__c = Date.today().addDays(10);
        insert pq;
         
        System.debug('Testing should have been succesful!');
  
    }
}