@isTest public class DecisioningAttributeEntityMapperTest {

    @isTest static void maps_attributes () {
        Opportunity app = LibraryTest.createApplicationTH();
        ProxyApiCreditReportEntity creditReportEntity = LibraryTest.fakeCreditReportEntity(app.Id);

        List<DecisioningAttributeEntity> result = DecisioningAttributeEntityMapper.mapAttributes(creditReportEntity, creditReportEntity, 'Refinance');

        System.assert(result.size() > 0);
    }

}