@isTest
public class LoanDisbursalSummarizedByLineTest {
    public static testMethod void testFileCreation(){ 
        
        
        //Load csv file
        Test.loadData(filegen__File_Metadata__c.sObjectType, 'LendingFilegen');
        
        //Setup seed data
        TestHelperForManaged.createSeedDataForTesting();
        //TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupMetadataSegments();
        loan.TestHelper.systemDate = Date.newInstance(2014, 3, 3); // Mar 3 - Monday
        
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
        
        loan__Transaction_Approval_Config__c c = new loan__Transaction_Approval_Config__c();
        c.loan__Payment__c = true;
        c.loan__Payment_Reversal__c = false;
        c.loan__Funding__c = false;
        c.loan__Funding_Reversal__c = false;
        c.loan__Write_off__c = false;
        c.loan__Accounting__c = false;
        
        insert c;
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                                                                          dummyAccount, 
                                                                          curr, 
                                                                          dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = TestHelperForManaged.createLoanPurpose();
        
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        b1.peer__National_Id__c = '1223';
        update b1;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Advance Trust Account');
        
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
        
        //Create a dummy Loan Account
        loan__Loan_Account__c dummylaMonthly =  LibraryTest.createContractTH();
        dummylaMonthly.loan__Loan_Status__c = 'Approved';
        dummylaMonthly.loan__Borrower_ACH__c = dummyBank.Id;
        dummylaMonthly.loan__ACH_On__c = true;
        dummylaMonthly.loan__ACH_Frequency__c = 'Monthly';
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = Date.today().addMonths(1);
        dummylaMonthly.loan__ACH_Start_Date__c = Date.today();
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__Ach_Debit_Day__c = 3;
        dummylaMonthly.loan__ACH_End_Date__c = Date.today().addYears(1);
        Update dummylaMonthly;
        
        loan__Payment_Mode__c payModDisbusral = [Select id,Name from loan__Payment_Mode__c where Name='ACH'];
        
        //Create a Loan Disbursal Transaction
        loan__Loan_Disbursal_Transaction__c loanDis = new loan__Loan_Disbursal_Transaction__c(loan__Loan_Account__c = dummylaMonthly.Id,
                                                                                              loan__Mode_of_Payment__c=payModDisbusral.id,
                                                                                              loan__Disbursed_Amt__c= 10000,
                                                                                              loan__Cleared__c = true
                                                                                             );
        insert loanDis;
        //update dummylaMonthly;
        
        
        System.debug('Payment mode: '+loanDis.loan__Mode_of_Payment__c+'  :: name:: '+payModDisbusral.name+'  :: id::'+payModDisbusral.id);
        System.debug('Loan Status: '+dummylaMonthly.loan__Loan_Status__c);
        system.debug('disbursement cleared:'+loanDis.loan__Cleared__c);
        test.startTest();
        loan.LoanDisbursalTxnSweepToACHJob j = new loan.LoanDisbursalTxnSweepToACHJob(false);
        Database.executeBatch(j);
        test.stopTest();
        
        
        loan__Loan_Disbursal_Transaction__c loanDisTxn = [select id,name,
                                                          loan__Disbursed_Amt__c,
                                                          loan__ACH_Filename__c,
                                                          loan__Sent_To_ACH__c
                                                          from loan__Loan_Disbursal_Transaction__c 
                                                          where loan__Loan_Account__c =: dummylaMonthly.id]; 
        
        System.debug('pmt txn: '+ loanDisTxn);
        loan__ACH_Parameters__c acha = loan__ACH_Parameters__c.getInstance();
        
        acha.Finwise_Origin_ABA__c = '123456789';
        acha.Finwise_Origin_Name__c = 'BNAME';
        acha.Finwise_Origin_Number__c = '123456789';
        acha.FINWISE_debit_Account__c = 123459223;
        acha.Disbursal_File_Cutoff_Hour__c = 20;
        System.debug('ACH Status: '+loanDisTxn.loan__Sent_To_ACH__c);
        System.debug('file name: '+loanDisTxn.loan__ACH_Filename__c);
        System.assertEquals('Shared Folder for ACH', acha.loan__Folder_Name__c);
        upsert acha;
        
        List<loan__Loan_Disbursal_Transaction__c> p = [SELECT Id FROM loan__Loan_Disbursal_Transaction__c];
        System.assert(p.size() > 0 );
        
        LoanDisbursalSummarizedByLine t = new LoanDisbursalSummarizedByLine();
        LoanDisbursalSummarizedByLine.executeSummarizedLineFile();
        //t.executeSummarizedLineFile();
        
        LoanDisbursalSummarizedFinwiseJob ft = new LoanDisbursalSummarizedFinwiseJob();
        ft.execute(null);

        LoanDisbursalSummarizedRegularJob rt = new LoanDisbursalSummarizedRegularJob();
        rt.execute(null);

        LoanDisbursalSummarizedFEBJob et = new LoanDisbursalSummarizedFEBJob();
        et.execute(null);


        List<loan__Loan_Disbursal_Transaction__c> lt = [select Id from loan__Loan_Disbursal_Transaction__c 
                                                  ];
        t.generateFile(lt, 'REG', '', false);
        
        
        List<Document> d = [Select Id from Document where CreatedDate = TODAY];
        System.assert(d.size() > 0, 'Documents were created!');        
        
        
    }
}