public with sharing class FactorTrustUtil {

    static String getValue(String value) {
        return value == null ? '' : value;
    }

    static Contact getContact(String cid) {
        return [SELECT Id, 
                            Loan_Amount__c, 
                            Annual_Income__c, 
                            MailingCountry, 
                            Employer_Name__c, 
                            SSN__c, 
                            Email, 
                            Birthdate, 
                            FirstName, 
                            LastName, 
                            MailingStreet, 
                            MailingPostalCode, 
                            MailingCity, 
                            MailingState, 
                            Phone_Clean__c, 
                            Factor_Trust_Verification__c,
                            Factor_Trust_Acceptance__c 
                    FROM    Contact 
                    WHERE   Id =: cid];
    }

    static Lead getLead(String lId) {
        return [SELECT Id,
                            Loan_Amount__c, 
                            Annual_Income__c,
                            Country, 
                            Employer_Name__c, 
                            SSN__c, 
                            Date_of_Birth__c, 
                            FirstName, 
                            LastName, 
                            Street, 
                            PostalCode, 
                            City, 
                            State, 
                            Phone_Clean__c, 
                            Email, 
                            LeadSource,
                            Factor_Trust_Verification__c,
                            Authorization_to_pull_credit__c,
                            IsConverted,
                            Factor_Trust_Acceptance__c,
                            Demyst_Email_Acceptance__c,
                            Demyst_Income_Acceptance__c,
                            Demyst_Employer_Acceptance__c, 
                            ConvertedContactID 
                    FROM    Lead 
                    WHERE   Id =: lId];
    }

    public static void createHeader(Xmlstreamwriter xmlW){
        Boolean isSandbox = FactorTrust.runningInASandbox();
        xmlW.writeStartElement(null,'LoginInfo',null);                      
            addNode(xmlW, 'Username', isSandbox ? Label.FT_Sandbox_UserName :  Label.FT_UserName);
            addNode(xmlW, 'Password', isSandbox ? Label.FT_Sandbox_Password : Label.FT_Password);
            addNode(xmlW, 'LenderIdentifier', isSandbox ? Label.FT_Sandbox_LenderID :  Label.FT_LEnderIdentifier);
            addNode(xmlW, 'Channelidentifier', isSandbox ? Label.FT_Sandbox_LenderID :  Label.FT_LEnderIdentifier);
            addNode(xmlW, 'MerchantIdentifier', isSandbox ? Label.FT_Sandbox_MerchantID :Label.FT_MerchantIdentifier);
            addNode(xmlW, 'StoreIdentifier', isSandbox ? Label.FT_Sandbox_StoreID :  Label.FT_StoreIdentifier);
        xmlW.writeEndElement();
    }

    public static string FTService(string LeadId, string contactId) {
        Map<String, String> RespMap = new Map<String, String>();
        RespMap.put('statusCode','200');
        RespMap.put('request','');
        RespMap.put('response', '');  
        RespMap.put('acceptance','Y'); 
        return JSON.SerializePretty(RespMap);
    }

    public static string FTStore2Service(string LeadId, string contactId,string applnId) {
        return FTStore2Servicewapper(LeadId, contactId, applnId, null, null);
    }

    public static string FTStore2Servicewapper(string LeadId, string contactId,string applnId, string accNo, string routNo) {
        Map<String, String> RespMap = new Map<String, String>();
        List<Opportunity> applist = new List<Opportunity>();

        Boolean FactorTrustDone = false, isSandbox = FactorTrust.runningInASandbox();
        
        String  ApplicationID = '',Phone = '', FirstName = '', LastName = '', Street = '',
                ZipCode = '', City = '', state = '', Email = '', Country = 'US', Msg='Failed',
                SSN = '', EmployerName = '', xmlResponse = '', xmlStringxmlRes, AccountNumber='', RoutingNumber='', 
                responseStatus, Response='N', appid, ReturnVal = FactorTrust.getConfigResult('factorTrust','AllowProceed');
        
        Date DOB = null;
        Decimal RequestedAmount = 0,  AnnualIncome = 0;
        Integer StatusCode = 200;
        try {
            if(String.isNotEmpty(contactId)) {
                Contact c = getContact(contactId);                
                ApplicationID = c.Id;
                Phone = getValue(c.Phone_Clean__c);
                FirstName = getValue(c.FirstName);
                LastName = getValue(c.LastName);
                Street = getValue(c.MailingStreet);
                ZipCode = getValue(c.MailingPostalCode);
                City = getValue(c.MailingCity);
                state = getValue(c.MailingState);
                Email = getValue(c.Email);
                DOB = c.Birthdate;
                SSN = getValue(c.SSN__c);
                EmployerName = getValue(c.Employer_Name__c);
                AnnualIncome = (c.Annual_Income__c == null) ? 0 : c.Annual_Income__c;
                RequestedAmount = (c.Loan_Amount__c == null) ? 0 : c.Loan_Amount__c;
                Country = String.isEmpty(c.MailingCountry) ? Country : c.MailingCountry;

                for (Opportunity app : [SELECT Id, 
                                                            ACH_Account_Number__c, 
                                                            ACH_Routing_Number__c 
                                                    FROM    Opportunity 
                                                    WHERE   id =: applnId 
                                                    LIMIT 1]) 
                {
                    RoutingNumber= !string.isEmpty(routNo)? routNo : getValue(app.ACH_Routing_Number__c);
                    appid= getValue(app.id);
                    AccountNumber= !string.isEmpty(accNo) ? accNo :getValue(app.ACH_Account_Number__c);
                }
            }
            
            if(String.isNotEmpty(leadId)) {
                Lead l = getLead(leadId);
                ApplicationID = l.id;
                Phone = getValue(l.Phone_Clean__c);
                FirstName = getValue(l.FirstName);
                LastName = getValue(l.LastName);
                Street = getValue(l.Street);
                ZipCode = getValue(l.PostalCode);
                City = getValue(l.City);
                state = getValue(l.State);
                Email = getValue(l.Email);
                DOB = l.Date_of_Birth__c;
                SSN = getValue(l.SSN__c);
                EmployerName = getValue(l.Employer_Name__c);
                AnnualIncome = (l.Annual_Income__c == null) ? 0 : l.Annual_Income__c;
                RequestedAmount = (l.Loan_Amount__c == null) ? 0 : l.Loan_Amount__c;
                Country = String.isEmpty(l.Country) ? Country : l.Country;
            }

            SSN = SSN.replace('-','').replace(' ','');
            if (String.isNotEmpty(contactId) || String.isNotEmpty(leadId)) {
                
                Xmlstreamwriter xmlW = new Xmlstreamwriter();
                xmlW.writeStartDocument('utf-8','1.0');
                xmlW.writeStartElement(null,'Application', null); 
                xmlW.writeStartElement(null,'LoginInfo',null);    
                addNode(xmlW, 'Username', isSandbox ? Label.FT_Store2_Sandbox_UserName :  Label.FT_Store2_UserName);
                addNode(xmlW, 'Password', isSandbox ? Label.FT_Store2_Sandbox_Password : Label.FT_Store2_Password);
                addNode(xmlW, 'Channelidentifier', '');
                addNode(xmlW, 'MerchantIdentifier', isSandbox ? Label.FT_Store2_Sandbox_MerchantID :Label.FT_Store2_MerchantIdentifier);
                addNode(xmlW, 'StoreIdentifier', isSandbox ? Label.FT_Store2_Sandbox_StoreID :  Label.FT_Store2_StoreIdentifier);     
                xmlW.writeEndElement();   

                xmlW.writeStartElement(null,'ApplicationInfo',null);                 
                addNode(xmlW, 'ApplicationID', ApplicationID);
                addNode(xmlW, 'MobileNumber', '');
                addNode(xmlW, 'FirstName', FirstName);
                addNode(xmlW, 'LastName', LastName);
                addNode(xmlW, 'EmailAddress', Email);
                addNode(xmlW, 'IPAddress', '');
                addNode(xmlW, 'DLNumber', '');
                addNode(xmlW, 'DLNumberIssueState', '');
                addNode(xmlW, 'DateOfBirth', (DOB == null) ? '' : DOB.format());
                addNode(xmlW, 'Address1', Street);
                addNode(xmlW, 'Address2', null);
                addNode(xmlW, 'City', City);
                addNode(xmlW, 'State', State);
                addNode(xmlW, 'Zip', ZipCode);
                addNode(xmlW, 'Country', Country);
                addNode(xmlW, 'MonthsAtAddress', '24');
                addNode(xmlW, 'AlternateZip', null);
                addNode(xmlW, 'HomePhone', Phone);
                addNode(xmlW, 'SSN', SSN);
                addNode(xmlW, 'SSNIssueState', '');
                addNode(xmlW, 'AccountABA', RoutingNumber);
                addNode(xmlW, 'AccountDDA', AccountNumber);
                addNode(xmlW, 'AccountType', 'Checking');    
                addNode(xmlW, 'EmployerName', EmployerName);
                addNode(xmlW, 'LengthOfEmployment', null);
                addNode(xmlW, 'MonthlyIncome', (AnnualIncome / 12 ).format().replace(',',''));
                addNode(xmlW, 'PayrollType', '');
                addNode(xmlW, 'PayrollFrequency', '');
                addNode(xmlW, 'PayrollGarnishment', '');
                addNode(xmlW, 'HasBankruptcy', '');
                addNode(xmlW, 'RequestedLoanAmount', RequestedAmount.format().replace(',',''));
                addNode(xmlW, 'RequestedEffectiveDate', Date.today().addDays(1).format());
                addNode(xmlW, 'RequestedDueDate', Date.today().addDays(31).format());
                addNode(xmlW, 'LeadType', null);   
                addNode(xmlW, 'ProductType', 'PER');
                addNode(xmlW, 'ECOACode', 'I');
                addNode(xmlW, 'PointOfOrigination', 'V');
                addNode(xmlW, 'PortfolioType', 'I');
                addNode(xmlW, 'SecuritizationType', 'U');
                addNode(xmlW, 'BlackBox', null);
                xmlW.writeEndElement();  
                xmlW.writeEndElement();                         
                xmlW.writeEndDocument(); 
                
                xmlStringxmlRes = xmlW.getXmlString();  
                xmlW.close();
                
                HTTPResponse res = doCall(isSandbox ? Label.FT_Store2_Sandbox_URL : Label.FT_Store_2_Scoring_URL, xmlStringxmlRes);
                
                StatusCode = res.GetStatusCode();     
                xmlResponse = res.GetBody();
            } 
            if(StatusCode == 200) {
                ReturnVal = 'Y'; 
                Msg = 'Passed';             
            } 
            RespMap.put('statusCode',string.valueOf(StatusCode));
            RespMap.put('acceptance',ReturnVal);
            RespMap.put('request',xmlStringxmlRes);            
            RespMap.put('response',getValue(xmlResponse));           

            if(String.isNotEmpty(xmlStringxmlRes))
                MelisaCtrl.DoLogging('FactorTrust', xmlStringxmlRes, xmlResponse, leadId, contactId); 
              
        } catch(Exception exp) {
            ReturnVal = FactorTrust.getConfigResult('factorTrust','AllowProceed');
            RespMap.put('Status','error'); 
            WebToSFDC.notifyDev('FactorTrustUtil FTService() ERROR', 'ERROR = ' + exp.getMessage() + '\n Line = ' + exp.getLineNumber() + '\n ' + exp.getStackTraceString());
            RespMap.put('acceptance',ReturnVal); 
        }

        return JSON.SerializePretty(RespMap);
    }
      
    public static HTTPResponse doCall(String URL, String xml) {
        HTTPResponse res = null;
        HttpRequest Req = new HttpRequest();
        req.setEndpoint(URL);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'text/xml'); 
        req.setBody(xml);
        req.setTimeout(120000);
        Http http = new Http();
        
        if(!Test.isRunningTest())
            res = http.send(req);
        else {
            res = new HttpResponse();
            res.setBody(xml);
        } 

        return res;
    }

    @Future(callout=true)
    public static void FactorTrustEWS(string appid) {
        try{
            system.debug('====EWS Call start===');
            if(String.isNotEmpty(appid)) {
                system.debug('====appId==='+appid);
                for (Opportunity app : [SELECT Contact__c, 
                                Id, 
                                FT_Status__c, 
                                FT_Description__c, Status__c 
                        FROM    Opportunity 
                        WHERE   Id =: appid]) {

                    if(String.isNotEmpty(app.Contact__c)) {
                        String FT_Response = FTStore2Service(null, app.Contact__c, appid);
                        system.debug('===FT_Response=='+FT_Response);
                        if(String.isNotEmpty(FT_Response)) {
                            UpdateEWSResponse(app, FT_Response);
                            system.debug('===application=='+app);
                             update app;
                        }
                    }         
                }
            }
        } catch(exception ex) {
            WebToSFDC.notifyDev('FactorTrustUtil.FactorTrustEWS ERROR', 'appid = ' + appid + '\n' + ex.getMessage() + ' line ' + (ex.getLineNumber()) + '\n' + ex.getStackTraceString());
        }
    }
     
    public static void addNode(Xmlstreamwriter xmlW, String Name, String pValue) {
        xmlW.writeStartElement(null,Name, null);
        if (pValue != null) 
            xmlW.writeCharacters(pValue);
        xmlW.writeEndElement();
    }

    public static void UpdateEWSResponse(Opportunity app,string FT_Response) {
        String leadId, FT_Status, Description, Response = null;
        if(app != null) {
            FT_Status = app.FT_Status__c;
            if(String.isNotEmpty(FT_Response)) {
                Map<String, String> fileMap = (Map<String, String>)JSON.deserialize(FT_Response, Map<String,String>.class); 
                
                if(fileMap.containsKey('statusCode') && fileMap.get('statusCode') == '200')
                    if (fileMap.containsKey('response'))Response = fileMap.get('response');
            }
        }
        if(String.isNotEmpty(Response)) {
            XmlStreamReader reader = new XmlStreamReader(Response);                                            
            while(reader.hasNext()) {
                if (reader.getEventType() == XmlTag.START_ELEMENT && 
                    (reader.getLocalName() == 'ResponseCodes' || reader.getLocalName() == 'DDAPlusResponse' )) 
                { 
                    if(reader.getLocalName() == 'DDAPlusResponse') {
                        reader.next();
                        if (reader.getEventType() == XmlTag.CHARACTERS)
                            Description=reader.getText();
                    }                    
                }
                reader.next();
            }
             
             
            if(String.isEmpty(Description) && FactorTrust.runningInASandbox())
                Description= Label.FT_Test_Description;
             
            if(String.isNotEmpty(Description)) {
                FT_Store2_ReponseDetail__c FT_ReponseDetails = FT_Store2_ReponseDetail__c.getInstance(Description);
                
                ///Testing Purposes
                if(FactorTrust.runningInASandbox() && (app.Id == '0064O00000jeLVSQA2' || app.Id == '0064O00000jeLV8QAM')){
                    if (FT_ReponseDetails == null){
                        FT_ReponseDetails = new FT_Store2_ReponseDetail__c();
                    }
                    FT_ReponseDetails.Status__c = app.Id == '0064O00000jeLVSQA2' ? 'Unknown' : 'Negative';
                    FT_Status = 'Negative';
                }
                
                System.debug('FT Response Details ' + FT_ReponseDetails);
                ///
                
                if(FT_ReponseDetails != null) {
                    String Status = FT_ReponseDetails.Status__c, Code = FT_ReponseDetails.Code__c;
                    
                    if(FT_Status == 'Negative' && (Status == 'Positive' || Status == 'Unknown')) {
                        Opportunity appl = [SELECT Contact__c,
                                                                (SELECT OldValue, 
                                                                        NewValue,
                                                                        CreatedDate 
                                                                FROM    Histories 
                                                                WHERE   Field = 'Status__c' 
                                                                ORDER BY CreatedDate DESC), 
                                                                FT_Status__c
                                                        FROM    Opportunity
                                                        WHERE   id = : app.id]; 
                                                 
                        if(appl.histories!= null && appl.histories.size() >= 0) {
                            for(OpportunityFieldHistory fundHistory : appl.histories) {
                                if(fundHistory.NewValue == 'Aged / Cancellation' || fundHistory.NewValue == 'Declined (or Unqualified)'){
                                    app.Status__c = String.valueOf(fundHistory.OldValue);
                                    app.Reason_of_Opportunity_Status__c = app.Status__c == 'Aged / Cancellation' ? 'Does Not Meet Program Parameters':'';
                                    
                                    break;
                                }
                            }  
                        }                                   
                    } 

                    if(Status == 'Negative' && app.Status__c  != 'Funded') {
                        app.Status__c = 'Aged / Cancellation';
                        app.Decline_Note__c = 'Failed EWS';
                        app.Reason_of_Opportunity_Status__c = 'Does Not Meet Program Parameters';
                    }
                    app.FT_Status__c=Status;
                    app.Ft_Code__c=Code;   
                    app.Ft_Description__c=Description;
                    
                }
            } 
        }        
    }
}