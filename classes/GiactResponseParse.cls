public class GiactResponseParse {
    public PostInquiryResponse PostInquiryResponse;
    
    public class PostInquiryResponse {
        public PostInquiryResult PostInquiryResult;
    }
    
    public class PostInquiryResult {
        public String AccountResponseCode;
        public String CreatedDate;
        public String CustomerResponseCode;
        public String ItemReferenceId;
        public String VerificationResponse;
        public String AccountAddedDate;
        public String AccountLastUpdatedDate;
        public String BankName;
    }
    
}