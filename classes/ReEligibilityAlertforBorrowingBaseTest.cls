@isTest
private class ReEligibilityAlertforBorrowingBaseTest {

    @isTest static void sends_email(){
        loan__loan_Account__c clContract = LibraryTest.createContractTH();
        clContract.First_Payment_Ineligibility_Cleared__c = false;
        update clContract;
        
        List<String> contractList = new List<String>();
        contractList.add(clContract.Id);
        
        createBill(clContract.Id, System.today().addDays(1), System.today().addDays(1), true, true);
        
        createBill(clContract.Id, System.today().addDays(2), System.today().addDays(2), true, true);

        createBill(clContract.Id, System.today().addDays(3), System.today().addDays(3), true, true);
        
        ReEligibilityAlertforBorrowingBase.checkEligibility(contractList);
        
       	System.assertEquals(true,[SELECT First_Payment_Ineligibility_Cleared__c FROM loan__Loan_Account__c WHERE Id =: clContract.Id LIMIT 1].First_Payment_Ineligibility_Cleared__c);
        System.assertNotEquals(null,[SELECT First_Payment_Ineligibility_Cleared_Date__c FROM loan__Loan_Account__c WHERE Id =: clContract.Id LIMIT 1].First_Payment_Ineligibility_Cleared_Date__c); 
    }
    
    @isTest static void not_eligible_for_borrowing_base_pay_later(){
        loan__loan_Account__c clContract = LibraryTest.createContractTH();
        clContract.First_Payment_Ineligibility_Cleared__c = false;
        update clContract;
        
        List<String> contractList = new List<String>();
        contractList.add(clContract.Id);
        
        createBill(clContract.Id, System.today().addDays(1), System.today().addDays(3), true, true);
        
        createBill(clContract.Id, System.today().addDays(2), System.today().addDays(2), true, true);

        createBill(clContract.Id, System.today().addDays(3), System.today().addDays(3), true, true);
        
        ReEligibilityAlertforBorrowingBase.checkEligibility(contractList);
        
       	System.assertEquals(false,[SELECT First_Payment_Ineligibility_Cleared__c FROM loan__Loan_Account__c WHERE Id =: clContract.Id LIMIT 1].First_Payment_Ineligibility_Cleared__c);
        System.assertEquals(null,[SELECT First_Payment_Ineligibility_Cleared_Date__c FROM loan__Loan_Account__c WHERE Id =: clContract.Id LIMIT 1].First_Payment_Ineligibility_Cleared_Date__c);
    }
    
    @isTest static void not_eligible_payment_not_satisfied(){
        loan__loan_Account__c clContract = LibraryTest.createContractTH();
        clContract.First_Payment_Ineligibility_Cleared__c = false;
        update clContract;
        
        List<String> contractList = new List<String>();
        contractList.add(clContract.Id);
        
		createBill(clContract.Id, System.today().addDays(1), System.today().addDays(1), false, true);
        
        createBill(clContract.Id, System.today().addDays(2), System.today().addDays(2), true, true);

        createBill(clContract.Id, System.today().addDays(3), System.today().addDays(3), true, true);
        
        ReEligibilityAlertforBorrowingBase.checkEligibility(contractList);
        
       	System.assertEquals(false,[SELECT First_Payment_Ineligibility_Cleared__c FROM loan__Loan_Account__c WHERE Id =: clContract.Id LIMIT 1].First_Payment_Ineligibility_Cleared__c);
        System.assertEquals(null,[SELECT First_Payment_Ineligibility_Cleared_Date__c FROM loan__Loan_Account__c WHERE Id =: clContract.Id LIMIT 1].First_Payment_Ineligibility_Cleared_Date__c);
    }
    
    @isTest static void not_eligible_not_enough_payments(){
        loan__loan_Account__c clContract = LibraryTest.createContractTH();
        clContract.First_Payment_Ineligibility_Cleared__c = false;
        update clContract;
        
        List<String> contractList = new List<String>();
        contractList.add(clContract.Id);
        
		createBill(clContract.Id, System.today().addDays(1), System.today().addDays(1), false, true);
        
        ReEligibilityAlertforBorrowingBase.checkEligibility(contractList);
        
       	System.assertEquals(false,[SELECT First_Payment_Ineligibility_Cleared__c FROM loan__Loan_Account__c WHERE Id =: clContract.Id LIMIT 1].First_Payment_Ineligibility_Cleared__c);
        System.assertEquals(null,[SELECT First_Payment_Ineligibility_Cleared_Date__c FROM loan__Loan_Account__c WHERE Id =: clContract.Id LIMIT 1].First_Payment_Ineligibility_Cleared_Date__c);
    }
    
    private static void createBill(String contractId, Date dueDate, Date pmtDate, Boolean pmtSatisfied, Boolean primaryFlag){
        loan__Loan_account_Due_Details__c bill = new loan__Loan_account_Due_Details__c();
        bill.loan__Due_Date__c = dueDate;
        bill.loan__Payment_Date__c = pmtDate;
        bill.loan__Payment_Satisfied__c = pmtSatisfied;
        bill.loan__Loan_Account__c = contractId;
        bill.loan__DD_Primary_Flag__c = primaryFlag;
        insert bill;
    }
}