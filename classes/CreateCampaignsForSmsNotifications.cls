global class CreateCampaignsForSmsNotifications implements Schedulable {
    public CreateCampaignsForSmsNotifications() {}

    global void execute(SchedulableContext SC) {
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();
        Integer daysUntilNextPayment = 3;
        Date nextPaymentDate = System.now().addDays(daysUntilNextPayment).date();

        scheduler.createCampaignForDebitCardPayments(nextPaymentDate);
        scheduler.createCampaignForBankAccountPayments(nextPaymentDate);
        scheduler.createCampaignForManualPayments(nextPaymentDate);

        String dayOfWeek = System.now().format('EEEE');

        // Create campaign for Sunday's notifications
        if (dayOfWeek == 'Saturday') {
            scheduler.createCampaignForDebitCardPayments(nextPaymentDate.addDays(1));
            scheduler.createCampaignForBankAccountPayments(nextPaymentDate.addDays(1));
            scheduler.createCampaignForManualPayments(nextPaymentDate.addDays(1));
        }
    }

    public Campaign createCampaignForDebitCardPayments(Date nextPaymentDate) {
        String campaignType = 'SMS Payment Reminder - Debit Card';
        List<loan__Loan_Account__c> contracts = getContractsForDebitCardPayments(nextPaymentDate);

        if (contracts.size() == 0) { return null; }

        Campaign campaign = createCampaign(campaignType, nextPaymentDate);
        assignMembersToCampaign(campaign, contracts);
        return campaign;
    }

    public Campaign createCampaignForBankAccountPayments(Date nextPaymentDate) {
        String campaignType = 'SMS Payment Reminder - Bank Account';
        List<loan__Loan_Account__c> contracts = getContactsForBankAccountPayments(nextPaymentDate);

        if (contracts.size() == 0) { return null; }

        Campaign campaign = createCampaign(campaignType, nextPaymentDate);
        assignMembersToCampaign(campaign, contracts);
        return campaign;
    }

    public Campaign createCampaignForManualPayments(Date nextPaymentDate) {
        String campaignType = 'SMS Payment Reminder - Manual Payment';
        List<loan__Loan_Account__c> contracts = getContactsForManualPayments(nextPaymentDate);

        if (contracts.size() == 0) { return null; }

        Campaign campaign = createCampaign(campaignType, nextPaymentDate);
        assignMembersToCampaign(campaign, contracts);
        return campaign;
    }

    @TestVisible private Campaign createCampaign(String notificationType, Date nextPaymentDate) {
        Campaign campaign = new Campaign();
        campaign.Name = notificationType + ' [' + nextPaymentDate.format() + ']';
        campaign.Type = 'SMS Notifications';
        campaign.SMS_Notifications_Planned_Delivery_Time__c = System.Now().addHours(4);
        campaign.SMS_Notification_Status__c = 'Active';
        campaign.SMS_Notification_Type__c = notificationType;

        insert campaign;
        return campaign;
    }

    @TestVisible private void assignMembersToCampaign(Campaign campaign, List<loan__Loan_Account__c> contracts) {
        List<CampaignMember> campaignMembers = new List<CampaignMember>();

        for (loan__Loan_Account__c contract : contracts) {
            String notification = '';
            
            if (campaign.SMS_Notification_Type__c == 'SMS Payment Reminder - Debit Card') {
                Datetime paymentDate = contract.loan__Next_Installment_Date__c.addDays(1);
                String formattedPaymentDate = paymentDate.format('EEEE, MMMM dd');

                notification = 'LendingPoint: Just a reminder that your automatic payment of $' +
                    contract.loan__Pmt_Amt_Cur__c + ' from your debit card is scheduled for this ' +
                    formattedPaymentDate + '.';
            }

            if (campaign.SMS_Notification_Type__c == 'SMS Payment Reminder - Bank Account') {
                Datetime paymentDate = contract.loan__ACH_Next_Debit_Date__c.addDays(1);
                String formattedPaymentDate = paymentDate.format('EEEE, MMMM dd');

                notification = 'LendingPoint: Just a reminder that your automatic payment of $' +
                    contract.loan__Pmt_Amt_Cur__c + ' from your bank account is scheduled for this ' +
                    formattedPaymentDate + '.';
            }

            if (campaign.SMS_Notification_Type__c == 'SMS Payment Reminder - Manual Payment') {
                Datetime paymentDate = contract.loan__Next_Installment_Date__c.addDays(1);
                String formattedPaymentDate = paymentDate.format('EEEE, MMMM dd');

                notification = 'LendingPoint: Just a reminder that your payment of $' +
                    contract.loan__Pmt_Amt_Cur__c + ' is due this ' + formattedPaymentDate +
                    '. You can pay online @lendingpoint.com.';
            }

            CampaignMember member = new CampaignMember(
                CampaignId=campaign.Id,
                ContactId=contract.loan__Contact__c,
                SMS_Notification__c=notification,
                Status='Active');

            campaignMembers.add(member);
        }
        
        Database.insert(campaignMembers, false);
    }

    @TestVisible private List<loan__Loan_Account__c> getContractsForDebitCardPayments(Date nextPaymentDate) {
        List<loan__Loan_Account__c> contracts = [SELECT Name,
                    Contact_First_Name__c,
                    loan__Contact__r.Phone,
                    loan__ACH_On__c,
                    loan__Pmt_Amt_Cur__c,
                    Is_Debit_Card_On__c,
                    loan__Pay_Off_Amount_As_Of_Today__c,
                    loan__Next_Installment_Date__c,
                    loan__ACH_Next_Debit_Date__c,
                    SMS_Opt_Out__c,
                    loan__Contact__r.Do_Not_Contact__c
                FROM
                    loan__Loan_Account__c
                WHERE
                    Overdue_Days__c = 0
                    AND loan__Loan_Status__c = 'Active - Good Standing'
                    AND loan__ACH_On__c = False
                    AND Is_Debit_Card_On__c = True
                    AND loan__Next_Installment_Date__c = :nextPaymentDate
                    AND SMS_Opt_Out__c = False
                    AND loan__Contact__r.Do_Not_Contact__c = False
                    AND Payoff_As_Of_Today_GT_Pmt_Amnt_Cur__c = 'true'
                    AND IsDeleted = False
                    AND loan__Contact__r.IsDeleted = False];

        return contracts;
    }

    @TestVisible private List<loan__Loan_Account__c> getContactsForBankAccountPayments(Date nextPaymentDate) {
        List<loan__Loan_Account__c> contracts = [SELECT Name,
                    loan__Pay_Off_Amount_As_Of_Today__c ,
                    Contact_First_Name__c,
                    loan__Contact__r.Id,
                    loan__Contact__r.Phone,
                    loan__ACH_On__c,
                    loan__Pmt_Amt_Cur__c ,
                    Is_Debit_Card_On__c,
                    loan__Next_Installment_Date__c,
                    loan__ACH_Next_Debit_Date__c,
                    SMS_Opt_Out__c,
                    loan__Contact__r.Do_Not_Contact__c
                FROM
                    loan__Loan_Account__c
                WHERE
                    Overdue_Days__c = 0
                    AND loan__Loan_Status__c = 'Active - Good Standing'
                    AND loan__ACH_On__c = True
                    AND Is_Debit_Card_On__c = False
                    AND loan__ACH_Next_Debit_Date__c = :nextPaymentDate
                    AND SMS_Opt_Out__c = False
                    AND loan__Contact__r.Do_Not_Contact__c = False
                    AND Payoff_As_Of_Today_GT_Pmt_Amnt_Cur__c = 'true'
                    and loan__Pmt_Amt_Cur__c > 0
                    AND IsDeleted = False
                    AND loan__Contact__r.IsDeleted=False];

        return contracts;
    }

    @TestVisible private List<loan__Loan_Account__c> getContactsForManualPayments(Date nextPaymentDate) {
        Date lastPaymentDate = nextPaymentDate.addDays(-8);

        List<loan__Loan_Account__c> contracts = [SELECT Name,
                    Contact_First_Name__c,
                    loan__Contact__r.Id,
                    loan__Contact__r.Phone,
                    loan__ACH_On__c,
                    loan__Pmt_Amt_Cur__c,
                    Is_Debit_Card_On__c,
                    loan__Pay_Off_Amount_As_Of_Today__c,
                    loan__Next_Installment_Date__c,
                    loan__ACH_Next_Debit_Date__c,
                    loan__Last_Payment_Date__c,
                    SMS_Opt_Out__c,
                    loan__Contact__r.Do_Not_Contact__c
                FROM
                    loan__Loan_Account__c
                WHERE
                    Overdue_Days__c = 0
                    AND loan__Loan_Status__c = 'Active - Good Standing'
                    AND loan__ACH_On__c = False
                    AND Is_Debit_Card_On__c = False
                    AND loan__Next_Installment_Date__c = :nextPaymentDate
                    AND SMS_Opt_Out__c = False
                    AND loan__Contact__r.Do_Not_Contact__c = False
                    AND Payoff_As_Of_Today_GT_Pmt_Amnt_Cur__c = 'true'
                    AND loan__Last_Payment_Date__c < :lastPaymentDate
                    AND IsDeleted = False
                    AND loan__Contact__r.IsDeleted = False];

        return contracts;
    }
}