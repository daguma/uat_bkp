public class GenerationPDFCtrl  {

	public String body {get; set;}

	public GenerationPDFCtrl () {

		body = EncodingUtil.base64Decode( blobToString( Blob.valueOf(ApexPages.currentPage().getParameters().get('bodyPDF')), 'UTF-8').replaceAll(' ', '+') ).toString();
	}

	public static String blobToString(Blob input, String inCharset) {
		String hex = EncodingUtil.convertToHex(input);
		System.assertEquals(0, hex.length() & 1);
		final Integer bytesCount = hex.length() >> 1;
		String[] bytes = new String[bytesCount];
		for (Integer i = 0; i < bytesCount; ++i)
			bytes[i] =  hex.mid(i << 1, 2);
		return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
	}

}