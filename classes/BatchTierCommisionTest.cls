@IsTest
public class BatchTierCommisionTest{  
      TestMethod static void doVerify1(){
        Commissions_Structure__c cs = new Commissions_Structure__c();
        cs.Tier_Range_Indicator__c = 'Count';
        cs.Commission_Cost_Base__c = 'Cost per Funding';
        cs.Commission_Structure_Status__c = 'Active';
        upsert cs;
        
        Tier_Structure__c ts=new Tier_Structure__c();
        ts.Name= 'T1';
        ts.Commission_Scheme_Value__c = decimal.ValueOf('20');
        ts.Maximum_Range_Value__c = decimal.ValueOf('11000.00');
        ts.Minimum_Range_Value__c = decimal.ValueOf('18000.00');
        ts.Commissions_Structure__c = cs.id;
        upsert ts;
        
        Account acc = new Account();
        acc.name = 'test';
        acc.Commission_Structure__c=cs.id;
        acc.Type = 'Partner';
        acc.Is_Parent_Partner__c = true;
        insert acc;
        System.debug('acc..'+acc);
        
        
        loan__Loan_Account__c cl = TestHelper.createContract();  
        genesis__Applications__c appln = [select Partner_Account__c from genesis__Applications__c where id=: cl.Application__c];
        appln.Partner_Account__c=acc.id;
        update appln;


        
        List<loan__Loan_Account__c> scope = new List<loan__Loan_Account__c>();
        scope.add(cl);
        
        Test.startTest();
        BatchTierCommision batchapex= new BatchTierCommision(); 
        database.executeBatch(batchapex);  
        Test.stopTest();    
      }
}