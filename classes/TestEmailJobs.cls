@isTest public class TestEmailJobs {

    //Covers EmailAutomatedJob

    @isTest static void PaymentReminders() {

        //Load csv file
        Test.loadData(filegen__File_Metadata__c.sObjectType, 'LendingFilegen');

        //Setup seed data
        TestHelperForManaged.createSeedDataForTesting();
        TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupMetadataSegments();

        loan.TestHelper.systemDate = Date.newInstance(2014, 3, 3); // Mar 3 - Monday


        loan__Currency__c curr = TestHelperForManaged.createCurrency();

        //    Currency__c curr = TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');

        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);

        loan__Bank__c bank = loan.TestHelper.createBank();
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        dummyOffice.loan__Branch_s_Bank__c = bank.id;
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                                        dummyAccount,
                                        curr,
                                        dummyFeeSet);

        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();

        //Account a1 = loan.TestHelper.createInvestor('Bull', 1000);
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        loan__Bank_Account__c ba = new loan__Bank_Account__c(loan__Bank_Account_Number__c = '12',
                loan__Bank_Name__c = 'Some Bank',
                loan__Routing_Number__c = '99999999',
                loan__Account_Type__c = 'Checking');

        insert ba;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1, 'Collections Trust Account');
        //Create a dummy Loan Account
        loan__Loan_Account__c dummylaMonthly = TestHelperForManaged.createLoanAccountForAccountObj(dummyLP,
                                               b1,
                                               dummyFeeSet,
                                               dummyLoanPurpose,
                                               dummyOffice,
                                               true,
                                               loan.TestHelper.systemDate,
                                               null,
                                               loan.LoanConstants.LOAN_PAYMENT_FREQ_MONTHLY);
        dummylaMonthly.loan__ACH_On__c = true;
        //dummylaMonthly.Borrower_ACH__c = ba.Id;
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = Date.newInstance(2014, 3, 3);
        dummylaMonthly.loan__ACH_Start_Date__c = Date.newInstance(2014, 3, 3);
        dummylaMonthly.loan__ACH_End_Date__c = Date.newInstance(2014, 3, 3).addMonths(4);
        dummylaMonthly.loan__ACH_Bank__c = dummyOffice.loan__Branch_s_Bank__c;
        dummylaMonthly.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        dummylaMonthly.loan__ACH_Account_Type__c = 'Checking';
        dummylaMonthly.loan__ACH_Account_Number__c = '123456';
        dummylaMonthly.loan__ACH_Routing_Number__c = '123456123';
        dummylaMonthly.loan__ACH_Relationship_Type__c = 'PRIMARY';
        dummylaMonthly.Reporting_Fee__c = 300;
        dummylaMonthly.Reporting_Fee_Balance__c = 300;
        dummylaMonthly.Reporting_Interest__c = 1300;
        update dummylaMonthly;
        loan__Loan_Account__c dummylaWeekly = TestHelperForManaged.createLoanAccountForAccountObj(dummyLP,
                                              b1,
                                              dummyFeeSet,
                                              dummyLoanPurpose,
                                              dummyOffice,
                                              true,
                                              loan.TestHelper.systemDate,
                                              null,
                                              loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY);
        dummylaWeekly.loan__ACH_On__c = true;
        //dummylaWeekly.Borrower_ACH__c = ba.Id;
        dummylaWeekly.loan__ACH_Debit_Amount__c = 100;
        dummylaWeekly.loan__ACH_Next_Debit_Date__c = Date.newInstance(2014, 3, 3);
        dummylaWeekly.loan__ACH_Start_Date__c = Date.newInstance(2014, 3, 3);
        dummylaWeekly.loan__ACH_End_Date__c = Date.newInstance(2014, 3, 3).addMonths(4);
        dummylaWeekly.loan__ACH_Bank__c = dummyOffice.loan__Branch_s_Bank__c;
        dummylaWeekly.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        dummylaWeekly.loan__ACH_Account_Type__c = 'Checking';
        dummylaWeekly.loan__ACH_Account_Number__c = '123456';
        dummylaWeekly.loan__ACH_Routing_Number__c = '123456123';
        dummylaWeekly.loan__ACH_Relationship_Type__c = 'PRIMARY';
        dummylaWeekly.Reporting_Fee__c = 300;
        dummylaWeekly.Reporting_Fee_Balance__c = 300;
        dummylaWeekly.Reporting_Interest__c = 1300;
        update dummylaWeekly;
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name = 'ACH'];
        test.startTest();
        loan.LoanPaymentTxnSweepToACHJob j = new loan.LoanPaymentTxnSweepToACHJob(false);
        Database.executeBatch(j);
        test.stopTest();


        loan__Loan_Account__c laMonthly = [SELECT Id
                                           , loan__ACH_Next_Debit_Date__c
                                           , loan__ACH_Debit_Day__c
                                           , loan__Next_Installment_Date__c
                                           , loan__ACH_Frequency__c
                                           , loan__Payment_Amount__c
                                           , loan__First_Installment_Date__c
                                           , loan__Contact__c
                                           , Name
                                           FROM loan__Loan_Account__c
                                           where loan__Frequency_of_Loan_Payment__c = : 'Monthly'];

        loan__Loan_Account__c laWeekly = [SELECT Id
                                          , loan__Disbursal_Date__c
                                          , loan__ACH_Next_Debit_Date__c
                                          , loan__Next_Installment_Date__c
                                          , loan__ACH_Frequency__c
                                          , loan__Payment_Amount__c
                                          , Name
                                          , loan__Contact__c
                                          FROM loan__Loan_Account__c
                                          where loan__Frequency_of_Loan_Payment__c = : 'Weekly'];

        List<loan__Loan_Payment_Transaction__c> p = [SELECT Id FROM loan__Loan_Payment_Transaction__c];
        System.assert(p.size() > 0 );

        for (loan__Loan_Payment_Transaction__c tr : p) {
            tr.loan__Cleared__c  = true;
            update tr;
        }

        EmailAutomatedJob eJb = new EmailAutomatedJob();

        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'test2';
        c.Email = 'test@test3.com';

        Account acc = new Account();
        acc.name = 'Test Me';
        insert acc;
        c.AccountId = acc.Id;
        insert c;

        TestHelper.createLPCustom('Sample Loan Product', 'LendingPoint');

        loan__Loan_Product__c lp = TestHelper.createLendingProduct('Sample Loan Product');

        genesis__Applications__c app = new genesis__applications__c();
        app.genesis__Contact__c = c.Id;
        app.genesis__Loan_Amount__c = 1000000;
        app.Lending_Product__c = [Select Id from loan__Loan_Product__C limit 1].Id;
        app.genesis__Product_Type__c = 'LOAN';
        app.genesis__Loan_Amount__c = c.Loan_Amount__c;
        //  app.Company__c = [Select Id from loan__Office_Name__c where Name =: 'LendingPoint' Limit 1].Id;
        app.genesis__Days_Convention__c = '30/360';
        app.Fee__c = 0.05 * (c.Loan_Amount__c == null ? 0.0 : c.Loan_amount__c);
        app.genesis__Interest_Calculation_Method__c = 'Declining Balance';

        app.RecordType = [Select ID from RecordType where DeveloperName = : 'LOAN'
                          and SObjectType = : 'genesis__Applications__c'];



        app.genesis__Term__c = 24;
        app.genesis__Expected_First_Payment_Date__c = Date.today().addDays(30);
        app.genesis__Loan_Amount__c = 15000;
        app.genesis__Interest_Rate__c = 24.99;
        app.genesis__Payment_Frequency__c = 'MONTHLY';
        app.genesis__Payment_Amount__c = 202.00;
 
        app.genesis__Status__c = 'Credit Qualified';
        insert app;

        laWeekly.loan__Contact__c = c.Id;
        update laWeekly;
        laMonthly.loan__Contact__c = c.id;
        update laMonthly;

        eJb.replaceValuesFromData('TEST', laWeekly, c, p.get(0), app);

        EmailTemplate eTemplate;
        OrgWideEmailAddress noRe;

        try {eTemplate = [select id, name, HtmlValue, Subject, Body from EmailTemplate where developername = : 'LP_Payment_Due_reminder_V2'];}
        catch (Exception e) {
            System.debug ('[U-03] Unable to locate EmailTemplate using name: ' + 'LP_Payment_Due_reminder_V2' + ' refer to Setup | Communications Templates ' );
            return;
        }

        try {noRe = [select Id, DisplayName from OrgWideEmailAddress where DisplayName = 'LendingPoint-NoReply' ];}
        catch (Exception e) {
            System.debug ('[U-03] Unable to locate OrgWideEmailAddress using name: ' + 'LendingPoint-NoReply' + ' refer to Setup | Email Communcations ' );
            return;
        }


        eJb.execute(null); //sendPaymentReminder();

    }

}