public class MultiloanPostSoftPull {

    public static final String ELIGIBILITY_CREDIT_REPORT_QUERY =  'SELECT Id, Contract__c, ' +
            ' FICO__c, Grade__c, ' +
            ' New_FICO__c, New_Grade__c, ' +
            ' Eligible_For_Multiloan__c, ' +
            ' IsLowAndGrow__c, ' +
            ' Last_MultiLoan_Eligibility_Review__c, ' +
            ' System_Debug__c, ' +
            ' Eligible_For_Soft_Pull_Params__c, ' +
            ' Eligible_For_Soft_Pull__c, ' +
            ' LoanAmountCheck__c ' +
            ' FROM Multiloan_Params__c ' +
            ' WHERE Eligible_For_Soft_Pull_Params__c != null ' +
            ' AND Eligible_For_Soft_Pull__c = true ';
            
    /**
    * [MultiloanPostSoftPull description]
    * @param contactList [description]
    */
    public static void executeProcess(String contractId, Multiloan_Params__c multiloanParams) {
        loan__Loan_Account__c contract = MultiloanUtil.getContract(contractId);
        MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');

        Contact c = new Contact();
        Boolean newAppSubmitted = false;
           if (contract != null) {
                String appId = '';
                Opportunity newApp = null;
                Opportunity lastApp = MultiloanUtil.getLastMultiOpportunity(contract);
                if (lastApp != null) {
                    appId = lastApp.Id;
                    newApp = lastApp;
                }

                if (appId == '') {
                    SubmitApplicationService submit = new SubmitApplicationService();
                    appId = submit.submitApplication(contract.loan__Contact__c);
                    newApp = MultiloanUtil.getApplication(appId);
                    newAppSubmitted = true;
                    newApp.Type =  'Multiloan';
                    newApp.LeadSource__c = multiloanSettings.Lead_Source__c ;
                    newApp.Lead_Sub_Source__c = multiloanSettings.Lead_Sub_Source__c;
                    newApp.Partner_Account__c = (OfferCtrlUtil.runningInASandbox() || Test.isRunningTest()) ? [SELECT Id FROM Account WHERE Name = 'Multi-Loan' LIMIT 1].Id : multiloanSettings.AccountId__c; //Add a validation for sandbox, to avoid exception
                    newApp.Source_Contract__c = contract.Id;
                }

                for (Opportunity fundedApp : [SELECT Estimated_Grand_Total__c
                                            FROM Opportunity
                                            WHERE  Contact__c = : contract.loan__Contact__c
                                                    AND Status__c IN ('Funded')
                                                    ORDER BY CreatedDate DESC
                                                    LIMIT 1]) {

                    if (fundedApp.Estimated_Grand_Total__c != null) {
                        c.id = contract.loan__Contact__c;
                        c.Annual_Income__c = fundedApp.Estimated_Grand_Total__c;
                        update c;
                    }
                }
                if (newApp != null && newApp.Is_Selected_Offer__c == false) {
                    Decimal newAmount = contract.Opportunity__r.Maximum_Loan_Amount__c - contract.loan__Loan_Amount__c;
                    Decimal maxAmount = null;        

                    if ((contract.Opportunity__r.StrategyType__c == '2' || contract.Opportunity__r.StrategyType__c == '3') && newAmount > Integer.valueOf(multiloanSettings.Maximum_Loan_Amount_LG__c))
                        newApp.Amount = Integer.valueOf(multiloanSettings.Maximum_Loan_Amount_LG__c); //maximum of difference and maximum
					else if (newAmount > Integer.valueOf(multiloanSettings.Maximum_Loan_Amount__c)) 
                        newApp.Amount = Integer.valueOf(multiloanSettings.Maximum_Loan_Amount__c); //maximum of difference and maximum
					else newApp.Amount = newAmount;            
                }
                update newApp;
                if (!String.isEmpty(appId)) {
                    multiloanParams.Eligible_For_Soft_Pull_Params__c =  contract.Opportunity__c + ',' + appId + ',' + contractId + ',' + newApp.Is_Selected_Offer__c + ',' + newApp.Status__c; // + ',' + primaryCreditReportId;
                    update multiloanParams;
                }
            }

    }

    public static void eligibilityCheckGetCR(Multiloan_Params__c multiloanParams) {
        try {
            MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');
            Decimal newFICO = 0;
            Boolean isEligible = false;
            Scorecard__c newScorecard = null;
            Offer__c oldOffer = null;

            String oldAppId = multiloanParams.Eligible_For_Soft_Pull_Params__c.split(',').get(0);
            String newAppId = multiloanParams.Eligible_For_Soft_Pull_Params__c.split(',').get(1);
            String contractId =  multiloanParams.Eligible_For_Soft_Pull_Params__c.split(',').get(2);
            String isSelectedOffer = multiloanParams.Eligible_For_Soft_Pull_Params__c.split(',').get(3);
            String newAppStatus = multiloanParams.Eligible_For_Soft_Pull_Params__c.split(',').get(4);
            String creditReportData = SalesForceUtil.GetLatestValidCreditReportId(newAppId, true);
            String creditReportId = creditReportData.split(',').get(0);
            String sequenceId = creditReportData.split(',').get(1);
            String strategyType = null;

            multiloanParams.Eligible_For_Soft_Pull_Params__c = null;
            multiloanParams.Eligible_For_Soft_Pull__c = false;
            Multiloan_Params_Logs__c multiloanParamsLogs = new Multiloan_Params_Logs__c();
            multiloanParamsLogs.Multiloan_Params__c = multiloanParams.Id;
            if (sequenceId != '1' && isSelectedOffer == 'false' && (newAppStatus == 'null' || String.isEmpty(newAppStatus) || newAppStatus == 'Aged / Cancellation' || newAppStatus == 'Declined (or Unqualified)' || newAppStatus == 'Credit Qualified')) {
                ProxyApiCreditReportEntity creditReportEntity = null;
                if (sequenceId == '10' && creditReportId != null ) {
                        creditReportEntity = CreditReport.get(newAppId, false, true, true, null, false, Long.valueOf(creditReportId));
                        isEligible = true;
                } else {
                    creditReportEntity = CreditReport.get(newAppId, false, true, true);
                    isEligible = true;
                }

                Opportunity newApp = MultiloanUtil.getApplication(newAppId);
                if (creditReportEntity != null && creditReportEntity.errorMessage == null && creditReportEntity.brmsApproved) {

                    newScorecard = ScorecardUtil.getScorecard(newAppId);
                    newFICO = creditReportEntity.getDecimalAttribute(CreditReport.FICO);
                    strategyType = creditreportEntity.brms.deStrategy_Type;
                    if (newScorecard.Grade__c != null && (newScorecard.Grade__c == 'A1' || newScorecard.Grade__c == 'A2' || newScorecard.Grade__c == 'B1')){
                        if (newFICO < multiloanParams.FICO__c - 10 ) {
                            multiloanParamsLogs.Fico_Fail__c = true;
                            isEligible = false;
                        }    
                    }
                    else if (newScorecard.Grade__c != null && (newScorecard.Grade__c == 'B2' || newScorecard.Grade__c == 'C1' || strategyType == '2' || strategyType == '3')){
                        if (newFICO < multiloanParams.FICO__c - 15 ) {
                            multiloanParamsLogs.Fico_Fail__c = true;
                            isEligible = false;
                        }    
                    }    

                    if (newScorecard.Grade__c == null || (newScorecard.Grade__c != 'C1' && newScorecard.Grade__c != 'B1' && newScorecard.Grade__c != 'B2' && newScorecard.Grade__c != 'A2' && newScorecard.Grade__c != 'A1')){
                        multiloanParamsLogs.Grade_Fail__c = true;
                        isEligible = false;
                    }                

                    /*if (!MultiloanUtil.isBetterGrade(multiloanParams.Grade__c, newScorecard.Grade__c)) {
                        multiloanParamsLogs.Grade_Fail__c = true;
                        isEligible = false;

                    }*/

                    List<Offer__c> newOffers = MultiloanUtil.getOffers(newAppId,  multiloanParams.LoanAmountCheck__c, newApp.StrategyType__c, newApp.Fee_Handling__c);

                    if (newOffers.isEmpty()) {
                        multiloanParamsLogs.No_Offers_Fail__c = true;
                        isEligible = false;

                    } else {
                        Decimal maxOfferAmount = newApp.Maximum_Offer_Amount__c;

                        Decimal tmpMaxAmount = 0;

                        for (Offer__c offr : newOffers) {
                            if (offr.Opportunity__r.Fee_Handling__c == null || offr.Opportunity__r.Fee_Handling__c == FeeUtil.NET_FEE_OUT) {
                                maxOfferAmount = offr.Total_Loan_Amount__c;
                            } else {
                                maxOfferAmount = offr.Loan_Amount__c;
                            }
                            if (maxOfferAmount > tmpMaxAmount) {
                                tmpMaxAmount = maxOfferAmount;
                            }
                        }
                        newApp.Maximum_Offer_Amount__c = tmpMaxAmount;

                    }
                    if (isEligible) {
                        oldOffer = MultiloanUtil.getSelectedOffer(oldAppId);

                    }

                } else {
                    isEligible = false;
                    if (multiloanParamsLogs.Fico_Fail__c == false && multiloanParamsLogs.Grade_Fail__c == false) {
                        System.debug('Soft Pull Decline - update');
                        multiloanParamsLogs.Soft_Pull_Fail__c = true;

                    }
                }

                multiloanParams.New_FICO__c = newFICO;
                multiloanParams.New_Grade__c = newScorecard != null ? newScorecard.Grade__c : '-';
                multiloanParams.Eligible_For_Multiloan__c = isEligible;

                if (isEligible) {

                    if ( String.isEmpty(newApp.Status__c) ||
                            newApp.Status__c == 'Aged / Cancellation' ||
                            newApp.Status__c == 'Declined (or Unqualified)' ) {
                        newApp.Status__c = 'Credit Qualified';

                    }
                    MultiloanUtil.insertNoteOldOffer(newAppId, oldAppId, oldOffer, contractId);
                    if (newApp.Dialer_Status__c != 'ContactDNC') newApp.Dialer_Status__c  = null;
                    newApp.Last_CQ_Date__c = Date.today();
                    newApp.Reason_of_Opportunity_Status__c = null;
                } else {

                    insert multiloanParamsLogs;

                    newApp.Status__c = 'Declined (or Unqualified)';
                }

                update newApp;

            } // sequenceId = '1'

        } catch (Exception e) {
            System.Debug(e.getMessage() + ' ' + e.getStackTraceString());
            multiloanParams.System_Debug__c += '\n' + e.getMessage() + '\n' + e.getStackTraceString();
        }

        update multiloanParams;

    }



}