global class BatchAONBilling implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts {

    public BatchAONBilling() {
    }

    public void execute(SchedulableContext sc) {
        BatchAONBilling batchapex = new BatchAONBilling();
        id batchprocessid = Database.executebatch(batchapex, 1);
        system.debug('Process ID: ' + batchprocessid);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String last24h = ((String.valueOf(System.now().addDays(-3))).replace(' ', 'T')) + '.000+0000';
        String query = 'SELECT Id,Contract__c,Contract__r.Name,Contract__r.loan__Pay_Off_Amount_As_Of_Today__c,Billing_Date__c,Company__c,IsProcessed__c,loan_AON_Information__c,ServiceError__c,Product_Code__c,Loan_Balance__c,' +
                       'Minimum_Monthly_Payment__c,Payment_Due_Date__c,Product_Fee__c,IsEnrollment__c,RetryCount__c,Contract__r.loan__Next_Installment_Date__c ' +
        		       'FROM loan_AON_Billing__c ' + 
                       'WHERE IsProcessed__c = false AND (Billing_Date__c < ' + last24h + ' OR IsEnrollment__c = true) AND RetryCount__c <= 3';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<loan_AON_Billing__c> scope) {
        String jsonResult;

        for (loan_AON_Billing__c CL : scope) {

            if (!CL.IsEnrollment__c) {
                CL.Loan_Balance__c = CL.Contract__r.loan__Pay_Off_Amount_As_Of_Today__c.setScale(2, RoundingMode.HALF_UP);
                CL.Payment_Due_Date__c = CL.Contract__r.loan__Next_Installment_Date__c;
            }

            AONObjectsParameters.AONBillingInputData aonBillingInputData = new AONObjectsParameters.AONBillingInputData(CL.Billing_Date__c,
                    String.valueOf(CL.Product_Fee__c.setScale(2, RoundingMode.HALF_UP)),
                    String.valueOf(CL.Minimum_Monthly_Payment__c.setScale(2, RoundingMode.HALF_UP)),
                    String.valueOf(CL.Loan_Balance__c.setScale(2, RoundingMode.HALF_UP)),
                    CL.Payment_Due_Date__c,
                    CL.Contract__r.Name,
                    CL.Product_Code__c);

            String jsonRequest  = new AONObjectsParameters.AONBillingInputData().get_JSON(aonBillingInputData);

            String jsonResponse =  ProxyApiAON.billing(jsonRequest);

            AONObjectsParameters.AonResponse aonResponse = new AONObjectsParameters.AonResponse(jsonResponse);

            if (aonResponse != null) {

                if (aonResponse.statusCode != 201 && aonResponse.statusCode != 200) {
                    if (String.isNotEmpty(aonResponse.errorMessage))
                        CL.ServiceError__c = (aonResponse.errorMessage.length() > 255) ? aonResponse.errorMessage.substring(0, 254) : aonResponse.errorMessage;
                    CL.RetryCount__c += 1;
                } else {
                    CL.IsProcessed__c = true;
                    CL.RetryCount__c = 0;
                }
                update CL;
            }

            AONProcesses.setContactLoggging((aonResponse.callType != null) ? aonResponse.callType : 'Billing',
                                            jsonRequest, jsonResponse, CL.Contract__c);
        }
    }

    global void finish(Database.BatchableContext BC) {
    }
}