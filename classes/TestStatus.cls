@isTest
public class TestStatus {
    
    public static testMethod void test1(){

        loan.TestHelper.createSeedDataForTesting();
        //As a result of moving validation from code to validation rule, updating org Params
        loan.TestHelper.useCLLoanCRM();
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        Loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        Loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        Loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        MaskSensitiveDataSettings__c mds = new MaskSensitiveDataSettings__c();
        mds.MaskAllData__c = true;
        insert mds;    
        
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                dummyAccount, 
                curr, 
                dummyFeeSet);

        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        User u = loan.TestHelper.createUser('testUser1', 'MyOffice1');
        
        Contact dummyContact = TestHelperForManaged.createContact('dummyLA','Other','87123','Test State');
        
        Account dummyAcc = new Account();
        dummyAcc.Name = 'DummyAccount';
        dummyAcc.c2g__CODABankName__c = 'Test Bank';
        dummyAcc.c2g__CODABankAccountNumber__c = '123456';
        dummyAcc.c2g__CODAPaymentMethod__c = 'Cash';
        
        insert dummyAcc;
        
        dummyContact.AccountId = dummyAcc.Id;
        dummyContact.OwnerId = u.Id;
        update dummyContact;
        
         //Create a dummy Loan Account
        loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccountForContact(dummyLP,
                                                    dummyContact,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
        Date da = date.newInstance(2016, 12, 30);
        loanAccount.loan__Loan_Amount__c = 10000;
        loanAccount.loan__Principal_Remaining__c = 9000;
        loanAccount.loan__Loan_Status__c = 'Active - Bad Standing';
        loanAccount.loan__Last_Accrual_Date__c = Da;
        loanAccount.loan__Interest_Remaining__c = 0.0;
        loanAccount.loan__Fees_Remaining__c = 0.0;
        update loanAccount;
        
       list<collect__Source_System__c> sourcesystm =[select id,name from collect__Source_System__c ];
        
        date da1 = date.newInstance(2017,1, 1);
        collect__Loan_Account__c CollectContract= new collect__Loan_Account__c();
        CollectContract.collect__Oldest_Due_Date__c = da1;
        CollectContract.collect__Number_of_Days_Overdue__c=5;
        CollectContract.collect__Account__c=dummyAcc.id;
        CollectContract.collect__Days_Past_Due__c=5;
		CollectContract.collect__Loan_Status__c='Open';
        CollectContract.collect__Contact__c=dummyContact.id;
        CollectContract.CL_Contract__c=loanAccount.id;
      //  CollectContract.collect__Source_System__c=sourcesystm.get(0).id;
       // CollectContract.collect__Collection_Status__c
		insert CollectContract;
        
        collect__Queue__c queue = new collect__Queue__c();
		queue.Name='CUSTOMER COLLECTION QUEUE';
		queue.collect__Enabled__c=true;
        insert queue;
        
        collect__Contract_Status__c status = new collect__Contract_Status__c();
        status.Status__c = 'Resolved';
        insert status;
        
        collect__Queue_Contract__c queueContract = new collect__Queue_Contract__c();
        //queueContract.Name='CUSTOMER COLLECTION QUEUE:LAI-00011333';
        queueContract.collect__Contract__c=CollectContract.Id;
		queueContract.collect__Contract_Name__c=CollectContract.name;
       	queueContract.collect__Queue__c= queue.id;
        queueContract.collect__Status__c='Closed';
		insert queueContract;
        

        test.startTest();
        queueContract.collect__Status__c='Open';
        update queueContract;
        
        queueContract.collect__Status__c='Closed';
        update queueContract;
        Test.stopTest();
        
    }
    
    public static testMethod void test2(){

        loan.TestHelper.createSeedDataForTesting();
        //As a result of moving validation from code to validation rule, updating org Params
        loan.TestHelper.useCLLoanCRM();
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        Loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        Loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        Loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        MaskSensitiveDataSettings__c mds = new MaskSensitiveDataSettings__c();
        mds.MaskAllData__c = true;
        insert mds;    
        
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                dummyAccount, 
                curr, 
                dummyFeeSet);

        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        User u = loan.TestHelper.createUser('testUser1', 'MyOffice1');
        
        Contact dummyContact = TestHelperForManaged.createContact('dummyLA','Other','87123','Test State');
        
        Account dummyAcc = new Account();
        dummyAcc.Name = 'DummyAccount';
        dummyAcc.c2g__CODABankName__c = 'Test Bank';
        dummyAcc.c2g__CODABankAccountNumber__c = '123456';
        dummyAcc.c2g__CODAPaymentMethod__c = 'Cash';
        
        insert dummyAcc;
        
        dummyContact.AccountId = dummyAcc.Id;
        dummyContact.OwnerId = u.Id;
        update dummyContact;
        
         //Create a dummy Loan Account
        loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccountForContact(dummyLP,
                                                    dummyContact,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
        Date da = date.newInstance(2016, 12, 30);
        loanAccount.loan__Loan_Amount__c = 10000;
        loanAccount.loan__Principal_Remaining__c = 9000;
        loanAccount.loan__Loan_Status__c = 'Active - Bad Standing';
        loanAccount.loan__Last_Accrual_Date__c = Da;
        loanAccount.loan__Interest_Remaining__c = 0.0;
        loanAccount.loan__Fees_Remaining__c = 0.0;
        update loanAccount;

        date da1 = date.newInstance(2017,1, 1);
        collect__Loan_Account__c CollectContract= new collect__Loan_Account__c();
        CollectContract.collect__Oldest_Due_Date__c = da1;
        CollectContract.collect__Number_of_Days_Overdue__c=5;
        CollectContract.collect__Account__c=dummyAcc.id;
        CollectContract.collect__Days_Past_Due__c=5;
        CollectContract.collect__Loan_Status__c='Open';
        CollectContract.collect__Contact__c=dummyContact.id;
        CollectContract.CL_Contract__c=loanAccount.id;
        //  CollectContract.collect__Source_System__c=sourcesystm.get(0).id;
        // CollectContract.collect__Collection_Status__c
        insert CollectContract;

        Date da2= date.newInstance(2017, 1, 30);
        collect__Promise_To_Pay__c promisetopay = new collect__Promise_To_Pay__c();
        promisetopay.collect__Account__c = dummyAcc.id;
        promisetopay.collect__Contact__c = dummyContact.id;
        promisetopay.collect__Dev_Contract__c= CollectContract.id;
        promisetopay.collect__Promise_To_Pay_Amount__c=500;
        promisetopay.collect__Promise_To_Pay_Date__c=da2;
        insert promisetopay;
        
        
		test.startTest();
        promisetopay.collect__Promise_Broken__c=true;
        update promisetopay;
        test.stopTest();

    }
}