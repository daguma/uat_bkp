public with sharing class DisqualifierScorecardCtrl {

    @AuraEnabled
    public static DisqualifierScorecardEntity getDisqualifierScorecardEntity(Id oppId) {

        DisqualifierScorecardEntity dse = new DisqualifierScorecardEntity();

        try {

            if (oppId != null) {

                ProxyApiCreditReportEntity creditReportEntity;
                Boolean hasCRInformation = CreditReport.hasAttachmentByEntityId(oppId);

                   if (!hasCRInformation) creditReportEntity = CreditReport.getByEntityId(oppId);
                    if (!hasCRInformation) hasCRInformation = (creditReportEntity != null && creditReportEntity.errorMessage == null);

                if (hasCRInformation) {
                    if (creditReportEntity == null) creditReportEntity = CreditReport.getByEntityId(oppId);
                    if (creditReportEntity != null) {
                        if (creditReportEntity.errorMessage != null) {
                            //Don´t send the error as the OfferCtrl is already taking care of that
                            dse.errorMessage = creditReportEntity.errorMessage;
                        } else {
                            //BRMS change
                            if (!CreditReport.validateBrmsFlag(oppId)) {
                                if (creditReportEntity.brms <> null && !String.isBlank(creditReportEntity.brms.deBRMS_ContractStatus) && !creditReportEntity.brms.deBRMS_ContractStatus.containsIgnoreCase('Declined'))
                                    dse.decision = 'Yes'; else if (creditReportEntity.brms <> null) {
                                    dse.decision = creditReportEntity.brms.autoDecision;
                                }
                            } else {
                                dse.decision = creditReportEntity.automatedDecision;
                            }
                            //BRMS change
                        }
                    }
                } else {
                    dse.addWarning('There is no credit report on file for this application. Please do a credit report pull.');
                }

                Scorecard__c score = ScorecardUtil.getScorecard(oppId);
                if(score != null){
                    dse.acceptanceSP = score.Acceptance__c;
                    dse.acceptanceHP = score.Acceptance_Hard_Pull__c;
                    dse.grade = score.Grade__c;
                    dse.DTI = score.DTI__c;
                    dse.FICO = score.FICO_Score__c	;
                    dse.RevUtilization = score.Revolving_Utilization__c;
                }
                list <Opportunity> opp = [SELECT id, StrategyType__c, Permission_to_Get_Hard_Pull__c FROM Opportunity WHERE id = : oppId LIMIT 1];
                if (opp != null){
                    dse.permissionToHP = opp[0].Permission_to_Get_Hard_Pull__c == true ? 'T' : 'F'; // MER-728
                    dse.salesStrategy = opp[0].StrategyType__c;    
                }
                
            }
        } catch (Exception e) {
            return new DisqualifierScorecardEntity(e.getMessage() + ' - ' + e.getStackTraceString());
        }
        return dse;
    }

    public class DisqualifierScorecardEntity {
        @AuraEnabled public Boolean hasError { get; set; }
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = !String.isEmpty(errorMessage);
            }
        }
        @AuraEnabled public Boolean hasWarning { get; set; }
        @AuraEnabled public List<String> warningMessages {
            get;
            set {
                warningMessages = value;
                hasWarning = !warningMessages.isEmpty();
            }
        }
        @AuraEnabled public String decision { get; set; }
        @AuraEnabled public String acceptanceSP { get; set; }
        @AuraEnabled public String acceptanceHP { get; set; }
        @AuraEnabled public String grade { get; set; }
        @AuraEnabled public String salesStrategy { get; set; }
        @AuraEnabled public Decimal DTI { get; set; }
        @AuraEnabled public Decimal FICO { get; set; }
        @AuraEnabled public String RevUtilization { get; set; }
        @AuraEnabled public String permissionToHP { get; set; } // MER-728

        public DisqualifierScorecardEntity() {
            hasError = false;
            hasWarning = false;
            warningMessages = new List<String>();
        }

        public DisqualifierScorecardEntity(String errorDetails) {
            errorMessage = errorDetails;
        }

        public void addWarning(String warning){
            warningMessages.add(warning);
            hasWarning = true;
        }
    }
}