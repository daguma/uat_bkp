public class ProxyApiEmailAge {

    private static final String EMAIL_AGE_API_PREFIX = '/v101/applications/';
    private static final String EMAIL_AGE_API_METHOD = '/email-age-result/';
    private static final String GET_EMAIL_AGE_RESULT = '/v101/brms/get-brms-email-age-result/';
    
    /**
    *accept all input data related to email age
    *call the email age proxy api
    **/
    public static string getEmailAgeResult(String inputData, String applicationId) {
        
        Boolean validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();
        
        String jsonResponse = null;

        if (validatedAPI) {
            
            String body = 'input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8');
            String url = EMAIL_AGE_API_PREFIX + applicationId + EMAIL_AGE_API_METHOD;
            
            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeEmailAgeResponse();
            } else {
                jsonResponse = ProxyApiUtil.ApiGetResult(url, body);
            }

        }
        
        return jsonResponse;
    }
    
     /**
    *accept all GDS input data related to email age
    *call the email age proxy api
    **/
    public static string getEmailAgeResultUsingSeqId(String inputData, String entityId) {
        Boolean validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        
        String jsonResponse = null;

        if (validatedAPI) {
            
            String body = 'input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8') +
                          '&entity-id=' + entityId;
            
            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeEmailAgeRulesResponse(entityId);
            } else {
                jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GET_EMAIL_AGE_RESULT, body);
            }
        }
        
        return jsonResponse;
    }
}