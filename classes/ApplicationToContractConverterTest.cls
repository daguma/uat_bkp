@isTest
public Class ApplicationToContractConverterTest{
     @testSetup
    static void setupTestFinwiseData(){
        //Create Custom setting data
       FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name='finwise',Finwise_Base_URL__c='https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/',Apply_State_Restriction__c=false,
                                      Finwise_LendingPartnerId__c='5',Finwise_LendingPartnerProductId__c='7',
                                      Finwise_LoanStatusId__c='1',finwise_partner_key__c='testKey',Finwise_product_Key__c='testkey',
                                      Finwise_RateTypeId__c='1',Whitelisted_States__c='MO',No_of_Days_to_Add_Daily_Interest_Batch__c = 1);
        
        insert FinwiseParams;
    }
    static testMethod void testLoanConversion1(){
        
        TestHelper th = new TestHelper();
        th.createLendingProductName('Term Loan');
        loan__Office_Name__c offc = [Select Id,name from loan__Office_Name__C limit 1];
        Testhelper.createLPCustom('Term Loan',offc.Name);
        loan__Loan_Product__c lp = [select Id from loan__Loan_Product__c limit 1];
        Lending_Product__c nP = new Lending_Product__c();
        nP.Loan_Lending_Product__c = lp.Id;
        insert nP;   
        
        Opportunity app = createLoanApplication();
        app.Lending_Product__c = nP.Id;
        update app;
        loan__Loan_Account__c contract = new loan__Loan_Account__c();
        contract.loan__Loan_Amount__c = app.Amount;
        contract.loan__Disbursal_Date__c = app.Expected_Start_Date__c;
        contract.loan__First_Installment_Date__c = app.Expected_First_Payment_Date__c;
        //contract.loan__Days_Convention__c = app.Days_Convention__c;
        contract.loan__Product_Type__c = app.Product_Type__c;
        
        Map<SObject,Sobject> objMap = new Map<SObject,SObject>();
        objMap.put(app,contract);
        ApplicationToContractConverter converter = new ApplicationToContractConverter();
        converter.setContracts(objMap);
        String message = converter.processContract();
        system.Debug(logginglevel.error,'Message ' + message);
        app.Fico__C = '600';
        app.DTI__c = '0.40';
        update app;
        CreateLoanAccountForLoanTypeHandler loanHandler = new CreateLoanAccountForLoanTypeHandler(converter.application,converter.loanAccount,ApplicationToContractConverter.getLoanProductDetails('Term Loan'));
        String retMsg = loanHandler.createLoanAccount();
        Boolean check = retMsg.contains('Application converted to Loan');
        System.debug(retMsg);
        system.assertEquals(check,true);
    }
    static testMethod void testLoanConversion2(){
        
        TestHelper th = new TestHelper();
        th.createLendingProductName('Term Loan');
        loan__Office_Name__c offc = [Select Id,name from loan__Office_Name__C limit 1];
        loan__Loan_Product__c lp = [select Id from loan__Loan_Product__c limit 1];
        
        Testhelper.createLPCustom('Term Loan',offc.Name);
        Lending_Product__c nP = new Lending_Product__c();
        nP.Loan_Lending_Product__c = lp.Id;
        insert nP;   
        
        Opportunity app = createLoanApplication2();
        app.Expected_Start_Date__c = Date.today();
        app.ACH_Account_Number__c = '12345';
        app.ACH_Account_Type__c = 'Checking';
        app.ACH_Bank_Name__c = 'THE BANK';
        app.ACH_Routing_Number__c = '987654321';        
        update app;

        loan__Loan_Account__c contract = new loan__Loan_Account__c();
        contract.loan__Loan_Amount__c = app.Amount;
        contract.loan__Disbursal_Date__c = app.Expected_Start_Date__c;
        contract.loan__First_Installment_Date__c = app.Expected_First_Payment_Date__c;
        //contract.loan__Days_Convention__c = app.Days_Convention__c;
        contract.loan__Product_Type__c = app.Product_Type__c;
        app.Fico__C = '600';
        app.DTI__c = '0.40';
        update app;

        Map<SObject,Sobject> objMap = new Map<SObject,SObject>();
        objMap.put(app,contract);
        ApplicationToContractConverter converter = new ApplicationToContractConverter();
        converter.setContracts(objMap);
        String message = converter.processContract();
        system.Debug(logginglevel.error,'Message ' + message);
        CreateLoanAccountForLoanTypeHandler loanHandler = new CreateLoanAccountForLoanTypeHandler(converter.application,converter.loanAccount,ApplicationToContractConverter.getLoanProductDetails('Term Loan'));
        String retMsg = loanHandler.createLoanAccount();
        Boolean check = retMsg.contains('Application converted to Loan');
        system.debug('retmsg = ' +  retMsg);
        system.assertEquals(check,true);
    }
    
    static testMethod void testLoanConversion3(){
        
        ezVerify_Settings__c ezVerifyCustom = new ezVerify_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), EZVerify_Base_URL__c='https://test.ezverify.me/api/lend', Product_Name__c = 'Medical', Default_Interest_Rate__c = 3.99, Promotional_Period__c = 90, Product_Name_Dev__c = 'Medical');
        ezVerifyCustom.EZVerify_Link_Expiration_Hours__c = 48;
        insert ezVerifyCustom;
                                      

        TestHelper th = new TestHelper();
        th.createLendingProductName('Medical');
        loan__Office_Name__c offc = [Select Id,name from loan__Office_Name__C limit 1];
        system.debug('-------offc---------'+offc);
        Testhelper.createLPCustom('Medical',offc.Name);
        
        loan__Loan_Product__c lp = [select Id from loan__Loan_Product__c limit 1];
        Lending_Product__c nP = new Lending_Product__c();
        nP.Loan_Lending_Product__c = lp.Id;
        insert nP;   
        
        Opportunity app = createLoanApplication3();
        Contact c = [select id, leadsource from Contact where id=:app.Contact__c];
        system.debug('----contact--------'+c);
        c.LeadSource = 'EZVerify';
        c.Product__c = 'Medical';
        update c;
        system.debug('----contact2--------'+c);
        //app.Lending_Product__c = null;
        //update app;
        system.debug('------------appContact-----------'+app.Contact__c+'---AppContactsLeadSource----'+app.Contact__r.LeadSource);
        loan__Loan_Account__c contract = new loan__Loan_Account__c();
        contract.loan__Loan_Amount__c = app.Amount;
        contract.loan__Disbursal_Date__c = app.Expected_Start_Date__c;
        contract.loan__First_Installment_Date__c = app.Expected_First_Payment_Date__c;
        //contract.loan__Days_Convention__c = app.Days_Convention__c;
        contract.loan__Product_Type__c = app.Product_Type__c;
        
        test.startTest();
        Map<SObject,Sobject> objMap = new Map<SObject,SObject>();
        objMap.put(app,contract);
        ApplicationToContractConverter converter = new ApplicationToContractConverter();
        converter.setContracts(objMap);
        String message = converter.processContract();
        system.debug('------------appProduct-----------'+app);
        system.Debug(logginglevel.error,'Message ' + message);
        /*app.Fico__C = '600';
        app.DTI__c = '0.40';
        update app;*/
        Boolean check = message.contains('Application converted to Loan');
        system.debug('retmsg = ' +  message);
        //system.assertEquals(check,true);
        test.stopTest();
    }
    
    static testMethod void appChecker_Test(){
        Opportunity opp = LibraryTest.createApplicationTH();
        ApplicationToContractConverter converter = new ApplicationToContractConverter();
        converter.appChecker();
        
    }
     
    
    static Opportunity createLoanApplication(){
        Opportunity app = new Opportunity();
        
        //app.Name = 'Test Application';
        List<RecordType> rtList = [Select Id from RecordType
                                    where SObjectType = 'Opportunity' and DeveloperName =: 'LP'];
        app.RecordTypeId = rtList[0].Id;
        app.Contact__c = Testhelper.createContact().Id;
        app.Product_Type__c = 'LOAN';           
        app.Term__c = 10;
        app.Amount = 1000;
        app.Interest_Rate__c = 20;
        app.Days_Convention__c = '365/365';
        app.Expected_Start_Date__c = System.today();
        app.Expected_Close_Date__c = System.Today().addMonths(10);
        app.Expected_First_Payment_Date__c = system.today().addMonths(1);
        app.Interest_Calculation_Method__c = 'Declining Balance';
        app.Payment_Frequency__c = 'Monthly';
        app.Payment_Amount__C = 100;
        app.ACH_Account_Number__c = '10000';
        app.ACH_Account_Type__c = 'Checking';
        app.ACH_Bank_Name__c = 'ABC';
        app.ACH_Routing_Number__c = '999999';
        app.Name = 'test1233';
        app.CloseDate = system.today();
        app.StageName = 'Qualification';
        
        
        app.Lending_Product__c = [Select Id from Lending_Product__c where Name =: 'Term Loan'][0].id;
//        app.Lending_Product__c = app.Lending_Product__c;
        system.debug('lending prod 1 ' + app.Lending_Product__c);
        
        insert app;
        return app;
    }
    
    static Opportunity createLoanApplication2(){
        Opportunity app = new Opportunity();
        
        //app.Name = 'Test Application';
        List<RecordType> rtList = [Select Id from RecordType
                                    where SObjectType = 'Opportunity' and DeveloperName =: 'LP'];
        app.RecordTypeId = rtList[0].Id;
        app.Contact__c = Testhelper.createContact().Id;
        app.Product_Type__c = 'LOAN';           
        app.Term__c = 10;
        app.Amount = 1000;
        app.Interest_Rate__c = 20;
        app.Days_Convention__c = '365/365';
        app.Expected_Start_Date__c = System.today();
        app.Expected_First_Payment_Date__c = system.today();
        app.Interest_Calculation_Method__c = 'Declining Balance';
        app.Payment_Frequency__c = 'Monthly';
        app.Payment_Amount__c = 200;
        app.Expected_Close_Date__c = System.Today().addMonths(10);
        app.Name = 'test1223';
        app.CloseDate = system.today();
        app.StageName = 'Qualification';

        /*app.Lending_Product__c = [Select Id from Loan_Product__c where Name =: 'Term Loan'][0].id;
        system.debug('lending prod 1 ' + app.Lending_Product__c);*/
        
        insert app;
        return app;
    }
    
    static Opportunity createLoanApplication3(){
        Opportunity app = new Opportunity();
        
        //app.Name = 'Test Application';
        List<RecordType> rtList = [Select Id from RecordType
                                    where SObjectType = 'Opportunity' and DeveloperName =: 'LP'];
        app.RecordTypeId = rtList[0].Id;
        app.Contact__c = Testhelper.createContact().Id;
        app.Product_Type__c = 'LOAN';           
        app.Term__c = 10;
        app.Amount = 1000;
        app.Interest_Rate__c = 20;
        app.Days_Convention__c = '365/365';
        app.Expected_Start_Date__c = System.today();
        app.Expected_Close_Date__c = System.Today().addMonths(10);
        app.Expected_First_Payment_Date__c = system.today().addMonths(1);
        app.Interest_Calculation_Method__c = 'Declining Balance';
        app.Payment_Frequency__c = 'Monthly';
        app.Payment_Amount__C = 100;
        app.ACH_Account_Number__c = '10000';
        app.ACH_Account_Type__c = 'Checking';
        app.ACH_Bank_Name__c = 'ABC';
        app.ACH_Routing_Number__c = '999999';
        app.Name = 'test123';
        app.CloseDate = system.today();
        app.StageName = 'Qualification';
        
        //app.Lending_Product__c = [Select Id from Loan_Product__c where Name =: 'Medical'][0].id;
        //system.debug('lending prod 1 ' + app.Lending_Product__c);
        
        insert app;
        return app;
    }
     
    

}