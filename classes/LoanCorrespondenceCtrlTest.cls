@isTest
public class LoanCorrespondenceCtrlTest {

    @isTest static void sends_email() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        LIST<EmailTemplate> emailList = [SELECT Id, HTMLValue FROM EmailTemplate];

        for (EmailTemplate eTem : emailList) {
            if (eTem.HTMLValue != null && eTem.HTMLValue != '') {
                correspondence.letterTemplateId = eTem.id;
                break;
            }
        }

        Integer notesBefore = [SELECT COUNT() FROM Member_Services_Notes__c];

        correspondence.onChangeLettersTemplatesOptions();
        correspondence.sendEmailTemplateAndInsertNote();

        System.assert([SELECT COUNT() FROM Member_Services_Notes__c] > notesBefore, 'Notes');
        System.assertEquals('Email', [SELECT Interaction_Codes__c FROM Member_Services_Notes__c ORDER BY CreatedDate DESC LIMIT 1].Interaction_Codes__c);
    }

    @isTest static void saves_us_return_mail() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        contract.U_S_Return_Mail_Reason__c = 'Test';
        contract.U_S_Return_Mail__c = true;
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        Integer notesBefore = [SELECT COUNT() FROM Note];

        correspondence.contract.U_S_Return_Mail_Reason__c = 'Test Return Mail';
        correspondence.saveUSReturnMail();

        System.assert([SELECT COUNT() FROM Note] > notesBefore, 'Notes');
    }

    @isTest static void gets_letters_template() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        contract.U_S_Return_Mail_Reason__c = 'Test';
        contract.U_S_Return_Mail__c = true;
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        List<SelectOption> result =  correspondence.getLettersTemplates();

        System.assertNotEquals(null, result);
    }

    @isTest static void gets_letters_template_finwise() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        contract.U_S_Return_Mail_Reason__c = 'Test';
        contract.U_S_Return_Mail__c = true;
        update contract;

        opportunity app = [SELECT Id, Finwise_Approved__c FROM opportunity WHERE Id = : contract.opportunity__c LIMIT 1];
        app.Finwise_Approved__c = true;
        update app;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        List<SelectOption> result =  correspondence.getLettersTemplates();

        System.assertNotEquals(null, result);
    }

    @isTest static void inserts_member_services_note() {
        Integer notesBefore = [SELECT COUNT() FROM Member_Services_Notes__c];

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        contract.U_S_Return_Mail_Reason__c = 'Test';
        contract.U_S_Return_Mail__c = true;
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        correspondence.onChangeLettersTemplatesOptions();
        correspondence.memberServicesNotePrint();

        System.assert([SELECT COUNT() FROM Member_Services_Notes__c] > notesBefore, 'Notes');
        System.assertEquals('Correspondence', [SELECT Interaction_Codes__c FROM Member_Services_Notes__c ORDER BY CreatedDate DESC LIMIT 1].Interaction_Codes__c);
    }

    @isTest static void formats_without_decimals() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        String result = correspondence.formatMoney('200');

        System.assertEquals('$200.00', result);
    }

    @isTest static void formats_with_one_decimal() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        String result = correspondence.formatMoney('200.1');

        System.assertEquals('$200.10', result);
    }

    @isTest static void formats_with_two_decimals() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        String result = correspondence.formatMoney('200.23');

        System.assertEquals('$200.23', result);
    }

    @isTest static void formats_with_three_decimals() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        String result = correspondence.formatMoney('200.239');

        System.assertEquals('$200.239', result);
    }

    @isTest static void creates_message() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        LoanCorrespondenceCtrl correspondence = new LoanCorrespondenceCtrl(controller);

        correspondence.createMessage(ApexPages.Severity.ERROR, 'Some exception has occured');

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        String messageDetail = (msgs.get(index)).getDetail();
        System.assert(messageDetail.contains('Some exception has occured'), 'Create Message');
    }

}