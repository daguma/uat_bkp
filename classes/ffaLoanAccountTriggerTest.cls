@isTest private class ffaLoanAccountTriggerTest {

    @testSetup static void initialize(){
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7',
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO');
        insert FinwiseParams;

        Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name = 'POS', Asset_Id__c = 5, SFDC_Value__c = false);
        insert AsetCls;
        
        
         LP_Custom__c lp = new LP_Custom__c();

        //lp.Name = 'a6gU0000000TN3V';
        //lp.SetupOwnerId = '00D0v0000000MZoEAM';
        lp.Default_Company__c = 'LendingPoint';
        lp.Default_Lending_Product__c = 'Lending Point';
        lp.SupportedStates__c = 'GA,NM,UT,MO,MT,SD,OR,AL,MI,WA,OH,CA,DE,NJ,TX,IL';
        //lp.Email_Alert_Portal_Apps__c = '005U0000005jmmG';
        //lp.Docusign_Notification_Email__c = '005U0000005jmmQ';
        //lp.LendingTree_Offer_Alert__c = '005U0000005jmfZ';
        //lp.Super_Money_Sub_Source__c = 'Supermoney';
        //lp.Super_Money_URL__c = 'http://track.supermoney.com/aff_lsr?offer_id=198&transaction_id=';
        lp.Use_Offer_API__c = true;
        lp.App_Contract_Start_Offset_days__c = 2;
        lp.Online_Payment_Alert__c = 'test@test.com';
        lp.Days_before_Declined_Retry__c = 180;
		lp.Gugg1_minimum_APY__c = 0.1099;
        lp.Gugg2_minimum_APY__c = 0.0999;
        lp.dummy_offers_email_domains__c = 'DONOTDELETETHISINPRODUTION!';
        lp.AAN_To_Send_Per_Batch__c = 40;
        lp.States_To_Assign_Immediately__c = 'GA,NM,UT,MO,MT,SD,OR,AL,MI,WA,OH,CA,DE,NJ,TX,IL';
        insert lp;
    }
    
    @isTest static void pending_logic(){
          //Load csv file
        Test.loadData(filegen__File_Metadata__c.sObjectType, 'LendingFilegen');
        
        //Setup seed data
        TestHelperForManaged.createSeedDataForTesting();
        //TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupMetadataSegments();
        loan.TestHelper.systemDate = Date.newInstance(2014, 3, 3); // Mar 3 - Monday
        Contact con = new Contact();
        con.LastName = 'Test';
        con.FirstName = 'Dan';
        con.Other_Use_Of_Funds__c = 'Test';
        Date birth = date.newInstance(1990, 12, 31);
        date endDate = date.newInstance(2016, 12, 1);
        date startDate = date.newInstance(2016, 10, 1);
        
        con.Previous_Employment_End_Date__c = endDate;
        con.Previous_Employment_Start_Date__c = startDate;
        con.Birthdate = birth;
        con.Email ='test@gmail.com';
        con.Employment_Start_Date__c = startDate;
        //con.ints__Social_Security_Number__c = SSN;
        con.MailingPostalCode = '30144';
        con.MailingState = 'GA';
        insert con;
        
        
        
        
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
        
        loan__Transaction_Approval_Config__c c = new loan__Transaction_Approval_Config__c();
        c.loan__Payment__c = true;
        c.loan__Payment_Reversal__c = false;
        c.loan__Funding__c = false;
        c.loan__Funding_Reversal__c = false;
        c.loan__Write_off__c = false;
        c.loan__Accounting__c = false;
        
        insert c;
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        dummyOffice.loan__Current_System_Date__c = Date.today();
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                                                                          dummyAccount, 
                                                                          curr, 
                                                                          dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = TestHelperForManaged.createLoanPurpose();
        
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        b1.peer__National_Id__c = '1223';
        update b1;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Advance Trust Account');
        
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
        
        //Create a dummy Loan Account
       loan__Loan_Account__c dummylaMonthly =  LibraryTest.createContractTH();
        dummylaMonthly.loan__Loan_Status__c = 'Active - Good Standing';
        dummylaMonthly.loan__Borrower_ACH__c = dummyBank.Id;
        dummylaMonthly.loan__ACH_On__c = true;
        dummylaMonthly.loan__ACH_Frequency__c = 'Monthly';
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = Date.today().addMonths(1);
        dummylaMonthly.loan__ACH_Start_Date__c = Date.today();
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__Ach_Debit_Day__c = 3;
        dummylaMonthly.loan__ACH_End_Date__c = Date.today().addYears(1);
        dummylaMonthly.Test_Bucket_Date__c = Date.today();
        dummylaMonthly.loan__Last_Accrual_Date__c = Date.today();
        
        
             dummylaMonthly.loan__Maturity_Date_Current__c = dummylaMonthly.loan__ACH_End_Date__c;
        dummylaMonthly.loan__Disbursal_Date__c = Date.today();
        dummylaMonthly.loan__Principal_Remaining__c = 1000;
         Update dummylaMonthly;
        dummylaMonthly.Is_On_Discount_Promotion__c = true;
        dummylaMonthly.Discount_End_Date__c = Date.today().addDays(90);
        dummylaMonthly.loan__Principal_Remaining__c = 1000;
        Update dummylaMonthly;
        dummylaMonthly.Is_On_Discount_Promotion__c = false;
        dummylaMonthly.Discount_End_Date__c = null;
        dummylaMonthly.loan__Principal_Remaining__c = 1000;
        Update dummylaMonthly;
        
        loan__Payment_Mode__c payModDisbusral = [Select id,Name from loan__Payment_Mode__c where Name='ACH'];
        
               loan__Loan_Disbursal_Transaction__c loanDis = new loan__Loan_Disbursal_Transaction__c(loan__Loan_Account__c = dummylaMonthly.Id,
                                                                                              loan__Mode_of_Payment__c=payModDisbusral.id,
                                                                                              loan__Disbursed_Amt__c= 10000,
                                                                                              loan__Cleared__c = true
                                                                                             );
        insert loanDis;
   
        
        System.debug('Payment mode: '+loanDis.loan__Mode_of_Payment__c+'  :: name:: '+payModDisbusral.name+'  :: id::'+payModDisbusral.id);
        System.debug('Loan Status: '+dummylaMonthly.loan__Loan_Status__c);
        system.debug('disbursement cleared:'+loanDis.loan__Cleared__c);
        test.startTest();
        loan.LoanDisbursalTxnSweepToACHJob j = new loan.LoanDisbursalTxnSweepToACHJob(false);
        Database.executeBatch(j);
        test.stopTest();
        
        loan__Payment_Mode__c paymentMode = [Select Id from loan__Payment_Mode__c limit 1];
        loan__Loan_Payment_Transaction__c  pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = dummylaMonthly.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today().addDays(-15);
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Payment_Mode__c = paymentMode.Id;

        pmt.Payment_Pending_End_Date__c = Date.today().addDays(3);
        insert pmt;
        
        dummylaMonthly.Active_Charge_Off__c = true;
        dummylaMonthly.loan__Oldest_Due_Date__c = Date.today().addDays(-30);
        dummylaMonthly.loan__Due_Day__c = 1;
        dummylaMonthly.loan__Number_of_Days_Overdue__c = 20;
        dummylaMonthly.loan__Payment_Amount__c = 200;
        update dummylaMonthly;
        
        
        System.assert([Select Id, Is_Payment_Pending__c from loan__Loan_Account__c where Id =: dummylaMonthly.Id].Is_Payment_Pending__c, 'Not Payment Pending');
        dummylaMonthly.loan__Charged_Off_Date__c = Date.today();
        dummylaMonthly.Asset_Sale_Date__c = Date.today();
		update dummylaMonthly;
        
        dummylaMonthly.Is_On_Discount_Promotion__c = true;
        dummylaMonthly.Discount_End_Date__c = Date.today();
        dummylaMonthly.loan__Pmt_Amt_Cur__c = 200;
        dummylaMonthly.loan__Principal_Remaining__c = 1000;
		update dummylaMonthly;
        
        
        
    } 
    
    @isTest static void updates_ach_next_debit_date() {

        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        contract.loan__Interest_Accrued_Not_Due__c = 500;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(5);
        contract.loan__Loan_Status__c = 'Approved';
        contract.loan__Oldest_Due_Date__c = Date.today().adddays(-1);
        update contract;
        
 

        Offer__c off = new Offer__c();
        off.IsSelected__c = true;
        off.Opportunity__c = contract.Opportunity__c;
        off.APR__c = 0.2;
        off.Fee__c = 100;
        off.Fee_Percent__c = 0.04;
        off.FirstPaymentDate__c = Date.today().addDays(-29);
        off.Installments__c = 36;
        off.Loan_Amount__c = 5000;
        off.Term__c = 36;
        off.Payment_Amount__c = 230;
        off.Repayment_Frequency__c = 'Monthly';
        off.Preferred_Payment_Date__c = Date.today().addDays(-23);
        insert off;
         

        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c 
                                                              FROM loan_Refinance_Params__c 
                                                              WHERE Contract__c =: contract.Id 
                                                              LIMIT 1];
        
        loan_Refinance_Params__c refinanceParams = null;
        
        if (refinanceParamsList.size() > 0){
            refinanceParams = refinanceParamsList.get(0);
        }
        else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;

        List<String> contractList = new List<String>();
        contractList.add(contract.Id);

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];
        //        RefinancePostSoftPull.RefinancePostSoftPull(contractList);

    }
    
    @isTest static void case_reassignment_based_on_contract_payment_plan() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        
        Case c = new Case();
        c.RecordTypeId = Label.RecordType_Collections_Id;
        c.Origin = 'Collections';
        c.Subject = 'A collection case has been created for LAI-00000000';
        c.Status = 'Manual Second Presentment';
        c.ContactId = contract.loan__Contact__c;
        c.Opportunity__c = contract.Opportunity__c;
        c.description = 'Collection case has been created.';
        c.CL_Contract__c = contract.Id;
        insert c;


        loan_Plan_Payment__c paymentPlan = new loan_Plan_Payment__c ();
        paymentPlan.Payment_Amount__c = 500;
        paymentPlan.Status__c = 'Pending';
        paymentPlan.Loan_Account__c = contract.Id;
        insert paymentPlan;
        
        contract.loan_Active_Payment_Plan__c = true;
        update contract;

        System.assertEquals([SELECT CreatedById FROM loan_Plan_Payment__c WHERE Loan_Account__c = :contract.Id LIMIT 1].CreatedById,[SELECT CreatedById FROM Case where Id= :c.Id LIMIT 1].CreatedById);
    }

    @isTest static void case_reassignment_based_on_contract_dmc() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        
        Case c = new Case();
        c.RecordTypeId = Label.RecordType_Collections_Id;
        c.Origin = 'Collections';
        c.Subject = 'A collection case has been created for LAI-00000000';
        c.Status = 'Manual Second Presentment';
        c.ContactId = contract.loan__Contact__c;
        c.Opportunity__c = contract.Opportunity__c;
        c.description = 'Collection case has been created.';
        c.CL_Contract__c = contract.Id;
        insert c;
        
        contract.DMC__c = 'Test';
        update contract;
        
        for (Group g : [SELECT Id FROM Group WHERE Name='CollectionGroupFrom60to119' LIMIT 1]) {
            c = [SELECT OwnerId FROM Case WHERE Id = :c.Id LIMIT 1];
            System.assert([SELECT COUNT() FROM GroupMember WHERE GroupId = :g.id AND UserOrGroupId = :c.OwnerId]==1);
        }
    }
    
    @isTest static void SR_Accrued_Interest_test() {

        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        //contract.loan__Interest_Accrued_Not_Due__c = 500;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(5);
        //contract.loan__Loan_Status__c = 'Approved';
        contract.loan__Oldest_Due_Date__c = Date.today().adddays(3);
        contract.loan__Last_Accrual_Date__c = Date.today().adddays(-37);
        contract.loan__Interest_Rate__c = 12.23;
        contract.loan__Accrual_Stop_Indicator__c = false;
        contract.loan__Charged_Off_Date__c = Null;
        update contract;
        
        loan__loan_Account__c  acc = [SELECT Id, Name, loan__Interest_Accrued_Not_Due__c, loan__Interest_Accrued_On_Investment_Loans__c, loan__Last_Accrual_Date__c, loan__Interest_Rate__c, loan__Principal_Remaining__c, loan__Accrual_Stop_Indicator__c 
                                      FROM loan__loan_Account__c 
                                      where id =: contract.Id];
        
        system.assertNotEquals(0, acc.loan__Interest_Accrued_On_Investment_Loans__c);


    }
    
    @isTest static void DebitCard_Checktest() {

        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        //contract.loan__Interest_Accrued_Not_Due__c = 500;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(5);
        //contract.loan__Loan_Status__c = 'Approved';
        contract.loan__Oldest_Due_Date__c = Date.today().adddays(3);
        contract.loan__Last_Accrual_Date__c = Date.today().adddays(-37);
        contract.loan__Interest_Rate__c = 12.23;
        contract.loan__Accrual_Stop_Indicator__c = false;
        contract.loan__Charged_Off_Date__c = Null;
        contract.Is_Debit_Card_On__c = true;
        update contract;
        
        loan__loan_Account__c  acc = [SELECT Id, Name, PaymentMethod__c 
                                      FROM loan__loan_Account__c 
                                      where id =: contract.Id];
        
        system.assertEquals('Debit Card', acc.PaymentMethod__c);


    }

}