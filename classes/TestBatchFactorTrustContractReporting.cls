@IsTest
public class TestBatchFactorTrustContractReporting{    
    TestMethod static void doVerify1(){  
       Test.startTest();  
            BatchFactorTrustContractReporting batchapex = new BatchFactorTrustContractReporting();
            
            loan__Loan_Account__c cl = TestHelper.createContract();
            List<loan__loan_Account__c> scope = new List<loan__loan_Account__c>();
            scope.add(cl);
            
           // id batchprocessid = Database.executebatch(batchapex,1); 
            
            Database.BatchableContext BC;          
            batchapex.execute(BC, scope) ;      
       Test.stopTest();               
    }
    
    TestMethod static void doVerify2(){  
       Test.startTest();  
            BatchFactorTrustContractReporting batchapex = new BatchFactorTrustContractReporting();
            
            loan__Loan_Account__c cl = TestHelper.createContract();
            List<loan__loan_Account__c> scope = new List<loan__loan_Account__c>();
            scope.add(cl);
            
            id batchprocessid = Database.executebatch(batchapex,1); 
            
            Database.BatchableContext BC;          
          //  batchapex.execute(BC, scope) ;      
       Test.stopTest();               
    }
}