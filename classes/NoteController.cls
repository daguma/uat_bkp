public class NoteController {
    
    @AuraEnabled
    public static void saveNote(Note noteEntity) {
        try {
            insert noteEntity;
        }catch(Exception e) {
            throw new AuraHandledException('Note insertion failed due to : ' + e.getMessage());
        }
    }

}