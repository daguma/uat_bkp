@isTest public with sharing class CallContractTest {

    @isTest static void testContractCreation() {

        loan__Loan_Account__c  ln= TestHelper.createContract();
        Opportunity app = LibraryTest.createApplicationTH();
        app.Lending_Account__c= ln.id;
        update app;

        string year = '2017';
        string month = '10';
        string day = '5';
        string hour = '12';
        string minute = '20';
        string second = '20';
        string stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
        CallContract.ContractCreation(app.Id, stringDate);

    }

}