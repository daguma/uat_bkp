global with sharing class FinwiseNACHAUpload {

    public static FinwiseDetails__c FinwiseParams = FinwiseDetails__c.getInstance();
    public static LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults(); 
    
	
    public static string parseStatus(string respons){
        JSONParser parser = JSON.createParser(respons);
        
        while(parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                String fieldName = parser.getText();          
                parser.nextToken();                   
                if(fieldName == 'Status'){
                    return parser.getText();                                
                }
            } 
        }              
        return null;              
    }   
       
    public static HttpRequest getReq(String Url, String method) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setTimeout(100000);
        req.setMethod(method);
        req.setHeader('Accept', 'application/json');         
        return req;
    }
    
    @future(callout=true)
    public static void uploadFile(blob docBody, string currentDocName){
        Boolean isSuccess = false, bIsSandbox = OfferCtrlUtil.runningInASandbox(), bIsEzVerify = false;
        ezVerify_Settings__c ezVerifyCustom = ezVerify_Settings__c.getOrgDefaults(); 

        try{            
            
            system.debug('File size: ' + docbody.size() + ' ' + 'Max size: ' + FinwiseParams.Max_File_Size__c);
            if(docbody.size() > FinwiseParams.Max_File_Size__c){ 
                string docNoteBody = 'Document named as '+currentDocName+' was not sent to Finwise as it exceeds the file limit.';                  
                system.debug('NACHA File too large to send to Finwise, Document Name: ' + currentDocName);
            }else{                                                             
                 
                String reqEndPoint;
                If (!bIsSandbox){
                    reqEndPoint = FinwiseParams.Finwise_Base_URL__c+'/partner/'+  (bIsEzVerify ? ezVerifyCustom.EZ_Finwise_Product_Key__c :  FinwiseParams.Finwise_Partner_Key__c) +'/document';
                }else{
                    reqEndPoint = FinwiseParams.Finwise_Base_URL_Dev__c+'/partner/'+  (bIsEzVerify ? ezVerifyCustom.EZ_Finwise_Product_Key__c :  FinwiseParams.Finwise_Partner_Key__c) +'/document';
                }
                system.debug('reqEndPoint: ' + reqEndPoint);
                
                String boundary = '----------------------------acebdf13572468';
                String header = '--'+boundary+'\r\nContent-Disposition: form-data; name="'+currentDocName.substringBeforeLast('.')+'"; filename="'+currentDocName+'"\r\nContent-Type: text/plain'; 
                String footer = '\r\n--'+boundary+'--\r\n';              
                String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
                while(headerEncoded.endsWith('='))
                {
                    header+=' ';
                    headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
                }
                
                String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                
                Blob bodyBlob = null;          
                transient String last4Bytes = EncodingUtil.base64Encode(docbody).substring(EncodingUtil.base64Encode(docbody).length()-4,EncodingUtil.base64Encode(docbody).length());
                if(last4Bytes.endsWith('=')){              
                    Blob decoded4Bytes = EncodingUtil.base64Decode(last4Bytes);
                    HttpRequest tmp = new HttpRequest();
                    tmp.setBodyAsBlob(decoded4Bytes);
                    String last4BytesFooter = tmp.getBody()+footer;   
                    bodyBlob = EncodingUtil.base64Decode(headerEncoded+EncodingUtil.base64Encode(docbody).substring(0,EncodingUtil.base64Encode(docbody).length()-4)+EncodingUtil.base64Encode(Blob.valueOf(last4BytesFooter)));               
                }else{                            
                    bodyBlob = EncodingUtil.base64Decode(headerEncoded+EncodingUtil.base64Encode(docbody)+footerEncoded);                
                }
                docbody = null;
                HttpRequest req = getReq(reqEndPoint, 'POST');
                req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);     
                req.setBodyAsBlob(bodyBlob);                                  
                bodyBlob = null;
                headerEncoded = null;                      
                Http http = new Http();        
                HTTPResponse res;
                
                if(!Test.isRunningTest()){
                    res = http.send(req);                        
                }else{
                    res = new HTTPResponse();                    
                    res.SetBody(TestHelper.createDomDocNew().toXmlString());               
                }
                System.debug(LoggingLevel.Debug, 'Heap Size: ' + Limits.getHeapSize() + '/' + Limits.getLimitHeapSize());
                req = null;
                system.debug('========res========================='+res);
                system.debug('========res doc========================='+res.getBody()); 
                string RespLog = 'Response: '+res +'\n Response Body: '+res.getBody();
                
                if(res.getStatusCode() == 200){              
                    if(parseStatus((String)res.getBody()) <> 'Success'){                                     
                       system.debug('Finwise NACHA Processing Unsuccessful ' + currentDocName + ' ' + RespLog);
                    }
                }else{                        
                    system.debug('Finwise NACHA Processing Error ' + currentDocName + ' ' + RespLog);
                }
            }             
            
        }catch(Exception exc){                             
            string ExcpNoteBody = 'Finwise NACHA Processing Exception:  '+exc+' at Line No. '+exc.getLineNumber();                  
            system.debug('Finwise NACHA Processing Exception ' + currentDocName + ' ' + ExcpNoteBody);                            
            WebToSFDC.notifyDev('Finwise NACHA Processing Exception',  exc.getMessage() + ' line ' + (exc.getLineNumber()) + '\n' + exc.getStackTraceString()); 
            system.debug(ExcpNoteBody);
        }                              
    }  
}