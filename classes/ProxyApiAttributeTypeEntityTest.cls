@isTest public class ProxyApiAttributeTypeEntityTest {

    @isTest static void validate_instance() {

        Test.startTest();
        ProxyApiAttributeTypeEntity te = new ProxyApiAttributeTypeEntity();
        Test.stopTest();

        System.assertEquals(null, te.id);
        System.assertEquals(null, te.isDisplayed);
        System.assertEquals(null, te.conceptType);
        System.assertEquals(null, te.displayName);
        System.assertEquals(null, te.description);
    }

    @isTest static void validate() {

        ProxyApiAttributeTypeEntity te = new ProxyApiAttributeTypeEntity();

        te.id = 1;
        System.assertEquals(1, te.id);

        te.isDisplayed = true;
        System.assertEquals(true, te.isDisplayed);

        te.conceptType = 'test data';
        System.assertEquals('test data', te.conceptType);

        te.displayName = 'test data 1';
        System.assertEquals('test data 1', te.displayName);

        te.description = 'test data';
        System.assertEquals('test data', te.description);
    }

    @isTest static void validates_assignment() {

        ProxyApiAttributeTypeEntity te = new ProxyApiAttributeTypeEntity('test name', 'test description');

        System.assertEquals('test name', te.displayName);
        System.assertEquals('test description', te.description);
        System.assertEquals(true, te.isDisplayed);
    }

}