@isTest
private class EmailAutomatedJobTest {
    
    @isTest static void execute() {
        try {
            Test.startTest();
            EmailAutomatedJob email_job = new EmailAutomatedJob();
            String cron = '0 0 23 * * ?';
            system.schedule('Test Schedule', cron, email_job);
            Test.stopTest();
            System.assert(true);
        } catch (Exception e) {
            System.debug('EmailAutomatedJob: execute: ' + e.getMessage());
        }
    }
    
    @isTest static void payment_confirmation(){

        Integer notesBefore = [SELECT Count() FROM Note];
        
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Disbursal_Date__c = Date.today().addDays(-2);
        contract.loan__Disbursal_Status__c = 'Completed';
        contract.loan__Disbursal_Amount__c = 5000;
        contract.loan__loan_status__c = 'Active - Good Standing';
       
        contract.loan__ACH_Debit_Day__c = 1;
        contract.loan__Next_Installment_Date__c = Date.today().addMonths(1);
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__First_Installment_Date__c = Date.today().addMonths(-2);                            
        contract.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addMonths(1);
        contract.loan__ACH_On__c = true;
        contract.loan__ACH_Debit_Amount__c = 100;
        contract.loan__ACH_Start_Date__c = Date.today().addMonths(-2);
        contract.loan__ACH_End_Date__c = Date.today().addMonths(6);
        contract.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        contract.loan__ACH_Account_Type__c = 'Checking';
        contract.loan__ACH_Account_Number__c = '123456';
        contract.loan__ACH_Routing_Number__c = '123456123';
        contract.loan__ACH_Relationship_Type__c = 'PRIMARY';
        contract.Reporting_Fee__c = 300;
        contract.Reporting_Fee_Balance__c = 300;
        contract.Reporting_Interest__c = 1300;
        contract.PaymentDue7DayReminderDate__c = Date.today().addMonths(-1);
        update contract;
        
        Contact c = [SELECT Id, AccountId FROM Contact WHERE Id =: contract.loan__Contact__c];

        loan__Bank_Account__c bank = new loan__Bank_Account__c();
        bank.loan__Account__c = c.AccountId;
        bank.loan__Account_Type__c = 'Checking';
        bank.loan__Account_Usage__c = 'Test Dummy Account';
        bank.loan__Active__c = true;
        bank.loan__Bank_Account_Number__c = '098765433';
        bank.loan__Bank_Name__c = '99';
        bank.peer__Branch_Code__c = '998';
        bank.loan__Routing_Number__c = '98776631';
        insert bank;

        contract.loan__Borrower_ACH__c = bank.Id;
        update contract;
        
        Test.startTest();
        
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name = 'ACH'];
        
        loan__Loan_Payment_Transaction__c payment = new loan__Loan_Payment_Transaction__c();
        payment.loan__balance__c = 100;
        payment.loan__Loan_Account__c = contract.id;
        payment.loan__Transaction_Amount__c = 100;
        payment.loan__Principal__c = 100;
        payment.loan__Interest__c = 10;
        payment.loan__Reversed__c = false;
        payment.loan__Payment_Mode__c = pm.Id;
        insert payment;
        
        Test.stopTest();
        
        System.debug('Payments ' + [SELECT COUNT() FROM loan__loan_payment_transaction__c]);
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        EmailAutomatedJob emailJob = new EmailAutomatedJob();
        emailJob.initialize();
        emailJob.paymentConfirmation(emails);
        
        System.debug([SELECT Confirmation_Email_Sent_Date__c FROM loan__Loan_Payment_Transaction__c where id =: payment.Id Limit 1].Confirmation_Email_Sent_Date__c);
        System.debug([SELECT Count() FROM Note]);
        
        Datetime confirmationDatetime = [SELECT Confirmation_Email_Sent_Date__c FROM loan__Loan_Payment_Transaction__c where id =: payment.Id Limit 1].Confirmation_Email_Sent_Date__c;
        
        Date confirmationDate = Date.newInstance(confirmationDatetime.year(), confirmationDatetime.month(), confirmationDatetime.day());
        
        System.assertEquals(Date.today(), confirmationDate);
        System.assert([SELECT Count() FROM Note] > notesBefore);
    }

    @isTest static void payment_reminder(){

        Integer notesBefore = [SELECT Count() FROM Note];
        
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
        System.RunAs(usr)
        {
            Test.startTest();
            EmailTemplate e = new EmailTemplate (developerName = 'LP_Payment_Due_reminder_V2', TemplateType= 'Text', Name = 'LP_Payment_Due_reminder_V2', FolderId = UserInfo.getUserId()); 
            insert e;
            emailAutoJob();
            Test.stopTest();
        }
        
        /*List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        EmailAutomatedJob emailJob = new EmailAutomatedJob();
        emailJob.initialize();
        emailJob.sendPaymentReminder(emails);
        */
        Date reminderDatetime = [SELECT PaymentDue7DayReminderDate__c FROM loan__loan_account__c where id =: contract.Id Limit 1].PaymentDue7DayReminderDate__c;
        /*if (reminderDatetime == null) reminderDatetime = Date.today();
        Date reminderDate = Date.newInstance(reminderDatetime.year(), reminderDatetime.month(), reminderDatetime.day());
        
        System.assertEquals(Date.today(), reminderDate);
        System.assert([SELECT Count() FROM Note] > notesBefore);*/
    }
    
    @future
    private static void emailAutoJob()
    {
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        EmailAutomatedJob emailJob = new EmailAutomatedJob();
        emailJob.initialize();
        emailJob.sendPaymentReminder(emails);
        
        //Date reminderDatetime = [SELECT PaymentDue7DayReminderDate__c FROM loan__loan_account__c where id =: contract.Id Limit 1].PaymentDue7DayReminderDate__c;
    }
    
}