@isTest 
private class AONRestResourceTest {

    @isTest static void creates_payment_successfully() {
	
		loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Last_Transaction_Timestamp__c = System.now();
        update contract;
        
        loan_AON_Information__c aonInfo = new loan_AON_Information__c();
        aonInfo.Contract__c = contract.Id;
        aonInfo.Product_Code__c = 'LPOPTION01';
        insert aonInfo;

        String aonDate = '2018-01-01';
		
		String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id =: contract.Id LIMIT 1].Name;
	
        String inputData = '{"accountNumber":"'+contractName+'","companyCode":"LPT0010000","productCode":"LPOPTION01","claimNumber":"","claimStatus":"","claimStatusDate":"'+aonDate+'","claimDisposition":"","paymentAmount":"200","paymentDate":"'+aonDate+'","paymentType":""}';
	
        String inputDataEncoded = EncodingUtil.base64Encode(Blob.valueOf(inputData));
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
		String result = AONRestResource.httpPostService('createPayment',inputDataEncoded);
        
        Test.stopTest();
		
        System.assertEquals('Transaction was successful', result);
	}
    
	@isTest static void creates_payment_unsuccessfully() {
	
		loan__Loan_Account__c contract = LibraryTest.createContractTH();
		
		String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id =: contract.Id LIMIT 1].Name;
	
        String inputData = '{"accountNumber":"'+contractName+'","companyCode":"LPT0010000","productCode":"LPOPTION01","claimNumber":"","claimStatus":"","claimStatusDate":"","claimDisposition":"","paymentAmount":"200","paymentDate":"","paymentType":""}';
	
        String inputDataEncoded = EncodingUtil.base64Encode(Blob.valueOf(inputData));
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
		String result = AONRestResource.httpPostService('createPayment',inputDataEncoded);
        
        Test.stopTest();
		
        System.assertEquals('Transaction wasn\'t successful', result);
	}
    
    @isTest static void creates_payment_with_post_service_null() {
        
        AONRestResource ini = new AONRestResource();
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
		String result = AONRestResource.httpPostService(null, null);
        
        Test.stopTest();
		
        System.assertEquals('Transaction wasn\'t successful', result);
	}
    
    @isTest static void other_than_create_payment() {
	
		loan__Loan_Account__c contract = LibraryTest.createContractTH();
		
		String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id =: contract.Id LIMIT 1].Name;
	
        String inputData = '{"accountNumber":"'+contractName+'","companyCode":"LPT0010000","productCode":"LPOPTION01","claimNumber":"","claimStatus":"","claimStatusDate":"","claimDisposition":"","paymentAmount":"200","paymentDate":"","paymentType":""}';
	
        String inputDataEncoded = EncodingUtil.base64Encode(Blob.valueOf(inputData));
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
		String result = AONRestResource.httpPostService('other',inputDataEncoded);
        
        Test.stopTest();
		
        System.assertEquals('Transaction wasn\'t successful', result);
	}
    
    @isTest static void cancels_service() {
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = clcontract.Id;
        enrollment.IsProcessed__c = true;
        enrollment.Product_Code__c = 'LPOPTION01';
        enrollment.Enrollment_Date__c = System.now();
        insert enrollment;
        
        AONObjectsParameters.AONCancellationInputData cancellationData = new AONObjectsParameters.AONCancellationInputData(clcontract.Name, 'JLP', '0');
        
        String inputData = new AONObjectsParameters.AONCancellationInputData().get_JSON(cancellationData);
        
        String inputDataEncoded = EncodingUtil.base64Encode(Blob.valueOf(inputData));
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
		String result = AONRestResource.httpPostService('aim_cancellation', inputDataEncoded);
        
        Test.stopTest();
        
        System.assert(result.containsIgnoreCase('"statusCode" : "201"'), 'Successful Cancellation');
    }
    
    @isTest static void cancels_service_error() {
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = clcontract.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;
        
        AONObjectsParameters.AONCancellationInputData cancellationData = new AONObjectsParameters.AONCancellationInputData(clcontract.Name, 'JLP', '0');
        
        String inputData = new AONObjectsParameters.AONCancellationInputData().get_JSON(cancellationData);
        
        String inputDataEncoded = EncodingUtil.base64Encode(Blob.valueOf(inputData));
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
		String result = AONRestResource.httpPostService('aim_cancellation', inputDataEncoded);
        
        Test.stopTest();
        
        System.assert(result.containsIgnoreCase('Account not Enrolled'), 'Failed Cancellation');
    }
    
    @isTest static void catches_exception_when_cancel() {
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = clcontract.Id;
        enrollment.IsProcessed__c = true;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;
        
        AONObjectsParameters.AONCancellationInputData cancellationData = new AONObjectsParameters.AONCancellationInputData(clcontract.Name, 'JLP', '0');
        
        String inputData = new AONObjectsParameters.AONCancellationInputData().get_JSON(cancellationData);
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
		String result = AONRestResource.httpPostService('aim_cancellation', inputData);
        
        Test.stopTest();
        
        System.assertEquals('Transaction wasn\'t successful', result);
    }

}