@isTest private class AutomaticAssignmentUtilsTest {

    @isTest static void users_group_contains() {

        DealAutomaticUserGroup daug = new DealAutomaticUserGroup('userId_1000', 'userName_Test', 'groupId_2000', 'groupName_Test', true, false);

        List<DealAutomaticUserGroup> lDaug = new List<DealAutomaticUserGroup>();
        lDaug.add(daug);

        Boolean result = AutomaticAssignmentUtils.userGroupContains(daug, new List<DealAutomaticUserGroup>());

        System.assertEquals(false, result);

        result = AutomaticAssignmentUtils.userGroupContains(daug, lDaug);

        System.assertEquals(true, result);
    }

    @isTest static void combines_list() {
        DealAutomaticUserGroup daug = new DealAutomaticUserGroup('userId_1000', 'userName_Test', 'groupId_2000', 'groupName_Test', true, false);

        List<DealAutomaticUserGroup> lDaug = new List<DealAutomaticUserGroup>();
        lDaug.add(daug);

        List<DealAutomaticUserGroup> result = AutomaticAssignmentUtils.combineList(lDaug, new List<DealAutomaticUserGroup>());

        System.assertEquals(new List<DealAutomaticUserGroup>(), result);

        DealAutomaticUserGroup daug2 = new DealAutomaticUserGroup('userId_1001', 'userName_Test1', 'groupId_2001', 'groupName_Test1', true, false);
        List<DealAutomaticUserGroup> lDaug2 = new List<DealAutomaticUserGroup>();
        lDaug2.add(daug2);

        result = AutomaticAssignmentUtils.combineList(lDaug, lDaug2);

        System.assertEquals(new List<DealAutomaticUserGroup>(), result);

        lDaug.add(daug2);

        result = AutomaticAssignmentUtils.combineList(lDaug, lDaug2);

        System.assertEquals(lDaug2, result);
    }

    @isTest static void gets_moda() {
        List<Integer> listOfNumbers = new List<Integer>();
        listOfNumbers.add(20);
        listOfNumbers.add(5);
        listOfNumbers.add(15);
        listOfNumbers.add(5);
        listOfNumbers.add(42);

        Integer result = AutomaticAssignmentUtils.getModa(listOfNumbers);
        System.assertEquals(5, result);
    }

    @isTest static void gets_mean() {
        List<Integer> listOfNumbers = new List<Integer>();
        listOfNumbers.add(20);
        listOfNumbers.add(5);
        listOfNumbers.add(15);
        listOfNumbers.add(13);
        listOfNumbers.add(42);

        Integer result = AutomaticAssignmentUtils.getMean(listOfNumbers);
        System.assertEquals(19, result);
    }

    @isTest static void is_sandbox() {
        Boolean result = AutomaticAssignmentUtils.isSandbox();

    }

    @isTest static void removes_user_from_list() {
        DealAutomaticUserGroup group1 = new DealAutomaticUserGroup('005U0000004YXGiIAO', 'Kimberly Gardner', '00GU0000002ccJvMAI', 'Kennesaw Sales Center', true, false);
        List<DealAutomaticUserGroup> listOfGroups = new List<DealAutomaticUserGroup>();
        listOfGroups.add(group1);

        List<DealAutomaticUserGroup> result = AutomaticAssignmentUtils.removeUserFromList(listOfGroups, '005U0000004YXGiIAO');

        System.assertEquals(0, result.size());

    }

    @isTest static void saves_user_online_mode() {
        List<User> user1 = [Select Id, Name, IsActive, isOnline__c From User LIMIT 1];

        AutomaticAssignmentUtils.saveUserOnlineMode(user1.get(0).id, true);

        User user2 = [SELECT isOnline__c FROM User WHERE id = : user1.get(0).id LIMIT 1];

        System.assertEquals(true, user2.isOnline__c);
    }
    
    @isTest static void gets_user_from_group_by_name(){
        DealAutomaticUserGroup daug = new DealAutomaticUserGroup('userId_1000', 'userName_Test', 'groupId_2000', 'groupName_Test', true, false);

        List<DealAutomaticUserGroup> lDaug = new List<DealAutomaticUserGroup>();
        lDaug.add(daug);
        
        DealAutomaticUserGroup result = AutomaticAssignmentUtils.getUserFromGroupByName('userName_Test', lDaug);
        
        System.assertNotEquals(null, result);
    }
    
    @isTest static void gets_user_from_group_does_not_find_user(){
        DealAutomaticUserGroup daug = new DealAutomaticUserGroup('userId_1000', 'userName_Test', 'groupId_2000', 'groupName_Test', true, false);

        List<DealAutomaticUserGroup> lDaug = new List<DealAutomaticUserGroup>();
        lDaug.add(daug);
        
        DealAutomaticUserGroup result = AutomaticAssignmentUtils.getUserFromGroupByName('userName', lDaug);
        
        System.assertEquals(null, result);
    }
    
    @isTest static void shuffle_user(){
        DealAutomaticUserGroup daug = new DealAutomaticUserGroup('userId_1000', 'userName_Test', 'groupId_2000', 'groupName_Test', true, false);

        List<DealAutomaticUserGroup> lDaug = new List<DealAutomaticUserGroup>();
        lDaug.add(daug);
        
        List<DealAutomaticUserGroup> result = AutomaticAssignmentUtils.shuffleUsers(lDaug);
        
        System.assertNotEquals(null, result);
    }
    
    @isTest static void gets_next_user(){
        
        TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimesAllAssignedToKennesaw();
        
        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'TodayLeads';
        attribute.Display_Name__c = 'Today\'s Leads';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;
        attribute.Key_Name__c = 'Test';
        insert attribute;

        Deal_Automatic_Assignment_Data__c data = new Deal_Automatic_Assignment_Data__c();
        data.Attribute_Id__c = 1;
        data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                 '"groupName":"Kennesaw Sales Center","groupId":"' + Label.Kennesaw_Group_Id + '"}]';
        insert data;
        
        DealAutomaticUserGroup daug = new DealAutomaticUserGroup(UserInfo.getUserId() , 'userName_Test', Label.DataEntry_Group_Id, 'groupName_Test', true, false);

        List<DealAutomaticUserGroup> lDaug = new List<DealAutomaticUserGroup>();
        lDaug.add(daug);
        
        try{
            Test.startTest();
            
            DealAutomaticAssignmentManager.GlobalParameters globalParams = new DealAutomaticAssignmentManager.GlobalParameters(false, 20);
            
            DealAutomaticUserGroup result = AutomaticAssignmentUtils.getNextUser(globalParams.userAppTotalsMap, lDaug);
            
            Test.stopTest();
        }
        catch(Exception e){}
    }
    
}