@isTest
public class TestM2 {

    @isTest static void test_LPM2_borrower_info_impl() {
        Contact c = new Contact(lastname = 'Joshi', Phone = '1234567890');
        LPM2BorrowerInfoImpl impl = new LPM2BorrowerInfoImpl();
        impl.setBorrower(c);
        impl.getBorrowerInfo();
        impl.getLoanAccountType(null);
        Integer i = LPM2QueryGenImpl.recordsLeft();
        LPM2QueryGenImpl.isBatchRunning('01pU0000001Byh2');
    }

    @isTest static void test_LPMetro2Tab_controller() {
        loan.TestHelper.createSeedDataForTesting();
        loan.TestHelper.integrateWithSFCRM();

        loan__Currency__c curr = loan.TestHelper.createCurrency();
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');

        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount , dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);

        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();

        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProductNoRules(
                                            'TestProd',
                                            dummyOffice,
                                            dummyAccount,
                                            curr,
                                            dummyFeeSet,
                                            'Declining Balance',
                                            10,
                                            10,
                                            null);

        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();

        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);
        if ([select Id from MaskSensitiveDataSettings__c].size() == 0 ) {
            MaskSensitiveDataSettings__c mac = new MaskSensitiveDataSettings__c();
            mac.MaskAllData__c = true;
            insert mac;
        }
        loan__Metro2_Parameters__c m2Params = loan.CustomSettingsUtil.getMetro2Parameters();
        m2Params.loan__Identification_Number__c = 'ABCID';
        m2Params.loan__Experian_Identifier__c = 'DAZVD';
        m2Params.loan__Program_Date__c = Date.newInstance(2015, 5, 12);
        m2Params.loan__Program_Revision_Date__c = Date.newInstance(2015, 5, 12);
        m2Params.loan__Reporter_Name__c = 'ABC';
        m2Params.loan__Reporter_Phone__c = '4129733110';
        m2Params.loan__Reporter_Address__c = 'ADDRESS';
        m2Params.loan__Metro2_Query_Class__c = 'LPM2QueryGenImpl';
        m2Params.loan__Borrower_Info_Class__c = 'LPM2BorrowerInfoImpl';
        upsert m2Params;

        Contact dummyContact = new Contact();
        dummyContact.FirstName = 'FN';
        dummyContact.LastName = 'FN';
        dummyContact.Use_of_Funds__c = 'Wedding';
        dummyContact.Email = 'test@gmail5.com';
        dummyContact.BirthDate = Date.Today().addYears(-21);
        dummyContact.Point_Code__c = '12345AB';
        dummyContact.ints__Social_security_Number__c = '999999990';
        dummyContact.Time_at_current_Address__c = Date.Today().addYears(-3);
        dummyContact.Phone = '1251231235';
        dummyContact.Mailingstreet = '234 Test street';
        dummyContact.MailingCity = 'City';
        dummyContact.MailingState = 'GA';
        dummyContact.MailingPostalCode = '123132';
        dummyContact.MailingCountry = 'US';
        dummyContact.Annual_Income__c = 100000;
        dummyContact.Loan_Amount__c = 4000;
        dummyContact.Employment_Start_date__c = Date.Today().addYears(-3);
        dummyContact.NetworkG_IDA_Date__c = Date.today();

        insert dummyContact;

        genesis__applications__c app = new genesis__applications__c();
        app.genesis__Contact__c = dummyContact.Id;
        app.Lending_Product__c = [Select Id from loan__Loan_Product__C limit 1].Id;
        app.genesis__Product_Type__c = 'LOAN';
        app.genesis__Loan_Amount__c = dummyContact.Loan_Amount__c;
        app.genesis__Days_Convention__c = '30/360';
        app.Fee__c = 0.04 * (dummyContact.Loan_Amount__c == null ? 0.0 : dummyContact.Loan_amount__c);
        app.genesis__Interest_Calculation_Method__c = 'Declining Balance';

        app.RecordType = [Select ID from RecordType where DeveloperName = : 'LOAN'
                          and SObjectType = : 'genesis__Applications__c'];

        app.ApplicationType__c = 'New';
        insert app;

        Date SystemDate = TestHelperForManaged.getBranchDate();
        Date lastDayOfMonth = SystemDate.toStartOfMonth().addDays(Date.daysInMonth(SystemDate.toStartOfMonth().year(), SystemDate.toStartOfMonth().month()) - 1);
        if (SystemDate < lastDayOfMonth) {
            SystemDate = lastDayOfMonth.addMonths(-1);
        }

        Account a  = new Account();
        a.name = 'Test Account';
        a.BillingState = 'CA';
        a.BillingCountry = 'US';
        a.BillingPostalCode = '99999';
        insert a;

        //Create a dummy Loan Account
        /*loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccountForContactObj(dummyLP,
                                            dummyContact,
                                            dummyFeeSet,
                                            dummyLoanPurpose,
                                            dummyOffice);*/
        loan__Loan_Account__c loanAccount = LibraryTest.createContractTH();
        loanAccount.loan__Loan_Status__c = loan.LoanConstants.LOAN_STATUS_ACTIVE_GOOD_STANDING;
        loanAccount.loan__Fees_Remaining__c  = 100;
        loanAccount.loan__Interest_Remaining__c = 500;
        loanAccount.loan__disbursal_date__c = systemDate.addDays(-10);
        loanAccount.loan__Principal_Remaining__c = 1000;
        loanAccount.loan__Pay_off_Amount_As_Of_Today__c = loanAccount.loan__Fees_Remaining__c + loanAccount.loan__Principal_Remaining__c;
        loanAccount.loan__Last_Accrual_Date__c = Date.Today();
        loanAccount.loan__Account__c = a.Id;
        loanAccount.loan__Previous_Installment_Date__c = Date.Today().addDays(-30);
        loanAccount.loan__Last_Payment_Date__c = Date.Today().addDays(-30);
        loanAccount.loan__Last_Payment_Amount__c = 100;
        loanAccount.loan__First_Installment_Date__c = systemDate.addDays(-10);
        loanAccount.loan__Include_In_Metro2_File__c = true;
        loanAccount.loan__Loan_Effective_Date__c = Date.Today();
        loanAccount.loan__Metro2_Account_highest_bal_amount__c = 10000;
        loanAccount.loan__Metro2_Account_Status_Code__c = '11';
        loanAccount.loan__Metro2_Payment_History__c = 'BBBBBBBBBBBBBBBBBBBBBBBB';
        loanAccount.loan__Metro2_Account_Type_Code__c = '01 - Unsecured';
        loanAccount.loan__Metro2_Payment_Rating__c = '00';
        loanAccount.loan__Metro2_Portfolio_Type__c = 'Installment';
        loanAccount.loan__Metro2_Account_highest_bal_amount__c = 100000;
        loanAccount.loan__Metro2_Special_Comment__c = 'SC';
        loanAccount.loan__Charged_Off_Fees__c = 0;
        loanAccount.loan__Charged_Off_Interest__c = 0;
        loanAccount.loan__Pmt_Amt_Cur__c = 100;
        loanAccount.loan__Charged_Off_Principal__c = 0;
        loanAccount.loan__Term_Cur__c = 24;
        loanAccount.loan__Delinquent_Amount__c = 0;
        loanAccount.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        loanAccount.loan__Metro2_First_Delinquency_Date__c  = Date.Today().addDays(-10);
        loanAccount.loan__Metro2_Account_pmt_history_date__c = systemDate.addDays(-10);
        loanAccount.loan__Last_Payment_Amount__c = 100;
        loanAccount.loan__Pmt_Amt_Cur__c = 100;
        loanAccount.loan__Last_Payment_Date__c = Date.Today().addDays(-10);
        loanAccount.loan__Charged_Off_Fees__c = 0;
        loanAccount.loan__Charged_Off_Interest__c = 0;
        loanAccount.loan__Charged_Off_Principal__c = 0;
        loanAccount.Application__c = app.id;

        update loanAccount;

        loan__CR_Org_Definition__c cr = new loan__CR_Org_Definition__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(cr);
        LPMetro2TabController con = new LPMetro2TabController(sc);
        con.generateFile();
    }

    @isTest static void test_LPMetro2Tab() {

        loan__Loan_Account__c loanAccount = LibraryTest.createContractTH();
        loanAccount.loan__Loan_Status__c = 'Active - Good Standing';
        update loanAccount;

        LPMetro2TabController con = new LPMetro2TabController();
        con.generateFile();
    }
}