@isTest public class PayrollDataCtrlTest {

    @isTest static void getPayrollDataEntity_test() {
        Contact TestContact = LibraryTest.createContactTH();
        Opportunity app = TestHelper.createApplicationFromContact(TestContact, true);

        PayrollDataCtrl.PayrollDataEntity result = PayrollDataCtrl.getPayrollDataEntity(app.Id);
        System.assertEquals(null, result.errorMessage);
    }

    @isTest static void savePayrollData_test() {
        Contact TestContact = LibraryTest.createContactTH();
        Opportunity app = TestHelper.createApplicationFromContact(TestContact, true);

        PayrollDataCtrl.PayrollDataEntity result = PayrollDataCtrl.savePayrollData(app);
        System.assertEquals(null, result.errorMessage);

        result = PayrollDataCtrl.savePayrollData(null);
        System.assertNotEquals(null, result.errorMessage);
    }

}