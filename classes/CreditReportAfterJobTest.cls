@isTest
private class CreditReportAfterJobTest {

  @isTest static void credit_report_after_job_test1() {
    opportunity app = LibraryTest.createApplicationTH();
    app.status__c = 'Credit Qualified';
    CreditReport.prepareGetTaxLiensFromLexisNexis(app.Id);

    CreditReportAfterJob job1 = new CreditReportAfterJob('LexisNexis');
    database.executebatch(job1, 1);
  }

  @isTest static void credit_report_after_job_test2() {
    opportunity app = LibraryTest.createApplicationTH();
    app.status__c = 'Credit Qualified';
    CreditReport.prepareGetAcceptanceModel(app.Id);

    CreditReportAfterJob job1 = new CreditReportAfterJob('AcceptanceModel');
    database.executebatch(job1, 1);

  }

}