@isTest public class DL_DeleteActivityByUserReportTest {

    @isTest static void deletes_reports() {
        
        DL_ActivityByUserReport__c report = new DL_ActivityByUserReport__c();
        report.Name = 'Test 1';
        report.DateRange__c = 10;
        insert report;
        
        DL_ActivityByUserReport__c report2 = new DL_ActivityByUserReport__c();
        report2.Name = 'Test 2';
        report2.DateRange__c = 5;
        insert report2;
        
        DL_DeleteActivityByUserReport delActivityRep = new DL_DeleteActivityByUserReport();
    
        System.assertEquals(0, [SELECT COUNT() FROM DL_ActivityByUserReport__c]);
    
    }
    
    @isTest static void no_reports_to_delete() {
        
        DL_DeleteActivityByUserReport delActivityRep = new DL_DeleteActivityByUserReport();
    
        System.assertEquals(0, [SELECT COUNT() FROM DL_ActivityByUserReport__c]);
    
    }
    
}