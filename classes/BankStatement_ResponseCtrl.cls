public with sharing class BankStatement_ResponseCtrl {

    @AuraEnabled
    public static String saveOpportunity(Id oppId, String dl_Account_Number_Found) {

        try{
            Opportunity opp = [SELECT Id FROM Opportunity WHERE Id =: oppId];
            opp.DL_Account_Number_Found__c = dl_Account_Number_Found;
            update opp;
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
        return 'Opp updated';

    }
}