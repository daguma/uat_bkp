global class ClearAddress {

    public String mailingStreet {get; set;}
    public String mailingCity {get; set;}
    public String mailingState {get; set;}
    public String mailingPostalCode {get; set;}

    public ClearAddress() {

    }

    public ClearAddress(String mailingStreet,
                        String mailingCity,
                        String mailingState,
                        String mailingPostalCode) {
        this.mailingStreet = mailingStreet != null ? mailingStreet : '';
        this.mailingCity = mailingCity != null ? mailingCity : '';
        this.mailingState = mailingState != null ? mailingState : '';
        this.mailingPostalCode = mailingPostalCode != null ? mailingPostalCode : '';
    }

    public Boolean isApproved(String mailingStreet,
                              String mailingCity,
                              String mailingState,
                              String mailingPostalCode) {
        return (this.mailingStreet.equalsIgnoreCase(mailingStreet) &&
                this.mailingCity.equalsIgnoreCase(mailingCity) &&
                this.mailingState.equalsIgnoreCase(mailingState) &&
                this.mailingPostalCode.equalsIgnoreCase(mailingPostalCode));
    }
}