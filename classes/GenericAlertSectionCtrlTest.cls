@isTest Private class GenericAlertSectionCtrlTest {
     
     @isTest static void Warning_Check_MultiLoanOpp() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'Multiloan';
        app.Status__c = 'Credit Qualified';
        upsert app;
        GenericAlertSectionCtrl.ContactsWarningEntity rae = GenericAlertSectionCtrl.isMultiloan(app.contact__c);

        System.assertEquals(true,rae.isMultiloan);
        System.assertEquals(false,rae.hasError);
    }
     
     @isTest static void Warning_Use_Of_Funds() {
         
         Opportunity app = LibraryTest.createApplicationTH();
         
         Contact cont = LibraryTest.createcontactTH();
         cont.Use_of_Funds__c = 'Funeral Expenses';
         update cont;
         
         app.contact__c = cont.Id;
         app.Status__c = 'Credit Qualified';
         update app;
         
         contact cont2 = [Select Id, name, Use_of_Funds__c FROM  Contact where Id =: app.Contact__c];
         
         
         Opportunity opp = [SELECT Id, Name, Contact__r.Use_of_Funds__c FROM Opportunity  WHERE Contact__c =: cont.Id];
         
         
         GenericAlertSectionCtrl.getUseOfFunds(opp.Id);
    }
     
     
     

}