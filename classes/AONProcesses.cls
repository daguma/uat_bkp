public with sharing class AONProcesses {
    private AONObjectsParameters aonParameters = new AONObjectsParameters();

    public AONProcesses() {}

    public Boolean createPayment(aonObjectsParameters.AONPaymentInputData payment) {

        for (loan_AON_Information__c AONInformation : [SELECT Id, Contract__c, Claim_Number__c, Product_Code__c, Claim_Status__c,
                Claim_Disposition__c, Contract__r.loan__Last_Transaction_Timestamp__c
                FROM loan_AON_Information__c
                WHERE Contract__r.Name = : payment.accountNumber
                                         AND Product_Code__c = : payment.productCode
                                                 ORDER BY CreatedDate DESC
                                                 LIMIT 1]) {

            loan__Loan_Account__c contract = [SELECT Id, loan__ACH_On__c, ACH_Flag_Reason__c FROM loan__Loan_Account__c WHERE Name = : payment.accountNumber LIMIT 1];
            loan__Loan_Payment_Transaction__c paymentTransaction = new loan__Loan_Payment_Transaction__c();

            Date paymentTransactionDate = (payment.paymentDate != null & !String.isEmpty(payment.paymentDate)) ? aonParameters.convertTimestamp(payment.paymentDate).Date() : null;
            System.debug('paymentTransactionDate: ' + paymentTransactionDate);
            Date contractLastTransaction = AONInformation.Contract__r.loan__Last_Transaction_Timestamp__c.Date();
            System.debug('contractLastTransaction: ' + contractLastTransaction);

            if (AONInformation.Claim_Status__c != payment.claimStatus) {
                if (payment.claimStatus == 'A') {
                    if (contract.loan__ACH_On__c) {
                        contract.loan__ACH_On__c = false;
                        contract.ACH_Flag_Reason__c = 'Active Payment Safeguard Claim';
                        update contract;
                    }
                }
                AONInformation.Claim_Status_Date__c = aonParameters.convertTimestamp(payment.claimStatusDate).Date();
            }

            AONInformation.Claim_Status__c = payment.claimStatus;
            AONInformation.Claim_Number__c = payment.claimNumber;
            AONInformation.Claim_Disposition__c = payment.claimDisposition;

            update AONInformation;

            if (payment.claimStatus == 'A' && paymentTransactionDate != null && paymentTransactionDate >= contractLastTransaction) {

                System.debug('payment.paymentAmount Decimal: ' + Decimal.valueOf(payment.paymentAmount));
                System.debug('AONInformation.Contract__c: ' + AONInformation.Contract__c);
                System.debug('loan__Current_Branch_s_System_Date__c: ' + [SELECT loan__Current_Branch_s_System_Date__c FROM User WHERE Name = 'HAL' LIMIT 1].loan__Current_Branch_s_System_Date__c);

                paymentTransaction.loan__Loan_Account__c = AONInformation.Contract__c;
                paymentTransaction.loan__Transaction_Amount__c = Decimal.valueOf(payment.paymentAmount);
                paymentTransaction.loan__Transaction_Date__c = paymentTransactionDate;
                paymentTransaction.loan__Payment_Mode__c = [SELECT Id FROM loan__Payment_Mode__c WHERE Name = 'ACH' LIMIT 1].Id;
                paymentTransaction.loan__Sent_to_ACH__c = true;


                paymentTransaction.loan__Receipt_Date__c = [SELECT loan__Current_Branch_s_System_Date__c FROM User WHERE Name = 'HAL' LIMIT 1].loan__Current_Branch_s_System_Date__c;
                paymentTransaction.loan__Skip_Validation__c = true;

                System.debug('paymentTransaction Object for insert: ' + paymentTransaction);

                insert paymentTransaction;

                loan_AON_Payments__c  AONPayment = new loan_AON_Payments__c();
                AONPayment.loan_AON_Information__c =  AONInformation.Id;
                AONPayment.Payment_Date__c = paymentTransactionDate;
                AONPayment.Payment_Type__c = payment.paymentType;
                AONPayment.Claim_Status__c = payment.claimStatus;
                AONPayment.Claim_Status_Date__c  = aonParameters.convertTimestamp(payment.claimStatusDate).Date();
                AONPayment.Claim_Disposition__c = payment.claimDisposition;

                insert AONPayment;
            }

            return true;
        }
        return false;
    }

    public AONObjectsParameters.SalesforceResponse aimCancellation(aonObjectsParameters.AONCancellationInputData aimCancellation) {

        AONObjectsParameters.SalesforceResponse salesforceResponse = new AONObjectsParameters.SalesforceResponse();

        for (loan_AON_Information__c aon : [SELECT   id,
                                            IsAIMCancellation__c,
                                            Contract__r.loan__Loan_Status__c,
                                            Contract__r.AON_Enrollment__c,
                                            IsCancellation__c,
                                            Enrollment_Date__c

                                            FROM loan_AON_Information__c
                                            WHERE Contract__r.Name = : aimCancellation.accountNumber
                                                    LIMIT 1]) {

            try {
                if (aon.Enrollment_Date__c == null) {
                    salesforceResponse.statusCode = '400';
                    salesforceResponse.errorMessage = 'Account not Enrolled';
                    salesforceResponse.errorDetails = 'Account not Enrolled, Account Number: ' + aimCancellation.accountNumber;
                    return salesforceResponse;
                }

                if (aon.IsCancellation__c || aon.IsAIMCancellation__c) {
                    salesforceResponse.statusCode = '400';
                    salesforceResponse.errorMessage = 'Account Already Canceled';
                    salesforceResponse.errorDetails = (aon.IsCancellation__c && !aon.IsAIMCancellation__c) ? 'Account Already Canceled from LendingPoint' : (aon.IsAIMCancellation__c) ? 'Account Already Canceled from AON' : '';
                    return salesforceResponse;
                }

                insert new loan_AON_Cancellations__c(Cancel_Reason_Code__c = aimCancellation.cancelReasonCode,
                                                     Effective_Date__c = aonParameters.convertTimestamp(aimCancellation.effectiveDate).Date(),
                                                     loan_AON_Information__c = aon.id,
                                                     Refund_Amount__c = (String.isNotEmpty(aimCancellation.refundAmount)) ? Decimal.valueOf(aimCancellation.refundAmount) : 0.00);
                aon.IsAIMCancellation__c = true;
                //aon.IsCancellation__c = true;
                //aon.Contract__r.AON_Enrollment__c = false;

                update aon;

                salesforceResponse.statusCode = '201';
                return salesforceResponse;

            } catch (DmlException e) {
                System.debug('An unexpected error has occurred: ' + e.getMessage());
                salesforceResponse.statusCode = '500';
                salesforceResponse.errorMessage = 'An unexpected error has occurred';
                salesforceResponse.errorDetails = e.getMessage();
                return salesforceResponse;
            }
        }

        salesforceResponse.statusCode = '400';
        salesforceResponse.errorMessage = 'Account not Found';
        salesforceResponse.errorDetails = 'Account not Found, Account Number: ' + aimCancellation.accountNumber;
        return salesforceResponse;
    }

    public static String proxyResponseProcess(HttpResponse response) {

        String aonJSONResponse;

        if (response != null) {
            AONObjectsParameters.AonResponse aonResponse =  new AONObjectsParameters.AonResponse(response.getBody());
            if (aonResponse != null) {
                aonResponse.statusCode = response.getStatusCode();
                aonJSONResponse = JSON.serializePretty(aonResponse, true);
            }
        }

        return aonJSONResponse;
    }

    public static void setContactLoggging(String callType, String request, String jsonResponse, String contractId) {

        if (jsonResponse != null && String.isNotEmpty(contractId)) {

            for (loan__Loan_Account__c contract : [SELECT Id, loan__Contact__c  FROM loan__Loan_Account__c WHERE id = : contractId LIMIT 1]) {

                Logging__c log = new Logging__c(Webservice__c = callType,
                                                API_Request__c = request,
                                                API_Response__c = (jsonResponse != null) ? jsonResponse : 'No Response, Proxy Error',
                                                Contact__c = contract.loan__Contact__c,
                                                CL_Contract__c = contract.id);

                insert log;
            }
        }
    }
}