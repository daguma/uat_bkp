global class BatchMultiloanFail implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable   {

	global final String query;
	global final Integer queryLimit;

	global BatchMultiloanFail(Integer queryLimit){
		this.queryLimit = queryLimit;
		this.query = MultiloanFails.getIneligibilityReviewQuery(this.queryLimit);

	}

	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		MultiloanFails.updateMultiloanParamsLogs(scope);
		
	}

	global void finish(Database.BatchableContext BC) {
	}

	global void execute(SchedulableContext sc) {
		
		BatchMultiloanFail job = new BatchMultiloanFail(queryLimit);
		database.executebatch(job, 1);
		
	}

}