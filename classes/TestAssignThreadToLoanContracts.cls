@isTest
public class TestAssignThreadToLoanContracts {
    @testSetup
    static void setup() {
        Test.startTest();
        loan.TestHelper.SystemDate = Date.newInstance(2015, 03, 01);
        loan.TestHelper.createSeedDataForTesting();
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose(); 
        
        //As a result of moving validation from code to validation rule, updating org Params 
        loan.TestHelper.useCLLoanCRM();
        
        loan__Org_Parameters__c orgParam = loan.CustomSettingsUtil.getOrgParameters();
        orgParam.loan__Concurrent_BatchJobs__c = 3;
        orgParam.loan__disable_triggers__c = true;
        upsert orgParam;
        
        
        List<loan__Loan_Account__c> loanAccountList = new List<loan__Loan_Account__c>();
        //Create a dummy Loan Accounts
        for(Integer i = 0; i < 4; i++) {
            loanAccountList.add(loan.TestHelper.createLoanAccount( dummyLP, dummyClient, dummyFeeSet, dummyLoanPurpose, dummyOffice));
        }
        
        for(loan__Loan_Account__c loan : loanAccountList) {
            loan.loan__LoanCnt__c = null;
            //loan.loan__Loan_Status__c = 'Active - Good Standing';
        }
        update loanAccountList;
        Test.stopTest();
    }
    
    @isTest
    public static void createContracts() {
        loan__Org_Parameters__c orgParams = loan.CustomSettingsUtil.getOrgParameters();
        //orgParams.loan__disable_triggers__c = true;
        //update orgParams;
        List<loan__Loan_Account__c> loanAccountList = [SELECT Id,
                                  loan__LoanCnt__c
                           FROM loan__Loan_Account__c
                          ];
        for(loan__Loan_Account__c loan : loanAccountList) {
            system.assertEquals(null, loan.loan__LoanCnt__c);
        }
        Test.startTest();
        AssignThreadToLoanContracts job = new AssignThreadToLoanContracts();
        DataBase.executeBatch(job);
        Test.stopTest();
        
        
        loanAccountList = [SELECT Id,
                                  loan__LoanCnt__c
                           FROM loan__Loan_Account__c
                          ];
        Integer threadNum = 1;
        for(loan__Loan_Account__c loan : loanAccountList) {
            system.assertEquals(threadNum, loan.loan__LoanCnt__c);
            if(threadNum == 3) {
                threadNum = 1;
            }
            else {
                threadNum++;
            }
        }
        orgParams = loan.CustomSettingsUtil.getOrgParameters();
        orgParams.loan__disable_triggers__c = false;
        update orgParams;
    }
}