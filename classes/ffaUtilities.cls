/****************************************************************************************
Name            : ffaUtilities
Author          : CLD
Created Date    : 1/14/16
Description     : Contains various methods for FFA Utilities
*****************************************************************************************/
global class ffaUtilities {
    public static String myMsg;
    
    /******************************************************************************/
    /** Method to return a list of the current companies
    /******************************************************************************/
    public static List<c2g__codaCompany__c> getCurrentCompanies() {
        List<c2g__codaCompany__c> allCompanies = [SELECT Id, OwnerId, Name FROM c2g__codaCompany__c];
        Id currentUserId = UserInfo.getUserId();
        List<c2g__codaCompany__c> myCompanies = new List<c2g__codaCompany__c>();
        Set<Id> currentUserGroupIds = new Set<Id>();
        
        for(GroupMember gm :[SELECT Id, GroupId FROM GroupMember WHERE UseroRGroupId = :currentUserId]) {
            currentUserGroupIds.add(gm.GroupId);
        }

        for(c2g__codaCompany__c company : allCompanies) {
            if(currentUserGroupIds.contains(company.ownerid))
                myCompanies.add(company);
        }
        return myCompanies;
    }

    /******************************************************************************/
     /** Method to retrieve map of FFA Group ID to 
    /******************************************************************************/
    public static Map<Id, Id> getCompanyToFFAGroupIDMap() {
        List<c2g__codaCompany__c> allCompanies = [SELECT Id, OwnerId, Name FROM c2g__codaCompany__c];
        List<Group> gm = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint%']; 
        Map<Id, Id> returnMap = new Map<Id, Id>(); 

        for (Group g: gm) {
            for (c2g__codaCompany__c c: allCompanies) {
                if (c.OwnerId == g.Id)
                    returnMap.put(c.Id, g.Id); 
            }
        }

        return returnMap; 
    }

    /******************************************************************************/
     //Method for Bulk discarding a Journal Entry       
    /******************************************************************************/
    global static String bulkDiscardJournal(List<Id> journalIDs) {
        String companyName = '';
        List<c2g__codaJournalLineItem__c> lineItemDeleteList = new List<c2g__codaJournalLineItem__c>();
        List<c2g.CODAAPIJournalTypes_12_0.Journal> apiJournalUpdateList = new List<c2g.CODAAPIJournalTypes_12_0.Journal>();
        Set<String> companyNameSet = new Set<String>();
        List<c2g.CODAAPICommon.Reference> returnReferenceList = new List<c2g.CODAAPICommon.Reference>();

        List<c2g__codaJournal__c> discardJournalList = [SELECT  Id,
                                                                c2g__ownerCompany__c,
                                                                c2g__ownerCompany__r.Name,
                                                                c2g__JournalCurrency__c,
                                                                c2g__Period__c,
                                                                (SELECT Id FROM c2g__JournalLineItems__r)
                                                        FROM c2g__codaJournal__c
                                                        WHERE id in :journalIDs
                                                        AND c2g__JournalStatus__c = 'In Progress'];

        if(discardJournalList.size() == 0)
            return 'Attention: No Journals were found for discarding, please ensure you have selected at least 1 In Progress Journal Entry.';

        for(c2g__codaJournal__c journal : discardJournalList) {
            companyNameSet.add(journal.c2g__ownerCompany__r.Name);
            companyName = journal.c2g__ownerCompany__r.Name;
        }

        if(companyNameSet.size()>1)
            return 'Attention: All journals select must belong to 1 company.';

        for(c2g__codaJournal__c journal : discardJournalList) {
            lineItemDeleteList.addAll(journal.c2g__JournalLineItems__r);
        }

        delete lineItemDeleteList;

        for(c2g__codaJournal__c journal : discardJournalList) {
            c2g.CODAAPIJournalTypes_12_0.Journal apiJournal = new c2g.CODAAPIJournalTypes_12_0.Journal();
            c2g.CODAAPICommon.Reference currencyReference = new c2g.CODAAPICommon.Reference();
            currencyReference.id = journal.c2g__JournalCurrency__c;
            c2g.CODAAPICommon.Reference periodReference = new c2g.CODAAPICommon.Reference();
            periodReference.id = journal.c2g__Period__c;
            apiJournal.id = journal.id;
            apiJournal.JournalStatus = c2g.CODAAPIJournalTypes_12_0.enumJournalStatus.Discarded;
            apiJournal.DiscardReason = 'Journal created in error- Mass discarded ' + UserInfo.getName() + ' on ' + Date.today().format();
            List<c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem> apiLineItemList = new List<c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItem>();
            c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItems apiLineItems = new c2g.CODAAPIJournalLineItemTypes_12_0.JournalLineItems();
            apiLineItems.LineItemList = apiLineItemList;
            apiJournal.LineItems = apiLineItems;
            apiJournal.TypeRef = c2g.CODAAPIJournalTypes_12_0.enumType.ManualJournal;

            apiJournalUpdateList.add(apiJournal);
        }
        
        c2g.CODAAPICommon_10_0.Context companyContext = new c2g.CODAAPICommon_10_0.Context();
        companyContext.CompanyName = companyName;

        try {
            returnReferenceList = c2g.CODAAPIJournal_12_0.BulkUpdateJournal(companyContext, apiJournalUpdateList);
        }
        catch(Exception e) {
            return 'The following Error occured while discarding: ' + e.getMessage();
        }

        return 'Success! - '+apiJournalUpdateList.size() + ' Journals have been discarded';
    }

    /******************************************************************************/
    /** webservice method that is invoked from JNL List View        **/
    /******************************************************************************/
    Webservice static string bulkDiscardJournal_WS(List<Id> jnlIDs) {
        String message = '';
        try {
           message = ffaUtilities.bulkDiscardJournal(jnlIDs);
        }
        catch(System.CalloutException e) {
            message = e.getMessage();
        }
        return message;
    }

    /******************************************************************************/
    /**  Helper method used to generate Loan_Capitalization___c records
         based on triggering Loan Disbursal Transaction events       **/
    /******************************************************************************/
    public static void createLoanCapitalizationRecords(Set<Id> disbursalTxIds) {
        List<loan__Loan_Disbursal_Transaction__c> dTx = [SELECT Revenue_Start_Date__c, 
                                                                Revenue_End_Date__c, 
                                                                Revenue_To_Amortize_Capitalize__c, 
                                                                ffrrtemplate__c, 
                                                                Amount_to_Capitalize__c
                                                        FROM    loan__Loan_Disbursal_Transaction__c
                                                        WHERE   Id in :disbursalTxIds]; 

        List<Loan_Capitalization__c> newShadowObjs = new List<Loan_Capitalization__c>();  
        for (loan__Loan_Disbursal_Transaction__c lDtx: dTx) {
            Loan_Capitalization__c newObj = new Loan_Capitalization__c(
                    Amount_to_Capitalize__c = lDtx.Amount_to_Capitalize__c, 
                    Loan_Disbursal_Transaction__c = lDtx.Id, 
                    Revenue_Start_Date__c = lDtx.Revenue_Start_Date__c, 
                    Revenue_End_Date__c = lDtx.Revenue_End_Date__c 
            ); 
            newShadowObjs.add(newObj); 
        }

        insert newShadowObjs; 

        createLoanCapitalizationJournals(newShadowObjs);
    }

    public static void createLoanCapitalizationJournals (List<Loan_Capitalization__c> listCap) {
        List<c2g__codaJournal__c> journalsToInsert = new List<c2g__codaJournal__c>();
        for(Loan_Capitalization__c t: listCap) {
            c2g__codaJournal__c jnl = new c2g__codaJournal__c(
                c2g__OwnerCompany__c = 'aARU0000000CaRR',
                c2g__journalDate__c = t.Revenue_Start_Date__c,
                c2g__DeriveCurrency__c = TRUE,
                Loan_Account__c = t.Loan_Disbursal_Transaction__r.loan__Loan_Account__c, 
                Loan_Capitalization__c = t.Id, 
                c2g__DerivePeriod__c = TRUE,
                c2g__JournalDescription__c = 'Establishing Deferred Cost of Acquisition on Balance Sheet Journal', 
                c2g__Type__c = 'Manual Journal'); 
            
            journalsToInsert.add(jnl);
        }
        insert journalsToInsert;
    
        Map<String, String> transactionToJournalMap = new Map<String, String>();
        for(integer i = 0; i< listCap.size(); i++) {
            transactionToJournalMap.put(listCap[i].Id, journalsToInsert[i].Id);
        }

        List<c2g__codaJournalLineItem__c> lineItemsToInsert = new List<c2g__codaJournalLineItem__c>();
        loan_Capitalization_Settings__c capCreditSettings = loan_Capitalization_Settings__c.getOrgDefaults();
        for(Loan_Capitalization__c t : listCap) {       
            c2g__codaJournalLineItem__c line1 = new c2g__codaJournalLineItem__c();
            line1.c2g__Journal__c = transactionToJournalMap.Get(t.Id);
            line1.c2g__GeneralLedgerAccount__c = capCreditSettings.Capitalization_Journal_Credit__c;
            line1.c2g__Value__c = ((t.Amount_to_Capitalize__c)*(-1)).setScale(2);
            line1.c2g__DebitCredit__c = 'Credit';
            line1.c2g__LineType__c = 'General Ledger Account';
            lineItemsToInsert.add(line1);

            c2g__codaJournalLineItem__c line2 = new c2g__codaJournalLineItem__c();
            line2.c2g__Journal__c = transactionToJournalMap.Get(t.Id);
            line2.c2g__GeneralLedgerAccount__c = capCreditSettings.Capitalization_Journal_Debit__c; 
            line2.c2g__Value__c = t.Amount_to_Capitalize__c.setScale(2);
            line2.c2g__DebitCredit__c = 'Debit';
            line2.c2g__LineType__c = 'General Ledger Account';
            lineItemsToInsert.add(line2);
        }
        insert lineItemsToInsert;
    }
}