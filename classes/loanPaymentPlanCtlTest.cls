@isTest
private class loanPaymentPlanCtlTest {

    @isTest static void inserts_payment_plan() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        loan__Bank_Account__c bankAccount = new loan__Bank_Account__c();
        bankAccount.loan__Account_Type__c = 'Checking';
        bankAccount.loan__Bank_Account_Number__c = '1234567890';
        bankAccount.loan__Bank_Name__c = 'Test Bank';
        insert bankAccount;

        Integer planBefore = [SELECT COUNT() FROM loan_Plan_Payment__c];

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        paymentPlan.theP.ACH_Debit_Date__c = Date.today().addDays(10);
        paymentPlan.theP.Payment_Amount__c = 200;
        paymentPlan.theP.Routing_Number__c = '123456789';
        paymentPlan.theP.Account_Number__c = '1234567890';
        paymentPlan.theP.Comment__c = 'Test';
        paymentPlan.theP.Bank_Account__c = bankAccount.Id;
        paymentPlan.theP.Payment_Plan_Reason__c = 'Divorce';

        paymentPlan.insertPayment();

        System.assert([SELECT COUNT() FROM loan_Plan_Payment__c] > planBefore, 'Insert Payment Plan');
    }

    @isTest static void deletes_payment_plan() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        loan__Bank_Account__c bankAccount = new loan__Bank_Account__c();
        bankAccount.loan__Account_Type__c = 'Checking';
        bankAccount.loan__Bank_Account_Number__c = '1234567890';
        bankAccount.loan__Bank_Name__c = 'Test Bank';
        insert bankAccount;

        loan_Plan_Payment__c theP = new loan_Plan_Payment__c();
        theP.Loan_Account__c = contract.Id;
        theP.ACH_Debit_Date__c = Date.today().addDays(10);
        theP.Payment_Amount__c = 200;
        theP.Routing_Number__c = '123456789';
        theP.Account_Number__c = '1234567890';
        theP.Comment__c = 'Test';
        theP.Bank_Account__c = bankAccount.Id;
        theP.Payment_Plan_Reason__c = 'Divorce';
        insert theP;

        Integer planBefore = [SELECT COUNT() FROM loan_Plan_Payment__c];

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        System.currentPageReference().getParameters().put('paymentid', theP.Id);

        paymentPlan.getselectedPayments();
        paymentPlan.deletePayment();

        System.assert(planBefore > [SELECT COUNT() FROM loan_Plan_Payment__c], 'Deletes Payment Plan');
    }

    @isTest static void validates_ach_debit_date_on_insert() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        paymentPlan.theP.ACH_Debit_Date__c = Date.today().addDays(-1);
        paymentPlan.theP.Payment_Amount__c = 200;
        paymentPlan.theP.Routing_Number__c = '123456789';

        paymentPlan.insertPayment();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'The first installment/payment of the plan must be greater than 3 calendar days from the current date (today).');
    }

    @isTest static void validates_payment_amount_on_insert() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        paymentPlan.theP.Routing_Number__c = '123456789';

        paymentPlan.insertPayment();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Please enter Payment Amount and ACH Debit Date before inserting payment.');
    }

    @isTest static void validates_routing_number_on_insert() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        paymentPlan.theP.ACH_Debit_Date__c = Date.today().addDays(4);
        paymentPlan.theP.Payment_Amount__c = 200;
        paymentPlan.theP.Routing_Number__c = '1234567';

        paymentPlan.insertPayment();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Routing Number must be exactly 9 digits.');
    }

    @isTest static void changes_status_to_pending() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        loan__Bank_Account__c bankAccount = new loan__Bank_Account__c();
        bankAccount.loan__Account_Type__c = 'Checking';
        bankAccount.loan__Bank_Account_Number__c = '1234567890';
        bankAccount.loan__Bank_Name__c = 'Test Bank';
        insert bankAccount;

        loan_Plan_Payment__c theP = new loan_Plan_Payment__c();
        theP.Loan_Account__c = contract.Id;
        theP.ACH_Debit_Date__c = Date.today().addDays(10);
        theP.Payment_Amount__c = 200;
        theP.Routing_Number__c = '123456789';
        theP.Account_Number__c = '1234567890';
        theP.Comment__c = 'Test';
        theP.Bank_Account__c = bankAccount.Id;
        theP.Payment_Plan_Reason__c = 'Divorce';
        insert theP;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        List<loanPaymentPlanCtl.selectedPayment> selectedList = paymentPlan.getselectedPayments();
        selectedList.get(0).selected = true;

        paymentPlan.selectedPaymentList = selectedList;

        paymentPlan.pendingSelectedPayment();

        System.assertEquals('Pending', [SELECT Status__c FROM loan_Plan_Payment__c WHERE Id = : theP.Id].Status__c);
    }

    @isTest static void changes_status_to_pending_select_a_record() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        loan__Bank_Account__c bankAccount = new loan__Bank_Account__c();
        bankAccount.loan__Account_Type__c = 'Checking';
        bankAccount.loan__Bank_Account_Number__c = '1234567890';
        bankAccount.loan__Bank_Name__c = 'Test Bank';
        insert bankAccount;

        loan_Plan_Payment__c theP = new loan_Plan_Payment__c();
        theP.Loan_Account__c = contract.Id;
        theP.ACH_Debit_Date__c = Date.today().addDays(10);
        theP.Payment_Amount__c = 200;
        theP.Routing_Number__c = '123456789';
        theP.Account_Number__c = '1234567890';
        theP.Comment__c = 'Test';
        theP.Bank_Account__c = bankAccount.Id;
        theP.Payment_Plan_Reason__c = 'Divorce';
        insert theP;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        paymentPlan.pendingSelectedPayment();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Please select at least one record.');
    }

    @isTest static void changes_status_to_cancelled() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        loan__Bank_Account__c bankAccount = new loan__Bank_Account__c();
        bankAccount.loan__Account_Type__c = 'Checking';
        bankAccount.loan__Bank_Account_Number__c = '1234567890';
        bankAccount.loan__Bank_Name__c = 'Test Bank';
        insert bankAccount;

        loan_Plan_Payment__c theP = new loan_Plan_Payment__c();
        theP.Loan_Account__c = contract.Id;
        theP.ACH_Debit_Date__c = Date.today().addDays(10);
        theP.Payment_Amount__c = 200;
        theP.Routing_Number__c = '123456789';
        theP.Account_Number__c = '1234567890';
        theP.Comment__c = 'Test';
        theP.Bank_Account__c = bankAccount.Id;
        theP.Payment_Plan_Reason__c = 'Divorce';
        insert theP;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        List<loanPaymentPlanCtl.selectedPayment> selectedList = paymentPlan.getselectedPayments();
        selectedList.get(0).selected = true;

        paymentPlan.selectedPaymentList = selectedList;

        paymentPlan.cancelSelectedPayment();

        System.assertEquals('Cancelled', [SELECT Status__c FROM loan_Plan_Payment__c WHERE Id = : theP.Id].Status__c);
    }

    @isTest static void changes_status_to_cancelled_select_a_record() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Good Standing';
        update contract;

        loan__loan_Disbursal_Transaction__c testTransaction = new loan__loan_Disbursal_Transaction__c();
        testTransaction.loan__Loan_Account__c = contract.Id;
        testTransaction.loan__Disbursal_Date__c = Date.today();
        testTransaction.loan__Disbursed_Amt__c = 100;
        insert testTransaction;

        loan__Bank_Account__c bankAccount = new loan__Bank_Account__c();
        bankAccount.loan__Account_Type__c = 'Checking';
        bankAccount.loan__Bank_Account_Number__c = '1234567890';
        bankAccount.loan__Bank_Name__c = 'Test Bank';
        insert bankAccount;

        loan_Plan_Payment__c theP = new loan_Plan_Payment__c();
        theP.Loan_Account__c = contract.Id;
        theP.ACH_Debit_Date__c = Date.today().addDays(10);
        theP.Payment_Amount__c = 200;
        theP.Routing_Number__c = '123456789';
        theP.Account_Number__c = '1234567890';
        theP.Comment__c = 'Test';
        theP.Bank_Account__c = bankAccount.Id;
        theP.Payment_Plan_Reason__c = 'Divorce';
        insert theP;

        ApexPages.StandardController controller = new ApexPages.StandardController(contract);
        loanPaymentPlanCtl paymentPlan = new loanPaymentPlanCtl(controller);

        paymentPlan.cancelSelectedPayment();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Please select at least one record.');
    }

}