@isTest
public class CollectionsCasesLBAlgorithmTest {

    @isTest static void validate_process() {
        Test.startTest();
        Map<Id,Integer> testMap = new Map<Id, Integer>();
        Integer testInteger = 0;
        for(User testUser : [SELECT Id FROM User LIMIT 10]){
            testMap.put(testUser.Id,testInteger++);
        }
        CollectionsCasesAssignmentLBEntity testEntity = CollectionsCasesAssignmentLBAlgorithm.process(30,testMap);
        System.assertNotEquals(null,testEntity);
        System.assertNotEquals(null,testEntity.getNextAssigneeId());

        CollectionsCasesAssignmentLBEntity testEntity2 = CollectionsCasesAssignmentLBAlgorithm.process(0,new Map<Id,Integer>());
        System.assertNotEquals(null,testEntity2);
        System.assertEquals(null,testEntity2.getNextAssigneeId());

        CollectionsCasesAssignmentLBEntity testEntity3 = CollectionsCasesAssignmentLBAlgorithm.process(null,null);
        System.assertEquals(null,testEntity3);
        Test.stopTest();
    }

    @isTest static void validate_SpecificScenario_AssignedLess() {
        Test.startTest();
        List<User> testTeam = [SELECT Id FROM User LIMIT 5];
        Map<Id,Integer> testMap = new Map<Id, Integer>();
        testMap.put(testTeam.get(0).Id,166);
        testMap.put(testTeam.get(1).Id,159);
        testMap.put(testTeam.get(2).Id,166);
        testMap.put(testTeam.get(3).Id,169);
        testMap.put(testTeam.get(4).Id,188);
        CollectionsCasesAssignmentLBEntity testEntity = CollectionsCasesAssignmentLBAlgorithm.process(50, testMap);
        System.assertNotEquals(null,testEntity);
        System.assertEquals(11 ,testEntity.loadBalancedMap.get(testTeam.get(0).Id));
        System.assertEquals(14 ,testEntity.loadBalancedMap.get(testTeam.get(1).Id));
        System.assertEquals(11 ,testEntity.loadBalancedMap.get(testTeam.get(2).Id));
        System.assertEquals(10 ,testEntity.loadBalancedMap.get(testTeam.get(3).Id));
        System.assertEquals( 4 ,testEntity.loadBalancedMap.get(testTeam.get(4).Id));

        Test.stopTest();
    }

    @isTest static void validate_SpecificScenario_AssignedMore() {
        Test.startTest();
        List<User> testTeam = [SELECT Id FROM User LIMIT 5];
        Map<Id,Integer> testMap = new Map<Id, Integer>();
        testMap.put(testTeam.get(0).Id,166);
        testMap.put(testTeam.get(1).Id,159);
        testMap.put(testTeam.get(2).Id,166);
        testMap.put(testTeam.get(3).Id,169);
        testMap.put(testTeam.get(4).Id,188);
        CollectionsCasesAssignmentLBEntity testEntity = CollectionsCasesAssignmentLBAlgorithm.process(3, testMap);
        System.assertNotEquals(null,testEntity);
        System.assertEquals(0,testEntity.loadBalancedMap.get(testTeam.get(0).Id));
        System.assertEquals(1,testEntity.loadBalancedMap.get(testTeam.get(1).Id));
        System.assertEquals(1,testEntity.loadBalancedMap.get(testTeam.get(2).Id));
        System.assertEquals(1,testEntity.loadBalancedMap.get(testTeam.get(3).Id));
        System.assertEquals(0,testEntity.loadBalancedMap.get(testTeam.get(4).Id));

        Test.stopTest();
    }
    
    @isTest static void validate_SpecificScenario_AssignOne() {
        Test.startTest();
        List<User> testTeam = [SELECT Id FROM User LIMIT 1];
        Map<Id,Integer> testMap = new Map<Id, Integer>();
        testMap.put(testTeam.get(0).Id,1);
        CollectionsCasesAssignmentLBEntity testEntity = CollectionsCasesAssignmentLBAlgorithm.process(1, testMap);
        System.assertNotEquals(null,testEntity);
        System.assertEquals(1,testEntity.loadBalancedMap.get(testTeam.get(0).Id));
        Test.stopTest();
    }
    
    @isTest static void validate_SpecificScenario_AssignTen() {
        Test.startTest();
        List<User> testTeam = [SELECT Id FROM User LIMIT 1];
        Map<Id,Integer> testMap = new Map<Id, Integer>();
        testMap.put(testTeam.get(0).Id,10);
        CollectionsCasesAssignmentLBEntity testEntity = CollectionsCasesAssignmentLBAlgorithm.process(10, testMap);
        System.assertNotEquals(null,testEntity);
        System.assertEquals(10,testEntity.loadBalancedMap.get(testTeam.get(0).Id));
        Test.stopTest();
    }
}