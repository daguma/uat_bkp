public with sharing class DecisioningEntity {

    @AuraEnabled public Opportunity opportunity { get; set; }
    @AuraEnabled public Boolean isPreBRMS { get; set; }
    @AuraEnabled public CustomerDetailsCtrl.CustomerDetail customerDetail { get; set; }
    @AuraEnabled public List<CreditReportCtrl.CreditReportItem> creditReportItems { get; set; }
    @AuraEnabled public List<ProxyApiCreditReportEntityLite> crList { get; set; }
    @AuraEnabled public List<ApplicationRulesCtrl.ApplicationRule> applicationRulesItems { get; set; }
    @AuraEnabled public String crName { get; set; }
    @AuraEnabled public String crAutomatedDecision { get; set; }
    @AuraEnabled public String crApprovedByName { get; set; }
    @AuraEnabled public List<DecisioningAttributeEntity> decisioningAttributeList { get; set; }
    @AuraEnabled public Boolean hasWarning { get; set; }
    @AuraEnabled public List<String> warningsList { get; set; }
    @AuraEnabled public Boolean hasError { get; set; }
    @AuraEnabled public List<String> errorsList { get; set; }
    @AuraEnabled public String infoMessage { get; set; }
    @AuraEnabled public Boolean hasInfo { get; set; }
    @AuraEnabled public String crId { get; set; }
    @AuraEnabled public Boolean isOverrideButtonDisabled { get; set; }
    @AuraEnabled public Boolean isOverrideProfile { get; set; }
    @AuraEnabled public Boolean isSoftPullButtonDisabled { get; set; }
    @AuraEnabled public Boolean isHardPullButtonDisabled { get; set; }
    @AuraEnabled public Boolean isVerifyEmailRequired { get; set; }
    @AuraEnabled public Boolean isMeridianReport { get; set; }
    @AuraEnabled public Boolean hasAllCRInformation { get; set; }
    @AuraEnabled public List<DisplayGDSResponseWrapper> finalDisplayList { get; set; }
    @AuraEnabled public Boolean isHardPullCreditReportPresent { get; set; }
    @AuraEnabled public Boolean hasRulesInformation { get; set; }
    @AuraEnabled public Boolean isProfileAllowed { get; set; }
    @AuraEnabled public String crBrmsAutomatedDecision { get; set; }
    @AuraEnabled public String crBrmsApprovedByName { get; set; }
    @AuraEnabled public List<ClarityClearRecentHistory> clearRecentHistoryList { get; set; } //CNR-4
    @AuraEnabled public String errorDetails { get; set; } //CNR-4
     @AuraEnabled public Boolean isDisableFundingApproval { get; set; }

    public BRMS_Configurable_Settings__c brmsSettings { get; set; }
    public Override_Profiles__c cuSettings { get; set; }

    public DecisioningEntity() {
        initialize();
    }

    public DecisioningEntity(DecisioningCtrl dc) {
        initialize();
        if (dc != null) {
            if (dc.genesisApp != null) {
                this.opportunity = dc.genesisApp;
                this.isPreBRMS = dc.displayOldView;
                this.customerDetail = CustomerDetailsCtrl.getCustomerDetails(null, dc.genesisApp);
                this.customerDetail.brmsVersionId = dc.brmsRuleVersion;
                if (dc.cr != null && dc.cr.brms != null)
                    this.customerDetail.finalGrade = dc.cr.brms.finalGrade;
                this.customerDetail.scoringModelA = dc.scoringModelA;
                this.customerDetail.scoringModelB = dc.scoringModelB;
                this.customerDetail.applicationRisk = String.valueOf(dc.applicationRisk);
                isHardPullButtonDisabled = dc.isHardPullButtonDisabled;
                isMeridianReport = dc.isMeridianReport;
                isVerifyEmailRequired = dc.isVerifyEmailRequired;
                isSoftPullButtonDisabled = dc.isSoftPullButtonDisabled;
                this.crId = String.valueOf(dc.crid);
                this.crName = dc.crName;
                this.crAutomatedDecision = dc.crAutomatedDecision;
                this.crApprovedByName = dc.crApprovedByName;
                this.decisioningAttributeList = dc.decisioningAttributeList;
                this.hasAllCRInformation = dc.hasAllCRInformation;
                this.crList = dc.crList;
                this.warningsList = dc.warningsList;
                this.hasWarning = dc.hasWarning;
                this.errorsList = dc.errorsList;
                for (String s : errorsList) {
                    if (s.contains('javascript:void(0);')) {
                        this.errorDetails = s.substringBetween('alert(\"', '\");');
                    }
                }
                this.hasError = dc.hasError;
//                if(dc.crhard != null) {
//                    if (!String.isEmpty(dc.crhard.errorDetails)) {
//                        this.errorDetails = dc.crhard.errorDetails;
//                    }
//                }else if(dc.cr != null) {
//                    if (String.isEmpty(dc.cr.errorDetails)){
//                        this.errorDetails = dc.cr.errorDetails;
//                    }
//                }

                this.hasInfo = dc.hasInfo;
                this.infoMessage = dc.infoMessage;
                this.isHardPullCreditReportPresent = dc.isHardPullCreditReportPresent;
                this.finalDisplayList = dc.finalDisplayList;
                this.hasRulesInformation = dc.hasRulesInformation;
                this.crBrmsAutomatedDecision = dc.crBrmsAutomatedDecision;
                this.crBrmsApprovedByName = dc.crBrmsApprovedByName;
                this.clearRecentHistoryList = dc.clearRecentHistoryList;
                this.isProfileAllowed = dc.isProfileAllowed;

            } else {
                this.errorsList.add('DecisionCtrl --> Opportunity is null.');
            }
        } else {
            this.errorsList.add('DecisionCtrl --> Credit Report is null.');
        }
    }

    private void initialize() {
        this.brmsSettings = BRMS_Configurable_Settings__c.getValues('settings');
        this.cuSettings = Override_Profiles__c.getOrgDefaults();
        this.isOverrideProfile = (cuSettings.Soft_Hard_Pull_Buttons__c != null &&
                cuSettings.Soft_Hard_Pull_Buttons__c.contains(UserInfo.getProfileId()));
        this.isOverrideButtonDisabled = !(cuSettings.Override_Disqualifiers__c != null &&
                cuSettings.Override_Disqualifiers__c.contains(UserInfo.getProfileId()));
        this.isSoftPullButtonDisabled = !isOverrideProfile;
        this.isHardPullButtonDisabled = !isOverrideProfile;
        this.isVerifyEmailRequired = true;
        this.isMeridianReport = false;
        this.hasWarning = false;
        this.errorsList = new List<String>();
        this.warningsList = new List<String>();
        this.hasError = false;
        this.hasInfo = false;
        this.isDisableFundingApproval = isDisableFundingApproval();
    }

    public void setCustomerDetail(CustomerDetailsCtrl.CustomerDetail cd) {
        this.customerDetail = cd;
    }

    public void setCreditReportItems(List<CreditReportCtrl.CreditReportItem> cri_list) {
        this.creditReportItems = cri_list;
    }

    public void setApplicationRules(List<ApplicationRulesCtrl.ApplicationRule> ar_list) {
        this.applicationRulesItems = ar_list;
    }

    public boolean isDisableFundingApproval(){
        string profileName = [SELECT id, profile.name FROM User WHERE id = : UserInfo.getUserId()].profile.name;
        for(Enabled_Profiles_for_Funding_Approval__c profileId : [SELECT Id, Profile_Funding_Approval__c, name FROM Enabled_Profiles_for_Funding_Approval__c]){
            if(profileId.Name == profileName ) return false;
        }
        return true;
  
    }
/*
    private Boolean checkForPreBRMS() {

        if (brmsSettings.Do_Not_Call__c == true) {
            return true;
        } else if (this.opportunity != null && this.opportunity.CreatedDate < brmsSettings.Milestone_day__c) {
            return true;
        } else return false;
    }
    */
    public void setCreditReportDetails(ProxyApiCreditReportEntity pacre) {
        if (pacre != null) {
            this.crId = String.valueOf(pacre.id);
            this.crName = pacre.name;
            this.crAutomatedDecision = pacre.automatedDecision;
            this.crApprovedByName = pacre.attributesApprovedByName;
        }
    }
/*
    public void clearErrorMessage() {
        this.errorsList.clear();
    }

    public void setErrorMessage(String errorMessage) {
        this.errorsList.add(errorMessage);
        this.hasError = true;
    }

    public class DecisioningEntityAttribute {

        @AuraEnabled public string displayName;
        @AuraEnabled public string value;
        @AuraEnabled public Boolean isSoftApproved;
        @AuraEnabled public Boolean isHardApproved;
        @AuraEnabled public string approvedByDisplayText;

        public DecisioningEntityAttribute(String displayName
                , String value
                , Boolean isSoftApproved
                , Boolean isHardApproved
                , String approvedByDisplayText) {

            this.displayName = displayName;
            this.value = value;
            this.isSoftApproved = isSoftApproved;
            this.isHardApproved = isHardApproved;
            this.approvedByDisplayText = approvedByDisplayText;

        }

    }
*/
}