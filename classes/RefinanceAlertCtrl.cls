public with sharing class RefinanceAlertCtrl {

    @AuraEnabled
    public static RefinanceAlertEntity getRefiAppByContact(Id contactId) {

        RefinanceAlertEntity rae = new RefinanceAlertEntity();

        try {
            rae.refiAppsCSV = getOppsAsString(getOppsList(contactId));
        } catch (Exception e) {
            System.debug('RefinanceAlertCtrl - getRefiAppByContact - StackTrace: ' + e.getStackTraceString());
            rae.errorMessage = e.getStackTraceString();
        }
        return rae;
    }

    private static String getOppsAsString(List<Opportunity> oppsList) {
        String refiAppString = '';
        for (Opportunity opp : oppsList) {
            if (String.isEmpty(refiAppString)) {
                refiAppString = opp.name;
            } else {
                refiAppString += ', ' + opp.name;
            }
        }
        return refiAppString;
    }

    private static List<Opportunity> getOppsList(Id contactId) {
        List<Opportunity> oppsList = [
                SELECT id, name
                FROM Opportunity
                WHERE contact__c = :contactId
                and type = 'Refinance'
                and status__c not in ('Declined (or Unqualified)', 'Aged / Cancellation', 'Refinance Unqualified')
        ] ;
        return oppsList != null ? oppsList : new List<Opportunity>();
    }

    public class RefinanceAlertEntity {

        @AuraEnabled public Boolean hasError { set; get; }
        @AuraEnabled public String errorMessage {
            set {
                errorMessage = value;
                hasError = !String.isEmpty(errorMessage);
            }
            get;
        }
        @AuraEnabled public String refiAppsCSV { set; get; }

        public RefinanceAlertEntity() {
            this.errorMessage = '';
            this.refiAppsCSV = '';
        }

    }
}