@isTest public class ClearAddressTest {
    
    @isTest static void validates(){
        ClearAddress ca = new ClearAddress('street','city','state','10000');
        System.assertEquals('city',ca.mailingCity);
        System.assertEquals('street',ca.mailingStreet);
        System.assertEquals('10000',ca.mailingPostalCode);
        System.assertEquals('state',ca.mailingState);
        
        Boolean isApproved = ca.isApproved('street', 'city', 'state', '10000');
        
        System.assertEquals(true, isApproved);
        
        isApproved = ca.isApproved('street', 'city', 'GA', '10000');
        
        System.assertEquals(false, isApproved);
    }
    
}