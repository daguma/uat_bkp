public class process_ContactUpdCls{
     /* INC- 546 */
    @InvocableMethod
    public static void updateApplication(List<Contact> conList){
        Set<Id> conSet = new Set<Id>();
        Set<String> PartnerSet = new Set<String>();
        Map<String,Boolean> partnerMap = new Map<String,Boolean>();
        Map<String,Id> partnerActMap = new Map<String,Id>();
        Map<Id,genesis__Applications__c> appsToUpdateMap = new Map<Id,genesis__Applications__c>();
        Map<Id,String> conActMap = new Map<Id,String>();
        /*
        Set<ID> contactSet = new Set<ID>();
        
        for(Id contId : contactIds){
            contactSet.add(contId);  
        }
       
        List<Contact> conList = [Select id,Lead_Sub_Source__c, Old_Lead_Sub_Source__c
                                 FROM Contact 
                                 WHERE id IN: contactSet];
       */
        system.debug('conList =====' +conList);
        try{
        for(Contact c : conList){
            PartnerSet.add(c.Lead_Sub_Source__c);
            If(String.isNotBlank(c.Old_Lead_Sub_Source__c))
            PartnerSet.add(c.Old_Lead_Sub_Source__c);
        }
      
        List<Account> PartnerAccts = [Select Id, Partner_Sub_Source__c, Contact_Restricted_Partner__c 
                                      FROM Account
                                      WHERE Partner_Sub_Source__c IN: PartnerSet AND Type = 'Partner'];
        for(Account act : PartnerAccts){
            partnerMap.put(act.Partner_Sub_Source__c,act.Contact_Restricted_Partner__c);
            partnerActMap.put(act.Partner_Sub_Source__c,act.Id);
        }
      
        system.debug('partnerMap =====' +partnerMap);
      
        for(Contact con : conList){
            If(partnerMap.containsKey(con.Lead_Sub_Source__c) && !partnerMap.get(con.Lead_Sub_Source__c) && partnerMap.containsKey(con.Old_Lead_Sub_Source__c) && partnerMap.get(con.Old_Lead_Sub_Source__c)  ){
                system.debug('Testing Inn =====');
                conSet.add(con.Id);
                conActMap.put(con.Id,con.Lead_Sub_Source__c);
            }
        }
      
        List<genesis__Applications__c> apps = [Select Id, genesis__Contact__c, Partner_Account__c, genesis__Status__c
                                               FROM genesis__Applications__c 
                                               WHERE genesis__Contact__c IN: conSet AND Selected_Offer_Id__c = null 
                                               ORDER BY createdDate desc ];  
      
        for(genesis__Applications__c app : apps){
            If(appsToUpdateMap.isEmpty()){
                app.Partner_Account__c = partnerActMap.get(conActMap.get(app.genesis__Contact__c));
               //s If(app.genesis__Status__c == 'Credit Approved - No Contact Allowed')
                    app.genesis__Status__c = 'Credit Qualified';
                appsToUpdateMap.put(app.genesis__Contact__c,app);
            }else {
                If(!appsToUpdateMap.containsKey(app.genesis__Contact__c)){
                    app.Partner_Account__c = partnerActMap.get(conActMap.get(app.genesis__Contact__c));
                  //s  If(app.genesis__Status__c == 'Credit Approved - No Contact Allowed')
                        app.genesis__Status__c = 'Credit Qualified';
                    appsToUpdateMap.put(app.genesis__Contact__c,app);
                }
            }
        }
      
        system.debug('appsToUpdateMap ===============' +appsToUpdateMap);
        If(!appsToUpdateMap.isEmpty()){
            update appsToUpdateMap.values();
        }
        }catch(Exception e){
            system.debug('Exception Found ' +e.getMessage());
        }
      
    }
     /* INC- 546 */
}