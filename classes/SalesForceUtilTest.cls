@isTest public class SalesForceUtilTest {

    @isTest static void sends_email_by_user_id() {
        boolean result = SalesForceUtil.EmailByUserId('Subject Testing', 'Body Testing', '0053B000000ejaeQAA');
        System.assertEquals(true, result);

        result = SalesForceUtil.EmailByUserId('Subject Testing', 'Body <br/> Testing', '0053B000000ejaeQAA');
        System.assertEquals(true, result);
    }

    @isTest static void sends_email_by_user_id_with_org_wide() {
        boolean result = SalesForceUtil.EmailByUserIdWithOrgWide('Subject Testing', 'Body Testing', '0053B000000ejaeQAA', '0D2U0000000PDLUKA4');
        System.assertEquals(true, result);

        result = SalesForceUtil.EmailByUserIdWithOrgWide('Subject Testing', 'Body <br/> Testing', '0053B000000ejaeQAA', '0D2U0000000PDLUKA4');
        System.assertEquals(true, result);
    }

    @isTest static void calculates_business_days() {
        Datetime newDate = Datetime.newInstance(2017, 07, 20);

        Integer result = SalesForceUtil.calculateBusinessDays(newDate, newDate.addDays(1));
        System.assertEquals(1, result);

        Date oneYearAgo = Date.today().addYears(-1);
        Datetime newDate2 = Datetime.newInstance(oneYearAgo.year(), oneYearAgo.month(), oneYearAgo.day());

        result = SalesForceUtil.calculateBusinessDays(newDate2, Datetime.now());
        
        System.assert(result >= 251 && result <= 256, result);
        
        newDate = Datetime.newInstance(2017, 05, 29);
        
        result = SalesForceUtil.calculateBusinessDays(newDate, newDate.addDays(2));
        System.assertEquals(1, result);
        
        newDate = Datetime.newInstance(2017, 01, 01);

        result = SalesForceUtil.calculateBusinessDays(newDate, newDate.addDays(2));
        System.assertEquals(1, result);
    }
    
    @isTest static void calculates_business_date() {
        Datetime result = SalesForceUtil.getBusinessDate(10);
        Datetime expected = System.now().addDays(-10);
        
        System.assert(expected > result,'Datetime is not accurate');
    }
    
    @isTest static void formats_without_decimals() {
        String result = SalesForceUtil.formatMoney('200');

        System.assertEquals('$200.00', result);
    }

    @isTest static void formats_with_one_decimal() {
        String result = SalesForceUtil.formatMoney('200.1');

        System.assertEquals('$200.10', result);
    }

    @isTest static void formats_with_two_decimals() {
        String result = SalesForceUtil.formatMoney('200.23');

        System.assertEquals('$200.23', result);
    }

    @isTest static void formats_with_three_decimals() {
        String result = SalesForceUtil.formatMoney('200.239');

        System.assertEquals('$200.239', result);
    }

   @isTest static void gets_credit_report_not_generated() {
        Opportunity app = LibraryTest.createApplicationTH();
        app.Alerts__c = 'Queue';
        update app;

        Scorecard__c score = new Scorecard__c();
        score.Acceptance__c = 'Y';
        score.Opportunity__c = app.Id;
        score.Grade__c = 'B1';
        score.FICO_Score__c = 720;
        insert score;

        String result = SalesForceUtil.getCreditReportNotGenerated();

        app = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Alerts__c
               FROM Opportunity
               WHERE Id =: app.Id LIMIT 1];
        
        System.assertEquals('Changed - Existing', app.Alerts__c);
        System.assertEquals('Credit Qualified', app.Status__c);
        System.assertEquals(null, app.Reason_of_Opportunity_Status__c);
        
        score.Acceptance__c = 'N';
        update score;

        app.Alerts__c = 'Queue';
        update app;

        result = SalesForceUtil.getCreditReportNotGenerated();
        
        app = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Alerts__c
               FROM Opportunity
               WHERE Id =: app.Id LIMIT 1];
        
        System.assertEquals('Changed - Existing', app.Alerts__c);
        System.assertEquals('Declined (or Unqualified)', app.Status__c);
        System.assertEquals('Updated Scoring – No Offer', app.Reason_of_Opportunity_Status__c);
    } 
    
    @isTest static void gets_credit_report_not_generated_N() {
        Opportunity app = LibraryTest.createApplicationTH();
        app.Alerts__c = 'Queue';
        update app;

        Scorecard__c score = new Scorecard__c();
        score.Acceptance__c = 'N';
        score.Opportunity__c = app.Id;
        score.Grade__c = 'F';
        score.FICO_Score__c = 720;
        insert score;

        String result = SalesForceUtil.getCreditReportNotGenerated();
        
        app = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Alerts__c
               FROM Opportunity
               WHERE Id =: app.Id LIMIT 1];
        
        System.assertEquals('Changed - Existing', app.Alerts__c);
        System.assertEquals('Declined (or Unqualified)', app.Status__c);
        System.assertEquals('Updated Scoring – No Offer', app.Reason_of_Opportunity_Status__c);
    } 
    
    @isTest static void get_unemployment_rate() {
        StateIndexUR__c s = new StateIndexUR__c();
        s.UnemploymentRate__c = 5.00;
        s.State__c = 'GA';
        s.ZipCode__c = '31050';
        s.County__c = '040';
        insert s;

        s = new StateIndexUR__c();
        s.UnemploymentRate__c = 4.00;
        s.State__c = 'GA';
        s.ZipCode__c = '';
        s.County__c = '040';
        insert s;

        Contact c = LibraryTest.createContactTH();
        decimal result = SalesForceUtil.getUnemploymentRate(c);
        System.assertEquals(true, result>0);
        c.MailingPostalCode = '31050';
        update c;
        result = SalesForceUtil.getUnemploymentRate(c);
        System.assertEquals(true, result>0);
        result = SalesForceUtil.getUnemploymentRate(null);
        System.assertEquals(true, result==0.00);
    }
    
    @isTest static void account_management_pull() {
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Lead_Sub_Source__c = 'LoanHero';
        update opp;
        
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.Opportunity__c = opp.Id;
        testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
        upsert testLoanAccount;
        
        Contact c = [SELECT Id, Other_Use_Of_Funds__c
                     FROM Contact 
                     WHERE Id =: testLoanAccount.loan__Contact__c];
        
        c.Other_Use_Of_Funds__c = 'SUBMITTED_AC';
        update c;
        
        SalesForceUtil.accountManagementPullsASC();
        SalesForceUtil.accountManagementPullsDESC();
    }
    
    @isTest static void get_time_between_dates() {
        DateTime d1 = system.today();
        DateTime d2 = system.today().adddays(10);
        Time t = SalesForceUtil.GetTimeBetweenDates(d1,d2);
        SalesForceUtil.QueryByGroupId();
        GroupMember gm = [Select Id, UserOrGroupId, GroupId From GroupMember LIMIT 1];
        SalesForceUtil.SetOfflineUserByGroupId(gm.Id);
    }
    
    @isTest static void gets_latest_credit_report_am(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.CreditPull_Date__c = Date.today().addDays(-2);
        update opp;
        loan__loan_account__c contract = LibraryTest.createContractTH();
        AccountManagementHistory__c am = new AccountManagementHistory__c();
        am.CreditReportId__c = 88888888;
        am.Opportunity__c = opp.Id;
        am.Contract__c = contract.Id;
        insert am;
        Test.setCreatedDate(am.Id, Datetime.now().addDays(-1));
        Test.startTest();
        String creditReportData = SalesForceUtil.GetLatestValidCreditReportId(opp.Id, true);
        String creditReportId = creditReportData.split(',').get(0);
        String sequenceId = creditReportData.split(',').get(1);
        Test.stopTest();
        System.assertEquals('88888888',creditReportId);
        System.assertEquals('10', sequenceId);
    }
    
    @isTest static void gets_latest_credit_report_am_not_valid(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.CreditPull_Date__c = Date.today().addDays(-1);
        opp.PrimaryCreditReportId__c = 88888882;
		update opp;
        loan__loan_account__c contract = LibraryTest.createContractTH();
        AccountManagementHistory__c am = new AccountManagementHistory__c();
        am.CreditReportId__c = 88888888;
        am.Opportunity__c = opp.Id;
        am.Contract__c = contract.Id;
        insert am;
        Test.setCreatedDate(am.Id, Datetime.now().addDays(-40));
        Test.startTest();
        String creditReportData = SalesForceUtil.GetLatestValidCreditReportId(opp.Id, true);
        String creditReportId = creditReportData.split(',').get(0);
        String sequenceId = creditReportData.split(',').get(1);
        Test.stopTest();
        System.assertEquals('88888882',creditReportId);
        System.assertEquals('1', sequenceId);
    }
    
    @isTest static void gets_latest_credit_report_soft_am_not_valid(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.CreditPull_Date__c = Date.today().addDays(-35);
        opp.PrimaryCreditReportId__c = 88888882;
		update opp;
        loan__loan_account__c contract = LibraryTest.createContractTH();
        AccountManagementHistory__c am = new AccountManagementHistory__c();
        am.CreditReportId__c = 88888888;
        am.Opportunity__c = opp.Id;
        am.Contract__c = contract.Id;
        insert am;
        Test.setCreatedDate(am.Id, Datetime.now().addDays(-40));
        Test.startTest();
        String creditReportData = SalesForceUtil.GetLatestValidCreditReportId(opp.Id, true);
        String creditReportId = creditReportData.split(',').get(0);
        String sequenceId = creditReportData.split(',').get(1);
        Test.stopTest();
        System.assertEquals('0',creditReportId);
        System.assertEquals('0', sequenceId);
    }
    
    @isTest static void gets_latest_credit_report_soft_not_valid(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.CreditPull_Date__c = Date.today().addDays(-35);
        opp.PrimaryCreditReportId__c = 88888882;
		update opp;
        Test.startTest();
        String creditReportData = SalesForceUtil.GetLatestValidCreditReportId(opp.Id, true);
        String creditReportId = creditReportData.split(',').get(0);
        String sequenceId = creditReportData.split(',').get(1);
        Test.stopTest();
        System.assertEquals('0',creditReportId);
        System.assertEquals('0', sequenceId);
    }
    
    @isTest static void gets_latest_credit_report_soft(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.CreditPull_Date__c = Date.today().addDays(-15);
        opp.PrimaryCreditReportId__c = 88888882;
		update opp;
        Test.startTest();
        String creditReportData = SalesForceUtil.GetLatestValidCreditReportId(opp.Id, true);
        String creditReportId = creditReportData.split(',').get(0);
        String sequenceId = creditReportData.split(',').get(1);
        Test.stopTest();
        System.assertEquals('88888882',creditReportId);
        System.assertEquals('1', sequenceId);
    }
    
    @isTest static void Test_checkValuesForOffer_Validate_Fee(){
        
        Check_Values_Offer_Selection__c setting = New Check_Values_Offer_Selection__c();
        setting.Name = 'LP_GA_3k_+k';
        setting.check_Product_Name__c = 'Lending Point';
        setting.check_Loan_Amount_min__c = 3001.00;
        setting.check_Effective_APR_min__c = 0;
        setting.check_Effective_APR_max__c = 36 ;
        setting.check_Fee_Percent_min__c = 0;
        setting.check_Fee_Percent_max__c  = 6 ;
        setting.check_States_Included__c = 'GA';
        setting.check_IsFinwise__c = false;
        setting.check_IsFEB__c = false;
        insert setting;
        
        

        
        Opportunity opp = Librarytest.createApplicationTH();
        opp.Fee__c = 320.00;
        opp.LeadSource__c  = 'EZVERIFY';
        opp.Effective_APR__c = 19.89;
        update opp;
        
        Opportunity app = [SELECT  Id,
                           LeadSource__c,
                           Lending_Product__r.Name,
                           ProductName__c,
                           Effective_APR__c,
                           Lending_Product__c,
                           Status__c,Is_FEB__c,
                           Total_Loan_Amount__c,
                           State__c,
                           Fee__c,
                           Is_Finwise__c
                           FROM    Opportunity
                           WHERE   Id = : opp.Id LIMIT 1];
        
        SalesForceUtil.checkValuesForOffer( String.valueOf(app.State__c), 
                                                    Double.valueOf(app.Effective_APR__c), 
                                                     Double.valueOf(app.Fee__c),
                                                      Double.valueOf(app.Total_loan_Amount__c),
                                                       String.valueOf(app.Lending_Product__r.Name),
                                                        Boolean.valueOf(app.Is_Finwise__c),
                                                         Boolean.valueOf(app.Is_FEB__c) );
        
        system.assertEquals(setting.Name , 'LP_GA_3k_+k');
        
    }
    
    @isTest static void Test_checkValuesForOffer_Validate_APR(){
    
    Check_Values_Offer_Selection__c setting2 = New Check_Values_Offer_Selection__c();
        setting2.Name = 'Finwise_GA_-k_3k';
        setting2.check_Product_Name__c = '';
        setting2.check_Loan_Amount_min__c = 0;
        setting2.check_Loan_Amount_max__c = 4700;
        setting2.check_Effective_APR_max__c = 36 ;
        setting2.check_Fee_Percent_min__c = 0;
        setting2.check_Fee_Percent_max__c  = 6 ;
        setting2.check_States_Included__c = 'GA';
        setting2.check_IsFinwise__c = false;
        setting2.check_IsFEB__c = true;
        insert setting2;
        
        Opportunity opp = Librarytest.createApplicationTH();
        opp.Fee__c = 120.00;
        opp.LeadSource__c  = 'EZVERIFY';
        opp.Effective_APR__c = 36.89;
        update opp;
        
        Opportunity app = [SELECT  Id,
                           LeadSource__c,
                           Lending_Product__r.Name,
                           ProductName__c,
                           Effective_APR__c,
                           Lending_Product__c,
                           Status__c,Is_FEB__c,
                           Total_Loan_Amount__c,
                           State__c,
                           Fee__c,
                           Is_Finwise__c
                           FROM    Opportunity
                           WHERE   Id = : opp.Id LIMIT 1];
        
        SalesForceUtil.checkValuesForOffer( String.valueOf(app.State__c), 
                                                    Double.valueOf(app.Effective_APR__c), 
                                                     Double.valueOf(app.Fee__c),
                                                      Double.valueOf(app.Total_loan_Amount__c),
                                                       String.valueOf(app.Lending_Product__r.Name),
                                                        Boolean.valueOf(app.Is_Finwise__c),
                                                         Boolean.valueOf(app.Is_FEB__c) );
        
        system.assertEquals(setting2.Name, 'Finwise_GA_-k_3k');
    }
    
    @isTest static void case_To_Close() {
        Opportunity app = LibraryTest.createApplicationTH();
        
        Case c = new Case();
        c.Description = 'something to test Test258@MyCompany.COM test test test test docs@lendingpoint.com test';
        c.SuppliedEmail = 'docs@lendingpoint.com';
        c.Subject = 'Documents Have Been Uploaded from Web';
        c.Opportunity__c = app.Id;
        
        insert c;
        
		app.Status__c = 'Funded';
        update app;
        
        case ctest = [SELECT Id, Status, IsClosed FROM Case WHERE Opportunity__c =: app.Id];
        System.assert(ctest.IsClosed);     
    }
}