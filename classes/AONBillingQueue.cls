public class AONBillingQueue {

    private Static Final String AON_FEE = 'AON Fee';
    
	/*@invocableMethod
	public static void insertBillingRecord(List<String> contractList) {
		AON_Fee__c aonFee = AON_Fee__c.getValues(AON_FEE);
        LIst<loan_AON_Billing__c> billList = new List<loan_AON_Billing__c>();
        List<loan_AON_Information__c> aonList = [SELECT Id,
			                        Company__c,
			                        Contract__c,
			                        Contract__r.AON_Fee__c,
			                        Contract__r.loan__Pmt_Amt_Cur__c,
			                        Contract__r.loan__Next_Installment_Date__c,
			                        Product_Code__c,
			                        Contract__r.Opportunity__r.AON_FeesTotal__c,
			                        Contract__r.Opportunity__r.Term__c
			                        FROM loan_AON_Information__c
			                        WHERE Contract__c IN : contractList ORDER BY CreatedDate DESC ];
        
        for (String cnt : contractList) {
            for (loan_AON_Information__c contractToProcess : aonList) {
                if (contractToProcess.Contract__c == cnt) {
                    loan_AON_Billing__c billToInsert = new loan_AON_Billing__c();
                    
                    billToInsert.loan_AON_Information__c = contractToProcess.Id;
                    billToInsert.Billing_Date__c = Datetime.now();
                    billToInsert.Company__c = contractToProcess.Company__c;
                    billToInsert.Contract__c = contractToProcess.Contract__c;
                    billToInsert.Minimum_Monthly_Payment__c = contractToProcess.Contract__r.loan__Pmt_Amt_Cur__c.setScale(2, RoundingMode.HALF_UP);
                    billToInsert.Payment_Due_Date__c = contractToProcess.Contract__r.loan__Next_Installment_Date__c;
                    billToInsert.Product_Code__c = contractToProcess.Product_Code__c;
                    billToInsert.Product_Fee__c = (contractToProcess.Contract__r.AON_Fee__c == 0) ? ((aonFee.Fee__c / 100) * contractToProcess.Contract__r.loan__Pmt_Amt_Cur__c).setScale(2, RoundingMode.HALF_UP) : 
                    (contractToProcess.Contract__r.Opportunity__r.AON_FeesTotal__c / contractToProcess.Contract__r.Opportunity__r.Term__c).setScale(2, RoundingMode.HALF_UP);
                    billToInsert.IsEnrollment__c = false;
                    billToInsert.IsProcessed__c = false;
                    billList.add(billToInsert);
                    break;
                }
            }
		}
        if (billList.size() > 0) insert billList;
    }*/

    @invocableMethod
    public static void insertBillingRecord(List<String> billsList){
        System.debug(billsList);
        AON_Fee__c aonFee = AON_Fee__c.getValues('AON Fee');
        List<loan_AON_Billing__c> billList = new List<loan_AON_Billing__c>();
        List<loan__Loan_Account_Due_Details__c> dueDetailsList = [SELECT Id, loan__loan_Account__c FROM loan__Loan_account_Due_Details__c WHERE Id IN :billsList];
        
        List<String> contractList = new List<String>();
		Map<String,String> billToContract = new Map<String,String>();
		
        for(loan__Loan_account_Due_Details__c dueDetails : dueDetailsList){
			billToContract.put(dueDetails.Id, dueDetails.loan__loan_Account__c);
            contractList.add(dueDetails.loan__loan_Account__c);
		}
		
		List<loan_AON_Information__c> aonList = [SELECT Id,
									Company__c,
									Contract__c,
									Contract__r.AON_Fee__c,
									Contract__r.loan__Pmt_Amt_Cur__c,
									Contract__r.loan__Next_Installment_Date__c,
									Product_Code__c,
									Contract__r.Opportunity__r.AON_FeesTotal__c,
									Contract__r.Opportunity__r.Term__c
									FROM loan_AON_Information__c
									WHERE Contract__c IN : contractList ORDER BY CreatedDate DESC ];

		Map<String,String> existBilling = new Map<String,String>();
		for(loan_AON_Billing__c billings : [SELECT Id, Account_Due_Details__c, Contract__c FROM loan_AON_Billing__c WHERE Account_Due_Details__c IN :billsList]){
			existBilling.put(billings.Account_Due_Details__c, billings.Contract__c);
		}

        String contractVal = null;
		for(String bill : billsList){
			for (loan_AON_Information__c contractToProcess : aonList) {
				if(existBilling.containsKey(bill)){
					break;
				}
				contractVal = billToContract.get(bill);
				if(contractVal == contractToProcess.Contract__c){
					loan_AON_Billing__c billToInsert = new loan_AON_Billing__c();
					billToInsert.loan_AON_Information__c = contractToProcess.Id;
					billToInsert.Account_Due_Details__c = bill;
					billToInsert.Billing_Date__c = Datetime.now();
					billToInsert.Company__c = contractToProcess.Company__c;
					billToInsert.Contract__c = contractToProcess.Contract__c;
					billToInsert.Minimum_Monthly_Payment__c = contractToProcess.Contract__r.loan__Pmt_Amt_Cur__c.setScale(2, RoundingMode.HALF_UP);
					billToInsert.Payment_Due_Date__c = contractToProcess.Contract__r.loan__Next_Installment_Date__c;
					billToInsert.Product_Code__c = contractToProcess.Product_Code__c;
					billToInsert.Product_Fee__c = (contractToProcess.Contract__r.AON_Fee__c == 0) ? ((aonFee.Fee__c / 100) * contractToProcess.Contract__r.loan__Pmt_Amt_Cur__c).setScale(2, RoundingMode.HALF_UP) : 
					(contractToProcess.Contract__r.Opportunity__r.AON_FeesTotal__c / contractToProcess.Contract__r.Opportunity__r.Term__c).setScale(2, RoundingMode.HALF_UP);
					billToInsert.IsEnrollment__c = false;
					billToInsert.IsProcessed__c = false;
					billList.add(billToInsert);
					break;
				}
			}
		}

		if (billList.size() > 0) insert billList;
    }
}