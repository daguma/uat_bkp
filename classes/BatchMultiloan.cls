global class BatchMultiloan implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable  {

	global final String query;
	global final String option;
	global final Integer queryLimit; 


	global BatchMultiloan(String option, Integer queryLimit) {
		this.option = option;
		this.queryLimit = queryLimit;

		if (option == 'EligibilityCheck') {
			this.query = MultiloanPreSoftPull.getEligibilityReviewQuery(this.queryLimit);
		} else if (option == 'EligibilityCheckGetCR') {
			this.query = MultiloanPostSoftPull.ELIGIBILITY_CREDIT_REPORT_QUERY;
		}
	}
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		if (option == 'EligibilityCheck') {
			for (sobject s : scope) {
				Multiloan_Params__c param = (Multiloan_Params__c) s;
				MultiloanPreSoftPull.EligibilityCheck(param);
			}
		} else if (option == 'EligibilityCheckGetCR') {
			for (sobject s : scope) {
				Multiloan_Params__c param = (Multiloan_Params__c) s;
				MultiloanPostSoftPull.eligibilityCheckGetCR(param);
			}
		}
	}

	global void finish(Database.BatchableContext BC) {
		if (option == 'EligibilityCheck') {
			BatchMultiloan job = new BatchMultiloan('EligibilityCheckGetCR', 0);
			System.scheduleBatch(job, 'MultiloanJob_EligibilityCheckGetCR', 1, 1);  //Each 1 minute, process batches of 1 by 1
		}
	}

	global void execute(SchedulableContext sc) {
		if (option == 'EligibilityCheck') {
			MultiLoan_Settings__c multiLoanSettings = MultiLoan_Settings__c.getValues('settings');
			BatchMultiloan job = new BatchMultiloan(option, queryLimit);
			database.executebatch(job, multiLoanSettings != null && multiLoanSettings.Eligibility_Check_Pre_Batch_Size__c != null ? multiLoanSettings.Eligibility_Check_Pre_Batch_Size__c.intValue() : 1);
		}
	}
}