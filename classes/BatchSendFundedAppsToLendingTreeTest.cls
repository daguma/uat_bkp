@isTest()
Public class BatchSendFundedAppsToLendingTreeTest{
    static testMethod void bulkTest(){
        Partner_Configurations__c partConf = new Partner_Configurations__c();
        partConf.Name = 'LendingTree';
        partConf.End_Point__c = 'test.com';
        partConf.Funded_Day__c = system.today().addDays(-30);
        partConf.LenderUID__c = '123';
        partConf.Password__c = '321';
        partConf.Username__c = 'testUser';
        insert partConf;
                
        Account lendTree = new Account(name='Lending Tree', Partner_Sub_Source__c= 'LendingTree', Type= 'Partner',Is_Parent_Partner__c = true);
        insert lendTree;
 
        
        loan__loan_Account__c loan = LibraryTest.createContractTH();

        loan__Loan_Product__c lp = [SELECT Id, Name, loan__Default_Interest_Rate__c, loan__Interest_Calculation_Method__c, loan__Delinquency_Grace_Days__c, loan__Write_off_Tolerance_Amount__c, loan__Late_Charge_Grace_Days__c,
                                    loan__Amortize_Balance_type__c, loan__Amortization_Frequency__c, loan__Amortization_Enabled__c, loan__Amortization_Calculation_Method__c,
                                    loan__Fee_Set__c, loan__Pre_Bill_Days__c
                                    FROM loan__Loan_Product__c WHERE Name = 'Sample Loan Product' LIMIT 1];
        
        opportunity app1 = LibraryTest.createApplicationTH();
        app1.Status__c  = 'Funded';
        app1.Funded_Date__c = system.today();
        app1.Partner_Account__c = lendTree.Id;
        app1.Point_Code__c = 'LT500084255';
        app1.Interest_Rate__c =  5.5;
        app1.effective_APR__c = 10.2;
        app1.Term__c = 12;
        app1.Expected_First_Payment_Date__c = Date.today().addDays(15);
        app1.Term__c = 36;
        app1.Expected_Start_Date__c = Date.today().addDays(2);
        app1.ACH_Account_Number__c = '1234567890';
        app1.ACH_Account_Type__c = 'Test';
        app1.ACH_Bank_Name__c = 'Test Bank';
        app1.Payment_Amount__c = 196.14;
        app1.Payment_Frequency__c = 'Monthly';
        app1.ACH_Routing_Number__c = '123456789';
        app1.Interest_Rate__c = .2399;
        app1.Payment_Method__c = 'ACH';
        app1.Expected_Disbursal_Date__c = Date.today().addMonths(2);
        app1.Estimated_Grand_Total__c = 50000;
        app1.Payroll_Frequency__c = 'Monthly';
        app1.Expected_Close_Date__c = System.Today().addMonths(36);
        app1.DTI__C = '18.000';
        app1.FICO__c = '700';
        app1.Lead_Sub_Source__c = 'LendingTree'; 
        app1.Product_Type__c = 'LOAN';
        update app1;

        CreateLoanAccountForLoanTypeHandler loanAccount = new CreateLoanAccountForLoanTypeHandler(app1, loan, lp);

        String result = loanAccount.createLoanAccount();

        
        opportunity app2 = LibraryTest.createApplicationTH2();
        app2.Status__c  = 'Funded';
        app2.Funded_Date__c = system.today();
        app2.Partner_Account__c = lendTree.Id;
        app2.Point_Code__c = 'LT500084112';
        app2.Interest_Rate__c =  5.5;
        app2.effective_APR__c = 10.2;
        app2.Term__c = 12;
        app2.Expected_First_Payment_Date__c = Date.today().addDays(15);
        app2.Term__c = 36;
        app2.Expected_Start_Date__c = Date.today().addDays(2);
        app2.ACH_Account_Number__c = '1234567890';
        app2.ACH_Account_Type__c = 'Test';
        app2.ACH_Bank_Name__c = 'Test Bank';
        app2.Payment_Amount__c = 196.14;
        app2.Payment_Frequency__c = 'Monthly';
        app2.ACH_Routing_Number__c = '123456789';
        app2.Interest_Rate__c = .2399;
        app2.Payment_Method__c = 'ACH';
        app2.Expected_Disbursal_Date__c = Date.today().addMonths(2);
        app2.Estimated_Grand_Total__c = 50000;
        app2.Payroll_Frequency__c = 'Monthly';
        app2.Expected_Close_Date__c = System.Today().addMonths(36);
        app2.DTI__C = '18.000';
        app2.FICO__c = '700';
        app2.Lead_Sub_Source__c = 'LendingTree';
        update app2;

        loanAccount = new CreateLoanAccountForLoanTypeHandler(app2, loan, lp);

        result = loanAccount.createLoanAccount();
        
        // Using StaticResourceCalloutMock built-in class to specify fake response and include response body 
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('LendingTree_FundingAPI_Mock_Response');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/xml');
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        
        

        BatchSendFundedAppsToLendingTree callBatch = new BatchSendFundedAppsToLendingTree();
        callBatch.execute(null);
        
        string resp = '<?xml version="1.0" encoding="UTF-8"?><test11> ....</test11>';
        
        callBatch.parseResponse(resp);
        
    }
}