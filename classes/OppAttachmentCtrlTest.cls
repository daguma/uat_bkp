@IsTest
public class OppAttachmentCtrlTest {
    
    @IsTest
    static void get_attachments(){
        Opportunity app = LibraryTest.createApplicationTH();
        
        Attachment customAttachment = new Attachment();
        customAttachment.Body = Blob.valueof('');
        customAttachment.name ='Test';
        customAttachment.parentId = app.Id;
        insert customAttachment;
        
        Case customCase = new Case();
        customCase.Opportunity__c = app.Id;
        insert customCase;

        System.assert(OppAttachmentCtrl.getOpportunityAttachments(customCase.Id).size() > 0);
    }
    
    @IsTest
    static void delete_attachment(){
        Opportunity app = LibraryTest.createApplicationTH();
        
        Attachment customAttachment = new Attachment();
        customAttachment.Body = Blob.valueof('');
        customAttachment.name ='Test';
        customAttachment.parentId = app.Id;
        insert customAttachment;
        
        Integer attachmentBefore = [SELECT COUNT() FROM Attachment WHERE parentid = : app.id];
        OppAttachmentCtrl.deleteAttachment(customAttachment.Id);
        Integer attachmentAfter = [SELECT COUNT() FROM Attachment WHERE parentid = : app.id];
        
        System.assert(attachmentAfter < attachmentBefore, 'Attachment removed');
    }
    
    @IsTest
    static void create_attachment(){
        Opportunity app = LibraryTest.createApplicationTH();
        
        Case customCase = new Case();
        customCase.Opportunity__c = app.Id;
        insert customCase;
		
        Integer attachmentBefore = [SELECT COUNT() FROM Attachment WHERE parentid = : app.id];
        OppAttachmentCtrl.saveAttachment(customCase.Id, 'Test File', 'VGVzdA==', 'test type');
        Integer attachmentAfter = [SELECT COUNT() FROM Attachment WHERE parentid = : app.id];
        
        System.assert(attachmentAfter > attachmentBefore, 'Attachment created');

    }
    

}