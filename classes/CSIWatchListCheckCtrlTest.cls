@isTest
class CSIWatchListCheckCtrlTest {
    static testMethod void test() {
        Contact con = new Contact(LastName='TestContact',Firstname = 'David',Email = 'test@gmail2.com',MailingState = 'GA');
        insert con;

        Opportunity opp = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id);
        insert opp;
        
         CSIRCD__ATTUSWDSFLookup__c csirc = new CSIRCD__ATTUSWDSFLookup__c();
        csirc.CSIRCD__review_status__c = 'No match';
        csirc.CSIRCD__Contact__c = opp.Contact__c;
        csirc.CSIRCD__Lookup_By__c = 'test';
        csirc.CSIRCD__Name_Searched__c = 'test';
        csirc.CSIRCD__Number_of_Hits__c = 0;
        insert csirc;        
        CSIWatchListCheckCtrl.getCSIDetails(opp.Id);
    }
}