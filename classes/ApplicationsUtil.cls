public with sharing class ApplicationsUtil {
    public Opportunity application {get; set;}
    public Employment_Details__c employmentDetails {get; set;}

    private Override_Profiles__c cuSettings;

    public ApplicationsUtil(Opportunity App) {

        this.application = null;
        this.employmentDetails = null;

        for (Opportunity currentApp : [SELECT Id,
                Name,
                Lead_Sub_Source__c,
                Status__c,
                CreatedDate,
                Reason_of_Opportunity_Status__c,                       
                Contact__r.Employment_Type__c,
                Contact__r.Employment_Start_Date__c,
                Employment_Defaulted_Value__c
                FROM Opportunity
                WHERE Id = : App.Id
                ]) {
            this.application = currentApp;
        }

        for (Employment_Details__c details : [SELECT Id, Hire_Date__c
                                              FROM Employment_Details__c
                                              WHERE Opportunity__c = : App.Id
                                             ]) {
            this.employmentDetails = details;
        }

        cuSettings = Override_Profiles__c.getOrgDefaults();
    }

    public  String ValidatePartnerForApplication() {
        if (this.application.Id != null && this.application.Lead_Sub_Source__c == 'Credit Karma') return 'Please be aware that you cannot contact this customer in any way unless there is already an offer accepted by the customer with us.';
        else return '';
    }

    public void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }

    /**
     * Create a new Note in Applications.
     * @param  parenID [application Id], body, title.
     * @return  None.
     */
    public void InsertNote(String parentid, String body, String title) {

        Note newNote = new Note();
        newNote.parentid = parentid;
        newNote.title = title;
        newNote.body = body;
        insert newNote;
    }

    /**
     * Check if the application is readonly. An application is readonly if the status is
     * Funded and the user profile is not system admin or manager.
     * @param  userProfileId [description]
     * @return               [description]
     */
    public Boolean isReadOnly(String userProfileId) {

        Boolean result = false;

        if (this.application.Status__c != null) {
            Boolean isOverrideProfile = (cuSettings.Funded_Applications__c != null &&
                                         cuSettings.Funded_Applications__c.contains(userProfileId));

            result = this.application.Status__c.equalsIgnoreCase('Funded') && !isOverrideProfile;
        }

        return result;
    }

    /**
     * [getReadOnlyClass description]
     * @param  userProfileId [description]
     * @return               [description]
     */
    public String getReadOnlyClass(String userProfileId) {
        return isReadOnly(userProfileId) ? 'readOnly' : '';
    }

    /**
     * Checks if the application does not pass disqualifiers at hard pull
     * @return [description]
     */
    public Boolean isDeclineByHardPull() {
        Boolean result = false;

        if (!String.isEmpty(this.application.Status__c) &&
                !String.isEmpty(this.application.Reason_of_Opportunity_Status__c) &&
                this.application.Status__c.equalsIgnoreCase('Declined (or Unqualified)') &&
                this.application.Reason_of_Opportunity_Status__c.equalsIgnoreCase('Did not pass disqualifiers at Hard Pull')) {
            result = true;
        }

        return result;
    }

    public Boolean isDefaultEmploymentApp() {
        Date employmentStartDate = this.application.Contact__r.Employment_Start_Date__c;
        Date appCreatedDate1YearBack = this.application.CreatedDate.addMonths(-12).date();
        Date appCreatedDate2YearsBack = this.application.CreatedDate.addMonths(-24).date();
        Date appCreatedDate3YearsBack = this.application.CreatedDate.addMonths(-36).date();

        Boolean isEmploymentDefaultedValue = (this.application.Contact__r.Employment_Type__c == null ||
                                              this.application.Contact__r.Employment_Type__c.equalsIgnoreCase('Employee')) &&
                                             employmentStartDate != null &&
                                             ((employmentStartDate == appCreatedDate1YearBack) ||
                                              (employmentStartDate == appCreatedDate2YearsBack) ||
                                              (employmentStartDate == appCreatedDate3YearsBack));

        if (isEmploymentDefaultedValue && !this.application.Employment_Defaulted_Value__c) {
            this.application.Employment_Defaulted_Value__c = true;
            update this.application;
        }

        return isEmploymentDefaultedValue &&
               (this.employmentDetails == null || (this.employmentDetails != null && this.employmentDetails.Hire_Date__c == null));
    }
}