@isTest
private class EmailAgeTest {

    @isTest static void get_Email_Age_Result() {
        Proxy_Api__c proxy = LibraryTest.fakeSettings();
        Opportunity app = LibraryTest.createApplicationTH();
        EmailAge.getEmailAgeResult(app.Id,true);
    }
    
    @isTest static void map_Email_Age_Input_Data() {
        Proxy_Api__c proxy = LibraryTest.fakeSettings();
        Opportunity app = LibraryTest.createApplicationTH();
        EmailAgeRequestParameter emailAgeReqParam = EmailAge.mapEmailAgeInputData(app.Id);
        
        System.assertNotEquals(null, emailAgeReqParam);
    }
    
    @isTest static void get_Email_Age_Result_Using_SeqId() {
        Proxy_Api__c proxy = LibraryTest.fakeSettings();
        Opportunity app = LibraryTest.createApplicationTH();
        
        EmailAge.getEmailAgeResultUsingSeqId(app.Id,'123456',true);
        
        list<Email_Age_Details__c> emailAgeDetails = [select Id,Contact__c,Email_Age_Status__c,errorCode__c,status__c from Email_Age_Details__c 
                            where Contact__c = :app.contact__c];
        
        system.assertEquals(emailAgeDetails.size() > 0, true);
    }
}