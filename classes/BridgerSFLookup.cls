public class BridgerSFLookup {

	private string endpoint {get; set;}
	private string soapAction {get; set;}
	private Contact cont {get; set;} 

	public string clientId {get; set;}
	public string userId {get; set;}
	public string passWord {get; set;}
	private String templateName {get; set;}
	public String bridgerError {get; set;} 

	public list<BridgerWDSFLookup__c> matchList = new list<BridgerWDSFLookup__c>();

	public static final String ELIGIBILITY_BRIDGER_QUERY = 'SELECT Id, Contact__c, newCallNecessary__c ' + 
	                                                       'FROM BridgerWDSFLookup__c ' +
	                                                       'WHERE newCallNecessary__c  = true';

	public BridgerSFLookup(Contact cont){
		if (cont != null) this.cont = cont;
		getConfig();
	}  

	private void getConfig(){
		Bridger_Settings__c csBridger = Bridger_Settings__c.getOrgDefaults();

		if (runningInASandbox() || Test.isRunningTest()){
			clientId = csBridger.ClientId_UAT__c;
			userId = csBridger.UserId_UAT__c;
			passWord = csBridger.Password_UAT__c;
			endpoint = csBridger.EndPoint_UAT__c;
		}
		else {
			clientId = csBridger.ClientId_Prod__c; 
			userId = csBridger.UserId_Prod__c; 
			passWord = csBridger.Password_Prod__c; 
			endpoint = csBridger.EndPoint_Prod__c;
		}
		soapAction = csBridger.SoapAction_Search__c;
		templateName = csBridger.Template_Name__c;
	}

	public HttpRequest createRequest(){

		HttpRequest req = new HttpRequest();
        
		req.setMethod('POST');
		req.setHeader('Content-Type', 'text/xml');
        req.setBody(getSoapRequestBody());
		        
        if (test.isRunningTest()) 
        	return req;

        req.setEndpoint(endpoint);
        req.setHeader('SOAPAction', soapAction);
		return req;  
	}


	public string getSoapRequestBody(){
        
		for (EmailTemplate soapRequest : [SELECT Id, HTMLValue
		                      				FROM EmailTemplate 
		                      				WHERE Name =: templateName]){
		                                        
			return replaceValues(soapRequest.HTMLValue);                                    
		}
		return '';
	}

	@TestVisible
	private String replaceValues(String soapRequest){       
		DateTime dt = DateTime.newInstance(this.cont.BirthDate.year(),
		                                   this.cont.BirthDate.month(),
		                                   this.cont.BirthDate.day());  

		soapRequest = soapRequest.replace('##ClientId##', clientId);
		soapRequest = soapRequest.replace('##UserId##', userId);
		soapRequest = soapRequest.replace('##PassWord##', passWord);

		soapRequest = soapRequest.replace('##DOB##', dt.format('yyyy/MM/dd') ); //1983/09/16
		soapRequest = soapRequest.replace('##City##', this.cont.MailingCity);
		soapRequest = soapRequest.replace('##Postal##', this.cont.MailingPostalCode.length() <= 5 ? this.cont.MailingPostalCode : this.cont.MailingPostalCode.left(5));
		soapRequest = soapRequest.replace('##Street##', this.cont.MailingStreet);
		soapRequest = soapRequest.replace('##State##', this.cont.MailingState);
		soapRequest = soapRequest.replace('##SSN##', this.cont.SSN__c);
		soapRequest = soapRequest.replace('##Fname##', this.cont.FirstName);
		soapRequest = soapRequest.replace('##Lname##', this.cont.LastName);

		return soapRequest;
	}  

	public string connect(HttpRequest req){
		Http http = new Http();
		HTTPResponse res;
		string retString= '';

		try 
		 {	
		 	if (!test.isRunningTest()) {
	            res = http.send(req);
			    retString = res.getBodyDocument().toXmlString();
		    }
		 } 
		catch(System.CalloutException e) { 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,e.getmessage()); 
            bridgerError = 'ERROR trying to connect Bridger OFAC API, ' + e.getmessage();
        }	
        return retString; 
	}  

	public void parseDOC(string xmlData){
	    try
	    {
	    	if (String.isNotEmpty(xmlData) && !xmlData.contains('Records')){
				BridgerWDSFLookup__c item = new BridgerWDSFLookup__c( Contact__c = this.cont.Id ,
				                                                    name_searched__c = this.cont.Name,
				                                                    address_type__c = 'Mailing',
				                                                    search_street__c = this.cont.MailingStreet,
				                                                    search_city__c = this.cont.mailingcity,
				                                                    search_state__c = this.cont.mailingstate,
				                                                    search_postal_code__c = this.cont.MailingPostalCode,
				                                                    search_country__c = this.cont.mailingcountry,
				                                                    lookup_by__c = UserInfo.getName() + ' ' + Datetime.now().formatLong());
			
				item.List_Description__c = 'No match';
				item.List_Date__c = null;
				item.Result_Node__c = 'No match';
				item.Number_of_Hits__c = 0;
				item.review_status__c = 'No match';
				
                
                if (xmlData.contains('faultstring')){
                    item.List_Description__c = 'Bridger Call Error';
                    salesForceUtil.EmailByUserId('Bridger Call Error', 'Having some issues calling Bridger. \n \n Contact Name: ' + this.cont.Name + '.\n \n Contact Id: ' + this.cont.Id, '0050B0000089FUNQA2');
					
                }
                
                matchList.add(item);
			}
			else if (String.isNotEmpty(xmlData) && xmlData.contains('Records')){
				DOM.Document doc = new DOM.Document();
			    doc.load(xmlData);
			    DOM.XmlNode root = doc.getRootElement();
			    walkThroughXml(root);
			}		    
	    }
	    catch(exception ex)
	    {
	        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,ex.getmessage());
	    }
	    finally{
	    	if (matchList.size() > 0)
	    		insert matchList;
	    }
	}
    
    
    private void walkThroughXml(DOM.XMLNode node){
    	if (node == null){
            return;
        }
    	for (Dom.XMLNode child: node.getChildElements()) {
    		if(child.getName() == 'Matches'){
    			for (Dom.XMLNode match: child.getChildElements()){
    				if(match.getName() == 'WLMatch'){

    					BridgerWDSFLookup__c item = new BridgerWDSFLookup__c( Contact__c = this.cont.Id ,
    						                                                    name_searched__c = this.cont.Name,
    						                                                    address_type__c = 'Mailing',
    						                                                    search_street__c = this.cont.MailingStreet,
    						                                                    search_city__c = this.cont.mailingcity,
    						                                                    search_state__c = this.cont.mailingstate,
    						                                                    search_postal_code__c = this.cont.MailingPostalCode,
    						                                                    search_country__c = this.cont.mailingcountry,
    						                                                    lookup_by__c = UserInfo.getName() + ' ' + Datetime.now().formatLong());
    					
						item.List_Description__c = getNodeValue(match, 'File', 'Name');
						item.List_Date__c = date.valueof(getNodeValue(match, 'File', 'Published'));
						item.Result_Node__c = getNodeValue(match, 'EntityDetails', 'Comments');
						if (String.isEmpty(item.Result_Node__c)) item.Result_Node__c = getCommentsFromXML(match);
    					item.Number_of_Hits__c = 1;
    					item.review_status__c = 'True Match';

    					matchList.add(item);
    				}
    			}
    			break;
    		}
    		else{
                walkThroughXml(child);
            }
    	}
    }

    private String getNodeValue(DOM.XMLNode node, String nodeName, String field){
    	if (node == null) return '';
    	for(DOM.XMLNode n : node.getChildElements()){
    		if(n.getName() == nodeName){
                for(DOM.XMLNode c : n.getChildElements()){
                    if(c.getName() == field){
                        return c.getText();
                    }                     
                }
    		}
    	}
    	return '';
    }
    
    @TestVisible
    private String getCommentsFromXML(DOM.XMLNode node){
    	if (node == null) return '';
    	String resp = '';
        for (Dom.XMLNode child: node.getChildElements()) {
	    	if(node.getName() == 'AdditionalInfo'){
	    		for(DOM.XMLNode n : node.getChildElements()){
	    			if (n.getName() == 'EntityAdditionalInfo'){
	    				for(DOM.XMLNode c : node.getChildElements()){
		    				if(c.getName() == 'Comments'){
		    					resp +=  c.getText() + '. // ';
		    				}
		    			}
		    			break;
	    			}			
    			}
	    	}
	    	else{
	    		getCommentsFromXML(child);
	    	}
	    	resp.removeEnd(' // ');
		}	
	    return resp;
    }
    
     public static void callBridgerWhenChangeStatus(String contacId){
		for (BridgerWDSFLookup__c bridgerItem : [SELECT ID, CreatedDate, newCallNecessary__c 
								                FROM BridgerWDSFLookup__c
								                WHERE Contact__c = : contacId
                                    			ORDER BY CreatedDate DESC
                                   				LIMIT 1 ]) {
                                                    
            if  (bridgerItem.CreatedDate < Date.today().addDays(-30)){
                                                        
            	bridgerItem.newCallNecessary__c = true;
            	update bridgerItem; 
            }		    
        }
    }

    public static void bridgerNewCall(BridgerWDSFLookup__c bridgerInfo){
    	List<Logging__c> exceptionLogs = new List<Logging__c>();
    	try{
    		Contact cont = [SELECT  Id,
		                        	Name,
			                        SSN__c,
			                        BirthDate,
			                        MailingStreet,
			                        MailingState,
			                        MailingPostalCode,
			                        MailingCountry,
			                        MailingCity,
			                        FirstName,
			                        LastName,
			                        AccountID
			                        FROM Contact
			                        WHERE Id = : bridgerInfo.Contact__c
	                                ORDER by CreatedDate
	                                LIMIT 1];
	        
	        String xmlResponse = '';

	        if (!Test.isRunningTest()) {
	        	BridgerSFLookup bridger = new BridgerSFLookup(cont);
	            xmlResponse = bridger.connect(bridger.createRequest());
	            bridger.parseDOC(xmlResponse);
	        }
    	}
    	catch(exception ex){
    		exceptionLogs.add(new Logging__c(Contact__c = bridgerInfo.Contact__c,
                                             Request__c = 'Call Bridger',
                                             Response__c = ex.getMessage() + ' line ' + (ex.getLineNumber()) + '\n' + ex.getStackTraceString(),
                                             Webservice__c = 'BridgerSFLookup.bridgerNewCall'));
    	}
    	finally{
    		bridgerInfo.newCallNecessary__c = false;
    		update bridgerInfo;
    	}
    }
    
	private static Boolean runningInASandbox() {
	    return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
	} 
}