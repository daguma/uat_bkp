/**
 * This class contains all the calls to the ProxyAPI
 * related to the credit report
 */
public class ProxyApiCreditReport extends ProxyApiUtil {
    private static final String GET_CREDIT_REPORT_URL = '/v101/get-credit-report/';
    private static final String APPROVED_CREDIT_REPORT_ATTRIBUTES_URL = '/v101/credit-report/approved-attributes/';
    private static final String GET_CREDIT_REPORTS_BY_ENTITY_URL = '/v101/get-credit-reports/by-entity-id=';
    private static final String GET_CREDIT_REPORT_BY_ID_URL = '/v101/get-credit-report/by-id=';
    private static final String RE_GRADE_URL = '/v101/credit-report/re-grade/';
    private static final String GET_HARD_CREDIT_REPORT_BY_ENTITY_ID = '/v101/get-last-hard-credit-report/by-entity-id=';
    private static final String GDS_URL = '/v101/get-gds-response/';
    
    /**
     * Calls the ProxyAPI to get a new credit report from ATB
     * @param  inputData The data needed in the request to get the credit report
     * @param  applicationId  The id of the application to get the credit report
     * @param  accountId  The id of the account
     * @param  contactId  The id of the contact
     * @param  leadId  The id of the lead
     * @param  isHardPull  Set to true if is a hard pull, otherwise false
     * @return creditReportEntity Returns a credit report entity
     */
    public static ProxyApiCreditReportEntity getCreditReport(String inputData,
            Boolean isHardPull,
            Boolean forcePull) {
 
        ProxyApiCreditReportEntity creditReportEntity = null;
        
        //BRMS change
        Boolean validatedAPI = false;
        
        CreditReportInputData crInputParam =
                (CreditReportInputData) JSON.deserialize(inputData, CreditReportInputData.class);
              
        if(crInputParam.gdsRequestParameter <> null) 
            validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        else
            validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();
        //BRMS change
        
        if (validatedAPI) {

            String body = 'input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8') +
                          '&is-hard-pull=' + isHardPull +
                          '&force-pull=' + forcePull;
            system.debug('--getCreditReport:body--'+body);
            String jsonResponse = null;
            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeCreditReportData();
            } else {
                 if(crInputParam.gdsRequestParameter <> null) {
                    if(!isHardPull)  body += '&enable-tags=' + Label.EnableTags ; //API-166 - only for post-brms
                    jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GET_CREDIT_REPORT_URL, body);
                 }
                 else    
                    jsonResponse = ProxyApiUtil.ApiGetResult(GET_CREDIT_REPORT_URL, body); 
            }
            system.debug('Json Response' + jsonResponse);

            if (jsonResponse != null) {
                creditReportEntity =
                    (ProxyApiCreditReportEntity)JSON.deserialize(jsonResponse, ProxyApiCreditReportEntity.class);
            }
        }

        return creditReportEntity;
    }
    

    public static ProxyApiCreditReportEntity getCreditReportById(Long creditReportId,String entityId) {
        ProxyApiCreditReportEntity creditReportEntity = null;
        //===SM-483==Start==
        Boolean UseOldEndPoint = string.isEmpty(entityId)? true :CreditReport.validateBrmsFlag(entityId);
        system.debug('==UseOldEndPoint =='+UseOldEndPoint );
        Boolean validatedAPI = false;
        
        if(!UseOldEndPoint)
            validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        else
            validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();
        //===SM-483==End==
        system.debug('==validatedAPI=='+validatedAPI);
        if (validatedAPI) {

            String jsonResponse = null;
            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeCreditReportDataWithApp(entityId);
            } else {
                //===SM-483==Start==
                if(!UseOldEndPoint) { jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GET_CREDIT_REPORT_BY_ID_URL + creditReportId);
                }else{ jsonResponse = ProxyApiUtil.ApiGetResult(GET_CREDIT_REPORT_BY_ID_URL + creditReportId);}
                    system.debug('---*****getSoftPullByCreditReportId response '+jsonResponse ); 
                //===SM-483==End==
            }

            if (jsonResponse != null) {
                creditReportEntity =
                    (ProxyApiCreditReportEntity)JSON.deserialize(jsonResponse, ProxyApiCreditReportEntity.class);
            }
        }

        return creditReportEntity;    
    }
    
    /**
     * Call the ProxyAPI to get a credit report by id
     * @param  creditReportId id of the credit report
     * @return creditReportEntity Returns a credit report entity
     */
    public static ProxyApiCreditReportEntity getCreditReportById(Long creditReportId) {
        return getCreditReportById(creditReportId, null);        
    }

    /**
     * Call the ProxyAPI to get the last hard credit pull by entity id
     * @param  entityId  The id of the entity to inserted (appId, contactId, leadId, accountId)
     * @return creditReportId  The id of the application to get the credit report
     */
    public static ProxyApiCreditReportEntity getHardPullByEntityId(String entityId) {
        system.debug('---*****getHardPullByEntityId IN' ); 
        ProxyApiCreditReportEntity creditReportEntity = null;

        //BRMS changes
        Boolean UseOldEndPoint = CreditReport.validateBrmsFlag(entityId);
        
        Boolean validatedAPI = false;
        
        if(!UseOldEndPoint)
            validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        else
            validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();
        //BRMS changes
        
        if (validatedAPI) {

            String jsonResponse = null;
            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeCreditReportData();
            } else {
                if(!UseOldEndPoint) 
                    jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GET_HARD_CREDIT_REPORT_BY_ENTITY_ID + entityId);
                else
                    jsonResponse = ProxyApiUtil.ApiGetResult(GET_HARD_CREDIT_REPORT_BY_ENTITY_ID + entityId);
                system.debug('---*****getHardPullByEntityId response '+jsonResponse ); 
            }
          
                if (jsonResponse != null) {
                creditReportEntity =
                    (ProxyApiCreditReportEntity)JSON.deserialize(jsonResponse, ProxyApiCreditReportEntity.class);
            }
        }
        system.debug('---*****getHardPullByEntityId creditReportEntity'+creditReportEntity ); 
        return creditReportEntity;
    }

    /**
    * Get credit reports by entity id
    * @param  entityId  The id of the entity to inserted (appId, contactId, leadId, accountId)
    * @return a List of creditReportEntity
    */
    public static List<ProxyApiCreditReportEntityLite> getAllCreditReportByEntityId(String entityId) {
        List<ProxyApiCreditReportEntityLite> result = null;
        
        //BRMS change
        Boolean useOldEndPoint = CreditReport.applicationExists(entityId) ? CreditReport.validateBrmsFlag(entityId) : false;

        Boolean validatedAPI = false;
        
        if(!useOldEndPoint) validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        else validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();
        //BRMS change
        
        if (validatedAPI) {
            
            String jsonResponse = null;
            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeCreditReportDataLite(entityId);
            } else {
                
                //BRMS changes
                if(!useOldEndPoint) jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GET_CREDIT_REPORTS_BY_ENTITY_URL + entityId);
                else jsonResponse = ProxyApiUtil.ApiGetResult(GET_CREDIT_REPORTS_BY_ENTITY_URL + entityId);
                //BRMS changes
            }
           
            system.debug('---getAllCreditReportByEntityId jsonResponse  '+jsonResponse ); 
            if (jsonResponse != null) {
                result =
                    (List<ProxyApiCreditReportEntityLite>)JSON.deserialize(jsonResponse, List<ProxyApiCreditReportEntityLite>.class);
            }
        }
        return result;
    }

    /**
     * Overrides all disqualifiers not approved
     * @param  userName The data needed in the request to get the credit report
     * @param  creditReportId  The id of the application to get the credit report
     */
    public static ProxyApiCreditReportEntity approvedCreditReportAttributes(String userName, Long creditReportId) {

        ProxyApiCreditReportEntity creditReportEntity = null;

        Boolean validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();

        if (validatedAPI) {

            String jsonResponse = null;

            if (Test.isRunningTest()) {
                //genesis__Applications__c application = LibraryTest.createApplicationTH();
                jsonResponse = LibraryTest.fakeCreditReportData();
            } else {
                String body = 'credit-report-id=' + creditReportId + '&user-name=' + EncodingUtil.urlEncode(userName, 'UTF-8');
                jsonResponse = ProxyApiUtil.ApiGetResult(APPROVED_CREDIT_REPORT_ATTRIBUTES_URL, body);
             system.debug('---***** '+body ); 
             system.debug('---***** '+jsonResponse ); 
            }

            if (jsonResponse != null) {
                creditReportEntity =
                    (ProxyApiCreditReportEntity)JSON.deserialize(jsonResponse, ProxyApiCreditReportEntity.class);
                    
                
            }
        }
        return creditReportEntity;
    }
    
    /** BRMS change
     * Overrides all disqualifiers not approved - new method for brms
     * @param  userName The data needed in the request to get the credit report
     * @param  creditReportId  The id of the application to get the credit report
     */
    public static ProxyApiCreditReportEntity approvedCreditReportAttributesNew(String userName, Long creditReportId) {

        ProxyApiCreditReportEntity creditReportEntity = null;

        Boolean validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();

        if (validatedAPI) {

            String jsonResponse = null;
            //fetch gds configuration settings
            
            GDS_Configuration__c setting = GDS_Configuration__c.getOrgDefaults();
            
            GDSRequestParameter gdsRequestParameter = new GDSRequestParameter ();
            gdsRequestParameter.apiSessionId = (setting != null) ? setting.SF_Token__c : 'token';
            gdsRequestParameter.organizationId = (setting != null) ? setting.Org_Id__c : '00DU0000000LpAk';
            gdsRequestParameter.organizationURL = (setting != null) ? setting.Org_Native_API_URL__c +'/'+setting.Org_Id__c: 'https://na48.salesforce.com/services/Soap/c/40.0/0DF0B0000001EuX/00DU0000000LpAk';

            String inputData = JSON.serialize(gdsRequestParameter);
            String body = 'credit-report-id=' + creditReportId + '&user-name=' + EncodingUtil.urlEncode(userName, 'UTF-8') + '&enable-tags=' + Label.EnableTags + '&input-data=' + EncodingUtil.urlEncode(inputData,'UTF-8'); //API-166  - only for post-brms


            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeCreditReportData();
            } else {
                jsonResponse = ProxyApiBRMSUtil.ApiGetResult(APPROVED_CREDIT_REPORT_ATTRIBUTES_URL, body);
                system.debug('Input Body ==>'+body+'-Testing approved attributes api jsonResponse ====>' +jsonResponse);
            }

            if (jsonResponse != null) {
                creditReportEntity =
                    (ProxyApiCreditReportEntity)JSON.deserialize(jsonResponse, ProxyApiCreditReportEntity.class);
                
            }
        }
        return creditReportEntity;
    }

    /**
     * Re Grade
     * @param  inputData The data needed in the request to re grade
     */
    public static ProxyApiCreditReportEntity reGrade(String inputData) {

        ProxyApiCreditReportEntity creditReportEntity = null;
        
        //BRMS change
        //get entity Id from input data
        String entityId;
        ScorecardInputData regradeInputParam =
            (ScorecardInputData) JSON.deserialize(inputData, ScorecardInputData.class);
        
        entityId = regradeInputParam.entityId;
        
        Boolean useOldEndPoint = CreditReport.validateBrmsFlag(entityId);
        
        Boolean validatedAPI = false;
        
        if(!useOldEndPoint)
            validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        else
            validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();
        //BRMS change
        
        if (validatedAPI) {
            String jsonResponse = null;

            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeCreditReportData();
            } else {
                String body = 'regrade-input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8');
                
                //BRMS change
                if(!useOldEndPoint) {
                    body += '&enable-tags=' + Label.EnableTags; //API-166 - only for post-brms
                    jsonResponse = ProxyApiBRMSUtil.ApiGetResult(RE_GRADE_URL, body);
                }
                else
                    jsonResponse = ProxyApiUtil.ApiGetResult(RE_GRADE_URL, body);
                //BRMS change
            }

            if (jsonResponse != null) {
                creditReportEntity =
                    (ProxyApiCreditReportEntity)JSON.deserialize(jsonResponse, ProxyApiCreditReportEntity.class);
            }
        }
        return creditReportEntity;
    }
    
    /**
     * Acceptance Model
     * @param  inputData Endpoint paramters as JSON string 
     */
    public static ProxyApiCreditReportEntity callGds(String inputData) {
        System.debug('inputData: ' + inputData);
        CreditReportInputData inputParam = (CreditReportInputData) JSON.deserialize(inputData, CreditReportInputData.class);
        Boolean useOldEndPoint = CreditReport.validateBrmsFlag(inputParam.decisioningParameter.entityId), validatedAPI = false;
        ProxyApiCreditReportEntity creditReportEntity = null;

        if(!useOldEndPoint) validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        else validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();
        
        if (validatedAPI) {
            String jsonResponse = null;
            if (Test.isRunningTest()) { jsonResponse = LibraryTest.fakeCreditReportData();
            } else { String body = 'input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8');
                body += '&is-hard-pull=false&actual-pull=false&entityId=' + inputParam.decisioningParameter.entityId;
                
                if(!useOldEndPoint) jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GDS_URL, body);
                else jsonResponse = ProxyApiUtil.ApiGetResult(GDS_URL, body);
            }
            System.debug('jsonResponse: ' + jsonResponse);
            if (jsonResponse != null) creditReportEntity =(ProxyApiCreditReportEntity)JSON.deserialize(jsonResponse, ProxyApiCreditReportEntity.class);
            
        }
        return creditReportEntity;
    }
}