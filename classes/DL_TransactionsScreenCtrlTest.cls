@isTest
private class DL_TransactionsScreenCtrlTest {

    @isTest static void loads_null_statement() {

        genesis__applications__c a = LibraryTest.createApplicationTH();

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        DL_TransactionsScreenCtrl ctrl  = new DL_TransactionsScreenCtrl(sc);

        System.assertEquals(null, ctrl.statement);
    }

    @isTest static void loads_statement() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        genesis__applications__c a = LibraryTest.createApplicationTH();

        Apexpages.currentpage().getparameters().put('contact-id', a.genesis__contact__c);
        Apexpages.currentpage().getparameters().put('application-id', a.id);
        Apexpages.currentpage().getparameters().put('account-number', '0000');
        Apexpages.currentpage().getparameters().put('type-code', '000');

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        DL_TransactionsScreenCtrl ctrl  = new DL_TransactionsScreenCtrl(sc);

        System.assertNotEquals(null, ctrl.statement);
    }

}