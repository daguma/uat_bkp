public with sharing class ATTUSEditLookupCtrlAccount 
{
public string resHTML {get;set;}
public string lookupby {get;set;}
private ATTUSWDSAccount__c tmpData;
//private Account acct;
private string prevstatus;
        
        public ATTUSEditLookupCtrlAccount(ApexPages.StandardController stdController) 
        {   
                ATTUSWatchDOGSFLookup lu = new ATTUSWatchDOGSFLookup(); 
                tmpData= (ATTUSWDSAccount__c) stdController.getRecord();
        
                lookupby = '';        
    
                resHTML = lu.getHTMLTable(tmpData.result_node__c);
                prevstatus = tmpdata.review_status__c;
                //acct = [select id from Account where id = :tmpData.Account__c];
                
                
        }
               
       public PageReference save() 
       {
           try
           {
            if(tmpdata.review_status__c == null)
            {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Review Status cannot be None.');
              ApexPages.addMessage(myMsg);
              return null;
            }
            if(prevstatus != tmpdata.review_status__c)
            {
                tmpdata.last_reviewed__c = userinfo.getname() + ' ' + datetime.now().formatlong();
            }
            
            update tmpData;
            PageReference pageRef = new PageReference('/' + tmpData.Account__c);  
            pageRef.setRedirect(true);
            return pageRef;  
           }
            catch (exception ex)
            {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,ex.getmessage());
              ApexPages.addMessage(myMsg);
              return null;
            }
       }
}