global class deleteContactScript {

     Webservice static string deleteContact(string inputParams){
      string cntId,status='ERR',message;
         Map<string,string> ReturnMap = new Map<string,string>();
          Boolean bIsSandbox = OfferCtrlUtil.runningInASandbox();
         try{
             Map<string, string> pInfoMap = (Map<String, String>) JSON.deserializeStrict(inputParams, Map<String, String>.class);
             system.debug('pInfoMap===='+pInfoMap );
             if((bIsSandbox || Test.isRunningTest()) && !pInfoMap.isEmpty())
                  if(pInfoMap.ContainsKey('cntId')) {cntId= string.IsNotEmpty(pInfoMap.get('cntId')) ? pInfoMap.get('cntId').trim() : '';}
             if(string.isNotEmpty(cntId)){
               // Delete test data script + cases start
                set<string> ContIdSet = new set<string>();
                set<string> AppSet = new set<string>();
                set<string> AccSet = new set<string>();
                set<string> OppSet = new set<string>();                
                ContIdSet.add(cntId);
                
                list<Contact> cont = [select id,AccountId from Contact where id IN: ContIdSet];
                if(cont.size()>0){
                    List<Logging__c> delLogsOfTestData = [Select id from Logging__c where (Contact__c IN: ContIdSet and Contact__c <> null)];
                    
                    List<Case> delCasesOfTestData = [Select id from Case where (ContactId IN: ContIdSet and ContactId <> null)];
                    if(cont.size() >0) AccSet.add(cont[0].AccountId);
                    
                    //get accounts and opportunities
                    list<Account> accs = new list<Account>();
                    accs = [select id from Account where id IN :AccSet];
                    list<Opportunity> opps = new list<Opportunity>();
                    list<genesis__applications__c> apps = new list<genesis__applications__c>();
                    opps = [select id, AccountId from Opportunity where (Contact__c IN :ContIdSet AND Contact__c<>null) or (AccountId IN :AccSet and AccountId <> null)];
                    apps = [select id, genesis__Account__c from genesis__applications__c where (genesis__Contact__c IN :ContIdSet AND genesis__Contact__c<>null) or (genesis__Account__c IN :AccSet and genesis__Account__c <> null)];
                    list<Scorecard__c> lstscoreCard = [select id from Scorecard__c where Contact__c IN :ContIdSet];
                    
                    for(Opportunity a: opps) OppSet.add(a.id);                    
                    for(genesis__applications__c a: apps)AppSet.add(a.id);
                    
                    if(!opps.IsEmpty()){
                        list<ints__Credit_Report__c> CreditReport = new list<ints__Credit_Report__c>();
                        list<Offer__c> offr = new list<Offer__c>();
                        list<ReGradeHistory__c> regrdHist= new list<ReGradeHistory__c>();
                        list<AccountManagementHistory__c> accountMaHist= new list<AccountManagementHistory__c>();
                         if(!apps.IsEmpty()){
                              CreditReport = [select id from ints__Credit_Report__c where (Opportunity__c=: OppSet and Opportunity__c <> null) or  (genesis__Applications__c =:AppSet and genesis__Applications__c <> null)];
                              offr = [select id from Offer__c where (Opportunity__c =:OppSet and Opportunity__c <> null) or (Application__c = :AppSet and     Application__c <> null)];
                              regrdHist = [select id from ReGradeHistory__c where (Opportunity__c =:OppSet and Opportunity__c <> null) or (Application__c =:AppSet and Application__c<> null) ]; 
                              accountMaHist = [select id from AccountManagementHistory__c where (Opportunity__c =:OppSet and Opportunity__c <> null) or (Application__c =:AppSet and Application__c<> null) ]; 
                          }
                         else{
                               CreditReport = [select id from ints__Credit_Report__c where (Opportunity__c=: OppSet and Opportunity__c <> null)];
                               offr = [select id from Offer__c where (Opportunity__c =:OppSet and Opportunity__c <> null)];
                              regrdHist = [select id from ReGradeHistory__c where (Opportunity__c =:OppSet and Opportunity__c <> null)];
                              accountMaHist = [select id from AccountManagementHistory__c where (Opportunity__c =:OppSet and Opportunity__c <> null)];
                          }
                         if(offr.size()>0) delete offr;
                         if(CreditReport.size()>0) delete CreditReport;
                         if(regrdHist.size()>0) delete regrdHist;
                         if(accountMaHist.size()>0) delete accountMaHist;
                    }
                    
                    //unique code hits
                    List<Unique_Code_Hit__c> uniqueCodeHits = [select id,Contact__c from Unique_Code_Hit__c where (Contact__c IN :ContIdSet and Contact__c <> null)];
                    delete uniqueCodeHits;
                    
                    delete delCasesOfTestData;
                    if(delLogsOfTestData.size()>0)delete delLogsOfTestData;
                    if(lstscoreCard.size()>0)delete lstscoreCard;
                    if(opps.size()>0) delete opps;
                    if(apps.size()>0) delete apps;
                    if(cont!=null)delete cont;
                    if(accs!=null)delete accs;
                    status = '200';
                    message = 'Contact deleted Successfully';
                    // Delete test data script end
                }else message = 'No record found for the given ContactID : ' + cntId;
             }else message = 'Contact Id is either Empty or you are hitting on Prod. Please check the details.';
             
           }
           Catch(Exception ex){
              message = ex.getMessage() + ' at Line no ' + ex.getLineNumber();
           }
           ReturnMap.put('status',status);
           ReturnMap.put('message',message);
           system.debug('==ReturnMap=='+ReturnMap);
           return JSON.serializePretty(ReturnMap);
     }
}