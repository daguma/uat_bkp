@isTest(SeeAllData=true)
public class TestShowAttachmentsCtrl{

  static testmethod void test(){
   //Dummy Contact
    contact cntct= new contact();
    cntct.Lastname='Test';
    cntct.Annual_Income__c =10000;
    insert cntct;
    
    //Dummy Application
    genesis__Applications__c aplcn= new genesis__Applications__c();
    aplcn.genesis__Contact__c= cntct.id;
    aplcn.ACH_Bank_Name__c='Dummy Bank';
    insert aplcn;

    Blob b;
    String b64;
    String strUrlUTF8;
    String strOriginal;
    
    strOriginal = 'test blob string';    
    strUrlUTF8 = EncodingUtil.urlEncode(strOriginal, 'UTF-8');    
    b = Blob.valueOf(strUrlUTF8);    
    b64 = EncodingUtil.base64Encode(b);    
    b = EncodingUtil.base64Decode(b64); 
       
    list<attachment> attList= new list<attachment>();    
      for(Integer i=0;i<=10;i++){
       attachment att = new attachment();
       att.Name       = 'Dummy Att..';
       att.Body       = b;
       att.parentId   = aplcn.Id;
       attList.add(att);
      }
     insert attList;
            
     string err = 'error';
     
     PageReference pageRef = Page.ShowAttachments;
     Test.setCurrentPage(pageRef);
     ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(attList.get(0));
     System.debug('attList.get(0).parentId>>>>>>>>'+attList.get(0).parentId);
     ApexPages.currentPage().getParameters().put('appId',attList.get(0).parentId);
    
     list<ShowAttachmentsCtrl.attachmentErrorcls> a = new list<ShowAttachmentsCtrl.attachmentErrorcls>();
     for(Attachment att:attList){
 //     a.add(new ShowAttachmentsCtrl.attachmentErrorcls(att,'--None--'));
      a.add(new ShowAttachmentsCtrl.attachmentErrorcls(att,err));
     }

     System.debug('attachmentErrorclsLIST>>>>>>>>>>>'+a);
           
     Test.startTest();
     ShowAttachmentsCtrl showAtt = new ShowAttachmentsCtrl();

     showAtt.getErrorTypes();
  
     showAtt.getattachmentErrorList();
     showAtt.save();
     Test.stopTest();

  }
}