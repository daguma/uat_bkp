public with sharing class CreditReportCtrl {

    @AuraEnabled
    public static List<CreditReportItem> getCreditReports(Id oppId) {

        List<CreditReportItem> myList = new List<CreditReportItem>();

        try{

            //ProxyApiCreditReportEntity pacre = CreditReportALPHA.getByEntityId(oppId);
            ProxyApiCreditReportEntity pacre = CreditReport.getByEntityId(oppId);

            if(pacre != null){
                myList.add(mapCRtoCreditReportItem(pacre));
            }else{
                myList.add(new CreditReportItem('-'));
            }

        }catch(Exception e){
            system.debug('CreditReportCtrl.getCreditReports exception: '
                    + e.getMessage()
                    + ' StackTrace '
                    + e.getStackTraceString());
            return new List<CreditReportItem>{
                    new CreditReportItem('-')
            };
        }

        return myList;
    }
    
    @AuraEnabled
    public static List<ProxyApiCreditReportEntityLite> getAllCreditReports(Id entityId) {

        List<ProxyApiCreditReportEntityLite> AllCRList = new List<ProxyApiCreditReportEntityLite>();

        try{

            AllCRList = CreditReport.getAllByEntityId(entityId);

        }catch(Exception e){
            System.debug('All Credit Report Error :- ' + e.getMessage());
        }

        return AllCRList;
    }

    public static CreditReportItem mapCRtoCreditReportItem(ProxyApiCreditReportEntity pacre){

        CreditReportItem creditReportItem;
        if(pacre != null){
            creditReportItem = new CreditReportItem();
            creditReportItem.creditReportName = pacre.name;
            creditReportItem.creditReportId = String.valueOf(pacre.id);
            creditReportItem.models = '';
            creditReportItem.errorResponse = pacre.errorMessage;
            creditReportItem.createdDate = String.valueOf( pacre.createdDate);

            if(pacre.brms != null) {

                creditReportItem.applicationRisk = getApplicationRisk(pacre.brms);
                creditReportItem.sequence = pacre.brms.sequenceId;
                creditReportItem.finalGrade = pacre.brms.finalGrade;
                creditReportItem.automatedDecision = pacre.brms.automatedDecision;
                creditReportItem.brmsVersionId = pacre.brms.ruleVersion;

                if(pacre.brms.scoringModels != null){

                    for(ProxyApiCreditReportScoringModelEntity scoreMod : pacre.brms.scoringModels) {

                        if(!String.isEmpty(scoreMod.modelName) && scoreMod.modelName.contains('ModelV2'))
                            creditReportItem.lastV2ModelGrade = scoreMod.grade;

                        if(!String.isEmpty(scoreMod.modelName) && scoreMod.modelName.contains('ModelV3'))
                            creditReportItem.lastV3ModelGrade = scoreMod.grade;
                    }
                }
            }
            return creditReportItem;
        }else{
            return new CreditReportItem('-');
        }
    }

    public static String getApplicationRisk(ProxyApiCreditBRMSResponse brms){

        String result = '0';

        if(brms != null && brms.rulesList != null) {

            for(ProxyApiCreditReportRuleEntity softPullRule : brms.rulesList) {

                //display only those rules which has deIsdisplayed as yes
                If( String.isNotBlank(softPullRule.deIsDisplayed)
                        && softPullRule.deIsDisplayed.equalsIgnoreCase('Yes') ){

                    if( !(String.isNotBlank(softPullRule.deRuleStatus)
                            && softPullRule.deRuleStatus.equalsIgnoreCase('Pass')) ) {
                        if(String.isNotBlank(brms.deApplicationRisk) )
                            result = brms.deApplicationRisk;
                    }
                }
            }
        }

        return result;

    }

    public class CreditReportItem {

        @AuraEnabled public string creditReportName;
        @AuraEnabled public string creditReportId;
        @AuraEnabled public string sequence;
        @AuraEnabled public string finalGrade;
        @AuraEnabled public string models;
        @AuraEnabled public string errorResponse;
        @AuraEnabled public string automatedDecision;
        @AuraEnabled public string createdDate;
        @AuraEnabled public string brmsVersionId;
        @AuraEnabled public string lastV2ModelGrade;
        @AuraEnabled public string lastV3ModelGrade;
        @AuraEnabled public string applicationRisk;

        public CreditReportItem(){}

        public CreditReportItem(String name){
            this.creditReportName = name;
        }
    }

}