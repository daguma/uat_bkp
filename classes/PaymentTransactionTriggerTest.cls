@isTest
private class PaymentTransactionTriggerTest {

    //To test the Process Builder and Flow for PaymentTransactionTrigger
    
    @isTest static void updates_reversal_reason() {

        loan__Loan_Account__c cl = TestHelper.createContract();
        cl.loan__Disbursal_Date__c = System.today();
        cl.loan__Loan_Status__c = 'Active - Good Standing';
        cl.loan__Pmt_Amt_Cur__c = 200;
        cl.Next_Payment_is_Additional_Payment__c = true;
        update cl;
        TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.createSeedDataForTesting();

        loan__Payment_Mode__c pMode = [select Id from loan__Payment_Mode__c where Name = 'ACH' order by CreatedDate desc limit 1];


        loan__Loan_Payment_Transaction__c transactionPay = new loan__Loan_Payment_Transaction__c();
        transactionPay.loan__Loan_Account__c = cl.id;
        transactionPay.loan__Transaction_Amount__c = 1000;
        transactionPay.loan__Cleared__c = true;
        transactionPay.FT_Payment_Reporting__c = false;
        transactionPay.loan__Payment_Mode__c = pMode.Id;
        insert transactionPay;

        System.assert([Select Id, Next_Payment_is_Additional_Payment__c from loan__Loan_Account__c LIMIT 1].Next_Payment_is_Additional_Payment__c == false, 'Additional Payment flag not cleared');
        
        cl.Next_Payment_is_Additional_Payment__c = [Select Id, Next_Payment_is_Additional_Payment__c from loan__Loan_Account__c LIMIT 1].Next_Payment_is_Additional_Payment__c ;
		cl.loan__Number_of_Days_Overdue__c = 30;
        cl.loan__Pmt_Amt_Cur__c = 200;
        update cl;
        
        transactionPay.Payment_Pending_End_Date__c = Date.today();
        update transactionPay;
        cl.Is_Payment_Pending__C = true;
        update cl;
		cl.loan__Number_of_Days_Overdue__c = 10;
        update cl;
        cl.loan__Number_of_Days_Overdue__c = 20;
        update cl;

        transactionPay.Payment_Pending_End_Date__c = null;
        update transactionPay;
        cl.loan__Number_of_Days_Overdue__c = 20;
        update cl;

        System.assert([Select Id from loan__Loan_Payment_Transaction__c].size() > 0);

        System.debug('Testing should have been succesful!');
  
        System.assert([Select Id, Next_Payment_is_Additional_Payment__c from loan__Loan_Account__c LIMIT 1].Next_Payment_is_Additional_Payment__c == false, 'Additional Payment flag not cleared');

    }
     

}