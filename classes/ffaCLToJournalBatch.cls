global class ffaCLToJournalBatch implements Database.Batchable<sObject>, Database.Stateful {
    global String query;
    global String clObjectName;
    global c2g__codaCompany__c userCompany = null; 
    global Id selectedBankAccount = null; 
    global String transactionType = '';
    global Set<Id> recordIds;  
    global Boolean batchOneComplete = false; 
        
    global ffaCLToJournalBatch(String inputTransactionType, Set<Id> selectedRecords, c2g__codaCompany__c inputCompany, Id selectedAccount, Boolean llcBatchComplete) {
        clObjectName = clObjectName; 
        transactionType = inputTransactionType; 
        recordIds = selectedRecords; 
        userCompany = inputCompany;
        selectedBankAccount = selectedAccount;
        batchOneComplete = llcBatchComplete;   
        
        if (transactionType == 'Initial Disbursement')
            query = 'SELECT Id, Name, loan__Disbursed_Amt__c, loan__Loan_Account__c, loan__Disbursal_Date__c, Disbursement_less_Fee__c, ' + 
                    'Fee__c, FFA_Integration_Complete__c FROM loan__Loan_Disbursal_Transaction__c WHERE FFA_Integration_Complete__c != true ' + 
                    'AND loan__Loan_Account__r.Company__c = \'' + userCompany.Id + '\'';
        
        else if (transactionType == 'Initial Disbursement Reversal')
            query = 'SELECT Id, Name, loan__Loan_Disbursal_Transaction__c,  loan__Loan_Disbursal_Transaction__r.loan__Disbursed_Amt__c, ' + 
                    'loan__Loan_Disbursal_Transaction__r.loan__Loan_Account__r.loan__Account__r.Name, loan__Adjustment_Txn_Date__c, ' + 
                    'loan__Loan_Disbursal_Transaction__r.Fee__c, loan__Loan_Disbursal_Transaction__r.Disbursement_less_Fee__c FROM ' + 
                    'loan__Disbursal_Adjustment__c WHERE FFA_Disbursement_Reversal_Complete__c != true AND ' + 
                    'loan__Loan_Disbursal_Transaction__r.loan__Loan_Account__r.Company__c = \'' + userCompany.Id + '\'';
        
        else if (transactionType == 'Payment')
            query = 'SELECT Id, Name, loan__Transaction_Amount__c, loan__Excess__c, loan__Fees__c, loan__Loan_Account__c, ' + 
                    'loan__Loan_Account__r.Company__r.Name, loan__Loan_Account__r.Company__c, loan__Principal__c, loan__Interest__c, ' + 
                    'loan__Transaction_Date__c, loan__Payment_Mode__r.name, loan__Early_Total_Repayment_of_the_Loan__c FROM ' + 
                    'loan__Loan_Payment_Transaction__c WHERE Payment_Processing_Complete__c != true AND loan__Write_Off_Recovery_Payment__c != true ' + 
                    'AND loan__Loan_Account__r.Company__c = \'' + userCompany.Id + '\'';
        
        else if (transactionType == 'Recovery Payment')
            query = 'SELECT Id, Name, loan__Excess__c, loan__Fees__c, loan__Transaction_Amount__c, loan__Loan_Account__r.Company__r.Name, ' + 
                    'loan__Loan_Account__c, loan__Principal__c, loan__Interest__c, loan__Transaction_Date__c, loan__Payment_Mode__r.name, ' + 
                    'loan__Early_Total_Repayment_of_the_Loan__c FROM loan__Loan_Payment_Transaction__c WHERE Recovery_Payment_Processing_Complete__c != ' + 
                    'true AND loan__Write_Off_Recovery_Payment__c = true AND loan__Loan_Account__r.Company__c = \'' + userCompany.Id + '\'';
        
        else if (transactionType == 'Payment Reversal')
            query = 'SELECT Id, Name, loan__Loan_Payment_Transaction__r.loan__Excess__c, loan__Loan_Payment_Transaction__r.loan__Fees__c, ' + 
                    'loan__Loan_Payment_Transaction__r.loan__Loan_Account__c, loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.Company__r.Name, ' + 
                    'loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.Company__c, loan__Loan_Payment_Transaction__r.loan__Transaction_Amount__c, ' + 
                    'loan__Loan_Payment_Transaction__r.loan__Principal__c, loan__Loan_Payment_Transaction__r.loan__Interest__c, loan__Adjustment_Txn_Date__c, ' + 
                    'FFA_Integration_Complete__c FROM loan__Repayment_Transaction_Adjustment__c WHERE FFA_Integration_Complete__c != true AND ' + 
                    'loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.Company__c = \'' + userCompany.Id + '\'';
        
        else if (transactionType == 'Charges')
            query = 'SELECT Id, Name, loan__Charge__r.loan__Fee__r.Revenue_GLA__c, loan__Charge__r.loan__Loan_Account__r.loan__Account__c, ' + 
                    'loan__Charge__r.loan__Loan_Account__r.loan__Account__r.Name, loan__Charge__r.loan__Loan_Account__r.Company__r.Name, ' + 
                    'loan__Transaction_Amount__c, loan__Transaction_Date__c FROM loan__Fee_Payment__c WHERE Fee_Payment_Processing_Complete__c != ' + 
                    'true AND loan__Charge__r.loan__Fee__r.Revenue_GLA__c != null AND loan__Charge__r.loan__Loan_Account__r.Company__r.id = \'' + userCompany.Id + '\'';
        
        else if (transactionType == 'Charge Off' )    
            query = 'SELECT Id, Company__c, Company__r.Name, loan__Principal_Remaining__c, loan__Charged_Off_Date__c, loan__Interest_Remaining__c, ' + 
                    'loan__Charged_Off_Principal__c, loan__Charged_Off_Interest__c, FFA_Integration_Charge_Off_Complete__c FROM loan__Loan_Account__c ' + 
                    'WHERE FFA_Integration_Charge_Off_Complete__c != true AND loan__Charged_Off_Date__c != null AND ' + 
                    'loan__Loan_Status__c = \'Closed- Written Off\' AND Company__c = \'' + userCompany.Id + '\'';
        
        else if (transactionType == 'Selling to SPE')
            query = 'SELECT Id, Name, loan__Principal_Remaining__c, loan__Interest_Remaining__c, Asset_Sale_Date__c, FFA_Integration_Sale_Complete__c ' + 
                    'FROM loan__Loan_Account__c WHERE FFA_Integration_Sale_Complete__c != true AND Asset_Sale_Date__c != null';  
        
        if (selectedRecords != null && selectedRecords.size() > 0) {
            String queryIdFilter = ''; 
            for (Id r: selectedRecords) {
                String idInQuotes = '\'' + r + '\''; 
                
                if (queryIdFilter != '')
                    queryIdFilter += ','; 
                
                queryIdFilter += idInQuotes; 
            }

            query += ' AND Id IN (' + queryIdFilter + ')'; 
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {   
        SavePoint sp = Database.setSavepoint();
        try {
            if (transactionType == 'Initial Disbursement') {
                List<loan__Loan_Disbursal_Transaction__c> disbursalList = new List<loan__Loan_Disbursal_Transaction__c>(); 
                for (sObject so: scope) {
                    loan__Loan_Disbursal_Transaction__c newInstance = (loan__Loan_Disbursal_Transaction__c)so; 
                    disbursalList.add(newInstance); 
                }
                ffaCLToJournalBatchHandler.processInitialDisbursements(disbursalList, transactionType, selectedBankAccount);
            }
            else if (transactionType == 'Initial Disbursement Reversal') {
                List<loan__Disbursal_Adjustment__c> disbursaladjList = new List<loan__Disbursal_Adjustment__c>(); 
                for (sObject so: scope) {
                    loan__Disbursal_Adjustment__c newInstance = (loan__Disbursal_Adjustment__c)so; 
                    disbursaladjList.add(newInstance); 
                }
                ffaCLToJournalBatchHandler.processInitialDisbursementsReversal(disbursaladjList, transactionType, selectedBankAccount);
            }
            else if(transactionType == 'Payment' || transactionType == 'Recovery Payment') {
                List<loan__Loan_Payment_Transaction__c> paymentAndRepaymentList = new List<loan__Loan_Payment_Transaction__c>();
                for (sObject so: scope) {
                    loan__Loan_Payment_Transaction__c newInstance = (loan__Loan_Payment_Transaction__c)so;
                    paymentAndRepaymentList.add(newInstance);
                }
                if (!batchOneComplete)
                    ffaCLToJournalBatchHandler.processPayments(paymentAndRepaymentList, transactionType, selectedBankAccount);   
                else if(paymentAndRepaymentList[0].loan__Loan_Account__r.Company__r.Name == 'Lendingpoint SPE LLC')
                    ffaCLToJournalBatchHandler.processSPEPayments_Step2(paymentAndRepaymentList, transactionType, selectedBankAccount);
            }
            else if(transactionType == 'Payment Reversal') {
                List<loan__Repayment_Transaction_Adjustment__c> repaymentAdjustmentList = new List<loan__Repayment_Transaction_Adjustment__c>();
                for (sObject so : scope) {
                    loan__Repayment_Transaction_Adjustment__c newInstance = (loan__Repayment_Transaction_Adjustment__c)so;
                    repaymentAdjustmentList.add(newInstance);
                }
                if (!batchOneComplete)
                    ffaCLToJournalBatchHandler.processPaymentReversal(repaymentAdjustmentList, transactionType, selectedBankAccount);
                else if(repaymentAdjustmentList[0].loan__Loan_Payment_Transaction__r.loan__Loan_Account__r.Company__r.Name == 'Lendingpoint SPE LLC')
                    ffaCLToJournalBatchHandler.processSPEPaymentReversal_Step2(repaymentAdjustmentList, transactionType, selectedBankAccount);
            }
            else if(transactionType == 'Charges') {
                List<loan__Fee_Payment__c> chargeList = new List<loan__Fee_Payment__c>();
                for (sObject so : scope) {
                    loan__Fee_Payment__c newInstance = (loan__Fee_Payment__c) so;
                    chargeList.add(newInstance);
                }
                ffaCLToJournalBatchHandler.processCharges(chargeList, transactionType, selectedBankAccount);
            }
            else if(transactionType == 'Charge Off') {
                List<loan__Loan_Account__c> listChargeOff  = new List<loan__Loan_Account__c>();
                for(sObject so : scope) {
                    loan__Loan_Account__c newInstance = (loan__Loan_Account__c)so;
                    listChargeOff.add(newInstance);
                }
                if (!batchOneComplete)
                    ffaCLToJournalBatchHandler.processChargeOffs(listChargeOff, transactionType);
                else if(listChargeOff[0].Company__r.Name == 'Lendingpoint SPE LLC')
                    ffaCLToJournalBatchHandler.processChargeOffsSPE(listChargeOff, transactionType);   
            }
            else if(transactionType == 'Selling to SPE') {
                List<loan__Loan_Account__c> listSPE  = new List<loan__Loan_Account__c>();
                for(sObject so : scope) {
                    loan__Loan_Account__c newInstance = (loan__Loan_Account__c)so;
                    listSPE.add(newInstance);
                }
                if (!batchOneComplete)
                    ffaCLToJournalBatchHandler.processSellToSPELLC(listSPE, transactionType);
                else 
                    ffaCLToJournalBatchHandler.processSellToSPESPE(listSPE, transactionType);   
            }
        }
        catch (Exception e) {
            Database.rollback(sp); 
            Error_Log__c errLog = new Error_Log__c(
                Error_Date_Time__c = Datetime.now(),
                Error_Message__c = e.getStackTraceString() + ' | '+ e.getMessage(),
                Transaction_Type__c = transactionType);
        }      
    }

    global void finish(Database.BatchableContext BC) {
        if(!Test.isRunningTest()) {
            if (transactionType == 'Selling to SPE' && !batchOneComplete) {
                ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, recordIds, userCompany, selectedBankAccount, true); 
                Database.executeBatch(batchClass, 1);    
            }

            if(transactionType == 'Charge Off' && !batchOneComplete) {   
                ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, recordIds, userCompany, selectedBankAccount, true);
                Database.executeBatch(batchClass, 1);
            }
            
            if(transactionType == 'Payment' && !batchOneComplete) {
                ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, recordIds, userCompany, selectedBankAccount, true);
                Database.executeBatch(batchClass, 1);
            }
            
            if(transactionType == 'Payment Reversal' && !batchOneComplete) {
                ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, recordIds, userCompany, selectedBankAccount, true);
                Database.executeBatch(batchClass, 1);
            }
        }
    }
}