public class DocusignEnvelopeCreateResponse {

    public String envelopeId;
    public String uri;
    public String statusDateTime;
    public String status;
    
    public static DocusignEnvelopeCreateResponse parse(String json) {
        return (DocusignEnvelopeCreateResponse) System.JSON.deserialize(json, DocusignEnvelopeCreateResponse.class);
    }
}