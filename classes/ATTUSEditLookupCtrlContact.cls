public with sharing class ATTUSEditLookupCtrlContact 
{
        //private Contact cont;
        public string resHTML {get;set;}
        ATTUSWDSFLookup__c tmpData;
        private string prevstatus;
        
        public ATTUSEditLookupCtrlContact(ApexPages.StandardController stdController) 
        {   
                ATTUSWatchDOGSFLookup lu = new ATTUSWatchDOGSFLookup(); 
                tmpData= (ATTUSWDSFLookup__c) stdController.getRecord();
                //cont = [select id from Contact where id = :tmpData.Contact__c];
                prevstatus = tmpdata.review_status__c; 
                resHTML = lu.getHTMLTable(tmpData.result_node__c);
        }  
               
       public PageReference save() 
       {
           try
           {
            if(tmpdata.review_status__c == null)
            {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Review Status cannot be None.');
              ApexPages.addMessage(myMsg);
              return null;
            }
            if(prevstatus != tmpdata.review_status__c)
            {
                tmpdata.last_reviewed__c = userinfo.getname() + ' ' + datetime.now().formatlong();
            }

            update tmpData;
            PageReference pageRef = new PageReference('/' + tmpData.Contact__c);  
            pageRef.setRedirect(true);
            return pageRef;  
           }
            catch (exception ex)
            {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,ex.getmessage());
              ApexPages.addMessage(myMsg);
              return null;
            }
       }
}