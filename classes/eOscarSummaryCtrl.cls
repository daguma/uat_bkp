public with sharing class eOscarSummaryCtrl {
    
    @AuraEnabled public loan__Loan_Account__c acc {get; set;}
    @AuraEnabled public String firstName {get; set;}
    @AuraEnabled public String birthdate {get; set;}
    @AuraEnabled public String fundedDate {get; set;}
    @AuraEnabled public String loanClosedDate {get; set;}
    @AuraEnabled public String firstDelinquencyDate {get; set;}
    @AuraEnabled public String currentBalanceAmount {get; set;}
    @AuraEnabled public String lastPaymentDate {get; set;}
    @AuraEnabled public String originalLoanAmount {get; set;}
    @AuraEnabled public String chargedOffPrincipal {get; set;}
    @AuraEnabled public String scheduledMonthlyPayment {get; set;}
    @AuraEnabled public String actualPaymentAmount {get; set;}
    @AuraEnabled public String originalTermMonths {get; set;}
    @AuraEnabled public String paymentFrequency {get; set;}
    @AuraEnabled public String message {get; set;}
    @AuraEnabled public Boolean isSuccess {get; set;}
    
    @AuraEnabled
    public static eOscarSummaryCtrl geteOscarSummary(String contactId) {
        String appId = '';
        eOscarSummaryCtrl eOscarEntity = new eOscarSummaryCtrl();
        eOscarEntity.isSuccess = true;
        eOscarEntity.message = '';
        
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
		String currentProflieName = PROFILE[0].Name;

		if(currentProflieName == 'System Administrator' || 
           currentProflieName == 'Legal' || currentProflieName == 'QTM') {
               try {
                Contact con = [SELECT Id, Birthdate FROM Contact WHERE Id =: contactId];
                
                eOscarEntity.birthdate = con.Birthdate.format(); 
                
                for ( loan__Loan_Account__c acc : [SELECT Id, Opportunity__c ,loan__Closed_Date__c, loan__Metro2_First_Delinquency_Date__c, loan__Last_Payment_Date__c, 
                                                   Original_Loan_Amount_No_Fee__c, loan__Charged_Off_Principal__c, loan__Payment_Amount__c,
                                                   Original_Term_Months__c
                                                   FROM loan__Loan_Account__c
                                                   WHERE  loan__Contact__c =: contactId
                                                   ORDER BY CreatedDate DESC
                                                   LIMIT 1]) {
                                                       
                                                       eOscarEntity.lastPaymentDate = acc.loan__Last_Payment_Date__c != null? acc.loan__Last_Payment_Date__c.format() : ''; 
                                                       eOscarEntity.loanClosedDate = acc.loan__Closed_Date__c != null? acc.loan__Closed_Date__c.format() : ''; 
                                                       eOscarEntity.firstDelinquencyDate = acc.loan__Metro2_First_Delinquency_Date__c != null? acc.loan__Metro2_First_Delinquency_Date__c.format() : '';
                                                       
                                                       eOscarEntity.originalLoanAmount = acc.Original_Loan_Amount_No_Fee__c != null? String.valueOf(acc.Original_Loan_Amount_No_Fee__c) : '';
                                                       eOscarEntity.chargedOffPrincipal = acc.loan__Charged_Off_Principal__c != null? String.valueOf(acc.loan__Charged_Off_Principal__c) : '';
                                                       eOscarEntity.actualPaymentAmount = acc.loan__Payment_Amount__c != null? String.valueOf(acc.loan__Payment_Amount__c) : '';
                                                       eOscarEntity.originalTermMonths = acc.Original_Term_Months__c != null? String.valueOf(acc.Original_Term_Months__c) : '';
                                                       appId = acc.Opportunity__c ;
                                                   }    
                
                
                if (String.isNotEmpty(appId)) {
                    for ( Opportunity app : [SELECT Id, Funded_Date__c, Current_Balance_Amount__c, Payment_Frequency_Masked__c
                                                          FROM Opportunity
                                                          WHERE Id =: appId
                                                          LIMIT 1]) {
                                                              
                                                              eOscarEntity.fundedDate = app.Funded_Date__c != null? app.Funded_Date__c.format() : '';
                                                              eOscarEntity.currentBalanceAmount = app.Current_Balance_Amount__c != null? String.valueOf(app.Current_Balance_Amount__c) : '';
                                                              eOscarEntity.paymentFrequency = app.Payment_Frequency_Masked__c != null? app.Payment_Frequency_Masked__c : '';
                                                              
                                                          }  
                }
        	}catch(Exception e) {
            	eOscarEntity.isSuccess = false;
        		eOscarEntity.message = e.getMessage();
        	}
		}else {
        	eOscarEntity.isSuccess = false;
        	eOscarEntity.message = 'You do not have permissions to see this data.';
		}
        return eOscarEntity;
    }
}