@isTest
 public Class TestCommissionsTierStructureExt{           
     TestMethod static void TestNewCommissionStrAndClone(){
         Commissions_Structure__c cs = new Commissions_Structure__c();
         CommissionsTierStructureExt testCommStr = new CommissionsTierStructureExt(new ApexPages.StandardController(cs));
         testCommStr.cs.Name = 'Test cs';
         testCommStr.cs.Commission_Structure_Status__c = 'Active';
         testCommStr.cs.Commission_Structure_Start_Date__c = system.today()+1;
         testCommStr.cs.Commission_Structure_End_Date__c = system.today()-1;
         testCommStr.cs.Commission_Payment_Frequency__c = 'Monthly';
         testCommStr.cs.Commission_Scheme_Indicator__c = 'Percentage';
         testCommStr.cs.Commission_Scheme_Value__c = -200;
         testCommStr.cs.Commission_Cost_Base__c = 'Cost per Click';
         testCommStr.cs.Non_Split__c = true;
         testCommStr.clearSplitCommIndicator();
         testCommStr.cs.Commission_Tier_Indicator__c = true;
         testCommStr.saveCommStr();
         testCommStr.cs.Commission_Structure_End_Date__c = system.today()+30;
         testCommStr.saveCommStr();
         testCommStr.cancelAndRedirectToListView();
         testCommStr.cs.Commission_Tier_Indicator__c = false;
         testCommStr.cancelAndRedirectToListView();
         testCommStr.cs.Commission_Scheme_Value__c = 200;
         testCommStr.saveCommStr();
         testCommStr.cs.Commission_Scheme_Value__c = 100;
         ApexPages.currentPage().getParameters().put('clone','1');
         testCommStr.cloneCommStr();
         testCommStr.saveCommStr();
         testCommStr.cs.Name = 'Cloned cs';
         testCommStr.saveCommStr();
     }
     
     TestMethod static void TestNewCommissionStrWithSplit(){
         Account parentAct = new Account(Name = 'Test Parent',Is_Parent_Partner__c = true,Type = CommissionsTierStructureExt.actType);
         insert parentAct;
         Account childAct = new Account(Name = 'Test Child',Is_Child_Partner__c = true,Type = CommissionsTierStructureExt.actType,Account__c = parentAct.Id);
         insert childAct;
         Commissions_Structure__c cs = new Commissions_Structure__c();
         CommissionsTierStructureExt testCommStr = new CommissionsTierStructureExt(new ApexPages.StandardController(cs));
         testCommStr.cs.Name = 'Test cs';
         testCommStr.cs.Commission_Structure_Status__c = 'Active';
         testCommStr.cs.Commission_Structure_Start_Date__c = system.today()+1;
         testCommStr.cs.Commission_Structure_End_Date__c = system.today()+30;
         testCommStr.cs.Commission_Payment_Frequency__c = 'Monthly';
         testCommStr.cs.Commission_Scheme_Indicator__c = 'Dollar Amount';
         testCommStr.cs.Commission_Scheme_Value__c = 200;
         testCommStr.cs.Commission_Cost_Base__c = 'Cost per Click';
         testCommStr.cs.Non_Split__c = false;
         testCommStr.clearSplitCommIndicator();
         testCommStr.selectedParentName = CommissionsTierStructureExt.nullStr;
         testCommStr.saveCommStr();
         testCommStr.getParentPartners();
         testCommStr.selectedParentName = parentAct.Id;
         testCommStr.getChildPartners();
         testCommStr.selectedChildName = CommissionsTierStructureExt.nullStr;
         testCommStr.saveCommStr();
         testCommStr.selectedChildName = 'Test Child';
         testCommStr.cs.Parent_Partner_Commission_Value__c = -50;
         testCommStr.saveCommStr();
         testCommStr.cs.Parent_Partner_Commission_Value__c = 50;
         testCommStr.cs.Child_Partner_Commission_Value__c = -50;
         testCommStr.saveCommStr();
         testCommStr.cs.Child_Partner_Commission_Value__c = 40;
         testCommStr.saveCommStr();
         testCommStr.cs.Child_Partner_Commission_Value__c = 50;
         testCommStr.cs.Commission_Tier_Indicator__c = false;
         testCommStr.saveCommStr();
     }
     
     TestMethod static void TestNewCommissionStrWithTierAndClone(){
         Commissions_Structure__c cs = new Commissions_Structure__c();
         CommissionsTierStructureExt testCommStr = new CommissionsTierStructureExt(new ApexPages.StandardController(cs));
         testCommStr.cs.Name = 'Test cs';
         testCommStr.cs.Commission_Structure_Status__c = 'Active';
         testCommStr.cs.Commission_Structure_Start_Date__c = system.today()+1;
         testCommStr.cs.Commission_Structure_End_Date__c = system.today()+30;
         testCommStr.cs.Commission_Payment_Frequency__c = 'Monthly';
         testCommStr.cs.Commission_Scheme_Indicator__c = 'Percentage';
         testCommStr.cs.Commission_Scheme_Value__c = 200;
         testCommStr.cs.Commission_Cost_Base__c = 'Cost per Funding';
         testCommStr.cs.Non_Split__c = true;
         testCommStr.clearSplitCommIndicator();
         testCommStr.cs.Commission_Tier_Indicator__c = true;
         testCommStr.AddRow();
         testCommStr.tiersList.clear();
         testCommStr.saveCommStr();
         Tier_Structure__c tier1 = new Tier_Structure__c(Name = 'Tier 1',Commission_Scheme_Value__c = -10,Minimum_Range_Value__c= 11.00,Maximum_Range_Value__c= 10.00);
         Tier_Structure__c tier2 = new Tier_Structure__c(Name = 'Tier 1',Commission_Scheme_Value__c = 5,Minimum_Range_Value__c= 1.00,Maximum_Range_Value__c= 10.00);
         testCommStr.tiersList.add(tier1);
         testCommStr.tiersList.add(tier2);
         testCommStr.saveCommStr();
         tier1.Commission_Scheme_Value__c = 110;
         testCommStr.saveCommStr();
         tier1.Commission_Scheme_Value__c = 10;
         testCommStr.saveCommStr();
         tier1.Minimum_Range_Value__c = 1.05;
         testCommStr.saveCommStr();
         tier2.Minimum_Range_Value__c = 9.78;
         tier2.Maximum_Range_Value__c = 20.67;
         testCommStr.saveCommStr();
         testCommStr.saveCommStr();
         tier2.Name = 'Tier 2';
         testCommStr.saveCommStr();
         tier2.Minimum_Range_Value__c = 11.22;
         testCommStr.saveCommStr();
         tier2.Commission_Scheme_Value__c = 20;
         testCommStr.saveCommStr();
     }
     
     TestMethod static void TestExistingCommissionStr(){
         Account parentAct = new Account(Name = 'Test Parent',Is_Parent_Partner__c = true,Type = CommissionsTierStructureExt.actType);
         insert parentAct;
         Account childAct = new Account(Name = 'Test Child',Is_Child_Partner__c = true,Type = CommissionsTierStructureExt.actType,Account__c = parentAct.Id);
         insert childAct;
         Commissions_Structure__c cs = new Commissions_Structure__c();
         cs.Name = 'Test cs';
         cs.Commission_Structure_Status__c = 'Active';
         cs.Commission_Structure_Start_Date__c = system.today()+1;
         cs.Commission_Structure_End_Date__c = system.today()+30;
         cs.Commission_Payment_Frequency__c = 'Monthly';
         cs.Commission_Scheme_Indicator__c = 'Dollar Amount';
         cs.Commission_Scheme_Value__c = 200;
         cs.Commission_Cost_Base__c = 'Cost per Click';
         cs.Non_Split__c = false;
         cs.Parent_Partner__c = parentAct.Id;
         cs.Child_Partner__c = 'Test Child';
         cs.Parent_Partner_Commission_Value__c = 50;
         cs.Child_Partner_Commission_Value__c = 50;
         cs.Commission_Tier_Indicator__c = true;
         insert cs;
         Tier_Structure__c tier1 = new Tier_Structure__c(Name = 'Tier 1',Commission_Scheme_Value__c = 10,Minimum_Range_Value__c= 1.00,Maximum_Range_Value__c= 10.00,Commissions_Structure__c = cs.Id);
         Tier_Structure__c tier2 = new Tier_Structure__c(Name = 'Tier 2',Commission_Scheme_Value__c = 20,Minimum_Range_Value__c= 11.00,Maximum_Range_Value__c= 20.00,Commissions_Structure__c = cs.Id);
         insert tier1;
         insert tier2;
         CommissionsTierStructureExt testCommStr = new CommissionsTierStructureExt(new ApexPages.StandardController(cs));
         ApexPages.currentPage().getParameters().put('clone','1');
         testCommStr.cloneCommStr();
         testCommStr.saveCommStr();
         Apexpages.currentpage().getparameters().put('tierId',tier2.Id);
         //testCommStr.delRow();
     }
     
     TestMethod static void TestupdateCommStr(){
         Account parentAct = new Account(Name = 'Test Parent',Is_Parent_Partner__c = true,Type = CommissionsTierStructureExt.actType);
         insert parentAct;
         Account childAct = new Account(Name = 'Test Child',Is_Child_Partner__c = true,Type = CommissionsTierStructureExt.actType,Account__c = parentAct.Id);
         insert childAct;
         Commissions_Structure__c cs = new Commissions_Structure__c();
         cs.Name = 'Test cs';
         cs.Commission_Structure_Status__c = 'Active';
         cs.Commission_Structure_Start_Date__c = system.today()+1;
         cs.Commission_Structure_End_Date__c = system.today()+30;
         cs.Commission_Payment_Frequency__c = 'Monthly';
         cs.Commission_Scheme_Indicator__c = 'Dollar Amount';
         cs.Commission_Scheme_Value__c = 200;
         cs.Commission_Cost_Base__c = 'Cost per Click';
         cs.Non_Split__c = false;
         cs.Parent_Partner__c = parentAct.Id;
         cs.Child_Partner__c = 'Test Child';
         cs.Parent_Partner_Commission_Value__c = 50;
         cs.Child_Partner_Commission_Value__c = 50;
         cs.Commission_Tier_Indicator__c = true;
         insert cs;
         Tier_Structure__c tier1 = new Tier_Structure__c(Name = 'Tier 1',Commission_Scheme_Value__c = 10,Minimum_Range_Value__c= 1.00,Maximum_Range_Value__c= 10.00,Commissions_Structure__c = cs.Id);
         Tier_Structure__c tier2 = new Tier_Structure__c(Name = 'Tier 2',Commission_Scheme_Value__c = 20,Minimum_Range_Value__c= 11.00,Maximum_Range_Value__c= 20.00,Commissions_Structure__c = cs.Id);
         insert tier1;
         insert tier2;
         CommissionsTierStructureExt testCommStr = new CommissionsTierStructureExt(new ApexPages.StandardController(cs));
         ApexPages.currentPage().getParameters().put('selectedParentName','Test Parent');
         ApexPages.currentPage().getParameters().put('selectedChildName','Test Child');
         testCommStr.updateCommStr();
         
         ApexPages.currentPage().getParameters().put('index','1');
         ApexPages.currentPage().getParameters().put('tierId',tier1.Id);
         testCommStr.delRow();
         
     }
 }