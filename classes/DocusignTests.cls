@IsTest
public class DocusignTests {

    static testMethod void TestQueue() {
        Settings();
        Account acc = SetupAccount();
        DocusignProcessor proc = new DocusignProcessor(acc.Id);
        
        Test.startTest();
        System.enqueueJob(proc);
        Test.stopTest();  
    }

    static testMethod void TestRequestBuilder() {
        Settings();
        Account acc = SetupAccount();
        acc.Merchant_Agreement_Envelope_Id__c = EnvelopeId();
        acc.Cobranding_Name__c='test';
		acc.type = 'Merchant';
        update acc;
        
        /*List<Contact> authSigners = [SELECT Title, Account.Phone, FirstName, LastName, Email, Account.dba_doing_business_as__c, Account.Bank_Account_Type__c, Account.BillingStreet, Account.BillingCity, Account.BillingState,
                    Account.BillingPostalCode, Account.financial_institution_name__c, Account.name_on_account__c, Account.transit_aba_number_9digits__c, Account.financial_institution_account_number__c
                    FROM Contact WHERE AccountId = :accountId AND IsBusinessOwner__c = 'Yes']; */
        
        //DocusignProcessor.SubmitContractRequest(acc.Id);
        
        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueOf(mockResponse());
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Docusign'; 
        req.httpMethod = 'POST';
    
        RestContext.request = req;
        RestContext.response= res;
    
        Test.startTest();
        DocusignREST.doPost();
        Test.stopTest();
    }
    
    private static String mockResponse() {
        return '<?xml version="1.0" encoding="utf-8"?>'+
'<DocuSignEnvelopeInformation'+
'    xmlns:xsd="http://www.w3.org/2001/XMLSchema"'+
'    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'+
'    xmlns="http://www.docusign.net/API/3.0">'+
'    <EnvelopeStatus>'+
'        <RecipientStatuses><RecipientStatus><Type>Signer</Type><Email>michelle@loanhero.com</Email><UserName>Michelle Baumann</UserName><RoutingOrder>1</RoutingOrder><Sent>2018-01-26T14:17:05.637</Sent><Delivered>2018-01-26T14:18:30.993</Delivered><Signed>2018-01-26T14:18:38.34</Signed><DeclineReason xsi:nil="true" /><Status>Completed</Status><RecipientIPAddress>54.215.250.49</RecipientIPAddress><RecipientAuthenticationStatus><IDQuestionsResult><Status>Passed</Status><EventTimestamp>2018-01-26T22:18:23.557Z</EventTimestamp></IDQuestionsResult><IDLookupResult><Status>Passed</Status><EventTimestamp>2018-01-26T22:18:23.557Z</EventTimestamp></IDLookupResult></RecipientAuthenticationStatus><CustomFields /><TabStatuses><TabStatus><TabType>SignHere</TabType><Status>Signed</Status><XPosition>241</XPosition><YPosition>114</YPosition><TabLabel>Signature 1_SignHere_965c299a-f996-494d-84c1-230d38597205</TabLabel><TabName /><DocumentID>1</DocumentID><PageNumber>1</PageNumber></TabStatus></TabStatuses><AccountStatus>Active</AccountStatus><RecipientId>965c299a-f996-494d-84c1-230d38597205</RecipientId></RecipientStatus><RecipientStatus><Type>Signer</Type><Email>michbaumann@me.com</Email><UserName>Michelle Baumann</UserName><RoutingOrder>1</RoutingOrder><Sent>2018-01-26T14:17:05.34</Sent><Delivered>2018-01-26T14:21:53.74</Delivered><Signed>2018-01-26T14:22:06.68</Signed><DeclineReason xsi:nil="true" /><Status>Completed</Status><RecipientIPAddress>54.215.250.49</RecipientIPAddress><RecipientAuthenticationStatus><IDQuestionsResult><Status>Passed</Status><EventTimestamp>2018-01-26T22:21:45.397Z</EventTimestamp></IDQuestionsResult><IDLookupResult><Status>Passed</Status><EventTimestamp>2018-01-26T22:21:45.397Z</EventTimestamp></IDLookupResult></RecipientAuthenticationStatus><CustomFields /><TabStatuses><TabStatus><TabType>SignHere</TabType><Status>Signed</Status><XPosition>876</XPosition><YPosition>200</YPosition><TabLabel>Signature 1_SignHere_b6126e77-6aff-40a3-b613-2db8ca08ae00</TabLabel><TabName /><DocumentID>1</DocumentID><PageNumber>1</PageNumber></TabStatus></TabStatuses><AccountStatus>Active</AccountStatus><RecipientId>b6126e77-6aff-40a3-b613-2db8ca08ae00</RecipientId></RecipientStatus></RecipientStatuses><TimeGenerated>2018-01-26T14:22:14.5053434</TimeGenerated><EnvelopeID>test</EnvelopeID><Subject>Please sign</Subject><UserName>Scott Douglas</UserName><Email>scott.douglas1989@gmail.com</Email><Status>Completed</Status><Created>2018-01-26T14:17:04.637</Created><Sent>2018-01-26T14:17:05.73</Sent><Delivered>2018-01-26T14:21:53.93</Delivered><Signed>2018-01-26T14:22:07.15</Signed><Completed>2018-01-26T14:22:07.15</Completed><ACStatus>Original</ACStatus><ACStatusDate>2018-01-26T14:17:04.637</ACStatusDate><ACHolder>Scott Douglas</ACHolder><ACHolderEmail>scott.douglas1989@gmail.com</ACHolderEmail><ACHolderLocation>DocuSign</ACHolderLocation><SigningLocation>Online</SigningLocation><SenderIPAddress>136.147.62.8 </SenderIPAddress><EnvelopePDFHash /><CustomFields><CustomField><Name>AccountId</Name><Show>false</Show><Required>false</Required><Value>14601664</Value><CustomFieldType>Text</CustomFieldType></CustomField><CustomField><Name>AccountName</Name><Show>false</Show><Required>false</Required><Value>LoanHero</Value><CustomFieldType>Text</CustomFieldType></CustomField><CustomField><Name>AccountSite</Name><Show>false</Show><Required>false</Required><Value>na3</Value><CustomFieldType>Text</CustomFieldType></CustomField></CustomFields><AutoNavigation>true</AutoNavigation><EnvelopeIdStamping>true</EnvelopeIdStamping><AuthoritativeCopy>false</AuthoritativeCopy><DocumentStatuses><DocumentStatus><ID>1</ID><Name>pdf-sample.pdf</Name><TemplateName>BlankTest</TemplateName><Sequence>1</Sequence></DocumentStatus></DocumentStatuses></EnvelopeStatus>'+
'</DocuSignEnvelopeInformation>';
    }
    
    private static Account SetupAccount() {
        Recordtype LMS = [select id from Recordtype where name like 'LMS%' AND sObjectType='Account' limit 1];
       
        Account retVal = new Account(Channel__c='Direct',Name = 'Text', /*Merchant_Agreement_Envelope_Id__c = EnvelopeId(),*/ recordtypeid = LMS.id,Onboarding_Stage__c ='Fail');
        insert retVal;
        
        retVal.Onboarding_Stage__c = 'Needs Attention';
        update retVal;
        
        retVal.Onboarding_Stage__c = 'Fail';
        update retVal;
        
        insert new Contact(AccountId = retVal.Id, LastName = 'Test', FirstName = 'Test', IsBusinessOwner__c = 'Yes');
        
        return retVal;
    }
    
    private static String EnvelopeId() {
        return 'test';
    }
    
    private static void Settings() {
        insert new Docusign_Integration_Settings__c(CreateEnvelope_Url__c = 'Value', Email_Blurb__c = 'https://www.google.com', Email_Subject__c = 'Value', IntegratorKey__c = 'Value', MultiOwnerTemplateId__c = 'Value', Password__c = 'Value', SingleOwnerTemplateId__c = 'Value', Trigger_Status__c = 'Fail', Username__c = 'Under Review', Webhook_URL__c = 'Review Complete');
    } 
}