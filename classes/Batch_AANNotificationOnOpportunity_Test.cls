/*********************************************************************************
*
*
*
*
*
*
**********************************************************************************/
@isTest
public class Batch_AANNotificationOnOpportunity_Test {
    Public static testMethod void testMethodOne() 
    {
        List<contact> lstcontact= new List<contact>();
        contact c = new contact();
        c.lastname ='Test Contact';
        c.FirstName = 'test First';
        c.MailingState = 'CA';
        lstcontact.add(c);
        insert lstcontact;
        
        insert new FinwiseDetails__c(Whitelisted_States__c = 'CA');
        
        Account a = new Account();
        a.name = 'Test Account';
        a.Cobranding_Name__c = 'test';
        insert a;
        
        List<opportunity> lstopty= new List<opportunity>();
        opportunity o = new opportunity();

        o.Contact__c = lstcontact[0].id;
        o.Lead_Sub_Source__c = 'LPCustomerPortal';
        o.Status_Modified_Date__c =date.today().addDays(-20);
        o.type ='Refinance';
        o.Status__c ='Refinance Unqualified';
        o.LeadSource__c ='Organic';
        o.IsTestBucket__c =false;
        o.CloseDate = date.today();
        o.Name = 'Testing Oppty';
        o.stagename = 'Qualification';
        o.Partner_Account__c = a.id;
        lstopty.add(o);
        
        insert lstopty;
        
        
        Attachment Att = new Attachment();
        Att.name = 'system_credit_data_management_HP - Do Not Open';
        Att.body = Blob.valueof('Test Record please ignore');
        Att.ParentId = lstopty[0].id;
        
        insert Att;
        
        
        Test.startTest();

        Batch_AANNotificationOnOpportunity aanbatch = new Batch_AANNotificationOnOpportunity();
        DataBase.executeBatch(aanbatch); 
            
        Test.stopTest();
    }


}