@isTest 
public class ProxyApiCreditReportClassProbTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ProxyApiCreditReportClassProbabilities cp = new ProxyApiCreditReportClassProbabilities ();
        Test.stopTest();

        System.assertEquals(null, cp.innerValue);
    }
    
     static testmethod void validate_assign() {
        Test.startTest();
        ProxyApiCreditReportClassProbabilities  cp = new ProxyApiCreditReportClassProbabilities ();
        Test.stopTest();
        cp.innerValue = 'Test value';
        System.assertEquals('Test value', cp.innerValue);
     }
}