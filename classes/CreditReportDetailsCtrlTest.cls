@isTest public class CreditReportDetailsCtrlTest {
    
    @isTest static void getCreditReportDetailTest() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        insert ProxyApiUtil.settings;
        Opportunity app = LibraryTest.createApplicationTH();
        
        CreditReportDetailsCtrl.getCreditReportDetail(app.Id,'1',true);
        CreditReportDetailsCtrl.getCreditReportDetail(app.Contact__c,'1',true);
		CreditReportDetailsCtrl.CreditReportDetailEntity crDetailEntity = new CreditReportDetailsCtrl.CreditReportDetailEntity();
        crDetailEntity.setErrorMessage('Test');
 		System.assert(crDetailEntity.crLiabilities == null, 'Libs not null');       
 		System.assert(crDetailEntity.crInquiries == null, 'Inq not null');       
 		System.assert(crDetailEntity.crSummaries == null, 'Summ not null');       
    }
    
   
}