public class EmailCaseReplyCtrl {
	public Case casevar{get;set;}
	public String csId{get;set;}
	public String replyText{get;set;}
	public String  response;
	public boolean notAllow{get;set;}
    public EmailCaseReplyCtrl(){
        casevar = new Case();
        String csId = ApexPages.currentPage().getParameters().get('csId');
        System.debug('casevar..>>> '+csId);
        if(csId!=null){
            casevar = [SELECT Id, Subject, contactEmail, Description, LastModifiedDate, ContactId FROM Case where Id=:csId];
			if(casevar.Description != null)
			replyText = ' \r\n\r\n\r\n' +'--------------------------------------------------\r\n'+casevar.lastmodifiedDate+'\r\n\r\n'+casevar.Description; 
        }
    //notAllow = true;
    }
	public pagereference reply(){
    
		try{
		  
		  
		  if(casevar.Id <> null && !String.isEmpty(replyText)){
			//cs.Description = replyText.replaceAll('-', '');
			String res = sendEmail(replyText,casevar);
			  replyText = 'Reply: ' + replyText + '\n' +
                    		  'Replied At: ' + casevar.LastModifiedDate + '\n' + 
                			  '-------------------------------------------\n';
                
                if(String.isNotBlank(casevar.Description)) 
                    casevar.Description += '\n' + replyText;
                else
                    casevar.Description = replyText;
                //Update Case
                update casevar;
                
		  } 
		}catch(Exception e ){
				System.debug('Exception================ '+ e.getMessage());
			}  
		PageReference pageRef = new PageReference('/' + casevar.Id);
		pageRef.setRedirect(true);
		//return pageRef;
		return null;
	}
	
    @AuraEnabled
    public static String submitReply(String recordId, String description) {
		String responseMsg = '';        
        try {            
            if(String.isNotBlank(recordId) && String.isNotBlank(description)) {                
                Case cs = [SELECT Id, Subject, contactEmail, Description, LastModifiedDate, ContactId FROM Case WHERE Id =: recordId];
                
                //Proxy API HIT
                String response = sendEmail(description, cs);
                
                description = 'Reply: ' + description + '\n' +
                    		  'Replied At: ' + cs.LastModifiedDate + '\n' + 
                			  '-------------------------------------------\n';
                
                if(String.isNotBlank(cs.Description)) 
                    cs.Description += '\n' + description;
                else
                    cs.Description = description;
                //Update Case
                update cs;
                
                responseMsg = 'SUCCESS : Reply sent successfully.';
            }else {
                responseMsg = 'ERROR : Record or Description is blank';
            } 
        }catch(Exception e ){
            responseMsg = 'ERROR : ' + e.getMessage();
        }
        return responseMsg;
    }
    
    public static String sendEmail(String emailBody, Case cs) {   
        String response = '';
        try{            
            string caseReplyEndpoint, accessToken ;
            Java_API_Settings__c javaDetails = Java_API_Settings__c.getInstance('CaseReplySettings');
            
            caseReplyEndpoint = javaDetails.User_Create_Endpoint__c;
            //userCreateEndPoint = '';
            accessToken = EmailUtility.hitAccessTokenAPI(javaDetails); 
			System.debug('accessToken 1 ======================= >>> '+accessToken);
            System.debug('caseReplyEndpoint======================= '+caseReplyEndpoint); 
            if(accessToken <> null){
                System.debug('accessToken======================= '+accessToken);
                
                response = hitCaseReplyAPI(accessToken, emailBody, caseReplyEndpoint, cs); 
            } 
            
        }catch (Exception e){
            System.debug(e.getMessage());
        }
        return response;
    }
    
    public static String hitCaseReplyAPI(String accessToken, String emailBody, String endPoint, Case cs){
        String responseBody;
        try{
            Map<string,string> requestMap = new Map<string,string>();
            String jsonRequest;
            OrgWideEmailAddress[] owea = [select Id,Address from OrgWideEmailAddress where Address = 'noreply@lendingpoint.com'];
            
            if(owea.size() > 0) 
                requestMap.put('fromAddress',owea.get(0).Address);                
            
            requestMap.put('subject',cs.subject);
            requestMap.put('toAddress',cs.contactEmail);
            requestMap.put('emailBody',emailBody);
            requestMap.put('communicationDate',String.valueOf(System.today()));
            requestMap.put('contactId',cs.contactId);
            //requestMap.put('contactId','0030B000022hniu');
            jsonRequest = Json.SerializePretty(requestMap);
            System.debug('jsonRequest..>>  '+jsonRequest);
            
            
            HttpRequest req = new HttpRequest(); 
            req.setEndPoint(EndPoint);
            system.debug('===========accessToken=========='+accessToken);        
            req.setHeader('Authorization', 'Bearer '+accessToken);
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept', 'application/json');
            req.setHeader('Origin',Label.Origin_Java);
            req.setMethod('POST');   
            req.setTimeout(12000);
            req.setBody(jsonRequest);
            
            System.debug('Request ==============='+req);
            System.debug('Request ==============='+req.getBody());
            Http sendHttp = new Http();
            HTTPResponse res;                
            
            if(!Test.isRunningTest()){
                res =  sendHttp.send(req);
                
            }else{
                res = new HTTPResponse();                    
                res.SetBody(TestHelper.createDomDocNew().toXmlString());               
            }       
            responseBody = res.getBody();
            System.debug('Response======================= '+res.getBody());                       
            integer statusCode = res.getstatusCode();
            
            System.debug('statusCode======================= '+statusCode);
        }catch (Exception e){
            System.debug(e.getMessage());
            return e.getMessage();
        }
        return responseBody;
    }
    
}