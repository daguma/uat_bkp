global class EmpInfoRequestParameter{
   public string source_id{get;set;}
   public string verification_type{get;set;}
   public string permissible_purpose{get;set;}
   public string fill_strategy{get;set;}
   public string consent_date{get;set;}
   public employeeDetails employee;
   public employerDetails employer;
   
   public class employeeDetails{
   
       public string first_name{get;set;}
       public string last_name{get;set;}
       public string ssn{get;set;}
       public string phone{get;set;}
       public string email{get;set;}
       public addressDetails address;
   }
   public class addressDetails{
       public string street1{get;set;}
       public string city{get;set;}
       public string state{get;set;}
       public string zip{get;set;} 
   }
   public class employerDetails{
   
       public string name{get;set;}
       public string phone{get;set;}
       public string email{get;set;}
   }
    
}