@isTest public class DecisioningTest {

    @isTest static void sets_app_status_for_restricted_partner() {

        Account partnerAct = new Account(name = 'test', Cobranding_Name__c = 'Test', Contact_Restricted_Partner__c = true, Type = 'Partner', Is_Parent_Partner__c = true);
        insert partnerAct;

        Opportunity app = LibraryTest.createApplicationTH();
        app.Partner_Account__c = partnerAct.Id;
        update app;

        Scorecard__c score = new Scorecard__c();
        score.Opportunity__c = app.id;
        score.Acceptance__c = 'Y';
        score.Grade__c = 'B1';
        insert score;

        //Decision Yes with Scorecard and Partner Restricted
        Decisioning.applicationStatusAutomation(app.Id, 'Yes');

        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals('Credit Approved - No Contact Allowed', app.Status__c);
    }

    @isTest static void sets_app_status_declined_with_automated_yes() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact cntct = [Select Id, Name from Contact where Id = : app.Contact__c];

        cntct.Annual_Income__c = 100;
        cntct.Employment_Type__c = 'Self Employed';
        cntct.Loan_Amount__c = 30000;
        update cntct;

        app.Amount = 30000;
        update app;
        
        Scorecard__c score = new Scorecard__c();
        score.Opportunity__c = app.id;
        score.Acceptance__c = 'N';
        score.Grade__c = 'F';
        insert score;

        Decisioning.applicationStatusAutomation(app.id, 'Yes');

        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c 
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.Status__c);
        System.assertEquals('Updated Scoring – No Offer', app.Reason_of_Opportunity_Status__c);
    }

    @isTest static void sets_app_status_with_automated_decision_yes() {

        Opportunity app = LibraryTest.createApplicationTH();

        Scorecard__c score = new Scorecard__c();
        score.Opportunity__c = app.id;
        score.Acceptance__c = 'Y';
        score.Grade__c = 'B1';

        insert score;

        //Decision Yes with Scorecard
        Decisioning.applicationStatusAutomation(app.Id, 'Yes');

        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals('Credit Qualified', app.Status__c);
    }

    @isTest static void sets_app_status_with_automated_decision_no() {
        Opportunity app = LibraryTest.createApplicationTH();

        //Decision No
        Decisioning.applicationStatusAutomation(app.Id, 'No');

        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.Status__c);
    }

    @isTest static void sets_app_status_with_automated_decision_review() {
        Opportunity app = LibraryTest.createApplicationTH();

        //Decision Review
        Decisioning.applicationStatusAutomation(app.Id, 'Review');

        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals('Credit Review', app.Status__c);
    }

    @isTest static void status_automation_invalid_data() {

        Opportunity app = LibraryTest.createApplicationTH();
		String status = app.Status__c;

        Decisioning.applicationStatusAutomation(app.Contact__c, '');
        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals(status, app.Status__c);
    }

    @isTest static void declines_app_with_reason() {

        Opportunity app = LibraryTest.createApplicationTH();

        app.status__c = 'Credit Review';
        update app;

        Decisioning.declineApplication(app.Id, 'Did not pass Disqualifiers');

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.status__c);
        System.assertEquals('Did not pass disqualifiers', app.Reason_of_Opportunity_Status__c);
    }

    @isTest static void declines_app_exception() {

        Opportunity app = LibraryTest.createApplicationTH();

        Integer countLog = [SELECT COUNT() FROM Logging__c];

        Decisioning.declineApplication(app.Id, 'Exception Decline Note');

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        //System.assert([SELECT COUNT() FROM Logging__c] > countLog, 'Catch Exception');
    }

    @isTest static void declines_app_by_hard_pull() {

        Opportunity app = LibraryTest.createApplicationTH();

        app.status__c = 'Credit Review';
        update app;

        Decisioning.declineByHardSecondLook(app);

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.status__c);
        System.assertEquals('Did not pass disqualifiers at Hard Pull', app.Reason_of_Opportunity_Status__c);
    }

    @isTest static void declines_app_by_risk_band() {
        Opportunity app = LibraryTest.createApplicationTH();
        Opportunity cnt = [Select Contact__c,Contact__r.email FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];
        Email_Age_Details__c detail = new Email_Age_Details__c(Contact__c=cnt.Contact__c,email__c=EncodingUtil.urlDecode(cnt.Contact__r.email,'UTF-8'),Email_Age_Status__c=false,errorCode__c='0');
        insert detail ;
        Email_Age_Results__c result = new Email_Age_Results__c(EARiskBandID__c='ABC',Email_Age_Details__c=detail.id);insert result; 
        TestHelper.createEmailAgeSettings();

        EmailAgeStorageController.saveEmailAgeDetails(TestHelper.sendEmailAgeMockFailResponse(app.id));

        Blob response = Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(LibraryTest.fakeCreditReportFailData(app.id))));

        LibraryTest.createAttachment(app.id, response);

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getByEntityId(app.id);

        System.assertNotEquals(creditReportEntity, null);

        Decisioning.declineByRiskBand(app.Id, creditReportEntity);

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.status__c);
        System.assertEquals('Cannot Verify Information Provided', app.Reason_of_Opportunity_Status__c);
    }

    @isTest static void declines_app_by_fico_score_decrease() {

        Opportunity app = LibraryTest.createApplicationTH();

        Scorecard__c score = new Scorecard__c();
        score.Opportunity__c = app.id;
        score.Acceptance__c = 'Y';
        score.Grade__c = 'B1';

        insert score;

        Offer__c off = new Offer__c();
        off.Opportunity__c = app.id;
        off.Fee__c = 500;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        insert off;

        Decisioning.declineByFicoScoreDecrease(app.id);

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.status__c);
        System.assertEquals('FICO Score Decrease', app.Reason_of_Opportunity_Status__c);
        System.assertEquals(0, [SELECT COUNT() FROM Offer__c WHERE Opportunity__c = : app.id]);
    }

    @isTest static void checks_if_fico_score_decrease() {

        ProxyApiCreditReportEntity soft = LibraryTest.fakeCreditReportEntity();
        ProxyApiCreditReportEntity hard = LibraryTest.fakeCreditReportEntity();
        FICO_Drop_Rules__c obj1 = new FICO_Drop_Rules__c(Name = 'FICO_Between_600_And_619', Drop_Points__c = 5, Max_Limit__c = 619, Min_Limit__c = 600);
        FICO_Drop_Rules__c obj2 = new FICO_Drop_Rules__c(Name = 'FICO_Greater_Than_700', Drop_Points__c = 11, Max_Limit__c = 740, Min_Limit__c = 700);
        insert obj1; insert obj2;

        Boolean decrease = Decisioning.ficoScoreDecrease(soft, hard);

        System.assertEquals(false, decrease);
    }

    @isTest static void application_status_automation_thru_brms() {

        Opportunity app = LibraryTest.createApplicationTH();

        String response = LibraryTest.fakeBrmsRulesResponse(app.id, true, '1', true);
        ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(response, ProxyApiCreditReportEntity.class);

        app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntity, app);
        update app;

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Credit Qualified', app.status__c);
        System.assertEquals(null, app.Reason_of_Opportunity_Status__c);
    }

    @isTest static void application_status_automation_thru_brms_app_null() {

        Opportunity app = LibraryTest.createApplicationTH();

        String response = LibraryTest.fakeBrmsRulesResponse(app.id, true, '1', true);
        ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(response, ProxyApiCreditReportEntity.class);

        app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntity, null);
        update app;

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Credit Qualified', app.status__c);
        System.assertEquals(null, app.Reason_of_Opportunity_Status__c);
    }

    @isTest static void application_status_automation_thru_brms_fail() {
        LIST<Opportunity_Users__c> oppSettingList = [SELECT Id, User_Ids__c FROM Opportunity_Users__c];

        Opportunity_Users__c oppSetting;

        if (oppSettingList.size() > 0) {
            oppSetting = oppSettingList.get(0);
            oppSetting.User_Ids__c = UserInfo.getUserId();
            update oppSetting;
        } else {
            oppSetting = new Opportunity_Users__c();
            oppSetting.User_Ids__c = UserInfo.getUserId();
            insert oppSetting;
        }

        Opportunity app = LibraryTest.createApplicationTH();

        String response = LibraryTest.fakeBrmsFailRulesResponse(app.id, true, '1', false);
        ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(response, ProxyApiCreditReportEntity.class);

        app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntity, app);
        update app;

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.status__c);
        System.assertEquals('Did not pass disqualifiers', app.Reason_of_Opportunity_Status__c);

        //fail for hard pull
        String responseHP = LibraryTest.fakeBrmsFailRulesResponse(app.id, false, '3', false);
        ProxyApiCreditReportEntity creditReportEntityHP = (ProxyApiCreditReportEntity) JSON.deserializeStrict(responseHP, ProxyApiCreditReportEntity.class);

        app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntityHP, app);
        update app;

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.status__c);
        System.assertEquals('Did not pass disqualifiers at Hard Pull', app.Reason_of_Opportunity_Status__c);
    }

    @isTest static void application_status_automation_thru_brms_fail_without_reason_code() {

        Opportunity app = LibraryTest.createApplicationTH();
        //Adding this to pass the test (probably a bug)
        app.Decline_Note__c = 'Test';
        update app;

        String response = ' {"errorMessage": "test", "datasourceName": "ExpConsUSV7", "errorDetails": "test", ' +
                          ' "entityName": "application", "entityId": " ' + app.Id + ' ", "isSoftPull": true, "errorResponse": true, "decision" : true, ' +
                          ' "automatedDecision" : "Yes", "attributesList":[], "segmentsList": [], ' +
                          ' "createdDate": null, "id": 1, "reportText": "test", "requestData": "test", "transactionId": "test", "offerCatalogList": [], ' +
                          ' "scorecardEntity" : null, "brms" : {"deBRMS_ContractStatus": "Declined (or Unqualified)","sessionId": "djsakdfidsfkd38243","sequenceId": "1","productId": "LOAN", ' +
                          ' "channelId": "Online Aggregator","executionType": "","ruleVersion": "AB-F9-19-54-C1-D6-7D-72","deApplicationRisk": "0", ' +
                          ' "rulesList": [{"deRuleNumber": "16","deRuleName": "EmailAge_Risk_Band","deIsDisplayed": "Yes","rulePriority": "518","deRuleValue": "-99","deRuleStatus": "Fail", ' +
                          ' "deSequenceCategory": "5","deRuleCategory": "Higher Risk","deActionInstructions": "No Action","deHumanReadableDescription": "test data","deBRMS_ReasonCode": ""}, ' +
                          ' {"deRuleNumber": "1","deRuleName": "DTI","deIsDisplayed": "Yes","rulePriority": "995","deRuleValue": "-99","deRuleStatus": "Pass", ' +
                          ' "deSequenceCategory": "4","deRuleCategory": "FIN","deActionInstructions": "No Action","deHumanReadableDescription": "test data","deBRMS_ReasonCode": ""}], ' +
                          ' "scoringModels": [{},{}],"scorecardAcceptance": "Yes","emailAgeResponse": {"query": {"queryType": "EmailAgeVerification", ' +
                          ' "created": "2017-05-10T05:04:42Z","lang": "en-US","email": "sample101%40sample.com%2b","responseCount": 0,"count": 1}, ' +
                          ' "responseStatus": {"description": "Authentication Error: The signature doesnt match or the user/consumer key file wasnt found.", ' +
                          ' "errorCode": "3001","status": "failed"}}} } ';
        ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(response, ProxyApiCreditReportEntity.class);

        app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntity, app);
        update app;

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Declined (or Unqualified)', app.status__c);
    }

    @isTest static void application_status_automation_thru_brms_review() {

        LIST<Opportunity_Users__c> oppSettingList = [SELECT Id, User_Ids__c FROM Opportunity_Users__c];

        Opportunity_Users__c oppSetting;

        if (oppSettingList.size() > 0) {
            oppSetting = oppSettingList.get(0);
            oppSetting.User_Ids__c = UserInfo.getUserId();
            update oppSetting;
        } else {
            oppSetting = new Opportunity_Users__c();
            oppSetting.User_Ids__c = UserInfo.getUserId();
            insert oppSetting;
        }

        Opportunity app = LibraryTest.createApplicationTH();

        String response = LibraryTest.fakeBrmsReviewRulesResponse(app.id, true, '1', true);
        ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(response, ProxyApiCreditReportEntity.class);

        app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntity, app);
        update app;

        app = [SELECT id, status__c, Reason_of_Opportunity_Status__c, Review_Reason__c
               FROM Opportunity
               WHERE id = : app.id
                          LIMIT 1];

        System.assertEquals('Credit Review', app.status__c);
        System.assertEquals(null, app.Reason_of_Opportunity_Status__c);
        System.assertEquals('High Income', app.Review_Reason__c);
    }

    @isTest static void application_status_fail_thru_brms_partner() {

        LIST<Opportunity_Users__c> oppSettingList = [SELECT Id, User_Ids__c FROM Opportunity_Users__c];

        Opportunity_Users__c oppSetting;

        if (oppSettingList.size() > 0) {
            oppSetting = oppSettingList.get(0);
            oppSetting.User_Ids__c = UserInfo.getUserId();
            update oppSetting;
        } else {
            oppSetting = new Opportunity_Users__c();
            oppSetting.User_Ids__c = UserInfo.getUserId();
            insert oppSetting;
        }

        Account partnerAct = new Account(name = 'test', Cobranding_Name__c = 'Test',  Contact_Restricted_Partner__c = true, Type = 'Partner', Is_Parent_Partner__c = true);
        insert partnerAct;

        Opportunity app = LibraryTest.createApplicationTH();
        app.Partner_Account__c = partnerAct.Id;
        update app;

        String response = LibraryTest.fakeBrmsRulesResponse(app.id, true, '1', true);
        ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(response, ProxyApiCreditReportEntity.class);

        app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntity, app);
        update app;

        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals('Credit Qualified', app.Status__c);
    }

    @isTest static void application_status_fail_thru_brms_partner_owner() {

        Account partnerAct = new Account(name = 'test',Cobranding_Name__c = 'Test', Contact_Restricted_Partner__c = true, Type = 'Partner', Is_Parent_Partner__c = true);
        insert partnerAct;

        Opportunity app = LibraryTest.createApplicationTH();
        app.Partner_Account__c = partnerAct.Id;
        app.OwnerId = [SELECT Id FROM User WHERE Name = 'HAL' LIMIT 1].Id;
        update app;

        String response = LibraryTest.fakeBrmsRulesResponse(app.id, true, '1', true);
        ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(response, ProxyApiCreditReportEntity.class);

        app = [SELECT  Id,  Status__c,  Reason_of_Opportunity_Status__c, 
               Partner_Account__c, Selected_Offer_Id__c, Lending_Product__c, Lending_Product__r.Name,
               Contact__c, Contact__r.Product__c, OwnerId, Partner_Account__r.Contact_Restricted_Partner__c
               FROM Opportunity
               WHERE Id = : app.Id LIMIT 1];

        app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntity, app);
        update app;

        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        //System.assertEquals('Credit Approved - No Contact Allowed', app.Status__c);
    }

    @isTest static void evalutate_clarity_test() {

        Opportunity app = LibraryTest.createApplicationTH();
        ProxyApiClarityInquiryEntity clarityInquiryEntity = new ProxyApiClarityInquiryEntity();

        String jsonResponse = LibraryTest.fakeFailProxyApiClarityInquiryEntity();
        clarityInquiryEntity = (ProxyApiClarityInquiryEntity) JSON.deserializeStrict(jsonResponse, ProxyApiClarityInquiryEntity.class);

        Decisioning.EvalutateClarity(null, clarityInquiryEntity, app);

        app = [Select Id,
               Status__c,
               Reason_of_Opportunity_Status__c, 
               Clarity_Status__c,
               Clarity_Fraud_Score__c,
               Clarity_Fraud_Crosstab_Multiple__c,
               Clarity_Fraud_Reason_Code__c,
               Clarity_Submitted__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals(clarityInquiryEntity.isApproved, app.Clarity_Status__c);
        System.assertEquals(clarityInquiryEntity.fraudScore, app.Clarity_Fraud_Score__c);
        System.assertEquals(clarityInquiryEntity.fraudCrosstabMultiple, app.Clarity_Fraud_Crosstab_Multiple__c);
        System.assertEquals(clarityInquiryEntity.reasonCodeDescription, app.Clarity_Fraud_Reason_Code__c);
        System.assertEquals(true, app.Clarity_Submitted__c);
    }

    @isTest static void evalutate_clarity_test_fail() {

        Opportunity app = LibraryTest.createApplicationTH();
        ProxyApiClarityInquiryEntity clarityInquiryEntity = new ProxyApiClarityInquiryEntity();

        String jsonResponse = LibraryTest.fakeProxyApiClarityInquiryEntity();
        clarityInquiryEntity = (ProxyApiClarityInquiryEntity) JSON.deserializeStrict(jsonResponse, ProxyApiClarityInquiryEntity.class);

        Decisioning.EvalutateClarity(null, clarityInquiryEntity, app);

        app = [Select Id,
               Status__c,
               Clarity_Status__c,
               Clarity_Fraud_Score__c,
               Clarity_Fraud_Crosstab_Multiple__c,
               Clarity_Fraud_Reason_Code__c,
               Reason_of_Opportunity_Status__c, 
               Clarity_Submitted__c
               from Opportunity
               where Id = : app.Id LIMIT 1];

        System.assertEquals(clarityInquiryEntity.isApproved, app.Clarity_Status__c);
        System.assertEquals(clarityInquiryEntity.fraudScore, app.Clarity_Fraud_Score__c);
        System.assertEquals(clarityInquiryEntity.fraudCrosstabMultiple, app.Clarity_Fraud_Crosstab_Multiple__c);
        System.assertEquals(clarityInquiryEntity.reasonCodeDescription, app.Clarity_Fraud_Reason_Code__c);
        System.assertEquals(true, app.Clarity_Submitted__c);
    }
    
    @isTest static void evalutate_clarity_history_pass() {
		Decisioning.isClarityClearRecentHistoryPassed(new List<ClarityClearRecentHistory>());
    }
}