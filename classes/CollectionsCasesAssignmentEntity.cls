public with sharing class CollectionsCasesAssignmentEntity {

   public Id contractId, caseId;
   public Id collectionsCaseOwnerId, caseOwnerId, paymentPlanOwnerId, otACHOwnerId;
   public Integer dpd; 
   public String assignedScenario = '', assignedScenarioDescription = '', caseStatus, lastPaymentReversalReason, loadBalanceGroup = '';
   public Boolean hasActivePaymentPlan, hasPendingPayment, isDMC, /*isAlreadyDMCAssigned,*/ isLMS, missedFirstPayment, hasOtACH, hasCase;
   public Boolean updateCase = false, createCase = false, isLastPaymentReturned = false/*, needsLoadBalancing = false*/;
   public Id caseOwnerIdNew = null; 

   public CollectionsCasesAssignmentEntity(loan__Loan_Account__c contract, Case theCase, String message){
       if(contract == null){
           assignedScenario = 'Contract is null: ' + message;
       }else {
           contractId = contract.Id;
           if(theCase != null){
                caseId = theCase.Id;
                caseOwnerId = theCase.Owner.Id;
                caseStatus = theCase.Status;
            }
            assignedScenario = message;
       }
       assignedScenario = assignedScenario.left(255);
   } 
   
   public CollectionsCasesAssignmentEntity(loan__Loan_Account__c contract, Case theCase, Id paymentPlanOwnerId, Id otAchOwnerId, String lastPaymentTxnReversalReason){

       if(contract != null){
            contractId = contract.Id;
            collectionsCaseOwnerId = contract.Collection_Case_Owner__c;
            dpd = Integer.valueOf(contract.Overdue_Days__c);
            hasActivePaymentPlan = contract.loan_Active_Payment_Plan__c;
            hasPendingPayment = contract.Is_Payment_Pending__c;
            missedFirstPayment = contract.First_Payment_Missed__c;
            isDMC = !String.isEmpty(contract.DMC__c); 
            //isAlreadyDMCAssigned = false;
            isLMS = 'Point Of Need'.equals(contract.Product_Name__c);

            if(theCase != null){
                caseId = theCase.Id;
                caseOwnerId = theCase.Owner.Id;
                caseStatus = theCase.Status;
            }
            hasCase = theCase != null;

            this.paymentPlanOwnerId = paymentPlanOwnerId;

            hasOtACH = otAchOwnerId != null;
            this.otACHOwnerId = otAchOwnerId;

            this.lastPaymentReversalReason = lastPaymentTxnReversalReason;
            if(!String.isEmpty(this.lastPaymentReversalReason)){
                if( this.lastPaymentReversalReason.contains('R01') ||
                    this.lastPaymentReversalReason.contains('R02') ||
                    this.lastPaymentReversalReason.contains('R03') ||
                    this.lastPaymentReversalReason.contains('R08') ||
                    this.lastPaymentReversalReason.contains('R16') ){
                        isLastPaymentReturned = true;
                }
            }            
       }
   }    
}