public class updateEmailInCPDatabaseUtility{

    static integer errorCount = 0;
    
      /**This Method is called by Contact Trigger once email of any Contact is changed.**/            
     @future(callout=true)
     Public static void tokenAndEmailUpdateMethod(string tokenParams){
        Map<string,string> paramMap = (Map<String,String>) JSON.deserialize(tokenParams, Map<String,String>.class);
        
        string conId = paramMap.get('contactId'); 
        string email = paramMap.get('email'); 
        
        try{ 
            Java_API_Settings__c javaDetails = Java_API_Settings__c.getInstance('JavaSettings');
            string emailUpdateEndPoint = javaDetails.Email_Update_Endpoint__c;                                 
                
            string accessToken = EmailUtility.hitAccessTokenAPI(javaDetails);
            
            if(accessToken <> null)
            hitEmailUpdateAPI(accessToken,conId,email,emailUpdateEndPoint);
                 
        }catch(Exception e){
            WebToSFDC.notifyDev('Update Email to CustomerPortal Error', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() ) ;
        }
        
     }
     
       /**This method will update this email in Customer Portal DB if same User exists there**/
  //   @TestVisible
     public static void  hitEmailUpdateAPI(String accessToken,string contactId,string email,string endPoint){
          try{  
               Map<string,string> requestMap = new Map<string,string>();
               requestMap.put('contactId',contactId);
               requestMap.put('email',email);             
               string jsonRequest = Json.SerializePretty(requestMap);
           
                HttpRequest req = new HttpRequest(); 
                req.setEndPoint(EndPoint); 
                
                system.debug('===========accessToken=========='+accessToken);        
                req.setHeader('Authorization', 'Bearer '+accessToken);
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('Accept', 'application/json');
                req.setHeader('Origin', Label.Origin_Java);
                req.setMethod('POST');            
                req.setBody(jsonRequest);
                
                System.debug('Request ==============='+req);
                Http sendHttp = new Http();
                HTTPResponse res;
               if(!Test.isRunningTest()){
                    res = sendHttp.send(req);
                }else{
                    res = new HTTPResponse();                    
                    res .SetBody(TestHelper.createDomDocNew().toXmlString());               
                }
                System.debug('res======================= '+res);  
                System.debug('Response======================= '+res.getBody());                       
                integer statusCode = res.getstatusCode();
                 System.debug('statusCode ======================= '+statusCode );  
                
                if(statusCode == 401 && errorCount < 4){
                     Map<string,string> responseMap = (Map<String,String>) JSON.deserialize(res.getBody(), Map<String,String>.class);
                     if(responseMap.containsKey('error')  && responseMap.get('error') == 'invalid_token'){
                         errorCount++;
                         tokenAndEmailUpdateMethod(jsonRequest);
                     }                
                }
                System.debug('statusCode======================= '+statusCode);
            }catch(Exception e){
                WebToSFDC.notifyDev('Update Email to CustomerPortal Error', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() ) ;
            }
     }
}