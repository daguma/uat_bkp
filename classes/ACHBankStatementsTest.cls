//It also needs ACHScreenCtrlTest to get the coverage

@isTest
private class ACHBankStatementsTest {

	@isTest static void bank_statements_period_with_is_title() {
		ACHBankStatements.BankStatementsPeriod bsp = new ACHBankStatements.BankStatementsPeriod(true);

		System.assertEquals('0', bsp.selectedP1);
		System.assertEquals('0', bsp.selectedP2);
		System.assertEquals('0', bsp.selectedP3);
		System.assertEquals('0', bsp.selectedP4);
		System.assertEquals(false, bsp.is60DayStatement);
	}

	@isTest static void bank_statements_period_without_title() {
		ACHBankStatements.BankStatementsPeriod bsp = new ACHBankStatements.BankStatementsPeriod(false);

		System.assertNotEquals('0', bsp.selectedP1);
		System.assertNotEquals('0', bsp.selectedP2);
		System.assertNotEquals('0', bsp.selectedP3);
		System.assertNotEquals('0', bsp.selectedP4);
		System.assertEquals(false, bsp.is60DayStatement);
	}

	@isTest static void bank_statements_period_variables_assignment() {
		ACHBankStatements.BankStatementsPeriod bsp = new ACHBankStatements.BankStatementsPeriod('1', '2', '3', '4', false, false);

		System.assertEquals('1', bsp.selectedP1);
		System.assertEquals('2', bsp.selectedP2);
		System.assertEquals('3', bsp.selectedP3);
		System.assertEquals('4', bsp.selectedP4);
		System.assertEquals(false, bsp.is60DayStatement);
		System.assertEquals(false, bsp.isTitle);
	}

	@isTest static void bank_statements_deposits() {
		ACHBankStatements.BankStatementsDeposits bsd = new ACHBankStatements.BankStatementsDeposits();

		System.assertEquals('Payroll Deposits', bsd.labelName);
	}

	@isTest static void bank_statements_deposits_variables_assignment() {
		ACHBankStatements.BankStatementsDeposits bsd = new ACHBankStatements.BankStatementsDeposits('1', '2', '3', '4', '1', '2', '3', '4');

		System.assertEquals('Payroll Deposits', bsd.labelName);
		System.assertEquals('1', bsd.valForP1);
		System.assertEquals('2', bsd.valForP2);
		System.assertEquals('3', bsd.valForP3);
		System.assertEquals('4', bsd.valForP4);

		System.assertEquals('1', bsd.dateForP1);
		System.assertEquals('2', bsd.dateForP2);
		System.assertEquals('3', bsd.dateForP3);
		System.assertEquals('4', bsd.dateForP4);
	}

	@isTest static void gets_avg_daily_balance_amt() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetAvgDailyBalanceAmt(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_avg_negative_days_num() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetAvgNegativeDaysNum(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_current_avg_daily_balance_amt() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetCurrentAvgDailyBalanceAmt(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_ytd_nsf_amt() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetYTD_NSFsAmt(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_ytd_nsf_num() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Integer result = bs.GetYTD_NSFsNum(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_nsf_amt() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetNSFsAmt(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_nsf_num() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Integer result = bs.GetNSFsNum(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_total_deposits_amt() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetTotalDepositsAmt(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_ytd_overdrafts_amt() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetYTD_OverdraftsAmt(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_ytd_overdrafts_num() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Integer result = bs.GetYTD_OverdraftsNum(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_overdrafts_amt() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetOverdraftsAmt(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_overdrafts_num() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Integer result = bs.GetOverdraftsNum(listBsi);

		System.assert(result > 0);
	}

	@isTest static void gets_current_balance() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Decimal result = bs.GetCurrentBalance(listBsi);

		System.assert(result > 0);
	}

	@isTest static void is_last_three_months_bank_statement() {

		List<ACHBankStatements.BankStatementsInfo> listBsi = LibraryTest.fakeBankStatementsInfoList();

		ACHBankStatements bs = new ACHBankStatements();

		Boolean result = bs.IsLast3MonthsBankStatementsPopulated(listBsi);

		System.assertEquals(true, result);
	}

	@isTest static void is_last_three_months_bank_deposits() {

		ACHBankStatements.BankStatementsDeposits bsd = new ACHBankStatements.BankStatementsDeposits('1', '2', '3', '4', '1', '2', '3', '4');

		List<ACHBankStatements.BankStatementsDeposits> listBsd = new List<ACHBankStatements.BankStatementsDeposits>();

		listBsd.add(bsd);

		ACHBankStatements bs = new ACHBankStatements();

		Boolean result = bs.IsLast3MonthsBankDepositsPopulated(listBsd);

		System.assertEquals(true, result);
	}

	@isTest static void has_bank_statements_deposits() {

		ACHBankStatements.BankStatementsDeposits bsd = new ACHBankStatements.BankStatementsDeposits('1', '2', '3', '4', '1', '2', '3', '4');

		List<ACHBankStatements.BankStatementsDeposits> listBsd = new List<ACHBankStatements.BankStatementsDeposits>();

		listBsd.add(bsd);

		ACHBankStatements bs = new ACHBankStatements();

		Boolean result = bs.HasBankStatementsDeposits(listBsd);

		System.assertEquals(true, result);
	}

}