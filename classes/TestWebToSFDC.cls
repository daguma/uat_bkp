@isTest public class TestWebToSFDC{

  @isTest public static void ConvertedCase() {
    MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();             maskSettings.MaskAllData__c = false;             insert maskSettings;
    Testing_Settings__c TS = new Testing_Settings__c(Name = 'TestData', Test_Email__c = 'testnew123@gmail.com', Test_SSN__c = '351762213' );
    insert TS;

    LP_Custom__c sett = new LP_Custom__c(SupportedStates__c = 'CA,GA,MO');
    insert sett;

    //Lead already exist
    Lead TestLead = new Lead();
    TestLead.Firstname = 'test';
    TestLead.Lastname = 'Testcase';
    TestLead.Email = 'testnew123@gmail.com';
    TestLead.Date_of_Birth__c = Date.Today().addYears(-21);
    TestLead.Point_Code__c = '54354657';
    TestLead.SSN__c = '123678905';
    TestLead.Time_at_current_Address__c = Date.Today().addYears(-3);
    TestLead.Phone = '1231231233';
    TestLead.street = 'Test street';
    TestLead.City = 'City';
    TestLead.State = 'GA';
    TestLead.Use_of_Funds__c = 'Wedding';
    TestLead.PostalCode = '123132';
    TestLead.Country = 'US';
    TestLead.Annual_Income__c = 100000;
    TestLead.Loan_Amount__c = 4000;
    TestLead.Employment_Start_date__c = Date.Today().addYears(-3);
    TestLead.Company = 'Test Company';
    TestLead.status = 'Mailed';
    insert TestLead;


    //Lead already exist
    TestLead = new Lead();
    TestLead.Firstname = 'test';
    TestLead.Lastname = 'Testcase';
    TestLead.Email = 'testnew123@gmail.com';
    TestLead.Date_of_Birth__c = Date.Today().addYears(-21);
    TestLead.Point_Code__c = '54354657';
    TestLead.SSN__c = '123678905';
    TestLead.Time_at_current_Address__c = Date.Today().addYears(-3);
    TestLead.Phone = '1231231233';
    TestLead.street = 'Test street';
    TestLead.City = 'City';
    TestLead.State = 'GA';
    TestLead.PostalCode = '123132';
    TestLead.Country = 'US';
    TestLead.Annual_Income__c = 100000;
    TestLead.Loan_Amount__c = 4000;
    TestLead.Employment_Start_date__c = Date.Today().addYears(-3);
    TestLead.Company = 'Test Company';
    TestLead.status = 'Organic';
    TestLead.Use_of_Funds__c = 'Wedding';
    insert TestLead;
      
      Use_of_Funds_Mapping__c uf = new Use_of_Funds_Mapping__c();
      uf.SFDC_Value__c = 'Test Loan';
      uf.Name = 'Test Loan';
      insert uf;


    Map<string, string> ReqJsonMap = new Map<string, string>();
    ReqJsonMap.put('Firstname', 'test');
    ReqJsonMap.put('lastname', 'Testcase');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('City', 'ANTHILL');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '220 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('Phone', '1231231233');
    ReqJsonMap.put('mail', 'testnew123@gmail.com');
    //marisol.testcasetest03240508@gmail.com
    ReqJsonMap.put('SSN', '123678905');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '54354657');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    ReqJsonMap.put('SubSource', 'LPCustomerPortal');
    ReqJsonMap.put('SourceCode', '1234|12');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '12000');
    ReqJsonMap.put('PullAuth', 'True');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1980');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    ReqJsonMap.put('kountResponse', 'Dummy');

    string PersonalInfo = JSON.serializePretty(ReqJsonMap);

    string Response;
    string resp = WebToSFDC.CreateLead(PersonalInfo);
    system.debug('===resp=Converted=' + resp);
    system.debug('==TestLead Status==' + TestLead);
    //Contact already exist
    Account TestAcc = new Account(name = 'TestCase');
    insert TestAcc;

    Contact TestContact = TestHelper.createContact();
    TestContact.Accountid = TestAcc.id;
    update TestContact;

    //Lead already exist and converted
    TestContact.Lead__c = TestLead.id;
    update TestContact;

    Database.LeadConvert lc = new Database.LeadConvert();
    lc.setLeadId(TestLead.id);
    lc.setAccountId(TestAcc.id);
    lc.setContactId(TestContact.Id);
    lc.setConvertedStatus('Application Submitted');


    Database.LeadConvertResult lcr = Database.convertLead(lc);

    ReqJsonMap.remove('mail');
    ReqJsonMap.put('mail', TestLead.Email);



    resp = WebToSFDC.CreateLead(PersonalInfo);
    system.debug('===resp==' + resp);


  }


  @isTest public static void ExistingContactCase() {
    MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();             maskSettings.MaskAllData__c = false;             insert maskSettings;
    Testing_Settings__c TS = new Testing_Settings__c(Name = 'TestData', Test_Email__c = 'testnew123@gmail.com', Test_SSN__c = '351762213' );
    insert TS;

    //Contact already exist
    Account TestAcc = new Account(name = 'TestCase');
    insert TestAcc;
    
    // Insert Partner Accounts
    Account partnerAct_cs = new Account(name='Credit Smart',ParterType_ForIndexing__c='Partner',Is_Parent_Partner__c = true,Partner_Sub_Source__c='CreditSmart',Contact_Restricted_Partner__c=true);
    insert partnerAct_cs;
    Account partnerAct_lt = new Account(name='Lending Tree',ParterType_ForIndexing__c='Partner',Is_Parent_Partner__c = true,Partner_Sub_Source__c='LendingTree',Contact_Restricted_Partner__c=false);
    insert partnerAct_lt;
        
    LP_Custom__c sett = new LP_Custom__c(Days_before_Declined_Retry__c = 2);
    insert sett;

    Contact TestContact = TestHelper.createContact();
    TestContact.Accountid = TestAcc.id;
    TestContact.Email = 'testnew123@gmail.com';
    TestContact.Lead_Sub_Source__c = 'CreditSmart';
    update TestContact;
    
    Map<string, string> ReqJsonMap = new Map<string, string>();
    ReqJsonMap.put('Firstname', 'MARISOL');
    ReqJsonMap.put('lastname', 'TESTCASE');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('City', 'ANTHILL');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '220 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('Phone', '8040124578');
    ReqJsonMap.put('mail', 'testnew123@gmail.com');
    //marisol.testcasetest03240508@gmail.com
    ReqJsonMap.put('SSN', '1123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    //ReqJsonMap.put('SubSource', 'LPCustomerPortal');
    ReqJsonMap.put('SourceCode', '1234|12');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '12000');
    ReqJsonMap.put('PullAuth', 'True');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1980');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    ReqJsonMap.put('lengthOfEmp', '13');
    ReqJsonMap.put('kountResponse', 'Dummy');
    ReqJsonMap.put('SubSource','LendingTree');
    string PersonalInfo = JSON.serializePretty(ReqJsonMap);

    string Response;
    string resp = WebToSFDC.CreateLead(PersonalInfo);
    system.debug('TestContact'+TestContact.Id+' '+TestContact.Lead_Sub_Source__c);
    system.debug('===resp=Existing contact only=' + resp);
  }

  @isTest static void TestCreateLead() {
      MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();             maskSettings.MaskAllData__c = false;             insert maskSettings;
      Use_of_Funds_Mapping__c uf = new Use_of_Funds_Mapping__c();
      uf.SFDC_Value__c = 'Test Loan';
      uf.Name = 'Test Loan';
      insert uf;

    Testing_Settings__c TS = new Testing_Settings__c(Name = 'TestData', Test_Email__c = 'testnew123@gmail.com', Test_SSN__c = '351762213' );
    insert TS;

    DOB_Validation__c Dob = new DOB_Validation__c();
    Dob.Name = 'Allow18';
    Dob.AllowBelow18__c = true;
    insert Dob;

    Loan_Amount_Validation__c lnValid = new Loan_Amount_Validation__c();
    lnValid.name = 'Loan Amount Validation';
    lnValid.Allow_without_validation__c = true;
    insert lnValid;


    Map<string, string> ReqJsonMap = new Map<string, string>();
    ReqJsonMap.put('Firstname', 'MARISOL');
    ReqJsonMap.put('lastname', 'TESTCASE');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('City', 'ANTHILL');
    ReqJsonMap.put('state', 'GA');
    ReqJsonMap.put('Street', '220 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('Phone', '8040124578');
    ReqJsonMap.put('mail', 'marisol.testcasetest03240508@gmail.com');
    ReqJsonMap.put('SSN', '1123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    ReqJsonMap.put('SubSource', 'partnerAct'); // INC 546
    ReqJsonMap.put('SourceCode', '1234');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '12000');
    ReqJsonMap.put('PullAuth', 'True');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1980');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    ReqJsonMap.put('kountResponse', 'Dummy');

    string PersonalInfo = JSON.serializePretty(ReqJsonMap);

    string Response;

    //Lead and Contact does not exist
    //  WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','8040124578', 'marisol.testcasetest03240508@gmail.com','','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',true,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    string resp = WebToSFDC.CreateLead(PersonalInfo);
    system.debug('===resp=lead contact doesnt exist=' + resp);

    //Contact already exist
    Account TestAcc = new Account(name = 'TestCase', Partner_Sub_Source__c = 'partnerAct', Type = 'Partner', Is_Parent_Partner__c = true); // INC 546
    insert TestAcc;

    Contact TestContact = TestHelper.createContact();
    TestContact.Accountid = TestAcc.id;
    update TestContact;

    LP_Custom__c sett = new LP_Custom__c(Days_before_Declined_Retry__c = 2);
    insert sett;

    // Response = WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','8040124578', TestContact.email ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',true,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    ReqJsonMap.put('mail', TestContact.email);
    Response = WebToSFDC.CreateLead(PersonalInfo);
    Map<string, string> PersonInfoMap = (Map<String, String>) JSON.deserialize(Response, Map<String, String>.class);
    // system.assertEquals(PersonInfoMap.GET('status'), 'DUP');

    //Lead already exist
    Lead TestLead = new Lead();

    TestLead.Firstname = 'test';
    TestLead.Lastname = 'Testcase';
    TestLead.Email = 'testnew123@gmail.com';
    TestLead.Date_of_Birth__c = Date.Today().addYears(-21);
    TestLead.Point_Code__c = '54354657';
    TestLead.SSN__c = '123678905';
    TestLead.Time_at_current_Address__c = Date.Today().addYears(-3);
    TestLead.Phone = '1231231233';
    TestLead.street = 'Test street';
    TestLead.City = 'City';
    TestLead.State = 'GA';
    TestLead.PostalCode = '123132';
    TestLead.Country = 'US';
    TestLead.Annual_Income__c = 100000;
    TestLead.Loan_Amount__c = 4000;
    TestLead.Employment_Start_date__c = Date.Today().addYears(-3);
    TestLead.Company = 'Test Company';
    TestLead.Use_of_Funds__c = 'Wedding';

    insert TestLead;

    // WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','8040124578', TestLead.email ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',true,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');

    resp = WebToSFDC.CreateLead(PersonalInfo);
    system.debug('===resp=lead already exists=' + resp);
    ReqJsonMap.put('mail', TestLead.email);


    //Lead already exist and converted
    TestContact.Lead__c = TestLead.id;
    update TestContact;

    Database.LeadConvert lc = new Database.LeadConvert();
    lc.setLeadId(TestLead.id);
    lc.setAccountId(TestAcc.id);
    lc.setContactId(TestContact.Id);
    lc.setConvertedStatus('Application Submitted');

    Database.LeadConvertResult lcr = Database.convertLead(lc);

    //  WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','8040124578', TestLead.email ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',false,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    resp = WebToSFDC.CreateLead(PersonalInfo);
    system.debug('===resp=Converted lead already exists=' + resp);
  }


  @isTest static void TestValidate() {
      MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();             maskSettings.MaskAllData__c = false;             insert maskSettings;
      Use_of_Funds_Mapping__c uf = new Use_of_Funds_Mapping__c();
      uf.SFDC_Value__c = 'Test Loan';
      uf.Name = 'Test Loan';
      insert uf;


    Testing_Settings__c TS = new Testing_Settings__c(Name = 'TestData', Test_Email__c = 'testnew123@gmail.com', Test_SSN__c = '351762213' );
    insert TS;

    /* DOB_Validation__c Dob = new DOB_Validation__c();
    Dob.Name = 'Allow18';
    Dob.AllowBelow18__c= true;
    insert Dob;

    Loan_Amount_Validation__c lnValid= new Loan_Amount_Validation__c();
    lnValid.name='Loan Amount Validation';
    lnValid.Allow_without_validation__c= true;
    insert lnValid;
    */

    Map<string, string> ReqJsonMap = new Map<string, string>();
    string PersonalInfo;


    //First Name is empty
    // WebToSFDC.CreateLead('' ,'TESTCASE','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','8040124578',  'abc@gmail.com' ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',false,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    ReqJsonMap.put('Firstname', '');
    ReqJsonMap.put('lastname', 'TESTCASE');
    ReqJsonMap.put('Phone', '8040124578');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('City', 'ANTHILL');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '220 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('mail', 'marisol.testcasetest03240508@gmail.com');
    ReqJsonMap.put('SSN', '1123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    ReqJsonMap.put('SubSource', 'LPCustomerPortal');
    ReqJsonMap.put('SourceCode', 'LPCustomerPortal|LPCustomerPortal');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '12000');
    ReqJsonMap.put('PullAuth', 'True');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1980');
    ReqJsonMap.put('kountResponse', 'Dummy');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    PersonalInfo = JSON.serializePretty(ReqJsonMap);
    string resp = WebToSFDC.CreateLead(PersonalInfo);
    system.debug('===resp=validation error=' + resp);

    //Last Name is empty
    ReqJsonMap.clear();
    // WebToSFDC.CreateLead('MARISOL' ,'','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','8040124578',  'abc@gmail.com' ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',false,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    ReqJsonMap.put('lastname', '');
    ReqJsonMap.put('Firstname', 'MARISOL');
    ReqJsonMap.put('Phone', '8040124578');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('City', 'ANTHILL');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '220 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('mail', 'marisol.testcasetest03240508@gmail.com');
    ReqJsonMap.put('SSN', '1123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    ReqJsonMap.put('SubSource', 'LPCustomerPortal');
    ReqJsonMap.put('SourceCode', '1234');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '12000');
    ReqJsonMap.put('PullAuth', 'True');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1980');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('kountResponse', 'Dummy');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    PersonalInfo = JSON.serializePretty(ReqJsonMap);
    system.debug('===PersonalInfo===' + PersonalInfo);
    WebToSFDC.CreateLead(PersonalInfo);

    //ZipCode is empty
    ReqJsonMap.clear();
    // WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','', 'ANTHILL',  'MO','220 LOCUST AVE', '45','8040124578',  'abc@gmail.com' ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',false,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    ReqJsonMap.put('ZIPCODE', '');
    ReqJsonMap.put('lastname', '');
    ReqJsonMap.put('Firstname', 'MARISOL');
    ReqJsonMap.put('Phone', '8040124578');
    ReqJsonMap.put('City', 'ANTHILL');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '220 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('mail', 'marisol.testcasetest03240508@gmail.com');
    ReqJsonMap.put('SSN', '1123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    ReqJsonMap.put('SubSource', 'LPCustomerPortal');
    ReqJsonMap.put('SourceCode', '1234');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '12000');
    ReqJsonMap.put('PullAuth', 'True');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1980');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('kountResponse', 'Dummy');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    PersonalInfo = JSON.serializePretty(ReqJsonMap);
    system.debug('===PersonalInfo===' + PersonalInfo);
    WebToSFDC.CreateLead(PersonalInfo);

    //City is empty
    ReqJsonMap.clear();
    // WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','65488', '',  'MO','220 LOCUST AVE', '45','8040124578',  'abc@gmail.com' ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',false,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    ReqJsonMap.put('City', '');
    ReqJsonMap.put('Firstname', 'MARISOL');
    ReqJsonMap.put('lastname', 'TESTCASE');
    ReqJsonMap.put('Phone', '8040124578');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '220 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('mail', 'marisol.testcasetest03240508@gmail.com');
    ReqJsonMap.put('SSN', '1123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    ReqJsonMap.put('SubSource', 'LPCustomerPortal');
    ReqJsonMap.put('SourceCode', '1234');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '12000');
    ReqJsonMap.put('PullAuth', 'True');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1980');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('kountResponse', 'Dummy');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    PersonalInfo = JSON.serializePretty(ReqJsonMap);
    WebToSFDC.CreateLead(PersonalInfo);

    //Phone No. is empty
    ReqJsonMap.clear();
    //WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','',  'abc@gmail.com' ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',false,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    ReqJsonMap.put('Phone', '');
    ReqJsonMap.put('Firstname', 'MARISOL');
    ReqJsonMap.put('lastname', 'TESTCASE');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('City', 'ANTHILL');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '220 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('mail', 'marisol.testcasetest03240508@gmail.com');
    ReqJsonMap.put('SSN', '1123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Wedding');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    ReqJsonMap.put('SubSource', 'LPCustomerPortal');
    ReqJsonMap.put('SourceCode', '1234');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '12000');
    ReqJsonMap.put('PullAuth', 'True');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1980');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    ReqJsonMap.put('kountResponse', 'Dummy');
    PersonalInfo = JSON.serializePretty(ReqJsonMap);
    system.debug('===PersonalInfo===' + PersonalInfo);
    Contact c = TestHelper.createContact();
    c.Email = 'marisol.testcasetest03240508@gmail.com';
    c.FirstName = 'test';
    c.LastName = 'true';
    update c;

    WebToSFDC.CreateLead(PersonalInfo);
    List<Lead> l = [Select Id from Lead LIMIT 1];
    if (l.size() > 0)
    { 
        c.Lead__c = l[0].Id;
        update c;
        ReqJsonMap.put('leadId', l[0].Id);
    }
    WebToSFDC.updateContact(PersonalInfo);

    PersonalInfo = JSON.serializePretty(ReqJsonMap);
    System.debug('PersonalInfo = ' + PersonalInfo);
    WebToSFDC.updateContact(PersonalInfo);
    WebToSFDC.notifyDev('WebToSFDC Error - Inserting Lead', 'Test');

    //Phone No. is empty
    ReqJsonMap.clear();
    //WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','',  'abc@gmail.com' ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',false,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    ReqJsonMap.put('Phone', '4564541234');
    ReqJsonMap.put('Firstname', 'MARISOL6');
    ReqJsonMap.put('lastname', 'TESTCASE6');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('City', 'ANTHILL2');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '225 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('mail', 'marisol999.testcasetest03240508@gmail.com');
    ReqJsonMap.put('SSN', '414111123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LPCustomerPortal');
    ReqJsonMap.put('SubSource', 'LPCustomerPortal');
    ReqJsonMap.put('SourceCode', '1234');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '62000');
    ReqJsonMap.put('PullAuth', 'False');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1981');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    ReqJsonMap.put('kountResponse', 'Dummy');
    PersonalInfo = JSON.serializePretty(ReqJsonMap);
    system.debug('===PersonalInfo===' + PersonalInfo);

    WebToSFDC.CreateLead(PersonalInfo);
    WebToSFDC.CreateLead(PersonalInfo);


    ReqJsonMap.clear();
    //WebToSFDC.CreateLead('MARISOL' ,'TESTCASE','65488', 'ANTHILL',  'MO','220 LOCUST AVE', '45','',  'abc@gmail.com' ,'','121.12.15.1',15000, 'Test Loan',12000,'Merisol Testcase Employer','08/18/2013','Merisol Testcase Employer City','Merisol Testcase Employer State','65488','8547851058',false,true,Date.newInstance(1980, 05, 27),Date.newInstance(2013,08,18), Date.newInstance(2013,08,18), '', 'LPCustomerPortal', 'LPCustomerPortal','123','CEO','12345', 'Test Request');
    ReqJsonMap.put('Phone', '4564541234');
    ReqJsonMap.put('Firstname', 'MARISOL6');
    ReqJsonMap.put('lastname', 'TESTCASE6');
    ReqJsonMap.put('ZIPCODE', '65488');
    ReqJsonMap.put('City', 'ANTHILL2');
    ReqJsonMap.put('state', 'MO');
    ReqJsonMap.put('Street', '225 LOCUST AVE');
    ReqJsonMap.put('Unit', '45');
    ReqJsonMap.put('mail', 'marisol999.testcasetest03240508@gmail.com');
    ReqJsonMap.put('SSN', '414111123');
    ReqJsonMap.put('IP', '121.12.15.1');
    ReqJsonMap.put('LoanPurpose', 'Test Loan');
    ReqJsonMap.put('EmpName', 'test');
    ReqJsonMap.put('EmpStrt', 'test');
    ReqJsonMap.put('EmpCity', 'Merisol Testcase Employer City');
    ReqJsonMap.put('EmpState', 'test');
    ReqJsonMap.put('EmpZip', '1234');
    ReqJsonMap.put('EmpPhone', '12');
    ReqJsonMap.put('PointCod', '1234');
    ReqJsonMap.put('LeadSource', 'LendingTree');
    ReqJsonMap.put('SubSource', 'PPC');
    ReqJsonMap.put('SourceCode', '1234');
    ReqJsonMap.put('JobTitle', 'CEO');
    ReqJsonMap.put('LendingTreeReq', '');
    ReqJsonMap.put('LoanAmt', '15000');
    ReqJsonMap.put('AnlIncm', '62000');
    ReqJsonMap.put('PullAuth', 'False');
    ReqJsonMap.put('IdentityVerf', 'Test Request');
    ReqJsonMap.put('DOB', '05/27/1981');
    ReqJsonMap.put('EmpStrtDate', '08/18/2013');
    ReqJsonMap.put('AdresTime', '08/18/2013');
    ReqJsonMap.put('kountResponse', 'Dummy');
    PersonalInfo = JSON.serializePretty(ReqJsonMap);
    system.debug('===PersonalInfo===' + PersonalInfo);

    resp = WebToSFDC.CreateLead(PersonalInfo);
    system.debug('===resp===' + resp);
    WebToSFDC.CreateLead(PersonalInfo);


  }

}