@isTest public class ProxyApiCreditReportTest {

    @isTest static void gets_credit_report() {
        
        LibraryTest.createBrmsConfigSettings(1,true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity application = LibraryTest.createApplicationTH();
        
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        ProxyApiCreditReportEntity creditReportEntity = ProxyApiCreditReport.getCreditReport(LibraryTest.fakeCreditReportInputDataPreBRMS(application), true, true);
        
    
        System.debug(creditReportEntity);
        System.assertNotEquals(creditReportEntity, null);
    }

    @isTest static void gets_credit_report_by_id() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiCreditReportEntity creditReportEntity = ProxyApiCreditReport.getCreditReportById(1);

        System.assertNotEquals(creditReportEntity, null);
    }

    @isTest static void gets_approved_credit_report_attributes() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                //'[{"Proxy_API_TokenExpiresTime__c": ' + DateTime.now().addDays(-1).getTime() + '},{"Proxy_API_Token__c": "test"}]',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiCreditReportEntity creditReportEntity = ProxyApiCreditReport.approvedCreditReportAttributes('username', 1);

        System.assertNotEquals(creditReportEntity, null);

    }

    @isTest static void gets_list_credit_report_by_id() {
        
        LibraryTest.createBrmsConfigSettings(1,true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity application = LibraryTest.createApplicationTH();
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        List<ProxyApiCreditReportEntityLite> creditReportList = ProxyApiCreditReport.getAllCreditReportByEntityId(application.id);

        System.assertNotEquals(creditReportList, null);

    }

    @isTest static void regrade() {
        
        LibraryTest.createBrmsConfigSettings(1,true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity application = LibraryTest.createApplicationTH();
        
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiCreditReportEntity creditReportList = ProxyApiCreditReport.reGrade(LibraryTest.fakeRegradeInputData(application));

        System.assertNotEquals(creditReportList, null);

    }

    @isTest static void get_hard_pull() {
        
        LibraryTest.createBrmsConfigSettings(1,true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity application = LibraryTest.createApplicationTH();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        Boolean validate = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiCreditReportEntity creditReportList = ProxyApiCreditReport.getHardPullByEntityId(application.id);

        System.assertNotEquals(creditReportList, null);

    }
    
    @isTest static void approved_credit_report_attributes_new() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                //'[{"Proxy_API_TokenExpiresTime__c": ' + DateTime.now().addDays(-1).getTime() + '},{"Proxy_API_Token__c": "test"}]',
                '[{"Default_URL_New__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiCreditReportEntity creditReportEntity = ProxyApiCreditReport.approvedCreditReportAttributesNew('username', 1);

        System.assertNotEquals(creditReportEntity, null);
    }
    
   @isTest static void call_Gds_Test() { 
        
        LibraryTest.createBrmsConfigSettings(1,true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity application = LibraryTest.createApplicationTH();
        
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                //'[{"Proxy_API_TokenExpiresTime__c": ' + DateTime.now().addDays(-1).getTime() + '},{"Proxy_API_Token__c": "test"}]',
                '[{"Default_URL_New__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiCreditReportEntity creditReportEntity = ProxyApiCreditReport.callGds(LibraryTest.fakeCreditReportInputData(application));

    }
    

}