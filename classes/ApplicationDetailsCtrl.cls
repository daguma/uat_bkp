public with sharing class ApplicationDetailsCtrl {

    @AuraEnabled
    public static ApplicationDetailsEntity getPicklistValues(Opportunity opp) {

        ApplicationDetailsEntity ade = new ApplicationDetailsEntity();

        try {

            //Set the picklist values
            ade.paymentMethods =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Payment_Method__c.getDescribe(),
                            opp==null ? null : opp.Payment_Method__c,
                            true
                    );
            ade.incomeDenominators =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Income_Denominator__c.getDescribe(),
                            opp==null ? null : opp.Income_Denominator__c,
                            true
                    );
            ade.bankInfoSources =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Bank_Info_Source__c.getDescribe(),
                            opp==null ? null : opp.Bank_Info_Source__c,
                            true
                    );
        } catch (Exception e) {
            ade.errorMessage = e.getMessage() + ' -> ' + e.getStackTraceString();
            system.debug(ade.errorMessage);
            return ade;
        }

        return ade;
    }

    public class ApplicationDetailsEntity {
        @AuraEnabled public List<alphaHelperCls.PicklistItem> paymentMethods { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> incomeDenominators { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> bankInfoSources { get; set; }
        @AuraEnabled public Boolean hasError { get; set; }
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = false;
            }
        }

        public ApplicationDetailsEntity() {
            hasError = false;
        }
    }

}