@RestResource(urlMapping='/PartnersAPI/*')
global with sharing class CRPartnersRestAPI {
    
    @HttpPost
    global static void completeCrProcess() { insert new Logs__c(debug__c = 'Se está llamando al servicio');
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestRequest req = RestContext.request;
        String proxyResponse = req.requestBody.toString();         
        String apiResponse = '{"responseMessage": "The method has ended without errrors"}';
        
        ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity)JSON.deserialize(EncodingUtil.base64Decode(proxyResponse.replace('"', '')).toString(), ProxyApiCreditReportEntity.class);
        insert new Logs__c(debug__c = JSON.serialize(creditReportEntity));
        Opportunity application = null;
        application = CreditReport.getOpportunity(creditReportEntity.entityId);

        if (creditReportEntity != null && creditReportEntity.errorMessage == null) {
            if (application != null) {
                try{
                    setOpportunityFields(creditReportEntity, application);
                    if (!Test.isRunningTest() && creditReportEntity.brms != null && creditReportEntity.brms.rulesList == null && creditReportEntity.brms.errors != null && creditReportEntity.brms.errors.size() > 0 && String.isNotEmpty(creditReportEntity.brms.errors[0].errorMessage) )  {
                        processRulesMissing(creditReportEntity, application);
                        apiResponse = '{"responseMessage":"BRMS rules were missing - App  '+application.Name+' "}';
                    }
                    if (creditReportEntity.brms != null && String.isnotempty(creditReportEntity.brms.deBRMS_ContractStatus) && creditReportEntity.brms.deBRMS_ContractStatus.equalsIgnoreCase('Credit Qualified')) {
                        processCreditQualifiedOpp(creditReportEntity, application);
                    }
                }catch(Exception e ){
                    apiResponse = '{"responseMessage":"'+e.getMessage()+' Line number'+e.getLineNumber()+'"}';
                }
            } 
        } else if (creditReportEntity != null && (creditReportEntity.brms == null || creditReportEntity.brms.rulesList == null)) {
            processBrmsError(creditReportEntity, application);
            apiResponse = '{"responseMessage":"The Credit Report was not generated for application '+application.Name+' "}';
        } else if (creditReportEntity == null) {
            processNullCreditReportEntity(application);
            apiResponse = '{"responseMessage":"The Credit Report was not generated for application  '+application.Name+' "}';
        }
        
        Decisioning.applicationStatusAutomationThruBRMS(application.Id, creditReportEntity, application); 
        update application;
        RestContext.response.responseBody = Blob.valueOf(apiResponse);
    }   

    public static void setOpportunityFields(ProxyApiCreditReportEntity creditReportEntity, Opportunity application){insert new Logs__c(debug__c = 'setOpportunityFields');
        AON_Configurable_Settings__c aonSettings = AON_Configurable_Settings__c.getValues('Settings');
        String bureau = (creditReportEntity.datasourceName != null && creditReportEntity.datasourceName.toUpperCase().startsWith(CreditReport.TRANSUNION)) ? CreditReport.TRANSUNION : CreditReport.EXPERIAN;
        Contact contact = null;
        
        if (String.isEmpty(application.KeyFactors__c)) {
            application.KeyFactors__c = CreditReportScoreFactors.getScoreFactorStatements(creditReportEntity.segmentsList, bureau);
        }
        if (creditReportEntity.segmentsList != null && !creditReportEntity.segmentsList.isEmpty())
            contact = CreditReportMilitary.changeContactMilitaryFlag(application.Contact__r.Id, creditReportEntity.segmentsList, bureau);
        
        application.CreditPull_Date__c = date.today();
        application.FICO__c = creditReportEntity.getAttributeValue(CreditReport.FICO);
        application.DTI__c = creditReportEntity.getAttributeValue(CreditReport.DTI);
        application.StrategyType__c = (creditReportEntity.brms != null && creditReportEntity.brms.deStrategy_Type != null) ? creditReportEntity.brms.deStrategy_Type : '';
        application.Bureau__c = creditReportEntity.datasourceName != null ? ( creditReportEntity.datasourceName.length() > 2 ? creditReportEntity.datasourceName.substring(0, 2).toUpperCase() : '') : '';
        application.PrimaryCreditReportId__c = creditReportEntity.Id;
        application.is_Send_Pricing_Notice__c = true;

        if (aonSettings != null && aonSettings.AON_Enabled__c && application.CreatedDate >= aonSettings.Activation_Date__c && application.Analytic_Random_Number__c <= aonSettings.Enrollment_Percent__c) {

            Decimal AON_PaymentAmount = 0.00;
            Decimal AON_FeePct = 0.00;

            if (creditReportEntity.offerCatalogList != null) {

                for (ProxyApiOfferCatalog offer : creditReportEntity.offerCatalogList) {
                    AON_PaymentAmount = offer.AONPaymentAmount;
                    AON_FeePct = offer.AONFeePct;
                    break;
                }

            }
            application.AON_Military__c = contact != null ? contact.Military__c : false;
            application.AON_isBundle__c = OfferCtrlUtil.isAonBundle(application);
            application.AON_Enrollment__c = application.AON_isBundle__c || (AON_PaymentAmount != 0.00 && AON_FeePct != 0.00);
        }
        update application;
    }

    public static void processRulesMissing(ProxyApiCreditReportEntity creditReportEntity, Opportunity application){ insert new Logs__c(debug__c = 'processRulesMissing');
        creditReportEntity.errorDetails = '';
        for (ProxyApiCreditReportBrmsErros err : creditReportEntity.brms.errors) {
            creditReportEntity.errorDetails += err.errorDetails + '\n';
        }
        application.Status__c = 'Pending Credit Data';
        if (application.sub__c != null ) application.sub__c = null;
        update application;
        if (!CreditReport.isSandBox() && !Test.isRunningTest())
        SalesForceUtil.EmailByUserId('Error with BRMS', 'BRMS rules were missing - App: ' + application.Name + ' (' + application.Id + ') - ' + (String.isNotEmpty( creditReportEntity.errorDetails ) ?  creditReportEntity.errorDetails  : ''), SalesForceUtil__c.getOrgDefaults().LPTeamId__c + ',' + SalesForceUtil__c.getOrgDefaults().ITIssuesId__c);
    }

    public static void processCreditQualifiedOpp(ProxyApiCreditReportEntity creditReportEntity, Opportunity application){insert new Logs__c(debug__c = 'processCreditQualifiedOpp');
        if (creditReportEntity.brms.scoringModels != null)
        for (Integer i = 0; i < creditReportEntity.brms.scoringModels.size(); i++ ) {
            if (creditReportEntity.brms.scoringModels[i].modelName == 'AcceptanceModelV1') {
                application.Sales_Category_Soft_Pull__c = creditReportEntity.brms.scoringModels[i].grade;
                update application;
                break;
            }
        }
        creditReportEntity.offerCatalogList = null; // to avoid regeneration of offers
        CreditReport.prepareGetAcceptanceModel(creditReportEntity.entityId);
        CreditReport.prepareGetTaxLiensFromLexisNexis(creditReportEntity.entityId);
        ScorecardUtil.scorecardProcess(creditReportEntity, application);
        CreditReport.setContactSoftPullAddress(creditReportEntity.entityId);   
    }

    public static void processBrmsError(ProxyApiCreditReportEntity creditReportEntity, Opportunity application){insert new Logs__c(debug__c = 'processBrmsError');
        if (creditReportEntity.brms != null && creditReportEntity.brms.errors != null && creditReportEntity.brms.errors.size() > 0 && String.isNotEmpty(creditReportEntity.brms.errors[0].errorMessage) ) creditReportEntity.errorMessage = (string.isNotEmpty(creditReportEntity.errorMessage) ? creditReportEntity.errorMessage + ' ' : '') +  creditReportEntity.brms.errors[0].errorMessage;
        if (!CreditReport.isSandBox() && !Test.isRunningTest() && String.isNotEmpty(creditReportEntity.errorMessage) && !creditReportEntity.errorMessage.contains('An error from the credit bureau was returned')) {
            application.Status__c = 'Pending Credit Data';
            if (application.sub__c != null ) application.sub__c = null;
            update application;
            SalesForceUtil.EmailByUserId('Error with Credit Report', creditReportEntity.errorMessage + ' - App: ' + application.Opportunity_Number__c + ' (' + application.Id + ')' + '\n' + creditReportEntity.errorDetails,
                                        SalesForceUtil__c.getOrgDefaults().LPTeamId__c + ',' + SalesForceUtil__c.getOrgDefaults().ITIssuesId__c);
        }
    }

    public static void processNullCreditReportEntity(Opportunity application){insert new Logs__c(debug__c = 'processNullCreditReportEntity');
        application.Status__c = 'Pending Credit Data';
        if (application.sub__c != null ) application.sub__c = null;
        update application;
        if (!CreditReport.isSandBox() && !Test.isRunningTest())
            SalesForceUtil.EmailByUserId('Credit Report not generated for application', 'Credit Report not generated for application ' + application.Opportunity_Number__c + ' (' + application.Id + ')' + ': Database or ATB connectivity are down. SOFT PULL',
                                         SalesForceUtil__c.getOrgDefaults().LPTeamId__c + ',' + SalesForceUtil__c.getOrgDefaults().ITIssuesId__c);
    }
}