public with sharing class FeeUtil {

    public static Decimal DEFAULT_APR = 35;
    public static Decimal DEFAULT_FEE_PERCENT = 5;
    public static Decimal SIX_FEE_PERCENT = 6;
    public static final String NET_FEE_OUT = 'Net Fee Out';
    public static final String FINWISE_TYPE = 'FINWISE';
    public static final String LENDING_POINT_TYPE = 'LENDING_POINT';
    
    //Code Commented under SM-362 where Fee pricing Rules Custom Setting usage has to be removed from SFDC
/*  public static Fee_Pricing_Rules__c getFeePricingRule(Decimal fico, String grade, Decimal term, String state, String product) {
        if (Finwise.isFinwise(state)) {
            return getFeePricingRule(fico, grade, String.valueOf(term), state, FINWISE_TYPE, product);
        } else {
            if (state == 'MT' || state == 'NJ')
                return new Fee_Pricing_Rules__c(APR__c = DEFAULT_APR, Fee_Percent__c = DEFAULT_FEE_PERCENT = 0);
            else if (Label.FeeRulesStates.contains(state))
                return getFeePricingRule(fico, grade, String.valueOf(term), state, LENDING_POINT_TYPE, product);
            else
                return new Fee_Pricing_Rules__c(APR__c = DEFAULT_APR, Fee_Percent__c = DEFAULT_FEE_PERCENT);
        }
    }

    public static Fee_Pricing_Rules__c getFeePricingRule(Decimal fico, String grade, String term, String state, String type, String product) {
        
        for (Fee_Pricing_Rules__c rule : [SELECT Id, Fee_Percent__c, APR__c
                                          FROM Fee_Pricing_Rules__c
                                          WHERE FICO_Min_Limit__c <= : fico AND FICO_Max_Limit__c >= : fico
                                          AND Grade__c LIKE :'%' + grade + '%'
                                          AND Term__c LIKE :'%' + term + '%'
                                          AND States__c LIKE :'%' + state + '%'
                                          AND ProductType__c LIKE :'%' + product + '%'
                                          AND Type__c = : type LIMIT 1]) {
            return rule;
        }
        return new Fee_Pricing_Rules__c(APR__c = DEFAULT_APR,
                                        Fee_Percent__c = (type.equals(FINWISE_TYPE) || Label.FeeRulesStates.contains(state)) ? SIX_FEE_PERCENT : DEFAULT_FEE_PERCENT);
    }
    */ //Commented Under SM-362
    
    public static Decimal getFee(String state, Decimal FeePercent, Decimal loanAmount) {
        Decimal fee = (loanAmount * FeePercent) / 100, fivepercentcap = 0;

        if (!Finwise.isFinwise(state)) {
            if (state == 'MO')
                fee = 75;
            else if (state == 'MI') {
                //return 300 or 5%, the less of the two for MI
                fivepercentcap = (loanAmount * DEFAULT_FEE_PERCENT) / 100;
                fee = 300 > fivepercentcap ? fivepercentcap : 300;
            } else if (state == 'IL')
                fee = 25;
            else if (state == 'AL') {
                fivepercentcap = (loanAmount * SIX_FEE_PERCENT) / 100;
                fee = 120 > fivepercentcap ? fivepercentcap : 120;
            } else if (state == 'MT' || state == 'NJ')
                fee = 0;
            else if (state == 'KY')
                fee = loanAmount <= 15000 ? 0 :  (loanAmount * FeePercent / 100);
            else if (state == 'WA' || state == 'TN')
                fee = (loanAmount * 4) / 100;
            else if (state == 'OH')
                fee = loanAmount <= 5000 ? 100 : loanAmount > 25000 ? (loanAmount) / 100 : (loanAmount) / 100 > 250 ? (loanAmount) / 100 : 250;
            else if (state == 'CA')
                fee = loanAmount < 5000 ? 75 : (loanAmount * FeePercent / 100);
            else if (state == 'TX')
                fee = 100;
            else if (state == 'AZ') {
                fee = loanAmount <= 10000 ? 150 : (loanAmount * FeePercent / 100);
            }
        }
        return fee;
    }

    public static Decimal getFeeRefinance(String state, Decimal feePercent, Decimal loanAmount) {
        
        if(state == 'IL') return 25;
        
        Decimal fee = getFee(state, feePercent, loanAmount);
        Decimal feePct = (fee / loanAmount) * 100;

        if (feePct > Decimal.valueOf(Label.Origination_Fee_for_Refinances)) {
            feePct = Decimal.valueOf(Label.Origination_Fee_for_Refinances);
            fee = (loanAmount * feePct) / 100;
        }
        
        return fee;
    }

    public static Offer__c calculateLoanAmountByOFee(String feeHandling, Decimal loanAmount, Decimal fee) {
        Offer__c result = new Offer__c();

        result.Fee_Percent__c = 0;
        if (fee > 0)
            result.Fee_Percent__c = (fee / loanAmount) * 100;

        if (feeHandling == NET_FEE_OUT) {
            result.Total_Loan_Amount__c = loanAmount;
            result.Loan_Amount__c = loanAmount - fee;
        } else {
            result.Loan_Amount__c = loanAmount;
            result.Total_Loan_Amount__c = fee + loanAmount;
        }
        return result;
    }
 
    public static String getValidationFee(String appId) {
        for (Offer__c o : [SELECT Fee_Percent__c
                           FROM Offer__c
                           WHERE Opportunity__c = :appId
                                  AND Fee_Percent__c <> NULL
                                   AND IsSelected__c = true LIMIT 1]) {
            return string.ValueOf(o.Fee_Percent__c.setscale(2) + '%');
        }
        return '0.00%';
    }

    public static Boolean getFeeValidationTolerance(Decimal fee_precent, String state) {
        return ( (state == 'CO' && fee_precent <= 10)
                 || (Finwise.isFinwise(state) && fee_precent <= FeeUtil.SIX_FEE_PERCENT)
                 || ( (Label.FeeRulesStates.contains(state) || state == 'AL') && fee_precent <= FeeUtil.SIX_FEE_PERCENT)
                 || (fee_precent <= FeeUtil.DEFAULT_FEE_PERCENT) );
    }
       
}