global class PersonRequestParameter {

    public String lastName {get; set;}

    public String firstName {get; set;}

    public String ssn {get; set;}
}