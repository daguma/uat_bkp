@isTest
public class CollectionsCasesAssignmentLBEntityTest {

    @isTest static void validate_constructor() {
        Test.startTest();
        Map<Id,Integer> testMap = new Map<Id, Integer>();
        Integer testInteger = 0;
        Id testUserIdFixed;
        for(User testUser : [SELECT Id FROM User LIMIT 10]){
            if(String.isEmpty(testUserIdFixed)){
                testUserIdFixed = testUser.Id;
            }
            testMap.put(testUser.Id,testInteger++);
        }
        CollectionsCasesAssignmentLBEntity testEntity = new CollectionsCasesAssignmentLBEntity(testMap,testUserIdFixed);
        System.assertNotEquals(null,testEntity);
        Test.stopTest();
    }

    @isTest static void validate_getNextAssigneeId() {
        Test.startTest();
        Map<Id,Integer> testMap = new Map<Id, Integer>();
        Integer testInteger = 0;
        Id testUserIdFixed;
        for(User testUser : [SELECT Id FROM User LIMIT 10]){
            if(String.isEmpty(testUserIdFixed)){
                testUserIdFixed = testUser.Id;
            }
            testMap.put(testUser.Id,testInteger++);
        }
        CollectionsCasesAssignmentLBEntity testEntity = new CollectionsCasesAssignmentLBEntity(testMap,testUserIdFixed);
        System.assertNotEquals(null,testEntity.getNextAssigneeId());

        CollectionsCasesAssignmentLBEntity testEntity2 = new CollectionsCasesAssignmentLBEntity(new Map<Id,Integer>(),testUserIdFixed);
        System.assertEquals(null,testEntity2.getNextAssigneeId());

        CollectionsCasesAssignmentLBEntity testEntity3 =
                new CollectionsCasesAssignmentLBEntity(
                        new Map<Id,Integer>{ [SELECT Id FROM User WHERE Id != :testUserIdFixed LIMIT 1].Id =>0}
                        , testUserIdFixed
                );
        System.assertEquals(testUserIdFixed,testEntity3.getNextAssigneeId());

        Test.stopTest();
    }
}