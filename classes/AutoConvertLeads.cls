Public class AutoConvertLeads
{
    @TestVisible private static set<ID> accountIds = new set<ID>();
    @InvocableMethod
    public static void LeadAssign(List<Id> LeadIds)
    {
        //Make sure the Lead owner is a valid user
        List<Lead> leads = [SELECT id, ownerId FROM Lead WHERE id in :LeadIds];
        List<Lead> leadsToUpdate = new List<Lead>();
        for(Lead aLead : leads){
            aLead.ownerId = '0050B0000075aBxQAI';
            leadsToUpdate.add(aLead);
        } 
        update leadsToUpdate;

        LeadStatus CLeadStatus= [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true Limit 1];
        List<Database.LeadConvert> MassLeadconvert = new List<Database.LeadConvert>();
        for(id currentlead: LeadIds){
            Database.LeadConvert Leadconvert = new Database.LeadConvert();
            Leadconvert.setLeadId(currentlead);
            Leadconvert.setConvertedStatus(CLeadStatus.MasterLabel);
            // Leadconvert.setDoNotCreateOpportunity(TRUE); //Remove this line if you want to create an opportunity from Lead Conversion
            MassLeadconvert.add(Leadconvert);
        }
        
        List<Partner_Product_Bank_Account__c> bankAccountList = new list<Partner_Product_Bank_Account__c>();
        if (!MassLeadconvert.isEmpty()) {
            if(!Test.isRunningTest()) {
                List<Database.LeadConvertResult> lcr = Database.convertLead(MassLeadconvert);
                for(Database.LeadConvertResult lc : lcr) {
                    if(lc.isSuccess())
                        accountIds.add(lc.getAccountId());
                }
            }
            Partner_Product_Bank_Account_Setting__c bankAccountSetting = Partner_Product_Bank_Account_Setting__c.getValues('OnboardingBankAccountSetup'); 
            for(Account acc : [select id, Transit_ABA_Number_9digits__c, Financial_Institution_Account_Number__c from Account where Id In :accountIds and Transit_ABA_Number_9digits__c != null AND Financial_Institution_Account_Number__c != null]) {
                if(bankAccountSetting != null && string.isNOtEmpty(bankAccountSetting.Lending_Product_ID__c) && string.isNotEmpty(bankAccountSetting.Product_ID__c) && string.isNotEmpty(bankAccountSetting.Account_Type__c)) {
                    Partner_Product_Bank_Account__c ppba = new Partner_Product_Bank_Account__c();
                    ppba.Account__c = acc.id;
                    ppba.Account_Type__c = bankAccountSetting.Account_Type__c;
                    ppba.Bank_Account_Number__c = acc.Financial_Institution_Account_Number__c;
                    ppba.Deposit_to_Vendor__c = True;
                    ppba.Lending_Product__c = bankAccountSetting.Lending_Product_ID__c;
                    ppba.Product__c = bankAccountSetting.Product_ID__c;
                    ppba.Routing_Number__c = acc.Transit_ABA_Number_9digits__c;
                    bankAccountList.add(ppba);
                }
            }
        }
        insert bankAccountList;
    }
}