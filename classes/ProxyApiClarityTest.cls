@isTest public class ProxyApiClarityTest {

    @isTest static void gets_inquiries_null() {
        ProxyApiClarityInquiryEntity result = ProxyApiClarity.getInquiries('');
        System.assertEquals(null, result);
    }

    @isTest static void gets_inquiries() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        ProxyApiClarityInquiryEntity result = ProxyApiClarity.getInquiries('');
        System.assertNotEquals(null, result);
    }
    @isTest static void gets_clarity_inquiries_null() {
        ProxyApiClarityClearIncomeAttributes result = ProxyApiClarity.getClarityIncome('');
        System.assertEquals(null, result);
    }

    @isTest static void gets_Clarity_inquiries() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        ProxyApiClarityClearIncomeAttributes result = ProxyApiClarity.getClarityIncome('');
        System.assertNotEquals(null, result);
    }
}