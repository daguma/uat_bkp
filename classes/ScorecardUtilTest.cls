@isTest public class ScorecardUtilTest {

    @isTest static void score() {
        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'New';
        update app;
        
        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);
        
        Contact cntct = [Select Id, Name from Contact where Id = : app.Contact__c];
        
        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Acceptance__c = 'Y';
        score.Opportunity__c = app.id;
        score.ProdAnnualIncome__c = -1.041877;
        score.ProdCreditScore__c = 0;
        score.ProdDTI_Ratio__c = 0.1;
        score.ProdHCNC__c = 1;
        score.ProdInquiries_last_6_months__c = 1;
        score.ProdInst_Trades_Opened__c = 1;
        score.ProdSelfEmployed__c = 1;
        score.ProdTotal_Trade_Lines__c = 10;
        score.Logit__c = -1.79;
        score.Unit_Bad_Probability__c = 0.143;
        score.Dollar_Rate_Bad_Probability__c = 7.15;
        score.FICO_Score__c = 600;
        score.Contact__c = cntct.id;
        insert score;
        
        Offer__c off = new Offer__c();
        off.Repayment_Frequency__c = 'Monthly';
        off.APR__c = 1;
        off.Term__c = 1;
        off.Payment_Amount__c = 300;
        off.IsSelected__c = true;
        off.Selected_Date__c = Date.today().addDays(-1);
        off.Loan_Amount__c = 2500;
        off.Total_Loan_Amount__c = 4000;
        off.Fee__c = 0.5;
        off.IsCustom__c = false;
        off.Fee_Percent__c = 0.1;
        off.FirstPaymentDate__c = Date.today().addDays(15);
        off.Installments__c = 1;
        off.Opportunity__c = app.id;
        
        insert off;
        
        System.debug('creditReport ' + creditReport);
        
        ScorecardUtil.score(creditReport);
        
        System.assertEquals('B1', creditReport.scorecardEntity.grade);
        
        ScorecardUtil.scorecardProcess(creditReport);
        
        System.assertEquals('B1', creditReport.scorecardEntity.grade);
    }
    
    @isTest static void regrades_app() {
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'New';
        update app;
        
        Contact cntct = [Select Id, Name from Contact where Id = : app.Contact__c];
        
        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Acceptance__c = 'Y';
        score.Opportunity__c = app.id;
        
        score.ProdAnnualIncome__c = -1.041877;
        score.ProdCreditScore__c = 0;
        score.ProdDTI_Ratio__c = 0.1;
        score.ProdHCNC__c = 1;
        score.ProdInquiries_last_6_months__c = 1;
        score.ProdInst_Trades_Opened__c = 1;
        score.ProdSelfEmployed__c = 1;
        score.ProdTotal_Trade_Lines__c = 10;
        score.Logit__c = -1.79;
        score.Unit_Bad_Probability__c = 0.143;
        score.Dollar_Rate_Bad_Probability__c = 7.15;
        score.FICO_Score__c = 600;
        score.Contact__c = cntct.id;
        
        insert score;
        
        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);
        
        ScorecardUtil.reGrade(creditReport, score);
        
        System.assertEquals('B1', creditReport.scorecardEntity.grade);
        
        score.Grade__c = 'C1';
        score.ProdDTI_Ratio__c = 0.2;
        
        update score;
        
        ScorecardUtil.reGrade(creditReport, score);
        
        System.assertEquals('B1', creditReport.scorecardEntity.grade);
        
        Offer__c off = new Offer__c();
        off.Repayment_Frequency__c = 'Monthly';
        off.APR__c = 1;
        off.Term__c = 1;
        off.Payment_Amount__c = 300;
        off.IsSelected__c = true;
        off.Selected_Date__c = Date.today().addDays(-1);
        off.Loan_Amount__c = 2500;
        off.Total_Loan_Amount__c = 4000;
        off.Fee__c = 0.5;
        off.IsCustom__c = false;
        off.Fee_Percent__c = 0.1;
        off.FirstPaymentDate__c = Date.today().addDays(15);
        off.Installments__c = 1;
        off.Opportunity__c = app.id;
        
        insert off;
        
        ScorecardUtil.reGrade(creditReport, score);
        
        System.assertEquals('B1', creditReport.scorecardEntity.grade);
    }
    
    @isTest static void gets_max_amount() {
        Decimal result = ScorecardUtil.maxAmount('A1', '');
        System.assertEquals(25000, result);
        
        result = ScorecardUtil.maxAmount('A2', '');
        System.assertEquals(25000, result);
        
        result = ScorecardUtil.maxAmount('B1', '');
        System.assertEquals(25000, result);
        
        result = ScorecardUtil.maxAmount('B2', '');
        System.assertEquals(17500, result);
        
        result = ScorecardUtil.maxAmount('C1', '2');
        System.assertEquals(5000, result);
        
        result = ScorecardUtil.maxAmount('C1', '1');
        System.assertEquals(10000, result);
        
        result = ScorecardUtil.maxAmount('C2', '');
        System.assertEquals(5000, result);
        
        result = ScorecardUtil.maxAmount('D', '');
        System.assertEquals(5000, result);
        
        result = ScorecardUtil.maxAmount('F', '');
        System.assertEquals(0, result);
    }
    
    @isTest static void gets_max_amount_override() {
        OfferOverrideUsers__c over = new OfferOverrideUsers__c();
        over.Email__c = UserInfo.getUserEmail();
        over.Name = UserInfo.getName();
        insert over;
        
        Decimal result = ScorecardUtil.maxAmount('C2', '');
        System.assertEquals(5000, result);
    }
    
    @isTest static void runs_scorecard_process() {
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'New';
        update app;
        
        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);
        
        ScorecardUtil.scorecardProcess(creditReport);
        
        System.assertEquals('B1', creditReport.scorecardEntity.grade);
    }
    
    @isTest static void scores_app() {
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'New';
        update app;
        
        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);
        
        ScorecardUtil.score(creditReport);
        
        System.assertEquals('B1', creditReport.scorecardEntity.grade);
    }
    
    @isTest static void scores_brms_app() {
        
        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'New';
        update app;
        
        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);
        
        ScorecardUtil.score(creditReport);
        
        System.assertEquals('B1', creditReport.scorecardEntity.grade);
    }
    
    @isTest static void gets_refinance_offers() {
        
        loan__Loan_Account__c testContract = LibraryTest.createContractTH();
        
        Opportunity app = [SELECT Id, Status__c, Contact__c, Contact__r.Loan_Amount__c, Type, FICO__c FROM Opportunity LIMIT 1];
        
        app.FICO__c = '720';
        app.Type = 'New';
        app.Status__c = 'Credit Qualified';
        app.Manual_Grade__c = 'B1';
        update app;
        
        Scorecard__c scoreRef = new Scorecard__c();
        scoreRef.Grade__c = 'B1';
        scoreRef.Opportunity__c = app.Id;
        scoreRef.Acceptance__c = 'Y';
        scoreRef.FICO_Score__c = 720;
        scoreRef.Contact__c = app.Contact__c;
        insert scoreRef;
        
        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);
        
        Opportunity appRefi = new Opportunity();
        appRefi.Contact__c = app.Contact__c;
        appRefi.Lending_Product__c = [Select Id from Lending_Product__c where Name = 'Sample Loan Product' limit 1].Id;
        appRefi.Product_Type__c = 'LOAN';
        appRefi.Amount = app.Contact__r.Loan_Amount__c;
        appRefi.Days_Convention__c = '30/360';
        appRefi.Fee__c = 0.04 * (app.Contact__r.Loan_Amount__c == null ? 0.0 : app.Contact__r.Loan_amount__c);
        appRefi.Interest_Calculation_Method__c = 'Declining Balance';
        appRefi.Type = 'Refinance';
        appRefi.FICO__c = '730';
        appRefi.Manual_Grade__c = 'B1';
        appRefi.Status__c = 'Credit Qualified';
        appRefi.Estimated_Grand_Total__c = 205000;
        appRefi.Contract_Renewed__c = testContract.id;
        appRefi.Payment_Frequency__c = 'MONTHLY';
        appRefi.Name = 'Test Opp';
        appRefi.CloseDate = System.today();
        appRefi.StageName = 'Qualification';
        insert appRefi;
        
        ProxyApiCreditReportEntity creditReport2 = LibraryTest.fakeCreditReportEntity(appRefi.id);
        
        if ([SELECT Count() FROM loan_Refinance_Params__c] == 0) {
            loan_Refinance_Params__c refiParams = new loan_Refinance_Params__c();
            refiParams.Contract__c = testContract.Id;
            refiParams.FICO__c = 720;
            refiParams.Grade__c = 'B1';
            refiParams.New_FICO__c = 720;
            refiParams.New_Grade__c = 'B1';
            refiParams.Eligible_For_Refinance__c = false;
            refiParams.Last_Refinance_Eligibility_Review__c = Date.today();
            insert refiParams;
        } else {
            loan_Refinance_Params__c refiParams2 = [SELECT Contract__c, Id, FICO__c, Grade__c, New_FICO__c, New_Grade__c FROM loan_Refinance_Params__c WHERE Contract__c = : testContract.Id LIMIT 1];
            refiParams2.FICO__c = 720;
            refiParams2.Grade__c = 'B1';
            refiParams2.New_FICO__c = 720;
            refiParams2.New_Grade__c = 'B1';
            update refiParams2;
        }
        
        Scorecard__c scoreRefi = new Scorecard__c();
        scoreRefi.Grade__c = 'B1';
        scoreRefi.Opportunity__c = appRefi.Id;
        scoreRefi.Acceptance__c = 'Y';
        scoreRefi.FICO_Score__c = 720;
        scoreRefi.Contact__c = appRefi.Contact__c;
        insert scoreRefi;
        
        ScorecardUtil.regenerateOffersRefinance(appRefi.id, false);
    }
    
    @isTest static void gets_decision_pre_brms_app() {
        
        LibraryTest.createBrmsConfigSettings(2, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity app = LibraryTest.createApplicationTH();
        
        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);
        string decision = ScorecardUtil.appDecision(creditReport);
        
        System.assertEquals(decision, 'Yes');
    }
    
    @isTest static void updates_acceptance_for_hard_pull() {
        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'New';
        update app;
        
        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);
        
        Contact cntct = [SELECT Id, Name FROM Contact WHERE Id = : app.Contact__c LIMIT 1];
        
        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Acceptance__c = 'Y';
        score.Opportunity__c = app.id;
        score.ProdAnnualIncome__c = -1.041877;
        score.ProdCreditScore__c = 0;
        score.ProdDTI_Ratio__c = 0.1;
        score.ProdHCNC__c = 1;
        score.ProdInquiries_last_6_months__c = 1;
        score.ProdInst_Trades_Opened__c = 1;
        score.ProdSelfEmployed__c = 1;
        score.ProdTotal_Trade_Lines__c = 10;
        score.Logit__c = -1.79;
        score.Unit_Bad_Probability__c = 0.143;
        score.Dollar_Rate_Bad_Probability__c = 7.15;
        score.FICO_Score__c = 600;
        score.Contact__c = cntct.id;
        insert score;
        
        ScorecardUtil.updateAcceptanceForHardPull(creditReport);
        
        List<Scorecard__c> sc1 = new List<Scorecard__c>();
        sc1 = [SELECT id, Opportunity__c FROM Scorecard__c WHERE Opportunity__c = :app.id];
        System.assertEquals(sc1.size() > 0, true);
        
        ScorecardUtil.deleteScorecard(app.Id);
        
        List<Scorecard__c> sc = new List<Scorecard__c>();
        sc = [SELECT id, Opportunity__c FROM Scorecard__c WHERE Opportunity__c = :app.id];
        System.assertEquals(sc.size() == 0, true);
    }
    
    @isTest static void deletes_refinance_offer() {
        Opportunity app = LibraryTest.createApplicationTH();
        
        Offer__c off = new Offer__c();
        off.Repayment_Frequency__c = 'Monthly';
        off.APR__c = 22.95;
        off.Term__c = 24;
        off.Payment_Amount__c = 250;
        off.IsSelected__c = false;
        off.Selected_Date__c = Date.today().addDays(-1);
        off.Loan_Amount__c = 10000;
        off.Fee__c = 500;
        off.IsCustom__c = false;
        off.Fee_Percent__c = 5;
        off.FirstPaymentDate__c = Date.today().addDays(15);
        off.Installments__c = 24;
        off.Opportunity__c = app.id;
        insert off;
        
        RefinanceSnapshotOffer__c refiSnap = new RefinanceSnapshotOffer__c();
        refiSnap.APR__c = 22.95;
        refiSnap.Opportunity__c = app.Id;
        refiSnap.Fee__c = 500;
        refiSnap.Fee_Percent__c = 5;
        refiSnap.Loan_Amount__c = 10000;
        refiSnap.Offer__c = off.Id;
        refiSnap.Payment_Amount__c = 250;
        refiSnap.Repayment_Frequency__c = 'Monthly';
        refiSnap.Term__c = 24;
        insert refiSnap;
        
        ScorecardUtil.deleteRefinanceOffers(app.Id);
        
        System.assertEquals(0, [SELECT COUNT() FROM RefinanceSnapshotOffer__c]);
    }
    
    @isTest static void generates_offers_monthly() {
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Default_Payment_Frequency__c = '30 days';
        acc.Is_Parent_Partner__c = true;
        acc.Type = 'Partner';
        insert acc;
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Partner_Account__c = acc.Id;
        update app;
        
        ProxyApiOfferCatalog cat = new ProxyApiOfferCatalog();
        cat.offerCatalogId = 1;
        cat.state = 'GA';
        cat.annualIncome = 100000;
        cat.grade = 'B1';
        cat.loanAmount = 10000;
        cat.term = 24;
        cat.apr = 22.95;
        cat.feePercent = 5;
        cat.feeAmount = 500;
        cat.paymentAmount = 250;
        cat.maxPti = 5;
        cat.maxFundingAmount = 25000;
        cat.partner = 0;
        cat.installments = 24;
        cat.ear = 30.35;
        cat.AONFeePct = 0;
        cat.AONPaymentAmount = 0;
        
        List<ProxyApiOfferCatalog> catList = new List<ProxyApiOfferCatalog>();
        catList.add(cat);
        
        ScorecardUtil.generateOffers(app.Id, catList);
        
        System.assert([SELECT COUNT() FROM Offer__c] > 0, 'Generate Offers');
    }
    
    @isTest static void generates_offers_weekly() {
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Default_Payment_Frequency__c = 'Weekly';
        acc.Is_Parent_Partner__c = true;
        acc.Type = 'Partner';
        insert acc;
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Partner_Account__c = acc.Id;
        update app;
        
        ProxyApiOfferCatalog cat = new ProxyApiOfferCatalog();
        cat.offerCatalogId = 1;
        cat.state = 'GA';
        cat.annualIncome = 100000;
        cat.grade = 'B1';
        cat.loanAmount = 10000;
        cat.term = 24;
        cat.apr = 22.95;
        cat.feePercent = 5;
        cat.feeAmount = 500;
        cat.paymentAmount = 250;
        cat.maxPti = 5;
        cat.maxFundingAmount = 25000;
        cat.partner = 0;
        cat.installments = 26;
        cat.ear = 30.35;
        cat.AONFeePct = 0;
        cat.AONPaymentAmount = 0;
        
        List<ProxyApiOfferCatalog> catList = new List<ProxyApiOfferCatalog>();
        catList.add(cat);
        
        ScorecardUtil.generateOffers(app.Id, catList);
        
        System.assert([SELECT COUNT() FROM Offer__c] > 0, 'Generate Offers');
    }
    
    @isTest static void generates_offers_biweekly() {
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Default_Payment_Frequency__c = 'Bi-weekly';
        acc.Is_Parent_Partner__c = true;
        acc.Type = 'Partner';
        insert acc;
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Partner_Account__c = acc.Id;
        update app;
        
        ProxyApiOfferCatalog cat = new ProxyApiOfferCatalog();
        cat.offerCatalogId = 1;
        cat.state = 'GA';
        cat.annualIncome = 100000;
        cat.grade = 'B1';
        cat.loanAmount = 10000;
        cat.term = 24;
        cat.apr = 22.95;
        cat.feePercent = 5;
        cat.feeAmount = 500;
        cat.paymentAmount = 250;
        cat.maxPti = 5;
        cat.maxFundingAmount = 25000;
        cat.partner = 0;
        cat.installments = 52;
        cat.ear = 30.35;
        cat.AONFeePct = 0;
        cat.AONPaymentAmount = 0;
        
        List<ProxyApiOfferCatalog> catList = new List<ProxyApiOfferCatalog>();
        catList.add(cat);
        
        ScorecardUtil.generateOffers(app.Id, catList);
        
        System.assert([SELECT COUNT() FROM Offer__c] > 0, 'Generate Offers');
    }
    
    @isTest static void generates_offers_semi_monthly() {
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Default_Payment_Frequency__c = 'Semi-Monthly';
        acc.Is_Parent_Partner__c = true;
        acc.Type = 'Partner';
        insert acc;
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Partner_Account__c = acc.Id;
        update app;
        
        ProxyApiOfferCatalog cat = new ProxyApiOfferCatalog();
        cat.offerCatalogId = 1;
        cat.state = 'GA';
        cat.annualIncome = 100000;
        cat.grade = 'B1';
        cat.loanAmount = 10000;
        cat.term = 24;
        cat.apr = 22.95;
        cat.feePercent = 5;
        cat.feeAmount = 500;
        cat.paymentAmount = 250;
        cat.maxPti = 5;
        cat.maxFundingAmount = 25000;
        cat.partner = 0;
        cat.installments = 48;
        cat.ear = 30.35;
        cat.AONFeePct = 0;
        cat.AONPaymentAmount = 0;
        
        List<ProxyApiOfferCatalog> catList = new List<ProxyApiOfferCatalog>();
        catList.add(cat);
        
        ScorecardUtil.generateOffers(app.Id, catList);
        
        System.assert([SELECT COUNT() FROM Offer__c] > 0, 'Generate Offers');
    }
    
}