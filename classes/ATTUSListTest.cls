@isTest
private class ATTUSListTest {
   public static testMethod void listCheckName()
    {
        ATTUSWatchDOGSFLookup ls = null;
        try
        {       
        test.startTest();
        ls = new ATTUSWatchDOGSFLookup();
        ls.street = 'Street Address';
        ls.city='City';
        ls.state = 'ST';
        ls.postalcode = '28079';
        ls.country = 'Syria';
       
        httprequest req = ls.createRequestBus('Salesforce.com');  
        System.assertnotequals('', req.getbody());    
        req = ls.createRequestInd('Usama','bin Ladin');
        System.assertnotequals('', req.getbody());           
        req = ls.createRequestInd('Jorge','Smith');
        System.assertnotequals('', req.getbody());    
        req = ls.createRequestBus('Salesforce.com');
        System.assertnotequals('', req.getbody());    
        req = ls.createRequestBus('ACEFROSTY SHIPPING');
        System.assertnotequals('', req.getbody());    
        
        string xmlResp = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ATTUSWATCHDOGRESPONSE tranid="317302009" trandate="2011-04-01 08:15:21"><BILLINGCODE partnerid="0f066555-caee-439f-88d7-7ce72767592b " billingid="0f066555-caee-439f-88d7-7ce72767592b" clientdefined1="SF Testing" clientdefined2="" clientdefined3="" /><LISTS><LIST code="OFAC" /></LISTS><QUERY><ITEM operator="" threshold=".85" surname="bin Ladin" givenname="Usama" unparsedname="" othername="" allentities="" id="" state="" city="" postalcode="" country="" blockedcountryname="" addressdisqualification="true" addressmatch="true"/></QUERY><SEARCHCRITERIA text="search criteria text"><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-03-31 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-03-31" /><ENTITIES><ENTITY entityid="755" entitynum="6365" thisentitysmaxscore="1.000"><LASTNAME>BIN LADIN</LASTNAME><FIRSTNAME tnode="junk" >Usama bin Muhammad bin Awad</FIRSTNAME><OTHERNAME /><WHOLENAME /><ISALIASHIT>1</ISALIASHIT><SCORE>1.000</SCORE><TYPE>individual</TYPE><PROGRAMS>SDGT(Specially Designated Global Terrorist); SDT(Specially Designated Terrorist)</PROGRAMS><REMARKS>Modified On or Before: 10/26/2001; DOB 30 Jul 1957; alt. DOB 1958; POB Jeddah, Saudi Arabia; alt. POB Yemen.</REMARKS><ALIASES><ALIAS name="aka, BIN LADEN, Osama" type="individual" score="0.865" /><ALIAS name="aka, BIN LADEN, Usama" type="individual" score="0.942" /><ALIAS name="aka, BIN LADIN, Osama" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Osama bin Muhammad bin Awad" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Usama" type="individual" score="1.000" /></ALIASES><ADDRESSES /></ENTITY></ENTITIES></SEARCHCRITERIA></ATTUSWATCHDOGRESPONSE></soap:Body></soap:Envelope>';
        ls.parseDOC(xmlResp);        
        xmlResp = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><soap:Fault><faultcode>soap:Client</faultcode><faultstring>Server did not recognize the value of HTTP Header SOAPAction: ProcessNameListMessage_An.</faultstring><detail /></soap:Fault></soap:Body></soap:Envelope>';
        ls.parseDOC(xmlResp);
        xmlResp = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ATTUSWATCHDOGRESPONSE tranid="317302034" trandate="2011-04-01 09:08:20"><BILLINGCODE partnerid="0f066555-caee-439f-88d7-7ce72767592b " billingid="0f066555-caee-439f-88d7-7ce72767592b" clientdefined1="SF Testing" clientdefined2="" clientdefined3="" /><LISTS><LIST code="OFAC" /></LISTS><QUERY><ITEM operator="" threshold=".85" surname="" givenname="" unparsedname="" othername="" allentities="ATTUS Technologies" id="" state="" city="" postalcode="" country="" blockedcountryname="" /></QUERY><SEARCHCRITERIA><NUMRECORDS totalhits="0" totalentityhits="0" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-03-31 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-03-31" /></SEARCHCRITERIA></ATTUSWATCHDOGRESPONSE></soap:Body></soap:Envelope>';
        ls.parseDOC(xmlResp);
        xmlResp = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ATTUSWATCHDOGRESPONSE tranid="317302038" trandate="2011-04-01 09:11:10"><BILLINGCODE partnerid="0f066555-caee-439f-88d7-7ce72767592b " billingid="0f066555-caee-439f-88d7-7ce72767592b" clientdefined1="SF Testing" clientdefined2="" clientdefined3="" /><LISTS><LIST code="OFAC" /></LISTS><QUERY><ITEM operator="" threshold=".85" surname="" givenname="" unparsedname="" othername="" allentities="ACEFROSTY SHIPPING" id="" state="" city="" postalcode="" country="" blockedcountryname="" /></QUERY><SEARCHCRITERIA><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-03-31 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-03-31" /><ENTITIES><ENTITY entityid="5" entitynum="25" thisentitysmaxscore="0.980"><LASTNAME /><FIRSTNAME /><OTHERNAME>ACEFROSTY SHIPPING CO., LTD.</OTHERNAME><WHOLENAME /><ISALIASHIT>0</ISALIASHIT><SCORE>0.980</SCORE><TYPE>other</TYPE><PROGRAMS>CUBA(Cuba)</PROGRAMS><REMARKS>Modified On or Before: 11/15/2002; </REMARKS><ALIASES count="3" /><ADDRESSES><ADDRESS streetaddress="171 Old Bakery Street" city="Valletta" country="Malta" remark="" state="" zip="" /></ADDRESSES></ENTITY></ENTITIES></SEARCHCRITERIA></ATTUSWATCHDOGRESPONSE></soap:Body></soap:Envelope>';
        ls.parseDOC(xmlResp);
        xmlResp = '<?xml version="1.0" encoding="UTF-8"?><SEARCHCRITERIA><NUMRECORDS totalhits="6" totalentityhits="6" totalblockedcountryhits="0"/><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-04-20 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-04-20"/><ENTITIES><ENTITY entityid="261" entitynum="4107" thisentitysmaxscore="1.000"><LASTNAME text="RODRIGUEZ OREJUELA"/><FIRSTNAME text="Gilberto Jose"/><OTHERNAME/><WHOLENAME/><ISALIASHIT text="1"/><SCORE text="1.000"/><TYPE text="individual"/><PROGRAMS text="SDNT(Specially Designated Narcotics Traffickers)"/><REMARKS text="Modified On or Before: 11/15/2002; DOB 31 Jan 1939"/><ALIASES/><ADDRESSES><ADDRESS streetaddress="" city="Cali" country="Colombia" remark="" state="" zip=""/></ADDRESSES></ENTITY><ENTITY entityid="621" entitynum="4607" thisentitysmaxscore="1.000"><LASTNAME text="LEAL RODRIGUEZ"/><FIRSTNAME text="Jose Guillermo"/><OTHERNAME/><WHOLENAME/><ISALIASHIT text="0"/><ISADDRESSHIT text="0" /><SCORE text="1.000"/><TYPE text="individual"/><PROGRAMS text="SDNT(Specially Designated Narcotics Traffickers)"/><REMARKS text="Modified On or Before: 11/15/2002"/><ALIASES/><ADDRESSES><ADDRESS streetaddress="c/o DECAFARMA S.A." city="Bogota" country="Colombia" remark="" state="" zip=""/><ADDRESS streetaddress="c/o LABORATORIOS KRESSFOR DE COLOMBIA S.A." city="Bogota" country="Colombia" remark="" state="" zip=""/><ADDRESS streetaddress="c/o PENTA PHARMA DE COLOMBIA S.A." city="Bogota" country="Colombia" remark="" state="" zip=""/><ADDRESS streetaddress="c/o PENTACOOP LTDA." city="Bogota" country="Colombia" remark="Stuff" state="" zip="" score="0.976" /></ADDRESSES></ENTITY><ENTITY entityid="3576" entitynum="10949" thisentitysmaxscore="1.000"><LASTNAME text="CORREDOR IBAGUE"/><FIRSTNAME text="Jose Maria"/><OTHERNAME/><WHOLENAME/><ISALIASHIT text="0"/><SCORE text="1.000"/><TYPE text="individual"/><PROGRAMS text="SDNTK(Specially Designated Narcotics Traffickers - Kingpin Act)"/><REMARKS text="Modified On: 07/31/2008"/><ALIASES/><ADDRESSES><ADDRESS streetaddress="" city="" country="Colombia" remark="" state="" zip=""/></ADDRESSES></ENTITY><ENTITY entityid="195" entitynum="1811" thisentitysmaxscore="0.942"><LASTNAME text="RODRIQUEZ"/><FIRSTNAME text="Jose Julio"/><OTHERNAME/><WHOLENAME/><ISALIASHIT text="0"/><SCORE text="0.942"/><TYPE text="individual"/><PROGRAMS text="CUBA(Cuba)"/><REMARKS text="Modified On or Before: 11/15/2002; ; Chairman, Havana International Bank."/><ALIASES/><ADDRESSES><ADDRESS streetaddress="20 Ironmonger Lane" city="London" country="United Kingdom" remark="" state="" zip="EC2V 8EY"/></ADDRESSES></ENTITY><ENTITY entityid="3144" entitynum="10403" thisentitysmaxscore="0.857"><LASTNAME text="RODRIGUEZ MENDIETA"/><FIRSTNAME text="Jorge Enrique"/><OTHERNAME/><WHOLENAME/><ISALIASHIT text="0"/><SCORE text="0.857"/><TYPE text="individual"/><PROGRAMS text="SDNTK(Specially Designated Narcotics Traffickers - Kingpin Act)"/><REMARKS text="Modified On: 11/05/2007; DOB 15 Jan 1963"/><ALIASES/><ADDRESSES/></ENTITY><ENTITY entityid="4036" entitynum="11504" thisentitysmaxscore="0.857"><LASTNAME text="LOPEZ RODRIGUEZ"/><FIRSTNAME text="Jorge Octavio"/><OTHERNAME/><WHOLENAME/><ISALIASHIT text="0"/><SCORE text="0.857"/><TYPE text="individual"/><PROGRAMS text="SDNT(Specially Designated Narcotics Traffickers)"/><REMARKS text="Modified On: 06/12/2009; DOB 01 Apr 1976; alt. DOB 01 Jan 1976"/><ALIASES/><ADDRESSES><ADDRESS streetaddress="c/o CIMIENTOS LA TORRE S.A. DE C.V." city="Guadalajara" country="Mexico" remark="" state="Jalisco" zip=""/><ADDRESS streetaddress="c/o CUMBRES SOLUCIONES INMOBILIARIAS S.A. DE C.V." city="Zapopan" country="Mexico" remark="" state="Jalisco" zip=""/><ADDRESS streetaddress="Calle Aurora y Andres, Benito Juarez" city="Quintana Roo" country="Mexico" remark="" state="" zip=""/><ADDRESS streetaddress="Calle Boyero No. 3500, Torre 4, Dpto. 2, Fraccionamiento La Calma" city="Zapopan"  country="Mexico" remark="" state="Jalisco" zip=""/></ADDRESSES></ENTITY></ENTITIES>><COUNTRIES><COUNTRY countryid="17" countrycodeISO2="SY" countrycodeISO3="SYR" countrycodeNum="760"><NAME text="SYRIA" score="1.000" /><DESCRIPTION text="Syria" /></COUNTRY></COUNTRIES></SEARCHCRITERIA>';
        ls.getHTMLTable(xmlResp);
        xmlResp = '<?xml version="1.0" encoding="UTF-8"?><SEARCHCRITERIA><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-04-20 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-04-20" /><ENTITIES><ENTITY entityid="755" entitynum="6365" thisentitysmaxscore="1.000"><LASTNAME text="BIN LADIN" /><FIRSTNAME text="Usama bin Muhammad bin Awad" /><OTHERNAME text="Other Name"/><WHOLENAME text="Whole Name"/><ISALIASHIT text="1" /><ISADDRESSHIT text="1" /><SCORE text="1.000" /><TYPE text="individual" /><PROGRAMS text="SDGT(Specially Designated Global Terrorist); SDT(Specially Designated Terrorist)" /><REMARKS text="Modified On or Before: 10/26/2001; DOB 30 Jul 1957; alt. DOB 1958; POB Jeddah, Saudi Arabia; alt. POB Yemen." /><ALIASES><ALIAS name="aka, BIN LADEN, Osama" type="individual" score="0.865" /><ALIAS name="aka, BIN LADEN, Usama" type="individual" score="0.942" /><ALIAS name="aka, BIN LADIN, Osama" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Osama bin Muhammad bin Awad" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Usama" type="individual" score="1.000" /></ALIASES><ADDRESSES /></ENTITY></ENTITIES></SEARCHCRITERIA>';
        ls.getHTMLTable(xmlResp);
        xmlResp = '<?xml version="1.0" encoding="UTF-8"?><SEARCHCRITERIA><NUMRECORDS totalhits="0" totalentityhits="0" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-04-20 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-04-20" /><ENTITIES><ENTITY entityid="755" entitynum="6365" thisentitysmaxscore="1.000"><LASTNAME text="BIN LADIN" /><FIRSTNAME text="Usama bin Muhammad bin Awad" /><OTHERNAME text="Other Name"/><WHOLENAME text="Whole Name"/><ISALIASHIT text="1" /><ISADDRESSHIT text="1" /><SCORE text="1.000" /><TYPE text="individual" /><PROGRAMS text="SDGT(Specially Designated Global Terrorist); SDT(Specially Designated Terrorist)" /><REMARKS text="Modified On or Before: 10/26/2001; DOB 30 Jul 1957; alt. DOB 1958; POB Jeddah, Saudi Arabia; alt. POB Yemen." /><ALIASES><ALIAS name="aka, BIN LADEN, Osama" type="individual" score="0.865" /><ALIAS name="aka, BIN LADEN, Usama" type="individual" score="0.942" /><ALIAS name="aka, BIN LADIN, Osama" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Osama bin Muhammad bin Awad" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Usama" type="individual" score="1.000" /></ALIASES><ADDRESSES /></ENTITY></ENTITIES></SEARCHCRITERIA>';
        string data = ls.getHTMLTable(xmlResp);
        System.assertnotequals('', data);    
        ls.parsedoc('');
        }
        catch (Exception ex)
        {
         System.Debug(ex);  
        }
        
        
        test.stopTest();
    }
   public static testMethod void testContactNew()
    {
    test.starttest();
    system.debug('NEW CONTACT************************');

    //create an accoun to use for this test
    Account acct = new account(name='acme');
    insert acct;
    
    //create a contact to use for this test
    Contact cont = new contact(firstname = 'James', lastname = 'Rodriguez', accountid = acct.id);
    insert cont;
    
     ATTUSWDSFLookup__c layout = new ATTUSWDSFLookup__c();
     layout.Contact__c = cont.id;

    //create controller for contact
    ApexPages.StandardController sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    ATTUSNewLookupCtrlContact myPageCon = new ATTUSNewLookupCtrlContact(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlContact(sc, true, true);
    system.assertequals(null,myPageCon.checkList());
    
    
    cont = new contact(firstname = 'Jose', lastname = 'Jones', mailingstreet='131 Main Street', mailingcity='Saltillo', mailingstate='TN', mailingpostalcode='38370', mailingcountry='US', 
    otherstreet='Other Street', othercity='Memphis', otherstate='MS', otherpostalcode='34567', othercountry='CUBA', accountid = acct.id);
    insert cont;
    
     layout = new ATTUSWDSFLookup__c();
     layout.Contact__c = cont.id;

    //create controller for contact
    sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    //check base constructor
    myPageCon = new ATTUSNewLookupCtrlContact(sc);
    myPageCon = new ATTUSNewLookupCtrlContact(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlContact(sc, true, true);
    system.assertequals(null,myPageCon.checkList());
    cont = new contact(firstname = 'Jose', lastname = 'Jones', mailingstreet='131 Main Street', mailingcity='Saltillo', mailingstate='TN', mailingpostalcode='38370', mailingcountry='IRAN', 
    otherstreet='Other Street', othercity='Memphis', otherstate='MS', otherpostalcode='34567', othercountry='USA', accountid = acct.id);
    insert cont;

     layout = new ATTUSWDSFLookup__c();
     layout.Contact__c = cont.id;

    //create controller for contact
    sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    //check base constructor
    myPageCon = new ATTUSNewLookupCtrlContact(sc);
    myPageCon = new ATTUSNewLookupCtrlContact(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlContact(sc, true, true);
    system.assertequals(null,myPageCon.checkList());

    cont = new contact(firstname = 'Jose', lastname = 'Jones', 
    otherstreet='Other Street', othercity='Memphis', otherstate='MS', otherpostalcode='34567', othercountry='USA', accountid = acct.id);
    insert cont;
    
    layout = new ATTUSWDSFLookup__c();
    layout.Contact__c = cont.id;

    //create controller for contact
    sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    //check base constructor
    myPageCon = new ATTUSNewLookupCtrlContact(sc);
    myPageCon = new ATTUSNewLookupCtrlContact(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlContact(sc, true, true);
    system.assertequals(null,myPageCon.checkList());

    mypagecon.goback();    
    test.stoptest();
    }

    public static testMethod void testContactEdit()
    {
    test.starttest();
    system.debug('EDIT CONTACT************************');

    //create an accoun to use for this test
    Account acct = new account(name='al-rashead trust');
    insert acct;
    
    //create a contact to use for this test
    Contact cont = new contact(firstname = 'Jose', lastname = 'Rodriguez', accountid = acct.id);
    insert cont;
    

    ATTUSWDSFLookup__c layout = new ATTUSWDSFLookup__c();
            layout.Contact__c = cont.id;
            layout.List_Date__c = date.today();
            layout.Number_of_Hits__c = 0;
            layout.List_Description__c = 'OFAC';
            layout.Result_Node__c = '<?xml version="1.0" encoding="UTF-8"?><SEARCHCRITERIA><NUMRECORDS totalhits="0" totalentityhits="0" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="England HM Treasury Sanction List" description="2011-04-20 England HM Treasury Source Data" listcode="ENGLAND" threshold="0.85" listdate="2011-04-20" /><ENTITIES><ENTITY entityid="113" entitynum="20137" thisentitysmaxscore="1.000"><LASTNAME text="BIN LADEN" /><FIRSTNAME text="Usama Muhammed Awad" /><OTHERNAME /><WHOLENAME /><ISALIASHIT text="1" /><SCORE text="0.942" /><TYPE text="individual" /><PROGRAMS text="HMT_ALQ(HM Treasury AL QAIDA and Taliban Sanction)" /><REMARKS text="DOB 01/01/1957; DOB 03/10/1957; DOB 1956; DOB 1957; DOB 07/28/1957; DOB 07/30/1957; POB (1) - (2) Jeddah (1) Yemen (2) Saudi Arabia; Other Info: UN Ref QI.B.8.01. Saudi citizenship withdrawn. Afghan nationality given by the Taliban regime. Also referred to as Al Qaqa; GroupID: 6896; Listed On: 02/23/2001; Last Updated: 02/10/2011" /><ALIASES><ALIAS name="aka, ABD AL-HAKIM, Abu Abdallah" type="individual" score="0.000" /><ALIAS name="aka, AWDH, Bin Laden Osama Mohamed" type="individual" score="0.000" /><ALIAS name="aka, BIN LADEN, Usama" type="individual" score="0.942" /><ALIAS name="aka, BIN LADIN, Osama" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Osama bin Muhammad bin Awad" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Shaykh Usama" type="individual" score="1.000" /><ALIAS name="aka, BIN LADIN, Usama" type="individual" score="1.000" /><ALIAS name="aka, BIN LADIN, Usama bin Muhammad bin Awad" type="individual" score="1.000" /><ALIAS name="aka, BIN LADIN, Usamah Bin Muhammad" type="individual" score="0.946" /><ALIAS name="aka, BIN MUHAMMAD, Bin Laden Usamah" type="individual" score="0.000" /><ALIAS name="aka, OSAMA, Ben Laden" type="individual" score="0.000" /><ALIAS name="aka, OSSAMA, Ben Laden" type="individual" score="0.000" /><ALIAS name="aka, USAMA BIN MUHAMMED BIN AWAD, Osama Bin Laden" type="individual" score="0.000" /><ALIAS name="aka, USAMA, Ben Laden" type="individual" score="0.000" /></ALIASES><ADDRESSES /></ENTITY></ENTITIES></SEARCHCRITERIA>';            layout.name_searched__c = cont.firstname + ' ' + cont.lastname;
            layout.lookup_by__c = 'Joe';
            layout.review_status__c = 'No Match';
            insert layout;

    //create controller for contact
    ApexPages.StandardController sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    ATTUSEditLookupCtrlContact myPageCon = new ATTUSEditLookupCtrlContact(sc);

    layout = new ATTUSWDSFLookup__c();
            layout.Contact__c = cont.id;
            layout.List_Date__c = date.today();
            layout.Number_of_Hits__c = 1;
            layout.List_Description__c = 'OFAC';
            layout.Result_Node__c = '<?xml version="1.0" encoding="UTF-8"?><SEARCHCRITERIA><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="England HM Treasury Sanction List" description="2011-04-20 England HM Treasury Source Data" listcode="ENGLAND" threshold="0.85" listdate="2011-04-20" /><ENTITIES><ENTITY entityid="113" entitynum="20137" thisentitysmaxscore="1.000"><LASTNAME text="BIN LADEN" /><FIRSTNAME text="Usama Muhammed Awad" /><OTHERNAME /><WHOLENAME /><ISALIASHIT text="1" /><SCORE text="0.942" /><TYPE text="individual" /><PROGRAMS text="HMT_ALQ(HM Treasury AL QAIDA and Taliban Sanction)" /><REMARKS text="DOB 01/01/1957; DOB 03/10/1957; DOB 1956; DOB 1957; DOB 07/28/1957; DOB 07/30/1957; POB (1) - (2) Jeddah (1) Yemen (2) Saudi Arabia; Other Info: UN Ref QI.B.8.01. Saudi citizenship withdrawn. Afghan nationality given by the Taliban regime. Also referred to as Al Qaqa; GroupID: 6896; Listed On: 02/23/2001; Last Updated: 02/10/2011" /><ALIASES><ALIAS name="aka, ABD AL-HAKIM, Abu Abdallah" type="individual" score="0.000" /><ALIAS name="aka, AWDH, Bin Laden Osama Mohamed" type="individual" score="0.000" /><ALIAS name="aka, BIN LADEN, Usama" type="individual" score="0.942" /><ALIAS name="aka, BIN LADIN, Osama" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Osama bin Muhammad bin Awad" type="individual" score="0.923" /><ALIAS name="aka, BIN LADIN, Shaykh Usama" type="individual" score="1.000" /><ALIAS name="aka, BIN LADIN, Usama" type="individual" score="1.000" /><ALIAS name="aka, BIN LADIN, Usama bin Muhammad bin Awad" type="individual" score="1.000" /><ALIAS name="aka, BIN LADIN, Usamah Bin Muhammad" type="individual" score="0.946" /><ALIAS name="aka, BIN MUHAMMAD, Bin Laden Usamah" type="individual" score="0.000" /><ALIAS name="aka, OSAMA, Ben Laden" type="individual" score="0.000" /><ALIAS name="aka, OSSAMA, Ben Laden" type="individual" score="0.000" /><ALIAS name="aka, USAMA BIN MUHAMMED BIN AWAD, Osama Bin Laden" type="individual" score="0.000" /><ALIAS name="aka, USAMA, Ben Laden" type="individual" score="0.000" /></ALIASES><ADDRESSES /></ENTITY></ENTITIES></SEARCHCRITERIA>';            layout.name_searched__c = cont.firstname + ' ' + cont.lastname;
            layout.lookup_by__c = 'Joe';
            layout.review_status__c = 'No Match';
            insert layout;


    //create controller for contact
    sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    myPageCon = new ATTUSEditLookupCtrlContact(sc);
    system.assertnotequals(null,myPageCon.save());
    layout.review_status__c = 'True Match';
    system.assertnotequals(null,myPageCon.Save());
    layout.review_status__c = null;
    system.assertequals(null,myPageCon.Save());
    layout.lookup_by__c = '';
    system.assertequals(null,myPageCon.Save());
    
    test.stoptest();
    }   
    public static testMethod void testAccountNew()
    {
    system.debug('NEW ACCOUNT************************');
    //create an accoun to use for this test
    Account acct = new account(name='al-rashead trust');
    insert acct;
    
    ATTUSWDSAccount__c layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;

        
    ApexPages.StandardController sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    ATTUSNewLookupCtrlAccount myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());
    
    acct = new account(name='al-rashead trust', billingstreet='Billing Street', billingcity='Salisbury', billingstate='WV', billingpostalcode='34598',
    billingcountry='U.S.A.', shippingstreet='Shipping Street', shippingcity='Islamabad', shippingpostalcode='598235793-F3', shippingstate='LA', shippingcountry='US');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;

    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());
    acct = new account(name='al-rashead trust', billingstreet='Billing Street', billingcity='Salisbury', billingstate='WV', billingpostalcode='34598',
    billingcountry='IRAN', shippingstreet='Shipping Street', shippingcity='Islamabad', shippingpostalcode='598235793-F3', shippingstate='LA', shippingcountry='CUBA');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;
    
    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());

    acct = new account(name='al-rashead trust', 
    shippingstreet='Shipping Street', shippingcity='Islamabad', shippingpostalcode='598235793-F3', shippingstate='LA', shippingcountry='U.S.A.');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;
    
    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(myPageCon.checkList(),null);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());

    acct = new account(name='al-rashead trust', 
    shippingstreet='Shipping Street', shippingcity='Islamabad', shippingpostalcode='598235793-F3', shippingstate='LA', shippingcountry='Peru');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;
    
    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(myPageCon.checkList(),null);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());


    acct = new account(name='al-rashead trust', 
    shippingstreet='Shipping Streetghjklghjklshgljksdfhgsdfhgsdfhgjkhsdfljghsdfjklghdfjkghdfjghsdjklhuieyguiohviushvuivbosiusiob iosubiperub', shippingcity='Islamabad', shippingpostalcode='598235793-F3', shippingstate='LA', shippingcountry='U.S.A.');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;
    
    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());

    acct = new account(name='al-rashead trust', billingstreet='Billing Street', billingcity='Salisbury', billingstate='WV', billingpostalcode='34598',
    billingcountry='U.S.A.');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;
    
    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());

    acct = new account(name='al-rashead trust', billingstreet='Billing Street', billingcity='Salisbury', billingstate='WV', billingpostalcode='34598',billingcountry='Cuba');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;
    
    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(myPageCon.checkList(),null);

    acct = new account(name='al-rashead trust', billingstreet='Billing Street blajhgljhgjksdfhgjklhfghsdjklghsjklghsjklhgjkshdgjklshdjklghsjkfghsjkhgsjklhgsjkldhgjklshgkljsdhgljkshdgjklshlghjklghskldjhhsgljkdghsljkdgh', billingcity='Salisbury', billingstate='WV', billingpostalcode='34598',
    billingcountry='U.S.A.');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;
    
    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(null,myPageCon.checkList());
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());

    acct = new account(name='al-rashead trust', billingstreet='Billing Street', billingcity='Salisbury', billingstate='WV', billingpostalcode='34598',
    billingcountry='U.S.A.');
    insert acct;
    
     layout = new ATTUSWDSAccount__c();
     layout.account__c = acct.id;    
    
    sc = new ApexPages.standardController(layout);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, false);
    system.assertnotequals(myPageCon.checkList(),null);
    myPageCon = new ATTUSNewLookupCtrlAccount(sc, true, true);
    system.assertequals(null,myPageCon.checkList());

    system.assertnotequals(null,mypagecon.goback());
    
    }
    public static testMethod void testAccountEdit()
    {
    test.starttest();
    Account acct = new account(name='al-rashead trust');
    insert acct;
    
    ATTUSWDSAccount__c layout = new ATTUSWDSAccount__c();
            layout.account__c = acct.id;
            layout.List_Date__c = date.today();
            layout.Number_of_Hits__c = 0;
            layout.List_Description__c = 'OFAC';
            layout.Result_Node__c = '<?xml version="1.0" encoding="UTF-8"?><SEARCHCRITERIA><NUMRECORDS totalhits="0" totalentityhits="0" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="England HM Treasury Sanction List" description="2011-04-27 England HM Treasury Source Data" listcode="ENGLAND" threshold="0.85" listdate="2011-04-27" /><ENTITIES><ENTITY entityid="285" entitynum="20371" thisentitysmaxscore="0.917"><LASTNAME /><FIRSTNAME /><OTHERNAME text="AL RASHID TRUST" /><WHOLENAME /><ISALIASHIT text="1" /><SCORE text="0.000" /><TYPE text="other" /><PROGRAMS text="HMT_ALQ(HM Treasury AL QAIDA and Taliban Sanction)" /><REMARKS text="Other Info: UN Ref QE.A.5.01. HQ in Pakistan. Operations in Afghanistan, Herat Jalalabad, Kabul, Kandahar, Mazar Sherif, Kosovo and Chechnya. Has two accounts 05501741 and 06500138 in Habib Bank Ltd, Foreign Exchange Branch. Involved in the financing of Al-Qaida and the Taliban; GroupID: 6968; Listed On: 10/10/2001; Last Updated: 12/12/2008" /><ALIASES><ALIAS name="aka, AID ORGANIZATION OF THE ULEMA, PAKISTAN" type="other" score="0.000" /><ALIAS name="aka, AL AMEEN TRUST" type="other" score="0.000" /><ALIAS name="aka, AL AMIN TRUST" type="other" score="0.000" /><ALIAS name="aka, AL AMIN WELFARE TRUST" type="other" score="0.000" /><ALIAS name="aka, AL MADINA TRUST" type="other" score="0.000" /><ALIAS name="aka, AL RASHEED TRUST" type="other" score="0.917" /><ALIAS name="aka, AL-AMEEN TRUST" type="other" score="0.000" /><ALIAS name="aka, AL-MADINA TRUST" type="other" score="0.000" /><ALIAS name="aka, AL-RASHEED TRUST" type="other" score="0.000" /><ALIAS name="aka, AL-RASHID TRUST" type="other" score="0.000" /></ALIASES><ADDRESSES><ADDRESS streetaddress="302b-40, Good Earth Court Opposite Pia Planitarium Block 13a Gulshan-I-Iqbal" city="Karachi" country="Pakistan" remark="Phone 4979263" state="" zip="" /><ADDRESS streetaddress="605 Landmark Plaza 11 Chundrigar Road Opposite Jang Building" city="Karachi" country="Pakistan" remark="Phone 2623818-19" state="" zip="" /><ADDRESS streetaddress="617 Clifton Center Block 5 6th Floor Clifton" city="Karachi" country="Pakistan" remark="Phone 587-2545" state="" zip="" /><ADDRESS streetaddress="Jamia Maajid Sulalman Park Melgium Pura" city="Lahore" country="Pakistan" remark="" state="" zip="" /><ADDRESS streetaddress="Jamia Masjid Sulaiman Park Begum Pura" city="Lahore" country="Pakistan" remark="Phone 042-6812081" state="" zip="" /><ADDRESS streetaddress="Kitab Ghar Darul Ifta Wal Irshad Nazimabad No 4" city="Karachi" country="Pakistan" remark="Phone 6683301 and 0300-8209199, Fax 6623814" state="" zip="" /><ADDRESS streetaddress="Kitas Ghar Nazimabad 4 Dahgel-Iftah" city="Karachi" country="Pakistan" remark="" state="" zip="" /></ADDRESSES></ENTITY></ENTITIES></SEARCHCRITERIA>';
            layout.name_searched__c = acct.name;
            layout.lookup_by__c = 'Joe';
            layout.review_status__c = 'No Match';
            insert layout;
            
                //create controller for contact
    ApexPages.StandardController sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    ATTUSEditLookupCtrlAccount myPageCon = new ATTUSEditLookupCtrlAccount(sc);


    layout = new ATTUSWDSAccount__c();
            layout.account__c = acct.id;
            layout.List_Date__c = date.today();
            layout.Number_of_Hits__c = 1;
            layout.List_Description__c = 'OFAC';
            layout.Result_Node__c = '<?xml version="1.0" encoding="UTF-8"?><SEARCHCRITERIA><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="England HM Treasury Sanction List" description="2011-04-27 England HM Treasury Source Data" listcode="ENGLAND" threshold="0.85" listdate="2011-04-27" /><ENTITIES><ENTITY entityid="285" entitynum="20371" thisentitysmaxscore="0.917"><LASTNAME /><FIRSTNAME /><OTHERNAME text="AL RASHID TRUST" /><WHOLENAME /><ISALIASHIT text="1" /><SCORE text="0.000" /><TYPE text="other" /><PROGRAMS text="HMT_ALQ(HM Treasury AL QAIDA and Taliban Sanction)" /><REMARKS text="Other Info: UN Ref QE.A.5.01. HQ in Pakistan. Operations in Afghanistan, Herat Jalalabad, Kabul, Kandahar, Mazar Sherif, Kosovo and Chechnya. Has two accounts 05501741 and 06500138 in Habib Bank Ltd, Foreign Exchange Branch. Involved in the financing of Al-Qaida and the Taliban; GroupID: 6968; Listed On: 10/10/2001; Last Updated: 12/12/2008" /><ALIASES><ALIAS name="aka, AID ORGANIZATION OF THE ULEMA, PAKISTAN" type="other" score="0.000" /><ALIAS name="aka, AL AMEEN TRUST" type="other" score="0.000" /><ALIAS name="aka, AL AMIN TRUST" type="other" score="0.000" /><ALIAS name="aka, AL AMIN WELFARE TRUST" type="other" score="0.000" /><ALIAS name="aka, AL MADINA TRUST" type="other" score="0.000" /><ALIAS name="aka, AL RASHEED TRUST" type="other" score="0.917" /><ALIAS name="aka, AL-AMEEN TRUST" type="other" score="0.000" /><ALIAS name="aka, AL-MADINA TRUST" type="other" score="0.000" /><ALIAS name="aka, AL-RASHEED TRUST" type="other" score="0.000" /><ALIAS name="aka, AL-RASHID TRUST" type="other" score="0.000" /></ALIASES><ADDRESSES><ADDRESS streetaddress="302b-40, Good Earth Court Opposite Pia Planitarium Block 13a Gulshan-I-Iqbal" city="Karachi" country="Pakistan" remark="Phone 4979263" state="" zip="" /><ADDRESS streetaddress="605 Landmark Plaza 11 Chundrigar Road Opposite Jang Building" city="Karachi" country="Pakistan" remark="Phone 2623818-19" state="" zip="" /><ADDRESS streetaddress="617 Clifton Center Block 5 6th Floor Clifton" city="Karachi" country="Pakistan" remark="Phone 587-2545" state="" zip="" /><ADDRESS streetaddress="Jamia Maajid Sulalman Park Melgium Pura" city="Lahore" country="Pakistan" remark="" state="" zip="" /><ADDRESS streetaddress="Jamia Masjid Sulaiman Park Begum Pura" city="Lahore" country="Pakistan" remark="Phone 042-6812081" state="" zip="" /><ADDRESS streetaddress="Kitab Ghar Darul Ifta Wal Irshad Nazimabad No 4" city="Karachi" country="Pakistan" remark="Phone 6683301 and 0300-8209199, Fax 6623814" state="" zip="" /><ADDRESS streetaddress="Kitas Ghar Nazimabad 4 Dahgel-Iftah" city="Karachi" country="Pakistan" remark="" state="" zip="" /></ADDRESSES></ENTITY></ENTITIES></SEARCHCRITERIA>';
            layout.name_searched__c = acct.name;
            layout.lookup_by__c = 'Joe';
            layout.review_status__c = 'No Match';
            insert layout;
            
                //create controller for contact
    sc = new ApexPages.standardController(layout);
    //create custom controller for doing lookup
    myPageCon = new ATTUSEditLookupCtrlAccount(sc);
    system.assertnotequals(null,myPageCon.save());
    layout.review_status__c = 'True Match';
    system.assertnotequals(null,myPageCon.save());
    layout.review_status__c = null;
    system.assertequals(null,myPageCon.save());
    layout.lookup_by__c = '';
    system.assertequals(null,myPageCon.Save());
    
    test.stoptest();
    }       
}