public class CallContract {

//@future(call=true)-7-7-2016

    public static void ContractCreation(String AppID, string ExpDisDate){
        string contactId;
        String  retMsg;
        user us;
        //  if(test.isrunningtest() || !test.isrunningtest()){-7-7-2016
        try{
            //Code added for logging   
            if(appId <> null && appId <> ''){
                Opportunity App= [select id,Contact__c from Opportunity where id=:appId];
                if(app <> null){
                    contactId=app.Contact__c ;
                }
                if(!test.isrunningtest()){
                    //genesis.ConvertApplicationCtrl ctrl = new genesis.ConvertApplicationCtrl();
                    //retMsg = ctrl.convertApplication(AppID);
                    retMsg = convertOpportunityToContractCtrl.createContract(AppID);
                    system.debug('ContractCreation@@@@ ' + retMsg);
                }
                MelisaCtrl.DoLogging('ContractGeneration',AppID,retMsg,null,contactId);
                App= [select id,Contact__c, Lending_Account__r.name, Funded_date__c, Customer_Name__c, FS_Assigned_To_Val__c, Amount, Name, Expected_Disbursal_Date__c from Opportunity where id=:appId];
                system.debug('====App.Lending_Account__c====='+App.Lending_Account__c);
                if(App.Lending_Account__c <> null){
                    string sub= 'FUNDED: '+ App.Lending_Account__r.name+ ' - '+ App.Customer_Name__c+' - '+App.Name+' - $'+App.Amount;
                    string emailBod='The following deal has been funded on '+ Date.valueOf(ExpDisDate)+' : '+'\n'+'Contract ID: '+ app.Lending_Account__r.name+'\n'+ 'Application ID: '+ app.Name+'\n'+'Applicant Name: '+ app.Customer_Name__c+'\n'+'Loan Amount: $'+app.Amount;
                    system.debug('====sub====='+sub);
                    system.debug('====emailBod====='+emailBod);
                    if(app.FS_Assigned_To_Val__c <> null && app.FS_Assigned_To_Val__c <> 'Not Assigned'){
                        us=[select id, name from user where name=: app.FS_Assigned_To_Val__c];
                    }
                    if(us <> null){
                        system.debug('====us====='+us);
                        WebToSFDC.notifyDev(sub,emailBod,us.id);
                    }
                    app.Funded_date__c= system.today();
                    update app;
                }
            }
        }
        catch (Exception e){
            system.debug('ContractCreation **** '+e.getMessage());
            MelisaCtrl.DoLogging('ContractGeneration',AppID,e.getMessage(),null,contactId);
        }

        //}
    }

}