@isTest public class CreditReportRequestParameterTest {
    @isTest static void validate() {
        CreditReportRequestParameter rp = new CreditReportRequestParameter();

        rp.last_name = 'test data';
        System.assertEquals('test data', rp.last_name);

        rp.first_name = 'test data 1';
        System.assertEquals('test data 1', rp.first_name);

        rp.ssn = 'test data';
        System.assertEquals('test data', rp.ssn);

        rp.state = 'test data 1';
        System.assertEquals('test data 1', rp.state);

        rp.city = 'test data';
        System.assertEquals('test data', rp.city);

        rp.zip_code = 'test data 1';
        System.assertEquals('test data 1', rp.zip_code);

        rp.house_nb = 'test data';
        System.assertEquals('test data', rp.house_nb);

        rp.pre_Dir = 'test data 1';
        System.assertEquals('test data 1', rp.pre_Dir);

        rp.street_type = 'test data';
        System.assertEquals('test data', rp.street_type);

        rp.street = 'test data 1';
        System.assertEquals('test data 1', rp.street);

        rp.post_Dir = 'test data';
        System.assertEquals('test data', rp.post_Dir);

        rp.apt_Nbr = 'test data 1';
        System.assertEquals('test data 1', rp.apt_Nbr);

        rp.unit_Type = 'test data';
        System.assertEquals('test data', rp.unit_Type);

        rp.pobNbr = 'test data 1';
        System.assertEquals('test data 1', rp.pobNbr);

        rp.Dob_MMDDYYYY = 'test data';
        System.assertEquals('test data', rp.Dob_MMDDYYYY);

        rp.dob_YYYYMMDD = 'test data 1';
        System.assertEquals('test data 1', rp.dob_YYYYMMDD);

        rp.phone_number = 'test data';
        System.assertEquals('test data', rp.phone_number);

        rp.phone_area = 'test data 1';
        System.assertEquals('test data 1', rp.phone_area);

        rp.bureau = 'test data';
        System.assertEquals('test data', rp.bureau);
        
        rp.email = 'test@test.com';
        System.assertEquals('test@test.com', rp.email);
        
        rp.ip_address = '123.134.345';
        System.assertEquals('123.134.345', rp.ip_address);
        
        rp.emailAgeStatusCode = '0';
        System.assertEquals('0', rp.emailAgeStatusCode);
        
    }
}