public class MultiloanUtil {
  /**
    * [getOffers description]
    * @param  appId [description]
    * @return       [description]
    */
  public static Offer__c getSelectedOffer(String appId) {

    Offer__c result = new Offer__c();

    if (!String.isEmpty(appId)) {
      for (Offer__c offer : [SELECT Id,
                             Repayment_Frequency__c,
                             APR__c,
                             Term__c,
                             Payment_Amount__c,
                             IsSelected__c,
                             Effective_APR__c,
                             Loan_Amount__c,
                             Total_Loan_Amount__c,
                             Fee__c,
                             IsCustom__c,
                             Fee_Percent__c,
                             FirstPaymentDate__c,
                             Installments__c,
                             Opportunity__c,
                             Customer_Disbursal__c,
                             Interest_Rate__c
                             FROM Offer__c
                             WHERE Opportunity__c = : appId
                                 AND IsSelected__c = true
                                     LIMIT 1]) {
        result = offer;
      }
    }

    return result;
  }
  /**
  * [getApplication description]
  * @param  appId [description]
  * @return       [description]
  */
  public static Opportunity getApplication(String appId) {

    Opportunity result = null;

    if (!String.isEmpty(appId)) {
      for (Opportunity app : [SELECT Id, Name, Type, OwnerId,
                              Fee_Handling__c, Amount, Dialer_Status__c,
                              Status__c,  Last_CQ_Date__c,
                              Contact__c, Partner_Account__c,
                              LeadSource__c, Lead_Sub_Source__c,
                              Next_ACH_Date__c, PrimaryCreditReportId__c,
                              Maximum_Loan_Amount__c, Maximum_Offer_Amount__c, Is_Selected_Offer__c, StrategyType__c
                              FROM Opportunity
                              WHERE Id = : appId]) {
        result = app;
      }
    }

    return result;
  }

  /**
  * [getContract description]
  * @param  contractId [description]
  * @return            [description]
  */
  public static loan__Loan_Account__c getContract(String contractId) {

    loan__Loan_Account__c result = null;

    if (!String.isEmpty(contractId)) {
      for (loan__Loan_Account__c contract : [SELECT Id, loan__Contact__c, Opportunity__c,
                                             loan__Principal_Remaining__c,
                                             loan__Fees_Remaining__c,
                                             loan__Interest_Accrued_Not_Due__c,
                                             loan__Due_Day__c,
                                             loan__ACH_Next_Debit_Date__c,
                                             loan__Loan_Amount__c,
                                             Opportunity__r.Maximum_Loan_Amount__c,
                                             Opportunity__r.StrategyType__c
                                             FROM loan__Loan_Account__c
                                             WHERE Id = : contractId]) {
        result = contract;
      }
    }

    return result;
  }
    

  /**
  * [getOffers description]
  * @param  appId [description]
  * @return       [description]
  */
  public static List<Offer__c> getOffers(String appId, decimal amountCh, string strategyType, string feeHandling) {
    decimal maxLoan;
    decimal loanLimit;
    MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');

    if (strategyType =='2' || strategyType =='3')
      loanLimit = multiloanSettings.Maximum_Loan_Amount_LG__c;
    else 
      loanLimit = multiloanSettings.Maximum_Loan_Amount__c;

    if (amountCh >= loanLimit) 
        maxLoan = loanLimit;
    else 
        maxLoan = amountCh;


    List<Offer__c> result = new List<Offer__c>();

    List<Offer__c> deleteOffer = new List<Offer__c>();

    if (!String.isEmpty(appId)) {
      if (feeHandling == null || feeHandling == FeeUtil.NET_FEE_OUT) {
        deleteOffer = [SELECT Id, Loan_Amount__c, Total_Loan_Amount__c
                      FROM Offer__c
                      WHERE Opportunity__c = : appId AND Total_Loan_Amount__c > : maxLoan]; // minimo entre la diferencia y el max del custom setting
      }
      else {
        deleteOffer = [SELECT Id, Loan_Amount__c, Total_Loan_Amount__c
                      FROM Offer__c
                      WHERE Opportunity__c = : appId AND Loan_Amount__c > : maxLoan]; // minimo entre la diferencia y el max del custom setting
      }

      if (deleteOffer.size() > 0)
        delete deleteOffer;

      for (Offer__c offer : [SELECT Id,
                             Repayment_Frequency__c,
                             APR__c,
                             Term__c,
                             Payment_Amount__c,
                             IsSelected__c,
                             Effective_APR__c,
                             Loan_Amount__c,
                             Total_Loan_Amount__c,
                             Fee__c,
                             IsCustom__c,
                             Fee_Percent__c,
                             FirstPaymentDate__c,
                             Installments__c,
                             Opportunity__c,
                             Customer_Disbursal__c,
                             Interest_Rate__c,
                             Opportunity__r.Fee_Handling__c
                             FROM Offer__c
                             WHERE Opportunity__c = : appId]) {
        result.add(offer);
      }
    }

    return result;
  }



  public static void insertNoteOldOffer (String parentId, String oldAppId, Offer__c oldOffer, String contractId) {
    if (oldOffer == null || String.isEmpty(parentId) || String.isEmpty(oldAppId)) return;

    loan__Loan_Account__c contract = getContract(contractId);

    String appName = 'Id: ' + oldAppId;
    for (Opportunity app : [SELECT id, Name FROM Opportunity WHERE Id = : oldAppId]) {
      appName = app.Name;
    }
    Note newNote = new Note ();
    newNote.parentid = parentId;
    newNote.title = 'Offers in Funded Application [' + appName + ']';
    NewNote.body =  'Interest Rate: ' + String.valueOf(oldOffer.Interest_Rate__c) + '%\n' +
                    'Term: ' + String.valueOf(oldOffer.Term__c) + '\n' +
                    'Repayment Frequency: ' + String.valueof(oldOffer.Repayment_Frequency__c) + '\n' +
                    'Installments: ' + String.valueOf(oldOffer.Installments__c) + '\n' +
                    'Payment Amount: ' + SalesForceUtil.formatMoney(String.valueof(oldOffer.Payment_Amount__c.format())) + '\n' +
                    'Disbursal Amount: ' + SalesForceUtil.formatMoney(String.valueOf(oldOffer.Customer_Disbursal__c.format())) + '\n' +
                    'Fee: ' + SalesForceUtil.formatMoney(String.valueof(oldOffer.fee__c.format())) + '\n' +
                    'Effective APR: ' + String.valueof(oldOffer.Effective_APR__c) + '%\n' +
                    'Current Due Day: ' + String.valueof(contract.loan__Due_Day__c) + '\n';
    insert newNote;
  }
  public static Opportunity getLastMultiOpportunity(loan__Loan_Account__c contract) {
    Opportunity foundApp = null;
	/*Opportunity lastApp = null;
    lastApp = [SELECT CreatedDate
                           FROM Opportunity
                           WHERE  Contact__c = : contract.loan__Contact__c
                               AND Type = 'Multiloan'                           	 
                               ORDER BY CreatedDate DESC
                               LIMIT 1];*/
      
      
            for (Opportunity lastValidMultiApp : [SELECT Id, Name, Type, OwnerId,
                                                  Fee_Handling__c, Amount,
                                                  Status__c,
                                                  Contact__c,
                                                  Reason_of_Opportunity_Status__c, PrimaryCreditReportId__c,
                                                  Is_Selected_Offer__c
                                                  FROM Opportunity
                                                  WHERE  Contact__c = : contract.loan__Contact__c
                                                      AND Type = 'Multiloan'
                                                          AND Status__c NOT IN ('Funded')
                                                          //AND CreatedDate = :lastApp.CreatedDate
                                                              ORDER BY CreatedDate DESC
                                                              LIMIT 1]) {
              foundApp = lastValidMultiApp;
            }
      
      return foundApp;
  }

  public static Boolean isBetterGrade(String previousGrade, String newGrade) {

    Boolean result = false;

    if (previousGrade.equalsIgnoreCase('A1')) {
      result = newGrade.equalsIgnoreCase(previousGrade);
    } else if (previousGrade.equalsIgnoreCase('A2')) {
      result = newGrade.equalsIgnoreCase(previousGrade) ||
               newGrade.equalsIgnoreCase('A1');
    } else if (previousGrade.equalsIgnoreCase('B1')) {
      result = newGrade.equalsIgnoreCase(previousGrade) ||
               newGrade.equalsIgnoreCase('A2') ||
               newGrade.equalsIgnoreCase('A1');
    } else if (previousGrade.equalsIgnoreCase('B2')) {
      result = newGrade.equalsIgnoreCase(previousGrade) ||
               newGrade.equalsIgnoreCase('B1') ||
               newGrade.equalsIgnoreCase('A2') ||
               newGrade.equalsIgnoreCase('A1');
    } else if (previousGrade.equalsIgnoreCase('C1')) {
      result = (newGrade.equalsIgnoreCase(previousGrade) ) ||
               newGrade.equalsIgnoreCase('B2') ||
               newGrade.equalsIgnoreCase('B1') ||
               newGrade.equalsIgnoreCase('A2') ||
               newGrade.equalsIgnoreCase('A1');
    } else if (previousGrade.equalsIgnoreCase('C2') ||
               previousGrade.equalsIgnoreCase('D') ||
               previousGrade.equalsIgnoreCase('E') ||
               previousGrade.equalsIgnoreCase('F')) {
      result = (newGrade.equalsIgnoreCase('C1')) ||
               newGrade.equalsIgnoreCase('B2') ||
               newGrade.equalsIgnoreCase('B1') ||
               newGrade.equalsIgnoreCase('A2') ||
               newGrade.equalsIgnoreCase('A1');
    }

    return result;
  }


  public static Multiloan_Params__c getMultiLoanParams(String contractId) {

    Multiloan_Params__c result = null;

    if (!String.isEmpty(contractId)) {
      for (Multiloan_Params__c params : [SELECT Id, Contract__c,
                                         FICO__c, Grade__c,
                                         New_FICO__c, New_Grade__c,
                                         Eligible_For_Multiloan__c,
                                         Last_MultiLoan_Eligibility_Review__c,
                                         Contract__r.loan__Principal_Remaining__c,
                                         Contract__r.loan__Fees_Remaining__c,
                                         Contract__r.loan__Interest_Accrued_Not_Due__c,
                                         System_Debug__c
                                         FROM Multiloan_Params__c
                                         WHERE Contract__c = : contractId]) {
        result = params;
      }
    }

    return result;
  }

}