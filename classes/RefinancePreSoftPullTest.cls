@isTest
private class RefinancePreSoftPullTest {

    @testSetup static void initializes_objects() {
        Refinance_Settings__c refiSet = new Refinance_Settings__c();
        refiSet.name = 'settings';
        refiSet.Eligibility_Days__c = 30;
        refiSet.FICO_Approved__c = 585;
        refiSet.Days_To_First_Check__c = 30;
        insert refiSet;

        Account acc = new Account();
        acc.Name = 'Refinance';
        insert acc;
    }

    @isTest static void get_eligibility_review_query_test() {
        LP_Custom__c lp = new LP_Custom__c();
        lp.Allowed_Refinance_States__c = 'AL,AK,AR,AZ,CA,DC,DE,FL,GA,ID,IL,IN,KS,KY,MI,MS,MO,MN,MT,NE,NC,NJ,NM,OH,OK,OR,PA,SD,TN,TX,VA,UT,WA,HI';
        insert lp;

        System.assertEquals(true, String.isNotEmpty(RefinancePreSoftPull.getEligibilityReviewQuery(5)));
    }

    @isTest static void eligibility_check_fail() {

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Envelope_Status__c = 'Completed';
        update opp;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Opportunity__c = opp.Id;
        contract.loan__Principal_Remaining__c = 1000;
        update contract;

        params.contract__c = contract.id;
        insert params;

        Integer notesBefore = [Select count() FROM Note where ParentId = : contract.loan__Contact__c];

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;
        System.debug('isEligible ' + isEligible);
        System.assert([SELECT COUNT() FROM Note WHERE ParentId = : contract.loan__Contact__c] > notesBefore);
        System.assertEquals(false, isEligible);
    }

    @isTest static void checks_ruleFail_for_OverdueDay() {
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Next_Installment_Date__c = Date.today().addDays(-5);
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(-5);
        contract.loan__Loan_Status__c = 'Active - Good Standing';
        contract.Is_Payment_Pending__c = false;
        update contract;

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();
        params.contract__c = contract.id;
        params.on_Past_Due__c = true;
        insert params;
        
        RefinancePreSoftPull.EligibilityCheck(params);
        
        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Overdue_Days_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Overdue_Days_Fail__c;

        System.assertEquals(true, ruleFail);
    }

    @isTest static void checks_default_rule_for_active_payment_plan() {

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Principal_Remaining__c = 1000;
        contract.loan_Active_Payment_Plan__c = True;
        update contract;

        params.contract__c = contract.id;
        insert params;

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Active_Payment_Plan_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Active_Payment_Plan_Fail__c;

        System.assertEquals(true, ruleFail);
    }

    @isTest static void checks_default_rule_for_point_of_need() {
        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        Lending_Product__c product = new Lending_Product__c();
        product.Name = 'Point Of Need';
        insert product;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Principal_Remaining__c = 1000;
        contract.loan_Active_Payment_Plan__c = false;
        contract.Lending_Product__c = product.Id;
        update contract;

        params.contract__c = contract.id;
        insert params;

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Original_Sub_Source_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Original_Sub_Source_Fail__c;

        System.assertEquals(true, ruleFail);
    }

    @isTest static void checks_default_rule_for_military() {

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        Contact cont = LibraryTest.createContactTH();
        cont.Military__c = True;
        update cont;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Principal_Remaining__c = 1000;
        contract.loan_Active_Payment_Plan__c = false;
        contract.loan__Contact__c = cont.Id;
        update contract;

        params.contract__c = contract.id;
        insert params;

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Contact_Military_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Contact_Military_Fail__c;

        System.assertEquals(true, ruleFail);
    }

    @isTest static void checks_rule_for_material_modification() {

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Is_Material_Modification__c = true;
        update contract;

        params.contract__c = contract.id;
        params.on_Past_Due__c = true;
        insert params;

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Material_Modification_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Material_Modification_Fail__c;

        System.assertEquals(true, ruleFail);
    }

    @isTest static void checks_rule_direct_pay_on() {
        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Is_Direct_Pay_On__c = true;
        update opp;

        loan__loan_Account__c contract = LibraryTest.createContractTH();

        params.contract__c = contract.id;
        params.on_Past_Due__c = true;
        insert params;

        Integer notesBefore = [SELECT COUNT() FROM Note WHERE ParentId = : contract.loan__Contact__c];

        RefinancePreSoftPull.EligibilityCheck(params);

        Integer notesAfter = [SELECT COUNT() FROM Note WHERE ParentId = : contract.loan__Contact__c];

        System.assert(notesAfter > notesBefore, 'Create Ineligibility Note');

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Is_Direct_Pay_On_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Is_Direct_Pay_On_Fail__c;

        System.assertEquals(true, ruleFail);
    }

    @isTest static void passes_eligibility_check_rules() {
        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_On__c = true;
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 1;
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(1);
        contract.loan__Loan_Amount__c = 4000;
        contract.loan__Principal_Paid__c = 2000;
        contract.loan__ACH_Routing_Number__c = '123456789';
        contract.loan__ACH_Account_Number__c = '0987654321';
        contract.loan__ACH_Debit_Amount__c = 50;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(6);
        contract.loan__Principal_Remaining__c = 1000;
        update contract;

        params.contract__c = contract.id;
        insert params;

        loan__Loan_account_Due_Details__c bill1 = new loan__Loan_account_Due_Details__c();
        bill1.loan__Payment_Satisfied__c = true;
        bill1.loan__Loan_Account__c = contract.Id;
        insert bill1;

        loan__Loan_account_Due_Details__c bill2 = new loan__Loan_account_Due_Details__c();
        bill2.loan__Payment_Satisfied__c = true;
        bill2.loan__Loan_Account__c = contract.Id;
        insert bill2;

        loan__Loan_account_Due_Details__c bill3 = new loan__Loan_account_Due_Details__c();
        bill3.loan__Payment_Satisfied__c = true;
        bill3.loan__Loan_Account__c = contract.Id;
        insert bill3;

        loan__Loan_account_Due_Details__c bill4 = new loan__Loan_account_Due_Details__c();
        bill4.loan__Payment_Satisfied__c = true;
        bill4.loan__Loan_Account__c = contract.Id;
        insert bill4;

        loan__Loan_account_Due_Details__c bill5 = new loan__Loan_account_Due_Details__c();
        bill5.loan__Payment_Satisfied__c = true;
        bill5.loan__Loan_Account__c = contract.Id;
        insert bill5;

        loan__Loan_account_Due_Details__c bill6 = new loan__Loan_account_Due_Details__c();
        bill6.loan__Payment_Satisfied__c = true;
        bill6.loan__Loan_Account__c = contract.Id;
        insert bill6;

        Integer notesBefore = [SELECT COUNT() FROM Note WHERE ParentId = :contract.id];
        Integer appsBefore = [SELECT COUNT() FROM Opportunity where Contact__c = :contract.loan__Contact__c];

        RefinancePreSoftPull.EligibilityCheck(params);

        System.assert([SELECT COUNT() FROM Note WHERE ParentId = :contract.id] == notesBefore, 'Is Eligible - No note');
        System.assert(appsBefore < [SELECT count() FROM  Opportunity where Contact__c = :contract.loan__Contact__c], 'Refinance Opp created');

        Account acc = [SELECT Id FROM Account WHERE Name = 'Refinance'];
        acc.Name = 'Test Exception';
        update acc;

        RefinancePreSoftPull.EligibilityCheck(params);
    }

    @isTest static void passes_eligibility_check_rules_Low_AND_Grow() {
        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.StrategyType__c = '2';
        update opp;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Opportunity__c = opp.Id;
        contract.loan__ACH_On__c = true;
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 1;
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(1);
        contract.loan__Loan_Amount__c = 4000;
        contract.loan__Principal_Paid__c = 2000;
        contract.loan__ACH_Routing_Number__c = '123456789';
        contract.loan__ACH_Account_Number__c = '0987654321';
        contract.loan__ACH_Debit_Amount__c = 50;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(6);
        contract.loan__Principal_Remaining__c = 1000;
        update contract;

        params.contract__c = contract.id;
        insert params;

        loan__Loan_account_Due_Details__c bill1 = new loan__Loan_account_Due_Details__c();
        bill1.loan__Payment_Satisfied__c = true;
        bill1.loan__Loan_Account__c = contract.Id;
        insert bill1;

        loan__Loan_account_Due_Details__c bill2 = new loan__Loan_account_Due_Details__c();
        bill2.loan__Payment_Satisfied__c = true;
        bill2.loan__Loan_Account__c = contract.Id;
        insert bill2;

        loan__Loan_account_Due_Details__c bill3 = new loan__Loan_account_Due_Details__c();
        bill3.loan__Payment_Satisfied__c = true;
        bill3.loan__Loan_Account__c = contract.Id;
        insert bill3;

        loan__Loan_account_Due_Details__c bill4 = new loan__Loan_account_Due_Details__c();
        bill4.loan__Payment_Satisfied__c = true;
        bill4.loan__Loan_Account__c = contract.Id;
        insert bill4;

        loan__Loan_account_Due_Details__c bill5 = new loan__Loan_account_Due_Details__c();
        bill5.loan__Payment_Satisfied__c = true;
        bill5.loan__Loan_Account__c = contract.Id;
        insert bill5;

        loan__Loan_account_Due_Details__c bill6 = new loan__Loan_account_Due_Details__c();
        bill6.loan__Payment_Satisfied__c = true;
        bill6.loan__Loan_Account__c = contract.Id;
        insert bill6;

        Integer notesBefore = [SELECT COUNT() FROM Note WHERE ParentId = :contract.id];
        Integer appsBefore = [SELECT COUNT() FROM Opportunity where Contact__c = :contract.loan__Contact__c];

        RefinancePreSoftPull.EligibilityCheck(params);

        System.assert([SELECT COUNT() FROM Note WHERE ParentId = :contract.id] == notesBefore, 'Is Eligible - No note');
        System.assert(appsBefore < [SELECT count() FROM  Opportunity where Contact__c = :contract.loan__Contact__c], 'Refinance Opp created');

        Account acc = [SELECT Id FROM Account WHERE Name = 'Refinance'];
        acc.Name = 'Test Exception';
        update acc;

        RefinancePreSoftPull.EligibilityCheck(params);
    }

    @isTest static void checks_default_rule_LowAndGrowRules_OverdueDays() {

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.StrategyType__c = '2';
        update opp;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Opportunity__c = opp.Id;
        contract.loan__Principal_Remaining__c = 1000;
        //contract.loan_Active_Payment_Plan__c = True;
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(-5);
        update contract;

        params.contract__c = contract.id;
        insert params;

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);
    }

    @isTest static void checks_default_rule_LowAndGrowRules_paymentPlan() {

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.StrategyType__c = '3';
        update opp;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Opportunity__c = opp.Id;
        contract.loan__Principal_Remaining__c = 1000;
        contract.loan_Active_Payment_Plan__c = True;
        update contract;

        params.contract__c = contract.id;
        insert params;

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Active_Payment_Plan_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Active_Payment_Plan_Fail__c;

        System.assertEquals(true, ruleFail);
    }

    @isTest static void checks_default_rule_LowAndGrowRules_IsMilitary() {

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();

        contact con = LibraryTest.createcontactTH();
        con.Military__c = true;
        update con;

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.StrategyType__c = '3';
        update opp;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Contact__c = con.Id;
        contract.Opportunity__c = opp.Id;
        contract.loan__Principal_Remaining__c = 1000;
        contract.loan_Active_Payment_Plan__c = True;
        update contract;

        params.contract__c = contract.id;
        insert params;

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Contact_Military_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Contact_Military_Fail__c;

        System.assertEquals(true, ruleFail);
    }

    @isTest static void checks_default_rule_LowAndGrowRules_OriginalSubSource() {

        loan_Refinance_Params__c params = new loan_Refinance_Params__c();
        Lending_Product__c product = null;
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.StrategyType__c = '3';
        update opp;

        if ([SELECT COUNT() FROM Lending_Product__c WHERE name = 'Point Of Need' LIMIT 1] > 0) {
            product = [SELECT Id FROM Lending_Product__c WHERE name = 'Point Of Need' LIMIT 1];
        } else {
            Lending_Product__c p = new Lending_Product__c();
            p.name = 'Point Of Need';
            insert p;
            product = p;
        }

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Original_Sub_Source__c = 'LoanHero';
        contract.Lending_Product__c = product.Id;
        contract.Opportunity__c = opp.Id;
        contract.loan__Principal_Remaining__c = 1000;
        update contract;

        params.contract__c = contract.id;
        insert params;

        RefinancePreSoftPull.EligibilityCheck(params);

        Boolean isEligible = [SELECT Eligible_For_Soft_Pull__c FROM loan_Refinance_Params__c WHERE contract__c = :contract.id ORDER BY CreatedDate DESC LIMIT 1].Eligible_For_Soft_Pull__c;

        System.assertEquals(false, isEligible);

        Boolean ruleFail = [SELECT Original_Sub_Source_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = : params.Id ORDER BY CreatedDate DESC LIMIT 1].Original_Sub_Source_Fail__c;

        System.assertEquals(true, ruleFail);
    }


}