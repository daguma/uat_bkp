global class Batch_AANNotificationOnOpportunity implements Database.Batchable<sObject>, Schedulable , Database.Stateful, Database.AllowsCallouts{
    // Local Variables 
    public String status = 'Refinance Unqualified';
    public String typeRefi = 'Refinance';
    public string query ;
    public String HARD_PULL_ATTACHMENT_FILE_NAME = 'system_credit_data_management_HP - Do Not Open';
 
    string AAN_TEMPLATE = 'AAN Notifications';
    Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    Integer iRecsProcessed = 0;
    
    global void execute (SchedulableContext sc) {
        system.debug('cuSettings    ' +cuSettings);
        Integer iNumReportsToProcess = cuSettings != null && cuSettings.AAN_To_Send_Per_Batch__c == null ? 9 : cuSettings.AAN_To_Send_Per_Batch__c.intValue();

        Batch_AANNotificationOnOpportunity job = new Batch_AANNotificationOnOpportunity();
        System.debug('Starting Batch_AANNotificationOnOpportunity at ' + Date.today());
        Database.executeBatch(job, iNumReportsToProcess);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //string oppIds = '0060x0000080w1U';
        Date dt = date.Today().addDays(-9);
        Date cutOffDate = date.valueof(Label.RefiNoticesCutOffDate);
            
                    query = 'SELECT id,name,type,Status__c,Contact_Email__c,Contact__c,Status_Modified_Date_Time__c,Status_Modified_Date__c,Is_Finwise__c,Permission_to_Get_Hard_Pull__c,KeyFactors__c,  '+
                '(Select Id, ParentId, Name From Attachments where Name =: HARD_PULL_ATTACHMENT_FILE_NAME order by createddate desc limit 1) '+
                ' FROM Opportunity WHERE Status__c =:status AND Is_Finwise__c = True and AAN_Sent_Date__c = null and createdDate >=:cutOffDate and type =:typeRefi and Status_Modified_Date__c <=: dt';//limit 10';
        iRecsProcessed = 0;
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> opportunityListToProcess){
         system.debug('===opportunityListToProcess======'+opportunityListToProcess);
         Set<Id> appIds = new Set<Id>();
        if(!opportunityListToProcess.isEmpty()){
            for(opportunity opp: opportunityListToProcess){
                if(opp.Attachments.size() > 0)
                    appIds.add(opp.id); 
            }
            try{
                if(appIds != null && appIds.size() > 0){
                    System.debug('Apps to Send AAN for = ' + appIds.size());
                    AANotificationDetails(appIds);
                }
            } catch(exception ex){
                System.debug('Exception:'+ ex.getMessage());
                WebToSFDC.notifyDev('Batch_AANNotificationOnOpportunity.QueryLocator ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
            }           

        }
    }
    
    public void AANotificationDetails(Set<Id> appIds) {
        try {
            List<Outbound_Emails_Temp__c> AppEmails = new List<Outbound_Emails_Temp__c>();
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            List<Note> notesEmails = new List<Note>();
            List<Opportunity> updatedApps = new List<Opportunity>();
            LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
            Map<Id,Attachment> hardpullAttachments = new Map<Id,Attachment>();
            Boolean thirdPartyCheck;

            Map< Id, Opportunity> apps;
            String plainBody = '';
            String HtmlValue = '', Subject = '', tName = '';
            Contact theContact = null;
            list<string> sendTo;
            EmailTemplate eTemplate;
            tName = AAN_TEMPLATE ;
            eTemplate = [select id, name,body,HtmlValue,Subject from EmailTemplate where name = :tName];

            apps = new Map<Id, Opportunity>(
                    [SELECT ID,CreditPull_Date__c, Lead_Sub_Source__c, LexisNexisDeclined__c,  Contact__r.Email, Contact__r.Id,Contact_Email__c,Status_Modified_Date__c,Permission_to_Get_Hard_Pull__c, 
                     Contact__r.Name, Contact__r.FirstName,Contact__r.MailingStreet, Contact__r.MailingCity, Is_Feb__C,
                     Contact__r.MailingState, Contact__r.MailingPostalCode,   CreatedDate, 
                     Status__c, Is_Finwise__c, Reason_of_Opportunity_Status__c, Contact__c, AAN_Sent_Date__c, 
                     KeyFactors__c, Clarity_Status__c, Partner_Account__r.Disable_AAN__c, Partner_Account__r.Name, 
                     Partner_Account__r.AAN_Email_Template__c, FICO__c, Type, Contract_Renewed__c
                     FROM Opportunity WHERE Id in : appIds]);
            
            System.debug('Applications to Send Notification too ' + apps.size());
            
            Map<Id, Email_Age_Details__c> mAges = new Map<Id, Email_Age_Details__c>();
            for (Email_Age_Details__c eAge : [select Id, Contact__c, Actual_Credit_Decision__c, Email_Age_Status__c, is_Email_Age_Timed_Out__c, errorCode__c, status__c from Email_Age_Details__c 
                                               where Contact__c in (select Contact__c from Opportunity where Id in: apps.keySet())  order by Contact__c, CreatedDate desc]) {
                                                   mAges.put(eAge.Contact__c, eAge);
                                               }
            
            ProxyApiCreditReportEntity creditReportEntity;
            for (Opportunity theApp : apps.values() ) {
                 try {
                        try {
                            creditReportEntity = CreditReport.getHardPullByEntityId(theApp.Id);
                        } catch (Exception ex) {
                            creditReportEntity = null;
                            System.debug('CR Get Exception = ' + ex.getStackTraceString());
                        }

                        theContact = theApp.Contact__r;
                        if (theContact != null) {
                            //Email Age changes
                            thirdPartyCheck =  isThirdPartyQualify(theApp,creditReportEntity,mAges);                                                        
                            System.debug(' appID = ' + theApp.Id + ' - Created on ' + theApp.CreatedDate);
                                                       
                            plainBody = replaceValuesFromData(eTemplate.Body, theContact, creditReportEntity, theApp, thirdPartyCheck);
                            HtmlValue = replaceValuesFromData(eTemplate.HtmlValue, theContact, creditReportEntity, theApp, thirdPartyCheck);
                            Subject = replaceValuesFromData(eTemplate.Subject, theContact, creditReportEntity, theApp,  thirdPartyCheck);
                            
                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                                sendTo = new List<String>();
                                sendTo.add(theContact.Email);
                                email.setWhatId(theApp.id);
                                email.setTargetObjectId(theApp.Contact__c);
                                email.setToAddresses(sendTo);                    
                                //email.setTemplateId(tempid);
                                email.setHtmlBody(HtmlValue);
                                email.setPlainTextBody(plainBody);
                                email.setSubject(Subject);
                                email.saveAsActivity = false;
                                email.setTreatTargetObjectAsRecipient(False);
                                system.debug('**email****'+email);
                            mails.add(email);
                            
                                theApp.AAN_Sent_Date__c = DateTime.now();
                            updatedApps.add(theApp);

                            //AppEmails.add(createEmail(HtmlValue, Subject, theContact.Id ));
                            notesEmails.add(createNote( 'To: ' + theContact.Email + '\n\n' + plainBody, Subject, theApp.Id ));
                            iRecsProcessed++;
                        } else System.debug('No Contact for v1 AppId = ' + theApp.Id);
                    } catch (Exception e) {
                        System.debug('Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString());
                    }
                
            }

            System.debug('Sending App Emails # : ' + AppEmails.size());

            if ((notesEmails != null && notesEmails.size() > 0) || Test.isRunningTest()) {
                insert notesEmails;
            }
            if(!mails.isEmpty()){
               try{
                     Messaging.sendEmail(mails);
                     if(updatedApps != null && updatedApps.size() > 0)
                         Update updatedApps;
                 }
                 Catch(Exception e){
                     System.debug('--------------Exception in Sending emails--------------------'+e.getMessage());
                 }
            }       
        } catch (Exception e) {
            WebToSFDC.notifyDev('AANotification Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString(), UserInfo.getUserId() ) ;
        }
    }
    
    public Outbound_Emails_Temp__c createEmail(String htmlBody, String subject, Id ContactID ) {
        return new Outbound_Emails_Temp__c(Contact__c = ContactID, Body__c = htmlBody, Subject__c = subject);

    }
    
    public boolean isThirdPartyQualify(Opportunity theApp,ProxyApiCreditReportEntity creditReportEntity, Map<Id, Email_Age_Details__c> mAges){
        boolean dontRunBRMS = CreditReport.validateBrmsFlag(theApp.id), thirdPartyCheck = false;
        if(String.isNotEmpty(theApp.Reason_of_Opportunity_Status__c) && theApp.Reason_of_Opportunity_Status__c == 'Cannot Verify Information Provided' && creditReportEntity <> null) {
            thirdPartyCheck = !theApp.Clarity_Status__c;
            if (!thirdPartyCheck && mAges.containsKey(theApp.Contact__c)) {
                Email_Age_Details__c theAge = mAges.get(theApp.Contact__c);
                if(dontRunBRMS)
                    thirdPartyCheck =  emailAgeSettings.Call_Email_age__c && creditReportEntity.decision && creditReportEntity.automatedDecision.equalsIgnoreCase('No') && !theAge.Email_Age_Status__c ; //if application declined due to email age
                else 
                    thirdPartyCheck = creditReportEntity.brms <> null && creditReportEntity.brms.autoDecision.equalsIgnoreCase('No') && !theAge.Email_Age_Status__c ; //if application declined due to email age
            }
        }
        if (!thirdPartyCheck ) thirdPartyCheck = theApp.LexisNexisDeclined__c ;
        if (!thirdPartyCheck && creditReportEntity != null) thirdPartyCheck = !Decisioning.isClarityClearRecentHistoryPassed(creditReportEntity.clearRecentHistory);

        return thirdPartyCheck;
    }

    
    public string replaceValuesFromData(String theValue, Contact theContact, ProxyApiCreditReportEntity creditReportEntity, Opportunity theOpp,  boolean bThirdParty) {
        boolean bHasCR = creditReportEntity != null, dontRunBRMS = CreditReport.validateBrmsFlag(theOpp); 
        if(!dontRunBRMS && bHasCR) {
            bHasCR = creditReportEntity <> null; 
        }
        String sFico = String.isEmpty(theOpp.FICO__c) ? 'N/A' : theOpp.FICO__c;
        String kFac = theOpp.KeyFactors__c != null ? theOpp.KeyFactors__c : ' ';
        kFac = !bHasCR ||  creditReportEntity.brms == null || sFico.startsWith('900') ? 'CREDIT REPORT UNAVAILABLE' : kFac;
        if (sFico.startsWith('900') || sFico.contains('-')) sFico = 'N/A';
        system.debug('======kFac ====='+kFac);
        string kFacForPlain = kFac;
        kFacForPlain= kFacForPlain.replaceAll('\r\n','<br/>');
        system.debug('======kFacafter modi ====='+kFac);
        theValue = theValue.replace('{!LetterDate}', (theOpp.CreditPull_Date__c != null) ? Datetime.newInstance(theOpp.CreditPull_Date__c, DateTime.now().Time() ).format('MMMM d,  yyyy') : Datetime.now().format('MMMM d,  yyyy'));
        theValue = theValue.replace('{!today()}', Datetime.now().format('MMMM d,  yyyy')); 
     
        if (theContact != null) {
            theValue = theValue.replace('{!Contact.Name}', theContact.Name);
            theValue = theValue.replace('{!Contact.FirstName}', theContact.FirstName);
            theValue = theValue.replace('{!Contact.MailingStreet}', theContact.MailingStreet != null ? theContact.MailingStreet : '' );
            theValue = theValue.replace('{!Contact.MailingCity}', (theContact.MailingCity != null) ? theContact.MailingCity : '');
            theValue = theValue.replace('{!Contact.MailingState}', theContact.MailingState != null ? theContact.MailingState : '');
            theValue = theValue.replace('{!Contact.MailingPostalCode}', theContact.MailingPostalCode != null ? theContact.MailingPostalCode : '');
        }
    
        theValue = theValue.replace('{!Disqualifier.Inquiries_in_last_6_Number}', ''); 

        theValue = theValue.replace('{!CreditReport.CreditBureau}', !bHasCR ||  creditReportEntity.brms == null ||  creditReportEntity.datasourceName == null ? 'N/A' : ( creditReportEntity.datasourceName.equalsIgnoreCase('ExpConsUSV7') ? 'Experian' : 'Transunion'));
        theValue = theValue.replace('{!CreditReport.CreditBureauAddress}', !bHasCR ||  creditReportEntity.brms == null ||  creditReportEntity.datasourceName == null   ? 'N/A' : ( creditReportEntity.datasourceName.equalsIgnoreCase('ExpConsUSV7') ? 'EXPERIAN': 'TRANSUNION CONSUMER SOLUTIONS' ));
        theValue = theValue.replace('{!CreditReport.CreditBureauAddress2}', !bHasCR ||  creditReportEntity.brms == null ||  creditReportEntity.datasourceName == null   ? 'N/A' : ( creditReportEntity.datasourceName.equalsIgnoreCase('ExpConsUSV7') ? 'P.O. Box 2002': 'P.O. Box 2000' ));
        theValue = theValue.replace('{!CreditReport.CreditBureauAddress3}', !bHasCR ||  creditReportEntity.brms == null ||  creditReportEntity.datasourceName == null   ? 'N/A' : ( creditReportEntity.datasourceName.equalsIgnoreCase('ExpConsUSV7') ? 'ALLEN, TX 75013' : 'CHESTER, PA 19016-2000' ));
        theValue = theValue.replace('{!CreditReport.CreditBureauPhone}', !bHasCR ||  creditReportEntity.brms == null || creditReportEntity.datasourceName == null  ? 'N/A' : (creditReportEntity.datasourceName.equalsIgnoreCase('ExpConsUSV7') ? '(866) 200-6020' : '(800) 916-8800'));
        
        
        theValue = theValue.replace('{!CreditReport.Credit_Score_Value__c}', sFico);
        theValue = theValue.replace('{!hasCreditScore}', (sFico != null &&  sFico != 'N/A')? '_' : 'X');
        theValue = theValue.replace('{!CreditReport.KeyFactors}', kFacForPlain);
        theValue = theValue.replace('{!CreditReport.KeyFactors2}', kFacForPlain );
        theValue = theValue.replace('{!hasKeyFactors}', kFac != null ? '_': 'X');
        
        theValue = theValue.replace('{!todayDate}', Datetime.now().format('MMMM d,  yyyy') );
        theValue = theValue.replace('{!ThirdPartyInfo}', bThirdParty ? 'X' : '_' );

        return theValue;
    }
    
    global void finish(Database.BatchableContext BC){
        WebToSFDC.notifyDev('Batch_AANNotificationOnOpportunity Finished', 'Records processed = ' + iRecsProcessed, UserInfo.getUserId() );
        System.debug('Batch_AANotificationOnOpportunity Finished!');
          
    }
    // process changed from contact note to opty note.
    public static Note createNote(String htmlBody, String subject, Id OpportunityID ) {
        return new Note(ParentId = OpportunityID, Body = htmlBody.replaceAll('<br/>','\n'), Title = subject);
    }
    
}