@isTest
public class AttachmentControllerTest {
    
    @isTest static void AttachmentTest() {
        Contact con = LibraryTest.createContactTH();
        
        Attachment att = new Attachment();
        att.ParentId = con.Id;
        att.Name = 'Attachment 1.pdf';
        att.ContentType = 'image/gif';
        att.Body = Blob.valueOf('Text Logic is Gone');
        insert att;
        
        List<Attachment> attList = AttachmentController.getAttachmentList(con.Id);
        
        Integer count1 = AttachmentController.getTotalCount(con.Id);
        System.assertEquals(1, count1);
        
        AttachmentController.deleteAttachments(att.Id);
        Integer count2 = AttachmentController.getTotalCount(con.Id);
        System.assertEquals(0, count2);
    }
}