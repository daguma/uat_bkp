//This batch Job is required to be run while upgrading from velaRC3 to hydra RC3
/* This will be run on LOC loans whose Interest Type is Floating.
 * 1 child Rate Schedule Record will be created for the 1st Disbursal and
 * child rate schedule records will be created for each OLT of index rate change type.
 * This script assumes for Multiple rate schedules, only rs with floating rate index exist
 */

global class FloatingRateRevisionScript implements Database.Batchable<sObject>{
    String query;
    //This function will sort the Rate Schedules by Start Date ASC followed by Parent, child
    public class RateSetupInfo implements comparable {
        String type;//Parent or Child
        String event;//Disbursal
        loan__Multi_Step_Loan_Setup__c rateSetup;
        public RateSetupInfo(String type, loan__Multi_Step_Loan_Setup__c rateSetup) {
            this.type = type;
            this.rateSetup = rateSetup;
        }
        public Integer compareTo(Object rs) {
            RateSetupInfo rs1 = (RateSetupInfo)rs;
            Integer retval = 0;
            if (this.rateSetup.loan__Start_Date__c > rs1.rateSetup.loan__Start_Date__c) {
                retval = 1;
            }
            else if (this.rateSetup.loan__Start_Date__c < rs1.rateSetup.loan__Start_Date__c) {
                retval = -1;
            }
            else {
                if(this.type == 'Parent' && rs1.type == 'Child') {
                    retval = -1;
                }
                if(this.type == 'Child' && rs1.type == 'Parent') {
                    retval = 1;
                }
            }
            return retval;
        }    
    }
    private String getRateScheduleFields() {
         return 'Id, ' + 
                'Floating_Rate_Index__c, ' +
                'Interest_Rate__c, ' +
                'Loan_Account__c, ' +
                'Margin_Rate__c, ' +
                'Sequence__c, ' +
                'Start_Date__c, ' +
                'Rate_Change_Date_Next__c, ' +
                'Parent_Multi_Step_Loan_Setup__c';
    }
    global FloatingRateRevisionScript(){
        mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
        mfiflexUtil.ObjectCache loanOC = ec.getObject('LoanAccount');
        if(loanOC != null) {
            ec.deleteObject('LoanAccount');
        }
        String nameSpace = loan.CustomSettingsUtil.getOrgParameters().loan__Namespace_Prefix__c;
        loanOC = ec.createObject('LoanAccount', 'Loan_Account__c', nameSpace);
        loanOC.addFields('Id');
        loanOC.addNamedParameter('interestType', loan.LoanConstants.INTEREST_TYPE_FLOATING);
        loanOC.addNamedParameter('productType', loan.LoanConstants.LOC);
        String whereClause = 'Interest_Type__c = :interestType AND Product_Type__c = :productType';
        loanOC.setWhereClause(whereClause);

        mfiflexUtil.ObjectCache multiStep = ec.createRelationship('Multi_Step_Loan_Setup__r')
                .addFields(getRateScheduleFields())
                .setWhereClause('Archived__c = false');

        loanOC.addRelationship(multiStep);
        
        mfiflexUtil.ObjectCache otxns = ec.createRelationship('Other_Loan_Transactions__r')
                .addFields('Id, New_Interest_Rate__c, Txn_Date__c, Loan_Account__c')
                .addNamedParameter('irc', loan.LoanConstants.LOAN_TRANSACTION_INDEX_RATE_CHANGE)
                .setWhereClause('Transaction_Type__c = :irc');
        loanOC.addRelationship(otxns);
        
        mfiflexUtil.ObjectCache dtxns = ec.createRelationship('Loan_Disbursal_Transactions__r')
                .addFields('Id, Disbursal_Date__c')
                .setWhereClause('Cleared__c = true AND Reversed__c = false AND Rejected__c = false')
                .setOrderClause('Disbursal_Date__c')
                .setLimitClause('1');
        loanOC.addRelationship(dtxns);

        loanOC.buildQuery();
        this.query = loanOC.getQuery();
    }

   global virtual database.querylocator start(Database.BatchableContext bc) {
       return Database.getQueryLocator(query);
   }
   global virtual void execute(Database.BatchableContext bc, List<sObject> scopes) {
       List<loan__Multi_Step_Loan_Setup__c> rateSchedulesToUpserted = new List<loan__Multi_Step_Loan_Setup__c>();
       Map<Id, List<loan__Floating_Rate__c>> friToFrMap = getFri();
       for(sObject scope : scopes) {
           loan__Loan_Account__c loan = (loan__Loan_Account__c)scope;
           List<loan__Multi_Step_Loan_Setup__c> rateSchedules = loan.loan__Multi_Step_Loan_Setup__r;
           List<loan__Other_Transaction__c> otxns = loan.loan__Other_Loan_Transactions__r;
           List<loan__Loan_Disbursal_Transaction__c> dtxns = loan.loan__Loan_Disbursal_Transactions__r;
           if(rateSchedules != null && rateSchedules.size() > 0 && otxns != null && otxns.size() > 0) {
               List<RateSetupInfo> rateSetupInfoList = new List<RateSetupInfo>();
               //Existing Rate Schedules are Parent schedules.
               for(loan__Multi_Step_Loan_Setup__c rateSchedule : rateSchedules) {
                   RateSetupInfo rsInfo = new RateSetupInfo('Parent', rateSchedule);
                   rateSetupInfoList.add(rsInfo);
               }
               //Create Rate Schedules(Child) from OLT.
               for(loan__Other_Transaction__c otxn : otxns) {
                   loan__Multi_Step_Loan_Setup__c rateSetUp = new loan__Multi_Step_Loan_Setup__c();
                   rateSetUp.loan__Interest_Rate__c = otxn.loan__New_Interest_Rate__c;
                   rateSetUp.loan__Start_Date__c = otxn.loan__Txn_Date__c;
                   rateSetUp.loan__Loan_Account__c = loan.Id;
                   RateSetupInfo rsInfo = new RateSetupInfo('Child', rateSetUp);
                   rateSetupInfoList.add(rsInfo);
               }
               //Create a Rate Schedule(Child)for LDT.
               if(dtxns != null && dtxns.size() > 0) {
                   loan__Multi_Step_Loan_Setup__c rateSetUp = new loan__Multi_Step_Loan_Setup__c();
                   rateSetUp.loan__Interest_Rate__c = null;
                   rateSetUp.loan__Start_Date__c = dtxns.get(0).loan__Disbursal_Date__c;
                   rateSetUp.loan__Loan_Account__c = loan.Id;
                   RateSetupInfo rsInfo = new RateSetupInfo('Child', rateSetUp);
                   rsInfo.event = 'Disbursal';
                   rateSetupInfoList.add(rsInfo);
               }
               rateSetupInfoList.sort();
               rateSchedulesToUpserted.addAll(assignParentWithSequence(rateSetupInfoList, friToFrMap));
           }
       }
       upsert rateSchedulesToUpserted;
   }
    //After arranging all RS start date wise sorted, assign the parent to child reschedules and update the sequence.
    private List<loan__Multi_Step_Loan_Setup__c> assignParentWithSequence(List<RateSetupInfo> rateSetupInfoList, Map<Id, List<loan__Floating_Rate__c>> friToFrMap) {
        system.debug('rateSetupInfoList :' + rateSetupInfoList.size());
        List<loan__Multi_Step_Loan_Setup__c> retval = new List<loan__Multi_Step_Loan_Setup__c>();
        Id parentId = null;
        Integer sequence = 1;
        RateSetupInfo parentRateSetupInfo;
        Decimal marginRate;
        for(RateSetupInfo rateSetupInfo : rateSetupInfoList) {
            system.debug('class : ' + rateSetupInfo);
            if(rateSetupInfo.type == 'Parent' 
               && rateSetupInfo.rateSetup.loan__Margin_Rate__c != null) {
                parentId = rateSetupInfo.rateSetup.Id;
                marginRate = rateSetupInfo.rateSetup.loan__Margin_Rate__c;
                parentRateSetupInfo = rateSetupInfo;
            }
            if(rateSetupInfo.type == 'Child' 
               && parentRateSetupInfo != null 
               && parentRateSetupInfo.rateSetup.loan__Floating_Rate_Index__c != null) {
                rateSetupInfo.rateSetup.loan__Parent_Multi_Step_Loan_Setup__c = parentId;
                rateSetupInfo.rateSetup.loan__Margin_Rate__c = marginRate;
                if(rateSetupInfo.event == 'Disbursal' 
                   && friToFrMap != null) { //disbursal
                    for(loan__Floating_Rate__c fr : friToFrMap.get(parentRateSetupInfo.rateSetup.loan__Floating_Rate_Index__c)) {
                        if(rateSetupInfo.rateSetup.loan__Start_Date__c >= fr.loan__Rate_Effective_From__c) {
                            if(fr.loan__Rate_Effective_To__c == null 
                               || rateSetupInfo.rateSetup.loan__Start_Date__c <= fr.loan__Rate_Effective_To__c) {
                                if(rateSetupInfo.rateSetup.loan__Margin_Rate__c == null) {
                                    rateSetupInfo.rateSetup.loan__Margin_Rate__c = 0;
                                }
                                if(fr.loan__Rate_Percentage__c == null) {
                                    fr.loan__Rate_Percentage__c = 0;
                                }
                                rateSetupInfo.rateSetup.loan__Interest_Rate__c = rateSetupInfo.rateSetup.loan__Margin_Rate__c + fr.loan__Rate_Percentage__c;
                                break;
                            }
                        }
                    }
                }
            }    
            rateSetupInfo.rateSetup.loan__Sequence__c = sequence++;
            retval.add(rateSetupInfo.rateSetup);
        }
        system.debug('retval :' + retval.size());
        return retval;
    }
    private Map<Id, List<loan__Floating_Rate__c>> getFri() {
        Map<Id, List<loan__Floating_Rate__c>> friMap = new Map<Id, List<loan__Floating_Rate__c>>();
        List<loan__Floating_Rate__c> frs = [Select Id, 
                                                   loan__Rate_Effective_From__c, 
                                                   loan__Rate_Effective_To__c, 
                                                   loan__Floating_Rate_Index__c,
                                                   loan__Rate_Percentage__c
                                           FROM loan__Floating_Rate__c
                                           WHERE loan__Active__c = true 
                                           AND loan__Archived__c = false 
                                           AND loan__Reset_Process_Status__c = 'Not Submitted'
                                           ORDER BY loan__Rate_Effective_To__c];
        for(loan__Floating_Rate__c fr : frs) {
            if(!friMap.containsKey(fr.loan__Floating_Rate_Index__c)) {
                friMap.put(fr.loan__Floating_Rate_Index__c, new List<loan__Floating_Rate__c>());
            }
            friMap.get(fr.loan__Floating_Rate_Index__c).add(fr);
        }
        return friMap;
    }
    global virtual void finish(Database.BatchableContext bc) {}   
}