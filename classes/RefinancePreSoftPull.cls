global with sharing class RefinancePreSoftPull {


  private static final String ELIGIBILITY_REVIEW_QUERY =  'SELECT Id, Contract__c, on_Past_Due__c, Eligible_For_Soft_Pull__c, ' +
      'ManualEligibilityCheck__c, Last_Refinance_Eligibility_Review__c, Eligible_For_Soft_Pull_Params__c, IsLowAndGrow__c ' +
      'FROM loan_Refinance_Params__c ' +
      'WHERE ((Last_Refinance_Eligibility_Review__c < {0} AND ' +
      '  Contract__r.loan__Total_Amount_Paid__c > 0 AND ' +
      '  Contract__r.loan__Loan_Status__c = {1} AND ' +
      '  Contract__r.loan__Disbursal_Date__c < {2} AND ' +
      ' ((Eligible_For_Soft_Pull__c = false AND on_Past_Due__c = false AND  ' +
      '    Contract__r.Product_Name__c NOT IN ({3}) AND ' +
      '    Contract__r.overdue_Days__c = 0 AND ' +
      '    Contract__r.loan_Active_Payment_Plan__c = false AND ' +
      '    Contract__r.loan__Contact__r.Military__c = false AND ' + 
      '    Contract__r.Opportunity__r.Is_Direct_Pay_On__c = false AND ' +
      '    Contract__r.Is_Material_Modification__c = false) OR Eligible_For_Soft_Pull__c = true) AND ' +
      '    Contract__r.loan__Contact__r.MailingState in ({4})) OR (ManualEligibilityCheck__c = true)) ' +
      ' {5} {6}';

  public static String getEligibilityReviewQuery(Integer queryLimit) {
      return getEligibilityReviewQuery (queryLimit, null);
  }

  public static String getEligibilityReviewQuery(Integer queryLimit, Integer iThread) {

    Refinance_Settings__c refinanceSettings = Refinance_Settings__c.getValues('settings');

    List<String> fillers = new list<String> {'LAST_N_DAYS:' + (Integer.valueOf(refinanceSettings.Eligibility_Days__c) - 1), '\'Active - Good Standing\''};
    fillers.add('LAST_N_DAYS:' + (Integer.valueOf(refinanceSettings.Days_To_First_Check__c) - 1));
    fillers.add('\'Point Of Need\'');
    fillers.add('\'' + [SELECT Allowed_Refinance_States__c FROM LP_Custom__c].Allowed_Refinance_States__c.replace(',', '\',\'') + '\'');
    fillers.add((iThread != null && iThread > 0) ? ' AND Contract__r.loan__Thread_Number__c =  ' + iThread + ' ' : '');
    fillers.add(queryLimit > 0 ? 'limit ' + queryLimit : '');
    return String.format(ELIGIBILITY_REVIEW_QUERY, fillers);
  }

  public static void ChangedSubStatusRQ(){
     List<Opportunity> opp = new List<Opportunity>();

     for(Opportunity opportunity : [SELECT Id, Sub__c 
                            FROM Opportunity 
                            WHERE Sub__c = 'Brand New RQ' ]){

      opportunity.Sub__c = null;
      opp.add(opportunity);

     }
     update opp;
  }

  public static void EligibilityCheck(loan_Refinance_Params__c params) {
    loan__Loan_Account__c contract = getContract(params.Contract__c);
    if (contract == null) return;

    if (contract.Opportunity__r.StrategyType__c != null && (contract.Opportunity__r.StrategyType__c == '2') || contract.Opportunity__r.StrategyType__c == '3' ) {
      EligibilityCheckLowAndGrow(params, contract);
    } else {
      EligibilityCheckDefault(params, contract);
    }

  }

  public static void EligibilityCheckDefault(loan_Refinance_Params__c params, loan__Loan_Account__c contract) {
    if (contract == null) return;

    Loan_Refinance_Params_Logs__c refinanceParamsLogs = new Loan_Refinance_Params_Logs__c();
    List<String> rulesFailed = new List<String>();

    try {
      refinanceParamsLogs.Loan_Refinance_Params__c = params.Id;
      params.Last_Refinance_Eligibility_Review__c = System.today();
      params.ManualEligibilityCheck__c = false;

      if (PassEligibilityDefaultRules(contract, refinanceParamsLogs)) {

        if (contract.overdue_Days__c > 0) {
          rulesFailed.add(' - Contract Status (other than Good Standing or with days past due)');
          refinanceParamsLogs.Overdue_Days_Fail__c = true;
        }

        if (!RefinanceConsecutivePayments.CheckConsecutivePaymentsRules(contract)) {
          rulesFailed.add(' - Consecutive Payments');
          refinanceParamsLogs.Consecutive_Payments_Fail__c = true;
        }

        /* RD-101 removing rule
        if (contract.modification_Type__c == 'Loan Workout Agreement Modification') {
          rulesFailed.add(' - Loan Workout Agreement History');
          refinanceParamsLogs.Modification_Type_Fail__c = true;
        } */

        /* RD-101 adding rule */
        if (contract.Is_Material_Modification__c) {
          rulesFailed.add(' - Loan Material Modification History');
          refinanceParamsLogs.Material_Modification_Fail__c = true;
        }
        /* RD-552 waiving rule under certain conditions */
        if (contract.principal_Balance_Paid_Percentage__c < 25) {
          if (contract.loan__Loan_Amount__c > 5000 || (contract.loan__Loan_Amount__c <= 5000 && contract.Originated_Grade__c != 'A1' && contract.Originated_Grade__c != 'A2' && contract.Originated_Grade__c != 'B1')) {
            rulesFailed.add(' - Principal Balance Paid Percentage');
            refinanceParamsLogs.Principal_Balance_Paid_Fail__c = true;
          }
        }
        if (params.on_Past_Due__c) {
          rulesFailed.add(' - Past Due History');
          refinanceParamsLogs.On_Past_Due_Fail__c = true;
        }
        if (contract.Opportunity__r.Is_Direct_Pay_On__c) {
          rulesFailed.add(' - Vendor Pay Exclusion');//MAINT-968
          refinanceParamsLogs.Is_Direct_Pay_On_Fail__c = true;
        }

        if (rulesFailed.size() == 0) {
          params.Eligible_For_Soft_Pull__c = true;
          update params;
          RefinancePostSoftPull.executeProcess(contract.Id, params);
        } else {
          params.Eligible_For_Soft_Pull__c = false;
          update params;
        }

      } else {
        params.Eligible_For_Soft_Pull__c = false;
        update params;
      }

      insertOrUpdatelog(refinanceParamsLogs,  params.Id, params.Eligible_For_Soft_Pull__c, rulesFailed, contract.loan__Contact__c);

    } catch (Exception e) {
      System.debug('Catch ' + e.getStackTraceString() + ' ' + e.getMessage());
      refinanceParamsLogs.System_Debug__c = 'ERROR: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
      update params; //  to update Last_Refinance_Eligibility_Review__c
      for (Loan_Refinance_Params_Logs__c paramLogs : [SELECT Id FROM Loan_Refinance_Params_Logs__c
           WHERE Loan_Refinance_Params__c = : params.Id
                                            ORDER BY CreatedDate DESC LIMIT 1]) {
        refinanceParamsLogs.Id = paramLogs.Id;
      }
      upsert refinanceParamsLogs;
    }

  }

  public static Boolean PassEligibilityDefaultRules(loan__Loan_Account__c contract, Loan_Refinance_Params_Logs__c refinanceParamsLogs) {
    if (contract == null) return false;

    /* RD-101 removing rule
    if (contract.loan__Principal_Remaining__c <= 10) {
      refinanceParamsLogs.Principal_Remaining_Fail__c = true;
      return false;
    }
    */

    if (contract.loan__Contact__r.Military__c) {
      refinanceParamsLogs.Contact_Military_Fail__c = true;
      return false;
    }
    if (contract.loan_Active_Payment_Plan__c) {
      refinanceParamsLogs.Active_Payment_Plan_Fail__c = true;
      return false;
    }
    /* RD-101 removing rule
    if (contract.Original_Sub_Source__c == 'LoanHero') {
      refinanceParamsLogs.Original_Sub_Source_Fail__c = true;
      return false;
    }
    */
    /* RD-101 adding rule */
    if (contract.Product_Name__c == 'Point Of Need') {
      refinanceParamsLogs.Original_Sub_Source_Fail__c = true;
      return false;
    }

    return true;
  }

  public static void insertOrUpdatelog(Loan_Refinance_Params_Logs__c  refinanceParamsLogs, string paramsId, Boolean eligibleForSoftPull,  List<String> rulesFailed, string contractId) {

    Loan_Refinance_Params_Logs__c lastRefiLog = null;
    String oldValuesParamsLogs = '';
    for (Loan_Refinance_Params_Logs__c lastLog : [SELECT Id,
         Loan_Refinance_Params__c,
         Contact_Military_Fail__c,
         Consecutive_Payments_Fail__c,
         Active_Payment_Plan_Fail__c,
         Is_Direct_Pay_On_Fail__c,
         On_Past_Due_Fail__c,
         Original_Sub_Source_Fail__c,
         Overdue_Days_Fail__c,
         Principal_Balance_Paid_Fail__c ,
         Material_Modification_Fail__c,
         Modification_Type_Fail__c,
         Principal_Remaining_Fail__c,
         Originated_Loan_Amount_Fail__c,
         To__c
         FROM Loan_Refinance_Params_Logs__c
         where Loan_Refinance_Params__c = : paramsId
                                          ORDER BY CreatedDate DESC limit 1]) {
      oldValuesParamsLogs = lastLog.Contact_Military_Fail__c + '|' + lastLog.Consecutive_Payments_Fail__c + '|' + lastLog.Active_Payment_Plan_Fail__c + '|'
                            + lastLog.Is_Direct_Pay_On_Fail__c + '|' + lastLog.On_Past_Due_Fail__c + '|' + lastLog.Original_Sub_Source_Fail__c + '|' + lastLog.Overdue_Days_Fail__c + '|' + lastLog.Principal_Balance_Paid_Fail__c
                            + '|' + lastLog.Material_Modification_Fail__c + '|' + lastLog.Modification_Type_Fail__c + '|' + lastLog.Principal_Remaining_Fail__c + '|' + lastLog.Originated_Loan_Amount_Fail__c;
      lastRefiLog = lastLog;
    }

    String newValuesParamsLogs = refinanceParamsLogs.Contact_Military_Fail__c + '|' + refinanceParamsLogs.Consecutive_Payments_Fail__c + '|' + refinanceParamsLogs.Active_Payment_Plan_Fail__c + '|'
                                 + refinanceParamsLogs.Is_Direct_Pay_On_Fail__c + '|' + refinanceParamsLogs.On_Past_Due_Fail__c + '|' + refinanceParamsLogs.Original_Sub_Source_Fail__c + '|' + refinanceParamsLogs.Overdue_Days_Fail__c + '|' + refinanceParamsLogs.Principal_Balance_Paid_Fail__c
                                 + '|' + refinanceParamsLogs.Material_Modification_Fail__c + '|' + refinanceParamsLogs.Modification_Type_Fail__c + '|' + refinanceParamsLogs.Principal_Remaining_Fail__c + '|' + refinanceParamsLogs.Originated_Loan_Amount_Fail__c;

    if (lastRefiLog ==  null || oldValuesParamsLogs != newValuesParamsLogs) {
      if (lastRefiLog != null) {
        lastRefiLog.To__c = Date.today();
        update lastRefiLog;
      }

      refinanceParamsLogs.To__c = null;
      insert refinanceParamsLogs;
      if (!eligibleForSoftPull && rulesFailed.size() > 0) {
        createIneligibilityNote(contractId, rulesFailed);
      }

    }

  }

  public static void EligibilityCheckLowAndGrow(loan_Refinance_Params__c params, loan__Loan_Account__c contract) {
    if (contract == null) return;
    String strategiesTypeLG = '2, 3';
    List<String> rulesFailed = new List<String>();
    Loan_Refinance_Params_Logs__c refinanceParamsLogs = new Loan_Refinance_Params_Logs__c();
    Refinance_Settings__c refinanceSettings = Refinance_Settings__c.getValues('settings');

    try {
      refinanceParamsLogs.Loan_Refinance_Params__c = params.Id;
      params.Last_Refinance_Eligibility_Review__c = System.today();
      params.IsLowAndGrow__c = (strategiesTypeLG.contains(contract.Opportunity__r.StrategyType__c) ? true : false);
      params.ManualEligibilityCheck__c = false;

      if (PassEligibilityLowAndGrowRules(contract, refinanceParamsLogs)) {

        if (contract.overdue_Days__c > 0) {
          rulesFailed.add(' - Contract Status (other than Good Standing or with days past due)');
          refinanceParamsLogs.Overdue_Days_Fail__c = true;
        }
        if (!RefinanceConsecutivePayments.CheckConsecutivePaymentsRules(contract)) {
          rulesFailed.add(' - Consecutive Payments');
          refinanceParamsLogs.Consecutive_Payments_Fail__c = true;
        }
        if (contract.principal_Balance_Paid_Percentage__c < 1) {
          rulesFailed.add(' - Principal Balance Paid Percentage');
          refinanceParamsLogs.Principal_Balance_Paid_Fail__c = true;
        }
        if (contract.Original_Loan_Amount_No_Fee__c > refinanceSettings.Max_Originated_Loan_Amount_LG__c) {
          rulesFailed.add(' - Originated Loan Amount');
          refinanceParamsLogs.Originated_Loan_Amount_Fail__c = true;
        }

        if (rulesFailed.size() == 0) {
          params.Eligible_For_Soft_Pull__c = true;
          update params;
          RefinancePostSoftPull.executeProcess(contract.Id, params);
        } else {
          params.Eligible_For_Soft_Pull__c = false;
          update params;
          //createIneligibilityNote(contract.loan__Contact__c, rulesFailed);
        }

      } else {
        params.Eligible_For_Soft_Pull__c = false;
        update params;
      }

      insertOrUpdatelog(refinanceParamsLogs, params.Id, params.Eligible_For_Soft_Pull__c, rulesFailed, contract.loan__Contact__c);

    } catch (Exception e) {
      refinanceParamsLogs.System_Debug__c = 'ERROR: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
      update params; //to update Last_Refinance_Eligibility_Review__c
      for (Loan_Refinance_Params_Logs__c paramLogs : [SELECT Id FROM Loan_Refinance_Params_Logs__c
           WHERE Loan_Refinance_Params__c = : params.Id
                                            ORDER BY CreatedDate DESC LIMIT 1]) {
        refinanceParamsLogs.Id = paramLogs.Id;
      }
      upsert refinanceParamsLogs;
    }

  }

  public static Boolean PassEligibilityLowAndGrowRules(loan__Loan_Account__c contract, Loan_Refinance_Params_Logs__c refinanceParamsLogs) {
    if (contract == null) return false;

    if (contract.loan__Contact__r.Military__c) {
      refinanceParamsLogs.Contact_Military_Fail__c = true;
      return false;
    }
    if (contract.loan_Active_Payment_Plan__c) {
      refinanceParamsLogs.Active_Payment_Plan_Fail__c = true;
      return false;
    }
    if (contract.Product_Name__c == 'Point Of Need') {
      refinanceParamsLogs.Original_Sub_Source_Fail__c = true;
      return false;
    }

    return true;
  }


  private static loan__Loan_Account__c getContract(String contractId) {

    loan__Loan_Account__c result = null;

    if (!String.isEmpty(contractId)) {
      for (loan__Loan_Account__c contract : [SELECT Id,
                                             loan__Contact__c,
                                             Opportunity__c,
                                             loan__Due_Day__c,
                                             loan__ACH_On__c,
                                             overdue_Days__c,
                                             modification_Type__c,
                                             principal_Balance_Paid_Percentage__c,
                                             payment_Frequency_Masked__c,
                                             Opportunity__r.StrategyType__c,
                                             Originated_Grade__c,
                                             loan__Principal_Remaining__c,
                                             loan_Active_Payment_Plan__c,
                                             loan__Loan_Amount__c,
                                             Original_Sub_Source__c,
                                             Product_Name__c,
                                             loan__Contact__r.Military__c,
                                             Opportunity__r.Is_Direct_Pay_On__c,
                                             Is_Material_Modification__c,
                                             Original_Loan_Amount_No_Fee__c
                                             FROM loan__Loan_Account__c
                                             WHERE Id = : contractId]) {
        result = contract;
      }
    }

    return result;
  }

  private static void createIneligibilityNote(String parentId, List<String> rulesFailed) {

    String bodyNote =   'On ' + System.now() + ', the eligibility check for this customer resulted in a decline.\n' +
                        'The following items caused the ineligibility: \n';

    for (String s : rulesFailed) bodyNote += s + '\n';

    Note n = new Note();
    n.Body = bodyNote;
    n.Title = 'Refinance Ineligibility Reasons';
    n.ParentId = parentId;

    insert n;
  }

}