public with sharing class Button_IncomeValidationCtrl {

    @AuraEnabled
    public static IncomeValidationEntity validateIncome(Id oppId) {

        IncomeValidationEntity ive = new IncomeValidationEntity();

        try {

            Opportunity opp = [SELECT id, lead__c, contact__c FROM Opportunity WHERE id = :oppId];

            if (opp != null) {

                String customerIdentifier = opp.Lead__c;
                if (String.isEmpty(customerIdentifier))
                    customerIdentifier = opp.contact__c;

                Map<String, String> jsonInfo = new Map<String, String>();
                jsonInfo.put('cusId', customerIdentifier);
                jsonInfo.put('appId', oppId);

                String JsonResponse = generalPurposeWebServices.isIncomeValidated(JSON.serialize(jsonInfo));
                Map<string, string> infoMap = (Map<String, String>) JSON.deserialize(JsonResponse, Map<String, String>.class);
                system.debug('==infoMap=='+ infoMap);
                String message, status;
                if (infoMap != null) {
                    message = infoMap.ContainsKey('message') ? infoMap.get('message') : ''; //00Q3D0000016nLlUAI
                    status = infoMap.ContainsKey('status') ? infoMap.get('status') : ''; //a3l3D00000000xmQAA
                } else {
                    message = '';
                    status = '';
                }

                //system.debug('==status=='+ status); system.debug('==message=='+ message);
                if (status.equalsIgnoreCase('err')) {
                    ive.errorMessage = message;//'Error in calculating the Income Validation. Please check corresponding logs for the same.';
                } else if(message.equalsIgnoreCase('Pass') || message.equalsIgnoreCase('Fail')) {
                    ive.successMessage = 'Income Validation is calculated for the Customer. Please check Income Validation Summary section.';
                } else if (String.isEmpty(message)){
                    ive.errorMessage = 'Message is empty.';
                } else {
                    ive.errorMessage = message;
                }

            } else {
                ive.errorMessage = 'Opportunity is null';
            }

        } catch (Exception e) {
            ive.errorMessage = e.getMessage() + ' - ' + e.getStackTraceString();
        }
        return ive;

    }

    public class IncomeValidationEntity {
        @AuraEnabled public Boolean hasError { get; set; }
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = true;
            }
        }
        @AuraEnabled public String successMessage {
            get;
            set {
                successMessage = value;
                hasError = false;
            }
        }

        public IncomeValidationEntity() {
            errorMessage = 'Empty response';
        }
    }
}