@isTest
public class RefinanceConsecutivePaymentsTest {

    @isTest static void calculates_consecutive_payments_zero() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        update contract;

        contract = [SELECT Id, Payment_Frequency_Masked__c, Opportunity__r.StrategyType__c FROM loan__Loan_Account__c where id = :contract.id];
        Boolean result = RefinanceConsecutivePayments.CheckConsecutivePaymentsRules(contract);

        System.assertEquals(false, result);
    }

    @isTest static void calculates_consecutive_payments_empty_list() {
        loan__loan_Account__c contract = new loan__loan_Account__c();

        Boolean result = RefinanceConsecutivePayments.CheckConsecutivePaymentsRules(contract);

        System.assertEquals(false, result);
    }

    @isTest static void calculates_consecutive_payments_less_than_six_payments_for_monthly() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        update contract;

        loan__Loan_account_Due_Details__c bill = new loan__Loan_account_Due_Details__c();
        bill.loan__Payment_Satisfied__c = true;
        bill.loan__Loan_Account__c = contract.Id;
        insert bill;

        contract = [SELECT Id, Payment_Frequency_Masked__c, Opportunity__r.StrategyType__c FROM loan__Loan_Account__c where id = :contract.id];
        Boolean result = RefinanceConsecutivePayments.CheckConsecutivePaymentsRules(contract);

        System.assertEquals(false, result);
    }

    @isTest static void calculates_consecutive_payments_more_than_six_payments_for_monthly() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 6;
        update contract;

        loan__Loan_account_Due_Details__c bill1 = new loan__Loan_account_Due_Details__c();
        bill1.loan__Payment_Satisfied__c = true;
        bill1.loan__Loan_Account__c = contract.Id;
        insert bill1;

        loan__Loan_account_Due_Details__c bill2 = new loan__Loan_account_Due_Details__c();
        bill2.loan__Payment_Satisfied__c = true;
        bill2.loan__Loan_Account__c = contract.Id;
        insert bill2;

        loan__Loan_account_Due_Details__c bill3 = new loan__Loan_account_Due_Details__c();
        bill3.loan__Payment_Satisfied__c = true;
        bill3.loan__Loan_Account__c = contract.Id;
        insert bill3;

        loan__Loan_account_Due_Details__c bill4 = new loan__Loan_account_Due_Details__c();
        bill4.loan__Payment_Satisfied__c = true;
        bill4.loan__Loan_Account__c = contract.Id;
        insert bill4;

        loan__Loan_account_Due_Details__c bill5 = new loan__Loan_account_Due_Details__c();
        bill5.loan__Payment_Satisfied__c = true;
        bill5.loan__Loan_Account__c = contract.Id;
        insert bill5;

        loan__Loan_account_Due_Details__c bill6 = new loan__Loan_account_Due_Details__c();
        bill6.loan__Payment_Satisfied__c = true;
        bill6.loan__Loan_Account__c = contract.Id;
        insert bill6;

        contract = [SELECT Id, Payment_Frequency_Masked__c, Opportunity__r.StrategyType__c FROM loan__Loan_Account__c where id = :contract.id];
        Boolean result = RefinanceConsecutivePayments.CheckConsecutivePaymentsRules(contract);

        System.assertEquals(true, result);
    }

    @isTest static void gets_payment_limit_by_frequency() {
        Integer result = RefinanceConsecutivePayments.getLimitByFrequency('Monthly');

        System.assertEquals(6, result);

        result = RefinanceConsecutivePayments.getLimitByFrequency('28 Days');

        System.assertEquals(6, result);

        result = RefinanceConsecutivePayments.getLimitByFrequency('Semi-Monthly');

        System.assertEquals(12, result);

        result = RefinanceConsecutivePayments.getLimitByFrequency('Bi-Weekly');

        System.assertEquals(13, result);

        result = RefinanceConsecutivePayments.getLimitByFrequency('Weekly');

        System.assertEquals(24, result);
    }

    @isTest static void gets_payment_limit_low_and_grow_by_frequency() {
        Integer result = RefinanceConsecutivePayments.getLimitLowAndGrowByFrequency('Monthly');

        System.assertEquals(6, result);

        result = RefinanceConsecutivePayments.getLimitLowAndGrowByFrequency('28 Days');

        System.assertEquals(6, result);

        result = RefinanceConsecutivePayments.getLimitLowAndGrowByFrequency('Semi-Monthly');

        System.assertEquals(12, result);

        result = RefinanceConsecutivePayments.getLimitLowAndGrowByFrequency('Bi-Weekly');

        System.assertEquals(12, result);

        result = RefinanceConsecutivePayments.getLimitLowAndGrowByFrequency('Weekly');

        System.assertEquals(24, result);
    }
    @isTest static void calculates_consecutive_payments_more_than_six_payments_no_satisfied() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 6;
        update contract;

        loan__Loan_account_Due_Details__c bill1 = new loan__Loan_account_Due_Details__c();
        bill1.loan__Payment_Satisfied__c = true;
        bill1.loan__Loan_Account__c = contract.Id;
        insert bill1;

        loan__Loan_account_Due_Details__c bill2 = new loan__Loan_account_Due_Details__c();
        bill2.loan__Payment_Satisfied__c = true;
        bill2.loan__Loan_Account__c = contract.Id;
        insert bill2;

        loan__Loan_account_Due_Details__c bill3 = new loan__Loan_account_Due_Details__c();
        bill3.loan__Payment_Satisfied__c = false;
        bill3.loan__Loan_Account__c = contract.Id;
        insert bill3;

        loan__Loan_account_Due_Details__c bill4 = new loan__Loan_account_Due_Details__c();
        bill4.loan__Payment_Satisfied__c = true;
        bill4.loan__Loan_Account__c = contract.Id;
        insert bill4;

        loan__Loan_account_Due_Details__c bill5 = new loan__Loan_account_Due_Details__c();
        bill5.loan__Payment_Satisfied__c = true;
        bill5.loan__Loan_Account__c = contract.Id;
        insert bill5;

        loan__Loan_account_Due_Details__c bill6 = new loan__Loan_account_Due_Details__c();
        bill6.loan__Payment_Satisfied__c = true;
        bill6.loan__Loan_Account__c = contract.Id;
        insert bill6;

        contract = [SELECT Id, Payment_Frequency_Masked__c, Opportunity__r.StrategyType__c FROM loan__Loan_Account__c where id = :contract.id];
        Boolean result = RefinanceConsecutivePayments.CheckConsecutivePaymentsRules(contract);

        System.assertEquals(false, result);
    }

    @isTest static void does_not_find_contract() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        update contract;

        contract = [SELECT Id, Payment_Frequency_Masked__c, Opportunity__r.StrategyType__c FROM loan__Loan_Account__c where id = :contract.id];
        Boolean result = RefinanceConsecutivePayments.CheckConsecutivePaymentsRules(contract);

        System.assertEquals(false, result);
    }

}