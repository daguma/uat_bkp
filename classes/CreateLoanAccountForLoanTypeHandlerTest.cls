@isTest public class CreateLoanAccountForLoanTypeHandlerTest {

    @isTest static void creates_loan() {
        TestHelperForManaged.setupACHParameters();
                loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();
        acha.Disbursal_File_Cutoff_Hour__c  =20;

        update acha;
        loan__loan_Account__c loan = LibraryTest.createContractTH();

        opportunity app = ApplicationToContractConverter.getApplicationDetails(loan.opportunity__c);
        
        Contact c = [SELECT Id, Name, Annual_Income__c FROM Contact WHERE Id = : loan.loan__contact__c LIMIT 1];
        c.Promotion_Term_In_Months__c = 24;
        c.Promotion_Active__c = true;
        c.Promotion_Rate__c = 1.33;
        c.Promotion_Expiration_Date__c = Date.today().addMonths(30);
        update c;
        
        loan__Loan_Product__c lp = [SELECT Id, Name, loan__Default_Interest_Rate__c, loan__Interest_Calculation_Method__c, loan__Delinquency_Grace_Days__c, loan__Write_off_Tolerance_Amount__c, loan__Late_Charge_Grace_Days__c,
                                    loan__Amortize_Balance_type__c, loan__Amortization_Frequency__c, loan__Amortization_Enabled__c, loan__Amortization_Calculation_Method__c,
                                    loan__Fee_Set__c, loan__Pre_Bill_Days__c
                                    FROM loan__Loan_Product__c WHERE Name = 'Sample Loan Product' LIMIT 1];

        Account Partneracc = new Account(Name = 'test' , Cobranding_Name__c = c.Name , Type = 'Partner', Allow_Offers__c = true, Display_only_Effective_APR__c = true, URL_AAN_Process__c = true, Suppress_AAN__c = true, Delayed_hours_to_send_AAN__c = 12, AAN_Email_Template__c = 'CK AAN Notification', Is_Parent_Partner__c = true, Is_child_Partner__c = false, Funding_Payment_Mode__c = 'i2c', Use_Promotion_Fields__c = true);
        insert Partneracc;

        app.Expected_First_Payment_Date__c = Date.today().addDays(15);
        app.Term__c = 36;
        app.Expected_Start_Date__c = Date.today().addDays(2);
        app.ACH_Account_Number__c = '1234567890';
        app.ACH_Account_Type__c = 'Checking';
        app.ACH_Bank_Name__c = 'Test Bank';
        app.Payment_Amount__c = 196.14;
        app.Payment_Frequency__c = 'Monthly';
        app.ACH_Routing_Number__c = '123456789';
        app.Interest_Rate__c = .2399;
        app.Payment_Method__c = 'ACH';
        app.Expected_Disbursal_Date__c = Date.today().addMonths(2);
        app.Estimated_Grand_Total__c = 50000;
        app.Payroll_Frequency__c = 'Monthly';
        app.Expected_Close_Date__c = System.Today().addMonths(36);
        app.DTI__C = '18.000';
        app.FICO__c = '700';
        app.Contact__c = c.Id;
        update app;

        Scorecard__c sc = new Scorecard__c();
        sc.Grade__c = 'B1';
        sc.Acceptance__c = 'Y';
        sc.opportunity__c = app.id;
        insert sc;

        loan__Bank_Account__c ba1 = new loan__Bank_Account__c(loan__Contact__c = app.Contact__c, loan__Routing_Number__c = app.ACH_Routing_Number__c,
                                                              loan__Bank_Name__c = app.ACH_Bank_Name__c, loan__Account_Type__c = app.ACH_Account_Type__c,
                                                              loan__Bank_Account_Number__c = app.ACH_Account_Number__c);
        insert ba1;
        CreateLoanAccountForLoanTypeHandler loanAccount = new CreateLoanAccountForLoanTypeHandler(app, loan, lp);

        String result = loanAccount.createLoanAccount();

        Boolean check = result.contains('Application converted to Loan');

        System.assertEquals(check, true);

        loanAccount.createRepaymentSchedule(10.0, 100.0);

        loan.loan__Loan_Amount__c = 5000;
        loan.Active_Charge_Off__c = true;
        loan.loan__Charged_Off_Date__c = Date.today();
        update loan;

        CreateLoanAccountForLoanTypeHandler l = new CreateLoanAccountForLoanTypeHandler();

        app.Partner_Account__c = Partneracc.id;
        update app;

        CreateLoanAccountForLoanTypeHandler loanAccountNew = new CreateLoanAccountForLoanTypeHandler(app, loan, lp);
    ba1.loan__Bank_Name__c = 'A TOTALLY DIFFERENT';
        update ba1;
        String resultNew = loanAccountNew.createLoanAccount();
        loanAccountNew.sendEmailWithoutOriginatedGrade(loan.Id);
    }
}