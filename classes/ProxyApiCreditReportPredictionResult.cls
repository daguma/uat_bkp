global class ProxyApiCreditReportPredictionResult{
    
    public String labelIndex{get;set;}
    public String label{get;set;}
    public List<ProxyApiCreditReportClassProbabilities> classProbabilities{get;set;}
    public String predictionErrorMessage{get;set;}
    public String predictionErrorDetails{get;set;}
}