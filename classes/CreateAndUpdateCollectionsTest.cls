@isTest public class CreateAndUpdateCollectionsTest {

    @isTest static void dummy_test_for_trigger() {
        genesis__Applications__c app = LibraryTest.createApplicationTH();

        app.genesis__Status__c = 'Credit Qualified';
        update app;

        Contact c = [Select Id, Name, Loan_Amount__c from Contact where Id = : app.genesis__Contact__c];

        Account acc = new Account();
        acc.name = c.Name;
        upsert acc;

        c.AccountId = acc.Id;
        update c;

        loan__Loan_Account__c loan = new loan__Loan_Account__c();
        loan.loan__Payment_Amount__c = 250;
        loan.loan__Delinquent_Amount__c = 0;
        loan.loan__Fees_Remaining__c = 0;
        loan.loan__Next_Installment_Date__c = Date.today().addDays(6);
        loan.loan__Contact__c = c.Id;
        loan.Application__c = app.Id;
        loan.Application__r = app;
        loan.loan__Number_of_Installments__c = 18;
        loan.loan__APR__c = .2399;
        loan.loan__Loan_Amount__c = c.Loan_Amount__c;
        loan.loan__Term_Cur__c = 10;
        loan.Original_Payment_Frequency__c = 'Monthly';
        loan.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        loan.loan__ACH_Account_Number__c = '123456789';
        loan.loan__ACH_Routing_Number__c = '234567890';
        loan.Proof_of_Claim_Filed__c = '';
        loan.loan__Loan_Status__c = 'Active - Good Standing';
        loan.Is_on_Collections__c = false;
        loan.Manual_Collections__c = false;
        loan.Late_on_Payment__c = false;
        loan.loan__Oldest_Due_Date__c = Date.today().addDays(-5);
        insert loan;

        Integer countBef = [SELECT COUNT() FROM Collection__c];

        loan.loan__ACH_Account_Number__c = '123455789';
        update loan;

    }

}