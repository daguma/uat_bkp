global class ffaCOABatch implements Database.Batchable<String>, Database.Stateful {
	
	List<String> loanAccountIds;
	Decimal apiVendorCOA; 
	Decimal staffCOA;  
	
	global ffaCOABatch(Set<Id> inputLoanAccountIds, Decimal apiVendorAmount, Decimal staffAmount) {
		apiVendorCOA = apiVendorAmount; 
		staffCOA = staffAmount; 
		loanAccountIds = new List<String>(); 
		for (Id i: inputLoanAccountIds){
			loanAccountIds.add(String.valueOf(i)); 
		}
	}
	
	global Iterable<String> start(Database.BatchableContext BC) {
		return loanAccountIds; 
	}

   	global void execute(Database.BatchableContext BC, List<String> scope) {
   		List<loan__Loan_Account__c> accountsToUpdate = [SELECT API_Vendor_Allocable_Cost__c, Staff_Allocable_Cost__c
                                                         FROM loan__Loan_Account__c
                                                         WHERE Id in :scope]; 

        for (loan__Loan_Account__c act: accountsToUpdate){
            act.API_Vendor_Allocable_Cost__c = apiVendorCOA; 
            act.Staff_Allocable_Cost__c = staffCOA; 
        }

        if (!accountsToUpdate.isEmpty()){
            update accountsToUpdate; 
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		//Send email 
    	AsyncApexJob job = [SELECT Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email 
							FROM AsyncApexJob
							WHERE Id = :bc.getJobId()
							LIMIT 1]; 
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
		String[] toAddresses = new String[]{job.CreatedBy.Email}; 
		mail.setToAddresses(toAddresses); 
		mail.setSubject('Update Loan Account Cost of Acquisition Job ' + job.Status); 
		mail.setPlainTextBody('records processed: ' + job.TotalJobItems + ' with ' + job.NumberOfErrors + ' failures.'); 
		Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail}); 
	}

	//static utility method for kicking off the batch from anonymous apex 
    public static void startBatch(Set<Id> accountIds, Decimal vendorCost, Decimal staffCost){
    	ffaCOABatch b = new ffaCOABatch(accountIds,vendorCost,staffCost); 
    	Database.executeBatch(b, 200); 
    }
	
}