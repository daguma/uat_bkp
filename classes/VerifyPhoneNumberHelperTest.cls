@isTest
public class VerifyPhoneNumberHelperTest {

    public static testMethod void testVerifyPhoneNumberHelper(){
        Contact con = TestHelper.createContact();
        con.Phone = '9815555555';
        update con;
        
        //create payfone settings custom setting record
        LibraryTest.createPayfoneSettings();
        
        VerifyPhoneNumberHelper verifyPhnHelper = new VerifyPhoneNumberHelper();
        VerifyPhoneNumberHelper.allOperations(con.Id);
        
        Contact contactAfter = [select id,Name_Score__c,Phone_Processed__c,Payfone_Response_Code__c from Contact where Id = :con.Id];
   }
   
   public static testMethod void test_get_access_token() {
       Payfone_Settings__c payfoneSettings = new Payfone_Settings__c();
       String token = VerifyPhoneNumberHelper.getAccessToken(payfoneSettings);
       System.assert(token == null);
   }

    public static testMethod void test_normalizeString() {
        String str = '(404) 219-5704';
        String updatedStr = VerifyPhoneNumberHelper.normalizeString(str);
        System.assert(updatedStr.length() < str.length());
        System.assertEquals(updatedStr,'4042195704');
    }
    
    public static testMethod void test_getGUIDString() {
        String guid = VerifyPhoneNumberHelper.getGUIDString();
        System.assert(guid <> null);
    }
    
    public static testMethod void test_get_request_for_verify_API() {
        Contact con = TestHelper.createContact();
        Payfone_Settings__c payfoneSet = LibraryTest.createPayfoneSettings('test1','test2','test3');
        String request_id = VerifyPhoneNumberHelper.getGUIDString();
        String requestURL = VerifyPhoneNumberHelper.getRequestForVerifyAPI(payfoneSet,'2127290854',con,request_id );
        System.assert(requestURL <> null);
        
        HttpRequest req = VerifyPhoneNumberHelper.getVerifyRequest('token',request_id ,requestURL,payfoneSet );
        System.assert(req <> null);
        
    }
    
    public static testMethod void test_decision_using_payfone() {
        Contact con = LibraryTest.createContactTH();
        con.Trust_Score__c = 700 ;
        con.Payfone_Verified_Flag__c = true;
        con.last_Name_score__c = 90;
        con.first_name_score__c = 90;
        con.address_score__c = 90;
        con.Street_Number_Score__c = 90;
        update con;
        
        Payfone_Settings__c payfoneSet = LibraryTest.createPayfoneSettings('test1','test2','test3');
        
        String payfoneresult = VerifyPhoneNumberHelper.decisionUsingPayfone(con);
        System.assertEquals(payfoneresult ,'Pass');
        
        contact conUpdated = [select id,Process_Date__c,Phone_Processed__c from contact where id = : con.Id];
        System.assertEquals(con.Phone,conUpdated.Phone_Processed__c);
        System.assert(conUpdated.Process_Date__c <> null);
        
        Boolean skipflag = VerifyPhoneNumberHelper.payfoneSkipIdologyCriteria(payfoneSet,con);
        System.assert(skipflag == false);
        
        VerifyPhoneNumberHelper.InsertPayfoneHistory(con,con.Phone,'','Pass','','');
        Boolean skipflag1 = VerifyPhoneNumberHelper.payfoneSkipIdologyCriteria(payfoneSet,con);
        System.assert(skipflag1 == true);
    }
    
    public static testMethod void test_parse_verify_result() {
        Contact con = LibraryTest.createContactTH();
        Payfone_Settings__c payfoneSet = LibraryTest.createPayfoneSettings('test1','test2','test3');
        String response = LibraryTest.payfoneResponse(con.Phone,700,90,90,90,90,90,true);
        con = VerifyPhoneNumberHelper.parseVerifyResult(response,con);
        
        String payfoneresult = VerifyPhoneNumberHelper.decisionUsingPayfone(con);
        System.assertEquals(payfoneresult ,'Pass');
        
        VerifyPhoneNumberHelper.InsertPayfoneHistory(con,con.Phone,'Success','Pass','token request',response);
        List<Payfone_Run_History__c> payfoneTrack = [select id,contact__c from Payfone_Run_History__c where contact__c = :con.Id];
        System.assert(payfoneTrack.size() > 0);
    }

    public static testMethod void test_Running_In_Sandbox() {
        Organization org = [SELECT Id, IsSandbox FROM Organization LIMIT 1];
        Boolean flag = VerifyPhoneNumberHelper.RunningInSandbox();
        System.assert(flag == org.IsSandbox);
    }
    
    public static testMethod void test_getTokenRequest() {
        Payfone_Settings__c payfoneSet = LibraryTest.createPayfoneSettings('test1','test2','test3');
        HttpRequest req = VerifyPhoneNumberHelper.getTokenRequest(payfoneSet);
        System.assert(req <> null);
    }
    
}