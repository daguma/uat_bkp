global class BusinessDaysPaymentTransactionsJob implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

	global final List<loan__Loan_Payment_Transaction__c> paymentTransactionToUpdate = new List<loan__Loan_Payment_Transaction__c>();
	String query;
    public Integer Count = 0;

	public void execute(SchedulableContext sc) { 
		BusinessDaysPaymentTransactionsJob b = new BusinessDaysPaymentTransactionsJob();
		database.executebatch(b, 5);
	}
	
	global BusinessDaysPaymentTransactionsJob() {}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id, loan__Transaction_Date__c, Business_Day_Count__c, Payment_Pending__c, loan__Reversed__c, loan__Payment_Mode__r.Name ' +
		' FROM loan__Loan_Payment_Transaction__c ' +
		' WHERE  loan__Payment_Mode__r.Name = \'ACH\' AND Payment_Pending__c NOT IN (\'Cleared\', \'Bounced\') AND loan__Transaction_Date__c = LAST_N_DAYS:30 ' ;
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<loan__Loan_Payment_Transaction__c> scope) {

   		for(loan__Loan_Payment_Transaction__c tr : scope){
   			tr.Business_Day_Count__c = calculateBusinessDays(tr);
            String newPending = setPaymentPendingValues(tr, Integer.valueOf(tr.Business_Day_Count__c));
            if (newPending != tr.Payment_Pending__c) {
               tr.Payment_Pending__c = setPaymentPendingValues(tr, Integer.valueOf(tr.Business_Day_Count__c));
               paymentTransactionToUpdate.add(tr);
            }
   		}
        
   		if (paymentTransactionToUpdate.size() > 0) Database.update(paymentTransactionToUpdate, false);
        Count += paymentTransactionToUpdate.size();
	}
	
	global void finish(Database.BatchableContext BC) {        
          WebToSFDC.notifyDev('BusinessDaysPaymentTransactionsJob Batch Completed', 'BusinessDaysPaymentTransactionsJob batch completed for Today. Total processed : ' + count);        
    }

	public String setPaymentPendingValues(loan__Loan_Payment_Transaction__c tr,Integer businessDays){
		if( !tr.loan__Reversed__c && businessDays < 7 && tr.loan__Payment_Mode__r.Name == 'ACH'){
			return 'Pending';
		}else if(!tr.loan__Reversed__c && tr.Payment_Pending__c != 'Cleared' && ((businessDays >= 7 && tr.loan__Payment_Mode__r.Name == 'ACH') || tr.loan__Payment_Mode__r.Name != 'ACH')){
			return 'Cleared';
		}else if(tr.loan__Reversed__c)
			return 'Bounced' ;
		return tr.Payment_Pending__c;
	}

	public Integer calculateBusinessDays(loan__Loan_Payment_Transaction__c tr){
		return (tr.loan__Transaction_Date__c != null) ? SalesForceUtil.calculateBusinessDays(tr.loan__Transaction_Date__c, Datetime.now().date()) : Integer.valueOf(tr.Business_Day_Count__c);
	}
}