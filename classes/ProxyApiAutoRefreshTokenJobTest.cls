@isTest public class ProxyApiAutoRefreshTokenJobTest {

    @testSetup static void inserts_custom_setting() {
        Proxy_API__c proxySettings = new Proxy_API__c();
        proxySettings.Proxy_API_ClientId__c = '10215415630';
        proxySettings.Proxy_API_ClientSecret__c = 'XBSgO86b00557n7HRWMxzXQ4Wmd307C1';
        proxySettings.Proxy_API_DefaultURL__c = 'https://iav.lendingpoint.com/proxy-api-pre-brms/rest';
        proxySettings.Proxy_API_Password__c = 'Password123!';
        proxySettings.Proxy_API_TokenExpiresTime__c =  Datetime.now().addDays(2).getTime();
        proxySettings.Proxy_API_TokenURL__c = 'https://iav.lendingpoint.com/proxy-api-pre-brms/oauth/token';
        proxySettings.Proxy_API_Token__c = '';
        proxySettings.DEV_Default_URL__c = 'http://54.173.150.238:9080/proxy-api-pre-brms/rest';
        proxySettings.Proxy_API_Username__c = 'salesforce@lendingpoint.com';
        proxySettings.DEV_Token_URL__c = 'http://54.173.150.238:9080/proxy-api-pre-brms/oauth/token';
        proxySettings.DEV_Default_URL_New__c = 'http://54.173.150.238:9080/proxy-api-pre-brms/rest';
        proxySettings.DEV_Token_URL_New__c = 'http://54.173.150.238:9080/proxy-api-pre-brms/oauth/token';
        proxySettings.Default_URL_New__c = 'https://iav.lendingpoint.com/proxy-api-post-brms/rest';
        proxySettings.Token_Expires_Time_New__c =  Datetime.now().addDays(2).getTime();
        proxySettings.Token_New__c = '';
        proxySettings.Proxy_API_Cookie__c = 'AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT';
        proxySettings.Token_URL_New__c = 'https://iav.lendingpoint.com/proxy-api-post-brms/oauth/token';
        proxySettings.Proxy_API_Cookie2__c = '; path=/; domain=.lendingpoint.com; HttpOnly';
        insert proxySettings;
    }

    @isTest static void fills_token() {

        Proxy_API__c proxySettings = [SELECT Id, Proxy_API_Token__c, Token_New__c FROM Proxy_API__c ORDER BY CreatedDate DESC LIMIT 1];

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT; path=/; domain=.lendingpoint.com; HttpOnly');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiAutoRefreshTokenJob autorefreshtoken_job = new ProxyApiAutoRefreshTokenJob();
        autorefreshtoken_job.execute(null);

        Test.stopTest();

        Proxy_API__c proxySettingsUpd = [SELECT Token_New__c, Proxy_API_Token__c FROM Proxy_API__c WHERE Id = : proxySettings.Id LIMIT 1];

        System.assertNotEquals(proxySettings.Proxy_API_Token__c, proxySettingsUpd.Proxy_API_Token__c);
        System.assertNotEquals(null, proxySettingsUpd.Proxy_API_Token__c);

        System.assertNotEquals(proxySettings.Token_New__c, proxySettingsUpd.Token_New__c);
        System.assertNotEquals(null, proxySettingsUpd.Token_New__c);

        System.assertEquals('tokentest2', proxySettingsUpd.Token_New__c);
    }

    @isTest static void updates_token() {
        Proxy_API__c proxySettings = [SELECT Id, Proxy_API_Token__c, Token_New__c
                                      FROM Proxy_API__c ORDER BY CreatedDate DESC LIMIT 1];
        proxySettings.Proxy_API_Token__c = '64bb188c-04fb-4ec0-97d6-0aa83d334cbd';
        proxySettings.Token_New__c = '64bb188c-04fb-4ec0-97d6-0aa83d334cbd';
        update proxySettings;

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT; path=/; domain=.lendingpoint.com; HttpOnly');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiAutoRefreshTokenJob autorefreshtoken_job = new ProxyApiAutoRefreshTokenJob();
        autorefreshtoken_job.execute(null);

        Test.stopTest();

        Proxy_API__c proxySettingsUpd = [SELECT Token_New__c, Proxy_API_Token__c FROM Proxy_API__c WHERE Id = : proxySettings.Id LIMIT 1];

        System.assertNotEquals(proxySettings.Proxy_API_Token__c, proxySettingsUpd.Proxy_API_Token__c);
        System.assertNotEquals(null, proxySettingsUpd.Proxy_API_Token__c);

        System.assertNotEquals(proxySettings.Token_New__c, proxySettingsUpd.Token_New__c);
        System.assertNotEquals(null, proxySettingsUpd.Token_New__c);

        System.assertEquals('tokentest2', proxySettingsUpd.Token_New__c);
    }

    @isTest static void cookie_less_than_255() {
        Proxy_API__c proxySettings = [SELECT Id, Proxy_API_Token__c, Token_New__c FROM Proxy_API__c ORDER BY CreatedDate DESC LIMIT 1];

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'TestCookie');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiAutoRefreshTokenJob autorefreshtoken_job = new ProxyApiAutoRefreshTokenJob();
        autorefreshtoken_job.execute(null);

        Test.stopTest();

        Proxy_API__c proxySettingsUpd = [SELECT Id, Proxy_API_Cookie__c, Proxy_API_Cookie2__c FROM Proxy_API__c ORDER BY CreatedDate DESC LIMIT 1];

        System.assertEquals('TestCookie', proxySettingsUpd.Proxy_API_Cookie__c);
        System.assertEquals(null, proxySettingsUpd.Proxy_API_Cookie2__c);
    }

    @isTest static void cookie_more_than_255() {
        Proxy_API__c proxySettings = [SELECT Id, Proxy_API_Token__c, Token_New__c FROM Proxy_API__c ORDER BY CreatedDate DESC LIMIT 1];

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT; path=/; domain=.lendingpoint.com; HttpOnly');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiAutoRefreshTokenJob autorefreshtoken_job = new ProxyApiAutoRefreshTokenJob();
        autorefreshtoken_job.execute(null);

        Test.stopTest();

        Proxy_API__c proxySettingsUpd = [SELECT Id, Proxy_API_Cookie__c, Proxy_API_Cookie2__c FROM Proxy_API__c ORDER BY CreatedDate DESC LIMIT 1];

        System.assertEquals('AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT', proxySettingsUpd.Proxy_API_Cookie__c);
        System.assertEquals('; path=/; domain=.lendingpoint.com; HttpOnly', proxySettingsUpd.Proxy_API_Cookie2__c);
    }

}