@isTest
public class CollectionsCasesAssignmentStructureTest {

    @isTest static void validate_constructor() {
        Test.startTest();

        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        System.assert(!usersStructure.teamsMap.isEmpty());

        for(CollectionsCasesAssignmentTeam team : usersStructure.teamsMap.values()){
            CollectionsCasesAssignmentUsersGroup usersGroup = team.usersGroupsMap.values()[0];
            Id testUserId;
            for(Id testUserIdTemp : usersGroup.getAllGroupMembersIds()){
                testUserId = testUserIdTemp;
                break;
            }
            loan__loan_Account__c contract = LibraryTest.createContractTH();
            CollectionsCasesAssignment__c cca = new CollectionsCasesAssignment__c(CL_Contract__c = contract.Id,loadBalanceGroup__c = usersGroup.getGroupName());
            insert cca;
            Case testCase = new Case(RecordTypeId = Label.RecordType_Collections_Id, CL_Contract__c = contract.Id, OwnerId = testUserId);
            insert testCase;
            break;
        }

        CollectionsCasesAssignmentStructure assignmentStructure = new CollectionsCasesAssignmentStructure(usersStructure.teamsMap);
        System.assert(!assignmentStructure.loadBalancedMap.isEmpty());

        Test.stopTest();
    }

    @isTest static void validate_getNextAssigneeId() {
        Test.startTest();

        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        System.assert(!usersStructure.teamsMap.isEmpty());

        String groupName;
        for(CollectionsCasesAssignmentTeam team : usersStructure.teamsMap.values()){
            CollectionsCasesAssignmentUsersGroup usersGroup = team.usersGroupsMap.values()[0];
            groupName = usersGroup.getGroupName();
            Id testUserId;
            for(Id testUserIdTemp : usersGroup.getAllGroupMembersIds()){
                testUserId = testUserIdTemp;
                break;
            }
            loan__loan_Account__c contract = LibraryTest.createContractTH();
            CollectionsCasesAssignment__c cca = new CollectionsCasesAssignment__c(CL_Contract__c = contract.Id,loadBalanceGroup__c = groupName);
            insert cca;
            Case testCase = new Case(RecordTypeId = Label.RecordType_Collections_Id, CL_Contract__c = contract.Id, OwnerId = testUserId);
            insert testCase;
            break;
        }

        CollectionsCasesAssignmentStructure assignmentStructure = new CollectionsCasesAssignmentStructure(usersStructure.teamsMap);
        System.assertNotEquals(null,assignmentStructure.getNextAssigneeId(groupName));
        System.assertEquals(null,assignmentStructure.getNextAssigneeId(null));
        System.assertEquals(null,assignmentStructure.getNextAssigneeId(''));

        Test.stopTest();
    }
}