@RestResource(urlMapping='/Docusign')
global with sharing class DocusignREST {
    
    @HttpPost
    global static String doPost() {    
        String content = RestContext.request.requestBody.toString();
        String envelopId = DocusignResponseHelper.getNodeValue('EnvelopeID', content);
         
        List<Account> matchingAccounts = [SELECT Id FROM Account WHERE Merchant_Agreement_Envelope_Id__c = :envelopId];
        if (matchingAccounts.size() == 0) {
            return 'No Account Found';
        }
        Account matchingAccount = matchingAccounts[0];
        
        List<Docusign_Document_Status__c> existingEnvelopes = [SELECT Id, Account__c, Envelope_Id__c, Xml_Payload__c, Sender_IP_Address__c, Envelope_Status__c FROM Docusign_Document_Status__c WHERE Envelope_Id__c = :envelopId];
        
        Docusign_Document_Status__c retVal = new Docusign_Document_Status__c();
        retVal.Account__c = matchingAccount.Id;
        retVal.Envelope_Id__c = envelopId;          
        retVal.Sender_IP_Address__c = DocusignResponseHelper.getNodeValue('SenderIPAddress', content);
        retVal.Envelope_Status__c = DocusignResponseHelper.getNodeValue('Status', DocusignResponseHelper.stripNode('RecipientStatuses', content)); //DocusignResponseHelper.getNodeValue('Status', content);    
        if (existingEnvelopes.size() != 0) {
            retVal.Id = existingEnvelopes[0].Id;
            update retVal;
        } else {
            insert retVal;
        }
        
        Docusign_Document_Status_Update__c log = new Docusign_Document_Status_Update__c();
        log.Docusign_Document_Status__c = retVal.Id;
        log.Xml_Payload__c = DocusignResponseHelper.getNodeValue('EnvelopeStatus', content);
        insert log;
        
        List<Docusign_Receipient__c> existingRecips = [SELECT Email__c FROM Docusign_Receipient__c WHERE Docusign_Document_Status__c = :retVal.Id];
        List<Docusign_Receipient__c> recipsToUpdate = new List<Docusign_Receipient__c>();
        List<Docusign_Receipient__c> recipsToInsert = new List<Docusign_Receipient__c>();        
        List<string> recipStati = DocusignResponseHelper.getNodeArray('RecipientStatus', content);
        for (string recipStatus : recipStati) {
            Docusign_Receipient__c recip = new Docusign_Receipient__c();
            boolean isExisting = false;
            for (Docusign_Receipient__c existingRecip : existingRecips) {
                if (existingRecip.Email__c == DocusignResponseHelper.getNodeValue('Email', recipStatus)) {
                    isExisting = true;
                    recip = existingRecip;
                }
            }
                        
            recip.Email__c = DocusignResponseHelper.getNodeValue('Email', recipStatus);
            recip.RecipientId__c = DocusignResponseHelper.getNodeValue('RecipientId', recipStatus);
            recip.Recipient_Status__c = DocusignResponseHelper.getNodeValue('Status', recipStatus);            
            recip.ID_Lookup_Result__c = DocusignResponseHelper.getIdLookupResult(recipStatus);
            recip.ID_Questions_Result__c = DocusignResponseHelper.getIdQuestionResult(recipStatus);                        
            
            if (isExisting) {
                recipsToUpdate.add(recip);
            } else {         
                recip.Docusign_Document_Status__c = retVal.Id;               
                recipsToInsert.add(recip);
            }
        }
        
        update recipsToUpdate;
        insert recipsToInsert;
        
        if (retVal.Envelope_Status__c == 'Completed') {
            List<Attachment> attachments = new List<Attachment>();
            List<string> pdfs = DocusignResponseHelper.getNodeArray('DocumentPDF', content);
            for (string pdf : pdfs) { 
                Attachment attachment = new Attachment();
                attachment.Body = EncodingUtil.base64Decode(DocusignResponseHelper.getNodeValue('PDFBytes', pdf));
                attachment.Name = 'Merchant_Agreement__' + DocusignResponseHelper.getNodeValue('Name', pdf);
                attachment.ContentType = 'application/pdf';
                attachment.ParentId = matchingAccount.Id; //retVal.Id; 
                attachments.add(attachment);
            }
                           
            insert attachments;
            
            matchingAccount.Merchant_Agreement_Exists__c = true;
            update matchingAccount;
        }
        
        return retVal.Id;
    }
}