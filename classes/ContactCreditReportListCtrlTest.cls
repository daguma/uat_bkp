@isTest public class ContactCreditReportListCtrlTest {

    @isTest static void ContactCreditReportListCtrl() {

        genesis__Applications__c app = LibraryTest.createApplicationTH();
        Contact c = [SELECT id, name FROM Contact WHERE id = : app.genesis__contact__c LIMIT 1];

        ApexPages.StandardController controller = new ApexPages.StandardController(c);

        ContactCreditReportListCtrl ccrlc = new ContactCreditReportListCtrl(controller);

        System.assertNotEquals(null, ccrlc.crDetailsLink);
        System.assertEquals(false, ccrlc.hasAllCRInformation);

        PageReference res = ccrlc.goBack();

        System.assertNotEquals(null, res);

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        controller = new ApexPages.StandardController(c);

        ccrlc = new ContactCreditReportListCtrl(controller);

        System.assertNotEquals(null, ccrlc.crDetailsLink);
        System.assertEquals(true, ccrlc.hasAllCRInformation);

        res = ccrlc.initialize();

        System.assertEquals(null, res);

    }

}