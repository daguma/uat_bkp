@IsTest
private class TestBatchUpdateSignedContractToVoid {

    @testSetup static void inserts_custom_setting() {
        LP_Custom__c cc = new LP_Custom__c();
        cc.Developer_Alert__c = UserInfo.getUserId();
        upsert cc;
    }

    @isTest static void updates_signed_contract() {
        BatchUpdateSignedContractToVoid batchapex = new BatchUpdateSignedContractToVoid();

        List<genesis__Applications__c> scope = new List<genesis__Applications__c>();
        genesis__Applications__c app  = LibraryTest.createApplicationTH();
        attachment att = new attachment();
        blob bodyblob = blob.ValueOf('dummybody');
        att.name = 'test.txt';
        att.Body = bodyblob;
        att.parentid = app.id;
        insert att;
        scope.add(app);

        Test.startTest();

        Database.BatchableContext BC;
        batchapex.execute(BC, scope) ;

        Test.stopTest();
    }

    @isTest static void executes_batch() {

        Test.startTest();

        BatchUpdateSignedContractToVoid batchapex = new BatchUpdateSignedContractToVoid();

        id batchprocessid = Database.executebatch(batchapex, 1);

        Test.stopTest();
    }

    @isTest static void executes_schedule() {
        Test.startTest();

        BatchUpdateSignedContractToVoid scheduleBatch = new BatchUpdateSignedContractToVoid();
        SchedulableContext SC;
        scheduleBatch.execute(SC);

        Test.stopTest();
    }
}