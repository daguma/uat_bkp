global class BatchAONCancellation implements Database.Batchable<sObject>, Schedulable,  Database.AllowsCallouts {

    String query;
    String CANCELED = 'Canceled';
    String WRITTEN_OFF = 'Closed- Written Off';
    String OBLIGATION_MET = 'Closed - Obligations met';
    String PAID_OFF_ACCOUNT = 'CLP';
    String NO_LOAN_CANCELLED_LOAN = 'CLP';
    String CALL_TYPE = 'Cancellation';

    global BatchAONCancellation() {}

    public void execute(SchedulableContext sc) {
        BatchAONCancellation  batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);
        system.debug('Process ID: ' + batchprocessid);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        query = 'SELECT Id, Name, IsProcessed__c, loan_AON_Information__c, loan_AON_Information__r.ServiceError__c, loan_AON_Information__r.IsCancellation__c,loan_AON_Information__r.Claim_Status__c, ' +
                'loan_AON_Information__r.IsAIMCancellation__c, loan_AON_Information__r.Contract__c, loan_AON_Information__r.Contract__r.Name, loan_AON_Information__r.Contract__r.loan__Loan_Status__c, ' +
                'loan_AON_Information__r.Cancellation_Date__c,loan_AON_Information__r.RetryCount__c, Cancel_Reason_Code__c ' +
                'FROM loan_AON_Cancellations__c ' +
                'WHERE IsProcessed__c = false AND loan_AON_Information__r.RetryCount__c <= 3';

        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext BC, List<loan_AON_Cancellations__c> scope) {

        for (loan_AON_Cancellations__c aonCancellation : scope) {

            String status = aonCancellation.loan_AON_Information__r.Contract__r.loan__Loan_Status__c;

            String aonCancelReasonCode = (status == CANCELED || status == WRITTEN_OFF) ? NO_LOAN_CANCELLED_LOAN : (status == OBLIGATION_MET) ? PAID_OFF_ACCOUNT : aonCancellation.Cancel_Reason_Code__c;

            String jsonResponse = 'AIM Cancellation.';

            Boolean updateCancellation = false, updateAONInfo = false, updateContract = false;

            if (String.isNotEmpty(aonCancelReasonCode)) {

                String jsonRequest  = new AONObjectsParameters.AONCancellationInputData().get_JSON(
                    new AONObjectsParameters.AONCancellationInputData(aonCancellation.loan_AON_Information__r.Contract__r.Name, aonCancelReasonCode, '0.00'));

                if (String.isNotEmpty(jsonRequest)) {

                    System.debug('isAIMCancellation ' + aonCancellation.loan_AON_Information__r.IsAIMCancellation__c);

                    if (!aonCancellation.loan_AON_Information__r.IsAIMCancellation__c) { //From LP

                        jsonResponse = ProxyApiAON.cancellation(jsonRequest);

                        AONObjectsParameters.AONResponse aonResponse = new AONObjectsParameters.AONResponse(jsonResponse);

                        System.debug('aonResponse ' + aonResponse);

                        if (aonResponse != null) {

                            System.debug('errorMessage ' + aonResponse.errorMessage);

                            if (String.isNotEmpty(aonResponse.errorMessage)) {

                                System.debug('Error Message');
                                aonCancellation.loan_AON_Information__r.ServiceError__c = (aonResponse.errorMessage.length() > 255) ? aonResponse.errorMessage.substring(0, 254) : aonResponse.errorMessage;
                                aonCancellation.loan_AON_Information__r.RetryCount__c += 1;
                                updateAONInfo = true;

                            } else {

                                System.debug('LP Successful');
                                aonCancellation.Cancel_Reason_Code__c = aonCancelReasonCode;
                                aonCancellation.Effective_Date__c = System.now();
                                aonCancellation.Refund_Amount__c = 0.00; //For now we don't have this value, Default 0.00;
                                //aonCancellation.IsProcessed__c = true;
                                //aonCancellation.loan_AON_Information__r.IsCancellation__c = true;
                                //aonCancellation.loan_AON_Information__r.Contract__r.AON_Enrollment__c = false;
                                //aonCancellation.loan_AON_Information__r.RetryCount__c = 0;

                                updateCancellation = true;
                                updateAONInfo = true;
                                updateContract = true;

                            }
                        }
                    } else { //From AON
                        System.debug('AON Successful');

                        updateCancellation = true;
                        updateAONInfo = true;
                        updateContract = true;
                    }

                    if (updateCancellation) {
                        System.debug('Update Cancellation');

                        aonCancellation.loan_AON_Information__r.IsCancellation__c = true;
                        aonCancellation.IsProcessed__c = true;
                        aonCancellation.loan_AON_Information__r.RetryCount__c = 0;

                        if (status != CANCELED && status != WRITTEN_OFF && status != OBLIGATION_MET)
                            aonCancellation.loan_AON_Information__r.Contract__r.AON_Enrollment__c = false;

                        String claimStatus = aonCancellation.loan_AON_Information__r.Claim_Status__c;
                        aonCancellation.loan_AON_Information__r.Claim_Status__c = (claimStatus == 'A') ? 'C' : claimStatus;
                        aonCancellation.loan_AON_Information__r.Cancellation_Date__c = System.now();
                        update aonCancellation;
                    }

                    if (updateAONInfo) update aonCancellation.loan_AON_Information__r;

                    if (updateContract) update aonCancellation.loan_AON_Information__r.Contract__r;

                    AONProcesses.setContactLoggging(CALL_TYPE, jsonRequest, jsonResponse, aonCancellation.loan_AON_Information__r.Contract__c);
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC) {}

}