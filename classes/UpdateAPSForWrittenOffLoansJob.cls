Global class UpdateAPSForWrittenOffLoansJob extends loan.MFiFlexBatchJob {
    private static String JOB_NAME = 'Upate APS For Written Off Loans';
    private String query;
    private List<loan__Loan_Account__c> failedLoans = new List<loan__Loan_Account__c>();
    
    global UpdateAPSForWrittenOffLoansJob() {
        super(JOB_NAME, getUpdateAPSForWrittenOffLoansJobquery());
    }
    
    global UpdateAPSForWrittenOffLoansJob(String query) {
        super(JOB_NAME, query);
        this.query = query;
    }
    
    private static String getUpdateAPSForWrittenOffLoansJobquery() {
        String query = 'SELECT Id, ' +
                              'loan__Loan_Status__c ' +
                       'FROM loan__Loan_Account__c ' +
                       'WHERE loan__Invalid_Data__c = false ' +
                       '  AND loan__Loan_Status__c = \'Closed- Written Off\'';
        return query;
    }
    
    public override void submitNextJob() {}//does nothing
    
    public override void doStart(Database.BatchableContext bc) {}

    public override void doExecute(Database.BatchableContext bc, List<sObject> scope) {
        List<loan__Loan_Account__c> loanAccounts = (List<loan__Loan_Account__c>)scope;
        Set<Id> loanIds = new Set<Id>();
        List<loan__Automated_Payment_Setup__c> setups;
        List<loan__Automated_Payment_Setup__c> UpdateAps = new List<loan__Automated_Payment_Setup__c>();
        
        try {
            for(loan__Loan_Account__c la : loanAccounts) {
                loanIds.add(la.id);
            }
            
            setups = [SELECT Id,
                             loan__CL_Contract__c,
                             loan__Active__c
                      FROM loan__Automated_Payment_Setup__c
                      WHERE loan__CL_Contract__c IN :loanIds
                     ];
            
            //Deactivating all the APS for written off loan.
            for(loan__Automated_Payment_Setup__c aps : setups) {
                aps.loan__Active__c = false;
                UpdateAps.add(aps);
            }
            update UpdateAps;
        }
        catch(Exception exe) {
            failedLoans.addAll(loanAccounts);
            system.debug(LoggingLevel.ERROR, 'Error in execution of Loans: '+failedLoans);
            system.debug(LoggingLevel.ERROR, 'Error Message :: ' + exe.getMessage() + ' Stack Trace :: ' + exe.getStackTraceString() + ' Line Number :: ' + exe.getLineNumber());
        }
    }
    
    public override void doFinish(Database.BatchableContext bc) {
        if(failedLoans.size() == 0) {
            system.debug(LoggingLevel.ERROR, 'Migration for all the loans is successful');
        }
    }
}