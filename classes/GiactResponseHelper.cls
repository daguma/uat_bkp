public class GiactResponseHelper {
    public static DateTime tryGetDate(string dtString) {
        try {
            return (DateTime) JSON.deserialize('"' + dtString + '"', DateTime.class);
        } catch (Exception ex) {
            return null;
        }
    }

    public static string getNodeValue(string nodeName, string content) {
        string nodeNameStuffed = '<'+nodeName+'>';
        string endNodeName = '</'+nodeName+'>';
        integer first = content.indexOf(nodeNameStuffed);
        integer second = content.indexOf(endNodeName);
        if (first == -1 || second == -1)
            return null;
            
        return content.substring(first + nodeNameStuffed.length(), second);
    }
    
    public static List<string> getNodeArray(string nodeName, string bod) {
        string nodeNameStuffed = '<'+nodeName+'>';
        string endNodeName = '</'+nodeName+'>';
        
        integer seek = 0;
        List<string> results = new List<string>();
        string content = bod.substring(seek);
        while (true) {  
            content = content.substring(seek);    
            system.debug(content);
            
            integer first = content.indexOf(nodeNameStuffed);
            system.debug(first);
            integer second = content.indexOf(endNodeName);
            system.debug(second);
            if (first == -1 || second == -1)
                break;       
            
            string item = content.substring(first + nodeNameStuffed.length(), second);
            results.add(item);
            system.debug(GiactResponseHelper.getNodeValue('ItemReferenceId', item));
            
            seek = second + endNodeName.length();
            system.debug(seek);
        }
        
        system.debug(seek);
        
        return results;
    }
}