@isTest public class applicationfundingAutomationTest {
    
    @isTest static void validate_application() {
        Opportunity app = TestHelper.createApplication();

        CutOff_time_on_automated_funding__c fundSett= new CutOff_time_on_automated_funding__c();
        DateTime gappTime = System.Now();
        String dummyEDValue = gappTime.format('h:mm a');
        integer colonIndex = DummyEDValue.indexOf(':');
        fundSett.name = 'CutOff time';
        fundSett.AM_PM__c = 'PM';
        fundSett.Hours__c = DummyEDValue.substringBefore(':');
        fundSett.Minutes__c = DummyEDValue.substring(colonIndex+1, colonIndex+3);
        insert fundSett;
        
        app.Status__c = 'Contract Pkg approved';
        app.Deal_is_Ready_to_Fund__c = true;
        app.FS_Assigned_To_Val__c = 'Eddie Verdin'; 
        try {
            update app;
            
            List<Opportunity> appList = new List<Opportunity>();
            appList.add(app);
            applicationfundingAutomation.FundingAutomation(appList);
            System.assertEquals(app.Status__c, 'Contract Pkg approved');
        } catch (Exception e) {
            System.debug(e.getStackTraceString());
        }
    }
    
    @isTest static void validate_application_with_existing_contract() {
        CutOff_time_on_automated_funding__c fundSett = new CutOff_time_on_automated_funding__c();
        fundSett.name = 'CutOff time';
        fundSett.AM_PM__c = 'PM';
        fundSett.Hours__c = '6';
        fundSett.Minutes__c = '59';
        insert fundSett;
        
        loan__Loan_Account__c ContractObj = TestHelper.createContract();
        
        Opportunity app = [Select Id, Deal_is_Ready_to_Fund__c, Contact__c, Status__c, Lending_Account__c, FS_Assigned_To_Val__c  from Opportunity LIMIT 1];

        app.Status__c = 'Contract Pkg approved';
        app.Deal_is_Ready_to_Fund__c = true;
        app.FS_Assigned_To_Val__c = 'Eddie Verdin';
        app.Lending_Account__c = ContractObj.id;
        app.Payment_Frequency__c = 'MONTHLY';
        update app;
        
        List<Opportunity> appList= new List<Opportunity>();
        appList.add(app);
        applicationfundingAutomation.FundingAutomation(appList);
        System.assertEquals(app.Status__c,'Contract Pkg approved');
    }
    
     @isTest static void validate_application_with_generated_contract() {
        CutOff_time_on_automated_funding__c fundSett = new CutOff_time_on_automated_funding__c();
        fundSett.name = 'CutOff time';
        fundSett.AM_PM__c = 'PM';
        fundSett.Hours__c = '6';
        fundSett.Minutes__c = '59';
        insert fundSett;
        
        loan__Loan_Account__c ContractObj = TestHelper.createContract();
        ContractObj.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        update ContractObj;
         
        AON_Product_Codes__c aonSetting = new AON_Product_Codes__c();
        aonSetting.ProductCode__c = 'LPOPTION01';
        aonSetting.Name = 'OPTIONAL MONTHLY';
        insert aonSetting; 
         
        System.debug('Payment Frequency ' + [SELECT Payment_Frequency_Masked__c FROM loan__Loan_Account__c WHERE Id =: contractObj.Id]);
         
        Opportunity app = [Select Id, Deal_is_Ready_to_Fund__c, Contact__c, Status__c, Lending_Account__c, FS_Assigned_To_Val__c  from Opportunity LIMIT 1];
        app.Status__c = 'Contract Pkg approved';
        app.Deal_is_Ready_to_Fund__c = True;
        app.FS_Assigned_To_Val__c = 'Eddie Verdin';
        app.Lending_Account__c = ContractObj.id;
        app.Expected_Disbursal_Date__c = System.Today();
        app.AON_Enrollment__c = true;
        update app;
        
        List<Opportunity> appList = new List<Opportunity>();
        appList.add(app);
        Map<id,string> ExpMap = new Map<id,string>();
        ExpMap.put(app.id, string.valueOf(app.Expected_Disbursal_Date__c));
        List<Logging__c> logList = new List<Logging__c>();
        
        applicationfundingAutomation.contractCreation(appList,ExpMap,logList);
        System.assertEquals(app.Status__c,'Contract Pkg approved');
    }
    
    @isTest static void validate_applicationWithGeneratedContractWithin24hrsWithCanceledContract() {
        CutOff_time_on_automated_funding__c fundSett = new CutOff_time_on_automated_funding__c();
        fundSett.name = 'CutOff time';
        fundSett.AM_PM__c = 'PM';
        fundSett.Hours__c = '6';
        fundSett.Minutes__c = '59';
        insert fundSett;
        
        applicationFundingAutomationValidatio__c validationSettings = new applicationFundingAutomationValidatio__c();
        validationSettings.name = 'fundingAutomation24hrCheck';
        validationSettings.Allow_24_hrs_check__c = true;
        validationSettings.Time__c = 24;
        insert validationSettings;
        
        loan__Loan_Account__c ContractObj = TestHelper.createContract();
        
        Opportunity app = [Select Id, Deal_is_Ready_to_Fund__c, Contact__c, Status__c, Lending_Account__c, FS_Assigned_To_Val__c  from Opportunity LIMIT 1];
        app.Status__c = 'Contract Pkg approved';
        app.Deal_is_Ready_to_Fund__c = true;
        app.FS_Assigned_To_Val__c = 'Eddie Verdin';
        app.Lending_Account__c = ContractObj.id;
        app.Expected_Disbursal_Date__c = System.Today();
        app.Payment_Frequency__c = 'MONTHLY';
        update app;
        
        List<Opportunity> appList = new List<Opportunity>();
        appList.add(app);
        Map<id,string> ExpMap = new Map<id,string>();
        ExpMap.put(app.id, string.valueOf(app.Expected_Disbursal_Date__c));
        List<Logging__c> logList = new List<Logging__c>();
        
        ContractObj.loan__Loan_Status__c = 'Canceled';
        update ContractObj;
        
        applicationfundingAutomation.contractCreation(appList,ExpMap,logList);        
    }
    
    @isTest static void validate_application_with_canceled_contract() {
        CutOff_time_on_automated_funding__c fundSett = new CutOff_time_on_automated_funding__c();
        fundSett.name = 'CutOff time';
        fundSett.AM_PM__c = 'PM';
        fundSett.Hours__c = '6';
        fundSett.Minutes__c = '59';
        insert fundSett;
        
        loan__Loan_Account__c ContractObj = TestHelper.createContract();

        ContractObj.loan__Loan_Status__c = 'Canceled';
        update ContractObj;
        
        Opportunity app = [Select Id, Deal_is_Ready_to_Fund__c, Contact__c, Status__c, Lending_Account__c, FS_Assigned_To_Val__c  from Opportunity LIMIT 1];

        app.Status__c = 'Contract Pkg Approved';
        app.Deal_is_Ready_to_Fund__c = true;
        app.FS_Assigned_To_Val__c = 'Eddie Verdin';
        app.Lending_Account__c = ContractObj.id;
        app.Payment_Frequency__c = 'MONTHLY';
        update app;
        
        List<Opportunity> appList = new List<Opportunity>();
        appList.add(app);

        applicationfundingAutomation.FundingAutomation(appList);
        
        System.assertEquals(null, [SELECT Lending_Account__c FROM Opportunity WHERE Id =: app.Id].Lending_Account__c);
    }
    
    @isTest static void validate_application_with_aon_product() {
        
        Integer countPeriodicFee = [SELECT COUNT() FROM loan__Periodic_Fee_Setup__c];
        
        loan__Fee__c fee = new loan__Fee__c();
        fee.Name = 'AON';
        fee.loan__Amount__c = 6.31;
        insert fee;
        
        CutOff_time_on_automated_funding__c fundSett = new CutOff_time_on_automated_funding__c();
        fundSett.name = 'CutOff time';
        fundSett.AM_PM__c = 'PM';
        fundSett.Hours__c = '6';
        fundSett.Minutes__c = '59';
        insert fundSett;
        
        loan__Loan_Account__c ContractObj = TestHelper.createContract();
        ContractObj.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        update ContractObj;
         
        AON_Product_Codes__c aonSetting = new AON_Product_Codes__c();
        aonSetting.ProductCode__c = 'LPOPTION01';
        aonSetting.Name = 'OPTIONAL MONTHLY';
        insert aonSetting; 
         
        System.debug('Payment Frequency ' + [SELECT Payment_Frequency_Masked__c FROM loan__Loan_Account__c WHERE Id =: contractObj.Id]);
         
        Opportunity app = [Select Id, Deal_is_Ready_to_Fund__c, Contact__c, Status__c, Lending_Account__c, FS_Assigned_To_Val__c  from Opportunity LIMIT 1];
        app.Status__c = 'Contract Pkg approved';
        app.Deal_is_Ready_to_Fund__c = True;
        app.FS_Assigned_To_Val__c = 'Eddie Verdin';
        app.Lending_Account__c = ContractObj.id;
        app.Expected_Disbursal_Date__c = System.Today();
        app.AON_Enrollment__c = true;
        app.AON_Fee__c = 6.31;
        app.Term__c = 24;
        update app;
        
        List<Opportunity> appList = new List<Opportunity>();
        appList.add(app);
        Map<id,string> ExpMap = new Map<id,string>();
        ExpMap.put(app.id, string.valueOf(app.Expected_Disbursal_Date__c));
        List<Logging__c> logList = new List<Logging__c>();
        
        applicationfundingAutomation.contractCreation(appList,ExpMap,logList);
        
        //System.assert([SELECT COUNT() FROM loan__Periodic_Fee_Setup__c] > countPeriodicFee, 'Insert Periodic Fee');
    }

}