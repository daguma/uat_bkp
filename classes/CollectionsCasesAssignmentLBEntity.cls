public class CollectionsCasesAssignmentLBEntity {

    @TestVisible Map<Id,Long> loadBalancedMap;
    Id leastLoadedUserId;

    public CollectionsCasesAssignmentLBEntity(Map<Id,Long> loadBalance, Id leastLoadedUser){
        this.leastLoadedUserId = leastLoadedUser;
        this.loadBalancedMap = loadBalance;
    }

    public Id getNextAssigneeId(){
        Id nextAvailableCollectorId;
        if(this.loadBalancedMap == null || this.loadBalancedMap.isEmpty()){
            return nextAvailableCollectorId;
        }
        List<Id> idsList = new List<Id>(this.loadBalancedMap.keySet());
        Integer listItem = 0;
        Long load = 0;
        do{
            nextAvailableCollectorId = idsList.get(listItem);
            load = (Long) this.loadBalancedMap.get(nextAvailableCollectorId);
            listItem++;
        }while(listItem < idsList.size() && load == 0);
        if(load == 0){
            return this.leastLoadedUserId;
        }else{
            this.loadBalancedMap.remove(nextAvailableCollectorId);
            this.loadBalancedMap.put(nextAvailableCollectorId, load.intValue()-1);
            return nextAvailableCollectorId;
        }
    }
}