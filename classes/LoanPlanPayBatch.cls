global class LoanPlanPayBatch implements Database.Batchable<sObject>,Schedulable , Database.Stateful {
    public String message = '', query = '';

    Integer igCount = 0, errCount = 0, totCount = 0;
    
    public void execute(SchedulableContext sc) { 
       LoanPlanPayBatch batchapex = new LoanPlanPayBatch();
       id batchprocessid = Database.executebatch(batchapex, 1);
       system.debug('Process ID: ' + batchprocessid);
    }
    
    // ************Start Method which retrieves the records of loan Plan Payment *********    
    global Database.QueryLocator start(Database.BatchableContext BC){
       try{
           String d=Datetime.now().adddays(2).format('yyyy-MM-dd');
           query = 'SELECT id,CreatedDate, Account_Number__c, Processed_Date__c, ACH_Debit_Date__c,Payment_Amount__c,Routing_Number__c, Loan_Account__c FROM loan_Plan_Payment__c WHERE ACH_Debit_Date__c='+d+' AND Status__c =\'pending\' Order by Createddate';     
           //query = 'SELECT id,CreatedDate, Account_Number__c, ACH_Debit_Date__c,Payment_Amount__c, Loan_Account__c FROM loan_Plan_Payment__c WHERE ACH_Debit_Date__c='+d+' AND Status__c =\'pending\' Order by Createddate';     
       }catch(exception ex){
           System.debug('Exception:'+ ex.getMessage());
           WebToSFDC.notifyDev('LoanPlanPayBatch ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage());
       }
       return Database.getQueryLocator(query);
    }
    
   global void execute(Database.BatchableContext BC,List<sObject> scope){
   try{ 
        loan_Plan_Payment__c[]  listToUpdatePlanPayment = new List<loan_Plan_Payment__c>();
        loan__Loan_Account__c[] listToUpdateContract = new List<loan__Loan_Account__c>();
        List<loan_Plan_Payment__c> lPPRow = (List<loan_Plan_Payment__c>)scope;
        Map<ID, loan_Plan_Payment__c> maptoPlanPayment = new Map<Id,loan_Plan_Payment__c>();
        System.debug(lPPRow);
         
        for(loan_Plan_Payment__c getlPPRowIDs : lPPRow){
           maptoPlanPayment.put(getlPPRowIDs.Loan_Account__c,getlPPRowIDs);
        }  
          
        for(loan__Loan_Account__c lLA : [SELECT loan__OT_ACH_Account_Number__c, loan__OT_Borrower_ACH__c, loan__Borrower_ACH__c, loan__OT_ACH_Debit_Date__c,loan__OT_ACH_Payment_Amount__c,loan__ACH_Next_Debit_Date__c , loan__OT_ACH_Routing_Number__c, loan__ACH_Account_Number__c,loan__ACH_Routing_Number__c FROM loan__Loan_Account__c WHERE Id IN: maptoPlanPayment.keyset()]){
            boolean needToUpdate = false;  
            loan_Plan_Payment__c lPP = maptoPlanPayment.get(lLA.id);
            
            if (lPP.ACH_Debit_Date__c != null && (lLa.loan__ACH_Next_Debit_Date__c == null || lLa.loan__ACH_Next_Debit_Date__c  <> lPP.ACH_Debit_Date__c || lLa.loan__ACH_Next_Debit_Date__c  <> lPP.ACH_Debit_Date__c.addDays(-1) ) ) {                     
              if(lLA.loan__OT_ACH_Account_Number__c == null && lLA.loan__OT_ACH_Routing_Number__c == null){                                                                                                                          
                 if(lPP.Account_Number__c <> null && lPP.Routing_Number__c <> null){  
                     lLA.loan__OT_ACH_Account_Number__c = lPP.Account_Number__c;
                     lLA.loan__OT_ACH_Routing_Number__c = lPP.Routing_Number__c;
                     needToUpdate = true;                       
                 }else if(lLA.loan__ACH_Account_Number__c <> null && lLA.loan__ACH_Routing_Number__c <> null){
                     lLA.loan__OT_ACH_Account_Number__c = lLA.loan__ACH_Account_Number__c;
                     lLA.loan__OT_ACH_Routing_Number__c = lLA.loan__ACH_Routing_Number__c;
                     needToUpdate = true;                                         
                 }
              }
                                   
              if(lLA.loan__OT_ACH_Debit_Date__c == null && lPP.ACH_Debit_Date__c <> null && (lLA.loan__OT_ACH_Payment_Amount__c== null || lLA.loan__OT_ACH_Payment_Amount__c == 0) && lPP.Payment_Amount__c <> null){ 
                   lLA.loan__OT_ACH_Debit_Date__c = lPP.ACH_Debit_Date__c;
                   lLA.loan__OT_ACH_Payment_Amount__c= Double.ValueOf(lPP.Payment_Amount__c);
                   if (!needToUpdate) lLA.loan__OT_Borrower_ACH__c = lLA.loan__Borrower_ACH__c;
                   needToUpdate = true;
               }
            }               
            if(needToUpdate){
                 listToUpdateContract.add(lLA);   
                 LPP.Processed_Date__c = Date.today();                        
                 lPP.Status__c = 'Processed';
                 totCount += 1;
            }else{              
                igCount += 1; 
                 lPP.Status__c = 'Ignored';                           
            } 
            listToUpdatePlanPayment.add(lPP);
       }
         
      if( listToUpdateContract.size()>0){
          update listToUpdateContract;
      }
      if( listToUpdatePlanPayment.size()>0){
          update listToUpdatePlanPayment;
      }

      totCount += listToUpdateContract.size(); 
    }catch(exception ex){
          errCount++;
          message += ex.getLineNumber() + '\n Detail = ' + ex.getMessage() + '\n\n';
      } 
    }
    
   global void finish(Database.BatchableContext BC){
        WebToSFDC.notifyDev('LoanPlanPayBatch Finished - Errors = ' + errCount, 'Contracts processed = ' + totCount + '\nIgnored = ' + igCount  + '\n\nLog: \n\n' + message + '\n\n' , UserInfo.getUserId() );
    }
}