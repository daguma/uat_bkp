public abstract class CollectionsCasesAssignmentTeam {

    public static final String  TEAM_COLLECTIONS = 'TeamCollections',
                                TEAM_LMS = 'TeamLms',
                                TEAM_DMC = 'TeamDmc';
    public Map<String, CollectionsCasesAssignmentUsersGroup> usersGroupsMap;
    public Map<Integer, String> groupsNamesByDpdMap;

    public CollectionsCasesAssignmentTeam(List<CollectionsCasesAssignmentUsersGroup> usersGroupsList){
        this.usersGroupsMap = new Map<String, CollectionsCasesAssignmentUsersGroup>();
        this.groupsNamesByDpdMap = new Map<Integer, String>();
        if(usersGroupsList != null){
            for(CollectionsCasesAssignmentUsersGroup usersGroup : usersGroupsList){
                this.usersGroupsMap.put(usersGroup.publicUserGroupName ,usersGroup);
                for (Integer i = usersGroup.minimumDPD; i <= usersGroup.maximumDPD; i++) {
                    this.groupsNamesByDpdMap.put(i,usersGroup.publicUserGroupName);
                }
            }
        }
    }

    public List<Id> getAllTeamMembersIds(){
        List<Id> result = new List<Id>();
        for(CollectionsCasesAssignmentUsersGroup usersGroup : usersGroupsMap.values()){
            result.addAll(usersGroup.getAllGroupMembersIds());
        }
        return result;
    }

    public String getGroupNameByDpd(Integer dpd){
        if(dpd == null) return null;
        return this.groupsNamesByDpdMap.get(dpd);
    }

    //For each team, the implementation of this method is different
    public abstract Boolean isTriggerHit(Id theUserId, Integer dpd);

    public class CollectionsTeam extends CollectionsCasesAssignmentTeam {
        public CollectionsTeam(List<CollectionsCasesAssignmentUsersGroup> usersGroupsList){
            super(usersGroupsList);
        }
        public override Boolean isTriggerHit(Id theUserId, Integer dpd) {
            //return ((dpd>=30 && dpd<=59 && !isLevel2Collector(userId) && !isLevel3Collector(userId)) || (dpd>=60 && !isLevel3Collector(userId)));
            if((dpd >= 31 && dpd <= 60
                    && !this.usersGroupsMap.get(getGroupNameByDpd(31)).isUserMemberOfGroup(theUserId)
                    && !this.usersGroupsMap.get(getGroupNameByDpd(61)).isUserMemberOfGroup(theUserId)
                    && !this.usersGroupsMap.get(getGroupNameByDpd(91)).isUserMemberOfGroup(theUserId)
                    && !this.usersGroupsMap.get(getGroupNameByDpd(121)).isUserMemberOfGroup(theUserId))
                    ||
                    (dpd >= 61 && dpd <= 90
                            && !this.usersGroupsMap.get(getGroupNameByDpd(61)).isUserMemberOfGroup(theUserId)
                            && !this.usersGroupsMap.get(getGroupNameByDpd(91)).isUserMemberOfGroup(theUserId)
                            && !this.usersGroupsMap.get(getGroupNameByDpd(121)).isUserMemberOfGroup(theUserId))
                    ||
                    (dpd >= 91 && dpd <= 120
                            && !this.usersGroupsMap.get(getGroupNameByDpd(91)).isUserMemberOfGroup(theUserId)
                            && !this.usersGroupsMap.get(getGroupNameByDpd(121)).isUserMemberOfGroup(theUserId))
                    ||
                    (dpd >= 121 && dpd <= 180
                            && !this.usersGroupsMap.get(getGroupNameByDpd(121)).isUserMemberOfGroup(theUserId))
                    ){
                return true;
            }
            return false;
        }
    }

    public class LmsTeam extends CollectionsCasesAssignmentTeam {
        public LmsTeam(List<CollectionsCasesAssignmentUsersGroup> usersGroupsList){
            super(usersGroupsList);
        }
        public override Boolean isTriggerHit(Id theUserId, Integer dpd) {
            //return ((dpd>=31 && dpd<=180 && !isLevel2Collector(userId));
            if((dpd >= 31 && dpd <= 60
                    && !this.usersGroupsMap.get(getGroupNameByDpd(31)).isUserMemberOfGroup(theUserId)
                    && !this.usersGroupsMap.get(getGroupNameByDpd(61)).isUserMemberOfGroup(theUserId)
                    && !this.usersGroupsMap.get(getGroupNameByDpd(91)).isUserMemberOfGroup(theUserId)
                    && !this.usersGroupsMap.get(getGroupNameByDpd(121)).isUserMemberOfGroup(theUserId))
                    ||
                    (dpd >= 61 && dpd <= 90
                            && !this.usersGroupsMap.get(getGroupNameByDpd(61)).isUserMemberOfGroup(theUserId)
                            && !this.usersGroupsMap.get(getGroupNameByDpd(91)).isUserMemberOfGroup(theUserId)
                            && !this.usersGroupsMap.get(getGroupNameByDpd(121)).isUserMemberOfGroup(theUserId))
                    ||
                    (dpd >= 91 && dpd <= 120
                            && !this.usersGroupsMap.get(getGroupNameByDpd(91)).isUserMemberOfGroup(theUserId)
                            && !this.usersGroupsMap.get(getGroupNameByDpd(121)).isUserMemberOfGroup(theUserId))
                    ||
                    (dpd >= 121 && dpd <= 180
                            && !this.usersGroupsMap.get(getGroupNameByDpd(121)).isUserMemberOfGroup(theUserId))
                    ){
                return true;
            }
            return false;
        }
    }

    public class DmcTeam extends CollectionsCasesAssignmentTeam {
        public DmcTeam(List<CollectionsCasesAssignmentUsersGroup> usersGroupsList){
            super(usersGroupsList);
        }
        public override Boolean isTriggerHit(Id theUserId, Integer dpd) {
            //return ((dpd>=30 && dpd<=59 && !isLevel2DmcCollector(userId)) || (dpd>=60 && !isLevel3DmcCollector(userId)));
            return ((dpd>=31 && dpd<=90
                    && !this.usersGroupsMap.get(getGroupNameByDpd(31)).isUserMemberOfGroup(theUserId))
                    || (dpd>=91 && !this.usersGroupsMap.get(getGroupNameByDpd(91)).isUserMemberOfGroup(theUserId)));
        }
    }
}