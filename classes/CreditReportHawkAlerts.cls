public with sharing class CreditReportHawkAlerts {

    private static final String COMMERCIAL_ADDRESS = 'Commercial Address';
    private static final String INSTITUTIONAL_ADDRESS = 'Institutional Address';
    private static final String GOVERNMENTAL_ADDRESS = 'Governmental Address';
    private static final String ADDRESS_INVESTIGATION = 'Address Investigation';
    private static final String SSN_INVESTIGATION = 'SSN Investigation';
    private static final String SYSTEM_MESSAGE = 'System Message';
    private static final String TELEPHONE_NUMBER_MESSAGE = 'Telephone Number Message';
    private static final String HAWK_ALERT_SEGMENT_ID = 'MC01';
    private static final String ALERT_RANGE = 'AlertRange';
    private static final String DESCRIPTION = 'Description';
    public static final String CREDIT_REVIEW = 'Credit Review';
    public static final String CREDIT_DECLINED = 'Declined (or Unqualified)';
    public static final String SYNTHETIC_ID = 'Synthetic Id';

    private static final List<String> alertRanges = new List<String> {
        COMMERCIAL_ADDRESS, INSTITUTIONAL_ADDRESS, GOVERNMENTAL_ADDRESS, ADDRESS_INVESTIGATION, 
        SSN_INVESTIGATION, SYSTEM_MESSAGE, TELEPHONE_NUMBER_MESSAGE, SYNTHETIC_ID
    };

    private static map<String, map<String, String>> hawkAlertsMapper = new map<String, map<String, String>> {
        '0001' => new map<String, String>
        {
            ALERT_RANGE => COMMERCIAL_ADDRESS,
            DESCRIPTION => '0001 - Input/File (Current/Previous) Address Is A Mail Receiving/Forwarding Service'
        },
        '0002' => new map<String, String>
        {
            ALERT_RANGE => COMMERCIAL_ADDRESS,
            DESCRIPTION => '0002 - Input/File (Current/Previous) Address Is A Hotel/Motel Or Temporary Residence'
        },
        '0004' => new map<String, String>
        {
            ALERT_RANGE => COMMERCIAL_ADDRESS,
            DESCRIPTION => '0004 - Input/File (Current/Previous) Address Is A Camp Site'
        },
        '0007' => new map<String, String>
        {
            ALERT_RANGE => COMMERCIAL_ADDRESS,
            DESCRIPTION => '0007 - Input/File (Current/Previous) Address Is A Restaurant /Bar/Night Club'
        },
        '0501' => new map<String, String>
        {
            ALERT_RANGE => INSTITUTIONAL_ADDRESS,
            DESCRIPTION => '0501 - Input/File (Current/Previous) Address Is A Correctional Institution'
        },
        '0502' => new map<String, String>
        {
            ALERT_RANGE => INSTITUTIONAL_ADDRESS,
            DESCRIPTION => '0502 - Input/File (Current/Previous) Address Is A Hospital Or Clinic'
        },
        '0503' => new map<String, String>
        {
            ALERT_RANGE => INSTITUTIONAL_ADDRESS,
            DESCRIPTION => '0503 - Input/File (Current/Previous) Address Is A Nursing Home'
        },
        '1001' => new map<String, String>
        {
            ALERT_RANGE => GOVERNMENTAL_ADDRESS,
            DESCRIPTION => '1001 - Input/File (Current/Previous) Address Is A U.S. Post Office'
        },
        '1503' => new map<String, String>
        {
            ALERT_RANGE => ADDRESS_INVESTIGATION,
            DESCRIPTION => '1503 - Input/File (Current/Previous) Address Has Been Reported Misused And Requires Further Investigation (Unit: #)'
        },
        '1504' => new map<String, String>
        {
            ALERT_RANGE => ADDRESS_INVESTIGATION,
            DESCRIPTION => '1504 - Input/File (Current/Previous) Address Is A Multi-Unit Building Reported Misused And Requires Further Investigation (Unit: #)'
        },
        '2001' => new map<String, String>
        {
            ALERT_RANGE => ADDRESS_INVESTIGATION,
            DESCRIPTION => '2001 - Input/File (Current/Previous) Address Is Reported Used In True-Name Fraud Or Credit Fraud'
        },
        '3001' => new map<String, String>
        {
            ALERT_RANGE => SSN_INVESTIGATION,
            DESCRIPTION => '3001 - Input/file SSN reported as suspicious or minor SSN'
        },
        '3003' => new map<String, String>
        {
            ALERT_RANGE => SSN_INVESTIGATION,
            DESCRIPTION => '3003 - Input/file SSN reported misused and requires further investigation'
        },
        '3501' => new map<String, String>
        {
            ALERT_RANGE => SSN_INVESTIGATION,
            DESCRIPTION => '3501 - Input/file SSN reported used in true-name fraud or credit fraud'
        },
        '4001' => new map<String, String>
        {
            ALERT_RANGE => SSN_INVESTIGATION,
            DESCRIPTION => '4001 - Input/file SSN reported deceased'
        },
        '4501' => new map<String, String>
        {
            ALERT_RANGE => SSN_INVESTIGATION,
            DESCRIPTION => '4501 - Input/file SSN is not likely issued prior to June 2011'
        },
        '5503' => new map<String, String>
        {
            ALERT_RANGE => SSN_INVESTIGATION,
            DESCRIPTION => '5503 - Input/file SSN issued within last (2,5,10) years; year issued: xxxx-xxxx; state: xx; (est. Age obtained: xx to xx)'
        },
        '5504' => new map<String, String>
        {
            ALERT_RANGE => SSN_INVESTIGATION,
            DESCRIPTION => '5504 - Input/file SSN issued:xxxx-xxxx; state:xx; (est. Age obtained:xx to xx)'
        },
        '6000' => new map<String, String>
        {
            ALERT_RANGE => SSN_INVESTIGATION,
            DESCRIPTION => '6000 - Input/file SSN used in death benefits claim for John Consumer. DOB:01/01/1950. DOC: 02/02/1990. Zip code where benefits were paid is 60657, most likely Chicago, IL. Zip code last residence is 60657, most likely Chicago, IL.'
        },
        '9001' => new map<String, String>
        {
            ALERT_RANGE => SYSTEM_MESSAGE,
            DESCRIPTION => '9001 - Input address(es), SSN and/or telephone number reported together in suspected misuse.'
        },
        '8001' => new map<String, String>
        {
            ALERT_RANGE => TELEPHONE_NUMBER_MESSAGE,
            DESCRIPTION => '8001 - Input/file telephone number reported used in true-name fraud or credit fraud.'
        },
        '9011' => new map<String, String>
        {
            ALERT_RANGE => SYNTHETIC_ID,
            DESCRIPTION => '9011 - Synthetic Fraud Id.'
        }/*,
        '2010' => new map<String, String>
        {
            ALERT_RANGE => SYNTHETIC_ID,
            DESCRIPTION => '2010 - Synthetic Fraud Address .'
        },
        '9016' => new map<String, String>
        {
            ALERT_RANGE => SYNTHETIC_ID,
            DESCRIPTION => '9016 - Synthetic Fraud Elements Not Verified.'
        },
        '9017' => new map<String, String>
        {
            ALERT_RANGE => SYNTHETIC_ID,
            DESCRIPTION => '9017 - Synthetic Fraud Elements Verified.'
        }*/
    };

    private static map<String, String> statusMapper = new map<String, String> {
        '0001' => CREDIT_REVIEW,
        '0007' => CREDIT_REVIEW,
        '1001' => CREDIT_REVIEW,
        '1503' => CREDIT_REVIEW,
        '1504' => CREDIT_REVIEW,
        '2001' => CREDIT_REVIEW,
        '3001' => CREDIT_REVIEW,
        '3003' => CREDIT_REVIEW,
        '3501' => CREDIT_REVIEW,
        '4501' => CREDIT_REVIEW,
        '9001' => CREDIT_REVIEW,
        '5503' => CREDIT_REVIEW,
        '5504' => CREDIT_REVIEW,
        '0002' => CREDIT_DECLINED,
        '0004' => CREDIT_DECLINED,
        '0501' => CREDIT_DECLINED,
        '0502' => CREDIT_DECLINED,
        '0503' => CREDIT_DECLINED,
        '4001' => CREDIT_DECLINED,
        '6000' => CREDIT_DECLINED
    };

    /**
     * extract hawk alert codes from credit report segments
     * @param  crSegmentsList
     * @return List of hawk codes
     */
    public static List<String> extracHawkAlertCodes(List<ProxyApiCreditReportSegmentEntity> crSegmentsList) {
        List<String> hawkCodes = new List<String>();

        for (ProxyApiCreditReportSegmentEntity creditReportSegmentEntity : crSegmentsList) {
            if (creditReportSegmentEntity.prefix == HAWK_ALERT_SEGMENT_ID) {
                hawkCodes.add(creditReportSegmentEntity.value.trim());
            }
        }

        return hawkCodes;
    }

    /**
     * get hawk alerts list from crSegmentsList
     * @param  crSegmentsList
     * @return List of CreditReportHawkAlertsResult
     */
    public static List<CreditReportHawkAlertsResult> getHawkAlertsFromCR(List<ProxyApiCreditReportSegmentEntity> crSegmentsList) {
        List<String> hawkAlertsCodes = extracHawkAlertCodes(crSegmentsList);

        return getHawkAlerts(hawkAlertsCodes);
    }

    /**
    * get hawk alerts list from hawkAlertsCodes
    * @param  hawkAlertsCodes
    * @return List of CreditReportHawkAlertsResult
    */
    public static List<CreditReportHawkAlertsResult> getHawkAlerts(List<String> hawkAlertsCodes) {
        Map<String, CreditReportHawkAlertsResult> creditReportHawkAlertsResultMapper = new Map<String, CreditReportHawkAlertsResult>();

        for (String alertRange : alertRanges) {
            creditReportHawkAlertsResultMapper.put(alertRange , new CreditReportHawkAlertsResult(alertRange, true, new List<String>()));
        }

        for (String code : hawkAlertsCodes) {
            if (hawkAlertsMapper.containsKey(code)) {
                map<String, String> hawkAlertMapper = hawkAlertsMapper.get(code);

                CreditReportHawkAlertsResult result = creditReportHawkAlertsResultMapper.get(hawkAlertMapper.get(ALERT_RANGE));
                result.isApproved = false;
                result.alertReasons.add(hawkAlertMapper.get(DESCRIPTION));
            }
        }
        return creditReportHawkAlertsResultMapper.values();
    }

    /**
     * get Set of status by match codes
     * @param  hawkCodes
     * @return Set with the set of status
     */
    public static Set<String> getStatusByCodes(List<String> hawkCodes) {
        Set<String> statusList = new Set<String>();

        for (String code : hawkCodes) {
            if (statusMapper.containsKey(code)) {
                statusList.add(statusMapper.get(code));
            }
        }
        return statusList;
    }

    /**
     * evaluate hawk alerts response
     * @param appId
     * @param crSegmentsList
     */
    public static void evaluateHawkAlerts(String appId, List<ProxyApiCreditReportSegmentEntity> crSegmentsList) {
        List<String> hawkCodes = extracHawkAlertCodes(crSegmentsList);

        for (Opportunity application :
                [SELECT Id,
                 Status__c,
                 Reason_of_Opportunity_Status__c,
                 Review_Reason__c,
                 Hawk_Alert_Codes__c,
                 Reviewed_and_Cleared__c,
                 ApplicationId__c,
                 Lending_Product__c, Lending_Product__r.Name,Contact__r.Product__c, //3194 Changes
                 ProductName__c, LeadSource__c
                 FROM Opportunity
                 WHERE (Id = : appId or ApplicationId__c = : appId) LIMIT 1]) {
            if (hawkCodes != null && !hawkCodes.isEmpty()) {
                application.Hawk_Alert_Codes__c = String.join(hawkCodes, ',');

                Set<String> statusByCodes = getStatusByCodes(hawkCodes);

                if (application.Status__c != 'Declined (or Unqualified)') {
                    if (statusByCodes.contains(CREDIT_REVIEW)) {
                        application.Status__c = CREDIT_REVIEW;
                        application.Review_Reason__c = 'HAWK Alerts';
                        application.Reason_of_Opportunity_Status__c = '';
                        application.Reviewed_and_Cleared__c = false;
                    }
                    if (statusByCodes.contains(CREDIT_DECLINED)) {
                        application.Status__c = CREDIT_DECLINED;
                        application.Reason_of_Opportunity_Status__c = 'Cannot Confirm Information Provided';
                        application.Review_Reason__c = '';
                        application.Reviewed_and_Cleared__c = false;
                        
                        //3194 - Call EzVerify API To Send Declined Status
                        String product = string.isNotEmpty(application.ProductName__c) ? application.ProductName__c : (application.Contact__c != null ? application.Contact__r.Product__c : ''); //4673
                        //Check if Application is ezverify App
                        if(((generalPurposeWebServices.isPointOfNeed(product) && 
                            generalPurposeWebServices.isEzVerify(application.LeadSource__c)) ||
                            generalPurposeWebServices.isSelfPayProduct(product)) && 
                            (System.isFuture() || System.isBatch()))
                            generalPurposeWebServices.ezVerifyNotificationInSync(new List<ID>{application.id}, 'NOTQ', 'Application is Declined (or Unqualified)');
                        //3194 - Call EzVerify API To Send Declined Status
                    }
                }
            } else {
                application.Hawk_Alert_Codes__c = '-';
            }
            //3194 - Logging to track failure in Status Updated
            try {
                system.debug('-----evaluateHawkAlerts----'+application.Status__c);
                update application;    
            }
            catch(Exception e) {
                Logging__c log= new Logging__c(Webservice__c='CreditReportHawkAlerts.evaluateHawkAlerts error', API_Request__c=string.valueOf(application), API_Response__c = e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString(), Opportunity__c = application.id);
                insert log;   
            }
            //3194 - Logging to track failure in Status Updated
        }
    }
    
    /* BRMS Change */
     /**
     * evaluate hawk alerts response
     * @param appId
     * @param crSegmentsList
     */
    
    public static void evaluateHawkAlertsThrBRMS(String appId, List<ProxyApiCreditReportRuleEntity> crRuleList, List<ProxyApiCreditReportSegmentEntity> crSegmentsList) {
        List<ProxyApiCreditReportRuleEntity> hawkRulesList = new List<ProxyApiCreditReportRuleEntity>();
        Boolean fetchHawkCodes= false;
        List<String> hawkCodes = new List<String>();
        String hawkCodesFrmBRMS = '';
        
        for(ProxyApiCreditReportRuleEntity rule: crRuleList) {
            system.debug('--rule---'+rule+'---'+ValidationTabCtrl.getRuleNumber('Hawk Alerts Credit Declined'));
            if(rule.deRuleNumber.equals(ValidationTabCtrl.getRuleNumber('Hawk Alerts Manual Review')) || 
                    rule.deRuleNumber.equals(ValidationTabCtrl.getRuleNumber('Hawk Alerts Credit Declined')) ||
                        rule.deRuleNumber.equals(ValidationTabCtrl.getRuleNumber('Synthetic Fraud ID')) ){
                hawkRulesList.add(rule);
                if(rule.deRuleStatus.equalsIgnoreCase('Fail')) {
                    fetchHawkCodes = true;
                    if(!String.isEmpty(rule.deRuleValue))
                        hawkCodesFrmBRMS = rule.deRuleValue;
                }
            }
            system.debug('Testing hawkRulesList==== ' +hawkRulesList);
            system.debug('---hawkCodesFrmBRMS---'+hawkCodesFrmBRMS);
        }
        
        for (Opportunity application :
                [SELECT Id,
                 Status__c,
                 ApplicationId__c,
                 Reason_of_Opportunity_Status__c,
                 Review_Reason__c,
                 Hawk_Alert_Codes__c,
                 Reviewed_and_Cleared__c
                 FROM Opportunity
                 WHERE (Id = : appId or ApplicationId__c = : appId) LIMIT 1]) {
                 
            if(fetchHawkCodes) {    
                //hawkCodes = extracHawkAlertCodes(crSegmentsList);
                //system.debug('---hawkCodes ---'+hawkCodes);
                //if (hawkCodes != null && !hawkCodes.isEmpty())
                    //application.Hawk_Alert_Codes__c = String.join(hawkCodes, ',');
                if(hawkCodesFrmBRMS != null)
                    application.Hawk_Alert_Codes__c = hawkCodesFrmBRMS;
            }
            else 
                application.Hawk_Alert_Codes__c = '-';    
                 
                for(ProxyApiCreditReportRuleEntity rule: hawkRulesList) {
                   if (application.Status__c != 'Declined (or Unqualified)') {
                        if(rule.deRuleNumber.equals(ValidationTabCtrl.getRuleNumber('Hawk Alerts Manual Review')) && rule.deRuleStatus.equalsIgnoreCase('Fail') ) {
                      //  if (statusByCodes.contains(CREDIT_REVIEW)) 
                            //application.Status__c = CREDIT_REVIEW;
                            //application.Review_Reason__c = rule.deBRMS_ReasonCode; //'HAWK Alerts';
                            //application.Reason_of_Opportunity_Status__c = '';
                            application.Reviewed_and_Cleared__c = false;
                        }
                        if(rule.deRuleNumber.equals(ValidationTabCtrl.getRuleNumber('Hawk Alerts Credit Declined')) && rule.deRuleStatus.equalsIgnoreCase('Fail') ) {
                        // if (statusByCodes.contains(CREDIT_DECLINED)) 
                            //application.Status__c = CREDIT_DECLINED;
                            //application.Reason_of_Opportunity_Status__c = rule.deBRMS_ReasonCode; //'Cannot Confirm Information Provided';
                            //application.Review_Reason__c = '';
                            application.Reviewed_and_Cleared__c = false;
                        }
                  }
              } 
            
            update application;
        }
        /* BRMS Change */    
    }
}