public with sharing class DecisionLogicGDSRequestParameter {
    
    public String applicationId {get;set;}
    
    public String contactId {get;set;}
    
    public String channelId {get;set;}
    
    public String sequenceId {get;set;}
    
    public String productId {get;set;}
    
    public String sessionId {get;set;}
    
    public String executionType {get;set;}
    
    public String apiSessionId{get;set;}
    
    public String organizationId {get;set;}
    
    public String organizationURL {get;set;}
    
    public boolean isClarityValidate {get;set;}
    
    public String creditreportData {get;set;}
    
    public String creditreportType {get;set;}
    
    public DecisionLogicGDSRequestParameter(){}
    
    public DecisionLogicGDSRequestParameter(GDSRequestParameter gdsRequestParameter){
        
        this.applicationId = gdsRequestParameter.applicationId;
        this.contactId = gdsRequestParameter.contactId;
        this.sequenceId = gdsRequestParameter.sequenceId;
        this.apiSessionId = gdsRequestParameter.apiSessionId;
        this.organizationId = gdsRequestParameter.organizationId;
        this.organizationURL = gdsRequestParameter.organizationURL;
        this.sessionId = gdsRequestParameter.sessionId;
        this.creditreportData = [SELECT GDS_Response__c FROM Decision_Logic_Rules__c WHERE Name = 'CR-Instance' LIMIT 1].GDS_Response__c;
        this.creditreportType = 'TransUnion';
    }
    
    public static String get_JSONDecisionLogicGDSRequest(DecisionLogicGDSRequestParameter decisionLogicGDSRequestParameter){
        return System.JSON.serialize(decisionLogicGDSRequestParameter, true);
    }
    
}