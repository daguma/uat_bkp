@isTest public class DealAutomaticExceptionsCtrlTest {

    @isTest static void gets_delay_exception() {

        Deal_Automatic_Time_Delay_Exception__c delayExceptionData = new Deal_Automatic_Time_Delay_Exception__c();
        delayExceptionData.Lead_Source__c = 'Online Aggregator';
        delayExceptionData.Sub_Source__c = 'LendingTree';
        delayExceptionData.Point_Code__c = 'Test';
        delayExceptionData.Delay__c = 1;
        delayExceptionData.Name = 'Test';
        insert delayExceptionData;

        String result = DealAutomaticExceptionsCtrl.getDelayExceptions();

        System.assertNotEquals(null, result);

        DealAutomaticExceptionsCtrl daec = new DealAutomaticExceptionsCtrl();
    }

    @isTest static void saves_delay_exception() {

        Deal_Automatic_Time_Delay_Exception__c delayExceptionData = new Deal_Automatic_Time_Delay_Exception__c();
        delayExceptionData.Lead_Source__c = 'Online Aggregator';
        delayExceptionData.Sub_Source__c = 'LendingTree';
        delayExceptionData.Point_Code__c = 'Test';
        delayExceptionData.Delay__c = 1;
        delayExceptionData.Available_States__c = '';
        delayExceptionData.Selected_States__c = '';
        delayExceptionData.Name = 'Test';
        insert delayExceptionData;

        Deal_Automatic_Time_Delay_Exception__c delayData = new Deal_Automatic_Time_Delay_Exception__c();

        delayData.Lead_Source__c = 'Online Aggregator';
        delayData.Sub_Source__c = 'LendingTree';
        delayData.Point_Code__c = 'Test2';
        delayData.Delay__c = 2;
        delayData.Available_States__c = '';
        delayData.Selected_States__c = '';
        delayData.Name = 'Test';

        System.assertEquals(true, DealAutomaticExceptionsCtrl.saveDelayExceptionData(delayData));

        System.assertEquals(false, DealAutomaticExceptionsCtrl.saveDelayExceptionData(null));
    }

}