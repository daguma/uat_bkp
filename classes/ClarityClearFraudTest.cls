@isTest 
public class ClarityClearFraudTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearFraud ccf = new ClarityClearFraud();
        Test.stopTest();
        System.assertEquals(null, ccf.action);
        System.assertEquals(null, ccf.deny_codes);
        System.assertEquals(null, ccf.deny_descriptions);
        System.assertEquals(null, ccf.exception_descriptions);
        System.assertEquals(null, ccf.clear_fraud_score);
        System.assertEquals(null, ccf.non_scorable_reason_code);
        System.assertEquals(null, ccf.non_scorable_reason_description);
        System.assertEquals(null, ccf.clear_fraud_reason_codes);
        System.assertEquals(null, ccf.clear_fraud_reason_code_description);
        System.assertEquals(null, ccf.crosstab_points_total);
        System.assertEquals(null, ccf.crosstab_multiple);
        System.assertEquals(null, ccf.bankruptcy_codes);
        System.assertEquals(null, ccf.bankruptcy_description);
        System.assertEquals(null, ccf.full_name);
        System.assertEquals(null, ccf.product_date);
        System.assertEquals(null, ccf.filter_codes);
        System.assertEquals(null, ccf.filter_descriptions);
        System.assertEquals(null, ccf.clear_fraud_identity_verification);
        System.assertEquals(null, ccf.clear_fraud_alternate_identity);
        System.assertEquals(null, ccf.clear_fraud_inquiry);
        System.assertEquals(null, ccf.clear_fraud_points_total);
        System.assertEquals(null, ccf.clear_fraud_multiple);
        System.assertEquals(null, ccf.clear_fraud_stabilities);
        System.assertEquals(null, ccf.clear_fraud_crosstabs);
        System.assertEquals(null, ccf.clear_fraud_ratios);
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearFraud ccf = new ClarityClearFraud();
        ClarityClearFraudIdentityVerification ccfiv = new ClarityClearFraudIdentityVerification();
        ClarityClearFraudAlternateIdentity ccfai = new ClarityClearFraudAlternateIdentity ();
        ClarityClearFraudCommonData ccfcd = new ClarityClearFraudCommonData();
        ClarityClearFraudStabilities ccfs = new ClarityClearFraudStabilities ();
        ClarityClearFraudStability ccfs1 = new ClarityClearFraudStability();
        ClarityClearFraudStability ccfs2 = new ClarityClearFraudStability();
        ClarityClearFraudCrosstabs ccfc = new ClarityClearFraudCrosstabs();
        ClarityClearFraudCrosstab ccfc1 = new ClarityClearFraudCrosstab();
        ClarityClearFraudCrosstab ccfc2 = new ClarityClearFraudCrosstab();
        ClarityClearFraudRatios ccfr = new ClarityClearFraudRatios();
        ClarityClearFraudRatio ccfr1 = new ClarityClearFraudRatio();
        Test.stopTest();
        ccf.action = '';
        ccf.deny_codes = '';
        ccf.deny_descriptions = '';
        ccf.exception_descriptions = '';
        ccf.clear_fraud_score = 1;
        ccf.non_scorable_reason_code = '';
        ccf.non_scorable_reason_description = '';
        ccf.clear_fraud_reason_codes = '';
        ccf.clear_fraud_reason_code_description = '';
        ccf.crosstab_points_total = 1.1;
        ccf.crosstab_multiple = 1.2;
        ccf.bankruptcy_codes = '';
        ccf.bankruptcy_description = '';
        ccf.full_name = '';
        ccf.product_date = '';
        ccf.filter_codes = '';
        ccf.filter_descriptions = '';
        
        ccfiv.overall_match_result = '';
        ccfiv.ssn_name_match = '';
        ccfiv.name_address_match = '';
        ccfiv.ssn_Dob_Match = '';
        ccfiv.overall_match_reason_code = '';
        
        ccf.clear_fraud_identity_verification = ccfiv;
        
        ccfai.found_with_bank_account = false;
        ccfai.fraud_risk = '';
        ccfai.identity_type_code = '';
        ccfai.identity_type_description = '';
        ccfai.credit_risk = '';
        ccfai.total_number_of_fraud_indicators = '';
        ccfai.input_ssn_issue_date_cannot_be_verified = '';
        ccfai.input_ssn_recorded_as_deceased = '';
        ccfai.input_ssn_invalid = '';
        ccfai.best_on_file_ssn_issue_date_cannot_be_verified = '';
        ccfai.best_on_file_ssn_recorded_as_deceased = '';
        ccfai.high_probability_ssn_belongs_to_another = '';
        ccfai.ssn_reported_more_frequently_for_another = '';
        ccfai.inquiry_age_younger_than_ssn_issue_date = '';
        ccfai.credit_established_before_age_18 = '';
        ccfai.credit_established_prior_to_ssn_issue_date = '';
        ccfai.more_than_3_inquiries_in_the_last_30_days  = '';
        ccfai.inquiry_on_file_current_address_conflict = '';
        ccfai.inquiry_address_first_reported_in_lt_90_days = '';
        ccfai.inquiry_current_address_not_on_file = '';
        ccfai.inquiry_address_high_risk = '';
        ccfai.inquiry_address_non_residential = '';
        ccfai.inquiry_address_cautious = '';
        ccfai.on_file_address_high_risk = '';
        ccfai.on_file_address_non_residential = '';
        ccfai.on_file_address_cautious = '';
        ccfai.current_address_reported_by_new_trade_only = '';
        ccfai.current_address_reported_by_trade_open_lt_90_days = '';
        ccfai.driver_license_inconsistent_with_on_file = '';
        ccfai.telephone_number_inconsistent_with_address = '';
        ccfai.telephone_number_inconsistent_with_state = '';
        ccfai.work_phone_previously_listed_as_cell_phone = '';
        ccfai.work_phone_previously_listed_as_home_phone = '';
        ccfai.max_number_of_ssns_with_any_bank_account = '';
        
        ccf.clear_fraud_alternate_identity = ccfai;
        
        ccfcd.one_minute_ago = 1.1;
        ccfcd.ten_minutes_ago = 1.1;
        ccfcd.one_hour_ago = 1.1;
        ccfcd.twentyfour_hours_ago = 1.1;
        ccfcd.seven_days_ago = 1.1;
        ccfcd.fifteen_days_ago = 1.1;
        ccfcd.thirty_days_ago = 1.1;
        ccfcd.ninety_days_ago = 1.1;
        ccfcd.threesixtyfive_days_ago = 1.1;
        
        ccf.clear_fraud_inquiry = ccfcd;
        ccf.clear_fraud_points_total = ccfcd;
        
        List<ClarityClearFraudStability> ccfsList = new List<ClarityClearFraudStability>(); 
        ccfs1.name = 'Test name';
        ccfs1.one_minute_ago = 1;
        ccfs1.ten_minutes_ago = 1;
        ccfs1.one_hour_ago = 1;
        ccfs1.twentyfour_hours_ago = 1;
        ccfs1.seven_days_ago = 1;
        ccfs1.fifteen_days_ago = 1;
        ccfs1.thirty_days_ago = 1;
        ccfs1.ninety_days_ago = 1;
        ccfs1.threesixtyfive_days_ago = 1;
        ccfsList.add(ccfs1);
        ccfs.clear_fraud_stability = ccfsList;
        
        ccf.clear_fraud_stabilities = ccfs; 
        
        List<ClarityClearFraudCrosstab> ccfcList = new List<ClarityClearFraudCrosstab>();
        ccfc1.name = 'Test name';
        ccfc1.ssn = 123;
        ccfc1.drivers_license = 123;
        ccfc1.bank_account = 123;
        ccfc1.home_address = 123;
        ccfc1.zip_code = 123;
        ccfc1.home_phone = 123;
        ccfc1.cell_phone = 123;
        ccfc1.email_address = 123;
        ccfcList.add(ccfc1);
        ccfc.clear_fraud_crosstab = ccfcList;
        ccf.clear_fraud_crosstabs = ccfc;
        
        List<ClarityClearFraudRatio> ccfrList = new List<ClarityClearFraudRatio>();
        ccfr1.name = 'Test name';
        ccfr1.one_minute_ago = 1.1;
        ccfr1.ten_minutes_ago = 1.1;
        ccfr1.one_hour_ago= 1.1;
        ccfr1.twentyfour_hours_ago = 1.1;
        ccfr1.seven_days_ago = 1.1;
        ccfr1.fifteen_days_ago = 1.1;
        ccfr1.thirty_days_ago = 1.1;
        ccfr1.ninety_days_ago = 1.1;
        ccfr1.threesixtyfive_days_ago = 1.1;
        ccfrList.add(ccfr1);
        ccfr.clear_fraud_ratio = ccfrList;
        
        ccf.clear_fraud_ratios = ccfr;
   }
}