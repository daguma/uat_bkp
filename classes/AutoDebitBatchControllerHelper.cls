public class AutoDebitBatchControllerHelper {

     public static final String AUTO_DEBIT_PROCESS = 'SELECT Id,Debit_Card_Account_Id__c,Name,loan__Loan_Amount__c,loan__Contact__c, ' +
        'loan__Contact__r.Name,loan__Contact__r.email, ' +
        'loan__Archived__c,Next_Payment_is_Additional_Payment__c, ' +
        'Is_Debit_Card_On__c,loan__Pmt_Amt_Cur__c,Payment_Frequency_Masked__c, ' +
        'Card_Next_Debit_Date__c FROM loan__Loan_Account__c ' +
        'WHERE loan__Charged_Off_Date__c = NULL ' +
        'AND loan__Loan_Status__c like \'Active%\' ' +
        'AND  PaymentMethod__c = \'Debit Card\' ' +
        'AND Is_Debit_Card_On__c = true ' +
        'AND Card_Next_Debit_Date__c = TODAY '+
        'AND loan__Pmt_Amt_Cur__c > 0';
        
    public static final String ONE_TIME_DEBIT_PROCESS_QUERY = 'SELECT Id, loan__Loan_Account__c, loan__Txn_Amt__c, loan__Txn_Date__c, '+
        'Debit_Card_Account__c, loan__Transaction_Type__c, Name, ' +
        'loan__Loan_Account__r.loan__Contact__c, loan__Loan_Account__r.loan__ACH_On__c, ' +
        'loan__Loan_Account__r.Name, CreatedById, '+
        'One_Time_Debit_Status__c, Proxy_Response__c ' +
        'FROM loan__Other_Transaction__c ' +
        'WHERE loan__Txn_Date__c = TODAY '+
        'AND loan__Transaction_Type__c = \'One Time Debit\' '+
        'AND One_Time_Debit_Status__c = \'Pending\' ';
    
    public static String fake_response='{"SC":200,"EC":"0","transactionID":"1111-22222eecZQWQkFoyQ","network":"Visa","networkRC":"00","status":"COMPLETED","approvalCode":"123456","AVS":{"codeAVS":"Y"}}';

    public static Map<String, Integer> executeAutoDebitProcess(List<loan__Loan_Account__c> scope) {
        Map<String, Integer> reportResults = new Map<String, Integer>();    
        String templateName = 'Autodebit Payment Failure';
        EmailTemplate template = [Select id, HTMLValue, subject, body from EmailTemplate where name = :templateName limit 1];
        OrgWideEmailAddress[] orgWideEmailAddress = [select Id from OrgWideEmailAddress where Address = 'noreply@lendingpoint.com'];
        
        Integer count = 0, errors = 0; 

        List<loan__Loan_Account__c> loanAccountList = new List<loan__Loan_Account__c>();
        List<Note> noteList = new List<Note>();
        List<Logging__c> logList = new List<Logging__c>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        
        for(loan__Loan_Account__c obLoan : scope)
        {
          try {
             
          //endpoint = 'https://testapi.lendingpoint.com/proxy-api-post-brms-autodebit/rest/v101/tabapay/autoDebitWithSavedDetails';
         
            string endpoint, accessToken ;
            Java_API_Settings__c javaDetails = Java_API_Settings__c.getInstance('AutoDebitEndPoint');
            endpoint = javaDetails.User_Create_Endpoint__c;
            
            decimal cardFeeAmt = 0.00;
            cardFeeAmt = (decimal.valueOf(Label.AutoDebitAmt) * obLoan.loan__Pmt_Amt_Cur__c) / 100;
            String body = 'contactId=' + obLoan.loan__Contact__c + '&contractId='+obLoan.Id+'&LAINumber='+obLoan.Name+ '&amount='+obLoan.loan__Pmt_Amt_Cur__c.setScale(2)+ '&isAch=False'+ '&isAdditionalPayment=False'+ '&cardFeeAmt='+cardFeeAmt.setScale(2) +'&accountId=' + obLoan.Debit_Card_Account_Id__c; //CPND-107
            Boolean validatedAPI = false;
            
            validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
            String response;
            if(validatedAPI || Test.isRunningTest()){
                response = !Test.isRunningTest() ? ProxyApiBRMSUtil.ApiGetResult(endpoint,body) : fake_response;
                System.debug('response==>'+response);
                Map<String,Object> resMap = (Map<String,Object>)JSON.deserializeUntyped(response);
                string strSC = string.valueOf(resMap.get('SC'));
                string strNetworkRC = string.valueOf(resMap.get('EC'));
                string strEM = string.valueOf(resMap.get('EM')); //OF-392
                System.debug('BINGO->'+response);
                
                loan__Loan_Account__c lla = new loan__Loan_Account__c();
                lla.id = obLoan.Id;
                
                System.debug('strSc=>'+strSC);
                //if((string.isnotempty(msgStatus) && (msgStatus != '200' || (string.isnotempty(tabapayMessage))) || Test.isRunningTest())
                if((string.isnotempty(strSC) && ((strSC != '200' )||(string.isNotEmpty(strEM)))) || Test.isRunningTest()){ //OF-392
                    errors++;
                    system.debug('strNetworkRC ' + strNetworkRC);
                    string mailBody = String.isEmpty(template.htmlValue) ? '' : template.htmlValue;
                    mailBody = obLoan.Name != null ? mailBody.replace('[LAI]', obLoan.Name) : mailBody;
                    mailBody = obLoan.loan__Contact__r.Name != null ? mailBody.replace('[Customer Name]', String.valueOf(obLoan.loan__Contact__r.Name)) : mailBody;
                    mailBody = obLoan.loan__Pmt_Amt_Cur__c != null ? mailBody.replace('[payment amount]', String.valueOf(obLoan.loan__Pmt_Amt_Cur__c.setScale(2))) : mailBody;
                    mailBody = string.valueOf(resMap.get('EM')) != null ? mailBody.replace('[Reason]', string.valueOf(resMap.get('EM'))) : mailBody.replace('[Reason]', '');
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    List<String> toAddresses = new List<String>();
                    toAddresses.add(obLoan.loan__Contact__r.email);
                    toAddresses.add(Label.Customer_Service_Email);
                    if (orgWideEmailAddress.size() > 0 ) mail.setOrgWideEmailAddressId(orgWideEmailAddress[0].Id); 
                    mail.setToAddresses(toAddresses);
                    string errorMessage = string.valueOf(resMap.get('EM')); //OF-392
                    mail.setSubject(('Autodebit transaction fail ')); 
                    //mail.setHtmlBody('Your transaction for '+ obLoan.Name+' has been failed due to '+string.valueOf(resMap.get('EM')));
                    mail.setHtmlBody(mailBody);
                    mails.add(mail);
                    
                    
                    if(obLoan.Payment_Frequency_Masked__c == 'Monthly') {
                        lla.Card_Next_Debit_Date__c = System.today().addmonths(1);
                    }
                    else {
                        Integer numOfDays = OfferCtrlUtil.daysPerPeriod(obLoan.Payment_Frequency_Masked__c);
                        lla.Card_Next_Debit_Date__c = System.today()+numOfDays;
                    }
                    loanAccountList.add(lla);
                    string subject='Online Payment Has Been Failed for ' + obLoan.Name + ((string.isnotempty(strEM)) ? (' due to ' + strEM) : ''); //OF-392
                    string contractBody='On ' + string.valueOf(System.today()) + ', ' + obLoan.loan__Contact__r.Name + ' submitted a payment'; 
                    contractBody+=' for the Contract ' + obLoan.name + ' for the amount of $' + obLoan.loan__Pmt_Amt_Cur__c.setScale(2) ;
                    Note contractNote = new Note(parentId=obLoan.Id,body=contractBody,title=subject,isPrivate=false);
                    system.debug('===Note==='+contractNote);
                    noteList.add(contractNote);
                } else
                    count++;
                    
                logList.add(CustomerPortalWSDLUtil.getLogging('Tabapay - Auto Debit Batch',String.IsNotEmpty(obLoan.Id) ? obLoan.Id : null,body,JSON.SerializePretty(response))); //OF-392
                System.debug('Listtt'+logList);
                
            }
                        
          }
          catch (Exception e) {         
            System.debug('Error:' + e.getMessage() + 'LN:' + e.getLineNumber() );           
          }
        }
        if(logList.size() > 0){ insert logList;}
        if(mails.size() > 0){
            Messaging.sendEmail(mails);
        }
        if(loanAccountList.size() > 0){update loanAccountList;}
        if(noteList.size() > 0){insert noteList;}

        reportResults.put('count', count); 
        reportResults.put('errors', errors); 

        return reportResults;
    }

    public static void executeOneTimeDebitProcess(loan__Other_Transaction__c otherTransaction) {
        Boolean regularPaymentValidated = true, oneTimeAchValidated = true;
        String params = '', unprocessedPaymentReason ='';

        regularPaymentValidated = validateRegularPayment(otherTransaction);
        oneTimeAchValidated = validateOneTimeAch(otherTransaction);
        
        try{
            if(regularPaymentValidated && oneTimeAchValidated){
                params += 'contactId=' + otherTransaction.loan__Loan_Account__r.loan__Contact__c;
                params += '&contractId=' + otherTransaction.loan__Loan_Account__c;
                params += '&amount=' + String.valueof(otherTransaction.loan__Txn_Amt__c.setScale(2));
                params += '&isAch=' + String.valueof(otherTransaction.loan__Loan_Account__r.loan__ACH_On__c);
                params += '&isAdditionalPayment=False';
                params += '&cardFeeAmt=' + String.valueof(0);
                params += '&LAINumber=' + otherTransaction.loan__Loan_Account__r.Name; 
                params += '&accountId=' + otherTransaction.Debit_Card_Account__c;

                ProxyApiTabaPay.PaymentResponse response = ProxyApiTabaPay.makeAutoPayment(params);
                    
                if(response.SC == '200'){
                    if(response.transactionID != null && response.status == 'COMPLETED'){
                        otherTransaction.One_Time_Debit_Status__c = 'Processed';
                    }else{
                        otherTransaction.One_Time_Debit_Status__c = 'Failed';
                        unprocessedPaymentReason = response.message;
                    }
                    otherTransaction.Proxy_Response__c = 'Transaction '+ response.transactionID + ' ' + response.status + ' with the approval code '+ response.approvalCode +'.';
                }else{
                    otherTransaction.Proxy_Response__c = 'Failed: ' + response.message +  ' SC '+ response.SC + ' EC: ' + response.EC + ' EM: '+ response.EM;
                    otherTransaction.One_Time_Debit_Status__c = 'Failed';
                    unprocessedPaymentReason = response.message;
                }
            }else{
                if(!regularPaymentValidated && !oneTimeAchValidated)
                    unprocessedPaymentReason = 'There was a payment which was processed today, and also there is an OT ACH that was set up for today too.';
                else if(!regularPaymentValidated && oneTimeAchValidated)
                    unprocessedPaymentReason = 'There was a payment which was processed today.';
                else if(regularPaymentValidated && !oneTimeAchValidated)
                    unprocessedPaymentReason = 'There is an OT ACH that was set up for today.';
                
                otherTransaction.One_Time_Debit_Status__c = 'Canceled';
            }
            if(String.isNotEmpty(unprocessedPaymentReason)){
                otherTransaction.Unprocessed_Payment_Reason__c = unprocessedPaymentReason;
                sendOneTimeDebitAlert(otherTransaction, unprocessedPaymentReason);
            }
           
            update otherTransaction;
        }catch(Exception e){
            System.debug('Error during OneTimeDebitProcess ' + e.getMessage());
        }
        
    }

    public static Boolean validateRegularPayment(loan__Other_Transaction__c otherTransaction){
        Boolean processPayment = true;

        for(loan__Loan_Payment_Transaction__c payment: [SELECT loan__Loan_Account__c, loan__Transaction_Date__c, loan__Receipt_Date__c, Payment_Pending__c
                                                        FROM loan__Loan_Payment_Transaction__c
                                                        WHERE loan__Loan_Account__c =: otherTransaction.loan__Loan_Account__c
                                                        ORDER BY createdDate desc LIMIT 1]){
            
            if((payment.loan__Transaction_Date__c == otherTransaction.loan__Txn_Date__c || payment.loan__Receipt_Date__c == otherTransaction.loan__Txn_Date__c) && payment.Payment_Pending__c == 'Cleared'){
                 // There is a payment for the same day and it was successfully processed
                processPayment = false;
            }
        }
        return processPayment;
    }

    public static Boolean validateOneTimeAch(loan__Other_Transaction__c otherTransaction){
        String processPaymentMessage = '';
        Boolean processPayment = true;
        
        for(loan__Other_Transaction__c oneTimeACH: [SELECT Id, loan__OT_ACH_Debit_Date__c, createdDate 
                                                     FROM loan__Other_Transaction__c
                                                     WHERE loan__Loan_Account__c =: otherTransaction.loan__Loan_Account__c
                                                     AND loan__Transaction_Type__c = 'One Time ACH']){
            
            if(oneTimeACH.loan__OT_ACH_Debit_Date__c  == otherTransaction.loan__Txn_Date__c){
                processPayment = false; // There is an OT ACH for the same day
            }

            for(loan__Other_Transaction__c oneTimeAchCanceled: [SELECT Id  
                                                     FROM loan__Other_Transaction__c
                                                     WHERE loan__Loan_Account__c =: otherTransaction.loan__Loan_Account__c
                                                     AND loan__Transaction_Type__c = 'Cancel One Time ACH'
                                                     AND createdDate >: oneTimeACH.createdDate]){
                processPayment = true; //The OT ACH was canceled 
            }
        }
        return processPayment;
    }

    public static void sendOneTimeDebitAlert(loan__Other_Transaction__c otherTransaction, String unprocessedPaymentReason){
        String notificationMessage = 'The One Time Debit Payment '+otherTransaction.Name +
                                     ' that you scheduled for the contract ' +
                                      otherTransaction.loan__Loan_Account__r.Name + ' was not processed: ';

        SalesForceUtil.EmailByUserId('One Time Debit Transaction Not Processed', 
                                     notificationMessage + unprocessedPaymentReason, 
                                     otherTransaction.CreatedById);
                                     
    }
}