public class AccountLocationsWrapper {
        
    public List<Locations> locationList {get;set;}
    public boolean success {get;set;}
    public String message {get;set;}
    public string totalCount{get;set;}
    
    
    public class Locations{
        public String accountId;
        public String accountName;
        public String subSource;
        public String source;
        public String subProvider;
        public String phone;
        //MER-259
        public String street;
        public String city;
        public String state;
        public String zipCode; //MER-134
        
        
        /*public Integer compareTo(Object compareTo) {
            
        Locations compareToAcc = (Locations)compareTo;

         system.debug('========compareToAcc.accountId===='+compareToAcc.accountId);
        if (compareToAcc.accountId == MuleServices.loggedInAccountIdGlobal)
            return 1;
        else
            return -1;         
        }*/
    }
    
    public AccountLocationsWrapper() {
        locationList = new List<Locations>();
    }

    public Void addLocation(Account objAccount) {
        Locations objLocation = new Locations();
        objLocation.accountId = objAccount.Id;
        objLocation.accountName = objAccount.Cobranding_Name__c;
        objLocation.subSource = objAccount.Partner_Sub_Source__c;
        objLocation.source = objAccount.Source__c;
        objLocation.subProvider = objAccount.Lead_Provided_Data__c;
        objLocation.phone = objAccount.Phone;
        objLocation.zipCode = objAccount.Partner_Zip_Postal_Code__c; //MER-134
        //MER-259
        objLocation.street = objAccount.Partner_Street__c;        
        objLocation.city = objAccount.Partner_City__c;
        objLocation.state = objAccount.Partner_State_Province__c;
        locationList.add(objLocation);
    }
    
    //Sorting Locations to make the logged in location show first
  /*  public List<Locations> sortLocations(List<Locations> locationList, string loggedInAccountId) {
         MuleServices.loggedInAccountIdGlobal = loggedInAccountId;
         locationList.sort();
         return locationList;
    }*/
    
    

}