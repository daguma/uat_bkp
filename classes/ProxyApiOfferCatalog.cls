global class ProxyApiOfferCatalog {

    public Integer offerCatalogId {get; set;}
    public String state {get; set;}
    public Decimal annualIncome {get; set;}
    public String grade {get; set;}
    public Decimal loanAmount {get; set;}
    public Integer term {get; set;}
    public Decimal apr {get; set;}
    public Decimal feePercent {get; set;}
    public Decimal feeAmount {get; set;}
    public Double paymentAmount {get; set;}
    public Decimal maxPti {get; set;}
    public Double maxFundingAmount {get; set;}
    public Integer partner {get; set;}
    public Integer installments {get; set;}
    public Decimal ear {get; set;}
    public String message {get; set;} //SM-362
    public String criteria {get; set;} //API-166
    public String offerType{get; set;} //API-312
    public Decimal AONFeePct {get; set;}
    public Decimal AONPaymentAmount {get; set;}
    public String offerScope {get; set;}
}