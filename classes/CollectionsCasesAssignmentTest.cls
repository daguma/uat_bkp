@isTest
public class CollectionsCasesAssignmentTest {

    static testmethod void validate_assignAndCommitCases() {
        Test.startTest();
        
        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        myContract.Collection_Case_Owner__c = Label.HAL_Id;
        update myContract;
        Case myCase = new Case();
        myCase.CL_Contract__c = myContract.Id;
        myCase.status = 'Promise Kept';
        insert myCase;
        Id myUserId = [SELECT Id FROM User WHERE name = 'Jose Otero'].Id;

        CollectionsCasesAssignment__c cca = new CollectionsCasesAssignment__c(
            CL_Contract__c = myContract.Id, 
            Case__c = myCase.Id, 
            UpdateCase__c = true,
            caseOwnerNewId__c = myUserId
        );
        insert cca;

        List<CollectionsCasesAssignment__c> ccaList = new List<CollectionsCasesAssignment__c>();
        ccaList.add(cca);
        CollectionsCasesAssignment.assignAndCommitCases(ccaList,new CollectionsCasesAssignmentStructure(new CollectionsCasesAssignmentUsersStructure().teamsMap), new CollectionsCasesAssignmentAudit());

        Test.stopTest();
        
    }


    @IsTest
    static void validate_assignAndCommitCases2() {

        loan__Loan_Account__c myContract2 = LibraryTest.createContractTH();
        myContract2.Collection_Case_Owner__c = Label.HAL_Id;
        update myContract2;
        Case myCase2 = new Case();
        myCase2.CL_Contract__c = myContract2.Id;
        myCase2.status = 'Promise Kept';
        insert myCase2;
        Id myUserId2 = [SELECT Id FROM User WHERE name = 'Jose Otero'].Id;

        CollectionsCasesAssignment__c cca2 = new CollectionsCasesAssignment__c(
                CL_Contract__c = myContract2.Id,
                Case__c = myCase2.Id,
                CreateCase__c = true,
                caseOwnerNewId__c = myUserId2
        );
        insert cca2;

        List<CollectionsCasesAssignment__c> ccaList = new List<CollectionsCasesAssignment__c>();
        ccaList.add(cca2);
        CollectionsCasesAssignment.assignAndCommitCases(ccaList,new CollectionsCasesAssignmentStructure(new CollectionsCasesAssignmentUsersStructure().teamsMap), new CollectionsCasesAssignmentAudit());
    }

    @IsTest
    static void validate_assignAndCommitCases3(){
      
      loan__Loan_Account__c myContract3 = LibraryTest.createContractTH();
        myContract3.Collection_Case_Owner__c = Label.HAL_Id;
        update myContract3;
        Case myCase3 = new Case();
        myCase3.CL_Contract__c = myContract3.Id;
        myCase3.status = 'Promise Kept';
        insert myCase3;

        CollectionsCasesAssignment__c cca3 = new CollectionsCasesAssignment__c(CL_Contract__c = myContract3.Id, Case__c = myCase3.Id);
        insert cca3;

        List<CollectionsCasesAssignment__c> ccaList = new List<CollectionsCasesAssignment__c>();
        ccaList.add(cca3);
        CollectionsCasesAssignment.assignAndCommitCases(ccaList,new CollectionsCasesAssignmentStructure(new CollectionsCasesAssignmentUsersStructure().teamsMap), new CollectionsCasesAssignmentAudit());
    } 
}