global class QueueTimeOutsScrubbingBatch implements Database.Batchable<sObject> {
	global set<string> appIdList = new set<string>();
	global List<genesis__applications__c> appListToUpdate = new List<genesis__applications__c>();
	String query;

	global QueueTimeOutsScrubbingBatch() {}

	global Database.QueryLocator start(Database.BatchableContext BC) 
	{
        String timeOutsScrubbingOwnerId = Label.TimeOutsScrubbingOwner;
		query = 'SELECT Id,Application__c,Application__r.ownerId,Application__r.genesis__Status__c,AssignmentDate__c FROM TimeoutWarnings__c WHERE Application__c != null AND AssignmentDate__c != null AND Application__r.ownerId !=\''+ timeOutsScrubbingOwnerId +'\' AND Application__r.genesis__Status__c IN (\'Credit Qualified\',\'Documents Requested\',\'Offer Accepted\') AND BusinessDays__c > 10 ORDER BY CreatedDate ASC LIMIT 10';

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<TimeoutWarnings__c> scope) 
   	{
   		if(scope.size() > 0)
   		{	
   			for (TimeoutWarnings__c tw : scope) 
   			{	
	   			TimeoutWarnings__c timeOut = (TimeoutWarnings__c)tw;

	   			if(!appIdList.contains(timeOut.Application__c))
	   			{
					timeOut.Application__r.ownerId = Label.TimeOutsScrubbingOwner;
					appListToUpdate.add(timeOut.Application__r);
					appIdList.add(timeOut.Application__c); 
	   			}	   			                                           			
	   		}
	   		if (appListToUpdate.size() > 0) Database.update(appListToUpdate);     
   		}
	}
    
	global void finish(Database.BatchableContext BC) {}

	global void execute(SchedulableContext sc) 
	{
        QueueTimeOutsScrubbingBatch conInstance = new QueueTimeOutsScrubbingBatch();
        database.executebatch(conInstance, 100);
    }
}