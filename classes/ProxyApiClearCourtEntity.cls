global class ProxyApiClearCourtEntity {
	
	public Long id {get; set;}
    public String contactId {get; set;}
    public String parsedClearValidation {get; set;}
    public String errorMessage {get; set;}
    public String errorDetails {get; set;}
    public Long createdDate {get; set;}

    public ClearCourtResults clearCourtResults {get; set;}

    public Boolean errorResponse {
        get { return errorMessage != null; }
        set;
    }

    public DateTime creationDate {
        get {
            if (createdDate == null) 
            	return null;
            return DateTime.newInstance(createdDate);
        }
        set;
    }
}