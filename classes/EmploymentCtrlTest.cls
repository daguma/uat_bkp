@isTest public class EmploymentCtrlTest {
	static testMethod void test(){
		Contact con = new Contact(LastName='TestContact',Firstname = 'David',Work_Email_Address__c = 'test@gmail2.com',MailingState = 'GA');
        insert con;

        Opportunity opp = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id);
        insert opp;
        

    //    EmploymentCtrl.getEmploye(opp);
		
        EmploymentCtrl.fetchLookUpValues('TestContact','Contact');
        
        EmploymentCtrl.getEmploymentEntity(opp.Id);
        
        EmploymentCtrl.verifyWorkEmail(con.Id,opp.Id);
        
    //    EmploymentCtrl.saveApp(EmploymentCtrl.getEmploye(opp));
        EmploymentCtrl.EmploymentEntity ee = new EmploymentCtrl.EmploymentEntity();
        ee.setErrorMessage('testError');
        ee.setWarnings('testWarning');
        ee.setReadOnly('Read');
	}
    
    
    static testMethod void testValidate(){
		Contact con = new Contact(LastName='TestContact',Firstname = 'David',Work_Email_Address__c = 'test@gmail2.com',MailingState = 'GA');
        insert con;

        Opportunity opp = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id);
        insert opp;
        

    //    EmploymentCtrl.getEmploye(opp);
		
        EmploymentCtrl.fetchLookUpValues('TestContact','Contact');
        
        EmploymentCtrl.EmploymentEntity ee = EmploymentCtrl.getEmploymentEntity(opp.Id);
        
        EmploymentCtrl.verifyWorkEmail(con.Id,opp.Id);
        
        EmploymentCtrl.saveApp(ee.empDetail);
        ee.setErrorMessage('testError');
        ee.setWarnings('testWarning');
        ee.setReadOnly('Read');
  		ee = new EmploymentCtrl.EmploymentEntity();
      
    }
}