global class ProxyApiCreditReportAttributeEntity {
    @AuraEnabled public Long id {get; set;}
    @AuraEnabled public Long creditReportId {get; set;}
    @AuraEnabled public ProxyApiAttributeTypeEntity attributeType {get; set;}
    @AuraEnabled public String value {get; set;}
    @AuraEnabled public Boolean isApproved {get; set;}
    @AuraEnabled public String approvedBy {get; set;}

    @AuraEnabled public String approvedByDisplayText {
        get
        {
            String result = '';

            if (!approvedBy.equals('system'))
                return result = approvedBy;

            return result;
        }
    }
}