@isTest
private class ffaBatchManagerTest {

    @testSetup static void setups_data() {

        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();

        c2g__codaCompany__c testCompany = null;
        Group newGroup = null;

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

        System.runAs(thisUser) {

            newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            // Create Company Queue
            c2g.CODACompanyWebService.createQueue(testCompany.Id, 'USD', testCompany.Name);
            // Activate the Company
            c2g.CODAYearWebService.calculatePeriods(null); // Workaround to bug in company API's, safe to remain once fixed
            c2g.CODACompanyWebService.activateCompany(testCompany.Id, 'USD', testCompany.Name);
            // Assign the User to the Company
            c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
            userCompany.c2g__Company__c = testCompany.Id;
            userCompany.c2g__User__c = UserInfo.getUserId();
            insert userCompany;

            GroupMember newGroupMember = new GroupMember();
            newGroupMember.GroupId = newGroup.Id;
            newGroupMember.UserOrGroupId = thisUser.Id;
            insert newGroupMember;

            // Create Year and Periods
            c2g__codaYear__c yr = new c2g__codaYear__c();
            yr.Name = String.valueOf(Date.today().year());
            yr.OwnerId = newGroup.Id;
            yr.c2g__NumberOfPeriods__c = 12;
            yr.c2g__OwnerCompany__c = testCompany.id;
            yr.c2g__AutomaticPeriodList__c = true;
            yr.c2g__StartDate__c = Date.valueOf(Date.today().year() + '-01-01 00:00:00');
            yr.c2g__EndDate__c = Date.valueOf(Date.today().year() + '-12-31 00:00:00');
            yr.c2g__PeriodCalculationBasis__c = 'Month End';
            insert yr;
            c2g.CODAYearWebService.calculatePeriods(yr.Id);
            // Create Accounting Currency?
            if (UserInfo.isMultiCurrencyOrganization()) {
                c2g__codaAccountingCurrency__c testCurrency = new c2g__codaAccountingCurrency__c();
                testCurrency.Name = 'USD';
                testCurrency.c2g__DecimalPlaces__c = 2;
                testCurrency.c2g__Home__c = true;
                testCurrency.c2g__Dual__c = true;
                insert testCurrency;
            }

            c2g__codaAccountingCurrency__c testcurr = [SELECT Id FROM c2g__codaAccountingCurrency__c WHERE c2g__ownerCompany__c = : testCompany.Id LIMIT 1];
            Account testAccount = ffaTestUtilities.createAccount('Test Account');

            c2g__codaJournal__c testJournal = ffaTestUtilities.createJNL(Date.today(), testGLA.id, 100, null, null, null, null, testCompany.id, newGroup.id);

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();

            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = testCompany.Id;
            update testLoanAccount;

            loan__Loan_Payment_Transaction__c pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = testLoanAccount.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today();
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Payment_Mode__c = paymentMode.Id;
            insert pmt;
        }
    }

    @isTest static void more_than_one_company() {

        User thisUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];

        System.runAs(thisUser) {

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint SPE LLC' ORDER BY Id LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint SPE LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
            userCompany.c2g__Company__c = testCompany.Id;
            userCompany.c2g__User__c = UserInfo.getUserId();
            insert userCompany;

            GroupMember newGroupMember = new GroupMember();
            newGroupMember.GroupId = newGroup.Id;
            newGroupMember.UserOrGroupId = UserInfo.getUserId();
            insert newGroupMember;
        }

        ffaBatchManager batchManager = new ffaBatchManager();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == '2 or more FFA Companies are selected for the current user. Please ensure that only one FFA Company record is selected, then try again.', 'Assigned to 2 companies');
    }

    @isTest static void no_company_assigned() {

        User thisUser = [SELECT Id FROM User WHERE Id <> :UserInfo.getUserId() AND IsActive = true LIMIT 1];

        System.runAs(thisUser) {

            ffaBatchManager batchManager = new ffaBatchManager();

            List<Apexpages.Message> msgs = ApexPages.getMessages();
            Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
            System.assert((msgs.get(index)).getDetail() == 'No FFA Company is selected for the current user. Please select an FFA Company and try again!', 'No Company Assigned');
        }
    }

    @isTest static void init() {

        ffaBatchManager batchManager = new ffaBatchManager();

        System.assert(batchManager.clObjectList.size() > 0);
    }

    @isTest static void fetches_data_initial_disbursement() {

        loan__loan_Account__c testLoanAccount = [SELECT Id FROM loan__Loan_Account__c ORDER BY CreatedDate DESC LIMIT 1];
        loan__Payment_Mode__c paymentMode = [SELECT Id FROM loan__Payment_Mode__c ORDER BY CreatedDate DESC LIMIT 1];

        ffaTestUtilities.createLoanDisbursal(testLoanAccount.id, paymentMode.id);

        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Initial Disbursement';

        batchManager.fetchData();

        batchManager.wrapperList.get(0).selected = true;

        batchManager.startBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('Records submitted'), 'Initial Disbursement');
    }

    @isTest static void fetches_data_reversal() {

        loan__loan_Account__c testLoanAccount = [SELECT Id FROM loan__Loan_Account__c ORDER BY CreatedDate DESC LIMIT 1];
        loan__Payment_Mode__c paymentMode = [SELECT Id FROM loan__Payment_Mode__c ORDER BY CreatedDate DESC LIMIT 1];

        ffaTestUtilities.createLoanDisbursalReversal(ffaTestUtilities.createLoanDisbursal(testLoanAccount.id, paymentMode.id).id);

        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Initial Disbursement Reversal';

        batchManager.fetchData();

        batchManager.wrapperList.get(0).selected = true;

        batchManager.startBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('Records submitted'), 'Initial Disbursement Reversal');
    }

    @isTest static void fetches_data_payment() {

        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Payment';

        batchManager.fetchData();

        batchManager.wrapperList.get(0).selected = true;

        batchManager.startBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('Records submitted'), 'Payment');
    }

    @isTest static void fetches_data_reversed_payment() {

        loan__loan_Account__c testLoanAccount = [SELECT Id FROM loan__Loan_Account__c ORDER BY CreatedDate DESC LIMIT 1];
        loan__Loan_Payment_Transaction__c pmt = [SELECT Id FROM loan__Loan_Payment_Transaction__c ORDER BY CreatedDate DESC LIMIT 1];

        testLoanAccount.loan__loan_status__c = 'Closed- Written Off';
        testLoanAccount.loan__Principal_Remaining__c = 200;
        update testLoanAccount;

        pmt.loan__Write_Off_Recovery_Payment__c = true;
        pmt.loan__Cleared__c = true;
        update pmt;

        loan__Repayment_Transaction_Adjustment__c rpmt = new loan__Repayment_Transaction_Adjustment__c();

        rpmt.loan__Loan_Payment_Transaction__c = pmt.Id;
        rpmt.FFA_Integration_Complete__c = false;
        rpmt.loan__Adjustment_Txn_Date__c = Date.today();
        insert rpmt;

        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Payment Reversal';

        batchManager.fetchData();

        batchManager.wrapperList.get(0).selected = true;

        batchManager.startBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('Records submitted'), 'Payment Reversal');
    }

    @isTest static void fetches_data_recovers_payment() {

        loan__loan_Account__c testLoanAccount = [SELECT Id FROM loan__Loan_Account__c ORDER BY CreatedDate DESC LIMIT 1];
        loan__Loan_Payment_Transaction__c pmt = [SELECT Id FROM loan__Loan_Payment_Transaction__c ORDER BY CreatedDate DESC LIMIT 1];

        testLoanAccount.loan__loan_status__c = 'Closed- Written Off';
        testLoanAccount.loan__Principal_Remaining__c = 200;
        update testLoanAccount;

        pmt.loan__Write_Off_Recovery_Payment__c = true;
        update pmt;

        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Recovery Payment';

        batchManager.fetchData();

        batchManager.wrapperList.get(0).selected = true;

        batchManager.startBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('Records submitted'), 'Recovery Payment');
    }

    @isTest static void fetches_data_charges_off() {

        loan__loan_Account__c testLoanAccount = [SELECT Id FROM loan__Loan_Account__c ORDER BY CreatedDate DESC LIMIT 1];

        testLoanAccount.FFA_Integration_Charge_Off_Complete__c = false;
        testLoanAccount.loan__Loan_Status__c = 'Closed- Written Off';
        testLoanAccount.loan__Charged_Off_Date__c = Date.today().addDays(-1);
        update testLoanAccount;

        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Charge Off';

        batchManager.fetchData();

        batchManager.wrapperList.get(0).selected = true;

        batchManager.startBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('Records submitted'), 'Charge Off');
    }

    @isTest static void fetches_data_selling_to_spe() {

        loan__loan_Account__c testLoanAccount = [SELECT Id FROM loan__Loan_Account__c ORDER BY CreatedDate DESC LIMIT 1];

        testLoanAccount.FFA_Integration_Sale_Complete__c = false;
        testLoanAccount.Asset_Sale_Date__c = Date.today().addDays(-15);
        update testLoanAccount;

        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Selling to SPE';

        batchManager.fetchData();

        batchManager.wrapperList.get(0).selected = true;

        batchManager.startBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('Records submitted'), 'Selling to SPE');
    }

    @isTest static void fetches_data_other() {

        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Other';

        batchManager.startBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('No Records Selected'), 'No Records Selected');
    }

    @isTest static void starts_all_batches() {
        ffaBatchManager batchManager = new ffaBatchManager();

        batchManager.selectedObject = 'Payment';

        batchManager.startAllBatch();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().containsIgnoreCase('Records submitted'), 'All Batches');
    }

}