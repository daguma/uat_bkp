public with sharing class CLContractAlertSectionCtrl {
    public boolean isProjectTransition {get; set;}
    public boolean isZipCodeFIL {get; set;}
    public boolean isMultiloan {get; set;}
    public loan__loan_Account__c acc {get; set;}
    public String refiMessage {get; set;}

    public CLContractAlertSectionCtrl(Apexpages.StandardController controller){
        isProjectTransition = false;
        isZipCodeFIL = false;
        isMultiloan = false;
        string ZipCodeTemp = '';
        if (!Test.isRunningTest()) controller.addFields(new List<String>{'Name', 'loan__Contact__c'});
        acc = (loan__loan_Account__c)controller.getRecord();

        for(Flag_Accounts__c fA : [Select Id, Name FROM Flag_Accounts__c where Name =: acc.Name LIMIT 1]){
            isProjectTransition = true;

        }

        for(Contact con: [Select Id, MailingPostalCode FROM Contact where Id =: acc.loan__Contact__c]){
            ZipCodeTemp = con.MailingPostalCode;

        }
        for(FIL_Zip_Codes__c FZC: [Select Id, Name FROM FIL_Zip_Codes__c where Name =: ZipCodeTemp]){
            isZipCodeFIL = true;
        }
        for(Opportunity parms: [SELECT Id, Type FROM Opportunity where Type= 'Multiloan' And Status__c not in('Aged / Cancellation','Declined (or Unqualified)') AND Contact__c =: acc.loan__Contact__c ]){
            isMultiloan = true;
        }

        refiMessage = '';
        RefinanceAlertCtrl.RefinanceAlertEntity refinanceAlertEntity = RefinanceAlertCtrl.getRefiAppByContact(acc.loan__Contact__c);
        if(refinanceAlertEntity.hasError){
            refiMessage = 'There was an error trying to search for this customers Refinance applications.';
        }else{
            if(!String.isEmpty(refinanceAlertEntity.refiAppsCSV)){
                refiMessage = 'This customer is refinance qualified. They can now lower their payment or request additional money. Don\'t forget to thank them for being an awesome customer and let them know about their options.';
            }
        }
    }

}