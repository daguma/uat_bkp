public with sharing class FraudSummaryCls {
    
    @AuraEnabled public Opportunity application {get; set;}
    @AuraEnabled public CreditReportSegmentsUtil.FraudIndicators fraudIndicators {get; set;}
    @AuraEnabled public List<CreditReportHawkAlertsResult> creditReportHawkAlertsResult {get; set;}
    private Transient ProxyApiCreditReportEntity creditReportEntity = null;
    
    @AuraEnabled public Contact con {get; set;}
    @AuraEnabled public Boolean isReviewNeeded {get; set;}
    @AuraEnabled public String clarityStatus {get; set;}
    @AuraEnabled public String clarityReasonCodes {get; set;}
    @AuraEnabled public Email_Age_Results__c emailAgeResult {get; set;}
    @AuraEnabled public List<String> warnings {get; set;}
    @AuraEnabled public Boolean hasError {get; set;}
    @AuraEnabled public String errorMessage {get; set;}
    
    //BRMS changes
    @AuraEnabled public List<DisplayGDSResponseWrapper> highRiskRuleList {get;set;}
    @AuraEnabled public boolean displayOldView {get;set;}
    @AuraEnabled public boolean isSoftPull{get;set;}
    @AuraEnabled public Boolean isHardPullCreditReportPresent {get;set;}
    //BRMS changes
    
    public FraudSummaryCls(String recordId) {
        application = getApplication(recordId);
        warnings = new List<String>();
        hasError = false;
    }
    
    public void setErrorMessage(String message) {
        hasError = true;
        errorMessage = message;
    }
    
    @AuraEnabled
    public static FraudSummaryCls getFraudSummaryEntity(String recordId) {
        FraudSummaryCls fraudEntity = new FraudSummaryCls(recordId);
        
        try {
            fraudEntity.loadFraudShieldIndicators();
            ApplicationsUtil AppUtil = new ApplicationsUtil(fraudEntity.application);
            if (String.isNotBlank(AppUtil.ValidatePartnerForApplication())) fraudEntity.warnings.add(AppUtil.ValidatePartnerForApplication());

            //BRMS changes - start
            fraudEntity.highRiskRuleList = new List<DisplayGDSResponseWrapper>();
            DecisioningCtrl decisionCntrl = new DecisioningCtrl(recordId);
            
            //Add Decisioning Tab Warning
            if(decisionCntrl.warningsList.size() > 0)
				fraudEntity.warnings.addAll(decisionCntrl.warningsList);
            
            //Add Credit Report Error
            if(decisionCntrl.cr.errorMessage != null)
            	fraudEntity.warnings.add(decisionCntrl.cr.errorMessage);
            
            if(decisionCntrl.brmsSettings.Do_Not_Call__c == false) {
                //decisionCntrl.initialize(); //default score display count (This function is already executed in constructor of "DecisioningCtrl" at line 46)
                fraudEntity.isSoftPull = decisionCntrl.cr.isSoftPull;
                fraudEntity.isHardPullCreditReportPresent = decisionCntrl.isHardPullCreditReportPresent;
                fraudEntity.highRiskRuleList = decisionCntrl.highRiskRuleList;
                
                fraudEntity.displayOldView = decisionCntrl.displayOldView ;
            }
            //BRMS ends
            if (AppUtil.isDeclineByHardPull()) {
                fraudEntity.warnings.add('The application has been declined after the hard credit report was pulled. You cannot move forward with this application.');
            }
            
            if (fraudEntity.application.Contact__c != null) {
                
                for(Contact c : VerifyPhoneNumberHelper.getContactRec(fraudEntity.application.Contact__c)) {
                    fraudEntity.con = c;
                }
                
                //Email Age changes
                //check if there is any email age account associated to this contact record
                List<Email_Age_Details__c> emailAgeDetails = new List<Email_Age_Details__c>();
                emailAgeDetails = [select Id, Contact__c, createddate, (select id, EARiskBandID__c, EAStatusID__c, EAScore__c, EAReason__c from Email_Age_Results__r) from Email_Age_Details__c
                                   where Contact__c = :fraudEntity.application.Contact__c order by CreatedDate desc limit 1];
                
                //check if there exists any email age record for this contact
                if (emailAgeDetails.size() > 0 && emailAgeDetails[0].Email_Age_Results__r.size () > 0) {
                        fraudEntity.emailAgeResult = emailAgeDetails[0].Email_Age_Results__r[0];
                }
                System.debug('EMAIL AGE RESULT : ' + fraudEntity.emailAgeResult);
            }
            
            //Unknown Result Warning
            if (fraudEntity.application.FT_Status__c != null && fraudEntity.application.FT_Status__c == 'Unknown') {
                fraudEntity.warnings.add('The response of UNKNOWN should be checked by for accuracy as this may be a participant Bank with EWS. Rekeying of the account # and routing # may be necessary to generate the proper response.');
            }
            
            // Clarity API
            if (fraudEntity.application.Clarity_Submitted__c) {
                fraudEntity.clarityStatus = fraudEntity.application.Clarity_Status__c ? 'PASS' : 'FAIL';
                fraudEntity.clarityReasonCodes = fraudEntity.application.Clarity_Fraud_Reason_Code__c != null
                    ? fraudEntity.application.Clarity_Fraud_Reason_Code__c.replaceAll('[|]', '<BR/>')
                    : '-';
            }
            
            //Hawk Alerts
            if (fraudEntity.application.Hawk_Alert_Codes__c != null) {
                List<String> hawkCodes = fraudEntity.application.Hawk_Alert_Codes__c.deleteWhitespace().split('\\,');
                
                fraudEntity.creditReportHawkAlertsResult = CreditReportHawkAlerts.getHawkAlerts(hawkCodes);
                
                for (CreditReportHawkAlertsResult hawkAlertResult : fraudEntity.creditReportHawkAlertsResult) {
                    if (hawkAlertResult.alertReasons != null && !hawkAlertResult.alertReasons.isEmpty()) {
                        fraudEntity.isReviewNeeded = true;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            System.debug(e);
            fraudEntity.setErrorMessage(e.getMessage());
        }
        return fraudEntity;
    }
    
    private void loadFraudShieldIndicators() {
        if (this.application != null) {
            creditReportEntity = CreditReport.getHardPullByEntityId(this.application.Id);
            
            fraudIndicators = new CreditReportSegmentsUtil.FraudIndicators('');
            if (creditReportEntity != null && creditReportEntity.errorMessage == null) {
                If(CreditReport.validateBrmsFlag(this.application.id)){ //pre BRMS
                    fraudIndicators = CreditReportSegmentsUtil.extractFraudIndicators(creditReportEntity.segmentsList);
                } //BRMS change
                else {
                    //09-02 change
                    If(creditReportEntity.brms <> null && creditReportEntity.brms.rulesList <> null && !creditReportEntity.brms.rulesList.isEmpty() )
                        this.fraudIndicators = CreditReportSegmentsUtil.extractFraudIndicatorsThrBRMS(creditReportEntity.brms.rulesList);
                }
                //BRMS change
            }
        }
    }
    
    @AuraEnabled
    public static void updateOpportunity(Opportunity opp){
        try{
            update opp;
            system.debug('Updated Opportunity==== ' +opp);
        }catch(Exception e){
            system.debug('Exception on Opportunity update==== '+e.getMessage());
        }
    }
    
    public static Opportunity getApplication(String oppId) {
        Opportunity opportunity = [SELECT Bankruptcy_Found__c, Contact__c, Hawk_Alert_Codes__c, Reviewed_And_Cleared__c,
                                   Clarity_Submitted__c, Clarity_Status__c, Clarity_Fraud_Score__c, Clarity_Fraud_Crosstab_Multiple__c, 
                                   Clarity_Fraud_Reason_Code__c, Idology_Accepted__c, Item_Scanned__c, Items_passed__c,
                                   Did_the_DL_number_verify_with_the_state__c, Alerts__c, Email_Name_Inconsistent__c,
                                   ExpectID_Result__c, FT_Status__c, FT_Description__c, FT_Code__c, Contact__r.Name_Score__c,
                                   Contact__r.Qualifiers__c, Contact__r.Address_Score__c, Contact__r.Payfone_Response_Code__c, 
                                   Contact__r.Email_Score__c, Contact__r.Is_Number_Verified__c, Contact__r.MatchCRM_Description__c,
                                   Contact__r.getIntelligence_Description__c, Contact__r.Phone_Processed__c, Contact__r.Process_Date__c,
                                   Contact__r.Verify_Description__c, Contact__r.Trust_Score__c, Status__c, Reason_of_Opportunity_Status__c,
                                   Do_these_2_responses_match__c, Type, Contact__r.Do_Not_Contact__c 
                                   FROM Opportunity
                                   WHERE Id = :oppId
                                  ];
        // Perform isAccessible() check here 
        return opportunity;
    }
    
}