public class DebitCardPaymentController {
    
    public loan__loan_account__c theAccount{ get; set; }
    public String lId {get;set;}
    public string MonthlyPaymentDueon{get;set;}
    public Decimal AdditionalPayment{get;set;}
    public Decimal PaymenttowardsPastDueBalance{get;set;}
    public string selectedCardType{get;set;}
    public string nameOnCard{get;set;}
    public string debitCardNumber{get;set;}
    public string tempdebitCardNumber{get;set;}    
    public integer securityCode{get;set;}
    public string selectedExpirationMonth{get;set;}
    public string selectedExpirationYear{get;set;}
    public string MonthlyPaymentSelection{get;set;}
    public string AdditionalPaymentSelection{get;set;}
    public Decimal amount=0;
    public string expirationDate;
    public string paymentType;
    public static String END_POINT_URL_CreateSession = Label.PayLeap_API + Label.PayLeap_Create_Transaction_Session_Method +'?'; 
    public static String END_POINT_URL_SaleComplete = Label.PayLeap_API + Label.process_debitor_creditCard_Method +'?';
    public static String USER_NAME = Label.PayLeap_UserName;
    public static String TRANSACTION_KEY = Label.PayLeap_Password;
    public string Payment_Submission = 'Payment_Submission';
    public string Payment_Monthly= 'Payment_Monthly';
    public string Payment_Failed = 'Payment_Failed';
    EmailTemplate eTemplate;
    public string TID;
    //public String enablePayment {get;set;}
    public String paymentMsg {get;set;}
    public String validationOTACHPayment {get;set;}
    public String validationAccrualDate {get;set;}
    public String isEndOfDayProcessRunning{get;set;}
    /* 3918 default message */
    public String errorMessage,mailErrorMsg;
    
    public deserializeResponse pinPadParams {get{return pinPadParams;} set;}
    public Map<string,string> mapPayLeapMeaasages=new Map<string,string>();
    deserializeResponse deserializeResp = new deserializeResponse ();
    
    public DebitCardPaymentController(Apexpages.StandardController controller){           
        lId = '';
        theAccount = (loan__loan_account__c)controller.getRecord();
        if (theAccount != null && theAccount.Id <> null) 
            lId = theAccount.Id;
        else 
            lId = System.currentPageReference().getParameters().get('id');
        if (lId != null && lId.Length() > 0) {
            /*3670 fetched loan__Last_Accrual_Date__c in query */
            theAccount = [Select Id, Name, loan__Contact__r.Name, Customer_Name__c, loan__ACH_Next_Debit_Date__c,loan__Pmt_Amt_Cur__c,loan__Pay_Off_Amount_As_Of_Today__c,loan__Amount_to_Current__c,
                                loan__Delinquent_Amount__c,loan__Contact__c, loan__OT_ACH_Debit_Date__c, loan__OT_ACH_Payment_Amount__c,loan__Last_Accrual_Date__c from loan__loan_account__c where Id =:lId ];
            
            amount=(theAccount.loan__Pmt_Amt_Cur__c !=null?theAccount.loan__Pmt_Amt_Cur__c:0); 
            
            List<PayLeap_Messages__c> objPayLeapMessages=[select name,Message__c,description__c from PayLeap_Messages__c];
            for(PayLeap_Messages__c  obj :objPayLeapMessages )
                mapPayLeapMeaasages.put(obj.name,obj.description__c);
          checkValidations(lId); // 4219 changes
            
        }  
        /* 3918 default message */
        if(mapPayLeapMeaasages.containsKey('-1')) errorMessage=mapPayLeapMeaasages.get('-1'); 
        if(mapPayLeapMeaasages.containsKey('-2')) mailErrorMsg=mapPayLeapMeaasages.get('-2');
        
            
    }
    /********4219 starts changes to stop Debit Card payment on conditional basis************/
    private void checkValidations(string contractId){
        string TransResp= CustomerPortalWSDL.checkToStopDebitCardPayment(contractId);
        if(!string.isEmpty(TransResp)){
         Map<string, string> PaymentResponse = (Map<String, String>) JSON.deserialize(TransResp, Map<String, String>.class);    
         //if(PaymentResponse.ContainsKey('enablePayment')){enablePayment = PaymentResponse.get('enablePayment');}
         if(PaymentResponse.ContainsKey('msg')){paymentMsg= PaymentResponse.get('msg');}
         if(PaymentResponse.ContainsKey('validationOTACHPayment')){validationOTACHPayment = PaymentResponse.get('validationOTACHPayment');}
         if(PaymentResponse.ContainsKey('validationAccrualDate')){validationAccrualDate = PaymentResponse.get('validationAccrualDate');}
         if(PaymentResponse.ContainsKey('isEndOfDayProcessRunning')){isEndOfDayProcessRunning= PaymentResponse.get('isEndOfDayProcessRunning');}
         }
    }
    /******** 4219 ends ************/
    public List<SelectOption> getcardType() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('VISA','VISA'));
        options.add(new SelectOption('MasterCard ','MasterCard'));
        options.add(new SelectOption('Amex ','Amex'));
        options.add(new SelectOption('Discover','Discover'));
        
        return options;        
    } 
    
    public List<SelectOption> getlistMonth(){   
        List<SelectOption> options = new List<SelectOption>();
        for(integer index=1;index<=12;index++)
            options.add(new SelectOption(String.valueOf(index),String.valueOf(index))); 
        return options;
    }
    
    public List<SelectOption> getlistYear(){
        List<SelectOption> options = new List<SelectOption>();
        integer year=System.Today().year();
        for(integer index=0;index<=70;index++)        
            options.add(new SelectOption(String.valueOf(year + index),String.valueOf(year + index)));
        return options;
    }
    
    //Method to call from client side to make Payment
    public pageReference Proceed(){  
        try
        {   
            checkValidations(lId);
            if(isEndOfDayProcessRunning=='true'){
              if(pinPadParams==null){pinPadParams= new deserializeResponse();
              pinPadParams.Message=Label.PayLeapEndOfDayProcessValidation;
              return null;}
             }
             if(validationOTACHPayment=='true'){
              if(pinPadParams==null){pinPadParams= new deserializeResponse();
              pinPadParams.Message=paymentMsg;
              return null;}
             }
             if(validationAccrualDate=='true'){
              if(pinPadParams==null){pinPadParams= new deserializeResponse();
              pinPadParams.Message=Label.PayLeapLastAccrualDateValidation;
              return null;}
             }
             
            /************3907 Mask Debit Card Number*********/     
            tempdebitCardNumber = maskAccountAndDebitCardNo(debitCardNumber);
            string slectedPaymentTypeID = apexpages.currentpage().getparameters().get('selected');
            if(slectedPaymentTypeID=='AdditionalPaymentSelection'){
                amount=AdditionalPayment;
                paymentType='Additional Payment';
            }
            else if(slectedPaymentTypeID=='PastDueBalancePaymentSelection'){
                amount=PaymenttowardsPastDueBalance;
                paymentType='Past Due Balance';
            }
            else
            {
                paymentType='Monthly Payment';
                amount=(theAccount.loan__Pmt_Amt_Cur__c !=null?theAccount.loan__Pmt_Amt_Cur__c:0); 
            }
            
             
            expirationDate = selectedExpirationMonth + ''+ selectedExpirationYear.substring(2,4);   
            
            pinPadParams = new deserializeResponse();
            pinPadParams = makePaymentCall();
            /* 3918 default message */
            if(pinPadParams==null){pinPadParams= new deserializeResponse();
            pinPadParams.Message=errorMessage;}
            //#3533 - Debit Card Payment Removing OT ACH Date
            if (theAccount.loan__OT_ACH_Debit_Date__c != null && theAccount.loan__OT_ACH_Payment_Amount__c != null) 
            {
                insertOtAchNote(theAccount.Id, theAccount.loan__OT_ACH_Payment_Amount__c, theAccount.loan__OT_ACH_Debit_Date__c, amount);
                theAccount.loan__OT_ACH_Debit_Date__c = null;
                theAccount.loan__OT_ACH_Payment_Amount__c = null;
                update theAccount;
            }
            //#3533 - Debit Card Payment Removing OT ACH Date

            return null;
        }
        catch(exception e)
        {            
            //createMessage(ApexPages.Severity.ERROR,'Error: Invalid Input.');
            /***************3918 Default Message in case of Exception*******************/
           
            if(pinPadParams==null)pinPadParams= new deserializeResponse();
            pinPadParams.Message=errorMessage;
            
        }
        
        return null;
        
    }
    //Method to call from client side to make a call for SaleComplete
    /**3918 default message **/
    public pageReference makeSaleCompleteCall(){
        deserializeResp= makePaymentCall();
        system.debug('==deserializeResp=='+ deserializeResp);
        if(deserializeResp!=null) addNoteSaleComplete(deserializeResp.Result,deserializeResp.Message,END_POINT_URL_SaleComplete+'NameOnCard='+nameOnCard+'TokenNumber='+deserializeResp.TokenNumber,deserializeResp.PNRef);
        else {
           createMessage(ApexPages.severity.ERROR, errorMessage);
            sendMail(Payment_Failed,mailErrorMsg);  
         }
        return null;
    }
    //API request method to get Payment response
    public deserializeResponse makePaymentCall(){
        deserializeResponse deserializeResp;
        string TransType=apexpages.currentpage().getparameters().get('TransType');
        string PNRef=apexpages.currentpage().getparameters().get('PNRef');
        
        Map<string,string> responseMap = new Map<string,string>();
        string logMsg,resString;
        list<logging__c> logList = new list<logging__c>(); 
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        try{
        if(TransType=='SaleComplete'){
            resString='UserName='+USER_NAME+'&Password='+TRANSACTION_KEY + '&TransType=SaleComplete&CardNum=' + debitCardNumber + '&ExpDate=' + expirationDate + '&MagData=&NameOnCard='+nameOnCard +'&Amount=' + amount + '&PNRef='+ PNRef + '&ExtData=';   
            req.setEndpoint(END_POINT_URL_SaleComplete);
        }
        else{     
            resString = 'UserName='+USER_NAME+'&Password='+TRANSACTION_KEY;
            req.setEndpoint(END_POINT_URL_CreateSession);
        }
        
        //resp1.sessionId = sessionId;
        
        req.setBody(resString);
        req.setMethod('GET');
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        //req.setTimeout(1);
        HttpResponse res ;
        if (Test.isRunningTest()) {
            ExampleCalloutMock mock=new ExampleCalloutMock ();
            res= mock.respond(req);
        } else {
            res = h.send(req); 
        }
        
        integer statusCode = res.getStatusCode();
        
        if(res <> null && statusCode == 200){
            Dom.Document doc = res.getBodyDocument();
            //Retrieve the root element for this document.
            Dom.XMLNode address = doc.getRootElement();
            // Alternatively, loop through the child elements.This prints out all the elements of the address
            for(Dom.XMLNode child : address.getChildElements()) {
                responseMap.put(child.getName(), child.getText());                
            }
            
            
            if(!responseMap.isEmpty()){  
                JSONParser parser = JSON.createParser(JSON.serializepretty(responseMap));
                deserializeResp = (deserializeResponse) parser.readValueAs(deserializeResponse.class); 
            }
            system.debug('===========deserializeResp============'+deserializeResp); 
            ApexPages.Message myMsg;
            system.debug('==========Message======='+ deserializeResp.Message);
            
            if(mapPayLeapMeaasages.containsKey(deserializeResp.Result)) 
                deserializeResp.Message= mapPayLeapMeaasages.get(deserializeResp.Result); 
            else if(string.IsEmpty(deserializeResp.Message))
                deserializeResp.Message=!string.IsEmpty(deserializeResp.RespMSG) ? deserializeResp.RespMSG : errorMessage; /*3918*/
            
            
            if(amount !=0) deserializeResp.Amount=amount;
            if(ExpirationDate!=null) deserializeResp.ExpirationDate=ExpirationDate;  
            //3716 Resilt is now Comapring with string rather than Integer as done previosly              
            if(String.valueOf(deserializeResp.Result) != '0'){
                
                logMsg = 'Transaction API Result is = '+deserializeResp.Result+ ' with Error message = '+ deserializeResp.Message; 
                //sendMail(Payment_Failed,'Incorrect Card Information');
            }          
        }else{
            logMsg = 'Transaction API responded with Status code = '+statusCode+' and message = '+res;
        }  
                                                                                                                                             
        
        
        if(logMsg <> null){   
            String logString =(string.isNotEmpty(TransType) ? 'TransType='+ TransType +'&': '') +'NameOnCard='+nameOnCard+'&Amount='+amount +'&PNRef='+ (deserializeResp!=null && !string.IsEmpty(deserializeResp.PNRef) ? deserializeResp.PNRef : '');
            logList.add(CustomerPortalWSDLUtil.getLogging('Acculynk Transaction API Error', (string)theAccount.Id, req.toString()+logString, logMsg));                  
        } 
        
        system.debug('===============logList=============='+logList);
        }
        catch(Exception ex){
         //createMessage(ApexPages.Severity.ERROR,'Error while processing the request. Please check Logs.');
         String logString =(string.isNotEmpty(TransType) ? 'TransType='+ TransType +'&': '') +'NameOnCard='+nameOnCard+'&Amount='+string.ValueOf(amount) +'&PNRef='+ (deserializeResp!=null && !string.IsEmpty(deserializeResp.PNRef) ? deserializeResp.PNRef : '');          
         logList.add(CustomerPortalWSDLUtil.getLogging('Acculynk Transaction API Error', (string)theAccount.Id, req.toString()+logString, ex.getMessage() + ' at Line no ' + ex.getLineNumber()));                  
         }
        finally{
        if(!logList.isEmpty()) insert logList; 
        }
        return deserializeResp;
    }
    //Method to call from client side to Addnote and transaction
    public pagereference AddNote(){       
        addNoteSaleComplete(null,null,null,null);
        return null;        
    }
    //Common Method to Addnote and transaction for Sale/SaleComplete
    public pagereference addNoteSaleComplete(String sResult,String sRespMSG,String sAPIRequest,string sPNRef){
        list<logging__c> logList = new list<logging__c>(); 
        try
        {
            ApexPages.severity msgSeverity = ApexPages.severity.ERROR;
            string Result,innerErrorMessage,APIRequest;
            string TransType=apexpages.currentpage().getparameters().get('TransType');
            if(TransType!=null){
                Result= sResult;TID= sPNRef;innerErrorMessage=sRespMSG;APIRequest=sAPIRequest;
            }
            else{
                Result=apexpages.currentpage().getparameters().get('Result');
                TID=apexpages.currentpage().getparameters().get('TID');
                innerErrorMessage=apexpages.currentpage().getparameters().get('InnerErrorMessage');
                APIRequest=apexpages.currentpage().getparameters().get('APIRequest');
            }
                        
            string ID = apexpages.currentpage().getparameters().get('id');
            
            if(Result!=null && string.IsNotEmpty(Result) && ID !=null){ 
                //3716 Resilt is now Comapring with string rather than Integer as done previosly
                if(String.valueOf(Result) == '0' ){ 
                    msgSeverity = ApexPages.severity.Confirm;
                    logging__c noteLog = customerPortalWSDLUtil.AddNote((string)theAccount.Id,'Debit Card',paymentType, amount,Date.Today(),theAccount.Customer_Name__c,theAccount.Name,selectedCardType,TID,0);
                    logging__c paymentLog = customerPortalWSDLUtil.addPayment((string)theAccount.Id, Label.Debit_Card_Payment_Mode, amount , System.today(), System.today(),paymentType,0);
                    //logging__c paymentLog = customerPortalWSDLUtil.addPayment((string)theAccount.Id, Label.Debit_Card_Payment_Mode, amount , Date.ValueOf('2017-7-18'), Date.ValueOf('2017-7-18'),paymentType,0);
                    if(noteLog <> null) logList.add(noteLog);
                    if(paymentLog <> null) logList.add(paymentLog);
                    
                    //if(innerErrorMessage == '' )                    
                    createMessage(msgSeverity, 'Your Debit Card Transaction is successful with TransactionID = '+TID + '.');                    
                    //else                     
                    // createMessage(msgSeverity, 'Your Debit Card Transaction is successful with TransactionID = '+TID + '. Description : ' + innerErrorMessage);                                                                                
                    
                    if(paymentType.contains('Monthly')) 
                        sendMail(Payment_Monthly,null);
                    else
                        sendMail(Payment_Submission,null);
                }else{                    
                    if(mapPayLeapMeaasages.containsKey(Result)){ 
                        createMessage(msgSeverity, mapPayLeapMeaasages.get(Result)); 
                        logList.add(CustomerPortalWSDLUtil.getLogging('Acculynk Transaction API Error', (string)theAccount.Id, APIRequest, 'Transaction API Result is = '+Result+ ' with Error message = '+ mapPayLeapMeaasages.get(Result)));                        
                    }
                    else{
                        if(innerErrorMessage == '' )                    
                            createMessage(msgSeverity, 'Your Debit Card Transaction is failed.Please Contact your Bank for more information.');                    
                        else                     
                            createMessage(msgSeverity, 'Your Debit Card Transaction is failed. Description : ' + innerErrorMessage);                                                                                
                        logList.add(CustomerPortalWSDLUtil.getLogging('Acculynk Transaction API Error', (string)theAccount.Id, APIRequest, 'Transaction API Result is = '+Result+ ' with Error message = '+ innerErrorMessage));
                        
                    }
                    
                    sendMail(Payment_Failed,'Incorrect Card Information');   
                    
                }
                theAccount = [Select Id, Name,  loan__Contact__r.Name,Customer_Name__c, loan__ACH_Next_Debit_Date__c,loan__Pmt_Amt_Cur__c,loan__Pay_Off_Amount_As_Of_Today__c,loan__Amount_to_Current__c,loan__Delinquent_Amount__c,loan__Contact__c,loan__OT_ACH_Debit_Date__c, loan__OT_ACH_Payment_Amount__c,loan__Last_Accrual_Date__c from loan__loan_account__c where Id =:lId ];
               
            } 
            /* 3918 default message */
            else if(ID !=null && !string.IsEmpty(innerErrorMessage)){              
                createMessage(msgSeverity,innerErrorMessage); 
                if(innerErrorMessage==errorMessage)                                                                              
                sendMail(Payment_Failed,mailErrorMsg);  
            }  
            nameOnCard = null;
            debitCardNumber = null;
            securityCode = null;                          
            
        }catch(exception e){        
            createMessage(ApexPages.severity.ERROR, 'Error while updating Transaction details in SFDC' );
             logList.add(CustomerPortalWSDLUtil.getLogging('Acculynk Transaction API Error', (string)theAccount.Id, 'addNoteSaleComplete', e.getMessage()));               
            
        }finally{
            if(!logList.isEmpty()){
                insert logList;
            }
        }
        return null;
    }           
    
    public PageReference goBack() {
        return new PageReference(URL.getSalesforceBaseUrl().toExternalForm().replace('c.', 'loan.') + '/apex/tabbedLoanAccount?id=' + this.theAccount.Id);
    }
    
    @TestVisible
    private void createMessage(ApexPages.severity severity, String message) {
        //ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }
    
    
    
    @TestVisible
    private void sendMail(string template,string errorMeaasage){
        AANNotificationJob  objAANNotificationJob =new AANNotificationJob(); 
        eTemplate = [SELECT id, name, HtmlValue, Subject, Body FROM EmailTemplate WHERE DeveloperName = : template];
        string subject=(eTemplate.Subject).replace('{!Contract.Id}',theAccount.Name);
        string mailbody = replaceValuesFromData(eTemplate.Body,theAccount.loan__Contact__r.Name, Date.Today(), amount, tempdebitCardNumber, paymentType,errorMeaasage,TID);    
        Outbound_Emails_Temp__c  emailContent = objAANNotificationJob.createEmail(mailbody,subject,theAccount .loan__Contact__c);
        insert emailContent; 
    }
    
    @TestVisible 
    private static string replaceValuesFromData(string bodyMail,string contactName, date paymentDate, decimal paymentAmount, string cardNo, string paymentType,string errorMeaasage,string transactionID){      
        if (contactName!= null) bodyMail = bodyMail.replace('{!Contact.Name}',contactName);
        if (paymentDate!= null) bodyMail = bodyMail.replace('[paymentDate]',paymentDate.format());
        if (paymentAmount!= null) bodyMail = bodyMail.replace('[paymentAmount]',string.valueOf(paymentAmount));
        if (cardNo!= null) bodyMail = bodyMail.replace('[cardNumber]',cardNo);
        if (paymentType!= null) bodyMail = bodyMail.replace('[paymentType]',paymentType);           
        if (errorMeaasage!= null) bodyMail = bodyMail.replace('{!Logging__c.API_Response__c}',errorMeaasage); 
        if (transactionID!= null) bodyMail = bodyMail.replace('[transactionID]',transactionID); 
        
        return bodyMail;
    }
    //wrapper class for extracting the response
    public class deserializeResponse{
        public String AuthCode{get;set;} 
        public String sessionId{get;set;} 
        public String GetAVSResult{get;set;}
        public String GetCVResult{get;set;}
        public String GetCommercialCard{get;set;}
        public String HostCode{get;set;}
        public String InvNum{get;set;}
        public String Message{get;set;}
        public String PNRef{get;set;}
        public String ProcessedAsCreditCard{get;set;}
        public String RespMSG{get;set;}
        public String Result{get;set;}
        public String Exponent{get;set;}
        public String GUID{get;set;}
        public String InnerErrorCode{get;set;}
        public String InnerErrorMessage{get;set;}
        public String Modulus{get;set;}      
        public String PinReferenceId{get;set;}  
        public string TokenNumber{get;set;}
        public decimal Amount{get;set;}
        public string expirationDate {get;set;}
    }

    private void insertOtAchNote(String contractId, Decimal otAchPayment, Date otAchDebitDate, Decimal payAmount) 
    {
        Note newNote = new Note();
        String noteTime = DateTime.now().format('HH:mm:ss');
        String noteDate = DateTime.now().format('MM/dd/YYYY');
        Integer d = otAchDebitDate.day();
        Integer mo = otAchDebitDate.month();
        Integer yr = otAchDebitDate.year();
        DateTime otDate = DateTime.newInstance(yr, mo, d);
        String otDateFormatted = otDate.format('MM/dd/YYYY');

        newNote.parentid = contractId;
        newNote.title = 'OT ACH Deleted over Debit Card Payment';
        newNote.body =  'On ' + noteDate + ' at ' + noteTime + ', the customer processed a debit card payment for $' + payAmount + '. Therefore, the One Time ACH scheduled for ' + otDateFormatted + 
                        ' for ' + otAchPayment + ' was deleted.';
        insert newNote;
    } 
    /************3907 Mask Debit Card Number*********/     
    public static String maskAccountAndDebitCardNo(String cardNumber){
      string digits='',lastFourDigit ='' ;
      if(!string.Isempty(cardNumber) && cardNumber.length() > 4 ){
           integer len = cardNumber.length();
           lastFourDigit = cardNumber.substring(len-4,len);
           for(integer i = 0 ; i<len-4 ; i++) 
              digits = digits  + 'X';            
           }
      return digits + lastFourDigit;
    } 
}