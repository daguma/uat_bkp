public class DealAutomaticSummaryList {

    public String usage {get; set;}
    public List<DealAutomaticSummaryAttribute> attributes {get; set;}
    public Boolean allSelected {get; set;}

    public DealAutomaticSummaryList() {

    }

    public DealAutomaticSummaryList(String usage,
                                    List<DealAutomaticSummaryAttribute> attributes) {
        this.usage = usage;
        this.attributes = attributes;
        this.allSelected = areAllAttributesSelected(attributes);
    }

    public Boolean areAllAttributesSelected(List<DealAutomaticSummaryAttribute> attributes) {
        Boolean result = true;

        if (!attributes.isEmpty()) {
            for (DealAutomaticSummaryAttribute attribute : attributes) {
                if (!attribute.isActive) {
                    result = false;
                    break;
                }
            }
        } else
            result = false;


        return result;
    }
}