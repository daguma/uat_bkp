@isTest public class BankStatement_ResponseCtrlTest {

    testmethod static void test1() {
        Opportunity app = LibraryTest.createApplicationTH();
        String accountNumberFound = '1234Account';
        BankStatement_ResponseCtrl.saveOpportunity(app.Id, accountNumberFound);
        Opportunity newApp = [SELECT Id, DL_Account_Number_Found__c FROM Opportunity WHERE Id =: app.Id];
        system.assertEquals(accountNumberFound,newApp.DL_Account_Number_Found__c);
    }

}