@isTest
private class CreditReportHawkAlertsResultTest {

    @isTest static void initializes_credit_report_hawk_alert_result() {
        String alertName = 'Commercial Address Test';
        Boolean isApproved = false;
        List<String> alertReasons = new List<String>();

        CreditReportHawkAlertsResult creditReportHawkAlertsResult =
            new CreditReportHawkAlertsResult();
        
        creditReportHawkAlertsResult =
            new CreditReportHawkAlertsResult(alertName, isApproved, alertReasons);

        System.assertEquals(alertName, creditReportHawkAlertsResult.alertName);
        System.assertEquals(isApproved, creditReportHawkAlertsResult.isApproved);
        System.assertEquals(alertReasons, creditReportHawkAlertsResult.alertReasons);
        System.assertEquals('Fail', creditReportHawkAlertsResult.hawkResult);
        System.assertEquals('red', creditReportHawkAlertsResult.hawkStyleClass);
    }
}