@isTest public with sharing class OpportunityHandlerTest {
    @isTest static void inserts_application() {

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];

        Opportunity app = LibraryTest.createApplicationTH();

        System.assert([SELECT COUNT() FROM Opportunity] > appsBefore);
        
        Account acc = LibraryTest.createAccount('test handler','test handler');
        acc = [select Id ,Merchant_Invoice_Required_To_Fund__c from Account where Id = :acc.Id];
        acc.Merchant_Invoice_Required_To_Fund__c = true;
        update acc;
        
        app.status__c = 'Approved, Pending Funding';
        app.partner_account__c = acc.Id;
        app.Deal_is_Ready_to_Fund__c = true;
        update app;
    }
    
    @isTest static void delete_application() {

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];

        Opportunity app = LibraryTest.createApplicationTH();
        
        delete app;
        
        System.assert([SELECT COUNT() FROM Opportunity] == appsBefore);
    }
}