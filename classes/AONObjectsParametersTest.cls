@isTest
private class AONObjectsParametersTest {

    @isTest static void enrollment_input_data() {
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        
        Contact cont = [SELECT FirstName, LastName, MailingStreet, MailingCity, MailingState, 
                        MailingPostalCode, Email, Phone, Id, ints__Middle_Name__c, Phone_Clean__c 
                        FROM Contact ORDER BY CreatedDate DESC LIMIT 1];
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
        AONObjectsParameters.AONEnrollmentInputData enrollmentData = new AONObjectsParameters.AONEnrollmentInputData(cont, clcontract.Name, '');
        
        System.assertEquals('US', enrollmentData.countryCode);

		String jsonRequest = new AONObjectsParameters.AONEnrollmentInputData().get_JSON(enrollmentData);
        
        System.assertNotEquals(null, jsonRequest);        
    }
    
    @isTest static void cancellation_input_data() {
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = clcontract.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;
        
        AONObjectsParameters.AONCancellationInputData cancellationData = new AONObjectsParameters.AONCancellationInputData(clcontract.Name, 'JLP', '0');
        
        System.assertEquals(cancellationData.accountNumber, clcontract.Name);
        
        String jsonRequest = new AONObjectsParameters.AONCancellationInputData().get_JSON(cancellationData);
        
        System.assertNotEquals(null, jsonRequest);
    }
    
    @isTest static void billing_input_data() {
        
        AONObjectsParameters.AONBillingInputData billingData = new AONObjectsParameters.AONBillingInputData(System.now(),'6.31','100','5000',System.now(),'LAI-00000001','LPOPTION01');
        
        System.assertEquals('5000', billingData.loanBalance);
        
        String jsonRequest = new AONObjectsParameters.AONBillingInputData().get_JSON(billingData);
        
        System.assertNotEquals(null, jsonRequest);
    }
    
    @isTest static void aon_response() {
        
        String resp = '{"aonResponse":null,"errorMessage":null,"errorDetails":null}';
        
    	AONObjectsParameters.AONResponse response = new AONObjectsParameters.AONResponse(resp);
        
        System.assertEquals(null, response.aonResponse);
    }
    
    @isTest static void aon_response_with_error_details() {
        
        String resp = '{"aonResponse":null,"errorMessage":null,"errorDetails":"test"}';
        
    	AONObjectsParameters.AONResponse response = new AONObjectsParameters.AONResponse(resp);
        
        System.assertEquals(null, response.aonResponse);
    }
    
    @isTest static void aon_response_with_error() {
        
        String resp = '{"aonResponse":null,"errorMessage":"Error","errorDetails":null}';
        
    	AONObjectsParameters.AONResponse response = new AONObjectsParameters.AONResponse(resp);
        
        System.assertEquals('Error', response.errorMessage);
    }
    
    @isTest static void salesforce_response() {
        
        AONObjectsParameters.SalesforceResponse response = new AONObjectsParameters.SalesforceResponse();
        
        System.assertEquals('Response from Salesforce', response.response);
        
        String jsonRequest = new AONObjectsParameters.SalesforceResponse().get_JSON(response);
        
        System.assertNotEquals(null, jsonRequest);
    }
    
    @isTest static void salesforce_response_with_input_data() {
        
        String inputData = 'test';
        
        AONObjectsParameters.SalesforceResponse response = new AONObjectsParameters.SalesforceResponse(inputData);
        
        System.assertEquals(inputData, response.inputData);
    }
    
    @isTest static void converts_timestamp() {
        AONObjectsParameters aon = new AONObjectsParameters();
        Datetime convert2 = aon.convertTimestamp('2012-07-31T11:22:33.444Z');
        
        System.assertEquals(2012,convert2.year());
    }
    
}