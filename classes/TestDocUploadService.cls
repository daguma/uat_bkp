@isTest
public Class TestDocUploadService {
    
    @testSetup
    static void testData(){
                
        MaskSensitiveDataSettings__c msd =new MaskSensitiveDataSettings__c(SetupOwnerId=UserInfo.getOrganizationId(),MaskAllData__c=False);
        insert msd;
    }
    
    static testMethod void uploadFileWithOppID() {
        //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Firstname = 'Test First';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 10000;
        cntct.MailingState = 'GA';
        insert cntct;
        
        Account dummyAccount = new Account();
        dummyAccount.Name = 'DummyAccount';
        dummyAccount.c2g__CODABankName__c = 'Test Bank';
        dummyAccount.c2g__CODABankAccountNumber__c = '123456';
        dummyAccount.c2g__CODAPaymentMethod__c = 'Cash';
        
        insert dummyAccount;
        cntct.AccountId = dummyAccount.Id;
        update cntct; 

        //Dummy Application
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000;
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification';
        aplcn.Type = 'New';

        insert aplcn;
        opportunity aplcnToSelect = [select id, Contact__c ,  Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id];

        //Dummy offers
        Offer__c off = new Offer__c();
        off.opportunity__c = aplcn.id;
        off.Fee__c = 500;
        off.APR__c = 0.0050;
        off.Payment_Amount__c = 1200;
        off.Loan_Amount__c = 13000;
        off.Loan_Amount__c = 4655;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Term__c = 1234;
        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;

        List<Upload_File_Max_Limits__c> uploadFileMaxList=new List<Upload_File_Max_Limits__c>();
        uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Personal Identification',No_of_Files_Limit__c=5,ReWrite__c=false));
        uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Signed Contract',No_of_Files_Limit__c=5,ReWrite__c=false));
        insert uploadFileMaxList;
        
        blob bodyblob = blob.ValueOf('dummybody');
        string bodydumy = EncodingUtil.base64Encode(bodyblob);
        Map<string, string> ReqMap = new Map<string, string>();
        ReqMap.put('OppId', aplcn.id);
        ReqMap.put('incomeSource', 'Self');
        ReqMap.put('docType', 'Signed Contract');
        ReqMap.put('fileName', 'Test file');
        ReqMap.put('fileContent', bodydumy);

        string request = Json.serializepretty(ReqMap);
        DocUploadService.UploadFile(request);
    }
    
    static testMethod void uploadFileWithPartnerID() {
        //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Firstname = 'Test First';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 10000;
        cntct.MailingState = 'GA';
        insert cntct;
        
        Account dummyAccount = new Account();
        dummyAccount.Name = 'DummyAccount';
        dummyAccount.c2g__CODABankName__c = 'Test Bank';
        dummyAccount.c2g__CODABankAccountNumber__c = '123456';
        dummyAccount.c2g__CODAPaymentMethod__c = 'Cash';
        
        insert dummyAccount;
        cntct.AccountId = dummyAccount.Id;
        update cntct;  

        //Dummy Application
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000;
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification';
        aplcn.Type = 'New';

        insert aplcn;
        opportunity aplcnToSelect = [select id, Contact__c ,  Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id];

        //Dummy offers
        Offer__c off = new Offer__c();
        off.opportunity__c = aplcn.id;
        off.Fee__c = 500;
        off.APR__c = 0.0050;
        off.Payment_Amount__c = 1200;
        off.Loan_Amount__c = 13000;
        off.Loan_Amount__c = 4655;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Term__c = 1234;
        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;

        List<Upload_File_Max_Limits__c> uploadFileMaxList=new List<Upload_File_Max_Limits__c>();
        uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Personal Identification',No_of_Files_Limit__c=5,ReWrite__c=false));
        uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Signed Contract',No_of_Files_Limit__c=0,ReWrite__c=false));
        insert uploadFileMaxList;
        
        blob bodyblob = blob.ValueOf('dummybody');
        string bodydumy = EncodingUtil.base64Encode(bodyblob);
        Map<string, string> ReqMap = new Map<string, string>();
        ReqMap.put('partnerId', dummyAccount.id);
        ReqMap.put('incomeSource', 'Self');
        ReqMap.put('docType', 'Signed Contract');
        ReqMap.put('fileName', 'Test file');
        ReqMap.put('fileContent', bodydumy);

        string request = Json.serializepretty(ReqMap);
        DocUploadService.UploadFile(request);
    }
    
    static testMethod void Scenario1() {

        //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Firstname = 'Test First';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 10000;
        cntct.MailingState = 'GA';
        insert cntct;

        //Dummy Application
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000;
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification';
        aplcn.Type = 'New';

        insert aplcn;
        
        attachment atch = new attachment(name='testAttach',parentid=aplcn.id, body = blob.valueOf('abc'), description = 'File from APIautoworkflow:test');
        insert atch;
        
        opportunity aplcnToSelect = [select id, Contact__c ,  Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id];

        //Dummy offers
        Offer__c off = new Offer__c();
        off.opportunity__c = aplcn.id;
        off.Fee__c = 500;
        off.APR__c = 0.0050;
        off.Payment_Amount__c = 1200;
        off.Loan_Amount__c = 13000;
        off.Loan_Amount__c = 4655;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Term__c = 1234;
        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;

        List<Upload_File_Max_Limits__c> uploadFileMaxList=new List<Upload_File_Max_Limits__c>();
        uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Personal Identification',No_of_Files_Limit__c=5,ReWrite__c=false));
        uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Signed Contract',No_of_Files_Limit__c=5,ReWrite__c=false));
        insert uploadFileMaxList;
        
        blob bodyblob = blob.ValueOf('dummybody');
        string bodydumy = EncodingUtil.base64Encode(bodyblob);
        Map<string, string> ReqMap = new Map<string, string>();
        ReqMap.put('offerId', off.id);
        ReqMap.put('incomeSource', 'Self');
        ReqMap.put('docType', 'Signed Contract');
        ReqMap.put('fileName', 'Test file');
        ReqMap.put('fileContent', bodydumy);

        string request = Json.serializepretty(ReqMap);
        DocUploadService.UploadFile(request);

        Map<string, string> NoteMap = new Map<string, string>();
        NoteMap.put('offerId', off.id);
        string NoteRequest = Json.serializepretty(NoteMap);
        DocUploadService.GetFiles(NoteRequest);

        Attachment ExistingAttch = [select name, id, createddate, parentid from Attachment where parentid = : aplcn.id limit 1];
        Testing_Settings__c TS = new Testing_Settings__c();
        TS.name = 'TestData';
        TS.Test_Email__c = 'test@yahoo.com';
        TS.Test_SSN__c = 'testSSN';
        insert TS;

        DocUploadService.insertNotes(ExistingAttch.id,'testing class');
    }
    static testMethod  void uploadTokenFileTest(){
    //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Firstname = 'Test First';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 10000;
        insert cntct;

        //Dummy Application
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000; 
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification'; 
        aplcn.Type = 'New';
      
        insert aplcn;
        
        aplcn=[select Opportunity_number__c, Name from opportunity where id=: aplcn.id];
        string token=encrypt(aplcn.Opportunity_number__c);
        blob bodyblob = blob.ValueOf('dummybody');
        string bodydumy = EncodingUtil.base64Encode(bodyblob);
        Map<string, string> ReqMap = new Map<string, string>();
        ReqMap.put('docType', 'Signed Contract');
        ReqMap.put('fileName', 'Test file');
        ReqMap.put('fileContent', bodydumy);
        ReqMap.put('token', token);
        
        string request = Json.serializepretty(ReqMap);
                 
         List<Upload_File_Max_Limits__c> uploadFileMaxList=new List<Upload_File_Max_Limits__c>();
         uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Personal Identification',No_of_Files_Limit__c=5,ReWrite__c=false));
          uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Signed Contract',No_of_Files_Limit__c=5,ReWrite__c=false));
         insert uploadFileMaxList;
         
        DocUploadService.uploadTokenFile(request);
        
        
    }
     static testMethod  void uploadTokenFileTest2(){
    //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Firstname = 'Test First';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 10000;
        insert cntct;

        //Dummy Application
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000; 
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
aplcn.Type = 'New';

        aplcn.StageName = 'Qualification';         
        insert aplcn;
        
        string token=encrypt([Select Opportunity_Number__c from Opportunity where Id =: aplcn.Id].Opportunity_Number__c);
        
        blob bodyblob = blob.ValueOf('dummybody');
        string bodydumy = EncodingUtil.base64Encode(bodyblob);
        Map<string, string> ReqMap = new Map<string, string>();
        ReqMap.put('docType', 'Signed Contract');
        ReqMap.put('fileName', 'Test file');
        ReqMap.put('fileContent', bodydumy);
        ReqMap.put('token', token);
        
        string request = Json.serializepretty(ReqMap);
        
         List<Upload_File_Max_Limits__c> uploadFileMaxList=new List<Upload_File_Max_Limits__c>();
         uploadFileMaxList.add(new Upload_File_Max_Limits__c(Name = 'Signed Contract',No_of_Files_Limit__c=5,ReWrite__c=false));
         insert uploadFileMaxList;
         
        DocUploadService.uploadTokenFile(request);
        
        
    }
    static testMethod  void uploadTokenFileTest3(){
    //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Firstname = 'Test First';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 10000;
        insert cntct;

        //Dummy Application
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000;  
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification';   
                    aplcn.Type = 'New';
   
        insert aplcn;
        
        
        blob bodyblob = blob.ValueOf('dummybody');
        string bodydumy = EncodingUtil.base64Encode(bodyblob);
        Map<string, string> ReqMap = new Map<string, string>();
        ReqMap.put('docType', 'Signed Contract');
        ReqMap.put('fileName', 'Test file');
        ReqMap.put('fileContent', bodydumy);
        ReqMap.put('token', 'UadnYN/+95yo/HDDlZj7vbHYZSRGn8+yGyCH/sSM+BoMehuOEnSaciolaH3ANRO6Nj2JcqvUcP/pYpdrBdGTkw==');
        
        string request = Json.serializepretty(ReqMap);
         
        DocUploadService.uploadTokenFile(request);
        
        
    }
    
    static testmethod void updateApplicationStatus(){
        //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Firstname = 'Test First';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 10000;
        insert cntct;

        //Dummy Application
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000;  
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification';   
        aplcn.Type = 'New';
   
        insert aplcn;
        
        aplcn=[select Opportunity_Number__c from opportunity where id=: aplcn.id];
        string token=encrypt(aplcn.Opportunity_Number__c);
        DocUploadService.updateTokenizedApplicationStatus('{"token":"'+token+'"}');
        aplcn=[select Name, Status__c  from opportunity where id=: aplcn.id];
        //System.assertEquals('Documents Requested', aplcn.Status__c);
        String dummyToken=encrypt('abcdef');
        DocUploadService.updateTokenizedApplicationStatus('token');
        DocUploadService.updateTokenizedApplicationStatus('{"token":""}');
        DocUploadService.updateTokenizedApplicationStatus('{"token":"asdfasdf"}');
        DocUploadService.updateTokenizedApplicationStatus('{"token":"'+dummyToken+'"}');
    }
    
    static string encrypt(string Name){
        DateTime dt = System.now();
        string ExpirationDate=Label.Expiration_Hours!=null ? string.valueOf(dt.addHours(Integer.valueOf(Label.Expiration_Hours))):string.valueOf(dt.addHours(48));
        string FullData=Name + 'DBQT' + ExpirationDate;
         Blob key = EncodingUtil.base64Decode(Label.Documnet_Encrypted_Key);//Crypto.generateAesKey(128);
         Blob data = Blob.valueOf(FullData);
         Blob encrypted = Crypto.encryptWithManagedIV('AES128', key, data);
        return EncodingUtil.base64Encode(encrypted);
    }
}