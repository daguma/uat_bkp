@isTest
public class CreateCampaignsForSmsNotificationsTest {
    @isTest static void testConstructor() {
        Test.startTest();
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();
        System.assertEquals(True, True);
        Test.stopTest();
    }

    @isTest static void testExecute() {
        Test.startTest();
        SchedulableContext SC;
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();
        scheduler.execute(SC);
        System.assertEquals(True, True);
        Test.stopTest();
    }

    @isTest static void testCreateCampaignForDebitCardPayments() {
        Test.startTest();
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();

        Date nextPaymentDate = System.now().date();
        Campaign campaign = scheduler.createCampaignForDebitCardPayments(nextPaymentDate);
        System.assertEquals(Null, campaign);
        Test.stopTest();
    }

    @isTest static void testCreateCampaignForBankAccountPayments() {
        Test.startTest();
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();

        Date nextPaymentDate = System.now().date();
        Campaign campaign = scheduler.createCampaignForBankAccountPayments(nextPaymentDate);
        System.assertEquals(Null, campaign);

        Test.stopTest();
    }

    // Campaign createCampaignForManualPayments(Date nextPaymentDate)
    @isTest static void testCreateCampaignForManualPayments() {
        Test.startTest();
        
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();
        Date nextPaymentDate = System.now().date();
        Campaign campaign = scheduler.createCampaignForManualPayments(nextPaymentDate);
        System.assertEquals(Null, campaign);
        
        Test.stopTest();
    }

    // Campaign createCampaign(String notificationType)
    @isTest static void testCreateCampaign() {
        Test.startTest();
        String campaignType = 'SMS Payment Reminder - Manual Payment';

        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();
        Campaign campaign = scheduler.createCampaign(campaignType, Date.today());
        System.assertNotEquals(Null, campaign);
        Test.stopTest();
    }

    @isTest static void testAssignMembersToCampaign() {
        Test.startTest();
        Campaign campaign;
		String campaignType;
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();

        List<loan__Loan_Account__c> contracts = new List<loan__Loan_Account__c>();
        contracts.add(createContractForManualPayment(Date.today()));

        // Debit Card
        campaignType = 'SMS Payment Reminder - Debit Card';
        campaign = new Campaign();
        campaign.Name = 'Foo Campaign';
        campaign.SMS_Notification_Status__c = 'Active';
		campaign.Type = 'SMS Notifications';
        campaign.SMS_Notification_Type__c = campaignType;
        insert campaign;        
        
        scheduler.assignMembersToCampaign(campaign, contracts);
        System.assertEquals(True, True);

        // Bank Account
        campaignType = 'SMS Payment Reminder - Bank Account';
        campaign = new Campaign();
        campaign.Name = 'Foo Campaign';
        campaign.SMS_Notification_Status__c = 'Active';
		campaign.Type = 'SMS Notifications';
        campaign.SMS_Notification_Type__c = campaignType;
        insert campaign;        
        
        scheduler.assignMembersToCampaign(campaign, contracts);
        System.assertEquals(True, True);

        //
        campaignType = 'SMS Payment Reminder - Manual Payment';
        campaign = new Campaign();
        campaign.Name = 'Foo Campaign';
        campaign.SMS_Notification_Status__c = 'Active';
		campaign.Type = 'SMS Notifications';
        campaign.SMS_Notification_Type__c = campaignType;
        insert campaign;        
        
        scheduler.assignMembersToCampaign(campaign, contracts);
        System.assertEquals(True, True);
        Test.stopTest();
    }


    // List<loan__Loan_Account__c> getContractsForDebitCardPayments(Date nextPaymentDate)
    @isTest static void testGetContractsForDebitCardPayments() {
        Test.startTest();
        Date nextPaymentDate = System.now().date();
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();
        List<loan__Loan_Account__c> contracts = scheduler.getContractsForDebitCardPayments(nextPaymentDate);
        System.assertEquals(new List<loan__Loan_Account__c>(), contracts);
        Test.stopTest();
    }

    @isTest static void testGetContactsForBankAccountPayments() {
        Test.startTest();
        Date nextPaymentDate = System.now().date();
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();
        List<loan__Loan_Account__c> contracts = scheduler.getContactsForBankAccountPayments(nextPaymentDate);
        System.assertEquals(new List<loan__Loan_Account__c>(), contracts);
        Test.stopTest();
    }

    @isTest static void testGetContactsForManualPayments() {
        Test.startTest();
        Date nextPaymentDate = System.now().date();
        CreateCampaignsForSmsNotifications scheduler = new CreateCampaignsForSmsNotifications();
        List<loan__Loan_Account__c> contracts;

        contracts = scheduler.getContactsForManualPayments(nextPaymentDate);
        System.assertEquals(new List<loan__Loan_Account__c>(), contracts);

        // createContractForManualPayment(nextPaymentDate);
        // contracts = scheduler.getContactsForManualPayments(nextPaymentDate);
        // System.assertNotEquals(new List<loan__Loan_Account__c>(), contracts);

        Test.stopTest();
    }
    
    static loan__Loan_Account__c createContractForManualPayment(Date nextPaymentDate) {
        Date lastPaymentDate = nextPaymentDate.addDays(-8);

        Opportunity a = null;

        if ([SELECT COUNT() FROM Opportunity] > 0) {
            a = [SELECT id, name, Contact__c FROM Opportunity LIMIT 1];
        } else {
            a = LibraryTest.createApplicationTH();
        }

        Contact c = [Select Id, Name from Contact where Id = : a.Contact__c];
        c.Phone = '+18584425066';
        upsert c;

        Account acc = new Account();
        acc.name = c.Name;
        acc.Cobranding_Name__c = c.Name;
        upsert acc;
        c.AccountId = acc.Id;
        update c;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Payment_Amount__c = 250;
        contract.loan__Pmt_Amt_Cur__c = 250;
        contract.loan__Delinquent_Amount__c = 0;
        contract.loan__Fees_Remaining__c = 0;
        contract.loan__Next_Installment_Date__c = Date.today().addDays(6);
        contract.loan__Contact__c = c.Id;
        contract.Opportunity__c = a.Id;
        contract.loan__Number_of_Installments__c = 18;
        contract.loan__APR__c = .2399;
        contract.loan__Loan_Amount__c = 4000;
        contract.loan__Term_Cur__c = 10;
        contract.Proof_of_Claim_Filed__c = '';
        contract.loan__Interest_Rate__c = 16;
        contract.loan__ACH_Next_Debit_Date__c = Date.today();
        contract.Originated_EAR__c = 16;
        contract.Original_Partner_Account__c = acc.Id;

        // contract.Name = 'Contract ' + System.now().format();
        // contract.Overdue_Days__c = 0;
        contract.loan__Loan_Status__c = 'Active - Good Standing';
        contract.loan__ACH_On__c = False;
        contract.Is_Debit_Card_On__c = False;
        contract.loan__Next_Installment_Date__c = nextPaymentDate;
        // contract.SMS_Opt_Out__c = False;
        // contract.loan__Contact__r.Do_Not_Contact__c = False;
        // contract.Payoff_As_Of_Today_GT_Pmt_Amnt_Cur__c = 'true';
        contract.loan__Last_Payment_Date__c = lastPaymentDate;
		contract.loan__ACH_Next_Debit_Date__c = Date.today();

        // contract.IsDeleted = False;
        upsert contract;
        return contract;
    }

}