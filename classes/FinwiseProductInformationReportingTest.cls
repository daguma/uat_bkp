@isTest(seeAllData=false)
public class FinwiseProductInformationReportingTest {
    static testmethod void test1(){
        loan__Loan_Product__c sProd=TestHelper.createLendingProduct('sample product');
        FinwiseProductInformationReporting.executeProductInformationAPI(new LIST<Id>{sProd.Id});
    }
    static testmethod void test2(){
        FinwiseDetails__c fwd=new FinwiseDetails__c(Finwise_LendingPartnerProductId__c='7',Minimum_FICO__c=600,Origination_Fee_Range__c='0%-5%',Name='finwise');
        insert fwd;
        loan__Loan_Product__c sProd=TestHelper.createLendingProduct('sample product');
        FinwiseProductInformationReporting.executeProductInformationAPI(new LIST<Id>{sProd.Id});
    }
}