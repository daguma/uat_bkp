@isTest public class ProxyApiBankStatementsTest {

    @isTest static void gets_stand_alone_link() {

        ProxyApiUtil.settings = null;

        String jsonResult = ProxyApiBankStatements.ApiGetStandAloneLink('test123contact', 'M', 'Drapper', '56-4325647', '999999963', '0', 'test123app', '005n0000002D6Q0AAK');
        System.assertEquals(null, jsonResult);

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        jsonResult = ProxyApiBankStatements.ApiGetStandAloneLink('test123contact', 'M', 'Drapper', '56-4325647', '999999963', '0', 'test123app', '005n0000002D6Q0AAK');
        System.assertNotEquals(null, jsonResult);
    }

    @isTest static void gets_stand_alone_link_empty_parameters() {

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        String jsonResult = ProxyApiBankStatements.ApiGetStandAloneLink('', '', '', '', '', '', '', '');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_api_get_statements() {

        ProxyApiUtil.settings = null;

        List<DL_AccountStatementSF> dl_AccountStatementSFList =
            ProxyApiBankStatements.ApiGetStatements('003Q0000017zWh4IAE', 'a3lQ00000006xHWIAY');
        System.assertEquals(null, dl_AccountStatementSFList);

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        dl_AccountStatementSFList =
            ProxyApiBankStatements.ApiGetStatements('003Q0000017zWh4IAE', 'a3lQ00000006xHWIAY');
        System.assertNotEquals(null, dl_AccountStatementSFList);

    }

    @isTest static void gets_statements_empty_parameters() {

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        List<DL_AccountStatementSF> dl_AccountStatementSFList =
            ProxyApiBankStatements.ApiGetStatements('', '');
        System.assertEquals(null, dl_AccountStatementSFList);
    }

    @isTest static void gets_api_get_specific_transactions() {

        ProxyApiUtil.settings = null;

        List<DL_AccountStatementSF> dl_AccountStatementSFList =
            ProxyApiBankStatements.ApiGetSpecificTransactions('003Q0000017zWh4IAE', 'a3lQ00000006xHWIAY', '981234', '');
        System.assertEquals(null, dl_AccountStatementSFList);

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        dl_AccountStatementSFList =
            ProxyApiBankStatements.ApiGetSpecificTransactions('003Q0000017zWh4IAE', 'a3lQ00000006xHWIAY', '981234', '');
        System.assertNotEquals(null, dl_AccountStatementSFList);

    }

    @isTest static void gets_api_get_specific_transactions_with_replace() {

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        List<DL_AccountStatementSF> dl_AccountStatementSFList =
            ProxyApiBankStatements.ApiGetSpecificTransactions('100000', 'a3lQ00000006xHWIAY', '981234', '');
        System.assertNotEquals(null, dl_AccountStatementSFList);

    }

    @isTest static void gets_specific_transactions_empty_parameters() {

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        List<DL_AccountStatementSF> dl_AccountStatementSFList =
            ProxyApiBankStatements.ApiGetSpecificTransactions('', '', '', '');
        System.assertEquals(null, dl_AccountStatementSFList);
    }

    @isTest static void gets_bank_info_by_bank_name() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        String result = ProxyApiBankStatements.ApiGetBankInformationByBankName('contactId', 'applicationId', 'userId', 'bankName');

        System.assertNotEquals(result, null);

        result = ProxyApiBankStatements.ApiGetBankInformationByBankName('', '', 'userId', 'bankName');

        System.assertEquals(result, null);
    }

    @isTest static void gets_bank_info_by_routing_number() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        String result = ProxyApiBankStatements.ApiGetBankInformationByRoutingNumber('contactId', 'applicationId', 'userId', 'routingNumber');

        System.assertNotEquals(result, null);

        result = ProxyApiBankStatements.ApiGetBankInformationByRoutingNumber('', '', 'userId', 'routingNumber');

        System.assertEquals(result, null);
    }

    @isTest static void gets_bank_info_by_bank_name_without_validated() {
        ProxyApiUtil.settings = null;

        String result = ProxyApiBankStatements.ApiGetBankInformationByBankName('contactId', 'applicationId', 'userId', 'bankName');

        System.assertEquals(null, result);
    }

    @isTest static void gets_bank_info_by_routing_number_without_validated() {
        ProxyApiUtil.settings = null;

        String result = ProxyApiBankStatements.ApiGetBankInformationByRoutingNumber('contactId', 'applicationId', 'userId', 'routingNumber');

        System.assertEquals(null, result);
    }

}