@isTest
private class BridgerNewLookupCtrlTest {

    @isTest static void test_method_one() {

        Contact con = LibraryTest.createContactTH();


        BridgerWDSFLookup__c bridger = New BridgerWDSFLookup__c();
        bridger.contact__c = con.Id;
        upsert bridger;

        list<BridgerWDSFLookup__c> bridgerLookupList = new list<BridgerWDSFLookup__c>();

        bridger = [Select Id, contact__c FROM BridgerWDSFLookup__c where contact__c = : bridger.contact__c];

        Apexpages.StandardController controller = New Apexpages.StandardController(bridger);
        BridgerNewLookupCtrl newbridger = New BridgerNewLookupCtrl(controller);
        newbridger.createBrigerCall();
        newbridger.goback();

        bridgerLookupList.add(bridger);        
        system.assertEquals( 1, bridgerLookupList.size());
    }

}