@isTest public with sharing class Button_InvitationEmailCtrlTest {
    
    static testMethod void test1() {
        
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name = 
                            'Standard User' or name = 'System Administrator'];
        
        for(Profile p : ps){
            profiles.put(p.name, p.id);
        }       
        
        User standard = new User(alias = 'standt', 
                                 email='standarduser@testorg.com', 
                                 emailencodingkey='UTF-8', 
                                 lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', 
                                 profileid = profiles.get('Standard User'), 
                                 timezonesidkey='America/Los_Angeles', 
                                 username='alphav2_test@testorg.com');
        
        insert standard;
        
        //Insert Email Template
        EmailTemplate emailTemp = new EmailTemplate();
        emailTemp.isActive = true;
        emailTemp.Name = 'Decision Logic Email - Alpha';
        emailTemp.DeveloperName = 'test_notification_template';
        emailTemp.TemplateType = 'Text';
        emailTemp.HtmlValue = '<html>testing in progress</html>';
        emailTemp.FolderId = standard.id;
        insert emailTemp;
        
        System.runAs(standard) {                   
            Contact TestContact = LibraryTest.createContactTH();
            Opportunity opp = TestHelper.createApplicationFromContact(TestContact,true);
            
            Button_InvitationEmailCtrl.InvitationEmailEntity da = Button_InvitationEmailCtrl.sendInvitationEmail(opp.Id,'TESTAC','021001318');
            
            opp.invitation_Link__c = 'myLink';
            update opp;
            
            Button_InvitationEmailCtrl.InvitationEmailEntity da2 = Button_InvitationEmailCtrl.sendInvitationEmail(opp.Id,'TESTAC','021001318');
        }
    }
    
}