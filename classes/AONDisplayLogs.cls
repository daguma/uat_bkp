global class AONDisplayLogs {

	public String dialogHTML { get; set; }

 	public String recordId {get; set;}
    private List<Logging__c> logs {get; set;}


    Webservice static String getAllLogs(String thisId) 
    {
        AONDisplayLogs u = new AONDisplayLogs();
        u.recordId = thisId;
        u.initialize();
        return u.parseForLayout();
    }
  
   public void initialize() 
    {
        System.debug('recordId: ' + recordId);
        
        logs = [SELECT 
        				Id, 
        				CreatedDate,
        				Webservice__c, 
        				API_Request__c, 
        				API_Response__c, 
        				CreatedById,
                        CreatedBy.FirstName,
                        CreatedBy.LastName
        		  FROM Logging__c 
        		  WHERE CL_Contract__c = : recordId
                  AND (Webservice__c = 'Billing' OR Webservice__c  = 'Cancellation' OR Webservice__c ='Enrollment' )
        		  ORDER BY CreatedDate DESC];

        dialogHTML = parseForLayout();

        System.debug('dialogHTML: ' + dialogHTML);
    }

   
     public String parseForLayout() 
    {
        String divHeader = '<div id="AONLogPopup" title="AON Service Calls"><div id="AONLogContent"><table>';

        String divTableHeaders =    '<thead>' + 
                                    '<th>Call Date</th>' + 
                                    '<th>Created By</th>' +
                                    '<th>Service Method</th>' + 
                                    '<th>Request</th>' + 
                                    '<th>Response</th>' + 
                                    '</thead>';

        String divTableRows = '<tbody>';
        String divTableFooter = '</table></div></div>';

        for(Logging__c log : logs){

            String createdByName = log.CreatedBy.FirstName == null && log.CreatedBy.LastName == 'HAL' ? 'HAL' : log.CreatedBy.FirstName + ' ' + log.CreatedBy.LastName;
            
            divTableRows += '<tr>';
            divTableRows += '<td>' + log.CreatedDate + '</td>';
            divTableRows += '<td weight="20%">' + createdByName + '</td>';
            divTableRows += '<td>' + log.Webservice__c + '</td>';
            divTableRows += '<td>' + log.API_Request__c + '</td>';
            divTableRows += '<td>' + log.API_Response__c + '</td>';
            
            divTableRows += '</tr>';
        }

        divTableRows += '</tbody>';

        return divHeader + divTableHeaders + divTableRows + divTableFooter;
    }

	public AONDisplayLogs() {
		
	}
}