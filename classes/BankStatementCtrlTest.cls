@isTest public with sharing class BankStatementCtrlTest {

    static testMethod void test1() {

        BankStatementCtrl.BankStatementEntity bse = BankStatementCtrl.getBankStatementEntity(null, '', false);
        system.assertNotEquals(null, bse.errorMessage);

        Proxy_Api__c settings = LibraryTest.fakeSettings();
        Contact TestContact = LibraryTest.createContactTH();
        //Opportunity opp = TestHelper.createApplicationFromContact(TestContact, true);
        Opportunity opp = LibraryTest.createApplicationTH();
        bse = BankStatementCtrl.getBankStatementEntity(opp.Id, '', false);
        system.assertEquals(null, bse.errorMessage);

        opp.DL_Available_Balance__c = 12.00;
        opp.ACH_Account_Number__c = '12345678';
        update opp;
        bse = BankStatementCtrl.getBankStatementEntity(opp.Id, '', true);
        system.assertEquals(null, bse.errorMessage);

        opp.DL_Available_Balance__c = null;
        opp.DL_Current_Balance__c = 12.00;
        update opp;
        bse = BankStatementCtrl.getBankStatementEntity(opp.Id, '', true);
        system.assertEquals(null, bse.errorMessage);

        opp.DL_Available_Balance__c = null;
        opp.DL_Current_Balance__c = null;
        opp.DL_Average_Balance__c = 12.00;
        update opp;
        bse = BankStatementCtrl.getBankStatementEntity(opp.Id, '', true);
        system.assertEquals(null, bse.errorMessage);

        opp.DL_Available_Balance__c = null;
        opp.DL_Current_Balance__c = null;
        opp.DL_Average_Balance__c = null;
        opp.DL_Deposits_Credits__c = 12.00;
        update opp;
        bse = BankStatementCtrl.getBankStatementEntity(opp.Id, '', true);
        system.assertEquals(null, bse.errorMessage);

        opp.DL_Available_Balance__c = null;
        opp.DL_Current_Balance__c = null;
        opp.DL_Average_Balance__c = null;
        opp.DL_Deposits_Credits__c = null;
        opp.DL_Withdrawals_Debits__c = 12.00;
        update opp;
        bse = BankStatementCtrl.getBankStatementEntity(opp.Id, '', true);
        system.assertEquals(null, bse.errorMessage);

        opp.DL_Available_Balance__c = null;
        opp.DL_Current_Balance__c = null;
        opp.DL_Average_Balance__c = null;
        opp.DL_Deposits_Credits__c = null;
        opp.DL_Withdrawals_Debits__c = null;
        opp.Number_of_Negative_Days_Number__c = 12;
        update opp;
        bse = BankStatementCtrl.getBankStatementEntity(opp.Id, '', true);
        system.assertEquals(null, bse.errorMessage);

    }

}