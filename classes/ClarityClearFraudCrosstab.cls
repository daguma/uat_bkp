global class ClarityClearFraudCrosstab {

    public String name{get;set;}
    public Integer ssn{get;set;}
    public Integer drivers_license{get;set;}
    public Integer bank_account{get;set;}
    public Integer home_address{get;set;}
    public Integer zip_code{get;set;}
    public Integer home_phone{get;set;}
    public Integer cell_phone{get;set;}
    public Integer email_address{get;set;}
}