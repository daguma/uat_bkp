@isTest public class ProxyApiCreditReportSegmentEntityTest {
    @isTest static void validate_instance() {
        Test.startTest();
        ProxyApiCreditReportSegmentEntity se = new ProxyApiCreditReportSegmentEntity();
        Test.stopTest();

        System.assertEquals(null, se.id);
        System.assertEquals(null, se.value);
        System.assertEquals(null, se.parentId);
        System.assertEquals(null, se.prefix);
        System.assertEquals(null, se.segmentTypeEntity);
    }

    @isTest static void validate_assign() {
        Test.startTest();
        ProxyApiCreditReportSegmentEntity se = new ProxyApiCreditReportSegmentEntity();
        ProxyApiCreditReportSegmentTypeEntity ste = new ProxyApiCreditReportSegmentTypeEntity();
        Test.stopTest();

        se.id = 1;
        se.value = 'value test';
        se.parentId = 1;
        se.prefix = 'prefix test';

        System.assertEquals(1, se.id);
        System.assertEquals('value test', se.value);
        System.assertEquals(1, se.parentId);
        System.assertEquals('prefix test', se.prefix);

        ste.id = 1;
        ste.fieldName = 'name test';
        ste.length = 10;
        ste.startPosition = 1;
        ste.endPosition = 5;
        ste.type = 'type test';
        ste.description = 'description test';
        ste.parentId = 1;
        ste.isDisplayed = true;
        ste.isLengthValue = true;
        ste.isApplyLength = true;

        se.segmentTypeEntity = ste;

        System.assertEquals(ste, se.segmentTypeEntity);

    }
}