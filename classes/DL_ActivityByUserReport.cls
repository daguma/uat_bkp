global class DL_ActivityByUserReport {
    
    public List<ActivityByUserReportLine> reportLines {get; set;}
    
    global class ActivityByUserReportLine {

        public String salesforceUserId { get; set; }
        
        public Integer total { get; set; }
        public Integer not_Started { get; set; }
        public Integer started_not_Completed { get; set; }
        public Integer bank_Error { get; set; }
        public Integer account_Error { get; set; }
        public Integer login_not_Verified { get; set; }
        public Integer login_Verified { get; set; }
        
        public Integer dateRange { get; set; }
        
    }
   
}