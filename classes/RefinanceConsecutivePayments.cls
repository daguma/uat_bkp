public with sharing class RefinanceConsecutivePayments {


    /**
     * [RefiananceCheckConsecutivePayments description]
     * @param  contractList [description]
     * @return              [description]
     */
    public static Boolean CheckConsecutivePaymentsRules(loan__Loan_Account__c contract) {

        Boolean result = false;
        String strategiesType = '2, 3';

        if (contract != null) {

            Integer paymentLimit = contract.Opportunity__r.StrategyType__c != null && strategiesType.contains(contract.Opportunity__r.StrategyType__c) ? getLimitLowAndGrowByFrequency(contract.Payment_Frequency_Masked__c) : getLimitByFrequency(contract.Payment_Frequency_Masked__c);
            //Integer paymentLimit = getLimitByFrequency(contract.Payment_Frequency_Masked__c);
            List<loan__Loan_account_Due_Details__c> bills = getPaymentBills(contract.Id, paymentLimit);

            if ((bills.size() == paymentLimit) && (paymentLimit > 0)) {
                System.debug('Payments ' + bills.size() + ' limit ' + paymentLimit);
                result = isConsecutivePaymentsApproved(bills);
            }
        }

        return result;
    }

    /**
     * [isConsecutivePaymentsApproved description]
     * @param  bills [description]
     * @return       [description]
     */
    public static Boolean isConsecutivePaymentsApproved(List<loan__Loan_account_Due_Details__c> bills) {

        Boolean result = true;

        for (loan__Loan_account_Due_Details__c bill : bills) {
            if (!bill.loan__Payment_Satisfied__c) {
                result = false;
                break;
            }
        }

        return result;
    }


    /**
     * [getPaymentBills description]
     * @param  contractId   [description]
     * @param  paymentLimit [description]
     * @return              [description]
     */
    public static List<loan__Loan_account_Due_Details__c> getPaymentBills(String contractId, Integer paymentLimit) {

        List<loan__Loan_account_Due_Details__c> result = new List<loan__Loan_account_Due_Details__c>();

        if (!String.isEmpty(contractId)) {
            for (loan__Loan_account_Due_Details__c bill : [SELECT Id, Name, CreatedDate, loan__Payment_Satisfied__c
                    FROM loan__Loan_account_Due_Details__c
                    WHERE loan__Loan_Account__c = : contractId
                                                  ORDER BY CreatedDate DESC
                                                  LIMIT : paymentLimit]) {

                result.add(bill);
            }
        }

        return result;
    }

    /**
     * [getLimitByFrequency description]
     * @param  frequency [description]
     * @return           [description]
     */
    public static Integer getLimitByFrequency(String frequency) {

        Integer result = 0;

        if (!String.isEmpty(frequency)) {

            if (frequency.equalsIgnoreCase('Monthly')) {
                result = 6;
            } else if (frequency.equalsIgnoreCase('28 Days')) {
                result = 6;
            } else if (frequency.equalsIgnoreCase('Semi-Monthly')) {
                result = 12;
            } else if (frequency.equalsIgnoreCase('Bi-weekly')) {
                result = 13;
            } else if (frequency.equalsIgnoreCase('Weekly')) {
                result = 24;
            }
        }

        return result;
    }

    /**
     * [getLimitByFrequency description]
     * @param  frequency [description]
     * @return           [description]
     */
    public static Integer getLimitLowAndGrowByFrequency(String frequency) {

        Integer result = 0;

        if (!String.isEmpty(frequency)) {

            if (frequency.equalsIgnoreCase('Monthly')) {
                result = 6;
            } else if (frequency.equalsIgnoreCase('28 Days')) {
                result = 6;
            } else if (frequency.equalsIgnoreCase('Semi-Monthly')) {
                result = 12;
            } else if (frequency.equalsIgnoreCase('Bi-weekly')) {
                result = 12;
            } else if (frequency.equalsIgnoreCase('Weekly')) {
                result = 24;
            }
        }

        return result;
    }
}