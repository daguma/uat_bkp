global class ProxyApiBankInformation {

    public BankAccountGeneralInformation bankAccountGeneralInformation {get; set;}

    public BankStatementsPeriods bankStatementsPeriods {get; set;}

    public List<BankStatementsDetailsList> bankStatementsDetailsList {get; set;}

    global class BankAccountGeneralInformation {

        public Long yodleeItemAccountId {get; set;}

        public Long yodleeItemId {get; set;}
        public String email {get; set;}
        public String accountName {get; set;}
        public String accountAddress {get; set;}
        public String accountType {get; set;}
        public String accountNumber {get; set;}
        public String routingNumber {get; set;}
        public Decimal currentBalance {get; set;}
        public Integer overdraftProtection {get; set;}

    }

    global class BankStatementsPeriods {

        public String selectedP1 {get; set;}
        public String selectedP2 {get; set;}
        public String selectedP3 {get; set;}
        public String selectedP4 {get; set;}

    }

    global class BankStatementsDetailsList {

        public Integer id { get; set; }


        public String amtForP1 {get; set;}
        public String amtForP2 {get; set;}
        public String amtForP3 {get; set;}
        public String amtForP4 {get; set;}

        public String numForP1 {get; set;}
        public String numForP2 {get; set;}
        public String numForP3 {get; set;}
        public String numForP4 {get; set;}

    }

}