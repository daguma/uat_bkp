global class BatchAccountManagementJob implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts {
    global final String query;
    global final Account_Management_Settings__c reasonAcMa;
	
	global BatchAccountManagementJob() {
		
        this.reasonAcMa = [SELECT Id, Loan_Status__c, ChargeOff_Reason__c from Account_Management_Settings__c];
   		this.query = 'SELECT Id, Name, Lending_Account__r.Name,  Account_Management_Pull_Date__c, ' +
						'Lending_Account__c, Lending_Account__r.Charged_Off_Reason__c ' +
					 'FROM Opportunity ' +
					 'WHERE ((Lending_Account__r.CreatedDate  < LAST_N_DAYS:30 ' +
						'AND Account_Management_Pull_Date__c = NULL ) ' +
						'OR ( Account_Management_Pull_Date__c < LAST_N_DAYS:30 )) ' +
						'AND Lending_Account__r.loan__Loan_Status__c IN (' + this.reasonAcMa.Loan_Status__c + ') ' +
            			'AND Lending_Account__r.Charged_Off_Reason__c NOT IN (' + this.reasonAcMa.ChargeOff_Reason__c + ') ' +
					 'ORDER BY CreatedDate';
	}
    
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		List<String> conIds = new List<String>();

        for(sobject s : scope){
            Opportunity opp = (Opportunity) s;
            conIds.add(opp.Id);
        }
        
        if (conIds.size() > 0)
        	ProxyApiAccountManagement.getAccountManagementSoftPull(conIds);
    }

	global void finish(Database.BatchableContext BC) {
	}

	global void execute(SchedulableContext sc) {
	  	BatchAccountManagementJob job = new BatchAccountManagementJob();
		database.executebatch(job, 1);
	}

}

//System.schedule('Account Management Job', '0 0 14 * * ? *', new BatchAccountManagementJob());