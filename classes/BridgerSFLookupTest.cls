@isTest public class BridgerSFLookupTest {

    @isTest static void BridgerXMLwithRecords(){

        Contact con = LibraryTest.createContactTH();       
        BridgerSFLookup bridger = new BridgerSFLookup(con);

        String xmlResponse = bridger.connect(bridger.createRequest());

        bridger.getSoapRequestBody();

        String xml = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><SearchResponse xmlns="https://bridgerinsight.lexisnexis.com/BridgerInsight.Web.Services.Interfaces.11.0"><SearchResult><ClientReference>optional transaction id4</ClientReference><Records><ResultRecord><Record>1</Record><RecordDetails><AcceptListID>-1</AcceptListID><AccountAmount/><AccountGroupID/><AccountOtherData/><AccountProviderID/><AccountType/><AddedToAcceptList>false</AddedToAcceptList><AdditionalInfo><InputAdditionalInfo><Type>DOB</Type><Value>1983-09-XX</Value></InputAdditionalInfo></AdditionalInfo><Addresses><InputAddress><City>Oak Lawn</City><PostalCode>60453</PostalCode><StateProvinceDistrict>Illinois</StateProvinceDistrict><Street1>8820 Mobile Ave</Street1><Type>Current</Type></InputAddress></Addresses><Division>Default division</Division><DPPA>NA</DPPA><EFTContext/><EFTType>None</EFTType><EntityType>Individual</EntityType><Gender>None</Gender><GLB>255</GLB><IDs><InputID><Number>12345XXXX</Number><Type>SSN</Type></InputID></IDs><LastUpdatedDate>2018-10-31T16:34:51Z</LastUpdatedDate><Name><First>Viktor</First><Full>Viktor Bout</Full><Generation/><Last>Bout</Last><Middle/><Title/></Name><RecordState><AddedToAcceptList>false</AddedToAcceptList><AlertState>Open</AlertState><AssignedTo><string>Administrator</string></AssignedTo><AssignmentType>Role</AssignmentType><Division>Default division</Division><History><AuditItem><Date>2018-10-31T16:34:51Z</Date><Event>List Screening Search Conducted</Event><Note>List Screening Search Conducted</Note><User>Shylee Shunda</User></AuditItem><AuditItem><Date>2018-10-31T16:34:51Z</Date><Event>Alert Opened</Event><Note>Alert threshold met</Note><User>System</User></AuditItem><AuditItem><Date>2018-10-31T16:34:51Z</Date><Event>Record Created</Event><Note>Record Created</Note><User>Shylee Shunda</User></AuditItem></History><MatchStates><WatchlistMatchState><MatchID>5582154725</MatchID><Type>None</Type></WatchlistMatchState></MatchStates><Status/></RecordState><SearchDate>2018-10-31T16:34:51Z</SearchDate><Text/></RecordDetails><ResultID>4257530905</ResultID><RunID>84089215</RunID><Watchlist><Matches><WLMatch><AcceptListID>-1</AcceptListID><AddedToAcceptList>false</AddedToAcceptList><AddressName>false</AddressName><AutoFalsePositive>false</AutoFalsePositive><BestAddressIsPartial>false</BestAddressIsPartial><BestCountry/><BestCountryScore>-1</BestCountryScore><BestCountryType>None</BestCountryType><BestDOBIsPartial>false</BestDOBIsPartial><BestName>BOUT, Viktor Anatolijevitch</BestName><BestNameScore>96</BestNameScore><CheckSum>20026</CheckSum><Conflicts><AddressConflict>false</AddressConflict><CitizenshipConflict>false</CitizenshipConflict><CountryConflict>false</CountryConflict><DOBConflict>false</DOBConflict><EntityTypeConflict>false</EntityTypeConflict><GenderConflict>false</GenderConflict><IDConflict>false</IDConflict><PhoneConflict>false</PhoneConflict></Conflicts><EntityDetails><AdditionalInfo><EntityAdditionalInfo><ID>0</ID><Type>DOB</Type><Value>1967-01-XX</Value></EntityAdditionalInfo><EntityAdditionalInfo><ID>0</ID><Type>DOB</Type><Value>1970-01-XX</Value></EntityAdditionalInfo><EntityAdditionalInfo><ID>0</ID><Type>PlaceOfBirth</Type><Value>Dushanbe, Tajikistan</Value></EntityAdditionalInfo></AdditionalInfo><AKAs><EntityAKA><Category>Weak</Category><ID>0</ID><Name><Full>BONT</Full><Last>BONT</Last></Name><Type>AKA</Type></EntityAKA><EntityAKA><Category>Weak</Category><ID>0</ID><Name><Full>BOUTOV</Full><Last>BOUTOV</Last></Name><Type>AKA</Type></EntityAKA><EntityAKA><Category>Weak</Category><ID>0</ID><Name><Full>BUTT</Full><Last>BUTT</Last></Name><Type>AKA</Type></EntityAKA><EntityAKA><Category>Weak</Category><ID>0</ID><Name><Full>BUTTE</Full><Last>BUTTE</Last></Name><Type>AKA</Type></EntityAKA><EntityAKA><Category>Strong</Category><ID>0</ID><Name><First>Vitali</First><Full>Vitali SERGITOV</Full><Last>SERGITOV</Last></Name><Type>AKA</Type></EntityAKA></AKAs><Comments>Dealer and transporter of weapons and minerals | Owner, Great Lakes Business Company and Compagnie Aerienne des Grands | Program: DRCONGO</Comments><EntityType>Individual</EntityType><ListReferenceNumber>8279</ListReferenceNumber><Name><First>Viktor Anatolijevitch</First><Full>Viktor Anatolijevitch BOUT</Full><Last>BOUT</Last></Name><ReasonListed>DRCONGO</ReasonListed></EntityDetails><EntityName>BOUT, Viktor Anatolijevitch</EntityName><EntityScore>96</EntityScore><EntityUniqueID>LN0000428250</EntityUniqueID><FalsePositive>false</FalsePositive><File><Build>2018-10-25T14:06:53Z</Build><Custom>false</Custom><ID>301035</ID><Name>OFAC SDN.BDF</Name><Published>2018-10-25T10:51:01Z</Published><Type>BDF</Type></File><GatewayOFACScreeningIndicatorMatch>false</GatewayOFACScreeningIndicatorMatch><ID>5582154725</ID><MatchReAlert>false</MatchReAlert><PreviousResultID>-1</PreviousResultID><ReasonListed>DRCONGO</ReasonListed><ResultDate>2018-10-31T16:34:51.2996762Z</ResultDate><SecondaryOFACScreeningIndicatorMatch>false</SecondaryOFACScreeningIndicatorMatch><TrueMatch>false</TrueMatch></WLMatch></Matches><Status>Results</Status></Watchlist></ResultRecord></Records><SearchEngineVersion>5.15.913.0</SearchEngineVersion></SearchResult></SearchResponse></s:Body></s:Envelope>';
        bridger.parseDOC(xml);

        list<BridgerWDSFLookup__c> bridgerLookupList = new list<BridgerWDSFLookup__c>();

        for (BridgerWDSFLookup__c bridgerLookup : [SELECT ID
                                                    FROM BridgerWDSFLookup__c
                                                    WHERE Contact__c = : con.Id]){

            bridgerLookupList.add(bridgerLookup);
        }                                    
        //system.assertEquals( 1, bridgerLookupList.size());             
    }

    @isTest static void BridgerXMLwithNoRecords(){
        Contact con = LibraryTest.createContactTH();  
        BridgerSFLookup bridger = new BridgerSFLookup(con);

        bridger.getSoapRequestBody();
        String xml1 = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><SearchResponse xmlns="https://bridgerinsight.lexisnexis.com/BridgerInsight.Web.Services.Interfaces.11.0"><SearchResult><ClientReference>optional transaction id4</ClientReference><SearchEngineVersion>5.15.913.0</SearchEngineVersion></SearchResult></SearchResponse></s:Body></s:Envelope>';
        bridger.parseDOC(xml1);

        list<BridgerWDSFLookup__c> bridgerLookupList = new list<BridgerWDSFLookup__c>();

        for (BridgerWDSFLookup__c bridgerLookup : [SELECT ID
                                                    FROM BridgerWDSFLookup__c
                                                    WHERE Contact__c = : con.Id]){

            bridgerLookupList.add(bridgerLookup);
        } 
        bridger.bridgerError = '';                                 
        //system.assertEquals( 1, bridgerLookupList.size());                       
    }

    @isTest static void BridgerInvocableMethod(){
        
        Contact cont = Librarytest.createContactTH();
        
        BridgerWDSFLookup__c item = new BridgerWDSFLookup__c( Contact__c = cont.Id);
        insert item;
        
        BridgerSFLookup.callBridgerWhenChangeStatus(cont.Id);
        
        BridgerSFLookup.bridgerNewCall(item);
        
        
        
    }
    
    @isTest static void getComments(){
        
        Contact cont = Librarytest.createContactTH();
        
        BridgerSFLookup bridger = new BridgerSFLookup(cont);
        
        String xmlData =   '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><SearchResponse xmlns="https://bridgerinsight.lexisnexis.com/BridgerInsight.Web.Services.Interfaces.11.0"><SearchResult><ClientReference>optional transaction id4 </ClientReference><AdditionalInfo><EntityAdditionalInfo><Comments>Test</Comments></EntityAdditionalInfo></AdditionalInfo><SearchEngineVersion>5.15.913.0</SearchEngineVersion></SearchResult></SearchResponse></s:Body></s:Envelope>';
        
        DOM.Document doc = new DOM.Document();
		doc.load(xmlData);
		DOM.XmlNode root = doc.getRootElement();
        
        bridger.getCommentsFromXML(root);
        
    }

    @isTest static void BridgerreplaceValues(){
        Contact con = LibraryTest.createContactTH();  
        BridgerSFLookup bridger = new BridgerSFLookup(con);
        bridger.clientId = 'test';
        bridger.userId = 'test';
        bridger.passWord = 'test';

        bridger.replaceValues('Testing');

    }
    
    @isTest static void Bridger_Call_Error(){
         
         Contact con = LibraryTest.createContactTH();
         
         BridgerSFLookup bridger = new BridgerSFLookup(con);
         
         bridger.getSoapRequestBody();
         String xmlData = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><s:Fault><faultcode xmlns:a="https://bridgerinsight.lexisnexis.com/BridgerInsight.Web.Services.Interfaces.10.1">a:ServiceFaultFault</faultcode><faultstring ns0:lang="en-US" xmlns:ns0="http://www.w3.org/XML/1998/namespace">The following error occurred while processing your request. Please contact your system administrator. Invalid Credentials</faultstring><detail><ServiceFault xmlns="https://bridgerinsight.lexisnexis.com/BridgerInsight.Web.Services.Interfaces.10.1" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><Message>The following error occurred while processing your request. Please contact your system administrator. Invalid Credentials</Message><Type>InvalidCredentials</Type></ServiceFault></detail></s:Fault></s:Body></s:Envelope>'; 
         bridger.parseDOC(xmlData);

         list<BridgerWDSFLookup__c> bridgerLookupList = new list<BridgerWDSFLookup__c>();
         
         for (BridgerWDSFLookup__c bridgerLookup : [SELECT ID, List_Description__c
                                                    FROM BridgerWDSFLookup__c
                                                    WHERE Contact__c = : con.Id]){
                                                        
                                                        bridgerLookupList.add(bridgerLookup);
                                                    
                                                    }
         
         bridger.bridgerError = '';
         
         BridgerWDSFLookup__c br = [Select Id, List_Description__c 
                                    FROM BridgerWDSFLookup__c 
                                    WHERE Contact__c = : con.Id];
         
         system.assertEquals(br.List_Description__c , 'Bridger Call Error');
        
    }
    
}