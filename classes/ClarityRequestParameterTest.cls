@isTest
private class ClarityRequestParameterTest {
    
    @isTest static void sets_variables(){
        
        ClarityRequestParameter clarityRequest = new ClarityRequestParameter();
        
        clarityRequest.bank_account_number = '1234';
        clarityRequest.bank_account_type = 'ACH';
        clarityRequest.bank_routing_number = '123456789';
        clarityRequest.cell_phone = '555';
        clarityRequest.city = 'Auburn';
        clarityRequest.date_of_birth = String.valueOf(System.today().addYears(-35));
        clarityRequest.date_of_next_payday = String.valueOf(System.today().addDays(15));
        clarityRequest.drivers_license_number = '123';
        clarityRequest.drivers_license_state = 'OH';
        clarityRequest.email_address = 'test@mail.com';
        clarityRequest.employer_address = '123 test street';
        clarityRequest.employer_city = 'Atlanta';
        clarityRequest.employer_state = 'Georgia';
        clarityRequest.first_name = 'Test';
        clarityRequest.last_name = 'Name';
        clarityRequest.home_phone = '555';
        clarityRequest.housing_status = 'OK';
        clarityRequest.net_monthly_income = '4000';
        clarityRequest.occupation_type = 'Assistant';
        clarityRequest.pay_frequency = 'Monthly';
        clarityRequest.paycheck_direct_deposit = 'true';
        clarityRequest.reference_first_name = 'Wife';
        clarityRequest.reference_last_name = 'Name';
        clarityRequest.reference_phone = '555';
        clarityRequest.reference_relationship = 'Partner';
        clarityRequest.social_security_number = '45674433';
        clarityRequest.state = 'AL';
        clarityRequest.street_address_1 = 'Test street';
        clarityRequest.street_address_2 = 'Test street';
        clarityRequest.months_at_address = '40';
        clarityRequest.months_at_current_employer = '20';
        clarityRequest.work_fax_number = '555';
        clarityRequest.work_phone = '555';
        clarityRequest.zip_code = '35045';
         clarityRequest.annual_income = 5555;
        clarityRequest.entity_id = '12324234';
        
        System.assertEquals('Auburn', clarityRequest.city);
        System.assertEquals('45674433', clarityRequest.social_security_number);
    }
    
}