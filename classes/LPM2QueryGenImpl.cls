global class LPM2QueryGenImpl implements loan.M2QueryGen2 {
        private String allowabledLoanStatuses = '\'Active - Good Standing\'' + ',' + 
                                        '\'Active - Bad Standing\'' + ',' + 
                                        '\'Closed - Obligations Met\'' + ',' + 
                                        '\'Closed - Rescheduled\'' + ',' + 
                                        '\'Closed- Written Off\'' + ',' + 
                                        '\'Active - Matured\''+ ',' +
                                        '\'Closed - Sold Off\'' + ',' +
                                        '\'Closed - Settled Off\'';
        
        private String allowabledActiveLoanStatuses ='\'Active - Good Standing\'' + ',' + 
                                        '\'Active - Bad Standing\'' + ',' +                                                                      
                                        '\'Active - Matured\'';

        private String allowabledClosedLoanStatuses = '\'Closed - Rescheduled\'' + ',' + 
                                              '\'Closed - Obligations met\'';    
        private String allowabledWrittenOffLoanStatuses = '\'Closed- Written Off\'';
                                       
        private String allowabledSoldOffLoanStatuses = '\'Closed - Sold Off\'' + ',' +
                                        '\'Closed - Settled Off\''; 
                                          
        private String allowabledConditions ='\'Open Compliance Condition(METRO2)\'' + ',' + 
                                        '\'Open Consumer Information(METRO2)\'' + ',' +    
                                        '\'Open Special Condition(METRO2)\'' + ',' +                                                                      
                                        '\'Stop Credit Bureau\''; 

    global virtual String getLoanIdQuery() {
        return 'SELECT ID ' + getWhereQuery();
    }
    
    global String getWhereQuery() {
        Date SystemDate = new loan.GlobalLoanUtilFacade().getCurrentSystemDate(),  lastDayOfMonth = SystemDate.toStartOfMonth().addDays(Date.daysInMonth(SystemDate.toStartOfMonth().year(), SystemDate.toStartOfMonth().month()) - 1);
        if (SystemDate < lastDayOfMonth){
            SystemDate = lastDayOfMonth.toStartOfMonth().addDays(-1); 
        }    
        String dateStr =String.valueOf(SystemDate); 
        String reportingClosedDateStr =String.valueOf(SystemDate.addMonths(-2)), reportingWrittenOffDateStr =String.valueOf(SystemDate.addMonths(-6)), reportingSoldOffDateStr =String.valueOf(SystemDate.addMonths(-1)); //= String.valueOf(iDate);
        return  'FROM loan__Loan_Account__c ' +
                'WHERE ((loan__Loan_Status__c  IN (' + allowabledActiveLoanStatuses + ') ) '+ 
                '    OR (loan__Loan_Status__c IN ('+allowabledClosedLoanStatuses+') AND loan__closed_date__c >'+reportingClosedDateStr+')'+
                '    OR (loan__Loan_Status__c IN ('+allowabledWrittenOffLoanStatuses+') AND ((loan__Last_Payment_Date__c!= null AND loan__Last_Payment_Date__c  >'+reportingWrittenOffDateStr+') OR (loan__Charged_Off_Date__c!= null AND loan__Charged_Off_Date__c  >'+reportingWrittenOffDateStr+')) )'+
                '    OR (loan__Loan_Status__c IN ('+allowabledSoldOffLoanStatuses+') AND Charge_Off_Sold_On__c >'+reportingSoldOffDateStr+')'+
                '    OR (loan__Loan_Status__c IN ('+allowabledSoldOffLoanStatuses+') AND Contract_settled_off_date__c >'+reportingSoldOffDateStr+'))'+
                ' AND loan__Include_In_Metro2_File__c=true ' +  
                ' and loan__Metro2_Account_pmt_history_date__c < ' + dateStr + 
                ' AND loan__disbursal_Date__c <= ' + dateStr  ; 
    }
    
    global String getAccountQuery() {
        return 'loan__Account__c, ' +
               'loan__Account__r.Name, ' +
               'loan__Account__r.BillingStreet, ' +
               'loan__Account__r.BillingCity, ' +
               'loan__Account__r.BillingState, ' +
               'loan__Account__r.Phone, ' +
               'loan__Account__r.BillingPostalCode, ' +
               'loan__Account__r.BillingCountry ';
    }
    
    global String getContactQuery() {
        return 'loan__Contact__c, ' +
               'loan__Contact__r.lastName, ' +
               'loan__Contact__r.firstName, ' +
               'loan__Contact__r.Birthdate, ' +
               'loan__Contact__r.Phone, ' +
               'loan__Contact__r.MailingStreet, ' +
               'loan__Contact__r.MailingCity, ' +
               'loan__Contact__r.MailingState, ' +
               'loan__Contact__r.MailingPostalCode, ' +
               'loan__Contact__r.MailingCountry, ' +
               'loan__Contact__r.ints__Social_Security_Number__c' ;
    }
    
    private String getCoborrowerQuery() {
        String query = '(SELECT ';
        query += getAccountQuery() + ', ';
        query += getContactQuery();
        query += ' FROM loan__Coborrowers__r)';
        return query;
    }
    
    global virtual String getLoanQuery() {
    
        Date SystemDate = new loan.GlobalLoanUtilFacade().getCurrentSystemDate();
        Date lastDayOfMonth = SystemDate.toStartOfMonth().addDays(Date.daysInMonth(SystemDate.toStartOfMonth().year(), SystemDate.toStartOfMonth().month()) - 1);
        if (SystemDate < lastDayOfMonth){
            SystemDate = lastDayOfMonth.toStartOfMonth().addDays(-1); 
        }    
        String dateStr =String.valueOf(SystemDate); 
 
        return 'SELECT ID, ' +
                'Name, ' +
                'loan__Loan_Account_External_Id__c, ' +
                'loan__Frequency_of_Loan_Payment__c, ' +
                'loan__Term_Cur__c, ' +
                'Metro2_Ignore_Update__c,' +
                'Current_Term_Months__c, ' + 
                'loan__Pmt_Amt_Cur__c, ' +
                'loan__Payment_Frequency_Cycle__c, ' +
                'loan__Metro2_Account_Status_Code__c, ' +
                'loan__Metro2_CIIC_Date__c, ' +
                'loan__Metro2_CCCD_Date__c, ' +
                'loan__Metro2_Compliance_Condition_Code__c, ' +
                'loan__Metro2_Consumer_Information_Code__c, ' +
                'loan__Metro2_Account_Type_Code__c, ' +
                'loan__Metro2_Payment_Rating__c, ' +
                'loan__Metro2_Portfolio_Type__c, ' +
                'loan__Last_Payment_Amount__c, '+
                'loan__Interest_Rate__c, '+
                'loan__Principal_Remaining__c, '+
                'loan__Product_Type__c, '+
                'loan__Metro2_Payment_History__c, ' +
                'loan__Metro2_Special_Comment__c, ' +
                'loan__Metro2_Account_highest_bal_amount__c, ' +
                'loan__Metro2_Account_pmt_history_date__c, '+
                'loan__Metro2_First_Delinquency_Date__c , '+
                'loan__Loan_Amount__c, ' +
                'loan__closed_date__c, '+                
                'loan__Pay_Off_Amount_As_Of_Today__c, ' +
                'loan__Charged_Off_Fees__c, '+
                'loan__Charged_Off_Interest__c, '+
                'loan__Charged_Off_Principal__c, '+
                'loan__Last_Payment_Date__c , '+
                'loan__Amount_to_Current__c, ' +
                'loan__Charged_Off_Date__c, '+
                'loan__Time_Counting_Method__c, '+                
                'loan__Write_off_Tolerance_Amount__c, ' +
                'loan__Loan_Status__c, ' +
                'loan__Previous_Installment_Date__c, ' +
                'loan__Disbursal_Date__c, ' +
                'loan__Loan_Effective_Date__c, ' +
                'loan__Delinquent_Amount__c, ' +
                'loan__Oldest_Due_Date__c, ' +
                'loan__Expected_Disbursal_Date__c, ' +
                'loan__Delinquency_Grace_Days__c, ' +
                'SystemModStamp, ' +
                '(select Id,Name, ' +
                            ' loan__Fees__c, ' +
                            ' loan__Interest__c, ' +
                            ' loan__Principal__c, ' +
                            ' loan__Excess__c, ' +
                            ' loan__Cleared__c, ' +
                            ' loan__Reversed__c, ' +
                            ' loan__Transaction_Amount__c, ' +
                            ' loan__Transaction_Date__c, ' +
                            ' loan__Payment_Mode__c, ' +
                            ' loan__payment_mode__r.name, ' +
                            ' loan__Rejected__c, ' +
                            ' CreatedDate ' +
                        ' from loan__Loan_Payment_Transactions__r '+
                        ' where loan__Cleared__c=true '+
                        ' and loan__Reversed__c = false '+  
                        ' order by loan__Transaction_Date__c desc), '+   
             '(select Id, name,' +
                         'loan__Balance_Amount__c, ' +
                         'loan__Due_Amt__c, ' +
                         'loan__Due_Date__c, ' +
                         'loan__Due_Type_Description__c, ' +
                         'loan__Due_Type__c, ' +
                         'loan__Payment_Amt__c, ' +
                         'loan__DD_Primary_Flag__c , '+
                         'loan__Payment_Date__c, ' +
                         'loan__Payment_Satisfied__c, ' +
                         'loan__Tolerance__c, ' +
                         'loan__Transaction_Date__c ' +                            
                    'from loan__Dues_Details__r ' +
                   'where loan__DD_Primary_Flag__c = true ' +
                   'order by loan__Due_Date__c desc), ' +                        
                 '(select Id,Name, ' +
                            ' loan__Action__c, ' +
                            ' loan__End_Date__c, ' +
                            ' loan__Reason_Code__c, ' +
                            ' loan__Start_Date__c, ' +
                            ' loan__Result__c, ' +
                            ' loan__Reason_Code__r.id '+
                        ' from loan__Loan_Account_Conditions__r '+
                        ' where loan__Result__c in (' +allowabledConditions +') '+
                        ' and loan__start_Date__c <= ' + dateStr +   
                        ' and (loan__end_Date__c = null OR loan__end_Date__c > ' + dateStr + ') '+             
                        ' and loan__Enabled_Flag__c = true '+   
                        ' order by loan__Result__c,loan__Start_Date__c), '+                                           
                getAccountQuery() + ' , ' +
                getContactQuery() + ' , ' +
                getCoborrowerQuery() + ' ' +
                getWhereQuery(); 
    }
    public static string getMetroQuery(){
          loan.M2QueryGen queryGen = null;
        loan__Metro2_Parameters__c m2Params = loan__Metro2_Parameters__c.getInstance();
        if (m2Params == null) m2Params = loan__Metro2_Parameters__c.getOrgDefaults(); // defaulting
        if (m2Params.loan__Metro2_Query_Class__c != null) {
            Type t = Type.forName(null, m2Params.loan__Metro2_Query_Class__c);
            if (t == null) t = Type.forName('loan', m2Params.loan__Metro2_Query_Class__c);
            queryGen = (loan.M2QueryGen) t.newInstance();
        }
        return queryGen == null ? '' : queryGen.getLoanQuery();
    }
 
    
    Webservice static Boolean isBatchRunning(String ids){
        List<AsyncApexJob> jbs = [SELECT Id FROM AsyncApexJob WHERE ApexClassID =: ids and Status in ('Processing', 'Queued','Preparing','Holding') and CreatedDate >= LAST_N_DAYS:3];
        return jbs.size()  != 0;
    }
    
    Webservice static  Integer recordsLeft() {
        
        String query= getMetroQuery();
        List<SObject> rows =  (query.length() == 0) ? null : Database.query(query);
        return  (query.length() == 0) ? 0 : rows.size();
    }
    
}