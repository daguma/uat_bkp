global class ClarityClearFraudIdentityVerification {

    public String overall_match_result{get;set;}
    public String ssn_name_match{get;set;}
    public String name_address_match{get;set;}
    public String ssn_Dob_Match{get;set;}
    public String overall_match_reason_code{get;set;}
}