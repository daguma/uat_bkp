@IsTest
public class TestFundingAutomation  {
     static testMethod void FundingAutomation()
    {  
        Try
        {        
          
            //Dummy Contact
            contact cntct= new contact();
            cntct.Lastname='Test1';
            cntct.Annual_Income__c =10000;
            insert cntct;
            genesis__Applications__c aplcn= testhelper.createApplication();
            aplcn.DummyExpected_DisbursalDate__c =  Datetime.newInstance(2016, 05, 14, 18, 57, 00);
            system.debug (' aplcn.DummyExpected_DisbursalDate__c' +  aplcn.DummyExpected_DisbursalDate__c);
            Map<String,String> info = new Map<String,String>();
            info.put('genesis__Applications__c',String.valueOf(aplcn.Id));
            test.startTest();
            aplcn.genesis__Contact__c= cntct.id;
            aplcn.genesis__Status__c = 'Credit review';
            update aplcn;
            aplcn.genesis__Status__c = 'Funded';
            update aplcn;    
            test.stoptest(); 
        }
        catch (Exception e) {}
        }
          static testmethod void FundingAutomation3()
    {  
        Try
        {          
           test.starttest();
           contact cntct3= new contact();
           cntct3.Lastname='Test2';
           cntct3.Annual_Income__c =10000;
           insert cntct3;    
            //Dummy Application
            genesis__Applications__c aplcn3= testhelper.createApplication();
            aplcn3.DummyExpected_DisbursalDate__c =  Datetime.newInstance(2016, 05, 11, 18, 57, 00);  
            Map<String,String> info3 = new Map<String,String>();
            info3.put('genesis__Applications__c',String.valueOf(aplcn3.Id));   
            aplcn3.genesis__Contact__c= cntct3.id;         
            aplcn3.genesis__Status__c = 'Contract Pkg Approved';
            update aplcn3; 
            aplcn3.genesis__Status__c = 'Funded';
            update aplcn3;
            loan__Loan_Account__c  ln= TestHelper.createContract();
            aplcn3.Lending_Account__c= ln.id;
            update aplcn3;
            test.stopTest();
        }
        catch (Exception e) {}
        }
          static testmethod void FundingAutomation2()
    {  
        Try
        {        
            test.starttest();    
            contact cntct2= new contact();
            cntct2.Lastname='Test3';
            cntct2.Annual_Income__c =10000;
            insert cntct2;    
             //Dummy Application
            genesis__Applications__c aplcn2= testhelper.createApplication();
            aplcn2.DummyExpected_DisbursalDate__c =  Datetime.newInstance(2016, 05, 15, 02, 57, 00);    
            Map<String,String> info2 = new Map<String,String>();
            info2.put('genesis__Applications__c',String.valueOf(aplcn2.Id));
            aplcn2.genesis__Contact__c= cntct2.id;
            aplcn2.genesis__Status__c = 'Credit Review';
            update aplcn2;
            aplcn2.genesis__Status__c = 'Funded';
            update aplcn2;
            test.stopTest();
        }
        catch (Exception e) {}
    }
    
    
  static testmethod void FundingAutomation1()
    {  
        Try
        {  
        test.starttest();       
          contact cntct1= new contact();
          cntct1.Lastname='Test1';
          cntct1.Annual_Income__c =10000;
          insert cntct1;    
            //Dummy Application
          genesis__Applications__c aplcn1= testhelper.createApplication();
          aplcn1.DummyExpected_DisbursalDate__c =  Datetime.newInstance(2016, 05, 11, 18, 57, 00);      
          Map<String,String> info1 = new Map<String,String>();
          info1.put('genesis__Applications__c',String.valueOf(aplcn1.Id));
          aplcn1.genesis__Status__c = 'Credit Review';
          aplcn1.genesis__Contact__c= cntct1.id;
          update aplcn1; 
          aplcn1.genesis__Status__c = 'Funded';
          update aplcn1;
          test.stopTest();
        }
        catch (Exception e) {}
    }
    
    static testmethod void FundingAutomation4()
    {  
        Try
        {       
        test.starttest();  
         contact cntct1= new contact();
            cntct1.Lastname='Test1';
            cntct1.Annual_Income__c =10000;
            insert cntct1;    
            //Dummy Application
            genesis__Applications__c aplcn1= testhelper.createApplication();
            aplcn1.DummyExpected_DisbursalDate__c =  Datetime.newInstance(2016, 07, 7, 10, 57, 00);  
            Map<String,String> info1 = new Map<String,String>();
            info1.put('genesis__Applications__c',String.valueOf(aplcn1.Id));
            aplcn1.genesis__Status__c = 'Credit Review';
            aplcn1.genesis__Contact__c= cntct1.id;
            update aplcn1; 
            aplcn1.genesis__Status__c = 'Funded';
            update aplcn1; 
            test.stopTest();
        }
        catch (Exception e) {}
    }
    
       static testmethod void FundingAutomation5()
    {  
        Try
        {       
            test.starttest();  
            contact cntct1= new contact();
            cntct1.Lastname='Test1';
            cntct1.Annual_Income__c =10000;
            insert cntct1;    
            genesis__Applications__c aplcn1= testhelper.createApplication();
            aplcn1.DummyExpected_DisbursalDate__c =  Datetime.newInstance(2016, 07, 2, 22, 57, 00); 
            aplcn1.genesis__Contact__c= cntct1.id; 
            aplcn1.genesis__Status__c = 'Credit Review';
            update aplcn1; 
            aplcn1.genesis__Status__c = 'Funded';
            update aplcn1; 
            loan__Loan_Account__c  ln= TestHelper.createContract();
            aplcn1.Lending_Account__c= ln.id;
            update aplcn1;
            test.stopTest();
        }
        catch (Exception e) {}
    }
    static testmethod void contractcreation()
    {  
     
            contact cntct1= new contact();
            cntct1.Lastname='Test1';
            cntct1.Annual_Income__c =10000;
            insert cntct1;  
            loan__Loan_Account__c  ln= TestHelper.createContract();
            system.debug('===Loan Account==='+ln);  
            genesis__Applications__c aplcn1= [Select Id, DummyExpected_DisbursalDate__c, genesis__Contact__c, genesis__Status__c, Lending_Account__c, FS_Assigned_To_Val__c  from genesis__Applications__c where Id =: ln.Application__c];
            aplcn1.DummyExpected_DisbursalDate__c =  Datetime.newInstance(2016, 07, 2, 22, 57, 00); 
            aplcn1.genesis__Contact__c= cntct1.id; 
            aplcn1.genesis__Status__c = 'Credit Review';
            update aplcn1; 
            aplcn1.genesis__Status__c = 'Funded';
            update aplcn1; 
            aplcn1.Lending_Account__c= ln.id;
            aplcn1.FS_Assigned_To_Val__c='TestingJas';
            update aplcn1;
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'standt', Email='jaspinder.kaur@trantorinc.com', 
            EmailEncodingKey='UTF-8', LastName='TestingJas', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='jaspinder.kaur@trantorinc.com.call');
            insert u;
            system.debug('===user=='+u);
            date dummyDate= system.today();
            CallContract.ContractCreation(aplcn1.id, string.valueOf(dummyDate));
      
    }
    
    
            

}