@isTest
private class UniversalNotesCtrlTest {
    @isTest static void gets_universal_notes() {
        Opportunity app = LibraryTest.createApplicationTH();

        Note noteDetails = new Note();
        noteDetails.ParentId = app.id;
        noteDetails.body = 'UniversalNotesCtrlTest at ' + Datetime.now().format('hh:mm:ss') + ' on ' + Date.today().format();
        noteDetails.Title = 'UniversalNotesCtrlTest';
        insert noteDetails;

        String html = UniversalNotesCtrl.getAllNotes(app.Contact__c);

        System.assert(html.contains('UniversalNotesCtrlTest'), html);
    }

    @isTest static void gets_member_services_notes() {

        Opportunity app = LibraryTest.createApplicationTH();
        
        loan__Loan_Account__c cnt = LibraryTest.createContractTH();

        Note noteDetails = new Note();
        noteDetails.ParentId = app.Contact__c;
        noteDetails.body = 'UniversalNotesCtrlTest at ' + Datetime.now().format('hh:mm:ss') + ' on ' + Date.today().format();
        noteDetails.Title = 'UniversalNotesCtrlTest';
        insert noteDetails;

        Member_Services_Notes__c memberServices = new Member_Services_Notes__c();
        memberServices.Body__c = 'Test Body';
        memberServices.Title__c = 'Test Member Services';
        memberServices.Contact__c = app.Contact__c;
        memberServices.CL_Contract__c = cnt.Id;
        insert memberServices;

        Note noteContact = new Note();
        noteContact.ParentId = app.Contact__c;
        noteContact.Body = 'Test';
        noteContact.Title = 'Test Contact';
        insert noteContact;

        String html = UniversalNotesCtrl.getAllNotes(app.Id);

        System.assert(html.contains('Test Member Services'), html);
        System.assert(html.contains('Test Contact'), html);
        
        UniversalNotesCtrl.getRelatedNotes(app.Contact__c);
        
    }

}