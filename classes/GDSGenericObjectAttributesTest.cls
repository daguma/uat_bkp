@isTest
private class GDSGenericObjectAttributesTest {
	
    @isTest static void initializes_gds_generic_object_attributes(){
        GDSGenericObjectAttributes gdsGeneric = new GDSGenericObjectAttributes();
    }
    
    @isTest static void gets_gds_object_attributes(){
        GDSGenericObjectAttributes.GDSGenericCallResponse gdsCallResponse = new GDSGenericObjectAttributes.GDSGenericCallResponse();
        gdsCallResponse.http_status_code = '201';
		gdsCallResponse.b64Response = 'Test';
		gdsCallResponse.isValidResponse = 'Test';
        gdsCallResponse.errorMessage = 'Test';
		gdsCallResponse.errorDetails = 'Test';
		gdsCallResponse.fullErrorMessage = 'Test';
    }
    
    @isTest static void gets_gds_error(){
        GDSGenericObjectAttributes.Error gdsGenericError = new GDSGenericObjectAttributes.Error();
        gdsGenericError.errorMessage = 'Test';
		gdsGenericError.errorDetails = 'Test';
    }
    
}