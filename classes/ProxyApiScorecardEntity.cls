global class ProxyApiScorecardEntity {
    public Decimal annualIncome {get; set;}
    public Decimal dtiRatio {get; set;}
    public Decimal selfEmployed {get; set;}
    public Decimal creditScore {get; set;}
    public Decimal inquiries {get; set;}
    public Decimal totalTradeLines {get; set;}
    public Decimal highestBalance {get; set;}
    public Decimal totalOpenInstallments {get; set;}
    public Decimal logit {get; set;}
    public Decimal unitBadProb {get; set;}
    public Decimal dollarRateBadProb {get; set;}
    public Decimal maximumPaymentAmount {get; set;}
    public String grade {get; set;}
    public String acceptance {get; set;}
    public Boolean highRisk {get; set;}
}