global with sharing class MelisaCtrl {
    public static string Respns{get;set;}
    public static XmlStreamReader reader{get;set;}
    public static string FN , LN, Street, City, State, ZipVar, Country, email, income;
    public static string employer, jobtitle, TextFieldName, EmailAcceptanceVar,IncomeAcceptanceVar,  EMAIL_IGNORE = 'no@lendingpoint.com';   
    
    public static Contact c;
    public static Lead l;
    
    
    public static void SendRequest(){
        //For testing
        
        Respns = FactorTrust.FactorTrust('00QJ0000009S7pN','003J000001ARt9H');
        
        Map<string,string> testmap = new map<string,string>();  
        testmap.put('leadId','00QJ0000009S7pN');
        testmap.put('contactId',null);               
        string test = JSON.SerializePretty(testmap);
        
    }
    
    @future (callout=true)
    public static void notifySuperMoney(List<Id> pAppIds){
        try {
            LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
            string Url = cuSettings.Super_Money_URL__c, Respns1;
            
            
            http httpProtocol = new http();
            HttpResponse Resp;
            
            for (Opportunity co : [Select Id, Point_Code__c, Lead_Sub_Source__c from Opportunity where Id in : pAppIds]){
                if (co.Lead_Sub_Source__c == cuSettings.Super_Money_Sub_Source__c) {
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(Url + co.Point_Code__c);
                    req.setMethod('GET');
                    
                    if(!Test.isRunningTest()){
                        Resp = httpProtocol.send(Req); 
                    }else{
                        Resp = new HTTPResponse();                    
                    }
                }
                
                
            }
        } catch (Exception e) {
            WebToSFDC.notifyDev('notifySuperMoney Error', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() ) ;
            
        }
        
    }
    
    
    WebService static string DymistNew(string PersonalInfoString){
        
        //variables declaration
        Map<string,string> ResponseMap = new Map<string,string>();           
        string AcceptanceVar,EmpAcceptanceVar, leadId, contactId, WebServicename = 'Demyst', field, phon, request, response, status = '200';
        
        try{
            Map<string,string> PersonInfoMap = (Map<String,String>) JSON.deserialize(PersonalInfoString, Map<String,String>.class); 
            
            if(PersonInfoMap.ContainsKey('leadId')){leadId = (PersonInfoMap.get('leadId') <> null) ?EncodingUtil.urlEncode(PersonInfoMap.get('leadId'), 'UTF-8'): null;}
            if(PersonInfoMap.ContainsKey('contactId')){contactId = (PersonInfoMap.get('contactId') <> null) ?EncodingUtil.urlEncode(PersonInfoMap.get('contactId'), 'UTF-8'): null;}
            
            if(String.isNotEmpty(leadId) ){
                l = [select Phone,id,Factor_Trust_Verification__c,Demyst_Income_Verification__c,Demyst_Employer_Verification__c,Demyst_Email_Verification__c,Demyst_Email_Acceptance__c,Factor_Trust_Acceptance__c,Demyst_Employer_Acceptance__c,Demyst_Income_Acceptance__c, firstName, lastName, Street,State,City,IsConverted,PostalCode,Country,Email, Annual_Income__c, Employer_Name__c, Title from Lead where id=: leadId];
            }
            
            if(String.isNotEmpty(contactId) ){
                c  = [select Phone,id,Factor_Trust_Verification__c,Demyst_Income_Verification__c,Demyst_Employer_Verification__c,Demyst_Email_Verification__c,Factor_Trust_Acceptance__c,Demyst_Email_Acceptance__c,Demyst_Employer_Acceptance__c,Demyst_Income_Acceptance__c, firstName, lastName, MailingStreet,MailingState,MailingCity,MailingPostalCode,MailingCountry,Email, Annual_Income__c, Employer_Name__c, Title from Contact where id=: contactId];              
            } 
            
            //Called method to verify data from demyst service
            string DemystRes= DemystUtil.DemystService(PersonalInfoString);
            system.debug('===DemystRes=='+DemystRes);
            Map<string,string> ResMap = (Map<String,String>) JSON.deserialize(DemystRes, Map<String,String>.class);
            system.debug('===ResMap===='+ResMap );
            
            request= ResMap.containsKey('request')?ResMap.get('resquest'):null;
            response= ResMap.containsKey('response')?ResMap.get('response'):null;
            EmailAcceptanceVar = ResMap.containsKey('EmailAcceptanceVar')?ResMap.get('EmailAcceptanceVar'):null;
            IncomeAcceptanceVar = ResMap.containsKey('IncomeAcceptanceVar')?ResMap.get('IncomeAcceptanceVar'):null;
            EmpAcceptanceVar = ResMap.containsKey('EmpAcceptanceVar')?ResMap.get('EmpAcceptanceVar'):null;
            AcceptanceVar = ResMap.containsKey('AcceptanceVar')?ResMap.get('AcceptanceVar'):null;
            //Called method to update verification result on lead and contact object
            DemystUtil.updateResult(l,c,ResMap);
            
            DoLogging(WebServicename,request,response, (l != null && !l.IsConverted ) ? leadId : null,contactId);                                
            
            ResponseMap.put('Acceptance', AcceptanceVar);
            ResponseMap.put('message','success');
            System.debug('============ResponseMap============================='+ResponseMap); 
            
        }catch(Exception Exc){
            system.debug('===Exception==='+Exc.getMessage()); 
            system.debug('===Exception==='+Exc.getLineNumber());   
            
            string EmailReslt  = FactorTrust.getConfigResult('email','AllowProceed'),  IncomeReslt = FactorTrust.getConfigResult('income','AllowProceed');
            string Acc = (EmailReslt == 'N' || IncomeReslt == 'N')? 'N' : 'Y';               
            ResponseMap.put('Acceptance', Acc);
            status = 'error'; 
            ResponseMap.put('message', Exc.getMessage());                                                                                  
        }  
        ResponseMap.put('status',status);
        return JSON.SerializePretty(ResponseMap);            
        
    }   
    
    public static void DoLogging(string webServiceName, string Request, string Response, string LeadIdd, string ContactIdd){
        Logging__c newLog = new Logging__c(Webservice__c = webServiceName,  API_Request__c = Request, API_Response__c = Response);
        newLog.Webservice__c = webServiceName;
        if(String.isNotEmpty(LeadIdd) && (![select IsConverted from Lead where id=: LeadIdd].IsConverted)){
            newLog.Lead__c    = LeadIdd;
        }       
        if(String.isNotEmpty(ContactIdd)){newLog.Contact__c = ContactIdd;}
        insert newLog;
    }
    
    
    
    //USPS Integration
    WebService static string USPS(string Zip){
        Map<string,string> ReturnMap = new Map<string,string>();
        String PrevEvent;                
        HttpResponse Resp;
        
        if (Zip != null && Zip.isNumeric()) {
            try{                
                String xmlString = '<CityStateLookupRequest%20USERID=\''+Label.USPS_USERID+'\'><ZipCode%20ID=\'0\'><Zip5>'+Zip+'</Zip5></ZipCode></CityStateLookupRequest>';      
                HTTPRequest Req = new httpRequest();
                Req.SetMethod('GET');               
                Req.SetEndPoint('http://production.shippingapis.com/ShippingAPITest.dll?API=CityStateLookup&XML='+xmlString);  
                //Req.SetEndPoint('http://stg-production.shippingapis.com/ShippingAPI.dll?API=CityStateLookup&XML='+xmlString);     
                
                http httpProtocol = new http();
                
                
                if(!Test.isRunningTest()){
                    Resp = httpProtocol.send(Req); 
                }else{
                    Resp = new HTTPResponse();                    
                    Resp.SetBody(TestHelper.createDomDoc().toXmlString());               
                }
                
                reader = Resp.getXmlStreamReader();
               
                //Create Logging Record
                DoLogging('USPS',String.valueOf(Req),Resp.Getbody(),null,null);
                
                while(reader.hasNext()) {
                    
                    if(PrevEvent == 'City'  && (reader.getEventType() == XmlTag.CHARACTERS)){
                        ReturnMap.put('city',reader.GetText());           
                        
                    }else if(PrevEvent == 'State'  && (reader.getEventType() == XmlTag.CHARACTERS)){
                        ReturnMap.put('state',reader.GetText());           
                    }
                    
                    if (reader.getEventType() == XmlTag.START_ELEMENT) {              
                        PrevEvent = reader.getLocalName();
                    }
                    
                    reader.next();
                }
                
                if(ReturnMap.size() == 0){
                    ReturnMap.put('status','error');
                    ReturnMap.put('message','No match found');
                }else{
                    ReturnMap.put('status','200');
                }
            }Catch(Exception exc){              
                ReturnMap.put('status','error');
                ReturnMap.put('message',exc.getMessage()); 
            }
        } else {
            ReturnMap.put('status','error');
            ReturnMap.put('message','No match found');

        }
        return JSON.SerializePretty(ReturnMap);              
    }
    
    //*****************************************
    //*To update result on Lead and Contact
    //*****************************************
    public static void updateResult(string FieldName,Lead leadRow, Contact contactRow, string Acceptance){
        boolean result = Acceptance == 'Y';
        string Textresult = result ? 'Pass': 'Fail'; 
        
        if(contactRow <> null){               
            contactRow.put(FieldName,result);
            if(TextFieldName <> null){ contactRow.put(TextFieldName,Textresult); }                                       
            update contactRow;
        }
        
        if(leadRow <> null){
            if(!leadRow.IsConverted){
                leadRow.put(FieldName,result);
                if(TextFieldName <> null){ leadRow.put(TextFieldName,Textresult);}
                update leadRow;
            }
        }     
        
    }
    //==MAINT-152==Start==
    @InvocableMethod(label='GetMaximumOfferAmount')
    public static void getMaximumOfferAmount(List<ID> oppIds){
        if(!System.isFuture() && !System.isBatch()) {
            MelisaCtrl.internalMethodToGetAmount(oppIds);
        }   
        
    }
    
    public static void internalMethodToGetAmountNoF(List<ID> oppIds){
        
        List<opportunity> oppListToUpdate= new list<opportunity>();
        Map<id, decimal> appMap= new Map<id, decimal>();
        Map<id, decimal> appMaxFundAmtMap= new Map<id, decimal>();
        set<id> appIds= new set<id>();
        system.debug('==oppIds=='+oppIds);
        try {
            if(!oppIds.isEmpty()){ 
                appIds.addAll(oppIds);
                //MER-133
                List<offer__C> offList= [select id, opportunity__c,Max_Funding_Amount__c, opportunity__r.Maximum_Offer_Amount__c, Total_Loan_Amount__c, Loan_Amount__c, opportunity__r.Fee_Handling__c  from offer__c where opportunity__c IN: appIds];
                for(Offer__c offr : offList){
                    system.debug('==Offer=='+ offr);
                    
                    decimal maxOfferAmount;
                    if(offr.opportunity__r.Fee_Handling__c == null || offr.opportunity__r.Fee_Handling__c == FeeUtil.NET_FEE_OUT){
                        maxOfferAmount= offr.Total_Loan_Amount__c;  
                    }else{    
                        maxOfferAmount= offr.Loan_Amount__c;  
                        
                    }
                    if(appMap.containsKey(offr.opportunity__c) && appMap.get(offr.opportunity__c) > maxOfferAmount){
                        maxOfferAmount = appMap.get(offr.opportunity__c);     
                    }

                    appMap.put(offr.opportunity__c,maxOfferAmount);  
                    appMaxFundAmtMap = returnMaxLoanAmount(oppIds, offList);  
                    system.debug('===Final appMap=='+appMap);
                } 
                
                for(id opprId: appMap.keySet()){
                    opportunity opp= new opportunity(id=opprId, Maximum_Offer_Amount__c=appMap.get(opprId), Maximum_Loan_Amount__c =appMaxFundAmtMap.get(opprId)); 
                    oppListToUpdate.add(opp);    
                }
                system.debug('===oppListToUpdate=='+oppListToUpdate);
                if(!oppListToUpdate.isEmpty())
                    update oppListToUpdate;
            }   
        }
        Catch(Exception exp) {
            System.debug('exception:' + exp.getMessage());
            WebToSFDC.notifyDev('getMaximumOfferAmount ERROR', 'ERROR = ' + exp.getMessage() + '\n Line = ' + exp.getLineNumber() + '\n ' + exp.getStackTraceString());
        }
    }
    public static map<id,decimal> returnMaxLoanAmount(List<id> oppList, List<offer__C> offerList){
         Map<id, decimal> appMaxFundAmtMap= new Map<id, decimal>();
         List <offer__c> offList= new list<offer__C>();
         if(!oppList.isEmpty()){ 
             if(!offerList.isEmpty()){
                 offList = offerList;       
             }else{
                 offList = [select id, opportunity__c,Max_Funding_Amount__c, opportunity__r.Maximum_Offer_Amount__c, Total_Loan_Amount__c, Loan_Amount__c, opportunity__r.Fee_Handling__c  from offer__c where opportunity__c IN: oppList];
             
             }
         if(!offList.isEmpty()){
             for(Offer__c offr : offList) {
                     decimal maxFundingAmount = offr.Max_Funding_Amount__c; 
                     if(appMaxFundAmtMap.containsKey(offr.opportunity__c) && appMaxFundAmtMap.get(offr.opportunity__c) > maxFundingAmount){
                        maxFundingAmount = appMaxFundAmtMap.get(offr.opportunity__c);
                        
                    }  
                    appMaxFundAmtMap.put(offr.opportunity__c,maxFundingAmount);  
                    
                 }
             }
         }
         return appMaxFundAmtMap;
    }
    
    @future
    public static void internalMethodToGetAmount(List<ID> oppIds){
        internalMethodToGetAmountNoF(oppIds);
    }
    
}