@isTest
public class TestUpdateHighestBalanceAmtJob {
    
    @testSetup
    public static void testSetUp(){
        loan.TestHelper.systemDate = Date.newInstance(2015, 3, 01);
        loan.TestHelper.createSeedDataForTesting();
        
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Set the Fiscal Year days
        loan.TestHelper.createDayProcessForFullYear(loan.TestHelper.SystemDate);
        
        //Client__c dummyClient = TestHelper.createClient(dummyOffice);
        
        //Create a dummy Loan Product
        
        loan__Metro2_Parameters__c m2Params = loan.CustomSettingsUtil.getMetro2Parameters();
        m2Params.loan__Identification_Number__c='CLOUDLENDINGINCCT';
        m2Params.loan__Experian_Identifier__c='DAZVD';
        m2Params.loan__Program_Date__c=Date.newInstance(2008, 5, 12);
        m2Params.loan__Program_Revision_Date__c=Date.newInstance(2008, 5, 12);
        m2Params.loan__Reporter_Name__c='CLOUD LENDING INC';
        m2Params.loan__Reporter_Phone__c='4129733110';
        m2Params.loan__Reporter_Address__c='SAN MATEO';
        m2Params.loan__Borrower_Info_Class__c = 'loan.M2BorrowerInfoSampleClass2';
        m2Params.loan__Original_Creditor_Name__c='CLOUD LENDING INC';
        m2Params.loan__Borrower_Employment_Info_Class__c ='loan.M2CRBorrowerEmploymentInfoSample';
        m2Params.loan__include_K1__c = false;
        m2Params.loan__include_N1__c = false;
        m2Params.loan__Metro2_Query_Class__c = 'loan.M2QueryGenImpl';
        upsert m2Params;
        
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct('LOC Product',
                            dummyOffice,
                            dummyAccount,
                            curr,
                            dummyFeeSet,
                            'Interest Only',
                            12,
                            12,
                            null);
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();        
        
        loan__Payment_Mode__c pMode = [SELECT Id 
                                       FROM loan__Payment_Mode__c 
                                       LIMIT 1
                                      ];
        
        Contact dummyContact = new Contact();
        dummyContact.FirstName = 'FN';
        dummyContact.LastName = 'FN';
        insert dummyContact;
        
        Test.startTest();
        loan__Loan_Account__c dummyLoanAccount = loan.TestHelper2.createLOC(dummyLP,
                                          10000.00,
                                          dummyContact,
                                          null,
                                          dummyFeeSet,
                                          dummyLoanPurpose,
                                          dummyOffice,
                                          5,
                                          12,
                                          'Interest Only',
                                          100,
                                          'Principal Plus Interest',
                                          100,
                                          'Monthly',
                                          'Declining Balance',
                                          loan.TestHelper.systemDate.addDays(30),
                                          25
                                          );
        
        loan__Loan_Disbursal_Transaction__c distxn = new loan__Loan_Disbursal_Transaction__c();
        distxn.loan__Loan_Account__c = dummyLoanAccount.Id;
        distxn.loan__Mode_of_Payment__c = pMode.Id;
        distxn.loan__Disbursed_Amt__c = 1000;
        loan.LoanDisbursalActionAPI a = new loan.LoanDisbursalActionAPI(distxn);
        a.disburseLoanAccount();       
    }
    
    @isTest
    public static void testHighestBalance() {
        Date systemDate = loan.TestHelper.systemDate;
        loan__Loan_Account__c loanAccount = [SELECT Id
                                             FROM loan__Loan_Account__C
                                            ];
        
        loan__Payment_Mode__c pMode = [SELECT ID
                                       FROM loan__Payment_Mode__c
                                       LIMIT 1
                                      ];
        
        loan__Loan_Payment_Transaction__c loanPayment = new loan__Loan_Payment_Transaction__c();
        //loanPayment.loan__Transaction_Date__c = systemDate;
        loanPayment.loan__Transaction_Amount__c = 200;
        loanPayment.loan__Loan_Account__c = loanAccount.Id;
        loanPayment.loan__Payment_Mode__c = pMode.id;
        insert loanPayment;
        
        loan__Loan_Disbursal_Transaction__c distxn = new loan__Loan_Disbursal_Transaction__c();
        distxn.loan__Loan_Account__c = loanAccount.Id;
        distxn.loan__Mode_of_Payment__c = pMode.Id;
        distxn.loan__Disbursed_Amt__c = 1000;
        loan.LoanDisbursalActionAPI a = new loan.LoanDisbursalActionAPI(distxn);
        a.disburseLoanAccount();
        
        loanAccount = [SELECT Id,
                              loan__Metro2_Account_highest_bal_amount__c
                       FROM loan__Loan_Account__C
                      ];
        loanAccount.loan__Metro2_Account_highest_bal_amount__c = 100;
        update loanAccount;
        
        Test.startTest();
        
        UpdateHighestBalanceAmtJob job = new UpdateHighestBalanceAmtJob();
        Database.executeBatch(job);
        
        Test.stopTest();
        
        loanAccount = [SELECT Id,
                              loan__Metro2_Account_highest_bal_amount__c
                       FROM loan__Loan_Account__C
                      ]; 
        system.assertEquals(1800, loanAccount.loan__Metro2_Account_highest_bal_amount__c, 'Highest Balance value is wrong');
    }
}