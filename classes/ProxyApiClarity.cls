public class ProxyApiClarity {

    private static final String GET_CLARITY_INQUIRIES = '/v101/clarity/get-inquiries/';
    private static final String GET_CLARITY_INCOME = '/v101/clarity/get-income-inquiries/';
    
    public static ProxyApiClarityInquiryEntity getInquiries(String inputData) {

        ProxyApiClarityInquiryEntity result = null;

        Boolean validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();

        if (validatedAPI) {
            String body = 'input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8');

            String jsonResponse = null;

            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeProxyApiClarityInquiryEntity();
            } else {
                jsonResponse = ProxyApiUtil.ApiGetResult(GET_CLARITY_INQUIRIES, body);
            }

            System.debug('Json Response' + jsonResponse);

            if (jsonResponse != null) {
                JSONParser parser = JSON.createParser(jsonResponse);

                result = (ProxyApiClarityInquiryEntity)parser.readValueAs(ProxyApiClarityInquiryEntity.class);
            }
        }
        return result;
    }
    public static ProxyApiClarityClearIncomeAttributes getClarityIncome(String inputData) {

        ProxyApiClarityClearIncomeAttributes result = null;

        Boolean validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();

        if (validatedAPI) {
            String body = 'input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8');

            String jsonResponse = null;

            if (Test.isRunningTest()) {
                jsonResponse = LibraryTest.fakeProxyApiClarityClearIncomeAttributes();
            } else {
                jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GET_CLARITY_INCOME, body);
            }

            System.debug('Json Response' + jsonResponse);

            if (jsonResponse != null) {
                JSONParser parser = JSON.createParser(jsonResponse);

                result = (ProxyApiClarityClearIncomeAttributes )parser.readValueAs(ProxyApiClarityClearIncomeAttributes.class);
            }
        }
        return result;
    }
}