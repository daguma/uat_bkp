@isTest
private class FeeUtilTest {

    /*@isTest static void gets_fee_percentage_for_finwise_state() {
        Finwise_Details__c finwiseParams = new Finwise_Details__c(name = 'finwise', Enable_Finwise__c = true);
        insert FinwiseParams;

        //new change

        Fee_Pricing_Rules__c feeRules = new Fee_Pricing_Rules__c(Name = 'Test',
                Fee_Percent__c = 5,
                FICO_Min_Limit__c = 640,
                FICO_Max_Limit__c = 659,
                Grade__c = 'B1',
                Term__c = '48',
                Type__c = 'FINWISE',
                States__c = 'FL',
                ProductType__c = 'LPC-0001',
                APR__c = 36.00);

        insert feeRules;

        Decimal result = FeeUtil.getFeePricingRule(650, 'B1', 48, 'FL', 'LPC-0001').Fee_Percent__c;
    
        System.assertEquals(5, result);
    }

    @isTest static void gets_fee_percentage_for_GA() {
        Finwise_Details__c finwiseParams = new Finwise_Details__c(name = 'finwise', Enable_Finwise__c = true);
        insert FinwiseParams;

        Fee_Pricing_Rules__c feeRules = new Fee_Pricing_Rules__c(Name = 'Test',
                Fee_Percent__c = 5,
                FICO_Min_Limit__c = 640,
                FICO_Max_Limit__c = 659,
                Grade__c = 'B1',
                Term__c = '48',
                Type__c = 'LENDING_POINT',
                States__c = 'GA',
                ProductType__c = 'LPC-0001',
                APR__c = 36.00);

        insert feeRules;

        Decimal result = FeeUtil.getFeePricingRule(650, 'B1', 48, 'GA', 'LPC-0001').Fee_Percent__c;

        System.assertEquals(5, result);
    }

    @isTest static void gets_fee_percentage_for_NJ() {

        Finwise_Details__c finwiseParams = new Finwise_Details__c(name = 'finwise', Enable_Finwise__c = false);
        insert FinwiseParams;

        Decimal result = FeeUtil.getFeePricingRule(650, 'B1', 32, 'NJ', 'LPC-0001').Fee_Percent__c;

        System.assertEquals(0, result);
    }

    @isTest static void gets_default_fee_percentage() {

        Finwise_Details__c finwiseParams = new Finwise_Details__c(name = 'finwise', Enable_Finwise__c = true);
        insert FinwiseParams;

        Fee_Pricing_Rules__c feeRules = new Fee_Pricing_Rules__c(Name = 'Test',
                Fee_Percent__c = 5,
                FICO_Min_Limit__c = 640,
                FICO_Max_Limit__c = 659,
                Grade__c = 'B1',
                Term__c = '36',
                Type__c = 'FINWISE',
                States__c = 'CA',
                ProductType__c = 'LPC-0001',
                APR__c = 23.85);

        Decimal result = FeeUtil.getFeePricingRule(650, 'B1', 32, 'CA', 'LPC-0001').Fee_Percent__c;

        System.assertEquals(6, result);
    }*/

    @isTest static void gets_fee() {
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7',
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO');
        insert FinwiseParams;

        Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name = 'POS', Asset_Id__c = 5, SFDC_Value__c = false);
        insert AsetCls;
        Decimal result = FeeUtil.getFee('MO', 0.4, 1000);

        System.assertEquals(75, result);

        result = FeeUtil.getFee('MI', 0.4, 1000);

        System.assertEquals(50, result);

        result = FeeUtil.getFee('MI', 40, 1000);

        System.assertEquals(50, result);

        result = FeeUtil.getFee('MI', 4, 10000);

        System.assertEquals(300, result);

        result = FeeUtil.getFee('IL', 0.4, 1000);

        System.assertEquals(25, result);

        result = FeeUtil.getFee('AL', 0.4, 1000);

        System.assertEquals(60, result);

        result = FeeUtil.getFee('MT', 0.4, 1000);

        System.assertEquals(0, result);

        result = FeeUtil.getFee('WA', 0.4, 1000);

        System.assertEquals(40, result);

        result = FeeUtil.getFee('TN', 0.4, 1000);

        System.assertEquals(40, result);

        result = FeeUtil.getFee('KY', 0.4, 1000);

        System.assertEquals(0, result);

        result = FeeUtil.getFee('AZ', 0.4, 1000);

        System.assertEquals(150, result);

        result = FeeUtil.getFee('OH', 0.4, 1000);

        System.assertEquals(100, result);

        result = FeeUtil.getFee('OH', 0.4, 6000);

        System.assertEquals(250, result);

        result = FeeUtil.getFee('OH', 0.4, 25000);

        System.assertEquals(250, result);

        result = FeeUtil.getFee('OH', 0.4, 26000);

        System.assertEquals(260, result);

        result = FeeUtil.getFee('CA', 0.4, 3000);

        System.assertEquals(75, result);

        result = FeeUtil.getFee('CA', 0.4, 5000);

        System.assertEquals(20, result);
    }

    @isTest static void gets_fee_refinance() {
        Decimal result = FeeUtil.getFeeRefinance('GA', 8, 10000);

        System.assertEquals(500, result);
    }

    @isTest static void calculates_loan_amount_by_net_fee_out() {
        Offer__c result = FeeUtil.calculateLoanAmountByOFee('Net Fee Out', 10000, 500);

        System.assertEquals(9500, result.Loan_Amount__c);
    }

    @isTest static void calculates_loan_amount_by_fee_on_top() {
        Offer__c result = FeeUtil.calculateLoanAmountByOFee('Fee On Top', 10000, 500);

        System.assertEquals(10000, result.Loan_Amount__c);
    }
    
    /*
        @isTest static void recalculates_refinance_fee() {
            opportunity app = LibraryTest.createApplicationTH();

            Offer__c offer = new Offer__c();
            offer.Loan_Amount__c = 5000;
            offer.Payment_Amount__c = 100;
            offer.Application__c = app.Id;
            offer.IsSelected__c = false;
            offer.Fee_Percent__c = 0.2;
            offer.Selected_Date__c = Date.Today().addDays(-1);
            offer.Installments__c = 10;
            offer.Term__c = 12;
            offer.APR__c = 0.1;
            offer.Fee__c = 0.2;
            offer.Repayment_Frequency__c = 'Weekly';
            offer.Total_Loan_Amount__C = 6000;

            insert offer;

            Offer__c result = FeeUtil.recalculateRefinanceFee(offer, 'GA', 'Net Fee Out');

            System.assertEquals(6000, result.Total_Loan_Amount__C);
        }
    */

    @isTest static void gets_validation_fee() {
        opportunity app = LibraryTest.createApplicationTH();

        Offer__c offer = new Offer__c();
        offer.Opportunity__c = app.Id;
        offer.Fee_Percent__c = 0.2;
        offer.IsSelected__c = true;

        insert offer;

        String result = FeeUtil.getValidationFee(app.Id);

        System.assertEquals('0.20%', result);
    }

    @isTest static void gets_fee_validation_tolerance_co() {
        Boolean result = FeeUtil.getFeeValidationTolerance(9, 'CO');

        System.assertEquals(true, result);
    }

    @isTest static void gets_fee_validation_tolerance() {
        Boolean result = FeeUtil.getFeeValidationTolerance(9, 'GA');

        System.assertEquals(false, result);

        result = FeeUtil.getFeeValidationTolerance(4, 'GA');

        System.assertEquals(true, result);
    }
}