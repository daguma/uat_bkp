@isTest public class DailyReferralFileJobTest 
{

    @isTest static void processDailyFile() 
    {
    	String referralPartner = 'TestLoans';
    	String referralLeadID = 'someLenderAppId';
    	String firstName = 'David';
        String lastName = 'Testcase';
		String email = 'test@gmail2.com';
		String referralStatus = 'Funded';
		String referralFundingAmount = '4000';
		String referralAPR = '23';
		String referralTerm = '28';
		String referralFundingDateStr = '2016-12-28T05:00:00Z';

        Date d = Date.newInstance(2016,12,28);
        Time t = Time.newInstance(5,0,0,0);
        DateTime referralFundingDate = DateTime.newInstance(d, t);

        String docHeader = 	'Lender ID,Application ID,Lender Application ID,FirstName,LastName,Email,App_Dt,Apps,Offered,Accepted,' + 
        					'Funded,Pending,Rejected,Funded_Amount,APR,Term,Funded_Date_Time\n';
    	String documentStr = referralPartner + ',someAppId,' + referralLeadID + ',' + firstName + ',' + lastName + ',' + email + 
    						 ',12/21/2016,1,0,0,1,0,0,' + referralFundingAmount + ',' + referralAPR + ',' + referralTerm + ',' + 
    						 referralFundingDateStr;

    	Folder leadMonetizationFolder = [SELECT Id FROM Folder WHERE Name = 'Lead Monetization Report'];
    	Document dailyFile = new Document();
    	dailyFile.Name = 'Test Daily File';
    	dailyFile.Body = Blob.valueOf(docHeader + documentStr);
	    dailyFile.ContentType = 'text/csv';
	    dailyFile.Type = 'csv';
	    dailyFile.Description = 'PROCESS PENDING';
	    dailyFile.DeveloperName = 'Test_Daily_File';
	    dailyFile.IsPublic = true;
	    dailyFile.FolderId = leadMonetizationFolder.id;
	    insert dailyFile;

        genesis__Applications__c app = LibraryTest.createApplicationTH();

        app.FinMkt_LoanId__c = referralLeadID;
        update app;

        DailyReferralFileJob dailyJob = new DailyReferralFileJob();
        dailyJob.processDailyFile();

        List<Document> docs = [ SELECT  Id
                                FROM Document 
                                WHERE FolderId =: leadMonetizationFolder.Id
                                AND Type = 'csv'
                                AND Description = '**** FILE PROCESSED SUCCESSFULLY! ****' 
                                ORDER BY LastModifiedDate DESC, CreatedDate DESC
                                LIMIT 1];

        genesis__Applications__c updatedApp = [SELECT  Id, 
                                                            Referral_Date__c,
                                                            Referral_Partner__c,
                                                            Referral_Funding_Amount__c,
                                                            Referral_APR__c,
                                                            Referral_Term__c,
                                                            Referral_Funding_Date__c,
                                                            genesis__Contact__r.Referral_Status__c,
                                                            FinMkt_LoanId__c,
                                                            Referred__c
                                                    FROM genesis__Applications__c 
                                                    WHERE Id =: app.Id LIMIT 1];  
                       
        System.assert(docs.size() > 0);                    
        System.assertEquals(updatedApp.genesis__Contact__r.Referral_Status__c, referralStatus);
        System.assertEquals(updatedApp.Referral_Partner__c, referralPartner);
        System.assertEquals(updatedApp.Referral_Funding_Amount__c, Decimal.valueOf(referralFundingAmount));
        System.assertEquals(updatedApp.Referral_APR__c, Decimal.valueOf(referralAPR));
        System.assertEquals(updatedApp.Referral_Term__c, Integer.valueOf(referralTerm));
        System.assertEquals(updatedApp.Referral_Funding_Date__c, referralFundingDate);
        System.assert(updatedApp.Referred__c == true);
        System.assert(updatedApp.Referral_Date__c != null);
    }
}