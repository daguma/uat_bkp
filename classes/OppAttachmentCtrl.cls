public class OppAttachmentCtrl {

    @AuraEnabled
    public static List<Attachment> getOpportunityAttachments(String recordId) { 
    	String opportunityId = '';
        List<Attachment> attachments = new List<Attachment>();

        opportunityId = [SELECT Opportunity__c FROM Case WHERE Id = : recordId].Opportunity__c;
        attachments = [SELECT Id, Name, CreatedBy.Name, CreatedDate, LastModifiedDate FROM Attachment WHERE ParentId =: opportunityId ORDER BY CreatedDate DESC];
        return attachments;
    }

    @AuraEnabled
    public static Boolean deleteAttachment(String attachmentId) {
        try {
            Attachment attachment = [SELECT Id FROM Attachment WHERE Id =: attachmentId];
            delete attachment;
            return true;
        } catch (DmlException e) {
            system.debug('Error deleting the attachment: line ' + e.getLineNumber() + ' \n' + e.getMessage());
            return false;
        }
    }

    @AuraEnabled
    public static Attachment saveAttachment(Id caseId, String fileName, String base64Data, String contentType) {
        System.debug('caseId '+caseId);
        String opportunityId = '';

        try {
            opportunityId = [SELECT Opportunity__c FROM Case WHERE Id = : caseId].Opportunity__c;
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            Attachment attachment = new Attachment();
            
            attachment.parentId = opportunityId;
            attachment.Body = EncodingUtil.base64Decode(base64Data);
            attachment.Name = fileName;
            attachment.ContentType = contentType;
        
            insert attachment;
            attachment = [SELECT Id, Name, CreatedBy.Name, CreatedDate, LastModifiedDate FROM Attachment WHERE Id =:attachment.Id];
            return attachment;
        } catch (DmlException e) {
            system.debug('Error during the attachment creation: line ' + e.getLineNumber() + ' \n' + e.getMessage());
            return null;
        }
    }
}