public with sharing class RefinanceParameterRelatedList 
{
	public loan__Loan_Account__c contract {get; set;}
	public List<loan_Refinance_Params__c> params {get; set;}

	public RefinanceParameterRelatedList(ApexPages.StandardController controller) 
	{
		contract = (loan__Loan_Account__c)controller.getRecord();
		params = [	SELECT 	Id,
							ContactAddress__c,
							Eligible_For_Refinance__c,
							FICO__c,
							Grade__c,
							Last_Refinance_Eligibility_Review__c,
							On_Past_Due__c,
							On_Payment_Plan__c,
							ValidatedIncome__c
                 	FROM   	loan_Refinance_Params__c 
                    WHERE  	Contract__c =: contract.Id];
	}
}