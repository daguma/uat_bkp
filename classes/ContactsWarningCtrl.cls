public class ContactsWarningCtrl {
    @AuraEnabled
    public static ContactsWarningEntity isMultiloan(Id contactId) {

        ContactsWarningEntity rae = new ContactsWarningEntity();

        try {
            rae.isMultiloan = checkOppForMultiloan(contactId);
        } catch (Exception e) {
            System.debug('RefinanceAlertCtrl - getRefiAppByContact - StackTrace: ' + e.getStackTraceString());
            rae.errorMessage = e.getStackTraceString();
        }
        return rae;
    }

    private static Boolean checkOppForMultiloan(Id contactId) {
        
        for (Opportunity opp : [SELECT id
                FROM Opportunity
                WHERE contact__c = :contactId
                AND type = 'Multiloan' 
                AND  Status__c not in('Aged / Cancellation','Declined (or Unqualified)')]) {
               return true;
            
        }
        return false;
    }



    public class ContactsWarningEntity {

        @AuraEnabled public Boolean hasError { set; get; }
        @AuraEnabled public String errorMessage {
            set {
                errorMessage = value;
                hasError = !String.isEmpty(errorMessage);
            }
            get;
        }
        @AuraEnabled public Boolean isMultiloan { set; get; }

        public ContactsWarningEntity() {
            this.errorMessage = '';
            this.isMultiloan = false;
        }

    }

}