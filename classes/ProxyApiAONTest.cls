@isTest 
private class ProxyApiAONTest {
    
    @isTest static void creates_enrollment_without_validation() {
        
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        loan_AON_Information__c enrollmentInfo = new loan_AON_Information__c();
        enrollmentInfo.Contract__c = clcontract.id;
        enrollmentInfo.IsProcessed__c = false;
        insert enrollmentInfo;
        
        Contact cont = [SELECT FirstName, LastName, MailingStreet, MailingCity, MailingState, MailingPostalCode, 
                        Email, Phone, Id, ints__Middle_Name__c 
                        FROM Contact ORDER BY CreatedDate DESC LIMIT 1];
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
    	AONObjectsParameters.AONEnrollmentInputData enrollmentData = new AONObjectsParameters.AONEnrollmentInputData(cont, clcontract.Name, '');
        
        String jsonRequest = new AONObjectsParameters.AONEnrollmentInputData().get_JSON(enrollmentData);
        
        ProxyApiAON initializeAON = new ProxyApiAON();
        
        ProxyApiBRMSUtil.settings = null;
        
        String result = ProxyApiAON.enrollment(jsonRequest);
        
        System.assertEquals(null, result);
    }
    
    @isTest static void creates_enrollment() {
        
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        loan_AON_Information__c enrollmentInfo = new loan_AON_Information__c();
        enrollmentInfo.Contract__c = clcontract.id;
        enrollmentInfo.IsProcessed__c = false;
        insert enrollmentInfo;
        
        Contact cont = [SELECT FirstName, LastName, MailingStreet, MailingCity, MailingState, MailingPostalCode, 
                        Email, Phone, Id, ints__Middle_Name__c 
                        FROM Contact ORDER BY CreatedDate DESC LIMIT 1];
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        
    	AONObjectsParameters.AONEnrollmentInputData enrollmentData = new AONObjectsParameters.AONEnrollmentInputData(cont, clcontract.Name, '');
    	
        String jsonRequest = new AONObjectsParameters.AONEnrollmentInputData().get_JSON(enrollmentData);
        
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"aonResponse":null,"errorMessage":null,"errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Test.startTest();
        String result = ProxyApiAON.enrollment(jsonRequest);
        Test.stopTest();
    }
    
    @isTest static void creates_billing_without_validation() {
        
        AONObjectsParameters.AONBillingInputData billingData = new AONObjectsParameters.AONBillingInputData(System.now(),'6.31','100','5000',System.now(),'LAI-00000001','LPOPTION01');
        
        String jsonRequest = new AONObjectsParameters.AONBillingInputData().get_JSON(billingData);
        
        ProxyApiAON initializeAON = new ProxyApiAON();
        
        ProxyApiBRMSUtil.settings = null;
        
        String result = ProxyApiAON.billing(jsonRequest);
        
        System.assertEquals(null, result);
    }
    
    @isTest static void creates_billing() {
        
        AONObjectsParameters.AONBillingInputData billingData = new AONObjectsParameters.AONBillingInputData(System.now(),'6.31','100','5000',System.now(),'LAI-00000001','LPOPTION01');
        
        String jsonRequest = new AONObjectsParameters.AONBillingInputData().get_JSON(billingData);
        
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"aonResponse":null,"errorMessage":null,"errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Test.startTest();
        String result = ProxyApiAON.billing(jsonRequest);
        Test.stopTest();
    }
    
    @isTest static void creates_cancellation_without_validation() {
        
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = clcontract.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;
        
        AONObjectsParameters.AONCancellationInputData cancellationData = new AONObjectsParameters.AONCancellationInputData(clcontract.Name, 'JLP', '0');
        
        String jsonRequest = new AONObjectsParameters.AONCancellationInputData().get_JSON(cancellationData);
        
        ProxyApiAON initializeAON = new ProxyApiAON();
        
        ProxyApiBRMSUtil.settings = null;
        
        String result = ProxyApiAON.cancellation(jsonRequest);
        
        System.assertEquals(null, result);
    }
    
    @isTest static void creates_cancellation() {
        
        loan__Loan_Account__c clcontract = LibraryTest.createContractTH();
        
        clcontract = [SELECT Id, Name FROM loan__Loan_Account__c WHERE Id =: clcontract.Id LIMIT 1];
        
        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = clcontract.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;
        
        AONObjectsParameters.AONCancellationInputData cancellationData = new AONObjectsParameters.AONCancellationInputData(clcontract.Name, 'JLP', '0');
        
        String jsonRequest = new AONObjectsParameters.AONCancellationInputData().get_JSON(cancellationData);
        
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"aonResponse":null,"errorMessage":null,"errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Test.startTest();
        String result = ProxyApiAON.cancellation(jsonRequest);
        Test.stopTest();
    }
}