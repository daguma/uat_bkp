public class RefinanceCreditReviewRules {
    @invocableMethod
    public static void needsCreditReview(List<Opportunity> apps) {
            Opportunity thisApp = apps[0];
        try {
            Boolean needsCR = false, diffFICO = false, diffAddress = false,diffIncome = false;
            Boolean hadCreditReview = false;
            
            String reviewReason = 'Credit Profile', existingRevReason = '';
            String crStatus = 'Credit Review';
            
            
            String oldAppId = [ SELECT  Id
                               FROM    Opportunity
                               WHERE   Contact__c = : thisApp.Contact__c
                               AND Lending_Account__c <> ''
                               AND Id <> : thisApp.Id
                               ORDER BY CreatedDate DESC
                               LIMIT 1].Id;
            
            
            System.debug(LoggingLevel.INFO, 'Old App Id: ' + oldAppId); 
            
            // NEED TO COMPARE FICO, CONTACT ADDRESS, ESTIMATED GRAND TOTAL:
            for (loan_Refinance_Params__c theseParams : [SELECT FICO__c,
                                                         Grade__c,
                                                         ValidatedIncome__c,
                                                         ContactAddress__c
                                                         FROM   loan_Refinance_Params__c
                                                         WHERE  Contract__c in (SELECT Id
                                                                                FROM   loan__Loan_Account__c
                                                                                WHERE  Opportunity__c = : oldAppId)
                                                         ORDER BY CreatedDate DESC
                                                         LIMIT 1]) {
                                                             
                                                             ProxyApiCreditReportEntity newestCeditReport = CreditReport.getByEntityId(thisApp.Id);
                                                             Integer newestFICO = 0;
                                                             if (newestCeditReport.getAttributeValue(CreditReport.FICO).isNumeric()){
                                                                 newestFICO = Integer.valueOf(newestCeditReport.getAttributeValue(CreditReport.FICO));
                                                             }
                                                             
                                                             System.debug(LoggingLevel.INFO, 'newestFICO:' + newestFICO);
                                                             
                                                             Set<String> appHistStatus = new Set<String>();
                                                             Contact c = [ select id, MailingStreet, MailingCity, MailingState, Annual_Income__c from Contact where Id =: thisApp.Contact__c];
                                                             
                                                             System.debug(LoggingLevel.INFO, 'Current address: ' + c.MailingStreet);
                                                             System.debug(LoggingLevel.INFO, 'Current address: ' + c.MailingCity);
                                                             System.debug(LoggingLevel.INFO, 'Current address: ' + c.MailingState);
                                                             
                                                             String currentAddress = c.MailingStreet + ', ' + c.MailingCity + ', ' + c.MailingState;
                                                             
                                                             System.debug(LoggingLevel.INFO, 'Current address: ' + currentAddress);
                                                             
                                                             hadCreditReview = (appHistStatus.contains('Credit Review'));
                                                             
                                                             Double thisIncome = ((thisApp.Estimated_Grand_Total__c == 0 || thisApp.Estimated_Grand_Total__c == null) ? c.Annual_Income__c : thisApp.Estimated_Grand_Total__c);
                                                             
                                                             System.debug(LoggingLevel.INFO, 'thisIncome: ' + thisIncome + ', Grand Total: ' + thisApp.Estimated_Grand_Total__c + ', Annual Income: ' + c.Annual_Income__c + ' theseParams.ValidatedIncome__c:' + theseParams.ValidatedIncome__c);
                                                             
                                                             diffIncome = (theseParams.ValidatedIncome__c < 200000 && thisIncome >= 200000);
                                                             
                                                             diffAddress = (hadCreditReview && (currentAddress.compareTo(theseParams.ContactAddress__c) != 0));
                                                             
                                                             diffFICO = (newestFICO >= 720 && ((theseParams.FICO__c >= 720 && !hadCreditReview) || theseParams.FICO__c < 720));
                                                             
                                                             System.debug(LoggingLevel.INFO, 'theseParams: ' + theseParams);
                                                             System.debug(LoggingLevel.INFO, 'diffFICO: ' + diffFICO);
                                                             System.debug(LoggingLevel.INFO, 'diffAddress: ' + diffAddress);
                                                             System.debug(LoggingLevel.INFO, 'diffIncome: ' + diffIncome);
                                                             System.debug(LoggingLevel.INFO, 'hadCreditReview? ' + hadCreditReview);
                                                             
                                                             if (diffIncome || diffAddress || diffFICO) {
                                                                 Opportunity gapp = new Opportunity (ID = thisApp.Id, Status__c = 'Credit Review', Review_Reason__c = 'Credit Profile');
                                                                 gapp.Review_Reason__c = reviewReason;
                                                                 gapp.Status__c = crStatus;
                                                                 System.debug(LoggingLevel.INFO, 'new review reason: ' + gapp.Review_Reason__c);
                                                                 update gapp;
                                                             }
                                                         }
        } catch (Exception e) {
            WebToSFDC.notifyDev('needsCreditReview Error', 'needsCreditReview error - ' + thisApp.Id +  ' \n' + e.getCause() + '\n\n' + e.getLineNumber() + '\n\n' + e.getMessage() + '\n\n' + e.getStackTraceString());        
            
        }
    }
}