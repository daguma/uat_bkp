@RestResource(urlMapping='/AONRestResource/*')
global with sharing class AONRestResource {

    private AONObjectsParameters aonObjectsParameters = new AONObjectsParameters();
    private static final String CREATE_PAYMENT = 'createPayment';
    private static final String AIM_CANCELLATION = 'aim_cancellation';
    private static final Integer CREATED = 201;
    private static final Integer NOT_IMPLEMENTED = 501;
    private static final Integer METHOD_NOT_ALLOWE = 405;
    private static final Integer NO_CONTENT = 204;
    private static final Integer BAD_REQUEST = 400;
    private static final String SUCCSESS_RESULT = 'Transaction was successful';
    private static final String NOT_SUCCSESS_RESULT = 'Transaction wasn\'t successful';

    public AONRestResource(){}

    @HttpPost
    global static String httpPostService(String postService, String inputData) {

        RestResponse response = RestContext.response;
		AONProcesses process = new AONProcesses();
        String result = NOT_SUCCSESS_RESULT;

        try {

            if(postService != null && inputData != null){
                System.debug('postService: ' + postService);
                if(postService == CREATE_PAYMENT) {
                    
                    aonObjectsParameters.AONPaymentInputData payment = (aonObjectsParameters.AONPaymentInputData) System.JSON.deserialize(
                    EncodingUtil.base64Decode(inputData).toString(), aonObjectsParameters.AONPaymentInputData.class);

                    if(payment != null){

                        if(process.createPayment(payment)){
                            response.statusCode = CREATED;
                            result = SUCCSESS_RESULT;
                        }else {
                            response.statusCode = NOT_IMPLEMENTED;
                            result = NOT_SUCCSESS_RESULT;
                        }
                    }
                }else if(postService == AIM_CANCELLATION) {

                    aonObjectsParameters.AONCancellationInputData cancellation = (aonObjectsParameters.AONCancellationInputData) System.JSON.deserialize(
                        EncodingUtil.base64Decode(inputData).toString(), aonObjectsParameters.AONCancellationInputData.class);

                    System.debug('cancellation: ' + cancellation);

                    if(cancellation != null){

                        aonObjectsParameters.SalesforceResponse salesforceResponse = process.aimCancellation(cancellation);

                        System.debug('salesforceResponse: ' + salesforceResponse);

                        if(Integer.valueOf(salesforceResponse.statusCode) == CREATED){
                            response.statusCode = CREATED;
                            result = salesforceResponse.get_JSON(salesforceResponse);
                        }else {
                            response.statusCode = BAD_REQUEST;
                            result = salesforceResponse.errorDetails;
                        } 
                    }
                }

                /* Here are the other HttpPostServices  */

                else{
                    response.statusCode = METHOD_NOT_ALLOWE;
                }

                
            }else{
                response.statusCode = NO_CONTENT;
                result = NOT_SUCCSESS_RESULT;
            }

        } catch(StringException e) {
            System.debug(e.getMessage());
            response.statusCode = NOT_IMPLEMENTED;
            result = NOT_SUCCSESS_RESULT;
        }

        return result;
    } 
}