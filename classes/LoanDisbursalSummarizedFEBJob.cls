global class LoanDisbursalSummarizedFEBJob  implements Schedulable  {
    global void execute (SchedulableContext sc) {
        try {
            LoanDisbursalSummarizedByLine t = new LoanDisbursalSummarizedByLine();
            t.calculateDates();            
            t.generateFEB();
            WebToSFDC.notifyDev('FEB Disbursal file generation', 'File generate succesfully');
        }catch (Exception e) {  WebToSFDC.notifyDev('FEB Disbursal file generation', e.getMessage() + '\n' + e.getLineNumber() + '\n' + e.getStackTraceString());}
    }
}