@isTest

public class TestloanPaymentPlanCtl{
    static testmethod void test(){
     
       TestHelperForManaged.createSeedDataForTesting();
        //TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupMetadataSegments();
        loan.TestHelper.systemDate = Date.newInstance(2014, 3, 3); // Mar 3 - Monday
       
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
       
        loan__Transaction_Approval_Config__c c = new loan__Transaction_Approval_Config__c();
            c.loan__Payment__c = true;
            c.loan__Payment_Reversal__c = false;
            c.loan__Funding__c = false;
            c.loan__Funding_Reversal__c = false;
            c.loan__Write_off__c = false;
            c.loan__Accounting__c = false;
          
            insert c;
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = TestHelperForManaged.createLoanPurpose();
        
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        b1.peer__National_Id__c = '1223';
        update b1;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Advance Trust Account');
        
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
        
        //Create a dummy Loan Account
        loan__Loan_Account__c dummylaMonthly = TestHelperForManaged.createLoanAccountForAccountObj(dummyLP,
                                        b1,
                                        dummyFeeSet,
                                        dummyLoanPurpose,
                                        dummyOffice,
                                        true,
                                        loan.TestHelper.systemDate.addDays(1),
                                        null,
                                        loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY);
       
        dummylaMonthly.loan__Borrower_ACH__c = dummyBank.Id;
        dummylaMonthly.loan__ACH_On__c = true;
        dummylaMonthly.loan__ACH_Frequency__c = 'Monthly';
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = loan.TestHelper.systemDate;
        dummylaMonthly.loan__ACH_Start_Date__c = loan.TestHelper.systemDate;
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__Ach_Debit_Day__c = 3;
        dummylaMonthly.loan__ACH_End_Date__c = loan.TestHelper.systemDate.addYears(1);
     //   dummylaMonthly.ACH_Bank_Account__c = dummyBank.Id;
        Update dummylaMonthly;
        
       

           loan_Plan_Payment__c lPP = new loan_Plan_Payment__c();
           lPP.Payment_Amount__c = 355;
           lpp.Account_Number__c='123456';
           lPP.ACH_Debit_Date__c    = System.today().adddays(2);
           
           lPP.Loan_Account__c = dummylaMonthly.Id;
           

           insert lPP;
           
        ApexPages.StandardController sc = new ApexPages.StandardController(dummylaMonthly);
        loanPaymentPlanCtl lc = new loanPaymentPlanCtl(sc);
        
        
        lc.insertPayment();
        System.currentPageReference().getParameters().put('paymentid', lPP.id);
      
        lc.cancelSelectedPayment();
        lc.deletePayment();  
        lc.pendingSelectedPayment();

    }
    
    static testmethod void test1(){
     
       TestHelperForManaged.createSeedDataForTesting();
        //TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupMetadataSegments();
        loan.TestHelper.systemDate = Date.newInstance(2014, 3, 3); // Mar 3 - Monday
       
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
       
        loan__Transaction_Approval_Config__c c = new loan__Transaction_Approval_Config__c();
            c.loan__Payment__c = true;
            c.loan__Payment_Reversal__c = false;
            c.loan__Funding__c = false;
            c.loan__Funding_Reversal__c = false;
            c.loan__Write_off__c = false;
            c.loan__Accounting__c = false;
          
            insert c;
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = TestHelperForManaged.createLoanPurpose();
        
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        b1.peer__National_Id__c = '1223';
        update b1;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Advance Trust Account');
        
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
        
        //Create a dummy Loan Account
        loan__Loan_Account__c dummylaMonthly = TestHelperForManaged.createLoanAccountForAccountObj(dummyLP,
                                        b1,
                                        dummyFeeSet,
                                        dummyLoanPurpose,
                                        dummyOffice,
                                        true,
                                        loan.TestHelper.systemDate.addDays(1),
                                        null,
                                        loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY);
       
        dummylaMonthly.loan__Borrower_ACH__c = dummyBank.Id;
        dummylaMonthly.loan__ACH_On__c = true;
        dummylaMonthly.loan__ACH_Frequency__c = 'Monthly';
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = loan.TestHelper.systemDate;
        dummylaMonthly.loan__ACH_Start_Date__c = loan.TestHelper.systemDate;
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__Ach_Debit_Day__c = 3;
        dummylaMonthly.loan__ACH_End_Date__c = loan.TestHelper.systemDate.addYears(1);
     //   dummylaMonthly.ACH_Bank_Account__c = dummyBank.Id;
        Update dummylaMonthly;
        
       

           loan_Plan_Payment__c theP1 = new loan_Plan_Payment__c();
           theP1.Payment_Amount__c = 355;
           theP1.ACH_Debit_Date__c    = System.today().adddays(3);
           theP1.Status__c            = 'Pending';
           theP1.Loan_Account__c = dummylaMonthly.Id;
                                
           insert theP1;
           
           loan_Plan_Payment__c theP = new loan_Plan_Payment__c();
           theP=[select Account_Number__c,Routing_Number__c ,Comment__c,ACH_Debit_Date__c ,Bank_Account__c,Payment_Amount__c,Status__c ,Loan_Account__c from loan_Plan_Payment__c where id=:theP1.id];
        ApexPages.StandardController sc = new ApexPages.StandardController(dummylaMonthly);
        loanPaymentPlanCtl lc = new loanPaymentPlanCtl(sc);


        System.currentPageReference().getParameters().put('id', dummylaMonthly.Id);
        lc.theP = theP;
        lc.insertPayment();
        lc.cancelSelectedPayment();
        lc.deletePayment();  
        lc.pendingSelectedPayment();        



    }
    

}