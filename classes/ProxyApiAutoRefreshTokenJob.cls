global class ProxyApiAutoRefreshTokenJob implements Schedulable {
    global void execute(SchedulableContext sc) {
    	ProxyApiUtil.ApiRefreshToken();
    }
}