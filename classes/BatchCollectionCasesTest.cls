@isTest
private class BatchCollectionCasesTest {
    
	@isTest static void executes_job() {
        Test.startTest();

        BatchCollectionCases bathCollJob = new BatchCollectionCases();
        String cron = '0 0 23 * * ?';
        system.schedule('Test Batch Collection Cases', cron, bathCollJob);

        Test.stopTest();
    }
    
    @isTest static void creates_case() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(-15);
        contract.loan__Loan_Status__c = 'Active - Bad Standing';
        contract.loan__Pmt_Amt_Cur__c = 200;
        contract.loan__Payment_Amount__c = 200;
        update contract;
        
        Test.startTest();

        BatchCollectionCases b = new BatchCollectionCases();
        database.executebatch(b);

        Test.stopTest();
    }
    
   @isTest static void test_dmc() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(-15);
        contract.loan__Loan_Status__c = 'Active - Bad Standing';
        contract.DMC__c = 'test';
        contract.loan__Pmt_Amt_Cur__c = 200;
        contract.loan__Payment_Amount__c = 200;
        update contract;
        
        Test.startTest();

        BatchCollectionCases b = new BatchCollectionCases();
        database.executebatch(b);

        Test.stopTest();
    }

}