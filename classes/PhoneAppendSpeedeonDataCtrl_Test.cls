@isTest
global class PhoneAppendSpeedeonDataCtrl_Test implements HttpCalloutMock{
    testmethod static void test1(){
        Test.setMock(HttpCalloutMock.class, new PhoneAppendSpeedeonDataCtrl_Test());
        //HttpResponse res = PhoneAppendSpeedeonDataCtrl.callSpeedeonData();
        Phone_Append_SpeedeonData__c pas = new Phone_Append_SpeedeonData__c();
        pas.customerId__c = 'test';
        pas.username__c = 'test';
        pas.password__c = 'test';
        pas.messageType__c = 'test';
        pas.Phone_Append_End_url__c = '/test';
        pas.name = 'PhoneAppendSettings';
        insert pas;
        
    Contact con = LibraryTest.createContactTH();
    con.phone = null;
    con.Lead_Sub_Source__c = 'CreditKarma_API';
    update con;
    
    Opportunity app = LibraryTest.createApplicationTH();
    app.Status__c = 'Credit Approved - No Contact Allowed';
    update app;
        List<id> ids = new List<Id>();
        ids.add(app.id);
        PhoneAppendSpeedeonDataCtrl.callSpeedeonData(ids);
    }
    
    
    global HTTPResponse respond(HTTPRequest req) {
       
        String response = '<?xml version="1.0" ?><SpeedeonResponse><messageType>T</messageType><customerId>10005</customerId><reasonNumber1>9001</reasonNumber1><transactionType /><actionCode>E</actionCode><transactionId>123456</transactionId><attributes><firstName>TIMOTHY</firstName><lastName>KENNEDY</lastName><addressLine1>212 Acorn Dr </addressLine1><city>Titusville</city><zip5>32780</zip5><phone>3217591317</phone><phonetype>W</phonetype><DNC>1</DNC><SPEEDEON_NAMEID>31201802050031249883</SPEEDEON_NAMEID></attributes></SpeedeonResponse>';
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(response);
        res.setStatusCode(200);
        return res;
    }
}