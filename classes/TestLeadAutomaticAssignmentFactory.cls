@IsTest
public class TestLeadAutomaticAssignmentFactory {
    public static void createSalesCenterLoginTimes(boolean addDefaultTimes){
        List<Sales_Center_Login_Times__c> allSettings = new List<Sales_Center_Login_Times__c>();
        Sales_Center_Login_Times__c kennesawSetting = new Sales_Center_Login_Times__c();
        kennesawSetting.name = 'Kennesaw Sales Center';
        kennesawSetting.Sales_Center_Id__c = Label.Kennesaw_Group_Id;
        if(addDefaultTimes){
            kennesawSetting.Monday_Time_From__c = 0;
            kennesawSetting.Monday_Time_To__c = 0;
            kennesawSetting.Tuesday_Time_From__c = 0;
            kennesawSetting.Tuesday_Time_To__c = 0;
            kennesawSetting.Wednesday_Time_From__c = 0;
            kennesawSetting.Wednesday_Time_To__c = 0;
            kennesawSetting.Thursday_Time_From__c = 0;
            kennesawSetting.Thursday_Time_To__c = 0;
            kennesawSetting.Friday_Time_From__c = 0;
            kennesawSetting.Friday_Time_To__c = 0;
            kennesawSetting.Saturday_Time_From__c = 0;
            kennesawSetting.Saturday_Time_To__c = 0;
            kennesawSetting.Sunday_Time_From__c = 0;
            kennesawSetting.Sunday_Time_To__c = 0;
        }
        allSettings.add(kennesawSetting);
        Sales_Center_Login_Times__c drSetting = new Sales_Center_Login_Times__c();
        drSetting.name = 'DR Sales Center';
        drSetting.Sales_Center_Id__c = Label.DR_Group_Id;
        if(addDefaultTimes){
            drSetting.Monday_Time_From__c = 0;
            drSetting.Monday_Time_To__c = 0;
            drSetting.Tuesday_Time_From__c = 0;
            drSetting.Tuesday_Time_To__c = 0;
            drSetting.Wednesday_Time_From__c = 0;
            drSetting.Wednesday_Time_To__c = 0;
            drSetting.Thursday_Time_From__c = 0;
            drSetting.Thursday_Time_To__c = 0;
            drSetting.Friday_Time_From__c = 0;
            drSetting.Friday_Time_To__c = 0;
            drSetting.Saturday_Time_From__c = 0;
            drSetting.Saturday_Time_To__c = 0;
            drSetting.Sunday_Time_From__c = 0;
            drSetting.Sunday_Time_To__c = 0;
        }
        allSettings.add(drSetting);
        insert allSettings;
    }
    
    public static void createSalesCenterLoginTimesAllAssignedToKennesaw(){
        List<Sales_Center_Login_Times__c> allSettings = new List<Sales_Center_Login_Times__c>();
        Sales_Center_Login_Times__c kennesawSetting = new Sales_Center_Login_Times__c();
        kennesawSetting.name = 'Kennesaw Sales Center';
        kennesawSetting.Sales_Center_Id__c = Label.Kennesaw_Group_Id;
        kennesawSetting.Monday_Time_From__c = 1;
        kennesawSetting.Monday_Time_To__c = 25;
        kennesawSetting.Tuesday_Time_From__c = 1;
        kennesawSetting.Tuesday_Time_To__c = 25;
        kennesawSetting.Wednesday_Time_From__c = 1;
        kennesawSetting.Wednesday_Time_To__c = 25;
        kennesawSetting.Thursday_Time_From__c = 1;
        kennesawSetting.Thursday_Time_To__c = 25;
        kennesawSetting.Friday_Time_From__c = 1;
        kennesawSetting.Friday_Time_To__c = 25;
        kennesawSetting.Saturday_Time_From__c = 1;
        kennesawSetting.Saturday_Time_To__c = 25;
        kennesawSetting.Sunday_Time_From__c = 1;
        kennesawSetting.Sunday_Time_To__c = 25;
        allSettings.add(kennesawSetting);
        Sales_Center_Login_Times__c drSetting = new Sales_Center_Login_Times__c();
        drSetting.name = 'DR Sales Center';
        drSetting.Sales_Center_Id__c = Label.DR_Group_Id;
        drSetting.Monday_Time_From__c = 0;
        drSetting.Monday_Time_To__c = 0;
        drSetting.Tuesday_Time_From__c = 0;
        drSetting.Tuesday_Time_To__c = 0;
        drSetting.Wednesday_Time_From__c = 0;
        drSetting.Wednesday_Time_To__c = 0;
        drSetting.Thursday_Time_From__c = 0;
        drSetting.Thursday_Time_To__c = 0;
        drSetting.Friday_Time_From__c = 0;
        drSetting.Friday_Time_To__c = 0;
        drSetting.Saturday_Time_From__c = 0;
        drSetting.Saturday_Time_To__c = 0;
        drSetting.Sunday_Time_From__c = 0;
        drSetting.Sunday_Time_To__c = 0;
        allSettings.add(drSetting);
        insert allSettings;
    }
    
    public static List<Automatic_Assignment_Lead_Category__c> createAutomaticAssignmentLeadCategoriesWithoutExceptions(){
        List<Automatic_Assignment_Lead_Category__c> allCategories = new List<Automatic_Assignment_Lead_Category__c>();
        Automatic_Assignment_Lead_Category__c offerAcceptedRecord = new Automatic_Assignment_Lead_Category__c();
        offerAcceptedRecord.id = Label.Offer_Accepted_Category_Id;
        offerAcceptedRecord.name = 'Offer Accepted';
        offerAcceptedRecord.All_Reps_Selected__c = true;                
        allCategories.add(offerAcceptedRecord);
        Automatic_Assignment_Lead_Category__c leadsTodayRecord = new Automatic_Assignment_Lead_Category__c();
        leadsTodayRecord.id = Label.Leads_Today_Category_Id;
        leadsTodayRecord.name = 'Today Leads';
        leadsTodayRecord.All_Reps_Selected__c = true;
        allCategories.add(leadsTodayRecord);
        Automatic_Assignment_Lead_Category__c creditQualifiedRecord = new Automatic_Assignment_Lead_Category__c();
        creditQualifiedRecord.id = Label.Credit_Qualified_Category_Id;
        creditQualifiedRecord.name = 'Credit Qualified';
        creditQualifiedRecord.All_Reps_Selected__c = true;
        allCategories.add(creditQualifiedRecord);
        update allCategories;
        return allCategories;
    }
    
    public static List<Automatic_Assignment_Lead_Category__c> createAutomaticAssignmentLeadCategoriesWithExceptions(){
        List<User> allSalesReps = getUsersFromSalesCenters();
        allSalesReps.remove(0);
        List<Automatic_Assignment_Lead_Category__c> allCategories = new List<Automatic_Assignment_Lead_Category__c>();
        Automatic_Assignment_Lead_Category__c offerAcceptedRecord = new Automatic_Assignment_Lead_Category__c();
        offerAcceptedRecord.id = Label.Offer_Accepted_Category_Id;
        offerAcceptedRecord.name = 'Offer Accepted';
        offerAcceptedRecord.All_Reps_Selected__c = false;
        offerAcceptedRecord.Exception_Rep_Ids__c = String.join(allSalesReps, ';');
        allCategories.add(offerAcceptedRecord);
        Automatic_Assignment_Lead_Category__c leadsTodayRecord = new Automatic_Assignment_Lead_Category__c();
        leadsTodayRecord.id = Label.Leads_Today_Category_Id;
        leadsTodayRecord.name = 'Today Leads';
        leadsTodayRecord.All_Reps_Selected__c = false;
        leadsTodayRecord.Exception_Rep_Ids__c = String.join(allSalesReps, ';');
        allCategories.add(leadsTodayRecord);
        Automatic_Assignment_Lead_Category__c creditQualifiedRecord = new Automatic_Assignment_Lead_Category__c();
        creditQualifiedRecord.id = Label.Credit_Qualified_Category_Id;
        creditQualifiedRecord.name = 'Credit Qualified';
        creditQualifiedRecord.All_Reps_Selected__c = false;
        creditQualifiedRecord.Exception_Rep_Ids__c = String.join(allSalesReps, ';');
        allCategories.add(creditQualifiedRecord);
        update allCategories;
        return allCategories;
    }
    
    public static Lead createLead(){
        Date employmentStartDate = Date.today().addYears(-10);
        Date dateOfBirth = Date.today().addYears(-40);
        Lead newLead = new Lead(FirstName='Sundar', LastName='Pichai', Company= 'Google', Status='Open', Use_of_Funds__c='Other', Employment_Start_Date__c=employmentStartDate, Date_of_Birth__c=dateOfBirth, OwnerId=Label.Default_Lead_Owner_ID );
        insert newLead;
        return newLead;
    }
    
    public static void convertLead(Lead leadToConvert){
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadToConvert.id);
        Account dummyAccount = new Account();
        dummyAccount.name = 'Google';
        insert dummyAccount;
        Contact dummyContact = TestHelper.createContact();
        dummyContact.AccountId = dummyAccount.id;
        update dummyContact;
        lc.setContactId(dummyContact.id);
        lc.setAccountId(dummyAccount.id);
        lc.setConvertedStatus('Application Submitted');
        Database.LeadConvertResult lcr = Database.convertLead(lc);
    }
    
    public static genesis__Applications__c createCreditQualifiedApplicationFromContactId(Id contactId){
        TestHelper.createLPCustom('Sample Loan Product','LendingPoint');
  
        loan__Loan_Product__c lp = TestHelper.createLendingProduct('Sample Loan Product');
        
        genesis__Applications__c app = new genesis__applications__c();
        app.genesis__Contact__c = contactId;
        app.genesis__Loan_Amount__c = 1000000;
        app.Lending_Product__c = [Select Id from loan__Loan_Product__C where NAme = 'Sample Loan Product' limit 1].Id;
        app.genesis__Product_Type__c = 'LOAN';
        //app.genesis__Loan_Amount__c = c.Loan_Amount__c;
        app.genesis__Days_Convention__c = '30/360';
        app.OwnerId = Label.Default_Lead_Owner_ID;
        //app.Fee__c = 0.04 * (c.Loan_Amount__c == null?0.0:c.Loan_amount__c);
        app.Fee__c = 0.0;
        app.genesis__Status__c = 'Credit Qualified';
        app.genesis__Interest_Calculation_Method__c = 'Declining Balance';
        app.Automatically_Assigned__c = false;
        app.RecordType = [Select ID from RecordType where DeveloperName =: 'LOAN'
                                and SObjectType =: 'genesis__Applications__c'];
    
        insert app;
        return app;
        
    }
    
    public static genesis__Applications__c createOffersAcceptedApplicationFromContactId(Id contactId){
        TestHelper.createLPCustom('Sample Loan Product','LendingPoint');
  
        loan__Loan_Product__c lp = TestHelper.createLendingProduct('Sample Loan Product');
        
        genesis__Applications__c app = new genesis__applications__c();
        app.genesis__Contact__c = contactId;
        app.genesis__Loan_Amount__c = 1000000;
        app.Lending_Product__c = [Select Id from loan__Loan_Product__C where NAme = 'Sample Loan Product' limit 1].Id;
        app.genesis__Product_Type__c = 'LOAN';
        //app.genesis__Loan_Amount__c = c.Loan_Amount__c;
        app.genesis__Days_Convention__c = '30/360';
        app.OwnerId = Label.Default_Lead_Owner_ID;
        //app.Fee__c = 0.04 * (c.Loan_Amount__c == null?0.0:c.Loan_amount__c);
        app.Fee__c = 0.0;
        app.genesis__Interest_Calculation_Method__c = 'Declining Balance';
        app.Automatically_Assigned__c = false;
        app.RecordType = [Select ID from RecordType where DeveloperName =: 'LOAN'
                                and SObjectType =: 'genesis__Applications__c'];
    
        insert app;
        return app;
        
    }
    
    private static List<User> getUsersFromSalesCenters(){
        List<SelectOption> options = new List<SelectOption>();
        String userType = Schema.SObjectType.User.getKeyPrefix();
        String[] groupIds = new String[]{ Label.Kennesaw_Group_Id, Label.DR_Group_Id };
        List<GroupMember> salesReps = [Select Id, UserOrGroupId From GroupMember Where GroupId IN :groupIds];
        Set<String> salesRepsIds = new Set<String>();
        for(GroupMember salesRep : salesReps){
            if (((String)salesRep.UserOrGroupId).startsWith(userType))
            {
                salesRepsIds.add(salesRep.UserOrGroupId);
            }
        }
        List<User> reps = [Select Id, name from User where Id IN :salesRepsIds];
        return reps;
    }
}