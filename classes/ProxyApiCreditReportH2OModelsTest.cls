@isTest public class ProxyApiCreditReportH2OModelsTest {
    @isTest static void validate() {
        
        ProxyApiCreditReportH2OModels h2oModels = new ProxyApiCreditReportH2OModels ();
        
        h2oModels.predictionModelName = 'test data';
        system.assertEquals('test data',h2oModels.predictionModelName);
        
        h2oModels.predictionModelType = 'test data';
        system.assertEquals('test data',h2oModels.predictionModelType);
        
        h2oModels.predictionResult = new ProxyApiCreditReportPredictionResult ();
        system.assertEquals(null,h2oModels.predictionResult.labelIndex);
        system.assertEquals(null,h2oModels.predictionResult.label);
        system.assertEquals(null,h2oModels.predictionResult.predictionErrorMessage);
        system.assertEquals(null,h2oModels.predictionResult.predictionErrorDetails);
        
    }
}