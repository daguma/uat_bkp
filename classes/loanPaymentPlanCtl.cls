public class loanPaymentPlanCtl {

    public String ContactName {get; set;}
    public loan__loan_account__c theAccount { get; set; }
    public List<loan_Plan_Payment__c> payments { get; set; }
    public loan_Plan_Payment__c theP {get; set;}
    public String lId {get; set;}
    public List<selectedPayment> selectedPaymentList {get; set;}

    public loanPaymentPlanCtl(Apexpages.StandardController controller) {
        lId = '';
        payments = new List<loan_Plan_Payment__c>();
        theAccount = (loan__loan_account__c)controller.getRecord();

        if (theAccount != null && theAccount.Id <> null) {
            lId = theAccount.Id;
        } else {
            lId = System.currentPageReference().getParameters().get('id');
        }

        if (lId != null && lId.Length() > 0) {
            theAccount = [Select Id, Name,  loan__Contact__r.Name,  loan__Pmt_Amt_Cur__c , loan__Previous_Installment_Date__c, loan__Next_Installment_Date__c, loan__Oldest_Due_Date__c from loan__loan_account__c where Id = : theAccount.Id];
            ContactName = theAccount.Loan__Contact__r.Name;

            payments = [Select Id, name, CreatedDate, Account_Number__c,  ACH_Debit_Date__c, Bank_Account__c, Loan_Account__c,
                        Comment__c, Payment_Amount__c, Processed_Date__c, Routing_number__c, Status__c, Payment_Plan_Reason__c
                        from loan_Plan_Payment__c where Loan_Account__c = : theAccount.Id order by CreatedDate Desc];
        }

        theP = new loan_Plan_Payment__c();
    }

//********* method to insert payment from " Add Payment " Section. ********

    public void insertPayment() {
        System.debug('theP ' + theP );
        loan_Plan_Payment__c newPaymentRow = new loan_Plan_Payment__c();
        newPaymentRow.Loan_Account__c = lId;
        try {

            if (theP.ACH_Debit_Date__c <= date.today().adddays(3) ||  theP.ACH_Debit_Date__c == System.today()) {
                createMessage(ApexPages.severity.WARNING, 'The first installment/payment of the plan must be greater than 3 calendar days from the current date (today).') ;

            } else if (theP.ACH_Debit_Date__c == null || theP.Payment_Amount__c == null) {
                createMessage(ApexPages.severity.WARNING, 'Please enter Payment Amount and ACH Debit Date before inserting payment.') ;

            } else if ((theP.Routing_Number__c <> null) && (theP.Routing_Number__c.length() <> 9)) {
                createMessage(ApexPages.severity.WARNING, 'Routing Number must be exactly 9 digits.') ;

            } else {

                newPaymentRow.Account_Number__c    = theP.Account_Number__c;
                newPaymentRow.Payment_Amount__c    = theP.Payment_Amount__c;
                newPaymentRow.Routing_Number__c    = theP.Routing_Number__c;
                newPaymentRow.Comment__c           = theP.Comment__c;
                newPaymentRow.Bank_Account__c      = theP.Bank_Account__c;
                newPaymentRow.Status__c = 'Pending';
                newPaymentRow.ACH_Debit_Date__c = theP.ACH_Debit_Date__c;
                newPaymentRow.Payment_Plan_Reason__c = theP.Payment_Plan_Reason__c;
                insert newPaymentRow;

                newPaymentRow = [Select Name, Id, Account_Number__c, Payment_Amount__c, Routing_Number__c, Comment__c, Bank_Account__c, ACH_Debit_Date__c, Status__c, createdDate, Processed_Date__c, Payment_Plan_Reason__c from loan_Plan_Payment__c where Id = : newPaymentRow.Id];
                selectedPaymentList.add(new selectedPayment(newPaymentRow));

            }

        } catch (Exception ex) {
            system.debug('===Exception===deletePayment' + ex.getMessage() + 'at line ' + ex.getlineNumber());
            WebToSFDC.notifyDev('Loan Payment Plan Class ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage());
        }
    }

    //********* method to delete payment from " Payments " Section. ********

    public void deletePayment() {

        List<loan_Plan_Payment__c> paymntNedToDelete = new  List<loan_Plan_Payment__c>();
        String pId = System.currentPageReference().getParameters().get('paymentid');
        system.debug('pId to delete >>  ' + pId );
        try {
            if (pId != null && pId.length() > 0) {

                Integer j = 0;
                while (j < selectedPaymentList.size()) {
                    if (selectedPaymentList.get(j).paymentRow.id == pId) {
                        paymntNedToDelete.add(selectedPaymentList.get(j).paymentRow);
                        selectedPaymentList.remove(j);

                    } else {
                        j++;
                    }
                }

                if (!paymntNedToDelete.IsEmpty()) {
                    delete paymntNedToDelete;
                }
            }

        } catch (Exception ex) {
            system.debug('===Exception===deletePayment' + ex.getMessage() + 'at line ' + ex.getlineNumber());
            WebToSFDC.notifyDev('Loan Payment Plan Class ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage());
        }
    }

//********* method to cancel selected payments in " Payments " Section. ********

    public void cancelSelectedPayment() {
        List<loan_Plan_Payment__c> paymentToUpdate = new List<loan_Plan_Payment__c>();
        try {
            for (selectedPayment l : getselectedPayments()) {
                if (l.selected == true) {
                    l.paymentRow.Status__c = 'Cancelled';
                    paymentToUpdate.add(l.paymentRow);
                }

                l.selected = false;
            }
            if (paymentToUpdate.size() > 0) {
                update paymentToUpdate;

            } else {
                createMessage(ApexPages.severity.WARNING, 'Please select at least one record.') ;
            }

        } catch (exception ex) {
            system.debug('===Exception===cancelSelectedPayment' + ex.getMessage() + 'at line ' + ex.getlineNumber());
            WebToSFDC.notifyDev('Loan Payment Plan Class ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage());

        }
    }

    //********* method to change status of selected payments to " Pending " in " Payments " Section. ********

    public void pendingSelectedPayment() {
        List<loan_Plan_Payment__c> paymentToUpdate = new List<loan_Plan_Payment__c>();
        try {
            for (selectedPayment l : getselectedPayments()) {
                if (l.selected == true) {
                    l.paymentRow.Status__c = 'pending';
                    paymentToUpdate.add(l.paymentRow);
                }

                l.selected = false;
            }
            if (paymentToUpdate.size() > 0) {
                update paymentToUpdate;

            } else {
                createMessage(ApexPages.severity.WARNING, 'Please select at least one record.') ;
            }

        } catch (exception ex) {
            system.debug('===Exception===pendingSelectedPayment' + ex.getMessage() + 'at line ' + ex.getlineNumber());
            WebToSFDC.notifyDev('Loan Payment Plan Class ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage());
        }
    }

    //****** Wrapper Class *******

    public class selectedPayment {
        public loan_Plan_Payment__c paymentRow {get; set;}
        public Boolean selected {get; set;}

        public selectedPayment(loan_Plan_Payment__c l) {
            paymentRow = l;
            selected = false;
        }
    }

    public List<selectedPayment> getselectedPayments() {
        if (selectedPaymentList == null) {
            selectedPaymentList = new list<selectedPayment>();
            for (loan_Plan_Payment__c l : payments) {
                selectedPaymentList.add(new selectedPayment(l));
            }
        }
        return selectedPaymentList;
    }

    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }
}