@isTest public class ClContractJobTestCtrl {
    
    @isTest static void MarkedAsModifiedAlert(){
        
        loan__Loan_Account__c theAccount = LibraryTest.createContractTH();
        theAccount.loan__Pmt_Amt_Cur__c = 120.00;
        update theAccount;
        
        loan__Other_Transaction__c OtherTrans = New loan__Other_Transaction__c();
        OtherTrans.loan__Loan_Account__c = theAccount.Id;
        OtherTrans.loan__Txn_Date__c = Date.today();
        OtherTrans.loan__OT_ACH_Payment_Amount__c = theAccount.loan__Pmt_Amt_Cur__c;
        OtherTrans.loan__OT_ACH_Debit_Date__c = Date.today();
        OtherTrans.loan__Transaction_Type__c = 'Payment Change';
        OtherTrans.loan__Next_Due_Generation_Date__c = Date.today();
        insert OtherTrans;
        
        test.startTest();
        
        ClContractJob job2 = new ClContractJob('MarkedAsModifiedAlert');
        database.executebatch(job2,1);
        
        test.stopTest();
        
        OtherTransactionLogs__c log = [SELECT Id, createdDate, CL_Contract__c, Email_Body__c, NotMarkedAsModified__c, System_Debug__c, Other_Loan_Transaction__c 
                                       FROM OtherTransactionLogs__c where CL_Contract__c =: theAccount.Id];
        
        system.assertEquals(true, log.NotMarkedAsModified__c);
        system.assertNotEquals(null, log.Email_Body__c);
        
      }
    
    
    @isTest static void Without_MarkedAsModifiedAlert(){
        
        loan__Loan_Account__c theAccount = LibraryTest.createContractTH();
        theAccount.loan__Pmt_Amt_Cur__c = 120.00;
        update theAccount;
        
        loan__Other_Transaction__c OtherTrans = New loan__Other_Transaction__c();
        OtherTrans.loan__Loan_Account__c = theAccount.Id;
        OtherTrans.loan__Txn_Date__c = Date.today();
        OtherTrans.loan__OT_ACH_Payment_Amount__c = theAccount.loan__Pmt_Amt_Cur__c;
        OtherTrans.loan__OT_ACH_Debit_Date__c = Date.today();
        OtherTrans.loan__Transaction_Type__c = 'Payment Change';
        OtherTrans.loan__Next_Due_Generation_Date__c = Date.today();
        insert OtherTrans;
        
        LoanModificationHistory__c mod = New LoanModificationHistory__c();
        mod.Contract__c = theAccount.Id;
        mod.Agreement_Modified_Date__c = Date.today();
        mod.Modification_Made__c = 'Term';
        mod.Modification_Reason_Text_Line__c = 'Loss of Job';
        mod.Modification_Reason__c = 'Reduced Income';
        mod.Modification_Type__c = 'Loan Workout Agreement Modification';
        mod.CreatedById = UserInfo.getUserId();
        insert mod;
        
        test.startTest();
        
        ClContractJob job2 = new ClContractJob('MarkedAsModifiedAlert');
        database.executebatch(job2,1);
        
        test.stopTest();
        
        OtherTransactionLogs__c log = [SELECT Id, createdDate, CL_Contract__c, Email_Body__c, NotMarkedAsModified__c, System_Debug__c, Other_Loan_Transaction__c 
                                       FROM OtherTransactionLogs__c where CL_Contract__c =: theAccount.Id];
        
           
        system.assertEquals(false, log.NotMarkedAsModified__c);
        system.assertEquals(null, log.Email_Body__c);
         
    }
    
    @isTest static void FinanceProfile_Not_SendAlert(){
        
        User us = [SELECT Name, ProfileId, IsActive FROM User WHERE ProfileId =: Label.Profiles_Mark_Modified_No_Alert AND IsActive = true Limit 1];
        
        System.runAs(us) {
            
            loan__Loan_Account__c theAccount = LibraryTest.createContractTH();
            theAccount.loan__Pmt_Amt_Cur__c = 120.00;
            theAccount.Modification_Type__c = 'Loan Modification - Payment Date Change';
            theAccount.Agreement_Modified_Date__c = Date.today(); //Date.Today().addDays(1);
            update theAccount;
            system.debug('aggremet date:: ' + theAccount.Agreement_Modified_Date__c);
            
            loan__Other_Transaction__c OtherTrans = New loan__Other_Transaction__c();
            OtherTrans.loan__Loan_Account__c = theAccount.Id;
            OtherTrans.loan__Txn_Date__c = Date.today();
            OtherTrans.loan__OT_ACH_Payment_Amount__c = theAccount.loan__Pmt_Amt_Cur__c;
            OtherTrans.loan__OT_ACH_Debit_Date__c = Date.today();
            OtherTrans.loan__Transaction_Type__c = 'Payment Change';
            OtherTrans.loan__Next_Due_Generation_Date__c = Date.today();
            insert OtherTrans;
            
            
            
            OtherTransactionLogs__c log = [SELECT Id, createdDate, CL_Contract__r.Modification_Type__c, CL_Contract__r.Agreement_Modified_Date__c, CL_Contract__c, CreatedById, Created_By_Name__c, Email_Body__c, Modification_Type__c, CL_Contract__r.Name, NotMarkedAsModified__c, System_Debug__c, Other_Loan_Transaction__c  
                                           FROM OtherTransactionLogs__c where CL_Contract__c =: theAccount.Id];
            
            
         
           
            test.startTest();
            
            ClContractJob job2 = new ClContractJob('MarkedAsModifiedAlert');
            database.executebatch(job2,1);
            
            test.stopTest();
            
            system.assertEquals(false, log.NotMarkedAsModified__c);
            
              
          }
        
        
    }
    
    
    
    
    
    @isTest static void Programing_MarkedAsModifiedAlert(){
  		// This test is only for the Schedule job time
        //System.schedule('ClContractJob - MarkedAsModifiedAlert', '0 30 7 1/1 * ? *', new ClContractJob('MarkedAsModifiedAlert'));
    }

}