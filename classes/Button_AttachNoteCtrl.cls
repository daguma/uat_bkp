public with sharing class Button_AttachNoteCtrl {

    @AuraEnabled
    public static Boolean save(Id pParentId, String pTitle, String pBody) {
        try{
            Note a = new Note(parentId = pParentId, title = pTitle, body = pBody);
            insert a;
            Boolean dummy = true;
            dummy = null;
        }catch(Exception e){
            return false;
        }
        return true;
    }
}