public with sharing class DealAutomaticEmploymentTypeCtrl {
    public DealAutomaticEmploymentTypeCtrl() {

    }

    /**
    * Get all employment types from the Deal Automatic Attributes
    * @return List of Deal_Automatic_Attributes__c
    */
    public static String getEmploymentTypes() {
        return JSON.serialize([SELECT Id__c, Name, Display_Name__c, Usage__c
                               FROM Deal_Automatic_Attributes__c
                               WHERE Usage__c LIKE :DealAutomaticAssignmentUtils.EMPLOYMENT_TYPE_USAGE
                               ORDER BY Display_Name__c]);
    }

    /**
    * [saveUsers description]
    * @param  attribute to save the selected users
    * @param  usersGroups    to save in the specific category
    * @return Save the selected users in the Deal_Automatic_Assignment_Data__c object
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> saveUsers(Deal_Automatic_Attributes__c attribute,
            List<DealAutomaticUserGroup> newUsersGroups,
            List<DealAutomaticUserGroup> oldUserGroups) {
        return DealAutomaticAssignmentUtils.saveUsers(attribute, newUsersGroups, oldUserGroups);
    }

    /**
    * [getUsersGroupsBySource description]
    * @param  employment [description]
    * @return  List of DealAutomaticUserGroup
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> getUsersGroupsByEmployment(Deal_Automatic_Attributes__c employment) {
        return DealAutomaticAssignmentUtils.getUsersGroupsByCategory(employment);
    }
}