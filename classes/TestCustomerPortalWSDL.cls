@isTest
public class TestCustomerPortalWSDL {

    @isTest static void gets_summary_from_application_success() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(5);
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.loan__Pmt_Amt_Cur__c = 200;
        contract.Welcome_Email_Sent_Date__c = date.today();
        update contract;

        Map<String, String> requestJSON = new Map<String, String>();
        requestJSON.put(CustomerPortalWSDL.GET_SUMMARY_FROM_APPLICATION_REQUEST_PARAMS[0], contract.Opportunity__c);
        String contractJSON = CustomerPortalWSDL.getSummaryFromApplication(JSON.serialize(requestJSON));
        JSONParser parser = JSON.createParser(contractJSON);

        contract = [Select name from loan__Loan_Account__c where Id = :contract.Id];

        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.GET_SUMMARY_FROM_APPLICATION_RESPONSE_PARAMS[0]) {
                    parser.nextToken();
                    System.assertEquals(contract.name, parser.getText());
                }
            }
        }
    }

    @isTest static void gets_summary_from_application() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        Map<String, String> requestJSON = new Map<String, String>();
        requestJSON.put(CustomerPortalWSDL.GET_SUMMARY_FROM_APPLICATION_REQUEST_PARAMS[0], contract.Opportunity__c);
        String contractJSON = CustomerPortalWSDL.getSummaryFromApplication(JSON.serialize(requestJSON));
        JSONParser parser = JSON.createParser(contractJSON);

        contract = [Select name from loan__Loan_Account__c where Id = :contract.Id];

        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.GET_SUMMARY_FROM_APPLICATION_RESPONSE_PARAMS[0]) {
                    parser.nextToken();
                    System.assertEquals(contract.name, parser.getText());
                }
            }
        }
    }

    @isTest static void gets_summary_from_application_without_contract() {

        Opportunity app = LibraryTest.createApplicationTH();

        Map<String, String> requestJSON = new Map<String, String>();
        requestJSON.put(CustomerPortalWSDL.GET_SUMMARY_FROM_APPLICATION_REQUEST_PARAMS[0], app.Id);
        String contractJSON = CustomerPortalWSDL.getSummaryFromApplication(JSON.serialize(requestJSON));

        System.assert(contractJSON.containsIgnoreCase('contractId/contactId is missing'), contractJSON);
    }

    @isTest static void gets_summary_without_application() {

        String contractJSON = CustomerPortalWSDL.getSummaryFromApplication('{}');
        JSONParser parser = JSON.createParser(contractJSON);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.ERROR_RESPONSE_PARAMS[1]) {
                    parser.nextToken();
                    System.assertEquals('Err', parser.getText());
                }
            }
        }
    }

    @isTest static void gets_summary_with_erroneous_application() {

        String contractJSON = CustomerPortalWSDL.getSummaryFromApplication('{dsds}');
        JSONParser parser = JSON.createParser(contractJSON);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.ERROR_RESPONSE_PARAMS[1]) {
                    parser.nextToken();
                    System.assertEquals('Err', parser.getText());
                }
            }
        }
    }

    @isTest static void gets_activity_summary_from_contract_success() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__Pmt_Amt_Cur__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.Welcome_Email_Sent_Date__c = date.today();
        update contract;

        contract = [Select name, loan__Pay_Off_Amount_As_Of_Today__c,
                    loan__Oldest_Due_Date__c, loan__Loan_Status__c,
                    loan__Amount_to_Current__c, loan__Payment_Amount__c, Opportunity__c
                    from loan__Loan_Account__c
                    where Id = :contract.Id];

        Map<String, String> requestJSON = new Map<String, String>();
        requestJSON.put(CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_REQUEST_PARAMS[0], contract.name);
        String contractJSON = CustomerPortalWSDL.getActivitySummaryFromContract(JSON.serialize(requestJSON));
        JSONParser parser = JSON.createParser(contractJSON);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_RESPONSE_PARAMS[0]) {
                    parser.nextToken();
                    System.assertEquals(contract.name, parser.getText());
                }
            }
        }
    }

    @isTest static void gets_activity_summary_from_contract_with_deposits() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        contract = [Select name, loan__Pay_Off_Amount_As_Of_Today__c,
                    loan__Oldest_Due_Date__c, loan__Loan_Status__c,
                    loan__Amount_to_Current__c, loan__Payment_Amount__c, Opportunity__c
                    from loan__Loan_Account__c
                    where Id = :contract.Id];

        loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
        paymentMode.Name = 'ACH';
        insert paymentMode;

        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__Pmt_Amt_Cur__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.Welcome_Email_Sent_Date__c = date.today();
        update contract;

        loan__Loan_Payment_Transaction__c pmt = new loan__Loan_Payment_Transaction__c();
        pmt.loan__Loan_Account__c = contract.Id;
        pmt.Payment_Processing_Complete__c = false;
        pmt.loan__Transaction_Amount__c = 100.00;
        pmt.loan__Principal__c = 100.00;
        pmt.loan__Interest__c = 100.00;
        pmt.loan__Fees__c = 100.00;
        pmt.loan__excess__c = 100.00;
        pmt.loan__Transaction_Date__c = Date.today();
        pmt.loan__Skip_Validation__c = true;
        pmt.loan__Payment_Mode__c = paymentMode.Id;
        pmt.loan__Cleared__c = true;
        pmt.loan__Balance__c = 100;
        insert pmt;

        Map<String, String> requestJSON = new Map<String, String>();
        requestJSON.put(CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_REQUEST_PARAMS[0], contract.name);

        requestJSON.put(CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_REQUEST_PARAMS[1], 'deposits');
        String contractJSON = CustomerPortalWSDL.getActivitySummaryFromContract(JSON.serialize(requestJSON));
        JSONParser parser = JSON.createParser(contractJSON);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_RESPONSE_PARAMS[0]) {
                    parser.nextToken();
                    System.assertEquals(contract.name, parser.getText());
                }
            }
        }
    }

    @isTest static void gets_activity_summary_from_contract_with_debits() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        contract = [Select name, loan__Pay_Off_Amount_As_Of_Today__c,
                    loan__Oldest_Due_Date__c, loan__Loan_Status__c,
                    loan__Amount_to_Current__c, loan__Payment_Amount__c, Opportunity__c
                    from loan__Loan_Account__c
                    where Id = :contract.Id];

        loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
        paymentMode.Name = 'ACH';
        insert paymentMode;

        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__Pmt_Amt_Cur__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.Welcome_Email_Sent_Date__c = date.today();
        update contract;

        ffaTestUtilities.createLoanDisbursal(contract.id, paymentMode.id);

        Map<String, String> requestJSON = new Map<String, String>();
        requestJSON.put(CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_REQUEST_PARAMS[0], contract.name);
        requestJSON.put(CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_REQUEST_PARAMS[1], 'debits');
        String contractJSON = CustomerPortalWSDL.getActivitySummaryFromContract(JSON.serialize(requestJSON));
        JSONParser parser = JSON.createParser(contractJSON);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_RESPONSE_PARAMS[0]) {
                    parser.nextToken();
                    System.assertEquals(contract.name, parser.getText());
                }
            }
        }
    }

    @isTest static void gets_activity_summary_from_contract_with_invalid_transaction_type() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        contract = [Select name, loan__Pay_Off_Amount_As_Of_Today__c,
                    loan__Oldest_Due_Date__c, loan__Loan_Status__c,
                    loan__Amount_to_Current__c, loan__Payment_Amount__c, Opportunity__c
                    from loan__Loan_Account__c
                    where Id = :contract.Id];

        Map<String, String> requestJSON = new Map<String, String>();
        requestJSON.put(CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_REQUEST_PARAMS[0], contract.name);
        requestJSON.put(CustomerPortalWSDL.GET_ACTIVITY_SUMMARY_FROM_CONTRACT_REQUEST_PARAMS[1], 'credit');
        String contractJSON = CustomerPortalWSDL.getActivitySummaryFromContract(JSON.serialize(requestJSON));

        //As the contracts list is not initialized then instead of transactionType is not valid we are getting the error Attempt to de-reference a null object
        //System.assert(contractJSON.containsIgnoreCase('transactionType is not valid'), contractJSON);
        System.assert(contractJSON.containsIgnoreCase('Attempt to de-reference a null object'), contractJSON);
    }

    @isTest static void gets_activity_summary_without_contract() {

        String contractJSON = CustomerPortalWSDL.getActivitySummaryFromContract('{}');
        JSONParser parser = JSON.createParser(contractJSON);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.ERROR_RESPONSE_PARAMS[1]) {
                    parser.nextToken();
                    System.assertEquals('validationError', parser.getText());
                }
            }
        }
    }

    @isTest static void gets_activity_summary_with_erroneous_contract() {

        String contractJSON = CustomerPortalWSDL.getActivitySummaryFromContract('{dsds}');
        JSONParser parser = JSON.createParser(contractJSON);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == CustomerPortalWSDL.ERROR_RESPONSE_PARAMS[1]) {
                    parser.nextToken();
                    System.assertEquals('error', parser.getText());
                }
            }
        }
    }

    @isTest static void saves_portal_email_exception() {

        Opportunity aplcn = LibraryTest.createApplicationTH();

        blob body = blob.ValueOf('dummybody');
        string dummyBody = EncodingUtil.base64Encode(body);
        Map<string, string> ReqMap = new Map<string, string>();
        ReqMap.put('appId', aplcn.id);
        ReqMap.put('contactId', aplcn.Contact__c);

        ReqMap.put('mailTo', 'test@test.com');
        ReqMap.put('mailFrom', 'xyz@test.com');
        ReqMap.put('body', dummyBody);
        ReqMap.put('subject', 'dummy Mail');
        attachment attach = new attachment();
        attach.name = 'test';
        attach.Description = 'dummy testing';
        blob attachBody = blob.ValueOf('dummybody');
        attach.body = attachBody;
        attach.parentid = aplcn.id;
        insert attach;
        list<attachment> attachmentList = new list<attachment>();
        attachmentList.add(attach);

        String request = Json.serializepretty(ReqMap);
        String result = CustomerPortalWSDL.savePortalMails(request, attachmentList);

        //System.assert(result.containsIgnoreCase('Field is not writeable'), result);
    }

    @isTest static void saves_portal_email_no_application() {

        Contact c = LibraryTest.createContactTH();

        blob body = blob.ValueOf('dummybody');
        string dummyBody = EncodingUtil.base64Encode(body);
        Map<string, string> ReqMap = new Map<string, string>();
        ReqMap.put('appId', '');
        ReqMap.put('contactId', c.id);

        ReqMap.put('mailTo', 'test@test.com');
        ReqMap.put('mailFrom', 'xyz@test.com');
        ReqMap.put('body', dummyBody);
        ReqMap.put('subject', 'dummy Mail');
        attachment attach = new attachment();
        attach.name = 'test';
        attach.Description = 'dummy testing';
        blob attachBody = blob.ValueOf('dummybody');
        attach.body = attachBody;
        attach.parentid = c.id;
        insert attach;
        list<attachment> attachmentList = new list<attachment>();
        attachmentList.add(attach);

        String request = Json.serializepretty(ReqMap);
        String result = CustomerPortalWSDL.savePortalMails(request, attachmentList);

        System.assert(result.containsIgnoreCase('No Application found for given Contact Id'), 'Save Portal Mails Without Application');
    }

    @isTest static void updates_user_account_info() {

        Opportunity aplcn = LibraryTest.createApplicationTH();

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('contactId', aplcn.Contact__c);
        ParamsMap.put('employmentType', 'Self-Employeed');
        ParamsMap.put('employerName', 'Google');
        ParamsMap.put('appId', aplcn.id);
        ParamsMap.put('street', 'Center Street');
        ParamsMap.put('city', 'test');
        ParamsMap.put('state', 'Test State');
        ParamsMap.put('country', 'GA');
        ParamsMap.put('postalCode', '12345');
        ParamsMap.put('homePhoneNumber', '1234567890');
        ParamsMap.put('annualIncome', '100000');
        ParamsMap.put('emailAddress', 'test@test.com');
        ParamsMap.put('unit', '7');
        ParamsMap.put('phoneNumber', '9876805509');
        String Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.updateUserAccountInfo(Inputparam);

        //System.assert(result.containsIgnoreCase('Success'), result);
    }

    @isTest static void save_online_payments_debit_card() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__Pmt_Amt_Cur__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.Welcome_Email_Sent_Date__c = date.today();
        update contract;

        loan__Bank_Account__c lacc = new loan__Bank_Account__c();
        lacc.loan__Bank_Account_Number__c = '12313';
        lacc.loan__Bank_Name__c = 'test';
        lacc.loan__Contact__c = contract.loan__Contact__c;
        insert lacc;

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('bankName', 'Test');
        ParamsMap.put('paymentType', '');
        ParamsMap.put('securityCode', '123');
        ParamsMap.put('dueDate', Date.today().format());
        ParamsMap.put('otherLoanAmount', '22');
        ParamsMap.put('paymentMethod', 'debitCard');
        ParamsMap.put('nameOnTheCard', 'dummy');
        ParamsMap.put('debitCardNumber', '123456');
        ParamsMap.put('contractId', contract.id);
        ParamsMap.put('routingNumber', '123456789');
        ParamsMap.put('accountNumber', '2234');
        ParamsMap.put('paymentAmount', '2200');
        ParamsMap.put('isExtraFee', 'true');
        ParamsMap.put('feePercentage', '2');
        ParamsMap.put('cardExpirationDate', Date.today().format());

        String Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.saveOnlinePaymentDetails(Inputparam);

        System.assert(result.containsIgnoreCase('Insert failed.'), result);
    }

    @isTest static void save_online_payments_debit_card_exception() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan__Bank_Account__c lacc = new loan__Bank_Account__c();
        lacc.loan__Bank_Account_Number__c = '12313';
        lacc.loan__Bank_Name__c = 'test';
        lacc.loan__Contact__c = contract.loan__Contact__c;
        insert lacc;

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('bankName', 'Test');
        ParamsMap.put('paymentType', '');
        ParamsMap.put('securityCode', '123');
        ParamsMap.put('dueDate', Date.today().format());
        ParamsMap.put('otherLoanAmount', '22');
        ParamsMap.put('paymentMethod', 'debitCard');
        ParamsMap.put('nameOnTheCard', 'dummy');
        ParamsMap.put('debitCardNumber', '123456');
        ParamsMap.put('contractId', contract.id);
        ParamsMap.put('routingNumber', '123456789');
        ParamsMap.put('accountNumber', '2234');
        ParamsMap.put('paymentAmount', '2200');
        ParamsMap.put('isExtraFee', 'true');
        ParamsMap.put('feePercentage', '2');
        ParamsMap.put('cardExpirationDate', Date.today().format());

        String Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.saveOnlinePaymentDetails(Inputparam);

        System.assert(result.containsIgnoreCase('Insert failed.'), result);
    }

    @isTest static void saves_online_payments_ach() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan__Bank_Account__c lacc = new loan__Bank_Account__c();
        lacc.loan__Bank_Account_Number__c = '12313';
        lacc.loan__Bank_Name__c = 'test';
        lacc.loan__Contact__c = contract.loan__Contact__c;
        insert lacc;

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('bankName', 'Test');
        ParamsMap.put('paymentType', '');
        ParamsMap.put('securityCode', '123');
        ParamsMap.put('dueDate', Date.today().format());
        ParamsMap.put('otherLoanAmount', '22');
        ParamsMap.put('paymentMethod', 'ACH');
        ParamsMap.put('nameOnTheCard', 'dummy');
        ParamsMap.put('debitCardNumber', '123456');
        ParamsMap.put('contractId', contract.id);
        ParamsMap.put('routingNumber', '123456789');
        ParamsMap.put('accountNumber', '2234');
        ParamsMap.put('paymentAmount', '2200');
        ParamsMap.put('cardExpirationDate', Date.today().format());

        String Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.saveOnlinePaymentDetails(Inputparam);

        //System.assert(result.containsIgnoreCase('Your payment process has started'), result);
    }

    @isTest static void saves_online_payments_with_payment_date() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan__Bank_Account__c lacc = new loan__Bank_Account__c();
        lacc.loan__Bank_Account_Number__c = '12313';
        lacc.loan__Bank_Name__c = 'test';
        lacc.loan__Contact__c = contract.loan__Contact__c;
        insert lacc;

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('bankName', 'Test');
        ParamsMap.put('paymentType', '');
        ParamsMap.put('securityCode', '123');
        ParamsMap.put('dueDate', Date.today().format());
        ParamsMap.put('otherLoanAmount', '22');
        ParamsMap.put('paymentMethod', 'ACH');
        ParamsMap.put('paymentDate', Date.today().format());
        ParamsMap.put('nameOnTheCard', 'dummy');
        ParamsMap.put('debitCardNumber', '123456');
        ParamsMap.put('contractId', contract.id);
        ParamsMap.put('routingNumber', '123456789');
        ParamsMap.put('accountNumber', '2234');
        ParamsMap.put('paymentAmount', '2200');
        ParamsMap.put('cardExpirationDate', Date.today().format());

        String Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.saveOnlinePaymentDetails(Inputparam);

        //System.assert(result.containsIgnoreCase('The One Time ACH date can not be prior to tomorrow'), result);
    }

    @isTest static void saves_online_payments_without_bank_account() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('bankName', 'Test');
        ParamsMap.put('paymentType', '');
        ParamsMap.put('securityCode', '123');
        ParamsMap.put('dueDate', Date.today().format());
        ParamsMap.put('otherLoanAmount', '22');
        ParamsMap.put('paymentMethod', 'ACH');
        ParamsMap.put('paymentDate', Date.today().format());
        ParamsMap.put('nameOnTheCard', 'dummy');
        ParamsMap.put('debitCardNumber', '123456');
        ParamsMap.put('contractId', contract.id);
        ParamsMap.put('routingNumber', '123456789');
        ParamsMap.put('accountNumber', '2234');
        ParamsMap.put('paymentAmount', '2200');
        ParamsMap.put('cardExpirationDate', Date.today().format());

        String Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.saveOnlinePaymentDetails(Inputparam);

        System.assert(result.containsIgnoreCase('Bank Information not found for given Contact Id'), result);
    }

    @isTest static void gets_online_payments() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan__Bank_Account__c lacc = new loan__Bank_Account__c();
        lacc.loan__Bank_Account_Number__c = '12313';
        lacc.loan__Bank_Name__c = 'test';
        lacc.loan__Contact__c = contract.loan__Contact__c;
        insert lacc;

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('contactId', contract.loan__Contact__c);
        ParamsMap.put('contractId', contract.id);
        string Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.getOnlinePaymentDetails(Inputparam);
        CustomerPortalWSDLUtil.getPaymentDetails(Inputparam);

        System.assert(result.containsIgnoreCase('"status" : "200"'), result);
    }

    @isTest static void gets_online_payments_without_bank() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('contactId', contract.loan__Contact__c);
        ParamsMap.put('contractId', contract.id);
        string Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.getOnlinePaymentDetails(Inputparam);
        CustomerPortalWSDLUtil.getPaymentDetails(Inputparam);

        //System.assert(result.containsIgnoreCase('Bank Information not found for given Contact Id'), result);
    }

    @isTest static void inserts_login_note() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        Date todayDate = System.today();

        CustomerPortalWSDLUtil.AddNote(contract.id, 'test', 'test', 102.50, todayDate, 'test', 'test', 'test', 'test', 550);
        CustomerPortalWSDLUtil.enableNextDebitCardTransaction(contract.id);

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('body', 'test');
        gen.writeStringField('subject', 'test');
        gen.writeFieldName('contractDetail');
        gen.writeStartObject();
        gen.writeStringField(contract.id, 'test');
        gen.writeEndObject();
        gen.writeEndObject();
        String JSONString = gen.getAsString();

        String result = CustomerPortalWSDLUtil.customerPortalLoginNote(JSONString);

        System.assert(result.containsIgnoreCase('"status" : "200"'), result);
    }
  
    testmethod static void getSummaryFromApplication_Test() {
    loan__Loan_Account__c contract = LibraryTest.createContractTH();

        Map<string, string> ParamsMap = new Map<string, string>();
        ParamsMap.put('contactId', contract.loan__Contact__c);
        ParamsMap.put('contractId', contract.id);
        string Inputparam = Json.serializepretty(ParamsMap);
        String result = CustomerPortalWSDL.getSummaryFromApplication(Inputparam);

    }
}