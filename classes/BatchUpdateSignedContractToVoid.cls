global class BatchUpdateSignedContractToVoid implements Database.Batchable<sObject>,Schedulable {
    public void execute(SchedulableContext sc) { 
       BatchUpdateSignedContractToVoid batchapex = new BatchUpdateSignedContractToVoid();
       id batchprocessid = Database.executebatch(batchapex);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){       
       Integer contractVoidDays = Integer.valueOf(Label.ContractVoidDays);      
       Date cutOffDate = System.today().addDays(( 0 - contractVoidDays));                       
       String query = 'Select id,Contract_Signed_Date__c from genesis__Applications__c where  genesis__Status__c <> \'Funded\' AND Contract_Signed_Date__c <=: cutOffDate';
       return Database.getQueryLocator(query);
    }
    
    public BatchUpdateSignedContractToVoid(){}
    
    global void execute(Database.BatchableContext BC, List<genesis__Applications__c> scope){
        try { 
            Set<id> appIdSet = new Set<id>();          
          
            for(genesis__Applications__c app: scope){
                appIdSet.add(app.id);
            }
            ValidationTabCtrl.UpdateSignedContractToVoid(appIdSet);
        }
        catch(Exception e) {
            SalesForceUtil.EmailByUserId('Batch to Update Signed Contract To Void Contract Error', 
                                        'Exception in Batch to Update Signed Contract To Void\n' + 
                                        e.getMessage()+ '\n' + e.getStackTraceString() + '\n' + 'Line = ' + e.getLineNumber(), 
                                        LP_Custom__c.getOrgDefaults().Developer_Alert__c);
       }   
    }
    
    global void finish(Database.BatchableContext BC) {
        SalesForceUtil.EmailByUserId('Batch to Update Signed Contract To Void Completed', 
                                    'Batch to Update Signed Contract To Void completed for the current batch.', 
                                    LP_Custom__c.getOrgDefaults().Developer_Alert__c);
    }
}