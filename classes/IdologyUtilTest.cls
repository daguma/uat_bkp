@isTest public class IdologyUtilTest {
    
    @isTest static void test_responses(){
        
        IdologyUtil.IDologyQuestion idolgyQuestions = new IdologyUtil.IDologyQuestion('question', 'typeQuestion', 1);
        
        List<IdologyUtil.IDologyQuestion> questionsList = new List<IdologyUtil.IDologyQuestion>();
        questionsList.add(idolgyQuestions);
        
        Dom.XMLNode response1 = IdologyUtil.GenerateTestingData1();
        System.assertNotEquals(null, response1);
        Dom.XMLNode response2 = IdologyUtil.GenerateTestingData2();
        System.assertNotEquals(null, response2);
        Dom.XMLNode response3 = IdologyUtil.GenerateTestingData3();
        System.assertNotEquals(null, response3);
        Boolean result = IdologyUtil.runningInASandbox();
        IdologyUtil.ProcessResultXml(response1);
        IdologyUtil.ProcessAnswersResultXml(response2);
        
        Contact con = LibraryTest.createContactTH();
		
		Test.startTest();        
        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'TestCookie');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.getQuestions.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        IdologyUtil.CallApiToGetQuestions(con, false);
		IdologyUtil.CallApiToProcessQuestions('12348', questionsList, false);
        
        Test.stopTest();
        
        IdologyUtil.SaveIDologyRequest('Test', 'qualifiers', 4, 'result', 'message', con.Id, 'debug');
        
        IdologyUtil.GetParametersAnswersForApi('12348', questionsList, false);
    }

}