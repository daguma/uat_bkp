public with sharing class ProxyApiCreditBRMSResponse {
    @AuraEnabled public Long rulesExecutionStartDateTime{get;set;}
    @AuraEnabled public Long rulesExecutionStopDateTime{get;set;}
    @AuraEnabled public String sessionId{get;set;}
    @AuraEnabled public String sequenceId{get;set;}
    @AuraEnabled public String productId{get;set;}
    @AuraEnabled public String channelId{get;set;}
    @AuraEnabled public String executionType{get;set;}
    @AuraEnabled public List<ProxyApiCreditReportRuleEntity> rulesList {get; set;}
    @AuraEnabled public List<ProxyApiCreditReportScoringModelEntity> scoringModels {get; set;}
    @AuraEnabled public List<ProxyApiCreditReportH2OModels > h2oModels {get; set;}
    @AuraEnabled public String scorecardAcceptance{get;set;}
    @AuraEnabled public String finalGrade{get;set;}
    @AuraEnabled public String automatedDecision{get;set;}
    @AuraEnabled public String errorMessage{get;set;}
    @AuraEnabled public String errorDetails{get;set;}
    @AuraEnabled public String ruleVersion {get; set;}
    @AuraEnabled public EmailAgeResponseEntity emailAgeResponse {get;set;}
    @AuraEnabled public ProxyApiClarityResponse clarityResponse {get;set;}
    @AuraEnabled public list<ProxyApiCreditReportBrmsErros> errors {get;set;}
    
    @AuraEnabled public String deBRMS_ContractStatus {get; set;}
    @AuraEnabled public String deSF_AcceptanceStatus {get; set;}
    @AuraEnabled public String deApplicationRisk{get; set;}
    @AuraEnabled public String deRuleViolated {get;set;}

    @AuraEnabled public String deStrategy_Type {get;set;}

    @AuraEnabled public ProxyApiLexisNexisResponse lexisNexisResponse {get; set;}
    
    @AuraEnabled public GiactResponseParse giactResponse {get; set;}    //GIACT changes
    
    public ProxyApiCreditBRMSResponse() { }
    
    /*
    public Boolean errorResponse {
        get {
            return errorMessage != null;
        }
        set;
    }
    */
    @AuraEnabled public String autoDecision {
        get {
            //if (this.rulesList == null && this.errors != null && this.errors.size() > 0) return 'No';
            if (this.rulesList == null) return 'No';
             
            List<ProxyApiCreditReportRuleEntity> criteriaRuleAttributeList = getCriteriaRuleAttributeList();
            Map<String,list<ProxyApiCreditReportRuleEntity>> ruleMap = new Map<String,list<ProxyApiCreditReportRuleEntity>>();

            String result = 'Yes';
            
            List<ProxyApiCreditReportRuleEntity> ruleList;
            if (String.IsNotEmpty(sequenceId) && sequenceId == '1' && String.IsNotEmpty(deBRMS_ContractStatus)  && deBRMS_ContractStatus == 'Credit Qualified') return result;

            for (ProxyApiCreditReportRuleEntity item : criteriaRuleAttributeList ) {
                If(String.isNotBlank(item.deIsDisplayed) && item.deIsDisplayed.equalsignorecase('Yes') ) {
                    if(ruleMap.containsKey(item.deRuleStatus)) {
                        ruleList = ruleMap.get(item.deRuleStatus);
                    }
                    else {
                        ruleList = new List<ProxyApiCreditReportRuleEntity>();
                    }
                    ruleList.add(item);
                    ruleMap.put(item.deRuleStatus, ruleList);
                }
            }
            
            if (ruleMap.containsKey('Fail')) {
                result = 'No';
            }
            return result;
        }
        set;
    }

    @AuraEnabled public String scAcceptance {
       get {
            If(String.isNotBlank(scorecardAcceptance) && scorecardAcceptance.equalsIgnoreCase(CreditReport.DECISION_YES) )
                return 'Y';
            else 
                return 'N';
       }set;
   }
    @AuraEnabled public DateTime ruleExecutionStartDateTime {
        get {
            if (rulesExecutionStartDateTime == null) return null;

            return DateTime.newInstance(rulesExecutionStartDateTime);
        }
        set;
    }

    @AuraEnabled public DateTime ruleExecutionEndDateTime {
        get {
            if (rulesExecutionStopDateTime == null) return null;

            return DateTime.newInstance(rulesExecutionStopDateTime);
        }
        set;
    }
    
    /**
    * Get the rules approve name
    * @return approve name
    */
    @AuraEnabled public String ruleAttributesApprovedByName {
        get {
            if (this.rulesList == null) return ' - ';

            List<ProxyApiCreditReportRuleEntity> criteriaRuleAttributeList = getCriteriaRuleAttributeList();

            String result = 'Automated';

            for (ProxyApiCreditReportRuleEntity item : criteriaRuleAttributeList) {
                if(item.approvedBy <> null) {
                    if (item.approvedBy.equals('Dig Deeper')) {
                        result = 'Automated by Dig Deeper';
                        break;
                    } else if (!item.approvedBy.equals('system')) {
                        result = 'Manually Overriden by ' + item.approvedBy;
                        break;
                    }
                }
            }
            return result;
        }
        set;
    }
    
     /**
    * Get the list of rules to be displayed in the criteria values
    * @return An rule entity list
    */
    public List<ProxyApiCreditReportRuleEntity> getCriteriaRuleAttributeList() {
        if (this.rulesList == null) return null;
        List<ProxyApiCreditReportRuleEntity> result = new List<ProxyApiCreditReportRuleEntity>();
        Integer length = rulesList.size();
        for (Integer i = 0; i < length; i++) {
            ProxyApiCreditReportRuleEntity ruleEntity = rulesList.get(i);
            result.add(ruleEntity);
        }
        return result;
    }
}