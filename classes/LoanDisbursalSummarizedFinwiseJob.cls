global class LoanDisbursalSummarizedFinwiseJob implements Schedulable  {
    global void execute (SchedulableContext sc) {
        try {
            LoanDisbursalSummarizedByLine t = new LoanDisbursalSummarizedByLine();
            t.calculateDates();
            t.generateFinwise();
            WebToSFDC.notifyDev('Finwise disbursal file generation', 'File generate succesfully');
        }catch (Exception e) {  WebToSFDC.notifyDev('Finwise disbursal file generation', e.getMessage() + '\n' + e.getLineNumber() + '\n' + e.getStackTraceString());}
    }
}