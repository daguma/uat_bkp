@isTest 
public class ProxyApiCreditReportCoeffEntityTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ProxyApiCreditReportCoefficientEntity ce = new ProxyApiCreditReportCoefficientEntity ();
        Test.stopTest();

        System.assertEquals(null, ce.coefficientName);
        System.assertEquals(null, ce.coefficientValue);
    }
    
     static testmethod void validate_assign() {
        Test.startTest();
        ProxyApiCreditReportCoefficientEntity  ce = new ProxyApiCreditReportCoefficientEntity ();
        Test.stopTest();
        ce.coefficientName = 'Test coeff';
        ce.coefficientValue = 'Test value';
        System.assertEquals('Test coeff', ce.coefficientName);
        System.assertEquals('Test value', ce.coefficientValue);
     }
}