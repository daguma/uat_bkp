public class EmpInfoQueryVerificationResponseParams {
     public string id{get;set;}
     public string verification_type{get;set;}
     public string permissible_purpose{get;set;}
     public string source_id{get;set;}
     public string order_id{get;set;}
     public string consent_date{get;set;}
     public string request_status{get;set;}
     public string request_status_description{get;set;}
     public string third_party_name{get;set;}
     public Long request_fullfillment_time{get;set;}
     public string fill_strategy{get;set;}
     public string message{get;set;}
     public string errorCode{get;set;}
     public string error{get;set;}
     public Long created{get;set;}
     public employerDetails employer{get;set;}
     public employeeDetails employee{get;set;}
    
    
    public class employerDetails {
    public string name{get;set;}
     public string email{get;set;}
     public string phone{get;set;}
     public string fax{get;set;}
     public string website{get;set;}
     public adressDetails address{get;set;}
    }
    public class adressDetails{
     
     public string street1{get;set;}
     public string street2{get;set;}
     public string city{get;set;}
     public string state{get;set;}
     public string zip{get;set;}
     public string county{get;set;}
     public string country{get;set;}
   }
    public class employeeDetails {
    public adressDetails address{get;set;}
     public string first_name{get;set;}
     public string last_name{get;set;}
     public string middle_initials{get;set;}
     public string limited_ssn{get;set;}
     public string ssn{get;set;}
     public string email{get;set;}
     public string phone{get;set;}
     public string employee_consent_id{get;set;}     
     public employmentDetails employment{get;set;}
     
    }
    public class employmentDetails{
     
     public string department{get;set;}
     public incomeDetails income{get;set;}
     public string job_title{get;set;}
     public string work_location{get;set;}
     public string employment_status{get;set;}
     public string hire_date{get;set;}
     public string status_start_date{get;set;}
     public string termination_reason{get;set;}
     public string eligible_for_rehire{get;set;}
     public string employer_comments{get;set;}
   }
    public class incomeDetails{
        public Decimal rate_of_pay{get;set;}
        public string pay_frequency{get;set;}
        public string pay_frequency_description{get;set;}
        public Decimal avg_weekly_hours_worked{get;set;}
        public Decimal last_payraise_ammount{get;set;}
        public Decimal next_payraise_ammount{get;set;}
        public string most_recent_pay_date{get;set;}
        public List<earningsDetails> earnings{get;set;}
    }
    public class earningsDetails {
     public Integer year{get;set;}
     public Decimal base{get;set;}
     public Decimal overtime{get;set;}
     public Decimal bonus{get;set;}
     public Decimal commission{get;set;}
     public Decimal stocks{get;set;}
     public Decimal other{get;set;}
     public Decimal total{get;set;}
     
    }
}