@isTest public class DisqualifierScorecardCtrlTest {

    @isTest static void test() {
        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        Contact con = new Contact(LastName = 'TestContact', Firstname = 'David', Email = 'test@gmail2.com', MailingState = 'GA');
        insert con;

        Opportunity app = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), StageName = 'Qualification', Contact__c = con.Id, Status__c = 'NEW - ENTERED');
        insert app;
        
                CreditReport.set(app.id, LibraryTest.fakeBrmsRulesResponse(app.Id, true, '1', true));


        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Acceptance__c = 'Y';
        score.Opportunity__c = app.id;
        score.ProdAnnualIncome__c = -1.041877;
        score.ProdCreditScore__c = 0;
        score.ProdDTI_Ratio__c = 0.1;
        score.ProdHCNC__c = 1;
        score.ProdInquiries_last_6_months__c = 1;
        score.ProdInst_Trades_Opened__c = 1;
        score.ProdSelfEmployed__c = 1;
        score.ProdTotal_Trade_Lines__c = 10;
        score.Logit__c = -1.79;
        score.Unit_Bad_Probability__c = 0.143;
        score.Dollar_Rate_Bad_Probability__c = 7.15;
        score.FICO_Score__c = 600;
        score.Contact__c = con.id;
        insert score;

        DisqualifierScorecardCtrl.getDisqualifierScorecardEntity(app.Id);

        DisqualifierScorecardCtrl.DisqualifierScorecardEntity dd = new DisqualifierScorecardCtrl.DisqualifierScorecardEntity('testError');
        DisqualifierScorecardCtrl.DisqualifierScorecardEntity dd1 = new DisqualifierScorecardCtrl.DisqualifierScorecardEntity();
        dd1.addWarning('warning');
    }

    @isTest static void test2(){
        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        DisqualifierScorecardCtrl.DisqualifierScorecardEntity  t = new DisqualifierScorecardCtrl.DisqualifierScorecardEntity ();
        t.errorMessage = 'test';
        
        Opportunity opp = LibraryTest.createApplicationTH();



        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Acceptance__c = 'Y';
        score.Opportunity__c = opp.id;
        score.ProdAnnualIncome__c = -1.041877;
        score.ProdCreditScore__c = 0;
        score.ProdDTI_Ratio__c = 0.1;
        score.ProdHCNC__c = 1;
        score.ProdInquiries_last_6_months__c = 1;
        score.ProdInst_Trades_Opened__c = 1;
        score.ProdSelfEmployed__c = 1;
        score.ProdTotal_Trade_Lines__c = 10;
        score.Logit__c = -1.79;
        score.Unit_Bad_Probability__c = 0.143;
        score.Dollar_Rate_Bad_Probability__c = 7.15;
        score.FICO_Score__c = 600;
        score.Contact__c = opp.Contact__c;
        insert score;

        
        DisqualifierScorecardCtrl.DisqualifierScorecardEntity de = DisqualifierScorecardCtrl.getDisqualifierScorecardEntity(opp.Id);
        system.assertNotEquals(null, de);

    }

}