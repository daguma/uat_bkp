global with sharing class FTReportingUtil{
    
    static void addNode(Xmlstreamwriter xmlW, String Name, String value) {
        FactorTrustUtil.addNode(xmlW, Name, Value);
    }
    
    
    public static Logging__c FactorTrustReporting(string Obj,string ObjId,string WebServcName,string type, string ssn, string LoanDate, string DueDate, string PaymAmnt, string Balance, string BAccNo, string TranDate, string LoanId, string AppId, string fName, string lName, date DOB, string street, string city, string state, string Zip, string country, string Phone, String rollOvernNum){
        
        Xmlstreamwriter xmlW = new Xmlstreamwriter();
        xmlW.writeStartDocument('utf-8','1.0');
        xmlW.writeStartElement('soap12','Envelope', ''); 
        xmlW.writeAttribute(null,null,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        xmlW.writeAttribute(null,null,'xmlns:xsd','http://www.w3.org/2001/XMLSchema');
        xmlW.writeAttribute(null,null,'xmlns:soap12','http://www.w3.org/2003/05/soap-envelope');
        
        xmlW.writeStartElement('soap12','Body', '');
        
        xmlW.writeStartElement(null,'ReportLoan', null);
        xmlW.writeAttribute(null,null,'xmlns','http://www.factortrust.com/'); 
        
        xmlW.writeStartElement(null,'LoanRecord', null); 
        FactorTrustUtil.createHeader(xmlW);
        
        xmlW.writeStartElement(null,'LoanInfo',null);                      
        addNode(xmlW, 'Type', type); 
        addNode(xmlW, 'TranDate', TranDate); 
        addNode(xmlW, 'SSN', ssn); 
        addNode(xmlW, 'AppID', AppId); 
        addNode(xmlW, 'LoanID', LoanId); 
        addNode(xmlW, 'LoanDate', LoanDate); 
        addNode(xmlW, 'DueDate', DueDate); 
        addNode(xmlW, 'PaymentAmt', PaymAmnt); 
        addNode(xmlW, 'Balance', Balance); 
        addNode(xmlW, 'ReturnCode', null) ; 
        addNode(xmlW, 'RollOverRef', type == 'RO' ? LoanId: null) ; 
        addNode(xmlW, 'RollOverNumber', rollOvernNum) ; 
        addNode(xmlW, 'BankABA', '0') ; 
        addNode(xmlW, 'BankAcct', BAccNo); 
        addNode(xmlW, 'FirstName', fName); 
        addNode(xmlW, 'LastName', lName); 
        addNode(xmlW, 'DateOfBirth', (DOB == null) ? '' : DOB.format()); 
        addNode(xmlW, 'Address1', street); 
        addNode(xmlW, 'City', city); 
        addNode(xmlW, 'State', state); 
        addNode(xmlW, 'Zip', Zip); 
        addNode(xmlW, 'HomePhone', Phone); 
        addNode(xmlW, 'Country', country); 
        
        xmlW.writeEndElement();
        xmlW.writeEndElement();
        xmlW.writeEndElement(); 
        xmlW.writeEndElement();
        xmlW.writeEndElement(); 
        String xmlStringxmlRes = xmlW.getXmlString();
        
        System.debug('The XML====================== :'+xmlW.getXmlString());     
        xmlW.close();
        
        
        HTTPResponse res = FactorTrustUtil.doCall(Label.Ft_Reporting_URL, xmlStringxmlRes );
        system.debug('==========res================='+res);  
        system.debug('==========res body================='+res.GetBody());  
        
        Logging__c Log = new Logging__c(Webservice__c = WebServcName,API_Request__c = xmlStringxmlRes,API_Response__c = String.ValueOf(res.getBody())); 
        
        if(Obj == 'LA'){
            Log.CL_Contract__c = ObjId;
        }else if(Obj == 'PT'){
            Log.Loan_Payment_Transaction__c =  ObjId;  
        } 
        
        return Log;                              
    }
    
    
}