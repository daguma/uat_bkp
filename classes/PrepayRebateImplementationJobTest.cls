//This test class includes coverage for PrepayRebateImplementationJob and PrepayRebateImplementationBatch

@isTest
private class PrepayRebateImplementationJobTest {

    @isTest static void executes_job() {
        Test.startTest();

        PrepayRebateImplementationBatch rebateJob = new PrepayRebateImplementationBatch();
        String cron = '0 0 23 * * ?';
        system.schedule('Test Rebate Process', cron, rebateJob);

        Test.stopTest();
    }

    @isTest static void does_not_update_pay_off() {

        Database.BatchableContext bc;

        loan__loan_account__c contract = LibraryTest.createContractTH();

        genesis__applications__c app = new genesis__applications__c(Id = contract.Application__c, Fee__c = 200, genesis__Loan_Amount__c = 5000, Finwise_Approved__c = true);
        update app;

        Contact c = new Contact(Id = contract.loan__Contact__c, Postal_code__c = '1234');
        update c;

        contract.loan__Pay_Off_Amount_As_Of_Today__c = 5200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-20);
        contract.loan__Maturity_Date_Current__c = Date.today().addYears(2);
        contract.loan__Loan_Status__c = 'Active - Good Standing';
        update contract;

        LIST<loan__Loan_Account__c> contractList = [SELECT Id , loan__Pay_Off_Amount_As_Of_Today__c,
                                    Remaining_Fee_Amount__c, RebatePayoffUpdated__c
                                    FROM loan__Loan_Account__c
                                    WHERE loan__Loan_Status__c not in ('Closed- Written Off', 'Closed - Obligations met', 'Canceled')
                                    AND RebatePayoffUpdated__c <> TODAY AND Remaining_Fee_Amount__c > 0 ORDER BY CreatedDate DESC];

        PrepayRebateImplementationBatch rebateBatch = new PrepayRebateImplementationBatch();
        rebateBatch.execute(bc, contractList);

        loan__loan_account__c contractUpd = [SELECT Id, loan__Pay_Off_Amount_As_Of_Today__c, Remaining_Fee_Amount__c FROM loan__loan_account__c WHERE Id = : contract.Id LIMIT 1];

        System.assertEquals(contract.loan__Pay_Off_Amount_As_Of_Today__c, contractUpd.loan__Pay_Off_Amount_As_Of_Today__c);
        System.assertEquals(0.00, contractUpd.Remaining_Fee_Amount__c);
        System.assertEquals(5200, contractUpd.loan__Pay_Off_Amount_As_Of_Today__c);
    }

    @isTest static void updates_pay_off() {

        Database.BatchableContext bc;

        loan__loan_account__c contract = LibraryTest.createContractTH();

        genesis__applications__c app = new genesis__applications__c(Id = contract.Application__c, Fee__c = 300, genesis__Loan_Amount__c = 5000, Finwise_Approved__c = true);
        update app;

        Contact c = new Contact(Id = contract.loan__Contact__c, Postal_code__c = '1234');
        update c;

        contract.loan__Pay_Off_Amount_As_Of_Today__c = 5300;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-20);
        contract.loan__Maturity_Date_Current__c = Date.today().addYears(2);
        contract.loan__Loan_Status__c = 'Active - Good Standing';
        update contract;

        LIST<loan__Loan_Account__c> contractList = [SELECT Id , loan__Pay_Off_Amount_As_Of_Today__c,
                                    Remaining_Fee_Amount__c, RebatePayoffUpdated__c
                                    FROM loan__Loan_Account__c
                                    WHERE loan__Loan_Status__c not in ('Closed- Written Off', 'Closed - Obligations met', 'Canceled')
                                    AND RebatePayoffUpdated__c <> TODAY AND Remaining_Fee_Amount__c > 0 ORDER BY CreatedDate DESC];

        PrepayRebateImplementationBatch rebateBatch = new PrepayRebateImplementationBatch();
        rebateBatch.execute(bc, contractList);

        loan__loan_account__c contractUpd = [SELECT Id, loan__Pay_Off_Amount_As_Of_Today__c, Remaining_Fee_Amount__c FROM loan__loan_account__c WHERE Id = : contract.Id LIMIT 1];

        System.assertEquals(contract.loan__Pay_Off_Amount_As_Of_Today__c - contractUpd.Remaining_Fee_Amount__c, contractUpd.loan__Pay_Off_Amount_As_Of_Today__c);
        System.assertEquals(34.07, contractUpd.Remaining_Fee_Amount__c);
        System.assertEquals(5265.93, contractUpd.loan__Pay_Off_Amount_As_Of_Today__c);
    }

}