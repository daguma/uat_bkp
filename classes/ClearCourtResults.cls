global class ClearCourtResults {
	public Integer openTaxLiens {get; set;}
    public Integer closedTaxLiens {get; set;}
    public Integer openJudgements {get; set;}
    public Integer closedJudgements {get; set;}
    public Integer totalLiensJudgements {get; set;}
    public String noteBody {get; set;}
    public List<ClearCourtReportData> clearCourtReportData {get; set;}
}