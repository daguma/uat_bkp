public with sharing class DealAutomaticGradeCtrl {
    public DealAutomaticGradeCtrl() {

    }

    /**
    * Get all grades from the Deal Automatic Attributes
    * @return List of Deal_Automatic_Attributes__c
    */
    public static String getGrades() {
        return JSON.serialize([SELECT Id__c, Name, Display_Name__c, Usage__c
                               FROM Deal_Automatic_Attributes__c
                               WHERE Usage__c LIKE :DealAutomaticAssignmentUtils.GRADE_USAGE
                               ORDER BY Display_Name__c]);
    }

    /**
        * [saveUsers description]
        * @param  attribute to save the selected users
        * @param  usersGroups    to save in the specific category
        * @return Save the selected users in the Deal_Automatic_Assignment_Data__c object
        */
    @RemoteAction
    public static List<DealAutomaticUserGroup> saveUsers(Deal_Automatic_Attributes__c attribute,
            List<DealAutomaticUserGroup> newUsersGroups,
            List<DealAutomaticUserGroup> oldUserGroups) {
        return DealAutomaticAssignmentUtils.saveUsers(attribute, newUsersGroups, oldUserGroups);
    }

    /**
    * [getUsersGroupsBySource description]
    * @param  grade [description]
    * @return  List of DealAutomaticUserGroup
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> getUsersGroupsByGrade(Deal_Automatic_Attributes__c grade) {
        return DealAutomaticAssignmentUtils.getUsersGroupsByCategory(grade);
    }
}