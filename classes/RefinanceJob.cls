global class RefinanceJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {

	global final String query;
	global final String option;
	global final Integer queryLimit;
    global final Integer iThread;

	global RefinanceJob(String option, Integer queryLimit) {
        this(option, queryLimit, null);
    }
	global RefinanceJob(String option, Integer queryLimit, Integer pThread) {
		this.option = option;
		this.queryLimit = queryLimit;
        this.iThread = pThread;
		
		if (option == 'EligibilityCheck') {
			this.query = RefinancePreSoftPull.getEligibilityReviewQuery(this.queryLimit, this.iThread);
		} else if (option == 'EligibilityCheckGetCR') {
			this.query = RefinancePostSoftPull.ELIGIBILITY_CREDIT_REPORT_QUERY + ((this.iThread != null && this.iThread > 0)? ' AND Contract__r.loan__Thread_Number__c =  ' + iThread : '');
		}
        
        System.debug(this.query);
	}
	global Database.QueryLocator start(Database.BatchableContext BC) {
		if (this.iThread == null || this.iThread <= 1) RefinancePreSoftPull.ChangedSubStatusRQ();
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		if (option == 'EligibilityCheck') {
			for (sobject s : scope) {
				loan_Refinance_Params__c param = (loan_Refinance_Params__c) s;
				RefinancePreSoftPull.EligibilityCheck(param);
			}
		} else if (option == 'EligibilityCheckGetCR') {
			for (sobject s : scope) {
				loan_Refinance_Params__c param = (loan_Refinance_Params__c) s;
				RefinancePostSoftPull.eligibilityCheckGetCR(param);
			}
		}
	}

	global void finish(Database.BatchableContext BC) {
		if (option == 'EligibilityCheck') {
			RefinanceJob job = new RefinanceJob('EligibilityCheckGetCR', 0, this.iThread);
			System.scheduleBatch(job, 'RefinanceJob_EligibilityCheckGetCR ' + (this.iThread != null ? String.valueOf(this.iThread) : ''), 1, 1);  //Each 1 minute, process batches of 1 by 1
		}
	}

	global void execute(SchedulableContext sc) {
        Refinance_Settings__c refinanceSettings = Refinance_Settings__c.getValues('settings');
        
        if (option == 'EligibilityCheck') {
            for (Integer i = 1; i < 6; i++) {
                RefinanceJob job = new RefinanceJob(option, queryLimit, i);
                database.executebatch(job, refinanceSettings != null && refinanceSettings.Eligibility_Check_Pre_Batch_Size__c != null ? refinanceSettings.Eligibility_Check_Pre_Batch_Size__c.intValue() : 1);
            }
        }
    }

}

//To run:
/*
	System.schedule('RefinanceJob - EligibilityCheck', '0 30 7 1/1 * ? *', new RefinanceJob('EligibilityCheck', N));
*/