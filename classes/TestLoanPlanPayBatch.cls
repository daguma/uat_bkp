@isTest

public class TestLoanPlanPayBatch{
    static testmethod void test(){
       String d= Datetime.now().adddays(2).format('yyyy-MM-dd');
       String query = 'SELECT id,CreatedDate, Account_Number__c, ACH_Debit_Date__c,Payment_Amount__c,Routing_Number__c, Loan_Account__c FROM loan_Plan_Payment__c WHERE ACH_Debit_Date__c='+d+' AND Status__c =\'pending\' Order by Createddate';
       list<loan_Plan_Payment__c> lPProw = new list<loan_Plan_Payment__c>();
       TestHelperForManaged.createSeedDataForTesting();
        //TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupMetadataSegments();
        loan.TestHelper.systemDate = Date.newInstance(2018, 6, 7); // Mar 3 - Monday
       
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
       
        loan__Transaction_Approval_Config__c c = new loan__Transaction_Approval_Config__c();
            c.loan__Payment__c = true;
            c.loan__Payment_Reversal__c = false;
            c.loan__Funding__c = false;
            c.loan__Funding_Reversal__c = false;
            c.loan__Write_off__c = false;
            c.loan__Accounting__c = false;
          
            insert c;
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
      ////  loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
      /****  loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);****/
        
        loan__Loan_Purpose__c dummyLoanPurpose = TestHelperForManaged.createLoanPurpose();
        
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        b1.peer__National_Id__c = '1223';
        update b1;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Advance Trust Account');
        
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
        
        //Create a dummy Loan Account
      /*  loan__Loan_Account__c dummylaMonthly = TestHelperForManaged.createLoanAccountForAccountObj(dummyLP,
                                        b1,
                                        dummyFeeSet,
                                        dummyLoanPurpose,
                                        dummyOffice,
                                        true,
                                        loan.TestHelper.systemDate.addDays(1),
                                        null,
                                        loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY);*/
       loan__Loan_Account__c dummylaMonthly = LibraryTest.createContractTH();                                
       
        dummylaMonthly.loan__Borrower_ACH__c = dummyBank.Id;
        dummylaMonthly.loan__ACH_On__c = true;
        dummylaMonthly.loan__ACH_Frequency__c = 'Monthly';
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = Date.today().addDays(30);
        dummylaMonthly.loan__ACH_Start_Date__c = loan.TestHelper.systemDate;
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__Ach_Debit_Day__c = 3;
        dummylaMonthly.loan__ACH_End_Date__c = loan.TestHelper.systemDate.addYears(1);
     //   dummylaMonthly.ACH_Bank_Account__c = dummyBank.Id;
        Update dummylaMonthly;
        
       
       for(Integer i=0;i<10;i++){
           loan_Plan_Payment__c lPP = new loan_Plan_Payment__c();
           lPP.Payment_Amount__c = 355;
           lPP.ACH_Debit_Date__c    = System.today().adddays(2);
           lPP.Status__c            = 'Pending';
           lPP.Account_Number__c    = '12345';
           lPP.Routing_Number__c    = '12345';
           lPP.Loan_Account__c = dummylaMonthly.Id;
           lPProw.add(lPP);
       }
       insert lPProw;
       
       Test.startTest();
       LoanPlanPayBatch lB = new LoanPlanPayBatch();
       Database.executeBatch(lB);
       Test.stopTest();

    }
      
    static testmethod void testSchedulableJob(){
        test.startTest();
        LoanPlanPayBatch sh = new LoanPlanPayBatch();
        sh.execute(null); 
        test.stopTest();
    }

}