public class generalPurposeWebServicesUtil{
    
    //MER - 602
    public static Opportunity updateProcedureAmount(Opportunity opp,decimal procedureAmount){
        opp.Amount = procedureAmount;
        Double i = (opp.Interest_Rate__c / 100) / offerCtrlUtil.getFrequencyUtil(opp.Payment_Frequency_Masked__c);
        Double f = Math.pow(1 + i, opp.Term__c.intValue() );
        opp.Payment_Amount__c = (opp.Amount + opp.Fee__c ) / ((f - 1) / (i * f));
        opp.Effective_APR__c = OfferCtrlUtil.calculateEffectiveAPR_GovComp(opp.Expected_Start_Date__c, opp.Expected_First_Payment_Date__c, opp.AMount, opp.Payment_Amount__c, opp.Term__c, opp.Payment_Frequency_Masked__c);
        opp.Contract_Type__c = 'DISBURSAL AMT OVERWRITTEN'; // This will be our flag and email trigger
                            
        return opp;
    }    
    //MER - 602
}