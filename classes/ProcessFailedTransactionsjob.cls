global class ProcessFailedTransactionsjob implements Database.Batchable<sObject>, Schedulable, Database.Stateful {
    
    string webserviceName = 'CustomerPortalWSDLUtil.addPayment Error';
    String PaymentModeId = '';
    Integer processed = 0, errors = 0;
    
    global Database.QueryLocator start(Database.BatchableContext bc) { 
        
        List<loan__Payment_Mode__c> paymentModeList = [SELECT Id FROM loan__Payment_Mode__c WHERE Name = 'Debit card' Limit 1];
        
        if(paymentModeList.size() > 0) 
            PaymentModeId = paymentModeList[0].Id;
        
        String query = 'SELECT API_Request__c, API_Response__c ,CreatedDate, Id, Request__c, Response__c, Webservice__c ';
        query += 'FROM Logging__c WHERE Webservice__c = '+ '\''+ webserviceName+ '\' ORDER BY CreatedDate DESC';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Logging__c> scope) {
        
        User userInfo = [SELECT loan__Current_Branch_s_System_Date__c FROM User WHERE Id =: UserInfo.getUserId()];
        
        Map<Id, loan__Loan_Payment_Transaction__c> loggingTransactionMap = new Map<Id, loan__Loan_Payment_Transaction__c>();
        
        for(Logging__c log : scope) {
            
            if(String.isNotBlank(log.API_Request__c) 
               && log.API_Request__c.contains('"type" : "loan__Loan_Payment_Transaction__c"')) {
                   
                   //DESERIALIZING LOGGING INTO LOAN PAYMENT TRANSACTION RECORD
                   loan__Loan_Payment_Transaction__c lpt = (loan__Loan_Payment_Transaction__c) JSON.deserialize(log.API_Request__c, 
                                                                                                               loan__Loan_Payment_Transaction__c.class);
                   
                   //ONLY PROCESS TRANSACTION's HAVING PAYMENT MODE - DEBIT CARD
                   if(String.isNotBlank(PaymentModeId) && lpt.loan__Payment_Mode__c != null 
                      && lpt.loan__Payment_Mode__c == PaymentModeId) {
                          //ADD NEW TRANSACTION RECORD TO MAP
                          loggingTransactionMap.put(log.Id, lpt);
                      }				
               }
        }
        
        if(loggingTransactionMap.size() > 0) {
            
            //INSERT TRANSACTION's
            Database.SaveResult[] srList = Database.insert(loggingTransactionMap.values(), false);
            
            //ITERATE THROUGH RETURNED RESULT
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    errors++;
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
            for(Id logId : loggingTransactionMap.keySet()) {
                if(loggingTransactionMap.get(logId).Id == null) {
                    //IF TRANSACTION FAILED TO INSERT, REMOVE IT FROM "loggingTransactionMap" MAP
                    loggingTransactionMap.remove(logId);
                }
            }
            
            if(loggingTransactionMap.size() > 0) {
                //DELETE LOGGING's FOR SUCCESSFULLY INSERTED TRANSACTION's           
                List<Id> deleteLoggings = new List<Id>();
                deleteLoggings.addAll(loggingTransactionMap.keySet());
                
                Database.DeleteResult[] deleteResults = Database.delete(deleteLoggings, true);
            }
            processed += loggingTransactionMap.size();
        }
        
    }
    
    global void finish(Database.BatchableContext bc){
          WebToSFDC.notifyDev('ProcessFailedTransactionsjob Finished', 'Payments Processed = ' + processed + '\n\nErrors = ' + errors + '\n\n' , UserInfo.getUserId() );

    }
    
    global void execute(SchedulableContext ctx){
        ProcessFailedTransactionsjob batchJob = new ProcessFailedTransactionsjob(); 
        Database.executebatch(batchJob, 1);
    }    
    
}