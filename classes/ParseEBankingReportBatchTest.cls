@isTest
public class ParseEBankingReportBatchTest {
    @isTest static void TestParseEBankingReportBatchTestPart1(){
               
        //Preparing Custom Setting      
        list<eBanking_Report__c> mapEBankingApiNamesList = new list<eBanking_Report__c>();        
        mapEBankingApiNamesList.add(new eBanking_Report__c(Name = 'Date return received',API_Name__c='loan__Rejection_Date__c'));
        mapEBankingApiNamesList.add(new eBanking_Report__c(Name = 'Identification number',API_Name__c='loan__Loan_Account__c'));
        mapEBankingApiNamesList.add(new eBanking_Report__c(Name = 'Original effective date',API_Name__c='loan__Transaction_Date__c'));
        mapEBankingApiNamesList.add(new eBanking_Report__c(Name = 'Recipient(Payee)name',API_Name__c='Customer_Name__c'));
        mapEBankingApiNamesList.add(new eBanking_Report__c(Name = 'Return reason',API_Name__c='loan__Reversal_Reason__c'));
        mapEBankingApiNamesList.add(new eBanking_Report__c(Name = 'Transaction amount',API_Name__c='loan__Transaction_Amount__c'));
        insert mapEBankingApiNamesList;
        
        //Preparing first Contract and corresponding Transaction
        List<loan__Loan_Payment_Transaction__c> lstPaymenttransaction = new List<loan__Loan_Payment_Transaction__c>();
        loan__Loan_Account__c objloanAccount = TestHelper.createContract();
        objloanAccount.loan__Disbursal_Date__c = system.today();
        objloanAccount.loan__Loan_Status__c= 'Active - Good Standing';
        update objloanAccount;        
        string LAI1 = objloanAccount.id;    
        
         loan__Other_Transaction__c OtherTrans1 = new loan__Other_Transaction__c(loan__Loan_Account__c=objloanAccount.id);
         insert OtherTrans1;
         system.debug('==OtherTrans1==' + OtherTrans1);
           
        lstPaymenttransaction.add(new loan__Loan_Payment_Transaction__c(loan__Transaction_Date__c=Date.parse('3/10/2017'),loan__Transaction_Amount__c=1000.00, loan__Loan_Account__c=objloanAccount.id,loan__Payment_Mode__c=Label.ACH_Payment_Mode));
        
        Map<string,string> mapOtherTransaction= new map<string,string>();
        
       
        //Preparing second Contract and corresponding Transaction
         Contact cont = createAnotherContact(); 
         genesis__Applications__c ap = TestHelper.createApplicationFromContact(cont);
         loan__Loan_Account__c objloanAccount2 = TestHelper.createContractFromApp(ap);
         
         objloanAccount2.loan__Disbursal_Date__c=system.today();
         objloanAccount2.loan__Loan_Status__c= 'Active - Good Standing';
         update objloanAccount2;         
         string LAI2 = objloanAccount2.id;         
         lstPaymenttransaction.add(new loan__Loan_Payment_Transaction__c(loan__Transaction_Date__c=Date.parse('6/22/2016'),loan__Transaction_Amount__c=229.13, loan__Loan_Account__c=objloanAccount2.id,loan__Payment_Mode__c=Label.ACH_Payment_Mode));         
         insert lstPaymenttransaction;
          
         
         system.assertequals([select loan__Reversed__c from loan__Loan_Payment_Transaction__c where id=:lstPaymenttransaction.get(1).id].loan__Reversed__c, false);       
                  
         Map<id,loan__Loan_Account__c> contractMap = new Map<id,loan__Loan_Account__c>([select id,Name from loan__Loan_Account__c where id=:LAI1 OR id=:LAI2]);                  
        
         Contact c = [Select Id, Name from Contact where Id = : objloanAccount2.loan__Contact__c];
         c.Firstname = 'Marc';
         c.Lastname = 'Griffith';
         c.Clear_Relevance__c = 0;
         update c;      
         
         
         
         //mapOtherTransaction.put(contractMap.get(LAI1).Name,'7/5/2016');
        // mapOtherTransaction.put(contractMap.get(LAI2).Name,'6/21/2016');
         
        //Inserting bank Reversal file in documents
        Folder myfolder = [Select Id From Folder Where Name = 'ACH Returns Report' LIMIT 1 ];
        if(myfolder != null ){
            Document document = new Document();
            document.Body = Blob.valueOf('ACH Company name/ID,Date return received,Transaction Type,Destination(Source)ABA/TRC,Destination(Source)Account,Recipient(Payee)name,Identification number,Original effective date,Transaction amount,Original trace number,Return reason \n LENDINGPOINT LLC/1471697765,8/19/2016,PPD Collection,261170931,1006503328,David Testcase,'+contractMap.get(LAI1).Name+',3/10/2017,$1000.00 ,1.22238E+14,R019 AVS \n LENDINGPOINT LLC/1471697765,8/20/2016,PPD Collection,42000013,1.53665E+11,Marc Griffith,' + contractMap.get(LAI2).Name +',6/22/2016,$229.13 ,1.22238E+14,R02 - Account closed  \n LENDINGPOINT LLC/1471697765,8/20/2016,PPD Collection,42000013,1.53665E+11,Marc Griffith,' + contractMap.get(LAI2).Name +',6/22/2016,$229.13 ,1.22238E+14,R02 - Account closed \n LENDINGPOINT LLC/1471697765,8/20/2016,PPD Collection,42000013,1.53665E+11,Marc ABC,' + contractMap.get(LAI2).Name +',6/22/2016,$228.13 ,1.22238E+14,R02 - Account closed \n LENDINGPOINT LLC/1471697765,8/20/2016,PPD Collection,42000013,1.53665E+11,\"Marc, Griffith\",' + contractMap.get(LAI2).Name +',6/22/2016,$228.13 ,1.22238E+14,R02 - Account closed');
            document.ContentType = 'csv';
            document.DeveloperName = 'my_document';
            document.IsPublic = true;
            document.Name = 'My Document';
            document.FolderId = myfolder.id;
            insert document;
        }
        
        ParseEBankingReportBatch.executeReturnFile();

    } 
    
    
     public static contact createAnotherContact() {        
        Contact c = new Contact(); 
        c.Firstname = 'Marc"';
        c.Lastname = 'Griffith';
        c.Email = 'testm@gmailm.com';
        c.BirthDate = Date.Today().addYears(-20);
        c.Point_Code__c = 'm234ABm';
        c.ints__Social_security_Number__c = '899999998';
        c.Time_at_current_Address__c = Date.Today().addYears(-2);
        c.Phone = '5251231234';
        c.Mailingstreet = 'mTest streetm';
        c.MailingCity = 'mCitym';
        c.MailingState = 'GA';
        c.MailingPostalCode = '823132';
        c.MailingCountry = 'US';
        c.Annual_Income__c = 150000;
        c.Loan_Amount__c = 6000;
        c.Employment_Start_date__c = Date.Today().addYears(-2);
        c.Clear_Relevance__c = 0;
        
        insert c;
        return c;
    }
        
}