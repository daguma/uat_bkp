@isTest
public class CollectionsCasesAssignmentUsersTest {

    @isTest static void validates_constructor() {
        Test.startTest();
        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        Test.stopTest();

        System.assertNotEquals(null, usersStructure);
    }

    @isTest static void is_user_member(){
        Test.startTest();
        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        Test.stopTest();

        system.assert(
                usersStructure.isUserMemberOf(
                        [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId
                        ,'TeamCollections'
                ), 'Team Collections'
        );

        List<GroupMember> gmPONList = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1];
        Id gmPONId = gmPONList != null && !gmPONList.isEmpty() ? gmPONList[0].UserOrGroupId : null;
        system.assertNotEquals(null,usersStructure.isUserMemberOf(gmPONId,'TeamLms'), 'Team LMS');
        if(gmPONId != null){
            system.assert(usersStructure.isUserMemberOf(gmPONId,'TeamLms'), 'Team LMS');
        }

        system.assert(
                usersStructure.isUserMemberOf(
                        [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsDMCLevel1 LIMIT 1].UserOrGroupId
                        ,'TeamDmc'
                ), 'Team DMC 1'
        );

        system.assert(
                usersStructure.isUserMemberOf(
                        [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsDMCLevel2 LIMIT 1].UserOrGroupId
                        ,'TeamDmc'
                ), 'Team DMC 2'
        );
    }

    @isTest static void is_user_from_dmc_3(){
        Test.startTest();
        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        System.debug('Test' + usersStructure);
        Boolean dmc3 = usersStructure.isUserMemberOf(
                [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsDMCLevel3 LIMIT 1].UserOrGroupId
                ,'TeamDmc');
        Test.stopTest();
        system.assertEquals(true,dmc3);
    }

    @isTest static void is_user_member_of_any_team(){
        Test.startTest();
        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        Test.stopTest();
        system.assert(usersStructure.isUserMemberOfAnyTeam([SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId));
        List<GroupMember> gmPONList = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1];
        Id gmPONId = gmPONList != null && !gmPONList.isEmpty() ? gmPONList[0].UserOrGroupId : null;
        if( gmPONId != null ){
            system.assert(usersStructure.isUserMemberOfAnyTeam(gmPONId));
        }
        system.assert(usersStructure.isUserMemberOfAnyTeam([SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsDMCLevel2 LIMIT 1].UserOrGroupId));
    }

    @isTest static void gets_team_group_name_by_dpd(){
        Test.startTest();
        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        for(CollectionsCasesAssignmentTeam team : usersStructure.teamsMap.values()){
            for(CollectionsCasesAssignmentUsersGroup usersGroup : team.usersGroupsMap.values()){
                for(Integer i = usersGroup.minimumDPD; i <= usersGroup.maximumDPD; i++){
                    System.assert(
                            usersGroup.getGroupName().equals(usersStructure.getTeamGroupNameByDpd(
                                    i
                                    , team instanceof CollectionsCasesAssignmentTeam.CollectionsTeam ? CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS
                                            : team instanceof CollectionsCasesAssignmentTeam.LmsTeam ? CollectionsCasesAssignmentTeam.TEAM_LMS
                                                    : team instanceof CollectionsCasesAssignmentTeam.DmcTeam ? CollectionsCasesAssignmentTeam.TEAM_DMC : ''
                            ))
                    );
                }
            }
        }
        Test.stopTest();
    }

    @isTest static void is_trigger_hit(){
        Test.startTest();
        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        Id collectionsLevel1UserId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId;
        System.assert(!usersStructure.isTriggerHit(null,collectionsLevel1UserId,2));
        System.assert(!usersStructure.isTriggerHit(CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS,null,2));
        System.assert(!usersStructure.isTriggerHit(CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS,collectionsLevel1UserId,null));
        System.assert(usersStructure.isTriggerHit(CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS,collectionsLevel1UserId,34));
        Test.stopTest();
    }
}