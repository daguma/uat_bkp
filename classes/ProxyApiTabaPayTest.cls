@isTest 
public class ProxyApiTabaPayTest {
    
    @isTest static void payment_response_test(){
        ProxyApiTabaPay.PaymentResponse payment = new ProxyApiTabaPay.PaymentResponse('-1', 'API Exception When Trying To Process The Payment<br/>Error details: ');
        payment.SC = 'Test';
        payment.EC = 'Test';
        payment.EM = 'Test';
        payment.transactionID = 'Test';
        payment.network = 'Test';
        payment.status = 'Test';
        payment.approvalCode = 'Test';
    }
    @isTest static void accounts_response_test(){
        ProxyApiTabaPay.AccountsResponse accountsResponse = new ProxyApiTabaPay.AccountsResponse();
        accountsResponse.accountId = '';
        accountsResponse.contactId = '';
        accountsResponse.contractId = '';
        accountsResponse.cardNo = '';
        accountsResponse.expDate = '2019-01-01';
        accountsResponse.createdDate = '';
        accountsResponse.active = true;
        
        system.assertEquals('Yes' , accountsResponse.activeToText);
        system.assertEquals('N/A' , accountsResponse.expDateFormat);
    }
    
    @isTest static void make_payment_test(){
        ProxyApiTabaPay.fake_response = 'generating error';
        ProxyApiTabaPay.makePayment('');
    }
    
    @isTest static void retrieve_accounts_test(){
        ProxyApiTabaPay.fake_response = 'generating error';
        ProxyApiTabaPay.retrieveAccounts('');
    }
    
    @isTest static void delete_account_test(){
        ProxyApiTabaPay.fake_response = 'generating error';
        ProxyApiTabaPay.deleteAccount('');
    }
    
    @isTest static void accounts_info_test(){
        new ProxyApiTabaPay.AccountsInfo(null,'');
    }
    
    @isTest static void add_account_test(){
        ProxyApiTabaPay.fake_response = 'generating error';
        ProxyApiTabaPay.addAccount('');
    }
    
    @isTest static void add_account_response_test(){     
        new ProxyApiTabaPay.AddAccountResponse('-1', '', 'API Exception When Trying to Add a TabaPay Account', 'Error details: ');
    }
        
    @isTest static void delete_account_response_test(){
        new ProxyApiTabaPay.DeleteAccountResponse('-1', '', 'API Exception When Trying to Delete a TabaPay Account<br/>Error details: ');
    }
    
     @isTest static void payment_account_response_test(){
        new ProxyApiTabaPay.PaymentResponse('-1', 'API Exception When Trying To Process The Payment<br/>Error details: ');
    }
    
      @isTest static void changeAuto_PaymentCardTest(){         
        ProxyApiTabaPay.ChangeAutoPayment changepayment = new ProxyApiTabaPay.ChangeAutoPayment('-1', 'API Exception When Trying To Process The Payment<br/>Error details: ', 'message');
        changepayment.SC = 'Test';
        changepayment.EC = 'Test';
        changepayment.message = 'Test';
    }
    
    @isTest static void retrieve_changeAutoPayment_test(){
        ProxyApiTabaPay.fake_response = 'generating error';
        ProxyApiTabaPay.changeAutoPayment('');
    }
    
    
}