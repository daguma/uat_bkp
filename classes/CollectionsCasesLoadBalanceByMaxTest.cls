@isTest
public class CollectionsCasesLoadBalanceByMaxTest {
    static testmethod void validate_constructor_basic_logic() {
        Test.startTest();
        List<User> usersList = [SELECT Id FROM user LIMIT 5];
        Map<Id, Integer> individualsLoadMap = new Map<Id, Integer>();
        individualsLoadMap.put(usersList.get(0).Id,166);
        individualsLoadMap.put(usersList.get(1).Id,159);
        individualsLoadMap.put(usersList.get(2).Id,166);
        individualsLoadMap.put(usersList.get(3).Id,169); 
        individualsLoadMap.put(usersList.get(4).Id,188);
        CollectionsCasesLoadBalanceByMax cclbbm = new CollectionsCasesLoadBalanceByMax(50,individualsLoadMap);
        Test.stopTest();
        System.assertEquals(188 ,cclbbm.maxCurrentIndividualLoad);
        System.assert(cclbbm.maxCurrentIndividualLoadModified > 188);
        System.assertEquals(197, cclbbm.maxCurrentIndividualLoadModified);

        System.assertEquals(1, cclbbm.individualLoadByMaxModifiedMap.get(usersList.get(0).Id) + cclbbm.individualLoadByMaxModifiedAbsMap.get(usersList.get(0).Id));
        System.assertEquals(1, cclbbm.individualLoadByMaxModifiedMap.get(usersList.get(1).Id) + cclbbm.individualLoadByMaxModifiedAbsMap.get(usersList.get(1).Id));
        System.assertEquals(1, cclbbm.individualLoadByMaxModifiedMap.get(usersList.get(2).Id) + cclbbm.individualLoadByMaxModifiedAbsMap.get(usersList.get(2).Id));
        System.assertEquals(1, cclbbm.individualLoadByMaxModifiedMap.get(usersList.get(3).Id) + cclbbm.individualLoadByMaxModifiedAbsMap.get(usersList.get(3).Id));
        System.assertEquals(1, cclbbm.individualLoadByMaxModifiedMap.get(usersList.get(4).Id) + cclbbm.individualLoadByMaxModifiedAbsMap.get(usersList.get(4).Id));

        Decimal individualLoadByMaxModifiedAbsSumFromList = 0;
        for(Decimal myDecimalValue : cclbbm.individualLoadByMaxModifiedAbsMap.values() ){
            individualLoadByMaxModifiedAbsSumFromList += myDecimalValue;
        }
        System.assertEquals(cclbbm.individualLoadByMaxModifiedAbsSum, individualLoadByMaxModifiedAbsSumFromList);

        Long individualFutureLoadFromList = 0;
        for(Long myLongValue : cclbbm.individualFutureLoadMap.values() ){
            individualFutureLoadFromList += myLongValue;
        }
        System.assertEquals(50, individualFutureLoadFromList);
    }

    static testmethod void validate_future_load(){
        Test.startTest();
        List<User> usersList = [SELECT Id FROM user LIMIT 5];
        Map<Id, Integer> individualsLoadMap = new Map<Id, Integer>();
        individualsLoadMap.put(usersList.get(0).Id,204);
        individualsLoadMap.put(usersList.get(1).Id,167);
        individualsLoadMap.put(usersList.get(2).Id,200);
        individualsLoadMap.put(usersList.get(3).Id,170); 
        individualsLoadMap.put(usersList.get(4).Id,150);
        CollectionsCasesLoadBalanceByMax cclbbm = new CollectionsCasesLoadBalanceByMax(76,individualsLoadMap);
        Test.stopTest();

        System.assertEquals(4, cclbbm.individualFutureLoadMap.get(usersList.get(0).Id));
        System.assertEquals(20, cclbbm.individualFutureLoadMap.get(usersList.get(1).Id));
        System.assertEquals(6, cclbbm.individualFutureLoadMap.get(usersList.get(2).Id));
        System.assertEquals(19, cclbbm.individualFutureLoadMap.get(usersList.get(3).Id));
        System.assertEquals(27, cclbbm.individualFutureLoadMap.get(usersList.get(4).Id));

    }
}