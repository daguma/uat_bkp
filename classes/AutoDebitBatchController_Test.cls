@isTest
public class AutoDebitBatchController_Test {
    
    static testmethod void autoDebitBatchController_Test1(){
        
        
        Contact con = LibraryTest.createContactTH();
        con.email = 'test@test.com';
        update  con;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Method__c = 'Debit Card';
        update opp;
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(5);
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.loan__Contact__c = con.Id;
        contract.Is_Debit_Card_On__c = true;
        contract.Card_Next_Debit_Date__c = System.today();
        contract.loan__Pmt_Amt_Cur__c = 100.00;
        contract.Opportunity__c = opp.Id;
        contract.paymentmethod__c = 'Debit Card';
        update contract;
        
        
        Java_API_Settings__c jas = new Java_API_Settings__c();
        jas.User_Create_Endpoint__c = 'https://testapi.lendingpoint.com/v101/tabapay/autoDebitWithSavedDetails';
        jas.clientId__c = 'sfdcclient';
        jas.Username__c = 'nikhil';
        jas.Password__c = 'test@123';
        jas.Name = 'AutoDebitEndPoint';
        insert jas;
        
    }
    
    
    
    static testmethod void batchExecutionTesting() {
        autoDebitBatchController_Test1();  
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        AutoDebitBatchController.respond(null);
        //Test.setMock(HttpCalloutMock.class, new AutoDebitBatchController());
        //Boolean validate = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        //System.assertEquals(true, validate);
            
      	AutoDebitBatchController b = new AutoDebitBatchController('AutoDebitProcess');
		Database.executeBatch(b,1);  
        
    }
    
    static testmethod void batchScheduledExecutionTesting() {
        Contact con = LibraryTest.createContactTH();
        con.email = 'test@test.com';
        update  con;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Method__c = 'Debit Card';
        update opp;
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(5);
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.loan__Contact__c = con.Id;
        contract.Is_Debit_Card_On__c = true;
        contract.Card_Next_Debit_Date__c = System.today();
        contract.loan__Pmt_Amt_Cur__c = 100.00;
        contract.Opportunity__c = opp.Id;
        contract.paymentmethod__c = 'Debit Card';
        update contract;
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        AutoDebitBatchController.respond(null);
        //Test.setMock(HttpCalloutMock.class, new AutoDebitBatchController());
        //Boolean validate = ProxyApiBRMSUtil.ValidateBeforeCallApi();
        //System.assertEquals(true, validate);
        test.startTest();
        AutoDebitBatchController autodebit = new AutoDebitBatchController('AutoDebitProcess');
		Database.executeBatch(autodebit,1);
        string option = 'AutoDebitProcess';
        List<loan__Loan_Account__c> Listcont = New List<loan__Loan_Account__c>();
        Listcont.add(contract);
        autodebit.execute(null, Listcont);
        //AutoDebitBatchController.scheduleJob(option);
        test.stopTest();
        
        
    }
    
    
    static testmethod void batchScheduledExecutionTestingOntime() {
        Contact con = LibraryTest.createContactTH();
        con.email = 'test@test.com';
        update  con;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Method__c = 'Debit Card';
        update opp;
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(5);
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.loan__Contact__c = con.Id;
        contract.Is_Debit_Card_On__c = true;
        contract.Card_Next_Debit_Date__c = System.today();
        contract.loan__Pmt_Amt_Cur__c = 100.00;
        contract.Opportunity__c = opp.Id;
        contract.paymentmethod__c = 'Debit Card';
        update contract;
        
        loan__Other_Transaction__c newTransaction = new loan__Other_Transaction__c();
        newTransaction.loan__Loan_Account__c = contract.Id;
        newTransaction.loan__Txn_Amt__c = 1;
        newTransaction.loan__Txn_Date__c = Date.Today();
        newTransaction.loan__Transaction_Type__c = 'One Time Debit';
        newTransaction.One_Time_Debit_Status__c = 'Pending';
        newTransaction.Debit_Card_Account__c = '';
        insert newTransaction;
        
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        AutoDebitBatchController.respond(null);

		Test.startTest();        
        AutoDebitBatchController autodebit = new AutoDebitBatchController('OneTimeDebitProcess');
		Database.executeBatch(autodebit,1);
      
        Test.stopTest();       
    }
    
     static testmethod void batchOneTimeDebitWithOneTimeACH() {
        Contact con = LibraryTest.createContactTH();
        con.email = 'test@test.com';
        update  con;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Method__c = 'Debit Card';
        update opp;
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(5);
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.loan__Contact__c = con.Id;
        contract.Is_Debit_Card_On__c = true;
        contract.Card_Next_Debit_Date__c = System.today();
        contract.loan__Pmt_Amt_Cur__c = 100.00;
        contract.Opportunity__c = opp.Id;
        contract.paymentmethod__c = 'Debit Card';
        update contract;
        
        loan__Other_Transaction__c newTransaction = new loan__Other_Transaction__c();
        newTransaction.loan__Loan_Account__c = contract.Id;
        newTransaction.loan__Txn_Amt__c = 1;
        newTransaction.loan__Txn_Date__c = Date.Today();
        newTransaction.loan__Transaction_Type__c = 'One Time Debit';
        newTransaction.One_Time_Debit_Status__c = 'Pending';
        newTransaction.Debit_Card_Account__c = '';
        insert newTransaction;
        
        loan__Other_Transaction__c oneTimeACH = new loan__Other_Transaction__c();
        oneTimeACH.loan__Loan_Account__c = contract.Id;
        oneTimeACH.loan__Payment_Amount__c  = 1;
        oneTimeACH.loan__OT_ACH_Debit_Date__c = Date.Today();
        oneTimeACH.loan__Transaction_Type__c = 'One Time ACH';
        insert oneTimeACH;
         
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        AutoDebitBatchController.respond(null);

		Test.startTest();        
        AutoDebitBatchController autodebit = new AutoDebitBatchController('OneTimeDebitProcess');
		Database.executeBatch(autodebit,1);
        
        Test.stopTest();       
    }
    
    static testmethod void batchOneTimeDebitWithPaymentDone() {
        Contact con = LibraryTest.createContactTH();
        con.email = 'test@test.com';
        update  con;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Method__c = 'Debit Card';
        update opp;
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(5);
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.loan__Contact__c = con.Id;
        contract.Is_Debit_Card_On__c = true;
        contract.Card_Next_Debit_Date__c = System.today();
        contract.loan__Pmt_Amt_Cur__c = 100.00;
        contract.Opportunity__c = opp.Id;
        contract.paymentmethod__c = 'Debit Card';
        contract.loan__Disbursal_Date__c = Date.today();
        update contract;
        
        loan__Other_Transaction__c newTransaction = new loan__Other_Transaction__c();
        newTransaction.loan__Loan_Account__c = contract.Id;
        newTransaction.loan__Txn_Amt__c = 1;
        newTransaction.loan__Txn_Date__c = Date.Today();
        newTransaction.loan__Transaction_Type__c = 'One Time Debit';
        newTransaction.One_Time_Debit_Status__c = 'Pending';
        newTransaction.Debit_Card_Account__c = '';
        insert newTransaction;
        
		TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.createSeedDataForTesting();

        loan__Payment_Mode__c pMode = [select Id from loan__Payment_Mode__c where Name = 'ACH' order by CreatedDate desc limit 1];


        loan__Loan_Payment_Transaction__c transactionPay = new loan__Loan_Payment_Transaction__c();
        transactionPay.loan__Loan_Account__c = contract.id;
        transactionPay.loan__Transaction_Amount__c = 1;
        transactionPay.loan__Cleared__c = true;
        transactionPay.FT_Payment_Reporting__c = false;
        transactionPay.loan__Payment_Mode__c = pMode.Id;
        transactionPay.loan__Transaction_Date__c = Date.today();
        transactionPay.Payment_Pending__c = 'Cleared';
        insert transactionPay;
         
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        AutoDebitBatchController.respond(null);

		Test.startTest();        
        AutoDebitBatchController autodebit = new AutoDebitBatchController('OneTimeDebitProcess');
		Database.executeBatch(autodebit,1);
        
        Test.stopTest();       
    }
    
}