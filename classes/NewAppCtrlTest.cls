@isTest public class NewAppCtrlTest {

    @isTest static void validate_initialize() {

        genesis__Applications__c app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewAppCtrl newApp = new NewAppCtrl(controller);

        PageReference pr = newApp.initialize();

        System.assertEquals(null, pr);

    }

    @isTest static void validate_small() {
        genesis__Applications__c app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewAppCtrl newApp = new NewAppCtrl(controller);

        newApp.hideattach = true;
        newApp.showpanel = false;

        PageReference pr = newApp.hidepanel();

        System.assertEquals(false, newApp.hideattach);
        System.assertEquals(true, newApp.showpanel);

        Note newNote = newApp.getmynote();

        newApp.mynote.title = 'Test Note';
        newApp.mynote.body = 'Test Body Note';

        System.debug('newNote data: ' + newNote);
        System.assert(true);

        List<Note> noteBefore = [select title, body from Note where parentId = : app.id];

        pr = newApp.Savedoc();

        List<Note> noteAfter = [select title, body from Note where parentId = : app.id];

        System.assertNotEquals(noteBefore.size(), noteAfter.size());

        newApp.notes = newNote;

        System.assertNotEquals(null, newApp.notes);

        newApp.hasCRInformation = true;

        System.assertEquals(true, newApp.hasCRInformation);

    }

    @isTest static void PullHardCreditReport() {

        genesis__Applications__c app = LibraryTest.createApplicationTH();

        System.debug('construction of app.id ' + app.id);

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewAppCtrl newApp = new NewAppCtrl(controller);

        newApp.HardcreditPullPending = true;

        newApp.PullHardCreditReport();

        System.assertEquals(false, newApp.HardcreditPullPending);

    }

    @isTest static void PullSoftCreditReport() {

        genesis__Applications__c app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewAppCtrl newApp = new NewAppCtrl(controller);

        LibraryTest.softPull = true;

        newApp.initialize();

        List<Apexpages.Message> msgs = ApexPages.getMessages();

        Boolean b = false;

        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;

        if ((msgs.get(index)).getDetail().equalsIgnoreCase('You must do a Hard Credit Pull before Funding this application.')) b = true;

        System.assert(b);

    }

    @isTest static void SaveReferralInfo() {

        genesis__Applications__c app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewAppCtrl newApp = new NewAppCtrl(controller);
        Boolean b = false;

        newApp.referralInfoApp.Referred__c = true;
        newApp.referralInfoApp.Referral_APR__c = 25;                   
        newApp.referralInfoApp.Referral_Funding_Amount__c = 0;
        newApp.referralInfoApp.Referral_Funding_Date__c = Datetime.now();
        newApp.referralInfoApp.FinMkt_LoanId__c = 'test123';
        newApp.referralInfoApp.Referral_Partner__c = 'test';
        newApp.referralInfoApp.Referral_Term__c = 36;
        
        newApp.saveReferralInfo();

        List<Apexpages.Message> msgs = ApexPages.getMessages();

        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;

        if ((msgs.get(index)).getDetail().equalsIgnoreCase('Referral Partner Information saved!')) 
            b = true;

        System.assert(b);

        b = false;

        newApp.referralInfoApp.Referred__c = false;
        
        newApp.saveReferralInfo();

        msgs = new List<ApexPages.Message>();
        msgs = ApexPages.getMessages();
        
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;

        if ((msgs.get(index)).getDetail().equalsIgnoreCase('Referral Partner Information saved!')) 
            b = true;

        System.assert(b);
    }

}