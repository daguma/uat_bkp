global class ProxyApiClarityClearIncomeAttributes {

    public Decimal medianIncome1yearAgo {
        get {
            return medianIncome1yearAgo;
        }
        set {
            medianIncome1yearAgo= value;
            medianIncome1yearAgo= medianIncome1yearAgo!= null ? medianIncome1yearAgo.abs() : medianIncome1yearAgo;            
        }
    }
     public Decimal stateTax {
        get {
            return stateTax;
        }
        set {
            stateTax = value;
            stateTax = stateTax != null ? stateTax.abs() : 0;
        }
    }
    public Decimal federalTax {
        get {
            return federalTax;
        }
        set {
            federalTax = value;
            federalTax = federalTax != null ? federalTax.abs() : 0;
        }
    }
     
    public string errorMessage{get;set;}
    public string errorDetails{get;set;}
    
 }