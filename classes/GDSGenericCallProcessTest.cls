@isTest
private class GDSGenericCallProcessTest {

    @isTest static void gets_decision_logic_null_response(){
        GDSGenericCallProcess gdsGeneric = new GDSGenericCallProcess();
        GDSGenericObjectAttributes.GDSGenericCallResponse response = GDSGenericCallProcess.get_GDSGenericCallResult('','','');
        System.assertEquals(null, response);
    }
    
    @isTest static void gets_decision_logic_response(){
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        Opportunity opp = LibraryTest.createApplicationTH();
        GDSGenericCallProcess gdsGeneric = new GDSGenericCallProcess();
        GDSGenericObjectAttributes.GDSGenericCallResponse response = GDSGenericCallProcess.get_GDSGenericCallResult('Test',opp.Id,'14');
        System.assertEquals(null, response);
    }
    
    @isTest static void initializes_gds_request_parameters(){
        GDS_Configuration__c setting = new GDS_Configuration__c();
        setting.SF_Token__c = 'test';
        insert setting;
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        BRMS_Sequence_Ids__c brms = new BRMS_Sequence_Ids__c();
        brms.Name = 'Decision Logic';
        brms.Sequence_Id__c = '14';
        insert brms;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        
        GDSRequestParameter response = GDSGenericCallProcess.gdsRequestParameterInitialize(opp.Id);
    }
    
}