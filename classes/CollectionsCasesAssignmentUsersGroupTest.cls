@isTest
public class CollectionsCasesAssignmentUsersGroupTest {

    static testmethod void validate_constructor() {
        Test.startTest();
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30));
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2,1,30));
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3,1,30));
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_1,1,30));
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_2,1,30));
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_3,1,30));
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_1,1,30));
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_2,31,180));
        Test.stopTest();
    }

    static testmethod void validate_getAllGroupMembersIds() {
        Test.startTest();
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30).getAllGroupMembersIds());
        System.assert(!new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30).getAllGroupMembersIds().isEmpty());
        System.assert(new CollectionsCasesAssignmentUsersGroup('',1,30).getAllGroupMembersIds().isEmpty());
        System.assert(new CollectionsCasesAssignmentUsersGroup('NonExistentGroup',1,30).getAllGroupMembersIds().isEmpty());
        Test.stopTest();
    }

    static testmethod void validate_isUserMemberOfGroup() {
        Test.startTest();
        System.assertNotEquals(null, new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30).getAllGroupMembersIds());
        System.assert(
                new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30)
                        .isUserMemberOfGroup([SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId)
        );
        System.assert(
                !new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30)
                        .isUserMemberOfGroup([SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel2 LIMIT 1].UserOrGroupId)
        );
        Test.stopTest();
    }
}