global class EmailAutomatedJob implements Schedulable {
    public OrgWideEmailAddress noRe;
    public static EmailUtility emailUtil = new EmailUtility();
    public Boolean bIsSandbox = runningInASandbox();
    public static map<string, string> portalEmailRequestSubjectMap = new map<string, string>();
    public Map<string, string> portalEmailRequestMap;
    public string[] testUserIds = new string[] {'0060B00000fnqXPQAY'};

    private string EMAIL_IGNORE = 'no@lendingpoint.com';
    private AANNotificationJob jo = null;

    public void initialize() {
        jo = new AANNotificationJob();

        try {
            noRe = [SELECT Id, Address, DisplayName
                    FROM OrgWideEmailAddress
                    WHERE DisplayName = 'LendingPoint-NoReply' ];
        } catch (Exception e) {
            System.debug ('[U-03] Unable to locate OrgWideEmailAddress using name: ' + 'LendingPoint-NoReply' + ' refer to Setup | Email Communcations ' );
        }
    }

    global void execute(SchedulableContext sc) {
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        System.debug('Starting EmailAutomatedJob execution!');

        portalEmailRequestMap = new Map<string, string>();
        initialize();
        //sendPaymentReminder(emails);
        paymentConfirmation(emails);

        if (Label.Call_Customer_Portal_API == 'true') {
            SendEmailRequestsToPortal(portalEmailRequestMap);
        }
    }


    public void paymentConfirmation(List<Messaging.SingleEmailMessage> emails) {
        try {
            List<Outbound_Emails_Temp__c> payEmails = new List<Outbound_Emails_Temp__c>();
            List<Note> notesEmails = new List<Note>();

            System.debug('Payment Confirmation');
            Map<Id, loan__Loan_Account__c> mloan = new Map<Id, loan__Loan_Account__c>();
            emails = new List<Messaging.SingleEmailMessage>();
            Map<string, EmailTemplate> templateMap= new Map<string, EmailTemplate>();
            JSONGenerator generator = JSON.createGenerator(false);
            generator.writeStartObject();
            generator.writeFieldName('mails');
            generator.writeStartArray();
            String HtmlValue = '', Subject = '';

            // Build list of payments done
            List<loan__Loan_Payment_Transaction__c> modTrans = new List<loan__Loan_Payment_Transaction__c>();

            Map<Id, loan__Loan_Payment_Transaction__c> mTrans;
            if (bIsSandbox) {
                mTrans = new Map<Id, loan__Loan_Payment_Transaction__c>(
                    [SELECT Id,
                             loan__balance__c,
                             loan__Loan_Account__c,
                             Confirmation_Email_Sent_Date__c,
                             loan__Transaction_Amount__c,
                             loan__Principal__c,
                             loan__Interest__c,
                             loan__Reversed__c,
                             masked_Bank_Account_Number__c
                     FROM loan__Loan_Payment_Transaction__c
                     WHERE loan__Loan_Account__r.Opportunity__c IN: testUserIds
                     ORDER BY loan__Loan_Account__c, id ASC ]);
            } else {
                mTrans = new Map<Id, loan__Loan_Payment_Transaction__c>(
                    [SELECT Id,
                             loan__balance__c,
                             loan__Loan_Account__c,
                             Confirmation_Email_Sent_Date__c,
                             loan__Transaction_Amount__c,
                             loan__Principal__c,
                             loan__Interest__c,
                             loan__Reversed__c,
                             masked_Bank_Account_Number__c
                     FROM loan__Loan_Payment_Transaction__c
                     WHERE loan__Loan_Account__r.loan__Contact__r.EmailBouncedDate = null
                             AND loan__Loan_Account__r.loan__Contact__r.Email <> null
                             AND loan__Loan_Account__r.loan__Contact__r.Email <>: EMAIL_IGNORE
                             AND loan__Loan_Account__r.loan__Loan_Status__c IN ('Active - Good Standing', 'Active - Bad Standing')
                             AND (loan__Loan_Account__r.loan__Number_of_Days_Overdue__c < 90
                                  OR loan__Loan_Account__r.loan__Number_of_Days_Overdue__c = null)
                             AND Confirmation_Email_Sent_Date__c  = null
                             AND loan__Rejected__c = false
                             AND loan__Reversed__c = false
                             AND loan__Transaction_Amount__c > 0
                             AND CreatedDate > : Date.today().addDays(-2)
                             AND loan__Payment_Mode__r.Name = 'ACH'
                     ORDER BY loan__Loan_Account__c, id ASC
                     LIMIT 40]);
            }

            if (Test.isRunningTest())
                mTrans = new Map<Id, loan__Loan_Payment_Transaction__c>(
                    [SELECT Id,
                             loan__balance__c,
                             loan__Loan_Account__c,
                             Confirmation_Email_Sent_Date__c,
                             loan__Transaction_Amount__c,
                             loan__Principal__c,
                             loan__Interest__c,
                             loan__Reversed__c,
                             masked_Bank_Account_Number__c
                     FROM loan__Loan_Payment_Transaction__c
                     ORDER BY loan__Loan_Account__c, id ASC ]);

            if (mTrans.size() > 0) {
                // Get their list of loans
                mloan = new Map<Id, loan__Loan_Account__c>(
                    [SELECT l.id,
                             l.loan__Payment_Amount__c,
                             l.loan__Pmt_Amt_Cur__c,
                             l.loan__Next_Installment_Date__c,
                             l.PaymentDue7DayReminderDate__c,
                             l.loan__Contact__c,
                             l.Name,
                             l.Opportunity__c
                     FROM loan__Loan_Account__c l
                     WHERE loan__Invalid_Data__c = false
                           AND Id IN (SELECT loan__Loan_Account__c
                                   FROM loan__Loan_Payment_Transaction__c
                                   WHERE ID IN : mTrans.keySet())]);

                Map<Id, Contact> lContacts =
                    new Map<Id, Contact>([SELECT Id,
                                                  FirstName,
                                                  LastName,
                                                  Name,
                                                  Email,
                                                  Salutation,
                                                  Customer_Number__c,
                                                  AccountId, Preferred_Language__c
                                                  From Contact
                                          WHERE Email <> NULL
                                          AND Email <> : EMAIL_IGNORE
                                          AND Id IN (SELECT loan__Contact__c
                                                     FROM loan__Loan_Account__c
                                                     WHERE loan__Invalid_Data__c = false
                                                             AND Id IN : mloan.keySet())]);
                //API-184===start===
                 for(EmailTemplate emTemplate: [SELECT id, name, HtmlValue, Subject, Body, DeveloperName
                             FROM EmailTemplate
                             WHERE developername = 'LP_Payment_Confirmation' OR developername = 'LP_Payment_Confirmation_ES' LIMIT 2]){
                     templateMap.put(emTemplate.developerName, emTemplate);
                 }
                 //API-184===End===
                
                for (loan__Loan_Payment_Transaction__c tr : mTrans.values()) {
                    if (mloan.containsKey(tr.loan__Loan_Account__c)) {
                        loan__Loan_Account__c acc = mloan.get(tr.loan__Loan_Account__c);

                        if (acc != null && lContacts.containsKey(acc.loan__Contact__c)) {
                            Contact con = lContacts.get(acc.loan__Contact__c);
                            if (con != null) {
                                //API-184===start===
                                EmailTemplate eTemplate= (string.isNotEmpty(con.Preferred_Language__c) && con.Preferred_Language__c.toLowerCase() == 'spanish') ? templateMap.get('LP_Payment_Confirmation_ES') : templateMap.get('LP_Payment_Confirmation'); 
                                //API-184===End===
                                HtmlValue = replaceValuesFromData(eTemplate.Body, acc, con, tr, null);
                                Subject = replaceValuesFromData(eTemplate.Subject, acc, con, tr, null);

                                if (con != null)
                                    System.debug ('Emailing Payment Confirmation  = ' + con.Email);

                                tr.Confirmation_Email_Sent_Date__c = DateTime.now();

                                modTrans.add(tr);

                                payEmails.add(jo.createEmail(HtmlValue, Subject, con.Id ));
                                notesEmails.add(jo.createNote(HtmlValue, Subject, con.Id ));
                                String HtmlValueForPortal = replaceValuesFromData(eTemplate.HtmlValue, acc, con, tr, null);
                                createJsonData(generator, con.id, con.Email, noRe.Address, Subject, HtmlValueForPortal, DateTime.now(), tr.id);
                            }
                           
                        }
                    }
                }

                generator.writeEndArray();
                generator.writeEndObject();
                System.debug('Sending Emails # : ' + payEmails.size());
                if ((payEmails != null && payEmails.size() > 0) || Test.isRunningTest() ) {
                    System.debug('Sending Emails # : ' + emails.size());
                    update modTrans;
                    insert payEmails;
                    insert notesEmails;
                    portalEmailRequestMap.put('Payment Confirmation Email', generator.getAsString());
                }
            }
        } catch (Exception e) {
            WebToSFDC.notifyDev('EmailAutomatedJob paymentConfirmation Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() ) ;
        }
    }

    public void sendPaymentReminder (List<Messaging.SingleEmailMessage> emails) {
        emails = new List<Messaging.SingleEmailMessage>();
        String Subject;
        List<Outbound_Emails_Temp__c> payEmails = new List<Outbound_Emails_Temp__c>();
        List<Note> notesEmails = new List<Note>();
        System.debug('Send Payment Reminders..');

        try {
            JSONGenerator generator = JSON.createGenerator(false);
            generator.writeStartObject();
            generator.writeFieldName('mails');
            generator.writeStartArray();

            EmailTemplate eTemplate = [SELECT id, name, HtmlValue, Subject, Body
                             from EmailTemplate
                             WHERE developername = : 'LP_Payment_Due_reminder_V2' LIMIT 1];

            // get payments that are coming up.
            Map<ID, loan__Loan_Account__c> mloans;
            if (bIsSandbox) {
                mloans  = new Map<ID, loan__Loan_Account__c> ([SELECT l.id,
                                                                        l.loan__Payment_Amount__c,
                                                                        l.loan__Pmt_Amt_Cur__c,
                                                                        l.loan__Next_Installment_Date__c,
                                                                        l.PaymentDue7DayReminderDate__c,
                                                                        l.loan__Contact__c,
                                                                        l.Name,
                                                                        l.Opportunity__c
                                                                FROM loan__Loan_Account__c l
                                                                WHERE l.Opportunity__c IN: testUserIds AND loan__ACH_On__c = true ]);
            } else {
                mloans = new Map<ID, loan__Loan_Account__c> (
                    [SELECT l.id,
                             l.loan__Payment_Amount__c,
                             l.loan__Pmt_Amt_Cur__c,
                             l.loan__Next_Installment_Date__c,
                             l.PaymentDue7DayReminderDate__c,
                             l.loan__Contact__c,
                             l.Name,
                             l.Opportunity__c
                     FROM loan__Loan_Account__c l
                     WHERE loan__ACH_On__c = true
                             AND l.loan__Contact__r.EmailBouncedDate = null
                             AND l.loan__Contact__r.Email <> null
                             AND l.loan__Contact__r.Email <>: EMAIL_IGNORE
                             AND loan__Loan_Status__c IN ('Active - Good Standing', 'Active - Bad Standing')
                             AND (loan__Number_of_Days_Overdue__c < 90 OR loan__Number_of_Days_Overdue__c = null)
                             AND (PaymentDue7DayReminderDate__c = Null OR PaymentDue7DayReminderDate__c != LAST_N_Days:8)
                             AND l.loan__Principal_Remaining__c > 0.50
                             AND (l.loan__Next_Installment_Date__c > NEXT_N_DAYS:5
                                     AND l.loan__Next_Installment_Date__c = NEXT_N_DAYS:7 )
                             LIMIT 40 ]);
            }

            if (Test.isRunningTest())
                mloans = new Map<ID, loan__Loan_Account__c> (
                    [SELECT l.id,
                             l.loan__Payment_Amount__c,
                             l.loan__Pmt_Amt_Cur__c,
                             l.loan__Next_Installment_Date__c,
                             l.PaymentDue7DayReminderDate__c,
                             l.loan__Contact__c,
                             l.Name,
                             l.Opportunity__c
                     FROM loan__Loan_Account__c l]);

            Map<Id, Contact> lContacts =
                new Map<Id, Contact>([SELECT Id,
                                              FirstName,
                                              LastName,
                                              Name,
                                              Email,
                                              Salutation,
                                              Customer_Number__c,
                                              AccountId
                                      FROM Contact
                                      WHERE Email <> NULL
                                      AND Email <> : EMAIL_IGNORE
                                      AND Id IN (SELECT loan__Contact__c
                                                 FROM loan__Loan_Account__C
                                                 WHERE ID IN : mloans.keySet() )]);

            List<loan__Loan_Account__c> modLoans = new List<loan__Loan_Account__c>();

            for (loan__Loan_Account__c loan : mloans.values()) {
                if (lContacts.containsKey(loan.loan__Contact__c)) {
                    Contact c = lContacts.get(loan.loan__Contact__c);
                    if (c != null) {
                        String HtmlValue = replaceValuesFromData(eTemplate.Body, loan, c, null, null);
                        Subject = replaceValuesFromData(eTemplate.Subject, loan, c, null, null);
                        System.debug ('Emailing PaymentDue Reminder = ' +  c.Email);
                        loan.PaymentDue7DayReminderDate__c = Date.today();

                        modLoans.add(loan);
                        payEmails.add(jo.createEmail(HtmlValue, Subject, c.Id ));
                        notesEmails.add(jo.createNote(HtmlValue, Subject, c.Id ));

                        String HtmlValueForPortal = replaceValuesFromData(eTemplate.HtmlValue, loan, c, null, null);
                        createJsonData(generator, c.id, c.Email, noRe.Address, Subject, HtmlValueForPortal, DateTime.now(), loan.id);
                    }
                    System.debug('Template ID = ' + eTemplate.Id);
                }
            }
            generator.writeEndArray();
            generator.writeEndObject();
            System.debug('Sending Emails # : ' + payEmails.size());
            if ((payEmails != null && payEmails.size() > 0) || Test.isRunningTest() ) {
                update modLoans;
                insert payEmails;
                insert notesEmails;
                System.debug('Email Sent');
                portalEmailRequestMap.put('Payment Reminder Email', generator.getAsString());
            }
        } catch (Exception e) {
            WebToSFDC.notifyDev('EmailAutomatedJob sendPaymentReminder Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() ) ;
        }
    }

    @future(callout = true)
    public static void SendEmailRequestsToPortal(Map<string, string> portalRequests) {
        list<logging__c> loggingRecords = new list<logging__c>();
        try {
            for (string req : portalRequests.KeySet()) {
                list<logging__c> logRecords = EmailUtility.sendEmailDetailNew(req, portalRequests.get(req));
                if (logRecords <> null && logRecords.size() > 0) {
                    loggingRecords.addAll(logRecords);
                }
            }
            system.debug('=========loggingRecords=================' + loggingRecords);
            if (loggingRecords.size() > 0) {
                insert loggingRecords;
            }
        } catch (Exception e) {
            System.debug('Exception================ ' + e.getMessage());
        }
    }



    public string replaceValuesFromData(String theValue, loan__Loan_Account__c loan, Contact cont,  loan__Loan_Payment_Transaction__c tran, Opportunity app) {
        try {
            if (loan != null && loan.loan__Next_Installment_Date__c != null) {
                Datetime nextDue =
                    DateTime.newInstance(loan.loan__Next_Installment_Date__c.Year(), loan.loan__Next_Installment_Date__c.month(), loan.loan__Next_Installment_Date__c.Day());

                theValue =
                    theValue.replace('{!loan__Loan_Account__c.loan__Next_Installment_Date__c}', nextDue.format('MMM dd, yyyy'));
            }
            theValue = theValue.replace('{!loan__Loan_Account__c.Name}', (loan != null && loan.Name != null ) ? loan.Name : '-');
            theValue = theValue.replace('{!loan__Loan_Account__c.loan__Payment_Amount__c}', loan != null && loan.loan__Pmt_Amt_Cur__c != null ? getCents(loan.loan__Pmt_Amt_Cur__c) : '-');
            theValue = theValue.replace('{!Contact.Name}', cont != null && cont.Name != null ? cont.Name : '-');
            theValue = theValue.replace('{!Contact.FirstName}', cont != null && cont.FirstName != null ? cont.FirstName : '-');
            theValue = theValue.replace('{!genesis__Applications__c.Effective_Date__c}', app != null && app.Effective_Date__c != null ? String.valueOf(app.Effective_Date__c) : '-');
            theValue = theValue.replace('{!Contact.Customer_Number__c}', cont != null && cont.Customer_Number__c != null ? cont.Customer_Number__c : '-');
            theValue = theValue.replace('{!Contact.Title}', cont != null && cont.Salutation != null ? cont.Salutation : '-');
            theValue = theValue.replace('{!Contact.Id}', cont != null ?  cont.Id : '-');
            theValue = theValue.replace('{!loan__Loan_Payment_Transaction__c.loan__Transaction_Amount__c}', tran != null && tran.loan__Transaction_Amount__c != null ? getCents(tran.loan__Transaction_Amount__c) : '-');
            theValue = theValue.replace('{!loan__Loan_Payment_Transaction__c.masked_Bank_Account_Number__c}',tran != null && tran.masked_Bank_Account_Number__c != null ? tran.masked_Bank_Account_Number__c : '****');
            theValue = theValue.replace('{!genesis__Applications__c.genesis__Loan_Amount__c}', app != null && app.Amount != null ? getCents(app.Amount) : '0.00');
            theValue = theValue.replace('{!genesis__Applications__c.Total_Loan_Amount__c}', app != null && app.Total_Loan_Amount__c != null ? getCents(app.Total_Loan_Amount__c) : '0.00');
            theValue = theValue.replace('{!genesis__Applications__c.genesis__Interest_Rate__c}', app != null && app.Interest_Rate__c != null ? String.valueOf((app.Interest_Rate__c).setScale(2)) : '0.00');
            theValue = theValue.replace('{!genesis__Applications__c.genesis__Payment_Frequency__c}', app != null && app.Payment_Frequency_Masked__c != null ? String.valueOf(((app.Payment_Frequency_Masked__c).toLowerCase()).capitalize()) : '-');
            theValue = theValue.replace('{!genesis__Applications__c.Payment_Frequency_Masked__c}', app != null && app.Payment_Frequency_Masked__c != null ? String.valueOf(((app.Payment_Frequency_Masked__c).toLowerCase()).capitalize()) : '-');
            theValue = theValue.replace('{!genesis__Applications__c.genesis__Payment_Amount__c}', app != null && app.Payment_Amount__c != null ? getCents(app.Payment_Amount__c) : '0.00');
            theValue = theValue.replace('{!genesis__Applications__c.genesis__Term__c}', (app != null && app.Term__c != null) ? String.valueOf(app.Term__c) : '0 ');
            theValue = theValue.replace('{!genesis__Applications__c.genesis__Contact__r.Customer_Number__c}', app != null && app.Contact__r.Customer_Number__c != null ? String.valueOf(app.Contact__r.Customer_Number__c) : '');
            theValue = theValue.replace('{!genesis__Applications__c.Payment_Method__c}', app != null && app.Payment_Method__c != null ? String.valueOf(app.Payment_Method__c) : '-');
            theValue = theValue.replace('{!genesis__Applications__c.genesis__Expected_First_Payment_Date__c}', (app != null && app.Expected_First_Payment_Date__c != null) ? DateTime.newInstance(app.Expected_First_Payment_Date__c.year(), app.Expected_First_Payment_Date__c.month(), app.Expected_First_Payment_Date__c.day()).format('MMM dd, yyyy') : '');

        } catch (Exception e) {
            WebToSFDC.notifyDev('EmailAutomatedJob replaceValuesFromData Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + '\n' + e.getStackTraceString()) ;
        }

        return theValue;
    }

    private String getCents(Decimal x) {
        String y = String.valueOf(x), z = '.';
        if (y.contains(',')) z = ',';
        if (y.contains(z)) y = y.substring(0, y.indexOf(z));
        if (x - Decimal.valueOf(y) == 0)
            return String.valueOf(x.format()) + z + '00';
        else return String.valueOf(x.format());
    }

    public void createJsonData(JSONGenerator generator, ID contactId, String ToMail, String SentFrom, String Subject, String HtmlBody, Datetime SentTime, string recordId) {

        String dtConverted = (SentTime.format('MM/dd/yyyy h:mm:ss a'));

        generator.writeStartObject();
        generator.writeStringField('recordId', recordId);
        generator.writeStringField('contactId', contactId);
        generator.writeStringField('to', ToMail);
        generator.writeStringField('from', SentFrom);
        generator.writeStringField('subject', Subject);
        generator.writeStringField('body', HtmlBody.escapeHtml4().escapeEcmaScript());
        generator.writeStringField('date', dtConverted );
        generator.writeEndObject();
    }

    public static Boolean runningInASandbox() {
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
}