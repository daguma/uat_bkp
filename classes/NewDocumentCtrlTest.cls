@isTest public class NewDocumentCtrlTest {

    @testSetup static void setup_custom_settings() {
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7',
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;

        Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name = 'POS', Asset_Id__c = 5, SFDC_Value__c = false);
        insert AsetCls;
        Upload_File_Max_Limits__c ufm = new Upload_File_Max_Limits__c(Document_Row_ID__c = '1010', XML_Fields__c = 'All_Applicants_XML__c,All_Applicants_XML__c', name = 'test');
        insert ufm;     
    }

    @isTest static void new_document_ctrl() {
        Contact TestContact = LibraryTest.createContactTH();
        Opportunity app = LibraryTest.createApplicationTH();
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        System.assertNotEquals(null, newDoc);
        //ProxyApiUtil.settings = LibraryTest.fakeSettings();
    }

    @isTest static void test_genDocument(){
        Opportunity app = LibraryTest.createApplicationTH();
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        NewDocumentCtrl.ProcessResult pr = newDoc.genDocument();
        system.assertNotEquals(null, pr);
    }
    @isTest static void test_genDocument2(){
        Opportunity app = LibraryTest.createApplicationTH();
        app.ACH_Bank_Name__c = 'MG Bank';
        app.Payment_Method__C = 'ACH';
        app.Expected_First_Payment_Date__c = date.today().addDays(7);
        app.Amount = 50.00;
        app.Payment_Amount__c = 1.00;
        app.Term__c = 24;
        app.Payment_Frequency__c = 'WEEKLY';
        update app;
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        NewDocumentCtrl.ProcessResult pr = newDoc.genDocument();
        system.assertNotEquals(null, pr);
    }

/*
    @isTest static void generates_document() {

        Opportunity app = LibraryTest.createApplicationTH();
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);

        Scorecard__c scorecard = new Scorecard__c(Opportunity__c = app.id, Acceptance_Hard_Pull__c = 'Y');
        insert scorecard;
        app.Clarity_Status__c = true;
        app.Clarity_Submitted__c = true;
        app.Bureau__c = 'EX';
        app.Manual_Grade__c = 'X';
        app.FICO__c = '678';
        update app;
        
        Attachment attach = new Attachment();
        attach.ParentId = app.id;
        attach.Name = 'Test Attachment';
        attach.description = 'test';
        attach.body = blob.valueOf('test');
        insert attach;  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        //List<EmailTemplate> e=[select id from EmailTemplate where DeveloperName='Document_Portal_Template'];

        NewDocumentCtrl.ProcessResult pr;

        System.RunAs(usr) {
            Test.startTest();
            //delete e;
            EmailTemplate eTemplate = new EmailTemplate(FolderId = UserInfo.getUserId(), TemplateType = 'Custom', name = 'templateBureu', HtmlValue = 'Welcome', Subject = 'subject', Body = 'Body', DeveloperName = 'templateBureu');
            insert eTemplate;
            system.debug(eTemplate.Body);

            pr = newDoc.genDocument();
            System.assertNotEquals(null, pr.errorMessage);
            System.assertEquals(true, pr.hasError);

            Test.stopTest();
        }

        app.Payment_Method__C = 'ACH';
        update app;

        newDoc = new NewDocumentCtrl(app);

        pr = newDoc.genDocument();
        System.assertNotEquals(null, pr.errorMessage);
        System.assertEquals(true, pr.hasError);

        app.ACH_Bank_Name__c = 'Bank of America';
        app.Deal_is_Ready_to_Fund__c = false;
        update app;

        Contact c = [SELECT id, name from Contact WHERE id = : app.contact__c LIMIT 1];
        c.MailingState = 'WI';
        update c;

        newDoc = new NewDocumentCtrl(app);

        pr = newDoc.genDocument();
        System.assertNotEquals(null, pr.errorMessage);

        app.Deal_is_Ready_to_Fund__c = true;
        app.Payment_Frequency__c = 'MONTHLY';
        app.Expected_First_Payment_Date__c = Date.today().addDays(28);
        app.Amount = 5000;
        app.Payment_Amount__c = 250;
        app.Term__c = 24;
        update app;

        newDoc = new NewDocumentCtrl(app);

        pr = newDoc.genDocument();
        System.assertNotEquals(null, pr);
    }
*/

    @isTest static void uploads_cancel_file() {
        Opportunity app = LibraryTest.createApplicationTH();

//        app.Contract_Signed_Date__c = Date.today();

//        update app;

        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);

//        newDoc.showAttachFileBtn = true;
//        newDoc.showUploadPanelGrid = false;
//        PageReference pr = newDoc.uploadFile();
//        System.assertEquals(false, newDoc.showAttachFileBtn);
//        System.assertEquals(true, newDoc.showUploadPanelGrid);

//        pr = newDoc.cancelFileUpload();
//        System.assertEquals(true, newDoc.showAttachFileBtn);
//        System.assertEquals(false, newDoc.showUploadPanelGrid);

//        ProxyApiUtil.settings = LibraryTest.fakeSettings();
//        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
//                'OK',
//                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
//                null);
//        Test.setMock(HttpCalloutMock.class, fakeResponse);
        newDoc.saveApp();

//        List<Apexpages.Message> msgs = ApexPages.getMessages();
//        -2 because actually it shows the successful message after the error message
//        Integer index = (msgs.size() > 0) ? (msgs.size() - 2) : 0;
//        String messageDetail = (msgs.get(index)).getDetail();
//        System.assert(messageDetail.contains('Error : '));

//        newDoc.allApplicantsList = new List<Applicants.AllApplicants>();
//        newDoc.employedApplicants1List = new List<Applicants.EmployedApplicants>();
//        newDoc.employedApplicants2List = new List<Applicants.EmployedApplicants>();
//        newDoc.retiredApplicantsList = new List<Applicants.RetiredApplicants>();
//        newDoc.selfemployedApplicants1List = new List<Applicants.SelfEmployedApplicants>();
//        newDoc.selfemployedApplicants2List = new List<Applicants.SelfEmployedApplicants>();
//        newDoc.miscellaneousApplicantsList = new List<Applicants.MiscellaneousApplicants>();

//        ProxyApiUtil.settings = LibraryTest.fakeSettings();
//        fakeResponse = new SingleRequestMockTest(200,
//                'OK',
//                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
//                null);
//        Test.setMock(HttpCalloutMock.class, fakeResponse);
//        newDoc.saveApp();

//        System.assertEquals(null, newDoc.application.Contract_Signed_Date__c);

//        msgs = ApexPages.getMessages();
//        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
//        System.assert((msgs.get(index)).getDetail().contains('Document Checklist updated.'));
    }

    @isTest static void validates_methods() {

        Opportunity app = LibraryTest.createApplicationTH();
        Attachment attach = new Attachment();
        attach.ParentId = app.id;
        attach.Name = 'Test Attachment';
        attach.description = 'File from APIautoworkflow:test';
        attach.body = blob.valueOf('test');
        insert attach;  

        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        

        Note newNote = newDoc.getmynote();
        newDoc.mynote.title = 'Test Note';
        newDoc.mynote.body = 'Test Body Note';
        List<Note> noteBefore = [select title, body from Note where parentId = : app.id];
        newDoc.Savedoc();
        List<Note> noteAfter = [select title, body from Note where parentId = : app.id];
        System.assertNotEquals(noteBefore.size(), noteAfter.size());
        newDoc.notes = newNote;
        System.assertNotEquals(null, newDoc.notes);
    }
/*

    @isTest static void inserts_document_status() {
        Opportunity app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewDocumentCtrl newDoc = new NewDocumentCtrl(controller);

        newDoc.isSignedContract = false;
        newDoc.attach.name = 'Test';
        list<string> strlst = new list<string> {'a', 'b'};
        string body1 = '';
        for (string s : strlst) {
            body1 = body1 + '' + s;
        }
        Blob response = Blob.valueOf(body1);
        newDoc.attach.body = response;

        List<genesis__Document_Status__c> objBefore = [select genesis__Status__c from genesis__Document_Status__c where genesis__Opportunity__c = : app.id];

        newDoc.processUpload();

        List<genesis__Document_Status__c> objAfter = [select genesis__Status__c from genesis__Document_Status__c where genesis__Opportunity__c = : app.id];

        System.assertNotEquals(objBefore.size(), objAfter.size());

        controller = new ApexPages.StandardController(app);
        newDoc = new NewDocumentCtrl(controller);

        newDoc.isSignedContract = true;
        newDoc.attach.name = 'Test';
        newDoc.attach.body = response;

        newDoc.processUpload();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        //'File uploaded successsfully'
        System.assert((msgs.get(index)).getDetail().contains('successsfully'));

        controller = new ApexPages.StandardController(app);
        newDoc = new NewDocumentCtrl(controller);

        newDoc.isSignedContract = true;
        newDoc.attach.name = 'Test.name';
        newDoc.attach.body = response;

        newDoc.processUpload();

        msgs = ApexPages.getMessages();
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().contains('successsfully'));
        System.assertEquals('Signed Contract.name', newDoc.attach.name);

        String title = 'Signed Contract.name';

        genesis__Document_Status__c docStatus = [SELECT genesis__Status__c, genesis__Attachment_Name__c, genesis__Doc_Name__c
                                                FROM genesis__Document_Status__c
                                                WHERE genesis__Opportunity__c = : app.id
                                                        ORDER BY id DESC
                                                        LIMIT 1];
        System.assertEquals(null, docStatus.genesis__Status__c);
        System.assertEquals(title.split('\\.')[0], docStatus.genesis__Attachment_Name__c);

        controller = new ApexPages.StandardController(app);
        newDoc = new NewDocumentCtrl(controller);

        newDoc.isSignedContract = false;
        newDoc.attach.name = 'Test.name';
        newDoc.attach.body = response;

        genesis__Document_Master__c docMaster = new genesis__Document_Master__c();
        docMaster.genesis__Doc_Name__c = newDoc.attach.name.split('\\.')[0];
        insert docMaster;

        newDoc.processUpload();

        msgs = ApexPages.getMessages();
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().contains('successsfully'));

        docStatus = [SELECT genesis__Status__c, genesis__Attachment_Name__c, genesis__Doc_Name__c FROM genesis__Document_Status__c WHERE genesis__Doc_Name__c = : docMaster.id LIMIT 1];
        System.assertEquals('CREATED', docStatus.genesis__Status__c);

        //Catch exception
        newDoc.processUpload();

        msgs = ApexPages.getMessages();
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        //'Some exception has occured'
        System.assert((msgs.get(index)).getDetail().contains('exception'));

    }
*/
    @isTest static void gets_yes_or_not_options() {
        Opportunity app = LibraryTest.createApplicationTH();

        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);

        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('true', 'Yes'));
        options.add(new SelectOption('false', 'No'));

        List<SelectOption> listOptions = newDoc.getYesOrNotOptions();

        System.assertEquals(options, listOptions);

        List<SelectOption> years = new List<SelectOption>();
        Integer currentYear = System.Today().year() - 10;

        for (Integer i = currentYear; i < currentYear + 20; i++) {
            years.add(new SelectOption(String.valueof(i), String.valueof(i)));
        }

        List<SelectOption> listYears = newDoc.getYears();

        System.assertEquals(years, listYears);
    }
/*
    @isTest static void inserts_note_income_edit() {
        Opportunity app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewDocumentCtrl newDoc = new NewDocumentCtrl(controller);

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        newDoc.initialize();

        String newXml = '<?xml version="1.0"?><Items></Items>';

        newDoc.insertNoteIncomeEdit(newXml, newXml, 'Income', 'Employed');

        List<Applicants.EmployedApplicants> lapp2 = new List<Applicants.EmployedApplicants>();
        List<Applicants.EmployedApplicants> lapp1 = new List<Applicants.EmployedApplicants>();
        List<Applicants.EmployedApplicants> lapp3 = new List<Applicants.EmployedApplicants>();

        Applicants apps = new Applicants();

        Applicants.EmployedApplicants apps2 = new Applicants.EmployedApplicants(1010, true, 'defaultName');
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        apps2.income = '20000';
        lapp2.add(apps2);

        String result = apps.GetXmlFromEmployedApplicantsList(lapp2);

        newDoc.application.Employed_Applicants_1_XML__c = result;

        update newDoc.application;

        newDoc.insertNoteIncomeEdit(result, null, 'Income', 'Employed');

        apps2 = new Applicants.EmployedApplicants(1020, true, 'testName');
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        apps2.income = '35000';
        lapp1.add(apps2);

        String result2 = apps.GetXmlFromEmployedApplicantsList(lapp1);

        List<Note> noteBefore = [select title, body from Note where parentId = : app.id];
        newDoc.insertNoteIncomeEdit(result, result2, 'Income', 'Employed');
        List<Note> noteAfter = [select title, body from Note where parentId = : app.id];
        System.assertNotEquals(noteBefore.size(), noteAfter.size());

        newDoc.insertNoteIncomeEdit(result, result2, 'Income', 'Miscellaneous');
        noteBefore = [select title, body from Note where parentId = : app.id];
        System.assertNotEquals(noteBefore.size(), noteAfter.size());

        newDoc.insertNoteIncomeEdit(result, result2, 'Income', 'Retired');
        noteAfter = [select title, body from Note where parentId = : app.id];
        System.assertNotEquals(noteBefore.size(), noteAfter.size());

        newDoc.insertNoteIncomeEdit(result, result2, 'Income', 'SelfEmployed');
        noteBefore = [select title, body from Note where parentId = : app.id];
        System.assertNotEquals(noteBefore.size(), noteAfter.size());

        Applicants.EmployedApplicants apps3 = new Applicants.EmployedApplicants(1010, true, 'defaultName');
        apps3.required = 'true';
        apps3.receivedDate = date.today().format();
        lapp3.add(apps3);

        result2 = apps.GetXmlFromEmployedApplicantsList(lapp3);

        newDoc.insertNoteIncomeEdit(result2, result2, 'Income', 'Employed');
    }
*/
    @isTest static void pull_hard_credit_report() {
        Opportunity app = LibraryTest.createApplicationTH();
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        NewDocumentCtrl.ProcessResult pr = new NewDocumentCtrl.ProcessResult();
        pr = newDoc.PullHardCreditReport(pr);
        System.assertEquals(false, newDoc.HardcreditPullPending);
    }
/*
    @isTest static void next_action_logic() {
        Opportunity app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewDocumentCtrl newDoc = new NewDocumentCtrl(controller);

        try {
            PageReference result = newDoc.nextActionLogic();

            System.assertEquals(null, result);

        } catch (Exception e) {
            //sObject type 'Conversion_Mapping__c' is not supported.
        }
    }
    */


    @isTest static void uploads_document_invite() {

        Opportunity app = LibraryTest.createApplicationTH();

        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        System.assertNotEquals(null, newDoc.application);
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        List<EmailTemplate> e = [select id from EmailTemplate where DeveloperName = 'Document_Portal_Template'];

        System.RunAs(usr) {
            Test.startTest();
            delete e;
            EmailTemplate eTemplate = new EmailTemplate(FolderId = UserInfo.getUserId(), TemplateType = 'Custom', name = 'Document Portal Template', HtmlValue = 'Welcome', Subject = 'subject', Body = 'Body', DeveloperName = 'Document_Portal_Template');
            insert eTemplate;
            system.debug(eTemplate.Body);
            NewDocumentCtrl.ProcessResult result = new  NewDocumentCtrl.ProcessResult();
            result = newDoc.documentUploadInvite(app);
            System.assertEquals(null, result.errorMessage);

            Test.stopTest();
        }
    }

    /*
    @isTest static void uploads_document_email_flow() {

        List<id> appList = new List<id>();

        Opportunity app = LibraryTest.createApplicationTH();
        appList.add(app.id);

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        NewDocumentCtrl newDoc = new NewDocumentCtrl(controller);

        Opportunity app2 = LibraryTest.createApplicationTH();
        appList.add(app2.id);

        System.assertNotEquals(null, newDoc.application);
        System.assertNotEquals(null, newDoc.contact);
        system.debug( newDoc.application); system.debug(newDoc.contact);
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        List<EmailTemplate> e = [select id from EmailTemplate where DeveloperName = 'Document_Portal_Template'];

        System.RunAs(usr) {
            Test.startTest();
            delete e;
            EmailTemplate eTemplate = new EmailTemplate(FolderId = UserInfo.getUserId(), TemplateType = 'Custom', name = 'Document Portal Template', HtmlValue = 'Welcome', Subject = 'subject', Body = 'Body', DeveloperName = 'Document_Portal_Template');
            insert eTemplate;
            system.debug(eTemplate.Body);


            //PageReference result = newDoc.documentUploadEmailFlow(appList);
            NewDocumentCtrl.documentUploadEmailFlow(appList);
            //System.assertEquals(null, result);

            Test.stopTest();
        }
    }
*/

    @isTest static void replaces_values_from_data_test() {
        string bodyMail = 'Welcome {!Contact.Id}. Your Token is {!token} Thanks.';
        string contactName = 'John Smith';
        string token = 'abccdsf';
        string result = NewDocumentCtrl.replaceValuesFromData(bodyMail, contactName, token);
        System.assertNotEquals(null, result);

    }

    @isTest static void test_insertEmailTemplateCopyIntoContact() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Bureau__c = 'EX';
        app.Manual_Grade__c = 'GRade';
        app.FICO__c =  '123';
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        newDoc.insertEmailTemplateCopyIntoContact();

    }

    @isTest static void test_replaceValuesFromTemplateData() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Bureau__c = 'EX';
        app.Manual_Grade__c = 'GRade';
        app.FICO__c =  '123';
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        String res = newDoc.replaceValuesFromTemplateData('{!Opportunity.Manual_Grade__c},{!Opportunity.Bureau__c},{!Opportunity.FICO__c}');
        System.assertNotEquals(null, res);

    }

    @isTest static void test_isAppDecline() {

        Opportunity app = LibraryTest.createApplicationTH();
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        System.assertEquals(false, newDoc.isAppDecline(app.Id));

    }

    @isTest static void test_isAppPendingToReview() {

        Opportunity app = LibraryTest.createApplicationTH();
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        System.assertEquals(false, newDoc.isAppPendingToReview(app.Id));

    }

    @isTest static void test_quickSaveApp() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Payment_Method__c = 'ACH';
        update app;
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app);
        NewDocumentCtrl.QuickSaveResult qsr = newDoc.quickSaveApp(null);
        System.assertEquals(qsr.qsErrorMessage, 'Opportunity is null');

        app = LibraryTest.createApplicationTH();
        app.Payment_Method__c = 'ACH';
        update app;
        newDoc = new NewDocumentCtrl(app);
        qsr = newDoc.quickSaveApp(app);
        system.debug('DG TEST: NewDocumentCtrl.QuickSaveResult -- ' + JSON.serialize(qsr));

    }

    @isTest static void test_constructor() {

        Opportunity app = LibraryTest.createApplicationTH();
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app.Id);
        System.assertNotEquals(null, newDoc.application);

    }

    @isTest static void test_documentsRequestChecklistBehavior() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.All_Applicants_XML__c = '<?xml version="1.0"?><Items><Item><Id>1010</Id><Required>true</Required><Approved>false</Approved><ReceivedDate>07/19/2017</ReceivedDate><YodleeFlag>false</YodleeFlag><CompledteBy>Deborah Sexton</CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1020</Id><Required>true</Required><ReceivedDate>07/19/2017</ReceivedDate><YodleeFlag>false</YodleeFlag><CompledteBy>Deborah Sexton</CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1030</Id><Required>true</Required><ReceivedDate>07/19/2017</ReceivedDate><YodleeFlag></YodleeFlag><CompledteBy>Deborah Sexton</CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1035</Id><Required>true</Required><ReceivedDate>11/14/2017</ReceivedDate><YodleeFlag></YodleeFlag><CompledteBy>Deborah Sexton</CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1040</Id><Required>true</Required><ReceivedDate>07/19/2017</ReceivedDate><YodleeFlag></YodleeFlag><CompledteBy>Deborah Sexton</CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1045</Id><Required>true</Required><ReceivedDate>07/19/2017</ReceivedDate><YodleeFlag></YodleeFlag><CompledteBy>Deborah Sexton</CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1050</Id><Required>false</Required><ReceivedDate></ReceivedDate><YodleeFlag></YodleeFlag><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1055</Id><Required>false</Required><ReceivedDate></ReceivedDate><YodleeFlag></YodleeFlag><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1060</Id><Required>false</Required><ReceivedDate></ReceivedDate><YodleeFlag></YodleeFlag><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1065</Id><Required>true</Required><ReceivedDate></ReceivedDate><YodleeFlag></YodleeFlag><CompledteBy></CompledteBy><Dispositions></Dispositions></Item></Items>';
        app.Employed_Applicants_1_XML__c = '<?xml version="1.0"?><Items><Item><Id>1010</Id><Required>true</Required><Approved>false</Approved><ReceivedDate>07/17/2017</ReceivedDate><Income>25,560.01</Income><PaymentPeriod>06/17/2017</PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome>Deborah Sexton</compledteByIncome><Dispositions>--None--</Dispositions></Item><Item><Id>1020</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions>--None--</Dispositions></Item><Item><Id>1030</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions>--None--</Dispositions></Item><Item><Id>1040</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions>--None--</Dispositions></Item><Item><Id>1050</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions>--None--</Dispositions></Item><Item><Id>1060</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod>2016</PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions>--None--</Dispositions></Item><Item><Id>1070</Id><Required>true</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions></Dispositions></Item></Items>';
        app.Employed_Applicants_2_XML__c = '<?xml version="1.0"?><Items><Item><Id>1010</Id><Required>true</Required><Approved>false</Approved><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions></Dispositions></Item><Item><Id>1020</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions></Dispositions></Item><Item><Id>1030</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions></Dispositions></Item><Item><Id>1040</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions></Dispositions></Item><Item><Id>1050</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions></Dispositions></Item><Item><Id>1060</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod>2016</PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions></Dispositions></Item><Item><Id>1070</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><PaymentPeriod></PaymentPeriod><CompledteBy></CompledteBy><compledteByIncome></compledteByIncome><Dispositions></Dispositions></Item></Items>';
        app.Retired_Applicants_XML__c = '<?xml version="1.0"?><Items><Item><Id>1010</Id><Required>true</Required><Approved>false</Approved><Form1099Income></Form1099Income><Form1099ReceivedDate></Form1099ReceivedDate><BenefitLetterIncome></BenefitLetterIncome><BenefitLetterReceivedDate></BenefitLetterReceivedDate><CompledteBy></CompledteBy><compledteByCurrentYear></compledteByCurrentYear><Dispositions>--None--</Dispositions></Item><Item><Id>1020</Id><Required>true</Required><Form1099Income></Form1099Income><Form1099ReceivedDate></Form1099ReceivedDate><BenefitLetterIncome>2,518.00</BenefitLetterIncome><BenefitLetterReceivedDate>07/18/2017</BenefitLetterReceivedDate><CompledteBy></CompledteBy><compledteByCurrentYear>Brandon Kaudy</compledteByCurrentYear><Dispositions>--None--</Dispositions></Item><Item><Id>1030</Id><Required>true</Required><Form1099Income></Form1099Income><Form1099ReceivedDate>07/18/2017</Form1099ReceivedDate><BenefitLetterIncome>2,051.47</BenefitLetterIncome><BenefitLetterReceivedDate>07/19/2017</BenefitLetterReceivedDate><CompledteBy></CompledteBy><compledteByCurrentYear>Deborah Sexton</compledteByCurrentYear><Dispositions>--None--</Dispositions></Item><Item><Id>1040</Id><Required>false</Required><Form1099Income></Form1099Income><Form1099ReceivedDate></Form1099ReceivedDate><BenefitLetterIncome></BenefitLetterIncome><BenefitLetterReceivedDate></BenefitLetterReceivedDate><CompledteBy></CompledteBy><compledteByCurrentYear></compledteByCurrentYear><Dispositions></Dispositions></Item></Items>';
        app.Self_Employed_Applicants_1_XML__c = '<?xml version="1.0"?><Items><Item><Id>1010</Id><Required>true</Required><Approved>false</Approved><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue>2017</CalendarValue><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1020</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue>2017</CalendarValue><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1030</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue>2017</CalendarValue><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1040</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue></CalendarValue><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1050</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue></CalendarValue><CompledteBy></CompledteBy><Dispositions></Dispositions></Item></Items>';
        app.Self_Employed_Applicants_2_XML__c = '<?xml version="1.0"?><Items><Item><Id>1010</Id><Required>true</Required><Approved>false</Approved><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue>2017</CalendarValue><CompledteBy></CompledteBy><Dispositions></Dispositions></Item><Item><Id>1020</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue>2017</CalendarValue><CompledteBy></CompledteBy><Dispositions></Dispositions></Item><Item><Id>1030</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue>2017</CalendarValue><CompledteBy></CompledteBy><Dispositions></Dispositions></Item><Item><Id>1040</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue></CalendarValue><CompledteBy></CompledteBy><Dispositions></Dispositions></Item><Item><Id>1050</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><NetIncome></NetIncome><CalendarValue></CalendarValue><CompledteBy></CompledteBy><Dispositions></Dispositions></Item></Items>';
        app.Miscellaneous_Applicants_XML__c = '<?xml version="1.0"?><Items><Item><Id>1010</Id><Required>true</Required><Approved>false</Approved><ReceivedDate></ReceivedDate><Income></Income><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1020</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><CompledteBy></CompledteBy><Dispositions>--None--</Dispositions></Item><Item><Id>1030</Id><Required>false</Required><ReceivedDate></ReceivedDate><Income></Income><CompledteBy></CompledteBy><Dispositions></Dispositions></Item></Items>';
        update app;
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app.Id);

        IDology_Request__c idr = new IDology_Request__c();
        idr.Verification_Result__c = 'Pass';
        idr.Contact__c = app.Contact__c;
        insert idr;

        newDoc.documentsRequestChecklistBehavior();

    }

    @isTest static void test_getStringLengthDifference() {
        System.assertEquals(false, NewDocumentCtrl.getStringLengthDifference('123456'));
    }

    @isTest static void test_documentUploadEmailFlow() {
        Opportunity app = LibraryTest.createApplicationTH();
        List<Id> idList = new List<Id>();
        idList.add(app.Id);
        NewDocumentCtrl.documentUploadEmailFlow(idList);
    }

    @isTest static void test_processResult() {

        NewDocumentCtrl.ProcessResult pr = new NewDocumentCtrl.ProcessResult();
        pr.warningMessage = 'There is a warning';
        System.assertEquals(true, pr.hasWarning);

    }

    @isTest static void test_getOtherApplicantsDispos() {
        Opportunity app = LibraryTest.createApplicationTH();
        NewDocumentCtrl newDoc = new NewDocumentCtrl(app.Id);
        System.assertNotEquals(null, newDoc.getOtherApplicantsDispos());
        System.assertNotEquals(null, newDoc.getEmpAppsOtherDispos());
        System.assertNotEquals(null, newDoc.getYtdGrossDispos());
        System.assertNotEquals(null, newDoc.getAllAppsOtherDispos());
        System.assertNotEquals(null, newDoc.getBankStatementsDispos());
    }
        
    
    @isTest static void test_TotalProviderPayments(){
        Opportunity app = LibraryTest.createApplicationTH();
        Provider_Payments__c p = new Provider_Payments__c (Payment_Amount__c =500,Opportunity__c = app.id);
        insert p;
        
         System.assertEquals(500,NewDocumentCtrl.getTotalProviderPayments(app));
    }
    
     @isTest static void Test_checkValuesForOffer(){
        
        Check_Values_Offer_Selection__c setting2 = New Check_Values_Offer_Selection__c();
        setting2.Name = 'Finwise_GA_-k_3k';
        setting2.check_Product_Name__c = '';
        setting2.check_Loan_Amount_min__c = 0;
        setting2.check_Loan_Amount_max__c = 4700;
        setting2.check_Effective_APR_max__c = 36 ;
        setting2.check_Fee_Percent_min__c = 0;
        setting2.check_Fee_Percent_max__c  = 6 ;
        setting2.check_States_Included__c = 'GA';
        setting2.check_IsFinwise__c = false;
        setting2.check_IsFEB__c = true;
        insert setting2;
        
        Opportunity opp = Librarytest.createApplicationTH();
        opp.Fee__c = 120.00;
        opp.LeadSource__c  = 'EZVERIFY';
        opp.ACH_Bank_Name__c = 'Test bank';
        opp.Effective_APR__c = 36.89;
        opp.Payment_Method__C = 'Monthly';
        update opp;
       
        NewDocumentCtrl newDoc = new NewDocumentCtrl(opp);
        NewDocumentCtrl.ProcessResult pr = newDoc.genDocument();
        system.assertEquals('The rate on the offer selected is not permitted for this state. Please check the offer and correct it.\n', pr.errorMessage);
    }

}