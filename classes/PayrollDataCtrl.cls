public with sharing class PayrollDataCtrl {

    @AuraEnabled
    public static PayrollDataEntity getPayrollDataEntity(Id oppId) {

        PayrollDataEntity pde = new PayrollDataEntity();

        try {
            Opportunity opp = [
                    SELECT Id, Payroll_Frequency__c, Next_Payroll_Date__c, Payment_Method__c, Contact__r.MailingState
                    FROM Opportunity
                    WHERE Id = :oppId
                    LIMIT 1
            ];

            if (opp == null) {
                pde.errorMessage = 'Opportunity data is invalid';
                return pde;
            }

            pde.opportunity = opp;

            //Set the picklist values for Salary Cycle
            pde.salaryCycleOptions =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Payroll_Frequency__c.getDescribe(),
                            opp.Payroll_Frequency__c,
                            false
                    );

            pde.paymentMethodOptions =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Payment_Method__c.getDescribe(),
                            opp.Payment_Method__c,
                            false
                    );

        } catch (Exception e) {
            String exceptionMessage = e.getMessage() + ' - ' + e.getStackTraceString();
            system.debug('PayrollDataCtrl.getPayrollDataEntity: ' + exceptionMessage);
            pde.errorMessage = exceptionMessage;
            return pde;
        }

        return pde;
    }

    @AuraEnabled
    public static PayrollDataEntity savePayrollData(Opportunity opp) {

        PayrollDataEntity pde = new PayrollDataEntity();

        try {
            OfferCtrl.doCalculationStartDate(opp, null);	//MAINT-575
            update opp;
        } catch (Exception e) {
            String exceptionMessage = e.getMessage() + ' - ' + e.getStackTraceString();
            system.debug('PayrollDataCtrl.savePayrollData: ' + exceptionMessage);
            pde.errorMessage = exceptionMessage;
            return pde;
        }

        return pde;
    }

    public class PayrollDataEntity {
        @AuraEnabled public Boolean hasError;
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = !String.isEmpty(errorMessage);
            }
        }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> salaryCycleOptions;
        @AuraEnabled public List<alphaHelperCls.PicklistItem> paymentMethodOptions;
        @AuraEnabled public Opportunity opportunity;

        public PayrollDataEntity() {
            hasError = false;
        }
    }

}