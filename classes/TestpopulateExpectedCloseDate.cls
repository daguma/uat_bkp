@IsTest
public class TestpopulateExpectedCloseDate {
    @IsTest static void verifypopulateExpectedCloseDate() {

        
         integer randomInt = (integer)(100.0 * Math.random());
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User usr = new User(LastName = 'test'+randomInt,
                           FirstName='example',
                           Alias = 'jex'+randomInt,
                           Email = 'example'+randomInt+'.test@asdf.com',
                           Username = 'example'+randomInt+'.test@asdf.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                           );
        insert usr;
        //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Annual_Income__c = 10000;
        insert cntct;

        //Dummy Application
        genesis__Applications__c aplcn = new genesis__Applications__c();
        aplcn.genesis__Contact__c = cntct.id;
        aplcn.ACH_Bank_Name__c = 'Dummy Bank';
        aplcn.genesis__Expected_Start_Date__c = system.today();
        aplcn.genesis__Expected_First_Payment_Date__c = system.today() + 30;
        aplcn.genesis__Loan_Amount__c = 8500;
        aplcn.genesis__Payment_Amount__c = 371.54;
        aplcn.genesis__Term__c = 36;
        aplcn.genesis__Payment_Frequency__c = 'MONTHLY';
        aplcn.Selected_Offer_Term__c  = 36;
        insert aplcn;
        aplcn.FS_Assigned_To_Val__c = 'sample@123.com';
        aplcn.Selected_Offer_Term__c  = 36;
        update aplcn;
        
		aplcn = [Select Id, genesis__APR__c from genesis__Applications__c where Id = : aplcn.Id];
        System.assert(aplcn.genesis__APR__c != null);
        
       
    }
    
}