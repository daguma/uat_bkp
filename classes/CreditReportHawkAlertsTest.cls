@isTest
private class CreditReportHawkAlertsTest {

    @isTest static void extracts_hawk_alerts_codes() {
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentHawkAlerts();

        List<String> result = CreditReportHawkAlerts.extracHawkAlertCodes(segmentList);
        String expected = '0001';
        System.assertEquals(expected, result[0]);
    }

    @isTest static void gets_hawk_alerts_from_credit_report() {
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentHawkAlerts();

        List<CreditReportHawkAlertsResult> result = CreditReportHawkAlerts.getHawkAlertsFromCR(segmentList);
        String expected = '0001 - Input/File (Current/Previous) Address Is A Mail Receiving/Forwarding Service';
        System.assertEquals(expected, result[0].alertReasons[0]);
    }

    @isTest static void gets_hawk_alerts() {
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentHawkAlerts();

        List<String> hawkAlertsCodes = CreditReportHawkAlerts.extracHawkAlertCodes(segmentList);

        List<CreditReportHawkAlertsResult> result = CreditReportHawkAlerts.getHawkAlerts(hawkAlertsCodes);
        String expected = '0001 - Input/File (Current/Previous) Address Is A Mail Receiving/Forwarding Service';
        System.assertEquals(expected, result[0].alertReasons[0]);
    }

    @isTest static void gets_status_by_codes() {
        Set<String> result = CreditReportHawkAlerts.getStatusByCodes(new List<String> { '1503'});

        System.assert(result.contains(CreditReportHawkAlerts.CREDIT_REVIEW));
    }

    @isTest static void evaluates_hawk_alerts() {
        Opportunity newApplication = LibraryTest.createApplicationTH();

        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentHawkAlerts();

        CreditReportHawkAlerts.evaluateHawkAlerts(newApplication.Id, segmentList);

        Opportunity application = [Select Id,
                                                Status__c,
                                                Reason_of_Opportunity_Status__c,
                                                Review_Reason__c,
                                                Hawk_Alert_Codes__c
                                                from Opportunity
                                                where Id = : newApplication.Id];

        System.assertEquals(CreditReportHawkAlerts.CREDIT_REVIEW, application.Status__c);
        System.assertEquals('0001', application.Hawk_Alert_Codes__c);
    }
    
    @isTest static void evaluate_hawk_alerts_thr_brms() {
    
        Test.starttest();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity newApplication = LibraryTest.createApplicationTH();

        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentHawkAlerts();
        ProxyApiCreditReportRuleEntity hawkRule = LibraryTest.fakeBrmsHawkDeclineRuleEntity();
        List<ProxyApiCreditReportRuleEntity> ruleList = new List<ProxyApiCreditReportRuleEntity>();
        Test.stoptest();
        ruleList.add(hawkRule);

        CreditReportHawkAlerts.evaluateHawkAlertsThrBRMS(newApplication.Id,ruleList,segmentList);

        Opportunity application = [Select Id,
                                                Status__c,
                                                Reviewed_and_Cleared__c,
                                                Reason_of_Opportunity_Status__c,
                                                Review_Reason__c,
                                                Hawk_Alert_Codes__c
                                                from Opportunity
                                                where Id = : newApplication.Id];
                                                
        system.assertEquals(false, application.Reviewed_and_Cleared__c);
    }
}