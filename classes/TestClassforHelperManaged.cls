@isTest
public class TestClassforHelperManaged {

    public static Date systemDate = Date.today().addDays(-16);
    public static Date branchSysDate;
    private static ID rootBranchRecordTypeId = null;
    
    public static testMethod void test1(){
        
        Test.startTest();
         loan.TestHelper.createSeedDataForTesting();
        Account acc = new Account();
        acc.Name = 'Dummy';
        String accountUsage = 'Borrower/Investor';
        insert acc;
        Contact dummyCon = TesthelperforManaged.createContact('Dummy', 'other', '84321', 'State');
        
        //Create CL Product
        clcommon__CL_Product__c product = new clcommon__CL_Product__c();
        product.clcommon__Product_Name__c = 'Dummy Product';
        
        //Create a Company
        genesis__Company__c company = TestHelperForManaged.createCompany('Test');
                
        //TestHelperForManaged thm = new TestHelperForManaged();
        //TestHelperForManaged.getBranchDate();
        TestHelperForManaged.createBankAccount(acc, accountUsage);
        
        //TestHelperForManaged.createContact('Dummy', 'other', '84321', 'State');
        
        TestHelperForManaged.createCompany('Cloud Lending');
        TestHelperForManaged.createBorrower('Account name');
        TestHelperForManaged.createLoanPurpose();
        TestHelperForManaged.createSeedDataForTesting();
        TestHelperForManaged.createCurrency();
        TestHelperForManaged.setupApprovalProcessForTxn();
        TestHelperForManaged.setupMetadataSegments();
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupORGParametersActive();
        TestHelperForManaged.getRootBranchRecordTypeID();
        TestHelperForManaged.createLoanPurpose('Purpose','Code Purpose','Description Purpose');
		TestHelperForManaged.createApplication(dummyCon, product, company);  
        //TestHelperForManaged.getBranchDate();  
        Test.stopTest();
    }
    
    public static testMethod void test2(){
        Test.startTest();
         loan.TestHelper.createSeedDataForTesting();
        loan__MF_Account__c dummyAcc = loan.TestHelper.createMFAccount('DummyAccount','10000 - ASSETS');
        User u = loan.TestHelper.createUser('testUser1', 'MyOffice1');
        
        Contact dummyContact = TestHelperForManaged.createContact('dummyLA','Other','87123','Test State');
        Account dummyAccount = new Account();
        dummyAccount.Name = 'DummyAccount';
        dummyAccount.c2g__CODABankName__c = 'Test Bank';
        dummyAccount.c2g__CODABankAccountNumber__c = '123456';
        dummyAccount.c2g__CODAPaymentMethod__c = 'Cash';
        
        insert dummyAccount;
         loan.TestHelper.useCLLoanCRM();
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        Loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        Loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        Loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                                                                          dummyAcc, 
                                                                          curr, 
                                                                          dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose(); 

        
        TestHelperForManaged.createLoanAccountForAccountObj(dummyLP, dummyAccount, dummyFeeSet,dummyLoanPurpose,dummyOffice,false,Date.today().addDays(30),Date.today().addDays(30),'Monthly');
         Test.stopTest();
    }
}