@isTest public class CalculateWorkDaysTest {
    @isTest static void CalculateWorkDaysTest(){
        Date startDate = Date.newInstance(2019,01,10);
        Date testEndDate = Date.newInstance(2019,01,14);
		Date newEndDate = CalculateWorkDays.dateWorkDays(startDate, 2, false);
		system.assertEquals(newEndDate, testEndDate);
        
        startDate = Date.newInstance(2019,01,14);
        testEndDate = Date.newInstance(2019,01,10);
		newEndDate = CalculateWorkDays.dateWorkDays(startDate, -2, false);
		system.assertEquals(newEndDate, testEndDate);
    }

}