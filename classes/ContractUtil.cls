public class ContractUtil {

    //Date systemDate = new loan.GlobalLoanUtilFacade().getCurrentSystemDate();
    //ContractUtil.previewReschedule('a1u0B000006FkuzQAC',systemDate.addDays(30),0,14,'Monthly',false,20,null,null);

    /**
    * Returns a preview for a Loan Reschedule according to the parameters passed.
    * It uses Cloud Lending's methods:
    * - loan.LoanRescheduleParameters rescheduleParams = new loan.LoanRescheduleParameters( Map<String, Object> );
    * - List<loan.LoanRescheduleParameters.ProposedRepaymentPlan> repaymentPlanList = rescheduleParams.getRepaymentPlan();
    * - loan.LoanActionFactory factory = new loan.LoanActionFactory();
    * - Loan.LoanAction10 loanAction = factory.getLoanAction10API();
    * - Map<String, Object> previewRescheduleMap = loanAction.previewReschedule(rescheduleParams);
    * @param  Id contractId
    * @param  Date repaymentStartDate
    * @param  Integer repaymentStartDay: if "0", it uses the day from the @param repaymentStartDate
    * @param  Integer numberOfInstallments
    * @param  String frequencyOfLoanPayment: contract's loan__Frequency_of_Loan_Payment__c
    * @param  Boolean maintainDelinquency
    * @param  Decimal interestRate
    * @param  Decimal loanPaymentAmount: this can be null if needed
    * @param  Decimal principalRemaining
    * @return  mean
    * */
    public static void previewReschedule(
              Id contractId
            , Date repaymentStartDate
            , Integer repaymentStartDay
            , Integer numberOfInstallments
            , String frequencyOfLoanPayment
            , Boolean maintainDelinquency
            , Decimal interestRate
            , Decimal loanPaymentAmount
            , Decimal principalRemaining){



        Integer amortizationTerm = 0;



        Map<String, Object> rescheduleParametersmap = new Map<String, Object>();
        rescheduleParametersmap.put('Loan_Account__c', contractId);
        rescheduleParametersmap.put('Txn_Date__c', new loan.GlobalLoanUtilFacade().getCurrentSystemDate());
        rescheduleParametersmap.put('Repayment_Start_Date__c', repaymentStartDate);
        rescheduleParametersmap.put('Second_Installment_Date__c', null);
        rescheduleParametersmap.put('OT_Maturity_Date__c', null);
        rescheduleParametersmap.put('Number_of_Installments__c', numberOfInstallments);
        rescheduleParametersmap.put('Interest_Only_Period__c', 0);
        rescheduleParametersmap.put('Interest_Rate__c', interestRate);
        rescheduleParametersmap.put('Frequency_of_Loan_Payment__c', frequencyOfLoanPayment);
        rescheduleParametersmap.put('Frequency_of_Loan_Payment__c', 'Weekly'.equals(frequencyOfLoanPayment) ? '4' : '1');
        rescheduleParametersmap.put('Maintain_Delinquency__c', maintainDelinquency);
        rescheduleParametersmap.put('Actual_Interest_Only_Payments__c', true);
        rescheduleParametersmap.put('Amortization_Term__c', amortizationTerm);
        rescheduleParametersmap.put('New_Due_Day__c', (repaymentStartDay == 0 || repaymentStartDay == null) ? repaymentStartDate.day() : repaymentStartDay);
        rescheduleParametersmap.put('StepUp_Option__c', null);
        rescheduleParametersmap.put('Interest_Only_Payment_Amt__c', 0);
        rescheduleParametersmap.put('Same_Monthly_Payment__c', true);
        //rescheduleParametersmap.put('Reschedule_Balance__c', String.valueOf(principalRemaining));
        if(loanPaymentAmount != null){
            rescheduleParametersmap.put('Payment_Amount__c', loanPaymentAmount);
        }

        //Cloud Lending's code for rescheduling
        loan.LoanRescheduleParameters rescheduleParams = new loan.LoanRescheduleParameters(rescheduleParametersmap);
        system.debug('ContractUtil.previewReschedule> rescheduleParams: ' + rescheduleParams);

        //List<loan.LoanRescheduleParameters.ProposedRepaymentPlan> repaymentPlanList = rescheduleParams.getRepaymentPlan();
        //system.debug('ContractUtil.previewReschedule> repaymentPlanList: ' + JSON.serialize(repaymentPlanList));

        loan.LoanActionFactory factory = new loan.LoanActionFactory();
        system.debug('ContractUtil.previewReschedule> factory: ' + factory);

        loan.LoanAction10 loanAction = factory.getLoanAction10API();
        system.debug('ContractUtil.previewReschedule> loanAction: ' + loanAction);

        Map<String, Object> previewRescheduleMap = loanAction.previewReschedule(rescheduleParams);
        system.debug('ContractUtil.previewReschedule> previewRescheduleMap: ' + JSON.serialize(previewRescheduleMap));

        //loanAction.rescheduleALoan(rescheduleParams); //—→ commit

    }


}