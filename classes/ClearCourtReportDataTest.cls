@isTest private class ClearCourtReportDataTest {
	@isTest static void validates() {

        ClearCourtReportData ccrd = new ClearCourtReportData();

        ccrd.documentType = 'DocType';
        ccrd.filedDate = '07-04-1776';
        ccrd.amount = '$1,000.00';
        ccrd.debtorName = 'JOHN RAMBO';
        ccrd.typeOfAction = 'DESTROY';

        System.assertEquals('DocType', ccrd.documentType);
        System.assertEquals('07-04-1776', ccrd.filedDate);
        System.assertEquals('$1,000.00', ccrd.amount);
        System.assertEquals('JOHN RAMBO', ccrd.debtorName);
        System.assertEquals('DESTROY', ccrd.typeOfAction);
    }
	
}