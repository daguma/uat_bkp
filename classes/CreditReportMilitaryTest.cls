@isTest private class CreditReportMilitaryTest {

	private static final String TU = 'TU';
	private static final String EX = 'EX';
	private static final String TU_MILITARY_SEGMENT = 'AO01';
	private static final String EX_MILITARY_SEGMENT = '361';
	private static final String TU_CODE = '07051';
	private static final String EX_CODE = '57';
	private static final String TU_SERVICE_CODE = 'Service Code';
	private static final String EX_MESSAGE_CODE = 'Message Code';
	private static final String TU_SEARCH_STATUS = 'Search Status';
	private static final String EX_MESSAGE_TEXT = 'Message Text';

	@isTest static void is_military_tu() {

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(TU_MILITARY_SEGMENT, TU_SERVICE_CODE, TU_CODE);

		Boolean result = CreditReportMilitary.isMilitary(crSegmentsList, 'TU');

		System.assertEquals(true, result);
	}

	@isTest static void is_not_military_tu() {

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(TU_MILITARY_SEGMENT, TU_SERVICE_CODE, EX_CODE);

		Boolean result = CreditReportMilitary.isMilitary(crSegmentsList, 'TU');

		System.assertEquals(false, result);
	}

	@isTest static void is_military_ex() {

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(EX_MILITARY_SEGMENT, EX_MESSAGE_CODE, EX_CODE, EX_MILITARY_SEGMENT, EX_MESSAGE_TEXT, '1203');

		Boolean result = CreditReportMilitary.isMilitary(crSegmentsList, 'EX');

		System.assertEquals(true, result);
	}

	@isTest static void is_not_military_ex() {

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(EX_MILITARY_SEGMENT, EX_MESSAGE_CODE, TU_CODE);

		Boolean result = CreditReportMilitary.isMilitary(crSegmentsList, 'EX');

		System.assertEquals(false, result);
	}

	@isTest static void gets_military_designation_for_ex() {

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(EX_MILITARY_SEGMENT, EX_MESSAGE_CODE, EX_CODE, EX_MILITARY_SEGMENT, EX_MESSAGE_TEXT, '1203');

		String result = CreditReportMilitary.getMilitaryDesignation(crSegmentsList, 'EX');

		System.assertEquals('1203', result);
	}

	@isTest static void does_not_get_military_designation_for_ex() {

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(EX_MILITARY_SEGMENT, EX_MESSAGE_CODE, TU_CODE, EX_MILITARY_SEGMENT, EX_MESSAGE_TEXT, '0335');

		String result = CreditReportMilitary.getMilitaryDesignation(crSegmentsList, 'EX');

		System.assertEquals('', result);
	}

	@isTest static void gets_military_designation_for_tu() {

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(TU_MILITARY_SEGMENT, TU_SERVICE_CODE, TU_CODE, TU_MILITARY_SEGMENT, TU_SEARCH_STATUS, 'M01');

		String result = CreditReportMilitary.getMilitaryDesignation(crSegmentsList, 'TU');

		System.assertEquals('M01', result);
	}

	@isTest static void does_not_get_military_designation_for_tu() {

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(TU_MILITARY_SEGMENT, TU_SERVICE_CODE, EX_CODE, TU_MILITARY_SEGMENT, TU_SEARCH_STATUS, 'M01');

		String result = CreditReportMilitary.getMilitaryDesignation(crSegmentsList, 'TU');

		System.assertEquals('', result);
	}

	@isTest static void changes_military_flag_for_tu() {
		Contact c = LibraryTest.createContactTH();
		c.Military__c = false;
		update c;

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(TU_MILITARY_SEGMENT, TU_SERVICE_CODE, TU_CODE, TU_MILITARY_SEGMENT, TU_SEARCH_STATUS, 'M01');

		CreditReportMilitary.changeContactMilitaryFlag(c.Id, crSegmentsList, 'TU');

		Contact contact = [Select Id,
		                   Military__c
		                   from Contact
		                   where Id = : c.Id];

		System.assertEquals(true, contact.Military__c);
	}

	@isTest static void changes_military_flag_for_ex() {
		Contact c = LibraryTest.createContactTH();
		c.Military__c = false;
		update c;

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(EX_MILITARY_SEGMENT, EX_MESSAGE_CODE, EX_CODE, EX_MILITARY_SEGMENT, EX_MESSAGE_TEXT, '1203');

		CreditReportMilitary.changeContactMilitaryFlag(c.Id, crSegmentsList, 'EX');

		Contact contact = [Select Id,
		                   Military__c
		                   from Contact
		                   where Id = : c.Id];

		System.assertEquals(true, contact.Military__c);
	}

	@isTest static void does_not_change_military_flag() {
		Contact c = LibraryTest.createContactTH();
		c.Military__c = false;
		update c;

		List<ProxyApiCreditReportSegmentEntity> crSegmentsList =
		    LibraryTest.fakeListSegmentEntity(TU_MILITARY_SEGMENT, TU_SERVICE_CODE, TU_CODE, TU_MILITARY_SEGMENT, TU_SEARCH_STATUS, 'M00');

		CreditReportMilitary.changeContactMilitaryFlag(c.Id, crSegmentsList, 'TU');

		Contact contact = [Select Id,
		                   Military__c
		                   from Contact
		                   where Id = : c.Id];

		System.assertEquals(false, contact.Military__c);
	}

}