@isTest
private class QueueSortBatchTest {

	@isTest static void update_contact() 
	{
		MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();
		maskSettings.MaskAllData__c = false;
		insert maskSettings;
		
		Contact thisContact = LibraryTest.createContactTH();

		List<Contact> contactList = new List<Contact>();
		contactList.add(thisContact);

		Database.BatchableContext bc;

		Test.startTest();

		QueueSortBatch sortBatch = new QueueSortBatch();
		sortBatch.execute(bc, contactList);

		Test.stopTest();
	}

	@isTest static void shuffle_string() 
	{
		QueueSortBatch sortBatch = new QueueSortBatch();
		String testString = 'testString';
		String result = sortBatch.shuffle(testString);
		System.assertNotEquals(testString, result);
	}
}