@isTest
global class TestLoanPaymentTxnPayPointGen {
    public static testMethod void testPaypointFileGen(){
    
        //Load csv file
        Test.loadData(filegen__File_Metadata__c.sObjectType, 'LendingFilegen');
        
        //Setup seed data
        TestHelperForManaged.createSeedDataForTesting();
        TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupORGParametersActive();
        TestHelperForManaged.createLoanPurpose();
        TestHelperForManaged.setupMetadataSegments();
        Id tId = TestHelperForManaged.getRootBranchRecordTypeID();
        Contact co = null;
        loan.TestHelper.systemDate = Date.newInstance(2014, 3, 3); // Mar 3 - Monday
        try {
            co = TestHelperForManaged.createContact('Test', 'Wedding', '12345', 'GA');
        } catch (Exception e){
        }
      
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
       
        //    Currency__c curr = TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Bank__c bank = loan.TestHelper.createBank();
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        dummyOffice.loan__Branch_s_Bank__c = bank.id;
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose =  TestHelperForManaged.createLoanPurpose('Pru1', 'Pri2', 'Pri3');
        
        ffaUtilities_TEST.createFFACompany('LendingTest', true, 'USD');
        
        //Account a1 = loan.TestHelper.createInvestor('Bull', 1000);
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        loan__Bank_Account__c ba = TestHelperForManaged.createBankAccount(b1, 'Checking');
                                             
        upsert ba;
 
        
        
        
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Collections Trust Account');
        loan__Bank_Account__c dummyBank2 = TestHelperForManaged.createBankAccount(b1,'Bofi Collections Account');
        //Create a dummy Loan Account
        loan__Loan_Account__c dummylaMonthly = LibraryTest.createContractTH();
        dummylaMonthly.loan__ACH_On__c = true;
        //dummylaMonthly.Borrower_ACH__c = ba.Id;
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = Date.today().addMonths(1);
        dummylaMonthly.loan__ACH_Start_Date__c = Date.today() ;
        dummylaMonthly.loan__ACH_End_Date__c = Date.today().addMonths(4);
        dummylaMonthly.loan__ACH_Bank__c = dummyOffice.loan__Branch_s_Bank__c;
        dummylaMonthly.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        dummylaMonthly.loan__ACH_Account_Type__c = 'Checking';
        dummylaMonthly.loan__ACH_Account_Number__c = '123456';
        dummylaMonthly.loan__ACH_Routing_Number__c = '123456123';
        dummylaMonthly.loan__ACH_Relationship_Type__c = 'PRIMARY';
        dummylaMonthly.loan__Contact__c = co.Id;
        dummylaMonthly.loan__Frequency_of_Loan_Payment__c = loan.LoanConstants.LOAN_PAYMENT_FREQ_MONTHLY;
          update dummylaMonthly;
        loan__Loan_Account__c dummylaWeekly = LibraryTest.createContractTH();
        dummylaWeekly.loan__ACH_On__c = true;
        //dummylaWeekly.Borrower_ACH__c = ba.Id;
        dummylaWeekly.loan__ACH_Debit_Amount__c = 100;
        dummylaWeekly.loan__ACH_Next_Debit_Date__c = Date.today().addDays(1);
        dummylaWeekly.loan__ACH_Start_Date__c = Date.today() ;
        dummylaWeekly.loan__ACH_End_Date__c = Date.today().addMonths(4);
        dummylaWeekly.loan__ACH_Bank__c = dummyOffice.loan__Branch_s_Bank__c;
        dummylaWeekly.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        dummylaWeekly.loan__ACH_Account_Type__c = 'Checking';
        dummylaWeekly.loan__ACH_Account_Number__c = '123456';
        dummylaWeekly.loan__ACH_Routing_Number__c = '123456123';
        dummylaWeekly.loan__ACH_Relationship_Type__c = 'PRIMARY';
        dummylaWeekly.loan__Contact__c = co.Id;
        dummylaWeekly.loan__Frequency_of_Loan_Payment__c = loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY;

        update dummylaWeekly;
        
        
        dummylaWeekly.Asset_Sale_Date__c = Date.today();
        update dummylaWeekly;
        dummylaWeekly.Active_Charge_Off__c = true;
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
         test.startTest();
        loan.LoanPaymentTxnSweepToACHJob j = new loan.LoanPaymentTxnSweepToACHJob(false);
        Database.executeBatch(j);
         test.stopTest();
        
        loan__Loan_Account__c laMonthly = [SELECT Id
                                        , loan__ACH_Next_Debit_Date__c
                                        , loan__ACH_Debit_Day__c
                                        , loan__ACH_Frequency__c
                                        , loan__First_Installment_Date__c 
                                        FROM loan__Loan_Account__c 
                                        where loan__Frequency_of_Loan_Payment__c =: loan.LoanConstants.LOAN_PAYMENT_FREQ_MONTHLY];
       
        loan__Loan_Account__c laWeekly = [SELECT Id
                                        , loan__Disbursal_Date__c
                                        , loan__ACH_Next_Debit_Date__c
                                        , loan__ACH_Frequency__c 
                                        FROM loan__Loan_Account__c 
                                        where loan__Frequency_of_Loan_Payment__c =: loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY];
        
        List<loan__Loan_Payment_Transaction__c> p = [SELECT Id FROM loan__Loan_Payment_Transaction__c];
        //   System.assert(p.size() > 0,p.size() );
        
        
        
    }                         
}