public class DocusignRequestHelper {
    public static String GenerateEnvelopeRequest(List<Contact> owners, String templateId) {
        String emailSubject = Docusign_Integration_Settings__c.getInstance().Email_Subject__c;
        String emailBlurb = Docusign_Integration_Settings__c.getInstance().Email_Blurb__c;
        String webhookUrl = Docusign_Integration_Settings__c.getInstance().Webhook_URL__c;
    
        String retVal = '{'+
        '  \"emailSubject\": \"'+ emailSubject +'\",'+        
        '  \"status\": \"sent\",'+
        '  \"templateId\": \"'+ templateId +'\",'+
        '  \"EventNotification\": {'+
        '    \"url\": \"'+ webhookUrl +'\",'+
        '    \"loggingEnabled\": \"true\",'+
        '    \"requireAcknowledgment\": \"true\",'+
        '    \"useSoapInterface\": \"false\",'+
        '    \"includeCertificateWithSoap\": \"false\",'+
        '    \"signMessageWithX509Cert\": \"false\",'+
        '    \"includeDocuments\": \"true\",'+
        '    \"includeEnvelopeVoidReason\": \"true\",'+
        '    \"includeTimeZone\": \"true\",'+
        '    \"includeSenderAccountAsCustomField\": \"true\",'+
        '    \"includeDocumentFields\": \"true\",'+
        '    \"includeCertificateOfCompletion\": \"true\",'+
        '    \"envelopeEvents\": ['+
        '      {'+
        '        \"envelopeEventStatusCode\": \"sent\"'+
        '      },'+
        '      {'+
        '        \"envelopeEventStatusCode\": \"delivered\"'+
        '      },'+
        '      {'+
        '        \"envelopeEventStatusCode\": \"completed\"'+
        '      },'+
        '      {'+
        '        \"envelopeEventStatusCode\": \"declined\"'+
        '      },'+
        '      {'+
        '        \"envelopeEventStatusCode\": \"voided\"'+
        '      }'+
        '    ],'+
        '    \"recipientEvents\": ['+
        '      {'+
        '        \"recipientEventStatusCode\": \"Sent\"'+
        '      },'+
        '      {'+
        '        \"recipientEventStatusCode\": \"Delivered\"'+
        '      },'+
        '      {'+
        '        \"recipientEventStatusCode\": \"Completed\"'+
        '      },'+
        '      {'+
        '        \"recipientEventStatusCode\": \"Declined\"'+
        '      },'+
        '      {'+
        '        \"recipientEventStatusCode\": \"AuthenticationFailed\"'+
        '      },'+
        '      {'+
        '        \"recipientEventStatusCode\": \"AutoResponded\"'+
        '      }'+
        '    ]'+
        '  },'+
        '  \"templateRoles\": ['+ generateTemplateRoles(owners) +
        '  ]'+
        '}';
        
        return retVal;
    }
    
    public static String generateTemplateRoles(List<Contact> owners) {
        String retVal = '';
        
        Integer count = 1;
        
        for (Contact c : owners) {
            retVal += '{'+
            '                \"email\": \"'+ c.Email +'\",'+
            '                \"name\": \"'+ c.FirstName + ' ' + c.LastName + '\",'+
            '                \"roleName\": \"Owner'+count+'\",'+
            '      \"tabs\": {'+
            '        \"textTabs\": ['+ generateTabs(c) +
            '        ]'+
            '      }'+
            '},';
            
            count++;
        }
        
        retVal = retVal .Substring(0,retVal.length()-1);
        
        return retVal;
    }
    
    public static String generateTabs(Contact c) {
        String bankType = c.Account.Bank_Account_Type__c == null ? '' : c.Account.Bank_Account_Type__c;
        String bankAccNum = c.Account.financial_institution_account_number__c == null ? '' : c.Account.financial_institution_account_number__c;
    
        String retVal = 
        '          {'+
        '            \"tabLabel\": \"\\\\*AccountName\",'+
        '            \"value\": \"'+ c.Account.dba_doing_business_as__c +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*AccountAddress\",'+
        '            \"value\": \"'+ c.Account.BillingStreet +', '+ c.Account.BillingCity +', '+ c.Account.BillingState +', '+ c.Account.BillingPostalCode +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*AccountStreet\",'+
        '            \"value\": \"'+ c.Account.BillingStreet +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*AccountCity\",'+
        '            \"value\": \"'+ c.Account.BillingCity +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*AccountState\",'+
        '            \"value\": \"'+ c.Account.BillingState +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*AccountZip\",'+
        '            \"value\": \"'+ c.Account.BillingPostalCode +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*BankName\",'+
        '            \"value\": \"'+ c.Account.financial_institution_name__c +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*BankAccountHolder\",'+
        '            \"value\": \"'+ c.Account.name_on_account__c +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*ABANumber\",'+
        '            \"value\": \"'+ c.Account.transit_aba_number_9digits__c +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*BankAccountNumber\",'+
        '            \"value\": \"'+ bankAccNum +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*BankAccountType\",'+
        '            \"value\": \"'+ bankType +'\"'+
        '          },'+
        '          {'+
        '            \"tabLabel\": \"\\\\*SignerTitle\",'+
        '            \"value\": \"'+ c.Title +'\"'+
        '          }';
        
        return retVal;
    }
}