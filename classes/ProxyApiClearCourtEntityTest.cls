@isTest private class ProxyApiClearCourtEntityTest {
	@isTest static void validates_entity() {

        Contact c = LibraryTest.createContactTH();

        ProxyApiClearCourtEntity pacce = new ProxyApiClearCourtEntity();
        pacce.id = 1182592983000L;
        pacce.contactId = c.id;
        pacce.errorMessage = 'Error Message';
        pacce.errorDetails = 'Error Details';
        pacce.createdDate = 1182592983000L;

        System.assertEquals(1182592983000L, pacce.id);
        System.assertEquals(c.id, pacce.contactId);
        System.assertEquals('Error Message', pacce.errorMessage);
        System.assertEquals('Error Details', pacce.errorDetails);
        System.assertEquals(1182592983000L, pacce.createdDate);

        ClearCourtResults ccr = new ClearCourtResults();
        pacce.clearCourtResults = ccr;

        System.assertEquals(true, pacce.errorResponse);
        System.assertEquals(DateTime.newInstance(pacce.createdDate), pacce.creationDate);

        pacce.createdDate = null;
        System.assertEquals(null, pacce.creationDate);

        pacce.errorMessage = null;
        System.assertEquals(false, pacce.errorResponse);
    }
	
}