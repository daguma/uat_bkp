@isTest
private class Test_AutoConvertLeads {

    @isTest static void test_LeadAssign(){
        Partner_Product_Bank_Account_Setting__c t = new Partner_Product_Bank_Account_Setting__c(Name='OnboardingBankAccountSetup', Product_ID__c='a250B00000CZr3BQAT', Lending_Product_ID__c='aJq0B0000008OI5SAM', Account_Type__c='Checking');
        insert t;
        Account acc = new Account(Name='testAutoCreateLead', Cobranding_Name__c = 'testAutoCreateLead_AL', Type='Merchant', Active_Partner__c = True, Transit_ABA_Number_9digits__c='512345674', Financial_Institution_Account_Number__c = '54324545232');
        insert acc;
        AutoConvertLeads.accountIds.add(acc.id);
        
        TestHelper.createLead();
        List<Id> leadIds = new List<Id>();
        leadIds.add([select id from lead limit 1].id);
        AutoConvertLeads.LeadAssign(leadIds);
    }
}