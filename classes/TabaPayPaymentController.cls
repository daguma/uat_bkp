public class TabaPayPaymentController {
 
  public String ContractId {get; set;}
  public String ContactId {get; set;}
  public String LAINumber {get; set;} //CPS-91

  public List<ProxyApiTabaPay.AccountsResponse> tabaPayAccountsList {get; set;}

  public Boolean existTabaPayAccount {
    get {
      return (this.tabaPayAccountsList != null && this.tabaPayAccountsList.size() > 0) ? true : false;
    }
  }

  public Decimal PaymentAmount {get; set;}
  public Decimal PaymentFee {get; set;}
  public String PaymentType {get; set;}

  //public Decimal FeePtcTabaPay {get; set;}
  public string selectedCardType {get; set;}
  public string cardForScheduledPayment {get; set;}
  public string debitCardNumber {get; set;}
  public String securityCode {get; set;}
  public string selectedExpirationMonth {get; set;}
  public string selectedExpirationYear {get; set;}
  public String tabaPayAccountId {get; set;}
  public Boolean setDefaultCard {get; set;}

  public Boolean disabledPaymentButton {get; set;}

  public Contact contact;
  public loan__loan_account__c loanAccount;
  public Boolean tabapayAccountPickListDisabled {get;set;}

  public TabaPayPaymentController(Apexpages.StandardController controller) {
    //this.FeePtcTabaPay = decimal.valueOf(Label.FeeTabaPay);
    this.tabaPayAccountsList = new List<ProxyApiTabaPay.AccountsResponse>();

    this.ContractId = '';
    this.LAINumber = ''; //CPS-91
    this.setDefaultCard = false;

    loan__loan_account__c contract = (loan__loan_account__c)controller.getRecord();
    if (contract != null && contract.Id <> null) {
      this.ContractId = contract.Id;
      
      loanAccount = [SELECT loan__Contact__c,Name, loan__ACH_On__c, Is_Debit_Card_On__c FROM loan__loan_account__c WHERE id = : this.ContractId Limit 1];
      this.ContactId = loanAccount.loan__Contact__c;
      this.LAINumber = loanAccount.Name; //CPS-91
      this.contact = [SELECT Id,
                      Firstname, 
                      LastName,
                      MailingCity,
                      MailingPostalCode,
                      MailingStreet,
                      MailingState,
                      Phone,
                            Phone_Clean__c 
                      FROM Contact
                      WHERE id = : this.ContactId limit 1];
      checkValidations(ContractId); // 4219 changes
    }
  }

  public void initialize() { 
    List<SelectOption> optionsForScheduling = new List<SelectOption>();
    ProxyApiTabaPay.AccountsInfo response2 = ProxyApiTabaPay.retrieveAccounts('contactId=' + this.ContactId + '&contractId=' + this.ContractId);

    this.tabaPayAccountsList = response2.responseList;
    if(!this.loanAccount.Is_Debit_Card_On__c)
      for(ProxyApiTabaPay.AccountsResponse account : this.tabaPayAccountsList)
        account.selectedForAutoPay = false;
      
    
    if (response2.result != 'OK') {
      ApexPages.getMessages().clear();
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, response2.result));
    }
  }

  public void AddCard() {
    ApexPages.getMessages().clear();
    ProxyApiTabaPay.AddAccountResponse response = ProxyApiTabaPay.addAccount(createAccountParams());

    if (response.SC == '200') {

      this.debitCardNumber = '';
      this.securityCode = '';
      this.selectedCardType = 'VISA';

      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, 'TabaPay Account Created Successfully!'));

      ProxyApiTabaPay.AccountsInfo response2 = ProxyApiTabaPay.retrieveAccounts('contactId=' + this.ContactId + '&contractId=' + this.ContractId);
      this.tabaPayAccountsList = response2.responseList;

      if (response2.result != 'OK') {
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, response2.result));
      }
    } else {
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, (String.isNotEmpty(response.EM) ? response.EM + '<br/>' : '') + (String.isNotEmpty(response.message) ? response.message : '') ));
    }
  }

  public void goDelete() {
    ProxyApiTabaPay.DeleteAccountResponse response = ProxyApiTabaPay.deleteAccount('contactId=' + this.ContactId + '&contractId=' + this.ContractId + '&accountId=' + this.tabaPayAccountId);
    if (response.SC == '200') {

      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, 'TabaPay Account Deleted Successfully!'));

      ProxyApiTabaPay.AccountsInfo response2 = ProxyApiTabaPay.retrieveAccounts('contactId=' + this.ContactId + '&contractId=' + this.ContractId);
      this.tabaPayAccountsList = response2.responseList;

      if (response2.result != 'OK') {
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, response2.result));
      }
    } else {
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, 'Error code: ' + (String.isNotEmpty(response.EC) ? response.EC + '<br/>' : '') + (String.isNotEmpty(response.message) ? response.message : '') ));
    }
  }

  public PageReference goBack() {
    return new PageReference(URL.getSalesforceBaseUrl().toExternalForm().replace('c.', 'loan.') + '/apex/tabbedLoanAccount?id=' + this.ContractId);
  }

  public PageReference goProceedPayment() {
    ApexPages.getMessages().clear();

    integer index = -1;
    for (integer i = 0; i < this.tabaPayAccountsList.size(); i++ ) {
      if (this.tabaPayAccountsList.get(i).accountId == this.tabaPayAccountId) {
        index = i;
        break;
      }
    }
    if (index == -1 ) {
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, 'Declined getting information from the selected TabaPay Account.'));
      return null;
    }

    ProxyApiTabaPay.PaymentResponse response = ProxyApiTabaPay.makePayment(paymentParams(index));
    if (response.SC == '200') {
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, '<b>Payment Processed!</b><br/>Payment Status: ' + response.status + '<br/>Approved Code: ' + response.approvalCode + (String.isNotEmpty(response.EM) ? ' <br/>EM:' + response.EM + ' - ' : ''))); //OF-392
    } else {
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, (String.isNotEmpty(response.EM) ? response.EM + '<br/>' : '') + (String.isNotEmpty(response.message) ? response.message : '')));
    }
        insert CustomerPortalWSDLUtil.getLogging('Tabapay', this.ContractId, 'goProceedPayment' , (String.isNotEmpty(response.EM) ? ' EM:' + response.EM + ' - ' : '') + (String.isNotEmpty(response.message) ? ' message: ' + response.message + ' - ' : '') + (String.isNotEmpty(response.status) ? ' status: ' + response.status + ' - ' : '') + (String.isNotEmpty(response.approvalCode) ? ' approvalCode: ' + response.approvalCode + ' - ' : '')) ;
    return null;
  }

  public PageReference schedulePayment() {
    ApexPages.getMessages().clear();
    String scheduledPaymentAmount = apexpages.currentpage().getparameters().get('scheduledPaymentAmount');
    String scheduledTransactionDate = apexpages.currentpage().getparameters().get('scheduledTransactionDate');

    loan__Other_Transaction__c newTransaction = new loan__Other_Transaction__c();
    try{

      if(!validateExistingScheduledPayment(this.ContractId)){
        newTransaction.loan__Loan_Account__c = this.ContractId;
        newTransaction.loan__Txn_Amt__c = decimal.valueOf(scheduledPaymentAmount);
        newTransaction.loan__Txn_Date__c = Date.parse(scheduledTransactionDate.replace('-','/'));
        newTransaction.loan__Transaction_Type__c = 'One Time Debit';
        newTransaction.One_Time_Debit_Status__c = 'Pending';
        newTransaction.Debit_Card_Account__c = cardForScheduledPayment;
        insert newTransaction;
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, 'The payment has been scheduled'));
      }else{
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, 'Currently, there is an active scheduled payment, you cannot create another one.'));
      }
    }catch(Exception e){
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, 'An error has occurred ' +e.getMessage()));
    }


    return null;
  }

  public Boolean validateExistingScheduledPayment(Id  contractId) {
    Boolean scheduledPaymentExistence = false;

    for(loan__Other_Transaction__c  otherTransaction: [SELECT Id 
                                                       FROM loan__Other_Transaction__c 
                                                       WHERE loan__Loan_Account__c =: contractId
                                                       AND loan__Transaction_Type__c = 'One Time Debit'
                                                       AND loan__Txn_Date__c >: System.Today()]){
      scheduledPaymentExistence = true;                                                     
    }
    return scheduledPaymentExistence;
  }
  /********4219 starts changes to stop Debit Card payment on conditional basis************/
  private void checkValidations(string contractId) {
    this.disabledPaymentButton = false;
    String message = '';
    string TransResp = CustomerPortalWSDL.checkToStopDebitCardPayment(contractId);
    if (!string.isEmpty(TransResp)) {
      Map<string, string> PaymentResponse = (Map<String, String>) JSON.deserialize(TransResp, Map<String, String>.class);
      System.debug('TransResp ' + TransResp);
      System.debug('PaymentResponse ' + PaymentResponse);

      if (PaymentResponse.ContainsKey('msg') && PaymentResponse.ContainsKey('validationOTACHPayment')) {
        message += PaymentResponse.get('msg') + ', therefore you will not be able to make debit card payment. <br/>';
      }
      if (PaymentResponse.ContainsKey('validationAccrualDate')) {
        message += Label.PayLeapLastAccrualDateValidation + '<br/>';
      }
      if (PaymentResponse.ContainsKey('isEndOfDayProcessRunning')) {
        message += Label.PayLeapEndOfDayProcessValidation + '<br/>';
      }

      if (String.isNotEmpty(message)) {
        this.disabledPaymentButton = true;
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, message));
      }
    }
  }
  /******** 4219 ends ************/

  public List<SelectOption> getcardType() {
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('VISA', 'VISA'));
    options.add(new SelectOption('MasterCard ', 'MasterCard'));
    options.add(new SelectOption('Discover', 'Discover'));
    options.add(new SelectOption('STAR', 'STAR'));
    options.add(new SelectOption('Pulse', 'Pulse'));
    options.add(new SelectOption('NYCE', 'NYCE'));
    options.add(new SelectOption('CU24', 'CU24'));
    options.add(new SelectOption('Accel', 'Accel'));

    return options;
  }

  public List<SelectOption> getAccountsForScheduling() {
    List<SelectOption> optionsForScheduling = new List<SelectOption>();
    for(ProxyApiTabaPay.AccountsResponse account : this.tabaPayAccountsList){
      optionsForScheduling.add(new SelectOption(account.accountId, account.cardNo));
    }

    if(!(optionsForScheduling.size() > 0)){
      tabapayAccountPickListDisabled = true;
      optionsForScheduling.add(new SelectOption('XXXXXXXXXXXXXXXX', 'XXXXXXXXXXXXXXXX'));
    }else{
      tabapayAccountPickListDisabled = false;
    }
    return optionsForScheduling;
  }


  public List<SelectOption> getlistMonth() {
    List<SelectOption> options = new List<SelectOption>();
    for (integer index = 1; index < 10; index++)
      options.add(new SelectOption('0' + String.valueOf(index), '0' + String.valueOf(index)));
    for (integer index = 10; index <= 12; index++)
      options.add(new SelectOption(String.valueOf(index), String.valueOf(index)));
    return options;
  }

  public List<SelectOption> getlistYear() {
    List<SelectOption> options = new List<SelectOption>();
    integer year = System.Today().year();
    for (integer index = 0; index <= 70; index++)
      options.add(new SelectOption(String.valueOf(year + index), String.valueOf(year + index)));
    return options;
  }

  private String createAccountParams() {
    String result = '';
    result += 'contactId=' + this.ContactId;
    result += '&contractId=' + this.ContractId;
    result += '&cardNo=' + this.debitCardNumber;
    result += '&expDate=' + this.selectedExpirationYear + this.selectedExpirationMonth;
    result += '&secCode=' + this.securityCode;
    result += '&firstName=' + EncodingUtil.urlEncode(this.contact.Firstname, 'UTF-8');
    result += '&lastName=' + EncodingUtil.urlEncode(this.contact.LastName, 'UTF-8');
    result += '&line1Address=' + EncodingUtil.urlEncode(String.isEmpty(this.contact.MailingStreet) ? '' : this.contact.MailingStreet.replace('\\', '') , 'UTF-8') + '&line2Address=';
    result += '&city=' + EncodingUtil.urlEncode(this.contact.MailingCity, 'UTF-8');
    result += '&state=' + EncodingUtil.urlEncode(this.contact.MailingState, 'UTF-8');
    result += '&zipcode=' + EncodingUtil.urlEncode(this.contact.MailingPostalCode, 'UTF-8'); 
    result += '&phoneNumber=' + EncodingUtil.urlEncode(this.contact.Phone_Clean__c , 'UTF-8'      );
    result += '&accountId=';

    return result;
  }

  private String paymentParams(integer index) {

    String result = '';
    result += 'contactId=' + this.ContactId;
    result += '&contractId=' + this.ContractId;
    result += '&cardNo=' + this.tabaPayAccountsList.get(index).cardNo;
    result += '&expDate=' + this.tabaPayAccountsList.get(index).expDate;
    result += '&amount=' + String.valueof(this.PaymentAmount.setScale(2));
    result += '&isAch=' + String.valueof(loanAccount.loan__ACH_On__c);
    result += '&isAdditionalPayment=' + (PaymentType == 'AdditionalPayment' ? 'True' : 'False');
    result += '&cardFeeAmt=' + String.valueof(0);
    result += '&LAINumber=' + this.LAINumber; //CPS-91
        
    return result;
  }

  public void changeAutoPaymentCard(){
    ProxyApiTabaPay.ChangeAutoPayment response = ProxyApiTabaPay.changeAutoPayment('contactId=' + this.ContactId + '&contractId=' + this.ContractId + '&accountId=' + this.tabaPayAccountId + '&setDefaultCard=' +this.setDefaultCard);
    if (response.SC == '200') {
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, 'The auto payment has been changed!'));
      ProxyApiTabaPay.AccountsInfo response2 = ProxyApiTabaPay.retrieveAccounts('contactId=' + this.ContactId + '&contractId=' + this.ContractId);
      this.tabaPayAccountsList = response2.responseList;
      if(this.setDefaultCard)
        update new loan__Loan_Account__c(Id = this.ContractId, Debit_Card_Account_Id__c = this.tabaPayAccountId, Is_Debit_Card_On__c = true, loan__ACH_On__c = false, PaymentMethod__c = 'Debit Card');
      else 
        update new loan__Loan_Account__c(Id = this.ContractId, Debit_Card_Account_Id__c = '', Is_Debit_Card_On__c = false, PaymentMethod__c = 'Certified Check');
      if (response2.result != 'OK') {
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, response2.result));
      }
    } else {
      ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Error, 'Error code: ' + (String.isNotEmpty(response.EC) ? response.EC + '<br/>' : '') + (String.isNotEmpty(response.message) ? response.message : '') ));
    }
  }
}