public with sharing class ApplicationRulesCtrl {

    @AuraEnabled
    public static List<ApplicationRule> getCRApplicationRules(Id oppId) {

        List<ApplicationRule> appRulesList = new List<ApplicationRule>();

        try {

            //Get CreditReport for the specific Opportunity
            //ProxyApiCreditReportEntity pacre = CreditReportALPHA.getByEntityId(oppId);
            ProxyApiCreditReportEntity pacre = CreditReport.getByEntityId(oppId);

            if (pacre != null
                    && pacre.brms != null) {
                appRulesList = getApplicationRules(pacre.brms.rulesList);
            }

        } catch (Exception e) {
            System.debug('ApplicationRulesCtrl.getApplicationRules: ' + e.getMessage() + e.getStackTraceString());
            return appRulesList;
        }

        return appRulesList;
    }

    public static List<ApplicationRule> getApplicationRules(List<ProxyApiCreditReportRuleEntity> pacrreList){

        List<ApplicationRule> appRulesList = new List<ApplicationRule>();
        appRulesList.add( new ApplicationRule('-') ); //If the CreditReport has no application rules

        if(pacrreList == null)
            return appRulesList;

        ApplicationRule displayWrapper;
        Boolean appRulesListWasCleared = false;

        for (ProxyApiCreditReportRuleEntity softPullRule : pacrreList) {

            //display only those rules which has deIsdisplayed as yes
            If (String.isNotBlank(softPullRule.deIsDisplayed)
                    && softPullRule.deIsDisplayed.equalsIgnoreCase('Yes')) {

                displayWrapper = new ApplicationRule();

                If (String.isNotBlank(softPullRule.deRuleCategory))
                    displayWrapper.ruleCategory = softPullRule.deRuleCategory;

                if (String.isNotBlank(softPullRule.deRuleName))
                    displayWrapper.ruleName = softPullRule.deRuleName;

                if (String.isNotBlank(softPullRule.deRuleStatus)
                        && softPullRule.deRuleStatus.equalsIgnoreCase('Pass')) {
                    displayWrapper.softPass = 'true';
                    displayWrapper.isHighlightRule = 'false';
                } else {
                    displayWrapper.softPass = 'false';
                    if (String.isNotBlank(softPullRule.deRuleStatus)
                            && softPullRule.deRuleStatus.equalsIgnoreCase('Fail'))
                        displayWrapper.isHighlightRule = 'true';
                }

                displayWrapper.ruleValue = softPullRule.deRuleValue;

                displayWrapper.queueAssignment = softPullRule.deQueueAssignment;

                If (String.isNotBlank(softPullRule.deHumanReadableDescription))
                    displayWrapper.recommendedActions = softPullRule.deHumanReadableDescription;

                If (String.isNotBlank(softPullRule.deActionInstructions))
                    displayWrapper.recommendedActions = softPullRule.deActionInstructions;

                if(!appRulesListWasCleared){
                    appRulesList.clear();
                    appRulesListWasCleared = true;
                }

                appRulesList.add(displayWrapper);
            }
        }

        return appRulesList;
    }

    public class ApplicationRule {

        @AuraEnabled public string ruleCategory;
        @AuraEnabled public string ruleName;
        @AuraEnabled public string softPass;
        @AuraEnabled public string recommendedActions;
        @AuraEnabled public string ruleValue;
        @AuraEnabled public string queueAssignment;
        @AuraEnabled public string isHighlightRule;

        public ApplicationRule() {}

        public ApplicationRule(String ruleCategory) {
            this.ruleCategory = ruleCategory;
        }

    }

}