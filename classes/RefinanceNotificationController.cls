public Class RefinanceNotificationController{
    
    public Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
    
    @future
    public static void sendRBPNotificationEmail(string oppId){
        List<Opportunity> oppList = new List<Opportunity>();
        List<String> sendTo;
        String tempid;
        String optytype = 'Refinance';
        EmailTemplate emailTemplates = [select Name, body, Id,HtmlValue,subject from EmailTemplate where DeveloperName = 'Risk_Based_Pricing_Notice_Finwise_Refinances' LIMIT 1]; 
        
        tempid = emailTemplates.id;        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();        
        Opportunity oppToCheck = new Opportunity();
        List<Note> notesEmails = new List<Note>();
        ProxyApiCreditReportEntity creditReportEntity;
        string emailId = '';
        String HtmlValue = '';
        String plainBody = '';
        boolean thirdPartyCheck;
        Contact theContact = null;
        Map<Id, Email_Age_Details__c> mAges = new Map<Id, Email_Age_Details__c>();
            for (Email_Age_Details__c eAge : [select Id, Contact__c, Actual_Credit_Decision__c, Email_Age_Status__c, is_Email_Age_Timed_Out__c, errorCode__c, status__c from Email_Age_Details__c 
                                               where Contact__c in (select Contact__c from Opportunity WHERE id =: oppId and Is_Finwise__c = true and Type = 'Refinance' and Partner_Account__r.Name = 'Refinance')  order by Contact__c, CreatedDate desc]) {
                                                   mAges.put(eAge.Contact__c, eAge);
                                               }
        
        if(String.isNotEmpty(oppId)){
            for(Opportunity opp : [select id,name,FICO__c,Type ,Contact_Email__c,Reason_of_Opportunity_Status__c,RBP_Sent_Date__c,CreatedDate,LexisNexisDeclined__c,KeyFactors__c,CreditPull_Date__c,Contact__c,Contact__r.name,Contact__r.MailingStreet,Contact__r.MailingCity,
                                    Contact__r.MailingState,Contact__r.MailingPostalCode,Is_Finwise__c,Partner_Account__c,Clarity_Status__c,Partner_Account__r.Name 
                                    FROM Opportunity WHERE id =: oppId and Is_Finwise__c = true and Type =: optytype and status__c <> 'Refinance Unqualified' and Partner_Account__r.Name =:optytype]){  //OF356
                
                if(opp.Partner_Account__c != null){
                    System.debug('=====opp====='+opp);
                    try {
                            creditReportEntity = CreditReport.getByEntityId(opp.Id, true);
                        } catch (Exception ex) {
                            creditReportEntity = null;
                            System.debug('Exception = ' + ex.getStackTraceString());
                        }
                     theContact = opp.Contact__r;
                     oppToCheck = opp;
                     emailId = opp.Contact_Email__c != Null ? opp.Contact_Email__c:'';
                     thirdPartyCheck =  isThirdPartyQualify(opp,creditReportEntity, mAges);
                     
                     //HtmlValue = emailTemplates.HtmlValue;
                     //HtmlValue = HtmlValue.replace('{!LetterDate}', (opp.CreditPull_Date__c != null) ? Datetime.newInstance(opp.CreditPull_Date__c, DateTime.now().Time() ).format('MMMM d,  yyyy') : Datetime.now().format('MMMM d,  yyyy'));
                     HtmlValue = replaceValuesFromData(emailTemplates.HtmlValue, theContact , creditReportEntity, opp, thirdPartyCheck);
                     plainBody = replaceValuesFromData(emailTemplates.Body, theContact , creditReportEntity, opp, thirdPartyCheck);
                        
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                        sendTo = new List<String>();
                        sendTo.add(emailId);
                        email.setWhatId(opp.id);
                        email.setTargetObjectId(opp.Contact__c);
                        email.setToAddresses(sendTo);                    
                        //email.setTemplateId(tempid);
                        email.setHtmlBody(HtmlValue);
                        email.setPlainTextBody(plainBody);
                        email.setSubject('Credit Score Disclosure');
                        email.saveAsActivity = false;
                        email.setTreatTargetObjectAsRecipient(False);
                        system.debug('**email****'+email);
                    mails.add(email);
                    
                    notesEmails.add(Batch_AANNotificationOnOpportunity.createNote( 'To: ' + opp.Contact_Email__c + '\n\n' + plainBody, emailTemplates.Subject, opp.id));
                     
               }
            }            
            if(notesEmails.size() > 0){ opportunity o = new opportunity(id=oppId,RBP_Sent_Date__c =date.today()); insert notesEmails; update o;} 
        }
        
        system.debug('===fico=='+oppToCheck.FICO__c);
        if(!mails.isEmpty() && oppToCheck != null && oppToCheck.FICO__c != null && oppToCheck.Contact_Email__c != null && String.isNotEmpty(oppToCheck.Contact_Email__c) && String.isNotEmpty(oppToCheck.FICO__c)){
               try{
                     Messaging.sendEmail(mails);                    
                 }
                 Catch(Exception e){
                     System.debug('--------------Exception in Sending emails--------------------'+e.getMessage());
                 }
             }
    }
    //Third Party Access
    public static boolean isThirdPartyQualify(Opportunity theApp,ProxyApiCreditReportEntity creditReportEntity, Map<Id, Email_Age_Details__c> mAges){
        boolean dontRunBRMS = CreditReport.validateBrmsFlag(theApp.id), thirdPartyCheck = false;
        Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
        if(String.isNotEmpty(theApp.Reason_of_Opportunity_Status__c) && theApp.Reason_of_Opportunity_Status__c == 'Cannot Verify Information Provided' && creditReportEntity <> null) {
            thirdPartyCheck = !theApp.Clarity_Status__c;            
            if (!thirdPartyCheck && mAges.containsKey(theApp.Contact__c)) {
                Email_Age_Details__c theAge = mAges.get(theApp.Contact__c);
                if(dontRunBRMS)
                    thirdPartyCheck =  emailAgeSettings.Call_Email_age__c && creditReportEntity.decision && creditReportEntity.automatedDecision.equalsIgnoreCase('No') && !theAge.Email_Age_Status__c ; //if application declined due to email age
                else 
                    thirdPartyCheck = creditReportEntity.brms <> null && creditReportEntity.brms.autoDecision.equalsIgnoreCase('No') && !theAge.Email_Age_Status__c ; //if application declined due to email age
            }
        }
        if (!thirdPartyCheck ) thirdPartyCheck = theApp.LexisNexisDeclined__c ;
        if (!thirdPartyCheck && creditReportEntity != null) thirdPartyCheck = !Decisioning.isClarityClearRecentHistoryPassed(creditReportEntity.clearRecentHistory);

        return thirdPartyCheck;
    }
    
    public static string replaceValuesFromData(String theValue, Contact theContact, ProxyApiCreditReportEntity creditReportEntity, Opportunity theOpp,  boolean bThirdParty) {
        boolean bHasCR = creditReportEntity != null, dontRunBRMS = CreditReport.validateBrmsFlag(theOpp); 
        if(!dontRunBRMS && bHasCR) {
            bHasCR = creditReportEntity <> null; 
        }
        
        String sFico = String.isEmpty(theOpp.FICO__c) ? 'N/A' : theOpp.FICO__c;
        String kFac = theOpp.KeyFactors__c != null ? theOpp.KeyFactors__c : ' ';
        kFac = !bHasCR ||  creditReportEntity.brms == null || sFico.startsWith('900') ? 'CREDIT REPORT UNAVAILABLE' : kFac;
        if (sFico.startsWith('900') || sFico.contains('-')) sFico = 'N/A';

            theValue = theValue.replace('{!LetterDate}', (theOpp.CreditPull_Date__c != null) ? Datetime.newInstance(theOpp.CreditPull_Date__c, DateTime.now().Time() ).format('MMMM d,  yyyy') : Datetime.now().format('MMMM d,  yyyy'));
            theValue = theValue.replace('{!CreditReport.CreditBureau}', !bHasCR ||  creditReportEntity.brms == null ||  creditReportEntity.datasourceName == null ? 'N/A' : ( creditReportEntity.datasourceName.equalsIgnoreCase('ExpConsUSV7') ? 'Experian' : 'Transunion'));            
            theValue = theValue.replace('{!Opportunity.FICO__c}', sFico);
            theValue = theValue.replace('{!hasCreditScore}', sFico != null ? '_' : 'X');
            

        return theValue;
    }
}