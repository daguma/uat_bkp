@isTest public class DecisioningAttributeEntityTest {
    
    @isTest static void validates(){
        
        ProxyApiCreditReportAttributeEntity attrEntity = new ProxyApiCreditReportAttributeEntity();
        
        DecisioningAttributeEntity decisioningAttr = new DecisioningAttributeEntity(attrEntity, attrEntity);
        
        Boolean result = decisioningAttr.showHardIsApproved;
        
        System.assertEquals(true, result);
    }

}