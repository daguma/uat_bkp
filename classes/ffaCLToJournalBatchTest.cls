//This test class includes coverage for ffaCLToJournalBatch and ffaCLToJournalBatchHandler

@isTest private class ffaCLToJournalBatchTest {

    private static c2g__codaCompany__c testCompany;
    private static c2g__codaCompany__c testCompany2;
    private static Account testAccount;
    private static loan__Loan_Account__c testLoanAccount;
    private static loan__Loan_Disbursal_Transaction__c loanDisbursal;
    private static loan__Payment_Mode__c testpayModDisbusral;
    private static Set<Id> loanDisbursalIds;
    private static Set<Id> disbursalReversalIds;
    private static Set<Id> loanPaymentIds;
    private static Set<Id> loanRecoveryPaymentIds;
    private static Set<Id> loanPaymentReversalIds;
    private static Set<Id> loanChargesIds;
    private static Set<Id> loanChargeOffIds;
    private static Set<Id> loanSellingtoSPEIds;
    private static String  transactionType;
    private static loan__Payment_Mode__c paymentMode;
    private static loan__Loan_Payment_Transaction__c pmt;
    private static c2g__codaGeneralLedgerAccount__c gla;

    private static void setups_test_data() {
        loanDisbursalIds = new Set<Id>();
        loanPaymentIds = new Set<Id>();
        loanRecoveryPaymentIds = new Set<Id>();
        loanPaymentReversalIds = new Set<Id>();
        loanChargesIds = new Set<Id>();
        loanChargeOffIds = new Set<Id>();
        loanSellingtoSPEIds = new Set<Id>();

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c companyLLC = new c2g__codaCompany__c();
            companyLLC.Name = 'Lendingpoint LLC';
            companyLLC.RecordTypeId = recordTy.Id;
            companyLLC.OwnerId = newGroup.Id;
            insert companyLLC;

            c2g__codaCompany__c companyHol = new c2g__codaCompany__c();
            companyHol.Name = 'Lendingpoint Holdings LLC';
            companyHol.RecordTypeId = recordTy.Id;
            companyHol.OwnerId = newGroup.Id;
            insert companyHol;

            c2g__codaCompany__c companySPE = new c2g__codaCompany__c();
            companySPE.Name = 'Lendingpoint SPE LLC';
            companySPE.RecordTypeId = recordTy.Id;
            companySPE.OwnerId = newGroup.Id;
            insert companySPE;

            testCompany = [SELECT id, Name, OwnerId FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' LIMIT 1];
            testAccount = TestHelperForManaged.createBorrower('ShoeString');
            testCompany2 = [SELECT id, Name, OwnerId FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint SPE LLC' LIMIT 1];

            paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'testName';
            insert paymentMode;

            testLoanAccount = LibraryTest.createContractTH();

            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = testCompany.Id;
            update testLoanAccount;

            pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = testLoanAccount.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today();
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Payment_Mode__c = paymentMode.Id;
            insert pmt;

            GroupMember newGroupMember = new GroupMember();
            newGroupMember.GroupId = newGroup.Id;
            newGroupMember.UserOrGroupId = thisUser.Id;
            insert newGroupMember;

            gla = new c2g__codaGeneralLedgerAccount__c();
            gla.Name = 'Test Accrued Interest';
            gla.c2g__ReportingCode__c = '1132';
            gla.c2g__TrialBalance1__c = 'Current Assets';
            gla.c2g__TrialBalance2__c = 'Other Assets';
            gla.c2g__Type__c = 'Balance Sheet';
            gla.c2g__UnitOfWork__c = 192;
            insert gla;

            CL_FFA_Mapping__c ffaMapping = new CL_FFA_Mapping__c();
            ffaMapping.Company__c = testCompany.Id;
            insert ffaMapping;

            CL_FFA_Mapping__c ffaMapping2 = new CL_FFA_Mapping__c();
            ffaMapping2.SPE_Payment_Debit_1__c = gla.Id;
            ffaMapping2.Company__c = testCompany2.Id;
            insert ffaMapping2;
        }
    }

    @isTest static void applies_disbursement() {

        setups_test_data();

        transactionType = 'Initial Disbursement';

        Test.startTest();

        loanDisbursal = ffaTestUtilities.createLoanDisbursal(testLoanAccount.id, paymentMode.id);
        loanDisbursalIds.add(loanDisbursal.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanDisbursalIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void reverses_disbursement() {

        setups_test_data();

        transactionType = 'Initial Disbursement Reversal';

        Test.startTest();

        loan__Disbursal_Adjustment__c disbursalReversal = ffaTestUtilities.createLoanDisbursalReversal(ffaTestUtilities.createLoanDisbursal(testLoanAccount.id, paymentMode.id).id);
        disbursalReversalIds = new Set<Id>();
        disbursalReversalIds.add(disbursalReversal.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, disbursalReversalIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void makes_a_payment() {

        setups_test_data();

        transactionType = 'Payment';

        Test.startTest();

        List<loan__Loan_Payment_Transaction__c> lptList = new List<loan__Loan_Payment_Transaction__c>();
        lptList.add(pmt);
        loanPaymentIds.add(pmt.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanPaymentIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void makes_a_payment_spe() {

        setups_test_data();

        transactionType = 'Payment';

        Test.startTest();

        //Set the parent loan accounts company to SPE to ensure the proper method is hit in the journal batch handler class
        testLoanAccount.Company__c = testCompany2.Id;
        update testLoanAccount;

        loanPaymentIds.add(pmt.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanPaymentIds, testCompany2, null, true);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void recovers_a_payment() {

        setups_test_data();

        transactionType = 'Recovery Payment';

        Test.startTest();

        testLoanAccount.loan__loan_status__c = 'Closed- Written Off';
        testLoanAccount.loan__Principal_Remaining__c = 200;
        update testLoanAccount;

        pmt.loan__Write_Off_Recovery_Payment__c = true;
        update pmt;

        loanRecoveryPaymentIds.add(pmt.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanRecoveryPaymentIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void reverses_a_payment() {

        setups_test_data();

        transactionType = 'Payment Reversal';

        Test.startTest();

        testLoanAccount.loan__loan_status__c = 'Closed- Written Off';
        testLoanAccount.loan__Principal_Remaining__c = 200;
        update testLoanAccount;

        pmt.loan__Write_Off_Recovery_Payment__c = true;
        pmt.loan__Cleared__c = true;
        update pmt;

        loan__Repayment_Transaction_Adjustment__c rpmt = new loan__Repayment_Transaction_Adjustment__c();

        rpmt.loan__Loan_Payment_Transaction__c = pmt.Id;
        rpmt.FFA_Integration_Complete__c = false;
        rpmt.loan__Adjustment_Txn_Date__c = Date.today();
        insert rpmt;

        List<loan__Repayment_Transaction_Adjustment__c> lstRepaymentTransactionAdjustment = new List<loan__Repayment_Transaction_Adjustment__c>();
        lstRepaymentTransactionAdjustment.add(rpmt);
        loanPaymentReversalIds.add(rpmt.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanPaymentReversalIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void reverses_a_payment_spe() {

        setups_test_data();

        transactionType = 'Payment Reversal';

        Test.startTest();

        testLoanAccount.Company__c = testCompany2.Id;
        testLoanAccount.loan__loan_status__c = 'Closed- Written Off';
        testLoanAccount.loan__Principal_Remaining__c = 200;
        update testLoanAccount;

        pmt.loan__Write_Off_Recovery_Payment__c = true;
        pmt.loan__Cleared__c = true;
        update pmt;

        loan__Repayment_Transaction_Adjustment__c rpmt = new loan__Repayment_Transaction_Adjustment__c();

        rpmt.loan__Loan_Payment_Transaction__c = pmt.Id;
        rpmt.FFA_Integration_Complete__c = false;
        rpmt.loan__Adjustment_Txn_Date__c = Date.today();
        insert rpmt;

        List<loan__Repayment_Transaction_Adjustment__c> lstRepaymentTransactionAdjustment = new List<loan__Repayment_Transaction_Adjustment__c>();
        lstRepaymentTransactionAdjustment.add(rpmt);
        loanPaymentReversalIds.add(rpmt.id);

        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanPaymentReversalIds, testCompany2, null, true);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void charges_loan() {

        setups_test_data();

        transactionType = 'Charges';

        Test.startTest();

        loan__Fee__c fee = new loan__Fee__c();
        fee.loan__Amount__c = 25;
        fee.loan__Default_fees__c = true;
        fee.loan__Fee_Calculation_Method__c = 'Fixed';
        fee.loan__Fee_Category__c = 'Loan';
        fee.Revenue_GLA__c = gla.Id;
        insert fee;

        loan__Charge__c loanCharge = new loan__Charge__c();
        loanCharge.loan__Loan_Account__c = testLoanAccount.Id;
        loanCharge.loan__Charge_Type__c = 'Other';
        loanCharge.loan__Date__c = Date.today().addDays(-15);
        loanCharge.loan__Fee__c = fee.Id;
        loanCharge.loan__Original_Amount__c = 25;
        loanCharge.loan__Paid_Amount__c = 0;
        loanCharge.loan__Paid__c = false;
        loanCharge.loan__Principal_Due__c = 25;
        loanCharge.loan__Balance__c = 5000;
        loanCharge.loan__Payoff_Balance__c = 8500;
        insert loanCharge;

        loan__Fee_Payment__c charge = new loan__Fee_Payment__c();
        charge.Fee_Payment_Processing_Complete__c = false;
        charge.loan__Charge__c = loanCharge.Id;
        charge.loan__Transaction_Amount__c = 200;
        charge.loan__Transaction_Date__c = Date.today().addDays(-2);
        insert charge;

        List<loan__Fee_Payment__c> newList = new List<loan__Fee_Payment__c>();
        newList.add(charge);
        loanChargesIds.add(charge.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanChargesIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void charges_loan_spe() {

        setups_test_data();

        transactionType = 'Charges';

        Test.startTest();

        testLoanAccount.Company__c = testCompany2.Id;
        update testLoanAccount;

        loan__Fee__c fee = new loan__Fee__c();
        fee.loan__Amount__c = 25;
        fee.loan__Default_fees__c = true;
        fee.loan__Fee_Calculation_Method__c = 'Fixed';
        fee.loan__Fee_Category__c = 'Loan';
        fee.Revenue_GLA__c = gla.Id;
        insert fee;

        loan__Charge__c loanCharge = new loan__Charge__c();
        loanCharge.loan__Loan_Account__c = testLoanAccount.Id;
        loanCharge.loan__Charge_Type__c = 'Other';
        loanCharge.loan__Date__c = Date.today().addDays(-15);
        loanCharge.loan__Fee__c = fee.Id;
        loanCharge.loan__Original_Amount__c = 25;
        loanCharge.loan__Paid_Amount__c = 0;
        loanCharge.loan__Paid__c = false;
        loanCharge.loan__Principal_Due__c = 25;
        loanCharge.loan__Balance__c = 5000;
        loanCharge.loan__Payoff_Balance__c = 8500;
        insert loanCharge;

        loan__Fee_Payment__c charge = new loan__Fee_Payment__c();
        charge.Fee_Payment_Processing_Complete__c = false;
        charge.loan__Charge__c = loanCharge.Id;
        charge.loan__Transaction_Amount__c = 200;
        charge.loan__Transaction_Date__c = Date.today().addDays(-2);
        insert charge;

        List<loan__Fee_Payment__c> newList = new List<loan__Fee_Payment__c>();
        newList.add(charge);
        loanChargesIds.add(charge.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanChargesIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void chargeoff_loan() {

        setups_test_data();

        transactionType = 'Charge Off';

        Test.startTest();

        testLoanAccount.FFA_Integration_Charge_Off_Complete__c = false;
        testLoanAccount.loan__Loan_Status__c = 'Closed- Written Off';
        testLoanAccount.loan__Charged_Off_Date__c = Date.today().addDays(-1);
        update testLoanAccount;

        List<loan__Loan_Account__c> newList = new List<loan__Loan_Account__c>();
        newList.add(testLoanAccount);
        loanChargeOffIds.add(testLoanAccount.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanChargeOffIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void chargeoff_loan_spe() {

        setups_test_data();

        transactionType = 'Charge Off';

        Test.startTest();

        testLoanAccount.Company__c = testCompany2.Id;
        testLoanAccount.FFA_Integration_Charge_Off_Complete__c = false;
        testLoanAccount.loan__Loan_Status__c = 'Closed- Written Off';
        testLoanAccount.loan__Charged_Off_Date__c = Date.today().addDays(-1);
        update testLoanAccount;

        List<loan__Loan_Account__c> newList = new List<loan__Loan_Account__c>();
        newList.add(testLoanAccount);
        loanChargeOffIds.add(testLoanAccount.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanChargeOffIds, testCompany2, null, true);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void sell_to_spe() {

        setups_test_data();

        transactionType = 'Selling to SPE';

        Test.startTest();

        testLoanAccount.FFA_Integration_Sale_Complete__c = false;
        testLoanAccount.Asset_Sale_Date__c = Date.today().addDays(-15);
        update testLoanAccount;

        loanSellingtoSPEIds.add(testLoanAccount.id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanSellingtoSPEIds, testCompany, null, false);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

    @isTest static void sell_to_spe_part_2() {

        setups_test_data();

        transactionType = 'Selling to SPE';

        Test.startTest();

        testLoanAccount.Company__c = testCompany2.Id;
        testLoanAccount.FFA_Integration_Sale_Complete__c = false;
        testLoanAccount.Asset_Sale_Date__c = Date.today().addDays(-15);
        update testLoanAccount;

        loanSellingtoSPEIds.add(testLoanAccount.Id);
        ffaCLToJournalBatch batchClass = new ffaCLToJournalBatch(transactionType, loanSellingtoSPEIds, testCompany2, null, true);
        Database.executeBatch(batchClass, 1);

        Test.stopTest();
    }

}