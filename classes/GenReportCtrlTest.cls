@isTest public class GenReportCtrlTest {

    @isTest static void test_initialize() {

        ApexPages.currentPage().getParameters().put('n' , '');

        GenReportCtrl grc = new GenReportCtrl();

        System.assertEquals('Invalid Parameters - Missing Name', grc.errorMessage);
    }
}