global class AutoAgingAppsBatch implements Database.Batchable<sObject>,  Schedulable {

    global List<Opportunity> appListToUpdate = new List<Opportunity>();
    
    global AutoAgingAppsBatch() {}

    public void execute(SchedulableContext sc) { 
      
            AutoAgingAppsBatch b = new AutoAgingAppsBatch();
            database.executebatch(b, 15);   

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {    
        
        List<Auto_Aging_Logic__c> autoAgingSettings = [SELECT Id, Status__c, Based_on__c, Days__c, Move_to__c, Reject_Offer_Reason__c, Decline_Note__c, Use_Business_Days__c FROM Auto_Aging_Logic__c ORDER BY Days__c DESC];
        
        String conditions = '';
        
        for (Auto_Aging_Logic__c autoSet : autoAgingSettings){

            Datetime businessDate = SalesForceUtil.getBusinessDate(Integer.valueOf(autoSet.Days__c));

            String day = String.valueOf(businessDate.dayGMT()), dateField = '';
            String month = String.valueOf(businessDate.monthGMT());

            String newDate = businessDate.year()+'-'+(month.length() == 1 ? '0' + month : month)+'-'+(day.length() == 1 ? '0' + day : day);


            if (isModifyEvent(autoSet.Based_on__c)) {
                dateField = ' Status_Modified_Date__c ';   
            }else{
                dateField = ' CreatedDate ';
                newDate += 'T00:00:00.000+0000'; 
            }

            conditions += '(Status__c = \'' + autoSet.Status__c + '\' AND ' + dateField + ' < ' + 
                (autoSet.Use_Business_Days__c ? newDate : ('LAST_N_DAYS:' + autoSet.Days__c )) +') OR ';         

        }
        
        String query = 'SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c FROM Opportunity '+
                       'WHERE Id = \'0064O00000jeLSiQAM\' AND Type <> \'Refinance\' AND Type <> \'Multiloan\' AND ( ' + conditions.removeEnd(' OR ') + ' ) ORDER BY CreatedDate ';
        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        List<Auto_Aging_Logic__c> settings =  [SELECT Id, Status__c, Based_on__c, Days__c, Move_to__c, Reject_Offer_Reason__c, Decline_Note__c, Use_Business_Days__c FROM Auto_Aging_Logic__c ORDER BY Days__c DESC ];
        for (Opportunity app : scope) {
            for(Auto_Aging_Logic__c setting: settings)
                if(meetAgingCriteria(setting, app)) break;  
        }    
        if (appListToUpdate.size() > 0) Database.update(appListToUpdate, false);
    }

    private Boolean meetAgingCriteria(Auto_Aging_Logic__c setting, Opportunity app) {
        
        if((checkStatus(app.Status__c, setting.Status__c) && !isModifyEvent(setting.Based_on__c) && meetDateAging(app.CreatedDate.date(), setting.Days__c, setting.Use_Business_Days__c)) || 
            ( checkStatus(app.Status__c, setting.Status__c) && isModifyEvent(setting.Based_on__c) && meetDateAging(app.Status_Modified_Date__c, setting.Days__c, setting.Use_Business_Days__c))){
            setAppStatusAndReason(app, setting.Move_to__c, setting.Reject_Offer_Reason__c, setting.Decline_Note__c);
            return true;
        }
        return false;          
    }

    private Boolean checkStatus(String status, String settingStatus) {
        return (status.containsIgnoreCase(settingStatus));
    }

    private Boolean isModifyEvent(String event) {
        return (event == 'Status Change Date');
    }

    private Boolean meetDateAging(Date appDate, Decimal days, Boolean isBusinessDays) {
        if (isBusinessDays)
            return (SalesForceUtil.calculateBusinessDays(appDate, Datetime.now().date()) >= days);
        else
            return (appDate < Date.today().addDays((days.intValue()) * -1));
    }

    private void setAppStatusAndReason(Opportunity app, String status, String reason, String note) {

        app.Status__c = status;
        app.Reason_of_Opportunity_Status__c = reason;
        app.Decline_Note__c = (note != '')? note : app.Decline_Note__c;   
        appListToUpdate.add(app);
    }
    
    global void finish(Database.BatchableContext BC) {
    }

}