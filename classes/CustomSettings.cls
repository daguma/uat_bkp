public class CustomSettings{

    public static LP_Custom__c getLPCustom(){
        LP_Custom__c lp = LP_Custom__c.getInstance();
        if(lp == null){
            lp = LP_Custom__c.getOrgDefaults();
        }
        
        return lp;
        
    }
      /* #3187 New method to get the Custom settings for EZVerify*/
    public static ezVerify_Settings__c getEZVerifyCustom(){
        ezVerify_Settings__c ez = ezVerify_Settings__c.getInstance();
        if(ez == null){
            ez = ezVerify_Settings__c.getOrgDefaults();
        }
        
        return ez;
        
    }
    
    ////API-244 & API-245
    @InvocableMethod(label='capexMDNotification')
    public static void capexMDNotification(List<ID> applicationIds){
        try {
            system.debug('---Inside capex---');
            capexMDNotificationAPI(applicationIds);
        }catch (Exception e) {
            System.debug('Error:' + e.getMessage());
            WebToSFDC.notifyDev('capexMDNotification Exception',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
    }
     //API-244 & API-245
    @future(callout=true)
    public static void capexMDNotificationAPI(list<ID> appIDList) {
        system.debug('---Inside API---');
        List<Logging__c> logList = new List<Logging__c>();
        map<id,Opportunity> oppMapToBeUpdated = new map<id,Opportunity>();
        Opportunity oppr;
        String endPointURL = '', body = '', dayString, API_NAME = '';
        map<ID, list<Provider_Payments__c>> providerPayMap = new map<ID, list<Provider_Payments__c >>();
        try {   
            for(Provider_Payments__c pPay : [select id, Payment_Amount__c, Opportunity__c from Provider_Payments__c where Opportunity__c In :appIDList order by createdDate desc]) {
                if(providerPayMap.containskey(pPay.Opportunity__c)) {
                    list<Provider_Payments__c> pPayList = providerPayMap.get(pPay.Opportunity__c);
                    pPayList.add(pPay);
                    providerPayMap.put(pPay.Opportunity__c,pPayList);
                } else providerPayMap.put(pPay.Opportunity__c,new List<Provider_Payments__c>{pPay});
            }        
            for(Opportunity opp : [Select id, Name, Lead_Sub_Source__c, Partner_Notification_Sent_Status__c, Status__c, Amount, Funded_Date__c, Point_Code__c, Total_Loan_Amount__c, Lending_Account__c, Lending_Account__r.Name from Opportunity where Lead_Sub_Source__c <> null and id In :appIDList]) {
                oppr = opp;
                API_NAME = label.CapexSubsources.contains(opp.Lead_Sub_Source__c.toLowerCase()) ? 'CapexMDNotification' : (Label.LeadSubSource_Motoloan.contains(opp.Lead_Sub_Source__c.toLowerCase()) ? 'MotoloanNotification': '');
                if(label.CapexSubsources.contains(opp.Lead_Sub_Source__c.toLowerCase()) || (string.isNotEmpty(opp.Partner_Notification_Sent_Status__c) && !opp.Partner_Notification_Sent_Status__c.contains(opp.Status__c) && Label.LeadSubSource_Motoloan.contains(opp.Lead_Sub_Source__c.toLowerCase())) || string.isEmpty(opp.Partner_Notification_Sent_Status__c)) {
                    if(opp.Status__c== 'Funded') {
                        date myDate = opp.Funded_Date__c != null ? date.newInstance(opp.Funded_Date__c.year(), opp.Funded_Date__c.month(), opp.Funded_Date__c.day()) : null;
                        dayString = myDate != null ? myDate.format() : 'null';
                        Decimal fundedAmt = null;
                        if(providerPayMap.containskey(opp.id)) {
                            for(Provider_Payments__c pp : providerPayMap.get(opp.id)) {
                                fundedAmt = fundedAmt <> null ? + pp.Payment_Amount__c : pp.Payment_Amount__c ; 
                            }
                        }
                        if (fundedAmt == null ) fundedAmt = opp.Amount;
                        system.debug('-------opp-----'+opp.Lending_Account__c);
                        endPointURL = (label.CapexSubsources.contains(opp.Lead_Sub_Source__c.toLowerCase()) ? label.CapexMD_Replace_Funding_Notice : (Label.LeadSubSource_Motoloan.contains(opp.Lead_Sub_Source__c.toLowerCase()) ? Label.MotoLoan_Funding_Notice_Endpoint : endPointURL));
                        body = 'FundingNotificationDate=' + dayString  +  
                        '&ApplicationNumber=' + opp.Point_Code__c +
                        '&FundedAmount=' + fundedAmt + 
                        '&LAINumber=' +   (opp.Lending_Account__c != null ? opp.Lending_Account__r.Name : '');
        
                    }
                    else if(opp.Status__c == 'Approved, Pending Funding') {
                        endPointURL = (label.CapexSubsources.contains(opp.Lead_Sub_Source__c.toLowerCase()) ? label.CapexMD_Replace_Approval_Notice : (Label.LeadSubSource_Motoloan.contains(opp.Lead_Sub_Source__c.toLowerCase()) ? Label.MotoLoan_Approval_Notice_Endpoint : endPointURL));
                        date myDate = date.newInstance(date.today().year(), date.today().month(), date.today().day());
                        dayString = myDate.format();
                        body = 'ApprovedDate=' + dayString +
                                '&LendingPointLoanAmount=' + opp.Amount +  
                                '&ApplicationNumber=' + opp.Point_Code__c + 
                                '&TrackingNumber=' + opp.Name;
                    }
                    if(string.isNotEmpty(endPointURL)) {
                        HttpRequest request = new HttpRequest();
                        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                        system.debug('URL = ' + endPointURL + body);
                        request.setEndpoint(endPointURL + body);
                        request.setMethod('POST');
                        request.setTimeout(120000);
                        //request.setBody(body);
                        HTTPResponse response = Test.isRunningTest() ? new HTTPResponse() : new Http().send(request);
                        if (Test.isRunningTest()) {
                            response.setBody('1');
                            response.setStatusCode(200);
                        }
                        system.debug('----response----'+response+'---body---'+response.getBody()+'--msg---'+response.getBody().contains('"message":"Success"'));
                        
                        if(response.getStatusCode() == 200 && (response.getBody().contains('1') || response.getBody().contains('"message":"Success"'))){
                            logList.add(new Logging__c(Webservice__c= API_NAME + ' API Success', API_Request__c=endPointURL + body, API_Response__c = 'Response for Status Changed To '+ opp.Status__c +': '+ response+ '\n Body: '+ response.getBody(), Opportunity__c = opp.id));
                            if(opp.Status__c == 'Funded' && label.CapexSubsources.contains(opp.Lead_Sub_Source__c.toLowerCase()))    opp.CapexMD_Funding_Notification_Sent__c = date.today();
                            else if(opp.Status__c == 'Approved, Pending Funding'  && label.CapexSubsources.contains(opp.Lead_Sub_Source__c.toLowerCase())) opp.CapexMD_Approval_Notification_Sent__c = date.today();
                            else if(Label.LeadSubSource_Motoloan.contains(opp.Lead_Sub_Source__c.toLowerCase())) opp.Partner_Notification_Sent_Status__c= string.isEmpty(opp.Partner_Notification_Sent_Status__c) ? opp.Status__c : opp.Partner_Notification_Sent_Status__c+ ';' + opp.Status__c;
                            oppMapToBeUpdated.put(opp.id, opp);
                        }
                        else {
                            logList.add(new Logging__c(Webservice__c= API_NAME  + ' API Failure', API_Request__c= endPointURL + body, API_Response__c = 'Response for Status Changed To '+opp.Status__c+': '+ response + '\n Body: '+ response.getBody()+' for Status' + opp.Status__c, Opportunity__c = opp.id));
                        }        
                    }
                }
            }
        }
        catch (Exception e) {
            if(oppr != null && oppr.id != null) logList.add(new Logging__c(Webservice__c='Partner Notification API Exception', API_Request__c= endPointURL + body, API_Response__c=e.getMessage(), Opportunity__c = oppr.id));
            WebToSFDC.notifyDev('CapexMDNotification Exception',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
        if(!logList.isEmpty()) insert logList;
        if(!oppMapToBeUpdated.values().isEmpty())  update oppMapToBeUpdated.values();
    }
    
   /* public static Loan_Hero_Settings__c getloanHeroCustom(){
        Loan_Hero_Settings__c lHero = Loan_Hero_Settings__c.getInstance();
        if(lHero == null){
            lHero = Loan_Hero_Settings__c.getOrgDefaults();
        }
        
        return lHero ;
        
    } */
    
     // API-335 New method to get the Custom settings for finTech
    public static Fintech_Custom_Setting__c getFintechCustom(){
        Fintech_Custom_Setting__c ft = Fintech_Custom_Setting__c.getInstance();
        if(ft== null){
            ft= Fintech_Custom_Setting__c.getOrgDefaults();
        }
        
        return ft;
        
    }
}