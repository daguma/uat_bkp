@isTest
private class BatchAONCancellationTest {

    @isTest static void contract_is_canceled() {
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Canceled';
        update testLoanAccount;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = testLoanAccount.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;

        loan_AON_Cancellations__c cancel = new loan_AON_Cancellations__c();
        cancel.loan_AON_Information__c = enrollment.Id;
        cancel.Cancel_Reason_Code__c = 'abc';
        cancel.Effective_Date__c = System.now();
        cancel.Refund_Amount__c = 100;
        insert cancel;

        Test.startTest();

        BatchAONCancellation batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);

        Test.stopTest();

        System.assertEquals('CLP', [SELECT Cancel_Reason_Code__c FROM loan_AON_Cancellations__c WHERE Id = : cancel.Id].Cancel_Reason_Code__c);
    }

    @isTest static void contract_is_closed_writen_off() {
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Closed- Written Off';
        update testLoanAccount;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = testLoanAccount.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;

        loan_AON_Cancellations__c cancel = new loan_AON_Cancellations__c();
        cancel.loan_AON_Information__c = enrollment.Id;
        cancel.Cancel_Reason_Code__c = 'abc';
        cancel.Effective_Date__c = System.now();
        cancel.Refund_Amount__c = 100;
        insert cancel;

        Test.startTest();

        BatchAONCancellation batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);

        Test.stopTest();

        System.assertEquals('CLP', [SELECT Cancel_Reason_Code__c FROM loan_AON_Cancellations__c WHERE Id = : cancel.Id].Cancel_Reason_Code__c);
    }

    @isTest static void contract_is_closed_obligations_met() {
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Closed - Obligations Met';
        update testLoanAccount;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = testLoanAccount.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;

        loan_AON_Cancellations__c cancel = new loan_AON_Cancellations__c();
        cancel.loan_AON_Information__c = enrollment.Id;
        cancel.Cancel_Reason_Code__c = 'abc';
        cancel.Effective_Date__c = System.now();
        cancel.Refund_Amount__c = 100;
        insert cancel;

        Test.startTest();

        BatchAONCancellation batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);

        Test.stopTest();

        System.assertEquals('CLP', [SELECT Cancel_Reason_Code__c FROM loan_AON_Cancellations__c WHERE Id = : cancel.Id].Cancel_Reason_Code__c);
    }

    @isTest static void contract_is_aim_canceled() {
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Pmt_Amt_Cur__c = 200;
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
        update testLoanAccount;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = testLoanAccount.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        enrollment.IsAIMCancellation__c = true;
        insert enrollment;

        loan_AON_Cancellations__c cancel = new loan_AON_Cancellations__c();
        cancel.loan_AON_Information__c = enrollment.Id;
        cancel.Cancel_Reason_Code__c = 'JLP';
        cancel.Effective_Date__c = System.now();
        cancel.Refund_Amount__c = 100;
        insert cancel;

        Test.startTest();

        BatchAONCancellation batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);

        Test.stopTest();

        System.assertEquals('JLP', [SELECT Cancel_Reason_Code__c FROM loan_AON_Cancellations__c WHERE Id = : cancel.Id].Cancel_Reason_Code__c);
    }

    @isTest static void contract_is_canceled_with_error() {
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Canceled';
        update testLoanAccount;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = testLoanAccount.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        insert enrollment;

        loan_AON_Cancellations__c cancel = new loan_AON_Cancellations__c();
        cancel.loan_AON_Information__c = enrollment.Id;
        cancel.Cancel_Reason_Code__c = 'JLP';
        cancel.Effective_Date__c = System.now();
        cancel.Refund_Amount__c = 100;
        insert cancel;

        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"aonResponse":null,"errorMessage":"Error","errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        BatchAONCancellation batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);

        //System.assertEquals('JLP',[SELECT Cancel_Reason_Code__c FROM loan_AON_Cancellations__c WHERE Id =: cancel.Id].Cancel_Reason_Code__c);
    }

    @isTest static void contract_is_unenrolled_after_cancellation() {
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Pmt_Amt_Cur__c = 200;
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
        testLoanAccount.AON_Enrollment__c = true;
        update testLoanAccount;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = testLoanAccount.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        enrollment.RetryCount__c = 2;
        insert enrollment;

        loan_AON_Cancellations__c cancel = new loan_AON_Cancellations__c();
        cancel.loan_AON_Information__c = enrollment.Id;
        cancel.Cancel_Reason_Code__c = 'abc';
        cancel.Effective_Date__c = System.now();
        cancel.Refund_Amount__c = 100;
        insert cancel;

        Test.startTest();

        BatchAONCancellation batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);

        Test.stopTest();

        System.assertEquals(0, [SELECT RetryCount__c FROM loan_AON_Information__c WHERE Id = : enrollment.Id].RetryCount__c);
        System.assertEquals(false, [SELECT AON_Enrollment__c FROM loan__loan_Account__c WHERE Id = : testLoanAccount.Id].AON_Enrollment__c);
    }

    @isTest static void contract_still_enrolled_after_closed() {
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Closed - Obligations Met';
        testLoanAccount.AON_Enrollment__c = true;
        update testLoanAccount;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = testLoanAccount.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        enrollment.RetryCount__c = 2;
        insert enrollment;

        loan_AON_Cancellations__c cancel = new loan_AON_Cancellations__c();
        cancel.loan_AON_Information__c = enrollment.Id;
        cancel.Cancel_Reason_Code__c = 'abc';
        cancel.Effective_Date__c = System.now();
        cancel.Refund_Amount__c = 100;
        insert cancel;

        Test.startTest();

        BatchAONCancellation batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);

        Test.stopTest();

        System.assertEquals(0, [SELECT RetryCount__c FROM loan_AON_Information__c WHERE Id = : enrollment.Id].RetryCount__c);
        System.assertEquals(true, [SELECT AON_Enrollment__c FROM loan__loan_Account__c WHERE Id = : testLoanAccount.Id].AON_Enrollment__c);
        System.assertEquals('CLP', [SELECT Cancel_Reason_Code__c FROM loan_AON_Cancellations__c WHERE Id = : cancel.Id].Cancel_Reason_Code__c);
    }

    @isTest static void close_claim_status() {
        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Closed - Obligations Met';
        testLoanAccount.AON_Enrollment__c = true;
        update testLoanAccount;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = testLoanAccount.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        enrollment.RetryCount__c = 0;
        enrollment.Claim_Status__c = 'A';
        insert enrollment;

        loan_AON_Cancellations__c cancel = new loan_AON_Cancellations__c();
        cancel.loan_AON_Information__c = enrollment.Id;
        cancel.Cancel_Reason_Code__c = 'abc';
        cancel.Effective_Date__c = System.now();
        cancel.Refund_Amount__c = 100;
        insert cancel;

        Test.startTest();

        BatchAONCancellation batchapex = new BatchAONCancellation();
        id batchprocessid = Database.executebatch(batchapex, 1);

        Test.stopTest();

        System.assertEquals('C', [SELECT Claim_Status__c FROM loan_AON_Information__c WHERE Id = : enrollment.Id].Claim_Status__c);
    }

    @isTest static void schedule_job() {
        Test.startTest();

        BatchAONCancellation cancelJob = new BatchAONCancellation();
        String cron = '0 0 23 * * ?';
        system.schedule('Test AON Cancellation Job', cron, cancelJob);

        Test.stopTest();
    }
}