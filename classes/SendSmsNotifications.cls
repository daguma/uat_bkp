global class SendSmsNotifications implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {
    String query;
    List<Campaign> campaigns;

    public SendSmsNotifications() {}

    global void execute(SchedulableContext SC) {
        SendSmsNotifications scheduler = new SendSmsNotifications();

        List<Campaign> campaigns = scheduler.retrieveActiveCampaignsCreatedToday();
        List<Campaign> campaignsToUpdate = new List<Campaign>();

        for (Campaign campaign : campaigns) {
            campaign.SMS_Notification_Status__c = 'In Progress';
            campaignsToUpdate.add(campaign);
        }

        scheduler.campaigns = campaigns;
        scheduler.buildQuery();
        Database.executeBatch(scheduler, 100);

        update campaignsToUpdate;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
 		return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
       for(CampaignMember campaignMember : (List<CampaignMember>) scope){
       // for(sObject campaignMember : scope){
           addSmsNotificationToDeliveryQueue(campaignMember);
       }
   }

    global void finish(Database.BatchableContext BC){
        List<Campaign> campaignsToUpdate = new List<Campaign>();

        for (Campaign campaign : campaigns) {
            campaign.SMS_Notification_Status__c = 'Completed';
            campaign.SMS_Notifications_Sent_At__c = System.now();
            campaignsToUpdate.add(campaign);
        }

        update campaignsToUpdate;
    }

    @AuraEnabled
    public static void sendSmsNotificationsForCampaign(String campaignId) {
        SendSmsNotifications scheduler = new SendSmsNotifications();

        Campaign campaign = scheduler.retrieveCampaignById(campaignId);
        campaign.SMS_Notification_Status__c = 'In Progress';
        campaign.SMS_Notifications_Sent_From__c = System.now();
        update campaign;

        scheduler.campaigns = new List<Campaign>();
        scheduler.campaigns.add(campaign);
        scheduler.buildQuery();

        Database.executeBatch(scheduler, 100);
    }

    @AuraEnabled
    public static void stopSmsNotificationsForCampaign(String campaignId) {
        SendSmsNotifications scheduler = new SendSmsNotifications();
        Campaign campaign = scheduler.retrieveCampaignById(campaignId);

        if (campaign.SMS_Notification_Status__c != 'Active')
            return;

        campaign.SMS_Notification_Status__c = 'Paused';
        update campaign;
    }

    @AuraEnabled
    public static void startSmsNotificationsForCampaign(String campaignId) {
        SendSmsNotifications scheduler = new SendSmsNotifications();
        Campaign campaign = scheduler.retrieveCampaignById(campaignId);

        if (campaign.SMS_Notification_Status__c != 'Paused')
            return;

        campaign.SMS_Notification_Status__c = 'Active';
        update campaign;
    }

    public void sendSmsNotificationsForActiveCampaigns() {
        List<Campaign> campaigns = retrieveActiveCampaigns();

        for (Campaign campaign : campaigns) {
            sendSmsNotificationsForCampaign(campaign);
        }
    }

    public Campaign retrieveCampaignById(String campaignId) {
        Campaign campaign;

        try{
            campaign = [SELECT Id, Name, SMS_Notification_Status__c, SMS_Notification_Type__c
            			FROM Campaign WHERE Id = :campaignId LIMIT 1];
        } catch(Exception ex){
            campaign = null;
        }

        return campaign;
    }

    public List<Campaign> retrieveActiveCampaigns() {
        Datetime plannedDeliveryTime = System.now();

        List<Campaign> campaigns = [SELECT Id, Name, SMS_Notification_Status__c, SMS_Notification_Type__c
            FROM Campaign
            WHERE SMS_Notification_Status__c = 'Active'
                AND SMS_Notifications_Planned_Delivery_Time__c <= :plannedDeliveryTime];

        return campaigns;
    }

    public List<Campaign> retrieveActiveCampaignsCreatedToday() {
        Datetime todaysDate = Date.today();
        Datetime tomorrowsDate = todaysDate.addDays(1);

        campaigns = [SELECT Id, Name, SMS_Notification_Status__c, SMS_Notification_Type__c
            FROM Campaign
            WHERE SMS_Notification_Status__c = 'Active'
                AND CreatedDate >= :todaysDate];

        return campaigns;
    }

    public String buildQuery() {
      List<String> campaignIds = new List<String>();
      for (Campaign campaign : campaigns) {
        campaignIds.add('\'' + campaign.Id + '\'');
      }

      query = 'SELECT Id, Phone, MobilePhone, SMS_Notification__c, CampaignId ' +
              'FROM CampaignMember '   +
              'WHERE CampaignId IN ( ' +
                  String.join(campaignIds, ',') +
              ')';

      return query;
    }

    public Boolean sendSmsNotificationsForCampaign(Campaign campaign) {
        if (campaign.SMS_Notification_Status__c != 'Active') { return False; }

        String campaignId = campaign.Id;
        List<CampaignMember> members = [SELECT Id, Phone, MobilePhone, SMS_Notification__c
            FROM CampaignMember WHERE CampaignId = :campaignId];

        campaign.SMS_Notification_Status__c = 'In Progress';
        // update campaign;

		List<CampaignMember> membersToUpdate = new List<CampaignMember>();
		List<TwilioSF__Message__c> twilioMessages = new List<TwilioSF__Message__c>();

        String fromPhoneNumber = '+14702841416';
        String twilioAccountId = 'AC5157b4393187f597c39f17c2db3ca315';
        String messagingServiceId = 'MG2e0008f3dcf78f053004a9fd4a272e05';

        SMS_Notifications__c fromPhoneNumberSetting;
        SMS_Notifications__c twilioAccountIdSetting;
        SMS_Notifications__c twilioMessagingServiceIdSetting;

        try {
            fromPhoneNumberSetting = [SELECT id, Name, Value__c FROM SMS_Notifications__c
                                      WHERE Name = 'SMS_Notifications_Phone_Number' LIMIT 1];

            fromPhoneNumber = fromPhoneNumberSetting.Value__c;
        } catch (Exception e) {}

        try {
            twilioAccountIdSetting = [SELECT id, Name, Value__c FROM SMS_Notifications__c
                                      WHERE Name = 'Twilio_Account_Id' LIMIT 1];

            twilioAccountId = twilioAccountIdSetting.Value__c;
        } catch (Exception e) {}

        try {
            twilioMessagingServiceIdSetting = [SELECT id, Name, Value__c FROM SMS_Notifications__c
                                      WHERE Name = 'Twilio_Messaging_Service_Id' LIMIT 1];

            messagingServiceId = twilioMessagingServiceIdSetting.Value__c;
        } catch (Exception e) {}

        for (CampaignMember member : members) {
            TwilioSF.TwilioApiClient api = new TwilioSF.TwilioApiClient();
            api.addUrlPart('Accounts');
            api.addUrlPart(twilioAccountId);
            api.addUrlPart('Messages.json');

            api.addParam('To',   '+1' + member.Phone);
            api.addParam('From', fromPhoneNumber);
            api.addParam('Body', member.SMS_Notification__c);

            TwilioSF.TwilioApiClientResponse response;

            try {
                response = api.doPost();
                TwilioSF__Message__c twilioMessage = generateTwilioMessageRecord(campaignId, messagingServiceId, response);

                member.Status = 'Sent';
                membersToUpdate.add(member);
                twilioMessages.add(twilioMessage);
            } catch (Exception e){
                System.debug(e);
                System.debug(response);
            }

            // Wait for one second
            Datetime sleepTo = System.now().addSeconds(1);
            do {} while(System.now() <= sleepTo);
        }

        campaign = retrieveCampaignById(campaign.Id);
        campaign.SMS_Notification_Status__c   = 'Completed';
        campaign.SMS_Notifications_Sent_At__c = System.now();
        update campaign;

        insert twilioMessages;
        update membersToUpdate;
        return True;
    }

    public TwilioSF__Message__c generateTwilioMessageRecord(String campaignId, String messagingServiceId, TwilioSF.TwilioApiClientResponse response) {
        TwilioSF.TwilioJsonParser parser = response.getTwilioJsonParser();

        TwilioSF__Message__c tm = new TwilioSF__Message__c();
        tm.TwilioSF__Messaging_Service_Sid__c = messagingServiceId;

        tm.TwilioSF__Message_SID__c = parser.get('sid').getString();
        tm.TwilioSF__From_Number__c = parser.get('from').getString();
        tm.TwilioSF__To_Number__c   = parser.get('to').getString();
        tm.TwilioSF__Body__c        = parser.get('body').getString();
        tm.TwilioSF__Date_Sent__c   = System.now();
        tm.TwilioSF__SF_Parent_Record_Id__c = null;
        tm.CampaignId__c = campaignId;

        if(response.hasError()) {
            tm.TwilioSF__Error_Code__c = parser.get('code').getString();
            tm.TwilioSF__Status__c = 'failed';
        } else {
            tm.TwilioSF__Status__c = 'delivered';
        }

        return tm;
    }

    public Boolean addSmsNotificationToDeliveryQueue(CampaignMember campaignMember){
        // Default URL;
        String endpoint = 'https://iodev.lendingpoint.com/phone/sms';
        String contentTypeHeader = 'application/json';
        String applicationTokenHeader = 'dzfh8lrfszk6';
        String authorizationTokenHeader = '';
        
        SMS_Notifications__c endpointSetting;
        SMS_Notifications__c authorizationTokenSetting;

        try {
            endpointSetting = [SELECT id, Name, Value__c FROM SMS_Notifications__c
                               WHERE Name = 'SMS_Notifications_Queue_Endpoint' LIMIT 1];

            endpoint = endpointSetting.Value__c;
        } catch (Exception e) {}

        try {
            authorizationTokenSetting = [SELECT id, Name, Value__c FROM SMS_Notifications__c
                               WHERE Name = 'SMS_Notifications_Authorization_Token' LIMIT 1];

            authorizationTokenHeader = authorizationTokenSetting.Value__c;
        } catch (Exception e) {}


        HttpRequest req = new HttpRequest();
      	req.setEndpoint(endpoint);
        req.setMethod('POST');

        req.setHeader('Application-Token', applicationTokenHeader);
        req.setHeader('Authorization', authorizationTokenHeader);
        req.setHeader('Content-Type', contentTypeHeader);

        req.setBody('{"body":"' + campaignMember.SMS_Notification__c +'", "phoneNumber":"+1' + campaignMember.Phone + '"}');

        Http http = new Http();
        HTTPResponse res = http.send(req);
        //System.debug(res.getBody());

        return True;
    }
}