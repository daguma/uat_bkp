@isTest public class BatchMultiLoanBadStandingTestCtrl {
    
    @testSetup static void initializes_objects(){
        MultiLoan_Settings__c multiSet = new MultiLoan_Settings__c();
        multiSet.name = 'settings';
        multiSet.Check_Frequency__c = 30;
        multiSet.Maximum_Loan_Amount__c = 5000;
        multiSet.Minimum_Loan_Amount__c = 1000;
        multiSet.Days_To_First_Check__c = 60;
        multiSet.AccountId__c = '0011700000t45FtAAI';
        insert multiSet;
        
        Account acc = new Account();
        acc.Name = 'Multi-loan';
        insert acc;
    }
    
    
    @isTest static void schedule_job() {
        Test.startTest();

        BatchMultiLoanBadStanding job = new BatchMultiLoanBadStanding();
        String cron = '0 0 23 * * ?';
        system.schedule('Test Multi-Loan Job', cron, job);

        Test.stopTest();
    }
    
    private static Lending_Product__c creates_Product(){
        
        Lending_Product__c product = New Lending_Product__c();
        product.Name = 'Lending Point';
        product.Allow_Payoff_With_Uncleared_Payments__c = false;
        product.Amortization_Calculation_Method__c = 'Interest Bearing';
        product.Amortization_Enabled__c = true;
        product.Amortization_Method__c = 'Interest Bearing';
        product.Days_In_A_Year__c = 365;
        insert product;
        
        return product;
        
    }
    
     private static loan__Loan_Account__c creates_contract(){
         
         Lending_Product__c product = creates_Product();
         
         Opportunity opp = LibraryTest.createApplicationTH();
         opp.Type = 'Multiloan';
         opp.Status__c = 'Credit Qualified';
         update opp;
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Opportunity__c = opp.Id;
        contract.loan__Loan_Status__c = 'Active - Bad Standing';
		contract.loan__ACH_On__c = true;
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 1;
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(1);
        contract.loan__Loan_Amount__c = 4000;
        contract.loan__Principal_Paid__c = 2000;
        contract.loan__ACH_Routing_Number__c = '123456789';
        contract.loan__ACH_Account_Number__c = '0987654321';
        contract.loan__ACH_Debit_Amount__c = 50;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(6);
        contract.loan__Principal_Remaining__c = 1000;
        contract.loan__Accrual_Start_Date__c = Date.today().addDays(-196);
        contract.SCRA__c = false;
        contract.Date_of_Settlement__c = Date.today().addDays(-66);
        contract.Settlement__c = 2.0;
        contract.Is_Material_Modification__c = false;
        contract.First_Payment_Missed__c = false;
        contract.First_Payment_Made_after_Due_Date__c = false;
        contract.DMC__c = null;
        contract.loan__Metro2_Account_pmt_history_date__c = Date.today().addDays(-6);
        contract.Asset_Sale_Line__c = null;
        contract.loan__Loan_Product_Name__c = product.Loan_Lending_Product__c;
        update contract;
         
        loan__Loan_account_Due_Details__c bill1 = new loan__Loan_account_Due_Details__c();
        bill1.loan__Payment_Satisfied__c = true;
        bill1.loan__Loan_Account__c = contract.Id;
        bill1.loan__DD_Primary_Flag__c = true;
        insert bill1;

        loan__Loan_account_Due_Details__c bill2 = new loan__Loan_account_Due_Details__c();
        bill2.loan__Payment_Satisfied__c = true;
        bill2.loan__Loan_Account__c = contract.Id;
        bill2.loan__DD_Primary_Flag__c = true;
        insert bill2;

        loan__Loan_account_Due_Details__c bill3 = new loan__Loan_account_Due_Details__c();
        bill3.loan__Payment_Satisfied__c = true;
        bill3.loan__Loan_Account__c = contract.Id;
        bill3.loan__DD_Primary_Flag__c = true;
        insert bill3;

        loan__Loan_account_Due_Details__c bill4 = new loan__Loan_account_Due_Details__c();
        bill4.loan__Payment_Satisfied__c = true;
        bill4.loan__Loan_Account__c = contract.Id;
        bill4.loan__DD_Primary_Flag__c = true;
        insert bill4;

        loan__Loan_account_Due_Details__c bill5 = new loan__Loan_account_Due_Details__c();
        bill5.loan__Payment_Satisfied__c = true;
        bill5.loan__Loan_Account__c = contract.Id;
        bill5.loan__DD_Primary_Flag__c = true;
        insert bill5;

        loan__Loan_account_Due_Details__c bill6 = new loan__Loan_account_Due_Details__c();
        bill6.loan__Payment_Satisfied__c = true;
        bill6.loan__Loan_Account__c = contract.Id;
        bill6.loan__DD_Primary_Flag__c = true;
        insert bill6;
       
        
        return contract;
        
    }
    
    @isTest static void checks_batch() {
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Maximum_Loan_Amount__c = 4500;
        opp.Status__c = 'Credit Qualified';
        Opp.Type = 'Multiloan';
        update opp;
        
        loan__Loan_Account__c contract = creates_contract();
        contract.Opportunity__c = opp.id;
        contract.loan__Loan_Amount__c = 4000;
        update contract;
        
        Multiloan_Params__c params = new Multiloan_Params__c();
        
        
        
        params.contract__c = contract.id;
        params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
        insert params;
		
        Test.startTest();
        BatchMultiLoanBadStanding job = new BatchMultiLoanBadStanding();
        database.executebatch(job, 1);
        Test.stopTest();
        
        
    }
    

}