@isTest public with sharing class OppTriggersTest {
    
    Public testMethod Static Void refiTestOne(){
 		
        loan__Loan_Account__c cl = TestHelper.createContract();
        //cl.Opportunity__C= app.id ;       
        cl.loan__Disbursal_Date__c = System.today();
        cl.loan__Loan_Status__c = 'Active - Good Standing';
        cl.loan__Pmt_Amt_Cur__c = 200;
        cl.Next_Payment_is_Additional_Payment__c = true;
        update cl;
         
        Opportunity app1 = LibraryTest.createApplicationTH();
       
        app1.Contract_Renewed__c = cl.id;
        app1.status__c = 'Refinance Qualified';
        app1.Partner_Account__c = null;
        app1.Same_Employment_as_Funded_Opportunity__c = True;
        app1.Same_Bank_Information_as_Funded__c = True;
        app1.Same_Income_as_Funded_Opportunity__c = True;
        app1.type = 'Refinance'; 
        Test.startTest(); 
        update app1;
        Test.stopTest(); 
         
     }
     
    @isTest static void updates_application() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact cnt = [select id,Kyc_Mode__c from Contact where id=:app.Contact__c];
        cnt.Kyc_Mode__c ='true';
        update cnt;
		app.type='Refinance';
        app.status__c = 'Refinance Qualified';
        update app;
	}
    
    @isTest static void inserts_application() {

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];

        Opportunity app = LibraryTest.createApplicationTH();

        System.assert([SELECT COUNT() FROM Opportunity] > appsBefore);
    }
	
   
    
    @isTest static void updates_application_fico() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact cnt = [select id,Kyc_Mode__c from Contact where id=:app.Contact__c];
        cnt.Kyc_Mode__c ='true';
        update cnt;
        app.status__c = 'Documents In Review';
        app.FICO__c = '730';
        app.Fico_Reviewed__c = false;
        update app;

        System.assertEquals('Credit Review', [SELECT status__c FROM Opportunity WHERE id = : app.id LIMIT 1].status__c);
    }
 

    @isTest static void updates_application_clear() {
        Opportunity app = LibraryTest.createApplicationTH();
        app.status__c = 'Offer Accepted';
        update app;

        //System.assertNotEquals(null, [SELECT Clear_Relevance__c FROM Contact WHERE id =: app.genesis__contact__c LIMIT 1].Clear_Relevance__c);
    }

    @isTest static void updates_application_idology() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact cnt = [select id,Kyc_Mode__c from Contact where id=:app.Contact__c];
        cnt.Kyc_Mode__c ='true';
        update cnt;
        Logging__c testLog = new Logging__c();
        testLog.Webservice__c = 'Kount';
        testLog.api_response__c = '{MOBILE_DEVICE=VMAX=0 MODE=Q IP_CITY= RULE_DESCRIPTION_0=Email Whitelist Approval IP_ORG= TRAN=PM5M0W06W7JV ' +
                'CARDS=1 PC_REMOTE= PIP_COUNTRY= SESS=5E32234FB75F447F064D8AF59DDF6C19 ORDR= FINGERPRINT= COUNTRY= SCOR=31 ' +
                'MASTERCARD= VERS=0630 IP_COUNTRY= BRND=NONE VOICE_DEVICE= TIMEZONE= PIP_REGION= LANGUAGE= PIP_LAT= IP_REGION= ' +
                'DSR= IP_IPAD= DEVICE_LAYERS=.... FLASH= REGION= AUTO=A IP_LON= PIP_IPAD= JAVASCRIPT= BROWSER= IP_LAT= PROXY= ' +
                'WARNING_COUNT=0 UAS= LOCALTIME=  RULES_TRIGGERED=1 RULE_ID_0=772856 EMAILS=1 HTTP_COUNTRY= MOBILE_FORWARDER= ' +
                'PIP_CITY= DDFS= VELO=0 COUNTERS_TRIGGERED=0 NETW=N SITE=DEFAULT COOKIES= OS= PIP_ORG= GEOX=IN REASON_CODE= ' +
                'MERC=146300 PIP_LON= KAPT=N DEVICES=1 REGN=IN_05 MOBILE_TYPE=}';
        testLog.Contact__c = app.contact__c;
        insert testLog;

        IDology_Request__c testIdology = new IDology_Request__c();
        testIdology.Qualifiers__c = '<li>DOB/YOB Not Available</li>';
        testIdology.Contact__c = app.contact__c;
        insert testIdology;

        app.status__c = 'Documents In Review';
        app.FICO__c = '730';
        app.Fico_Reviewed__c = false;
        update app;

        app = [SELECT Review_Reason__c, Status__c FROM Opportunity WHERE id = : app.Id LIMIT 1];

        System.assertEquals('Identify/KYC,Credit Profile', app.Review_Reason__c);
        System.assertEquals('Credit Review', app.status__c);
    }

    @isTest static void updates_application_expected_first_payment() {
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Refinance Qualified';
        app.Selected_Offer_Term__c = 24;
        app.Expected_First_Payment_Date__c = Date.today().addDays(20);
        app.Expected_Start_Date__c = Date.today().addDays(10);
        app.Payment_Amount__c = 300;
        app.Term__c = 24;
        app.Payment_Frequency__c = 'Monthly';
        app.Amount = 5000;
        app.Offer_Code__c = 'HAPPY6';
        update app;

        app = [SELECT Effective_APR__c, Expected_Close_Date__c FROM Opportunity WHERE id = : app.id LIMIT 1];

        System.assertNotEquals(null, app.Effective_APR__c);
        System.assertNotEquals(null, app.Expected_Close_Date__c);

    }
    
    @isTest static void updates_opportunity_fields_override_after_prequal() {
        
        Contact con = LibraryTest.createContactTH();
        con.Promotion_Active__c = true;
        con.Promotion_Bust_Date__c = Date.today().addDays(-2);
        con.Promotion_Expiration_Date__c = Date.today().addDays(20);
        con.Promotion_Grace_Days__c = 10;
        con.Promotion_Grace_Expiration_Date__c = Date.today().addDays(20);
        con.Promotion_Rate__c = 0.20;
        con.Promotion_Term_In_Months__c = 12;
        con.Promotion_Type__c = 'Test Promotion';
        update con;
        
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Contact__c = con.Id;
        app.Status__c = 'Refinance Qualified';
        app.Selected_Offer_Term__c = 24;
        app.Expected_First_Payment_Date__c = Date.today().addDays(20);
        app.Expected_Start_Date__c = Date.today().addDays(10);
        app.Payment_Amount__c = 300;
        app.Term__c = 24;
        app.Payment_Frequency__c = 'Monthly';
        app.Amount = 5000;
        app.Offer_Code__c = 'HAPPY6';
        update app;

        app = [SELECT Promotion_Active__c, Promotion_Rate__c FROM Opportunity WHERE id = : app.id LIMIT 1];
        con = [Select Promotion_Active__c, Promotion_Rate__c FROM Contact where Id=: con.Id];

        System.assertEquals(con.Promotion_Active__c, app.Promotion_Active__c);
        System.assertEquals(con.Promotion_Rate__c, app.Promotion_Rate__c);

    }
    
    @isTest static void updatesOpp_Case_Status() {
        
        Contact con = LibraryTest.createContactTH();
        con.Promotion_Active__c = true;
        con.Promotion_Bust_Date__c = Date.today().addDays(-2);
        con.Promotion_Expiration_Date__c = Date.today().addDays(20);
        con.Promotion_Grace_Days__c = 10;
        con.Promotion_Grace_Expiration_Date__c = Date.today().addDays(20);
        con.Promotion_Rate__c = 0.20;
        con.Promotion_Term_In_Months__c = 12;
        con.Promotion_Type__c = 'Test Promotion';
        update con;
        
        Opportunity app = LibraryTest.createApplicationTH();
        
        Case cs = New Case();
        cs.subject = 'Documents Have Been Uploaded from Web Test: ' + app.Name;
        cs.Description = 'Test Description New case was created';
        cs.Opportunity__c = app.Id;
        cs.ContactId = con.id;
        insert cs;
        
        app.Contact__c = con.Id;
        app.Status__c = 'Approved, Pending Funding';
        app.Deal_is_Ready_to_Fund__c = true;
        app.Selected_Offer_Term__c = 24;
        app.Expected_First_Payment_Date__c = Date.today().addDays(20);
        app.Expected_Start_Date__c = Date.today().addDays(10);
        app.Payment_Amount__c = 300;
        app.Term__c = 24;
        app.Payment_Frequency__c = 'Monthly';
        app.Amount = 5000;
        app.Offer_Code__c = 'HAPPY6';
        update app;
        
        cs = [SELECT Status FROM Case WHERE id = : cs.id LIMIT 1];
        
        System.assertEquals('Closed', cs.Status);
       
    }
    
    @isTest static void updates_Case_Status_NewOpp() {
        
        Contact con = LibraryTest.createContactTH();
        con.Promotion_Active__c = true;
        con.Promotion_Bust_Date__c = Date.today().addDays(-2);
        con.Promotion_Expiration_Date__c = Date.today().addDays(20);
        con.Promotion_Grace_Days__c = 10;
        con.Promotion_Grace_Expiration_Date__c = Date.today().addDays(20);
        con.Promotion_Rate__c = 0.20;
        con.Promotion_Term_In_Months__c = 12;
        con.Promotion_Type__c = 'Test Promotion';
        update con;
        
        Opportunity app = New Opportunity();
        app.Contact__c = con.Id;
        
        Case cs = New Case();
        cs.ContactId = con.id;
        cs.Opportunity__c = app.Id;
        cs.subject = 'Documents Have Been Uploaded from Web Test: ' + app.Name;
        cs.Description = 'Test Description New case was created';
        insert cs;
        
        app.CloseDate = Date.today().addDays(10);
        app.Status__c = 'Credit Qualified';
        app.Deal_is_Ready_to_Fund__c = true;
        app.Selected_Offer_Term__c = 24;
        app.Expected_First_Payment_Date__c = Date.today().addDays(20);
        app.Expected_Start_Date__c = Date.today().addDays(10);
        app.Payment_Amount__c = 300;
        app.Term__c = 24;
        app.Payment_Frequency__c = 'Monthly';
        app.Amount = 5000;
        app.Offer_Code__c = 'HAPPY6';
        insert app;
        
        app.Status__c = 'Approved, Pending Funding';
        update app;
       
        cs = [SELECT Status, ContactId, Opportunity__c FROM Case WHERE id = : cs.id LIMIT 1];
        
        System.assertEquals('Closed', cs.Status);
       
    }

}