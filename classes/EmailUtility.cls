public class EmailUtility {
    String auth, EndPoint;
    static integer errorCount = 0;
  
  /**This method is a wrapper to call Java Apis to get Access Token and to Post emails to Java Customer Portal DB.
     It will also return all the Logging records to Schedulable class EmailAutomatedJob to insert them in the SFDC DB at the end of all the Http requests **/
          
     Public static list<Logging__c> sendEmailDetailNew(string Subject, String emails){
       // emails = '{"contactId": "456","from": "sample@sample.com","to": "sample2@sample.com","subject": "Sample Subject","body": "Sample Body","date": "10/22/2015 10:15:00 AM"}';
        list<Logging__c>  finalLogRecords;
        Java_API_Settings__c javaDetails = Java_API_Settings__c.getInstance('JavaSettings');                                
        string mailReceiveEndPoint = javaDetails.Mail_Receive_EndPoint__c;
        
        try{                             
            string accessToken = hitAccessTokenAPI(javaDetails);
            System.debug('accessToken======================= '+accessToken);  
            if(accessToken <> null)
              finalLogRecords = hitMailRecieveAPI(accessToken,emails,mailReceiveEndPoint,Subject);   
        }
        catch(Exception e){
            System.debug('Exception================ '+ e.getMessage());
            WebToSFDC.notifyDev('sendEmailDetailNew hits Customer Portal ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() ) ;
        } 
        return finalLogRecords;  
    } 


    
  /**This method is to post the SFDC mails to Java Customer Portal DB.**/
  @TestVisible
    private static list<Logging__c>  hitMailRecieveAPI(String accessToken,string emailDetails,string endPoint,string logSubject){
        list<Logging__c> logingRecords = new list<Logging__c>();
        list<mailResponses> mailResponsesList =  new list<mailResponses>();
        
        try{
            System.debug('log size======================= '+logingRecords.size());     
            HttpRequest req = new HttpRequest(); 
            req.setEndPoint(EndPoint);         
            req.setHeader('Authorization', 'Bearer '+accessToken);
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Accept', 'application/json');
            req.setHeader('Origin', Label.Origin_Java);
            req.setMethod('POST');            
            req.setBody(emailDetails);
            req.setTimeout(100000); 
            System.debug('Request ==============='+req);
            Http sendHttp = new Http();
            HTTPResponse res;
            if(!Test.isRunningTest()){
                res = sendHttp.send(Req); 
                
            }else{
                res = new HTTPResponse();                    
                res .SetBody(TestHelper.createDomDocNew().toXmlString());               
            }
            System.debug('Response======================= '+res.getBody());                       
            integer statuscode = res.getstatusCode();
            
            
            //==================
            
            JSONParser parser = JSON.createParser(res.getBody());
            System.debug('parser'+parser);
            while (parser.nextToken() != null) {
               if (parser.getCurrentToken() == JSONToken.FIELD_NAME){
                String fieldName = parser.getText();
                parser.nextToken();

                   if(fieldName == 'data'){
                     if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                        while(parser.nextToken() != null){
                            if(parser.getCurrentToken() == JSONToken.START_OBJECT){                              
                                mailResponses responses = (mailResponses) parser.readValueAs(mailResponses.class);
                                mailResponsesList.add(responses);
                            }
                            else if(parser.getCurrentToken() == JSONToken.END_ARRAY){
                                break; 
                            }
                        }
                      }
                    }
                  } 
              }
                            
              for(mailResponses resp: mailResponsesList){
                  if(resp.status == 'ERR'){                                  
                      logingRecords.add(getLoggingRecord('Java Email Receive Error',logSubject,resp.message,resp.contactId));
                  
                  }else if(resp.status == 'VER'){                 
                      logingRecords.add(getLoggingRecord('Java Email Receive Validation Error',logSubject,JSON.SerializePretty(resp.errors),resp.contactId));                  
                  }              
              }
            //===================
            System.debug('mailResponsesList======================= '+mailResponsesList);
          
        }catch(Exception e ){
            System.debug('Exception================ '+ e.getMessage()+' at'+ e.getlinenumber());
            WebToSFDC.notifyDev('hitMailRecieveAPI hits Customer Portal ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() ) ;
        }  
        System.debug('log size======================= '+logingRecords.size()); 
        return logingRecords; 
    } 
   /** This method is to generate Access Token to hit Java APIs via Oauth Process**/ 
   public static string hitAccessTokenAPI(Java_API_Settings__c javaSettingDetails){
       string res = getAccessToken(javaSettingDetails,false);
       return res;
   }
     
    public static string getAccessToken(Java_API_Settings__c javaSettingDetails, boolean isMerchant){
        Java_API_Settings__c javaDetails;
        if(isMerchant)
            javaDetails = Java_API_Settings__c.getInstance('MerchantSettings');
        else
            javaDetails = Java_API_Settings__c.getInstance('JavaSettings');
                           
        string clientId     = javaSettingDetails.clientId__c, clientSecret = javaSettingDetails.clientSecret__c; 
        string EndPoint     = javaSettingDetails.Access_Token_EndPoint__c, userName     = javaSettingDetails.Username__c;
        string password     = javaSettingDetails.Password__c, accessTokenVal;
        
        try{
        
           Blob headerValue = Blob.valueOf(clientId + ':' + clientSecret);

           String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
           system.debug('=authorizationHeader='+authorizationHeader);
                
            HttpRequest req = new HttpRequest(); 
            req.setEndPoint(EndPoint);         
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            req.setHeader('Origin', Label.Origin_Java);
            req.setMethod('POST');           
            req.setTimeout(100000);
 
            req.setBody('username='+userName+'&password='+password);
             
            System.debug('Request ==============='+req);
            Http sendHttp = new Http();
            HTTPResponse Resp;
            
             if(!Test.isRunningTest()){
                Resp = sendHttp.send(Req); 
                
            }else{
                Resp = new HTTPResponse();                    
                Resp.SetBody(TestHelper.createDomDocNew().toXmlString());               
            }
            
            
            System.debug('Response======================= '+resp.getBody());
            System.debug('status code======================= '+resp.getStatusCode());
            
            XmlStreamReader reader = Resp.getXmlStreamReader();
            
            accessTokenVal = getAccessTokenVal(reader);
            System.debug('accessTokenVal======================= '+accessTokenVal);             
            
         }catch(Exception e ){
            System.debug('Exception================ '+ e.getMessage());
            WebToSFDC.notifyDev('hitAccessTokenAPI API Customer Portal ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + '\n' + EndPoint ) ;
         }   
           
          return accessTokenVal;    
    } 
    
     /** This method is to fetch Access Token Value by parsing XMLStreamReader**/
    @TestVisible
     private static string getAccessTokenVal(XmlStreamReader  xmlReader){
                                         
        system.debug('===xmlReader===================='+xmlReader);                
        
        String PrevEvent;                
                            
        while(xmlReader.hasNext()) {                    
            if(PrevEvent == 'access_token'  && (xmlReader.getEventType() == XmlTag.CHARACTERS)){
                return xmlReader.GetText();                                   
            }
            
            if (xmlReader.getEventType() == XmlTag.START_ELEMENT) {              
                PrevEvent = xmlReader.getLocalName();
            }
            
            xmlReader.next();
        }        
        return null;
    } 
     
    /** This method will create Logging Record for Java API Errors/Validation errors**/    
    public static Logging__c getLoggingRecord(string webServiceName,string request, string response, string contactid){
        return  new Logging__c(Webservice__c   = webServiceName, API_Request__c  = request, API_Response__c = response, Contact__c = contactid);
    }
    
     /** This method will create Logging Record for Java API Errors/Validation errors  and associate to account**/    
    public static Logging__c getLoggingRecordAcc(string webServiceName,string request, string response, string accountid){
        return  new Logging__c(Webservice__c   = webServiceName, API_Request__c  = request, API_Response__c = response, Account__c = accountid);
    }  
    
    public class mailResponses{
       string contactId, recordId, status, message;           
       map<string,string> errors;
    } 
     
}