@isTest public class ClearInputParameterTest {

    @isTest static void validates(){
        
        Contact c = LibraryTest.createContactTH();
        
        ClearInputParameter cip = new ClearInputParameter();
        
        cip.contactId = c.id;
        cip.phone = '1234567890';
        cip.address = new ClearAddress();
        
        System.assertEquals(c.id, cip.contactId);
        System.assertEquals('1234567890', cip.phone);
        
    }
    
}