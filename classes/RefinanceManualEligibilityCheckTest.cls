@isTest
private class RefinanceManualEligibilityCheckTest
{
	@isTest static void runs_manual_eligibility_check()
	{	
		loan__Loan_Account__c testContract = LibraryTest.createContractTH();
        
        Contact c = [SELECT Id FROM Contact WHERE Id =: testContract.loan__Contact__c];

        List<loan_Refinance_Params__c> refiParams = [SELECT  Id, 
                                                                Eligible_For_Soft_Pull__c,
                                                                ManualEligibilityCheck__c
                                                        FROM    loan_Refinance_Params__c 
                                                        WHERE   Contract__c =: testContract.id 
                                                        ORDER BY CreatedDate DESC 
                                                        LIMIT 1];
        String result = '';
        if (refiParams.size() > 0){
        	result = RefinanceManualEligibilityCheck.doRefinance(c.Id);
        	System.assertEquals('This customer is NOT eligible for a refinance at this time.', result);
        }
        else{
            result = RefinanceManualEligibilityCheck.doRefinance(c.Id);
        	System.assertEquals('The current contract does not have any Refinance parameters.', result);
            
            loan_Refinance_Params__c refi = new loan_Refinance_Params__c();
            refi.Eligible_For_Soft_Pull__c = false;
            refi.ManualEligibilityCheck__c = false;
            refi.Contract__c = testContract.id;
            refi.FICO__c = 710;
            refi.Grade__c = 'B1';
            refi.ValidatedIncome__c = 80000;
            refi.ContactAddress__c = 'Test';
            insert refi;
            
            result = RefinanceManualEligibilityCheck.doRefinance(c.Id);
            System.assertEquals('This customer is NOT eligible for a refinance at this time.', result);
        }
	}
}