public class PaymentHandler {
    public static void onUpdate(List<loan__Loan_Payment_Transaction__c> newRecs, List<loan__Loan_Payment_Transaction__c> oldRecs ) {
        for (loan__Loan_Payment_Transaction__c pmt : newRecs){
            if (pmt.Payment_Pending_End_Date__c != null && pmt.loan__Reversed__c )  pmt.Payment_Pending_End_Date__c = null;
            if (pmt.loan__Reversed__c )  pmt.Payment_Pending__c = 'Bounced';

        }              
    }
    public static void onInsert(List<loan__Loan_Payment_Transaction__c> newRecs ) {
		    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        	Boolean bAddDayPayOff = cuSettings != null && cuSettings.Add_Day_Interest_to_Pay_Off__c ;
			Boolean bAddDayAll = cuSettings != null && cuSettings.Add_Day_of_Interest__c ;
        	String newPaymentType = '';
		    
        	loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();
            System.debug('acha pars = ' + acha);
            // This code will auto append Fees for ACH transactions that will be generated via the File Generation. Only ACHs 
            // for Active (never written off) contracts
            Set<ID> setFees = new Set<Id>(), allLoans = new Set<Id>();     
            //===SM-762==
            List<loan__Loan_Account__c> loanToUpdate= new List<loan__Loan_Account__c>();
            loan__Payment_Mode__c achMode , checkMode ;



            for (loan__Payment_Mode__c pm : [select Id, NAme from loan__Payment_Mode__c where Name IN ('ACH', 'Check') order by CreatedDate desc ]) {
                if (pm.Name == 'ACH' && achMode == null) achMode = pm;
                if (pm.Name == 'Check' && checkMode == null) checkMode = pm;
            }
            
            for (loan__Loan_Payment_Transaction__c pmt : newRecs){
                allLoans.add(pmt.loan__Loan_Account__c);
            }

            for (String theId : (String.isNotEmpty(acha.Fees_To_Add_to_Payment__c) ? acha.Fees_To_Add_to_Payment__c : '').split(',')){
                if ( String.isNotEmpty(theId)) setFees.add(theId);
            }


            Map<Id, loan__Loan_Account__C> allcons = new Map<Id,loan__Loan_Account__c>([Select Id, loan__Pay_Off_Amount_As_Of_Today__c, Asset_Sale_Line__c, loan__Contact__c, loan__Interest_rate__c, loan__Principal_Remaining__c, loan__Number_of_Days_Overdue__c, Next_Payment_is_Additional_Payment__c, loan__Loan_Status__c, loan__Time_Counting_Method__c, (Select Id,loan__Loan_Account__c, loan__Principal_Due__c, loan__Waive__c, loan__Charge_Type__c, loan__Fee__c from loan__Charges__r where loan__Loan_Account__r.loan__Charged_Off_Date__c = null and loan__Waive__c = false and loan__Principal_Due__c > 0 and  loan__Fee__c in: setFees and CreatedDate >: acha.Append_Fees_Created_After__c) from loan__Loan_Account__c where Id in:  allLoans]);
            List<loan_Plan_Payment__c> lp = [Select id, ACH_Debit_Date__c, Name from loan_Plan_Payment__c where Loan_Account__c in: allLoans AND ACH_Debit_Date__c <> NULL ];
            for (loan__Loan_Payment_Transaction__c pmt : newRecs){
                loan__Loan_Account__c acc = allcons.get(pmt.loan__Loan_Account__c);
                Integer AccrualPeriod = acc.loan__Time_Counting_Method__c == clcommon.CLConstants.TIME_COUNTING_ACTUAL_360 ? 360 : acc.loan__Time_Counting_Method__c == loan.LoanConstants.TIME_COUNTING_ACTUAL_DAYS  ? 365 : 360 ;
                decimal perDiem = acc.loan__Interest_Rate__c != null ? ((acc.loan__Interest_Rate__c / 100) / AccrualPeriod) * acc.loan__Principal_Remaining__c : 0;
                if (pmt.loan__Payment_Mode__c == achMode.Id || pmt.loan__Payment_Mode__c == checkMode.id) {
                    if (pmt.loan__Payment_Mode__c == achMode.Id ) {
                        if (acc.loan__Charges__r != null) {
                            for (loan__Charge__c fee : acc.loan__Charges__r) {
                                pmt.loan__Transaction_Amount__c += fee.loan__Principal_Due__c;
                            }
                        }
                        if (acc.loan__Pay_Off_Amount_As_Of_Today__c + perDiem < pmt.loan__Transaction_Amount__c && acha.Cap_ACH_Payments_To_Payoff__c ) pmt.loan__Transaction_Amount__c = acc.loan__Pay_Off_Amount_As_Of_Today__c + perDiem;
                        for (loan_Plan_Payment__c plan : lp) {
                            if (pmt.loan__Transaction_Date__c != null && pmt.loan__Transaction_Date__c == plan.ACH_Debit_Date__c) {
                                pmt.Type__c = 'Payment Plan';
                                pmt.Original_ID__c = plan.Name;
                                
                            }
                        }
                    }

                    if (pmt.loan__Transaction_Date__c > Date.today().addDays(-7) && acc.loan__Number_of_Days_Overdue__c > 0 || acc.loan__Loan_Status__c == 'Closed- Written Off') pmt.Payment_Pending_End_Date__c = OfferCtrlUtil.nextBusinessDate(Date.today() , acha.Payment_Pending_Duration__c.intValue());
                    if (pmt.Payment_Pending_End_Date__c != null) pmt.Payment_Pending__c = 'Pending';
                    if(acc.Next_Payment_is_Additional_Payment__c){
                        acc.Next_Payment_is_Additional_Payment__c = false;
                        pmt.loan__Loan_Payment_Spread__c= Label.additional_payment_spread;
                        loan__Loan_Account__C loanAcc= new loan__Loan_Account__C(id=acc.id, Next_Payment_is_Additional_Payment__c=false);
                        loanToUpdate.add(loanAcc);
                    } 
                }
                
                if (pmt.loan__Transaction_Date__c != null && pmt.loan__Transaction_Amount__c > 0.05 && ((bAddDayPayOff && acc.loan__Pay_Off_Amount_As_Of_Today__c <= pmt.loan__Transaction_Amount__c) || bAddDayAll) ) {
                    pmt.loan__Transaction_Date__c = pmt.loan__Transaction_Date__c.addDays(1);
                    pmt.loan__Skip_Validation__c = true;
                }
                
                pmt.Asset_Line_at_Payment__c = allcons.get(pmt.loan__Loan_Account__c).Asset_Sale_Line__c;          
                pmt.Customer_Name__c = allcons.get(pmt.loan__Loan_Account__c).loan__Contact__c;          
                if (String.isEmpty(pmt.Payment_Pending__c)) pmt.Payment_Pending__c = 'Cleared';
                
                newPaymentType = getNewPaymentType(pmt);
        		pmt.Type__c = String.isNotBlank(newPaymentType) ? newPaymentType : pmt.Type__c;
            }
            
            System.debug('PaymentHandeler loanToUpdate = ' + loanToUpdate.size());
            if(!loanToUpdate.isEmpty()) update loanToUpdate;
    }
    
    public static String getNewPaymentType(loan__Loan_Payment_Transaction__c payment){
        String paymentType = '';
        for (loan__Loan_Account__c contract : [SELECT loan__Oldest_Due_Date__c, loan__Charged_Off_Date__c, Date_of_Settlement__c, 
                                               Overdue_Days__c, loan__Pay_Off_Amount_As_Of_Today__c, loan__Pmt_Amt_Cur__c 
                                               FROM loan__Loan_Account__c 
                                               WHERE Id =: payment.loan__Loan_Account__c]){

            if(payment.loan__Transaction_Amount__c >= (contract.loan__Pay_Off_Amount_As_Of_Today__c * 0.95)){   
                paymentType = 'Payoff Payment'; 
            }else{
                if(contract.Overdue_Days__c > 0 && contract.loan__Charged_Off_Date__c == null && contract.Date_of_Settlement__c == null){
                    if(payment.loan__Transaction_Amount__c == contract.loan__Pmt_Amt_Cur__c && payment.loan__Transaction_Date__c > contract.loan__Oldest_Due_Date__c){
                        paymentType = payment.Re_Submitted__c ? 'Regular Payment – Auto-Resubmitted' : 'Regular Payment - Late';
                    }
                }else{
                    if(contract.Date_of_Settlement__c != null  && payment.loan__Transaction_Date__c > contract.Date_of_Settlement__c){
                        paymentType = 'Settlement Payment';
                    }else{
                        if((contract.loan__Charged_Off_Date__c != null ) && (payment.loan__Transaction_Date__c > contract.loan__Charged_Off_Date__c) && (contract.Date_of_Settlement__c == null )){
                            paymentType = 'Recovery Payment';
                        }else{
                            if(contract.Overdue_Days__c == 0 && payment.loan__Transaction_Date__c != contract.loan__Oldest_Due_Date__c && payment.loan__Loan_Payment_Spread__c == Label.Recovery_Loans_Payment_Spread){
                                paymentType = 'Additional Payment';
                            }else{
                                if((payment.loan__Transaction_Amount__c == contract.loan__Pmt_Amt_Cur__c) && payment.loan__Transaction_Date__c <= contract.loan__Oldest_Due_Date__c){
                                    paymentType = 'Regular Payment';
                                }else{
                                    if((payment.loan__Transaction_Amount__c > contract.loan__Pmt_Amt_Cur__c) && (payment.loan__Transaction_Date__c <= contract.loan__Oldest_Due_Date__c)){
                                        paymentType = 'Regular Payment - Plus';
                                    }
                                }
                            }   
                        }
                    }
                }
            }
        }
        return paymentType;
    }
}