public class RefinanceHandler {
    
    public static Map<String, Opportunity> refinaceCopyRec(List<opportunity> newRec, Map<ID,Opportunity> oldRec){
        Map<String, Opportunity> mapfinalAppUpd = new Map<String, Opportunity>();
        id ids;
        for(opportunity app:newRec){
            opportunity oldApp = oldRec.get(app.id);
            system.Debug('oldapp'+oldApp.status__c);
            
            IF( (app.type =='Refinance' && !String.isEmpty(app.Contract_Renewed__c))&&(
                oldApp.Same_Employment_as_Funded_Opportunity__c!=app.Same_Employment_as_Funded_Opportunity__c||   
                oldApp.Same_Bank_Information_as_Funded__c!=app.Same_Bank_Information_as_Funded__c||
                oldapp.Same_Income_as_Funded_Opportunity__c !=app.Same_Income_as_Funded_Opportunity__c
            )) {
                
                List<loan__loan_account__c> loanOppId = [select id, opportunity__c from loan__loan_account__c where id =:app.Contract_Renewed__c limit 1];
                if(!loanOppId.isempty()){ 
                    ids = loanOppId[0].opportunity__c ;
                }
                
                if(oldApp.Same_Employment_as_Funded_Opportunity__c!=app.Same_Employment_as_Funded_Opportunity__c)
                {
                    if(app.Same_Employment_as_Funded_Opportunity__c==True){
                        Map<String, Schema.SobjectField> MeetingFields = Schema.SObjectType.Employment_Details__c.fields.getMap();
                        List<String> fieldNames = new List<String>();
                        for( String fieldName : MeetingFields.keySet() ) {
                            fieldNames.add( fieldName );
                            // For relationship fields
                            //if( Schema.DisplayType.Reference == mapFields.get(fieldName).getDescribe().getType() )
                            //fieldNames.add(mapFields.get(fieldName).getDescribe().getRelationshipName() + '.Name' ); 
                        } 
                        
                        //system.debug('lon ' + lonOppId.opportunity__c);  
                        String query = 
                            ' SELECT ' + 
                            String.join( fieldNames, ',' ) + 
                            ' FROM Employment_Details__c ' +
                            ' WHERE ' + 
                            ' opportunity__c =:ids order by createddate desc Limit 1';
                        
                        for(Employment_Details__c lstRecords : Database.query(query)){
                            Employment_Details__c newemp = lstRecords.clone();
                            newemp.opportunity__c = app.id;
                            upsert newemp;    
                        }
                        
                    }
                    
                    if(app.Same_Employment_as_Funded_Opportunity__c==false){
                        delete [select id from Employment_Details__c where opportunity__c=:app.id order by createddate desc limit 1];
                    }
                    
                }
                if(oldapp.Same_Income_as_Funded_Opportunity__c !=app.Same_Income_as_Funded_Opportunity__c)
                {     
                    
                    if(app.Same_Income_as_Funded_Opportunity__c==True){
                        opportunity fundedOppInc = [select id, Borrower_ID_State__c, Borrower_ID_Number__c,All_Applicants_XML__c,Employed_Applicants_1_XML__c,
                                                    Retired_Applicants_XML__c,Self_Employed_Applicants_1_XML__c,Self_Employed_Applicants_2_XML__c,
                                                    Miscellaneous_Applicants_XML__c,Employed_Applicants_2_XML__c
                                                    from opportunity where id=:ids limit 1];                               
                        if(fundedOppInc!=null){
                            app.Borrower_ID_State__c = fundedOppInc.Borrower_ID_State__c;
                            app.Borrower_ID_Number__c = fundedOppInc.Borrower_ID_Number__c;
                            app.All_Applicants_XML__c  = fundedOppInc.All_Applicants_XML__c;   
                            app.Employed_Applicants_1_XML__c = fundedOppInc.Employed_Applicants_1_XML__c;
                            app.Employed_Applicants_2_XML__c = fundedOppInc.Employed_Applicants_2_XML__c;
                            app.Retired_Applicants_XML__c = fundedOppInc.Retired_Applicants_XML__c;
                            app.Self_Employed_Applicants_1_XML__c = fundedOppInc.Self_Employed_Applicants_1_XML__c;
                            app.Self_Employed_Applicants_2_XML__c = fundedOppInc.Self_Employed_Applicants_2_XML__c;
                            app.Miscellaneous_Applicants_XML__c = fundedOppInc.Miscellaneous_Applicants_XML__c;
                        }
                        If([select count() from attachment where parentid=:app.id and (Not(Name like '%Do Not Open%')) and (not(name Like '%Signed Contract%'))]==0){
                            
                            list<attachment> lstInsertAtt = new List<attachment>();
                            for(attachment attFundedOpp :[select id ,name,body, parentId from attachment where parentid=:ids and (Not(Name like '%Do Not Open%')) and (not(name Like '%Signed Contract%'))])
                            {                       
                                attachment newAtt = attFundedOpp.clone();
                                newatt.parentId = app.id;
                                lstInsertAtt.add(newAtt);
                            }
                            system.debug('lstInsertAtt ' + lstInsertAtt );
                            insert lstInsertAtt;     
                        } 
                        
                    }
                    if(app.Same_Income_as_Funded_Opportunity__c==False){
                        app.Borrower_ID_State__c = null;
                        app.Borrower_ID_Number__c = null;
                        app.All_Applicants_XML__c  = null;   
                        app.Employed_Applicants_1_XML__c = null;
                        app.Employed_Applicants_2_XML__c = null;
                        app.Retired_Applicants_XML__c = null;
                        app.Self_Employed_Applicants_1_XML__c =null;
                        app.Self_Employed_Applicants_2_XML__c = null;
                        app.Miscellaneous_Applicants_XML__c =null;
                        
                    }
                }
                if(oldApp.Same_Bank_Information_as_Funded__c!=app.Same_Bank_Information_as_Funded__c)
                { 
                    if(app.Same_Bank_Information_as_Funded__c==True){ 
                        Opportunity fundedOpp = [select ACH_Account_Type__c,
                                                 ACH_Account_Number__c,
                                                 ACH_Routing_Number__c,
                                                 ACH_Bank_Name__c,
                                                 Verified_Checklist__c,
                                                 Invitation_Link__c,
                                                 ACH_Member_Account_Number__c,
                                                 Others__c,
                                                 DE_Assigned_To_ACH__c,
                                                 DL_Account_Number_Found__c,
                                                 Contact__r.Annual_Income__c, //contact
                                                 Estimated_Gross_Income__c,
                                                 Gross_Percentage_To_Declared__c, 
                                                 Decision_Logic_Result__c,
                                                 DL_Available_Balance__c,
                                                 DL_As_of_Date__c, 
                                                 DL_Current_Balance__c,
                                                 DL_Average_Balance__c, 
                                                 DL_Deposits_Credits__c, 
                                                 DL_Avg_Bal_Latest_Month__c,
                                                 DL_Withdrawals_Debits__c, 
                                                 Number_of_Negative_Days_Number__c,
                                                 Decision_Logic_Income__c,
                                                 Income_Verification_Source__c,
                                                 Clarity_Verified_Income__c from opportunity where id =:ids limit 1]; 
                        
                        if (fundedOpp!=null){
                            app.ACH_Routing_Number__c = fundedOpp.ACH_Routing_Number__c;
                            app.ACH_Account_Type__c = fundedOpp.ACH_Account_Type__c;
                            app.ACH_Account_Number__c = fundedOpp.ACH_Account_Number__c;
                            app.ACH_Bank_Name__c =  fundedOpp.ACH_Bank_Name__c;
                            app.Clarity_Verified_Income__c = fundedOpp.Clarity_Verified_Income__c;
                            app.Verified_Checklist__c = fundedOpp.Verified_Checklist__c;
                            app.Invitation_Link__c = fundedOpp.Invitation_Link__c;
                            app.ACH_Member_Account_Number__c  = fundedOpp.ACH_Member_Account_Number__c;
                            app.Others__c = fundedOpp.Others__c;
                            app.DE_Assigned_To_ACH__c = fundedOpp.DE_Assigned_To_ACH__c;
                            app.DL_Account_Number_Found__c = fundedOpp.DL_Account_Number_Found__c;
                            app.Estimated_Gross_Income__c = fundedOpp.Estimated_Gross_Income__c;
                            app.Gross_Percentage_To_Declared__c = fundedOpp.Gross_Percentage_To_Declared__c;
                            app.Decision_Logic_Result__c = fundedOpp.Decision_Logic_Result__c;
                            app.DL_Available_Balance__c = fundedOpp.DL_Available_Balance__c;
                            app.DL_As_of_Date__c = fundedOpp.DL_As_of_Date__c;
                            app.DL_Current_Balance__c = fundedOpp.DL_Current_Balance__c;
                            app.DL_Average_Balance__c = fundedOpp.DL_Average_Balance__c;
                            app.DL_Deposits_Credits__c = fundedOpp.DL_Deposits_Credits__c;
                            app.DL_Avg_Bal_Latest_Month__c = fundedOpp.DL_Avg_Bal_Latest_Month__c;
                            app.DL_Withdrawals_Debits__c = fundedOpp.DL_Withdrawals_Debits__c;
                            app.Number_of_Negative_Days_Number__c = fundedOpp.Number_of_Negative_Days_Number__c;
                            app.Decision_Logic_Income__c = fundedOpp.Decision_Logic_Income__c;
                            app.Income_Verification_Source__c = fundedOpp.Income_Verification_Source__c;
                            
                            system.debug('app  '+ app);
                        }
                        
                    }
                    if(app.Same_Bank_Information_as_Funded__c==False ||Test.isRunningTest())
                    {
                        app.ACH_Routing_Number__c = null;
                        app.ACH_Account_Type__c = null;
                        app.ACH_Account_Number__c = null;
                        app.ACH_Bank_Name__c = null;
                        app.Clarity_Verified_Income__c = null;
                        app.Verified_Checklist__c =null;
                        app.Invitation_Link__c =null;
                        app.ACH_Member_Account_Number__c  =null;
                        app.Others__c =null;
                        app.DE_Assigned_To_ACH__c = null;
                        app.DL_Account_Number_Found__c = null;
                        app.Estimated_Gross_Income__c = null;
                        app.Gross_Percentage_To_Declared__c =null;
                        app.Decision_Logic_Result__c = null;
                        app.DL_Available_Balance__c =null;
                        app.DL_As_of_Date__c = null;
                        app.DL_Current_Balance__c = null;
                        app.DL_Average_Balance__c = null;
                        app.DL_Deposits_Credits__c =null;
                        app.DL_Avg_Bal_Latest_Month__c = null;
                        app.DL_Withdrawals_Debits__c = null;
                        app.Number_of_Negative_Days_Number__c =null;
                        app.Decision_Logic_Income__c = null;
                        app.Income_Verification_Source__c = null;
                        system.debug('app  '+ app);
                    }    
                }
                
                if(app.Same_Income_as_Funded_Opportunity__c==True &&  app.Same_Bank_Information_as_Funded__c==True 
                   && app.Same_Employment_as_Funded_Opportunity__c==True)
                {
                    app.status__c = 'Contract Pkg Sent';
                }
                else {
                    if(app.Status__c =='Contract Pkg Sent'){
                        List<OpportunityFieldHistory> oldStatus = [Select  NewValue,oldvalue From OpportunityFieldHistory Where Field in ('Status__c') and OpportunityId =:app.id order by createddate DESC limit 1 ];
                        app.Status__c = string.valueOf(oldStatus[0].oldvalue);
                    }
                }
                mapfinalAppUpd.put(app.id,app); 
            }
            
        }
        return mapfinalAppUpd;
        
    }
    
}