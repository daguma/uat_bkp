public class CreateLoanAccountForLoanTypeHandler {
    loan__Loan_Product__c loanProduct = new loan__Loan_Product__c();
    loan__Loan_Account__c loanAccount = new loan__Loan_Account__c();

    Opportunity application = new Opportunity();
    private loan.GlobalLoanUtilFacade loanUtil;
    private Date systemDate;
    public CreateLoanAccountForLoanTypeHandler() {}
    public CreateLoanAccountForLoanTypeHandler(Opportunity application , loan__Loan_Account__c loanAccount, loan__Loan_Product__c loanProduct) {
        this.application = application;
        this.loanAccount = loanAccount;
        this.loanProduct = loanProduct;
        system.debug('=loanAccount =' + this.loanAccount );
        system.debug('=loanProduct=' + this.loanProduct );
        loanUtil = new loan.GlobalLoanUtilFacade();
        systemDate = loanUtil.getCurrentSystemDate();
    }
    private loan__Payment_Mode__c pmtMode = new loan__Payment_Mode__c();
    public String createLoanAccount() {
        boolean bIsSandBox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        ezVerify_Settings__c EZVerifySettings = CustomSettings.getEZVerifyCustom();
        loan__Org_Parameters__c org = loan.CustomSettingsUtil.getOrgParameters();
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();

        system.debug('EZVerifySettings=' + EZVerifySettings);
        Savepoint sp = Database.setSavepoint();
        system.debug('=cuSettings=' + cuSettings );
        Contact co = application.Contact__r;

        Integer hour = acha.Disbursal_File_Cutoff_Hour__c == null ? 24 : acha.Disbursal_File_Cutoff_Hour__c.intValue();
        Time newT = Time.newInstance(hour, 0, 0, 0);
        Integer dow = Date.today().toStartOfWeek().daysBetween(Date.today()); 
        DateTime cutOffDate = DateTime.newInstance(Date.today(), newT);
        system.debug('= cutOffDate = ' + cutOffDate);
        Decimal estIntStart;
        try{
            estIntStart = application.Partner_Account__r.Est_Interest_Start_Grace_Period__c;
        }
        catch(exception e){ //do nothing
        }
        if(String.ISBlank(String.valueOf(estIntStart))){
            estIntStart = 0;
        }
        loanAccount.loan__Payment_Tolerance_Rate__c = org.loan__Default_Tolerance_Rate__c;
        loanAccount.FEB_Originated__c = application.Is_FEB__c;
        loanAccount.Finwise_Originated__c = application.Finwise_Approved__c;
        loanAccount.Reporting_Fee__c = application.Fee__c;
        loanAccount.Reporting_Fee_Balance__c = application.Fee__c;
        loanAccount.Original_Partner_Account__c = application.Partner_Account__c;
        loanAccount.Original_Sub_Source__c = application.Lead_Sub_Source__c;        
        loanAccount.loan__Frequency_of_Loan_Payment__c = application.Payment_Frequency__c;
        loanAccount.loan__Payment_Frequency_Cycle__c = application.Payment_Frequency_Multiplier__c;
        loanAccount.loan__Loan_Amount__c = application.Total_Loan_Amount__c;
        loanAccount.loan__Number_of_Installments__c = application.Term__c;
        loanAccount.loan__draw_term__c = application.Term__c;
        loanAccount.Originated_EAR__c = application.Effective_APR__c;
        loanAccount.loan__Account__c = application.AccountId;
        loanAccount.AON_Enrollment__c = application.AON_Enrollment__c;
        loanAccount.AON_Fee__c = application.AON_Fee__c;
        loanAccount.Promotion_Period_Rate__c = 0;
        system.debug('=this.loanProduct=' + this.loanProduct.Name);
        system.debug('=ApplicationloanProduct=' + application.Lending_Product__c);
        loanAccount.Lending_Product__c = application.Lending_Product__c;
         if ((generalPurposeWebServices.isPointOfNeed(application.ProductName__c) || generalPurposeWebServices.isSelfPayProduct(application.ProductName__c)) &&
                   (application.Partner_Account__c == null || !application.Partner_Account__r.Use_Promotion_Fields__c || String.isnotempty(application.Offer_Code__c)) ) { //API-317
            /*MAINT -705 changes*/
            loanAccount.Promotion_Period_Rate__c = application.Promotion_Rate__c;//this.loanProduct != null ? this.loanProduct.loan__Default_Interest_Rate__c : EZVerifySettings.Default_Interest_Rate__c;            
            loanAccount.Discount_Duration_Days__c = 0;
            if (application.Partner_Account__c != null && application.Partner_Account__r.Promotion_Duration__c != null){
                loanAccount.Discount_Duration_Days__c = application.Partner_Account__r.Promotion_Duration__c; //MAINT-914
            }
            
            //API-317
            Integer durationDays;
            if(String.isnotempty(application.Offer_Code__c)) {
                 Promo_Information__mdt promoInfo = OfferCtrlUtil.getPromoInfo(application.Offer_Code__c,true);
                 if(promoInfo != null && promoInfo.Term__c != null) durationDays = (Integer.valueof(promoInfo.Term__c*30));
            }
            loanAccount.Discount_Duration_Days__c = (durationDays != null && durationDays > 0) ? durationDays : loanAccount.Discount_Duration_Days__c;
            //API-317

            loanAccount.Discount_Duration_Months__c =  (loanAccount.Discount_Duration_Days__c / 30).intValue(); //MER-235

            loanAccount.Is_On_Discount_Promotion__c = application.Promotion_Active__c;
            if (loanAccount.Discount_Duration_Days__c  > 0) loanAccount.promotion_Type__c = 'SAC';
            loanAccount.Discount_End_Date__c = application.Promotion_Expiration_Date__c;// application.Expected_Start_Date__c.addDays(loanAccount.Discount_Duration_Days__c.intValue());
        } else if (application.Partner_Account__c != null &&
            application.Partner_Account__r.Use_Promotion_Fields__c &&
            co.Promotion_Active__c &&
            ( co.Promotion_Expiration_Date__c != null ||
              co.Promotion_Term_In_Months__c != null) ) {

             loanAccount.Is_On_Discount_Promotion__c = True;
             loanAccount.Promotion_Period_Rate__c = co.Promotion_Rate__c != null ? co.Promotion_Rate__c : 0;
             loanAccount.Discount_Duration_Months__c = co.Promotion_Term_In_Months__c;
             loanAccount.Discount_End_Date__c = co.Promotion_Expiration_Date__c != null ? co.Promotion_Expiration_Date__c : 
                                    (application.Expected_Start_Date__c.addMonths(co.Promotion_Term_In_Months__c.intValue()));

        }
        if (loanAccount.Promotion_Period_Rate__c == null) loanAccount.Promotion_Period_Rate__c = 0;

        User curUser = [Select Id, loan__Current_Branch_s_System_Date__c  from User Where Id =: UserInfo.getUserId()];

        application.Expected_Disbursal_Date__c = bIsSandBox ? Date.newInstance(2019, 09, 09) : ( curUser.loan__Current_Branch_s_System_Date__c == null ? Date.today() : curUser.loan__Current_Branch_s_System_Date__c );
        loanAccount.loan__Expected_Disbursal_Date__c = application.Expected_Disbursal_Date__c;
        loanAccount.loan__Disbursal_Date__c = application.Expected_Disbursal_Date__c;
        loanAccount.loan__Payment_Amount__c = application.Payment_Amount__c;
        loanAccount.loan__Interest_Rate__c = application.Interest_Rate__c;
        loanAccount.loan__Contact__c = application.Contact__c;
        loanAccount.PaymentMethod__c = application.Payment_Method__c;  
        

        if (application.is_Finwise__c || (!application.is_Finwise__c && !application.is_Feb__c) || (application.Partner_Account__c != null && application.Partner_Account__r.Accrue_Interest_On_Disbursal__c) ) {
            loanAccount.loan__Accrual_Start_Date__c = DateTime.now() < cutOffDate && dow < 6 ? Date.today() :  OfferCtrlUtil.nextBusinessDate(Date.today(), 1);
        } else {
            loanAccount.loan__Accrual_Start_Date__c = OfferCtrlUtil.nextBusinessDate(loanAccount.loan__Disbursal_Date__c, 2);
        }


        if (loanAccount.Is_On_Discount_Promotion__c) loanAccount.Discounted_Balance__c = application.Amount;

        loanAccount.loan__contractual_Interest_Rate__c = application.Interest_Rate__c;
        loanAccount.loan__Product_Type__c = application.Product_Type__c;
        loanAccount.loan__Disbursal_Amount__c = application.Total_Loan_Amount__c;
        loanAccount.loan__Draw_Period_End_Date__c = application.Expected_Start_Date__c;
        //loanAccount.loan__Balloon_Payment__c = (application.Balloon_Payment__c == null) ? 0 : application.Balloon_Payment__c;

        system.debug(';;' + application.Interest_Only_Period__c);

        loanAccount.loan__Interest_Only_Period__c = application.Interest_Only_Period__c;

        loan.GlobalLoanUtilFacade info = new loan.GlobalLoanUtilFacade();

        loanAccount.loan__Approval_Date__c = info.getCurrentSystemDate();
        loanAccount.loan__Contractual_Due_Day__c = application.Expected_First_Payment_Date__c.day();
        loanAccount.loan__Expected_Repayment_Start_Date__c = application.Expected_First_Payment_Date__c;
        loanAccount.loan__Next_Installment_Date__c = application.Expected_First_Payment_Date__c;
        loanAccount.loan__First_Installment_Date__c = application.Expected_First_Payment_Date__c;
        loanAccount.loan__Due_Day__c = application.Expected_First_Payment_Date__c.day();
        loanAccount.loan__Application_Date__c = Date.valueOf(application.CreatedDate);

        loanAccount.RecordTypeId = [select Id,
                                    Name,
                                    SobjectType
                                    from RecordType
                                    where Name = : application.Product_Type__c
                                            and SObjectType = : 'loan__Loan_Account__c' LIMIT 1].id;

        //get Loan Product info to Loan Account...
        loanAccount.loan__Loan_Product_Name__c = loanProduct.id;
        loanAccount.loan__Interest_Calculation_Method__c = loanProduct.loan__Interest_Calculation_Method__c;
        loanAccount.loan__Delinquency_Grace_Days__c = loanProduct.loan__Delinquency_Grace_Days__c;
        loanAccount.loan__Write_off_Tolerance_Amount__c = loanProduct.loan__Write_off_Tolerance_Amount__c;
        loanAccount.loan__Grace_Period_for_Repayments__c = loanProduct.loan__Late_Charge_Grace_Days__c;
        loanAccount.loan__LA_Amortized_Balance_Type__c = loanProduct.loan__Amortize_Balance_type__c;
        loanAccount.loan__LA_Amortization_Frequency__c = loanProduct.loan__Amortization_Frequency__c;
        loanAccount.loan__LA_Amortization_Enabled__c = loanProduct.loan__Amortization_Enabled__c;
        loanAccount.loan__LA_Amortization_Calculation_Method__c = loanProduct.loan__Amortization_Calculation_Method__c;
        loanAccount.loan__Term_Cur__c = loanAccount.loan__Number_Of_Installments__c;
        loanAccount.loan__Fee_Set__c = loanProduct.loan__Fee_Set__c;
        loanAccount.loan__Pre_Bill_Days__c = loanProduct.loan__Pre_Bill_Days__c;
        loanAccount.loan__Pmt_Amt_Cur__c = loanAccount.loan__Payment_Amount__c;

        loanAccount.loan__Include_In_Metro2_File__c = true;
        loanAccount.loan__Loan_Status__c = loan.LoanConstants.LOAN_STATUS_PARTIAL_APPLICATION;
        loanAccount.loan__Interest_Type__c = 'Fixed';
        loanAccount.loan__Time_Counting_Method__c = application.Days_Convention__c == 'ACTUAL/360' ? clcommon.CLConstants.TIME_COUNTING_ACTUAL_360 : application.Days_Convention__c == '365/365' ? loan.LoanConstants.TIME_COUNTING_ACTUAL_DAYS : clcommon.CLConstants.TIME_COUNTING_ACTUAL_360;
        

        if (!Test.isRunningTest())    loanAccount.loan__Branch__c = application.Company__c;


        loanAccount.Opportunity__c = application.Id;
        loanAccount.Unique_Code_Hit_Id__c = application.Unique_Code_Hit_Id__c ; //API-101
        
        try {
            if (!Test.isRunningTest())
                insert loanAccount;
        } catch (Exception e) {
            system.debug('===Exception==' + e.getmessage());
            Database.Rollback(sp);
            return e.getMessage();
        }


        loanAccount.loan__Loan_Status__c = loan.LoanConstants.LOAN_STATUS_APPROVED;
        loanAccount.loan__Last_Installment_Date__c = application.Expected_Close_Date__c;
        loanAccount.loan__Maturity_Date_Current__c = loanAccount.loan__Last_Installment_Date__c;

        loanAccount.loan__ACH_Account_Number__c = String.valueOf(application.ACH_Account_Number__c);

        if (application.ACH_Account_Type__c == null) application.ACH_Account_Type__c = 'Checking';
        loanAccount.loan__ACH_Account_Type__c = application.ACH_Account_Type__c;
        loanAccount.loan__ACH_Bank_Name__c = application.ACH_Bank_Name__c == null || application.ACH_Bank_Name__c == '' ? 'N/A' : application.ACH_Bank_Name__c;
        loanAccount.loan__ACH_Debit_Amount__c = application.Payment_Amount__c;
        loanAccount.loan__Ach_Debit_Day__c = application.Expected_First_Payment_Date__c.day();
        loanAccount.loan__ACH_Frequency__c = application.Payment_Frequency__c;
        loanAccount.loan__ACH_Frequency_Cycle__c = application.Payment_Frequency_Multiplier__c;
        loanAccount.loan__ACH_Routing_Number__c = String.valueOf(application.ACH_Routing_Number__c);
        loanAccount.loan__ACH_Start_Date__c = application.Expected_First_Payment_Date__c;
        loanAccount.loan__ACH_Next_debit_date__c = application.Expected_First_Payment_Date__c;

        try {
            loanAccount.Originated_Grade__c = application.Scorecard_Grade__c;
            for ( Scorecard__c  sc : [Select Id, Grade__c from Scorecard__c where Opportunity__c = : application.id LIMIT 1] ) {
                loanAccount.Originated_Grade__c = sc.Grade__c;
            }
           
            loanAccount.Originated_Annual_Income__c = (application.Estimated_Grand_Total__c != null && application.Estimated_Grand_Total__c > 0) ? application.Estimated_Grand_Total__c : application.Contact__r.Annual_Income__c;
            if (application.DTI__c != null) loanAccount.Originated_DTI__c = Decimal.valueOf(application.DTI__c);
            if (application.FICO__c != null) loanAccount.Originated_FICO__c = Decimal.valueOf(application.FICO__c).setScale(0);
            loanAccount.Originated_PTI__c = loanAccount.Originated_Annual_Income__c != null && loanAccount.Originated_Annual_Income__c <= 0 ? 0 : ( application.Payment_Amount__c * OfferCtrlUtil.getFrequencyUtil(application.Payment_Frequency_Masked__c)) / loanAccount.Originated_Annual_Income__c ;
            //loanAccount.loan__ACH_On__c = application.Payment_Method__c == 'ACH';
            
            
            
            //E2E-78
            if(application.Payment_Method__c == 'ACH'){
                loanAccount.loan__ACH_On__c = true;
            }else if(application.Payment_Method__c == 'Debit Card'){
                loanAccount.Is_Debit_Card_On__c = true; //application.Payment_Method__c == 'ACH';
                //E2E-78
              loanAccount.Card_Next_Debit_Date__c = application.Expected_First_Payment_Date__c;
            }
            //E2E-78
         

        } catch (Exception e) {
            Database.Rollback(sp);
            System.debug(e.getMessage() + '\n' + e.getStackTraceString());
            return e.getMessage();
        }

        application.Lending_Account__c = loanAccount.Id;
    
        application.status__c = 'Funded';
        application.Funded_Date__c = system.today();
        try {
            if ( application.Product_Type__c.equalsIgnoreCase('Loan') && !Test.isRunningTest())
                    loan.RegenerateAmortizationScheduleCtrl.regenerateAmortizationSchedule(loanAccount.id);
             for (loan__Repayment_Schedule_Summary__c rss : [select id from loan__Repayment_Schedule_Summary__c 
                                                            where loan__RSS_Loan_Account__c =: loanAccount.Id limit 1 ]) {
                      rss.loan__RSS_Repayment_Amt__c = loanAccount.loan__Payment_Amount__c;
                      update rss;
             }
            
            createBorrowerACH();
            if (!Test.isRunningTest()) {
                           
                update loanAccount;
                system.debug('===Contract Created===' + loanAccount);
                update application;
                
                system.debug('===Application Updated===' + application);
            }

           /* Account acc;
            system.debug('====i2c started===');
            system.debug('====Partner account===' + application.Partner_Account__c);
            if (application.Partner_Account__c <> null)
                acc = [select id, Funding_Payment_Mode__c from account where id = : application.Partner_Account__c limit 1];
            if (acc <> null && !string.isEmpty(acc.Funding_Payment_Mode__c) && acc.Funding_Payment_Mode__c == 'i2c') {
                i2cProcessCall(application.id, acc.Funding_Payment_Mode__c, loanAccount.loan__Expected_Disbursal_Date__c, loanAccount.loan__Loan_Amount__c, loanAccount.id);
            } else*/
                createFirstDisbursal('ACH');
            system.debug('====i2c finished===');
        } catch (Exception e) {
            Database.Rollback(sp);
            System.debug(e.getMessage() + '\n' + e.getStackTraceString());
            return e.getMessage();
        }

        loan__Loan_Account__c loanAcc = new loan__loan_Account__c();

        if (!Test.isRunningTest()) {
            // Do this last update because CL Seems to overwrite with an invalid value
            update new loan__loan_Account__c(Id = loanAccount.id, loan__Last_Installment_Date__c = application.Expected_Close_Date__c,
                                             loan__Maturity_Date_Current__c = loanAccount.loan__Last_Installment_Date__c, Agreement_Modified_Date__c = null, Contract_Modification_Reason__c = null );
            //loanAcc = [select id, Name, CreatedDate, Originated_Grade__c from loan__loan_Account__c where Id = : loanAccount.Id];
            if(loanAccount.Originated_Grade__c == null || loanAccount.Originated_Grade__c == '')
            sendEmailWithoutOriginatedGrade(loanAcc.Id);
        }
        String msg = '<h3><a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + loanAcc.Id + '">' + loanAcc.Name + '</a></h3>.';
        system.debug('===Application converted to Loan==');
       
        return 'Application converted to Loan ' + msg;
    }

    public void sendEmailWithoutOriginatedGrade(String loanAccountId){
       

        loan__loan_Account__c loanAcc = [select id, Name, CreatedDate, Originated_Grade__c from loan__loan_Account__c where Id = : loanAccountId];
            string subject ='Loan originated without grade';
                string body = loanAcc.Name +' -was created on  ' + loanAcc.CreatedDate + '  without an Originated Grade.';
                List <GroupMember> dataEntries = [SELECT Id, UserOrGroupId, GroupId FROM GroupMember WHERE group.Name='Capital Markets'];
                Set <Id> userIds = new Set <Id> ();
    
                for (GroupMember member: dataEntries) {
                    userIds.add(member.UserOrGroupId);
                }
                
                for (Id userId: userIds) { 
                    salesForceUtil.EmailByUserId(subject, body, userId);
                } 
        
    }

    @TestVisible
    private void createRepaymentSchedule(Decimal terms, Decimal paymentAmount) {
        loan__Repayment_Schedule_Summary__c rss = new loan__Repayment_Schedule_Summary__c( loan__RSS_Contract_flag__c = true, loan__RSS_System_Generated_Flag__c = false,
                loan__RSS_Loan_Account__c = loanAccount.Id, loan__RSS_No_Of_Pmts__c = terms,
                loan__RSS_Primary_flag__c = true,  loan__RSS_Repayment_Amt__c = paymentAmount,
                loan__RSS_Repayment_Dt__c = loanAccount.loan__First_Installment_Date__c,
                loan__RSS_Seq__c = 1, loan__RSS_ID__c = '0');
        if (!Test.isRunningTest()) insert rss;
    }

    private void createBorrowerACH() {
        List<loan__Bank_Account__c> baList = [Select id, loan__Bank_Account_Number__c from loan__Bank_Account__c where loan__Contact__c = : application.Contact__r.Id
                                              AND loan__Bank_Name__c = : loanAccount.loan__ACH_Bank_Name__c
                                                      AND loan__Routing_Number__c = : loanAccount.loan__ACH_Routing_Number__c];

        if (baList != null && baList.size() > 0) {
            for (loan__Bank_Account__c ba : baList) {
                if (ba.loan__Bank_Account_Number__c == loanAccount.loan__ACH_Account_Number__c) {
                    loanAccount.loan__Borrower_ACH__c = ba.id;
                    return ;
                }
            }
        }
        loan__Bank_Account__c ba1 = new loan__Bank_Account__c(loan__Contact__c = application.Contact__r.Id, loan__Routing_Number__c = loanAccount.loan__ACH_Routing_Number__c,
                loan__Bank_Name__c = loanAccount.loan__ACH_Bank_Name__c, loan__Account_Type__c = loanAccount.loan__ACH_Account_Type__c,
                loan__Bank_Account_Number__c = loanAccount.loan__ACH_Account_Number__c);
        try {
            if (!Test.isRunningTest()) {
                insert ba1;
                loanAccount.loan__Borrower_ACH__c = ba1.Id;

            }
        } catch (Exception e) {}

    }

    private void createFirstDisbursal(String mode) {

        loan__Payment_Mode__c paymentMode = null;
        if (!String.isEmpty(mode)) {
            for (loan__Payment_Mode__c pmode :  [Select Id from loan__Payment_Mode__c where Name = : mode]) {
                paymentMode = pmode;
            }
        }
        if (paymentMode == null) paymentMode = [Select Id from loan__Payment_Mode__c where Name = : 'Cash'];
        
        loan.GlobalLoanUtilFacade info = new loan.GlobalLoanUtilFacade();

        try {
            loan__Loan_Disbursal_Transaction__c ld = new loan__Loan_Disbursal_Transaction__c(loan__Disbursal_Date__c = loanAccount.loan__Expected_Disbursal_Date__c,
                    loan__Disbursed_Amt__c = loanAccount.loan__Loan_Amount__c,  loan__Loan_Account__c = loanAccount.id,
                    loan__Mode_of_Payment__c = paymentMode.id, Refinance_Payoff_Amount__c = application.Refinance_Pay_Off_Amount__c != null && application.Refinance_Pay_Off_Amount__c > 0 ? application.Refinance_Pay_Off_Amount__c : null);
            loan.LoanDisbursalActionAPI disbursalAPI = new loan.LoanDisbursalActionAPI( ld);

            loan__Loan_Account__c lA = disbursalAPI.disburseLoanAccount();
            system.debug('====Final contract===' + lA);
        } catch (Exception e) {
            WebToSFDC.notifyDev('CreateLoanAccountForLoanTypeHandler Error - createFirstDisbursal', e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
    }
/*
    @future(callout = true)
    public static void i2cProcessCall(string applicationId, string accFundingMode, date expectedDisbursalDate, decimal loanAmount, string loanId) {
        loan__Payment_Mode__c paymentMode = [Select Id from loan__Payment_Mode__c where Name = : 'Cash'];
        loan__Payment_Mode__c achMode = [Select Id from loan__Payment_Mode__c where Name = : 'ACH'];
        try {
            string response = generalPurposeWebServices.i2cAPIDetails(applicationId);
            if (!string.isEmpty(response) && response == '200') {

                if (!String.isEmpty(accFundingMode)) {
                    for (loan__Payment_Mode__c lp : [Select Id from loan__Payment_Mode__c where Name = : accFundingMode])paymentMode = lp;
                    }
                Opportunity app = [Select Id, Finwise_Approved__c from Opportunity where Id =: applicationId];
                loan.GlobalLoanUtilFacade info = new loan.GlobalLoanUtilFacade();

                loan__Loan_Disbursal_Transaction__c disbursal =  new loan__Loan_Disbursal_Transaction__c(loan__Loan_Account__c = loanId, loan__Disbursed_Amt__c = loanAmount);
                boolean bIsSandBox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
                disbursal.loan__Disbursal_Date__c = bIsSandBox ? Date.newInstance(2017, 03, 02) : expectedDisbursalDate;
                disbursal.loan__Mode_of_Payment__c = app.Finwise_Approved__c ? achMode.Id : paymentMode.id;
                system.debug('====disbursal===' + disbursal);

                loan.LoanDisbursalActionAPI disbursalAPI = new loan.LoanDisbursalActionAPI(disbursal);
                loan__Loan_Account__c lA = disbursalAPI.disburseLoanAccount();
                system.debug('====Final contract===' + lA);

            }

        } catch (Exception e) {
            system.debug('===Exception===' + e.getMessage());
        }
    }
*/
}