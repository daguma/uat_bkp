public with sharing class ScoreCardGradeExt {
    
    public string creditGrade{get;set;}
    public loan__Loan_Account__c loanAcnt;
    public string appId;
    
    public ScoreCardGradeExt(ApexPages.StandardController controller) {
              loanAcnt = (loan__Loan_Account__c)controller.getRecord();
        	  system.debug(loanAcnt.id);
              if(loanAcnt.id <> null){
                  appId = [select Application__c from loan__Loan_Account__c where id=: loanAcnt.id].Application__c;
                  
                  for (Scorecard__c sc : [Select Grade__c from Scorecard__c where Application__c = :appId order by Id desc limit 1]){
                      creditGrade = sc.Grade__c;
                  }
              }
    }

}