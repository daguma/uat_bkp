@isTest
private class BridgerServiceTest {

    public static testMethod void VerifyBridgerService() {

        Contact c = TestHelper.createContact();
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('contactid', c.id);
        gen.writeEndObject();
        string req = gen.getAsString();

        BridgerService.verifyBridger(req);
    }

}