public with sharing class EmploymentDetailhandler{

  public static void onInsert(list<Employment_Details__c> EmpList){
  
      Map<id, Opportunity> AppMap= new Map<id, Opportunity>();
     
      AppMap= ContactFromEmployDetl(EmpList);
      system.debug('===AppMap==='+AppMap);
      
      for(Employment_Details__c emp: EmpList){
     
         emp.Completed_By__c    = (emp.Was_Employment_Verified__c  == true || emp.Employer_name_match_with_the_paystubs__c  == true || emp.Employer_Name__c  <> null || emp.Hire_Date__c  <> null || emp.Less_than_1_year__c  == true || emp.Position__c  <> null  || emp.Last_Pay_Date__c  <> null 
         || emp.Employment_Confirmed_By__c <> null || emp.Third_Party__c == true || emp.Salary_Cycle__c <> AppMap.get(emp.Opportunity__c).Payroll_Frequency__c || emp.Employer_Phone_Number__c<> AppMap.get(emp.Opportunity__c).Contact__r.Office_Phone_Number__c || emp.Second_Job__c == true
         || emp.Third_Job__c == true || emp.Prior_Company_Name__c <> null || emp.Prior_Employment_Confirmed_By__c <> null || emp.Prior_Third_Party__c == true
         || emp.Prior_Company_Phone__c <> null || emp.Previous_Employment_Start_Date__c <> null || emp.Previous_Employment_End_Date__c <> null)?userinfo.getuserid():null;
                 
         emp.SJ_Completed_By__c = (emp.SJ_Hire_Date__c <> null || emp.SJ_Less_than_1_year__c == true || emp.SJ_Was_Employment_Verified__c == true || emp.SJ_Employer_name_match_with_the_paystubs__c == true || emp.SJ_Employer_Name__c <> null || emp.SJ_Employer_Phone_Number__c <> null  || emp.SJ_Position__c <> null || emp.SJ_Salary_Cycle__c <> null || emp.SJ_Last_Pay_Date__c <> null || emp.SJ_Employment_Confirmed_By__c <> null || emp.SJ_Third_Party__c == true)?userinfo.getuserid():null;
         
         emp.TJ_Completed_By__c = (emp.TJ_Hire_Date__c <> null || emp.TJ_Less_than_1_year__c == true || emp.TJ_Was_Employment_Verified__c == true || emp.TJ_Employer_name_match_with_the_paystubs__c == true || emp.TJ_Employer_Name__c  <> null || emp.TJ_Employer_Phone_Number__c <> null  || emp.TJ_Position__c <> null || emp.TJ_Salary_Cycle__c <> null || emp.TJ_Last_Pay_Date__c <> null || emp.TJ_Employment_Confirmed_By__c <> null || emp.TJ_Third_Party__c == true)?userinfo.getuserid():null;           
      }
      
      
  }
  
  public static void onUpdate(Map<id,Employment_Details__c> empOldMap, list<Employment_Details__c> EmpList){     
         
          Map<id, Opportunity> AppMap= new Map<id, Opportunity>();
     
          AppMap= ContactFromEmployDetl(EmpList);          
          
       for(Employment_Details__c emp: EmpList){  
                      
            emp.Completed_By__c    = (empOldMap.get(emp.id).Was_Employment_Verified__c  <> emp.Was_Employment_Verified__c || empOldMap.get(emp.id).Employer_name_match_with_the_paystubs__c  <> emp.Employer_name_match_with_the_paystubs__c || empOldMap.get(emp.id).Employer_Name__c  <> emp.Employer_Name__c || empOldMap.get(emp.id).Hire_Date__c  <> emp.Hire_Date__c 
             || empOldMap.get(emp.id).Less_than_1_year__c  <> emp.Less_than_1_year__c || empOldMap.get(emp.id).Position__c  <> emp.Position__c || empOldMap.get(emp.id).Last_Pay_Date__c  <> emp.Last_Pay_Date__c
             || empOldMap.get(emp.id).Employment_Confirmed_By__c  <> emp.Employment_Confirmed_By__c || empOldMap.get(emp.id).Third_Party__c  <> emp.Third_Party__c 
             || (emp.Employer_Phone_Number__c<> AppMap.get(emp.Opportunity__c).Contact__r.Office_Phone_Number__c && empOldMap.get(emp.id).Employer_Phone_Number__c  <> emp.Employer_Phone_Number__c
             || empOldMap.get(emp.id).Second_Job__c <> emp.Second_Job__c || empOldMap.get(emp.id).Third_Job__c <> emp.Third_Job__c ||  empOldMap.get(emp.id).Prior_Company_Name__c <> emp.Prior_Company_Name__c
             || empOldMap.get(emp.id).Prior_Employment_Confirmed_By__c<> emp.Prior_Employment_Confirmed_By__c || empOldMap.get(emp.id).Prior_Third_Party__c<> emp.Prior_Third_Party__c || empOldMap.get(emp.id).Prior_Company_Phone__c<> emp.Prior_Company_Phone__c
             || empOldMap.get(emp.id).Previous_Employment_Start_Date__c<> emp.Previous_Employment_Start_Date__c || empOldMap.get(emp.id).Previous_Employment_End_Date__c<> emp.Previous_Employment_End_Date__c
             || (emp.Salary_Cycle__c <> AppMap.get(emp.Opportunity__c).Payroll_Frequency__c) && empOldMap.get(emp.id).Salary_Cycle__c <> emp.Salary_Cycle__c ))?userinfo.getuserid():emp.Completed_By__c;                        
                        
            emp.SJ_Completed_By__c = (empOldMap.get(emp.id).SJ_Hire_Date__c  <> emp.SJ_Hire_Date__c || empOldMap.get(emp.id).SJ_Less_than_1_year__c  <> emp.SJ_Less_than_1_year__c || empOldMap.get(emp.id).SJ_Was_Employment_Verified__c  <> emp.SJ_Was_Employment_Verified__c || empOldMap.get(emp.id).SJ_Employer_name_match_with_the_paystubs__c  <> emp.SJ_Employer_name_match_with_the_paystubs__c || empOldMap.get(emp.id).SJ_Employer_Name__c  <> emp.SJ_Employer_Name__c || empOldMap.get(emp.id).SJ_Employer_Phone_Number__c  <> emp.SJ_Employer_Phone_Number__c || empOldMap.get(emp.id).SJ_Position__c  <> emp.SJ_Position__c || empOldMap.get(emp.id).SJ_Salary_Cycle__c  <> emp.SJ_Salary_Cycle__c || empOldMap.get(emp.id).SJ_Last_Pay_Date__c  <> emp.SJ_Last_Pay_Date__c || empOldMap.get(emp.id).SJ_Employment_Confirmed_By__c  <> emp.SJ_Employment_Confirmed_By__c || empOldMap.get(emp.id).SJ_Third_Party__c  <> emp.SJ_Third_Party__c)?userinfo.getuserid():emp.SJ_Completed_By__c;                  
            
            emp.TJ_Completed_By__c = (empOldMap.get(emp.id).TJ_Hire_Date__c  <> emp.TJ_Hire_Date__c || empOldMap.get(emp.id).TJ_Less_than_1_year__c  <> emp.TJ_Less_than_1_year__c || empOldMap.get(emp.id).TJ_Was_Employment_Verified__c  <> emp.TJ_Was_Employment_Verified__c || empOldMap.get(emp.id).TJ_Employer_name_match_with_the_paystubs__c  <> emp.TJ_Employer_name_match_with_the_paystubs__c || empOldMap.get(emp.id).TJ_Employer_Name__c  <> emp.TJ_Employer_Name__c || empOldMap.get(emp.id).TJ_Employer_Phone_Number__c  <> emp.TJ_Employer_Phone_Number__c || empOldMap.get(emp.id).TJ_Position__c  <> emp.TJ_Position__c || empOldMap.get(emp.id).TJ_Salary_Cycle__c  <> emp.TJ_Salary_Cycle__c || empOldMap.get(emp.id).TJ_Last_Pay_Date__c  <> emp.TJ_Last_Pay_Date__c || empOldMap.get(emp.id).TJ_Employment_Confirmed_By__c  <> emp.TJ_Employment_Confirmed_By__c || empOldMap.get(emp.id).TJ_Third_Party__c  <> emp.TJ_Third_Party__c)?userinfo.getuserid():emp.TJ_Completed_By__c;            
      }
  }
  
  public static Map<id, Opportunity> ContactFromEmployDetl(list<Employment_Details__c> EmpList){
      set<id> AppIdSet= new Set<id>();     
      Map<id, Opportunity> AppMap;
      
      for(Employment_Details__c Emp: EmpList)
          AppIdSet.add(Emp.Opportunity__c);    
     
      AppMap= new Map<id, Opportunity>([select id, Contact__c, Contact__r.Office_Phone_Number__c, Contact__r.Employer_Name__c,Contact__r.name, Payroll_Frequency__c from Opportunity where id IN: AppIdSet]);
      
      return AppMap;
  }

}