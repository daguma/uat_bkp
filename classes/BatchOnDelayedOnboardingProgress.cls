global class BatchOnDelayedOnboardingProgress implements Database.Batchable<sObject>, Schedulable,  Database.Stateful {
    public integer count = 0;
    global void execute (SchedulableContext sc) {
        BatchOnDelayedOnboardingProgress job = new BatchOnDelayedOnboardingProgress();
        System.debug('Starting BatchOnDelayedOnboardingProgress at ' + Date.today());
        Database.executeBatch(job, 200);
    }
    String query = '';
    DateTime dt = system.now().addHours(-1).addMinutes(-20);
    global Database.QueryLocator start(Database.BatchableContext BC) {
        try {
            query = 'SELECT Id, Step_1_Information__c, Step_2_Information__c, createdDate, Invisalign_Account_Match__c ' + 
                ' from Lead ' +
                ' WHERE createdDate >= :dt AND LeadSource = \'Invisalign\' AND Step_1_Information__c = true AND Step_2_Information__c = false' ;
            
        } catch(exception ex){
            System.debug('Exception:'+ ex.getMessage());
            WebToSFDC.notifyDev('BatchOnDelayedOnboardingProgress.QueryLocator ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
        }
        return Database.getQueryLocator(query);    
     }
    
     global void execute(Database.BatchableContext BC,List<sObject> scope) {
         
         try { 
             system.debug('==scope============='+scope);
             Integer minutesOfCreation;
             List<Case> caseList = new list<Case>();
             map<string,Case> caseMap = new map<string, Case>();
             set<string> descSet = new set<string>();
             for(lead l : (List<Lead>)scope) {
                 descSet.add(URL.getSalesforceBaseUrl().getHost() +'/'+ l.id);
             }
             for(Case c : [select id, Supporting_Documentation__c from Case where Supporting_Documentation__c IN : descSet]) {
                 caseMap.put(c.Supporting_Documentation__c, c);
             }
             
            list<AssignmentRule> AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
            List<Merchant_Onboarding_Case_Setting__mdt> defaultCaseDataList = [select id, DeveloperName, Business_Hour_ID__c, Description__c, Origin__c, Priority__c, recordTypeId__c, Status__c, Subject__c from Merchant_Onboarding_Case_Setting__mdt where developerName = 'Case_On_Lead' limit 1];
            for (lead lead : (List<Lead>)scope) {
                if(lead.createdDate <= system.now().addHours(-1) && !caseMap.containskey(URL.getSalesforceBaseUrl().getHost() + lead.id) && !AR.isEmpty())
                {
                    if(!defaultCaseDataList.isEmpty()) {
                        Case c = new Case();
                        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
                        Database.DMLOptions dmlOpts = new Database.DMLOptions();
                        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR[0].id;
                        c.setOptions(dmlOpts);
                        c.BusinessHoursId = defaultCaseDataList[0].Business_Hour_ID__c;
                        c.Description = defaultCaseDataList[0].Description__c + lead.id;
                        c.Origin = defaultCaseDataList[0].Origin__c;
                        c.Priority = defaultCaseDataList[0].Priority__c;
                        c.recordTypeId = defaultCaseDataList[0].recordTypeId__c;
                        c.Status = defaultCaseDataList[0].Status__c;
                        c.Subject = defaultCaseDataList[0].Subject__c;
                        c.Supporting_Documentation__c = URL.getSalesforceBaseUrl().getHost() +'/'+ lead.id;
                        caseList.add(c);
                        count++;
                    }
                }
            }
             
            if(!caseList.isEmpty()) insert caseList;
             
        } catch(exception ex){
             System.debug('Exception:'+ ex.getMessage());
             WebToSFDC.notifyDev('BatchOnDelayedOnboardingProgress ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
        }
    }  
    global void finish(Database.BatchableContext BC){
        if(count > 0) WebToSFDC.notifyDev('BatchOnDelayedOnboardingProgress Finished', 'Reminders Processed = ' + count , UserInfo.getUserId() );
    }
}