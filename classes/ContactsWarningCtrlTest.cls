@isTest 
private class ContactsWarningCtrlTest {
    
    @isTest static void gets_new_app_by_contact() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'Multiloan';
        app.Status__c = 'Credit Qualified';
        upsert app;
        ContactsWarningCtrl.ContactsWarningEntity rae = ContactsWarningCtrl.isMultiloan(app.contact__c);

        System.assertEquals(true,rae.isMultiloan);
        System.assertEquals(false,rae.hasError);
    }
    

    private static loan__loan_Account__c creates_contract() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        contract.loan__Interest_Accrued_Not_Due__c = 500;
        update contract;

        Offer__c off = new Offer__c();
        off.IsSelected__c = true;
        off.Opportunity__c = contract.Opportunity__c;
        off.APR__c = 0.2;
        off.Fee__c = 100;
        off.Fee_Percent__c = 0.04;
        off.FirstPaymentDate__c = Date.today().addDays(-29);
        off.Installments__c = 36;
        off.Loan_Amount__c = 5000;
        off.Term__c = 36;
        off.Payment_Amount__c = 230;
        off.Repayment_Frequency__c = 'Monthly';
        off.Preferred_Payment_Date__c = Date.today().addDays(-23);
        insert off;

        return contract;
    }
    

}