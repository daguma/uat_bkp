global class PrepayRebateImplementationJob implements Schedulable {
	global void execute(SchedulableContext sc) {
		PrepayRebateImplementationBatch b = new PrepayRebateImplementationBatch();
		database.executebatch(b);
	}
}