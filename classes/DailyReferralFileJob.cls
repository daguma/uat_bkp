global class DailyReferralFileJob implements Schedulable
{
    public List<String> fileLines {get; set;}
    public List<String> rowValues {get; set;}
    public Set<String> finMktLoanIDs {get; set;}
    public String fileBody {get; set;}
    public Map<String, ElementWrapper> fileItems {get; set;}
    private ElementWrapper rowItems {get; set;}
    private Daily_Referral_Job_Log__c log {get; set;}

    global void execute(SchedulableContext sc) 
    {
        processDailyFile();
    }

    public Id processLogging(Boolean isSuccessful, String errorMsg, Integer appsProcessed, Integer totalApps) 
    {
        log = new Daily_Referral_Job_Log__c();
        log.Process_Successful__c = isSuccessful;
        log.Error__c = errorMsg;
        log.Apps_Processed__c = appsProcessed;
        log.Total_Apps_Found__c = totalApps;
        insert log;
        return log.Id;
    }

    public void processDailyFile() 
    {
        Integer count = 0;
        Integer hits = 0;
        List<Daily_Referral_Apps_Log__c> appsLog = new List<Daily_Referral_Apps_Log__c>();
        DateTime processDate = DateTime.now();

        try 
        {
            Folder leadMonetizationFolder = [SELECT Id FROM Folder WHERE Name = 'Lead Monetization Report'];
            List<Document> dailyFile = [SELECT  Id, 
                                                Name, 
                                                Body, 
                                                Type, 
                                                CreatedDate, 
                                                LastModifiedDate,
                                                Description
                                        FROM Document 
                                        WHERE FolderId =: leadMonetizationFolder.Id
                                        AND Type = 'csv'
                                        AND Description != '**** FILE PROCESSED SUCCESSFULLY! ****' 
                                        ORDER BY LastModifiedDate DESC, CreatedDate DESC
                                        LIMIT 1];

            if (dailyFile.size() > 0)
            {
                Document thisFile = dailyFile.get(0);
                fileBody = blobToString(thisFile.Body, 'UTF-8');
                fileLines = new List<String>();
                fileLines = fileBody.split('\n');

                fileItems = new Map<String, ElementWrapper>();

                for (Integer s = 1; s < fileLines.size(); s++)
                {
                    rowValues = new List<String>();
                    rowValues = fileLines[s].split(',');
                    rowItems = new ElementWrapper(rowValues);

                    if (rowItems.referralLeadID != null && rowItems.referralLeadID != '') 
                        fileItems.put(rowItems.referralLeadID, rowItems);

                }

                finMktLoanIDs = new Set<String>();
                finMktLoanIDs = fileItems.keySet();
            
                List<genesis__Applications__c> apps = [ SELECT  Id, 
                                                                Referral_Date__c,
                                                                Referral_Partner__c,
                                                                Referral_Funding_Amount__c,
                                                                Referral_APR__c,
                                                                Referral_Term__c,
                                                                Referral_Funding_Date__c,
                                                                genesis__Contact__r.Referral_Status__c,
                                                                FinMkt_LoanId__c,
                                                                Referred__c
                                                        FROM genesis__Applications__c 
                                                        WHERE FinMkt_LoanId__c in :finMktLoanIDs];

                List<genesis__Applications__c> appsToUpdate = new List<genesis__Applications__c>();
                List<Contact> contactsToUpdate = new List<Contact>();

                if (apps.size() > 0) 
                {
                    for (genesis__Applications__c thisApp : apps)
                    {
                        ElementWrapper thisRow = fileItems.remove(thisApp.FinMkt_LoanId__c);
                        if (thisRow != null) 
                        {
                            thisApp.Referral_Partner__c = thisRow.referralPartner;
                            thisApp.Referral_Funding_Amount__c = thisRow.referralFundingAmount;
                            thisApp.Referral_APR__c = thisRow.referralAPR;
                            thisApp.Referral_Term__c = thisRow.referralTerm;
                            thisApp.Referral_Funding_Date__c = thisRow.referralFundingDate;
                            thisApp.genesis__Contact__r.Referral_Status__c = thisRow.referralStatus;
                            thisApp.Referred__c = true;
                            thisApp.Referral_Date__c = Datetime.now();
                            appsToUpdate.add(thisApp);
                            contactsToUpdate.add(thisApp.genesis__Contact__r);
                        }
                    }

                    update appsToUpdate;
                    update contactsToUpdate;

                    thisFile.Description = '**** FILE PROCESSED SUCCESSFULLY! ****';
                    update thisFile;
                    Id logId = processLogging(true, 'File "' + thisFile.Name + '" was processed successfully', appsToUpdate.size(), fileLines.size());

                    if (fileItems.size() > 0) 
                    {
                        for (String item : fileItems.keySet()) 
                        {
                            ElementWrapper thisRow = fileItems.get(item);
                            Daily_Referral_Apps_Log__c dApps = new Daily_Referral_Apps_Log__c();
                            dApps.Application_GUID__c = thisRow.referralLeadID;
                            dApps.Error__c = 'Application could not be found.';
                            dApps.File_Process_Date__c = processDate;
                            dApps.Daily_Referral_Job_Log__c = logId;
                            appsLog.add(dApps);
                        }
                    }

                    insert appsLog;
                }
                else 
                {
                    thisFile.Description = '**** No applications have been found with given Lender Application IDs ****';
                    update thisFile;
                    processLogging(false, 'No applications have been found with given Lender Application IDs', apps.size(), fileLines.size());
                } 
            }
            else
                processLogging(false, 'No CSV files were found.', 0, 0);

            System.debug('Total apps not found: ' + count);
            System.debug('Total hits: ' + hits);
        }
        catch (Exception e)
        {
            processLogging(false, e.getMessage() + ' - ' + e.getStackTraceString(), 0, 0);
        }
    }

    /**
    * This function convers the input CSV file in BLOB format into a string
    * @param input - Blob data representing correct string in @inCharset encoding
    * @param inCharset - Encoding of the Blob data (for example 'ISO 8859-1')
    */
    public static String blobToString(Blob input, String inCharset)
    {
        String hex = EncodingUtil.convertToHex(input);
        
        System.assertEquals(0, hex.length() & 1);
        
        final Integer bytesCount = hex.length() >> 1;
        
        String[] bytes = new String[bytesCount];
        
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }

    private class ElementWrapper 
    {
        public String referralLeadID {get; set;}
        public String firstName {get; set;}
        public String lastName {get; set;}
        public String email {get; set;}
        public Double referralFundingAmount {get; set;}
        public Double referralAPR {get; set;}
        public Integer referralTerm {get; set;}
        public DateTime referralFundingDate {get; set;}
        public String referralStatus {get; set;}
        public String referralPartner {get; set;}

        private ElementWrapper(List<String> items) 
        {
            /** 
             * COLUMNS IN 'items':
             * Lender ID [0]
             * Application ID [1]
             * Lender Application ID [2]
             * First Name [3]
             * Last Name [4]
             * Email [5]
             * App_Dt [6]
             * Apps (Status) [7]
             * Offered (Status) [8]
             * Accepted (Status) [9]
             * Funded (Status) [10]
             * Pending (Status) [11]
             * Rejected (Status) [12]
             * Funded_Amount [13]
             * APR [14]
             * Term [15]
             * Funded_Date_Time [16]
             */

            this.referralPartner = items[0].trim();
            this.referralLeadID = items[2].trim();
            this.firstName = items[3].trim();
            this.lastName = items[4].trim();
            this.email = items[5].trim();

            //Referral Status:
            for (Integer t = 8; t <= 12; t++) 
            {
                if (items[t].trim() == '1')
                {
                    if (t == 8)
                        this.referralStatus = 'Offered';
                    else if (t == 9)
                        this.referralStatus = 'Accepted';
                    else if (t == 10)
                        this.referralStatus = 'Funded';
                    else if (t == 11)
                        this.referralStatus = 'Pending';
                    else if (t == 12)
                        this.referralStatus = 'Rejected';

                    break;
                }
            }

            if (items.size() >= 14)
                this.referralFundingAmount = items[13].trim() != '' ? Double.valueOf(items[13].trim()) : null;
            if (items.size() >= 15)
                this.referralAPR = items[14].trim() != '' ? Double.valueOf(items[14].trim()) : null;
            if (items.size() >= 16)
                this.referralTerm = items[15].trim() != '' ? Integer.valueOf(items[15].trim()) : null;
            if (items.size() >= 17)
            {
                if (items[16].trim() != null && items[16].trim() != '') 
                {
                    String thisTime = items[16].trim().replace('T',' ').replace('Z', '');
                    String[] thisDate = thisTime.split(' ');
                    String[] newDate = thisDate[0].split('-');
                    String[] newTime = thisDate[1].split(':');
                    Date d = Date.newInstance(Integer.valueOf(newDate[0].trim()),Integer.valueOf(newDate[1].trim()),Integer.valueOf(newDate[2].trim()));
                    Time t = Time.newInstance(Integer.valueOf(newTime[0].trim()),Integer.valueOf(newTime[1].trim()),Integer.valueOf(newTime[2].trim()), 0);
                    DateTime dt = DateTime.newInstance(d, t);
                    this.referralFundingDate = dt;
                }
                else
                    this.referralFundingDate = null;
            }
        }
    }     
}