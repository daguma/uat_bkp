global class Clarity {

    public static ProxyApiClarityInquiryEntity getInquiries(ProxyApiCreditReportEntity creditReportEntity) {

        ProxyApiClarityInquiryEntity result = null;

        ClarityInputData inputData = mapClarityInputData(creditReportEntity);

        if (inputData != null) {
            result = ProxyApiClarity.getInquiries(JSON.serialize(inputData));
        }

        return result;
    }
    //CSI-6 
     public static ProxyApiClarityClearIncomeAttributes getClarityIncome(string oppId) {

        ProxyApiClarityClearIncomeAttributes result = null;

        ClarityInputData inputData = mapClarityInputData(oppId);

        if (inputData != null) {
            result = ProxyApiClarity.getClarityIncome(JSON.serialize(inputData));
        }

        return result;
    }    
    public static ClarityInputData mapClarityInputData(ProxyApiCreditReportEntity creditReportEntity) {

        ClarityInputData result = new ClarityInputData();
        result.clarityRequestParameter = mapClarityRequestParameter(creditReportEntity.entityId);
        result.creditReportId = creditReportEntity.id;

        return result.clarityRequestParameter != null ? result : null;
    }
    //CSI-6 
     public static ClarityInputData mapClarityInputData(string OppId) {

        ClarityInputData result = new ClarityInputData();
        result.clarityRequestParameter = mapClarityRequestParameter(OppId);
        
        return result.clarityRequestParameter != null ? result : null;
    }
    
    public static ClarityRequestParameter mapClarityRequestParameter(String appId) {

        ClarityRequestParameter result = null;

        Opportunity app = getApplication(appId);
        Employment_Details__c employment = getEmploymentDetails(appId);
        Contact contact = getContact(app.Contact__r.Id);

        //if (app != null && contact != null && employment != null) {
           if (app != null && contact != null ) {
            result = new ClarityRequestParameter();
            result.bank_account_number = app.ACH_Account_Number__c != null ? app.ACH_Account_Number__c : '';
            result.bank_account_type = app.ACH_Account_Type__c != null ? app.ACH_Account_Type__c : '';
            result.bank_routing_number = app.ACH_Routing_Number__c != null ? app.ACH_Routing_Number__c : '';
            result.cell_phone = contact.MobilePhone != null ? contact.MobilePhone : '';
            result.city = contact.MailingCity != null ? contact.MailingCity : '';
            result.date_of_birth = contact.Birthdate != null ? String.valueOf(contact.Birthdate) : '';
            result.date_of_next_payday = app.Next_Payroll_Date__c != null ? String.valueOf(app.Next_Payroll_Date__c) : '';
            result.drivers_license_number = contact.Driver_s_License_Number__c != null ? contact.Driver_s_License_Number__c : '';
            result.drivers_license_state = '';
            result.email_address = contact.Email != null ? contact.Email : '';
            result.employer_address = contact.Office_Address_Street__c != null ? contact.Office_Address_Street__c : '';
            result.employer_city = contact.Office_Address_City__c != null ? contact.Office_Address_City__c : '';
            result.employer_state = contact.Office_Address_State__c != null ? contact.Office_Address_State__c : '';
            result.first_name = contact.FirstName != null ? contact.FirstName : '';
            result.last_name = contact.LastName != null ? contact.LastName : '';
            result.home_phone = contact.Phone != null ? contact.Phone : '';
            result.housing_status = contact.Residence_Type__c != null ? contact.Residence_Type__c : '';
            result.net_monthly_income = String.valueOf(getMonthlyIncome(app, contact));
            result.annual_income = contact.Annual_Income__c!=null ? contact.Annual_Income__c : 0 ;
            if(employment!=null) {
                result.occupation_type = employment.Position__c != null ? employment.Position__c : '';
                result.pay_frequency = employment.Salary_Cycle__c != null ? employment.Salary_Cycle__c : (contact.Salary_Cycle__c != null ? contact.Salary_Cycle__c : '');
                result.paycheck_direct_deposit = (employment.Payroll_Type__c != null) ? (employment.Payroll_Type__c.equalsIgnoreCase('Direct Deposit') ? '1' : '0') : '';
                result.months_at_current_employer = String.valueOf(getMonthsBetween(employment.Hire_Date__c));
                result.work_phone = employment.Employer_Phone_Number__c != null ? employment.Employer_Phone_Number__c : (contact.Work_Phone_Number__c != null ? contact.Work_Phone_Number__c : '');
            }
            result.reference_first_name = '';
            result.reference_last_name = '';
            result.reference_phone = '';
            result.reference_relationship = '';
            result.social_security_number = contact.SSN__c != null ? contact.SSN__c : '';
            result.state = contact.MailingState != null ? contact.MailingState : '';
            result.street_address_1 = contact.MailingStreet != null ? contact.MailingStreet : '';
            result.street_address_2 = '';
            result.months_at_address = String.valueOf(getMonthsBetween(contact.Time_at_Current_Address__c));
            
            result.work_fax_number = contact.Second_Work_Phone_Number__c != null ? contact.Second_Work_Phone_Number__c : '';
            
            result.zip_code = contact.MailingPostalCode != null ? contact.MailingPostalCode : '';
            result.entity_id = appId;
        }

        return result;
    }
    
    /**
     * Gets the net monthly income
     * @param  app  The application to get the proper annual income
     * @param  contact
     * @return income If the estimated annual income is greather than zero,
     * then returns the estimated annual income, otherwise return the annual income
     * reported by the contact.
     */
    public static Decimal getMonthlyIncome(Opportunity app, Contact contact) {
        Decimal estimatedIncome =
            app.Estimated_Grand_Total__c != null ? app.Estimated_Grand_Total__c : 0;

        Decimal contactIncome =
            contact.Annual_Income__c != null ? contact.Annual_Income__c : 0;

        Decimal income = estimatedIncome > 0 ? estimatedIncome : contactIncome;

        return income / 12;
    }
    
    /**
     * [getMonthsBetween description]
     * @param  startDate [description]
     * @return           [description]
     */
    public static Integer getMonthsBetween(Date startDate) {
        if (startDate == null) return 0;

        Date today = date.today();

        Integer monthDiff = startDate.monthsBetween(today);
        if (today.day() > startDate.day()) monthDiff++;

        return monthDiff;
    }
    
    /*
     * Gets a Opportunity by the application id
     * @param  appId  The id of the application
     * @return Opportunity
      */
    public static Opportunity getApplication(String appId) {
        Opportunity result = null;

        for (Opportunity application : [SELECT Id,
                Name,
                Contact__r.Id, Estimated_Grand_Total__c, ACH_Account_Number__c, ACH_Account_Type__c, ACH_Routing_Number__c, Next_Payroll_Date__c, ApplicationId__c FROM Opportunity
                WHERE (id = : appId or ApplicationId__c = :appId) LIMIT 1 ]) {
            result = application; 
        }
       return result;
    }
    
    /**
     * Gets a Contact by the id
     * @param  contactId  The id of the contact
     * @return Contact
     */
    private static Contact getContact(String contactId) {
        Contact result = null;

        for (Contact contact : [SELECT Id,
                                LastName,
                                FirstName,
                                Annual_Income__c,
                                MobilePhone,
                                Phone,
                                Work_Phone_Number__c,
                                Second_Work_Phone_Number__c,
                                Office_Phone_Number__c,
                                MailingStreet,
                                MailingCity,
                                MailingState,
                                MailingPostalCode,
                                Birthdate,
                                Driver_s_License_Number__c,
                                Email,
                                SSN__c,
                                Office_Address_Street__c,
                                Office_Address_City__c,
                                Office_Address_State__c,
                                Time_at_Current_Address__c,
                                Residence_Type__c,
                                Salary_Cycle__c
                                FROM Contact
                                WHERE id = : contactId
                                           LIMIT 1
                               ]) {
            result = contact;
        }

        return result;
    }
    
    /**
     * Get the employment details by the application id
     * @param  appId The id of the application to get the employment detail
     * @return Employment_Details__c
     */
    private static Employment_Details__c getEmploymentDetails(String appId) {
        Employment_Details__c result = null;

        for (Employment_Details__c employmentDetails : [SELECT
                Id,
                Hire_Date__c,
                Position__c,
                Payroll_Type__c,
                Salary_Cycle__c,
                Employer_Phone_Number__c
                FROM Employment_Details__c
                WHERE Opportunity__c = : appId LIMIT 1]) {
            result = employmentDetails;
        }
        return result;
    }
}