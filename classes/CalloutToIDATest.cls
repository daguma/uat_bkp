@isTest

public class CalloutToIDATest
{
     static testmethod void testCalloutToIDAForSingleContact(){
         Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'NetworkG_IDA',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='NETG1.0',Product_Name__c='NetworkG',User_Name__c='LendingPoint/T1');
         insert ResolveSett;
         List<Contact> conList = new List<Contact>();
         Contact c = LibraryTest.createContactTH();
         c.SSN__c = '123456789';
         conList.add(c);
         CalloutToIDA.invokeNetworkG_IDA(conList);
     }
     static testmethod void testCalloutToIDAForMultipleContact(){
         Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'NetworkG_IDA',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='NETG1.0',Product_Name__c='NetworkG',User_Name__c='LendingPoint/T1');
         insert ResolveSett;
         List<Contact> conList = new List<Contact>();
         Contact c1 = LibraryTest.createContactTH();
         c1.SSN__c = '123456789';
         conList.add(c1);
         Contact c2 = LibraryTest.createContactTH2();
         c2.SSN__c = '987654321';
         conList.add(c2);
         CalloutToIDA.invokeNetworkG_IDA(conList);
     }
}