@isTest public class CoborrowerScreenCtrlTest {

    @isTest static void validate() {
        genesis__Applications__c app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        CoborrowerScreenCtrl coborrower = new CoborrowerScreenCtrl(controller);

        coborrower.c = [Select Id, Name, Firstname, Lastname from Contact where Id = : app.genesis__Contact__c];

        coborrower.c.Employment_Verified_With__c = 'Other';

        update coborrower.c;

        coborrower.saveApp();

        List<Apexpages.Message> msgs = ApexPages.getMessages();

        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;

        System.assert((msgs.get(index)).getDetail() == 'Error : Please input value in Other field as Employment Verified Checklist is Others');

        coborrower.c.Employment_Verified_With__c = 'Employeer';

        update coborrower.c;

        coborrower.saveApp();

        msgs = ApexPages.getMessages();

        //-2 because actually it shows the successful message after the error message
        index = (msgs.size() > 0) ? (msgs.size() - 2) : 0;

        String messageDetail = (msgs.get(index)).getDetail();

        System.assert(messageDetail.contains('Error : '));
        System.assert(!(messageDetail == 'Error : Please input value in Other field as Employment Verified Checklist is Others'));

        coborrower.hideattach = true;
        coborrower.showpanel = false;

        PageReference pr = coborrower.hidepanel();

        System.assertEquals(false, coborrower.hideattach);
        System.assertEquals(true, coborrower.showpanel);

        Note newNote = coborrower.getmynote();

        coborrower.mynote.title = 'Test Note';
        coborrower.mynote.body = 'Test Body Note';

        System.assertEquals(newNote.title, coborrower.mynote.title);
        System.assertEquals(newNote.body, coborrower.mynote.body);

        List<Note> noteBefore = [select title, body from Note where parentId = : app.id];

        pr = coborrower.Savedoc();

        List<Note> noteAfter = [select title, body from Note where parentId = : app.id];

        System.assertNotEquals(noteBefore.size(), noteAfter.size());

        coborrower.notes = newNote;

        System.assertNotEquals(null, coborrower.notes);

        coborrower.creditPull();

        msgs = ApexPages.getMessages();

        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;

        System.assert((msgs.get(index)).getDetail() == 'Credit pull successful.');
    }

    @isTest static void validate_coborrower() {
        genesis__Applications__c app = LibraryTest.createApplicationTH();

        app.Coborrower__c = app.genesis__Contact__c;

        update app;

        ApexPages.StandardController controller = new ApexPages.StandardController(app);
        CoborrowerScreenCtrl coborrower = new CoborrowerScreenCtrl(controller);

        System.assertEquals(true, coborrower.ShowPullButton);

        System.assertEquals(coborrower.LastNameInput, coborrower.c.lastname);

        coborrower.saveApp();

        System.assertEquals(true, coborrower.ShowPullButton);

        List<Apexpages.Message> msgs = ApexPages.getMessages();

        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;

        System.assert((msgs.get(index)).getDetail() == 'Coborrower information saved');

    }

}