@isTest public class MiscellaneousJobTest {
    
    @isTest static void MiscellaneousJob_Test(){
        
        Contact cont = Librarytest.createContactTH();
        
        BridgerWDSFLookup__c item = new BridgerWDSFLookup__c( Contact__c = cont.Id ,
				                                                    name_searched__c = cont.Name,
				                                                    address_type__c = 'Mailing',
				                                                    search_street__c = cont.MailingStreet,
				                                                    search_city__c = cont.mailingcity,
				                                                    search_state__c = cont.mailingstate,
				                                                    search_postal_code__c = cont.MailingPostalCode,
				                                                    search_country__c = cont.mailingcountry,
				                                                    lookup_by__c = UserInfo.getName() + ' ' + Datetime.now().formatLong(),
				                                                    List_Description__c = 'No match',
																	List_Date__c = null,
																	Result_Node__c = 'No match',
																	Number_of_Hits__c = 0,
																	review_status__c = 'No match',
																	newCallNecessary__c = true
																	);
        insert item;
    
        
        MiscellaneousJob job = new MiscellaneousJob('Bridger', 5);
        database.executebatch(job, 1);
    }    


}