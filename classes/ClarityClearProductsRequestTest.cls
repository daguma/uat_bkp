@isTest
public class ClarityClearProductsRequestTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearProductsRequest ccf = new ClarityClearProductsRequest();
        Test.stopTest();
        
        System.assertEquals(null ,ccf.username);
        System.assertEquals(null , ccf.control_file_substituted);
        System.assertEquals(null , ccf.control_file_name);
        System.assertEquals(null , ccf.control_file_version_number);
        System.assertEquals(null , ccf.group_name); 
        System.assertEquals(null , ccf.account_name);
        System.assertEquals(null , ccf.location_name);
        System.assertEquals(null , ccf.action);
        System.assertEquals(null , ccf.deny_codes);
        System.assertEquals(null , ccf.deny_descriptions);
        System.assertEquals(null , ccf.exception_descriptions);
        System.assertEquals(null , ccf.filter_codes);
        System.assertEquals(null , ccf.filter_descriptions);
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearProductsRequest ccf = new ClarityClearProductsRequest();
        Test.stopTest();
        
        ccf.username = 'test';
        ccf.control_file_substituted = 'test';
        ccf.control_file_name = 'test';
        ccf.control_file_version_number = 1;
        ccf.group_name = 'test';
        ccf.account_name = 'test';
        ccf.location_name = 'test';
        ccf.action = 'test';
        ccf.deny_codes = 'test';
        ccf.deny_descriptions = 'test';
        ccf.exception_descriptions = 'test';
        ccf.filter_codes = 'test';
        ccf.filter_descriptions = 'test';
        
        System.assertEquals('test', ccf.username);
        System.assertEquals('test', ccf.control_file_substituted);
        System.assertEquals('test', ccf.control_file_name);
        System.assertEquals(1, ccf.control_file_version_number);
        System.assertEquals('test', ccf.group_name); 
        System.assertEquals('test', ccf.account_name);
        System.assertEquals('test', ccf.location_name);
        System.assertEquals('test', ccf.action);
        System.assertEquals('test', ccf.deny_codes);
        System.assertEquals('test', ccf.deny_descriptions);
        System.assertEquals('test', ccf.exception_descriptions);
        System.assertEquals('test', ccf.filter_codes);
        System.assertEquals('test', ccf.filter_descriptions);
        
    }
}