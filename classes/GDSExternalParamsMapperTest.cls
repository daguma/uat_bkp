@isTest public class GDSExternalParamsMapperTest {
    @isTest static void get_mapped_information() {
        Opportunity application = LibraryTest.createApplicationTH();
        
        GDS_Request_Params__c paramCreatedDate = new GDS_Request_Params__c();
        paramCreatedDate.ObjectName__c = 'Opportunity';
        paramCreatedDate.PropertyName__c ='CreatedDate';
        paramCreatedDate.ExternalName__c ='LP_SF_ApplicationCreatedDate';
        paramCreatedDate.IsDuplicated__c = false;
        paramCreatedDate.IsActive__c = true;
        insert paramCreatedDate; 
        
        GDS_Request_Params__c paramCreatedDateTime = new GDS_Request_Params__c();
        paramCreatedDateTime.ObjectName__c = 'Opportunity';
        paramCreatedDateTime.PropertyName__c ='CreatedDate';
        paramCreatedDateTime.ExternalName__c ='LP_SF_ApplicationCreatedDate_Time__c';
        paramCreatedDateTime.IsDuplicated__c = true;
        paramCreatedDateTime.IsActive__c = true;
        insert paramCreatedDateTime; 
        
        GDS_Request_Params__c paramAccount = new GDS_Request_Params__c();
        paramAccount.ObjectName__c = 'Account';
        paramAccount.PropertyName__c ='Channel__c';
        paramAccount.ExternalName__c ='LP_SF_Partner_Channel';
        paramAccount.IsDuplicated__c = false;
        paramAccount.IsActive__c = true;
        insert paramAccount; 
       
        System.assertEquals(3, GDSExternalParamsMapper.mappInformation(application.Id, application.Contact__c).size());
    }
}