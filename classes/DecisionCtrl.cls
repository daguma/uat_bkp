public with sharing class DecisionCtrl {


@AuraEnabled
    public static void createFundingApproval(String oppId, String oppName, String description) {

        
        Id groupId = [SELECT Id FROM Group where Name = 'Funding Approval'].Id;
        Case newCase = new Case();
        newCase.Opportunity__c = oppId;
        newCase.Status = 'New';
        newCase.Subject = 'Funding Approval';
        newCase.Description = oppName+ ' ' + description;
        newCase.OwnerId = groupId;
        insert newCase;

        List <GroupMember> dataEntries = [SELECT Id, UserOrGroupId, GroupId FROM GroupMember WHERE GroupId =: groupId];
        Set <Id> userIds = new Set <Id> ();

        for (GroupMember member: dataEntries) {
            userIds.add(member.UserOrGroupId);
        }
        
        for (Id userId: userIds) { 
            salesForceUtil.EmailByUserId(newCase.Subject, newCase.Description, userId);
        }                  
    }
    @AuraEnabled
    public static DecisioningEntity getDecisioningEntity(Id oppId) {
        DecisioningEntity decisioningEntity = new DecisioningEntity();

        try {
            if (String.isNotEmpty(oppId))             
                decisioningEntity = new DecisioningEntity(new DecisioningCtrl(oppId));

        } catch (Exception e) {
            system.debug('DecisionCtrl.getDecisioningEntity : ' + e.getmessage()+ '\n' + e.getStackTraceString());
        }
        System.debug(decisioningEntity);
        System.debug(decisioningEntity.clearRecentHistoryList);
        return decisioningEntity;
    }

    @AuraEnabled
    public static void overrideDisqualifier(Opportunity opp, Long reportId, Boolean displayOldView) {
        ProxyApiCreditReportEntity creditReportEntity;
        boolean updateApp = false;
        String decisionString = '';
        /* BRMS Change */
        if (!displayOldView) {
            creditReportEntity = ProxyApiCreditReport.approvedCreditReportAttributesNew(UserInfo.getName(), reportId);

            if (creditReportEntity != null) {
                //update attachment
                CreditReport.set(creditReportEntity.entityId, JSON.serialize(creditReportEntity));

                //score card processing
                ScorecardUtil.scorecardProcess(creditReportEntity);
                //MER-133
                List<id> oppList= new List<id>();
                oppList.add(opp.id);
                MAP<id, decimal> OppMap = new MAP<id, decimal>();
                List<offer__c> offList= new list<Offer__c>();
                OppMap = MelisaCtrl.returnMaxLoanAmount(oppList, offList);
                if(!oppMap.isEmpty() && OppMap.containsKey(opp.id)){
                    opp.Maximum_Loan_Amount__c =OppMap.get(opp.Id);
                    
                }
                
                //Start - SM-976 | Update Application Fields On Offer Deletion or De-selection
                Id selectedOfferId = [SELECT id,Selected_Offer_Id__c FROM Opportunity WHERE Id = : opp.Id].Selected_Offer_Id__c;
                system.debug('---appp2---'+selectedOfferId);
                opp.Selected_Offer_Id__c = selectedOfferId;
                //END - SM-976 | Update Application Fields On Offer Deletion or De-selection
                //do decisioning
                opp = Decisioning.applicationStatusAutomationThruBRMS(opp.Id, creditReportEntity, opp);
                system.debug('---appp3---'+opp);
                updateApp = true;

                //fetch gds configuration settings
                GDS_Configuration__c setting = GDS_Configuration__c.getOrgDefaults();

                GDSRequestParameter gdsRequestParameter = new GDSRequestParameter();

                gdsRequestParameter.apiSessionId = (setting != null) ? setting.SF_Token__c : 'token';
                gdsRequestParameter.organizationId = (setting != null) ? setting.Org_Id__c : '00DU0000000LpAk';
                gdsRequestParameter.organizationURL = (setting != null) ? setting.Org_Native_API_URL__c + '/' + setting.Org_Id__c : 'https://na48.salesforce.com/services/Soap/c/40.0/0DF0B0000001EuX/00DU0000000LpAk';

                String inputData = JSON.serialize(gdsRequestParameter);

                //do logging for override
                if (creditReportEntity.brms <> null) {
                    MelisaCtrl.DoLogging('BRMS logging-Override', 'credit-report-id=' + reportId + '&user-name=' + EncodingUtil.urlEncode(UserInfo.getName(), 'UTF-8') + '&input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8'), JSON.serialize(creditReportEntity.brms), opp.Lead__c, opp.Contact__c);
                }
            }
        } else
                creditReportEntity = CreditReport.approvedAttributes(UserInfo.getName(), reportId);

        if (creditReportEntity != null) {
            if (!displayOldView) {
                if (creditReportEntity.brms <> null) decisionString = creditReportEntity.brms.autoDecision;
                system.debug('Testing creditReportEntity.brms returned from Override Rules==== ' + creditReportEntity.brms);
            } else
                    decisionString = creditReportEntity.automatedDecision;

            Scorecard__c scorecard = ScorecardUtil.getScorecard(creditReportEntity.entityId);
            if (decisionString.equalsIgnoreCase(CreditReport.DECISION_YES) && scorecard != null && scorecard.Grade__c != 'C2') {
                opp.Status__c = 'Credit Qualified';
                opp.Reason_of_Opportunity_Status__c = '';
                if (opp.sub__c != null) opp.sub__c = null;
                updateApp = true;
            }
            system.debug('DG TEST: fine up till this point!!!');
            if (updateApp) update opp;
        }
        /* BRMS Change */
        //showDecision = true;
    }

    @AuraEnabled
    public static DecisioningEntity fetchNewHardCreditPull(String oppId) { // MER-728 Updated the Return type from void to DecisioningEntity and returned the message in case Hardpull Concent not signed.
        DecisioningEntity de = new DecisioningEntity();
        for(Opportunity opp_r : [select id, Permission_to_Get_Hard_Pull__c from opportunity where id=: oppId]){
            if(opp_r.Permission_to_Get_Hard_Pull__c){
                ProxyApiCreditReportEntity creditReportEntity = CreditReport.get(oppId, true, true, true);
                if(creditReportEntity <> null) //OF-73
                    RefinanceNotificationController.sendRBPNotificationEmail(oppId); 
                de.infoMessage = + 'Success';       
            }else{
                de.hasError = true;
                de.errorDetails = 'You cannot perform a hard pull as the Permission to Get Hard Pull flag is unchecked. Please go to the Details Tab, and check the flag if you have the authorization from the applicant and then try again.';
                system.debug('You cannot perform a hard pull as the Permission to Get Hard Pull flag is unchecked. Please go to the Details Tab, and check the flag if you have the authorization from the applicant and then try again.');
            }
        }
        return de;
    }

    @AuraEnabled
    public static void fetchNewSoftCreditPull(Opportunity opp) {

        Email_Age_Settings__c emailAgeSettings = Email_Age_Settings__c.getValues('settings');

        List<DisplayGDSResponseWrapper> finalDisplayList = new List<DisplayGDSResponseWrapper>();

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.get(opp.Id, false, true, true);
        system.debug('-----oppSel---'+opp.Selected_Offer_Id__c);
        //Start - SM-976 | Update Application Fields On Offer Deletion or De-selection
        ID selectedOfferId = [SELECT id,Selected_Offer_Id__c FROM Opportunity WHERE Id = : opp.Id].Selected_Offer_Id__c;
        system.debug('---appp2---'+selectedOfferId+'---selected--'+opp.Selected_Offer_Id__c);
        opp.Selected_Offer_Id__c = selectedOfferId;
        //END - SM-976 | Update Application Fields On Offer Deletion or De-selection
        if (emailAgeSettings.Call_Email_age__c) {
            System.debug('Email Age Setting Called');
            List<Email_Age_Details__c> emailAgeDetails = new List<Email_Age_Details__c>();

            //get the email age results
            emailAgeDetails = EmailAgeStorageController.getEmailAgeRecord(opp.Id);

            if (emailAgeDetails.size() > 0) {
                if (emailAgeDetails[0].is_Email_Age_Timed_Out__c) {
                    emailAgeDetails[0].is_Email_Age_Timed_Out__c = false;
                    update emailAgeDetails[0];

                    //re-calculate the automated deicision
                    String creditReportJsonResponse = creditReportEntity.automatedDecision;
                }
            }
        }
        
        System.debug('==creditReportEntity==' + creditReportEntity);
        if (creditReportEntity != null) {
            if (creditReportEntity.errorMessage == null) {
                Boolean isDocumentinReview = false;
                List<OpportunityFieldHistory> AppStatusList = [SELECT Field, NewValue, OldValue FROM OpportunityFieldHistory WHERE Field = 'Status__c' AND OpportunityId = :opp.Id LIMIT 1];

                for (OpportunityFieldHistory ofh : AppStatusList) {
                    if (ofh.OldValue == 'Documents In Review' || ofh.NewValue == 'Documents In Review')
                        isDocumentinReview = true;
                }

                if (!isDocumentinReview) {
                    if (CreditReport.validateBrmsFlag(opp.Id)) {
                        if (creditReportEntity.automatedDecision.equalsIgnoreCase('Yes')) {
                            Scorecard__c scorecard = ScorecardUtil.getScorecard(creditReportEntity.entityId);
                            if (scorecard.Acceptance__c == 'Y') {
                                opp.Status__c = 'Credit Qualified';
                                opp.Reason_of_Opportunity_Status__c = '';
                                if (opp.sub__c != null) opp.sub__c = null;
                                //change request to have status as 'Credit Approved - No Contact Allowed' even if it is a manual soft pull
                                if (opp.Partner_Account__c <> null && opp.OwnerId == Label.Do_Not_Contact_White_listed_User && opp.Partner_Account__r.Contact_Restricted_Partner__c) {
                                    opp.Status__c = 'Credit Approved - No Contact Allowed';
                                }
                            } else if (scorecard.Acceptance__c == 'N') {
                                opp.Status__c = 'Declined (or Unqualified)';
                                opp.Reason_of_Opportunity_Status__c = 'Updated Scoring – No Offer';
                                if (opp.sub__c != null) opp.sub__c = null;
                            }
                            update opp;
                        } else if (creditReportEntity.automatedDecision.equalsIgnoreCase('No')   ) {
                            if (creditReportEntity.decision)
                                 Decisioning.declineByRiskBand(opp.Id, creditReportEntity);
                            else {
                                opp.Status__c = 'Declined (or Unqualified)';
                                opp.Reason_of_Opportunity_Status__c = 'Did not pass disqualifiers';
                                if (opp.sub__c != null) opp.sub__c = null;
                                update opp;
                            }
                        }
                    } else {
                        system.debug('-use brms : fetchNewSoftCreditPull--');
                        //Do Decisioning Based on BRMS Rules
                        opp = Decisioning.applicationStatusAutomationThruBRMS(opp.Id, creditReportEntity, opp);
                        update opp;
                    }
                }

            }
        }
    }

    @AuraEnabled
    public static void fetchNewEmailAgeResult(Opportunity opp, Long reportId, Boolean displayOldView) {
        //call email age API
        if (!displayOldView) /* BRMS Change */
            EmailAge.getEmailAgeResultUsingSeqId(opp.Id, String.valueof(reportId), true); 
        else EmailAge.getEmailAgeResult(opp.Id, true);
        opp.Email_Age_Manual_Attempt__c = true;
        update opp;
    }
}