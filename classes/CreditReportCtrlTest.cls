@isTest class CreditReportCtrlTest{
	static testMethod void test(){
		Contact con = new Contact(LastName='TestContact',Firstname = 'David',Email = 'test@gmail2.com',MailingState = 'GA');
        insert con;

        Opportunity app = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id);
        insert app;
        
		List<ProxyApiCreditReportRuleEntity> pcrList = new List<ProxyApiCreditReportRuleEntity>();
        ProxyApiCreditReportRuleEntity pcrr = new ProxyApiCreditReportRuleEntity();
        pcrr.deIsDisplayed = 'Yes';
        pcrr.deRuleStatus = 'Pass';
        pcrList.add(pcrr);
        
        List<ProxyApiCreditReportScoringModelEntity> pacrsmeList = new List<ProxyApiCreditReportScoringModelEntity>();
        ProxyApiCreditReportScoringModelEntity pacrsme = new ProxyApiCreditReportScoringModelEntity();
        pacrsme.modelName = 'ModelV2';
        pacrsmeList.add(pacrsme);
        pacrsme = new ProxyApiCreditReportScoringModelEntity();
        pacrsme.modelName = 'ModelV3';
        pacrsmeList.add(pacrsme);
        
        ProxyApiCreditBRMSResponse pcb = new ProxyApiCreditBRMSResponse();
        pcb.rulesList = pcrList;
        pcb.deApplicationRisk = 'test';
        pcb.scoringModels = pacrsmeList;
        
        CreditReportCtrl.getApplicationRisk(pcb);
        
        CreditReportCtrl.getCreditReports(app.Id);
        
		ProxyApiCreditReportEntity pacre = new ProxyApiCreditReportEntity();
        pacre.brms = pcb;
        
        CreditReportCtrl.mapCRtoCreditReportItem(pacre);
        
        CreditReportCtrl.getAllCreditReports(app.Id);
	}
}