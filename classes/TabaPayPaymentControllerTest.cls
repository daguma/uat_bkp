@isTest 
public class TabaPayPaymentControllerTest {
    
    @isTest static void add_card_test(){
        loan__Loan_Account__c loan = LibraryTest.createContractTH();
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);   
        TabaPayPaymentController paymentCtrl = new TabaPayPaymentController(standardCtrl);
        ProxyApiTabaPay.fake_response = '{"SC":"200","EC":"3C3E1401","EM":"card.accountNumber","message":null}';
        paymentCtrl.AddCard();
    }
    
    @isTest static void go_delete_test(){
        loan__Loan_Account__c loan = LibraryTest.createContractTH();
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);   
        TabaPayPaymentController paymentCtrl = new TabaPayPaymentController(standardCtrl);
        ProxyApiTabaPay.fake_response = '{"SC":200,"EC":"0"}';
        paymentCtrl.goDelete(); 
    }
    
    @isTest static void go_proceed_payment_test(){
        loan__Loan_Account__c loan = LibraryTest.createContractTH();
        loan.loan__OT_ACH_Debit_Date__c = Date.today().addDays(15);
        loan.loan__OT_ACH_Payment_Amount__c = 100;
        loan.loan__Last_Accrual_Date__c = Date.today().addDays(5);
        loan.loan__ACH_On__c = false;
        update loan;
        
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);   
        TabaPayPaymentController paymentCtrl = new TabaPayPaymentController(standardCtrl);
        ProxyApiTabaPay.fake_response = '[{"contactId":"0030B00002HNDd4","contractId":null,"cardNo":"XXXXXXXXXXXX9997","expDate":"202202","accountId":"RvwypjPEMIhxfsHsLkujIg","createdDate":"2018-11-23","active":true,"default":false}]';
        paymentCtrl.initialize();
        paymentCtrl.tabaPayAccountId = 'RvwypjPEMIhxfsHsLkujIg';
        paymentCtrl.PaymentAmount = 0.10;
        paymentCtrl.PaymentFee = 0;
        ProxyApiTabaPay.fake_response = '{"SC":200,"EC":"0","transactionID":"CrgDl7HEuKlKelbR1uQCwA","network":"Visa","networkRC":"00","status":"COMPLETED","approvalCode":"152458","AVS":{"codeAVS":"Y"}}';
        if(paymentCtrl.existTabaPayAccount)
        paymentCtrl.goProceedPayment();
    }
    
    @isTest static void get_cardType_test(){
        loan__Loan_Account__c loan = LibraryTest.createContractTH();
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);   
        TabaPayPaymentController paymentCtrl = new TabaPayPaymentController(standardCtrl);
        paymentCtrl.getcardType();
    }
    
    @isTest static void get_list_month_test(){
        loan__Loan_Account__c loan = LibraryTest.createContractTH();
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);   
        TabaPayPaymentController paymentCtrl = new TabaPayPaymentController(standardCtrl);
        paymentCtrl.getlistMonth();
    }
    
    @isTest static void get_list_year_test(){
        loan__Loan_Account__c loan = LibraryTest.createContractTH();
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);   
        TabaPayPaymentController paymentCtrl = new TabaPayPaymentController(standardCtrl);
        paymentCtrl.getlistYear();
    }
    
      @isTest static void changeAuto_PaymentCardTest(){
        loan__Loan_Account__c loan = LibraryTest.createContractTH();
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);   
        TabaPayPaymentController paymentCtrl = new TabaPayPaymentController(standardCtrl);
        ProxyApiTabaPay.fake_response = '{"SC":"200","EC":"3C3E1401","EM":"card.accountNumber","message":null}';
        test.startTest();
        paymentCtrl.changeAutoPaymentCard();
        test.stopTest();
    }
    
    @isTest static void go_schedulePayment_Exeption_test(){
        loan__Loan_Account__c loan = LibraryTest.createContractTH();
        loan.loan__OT_ACH_Debit_Date__c = Date.today().addDays(15);
        loan.loan__OT_ACH_Payment_Amount__c = 100;
        loan.loan__Last_Accrual_Date__c = Date.today().addDays(5);
        loan.loan__ACH_On__c = false;
        update loan;
        
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);
        TabaPayPaymentController paymentCtrl = new TabaPayPaymentController(standardCtrl);
        ProxyApiTabaPay.fake_response = '[{"contactId":"0030B00002HNDd4","contractId":null,"cardNo":"XXXXXXXXXXXX9997","expDate":"202202","accountId":"RvwypjPEMIhxfsHsLkujIg","createdDate":"2018-11-23","active":true,"default":false}]';
        paymentCtrl.initialize();
        paymentCtrl.tabaPayAccountId = 'RvwypjPEMIhxfsHsLkujIg';
        paymentCtrl.PaymentAmount = 0.10;
        paymentCtrl.PaymentFee = 0;
        paymentCtrl.cardForScheduledPayment = 'XXXXXXXXXXXX9997';
        
        paymentCtrl.schedulePayment();
        ProxyApiTabaPay.fake_response = '{"SC":200,"EC":"0","transactionID":"CrgDl7HEuKlKelbR1uQCwA","network":"Visa","networkRC":"00","status":"COMPLETED","approvalCode":"152458","AVS":{"codeAVS":"Y"}}';
        if(paymentCtrl.existTabaPayAccount)
        paymentCtrl.schedulePayment();
        paymentCtrl.getAccountsForScheduling();
    }
   
    
    
}