global class ClarityClearRecentHistory {

    @AuraEnabled public String ruleName {get;set; }
    @AuraEnabled public String ruleDesc {get;set;}
    @AuraEnabled public Double ruleValue{get;set;}
    @AuraEnabled public String ruleCategory{get;set;}
    @AuraEnabled public boolean isRulePass{get;set;}

}