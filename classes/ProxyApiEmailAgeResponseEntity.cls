//EmailAge Response wrapper in ATB response
//EmailAge response is also appended with ATB response.
//This class parses EmailAge Response. 
global class ProxyApiEmailAgeResponseEntity{
   
    //Email Age id
    public Long id {get; set;}
    
    //Email Age Request
    public String requestData {get; set;}
    
    //Email Age emailAgeInquiryId 
    public Long emailAgeInquiryId {get; set;}
    
    //Email Age requestTime
    public Long  requestTime{get; set;}
    
    //Email Age processTime 
    public Long processTime {get; set;}
    
    //Email Age endProcessTime 
    public Long endProcessTime {get; set;}
    
    //Email Age responseData 
    public String responseData {get; set;}
    
  }