public with sharing class ShowAttachmentsCtrl {

  public list<attachment> attachmentsList{get;set;}
  public list<attachmentErrorcls> attachmentErrorList = new list<attachmentErrorcls>();
  
   public ShowAttachmentsCtrl(){   
      string appId = ApexPages.currentPage().getParameters().get('appId');   
      
      attachmentsList = [select id,name,lastmodifiedByid,CreatedById from attachment where parentId =: appId]; 
      
      for(attachment atch: attachmentsList){
          attachmentErrorList.add(new attachmentErrorcls(atch,'--None--'));      
      }   
   }
   
   public list<attachmentErrorcls> getattachmentErrorList(){
       return attachmentErrorList;   
   }
   
   
  public List<SelectOption> getErrorTypes(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None--', '--None--'));
        list<Attachment_Error_Types__c> errorList = Attachment_Error_Types__c.getAll().values();
        
        for(Attachment_Error_Types__c err: errorList){
           options.add(new SelectOption(err.name, err.name));    
        }
        
        return options;
  }
  
  @TestVisible
  public class attachmentErrorcls{
      public attachment attachRow{get;set;}
      public string errortype{get;set;}
      
     @TestVisible
     attachmentErrorcls(attachment attachmentRow, string error){
         attachRow = attachmentRow;
         errortype = error;
     }         
  }
  
  
  public void Save(){
     list<Attachment> listToUpdate = new list<Attachment>();
     
     for(attachmentErrorcls atchErr: attachmentErrorList){
        string extension;
        string Issue = atchErr.errortype;
        if(Issue  <> '--None--'){
            String attachmentName = atchErr.attachRow.Name;
            if(attachmentName.contains('.')) {
                 List<String> ext = attachmentName.split('\\.');
                 extension = ext[ext.size()-1];
                 attachmentName = ext[ext.size()-2];
            } 
            atchErr.attachRow.name = (extension <> null)? attachmentName +'-'+Issue+'.'+extension :attachmentName+'-'+Issue;
            listToUpdate.add(atchErr.attachRow);
        }
     } 
     
     update  listToUpdate;  
  }

}