public class DealAutomaticUserDetails {

    public String userId {get; set;}
    public List<Deal_Automatic_Attributes__c> attributeList {get; set;}

    public DealAutomaticUserDetails() {

    }

    public DealAutomaticUserDetails(String userId,
                                    List<Deal_Automatic_Attributes__c> attributeList) {
        this.userId = userId;
        this.attributeList = attributeList;
    }
}