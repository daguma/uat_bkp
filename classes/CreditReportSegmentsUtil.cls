public virtual class CreditReportSegmentsUtil {

    private static final Integer EX_RISK_MODEL_SEGMENT_ID = 4;
    private static final Integer EX_TRADELINE_SEGMENT_ID = 18;
    private static final Integer EX_INQUIRY_SEGMENT_ID = 41;
    private static final Integer EX_FRAUD_INDICATORS_SEGMENT_ID = 51;
    private static final Integer EX_PROFILE_SUMMARY_SEGMENT_ID = 54;

    private static final Integer TU_RISK_MODEL_SEGMENT_ID = 100;
    private static final Integer TU_TRADELINE_SEGMENT_ID = 85;
    private static final Integer TU_INQUIRY_SEGMENT_ID = 90;
    private static final Integer TU_FRAUD_INDICATORS_SEGMENT_ID = 92;
    private static final Integer TU_PROFILE_SUMMARY_SEGMENT_ID = 81;

    private static final String FRAUD_SHIELD_INDICATOR_03_VALUE = '03';
    private static final String FRAUD_SHIELD_INDICATOR_08_VALUE = '08';
    private static final String FRAUD_SHIELD_INDICATOR_27_VALUE = '27';
    
    /* BRMS Change */
    private static final String FRAUD_SHIELD_INDICATOR_05_VALUE = '05';
    private static final String FRAUD_SHIELD_INDICATOR_14_VALUE = '14';
    private static final String FRAUD_SHIELD_INDICATOR_25_VALUE = '25';
    /* BRMS Change */
    
    private static final String TYPE_TEXT = 'TEXT';
    private static final String TYPE_ANCHOR = 'ANCHOR';

    public virtual class PairValues {

        @AuraEnabled public String key {get; set;}
        @AuraEnabled public String value {get; set;}
        @AuraEnabled public String displayText {get; set;}
        @AuraEnabled public String type {get; set;}

        @AuraEnabled public Boolean isText {
            get {
                return type == CreditReportSegmentsUtil.TYPE_TEXT;
            }
        }

        @AuraEnabled public Boolean isAnchor {
            get {
                return type == CreditReportSegmentsUtil.TYPE_ANCHOR;
            }
        }

        public PairValues(String key, String displayText) {
            this.key = key;
            this.displayText = displayText;
            this.type = CreditReportSegmentsUtil.TYPE_TEXT;
        }

        public PairValues(String key, String value, String displayText, String type) {
            this.key = key;
            this.value = value;
            this.displayText = displayText;
            this.type = type;
        }

    }//class
    
    public virtual class Segment {

        @AuraEnabled public List<String> headersList {get; set;}
        @AuraEnabled public List<SegmentContent> contentList {get; set;}

        public Segment() {
            this.headersList = new List<String>();
            this.contentList = new List<SegmentContent>();
        }

    }//class
    
    public virtual class SegmentContent {

        @AuraEnabled public List<String> valuesList {get; set;}
        @AuraEnabled public List<SubSegment> subSegmentList {get; set;}
        @AuraEnabled public String subSegmentsLink {get; set;}
        @AuraEnabled public List<String> subSegmentsPrefix {get; set;}

        public String subSegmentDetails {
            get {

                String html = '<table style="width:100%">';
                for (Integer i = 0; i < subSegmentList.size(); i++) {
                    html += '<tr><td style="padding-top: 15px;"><b>' + subSegmentsPrefix.get(i) + '</b></td></tr><tr><td><table class="list"><tr>';

                    for (String header : subSegmentList.get(i).headersList) {
                        html += '<td class="colHeadCenter">' + header + '</td>';
                    }
                    html += '</tr>';
                    for (SegmentContent content : subSegmentList.get(i).contentList) {
                        html += '<tr>';
                        for (String value : content.valuesList) {
                            html += '<td class="dataCell"><center>' + value + '</center></td>';
                        }
                        html += '</tr>';
                    }

                    html += '</table></td><tr>';
                }
                html += '</table>';

                return EncodingUtil.base64Encode(Blob.valueOf(html));
            }
        }

        public SegmentContent() {
            this.valuesList = new List<String>();
            this.subSegmentList = new List<SubSegment>();
            this.subSegmentsLink = '';
            this.subSegmentsPrefix = new List<String>();
        }

    }//class
    
    public virtual class SubSegment {

        @AuraEnabled public List<String> headersList {get; set;}

        @AuraEnabled public List<SegmentContent> contentList {get; set;}

        public SubSegment() {
            this.headersList = new List<String>();
            this.contentList = new List<SegmentContent>();
        }
    }//class
    
    public virtual class FraudIndicators {

         @AuraEnabled public String indicator03 {get; set;}
         @AuraEnabled public String indicator08 {get; set;}
         @AuraEnabled public String indicator27 {get; set;}
        
         /* BRMS Change */
         @AuraEnabled public String indicator05 {get; set;}
         @AuraEnabled public String indicator14 {get; set;}
         @AuraEnabled public String indicator25 {get; set;}
        /* BRMS Change */
        
        public FraudIndicators(String indicators) {

            indicator03 = '-';
            indicator08 = '-';
            indicator27 = '-';
            /* BRMS Change */
            indicator05 = '-';
            indicator14 = '-';
            indicator25 = '-';
            /* BRMS Change */

            if (!String.isEmpty(indicators)) {
                indicator03 = indicators.contains(FRAUD_SHIELD_INDICATOR_03_VALUE) ? 'No Match' : 'Match';
                indicator08 = indicators.contains(FRAUD_SHIELD_INDICATOR_08_VALUE) ? 'Match' : 'No Match';
                indicator27 = indicators.contains(FRAUD_SHIELD_INDICATOR_27_VALUE) ? 'Yes' : 'No';
                /* BRMS Change */
                indicator05 = indicators.contains(FRAUD_SHIELD_INDICATOR_05_VALUE) ? 'Yes' : 'No';
                indicator14 = indicators.contains(FRAUD_SHIELD_INDICATOR_14_VALUE) ? 'Yes' : 'No';
                indicator25 = indicators.contains(FRAUD_SHIELD_INDICATOR_25_VALUE) ? 'Yes' : 'No';
                /* BRMS Change */
            }
        }
    }
    
    public static List<CreditReportSegmentsUtil.PairValues> extractMainInformation(ProxyApiCreditReportEntity cr) {

        Opportunity application = null;

        if (CreditReport.applicationExists(cr.entityId)) {
            for (Opportunity app : [SELECT Id, Name, Lending_Account__c, Lending_Account__r.Name, Contact__c, Contact__r.Name
                                                 FROM Opportunity
                                                 WHERE id = : cr.entityId
                                                         LIMIT 1]) {
                application = app;
            }
        }

        List<CreditReportSegmentsUtil.PairValues> result = new List<CreditReportSegmentsUtil.PairValues>();
        result.add(new CreditReportSegmentsUtil.PairValues('Credit Report Name', cr.name));
        result.add(new CreditReportSegmentsUtil.PairValues('Credit Report First Issued Date', cr.creationDate.format('MM/dd/yyyy HH:mm a')));
        result.add(new CreditReportSegmentsUtil.PairValues('Credit Report Last Updated Date', cr.creationDate.format('MM/dd/yyyy HH:mm a')));
        result.add(new CreditReportSegmentsUtil.PairValues('Credit Bureau Name', cr.datasourceName));
        result.add(new CreditReportSegmentsUtil.PairValues('Credit Transaction ID', cr.transactionId));
        //result.add(new CreditReportSegmentsUtil.PairValues('Credit Rating Code Type', ''));
        result.add(new CreditReportSegmentsUtil.PairValues('Automated Decision', cr.automatedDecision));

        if (application != null) {
            Boolean accountExists = application.Lending_Account__c != null;

            result.add(new CreditReportSegmentsUtil.PairValues('Application', application.Id, application.Name, CreditReportSegmentsUtil.TYPE_ANCHOR));
            if (accountExists)
                result.add(new CreditReportSegmentsUtil.PairValues('Account', application.Lending_Account__c, application.Lending_Account__r.Name, CreditReportSegmentsUtil.TYPE_ANCHOR));
            result.add(new CreditReportSegmentsUtil.PairValues('Contact', application.Contact__c, application.Contact__r.Name, CreditReportSegmentsUtil.TYPE_ANCHOR));
        }

        return result;
    }
    
    public static CreditReportSegmentsUtil.Segment extractLiabilities(List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {

        CreditReportSegmentsUtil.Segment result = new CreditReportSegmentsUtil.Segment();

        Integer ID = bureau == 'TU' ? TU_TRADELINE_SEGMENT_ID : EX_TRADELINE_SEGMENT_ID;

        loadSegmentHeaders(result, crSegmentsList, ID);
        loadSegmentContent(result, crSegmentsList, ID);

        return result;

    }

    public static CreditReportSegmentsUtil.Segment extractScores(List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {

        CreditReportSegmentsUtil.Segment result = new CreditReportSegmentsUtil.Segment();

        Integer ID = bureau == 'TU' ? TU_RISK_MODEL_SEGMENT_ID : EX_RISK_MODEL_SEGMENT_ID;

        loadSegmentHeaders(result, crSegmentsList, ID);
        loadSegmentContent(result, crSegmentsList, ID);

        return result;

    }
    
    public static CreditReportSegmentsUtil.Segment extractInquiries(List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {

        CreditReportSegmentsUtil.Segment result = new CreditReportSegmentsUtil.Segment();

        Integer ID = bureau == 'TU' ? TU_INQUIRY_SEGMENT_ID : EX_INQUIRY_SEGMENT_ID;

        loadSegmentHeaders(result, crSegmentsList, ID);
        loadSegmentContent(result, crSegmentsList, ID);

        return result;

    }
    
    public static CreditReportSegmentsUtil.Segment extractSummaries(List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {

        CreditReportSegmentsUtil.Segment result = new CreditReportSegmentsUtil.Segment();

        Integer ID = bureau == 'TU' ? TU_PROFILE_SUMMARY_SEGMENT_ID : EX_PROFILE_SUMMARY_SEGMENT_ID;

        loadSegmentHeaders(result, crSegmentsList, ID);
        loadSegmentContent(result, crSegmentsList, ID);

        return result;

    }

    public static CreditReportSegmentsUtil.FraudIndicators extractFraudIndicators(List<ProxyApiCreditReportSegmentEntity> crSegmentsList) {

        CreditReportSegmentsUtil.Segment fraudServicesIndicatorSegment =
            extracFraudIndicatorSegment(crSegmentsList);

        String fraudIndicatorsString = extractFraudIndicatorsString(fraudServicesIndicatorSegment);

        CreditReportSegmentsUtil.FraudIndicators result =
            new CreditReportSegmentsUtil.FraudIndicators(fraudIndicatorsString);

        return result;
    }
    
    @testVisible
    private static String extractFraudIndicatorsString(CreditReportSegmentsUtil.Segment segment) {

        String result = '';

        if (segment != null) {
            Integer length = segment.headersList.size();

            for (Integer i = 0; i < length; i++) {
                if (segment.headersList.get(i).startsWithIgnoreCase('Indicator')) {
                    String indicator = segment.contentList.get(0).valuesList.get(i);
                    result = result + ',' + indicator;
                }
            }
        }

        return result;
    }
    
    private static CreditReportSegmentsUtil.Segment extracFraudIndicatorSegment(List<ProxyApiCreditReportSegmentEntity> crSegmentsList) {

        CreditReportSegmentsUtil.Segment result = new CreditReportSegmentsUtil.Segment();

        loadSegmentHeaders(result, crSegmentsList, EX_FRAUD_INDICATORS_SEGMENT_ID);
        loadSegmentContent(result, crSegmentsList, EX_FRAUD_INDICATORS_SEGMENT_ID);

        return result;
    }
    
    private static void loadSegmentHeaders(CreditReportSegmentsUtil.Segment result, List<ProxyApiCreditReportSegmentEntity> crSegmentsList, Integer segmentId) {

        Integer id = -1;
        if (crSegmentsList != null) {
            for (ProxyApiCreditReportSegmentEntity item : crSegmentsList) {
                if (item.segmentTypeEntity.parentId == segmentId) {
                    if (id == -1) id = item.id;
                    if (id != item.id) break;

                    result.headersList.add(item.segmentTypeEntity.fieldName);
                }
            }
        }
    }
    
    private static void loadSubSegmentHeaders(CreditReportSegmentsUtil.SubSegment result, List<ProxyApiCreditReportSegmentEntity> crSegmentsList, Integer segmentId) {

        Integer id = -1;
        if (crSegmentsList != null) {
            for (ProxyApiCreditReportSegmentEntity item : crSegmentsList) {
                if (item.Id == segmentId) {
                    if (id == -1) id = item.id;
                    if (id != item.id) break;

                    result.headersList.add(item.segmentTypeEntity.fieldName);
                }
            }
        }
    }
    
    private static void loadSegmentContent(CreditReportSegmentsUtil.Segment result, List<ProxyApiCreditReportSegmentEntity> crSegmentsList, Integer segmentId) {

        Integer id = -1;
        if (crSegmentsList != null) {
            for (ProxyApiCreditReportSegmentEntity item : crSegmentsList) {
                if (item.segmentTypeEntity.parentId == segmentId && id != item.id) {
                    id = item.id;
                    SegmentContent segmentContent = new SegmentContent();

                    for (ProxyApiCreditReportSegmentEntity aux : crSegmentsList) {
                        if (aux.id == id) {
                            segmentContent.valuesList.add(aux.value);
                        }
                    }

                    loadSubSegmentContent(segmentContent, crSegmentsList, id);
                    result.contentList.add(segmentContent);
                }
            }
        }
    }
    
    private static void loadSubSegmentContent(SegmentContent segmentContent, List<ProxyApiCreditReportSegmentEntity> crSegmentsList, Integer segmentParentId) {

        Integer id = -1;
        if (crSegmentsList != null) {
            for (ProxyApiCreditReportSegmentEntity item : crSegmentsList) {
                if (item.parentId == segmentParentId && id != item.id) {
                    id = item.id;
                    segmentContent.subSegmentsLink += ' ' + item.prefix;
                    segmentContent.subSegmentsPrefix.add(item.prefix);

                    SubSegment subSegment = new SubSegment();
                    loadSubSegmentHeaders(subSegment, crSegmentsList, id);

                    SegmentContent subSegmentContent = new SegmentContent();
                    for (ProxyApiCreditReportSegmentEntity aux : crSegmentsList) {
                        if (aux.id == id) {
                            subSegmentContent.valuesList.add(aux.value);
                        }
                    }
                    subSegment.contentList.add(subSegmentContent);
                    segmentContent.subSegmentList.add(subSegment);
                }
            }
        }
    }

    /* BRMS Change */
    public static CreditReportSegmentsUtil.FraudIndicators extractFraudIndicatorsThrBRMS(List<ProxyApiCreditReportRuleEntity> crRulesList) {
        
        String fraudIndicatorsString = '';
        system.debug('Testing crRulesList==== ' +crRulesList);
        for(ProxyApiCreditReportRuleEntity rule: crRulesList) {
            //system.debug('--rule:extractFraudIndicatorsThrBRMS--'+rule);
            If(String.isNotBlank(rule.deRuleNumber) && rule.deRuleNumber.equals(ValidationTabCtrl.getRuleNumber('Address Mismatch')) && String.isNotBlank(rule.deRuleStatus) && rule.deRuleStatus.equalsIgnoreCase('Fail'))
                fraudIndicatorsString = fraudIndicatorsString + ',' + '03' ; 
            If(String.isNotBlank(rule.deRuleNumber) && rule.deRuleNumber.equals(ValidationTabCtrl.getRuleNumber('Fraud Shield Credit Established')) && String.isNotBlank(rule.deRuleStatus) && rule.deRuleStatus.equalsIgnoreCase('Fail'))
                fraudIndicatorsString = fraudIndicatorsString + ',' + '08' ;
            If(String.isNotBlank(rule.deRuleNumber) && rule.deRuleNumber.equals(ValidationTabCtrl.getRuleNumber('SSN')) && String.isNotBlank(rule.deRuleStatus) && rule.deRuleStatus.equalsIgnoreCase('Fail'))
                fraudIndicatorsString = fraudIndicatorsString + ',' + '27' ; 
        }
        system.debug('---extractFraudIndicatorsThrBRMS:fraudIndicatorsString---'+fraudIndicatorsString);
        CreditReportSegmentsUtil.FraudIndicators result = new CreditReportSegmentsUtil.FraudIndicators(fraudIndicatorsString);
        system.debug('---extractFraudIndicatorsThrBRMS:result---'+result);
        return result;
    }
    /* BRMS Change */

}