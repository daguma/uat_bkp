@isTest
public class ClarityClearFraudCrosstabTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearFraudCrosstab ccf = new ClarityClearFraudCrosstab();
        Test.stopTest();
        
        System.assertEquals(null ,ccf.name );
        System.assertEquals(null ,ccf.ssn );
        System.assertEquals(null ,ccf.drivers_license );
        System.assertEquals(null ,ccf.bank_account );
        System.assertEquals(null ,ccf.home_address );
        System.assertEquals(null ,ccf.zip_code );
        System.assertEquals(null ,ccf.home_phone );
        System.assertEquals(null ,ccf.cell_phone );
        System.assertEquals(null ,ccf.email_address );
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearFraudCrosstab ccf = new ClarityClearFraudCrosstab();
        Test.stopTest();
        
        ccf.name = 'test';
        ccf.ssn = 1;
        ccf.drivers_license = 1;
        ccf.bank_account = 1;
        ccf.home_address = 1;
        ccf.zip_code = 1;
        ccf.home_phone = 1;
        ccf.cell_phone = 1;
        ccf.email_address = 1;
        
        System.assertEquals('test' ,ccf.name );
        System.assertEquals(1 ,ccf.ssn );
        System.assertEquals(1 ,ccf.drivers_license );
        System.assertEquals(1 ,ccf.bank_account );
        System.assertEquals(1 ,ccf.home_address );
        System.assertEquals(1 ,ccf.zip_code );
        System.assertEquals(1 ,ccf.home_phone );
        System.assertEquals(1 ,ccf.cell_phone );
        System.assertEquals(1 ,ccf.email_address );
    }
}