@isTest
private class contact_AfterUpdateTest {

    @testSetup static void setups_settings() {
        Java_API_Settings__c javaSettings = new Java_API_Settings__c();
    javaSettings.name = 'JavaSettings';
    javaSettings.Access_Token_EndPoint__c = 'http://testapi.lendingpoint.com/CustomerPortalAPI/oauth/token?grant_type=password';
    javaSettings.clientId__c = 'sfdcclient';
    javaSettings.clientSecret__c = 'fdsbgsfsRjhsdklfhAsfdj5be4Gsdkcx67eH83fYFDUYU';
    javaSettings.Email_Update_Endpoint__c = 'http://testapi.lendingpoint.com/CustomerPortalAPI/api/user/update';
    javaSettings.Mail_Receive_EndPoint__c = 'https://testapi.lendingpoint.com/CustomerPortalAPI/api/mail/receive';
    javaSettings.Password__c = 'l3ndiN8p0IntS50cU8Er';
    javaSettings.User_Create_Endpoint__c = 'https://testapi.lendingpoint.com/CustomerPortalAPI/api/user';
    javaSettings.Username__c = 'sfdcapi';
    insert javaSettings;
        
        StateIndexUR__c s = new StateIndexUR__c();
        s.UnemploymentRate__c = 4.00;
        s.State__c = 'GA';
        s.ZipCode__c = '';
        s.County__c = '040';
        insert s;
    }
    
  @isTest static void updates_email_to_customer_portal() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {

            Contact c = LibraryTest.createContactTH();
    
            c.Email = 'newmail@test.com';
            update c;
    
            c = [SELECT Email FROM Contact WHERE Id = : c.Id LIMIT 1];
    
            System.assertEquals('newmail@test.com', c.Email);
        }
  }

  @isTest static void inserts_a_note_when_email_change() {
       
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
        
            Test.startTest();
            
            Opportunity app = LibraryTest.createApplicationTH();
    
            Contact c = [SELECT id, Email FROM Contact WHERE Id = : app.Contact__c LIMIT 1];
    
            Integer contactNotes = [SELECT COUNT() FROM Note WHERE ParentId = : c.Id];

            c.Email = 'newmail@test.com';
            update c;
    
            Test.stopTest();
            
            Integer contactNotesAfter = [SELECT COUNT() FROM Note WHERE parentId = : c.Id];
    
            // Need to disable assertion on Alpha, as the process builder causes errors whne running on deployment.
//            System.assert(contactNotesAfter > contactNotes);
        }
  }
    
    @isTest static void fills_unemployment_rate(){
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {
        
            Test.startTest();
            
          Contact c = LibraryTest.createContactTH();
            
            Test.stopTest();
        
          System.assertEquals(4.00, [SELECT UnemploymentRate__c FROM Contact WHERE Id =: c.Id LIMIT 1].UnemploymentRate__c);
        }
    }
    
    @isTest static void updates_unemployment_rate(){
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        StateIndexUR__c s = new StateIndexUR__c();
        s.UnemploymentRate__c = 5.00;
        s.State__c = 'CA';
        s.ZipCode__c = '';
        s.County__c = '040';
        insert s;

        System.runAs(thisUser) {
        
            Test.startTest();
        
            Contact c = LibraryTest.createContactTH();
            c.UnemploymentRate__c = null;
            c.MailingState = 'CA';
            update c;
            
            Test.stopTest();
            
            System.assertEquals(5.00, [SELECT UnemploymentRate__c FROM Contact WHERE Id =: c.Id LIMIT 1].UnemploymentRate__c);
        }
    }

}