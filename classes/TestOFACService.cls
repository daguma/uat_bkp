@isTest
public class TestOFACService{
     public static testMethod void VerifyOFACService(){
        
        Contact c = TestHelper.createContact();
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('contactid',c.id); 
        gen.writeEndObject();                                                            
        string req = gen.getAsString();
        
        OFACService.verifyWatchDog(req);     
     }               
}