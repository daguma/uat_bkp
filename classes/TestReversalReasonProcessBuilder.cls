@isTest

public class TestReversalReasonProcessBuilder {
    
    private static testMethod void test(){
       loan.TestHelper.createSeedDataForTesting();
        //As a result of moving validation from code to validation rule, updating org Params
        loan.TestHelper.useCLLoanCRM();
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        Loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        Loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        Loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        MaskSensitiveDataSettings__c mds = new MaskSensitiveDataSettings__c();
        mds.MaskAllData__c = true;
        insert mds;    
        
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                dummyAccount, 
                curr, 
                dummyFeeSet);

        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        User u = loan.TestHelper.createUser('testUser1', 'MyOffice1');
        
        Contact dummyContact = TestHelperForManaged.createContact('dummyLA','Other','87123','Test State');
        
        Account dummyAcc = new Account();
        dummyAcc.Name = 'DummyAccount';
        dummyAcc.c2g__CODABankName__c = 'Test Bank';
        dummyAcc.c2g__CODABankAccountNumber__c = '123456';
        dummyAcc.c2g__CODAPaymentMethod__c = 'Cash';
        
        insert dummyAcc;
        
        dummyContact.AccountId = dummyAcc.Id;
        dummyContact.OwnerId = u.Id;
        update dummyContact;
        
         //Create a dummy Loan Account
        loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccountForContact(dummyLP,
                                                    dummyContact,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
        Date da = date.newInstance(2016, 12, 30);
        loanAccount.loan__Loan_Amount__c = 10000;
        loanAccount.loan__Principal_Remaining__c = 10000;
        loanAccount.loan__Loan_Status__c = 'Active - Good Standing';
        loanAccount.loan__Last_Accrual_Date__c = Da;
        loanAccount.loan__Interest_Remaining__c = 0.0;
        loanAccount.loan__Fees_Remaining__c = 0.0;
        update loanAccount;
        
        list<loan__Payment_Mode__c> pMode = [select ID from loan__Payment_Mode__c limit 1];
        
        
        loan__Loan_Payment_Transaction__c payment = new loan__Loan_Payment_Transaction__c();
        payment.loan__Cleared__c = TRUE;
        //payment.Is_Latest__c = FALSE;
        //payment.loan__Interest__c = 50;
        payment.loan__Loan_Account__c = loanAccount.id;
        //payment.loan__Principal__c = 100;
        payment.loan__Payment_Mode__c = pmode.get(0).id;
        payment.loan__Receipt_Date__c = da;
        //payment.loan__Reversed__c = TRUE;
        payment.loan__Transaction_Amount__c = 150;
        payment.loan__Transaction_Date__c = da;
        insert payment;
        System.debug('Payment--- ' + payment);
        
        Date da1 = date.newInstance(2017, 1, 31);
        loan__Repayment_Transaction_Adjustment__c reversal = new loan__Repayment_Transaction_Adjustment__c();
        reversal.loan__Reason_Code__c = 'Other';
        reversal.loan__Loan_Payment_Transaction__c = payment.id;
        //reversal.loan__Loan_Payment_Transaction__r.loan__Loan_Account__c = loanAccount.id;
        reversal.loan__Adjustment_Txn_Date__c = da1;
        reversal.loan__Cleared__c = TRUE;
        insert reversal;
        
        
        list<id> idVal = new list<id>();
        idVal.add(reversal.id);
        
        Test.startTest();
        
        ReversalReasonProcessBuilder.ReversalReason(idVal);
        
        Test.stopTest();
        
    }

}