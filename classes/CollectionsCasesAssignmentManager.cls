public virtual class CollectionsCasesAssignmentManager {

    public static final String DELETE_JOB_QUERY = 'SELECT Id FROM CollectionsCasesAssignment__c';
    public static final String DELETE_JOB = 'Delete_CollectionCasesAssignment';
    
    public static final String ANALYZE_JOB_QUERY = 'SELECT Id, loan__Contact__r.Work_Phone_Number__c, loan__OT_ACH_Debit_Date__c, loan__Oldest_Due_Date__c, Overdue_Days__c, loan_Active_Payment_Plan__c, DMC__c, Collection_Case_Owner__c, Is_Payment_Pending__c, First_Payment_Missed__c, Product_Name__c ' +
            ' FROM loan__Loan_Account__c ' +
            ' WHERE ((Overdue_Days__c >=1 AND Overdue_Days__c < 180) ' +
            '     OR (Overdue_Days__c = 0 AND Collection_Case_Owner__r.Name != \'HAL\' AND Collection_Case_Owner__c != null ) ' +
            '     OR (Overdue_Days__c = 0 AND loan_Active_Payment_Plan__c = true)) ' +
            ' AND loan__Loan_Status__c NOT IN (\'Active - Marked for Closure\', \'Closed - Obligations met\', \'Closed- Written Off\', \'Canceled\') ' +
            ' AND Charge_Off_Sold_To__c = null AND Outside_Collection_Agency__c = null ' +
            ' AND date_of_settlement__c = null';
    public static final String ANALYZE_JOB = 'Analyze_CollectionCasesAssignment';

    public static final String ASSIGN_JOB_QUERY = 'SELECT Id, UpdateCase__c, CreateCase__c, CaseCreated__c, Assignment_Result__c, Assigned_to__c, caseOwnerNewId__c, Case__c, Case__r.OwnerId, CL_Contract__c, CL_Contract__r.name, CL_Contract__r.Opportunity__c, CL_Contract__r.loan__Contact__c, CL_Contract__r.Collection_Case_Owner__c, loadBalanceGroup__c ' +
            ' FROM CollectionsCasesAssignment__c ' +
            ' WHERE Assignment_Result__c = NULL OR Assignment_Result__c = \'\'';
    public static final String ASSIGN_JOB = 'Assign_CollectionCasesAssignment';

    public static String getQueryforJob(String job) {
        String query = '';
        if (DELETE_JOB.equals(job)) {
            query = DELETE_JOB_QUERY;
        } else if (ANALYZE_JOB.equals(job)) {
            query = ANALYZE_JOB_QUERY;
        } else if (ASSIGN_JOB.equals(job)) {
            query = ASSIGN_JOB_QUERY;
        }
        return query;
    }

    public static Database.QueryLocator start(Database.BatchableContext BC, CollectionsCasesAssignmentJob job){
        if(job.collectionsUsersStructure == null) job.collectionsUsersStructure = new CollectionsCasesAssignmentUsersStructure();
        if(job.auditProcess == null) job.auditProcess = new CollectionsCasesAssignmentAudit();
        if(job.collectionsCasesAssignmentHelper == null) job.collectionsCasesAssignmentHelper = new CollectionsCasesAssignmentHelper();
        if(ASSIGN_JOB.equals(job.jobName)) {
            job.collectionsAssignmentStructure = new CollectionsCasesAssignmentStructure(job.collectionsUsersStructure.teamsMap);
            job.auditProcess.emailAuditors('Collections Cases Assignment: Assignment Structure: ',JSON.serialize(job.collectionsAssignmentStructure));
        }
        return Database.getQueryLocator(job.query);
    }

    public static void executeBatchableContext( Database.BatchableContext BC, List<Object> scope, CollectionsCasesAssignmentJob job) {
        if (DELETE_JOB.equals(job.jobName)) {
            if (scope != null) {
                if (!scope.isEmpty()) {
                    delete (List<CollectionsCasesAssignment__c>) scope;
                }
            }
        } else if (ANALYZE_JOB.equals(job.jobName)) {
            CollectionsCasesAssignmentLogic.analyzeAssignments( (List<loan__loan_account__c>) scope, job.collectionsUsersStructure, job.auditProcess, job.collectionsCasesAssignmentHelper);
        } else if (ASSIGN_JOB.equals(job.jobName)) {
            CollectionsCasesAssignment.assignAndCommitCases((List<CollectionsCasesAssignment__c>) scope, job.collectionsAssignmentStructure, job.auditProcess);
        }
    }

    public static void executeSchedulableContext(SchedulableContext sc, CollectionsCasesAssignmentJob job) {
        if (DELETE_JOB.equals(job.jobName)) {
            database.executebatch(job, 145);
        } if (ANALYZE_JOB.equals(job.jobName)){
            database.executebatch(job, 50);
        }
    }

    public static void finish(Database.BatchableContext BC, CollectionsCasesAssignmentJob job) {
        if (job.runNextProcess) {
            if (DELETE_JOB.equals(job.jobName)) {
                job.jobName = ANALYZE_JOB;
                job.query += ' AND Id IN (\'a1u0B000006FDF9QAO\',\'a1u0B000006F5PjQAK\') ';
                job.query += ' LIMIT 4000';
                database.executebatch(job, 50);
            }else if(ANALYZE_JOB.equals(job.jobName)){
                job.jobName = ASSIGN_JOB;
                job.query += ' LIMIT 1';
                database.executebatch(job, 50);
            }else if(ASSIGN_JOB.equals(job.jobName)){
                //Schedule the job again for the contracts that were not analyzed.
                //This will also ASSIGN the CollectionsCasesAssignment__c records that were not processed by the previous run(s).
                //This new scheduled run will NOT reassign the cases from the previous run.
                if(!job.auditProcess.failedAnalyzedContracts.isEmpty()){
                    if(!job.hasAlreadyBeenScheduledToday){
                        job.scheduleAnalyzeJobForFailures();
                    }else{
                        job.auditProcess.sendAuditReport(job.collectionsUsersStructure, job.collectionsAssignmentStructure);
                    }
                }else{
                    job.auditProcess.sendAuditReport(job.collectionsUsersStructure, job.collectionsAssignmentStructure);
                }
            }
        }
    }
}