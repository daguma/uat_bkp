public with sharing class ProxyApiAON {

    private static final String ENROLLMENT_URL = '/v101/aon/enrollment/';
    private static final String CANCELLATION_URL = '/v101/aon/cancellation/';
    private static final String BILLING_URL = '/v101/aon/billing/';
    private AONObjectsParameters aonObjectsParemeters = new AONObjectsParameters();

    public ProxyApiAON() {}

    public static String enrollment(String jsonInputData){
        return (ProxyApiBRMSUtil.ValidateBeforeCallApi() && jsonInputData != null) ? ProxyApiBRMSUtil.ApiGetResult(ENROLLMENT_URL, 'input-data=' + EncodingUtil.urlEncode(jsonInputData, 'UTF-8')) : null;
    }

    public static String billing(String jsonInputData){
         return (ProxyApiBRMSUtil.ValidateBeforeCallApi() && jsonInputData != null) ? ProxyApiBRMSUtil.ApiGetResult(BILLING_URL, 'input-data=' + EncodingUtil.urlEncode(jsonInputData, 'UTF-8')) : null;
    }

    public static String cancellation(String jsonInputData){
        return (ProxyApiBRMSUtil.ValidateBeforeCallApi() && jsonInputData != null) ? ProxyApiBRMSUtil.ApiGetResult(CANCELLATION_URL, 'input-data=' + EncodingUtil.urlEncode(jsonInputData, 'UTF-8')) : null;
    }

}