public class SimulationRequestUtil {
	@invocableMethod
    public static void runSimulationRequest(list<genesis__applications__c> AppList) {

        genesis__Applications__c app = AppList.get(0);

        simulationRequest(app.Id);

    }

    @future (callout = true)
    public static void simulationRequest(String appID){

        System.debug('appID: '+ appID);

        genesis__Applications__c app = new genesis__Applications__c();
        List<genesis__Applications__c> appsToUpdate = new List<genesis__Applications__c>();

        String oldStatus = app.genesis__Status__c;
        app.id = appID;

        Boolean acceptance = true;
        Scorecard__c scorecard = null;
        
        if (Test.isRunningTest() && ScorecardUtil.getScorecard(app.Id) != null){
			acceptance = false;      
            scorecard = ScorecardUtil.getScorecard(app.Id);
        }
        
        ProxyApiCreditReportEntity creditReportEntity = CreditReport.get(appId, false, true, true);

        if (creditReportEntity != null){

            System.debug('creditReportEntity is not null');

            if (creditReportEntity.errorMessage == null){      
                if(CreditReport.validateBrmsFlag(app.Id)){
                    if (creditReportEntity.automatedDecision.equalsIgnoreCase('Yes')) {

                        if (acceptance)
                        	scorecard = ScorecardUtil.getScorecard(creditReportEntity.entityId);

                        if (scorecard.Acceptance__c == 'Y'){
                            app.genesis__Status__c = 'Credit Qualified';
                            app.Credit_Declined_Reason__c = '';
                        }else if (scorecard.Acceptance__c == 'N'){
                            app.genesis__Status__c = 'Declined (or Unqualified)';
                            app.Credit_Declined_Reason__c = 'Updated Scoring – No Offer';
                        }

                        update app;
                    
                    }else if (creditReportEntity.automatedDecision.equalsIgnoreCase('No') && creditReportEntity.decision == false){

                        app.genesis__Status__c = 'Declined (or Unqualified)';
                        app.Credit_Declined_Reason__c = 'Did not pass disqualifiers';

                        update app;
                        
                    }else if (creditReportEntity.automatedDecision.equalsIgnoreCase('No') && creditReportEntity.decision){
                        Decisioning.declineByRiskBand(app.Id, creditReportEntity);
                    }
                }else{

                    app = Decisioning.applicationStatusAutomationThruBRMS(app.Id, creditReportEntity, app);

                    if (app.genesis__Status__c != null && oldStatus != app.genesis__Status__c ) update app;
                }              
            }
        } 
    }

}