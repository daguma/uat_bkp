@isTest
public class ClarityClrFraudIdentityVerificationTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearFraudIdentityVerification ccf = new ClarityClearFraudIdentityVerification();
        Test.stopTest();
        
        system.assertEquals(null, ccf.overall_match_result);
        system.assertEquals(null, ccf.ssn_name_match);
        system.assertEquals(null, ccf.name_address_match);
        system.assertEquals(null, ccf.ssn_Dob_Match);
        system.assertEquals(null, ccf.overall_match_reason_code);

    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearFraudIdentityVerification ccf = new ClarityClearFraudIdentityVerification();
        Test.stopTest();
        
        ccf.overall_match_result = 'test';
        ccf.ssn_name_match = 'test';
        ccf.name_address_match = 'test';
        ccf.ssn_Dob_Match = 'test';
        ccf.overall_match_reason_code = 'test';
        
        system.assertEquals('test', ccf.overall_match_result);
        system.assertEquals('test', ccf.ssn_name_match);
        system.assertEquals('test', ccf.name_address_match);
        system.assertEquals('test', ccf.ssn_Dob_Match);
        system.assertEquals('test', ccf.overall_match_reason_code);
    }
}