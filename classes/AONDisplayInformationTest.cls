@isTest
private class AONDisplayInformationTest {

    @isTest static void displays_information() {
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        
        loan_AON_Information__c aonInfo = new loan_AON_Information__c();
        aonInfo.Product_Code__c = 'LPOPTION01';
        aonInfo.Enrollment_Date__c = System.now();
        aonInfo.Cancellation_Date__c = System.now();
        aonInfo.Claim_Number__c = '0123456';
        aonInfo.Claim_Status__c = 'A';
        aonInfo.Claim_Disposition__c = 'Test';
        aonInfo.Claim_Status_Date__c = System.now();
        aonInfo.Contract__c = contract.Id;
        insert aonInfo;
        
        AONDisplayInformation dispInfo = new AONDisplayInformation();
        String result = AONDisplayInformation.getAllInfo(contract.Id);
        
        System.assert(result.containsIgnoreCase('0123456'), 'Display Information');
    }
    
}