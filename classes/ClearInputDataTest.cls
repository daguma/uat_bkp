@isTest public class ClearInputDataTest {

    @isTest static void validates() {
        ClearInputData cid = new ClearInputData();

        cid.clearInputParameter = new ClearInputParameter();
        cid.courtRequestParameter = new PersonRequestParameter();

        System.assertNotEquals(null, cid.clearInputParameter);
        System.assertNotEquals(null, cid.courtRequestParameter);
    }

}