public class userNamePasswordUtilityClass{
    
    static integer errorCount = 0;
    
    /**This Method is called by Application Trigger once any Application is Funded.
It will generate UserName and Password for Customer Portal for the funded User**/
    @future(callout=true)
    Public static void sendUserToCustomerPortal(string appId){
        
       /* Logging__c  finalLogRecord;
        
        try{ 
            Java_API_Settings__c javaDetails = Java_API_Settings__c.getInstance('JavaSettings');
            string userCreateEndPoint = javaDetails.User_Create_Endpoint__c, accessToken = EmailUtility.hitAccessTokenAPI(javaDetails);                                 
            System.debug('userCreateEndPoint======================= '+userCreateEndPoint);       
            
            if(accessToken <> null)
                finalLogRecord = hitUserCreateAPI(accessToken,appId,userCreateEndPoint); 
            System.debug('finalLogRecord======================= '+finalLogRecord);
            if(finalLogRecord <> null){ insert finalLogRecord;}                                              
        }
        catch(Exception e ){
            System.debug('Exception================ '+ e.getMessage());
            WebToSFDC.notifyDev('Send User To CustomerPortal ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() ) ;
        }  */
        userNamePasswordUtilityClass.sendUserToCP(appId);       
    } 
    /**This Method is called by addProvider API once any opportunity is Funded.
It will generate UserName and Password for Customer Portal for the funded User**/
    Public static void sendUserToCP(string appId){
        
        Logging__c  finalLogRecord;
        
        try{ 
            Java_API_Settings__c javaDetails = Java_API_Settings__c.getInstance('JavaSettings');
            string userCreateEndPoint = javaDetails.User_Create_Endpoint__c, accessToken = EmailUtility.hitAccessTokenAPI(javaDetails);                                 
            System.debug('userCreateEndPoint======================= '+userCreateEndPoint);       
            
            if(accessToken <> null)
                finalLogRecord = hitUserCreateAPI(accessToken,appId,userCreateEndPoint); 
            System.debug('finalLogRecord======================= '+finalLogRecord);
            if(finalLogRecord <> null){ insert finalLogRecord;}                                              
        }
        catch(Exception e ){
            System.debug('Exception================ '+ e.getMessage());
            WebToSFDC.notifyDev('Send User To CustomerPortal ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() ) ;
        }         
    } 
    
    /**This method is to post the UserName and Password of Funded User to Java Customer Portal DB.**/
    @TestVisible
    private static Logging__c  hitUserCreateAPI(String accessToken,string applicationId,string endPoint){
        Logging__c logRecord;
        string contactId, email, passWord, firstName, lastName, jsonRequest; 
        boolean isUserCreated = false;       
        
        try{
            for (Opportunity theApp : [select id,status__c,Contact__r.email,Contact__c, Contact__r.firstName, Contact__r.lastName,(select id,Customer_Portal_Status__c,createddate,status__c from CL_Contracts__r order by createddate desc) from Opportunity  where id =:applicationId]) { //CPS-56
                
                contactId = theApp.Contact__c;
                email     = theApp.Contact__r.email;
                firstName = theApp.Contact__r.firstName;
                lastName  = theApp.Contact__r.lastName;
                passWord  = getPassword();
                
                Map<string,string> requestMap = new Map<string,string>();
                requestMap.put('contactId',contactId);
                requestMap.put('email',email);
                requestMap.put('password',passWord);
                requestMap.put('firstName',firstName);
                requestMap.put('lastName',lastName);
                requestMap.put('applicationId',theApp.Id); //CPS-56
                requestMap.put('isFunded',(String.isNotEmpty(theApp.status__c) && theApp .status__c == 'Funded') ? 'true' : 'false'); //CPS-56
                jsonRequest = Json.SerializePretty(requestMap);
                
                HttpRequest req = new HttpRequest(); 
                req.setEndPoint(EndPoint); 
                
                system.debug('===========accessToken=========='+accessToken);        
                req.setHeader('Authorization', 'Bearer '+accessToken);
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('Accept', 'application/json');
                req.setHeader('Origin',Label.Origin_Java);
                req.setMethod('POST');            
                req.setBody(jsonRequest);
                
                System.debug('Request ==============='+req);
                System.debug('Request ==============='+req.getBody());
                Http sendHttp = new Http();
                HTTPResponse res;                
                
                if(!Test.isRunningTest()){
                    res =  sendHttp.send(req);
                    
                }else{
                    res = new HTTPResponse();                    
                    res.SetBody(TestHelper.createDomDocNew().toXmlString());               
                }       
                
                System.debug('Response======================= '+res.getBody());                       
                integer statusCode = res.getstatusCode();
                
                System.debug('statusCode======================= '+statusCode);
                
                if(statusCode == 401 && errorCount < 4){
                    Map<string,string> responseMap = (Map<String,String>) JSON.deserialize(res.getBody(), Map<String,String>.class);
                    if(responseMap.containsKey('error')  && responseMap.get('error') == 'invalid_token'){
                        errorCount++;
                        sendUserToCustomerPortal(applicationId);
                    }
                    
                }else if(statusCode == 412){                              
                    logRecord = EmailUtility.getLoggingRecord('Customer Portal User Create Validation Error',jsonRequest,res.Getbody(),contactId);                      
                    
                }else if(statusCode == 200){
                    Map<string,string> responseMap = (Map<String,String>) JSON.deserialize(res.getBody(), Map<String,String>.class);
                    boolean isExistingUser = responseMap.containskey('existingUser') ? Boolean.valueOf(responseMap.get('existingUser')) : false;    //API-410
                    if(responseMap.containsKey('status')){ 
                        if(responseMap.get('status') == 'ERR' || responseMap.get('status') == 'VER'){
                            logRecord = EmailUtility.getLoggingRecord('Customer Portal User Create Error',jsonRequest,res.Getbody(),contactId); 
                            
                        }else if(responseMap.get('status') == '200'&& !isExistingUser){    //API-410
                            isUserCreated = true;
                            string loginURL = responseMap.get('loginUrl');
                            System.debug('loginURL======================= '+loginURL);    
                            sendpassword(contactId,email,passWord,loginURL);                              
                        }
                    }               
                }
                
                
                if(isUserCreated){  updateApp(theApp,'User Created Successfully');}                       
                else{ if(theApp.CL_Contracts__r.size() > 0 && String.isEmpty(theApp.CL_Contracts__r[0].Customer_Portal_Status__c)){ updateApp(theApp,'User is not created through API'); } }
                
                // commented for testing  
                
            }
        }catch(Exception e ){
            System.debug('Exception================ '+ e.getMessage());
            WebToSFDC.notifyDev('Send User To CustomerPortal ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() ) ;
        }  
        
        return logRecord; 
    } 
    
    @future(callout=true)
    public static void hitMerchantCreateAPI(string accId){
        Logging__c logRecord;
        String JSONString;
        boolean IsSandBox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        string contactId, email, passWord, firstName, lastName, jsonRequest; 
        Map<string,string> requestMap = new Map<string,string>();
        string isActive;
        List<account> accListToUpdate= new list<account>();
        integer merchantId;
        Mule_Settings__c muleDetails = Mule_Settings__c.getInstance();
        try{
            for (Account theAcc : [select id, RMC_Email__c, Active_Partner__c, Merchant_User_ID__c, name from Account where id =:accId]) { 
                AccId     = theAcc.id;
                email     = theAcc.RMC_Email__c;
                isActive  = string.valueOf(theAcc.Active_Partner__c);
                passWord  = getPassword();
                string endPoint = (IsSandBox ?muleDetails.Dev_Base_URL__c : muleDetails.Base_URL__c)+muleDetails.User_Create_Endpoint__c; 
                system.debug('==EndPoint=='+endpoint);
                JSONGenerator gen=JSON.createGenerator(true);
                gen.writeStartObject();
                gen.writeStringField('username',theAcc.RMC_Email__c);
                gen.writeStringField('password',passWord);
                gen.writeFieldName('scope');
                gen.writeStartObject();
                gen.writeStringField('accountId',theAcc.id);
                gen.writeStringField('merchantName',theAcc.name);
                gen.writeStringField('isActive',isActive); 
                gen.writeEndObject();
                gen.writeFieldName('userApplication');
                gen.writeStartObject(); 
                gen.writeNumberField('applicationId',integer.valueOf(muleDetails.User_Application_Id__c));
                gen.writeNumberField('roleId',integer.valueOf(muleDetails.Role_Id__c));
                gen.writeStringField('environment',muleDetails.Environment__c);
                gen.writeEndObject();    
                gen.writeEndObject();
                JSONString = gen.getAsString();
                HTTPResponse res=sendRequest(endPoint,JSONString,'POST'); 
                system.debug('==response Status=='+res.getStatus());
                if(res.getStatuscode() == 200){
                    JSONParser parser = JSON.createParser(res.getBody());
                    system.debug('==Parser=='+parser);
                    while (parser.nextToken() != null) {
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                            (parser.getText() == 'id')) {
                                parser.nextToken();
                                merchantId = parser.getIntegerValue();
                            }
                        
                    }
                    theAcc.Merchant_User_ID__c = merchantId;  
                    accListToUpdate.add(theAcc);
                    
                    //
                    Merchant_Portal_Welcome_Email_Mapping__c configSetting = Merchant_Portal_Welcome_Email_Mapping__c.getValues('merchantLoginCredentials');
                    if(configSetting != null) {
                        list<string> emailAddresses = new List<string>();
                        emailAddresses.add(theAcc.RMC_Email__c);
                        sendMerchantLoginCredentials(emailAddresses, passWord, string.isNotEmpty(configSetting.URL__c) ? configSetting.URL__c : '', string.isNotEmpty(configSetting.Sender_Email__c) ? configSetting.Sender_Email__c : '', string.isNotEmpty(configSetting.Email_Template__c) ? configSetting.Email_Template__c : '');
                    }
                    
                } 
                System.debug('Response======================= '+res.getBody()); 
                logRecord = EmailUtility.getLoggingRecordAcc('Merchant Portal User Create API',JSONString,res.Getbody(),accId);
                
            }
        }catch(Exception e){
            logRecord = EmailUtility.getLoggingRecordAcc('Merchant Portal User Create API',JSONString,e.getMessage(),accId);
            WebToSFDC.notifyDev('Merchant Portal User Create API', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() ) ;
        }
        if(logRecord <> null){ insert logRecord;}  
        if(!accListToUpdate.isEmpty()) update accListToUpdate;
    }
    
    //===Update Merchant===
    @future(callout=true)
    Public static void updateMerchantTomySql(string accId, string email, string isActive,integer merchantId, string merchantName){
        Map<string,string> requestMap = new Map<string,string>();
        boolean IsSandBox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox; 
        Logging__c  finalLogRecord;
        String JSONString;
        try{ 
            Mule_Settings__c muleDetails = Mule_Settings__c.getInstance();
            string merchantUpdateEndPoint = (IsSandBox ?muleDetails.Dev_Base_URL__c : muleDetails.Base_URL__c)+muleDetails.Update_Merchant_EndPoint__c+merchantId;                                   
            system.debug('==url=='+merchantUpdateEndPoint);
            if(string.isNotEmpty(accId)) 
            {
                JSONGenerator gen=JSON.createGenerator(true);
                gen.writeStartObject();
                if(email <> null)gen.writeStringField('username',email);
                gen.writeFieldName('scope');
                gen.writeStartObject();
                gen.writeStringField('accountId',accId);
                gen.writeStringField('merchantName',merchantName);
                if(isActive <> null) gen.writeStringField('is_active',isActive);
                gen.writeStringField('roleName','ROOT');
                gen.writeEndObject();
                /* gen.writeFieldName('user_application');
gen.writeStartObject(); 
gen.writeNumberField('application_id',integer.valueOf(muleDetails.User_Application_Id__c));
gen.writeNumberField('role_id',integer.valueOf(muleDetails.Role_Id__c));
gen.writeStringField('environment',muleDetails.Environment__c);
gen.writeEndObject(); */
                JSONString = gen.getAsString();
                
                
                HTTPResponse res=sendRequest(merchantUpdateEndPoint,JSONString,'PUT'); 
                
                // Map<string,Object> responseMap = (Map<String,Object>) JSON.deserialize(res.getBody(), Map<String,Object>.class);    
                
                finalLogRecord = EmailUtility.getLoggingRecordAcc('Merchant Account update API',JSONString,res.Getbody(),accId); 
            }
            System.debug('finalLogRecord======================= '+finalLogRecord);
            
        }
        catch(Exception e ){
            System.debug('Exception================ '+ e.getMessage());
            finalLogRecord = EmailUtility.getLoggingRecordAcc('Merchant Account update API',JSONString,e.getMessage(),accId);
            WebToSFDC.notifyDev('Merchant Account update API', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() ) ;
        } 
        if(finalLogRecord <> null){ insert finalLogRecord;}        
    } 
    public static HTTPResponse sendRequest(string merchantUpdateEndPoint, string JSONString,string methodType){
        Mule_Settings__c muleDetails = Mule_Settings__c.getInstance();
        boolean IsSandBox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;   
        HttpRequest req = new HttpRequest(); 
        req.setEndPoint(merchantUpdateEndPoint); 
        
        req.setHeader('Authorization', IsSandBox ? muleDetails.Dev_Authorization__c: muleDetails.Authorization__c);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Application-Token',IsSandBox ? muleDetails.Dev_Application_Token__c:muleDetails.Application_Token__c);
        req.setMethod(methodType);            
        req.setBody(JSONString);
        system.debug('==Endpoint=='+merchantUpdateEndPoint);
        system.debug('==Authorization=='+req.getHeader('Authorization'));
        system.debug('==Content-Type='+req.getHeader('Content-Type'));
        system.debug('==Application-Token='+req.getHeader('Application-Token'));
        System.debug('Request ==============='+req);
        System.debug('Request ==============='+req.getBody());
        Http sendHttp = new Http();
        HTTPResponse res; 
        if(!Test.isRunningTest()){
            res =  sendHttp.send(req);   
        }else{
            res = new HTTPResponse();                    
            res.SetBody(TestHelper.createJsonNew());               
        } 
        system.debug('==response=='+res.getbody());
        return res;
    }
    
    @TestVisible  
    private static void updateApp(Opportunity app, string status){
        
        //fetch the CL contract related to the application and updated the "Customer_Portal_Status__c" field
        if(app.CL_Contracts__r.size() > 0) {
            app.CL_Contracts__r[0].Customer_Portal_Status__c = status;
            update app.CL_Contracts__r[0];
        }
    }
    
    
    
    public static void sendpassword(String contactId,string Email,String Password, string urlForLogin){       
        List<Outbound_Emails_Temp__c> outBoundEmails = new List<Outbound_Emails_Temp__c>();           
        AANNotificationJob AANjob = new AANNotificationJob();
        
        EmailTemplate eTemplate = [select id, name, Subject, Body from EmailTemplate where developername = : 'Customer_Portal_Credentials_Email'];  
        
        string Subject  = eTemplate.Subject, mailBody = eTemplate.Body;  
        
        mailBody = replaceValuesFromData(mailBody,Email,Password, urlForLogin);               
        outBoundEmails.add(AANjob.createEmail(mailBody, Subject, contactId));
        
        if(outBoundEmails.size() > 0){insert outBoundEmails;}                        
    } 
    
    @TestVisible 
    private static string replaceValuesFromData(string bodyMail, string UN, string pswd, string loginUrl){      
        if (UN!= null) bodyMail = bodyMail.replace('{!username}',UN);
        if (pswd!= null) bodyMail = bodyMail.replace('{!password}',pswd);
        if (loginUrl!= null) bodyMail = bodyMail.replace('{!loginurl}',loginUrl);
        
        return bodyMail;
    }
    
    
    /**Method to generate Random Password**/
    public static string getPassword(){
        
        Integer len1 = 2, len2 = 4, len3 = 3, randomInt = (integer)(7.0 * Math.random());
        Blob blobKey1 = crypto.generateAesKey(256);
        String xkey1 = EncodingUtil.convertToHex(blobKey1);
        string Newkey1 = xkey1.substring(0,len1);                       
        system.debug('Newkey1================'+Newkey1);
        
        Blob blobKey2 = crypto.generateAesKey(256);
        String xkey2 = EncodingUtil.convertToHex(blobKey2);
        String Newkey2 = xkey2.substring(0,len2);                       
        system.debug('Newkey2================'+Newkey2);
        
        Blob blobKey3 = crypto.generateAesKey(256);
        String xkey3 = EncodingUtil.convertToHex(blobKey3);
        String Newkey3 = xkey3.substring(0,len3);
        system.debug('Newkey3================'+Newkey3);
        
        // e90ea6@d81
        /* Newkey1 = 'd90'; 
Newkey2 = 'ea6';
Newkey3 = 'd81';*/
        if(!Newkey1.isNumeric()){
            Newkey1 = Newkey1.toUpperCase();
            system.debug('Newkey1================'+Newkey1);
        }
        else if(!Newkey2.isnumeric()){
            Newkey2 = Newkey2.toUpperCase();
            system.debug('Newkey2================'+Newkey2);
        }
        else {
            if(!Newkey3.isnumeric()){
                Newkey3 = Newkey3.toUpperCase();
                system.debug('Newkey3================'+Newkey3);
            }        
        }
        //System.debug('pseudo random int between 1 and 7 :'  + randomInt );          
        list<string> SpecialChar = new list<string>{ '!', '@', '#', '$', '%', '&', '^'};
            system.debug(SpecialChar[randomInt]);                    
        
        if(Newkey1.isNumeric() && Newkey2.isNumeric() && Newkey3.isNumeric()){
            system.debug('Worst Case Scenario');
            return getPassword();                
        }
        
        string dummyKey = NewKey1+Newkey2+SpecialChar[randomInt]+Newkey3;
        system.debug('===dummyKey=='+dummyKey);
        string temp= dummyKey.left(1);
        string remainingKey;
        
        if(!temp.isNumeric() && temp.isAllUpperCase()){
            temp=dummyKey.left(1)+'e';
            remainingKey= dummyKey.substring(2);
            system.debug('===if==============');
        }
        else{
            temp='e';
            remainingKey= dummyKey.substring(1);
        }
        
        system.debug('==temp=='+temp);
        system.debug('==remainingKey=='+remainingKey);
        string FinalKey = temp+remainingKey;
        system.debug('FinalKey============='+FinalKey);
        return FinalKey;
    }
    public static void sendMerchantLoginCredentials(list<string> emailToAddressess, string password, string url, string senderEmail, string Email_TEMPLATE) {
        if(!emailToAddressess.isEmpty()) {
            OrgWideEmailAddress[] orgWideEmailAddress = [select Id from OrgWideEmailAddress where Address = :senderEmail]; 
            for(EmailTemplate eTemplate : [SELECT name, Subject, Body,HtmlValue  FROM EmailTemplate where DeveloperName = :Email_TEMPLATE]) {
                string htmlBody  = eTemplate.HtmlValue!=null ?  eTemplate.HtmlValue : eTemplate.Body;
                htmlBody =  htmlBody.replace('{!password}' , password);
                htmlBody  = htmlBody.replace('{!userName}' ,emailToAddressess[0]);
                htmlBody  = htmlBody.replace('{!url}' ,url); 
                Messaging.SingleEmailMessage eMessage = new Messaging.SingleEmailMessage();                        
                eMessage.subject = eTemplate.subject!=null ?  eTemplate.subject : '';
                eMessage.setToAddresses(emailToAddressess);
                eMessage.setHtmlBody(htmlBody);
                if (orgWideEmailAddress.size() > 0 ) eMessage.setOrgWideEmailAddressId(orgWideEmailAddress[0].Id);
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {eMessage};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            }
        }
    }
}