global with sharing class LoanReschedulingCtrl {

	public loan__Loan_Account__c contract {get; set;}
	public List<LoanModificationHistory__c> modHistoryList {get; set;}
	public Boolean isAdmin {get; set;}
	public Boolean isCollectionsSupervisor {get; set;}
	public Boolean isCollectionsManager {get; set;}
	public List<SelectOption> autoRescheduleTypeOptions {set;}
	public List<SelectOption> autoReschedulePaymentTypes {set;}
	public String selectedType {get; set;}
	public String paymentType {get; set;}
	public List<ReschedPreview__c> changeType {get; set;}
	public boolean displayACHPopup {get; set;}
	public boolean displayLastWoaDate {get; set;}
	public Integer billsSatisfied {get; set;}
	public Integer loanModificationCount {get; set;}
	public Integer woaModificationCount {get; set;}

	global LoanReschedulingCtrl(ApexPages.StandardController controller) {
		List<AggregateResult> billsSatisfiedList;
        
        if(!Test.isRunningTest()){
            controller.addFields(new List<String> {'Is_Debit_Card_On__c', 'loan__ACH_On__c','loan__Term_Cur__c', 'loan__Interest_Rate__c', 'loan__Due_Day__c', 'loan__Frequency_of_Loan_Payment__c'
				, 'loan__Maturity_Date_Current__c', 'loan__Pmt_Amt_Cur__c', 'Modification_Type__c', 'Agreement_Modified_Date__c'});
        }	
		contract = (loan__Loan_Account__c)controller.getRecord();

		displayLastWoaDate = contract.Modification_Type__c == null ? false : contract.Modification_Type__c.contains('Workout Agreement');

		modHistoryList = [	SELECT 	Agreement_Modified_Date__c,
				Modification_Made__c,
				Modification_Reason__c,
				Modification_Type__c,
				Modified_By__c,
				Modification_Reason_Text_Line__c
		FROM LoanModificationHistory__c
		WHERE Contract__c = : contract.Id];
		billsSatisfiedList = [SELECT COUNT(Id) Total FROM loan__Loan_account_Due_Details__c where loan__Loan_Account__c = : contract.Id and loan__Payment_Satisfied__c = true];
		for (AggregateResult ar : billsSatisfiedList)  {
			billsSatisfied = (Integer) ar.get('Total');
		}
		isAdmin = UserInfo.getProfileId() == [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
		isCollectionsSupervisor = UserInfo.getProfileId() == [SELECT Id FROM Profile WHERE Name = 'Collections Supervisor' LIMIT 1].Id;
		isCollectionsManager = UserInfo.getProfileId() == [SELECT Id FROM Profile WHERE Name = 'Collections Manager' LIMIT 1].Id;
		changeType = [SELECT ChangeType__c
		FROM ReschedPreview__c
		WHERE Contract__c = : contract.Id
		ORDER BY CreatedDate DESC LIMIT 1];
		selectedType = (!changeType.isEmpty()) ? changeType[0].ChangeType__c : 'Loan Workout Agreement';

		paymentType =  contract.Is_Debit_Card_On__c ? 'Debit Card' : contract.loan__ACH_On__c ? 'ACH' : 'Certified Check';

		loanModificationCount = 0;
		woaModificationCount =0 ;
		for (LoanModificationHistory__c loanModHistory : [SELECT Id, Modification_Type__c FROM LoanModificationHistory__c WHERE Contract__c = :contract.Id]){
			if(loanModHistory.Modification_Type__c.contains('Loan Modification')) loanModificationCount++;
			else if(loanModHistory.Modification_Type__c.contains('Workout Agreement Modification')) woaModificationCount++;
		}
	}

	public void closeACHPopup() {
		displayACHPopup = false;
	}

	public void showACHPopup() {
		displayACHPopup = true;
	}

	public void changePaymentTypeSelected(){
		paymentType = 'ACH';
	}

	public List<SelectOption> getAutoRescheduleTypeOptions () {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('Loan Workout Agreement', 'Loan Workout Agreement'));
		options.add(new SelectOption('Loan Modification', 'Loan Modification'));

		return options;
	}

	public List<SelectOption> getautoReschedulePaymentTypes () {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('ACH', 'ACH'));
		options.add(new SelectOption('Debit Card', 'Debit card'));
		options.add(new SelectOption('Certified Check', 'Certified Check'));

		return options;
	}



	/*******************************************
	* Marks as 'Archived' the old amortization
	* table and creates a new one.
	********************************************/
	@RemoteAction
	global static void saveAmortizationSchedule(
			String contractId,
			List<String> balances,
			List<String> paymentAmounts,
			List<String> paymentDates,
			List<String> interestAmounts,
			List<String> principalAmounts,
			List<String> paymentIds) {
		List<loan__Repayment_Schedule__c> amortSched = [SELECT 	loan__Archived__c,
				loan__Balance__c,
				loan__Barcode_Subtype__c,
				loan__Barcode_Type__c,
				loan__Date_Paid__c,
				loan__Due_Amount__c,
				loan__Due_Date__c,
				loan__Due_Interest__c,
				loan__Due_Interest_On_Overdue__c,
				loan__Due_Principal__c,
				loan__Id__c,
				loan__Installment_Snapshot__c,
				loan__Interest_Accrued_Not_Due__c,
				loan__Is_Archived__c,
				loan__Is_Billed__c,
				loan__isPaid__c,
				loan__Is_Partially_Paid__c,
				loan__Is_Posted__c,
				loan__Loan_Account__c,
				loan__Loan_Disbursal_Transaction__c,
				loan__Master_Archive_Object__c,
				loan__Paid_Fees__c,
				loan__Paid_Interest__c,
				loan__Paid_Interest_On_Overdue__c,
				loan__Paid_Principal__c,
				loan__Paid_Total__c,
				loan__Past_Due_Date__c,
				loan__Summary__c,
				loan__Summary_Record_Id__c,
				loan__Total_Due_Fees__c,
				loan__Total_Installment__c,
				loan__Total_Paid_Fees__c,
				loan__Transaction_Amount__c,
				loan__Unpaid_Fees__c,
				loan__Unpaid_Installment__c,
				loan__Unpaid_Interest__c,
				loan__Unpaid_Overdue_Interest__c,
				loan__Unpaid_Principal__c,
				loan__Waived_Interest__c
		FROM 	loan__Repayment_Schedule__c
		WHERE 	loan__Loan_Account__c = : contractId];

		loan__Loan_Account__c theContract = [SELECT Id, loan__Loan_Status__c FROM loan__Loan_Account__c WHERE Id = : contractId LIMIT 1];

		List<loan__Repayment_Schedule__c> newAmortSchedList = new List<loan__Repayment_Schedule__c>();

		for (loan__Repayment_Schedule__c a : amortSched) {
			a.loan__Is_Archived__c = true;
		}

		update amortSched;

		for (Integer i = 0; i < paymentIds.size(); i++) {
			Date paymentDate = getDateFromString(paymentDates[i]);
			loan__Repayment_Schedule__c newAmortSched = new loan__Repayment_Schedule__c();
			newAmortSched.loan__Archived__c = false; //Checkbox
			newAmortSched.loan__Balance__c = Decimal.valueOf(balances[i]); //Number (16,2)
			newAmortSched.loan__Barcode_Subtype__c = ''; //Picklist
			newAmortSched.loan__Barcode_Type__c = 'Code128'; //Picklist
			newAmortSched.loan__Due_Amount__c = Decimal.valueOf(paymentAmounts[i]); //Number(16,2)
			newAmortSched.loan__Due_Date__c = paymentDate; //Date
			newAmortSched.loan__Due_Interest__c = Decimal.valueOf(interestAmounts[i]); //Number(16,2)
			newAmortSched.loan__Due_Interest_On_Overdue__c = 0; //Number(16,2)
			newAmortSched.loan__Due_Principal__c = Decimal.valueOf(principalAmounts[i]); //Number(16,2)
			newAmortSched.loan__Id__c = paymentIds[i]; //Text(255)
			newAmortSched.loan__Installment_Snapshot__c = ''; //Text(255)
			newAmortSched.loan__Interest_Accrued_Not_Due__c = 0; //Number(16,2)
			newAmortSched.loan__Is_Archived__c = false; //Checkbox
			newAmortSched.loan__Is_Billed__c = false; //Checkbox
			newAmortSched.loan__isPaid__c = false; //Checkbox
			newAmortSched.loan__Is_Partially_Paid__c = false; //Checkbox
			newAmortSched.loan__Is_Posted__c = false;  //Checkbox
			newAmortSched.loan__Loan_Account__c = contractId; //Lookup a CL Contracts
			newAmortSched.loan__Master_Archive_Object__c = false; //Checkbox
			newAmortSched.loan__Paid_Fees__c = 0; //Number(16,2)
			newAmortSched.loan__Paid_Interest__c = 0; //Number(16,2)
			newAmortSched.loan__Paid_Interest_On_Overdue__c = 0; //Number(16,2)
			newAmortSched.loan__Paid_Principal__c = 0; //Number(16,2)
			newAmortSched.loan__Past_Due_Date__c = false; //Checkbox
			newAmortSched.loan__Summary__c = false; //Checkbox
			newAmortSched.loan__Summary_Record_Id__c = ''; //Text(100)
			newAmortSched.loan__Transaction_Amount__c = 0; //Number(16,2)
			newAmortSched.loan__Waived_Interest__c = null; //Number(16,2)
			newAmortSchedList.add(newAmortSched);
		}

		if (newAmortSchedList.size() > 0)
			insert newAmortSchedList;

		List<loan__Loan_Payment_Transaction__c> pendingPayments = [	SELECT 	Id,
				Payment_Pending_End_Date__c
		FROM 	loan__Loan_Payment_Transaction__c
		WHERE 	loan__Loan_Account__c = : theContract.Id
		AND 	loan__Reversed__c = false
		AND 	Payment_Pending_End_Date__c != null];
		List<loan__Loan_Payment_Transaction__c> newPaymentTable = new List<loan__Loan_Payment_Transaction__c>();
		for (loan__Loan_Payment_Transaction__c t : pendingPayments) {
			t.Payment_Pending_End_Date__c = null;
			newPaymentTable.add(t);
		}

		if (newPaymentTable.size() > 0)
			update newPaymentTable;

		theContract.loan__Loan_Status__c = 'Active - Good Standing';
		update theContract;
	}

	@RemoteAction
	global static void setUpPeriodicFee(
			String contractId,
			String waivedInterestAmount,
			String recurringDate) {
		loan__Fee__c feeObj = [	SELECT 	Id
		FROM 	loan__Fee__c
		WHERE 	Name = 'Waived Interest Recovery'
		LIMIT 1];

		List<loan__Periodic_Fee_Setup__c> periodfeeSetUpList = [SELECT 	Id,
				loan__Active__c,
				loan__Amount__c,
				loan__Archived__c,
				loan__Clear__c,
				loan__Lending_Account__c,
				loan__Master_Archive_Object__c,
				loan__Next_Recurring_Fee_Date__c,
				loan__Sequence__c,
				loan__Summary__c,
				loan__Summary_Record_Id__c
		FROM 	loan__Periodic_Fee_Setup__c
		WHERE 	loan__Lending_Account__c = : contractId
		AND 	loan__Fee__c = : feeObj.Id
		ORDER BY CreatedDate DESC];

		if (periodfeeSetUpList.size() > 0) {
			for (loan__Periodic_Fee_Setup__c feeSetUp : periodfeeSetUpList) {
				feeSetUp.loan__Active__c = false;
				feeSetUp.loan__Archived__c = true;
				feeSetUp.loan__Clear__c = true;
			}

			update periodfeeSetUpList;
		}

		loan__Periodic_Fee_Setup__c newFeeSetUp = new loan__Periodic_Fee_Setup__c();
		newFeeSetUp.loan__Active__c = true;
		newFeeSetUp.loan__Amount__c = Decimal.valueOf(waivedInterestAmount);
		newFeeSetUp.loan__Archived__c = false;
		newFeeSetUp.loan__Clear__c = false;
		newFeeSetUp.loan__Fee__c = feeObj.Id;
		newFeeSetUp.loan__Lending_Account__c = contractId;
		newFeeSetUp.loan__Master_Archive_Object__c = false;
		newFeeSetUp.loan__Next_Recurring_Fee_Date__c = getDateFromString(recurringDate);
		newFeeSetUp.loan__Sequence__c = 1;
		newFeeSetUp.loan__Summary__c = false;
		newFeeSetUp.loan__Summary_Record_Id__c = '';

		insert newFeeSetUp;

	}

	private static Date getDateFromString(String theDate) {
		List<String> dateParts = theDate.split('/');
		return Date.newInstance(Integer.valueOF(dateParts[2]), Integer.valueOF(dateParts[0]), Integer.valueOF(dateParts[1]));
	}
}