@istest public class LoanPaymentSummarizedByLineTest {
      @testSetup
    public static void doSetup(){
        //Setup seed data
        TestHelperForManaged.createSeedDataForTesting();
        TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupMetadataSegments();
        loan.TestHelper.systemDate = Date.newInstance(2014, 3, 3); // Mar 3 - Monday
        
        
    }
    public static testMethod void testFileCreation(){
        
        //Load csv file
        Test.loadData(filegen__File_Metadata__c.sObjectType, 'LendingFilegen');
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
        
        
        Contact co = null;
        try {
            co = TestHelperForManaged.createContact('Test', 'Wedding', '12345', 'GA');
        } catch (Exception e){
        }
      
        
        //    Currency__c curr = TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Bank__c bank = loan.TestHelper.createBank();
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        dummyOffice.loan__Branch_s_Bank__c = bank.id;
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                                                                          dummyAccount, 
                                                                          curr, 
                                                                          dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        
        //Account a1 = loan.TestHelper.createInvestor('Bull', 1000);
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        loan__Bank_Account__c ba = new loan__Bank_Account__c(loan__Bank_Account_Number__c = '12',
                                                             loan__Bank_Name__c = 'Some Bank',
                                                             loan__Routing_Number__c = '99999999',
                                                             loan__Account_Type__c = 'Checking');
        
        insert ba;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Collections Trust Account');
        loan__Bank_Account__c dummyBank2 = TestHelperForManaged.createBankAccount(b1,'Bofi Collections Account');
        //Create a dummy Loan Account
        loan__Loan_Account__c dummylaMonthly =  LibraryTest.createContractTH();
        dummylaMonthly.loan__ACH_On__c = true;
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = Date.today().addMonths(1);
        dummylaMonthly.loan__ACH_Start_Date__c = Date.today();
        dummylaMonthly.loan__ACH_End_Date__c = Date.today().addMonths(4);
        dummylaMonthly.loan__ACH_Bank__c = dummyOffice.loan__Branch_s_Bank__c;
        dummylaMonthly.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        dummylaMonthly.loan__ACH_Account_Type__c = 'Checking';
        dummylaMonthly.loan__ACH_Account_Number__c = '123456';
        dummylaMonthly.loan__ACH_Routing_Number__c = '123456123';
        dummylaMonthly.loan__ACH_Relationship_Type__c = 'PRIMARY';
        dummylaMonthly.loan__Contact__c = co.Id;
        update dummylaMonthly;
        loan__Loan_Account__c dummylaWeekly =  LibraryTest.createContractTH();
        dummylaWeekly.loan__ACH_On__c = true;
        //dummylaWeekly.Borrower_ACH__c = ba.Id;
        dummylaWeekly.loan__ACH_Debit_Amount__c = 100;
        dummylaWeekly.loan__ACH_Next_Debit_Date__c = Date.today().addMonths(1);
        dummylaWeekly.loan__ACH_Start_Date__c = Date.today();                                      
        dummylaWeekly.loan__ACH_End_Date__c = Date.today().addMonths(4);
        dummylaWeekly.loan__ACH_Bank__c = dummyOffice.loan__Branch_s_Bank__c;
        dummylaWeekly.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        dummylaWeekly.loan__ACH_Account_Type__c = 'Checking';
        dummylaWeekly.loan__ACH_Account_Number__c = '123456';
        dummylaWeekly.loan__ACH_Routing_Number__c = '123456123';
        dummylaWeekly.loan__ACH_Relationship_Type__c = 'PRIMARY';
        dummylaWeekly.loan__Contact__c = co.Id;
        dummylaWeekly.loan__Frequency_of_Loan_Payment__c = loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY;
        update dummylaWeekly;
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
         LoanPaymentTxnPayPointGen pg = new LoanPaymentTxnPayPointGen();
        
 
        loan__Loan_Payment_Transaction__c transactionPay = new loan__Loan_Payment_Transaction__c();
        transactionPay.loan__Loan_Account__c = dummylaWeekly.id;
        transactionPay.loan__Transaction_Amount__c = 100;
        transactionPay.loan__Cleared__c = false;
        transactionPay.loan__ACH_Filename__c = 'A - Loan Payments.txt';
        transactionPay.loan__Skip_Validation__c = true;
        transactionPay.FT_Payment_Reporting__c = false;
        transactionPay.loan__Payment_Mode__c = pm.Id;
 		List<loan__Loan_Payment_Transaction__c> lp = new List<loan__Loan_Payment_Transaction__c>();
        pg.getEntries(null, lp);
		pg.generateEntryDetailRecord(dummylaWeekly, transactionPay);
       
        test.startTest();
        LoanPaymentSummarizedByLine.executePaymentSweep();

        test.stopTest();
        
        loan__Loan_Account__c laMonthly = [SELECT Id
                                           , loan__ACH_Next_Debit_Date__c
                                           , loan__ACH_Debit_Day__c
                                           , loan__ACH_Frequency__c
                                           , loan__First_Installment_Date__c 
                                           FROM loan__Loan_Account__c 
                                           where loan__Frequency_of_Loan_Payment__c =: loan.LoanConstants.LOAN_PAYMENT_FREQ_MONTHLY LIMIT 1];
        
        loan__Loan_Account__c laWeekly = [SELECT Id
                                          , loan__Disbursal_Date__c
                                          , loan__ACH_Next_Debit_Date__c
                                          , loan__ACH_Frequency__c 
                                          FROM loan__Loan_Account__c 
                                          where loan__Frequency_of_Loan_Payment__c =: loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY LIMIT 1];
        
        List<loan__Loan_Payment_Transaction__c> p = [SELECT Id FROM loan__Loan_Payment_Transaction__c];
       // System.assert(p.size() > 0 );
        
        LoanPaymentSummarizedByLine t = new LoanPaymentSummarizedByLine();
        t.targetDate = DateTime.parse('10/01/2016 12:00 AM');
        t.execute(null);


            List<Document> d = [Select Id from Document where CreatedDate = TODAY];
            System.assert(d.size() > 0, 'Documents were created!');        

//        LoanPaymentSummarizedByLine.executeSummarizedLineFile();
      
        
    }
    
    
    
    public static testMethod void testFileCreation2(){
        
        //Load csv file
        Test.loadData(filegen__File_Metadata__c.sObjectType, 'LendingFilegen');
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
        
        
        Contact co = null;
        try {
            co = TestHelperForManaged.createContact('Test', 'Wedding', '12345', 'GA');
        } catch (Exception e){
        }
      
        
        //    Currency__c curr = TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Bank__c bank = loan.TestHelper.createBank();
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        dummyOffice.loan__Branch_s_Bank__c = bank.id;
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                                                                          dummyAccount, 
                                                                          curr, 
                                                                          dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        
        //Account a1 = loan.TestHelper.createInvestor('Bull', 1000);
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        loan__Bank_Account__c ba = new loan__Bank_Account__c(loan__Bank_Account_Number__c = '12',
                                                             loan__Bank_Name__c = 'Some Bank',
                                                             loan__Routing_Number__c = '99999999',
                                                             loan__Account_Type__c = 'Checking');
        
        insert ba;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Collections Trust Account');
        loan__Bank_Account__c dummyBank2 = TestHelperForManaged.createBankAccount(b1,'Bofi Collections Account');
        //Create a dummy Loan Account
        loan__Loan_Account__c dummylaMonthly =  LibraryTest.createContractTH();
        dummylaMonthly.loan__ACH_On__c = true;
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = Date.today().addMonths(1);
        dummylaMonthly.loan__ACH_Start_Date__c = Date.today();
        dummylaMonthly.loan__ACH_End_Date__c = Date.today().addMonths(4);
        dummylaMonthly.loan__ACH_Bank__c = dummyOffice.loan__Branch_s_Bank__c;
        dummylaMonthly.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        dummylaMonthly.loan__ACH_Account_Type__c = 'Checking';
        dummylaMonthly.loan__ACH_Account_Number__c = '123456';
        dummylaMonthly.loan__ACH_Routing_Number__c = '123456123';
        dummylaMonthly.loan__ACH_Relationship_Type__c = 'PRIMARY';
        dummylaMonthly.loan__Contact__c = co.Id;
        update dummylaMonthly;
     
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
 
        test.startTest();
        LoanPaymentSummarizedByLine.executePaymentSweep();
       test.stopTest();
        
   

        LoanPaymentSummarizedByLine.executeSummarizedLineFile();
      
        
    }
    
    
    public static testMethod void testFileDeletion(){
        LoanPaymentSummarizedByLine.deleteTodaysACHFiles();
        
    }
    
}