@isTest public class MultiloanUtilTestCtrl {
    
    @testSetup static void initializes_objects(){
        
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
        Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7',
        Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
        Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO');
        insert FinwiseParams;

        Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name = 'POS', Asset_Id__c = 5, SFDC_Value__c = false);
        insert AsetCls;
        MultiLoan_Settings__c multiSet = new MultiLoan_Settings__c();
        multiSet.name = 'settings';
        multiSet.Check_Frequency__c = 30;
        multiSet.Maximum_Loan_Amount__c = 5000;
        multiSet.Minimum_Loan_Amount__c = 1000;
        multiSet.Days_To_First_Check__c = 60;
        multiSet.AccountId__c = '0011700000t45FtAAI';
        insert multiSet;
        
        Account acc = new Account();
        acc.Name ='Multi-Loan'; 
        insert acc;
    }
    
    private static loan__loan_Account__c creates_contract() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        contract.loan__Interest_Accrued_Not_Due__c = 500;
        update contract;
		
        Opportunity opp = [Select Id, Maximum_Loan_Amount__c from Opportunity where Id =: contract.Opportunity__c ];
        opp.Maximum_Loan_Amount__c = 24000;
        upsert opp;
        
        Offer__c off = new Offer__c();
        off.IsSelected__c = true;
        off.Opportunity__c = contract.Opportunity__c;
        off.APR__c = 0.2;
        off.Fee__c = 100;
        off.Fee_Percent__c = 0.04;
        off.FirstPaymentDate__c = Date.today().addDays(-29);
        off.Installments__c = 34;
        off.Loan_Amount__c = 5000;
        off.Term__c = 34;
        off.Payment_Amount__c = 230;
        off.Repayment_Frequency__c = 'Monthly';
        off.Preferred_Payment_Date__c = Date.today().addDays(-23);
        insert off;

        return contract;
    }
    
    @isTest static void is_better_grade(){
        Boolean result = MultiloanUtil.isBetterGrade('A1', 'A1');
        System.assertEquals(true, result);
        
        result = MultiloanUtil.isBetterGrade('A2', 'A1');
        System.assertEquals(true, result);
        
        result = MultiloanUtil.isBetterGrade('B1', 'A1');
        System.assertEquals(true, result);
        
        result = MultiloanUtil.isBetterGrade('B2', 'A1');
        System.assertEquals(true, result);
        
        result = MultiloanUtil.isBetterGrade('C1', 'A1');
        
        result = MultiloanUtil.isBetterGrade('C1', 'A2');
        System.assertEquals(true, result);

        result = MultiloanUtil.isBetterGrade('C1', 'B2');
        System.assertEquals(true, result);
        
        result = MultiloanUtil.isBetterGrade('C2', 'D');
        System.assertEquals(false, result);
        
        result = MultiloanUtil.isBetterGrade('D', 'C1');
        System.assertEquals(true, result);
        
        result = MultiloanUtil.isBetterGrade('E', 'B2');
        System.assertEquals(true, result);
        
        result = MultiloanUtil.isBetterGrade('F', 'A1');
        System.assertEquals(true, result);

        System.assert(MultiloanUtil.isBetterGrade('F', 'B1'));        
    }
    
    @isTest static void SelectedOffer() {
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Maximum_Loan_Amount__c = 12500;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
        Offer__c off = new Offer__c();
        off.Opportunity__c = fundedApp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        insert off;
        
        loan__loan_Account__c contract = LibraryTest.createcontractTH();
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        update contract;
        
        AccountManagementHistory__c accM = New AccountManagementHistory__c();
        accM.Opportunity__c = fundedApp.Id;
        accM.CreditReportId__c = fundedApp.PrimaryCreditReportId__c;
        accM.FicoValue__c = 729;
        accM.Contract__c = contract.Id;
        insert accM;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, CreditPull_Date__c = Date.today().addDays(-2));
        upsert opp;

        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 729;
        multiloanParams.Grade__c = 'A1';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false' + ',' + opp.Status__c;
        upsert multiloanParams;
        
        Opportunity opp2 = [Select Id, Name from Opportunity where Id =: appId];

        Test.startTest();
        
        MultiloanPostSoftPull.eligibilityCheckGetCR(multiloanParams);
        MultiloanUtil.getSelectedOffer(opp2.Id);
        MultiloanUtil.getContract(contract.id);
        
        Test.stopTest();
        

        //System.assert(false, MultiloanPostSoftPull.isEligible);
        
        
    }
    
    @isTest static void InsertOffer_note() {
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Maximum_Loan_Amount__c = 12500;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
        Offer__c off = new Offer__c();
        off.Opportunity__c = fundedApp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        off.Total_Loan_Amount__C = 1200;
        insert off;
        
        loan__loan_Account__c contract = LibraryTest.createcontractTH();
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        contract.loan__Due_Day__c = 2.0;
        contract.loan__Pay_Off_Amount_As_Of_Today__c = 2000;
        update contract;
        
        AccountManagementHistory__c accM = New AccountManagementHistory__c();
        accM.Opportunity__c = fundedApp.Id;
        accM.CreditReportId__c = fundedApp.PrimaryCreditReportId__c;
        accM.FicoValue__c = 729;
        accM.Contract__c = contract.Id;
        insert accM;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, CreditPull_Date__c = Date.today().addDays(-2));
        upsert opp;

        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 729;
        multiloanParams.Grade__c = 'A1';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false' + ',' + opp.Status__c;
        upsert multiloanParams;
        
        Offer__c off2 = [Select Id, Name, Interest_Rate__c, Term__c, Repayment_Frequency__c, Installments__c, Payment_Amount__c, Customer_Disbursal__c, fee__c, Effective_APR__c 
                         FROM Offer__c where Id =: off.Id];

        Test.startTest();
        
        MultiloanUtil.insertNoteOldOffer(appId, fundedApp.Id, off2, contract.Id); 
        
        Test.stopTest();
        

        //System.assert(false, MultiloanPostSoftPull.isEligible);
        
        
    }
    
    @isTest static void get_Application() {
        
        MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');
        
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Maximum_Loan_Amount__c = 12500;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
        Offer__c off = new Offer__c();
        off.Opportunity__c = fundedApp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        off.Total_Loan_Amount__C = 1200;
        insert off;
        
        loan__loan_Account__c contract = LibraryTest.createcontractTH();
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        contract.loan__Due_Day__c = 2.0;
        contract.loan__Pay_Off_Amount_As_Of_Today__c = 2000;
        update contract;
        
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, CreditPull_Date__c = Date.today().addDays(-2));
        upsert opp;

        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 729;
        multiloanParams.Grade__c = 'A1';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false' + ',' + opp.Status__c;
        upsert multiloanParams;
        MultiloanPostSoftPull.eligibilityCheckGetCR(multiloanParams);
        
        Test.startTest();
        MultiloanUtil.getApplication(appId);
        Test.stopTest();
   
    }
    
    @isTest static void getLastMultiOpportunity() {
        
        MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');
        
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Contact con = LibraryTest.createContactTH();
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Contact__c = con.id;
        fundedApp.Maximum_Loan_Amount__c = 12500;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
        Opportunity oppMultiloan = LibraryTest.createApplicationTH();
        oppMultiloan.Contact__c = con.id;
        oppMultiloan.Maximum_Loan_Amount__c = 5000;
        oppMultiloan.PrimaryCreditReportId__c = 8749683;
        oppMultiloan.Type = 'Multiloan';
        oppMultiloan.Status__c = 'Credit Qualified';
        oppMultiloan.Estimated_Grand_Total__c = 75000;
        upsert oppMultiloan;
        
        
        loan__loan_Account__c contract = LibraryTest.createcontractTH();
        contract.loan__Contact__c = con.Id;
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        contract.loan__Due_Day__c = 2.0;
        contract.loan__Pay_Off_Amount_As_Of_Today__c = 2000;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, CreditPull_Date__c = Date.today().addDays(-2));
        upsert opp;

        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 729;
        multiloanParams.Grade__c = 'A1';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false' + ',' + opp.Status__c;
        upsert multiloanParams;
        MultiloanPostSoftPull.eligibilityCheckGetCR(multiloanParams);
        
        Test.startTest();
        MultiloanUtil.getLastMultiOpportunity(contract);
        Test.stopTest();

    }
    
    @isTest static void getOffersTest() {

        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Maximum_Loan_Amount__c = 12500;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;

        loan__loan_Account__c contract = creates_contract();
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, CreditPull_Date__c = Date.today().addDays(-60));
        upsert opp;
        
        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 729;
        multiloanParams.Grade__c = 'A1';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false' + ',' + opp.Status__c;
        upsert multiloanParams;
        
        Offer__c off = new Offer__c();
        off.Opportunity__c = opp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        off.Total_Loan_Amount__C = 1200;
        insert off;

        Test.startTest();
        MultiloanUtil.getOffers(appId, multiloanParams.LoanAmountCheck__c, '2', 'Net Fee Out');
        Test.stopTest();
        
        
    }

}