global class DL_ActivityByUserReport_UpdateJob implements Schedulable
{
    global void execute(SchedulableContext sc)
    { 
        updateReport();
    }
    
    @future(callout=true)
    public static void updateReport(){
        DL_UpdateActivityByUserReport dl_uabur = new DL_UpdateActivityByUserReport();
    }

}