public class DocusignProcessor implements Queueable, Database.AllowsCallouts {
    
    private string accountId;
    
    public DocusignProcessor(String accId) {
        accountId = accId;
    }
    
    public void execute(QueueableContext context){
    
        if (string.IsEmpty(accountId))
            return;
            
        //determine how many contacts to be sent
        List<Contact> owners = [SELECT Title, Account.Phone, FirstName, LastName, Email, Account.Bank_Account_Type__c, Account.dba_doing_business_as__c, Account.BillingStreet, Account.BillingCity, Account.BillingState,
            Account.BillingPostalCode, Account.financial_institution_name__c, Account.name_on_account__c, Account.transit_aba_number_9digits__c, Account.financial_institution_account_number__c 
            FROM Contact WHERE AccountId = :accountId AND IsBusinessOwner__c = 'Yes'];
            
        List<Contact> authSigners = [SELECT Title, Account.Phone, FirstName, LastName, Email, Account.Bank_Account_Type__c, Account.dba_doing_business_as__c, Account.BillingStreet, Account.BillingCity, Account.BillingState,
            Account.BillingPostalCode, Account.financial_institution_name__c, Account.name_on_account__c, Account.transit_aba_number_9digits__c, Account.financial_institution_account_number__c
            FROM Contact WHERE AccountId = :accountId AND Authorized_Signer__c = True];  
                          
        if (owners.size() == 0 && authSigners.size() == 0) {
            return;
        }
        
        String requestPayload = '';
        if (authSigners.size() == 1) {
            requestPayload = DocusignRequestHelper.GenerateEnvelopeRequest(authSigners, Docusign_Integration_Settings__c.getInstance().SingleOwnerTemplateId__c);
        }        
        else if (authSigners.size() > 1) {
            requestPayload = DocusignRequestHelper.GenerateEnvelopeRequest(authSigners, Docusign_Integration_Settings__c.getInstance().MultiOwnerTemplateId__c);
        }        
        else if (owners.size() == 1) {
            requestPayload = DocusignRequestHelper.GenerateEnvelopeRequest(owners, Docusign_Integration_Settings__c.getInstance().SingleOwnerTemplateId__c);
        }
        else if (owners.size() > 1) {
            requestPayload = DocusignRequestHelper.GenerateEnvelopeRequest(owners, Docusign_Integration_Settings__c.getInstance().MultiOwnerTemplateId__c);
        }
        
        //generate envelope
        Http m_http = new Http();
        HttpRequest req = new HttpRequest();                
        req.setEndpoint(Docusign_Integration_Settings__c.getInstance().CreateEnvelope_Url__c);
        req.setHeader('Content-Type','application/json');
        req.setHeader('X-DocuSign-Authentication', '{"Username":"'+ Docusign_Integration_Settings__c.getInstance().Username__c +'","Password":"'+ Docusign_Integration_Settings__c.getInstance().Password__c +'","IntegratorKey": "'+ Docusign_Integration_Settings__c.getInstance().IntegratorKey__c +'"}');
        req.setMethod('POST');
        req.setBody(requestPayload);
        req.setTimeout(120000);
        httpResponse response;
        if(Test.isRunningTest()){
            response = new httpResponse();
            response.setBody('{"envelopeId":"test"}');
        } else {
            response = m_http.send(req);        
        }
        system.debug(response.getStatusCode());        
        system.debug(response.getBody());

        //create note for send

        //update account        
        DocusignEnvelopeCreateResponse resp = DocusignEnvelopeCreateResponse.parse(response.getBody());               
        Account a = new Account(Id = accountId);
        a.Merchant_Agreement_Envelope_Id__c = resp.envelopeId;
        update a;
    }
}