public class DecisioningAttributeEntityMapper {

    public static List<DecisioningAttributeEntity> mapAttributes(ProxyApiCreditReportEntity softCreditReportEntity,
            ProxyApiCreditReportEntity hardCreditReportEntity,
            String appType) {

        List<DecisioningAttributeEntity> result = null;

        if (softCreditReportEntity != null && softCreditReportEntity.errorMessage == null) {

            result = new List<DecisioningAttributeEntity>();

          if(softCreditReportEntity.getCriteriaAttributeList() !=null ){
            for (ProxyApiCreditReportAttributeEntity softAttributeEntity : softCreditReportEntity.getCriteriaAttributeList()) {
                ProxyApiCreditReportAttributeEntity hardAttributeEntity = null;

                if (hardCreditReportEntity != null && hardCreditReportEntity.errorMessage == null)
                    hardAttributeEntity = hardCreditReportEntity.getAttributeEntity(softAttributeEntity.attributeType.id);

                if (softAttributeEntity.attributeType.id == 1 && appType == 'Refinance') {
                    softAttributeEntity.attributeType.displayName = 'FICO ( < 600 OR >= 800 )';
                    softAttributeEntity.attributeType.description = softAttributeEntity.attributeType.description.replace('740', '800');
                }

                DecisioningAttributeEntity decisioningAttribute = new DecisioningAttributeEntity(softAttributeEntity, hardAttributeEntity);

                result.add(decisioningAttribute);
            }
          } 
        }

        return result;
    }
}