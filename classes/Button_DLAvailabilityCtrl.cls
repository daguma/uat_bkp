public with sharing class Button_DLAvailabilityCtrl {

    @AuraEnabled
    public static DLAvailability checkForDLAvailable(String aCHRoutingNumber, String aCHBankname, Id oppId) {

        DLAvailability dla = new DLAvailability();
        Opportunity opp = [select id, lead__c, contact__c from opportunity where id = :oppId];

        if (opp != null) {

            String custIdentifier = opp.lead__c != null ? opp.lead__c : opp.contact__c;
            String oppIdentifier = opp.id;

            if (!String.isEmpty(custIdentifier) && !String.isEmpty(oppIdentifier)) {
                if (!String.isEmpty(aCHRoutingNumber)) {
                    dla = getBankInformationByRoutingNumber(aCHRoutingNumber, custIdentifier, oppIdentifier);
                } else if (!String.isEmpty(aCHBankname)) {
                    dla = getBankInformationByBankName(aCHBankname, custIdentifier, oppIdentifier);
                }
            }
        } else {
            dla.errorMessage = 'Opportunity is null';
        }

        return dla;
    }

    private static DLAvailability getBankInformationByRoutingNumber(String hiddenRoutingNbr, String customerIdentifier, String oppId) {

        DLAvailability dla = new DLAvailability();
        String bankName = '', jsonResult = '';
        try {
            if (String.isNotEmpty(hiddenRoutingNbr) && String.isNotEmpty(customerIdentifier)) {

                if(Test.isRunningTest()){
                    jsonResult =  '[{"bankName": "Chase - Bank","logoURL": "https://www.decisionlogic.com/ImageHandler.ashx?contenServiceId=20",' +
                            '"homeURL": "https://www.chase.com","contactPhone": null,"contactURL": "http://www.chase.com/"}';
                }else{
                    jsonResult = ProxyApiBankStatements.ApiGetBankInformationByRoutingNumber(customerIdentifier, oppId, UserInfo.getUserId(), hiddenRoutingNbr);
                }

                if (String.isNotEmpty(jsonResult)) {

                    jsonResult = jsonResult.replace('[', '');
                    jsonResult = jsonResult.replace(']', '');

                    Map<String, Object> bankInformationByRoutingNumberMap = (Map<String, Object>) JSON.deserializeUntyped(jsonResult);

                    bankName = (String) bankInformationByRoutingNumberMap.get('bankName') ;

                    if (String.isNotBlank(bankName) && !bankName.contains('null')) {
                        dla.successMessage = 'Decision Logic is available, this bank "' + bankName + '" was found by the routing number entered.';
                    } else {
                        dla.errorMessage = 'Bank Information from Decision Logic is Not available for the Routing Number entered (' + hiddenRoutingNbr + ').';
                    }

                } else {
                    dla.errorMessage = 'The Proxy API service is not available, please contact your system administrator.';
                }
            }
        } catch (Exception e) {
            dla.errorMessage = e.getMessage() + ' - ' + e.getStackTraceString();
        }
        return dla;
    }

    private static DLAvailability getBankInformationByBankName(String bankName, String customerIdentifier, String oppId) {

        DLAvailability dla = new DLAvailability();
        try {
            String jsonResult = '';
            if (String.isNotEmpty(bankName) && String.isNotEmpty(customerIdentifier)) {
                if(Test.isRunningTest()){
                    jsonResult =  '[{"contentServiceId": "2116","contentServiceDisplayName": "1st National Bank of Scotia - Bank",' +
                            '"homeURL": "http://www.firstscotia.com","reliability": "3"}]';
                }else{
                    jsonResult = ProxyApiBankStatements.ApiGetBankInformationByBankName(customerIdentifier, oppId, UserInfo.getUserId(), bankName);
                }
                if (String.isNotEmpty(jsonResult) && jsonResult.contains('contentServiceId')) {
                    dla.successMessage = 'Decision Logic is available by Bank Name entered';
                } else {
                    dla.errorMessage = 'Decision Logic is Not available by Bank Name, please try with a Routing Number.';
                }
            } else {
                dla.errorMessage = 'Bank name or customer identifier is null';
            }
        } catch (Exception e) {
            dla.errorMessage = e.getMessage() + ' - ' + e.getStackTraceString();
        }
        return dla;
    }

    public class DLAvailability {
        @AuraEnabled public Boolean hasError;
        @AuraEnabled public String successMessage{
            get;
            set{
                successMessage = value;
                hasError = false;
            }
        }
        @AuraEnabled public String errorMessage {
            get;
            set{
                errorMessage = value;
                hasError = true;
            }
        }

        public DLAvailability() {
            hasError = false;
        }
    }
}