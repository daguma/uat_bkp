@isTest
public class TestLoanDisbursalTxnPayPointGen {
    public static testMethod void testDisbursalPaypointFileGen(){
    
        //Load csv file
        Test.loadData(filegen__File_Metadata__c.sObjectType, 'LendingFilegen');
        
        //Setup seed data
        TestHelperForManaged.createSeedDataForTesting();
        //TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupMetadataSegments();
        loan.TestHelper.systemDate = Date.newInstance(2014, 3, 3); // Mar 3 - Monday
      
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
       
        loan__Transaction_Approval_Config__c c = new loan__Transaction_Approval_Config__c();
            c.loan__Payment__c = true;
            c.loan__Payment_Reversal__c = false;
            c.loan__Funding__c = false;
            c.loan__Funding_Reversal__c = false;
            c.loan__Write_off__c = false;
            c.loan__Accounting__c = false;
          
            insert c;
        
        Account acc=new Account(Name='test' ,Type='Partner',Allow_Offers__c=true,Display_only_Effective_APR__c=true,URL_AAN_Process__c=true,Suppress_AAN__c=true,Delayed_hours_to_send_AAN__c=12, AAN_Email_Template__c='CK AAN Notification', Is_Parent_Partner__c=true,Is_child_Partner__c=false);
          acc.Partner_Sub_Source__c = 'LPCU';
         insert acc;

        
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = TestHelperForManaged.createLoanPurpose();
        loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();
        acha.Disburse_to_Providers__c = true;
        update acha;
        
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        b1.peer__National_Id__c = '1223';
        update b1;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Advance Trust Account');
        
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];
        
        Partner_Product_Bank_Account__c pb = new Partner_Product_Bank_Account__c();
        pb.Account__c = acc.Id;
        pb.Bank_Account_Number__c = '12345';
        pb.Routing_Number__c = '908765421';
        pb.Product__c = dummyLP.id;
        pb.Deposit_to_Vendor__c = true;
        insert pb;
        
        //Create a dummy Loan Account
        loan__Loan_Account__c dummylaMonthly = LibraryTest.createContractTH();
        dummylaMonthly.loan__Loan_Status__c = 'Approved';
        dummylaMonthly.loan__Borrower_ACH__c = dummyBank.Id;
        dummylaMonthly.loan__ACH_On__c = true;
        dummylaMonthly.loan__ACH_Frequency__c = 'Monthly';
        dummylaMonthly.loan__ACH_Next_Debit_Date__c = Date.today().addDays(30);//loan.TestHelper.systemDate;
        dummylaMonthly.loan__ACH_Start_Date__c = loan.TestHelper.systemDate.addDays(30);
        dummylaMonthly.loan__ACH_Debit_Amount__c = 100;
        dummylaMonthly.loan__Ach_Debit_Day__c = 3;
        dummylaMonthly.loan__ACH_End_Date__c = loan.TestHelper.systemDate.addYears(1);
        dummylaMonthly.loan__Loan_Product_Name__c = dummyLP.id;
        
        Update dummylaMonthly;
        

        
        loan__Payment_Mode__c payModDisbusral = [Select id,Name from loan__Payment_Mode__c where Name='ACH'];
        
        //Create a Loan Disbursal Transaction
        loan__Loan_Disbursal_Transaction__c loanDis = new loan__Loan_Disbursal_Transaction__c(loan__Loan_Account__c = dummylaMonthly.Id,
                                                                                              loan__Mode_of_Payment__c=payModDisbusral.id,
                                                                                              loan__Disbursed_Amt__c= 10000,
                                                                                              loan__Cleared__c = true
                                                                                               );
        insert loanDis;
        
        
        

         opportunity a = [SELECT id, name, contact__c, Partner_Account__c FROM opportunity where Id =: dummylaMonthly.opportunity__c];
        a.Partner_Account__c = acc.Id;
        update a;
        //update dummylaMonthly;
        Provider_Payments__c pp = new Provider_Payments__c();
        pp.Opportunity__c = a.Id;
        pp.Payment_Amount__c = 1000;
        pp.Procedure_Date__c = Date.today();
        pp.Provider_Name__c = acc.Id;
        insert pp;
            
        System.debug('Payment mode: '+loanDis.loan__Mode_of_Payment__c+'  :: name:: '+payModDisbusral.name+'  :: id::'+payModDisbusral.id);
        System.debug('Loan Status: '+dummylaMonthly.loan__Loan_Status__c);
        system.debug('disbursement cleared:'+loanDis.loan__Cleared__c);
        test.startTest();
        loan.LoanDisbursalTxnSweepToACHJob j = new loan.LoanDisbursalTxnSweepToACHJob(false);
        Database.executeBatch(j);
        test.stopTest();
     
     
        loan__Loan_Disbursal_Transaction__c loanDisTxn = [select id,name,
                                                                 loan__Disbursed_Amt__c,
                                                                 loan__ACH_Filename__c,
                                                                 loan__Sent_To_ACH__c,
                                                                 loan__Loan_Account__r.id,
                                                                 loan__Loan_Account__r.name,
                                                                 loan__Loan_Account__r.loan__Loan_Amount__c,
                                                                 loan__Loan_Account__r.Reporting_Fee__c ,
                                                                 loan__Loan_Account__r.loan__ACH_Account_Type__c,
                                                                 loan__Loan_Account__r.loan__ACH_Routing_Number__c,
                                                                 loan__Loan_Account__r.loan__ACH_Account_Number__c,
                                                                 loan__Loan_Account__r.loan__ACH_Relationship_Type__c ,
                                                                 loan__Loan_Account__r.loan__Contact__c ,
                                                                 loan__Loan_Account__r.loan__Contact__r.Name,
                                                                 loan__Loan_Account__r.loan__Loan_Product_Name__c ,
                                                                 loan__Loan_Account__r.loan__ACH_Drawer_Name__c,
                                                                 Refinance_Payoff_Amount__c 
                                                                 
                                                            from loan__Loan_Disbursal_Transaction__c 
                                                           where loan__Loan_Account__c =: dummylaMonthly.id]; 
      
        LoanDisbursalTxnPayPointGen inst = new LoanDisbursalTxnPayPointGen();
       // inst.processProviderPayments(loanDisTxn);        
        inst.createDetailRecord('tCode', 'aba', 'debitAccount', 5000, '562312', 'iName', '45454');
      //  inst.generateEntryDetailRecord(loanDisTxn, b1.id, 5000.00);
        
        System.debug('pmt txn: '+ loanDisTxn);
        System.debug('ACH Status: '+loanDisTxn.loan__Sent_To_ACH__c);
        System.debug('file name: '+loanDisTxn.loan__ACH_Filename__c);
        System.assertEquals('Shared Folder for ACH', acha.loan__Folder_Name__c);
      
        List<loan__Loan_Disbursal_Transaction__c> lt = [select Id from loan__Loan_Disbursal_Transaction__c 
                                                    ];
        LoanDisbursalSummarizedByLine s = new LoanDisbursalSummarizedByLine();
        s.generateFile(lt, 'FIN', 'FINWISE', false);
        
  }   
}