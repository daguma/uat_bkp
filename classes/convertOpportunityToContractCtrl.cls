public class convertOpportunityToContractCtrl {
    
    public static string createContract(ID OpportunityId) { // Temporary Change, Replace 'webservice static' with 'public'
        
        String opportunityConversionStatus;
        Map<SObject,SObject> oppContractMap = new Map<SObject,SObject>();
        If(OpportunityID <> null) {
            try{
                Opportunity currentOpp = new Opportunity(Id = OpportunityId);
                
                loan__Loan_Account__c newContract = new loan__Loan_Account__c();    
                oppContractMap.put(currentOpp,newContract);
                
                /***************************/
                ApplicationToContractConverter ctrl = new ApplicationToContractConverter();
                ctrl.setContracts(oppContractMap);
                opportunityConversionStatus = ctrl.processContract();
                
                /**************************/
            }catch(Exception e){
                System.debug(e.getTypeName() + 'occured at line no:' + e.getLineNumber() + '.The exception says ' +e.getMessage());
            }
        }
        system.debug('opportunityConversionStatus==== ' +opportunityConversionStatus);
        return opportunityConversionStatus;  
    }
    
}