global with sharing class  DocUploadService {
    public static string appendedNoteText='File from APIautoworkflow:';
    /** Method to insert the attachment under Application and to update Income Source in the Application**/
    WebService static string UploadFile(string fileparameters){
        string isUpload = 'false', offerId, partnerId,invoiceId, docType, status = 'err', message = 'empty record id', OppId;
        integer fileLimit = 5;
        boolean reWrite   = false, reachedLimit = false;
        Map<string,string> ReturnMap = new Map<string,string>();
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        
        try{
            //Fetching all the values from JSON request Map
            Map<string,string> fileMap = (Map<String,string>) JSON.deserialize(fileparameters, Map<String,string>.class);
            System.debug('============fileMap============================='+fileMap);                
            
            if(fileMap.ContainsKey('offerId') && String.isNotEmpty(fileMap.get('offerId'))){offerId = fileMap.get('offerId').trim();}
            if(fileMap.ContainsKey('appId') && String.isNotEmpty(fileMap.get('appId'))){OppId = fileMap.get('appId').trim();}
            //API-117
            if(fileMap.ContainsKey('docType') && String.isNotEmpty(fileMap.get('docType'))){docType = fileMap.get('docType').trim();} 
            if(fileMap.ContainsKey('partnerId') && String.isNotEmpty(fileMap.get('partnerId'))){partnerId = fileMap.get('partnerId').trim();}
            if(fileMap.ContainsKey('invoiceId') && String.isNotEmpty(fileMap.get('invoiceId'))){invoiceId = fileMap.get('invoiceId').trim();}
            System.debug('============docType ============================='+docType );
            System.debug('============invoiceId ============================='+invoiceId );
            
            list<Opportunity> OppList = new List<Opportunity>();
            list<Account> partnerList = new List<Account>();
            system.debug('----partnerId----'+partnerId+'----offerId---'+offerId);
            
            if((String.isNotEmpty(offerId) && String.isNotEmpty(partnerId)) 
                || (String.isNotEmpty(offerId) && String.isNotEmpty(OppId))
                || (String.isNotEmpty(partnerId) && String.isNotEmpty(OppId)))   
                message = 'Invalid request';
            else if(String.isNotEmpty(offerId) 
                    || String.isNotEmpty(partnerId)  || String.isNotEmpty(OppId)) {
                        
                        if(String.isNotEmpty(offerId))
                            OppList = [Select id,Invoice_Number__c, Income_Source__c, Status__c, OwnerId, Name, Contact__c, 
                                       Contact__r.AccountId, Contact__r.Employment_Type__c, Contact__r.Second_Income_Type__c, 
                                       All_Applicants_XML__c, Employed_Applicants_1_XML__c,Retired_Applicants_XML__c, 
                                       Self_Employed_Applicants_1_XML__c, Self_Employed_Applicants_2_XML__c, 
                                       Miscellaneous_Applicants_XML__c, Applicants_Doc_Code__c From Opportunity 
                                       Where id IN (select Opportunity__c from Offer__c where id =: offerId)];
                        
                        else if(String.isNotEmpty(OppId)) 
                            OppList = [Select id,Invoice_Number__c, Income_Source__c, Status__c, OwnerId, Name, Contact__c, 
                                         Contact__r.AccountId, Contact__r.Employment_Type__c,Contact__r.Second_Income_Type__c, 
                                         All_Applicants_XML__c, Employed_Applicants_1_XML__c, Employed_Applicants_2_XML__c, 
                                         Retired_Applicants_XML__c, Self_Employed_Applicants_1_XML__c, 
                                         Self_Employed_Applicants_2_XML__c, Miscellaneous_Applicants_XML__c, 
                                         Applicants_Doc_Code__c From Opportunity Where id =: OppId]; 
                        
                        if(String.isNotEmpty(partnerId))    
                            partnerList = [select id, Name, OwnerId from Account where id = :partnerId]; 
                        system.debug('----partnerId----'+partnerId+'----offerId---'+offerId);
                        
                        if(OppList.isEmpty() && partnerList.isEmpty())
                            message = 'Record not found with given Id';
                else {
                    ID ParentId = !OppList.isEmpty() ? OppList[0].id : (!partnerList.isEmpty() ? partnerList[0].id : '');
                    Map<string,Upload_File_Max_Limits__c> docLimits = Upload_File_Max_Limits__c.getAll();
                    
                    if(docLimits.containsKey(docType)){
                        fileLimit = (docLimits.get(docType).No_of_Files_Limit__c <> null)?integer.valueOf(docLimits.get(docType).No_of_Files_Limit__c):fileLimit;
                        reWrite   = (docLimits.get(docType).ReWrite__c <> null)?docLimits.get(docType).ReWrite__c: reWrite;
                    }            
                    
                    system.debug('----partnerId----'+partnerId+'----offerId---'+offerId);
                    
                    if(string.isNotEmpty(invoiceId)){
                        appendedNoteText = 'File From Merchant Portal:'; 
                    }
                    
                    
                    string Descrp = appendedNoteText + docType;
                               
                    
                    list<Attachment> ExistingAttachmnts = [select id from Attachment where Description like : Descrp and parentid =: ParentId order by CreatedDate];                      
                    System.debug('============ExistingAttachmnts============================='+ExistingAttachmnts);  
                    
                    reachedLimit = ExistingAttachmnts.size() >= fileLimit;
                    
                    if((reachedLimit && !reWrite) || (fileLimit < 1)){
                        status = 'ver';
                        message = (fileLimit < 1) ? 'No documents can be attached. Check upload custom settings.':'Cannot upload more than '+fileLimit+' files for this Document Type.';
                    } else { 
                        if(reachedLimit && reWrite){
                            Attachment attachRow = ExistingAttachmnts[0];
                            ReturnMap.put('deleteAttachmentId', attachRow.Id);
                        }
                        
                        status = '200';
                        isUpload = 'true';
                        message = 'Upload an Attachment';
                        ReturnMap.put('parentId', ParentId);
                    } 
                }
                if(OppList.size() > 0){
                    Opportunity app = OppList[0];
                    System.debug('invoiceId..>>>>> '+invoiceId); 
                        if(String.isNotEmpty(invoiceId)){
                            app.Invoice_Number__c = invoiceId;
                            update app;
                        } 
                }
                
            } 
        }catch(Exception e){       
            message = e.getMessage(); 
            WebToSFDC.notifyDev('DocumentUpload.UploadFile Error ',  message + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
        
        ReturnMap.put('status', status);
        ReturnMap.put('message', message); 
        ReturnMap.put('isUpload', isUpload);
        system.debug('--------return----'+JSON.Serializepretty(ReturnMap));
        return JSON.Serializepretty(ReturnMap);
    }
    
     /** Method to return Document names and Income Source under Application of given Offer**/
    WebService static string GetFiles(string ReqParameters){
        string offerId, status = 'err';
        Map<string, string> ReturnMap = new Map<string, string>(), AttachMap = new Map<string, string>();
        boolean AppFound = false;
        try{                
            //Fetching all the values from JSON request Map
            Map<string,string> fileMap = (Map<String,string>) JSON.deserialize(ReqParameters, Map<String,string>.class);
            System.debug('============fileMap============================='+fileMap);                
            ReturnMap.put('docs','');
            ReturnMap.put('incomeSource','');
            
            
            if(fileMap.ContainsKey('offerId')){offerId = fileMap.get('offerId').trim();}
            
            if(String.isNotEmpty(offerId)){
               
                for (Opportunity App : [select id, Income_Source__c from Opportunity where id = : [select Opportunity__c from Offer__c where id =: offerId].Opportunity__c]){
                    ReturnMap.put('incomeSource',App.Income_Source__c);
                    
                    list<Attachment> attachlist = [select id,name,Description from Attachment where Description like : 'File from APIautoworkflow:%' and parentid =: App.id];
                    system.debug('=============attachlist==============='+attachlist);
                    
                    if(!attachlist.IsEmpty()){              
                        
                        for(Attachment atch: attachlist){                
                            string docType = atch.Description.substringAfter('File from APIautoworkflow:').trim(), fileName = atch.name; 
                            system.debug('===docType========================'+docType);              
                            
                            if(AttachMap.ContainsKey(docType)){
                                fileName = AttachMap.get(docType) + ';'+ atch.name;
                            }
                            
                            AttachMap.put(docType,fileName);               
                        }  
                        
                        system.debug('===AttachMap========================'+AttachMap); 
                        
                        String DocNames = JSON.Serializepretty(AttachMap);
                        system.debug('===DocNames========================'+DocNames); 
                        ReturnMap.put('docs', DocNames);
                        status = '200';
                        ReturnMap.put('message','Successful');          
                        AppFound = true;       
                    }else{
                        status = '0';               
                        ReturnMap.put('message','No Documents Found');
                    }                     

                    if (!AppFound) ReturnMap.put('message','No App Found');
                }
            }else{
                ReturnMap.put('message','empty offer id');
            }
        }Catch(Exception e){
            WebToSFDC.notifyDev('DocumentUpload.getFiles Error ',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());            
            ReturnMap.put('message',e.getMessage()); 
        }  
        
        ReturnMap.put('status', status);
        system.debug('===ReturnMap========================'+ReturnMap); 
        return JSON.Serializepretty(ReturnMap); 
    }
    
    /** Method to insert notes at the end of Docs upload**/
    //3695 Changed Note Title Recorded in salesforce
    WebService static string insertNotes(string attachIds,string source){  
        system.debug('=========source============='+source);
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();    
        Testing_Settings__c TS = Testing_Settings__c.getInstance('TestData'); 
        list<string> DocIds = attachIds.split(';'); 
        string DocBody, parentid, emailAddr;     
        map<string,string> ReturnMap = new map<string,string>(); 
        boolean bIsSandBox = OfferCtrlUtil.runningInASandbox(),  Testdata = false;        
        if(string.IsEmpty(source)) source='Web';
        ReturnMap.put('noteid', '');
        Note DocNote;List<Note> lstNote=new List<Note>();
        try{   
            
            list<Attachment> ExistingAttch = [select name, id, createddate,parentid,Description from Attachment where id IN: DocIds order by createddate desc];             
             
            if(!ExistingAttch.IsEmpty()){
               parentid=ExistingAttch[0].parentid;
                for(Attachment atch: ExistingAttch){
                    DocNote=new Note(parentid =atch.parentid); 
                    string docType = atch.name;
                    if(string.IsNotEmpty(atch.Description) && string.IsNotEmpty(atch.Description.substringAfter(appendedNoteText)))
                      docType=atch.Description.substringAfter(appendedNoteText);
                    system.debug(docType);
                    DocNote.Title = docType + ' - Uploaded from ' + source ;
                    DocNote.body = atch.name +'    (Uploaded at '+ atch.createddate.format() + ')';   
                    lstNote.add(DocNote);          
                }
                                
                Opportunity App = [select id, name, Contact__r.name,Contact__r.Email, Contact__r.ints__Social_security_Number__c from Opportunity where id=:ParentID];
                
                if ([select id from Case where (ContactId =: App.Contact__c OR Opportunity__c =:App.id) and (Subject like 'Documents uploaded via the Web for App%' OR Subject like '%Documents Have Been Uploaded from%') AND Status <> 'Closed' ].size() == 0) {
                
                    Testdata = ((App.Contact__r.Email == TS.Test_Email__c) && (App.Contact__r.ints__Social_security_Number__c == TS.Test_SSN__c));                    
                    emailAddr =  cuSettings.Docusign_Notification_Email__c;              
                    String subject = App.Contact__r.name +' - '+ App.Name +' - Documents Have Been Uploaded from ' + source;                    
                   
                    String Body = 'The customer has uploaded the documents from ' + source + ' and these documents are now available as attachments in SalesForce. Email Id of Customer is '+App.Contact__r.Email+'. The documents were uploaded on '+System.now().format()+'.\n\nThis email has been automatically generated.';
                    WebToSFDC.notifyDev(subject, Body, emailAddr);
                }
                ReturnMap.put('status','200');
                ReturnMap.put('message', 'Note inserted Successfully');
                
            }else{
                ReturnMap.put('status','0');
                ReturnMap.put('message', 'No Documents found');     
            }
        }Catch(Exception exc){
            ReturnMap.put('status', 'err');
            ReturnMap.put('message',exc.getMessage()); 
        }     
        finally{
            if(!lstNote.isEmpty()) insert lstNote;
        }
        system.debug(JSON.Serializepretty(ReturnMap));
        return JSON.Serializepretty(ReturnMap); 
    }
    
    /** Method to insert the attachment under Application according to tokenized url**/
    /***** INC 1800 ****/   
     WebService static string uploadTokenFile(string fileparameters){
        Map<string,string> ReturnMap = new Map<string,string>();
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        Blob token,fileContent,decrypted;
        string docType,encryptedToken, status = 'err',fileName,AppName,decryptedString,APP_ID=''; 
        integer fileLimit = 5;
        boolean reWrite   = false,reachedLimit = false, bDoAssignment = false;
        DateTime ExpirationDate;
        Opportunity App;
        boolean bIsSandBox = OfferCtrlUtil.runningInASandbox();
        try{
            //Fetching all the values from JSON request Map
            Map<string,string> fileMap = (Map<String,string>) JSON.deserialize(fileparameters, Map<String,string>.class);
            System.debug('=====fileMap====='+fileMap); 
            
            
            if(fileMap.ContainsKey('docType')){docType = fileMap.get('docType').trim();}
            if(fileMap.ContainsKey('fileName')){fileName = fileMap.get('fileName').trim();}         
            if(fileMap.ContainsKey('fileContent')){fileContent = EncodingUtil.base64Decode(fileMap.get('fileContent'));} 
            if(fileMap.ContainsKey('token')){encryptedToken = fileMap.get('token');} 
            
            System.debug('=====docType====='+docType ); 
            System.debug('=====fileName====='+fileName);
            System.debug('=====encryptedToken====='+encryptedToken); 
            ReturnMap.put('docId', '');
            map<String,String> decryptedMap= convertEncryptedToken(encryptedToken);
            if(decryptedMap.containsKey('message')){
                ReturnMap.put('message', decryptedMap.get('message'));
                status='0';
            }
            else{
                if(decryptedMap.containsKey('AppName')){
                    AppName=decryptedMap.get('AppName');
                }
                list<Opportunity> AppList = [select id,Status__c, Contact__c,Contact__r.AccountId,Name, OwnerID,
                               Contact__r.Employment_Type__c,Contact__r.Second_Income_Type__c,
                               All_Applicants_XML__c,Employed_Applicants_1_XML__c,Employed_Applicants_2_XML__c,
                               Retired_Applicants_XML__c,Self_Employed_Applicants_1_XML__c,Self_Employed_Applicants_2_XML__c,
                               Miscellaneous_Applicants_XML__c, Opportunity_Number__c,ApplicationName__c,Applicants_Doc_Code__c 
                               from Opportunity where (ApplicationName__c = : AppName or Name = : AppName or Opportunity_Number__c = : AppName)];
                if(AppList.isEmpty()){
                    ReturnMap.put('status', '0');
                    ReturnMap.put('APP_ID', '');
                    ReturnMap.put('message','Application not found with the given token.');
                    return JSON.Serializepretty(ReturnMap);
                }else
                    App = AppList[0];
                APP_ID=App.id;
                Map<string,Upload_File_Max_Limits__c> docLimits = Upload_File_Max_Limits__c.getAll();
                if(docLimits.containsKey(docType)){
                    fileLimit = (docLimits.get(docType).No_of_Files_Limit__c <> null)?integer.valueOf(docLimits.get(docType).No_of_Files_Limit__c):fileLimit;
                    reWrite   = (docLimits.get(docType).ReWrite__c <> null)?docLimits.get(docType).ReWrite__c: reWrite;
                }
                string Descrp = appendedNoteText + docType;
                list<Attachment> ExistingAttachmnts = [select id from Attachment where Description like : Descrp and parentid =: App.id order by CreatedDate];
                System.debug('=====ExistingAttachmnts====='+ExistingAttachmnts);
                if(ExistingAttachmnts.size() >= fileLimit)                    reachedLimit = true;
                if((reachedLimit && !reWrite) || (fileLimit < 1)){
                    status= 'ver';
                    ReturnMap.put('message',(fileLimit < 1)?'No documents can be attached. Check upload custom settings.':'Cannot upload more than '+fileLimit+' files for this Document Type.');
                }else{
                    if(reachedLimit && reWrite){
                        delete  ExistingAttachmnts[0];
                    }
                    if(docType.containsIgnoreCase('Signed Contract')){
                        fileName = 'Signed Contract.'+fileName.substringAfterLast('.');
                        DocusignService.NoteOnNewContractAfterRegrade(app.Id);
                    }
                    Attachment Attch = new Attachment(Name = fileName , Description = Descrp , parentid = App.id, body = fileContent);
                    insert Attch;

                    if (app.Status__c == 'Credit Approved - No Contact Allowed')  { app.Status__c = 'Offer Accepted'; bDoAssignment = true; }
                        app = applyDashboardService.removeDispositionsOnReUpload(docType,app);  //API-567
                    update app;
                        
                    if (bDoAssignment && app.OwnerId == cuSettings.WEB_API_User_Id__c)  DealAutomaticAssignmentManager.assignExistingAppFromWeb(app.Id);

                    ReturnMap.put('docId', Attch.id);
                    status= '200';
                    ReturnMap.put('message','Upload Successful');
                }
            }
        }catch(Exception e){
            if(!bIsSandBox)                WebToSFDC.notifyDev('DocUploadService.uploadTokenFile Error ',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
            status= 'err';
            ReturnMap.put('message',e.getMessage());   
        }
        ReturnMap.put('status', status);
        ReturnMap.put('APP_ID', APP_ID);
        return JSON.Serializepretty(ReturnMap); 
    }
    
     //3249 | Document Center: Change Application Status when Docs Are Uploaded
    WebService static String updateTokenizedApplicationStatus(String inputString){
        String encryptedToken='',AppName, Status= 'err', msg ='';
        boolean bIsSandBox = OfferCtrlUtil.runningInASandbox();
        JSONGenerator responseJson=JSON.createGenerator(true);
        responseJson.writeStartObject();
        try{
            Map<string,string> appIdMap = (Map<String,string>) JSON.deserialize(inputString, Map<String,string>.class);
            if(appIdMap.ContainsKey('token')) encryptedToken = appIdMap.get('token');
            if(!String.isEmpty(encryptedToken) ){
                map<String,String> decryptedMap= convertEncryptedToken(encryptedToken);
                if(decryptedMap.containsKey('message')){
                    msg = decryptedMap.get('message');
                }
                else{
                    if(decryptedMap.containsKey('AppName')){
                        AppName=decryptedMap.get('AppName');
                    }
                    list<Opportunity> AppList = new list<Opportunity>();
                    if(String.isNotEmpty(AppName))
                        AppList = [select id,Status__c,Reason_of_Opportunity_Status__c,Items_Requested__c,ApplicationName__c,Opportunity_Number__c from Opportunity where (ApplicationName__c = : AppName or Name = : AppName or Opportunity_Number__c = : AppName)];
                    if(AppList.isEmpty()){
                        msg = 'Application not found with the given token.';
                    }
                    else{
                        Opportunity app=AppList[0];
                        /** DC-5 **/
                        app.Status__c='Documents Received';
                        app.Status__c=label.Document_App_Status;
                        if(string.IsNotempty(app.Reason_of_Opportunity_Status__c)) app.Reason_of_Opportunity_Status__c=null;
                        /** DC-5 **/
                        update app;
                        Status = '200';
                        msg = 'Application Updated Successfully.';
                    }
                }
            }
            else{
                responseJson.writeStringField('message', 'Token is Empty ');
            }
        }
        catch(Exception e){
            if(!bIsSandBox)                WebToSFDC.notifyDev('DocUploadService.updateApplicationStatus Error ',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
            msg = e.getMessage();
        }
        responseJson.writeStringField('message', msg);
        responseJson.writeStringField('status', Status);
        responseJson.writeEndObject();
        return responseJson.getAsString();
    }
    
    @TestVisible
    public static Map<String,String> convertEncryptedToken(String encryptedToken){
        blob decrypted,token;
        String decryptedString,AppName,ExpirationDateString;
        DateTime ExpirationDate;
        Map<String,String> ReturnMap = new Map<String,String>();
        Blob Key=EncodingUtil.base64Decode(Label.Documnet_Encrypted_Key);
        System.debug('=====Key=====' + Key);
        boolean bIsSandBox = OfferCtrlUtil.runningInASandbox();
        if(Label.Documnet_Encrypted_Key!=null && encryptedToken !=null){
            token = EncodingUtil.base64Decode(encryptedToken);
            try{
                decrypted = Crypto.decryptWithManagedIV('AES128', key, token);
                decryptedString = decrypted.toString();
                System.debug('===decryptedString===' + decryptedString);
                String[] FinalData=decryptedString.split('DBQT');
                for(string data: FinalData){
                    if(data.startsWith('APP-')) AppName=data; 
                   else {ExpirationDate = DateTime.valueOf(data);ExpirationDateString=data;} 
                }
            }
            catch(exception e){
                if(!bIsSandBox)                    WebToSFDC.notifyDev('DocUploadService.convertEncryptedToken Error ',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
                system.debug('==Exception Message : Token is not valid.==');       
            } 
            if(AppName!=null && ExpirationDate!=null){
                if(System.now() < ExpirationDate ){
                    ReturnMap.put('AppName' , AppName);
                    ReturnMap.put('ExpirationDateString' , ExpirationDateString );
                }
                else ReturnMap.put('message','Token Expired. ');
            } 
            else ReturnMap.put('message','Token is invalid. ');
        }
        else ReturnMap.put('message','Token is Empty ');
        return ReturnMap;
    }
    
}