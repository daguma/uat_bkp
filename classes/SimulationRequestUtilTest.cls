@isTest
private class SimulationRequestUtilTest {

    @isTest static void run_simulation_qualified(){
        
        BRMS_Configurable_Settings__c settings = new BRMS_Configurable_Settings__c();
        settings.Name = 'settings';
        settings.Do_Not_Call__c = false;
        settings.Milestone_Day__c = Date.today().addDays(20);
        settings.Min_Rule_Count__c = 6;
        settings.Show_all_rules__c = true;
        upsert settings;
        
        List<genesis__applications__c> appList = new List<genesis__applications__c>();
        
        genesis__applications__c app = LibraryTest.createApplicationTH();
       
        appList.add(app);
        
        Test.startTest();
        
        SimulationRequestUtil.runSimulationRequest(appList);
        
        Test.stopTest();
        
        app = [SELECT Id, genesis__status__c FROM genesis__applications__c WHERE Id =: app.Id LIMIT 1];
        
        System.assertEquals('Credit Qualified', app.genesis__status__c);
    }
    
    @isTest static void run_simulation_declined(){
        
        BRMS_Configurable_Settings__c settings = new BRMS_Configurable_Settings__c();
        settings.Name = 'settings';
        settings.Do_Not_Call__c = false;
        settings.Milestone_Day__c = Date.today().addDays(20);
        settings.Min_Rule_Count__c = 6;
        settings.Show_all_rules__c = true;
        upsert settings;
        
        List<genesis__applications__c> appList = new List<genesis__applications__c>();
        
        genesis__applications__c app = LibraryTest.createApplicationTH();
        
        Scorecard__c newScore = new Scorecard__c();
        newScore.Acceptance__c = 'N';
        newScore.Grade__c = 'C2';
        newScore.Application__c = app.Id;
        upsert newScore;
        
        System.debug(newScore.Acceptance__c);
        
        appList.add(app);
        
        Test.startTest();
        
        SimulationRequestUtil.runSimulationRequest(appList);
        
        Test.stopTest();
        
        app = [SELECT Id, genesis__status__c FROM genesis__applications__c WHERE Id =: app.Id LIMIT 1];
        
        System.assertEquals('Declined (or Unqualified)', app.genesis__status__c);
    }
    
    @isTest static void run_simulation_brms(){
        
        BRMS_Configurable_Settings__c settings = new BRMS_Configurable_Settings__c();
        settings.Name = 'settings';
        settings.Do_Not_Call__c = false;
        settings.Milestone_Day__c = Date.today().addDays(-20);
        settings.Min_Rule_Count__c = 6;
        settings.Show_all_rules__c = true;
        upsert settings;
        
        List<genesis__applications__c> appList = new List<genesis__applications__c>();
        
        genesis__applications__c app = LibraryTest.createApplicationTH();
        
        appList.add(app);
        
        Test.startTest();
        
        SimulationRequestUtil.runSimulationRequest(appList);
        
        Test.stopTest();
        
        app = [SELECT Id, genesis__status__c FROM genesis__applications__c WHERE Id =: app.Id LIMIT 1];
        
        System.assertEquals('Declined (or Unqualified)', app.genesis__status__c);
    }
    
}