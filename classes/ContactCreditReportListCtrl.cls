public with sharing class ContactCreditReportListCtrl {

    public Contact currentContact {get; set;}
    public List<ProxyApiCreditReportEntityLite> crList {get; set;}
    public Boolean hasAllCRInformation {get; set;}
    public String crDetailsLink {get; set;}

    /**
     * [ContactCreditReportListCtrl description]
     * @param  controller [description]
     * @return            [description]
     */
    public ContactCreditReportListCtrl(ApexPages.StandardController controller) {

        this.currentContact = (Contact)controller.getRecord();
        hasAllCRInformation = true;

        if (this.currentContact != null && this.currentContact.id != null) {

            this.currentContact = [SELECT Id, Name
                                   FROM Contact
                                   WHERE Id = : this.currentContact.Id];

            crList = CreditReport.getAllByEntityId(this.currentContact.Id);

            if (crList == null) {
                crList = new List<ProxyApiCreditReportEntityLite>();
                hasAllCRInformation = false;
            }
        }

        crDetailsLink = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/CreditReportDetails?id=' + this.currentContact.id + '&isContact=true' + '&crid=';
    }

    /**
     * [initialize description]
     * @return [description]
     */
    public pagereference initialize() {

        return null;
    }

    /**
     * [goBack description]
     * @return [description]
     */
    public PageReference goBack() {

        return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.currentContact.Id);
    }

    /**
     * [createMessage description]
     * @param severity [description]
     * @param message  [description]
     */
    private void createMessage(ApexPages.severity severity, String message) {

        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }
}