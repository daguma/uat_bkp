@isTest public class TokenAuthenticationTest {

    private static GDS_Configuration__c create_gds_Config_Setting() {
        GDS_Configuration__c setting = new GDS_Configuration__c();
        setting.SF_Token__c = 'test';
        insert setting;
        
        return setting;
    }
    
    @isTest static void test_validate() {
        
        GDS_Configuration__c setting = create_gds_Config_Setting();
        
        String inputparams = '{"token":"test token"}';
        String response = TokenAuthentication.updateSfToken(inputparams);
        
        System.assertEquals('success', response);
        System.assertEquals('test token', GDS_Configuration__c.getOrgDefaults().SF_Token__c);
    }
    
    @isTest static void test_validate_notoken() {
        
        GDS_Configuration__c setting = create_gds_Config_Setting();
        
        String inputparams = '{"token01":"test token"}';
        String response = TokenAuthentication.updateSfToken(inputparams);
        
        System.assertEquals('token is not present', response);
        System.assertNotEquals('test token', setting.SF_Token__c);
    }
    
    @isTest static void test_validate_error() {
        
        String inputparams = '{token":"test token"}';
        String response = TokenAuthentication.updateSfToken(inputparams);
        
        System.assertEquals('error', response);
    }
}