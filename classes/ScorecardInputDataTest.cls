@isTest public class ScorecardInputDataTest {
    @isTest static void validate() {

        ScorecardInputData score = new ScorecardInputData();
        score.creditReportId = 1;
        score.income = '100';
        score.loanAmount = '50';
        score.employmentType = 'Employee';
        score.state = 'OH';
        score.isPartner = 'false';
        score.employmentDuration = '3';
        score.employmentGapDurationDays = '200';
        score.priorEmploymentDuration = '100';
        score.paymentFrequency = 'M';
        score.analyticRandomNumber = '99';
        score.feeHandling = 'Fee on Top';
        score.partner = 'CreditKarma';
        score.isFinwise = true;
        score.productCode = 'LP002';
        
        score.entityId = 'test';
        score.bureau = 'EX';
        score.isMrcState = True;
        score.merchantState = 'AL';
        Score.bankName = 'Test Bank';
        
        GDSRequestParameter gdsReqParam = new GDSRequestParameter();
        gdsReqParam.contactId = 'test';
        gdsReqParam.channelId  = 'test';
        gdsReqParam.sequenceId  = 'test';
        gdsReqParam.productId  = 'test';
        gdsReqParam.sessionId  = 'test';
        gdsReqParam.executionType  = 'test';
        
        score.gdsRequestParameter = gdsReqParam;

        score.applicationType = 'Refinance';
        score.previousGrade = 'B1';
        score.previousTerm = '24';
        score.fundedApr = '0.1999';
        score.currentPayoffBalance = '5000';

        System.assertEquals(1, score.creditReportId);
        System.assertEquals('100', score.income);
        System.assertEquals('50', score.loanAmount);
        System.assertEquals('Employee', score.employmentType);
        System.assertEquals('OH', score.state);
        System.assertEquals('false', score.isPartner);
        System.assertEquals('3', score.employmentDuration);
        System.assertEquals('200', score.employmentGapDurationDays);
        System.assertEquals('100', score.priorEmploymentDuration);
        System.assertEquals('M', score.paymentFrequency);
        System.assertEquals('99', score.analyticRandomNumber);
        System.assertEquals('Fee on Top', score.feeHandling);
        System.assertEquals('CreditKarma', score.partner);
        System.assertEquals(true, score.isFinwise);
        System.assertEquals('LP002', score.productCode);
        System.assertEquals('test', score.entityId );
        System.assertEquals('EX', score.bureau );
        System.assertEquals(gdsReqParam, score.gdsRequestParameter);
        System.assertEquals('Refinance', score.applicationType);
        System.assertEquals('B1', score.previousGrade);
        System.assertEquals('24', score.previousTerm);
        System.assertEquals('0.1999', score.fundedApr);
        System.assertEquals('5000', score.currentPayoffBalance);
        System.assertEquals(True, score.isMrcState);
        System.assertEquals('AL', score.merchantState);
        System.assertEquals('Test Bank', Score.bankName);
    }
}