@isTest public class DealAutomaticAssignmentUtilsTest {

    @isTest static void gets_all_users_groups_from_centers() {

        List<DealAutomaticUserGroup> list1 = DealAutomaticAssignmentUtils.getAllUsersGroupsFromCenters();

        System.assert(list1.size() > 0);
        System.assertNotEquals(null, list1);
    }

    @isTest static void gets_all_users_groups_for_summary_page() {

        List<DealAutomaticUserGroup> list1 = DealAutomaticAssignmentUtils.getAllUsersGroupsForSummaryPage();

        System.assert(list1.size() > 0);
        System.assertNotEquals(null, list1);
    }

    @isTest static void save_users() {

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'CreditQualified';
        attribute.Display_Name__c = 'Credit Qualified';
        attribute.Key_Name__c = 'Credit Qualified';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 2;
        attribute.Display_Order__c = 1;
        insert attribute;

        Integer listBef = [SELECT COUNT() FROM Deal_Automatic_Assignment_Data__c];

        DealAutomaticUserGroup daug = new DealAutomaticUserGroup('userId_1000', 'userName_Test', 'groupId_2000', 'groupName_Test', true, false);

        List<DealAutomaticUserGroup> listDaug = new List<DealAutomaticUserGroup>();
        listDaug.add(daug);

        List<DealAutomaticUserGroup> list1 = DealAutomaticAssignmentUtils.saveUsers(attribute, listDaug, new List<DealAutomaticUserGroup>());

        Integer listAft = [SELECT COUNT() FROM Deal_Automatic_Assignment_Data__c];

        System.assert(listAft > listBef);

        listBef = [SELECT COUNT() FROM Deal_Automatic_Assignment_Data__c];

        list1 = DealAutomaticAssignmentUtils.saveUsers(attribute, listDaug, new List<DealAutomaticUserGroup>());

        listAft = [SELECT COUNT() FROM Deal_Automatic_Assignment_Data__c];

        System.assert(list1.size() > 0);
        System.assertNotEquals(null, list1);
        System.assert(listAft == listBef);
    }

    @isTest static void gets_users_groups_by_category() {

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'CreditQualified';
        attribute.Display_Name__c = 'Credit Qualified';
        attribute.Key_Name__c = 'Credit Qualified';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 2;
        attribute.Display_Order__c = 1;
        insert attribute;

        Deal_Automatic_Assignment_Data__c data = new Deal_Automatic_Assignment_Data__c();
        data.Attribute_Id__c = 2;
        data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                 '"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"},{"userName":"Joe Valeo","userId":"005U0000005k9wOIAQ",' +
                                 '"selected":true,"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"}]';
        insert data;

        List<DealAutomaticUserGroup> list1 = DealAutomaticAssignmentUtils.getUsersGroupsByCategory(attribute);

        System.assert(list1.size() > 0);
        System.assertNotEquals(null, list1);
    }

    @isTest static void gets_specific_users_groups_by_category() {

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'CreditQualified';
        attribute.Display_Name__c = 'Credit Qualified';
        attribute.Key_Name__c = 'Credit Qualified';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 2;
        attribute.Display_Order__c = 1;
        insert attribute;

        Deal_Automatic_Assignment_Data__c data = new Deal_Automatic_Assignment_Data__c();
        data.Attribute_Id__c = 2;
        data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                 '"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"},{"userName":"Joe Valeo","userId":"005U0000005k9wOIAQ",' +
                                 '"selected":true,"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"}]';
        insert data;

        List<Deal_Automatic_Assignment_Data__c> lData = new List<Deal_Automatic_Assignment_Data__c>();
        lData.add(data);

        List<DealAutomaticUserGroup> list1 = DealAutomaticAssignmentUtils.getSpecificUsersGroupsByCategory(lData, attribute);

        System.assert(list1.size() > 0);
        System.assertNotEquals(null, list1);
    }

    @isTest static void gets_users_by_group_id_null() {

        List<DealAutomaticUserGroup> result = DealAutomaticAssignmentUtils.getUsersByGroupId('');
        System.assert(result.size() == 0);
    }

    @isTest static void gets_users_by_group_id() {

        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

        Group newGroup = null;

        System.runAs(thisUser) {

            newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            // Create Company Queue
            c2g.CODACompanyWebService.createQueue(testCompany.Id, 'USD', testCompany.Name);
            // Activate the Company
            c2g.CODAYearWebService.calculatePeriods(null);
            c2g.CODACompanyWebService.activateCompany(testCompany.Id, 'USD', testCompany.Name);
            // Assign the User to the Company
            c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
            userCompany.c2g__Company__c = testCompany.Id;
            userCompany.c2g__User__c = UserInfo.getUserId();
            insert userCompany;

            GroupMember newGroupMember = new GroupMember();
            newGroupMember.GroupId = newGroup.Id;
            newGroupMember.UserOrGroupId = thisUser.Id;
            insert newGroupMember;
        }

        List<DealAutomaticUserGroup> result = DealAutomaticAssignmentUtils.getUsersByGroupId(newGroup.Id);
        System.assert(result.size() > 0, 'Get Users');
    }

    @isTest static void gets_users_by_usage() {
        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'CreditQualified';
        attribute.Display_Name__c = 'Credit Qualified';
        attribute.Key_Name__c = 'CreditQualified';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 2;
        attribute.Display_Order__c = 1;
        insert attribute;

        Deal_Automatic_Assignment_Data__c data = new Deal_Automatic_Assignment_Data__c();
        data.Attribute_Id__c = 2;
        data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                 '"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"},{"userName":"Joe Valeo","userId":"005U0000005k9wOIAQ",' +
                                 '"selected":true,"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"}]';
        insert data;

        List<Deal_Automatic_Attributes__c> listOfAttributes = new List<Deal_Automatic_Attributes__c>();
        listOfAttributes.add(attribute);

        List<Deal_Automatic_Assignment_Data__c> listOfData = new List<Deal_Automatic_Assignment_Data__c>();
        listOfData.add(data);

        DealAutomaticUserGroup group1 = new DealAutomaticUserGroup('005U0000004YXGiIAO', 'Kimberly Gardner', '00GU0000002ccJvMAI', 'Kennesaw Sales Center', true, false);
        List<DealAutomaticUserGroup> listOfGroups = new List<DealAutomaticUserGroup>();
        listOfGroups.add(group1);

        List<DealAutomaticUserGroup> result = DealAutomaticAssignmentUtils.getUsersByUsage('00GU0000002ccJvMAI', listOfAttributes, 'Category', 'CreditQualified', listOfData, listOfGroups);

        System.assert(result.size() > 0, 'Get Users By Usage');
    }

    @isTest static void re_assign_deals() {

        User hal = [SELECT Id, Name, IsActive FROM User WHERE Name like '%HAL%' and IsActive = True LIMIT 1];
 
        String appId = '';

        System.runAs(hal) {
            Opportunity app = LibraryTest.createApplicationTH();
            app.Automatically_Assigned__c = true;
            app.Automatically_Assigned_Date__c = Date.today().addDays(-5);
            app.Status__c = 'Credit Qualified';
            update app;
            appId = app.Id;
        }

        String appOwner = [SELECT OwnerId FROM Opportunity WHERE Id = : appId LIMIT 1].OwnerId;

        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

        Date newDate = Date.today().addDays(-8);
        Datetime fromDate = Datetime.newInstance(newDate.year(), newDate.month(), newDate.day());
        DateTime toDate = DateTime.now();

        DealAutomaticAssignmentUtils.reAssignDeals(hal.Id, currentUser.Id, fromDate, toDate);

        System.assertNotEquals(appOwner, [SELECT OwnerId FROM Opportunity WHERE Id = : appId LIMIT 1].OwnerId);
    }

    @isTest static void save_and_get_summary_list_by_user_id() {

        DealAutomaticSummaryAttribute att = new DealAutomaticSummaryAttribute(2, 'CreditQualified', true);
        List<DealAutomaticSummaryAttribute> listOfAtt = new List<DealAutomaticSummaryAttribute>();
        listOfAtt.add(att);

        DealAutomaticSummaryList sum = new DealAutomaticSummaryList('Category', listOfAtt);
        List<DealAutomaticSummaryList> listOfSum = new List<DealAutomaticSummaryList>();
        listOfSum.add(sum);

        DealAutomaticAssignmentUtils.saveSummaryListByUserId(listOfSum, '005U0000004YXGiIAO');

        List<DealAutomaticSummaryList> result = DealAutomaticAssignmentUtils.getSummaryListByUserId('005U0000004YXGiIAO');

        System.assertEquals(0, result.size());

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'CreditQualified';
        attribute.Display_Name__c = 'Credit Qualified';
        attribute.Key_Name__c = 'CreditQualified';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 2;
        attribute.Display_Order__c = 1;
        insert attribute;

        List<Deal_Automatic_Assignment_Data__c> listData = [SELECT Id, Attribute_Id__c, Assigned_Users__c FROM Deal_Automatic_Assignment_Data__c WHERE Attribute_Id__c = 2 LIMIT 1];

        Deal_Automatic_Assignment_Data__c data = new Deal_Automatic_Assignment_Data__c();

        if (listData.size() > 0) {
            data = listData.get(0);
            data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                     '"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"},{"userName":"Joe Valeo","userId":"005U0000005k9wOIAQ",' +
                                     '"selected":true,"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"}]';
            update data;
        } else {

            data.Attribute_Id__c = 2;
            data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                     '"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"},{"userName":"Joe Valeo","userId":"005U0000005k9wOIAQ",' +
                                     '"selected":true,"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"}]';
            insert data;
        }


        DealAutomaticAssignmentUtils.saveSummaryListByUserId(listOfSum, '005U0000004YXGiIAO');

        result = DealAutomaticAssignmentUtils.getSummaryListByUserId('005U0000004YXGiIAO');

        System.assert(result.size() > 0);

    }

    /*@isTest static void status_exception_assignment() {

        Opportunity app = LibraryTest.createApplicationLeadTH();

        app.Contact__r = [SELECT id, name FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        update app;

        Deal_Automatic_Status_Exception__c statException = new Deal_Automatic_Status_Exception__c();
        statException.Object__c = 'Lead';
        statException.Name = 'Test';
        statException.Field__c = 'Test';
        statException.Trigger_Value__c = 'Test';
        insert statException;

        List<Deal_Automatic_Status_Exception__c> statusExceptionList = new List<Deal_Automatic_Status_Exception__c>();
        statusExceptionList.add(statException);

        Boolean result = DealAutomaticAssignmentUtils.statusExceptionCanBeAssignedForApp(app, statusExceptionList);

        System.assertEquals(false, result);

        result = DealAutomaticAssignmentUtils.statusExceptionCanBeAssignedForLead(app.Lead__r, statusExceptionList);

        statException.Object__c = 'Contact';
        update statException;

        result = DealAutomaticAssignmentUtils.statusExceptionCanBeAssignedForApp(app, statusExceptionList);

        System.assertEquals(false, result);

        statException.Object__c = 'Application';
        update statException;

        result = DealAutomaticAssignmentUtils.statusExceptionCanBeAssignedForApp(app, statusExceptionList);

        System.assertEquals(false, result);
    }

    @isTest static void is_status_exception_app() {
        Opportunity app = LibraryTest.createApplicationLeadTH();

        app.Lead_Sub_Source__c = 'LendingTree';
        update app;

        Deal_Automatic_Status_Exception__c statException = new Deal_Automatic_Status_Exception__c();
        statException.Object__c = 'Lead';
        statException.Name = 'Test';
        statException.Field__c = 'Test';
        statException.Trigger_Value__c = 'Test';
        statException.Sub_Source__c = 'Different';
        insert statException;

        List<Deal_Automatic_Status_Exception__c> statExceptionList = new List<Deal_Automatic_Status_Exception__c>();
        statExceptionList.add(statException);

        List<Deal_Automatic_Status_Exception__c> result = DealAutomaticAssignmentUtils.isStatusExceptionApplication(app, statExceptionList);

        System.assertEquals(new List<Deal_Automatic_Status_Exception__c>(), result);

        statException.Sub_Source__c = 'LendingTree';
        update statException;

        statExceptionList = new List<Deal_Automatic_Status_Exception__c>();
        statExceptionList.add(statException);

        result = DealAutomaticAssignmentUtils.isStatusExceptionApplication(app, statExceptionList);

        System.assertNotEquals(null, result);
    }*/

    @isTest static void is_status_exception_lead() {
        Lead l = LibraryTest.createLeadTH();

        l.Lead_Sub_Source__c = 'LendingTree';
        update l;

        Deal_Automatic_Status_Exception__c statException = new Deal_Automatic_Status_Exception__c();
        statException.Object__c = 'Lead';
        statException.Name = 'Test';
        statException.Field__c = 'Test';
        statException.Trigger_Value__c = 'Test';
        statException.Sub_Source__c = 'Different';
        insert statException;

        List<Deal_Automatic_Status_Exception__c> statExceptionList = new List<Deal_Automatic_Status_Exception__c>();
        statExceptionList.add(statException);

        List<Deal_Automatic_Status_Exception__c> result = DealAutomaticAssignmentUtils.isStatusExceptionLead(l, statExceptionList);

        System.assertEquals(new List<Deal_Automatic_Status_Exception__c>(), result);

        statException.Sub_Source__c = 'LendingTree';
        update statException;

        statExceptionList = new List<Deal_Automatic_Status_Exception__c>();
        statExceptionList.add(statException);

        result = DealAutomaticAssignmentUtils.isStatusExceptionLead(l, statExceptionList);

        System.assertNotEquals(null, result);
    }

    @isTest static void updates_app() {
        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Credit Qualified';
        update app;

        DealAutomaticAssignmentUtils.batchUpdateApps(1);

        //app = [SELECT id, genesis__ID_State__c FROM Opportunity WHERE id = :app.id LIMIT 1];

        //System.assertEquals('Test', app.genesis__ID_State__c);
    }

    @isTest static void updates_lead() {

        Lead l = new Lead();

        l.Firstname = 'BERNIER';
        l.Lastname = 'BERMARD';
        l.Email = 'test@mail.com';
        l.Date_of_Birth__c = Date.parse('08/03/1966');
        l.Point_Code__c = '1234AB';
        l.SSN__c = '666134826';
        l.Time_at_current_Address__c = Date.parse('12/04/2011');
        l.Phone = '7654835198';
        l.Street = '912 PINELAND AVE APT 33';
        l.City = 'HINESVILLE';
        l.State = 'GA';
        l.PostalCode = '31313';
        l.Country = 'US';
        l.Annual_Income__c = 80000;
        l.Loan_Amount__c = 5000;
        l.Employment_Start_date__c = Date.parse('02/03/2014');
        l.Company = 'Test Company';
        l.LeadSource = 'Online Aggregator';
        l.Lead_Sub_Source__c = 'LendingTree';
        l.Use_of_Funds__c = 'Debt Consolidation';
        l.IsConverted = false;
        l.Automatically_Assigned__c = false;
        l.OwnerId = '005U0000003pdKWIAY';
        l.Status = 'Test';
        insert l;

        DealAutomaticAssignmentUtils.batchLeads(1);

        l = [SELECT sanctionrisk__c FROM Lead WHERE id = : l.id LIMIT 1];

        System.assertEquals('Test', l.sanctionrisk__c);
    }

    @isTest static void gets_total_assigned_apps() {

        List<DealAutomaticUserGroup> userGroupList = DealAutomaticAssignmentUtils.getAllUsersGroupsFromCenters();

        Map<Id, Integer> notExpected = new Map<Id, Integer>();

        Map<Id, Integer> result = DealAutomaticAssignmentUtils.getTotalAssignedAppsForUserGroupList(userGroupList);

        //System.assertNotEquals(notExpected, result);

        result = DealAutomaticAssignmentUtils.getTotalAssignedLeadsForUserGroupList(userGroupList);

        //System.assertNotEquals(notExpected, result);
    }

    @isTest static void gets_attribute_by_type_name() {

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'CreditQualified';
        attribute.Display_Name__c = 'Credit Qualified';
        attribute.Key_Name__c = 'CreditQualified';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 2;
        attribute.Display_Order__c = 1;
        insert attribute;

        List<Deal_Automatic_Attributes__c> attributeList = new List<Deal_Automatic_Attributes__c>();
        attributeList.add(attribute);

        Deal_Automatic_Attributes__c result = DealAutomaticAssignmentUtils.getAttributeByTypeAndName(attributeList, 'Category', 'CreditQualified');

        System.assertNotEquals(null, result);
    }

    @isTest static void gets_all_deal_automatic_data() {
        List<Deal_Automatic_Assignment_Data__c> result = DealAutomaticAssignmentUtils.getAllDealAutomaticData();
        System.assertEquals(0, result.size());
    }

    @isTest static Void is_delay_exception_app() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Lead_Sub_Source__c = 'LendingTree';
        app.LeadSource__c = 'Online Aggregator';
        update app;

        Contact c = [SELECT Id, Name, MailingState FROM Contact WHERE Id = : app.Contact__c LIMIT 1];
        c.MailingState = 'GA';
        update c;

        app = [SELECT Id,  Lead_Sub_Source__c, LeadSource__c, Point_Code__c, State__c FROM Opportunity WHERE Id = : app.Id LIMIT 1];

        Deal_Automatic_Time_Delay_Exception__c delayExceptionData = new Deal_Automatic_Time_Delay_Exception__c();
        delayExceptionData.Is_Active__c = true;
        delayExceptionData.Sub_Source__c = 'LendingTree';
        delayExceptionData.Lead_Source__c = 'Online Aggregator';
        delayExceptionData.Point_Code__c = '';
        delayExceptionData.Name = 'Name';
        delayExceptionData.Delay__c = 0;
        delayExceptionData.FICO_Selected__c = '720';
        delayExceptionData.Amount_Selected__c = '5000';
        delayExceptionData.Selected_States__c = 'GA';
        insert delayExceptionData;

        List<Deal_Automatic_Time_Delay_Exception__c> delayExceptionList = new List<Deal_Automatic_Time_Delay_Exception__c>();
        delayExceptionList.add(delayExceptionData);

        Deal_Automatic_Time_Delay_Exception__c result = DealAutomaticAssignmentUtils.isDelayExceptionApplication(app, delayExceptionList, delayExceptionData.FICO_Selected__c, delayExceptionData.Amount_Selected__c);

        System.assertNotEquals(null, result);
    }

    @isTest static void is_delay_exception_lead() {

        Lead l = new Lead();

        l.Firstname = 'BERNIER';
        l.Lastname = 'BERMARD';
        l.Email = 'test@mail.com';
        l.Date_of_Birth__c = Date.parse('08/03/1966');
        l.Point_Code__c = '1234AB';
        l.SSN__c = '666134826';
        l.Time_at_current_Address__c = Date.parse('12/04/2011');
        l.Phone = '7654835198';
        l.Street = '912 PINELAND AVE APT 33';
        l.City = 'HINESVILLE';
        l.State = 'GA';
        l.PostalCode = '31313';
        l.Country = 'US';
        l.Annual_Income__c = 80000;
        l.Loan_Amount__c = 5000;
        l.Employment_Start_date__c = Date.parse('02/03/2014');
        l.Company = 'Test Company';
        l.LeadSource = 'Online Aggregator';
        l.Lead_Sub_Source__c = 'LendingTree';
        l.Use_of_Funds__c = 'Debt Consolidation';
        l.IsConverted = false;
        l.Automatically_Assigned__c = false;
        l.OwnerId = '005U0000003pdKWIAY';
        l.Status = 'Test';
        insert l;

        Deal_Automatic_Time_Delay_Exception__c delayExceptionData = new Deal_Automatic_Time_Delay_Exception__c();
        delayExceptionData.Sub_Source__c = 'LendingTree';
        delayExceptionData.Lead_Source__c = 'Online Aggregator';
        delayExceptionData.Point_Code__c = '';
        delayExceptionData.Name = 'Name';
        delayExceptionData.Delay__c = 0;
        delayExceptionData.Selected_States__c = 'GA';
        insert delayExceptionData;

        List<Deal_Automatic_Time_Delay_Exception__c> delayExceptionList = new List<Deal_Automatic_Time_Delay_Exception__c>();
        delayExceptionList.add(delayExceptionData);

        Deal_Automatic_Time_Delay_Exception__c result = DealAutomaticAssignmentUtils.isDelayExceptionLead(l, delayExceptionList);

        System.assertNotEquals(null, result);
    }

    @isTest static void delay_exception_can_be_assigned() {
        Deal_Automatic_Time_Delay_Exception__c delayExceptionData = new Deal_Automatic_Time_Delay_Exception__c();
        delayExceptionData.Is_Active__c = true;
        delayExceptionData.Sub_Source__c = 'LendingTree';
        delayExceptionData.Lead_Source__c = 'Online Aggregator';
        delayExceptionData.Point_Code__c = '';
        delayExceptionData.Name = 'Name';
        delayExceptionData.Delay__c = 0;
        delayExceptionData.FICO_Selected__c = '720';
        delayExceptionData.Amount_Selected__c = '5000';
        delayExceptionData.Selected_States__c = 'GA';
        insert delayExceptionData;

        BusinessHours bh = [SELECT Id, Name, IsActive, IsDefault, SundayStartTime, SundayEndTime, MondayStartTime, MondayEndTime,
                            TuesdayStartTime, TuesdayEndTime, WednesdayStartTime, WednesdayEndTime, ThursdayStartTime, ThursdayEndTime, FridayStartTime,
                            FridayEndTime, SaturdayStartTime, SaturdayEndTime, TimeZoneSidKey
                            FROM BusinessHours
                            WHERE Name = 'Deal Automatic Delay Exception Hours' LIMIT 1];

        Boolean result = DealAutomaticAssignmentUtils.delayExceptionCanBeAssigned(delayExceptionData, Datetime.now(), bh);

        //System.assertEquals(true, result);
    }
	
    @isTest static void sales_Assignment() {
        
        //Sales_Reps_Assignments__c lp = New Sales_Reps_Assignments__c();
        //lp.Sales_Assignments_Counter__c = null;
        //insert lp;
        
        //String groupId = 'test';
        
        Group gp = [Select Id, Name From Group where name = 'Sales Reps Assignments' limit 1];
        
        //DealAutomaticAssignmentManager.AssignmentParams params = new DealAutomaticAssignmentManager.AssignmentParams('OA_IP');
        /*params.salesCustom.Sales_Assignments_GroupId__c = '000test';
        params.salesCustom.Sales_Assignments_Counter__c = 0;
        
        Id test = UserInfo.getUserId();
        *///ids.add(test);
        //params.userIds = ids;
        //DealAutomaticAssignmentUtils.getNextUserIdInList(params.userIds, 1);
        
        List<String> idList = new list<String>();
        idList.add(UserInfo.getUserId());
        
        DealAutomaticAssignmentUtils.getUsersListByState(gp.Id); 
        DealAutomaticAssignmentUtils.getUsersListBySchedule(gp.Id);
        DealAutomaticAssignmentUtils.getNextUserIdInList(idList, 3);
    }
}