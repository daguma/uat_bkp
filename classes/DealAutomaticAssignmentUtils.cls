public class DealAutomaticAssignmentUtils {

    public static final String STRATEGY_TYPE_USAGE = 'Strategy_Type';
    public static final String SALES_QUEUE_USAGE = 'Sales_Category';
    public static final String CATEGORY_USAGE = 'Status';
    public static final String DEAL_SOURCE_USAGE = 'Deal_Source';
    public static final String EMPLOYMENT_TYPE_USAGE = 'Employment_Type';
    public static final String GRADE_USAGE = 'Grade';
    public static final String USE_OF_FUNDS_USAGE = 'Use_of_Funds';
    public static final String REQUESTED_AMOUNT_USAGE = 'Requested_Amount';
    public static final String SOFT_FICO_RANGE_USAGE = 'Soft_Fico_Range';

    public static final String EMPLOYMENT_CORRELATION = 'Employee,Field Supervisor,full time,Branch Manager,Full-time';
    public static final String EMPLOYMENT_SELF_EMPLOYMENT_CORRELATION = 'Self Employed';
    public static final String EMPLOYMENT_SOCIAL_SECURITY_CORRELATION = 'Social Security';
    public static final String EMPLOYMENT_DISABILITY_CORRELATION = 'Disability';
    public static final String EMPLOYMENT_RETIRED_CORRELATION = 'Retired,Retired / Disabled';
    public static final String EMPLOYMENT_OTHER_CORRELATION = 'Other,Unemployed';
    public static final String EMPLOYMENT_MILITARY_CORRELATION = 'Military';

    public static final String[] LOCATION_GROUP_OPTIONS = new String[] {'Kennesaw Sales Center', 'DR Sales Center'};

    public static List<DealAutomaticUserGroup> getAllUsersGroupsFromCenters() {
        String userType = Schema.SObjectType.User.getKeyPrefix();

        String[] groupIds = new String[] { Label.Kennesaw_Group_Id, Label.DR_Group_Id };

        List<GroupMember> salesReps = [Select Id, UserOrGroupId, GroupId From GroupMember Where GroupId IN :groupIds];

        List<DealAutomaticUserGroup> userWithGroups = new List<DealAutomaticUserGroup>();

        Set<Id> userOrGroupIds = new Set<Id>();
        for (GroupMember member : salesReps) {
            userOrGroupIds.add(member.UserOrGroupId);
        }

        Map<Id, User> userMap = new Map<Id, User>(
            [SELECT Id, Name
             FROM User
             WHERE Id In :userOrGroupIds
             AND IsActive = true
                            AND isOnline__c = true]);

        String groupName = '';

        for (GroupMember member : salesReps) {
            if (userMap.containsKey(member.UserOrGroupId)) {

                if (member.GroupId.equals(groupIds[0]))
                    groupName = LOCATION_GROUP_OPTIONS[0];
                else if (member.GroupId.equals(groupIds[1]))
                    groupName = LOCATION_GROUP_OPTIONS[1];
                else
                    groupName = LOCATION_GROUP_OPTIONS[0];

                userWithGroups.add(new DealAutomaticUserGroup(userMap.get(member.UserOrGroupId).Id,
                                   userMap.get(member.UserOrGroupId).Name,
                                   member.GroupId,
                                   groupName,
                                   false,
                                   true));
            }
        }
        return userWithGroups;
    }

    /**
     * This method force to get all the users from the salescenter no matter online or offline.
     * @return [description]
     */
    public static List<DealAutomaticUserGroup> getAllUsersGroupsForSummaryPage() {
        String userType = Schema.SObjectType.User.getKeyPrefix();

        String[] groupIds = new String[] { Label.Kennesaw_Group_Id, Label.DR_Group_Id };

        List<GroupMember> salesReps = [Select Id, UserOrGroupId, GroupId From GroupMember Where GroupId IN :groupIds];

        List<DealAutomaticUserGroup> userWithGroups = new List<DealAutomaticUserGroup>();

        Set<Id> userOrGroupIds = new Set<Id>();
        for (GroupMember member : salesReps) {
            userOrGroupIds.add(member.UserOrGroupId);
        }

        Map<Id, User> userMap = new Map<Id, User>(
            [SELECT Id, Name, isOnline__c
             FROM User
             WHERE Id In :userOrGroupIds
             AND IsActive = true]);

        String groupName = '';

        for (GroupMember member : salesReps) {
            if (userMap.containsKey(member.UserOrGroupId)) {

                if (member.GroupId.equals(groupIds[0]))
                    groupName = LOCATION_GROUP_OPTIONS[0];
                else if (member.GroupId.equals(groupIds[1]))
                    groupName = LOCATION_GROUP_OPTIONS[1];
                else
                    groupName = LOCATION_GROUP_OPTIONS[0];

                userWithGroups.add(new DealAutomaticUserGroup(userMap.get(member.UserOrGroupId).Id,
                                   userMap.get(member.UserOrGroupId).Name,
                                   member.GroupId,
                                   groupName,
                                   false,
                                   userMap.get(member.UserOrGroupId).isOnline__c));
            }
        }
        return userWithGroups;
    }

    /**
     * Saves the user groups into the Deal_Automatic_Assignment_Data__c object
     * @param  attribute   where need to insert the user groups
     * @param  usersGroups to insert
     * @return a list with the inserted user groups
     */
    public static List<DealAutomaticUserGroup> saveUsers(Deal_Automatic_Attributes__c attribute,
            List<DealAutomaticUserGroup> newUsersGroups,
            List<DealAutomaticUserGroup> oldUsersGroups) {

        List<DealAutomaticUserGroup> result = null;
        String newValueString = JSON.serialize(newUsersGroups);
        String oldValueString = JSON.serialize(oldUsersGroups);

        Deal_Automatic_Assignment_Data__c data = getDealAutomaticAssignmentDataById(attribute.Id__c);

        if (data == null)
            data = new Deal_Automatic_Assignment_Data__c();

        data.Attribute_Id__c = attribute.Id__c;
        data.Assigned_Users__c = newValueString;
        upsert data;

        if (!newValueString.equalsIgnoreCase(oldValueString)) {
            Deal_Automatic_Assignment_Data_History__c history = new Deal_Automatic_Assignment_Data_History__c();
            history.Attribute_Id__c = attribute.Id__c;
            history.Display_Name__c = attribute.Display_Name__c;
            history.New_Value__c = newValueString;
            history.Old_Value__c = oldValueString;
            insert history;
        }

        return result = (List<DealAutomaticUserGroup>)JSON.deserializeStrict(data.Assigned_Users__c, List<DealAutomaticUserGroup>.class);
    }

    /**
     * Get all the users by a specific category
     * @param  category
     * @return a list with all the user groups by a specific category
     */
    public static List<DealAutomaticUserGroup> getUsersGroupsByCategory(Deal_Automatic_Attributes__c category) {

        Deal_Automatic_Assignment_Data__c data = getDealAutomaticAssignmentDataById(category.Id__c);

        List<DealAutomaticUserGroup> savedUserGroupList = null;

        List<DealAutomaticUserGroup> allUserGroupList = getAllUsersGroupsFromCenters();

        if (data != null) {
            savedUserGroupList = (List<DealAutomaticUserGroup>)JSON.deserializeStrict(data.Assigned_Users__c, List<DealAutomaticUserGroup>.class);

            if (savedUserGroupList.size() > 0) {
                for (DealAutomaticUserGroup userGroup : allUserGroupList) {
                    userGroup.selected = AutomaticAssignmentUtils.userGroupContains(userGroup, savedUserGroupList) ? true : false;
                }
            }
        }

        return allUserGroupList;
    }

    /**
     * Get all the users by a specific category
     * @param  category
     * @return a list with all the user groups by a specific category
     */
    public static List<DealAutomaticUserGroup> getSpecificUsersGroupsByCategory(List<Deal_Automatic_Assignment_Data__c> assignmentData,
            Deal_Automatic_Attributes__c category) {

        List<DealAutomaticUserGroup> savedUserGroupList = new List<DealAutomaticUserGroup>();

        if (category != null) {

            Deal_Automatic_Assignment_Data__c data = getDealAutomaticAssignmentDataById(assignmentData, category.Id__c);

            if (data != null) {
                savedUserGroupList = (List<DealAutomaticUserGroup>)JSON.deserializeStrict(data.Assigned_Users__c, List<DealAutomaticUserGroup>.class);
            }
        }

        return savedUserGroupList;
    }

    public static List<DealAutomaticUserGroup> getUsersByGroupId(String groupId) {
        String userType = Schema.SObjectType.User.getKeyPrefix();

        String[] groupIds = new String[] { Label.Kennesaw_Group_Id, Label.DR_Group_Id };

        List<GroupMember> salesReps = [Select Id, UserOrGroupId, GroupId From GroupMember Where GroupId = :groupId];

        List<DealAutomaticUserGroup> userWithGroups = new List<DealAutomaticUserGroup>();

        Set<Id> userOrGroupIds = new Set<Id>();
        for (GroupMember member : salesReps) {
            userOrGroupIds.add(member.UserOrGroupId);
        }

        Map<Id, User> userMap = new Map<Id, User>(
            [Select Id, Name
             From User
             Where Id In :userOrGroupIds
             AND IsActive = true
                            AND isOnline__c = true]);

        String groupName = '';

        for (GroupMember member : salesReps) {
            if (userMap.containsKey(member.UserOrGroupId)) {

                if (member.GroupId.equals(groupIds[0]))
                    groupName = LOCATION_GROUP_OPTIONS[0];
                else if (member.GroupId.equals(groupIds[1]))
                    groupName = LOCATION_GROUP_OPTIONS[1];
                else
                    groupName = LOCATION_GROUP_OPTIONS[0];

                userWithGroups.add(new DealAutomaticUserGroup(userMap.get(member.UserOrGroupId).Id,
                                   userMap.get(member.UserOrGroupId).Name,
                                   member.GroupId,
                                   groupName,
                                   false,
                                   true));
            }
        }
        return userWithGroups;
    }

    public static List<DealAutomaticUserGroup> getUsersByUsage(String groupId,
            List<Deal_Automatic_Attributes__c> attributes,
            String usage,
            String status,
            List<Deal_Automatic_Assignment_Data__c> assignmentData,
            List<DealAutomaticUserGroup> dealAutomaticUserGroup) {

        List<DealAutomaticUserGroup> result = new List<DealAutomaticUserGroup>();

        Deal_Automatic_Attributes__c category = getAttributeByTypeAndName(attributes, usage, status);

        List<DealAutomaticUserGroup> users = getSpecificUsersGroupsByCategory(assignmentData, category);

        for (DealAutomaticUserGroup user : users) {
            if (user.groupId.contains(groupId)) {
                result.add(user);
            }
        }

        return result = AutomaticAssignmentUtils.combineList(dealAutomaticUserGroup, result);
    }

    /**
     * [getTotalAssignedAppsForUserGroupList description]
     * @param  userGroupList [description]
     * @return               [description]
     */
    public static Map<Id, Integer> getTotalAssignedAppsForUserGroupList(List<DealAutomaticUserGroup> userGroupList) {

        Map<Id, Integer> result = new Map<Id, Integer>();
        set<id> userIdSet = new set<id>();
        for (DealAutomaticUserGroup currentUser : userGroupList) {
            userIdSet.add(currentUser.userId);
            result.put(currentUser.userId, 0);
        }

        for (AggregateResult ar : [SELECT ownerId id, COUNT(id) total
                                   FROM Opportunity
                                   WHERE ownerId IN:userIdSet
                                   AND Status__c
                                   IN ('Credit Qualified', 'Offer Accepted', 'Documents Requested') GROUP BY ownerId]) {

            if (string.isNotEmpty((string)ar.get('id'))) {
                Integer tot = (integer)ar.get('total');
                result.put((id)ar.get('id'), tot == null ? 0 : tot);
            }
        }
        return result;
    }

    /**
     * [getTotalAssignedLeadsForUserGroupList description]
     * @param  userGroupList [description]
     * @return               [description]
     */
    public static Map<Id, Integer> getTotalAssignedLeadsForUserGroupList(List<DealAutomaticUserGroup> userGroupList) {

        Map<Id, Integer> result = new Map<Id, Integer>();
        //===API-161===start==
        set<id> userIdSet = new set<id>();
        for (DealAutomaticUserGroup currentUser : userGroupList) {
            userIdSet.add(currentUser.userId);
        }
        for (AggregateResult ar : [SELECT ownerId id, COUNT(id) total
                                   FROM Lead
                                   WHERE ownerId IN:userIdSet
                                   AND createdDate >= TODAY
                                   AND Status
                                   NOT IN ('Mailed',
                                           'Force',
                                           'Application Submitted') GROUP BY ownerId]) {

            if (string.isNotEmpty((string)ar.get('id'))) {
                Integer tot = (integer)ar.get('total');
                result.put((id)ar.get('id'), tot == null ? 0 : tot);
            }
        }
        //===API-161===End==
        return result;
    }

    /**
     * [getDealAttributeFromListByTypeAndName description]
     * @param  attributes [description]
     * @param  type       [description]
     * @param  name       [description]
     * @return            [description]
     */
    public static Deal_Automatic_Attributes__c getAttributeByTypeAndName(List<Deal_Automatic_Attributes__c> attributes,
            String type,
            String name) {

        Deal_Automatic_Attributes__c result = null;

        if (!String.isEmpty(type) && !String.isEmpty(name)) {
            for (Deal_Automatic_Attributes__c attribute : attributes) {
                if (attribute.Usage__c.equalsIgnoreCase(type) &&
                        attribute.Key_Name__c.equalsIgnoreCase(name)) {
                    result = attribute;
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Test Update Massive assigned apps
     */
    public static void batchUpdateApps(Integer size) {
        for (Opportunity app : [SELECT Id, OwnerId, Automatically_Assigned__c
                                FROM Opportunity
                                WHERE  Status__c
                                NOT IN ('Credit Declined (or Unqualified)',
                                        'Funded',
                                        'NEW - ENTERED',
                                        'Declined – Do to Fraud',
                                        'KYC Fail',
                                        'Documents Rejected')  LIMIT : size]) {
            app.OwnerId = Label.Default_Lead_Owner_ID;
            app.Automatically_Assigned__c = false;
            // app.genesis__ID_State__c = 'Test'; // Removed as not required

            update app;
        }
    }

    public static void batchLeads(Integer size) {
        List<Lead> leadList = new List<Lead>();

        for (Lead l : [SELECT Automatically_Assigned__c, sanctionrisk__c
                       FROM Lead
                       WHERE OwnerId = '005U0000003pdKWIAY'
                                       AND Lead.Automatically_Assigned__c = false
                                               AND Status NOT IN ( 'Mailed', 'Force', 'Application Submitted')
                                               LIMIT : size]) {
            l.sanctionrisk__c = 'Test';
            leadList.add(l);
        }

        update leadList;
    }

    /**
     * [isStatusExceptionApplication description]
     * @param  app [description]
     * @return     [description]
     */
    public static List<Deal_Automatic_Status_Exception__c> isStatusExceptionApplication(Opportunity app, List<Deal_Automatic_Status_Exception__c> statusExceptionList) {

        List<Deal_Automatic_Status_Exception__c> result = new List<Deal_Automatic_Status_Exception__c>();

        String leadSubSource = app.Lead_Sub_Source__c != null ? app.Lead_Sub_Source__c : '';

        for (Deal_Automatic_Status_Exception__c statusExceptionData : statusExceptionList) {
            if (leadSubSource.equalsIgnoreCase(statusExceptionData.Sub_Source__c)) {
                result.add(statusExceptionData);
            }
        }

        return result;
    }

    /**
     * [isStatusExceptionLead description]
     * @param  lead [description]
     * @return      [description]
     */
    public static List<Deal_Automatic_Status_Exception__c> isStatusExceptionLead(Lead lead, List<Deal_Automatic_Status_Exception__c> statusExceptionList) {

        List<Deal_Automatic_Status_Exception__c> result = new List<Deal_Automatic_Status_Exception__c>();

        String leadSubSource = lead.Lead_Sub_Source__c != null ? lead.Lead_Sub_Source__c : '';

        for (Deal_Automatic_Status_Exception__c statusExceptionData : statusExceptionList) {
            if (leadSubSource.equalsIgnoreCase(statusExceptionData.Sub_Source__c)) {
                result.add(statusExceptionData);
            }
        }

        return result;
    }

    /**
     * [delayExceptionCanBeAssigned description]
     * @param  createdDate [description]
     * @param  hours       [description]
     * @return             [description]
     */
    public static Boolean statusExceptionCanBeAssignedForApp(Opportunity app, List<Deal_Automatic_Status_Exception__c> statusExceptionDataList) {

        Boolean result = false;
        String value = '';

        for (Deal_Automatic_Status_Exception__c statusExceptionData : statusExceptionDataList) {
            if (statusExceptionData != null) {
                if (statusExceptionData.Object__c.equalsIgnoreCase('Lead')) {
                    Lead lead = (Lead)app.getSObject('Lead__r');

                    if (lead != null) {
                        Set<String> objectFields = Schema.SObjectType.Lead.fields.getMap().keySet();
                        value = objectFields.contains(statusExceptionData.Field__c.toLowerCase()) ? String.valueOf(lead.get(statusExceptionData.Field__c)) : '';
                    }
                } else if (statusExceptionData.Object__c.equalsIgnoreCase('Contact')) {
                    Contact contact = (Contact)app.getSObject('Contact__r');

                    if (contact != null) {
                        Set<String> objectFields = Schema.SObjectType.Contact.fields.getMap().keySet();
                        value = objectFields.contains(statusExceptionData.Field__c.toLowerCase()) ? String.valueOf(contact.get(statusExceptionData.Field__c)) : '';
                    }
                } else if (statusExceptionData.Object__c.equalsIgnoreCase('Application')) {
                    Set<String> objectFields = Schema.SObjectType.Opportunity.fields.getMap().keySet();
                    value = objectFields.contains(statusExceptionData.Field__c.toLowerCase()) ? String.valueOf(app.get(statusExceptionData.Field__c)) : '';
                }

                result = !String.isEmpty(value) ? value.equalsIgnoreCase(statusExceptionData.Trigger_Value__c) : false;
                if (result) break;
            }
        }

        return result;
    }

    /**
     * [delayExceptionCanBeAssigned description]
     * @param  createdDate [description]
     * @param  hours       [description]
     * @return             [description]
     */
    public static Boolean statusExceptionCanBeAssignedForLead(Lead lead, List<Deal_Automatic_Status_Exception__c> statusExceptionDataList) {

        Boolean result = false;
        String value = '';

        for (Deal_Automatic_Status_Exception__c statusExceptionData : statusExceptionDataList) {
            if (statusExceptionData != null) {
                if (statusExceptionData.Object__c.equalsIgnoreCase('Lead')) {
                    Set<String> objectFields = Schema.SObjectType.Lead.fields.getMap().keySet();
                    value = objectFields.contains(statusExceptionData.Field__c) ? String.valueOf(lead.get(statusExceptionData.Field__c)) : '';
                    result = !String.isEmpty(value) ? value.equalsIgnoreCase(statusExceptionData.Trigger_Value__c) : false;
                    if (result) break;
                }
            }
        }

        return result;
    }

    /**
     * [isDelayExceptionApplication description]
     * @param  app [description]
     * @return     [description]
     */
    public static  Deal_Automatic_Time_Delay_Exception__c isDelayExceptionApplication(Opportunity app, List<Deal_Automatic_Time_Delay_Exception__c> delayExceptionList, String fico, String amount) {

        Deal_Automatic_Time_Delay_Exception__c result = null;
        String leadSubSource = app.Lead_Sub_Source__c != null ? app.Lead_Sub_Source__c : '';
        String leadSource = app.LeadSource__c != null ? app.LeadSource__c : '';
        String pointCode = app.Point_Code__c != null ? app.Point_Code__c : '';
        String state = app.State__c != null ? app.State__c.toUpperCase() : '';

        for (Deal_Automatic_Time_Delay_Exception__c delayExceptionData : delayExceptionList) {
            String delaySubSource = delayExceptionData.Sub_Source__c != null ? delayExceptionData.Sub_Source__c : '';
            String delayLeadSource = delayExceptionData.Lead_Source__c != null ? delayExceptionData.Lead_Source__c : '';
            String delayPointCode = delayExceptionData.Point_Code__c != null ? delayExceptionData.Point_Code__c : '';

            if ((delayExceptionData.Is_Active__c && leadSubSource.equalsIgnoreCase(delaySubSource) && leadSource.equalsIgnoreCase(delayLeadSource) && (String.isEmpty(delayPointCode) || pointCode.startsWithIgnoreCase(delayPointCode))) &&
                    (!String.isEmpty(state) && !String.isEmpty(delayExceptionData.Selected_States__c) && delayExceptionData.Selected_States__c.toUpperCase().contains(state)) &&
                    ((!String.isEmpty(fico) && (String.isEmpty(delayExceptionData.FICO_Selected__c) || delayExceptionData.FICO_Selected__c.contains(fico))) ||
                     (!String.isEmpty(amount) && (String.isEmpty(delayExceptionData.Amount_Selected__c) || delayExceptionData.Amount_Selected__c.contains(amount))))) {
                result = delayExceptionData;
                break;
            }
        }

        return result;
    }

    /**
     * [isDelayExceptionLead description]
     * @param  lead [description]
     * @return      [description]
     */
    public static Deal_Automatic_Time_Delay_Exception__c isDelayExceptionLead(Lead lead, List<Deal_Automatic_Time_Delay_Exception__c> delayExceptionList) {

        Deal_Automatic_Time_Delay_Exception__c result = null;
        String leadSubSource = lead.Lead_Sub_Source__c != null ? lead.Lead_Sub_Source__c : '';
        String leadSource = lead.LeadSource != null ? lead.LeadSource : '';
        String pointCode = lead.Point_Code__c != null ? lead.Point_Code__c : '';
        String state = lead.State != null ? lead.State : '';

        for (Deal_Automatic_Time_Delay_Exception__c delayExceptionData : delayExceptionList) {
            String delaySubSource = delayExceptionData.Sub_Source__c != null ? delayExceptionData.Sub_Source__c : '';
            String delayLeadSource = delayExceptionData.Lead_Source__c != null ? delayExceptionData.Lead_Source__c : '';
            String delayPointCode = delayExceptionData.Point_Code__c != null ? delayExceptionData.Point_Code__c : '';

            if (leadSubSource.equalsIgnoreCase(delaySubSource) &&
                    leadSource.equalsIgnoreCase(delayLeadSource) &&
                    (String.isEmpty(delayPointCode) || pointCode.startsWithIgnoreCase(delayPointCode)) &&
                    !String.isEmpty(delayExceptionData.Selected_States__c) &&
                    !String.isEmpty(state) &&
                    delayExceptionData.Selected_States__c.toUpperCase().contains(state)) {
                result = delayExceptionData;
                break;
            }
        }

        return result;
    }

    /**
     * [delayExceptionCanBeAssigned description]
     * @param  createdDate [description]
     * @param  hours       [description]
     * @return             [description]
     */
    public static Boolean delayExceptionCanBeAssigned(Deal_Automatic_Time_Delay_Exception__c delayExceptionData, Datetime createdDate, BusinessHours hours) {

        Boolean result = false;

        if (delayExceptionData != null) {
            Integer delayHours = delayExceptionData.Delay__c.intValue();
            Datetime currentDate = Datetime.valueOf(Datetime.now().format('yyyy-MM-dd HH:mm:ss', 'America/New_York'));
            Datetime nextStartDate = BusinessHours.nextStartDate(hours.Id, createdDate);
            Datetime finalDate = nextStartDate.addHours(delayHours);
            finalDate = BusinessHours.nextStartDate(hours.Id, finalDate);

            result = currentDate >= finalDate;
        }

        return result;
    }

    /**
     * [getAllDealAutomaticAttributes description]
     * @return [description]
     */
    public static List<Deal_Automatic_Attributes__c> getAllDealAutomaticAttributes() {
        return [SELECT Id, Usage__c, Id__c, Key_Name__c, Display_Name__c, Name
                FROM Deal_Automatic_Attributes__c
                ORDER BY Display_Order__c, Display_Name__c];
    }

    /**
     * [getAllDealAutomaticData description]
     * @return [description]
     */
    public static List<Deal_Automatic_Assignment_Data__c> getAllDealAutomaticData() {
        return [SELECT Id, Assigned_Users__c, Attribute_Id__c
                FROM Deal_Automatic_Assignment_Data__c];
    }

    /**
     * [getDealAutomaticAssignmentDataMap description]
     * @return [description]
     */
    public static Map<Decimal, Deal_Automatic_Assignment_Data__c> getDealAutomaticAssignmentDataMap() {
        Map<Decimal, Deal_Automatic_Assignment_Data__c> result = new Map<Decimal, Deal_Automatic_Assignment_Data__c>();

        for (Deal_Automatic_Assignment_Data__c data : [SELECT Id, Assigned_Users__c, Attribute_Id__c
                FROM Deal_Automatic_Assignment_Data__c]) {

            result.put(data.Attribute_Id__c, data);
        }

        return result;
    }

    /**
     * [getDealAutomaticAssignmentDataById description]
     * @param  id [description]
     * @return    [description]
     */
    public static Deal_Automatic_Assignment_Data__c getDealAutomaticAssignmentDataById(Decimal id) {

        Deal_Automatic_Assignment_Data__c result = null;

        for (Deal_Automatic_Assignment_Data__c data : [SELECT Id, Assigned_Users__c, Attribute_Id__c
                FROM Deal_Automatic_Assignment_Data__c
                WHERE Attribute_Id__c = : id
                                        LIMIT 1]) {

            result = data;
        }

        return result;
    }

    /**
     * [getDealAutomaticAssignmentDataById description]
     * @param  id [description]
     * @return    [description]
     */
    public static Deal_Automatic_Assignment_Data__c getDealAutomaticAssignmentDataById(List<Deal_Automatic_Assignment_Data__c> assignmentData,
            Decimal id) {

        Deal_Automatic_Assignment_Data__c result = null;

        for (Deal_Automatic_Assignment_Data__c data : assignmentData) {
            if (data.Attribute_Id__c == id) {
                result = data;
                break;
            }
        }

        return result;
    }

    /**
     * [getSummaryListByUserId description]
     * @param  userId [description]
     * @return        [description]
     */
    public static List<DealAutomaticSummaryList> getSummaryListByUserId(String userId) {

        List<DealAutomaticSummaryList> result = new List<DealAutomaticSummaryList>();
        DealAutomaticSummaryList summaryItem = new DealAutomaticSummaryList();
        Map<Decimal, Deal_Automatic_Assignment_Data__c> dataMap = getDealAutomaticAssignmentDataMap();
        String currentUsage = '';

        for (Deal_Automatic_Attributes__c attribute : getAllDealAutomaticAttributes()) {

            if (!currentUsage.equalsIgnoreCase(attribute.Usage__c)) {

                if (summaryItem.attributes != null) {
                    summaryItem.allSelected = summaryItem.areAllAttributesSelected(summaryItem.attributes);
                    result.add(summaryItem);
                }

                currentUsage = attribute.Usage__c;
                summaryItem = new DealAutomaticSummaryList(currentUsage, new List<DealAutomaticSummaryAttribute>());
            }

            Deal_Automatic_Assignment_Data__c data = dataMap.containsKey(attribute.Id__c) ? dataMap.get(attribute.Id__c) : null;
            Boolean isActive = false;

            if (data != null) {
                isActive = data.Assigned_Users__c.containsIgnoreCase(userId);
            }

            summaryItem.attributes.add(new DealAutomaticSummaryAttribute(attribute.Id__c, attribute.Display_Name__c, isActive));
        }

        if (summaryItem.attributes != null) {
            summaryItem.allSelected = summaryItem.areAllAttributesSelected(summaryItem.attributes);
            result.add(summaryItem);
        }

        return result;
    }

    /**
     * [saveSummaryListByUserId description]
     * @param summaryList [description]
     * @param userId      [description]
     */
    public static void saveSummaryListByUserId(List<DealAutomaticSummaryList> summaryList, String userId) {

        User currentUser = [Select Id, Name From User Where Id = :userId LIMIT 1];

        GroupMember currentGroup = [Select Id, UserOrGroupId, GroupId From GroupMember Where GroupId = :Label.Kennesaw_Group_Id LIMIT 1];

        List<DealAutomaticUserGroup> userGroupList = new List<DealAutomaticUserGroup>();
        List<Deal_Automatic_Assignment_Data__c> assignmentData = new List<Deal_Automatic_Assignment_Data__c>();
        Map<Decimal, Deal_Automatic_Assignment_Data__c> dataMap = getDealAutomaticAssignmentDataMap();

        for (DealAutomaticSummaryList summaryItem : summaryList) {

            for (DealAutomaticSummaryAttribute summaryAttribute : summaryItem.attributes) {

                Deal_Automatic_Assignment_Data__c data = dataMap.containsKey(summaryAttribute.id) ? dataMap.get(summaryAttribute.id) : null;

                if (data != null) {
                    userGroupList = (List<DealAutomaticUserGroup>)JSON.deserializeStrict(data.Assigned_Users__c, List<DealAutomaticUserGroup>.class);

                    if (summaryAttribute.isActive && !data.Assigned_Users__c.containsIgnoreCase(userId)) {
                        userGroupList.add(new DealAutomaticUserGroup(currentUser.Id, currentUser.Name, currentGroup.GroupId, LOCATION_GROUP_OPTIONS[0], summaryAttribute.isActive, true));
                    } else if (!summaryAttribute.isActive && data.Assigned_Users__c.containsIgnoreCase(userId)) {
                        userGroupList = AutomaticAssignmentUtils.removeUserFromList(userGroupList, userId);
                    }
                } else if (data == null && summaryAttribute.isActive) {
                    data = new Deal_Automatic_Assignment_Data__c();
                    userGroupList = new List<DealAutomaticUserGroup>();
                    userGroupList.add(new DealAutomaticUserGroup(currentUser.Id, currentUser.Name, currentGroup.GroupId, LOCATION_GROUP_OPTIONS[0], summaryAttribute.isActive, true));
                }

                if (data != null) {
                    data.Attribute_Id__c = summaryAttribute.id;
                    data.Assigned_Users__c = JSON.serialize(userGroupList);
                    assignmentData.add(data);
                }
            }
        }

        if (assignmentData.size() > 0)
            upsert assignmentData;
    }

    /**
     * [reAssignDeals description]
     * @param fromUserId [description]
     * @param toUserId   [description]
     */
    public static void reAssignDeals(String fromUserId, String toUserId, Datetime fromDate, Datetime toDate) {

        Integer totalApps = 0;
        Integer totalLeads = 0;

        List<Opportunity> apps = new List<Opportunity>();

        for (Opportunity currentApp : [SELECT Id, OwnerId
                                       FROM Opportunity
                                       WHERE Status__c NOT IN ('Credit Declined (or Unqualified)', 'Funded', 'NEW - ENTERED', 'Declined – Do to Fraud', 'KYC Fail', 'Documents Rejected')
                                       AND Status__c != NULL
                                       AND Automatically_Assigned__c = true
                                               AND OwnerId = :fromUserId
                                                       AND Automatically_Assigned_Date__c >= :fromDate
                                                       AND Automatically_Assigned_Date__c <= :toDate]) {

            currentApp.OwnerId = toUserId;
            apps.add(currentApp);
            totalApps++;
        }

        update apps;
        System.debug('Apps = ' + totalApps);

        List<Lead> leads = new List<Lead>();

        for (Lead currentLead : [SELECT id, OwnerId
                                 FROM Lead
                                 WHERE Lead.Automatically_Assigned__c = true
                                         AND OwnerId = :fromUserId
                                                 AND Status NOT IN ('Mailed', 'Force', 'Application Submitted')
                                                 AND Automatically_Assigned_Date__c >= :fromDate
                                                 AND Automatically_Assigned_Date__c <= :toDate
                                                 AND IsConverted = false]) {
            currentLead.OwnerId = toUserId;
            leads.add(currentLead);
            totalLeads++;
        }

        update leads;
        System.debug('Leads = ' + totalLeads);
    }

    public static List<DealAutomaticUserGroup> getDefaultUsers() {
        List<DealAutomaticUserGroup> userWithGroups = new List<DealAutomaticUserGroup>();

        List<String> defaultUsers = Label.DAA_DefaultUsers.split(',');

        for (String s : defaultUsers) {
            for (User u : [Select Id, Name
                           From User
                           Where Id = : s]) {
                userWithGroups.add(new DealAutomaticUserGroup(u.Id, u.Name, '', '', false, true));
            }
        }
        return userWithGroups;
    }

    public static List<String> getUsersListByState(String groupId){
        List<String> userIdListResponse = new List<String>();
        List<User> userList = getUsersFromGroupMember(groupId);                                 
        if (userList != null && userList.size() > 0){
            String body = 'user-list='+JSON.serialize(userList);
            System.debug(body);
            for (ProxyApiIncontact.AgentsStateEntity entity : ProxyApiIncontact.getAgentsState(body)){
                userIdListResponse.add(entity.userId);
            }    
        }
        return userIdListResponse;
    }
    
    public static List<String> getUsersListBySchedule(String groupId){
        List<String> userIdListResponse = new List<String>();
        List<User> userList = getUsersFromGroupMember(groupId);
        System.debug('UserList:: ' + userList.size());                                    
        if (userList != null && userList.size() > 0){ 
            System.debug(Datetime.now());
            for (User u : userList){
                if (u.schedule_from__c != null && u.schedule_to__c != null && 
                    	checkTimeBetween(u.schedule_from__c, u.schedule_to__c)){
                	userIdListResponse.add(u.Id);
                }
            }    
        }
        return userIdListResponse;
    }
    
    public static String getNextUserIdInList(List<String> userList, Integer counter){        
        if (userList != null && counter != null){  
            integer index = math.mod( counter, userList.size() );
            return String.valueOf(userList[index]);
        }
        else
            return '';             
    }
    
    public static List<User> getUsersFromGroupMember(String groupId){
    	return [SELECT Id, Email, schedule_from__c, schedule_to__c
                FROM User
                WHERE ID IN (SELECT UserOrGroupId
                             FROM GroupMember
                             WHERE GroupId =: groupId)];
    } 
    
    public static boolean checkTimeBetween(Time t_from, Time t_to){
        Datetime now = Datetime.now().addSeconds(UserInfo.getTimezone().getOffset(Datetime.now())/1000 );
        //Datetime now = DateTime.parse('07/17/2019 01:30 PM').addSeconds(UserInfo.getTimezone().getOffset(DateTime.parse('07/17/2019 01:30 PM'))/1000 );
        DateTime min_dt = ((Datetime)System.today()).addHours(t_from.hour()).addMinutes(t_from.minute());
        DateTime max_dt = ((Datetime)System.today()).addHours(t_to.hour()).addMinutes(t_to.minute());
        
        if(t_from.hour() > t_to.hour()){
            if(now.getTime() > max_dt.getTime())
                max_dt = max_dt.addHours(24);
            else if(now.getTime() <= max_dt.getTime())
                min_dt = min_dt.addHours(-24);
        }
        
        return (now.getTime() >= min_dt.getTime() && now.getTime() <= max_dt.getTime());
    }
}