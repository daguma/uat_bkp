global class ScorecardInputData {

    public Long creditReportId {get; set;}
    public String income {get; set;}
    public String loanAmount {get; set;}
    public String state {get; set;}
    public String isPartner {get; set;}
    public String employmentType {get; set;}
    public String employmentDuration {get; set;}
    public String employmentGapDurationDays {get; set;}
    public String priorEmploymentDuration {get; set;}
    public String paymentFrequency {get; set;}
    public String analyticRandomNumber {get; set;}
    public String feeHandling {get; set;}
    public String partner {get; set;}
    public Boolean isFinwise {get; set;}
    public String productCode {get; set;}
    /* BRMS params */
    public GDSRequestParameter gdsRequestParameter {get; set;}
    public string entityId {get; set;}
    public string bureau {get; set;}
    /* BRMS params */
    public String applicationType {get; set;}
    public String previousGrade {get; set;}
    public String previousTerm {get; set;}
    public String fundedApr {get; set;}
    public String currentPayoffBalance {get; set;}

    public Boolean isMrcState {get; set;}
    public String merchantState {get; set;}
    public String bankName {get; set;}
    public String leadSource {get; set;}
    public String promotionTypes {get; set;}
    public String oppCreatedDate {get; set;}
    public Boolean AONEnrollment {get;set;}
    public String salesCategory  {get;set;}
}