@isTest public class ApplicantsTest {

    @isTest static void GetXmlFromAllApplicantsList() {

        List<Applicants.AllApplicants> lapp = new List<Applicants.AllApplicants>();

        String defaultName = 'test';

        List<String> values = new List<String>();
        values.add('03');

        CreditReportSegmentsUtil.SegmentContent content = new CreditReportSegmentsUtil.SegmentContent();
        content.valuesList = values;

        List<CreditReportSegmentsUtil.SegmentContent> lcontent = new List<CreditReportSegmentsUtil.SegmentContent>();
        lcontent.add(content);

        CreditReportSegmentsUtil.Segment segment = new CreditReportSegmentsUtil.Segment();
        segment.headersList.add('Indicator 1');
        segment.contentList = lcontent;

        CreditReportSegmentsUtil.FraudIndicators fraudIndicators = new CreditReportSegmentsUtil.FraudIndicators('Indicator 1');

        Applicants apps = new Applicants();

        String result = apps.GetXmlFromAllApplicantsList(lapp);

        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartDocument(null, '1.0');
        w.writeStartElement(null, 'Items', null);
        w.writeEndElement();
        w.writeEndDocument();

        String expected = w.getXmlString();
        w.close();

        System.assertEquals(expected, result);
        lapp = apps.InitializeAllApplicantsList(defaultName, fraudIndicators);

        result = apps.GetXmlFromAllApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        result = apps.GetXmlFromAllApplicantsList(lapp, '1010', true);

        System.assert(result != null);
        System.assert(result != '');
        System.assertNotEquals(expected, result);

        result = apps.GetXmlFromAllApplicantsList(lapp, '1000', false);

        System.assert(result != null);
        System.assert(result != '');
        System.assertNotEquals(expected, result);

        result = apps.GetXmlFromAllApplicantsList(lapp, '1020', false);

        System.assert(result != null);
        System.assert(result != '');
        System.assertNotEquals(expected, result);

        values.add('27');

        content.valuesList = values;

        lcontent.add(content);

        segment.headersList.add('Indicator 2');
        segment.contentList = lcontent;

        fraudIndicators = new CreditReportSegmentsUtil.FraudIndicators('Indicator 2');

        lapp = apps.InitializeAllApplicantsList(result, defaultName, fraudIndicators);

        System.assertNotEquals(null, lapp);

        lapp = apps.InitializeAllApplicantsList('', defaultName, fraudIndicators);

        System.assertNotEquals(null, lapp);

        Contact cntc = LibraryTest.createContactTH();

        apps.cntID = cntc.id;

        apps.SetContactID();

        System.assertEquals(cntc.id, Applicants.contactID);

        Boolean response = apps.isSecondGovernmentIdVerified(result, cntc.id);

        System.assertEquals(false, response);

        List<Applicants.AllApplicants> lapp1 = new List<Applicants.AllApplicants>();
        lapp1.add(new Applicants.AllApplicants(1010, defaultName, fraudIndicators));
        lapp1.add(new Applicants.AllApplicants(1020, defaultName, fraudIndicators));
        lapp1.add(new Applicants.AllApplicants(1030, defaultName, fraudIndicators));

        result = apps.GetXmlFromAllApplicantsList(lapp1);

        response = apps.isSecondGovernmentIdVerified(result, cntc.id);

        System.assertEquals(false, response);

        List<Applicants.AllApplicants> lapp2 = new List<Applicants.AllApplicants>();

        Applicants.AllApplicants apps2 = new Applicants.AllApplicants(1060, defaultName, fraudIndicators);
        apps2.required = 'true';
        lapp2.add(apps2);

        result = apps.GetXmlFromAllApplicantsList(lapp2, '1060', true);

        response = apps.isSecondGovernmentIdVerified(result, cntc.id);

        System.assertEquals(true, response);

        IDology_Request__c ido = new IDology_Request__c();
        ido.Contact__c = cntc.id;
        ido.Verification_Result__c = 'Pass';
        insert ido;

        ido = new IDology_Request__c();
        ido.Contact__c = cntc.id;
        ido.Verification_Result__c = 'Pass';
        insert ido;

        apps2 = new Applicants.AllApplicants(1060, defaultName, fraudIndicators);

        System.assertEquals('true', apps2.required);

        values = new List<String>();
        values.add('05');
        values.add('25');

        content = new CreditReportSegmentsUtil.SegmentContent();
        content.valuesList = values;

        lcontent = new List<CreditReportSegmentsUtil.SegmentContent>();
        lcontent.add(content);

        segment = new CreditReportSegmentsUtil.Segment();
        segment.headersList.add('Indicator 1');
        segment.headersList.add('Indicator 2');
        segment.contentList = lcontent;

        fraudIndicators = new CreditReportSegmentsUtil.FraudIndicators('Indicator 1');

        lapp2 = new List<Applicants.AllApplicants>();

        apps2 = new Applicants.AllApplicants(1050, defaultName, fraudIndicators);
        lapp2.add(apps2);

        apps2 = new Applicants.AllApplicants(1055, defaultName, fraudIndicators);
        lapp2.add(apps2);

        result = apps.GetXmlFromAllApplicantsList(lapp2);

        lapp = apps.InitializeAllApplicantsList(result, defaultName, fraudIndicators);
    }

    @isTest static void validations() {

        String defaultName = 'test';

        List<String> values = new List<String>();
        values.add('03');

        CreditReportSegmentsUtil.SegmentContent content = new CreditReportSegmentsUtil.SegmentContent();
        content.valuesList = values;

        List<CreditReportSegmentsUtil.SegmentContent> lcontent = new List<CreditReportSegmentsUtil.SegmentContent>();
        lcontent.add(content);

        CreditReportSegmentsUtil.Segment segment = new CreditReportSegmentsUtil.Segment();
        segment.headersList.add('Indicator 1');
        segment.contentList = lcontent;

        CreditReportSegmentsUtil.FraudIndicators fraudIndicators = new CreditReportSegmentsUtil.FraudIndicators('Indicator 1');

        Applicants apps = new Applicants();

        List<Applicants.AllApplicants> lapp2 = new List<Applicants.AllApplicants>();
        Applicants.AllApplicants apps2 = new Applicants.AllApplicants(1060, defaultName, fraudIndicators);

        lapp2.add(apps2);
        String result = apps.GetXmlFromAllApplicantsList(lapp2);

        Boolean idVerified = apps.isIdentificationVerified(result);

        System.assertEquals(false, idVerified);

        apps2 = new Applicants.AllApplicants(1030, defaultName, fraudIndicators);
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        lapp2.add(apps2);

        result = apps.GetXmlFromAllApplicantsList(lapp2);

        idVerified = apps.isIdentificationVerified(result);

        System.assertEquals(true, idVerified);

        Boolean phoneVerified = apps.isPhoneVerified(result);

        System.assertEquals(false, phoneVerified);

        lapp2 = new List<Applicants.AllApplicants>();

        apps2 = new Applicants.AllApplicants(1040, defaultName, fraudIndicators);
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        lapp2.add(apps2);

        result = apps.GetXmlFromAllApplicantsList(lapp2);

        phoneVerified = apps.isPhoneVerified(result);

        System.assertEquals(true, phoneVerified);

        Boolean utilVerified = apps.isUtilityForAddressVerificationVerified(result);

        System.assertEquals(false, utilVerified);

        lapp2 = new List<Applicants.AllApplicants>();

        apps2 = new Applicants.AllApplicants(1050, defaultName, fraudIndicators);
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        lapp2.add(apps2);

        result = apps.GetXmlFromAllApplicantsList(lapp2);

        utilVerified = apps.isUtilityForAddressVerificationVerified(result);

        System.assertEquals(true, utilVerified);

        Boolean ssnVerified = apps.isSSNReportedMoreFrequentlyForAnotherVerified(result);

        System.assertEquals(false, ssnVerified);

        lapp2 = new List<Applicants.AllApplicants>();

        apps2 = new Applicants.AllApplicants(1055, defaultName, fraudIndicators);
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        lapp2.add(apps2);

        result = apps.GetXmlFromAllApplicantsList(lapp2);

        ssnVerified = apps.isSSNReportedMoreFrequentlyForAnotherVerified(result);

        System.assertEquals(true, ssnVerified);
    }

    @isTest static void GetXmlFromEmployedApplicantsList() {

        List<Applicants.EmployedApplicants> lapp = new List<Applicants.EmployedApplicants>();

        String defaultName = 'test';

        Applicants apps = new Applicants();

        String result = apps.GetXmlFromEmployedApplicantsList(lapp);

        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartDocument(null, '1.0');
        w.writeStartElement(null, 'Items', null);
        w.writeEndElement(); //Items
        w.writeEndDocument();

        String expected = w.getXmlString();
        w.close();

        System.assertEquals(expected, result);

        lapp = apps.InitializeEmployedApplicantsList(true, defaultName);

        result = apps.GetXmlFromEmployedApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        lapp = apps.InitializeEmployedApplicantsList(false, defaultName);

        result = apps.GetXmlFromEmployedApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        lapp = apps.InitializeEmployedApplicantsList(result, defaultName);

        System.assertNotEquals(null, lapp);

        List<Applicants.EmployedApplicants> lapp2 = new List<Applicants.EmployedApplicants>();

        Applicants.EmployedApplicants apps2 = new Applicants.EmployedApplicants(1010, true, defaultName);
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        lapp2.add(apps2);

        result = apps.GetXmlFromEmployedApplicantsList(lapp2);

        Boolean existsValues = apps.ExistsValuesForEmployedApplicants(result);

        System.assertEquals(true, existsValues);

        lapp = apps.InitializeEmployedApplicantsList('', defaultName);

        System.assertNotEquals(null, lapp);

        String result2 = apps.GetXmlFromEmployedApplicantsList(lapp);

        existsValues = apps.ExistsValuesForEmployedApplicants(result2);

        System.assertEquals(false, existsValues);
    }

    @isTest static void GetXmlFromRetiredApplicantsList() {

        List<Applicants.RetiredApplicants> lapp = new List<Applicants.RetiredApplicants>();

        String defaultName = 'test';

        Applicants apps = new Applicants();

        String result = apps.GetXmlFromRetiredApplicantsList(lapp);

        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartDocument(null, '1.0');
        w.writeStartElement(null, 'Items', null);
        w.writeEndElement(); //Items
        w.writeEndDocument();

        String expected = w.getXmlString();
        w.close();

        System.assertEquals(expected, result);

        lapp = apps.InitializeRetiredApplicantsList(true, defaultName);

        result = apps.GetXmlFromRetiredApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        lapp = apps.InitializeRetiredApplicantsList(false, defaultName);

        result = apps.GetXmlFromRetiredApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        lapp = apps.InitializeRetiredApplicantsList(result, defaultName);

        System.assertNotEquals(null, lapp);

        List<Applicants.RetiredApplicants> lapp2 = new List<Applicants.RetiredApplicants>();

        Applicants.RetiredApplicants apps2 = new Applicants.RetiredApplicants(1010, true, defaultName);
        apps2.required = 'true';
        apps2.Form1099ReceivedDate = date.today().format();
        apps2.BenefitLetterReceivedDate = date.today().format();
        lapp2.add(apps2);

        result = apps.GetXmlFromRetiredApplicantsList(lapp2);

        Boolean existsValues = apps.ExistsValuesForRetiredApplicants(result);

        System.assertEquals(true, existsValues);

        lapp = apps.InitializeRetiredApplicantsList('', defaultName);

        System.assertNotEquals(null, lapp);

        String result2 = apps.GetXmlFromRetiredApplicantsList(lapp);

        existsValues = apps.ExistsValuesForRetiredApplicants(result2);

        System.assertEquals(false, existsValues);
    }

    @isTest static void GetXmlFromSelfEmployedApplicantsList() {

        List<Applicants.SelfEmployedApplicants> lapp = new List<Applicants.SelfEmployedApplicants>();

        String defaultName = 'test';

        Applicants apps = new Applicants();

        String result = apps.GetXmlFromSelfEmployedApplicantsList(lapp);

        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartDocument(null, '1.0');
        w.writeStartElement(null, 'Items', null);
        w.writeEndElement(); //Items
        w.writeEndDocument();

        String expected = w.getXmlString();
        w.close();

        System.assertEquals(expected, result);

        lapp = apps.InitializeSelfEmployedApplicantsList(true, defaultName);

        result = apps.GetXmlFromSelfEmployedApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        lapp = apps.InitializeSelfEmployedApplicantsList(false, defaultName);

        result = apps.GetXmlFromSelfEmployedApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        lapp = apps.InitializeSelfEmployedApplicantsList(result, defaultName);

        System.assertNotEquals(null, lapp);

        List<Applicants.SelfEmployedApplicants> lapp2 = new List<Applicants.SelfEmployedApplicants>();

        Applicants.SelfEmployedApplicants apps2 = new Applicants.SelfEmployedApplicants(1010, true, defaultName);
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        lapp2.add(apps2);

        result = apps.GetXmlFromSelfEmployedApplicantsList(lapp2);

        Boolean existsValues = apps.ExistsValuesForSelfEmployedApplicants(result);

        System.assertEquals(true, existsValues);

        lapp = apps.InitializeSelfEmployedApplicantsList('', defaultName);

        System.assertNotEquals(null, lapp);

        String result2 = apps.GetXmlFromSelfEmployedApplicantsList(lapp);

        existsValues = apps.ExistsValuesForSelfEmployedApplicants(result2);

        System.assertEquals(false, existsValues);
    }

    @isTest static void GetXmlFromMiscellaneousApplicantsList() {

        List<Applicants.MiscellaneousApplicants> lapp = new List<Applicants.MiscellaneousApplicants>();

        String defaultName = 'test';

        Applicants apps = new Applicants();

        String result = apps.GetXmlFromMiscellaneousApplicantsList(lapp);

        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartDocument(null, '1.0');
        w.writeStartElement(null, 'Items', null);
        w.writeEndElement(); //Items
        w.writeEndDocument();

        String expected = w.getXmlString();
        w.close();

        System.assertEquals(expected, result);

        lapp = apps.InitializeMiscellaneousApplicantsList(true, defaultName);

        result = apps.GetXmlFromMiscellaneousApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        lapp = apps.InitializeMiscellaneousApplicantsList(false, defaultName);

        result = apps.GetXmlFromMiscellaneousApplicantsList(lapp);
        System.assertNotEquals(expected, result);

        lapp = apps.InitializeMiscellaneousApplicantsList(result, defaultName);

        System.assertNotEquals(null, lapp);

        List<Applicants.MiscellaneousApplicants> lapp2 = new List<Applicants.MiscellaneousApplicants>();

        Applicants.MiscellaneousApplicants apps2 = new Applicants.MiscellaneousApplicants(1010, true, defaultName);
        apps2.required = 'true';
        apps2.receivedDate = date.today().format();
        lapp2.add(apps2);

        result = apps.GetXmlFromMiscellaneousApplicantsList(lapp2);

        Boolean existsValues = apps.ExistsValuesForMiscellaneousApplicants(result);

        System.assertEquals(true, existsValues);

        lapp = apps.InitializeMiscellaneousApplicantsList('', defaultName);

        System.assertNotEquals(null, lapp);

        String result2 = apps.GetXmlFromMiscellaneousApplicantsList(lapp);

        existsValues = apps.ExistsValuesForMiscellaneousApplicants(result2);

        System.assertEquals(false, existsValues);
    }

    @isTest static void DateParse() {

        Applicants apps = new Applicants();

        Date result = apps.DateParse('');

        System.assertEquals(null, result);

        result = apps.DateParse('20/042016');

        System.assertEquals(null, result);

        result = apps.DateParse('20-42016');

        System.assertEquals(null, result);

        result = apps.DateParse('04/20/2016');

        System.assertEquals(Date.parse('04/20/2016'), result);

        result = apps.DateParse('20/04/2016');

        System.assertEquals(null, result);

        result = apps.DateParse('04-20-2016');

        System.assertEquals(null, result);

        result = apps.DateParse('20042016');

        System.assertEquals(Date.parse('04/20/2016'), result);

        result = apps.DateParse('04202016');

        System.assertEquals(null, result);
    }
}