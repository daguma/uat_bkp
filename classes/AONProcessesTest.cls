@isTest
private class AONProcessesTest {

    @isTest static void creates_payment() {

        Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

        RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

        c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
        testCompany.Name = 'Lendingpoint LLC';
        testCompany.RecordTypeId = recordTy.Id;
        testCompany.OwnerId = newGroup.Id;
        insert testCompany;

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Last_Transaction_Timestamp__c = Datetime.now();
        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        contract.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
        update contract;

        loan_AON_Information__c aonInfo = new loan_AON_Information__c();
        aonInfo.Contract__c = contract.Id;
        aonInfo.Product_Code__c = 'LPOPTION01';
        aonInfo.Claim_Status__c = 'c';
        insert aonInfo;

        String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1].Name;

        AONObjectsParameters.AONPaymentInputData inputData = new AONObjectsParameters.AONPaymentInputData();
        inputData.accountNumber = contractName;
        inputData.companyCode = 'LPT0010000';
        inputData.productCode = 'LPOPTION01';
        inputData.claimStatusDate = '2018-01-01';
        inputData.paymentAmount = '200';
        inputData.claimStatus = 'A';
        inputData.paymentDate = String.valueOf(Date.today().addDays(1));

        AONProcesses process = new AONProcesses();

        Boolean result = process.createPayment(inputData);

        System.assertEquals(true, result);
    }

    @isTest static void does_not_create_payment() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1].Name;

        AONObjectsParameters.AONPaymentInputData inputData = new AONObjectsParameters.AONPaymentInputData();
        inputData.accountNumber = contractName;
        inputData.companyCode = 'LPT0010000';
        inputData.productCode = 'LPOPTION01';
        inputData.claimStatusDate = '2018-01-01';
        inputData.paymentAmount = '200';
        inputData.paymentDate = '2018-01-01';

        AONProcesses process = new AONProcesses();

        Boolean result = process.createPayment(inputData);

        System.assertEquals(false, result);
    }

    @isTest static void creates_payment_with_active_status() {

        String profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;

        User notHal = [SELECT Id, Name FROM User WHERE IsActive = true AND ProfileId = : profileId ORDER BY CreatedDate LIMIT 1];

        System.debug('User Not Hal ' + notHal.Name);

        System.runAs(notHal) {

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Loan_Account__c contract = LibraryTest.createContractTH();
            contract.loan__ACH_On__c = true;
            contract.loan__ACH_Routing_Number__c = '123123123';
            contract.loan__ACH_Account_Number__c = '1231231231';
            contract.loan__ACH_Debit_Amount__c = 200;
            contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(10);
            contract.loan__Last_Transaction_Timestamp__c = Datetime.now();
            contract.loan__Disbursal_Amount__c = 200;
            contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
            contract.loan__Disbursal_Status__c = 'Fully Disbursed';
            contract.loan__Disbursed_Amount__c = 200;
            contract.loan__loan_status__c = 'Active - Good Standing';
            contract.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            contract.loan__Repayment_Billing_Method__c = 'Interest Only';
            update contract;

            loan_AON_Information__c aonInfo = new loan_AON_Information__c();
            aonInfo.Contract__c = contract.Id;
            aonInfo.Product_Code__c = 'LPOPTION01';
            insert aonInfo;

            String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1].Name;

            AONObjectsParameters.AONPaymentInputData inputData = new AONObjectsParameters.AONPaymentInputData();
            inputData.accountNumber = contractName;
            inputData.companyCode = 'LPT0010000';
            inputData.productCode = 'LPOPTION01';
            inputData.claimStatusDate = '2018-01-01';
            inputData.paymentAmount = '200';
            inputData.paymentDate = String.valueOf(Date.today().addDays(1));
            inputData.claimStatus = 'A';

            AONProcesses process = new AONProcesses();

            //Boolean result = process.createPayment(inputData);

            loan__Loan_Account__c CLContract = [SELECT Id, loan__ACH_On__c, ACH_Flag_Reason__c FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1];
 

        }
    }

    @isTest static void processes_cancellation() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan_AON_Information__c aonInfo = new loan_AON_Information__c();
        aonInfo.Contract__c = contract.Id;
        aonInfo.Product_Code__c = 'LPOPTION01';
        aonInfo.Enrollment_Date__c = Datetime.now();
        insert aonInfo;

        Integer count = [SELECT COUNT() FROM loan_AON_Cancellations__c];

        String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1].Name;

        AONObjectsParameters.AONCancellationInputData inputData = new AONObjectsParameters.AONCancellationInputData(contractName, 'TST', '200');

        AONProcesses aonProcess = new AONProcesses();
        AONObjectsParameters.SalesforceResponse result = aonProcess.aimCancellation(inputData);

        System.assert([SELECT COUNT() FROM loan_AON_Cancellations__c] > count, 'Cancellation');
    }

    @isTest static void processes_cancellation_without_enrollment() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan_AON_Information__c aonInfo = new loan_AON_Information__c();
        aonInfo.Contract__c = contract.Id;
        aonInfo.Product_Code__c = 'LPOPTION01';
        insert aonInfo;

        Integer count = [SELECT COUNT() FROM loan_AON_Cancellations__c];

        String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1].Name;

        AONObjectsParameters.AONCancellationInputData inputData = new AONObjectsParameters.AONCancellationInputData(contractName, 'TST', '200');

        AONProcesses aonProcess = new AONProcesses();
        AONObjectsParameters.SalesforceResponse result = aonProcess.aimCancellation(inputData);

        System.assert([SELECT COUNT() FROM loan_AON_Cancellations__c] == count, 'Cancellation');
    }

    @isTest static void processes_cancellation_already_canceled() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan_AON_Information__c aonInfo = new loan_AON_Information__c();
        aonInfo.Contract__c = contract.Id;
        aonInfo.Product_Code__c = 'LPOPTION01';
        aonInfo.Enrollment_Date__c = Datetime.now();
        aonInfo.IsCancellation__c = true;
        insert aonInfo;

        Integer count = [SELECT COUNT() FROM loan_AON_Cancellations__c];

        String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1].Name;

        AONObjectsParameters.AONCancellationInputData inputData = new AONObjectsParameters.AONCancellationInputData(contractName, 'TST', '200');

        AONProcesses aonProcess = new AONProcesses();
        AONObjectsParameters.SalesforceResponse result = aonProcess.aimCancellation(inputData);

        System.assert([SELECT COUNT() FROM loan_AON_Cancellations__c] == count, 'Cancellation');
    }

    @isTest static void processes_cancellation_not_found() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        Integer count = [SELECT COUNT() FROM loan_AON_Cancellations__c];

        String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1].Name;

        AONObjectsParameters.AONCancellationInputData inputData = new AONObjectsParameters.AONCancellationInputData();
        inputData.company = 'LPT0010000';
        inputData.accountNumber = contractName;
        inputData.productCode = 'LPOPTION01';
        inputData.cancelReasonCode = 'TST';
        inputData.effectiveDate = String.valueOf(Datetime.now());
        inputData.refundAmount = '200';

        AONProcesses aonProcess = new AONProcesses();
        AONObjectsParameters.SalesforceResponse result = aonProcess.aimCancellation(inputData);

        System.assert([SELECT COUNT() FROM loan_AON_Cancellations__c] == count, 'Cancellation');
    }

    @isTest static void catches_exception_cancellation() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan_AON_Information__c aonInfo = new loan_AON_Information__c();
        aonInfo.Contract__c = contract.Id;
        aonInfo.Product_Code__c = 'LPOPTION01';
        aonInfo.Enrollment_Date__c = Datetime.now();
        insert aonInfo;

        Integer count = [SELECT COUNT() FROM loan_AON_Cancellations__c];

        String contractName = [SELECT Name FROM loan__Loan_Account__c WHERE Id = : contract.Id LIMIT 1].Name;

        AONObjectsParameters.AONCancellationInputData inputData = new AONObjectsParameters.AONCancellationInputData(contractName, '', '200');

        AONProcesses aonProcess = new AONProcesses();
        AONObjectsParameters.SalesforceResponse result = aonProcess.aimCancellation(inputData);

        
    }

    @isTest static void inserts_logging() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        Integer count = [SELECT COUNT() FROM Logging__c];

        AONProcesses.setContactLoggging('Test', 'Request', 'Response', contract.Id);

        System.assert([SELECT COUNT() FROM Logging__c] > count, 'Logging');
    }

    @isTest static void processes_http_response() {

        String aonResp = '{"aonResponse":null,"errorMessage":null,"errorDetails":null}';

        HttpResponse resp = new HttpResponse();
        resp.setBody(aonResp);
        resp.setStatus('201');

        String result = AONProcesses.proxyResponseProcess(resp);

        System.assertNotEquals(null, result);
    }
}