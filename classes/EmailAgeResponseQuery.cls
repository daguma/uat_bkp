global class EmailAgeResponseQuery {
    public String email;
    public String queryType;
    public Integer count;
    public String created;
    public String lang;
    public Integer responseCount;
    public List<EmailAgeResponseResults> results;
}