@isTest public class CreditReportInputDataTest {
    @isTest static void validate_instance() {
        Test.startTest();
        CreditReportInputData id = new CreditReportInputData();
        Test.stopTest();

        System.assertEquals(null, id.creditReportRequestParameter);
        System.assertEquals(null, id.decisioningParameter);


        CreditReportRequestParameter rp = new CreditReportRequestParameter();
        rp.last_name = 'test data';
        rp.first_name = 'test data';
        rp.ssn = 'test data';
        rp.state = 'test data';
        rp.city = 'test data';
        rp.zip_code = 'test data';
        rp.house_nb = 'test data';
        rp.pre_Dir = 'test data';
        rp.street_type = 'test data';
        rp.street = 'test data';
        rp.post_Dir = 'test data';
        rp.apt_Nbr = 'test data';
        rp.unit_Type = 'test data';
        rp.pobNbr = 'test data';
        rp.Dob_MMDDYYYY = 'test data';
        rp.dob_YYYYMMDD = 'test data';
        rp.phone_number = 'test data';
        rp.phone_area = 'test data';
        rp.bureau = 'test data';

        id.creditReportRequestParameter = rp;

        System.assertEquals(rp, id.creditReportRequestParameter);

        DecisioningParameter dp = new DecisioningParameter();
        dp.entityId = 'test';
        dp.entityName = 'test';
        dp.income = 'test';
        dp.employmentType = 'test';
        dp.employmentDuration = 'test';
        dp.employmentGapDurationDays = 'test';
        dp.priorEmploymentDuration = 'test';
        dp.useOfFunds = 'test';
        dp.isPartner = 'test';
        dp.loanAmount = 'test';

        id.decisioningParameter = dp;

        System.assertEquals(dp, id.decisioningParameter);
        /* BRMS Change */
        GDSRequestParameter gdsReqParam = new GDSRequestParameter();
        gdsReqParam.contactId = 'test';
        gdsReqParam.channelId  = 'test';
        gdsReqParam.sequenceId  = 'test';
        gdsReqParam.productId  = 'test';
        gdsReqParam.sessionId  = 'test';
        gdsReqParam.executionType  = 'test';
        
        id.gdsRequestParameter = gdsReqParam;
        
        System.assertEquals(gdsReqParam, id.gdsRequestParameter);
        /* BRMS Change */
    }
}