@isTest public class MultiloanFailsTestCtrl {
    
    @testSetup static void initializes_objects(){
        MultiLoan_Settings__c multiSet = new MultiLoan_Settings__c();
        multiSet.name = 'settings';
        multiSet.Check_Frequency__c = 30;
        multiSet.Maximum_Loan_Amount__c = 5000;
        multiSet.Minimum_Loan_Amount__c = 1000;
        multiSet.Days_To_First_Check__c = 60;
        multiSet.AccountId__c = '0011700000t45FtAAI';
        multiSet.Allowed_Multiloan_States__c = 'AK';
        insert multiSet;
        
        Account multiLoanAcc = new Account();
        multiLoanAcc.Name = 'Multi-Loan';
        insert multiLoanAcc;
        
    }
    
    @isTest static void get_eligibility_review_query_test(){
        
        System.assertEquals(true, String.isNotEmpty(MultiloanFails.getIneligibilityReviewQuery(5)));
    }
    
    private static Lending_Product__c creates_Product(){
        
        Lending_Product__c product = New Lending_Product__c();
        product.Name = 'Lending Point';
        product.Allow_Payoff_With_Uncleared_Payments__c = false;
        product.Amortization_Calculation_Method__c = 'Interest Bearing';
        product.Amortization_Enabled__c = true;
        product.Amortization_Method__c = 'Interest Bearing';
        product.Days_In_A_Year__c = 365;
        insert product;
        
        return product;
        
    }
    
     private static loan__Loan_Account__c creates_contract(){
         
         Lending_Product__c product = creates_Product();
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Loan_Status__c = 'Active - Good Standing';
		contract.loan__ACH_On__c = true;
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 1;
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(1);
        contract.loan__Loan_Amount__c = 4000;
        contract.loan__Principal_Paid__c = 2000;
        contract.loan__ACH_Routing_Number__c = '123456789';
        contract.loan__ACH_Account_Number__c = '0987654321';
        contract.loan__ACH_Debit_Amount__c = 50;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(6);
        contract.loan__Principal_Remaining__c = 1000;
        contract.loan__Accrual_Start_Date__c = Date.today().addDays(-196);
        contract.SCRA__c = false;
        contract.Date_of_Settlement__c = Date.today().addDays(-66);
        contract.Settlement__c = 2.0;
        contract.Is_Material_Modification__c = false;
        contract.First_Payment_Missed__c = false;
        contract.First_Payment_Made_after_Due_Date__c = false;
        contract.DMC__c = null;
        contract.loan__Metro2_Account_pmt_history_date__c = Date.today().addDays(-6);
        contract.Asset_Sale_Line__c = null;
        contract.loan__Loan_Product_Name__c = product.Loan_Lending_Product__c;
        update contract;
       
        
        return contract;
        
    }
    
    
    
    @isTest static void checks_ruleFail_for_LoanAmountCheck() {
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Maximum_Loan_Amount__c = 4500;
        opp.Status__c = 'Funded';
        Opp.Type = 'New';
        update opp;
        
        loan__Loan_Account__c contract = creates_contract();
        contract.Opportunity__c = opp.id;
        contract.loan__Loan_Amount__c = 4000;
        update contract;
        
        Multiloan_Params__c params = new Multiloan_Params__c();
        
        
        
        params.contract__c = contract.id;
        params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
        insert params;
		// Amount check field formula : Contract__r.Opportunity__r.Maximum_Loan_Amount__c - Contract__r.loan__Loan_Amount__c
        Test.startTest();
        BatchMultiloanFail job = new BatchMultiloanFail(1);
		database.executebatch(job, 1);
        Test.stopTest();
        
        Multiloan_Params_Logs__c log = [Select Id, Minimum_Amount_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                    WHERE Multiloan_Params__c =: params.Id];
        
        system.assertEquals(true, log.Minimum_Amount_Fail__c);
        
        
        
    }
    
    @isTest static void checks_ruleFail_for_SCRA_History() {
        
        loan__Loan_Account__c contract = creates_contract();
         contract.SCRA__c = true;
         update contract;
        
        Multiloan_Params__c params = new Multiloan_Params__c();
        params.contract__c = contract.id;
        params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
        insert params;
        
        Test.startTest();
        BatchMultiloanFail job = new BatchMultiloanFail(1);
		database.executebatch(job, 1);
        Test.stopTest();
        
        Multiloan_Params_Logs__c log = [Select Id, SCRA_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                    WHERE Multiloan_Params__c =: params.Id];
        
        system.assertEquals(true, log.SCRA_Fail__c);
        
    }
    
     @isTest static void checks_ruleFail_for_Settlement_Modification_History() {
         
         loan__Loan_Account__c contract = creates_contract();
         contract.Settlement__c = 2.0;
         update contract;
         
         Multiloan_Params__c params = new Multiloan_Params__c();
         params.contract__c = contract.id;
         params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
         insert params;

         Test.startTest();
         BatchMultiloanFail job = new BatchMultiloanFail(1);
         database.executebatch(job, 1);
         Test.stopTest();
         
         Multiloan_Params_Logs__c log = [Select Id, Settlement_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                         WHERE Multiloan_Params__c =: params.Id];
            
         system.assertEquals(true, log.Settlement_Fail__c);
        
    }
    
    @isTest static void checks_ruleFail_for_Material_Modification_History() {
         
         loan__Loan_Account__c contract = creates_contract();
         contract.Is_Material_Modification__c = true;
         update contract;
         
         Multiloan_Params__c params = new Multiloan_Params__c();
         params.contract__c = contract.id;
         params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
         insert params;

         Test.startTest();
         BatchMultiloanFail job = new BatchMultiloanFail(1);
         database.executebatch(job, 1);
         Test.stopTest();
         
         Multiloan_Params_Logs__c log = [Select Id, Material_Modification_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                         WHERE Multiloan_Params__c =: params.Id];
            
         system.assertEquals(true, log.Material_Modification_Fail__c);
        
    }
    
    @isTest static void checks_ruleFail_for_First_Payment_Missed() {
         
         loan__Loan_Account__c contract = creates_contract();
         contract.First_Payment_Missed__c = true;
         update contract;
         
         Multiloan_Params__c params = new Multiloan_Params__c();
         params.contract__c = contract.id;
         params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
         insert params;

         Test.startTest();
         BatchMultiloanFail job = new BatchMultiloanFail(1);
         database.executebatch(job, 1);
         Test.stopTest();
         
         Multiloan_Params_Logs__c log = [Select Id, First_Payment_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                         WHERE Multiloan_Params__c =: params.Id];
            
         system.assertEquals(true, log.First_Payment_Fail__c);
        
    }
    
    
    @isTest static void checks_ruleFail_for_DMC_Modification() {
         
         loan__Loan_Account__c contract = creates_contract();
         contract.DMC__c = 'DMC Testing';
         update contract;
         
         Multiloan_Params__c params = new Multiloan_Params__c();
         params.contract__c = contract.id;
         params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
         insert params;

         Test.startTest();
         BatchMultiloanFail job = new BatchMultiloanFail(1);
         database.executebatch(job, 1);
         Test.stopTest();
         
         Multiloan_Params_Logs__c log = [Select Id, DMC_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                         WHERE Multiloan_Params__c =: params.Id];
            
         system.assertEquals(true, log.DMC_Fail__c);
        
    }
    
    
    @isTest static void checks_ruleFail_for_ACH_OR_Debit_Car() {
        
        loan__Loan_Account__c contract = creates_contract();
        contract.loan__ACH_On__c = false;
        contract.Is_Debit_Card_On__c = false;
        update contract;
         
        Multiloan_Params__c params = new Multiloan_Params__c();
        params.contract__c = contract.id;
        params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
        insert params;

        Test.startTest();
        BatchMultiloanFail job = new BatchMultiloanFail(1);
        database.executebatch(job, 1);
        Test.stopTest();
         
        Multiloan_Params_Logs__c log = [Select Id, ACH_OR_Debit_Card_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                         WHERE Multiloan_Params__c =: params.Id];
            
        system.assertEquals(true, log.ACH_OR_Debit_Card_Fail__c);
        
    }
    
    @isTest static void checks_ruleFail_for_LastReportedMetro2Date_fail() {
        
        loan__Loan_Account__c contract = creates_contract();
        contract.loan__Metro2_Account_pmt_history_date__c = Date.today().addDays(1);
        update contract;
         
        Multiloan_Params__c params = new Multiloan_Params__c();
        params.contract__c = contract.id;
        params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
        insert params;

        Test.startTest();
        BatchMultiloanFail job = new BatchMultiloanFail(1);
        database.executebatch(job, 1);
        Test.stopTest();
         
        Multiloan_Params_Logs__c log = [Select Id, Metro2_Date_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                         WHERE Multiloan_Params__c =: params.Id];
            
        system.assertEquals(true, log.Metro2_Date_Fail__c);
        
    }
    
    @isTest static void checks_ruleFail_for_ProductName_fail() {
        
        Lending_Product__c prod =  creates_Product();
        prod.Name = 'Medical';
        //prod.Loan_Lending_Product__c = 'a2517000000JgFGAA0';
        update prod;
        
        loan__Loan_Account__c contract = creates_contract();
        contract.loan__Loan_Product_Name__c = prod.Loan_Lending_Product__c;
        update contract;
         
        Multiloan_Params__c params = new Multiloan_Params__c();
        params.contract__c = contract.id;
        params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
        insert params;

        Test.startTest();
        BatchMultiloanFail job = new BatchMultiloanFail(1);
        database.executebatch(job, 1);
        Test.stopTest();
         
        Multiloan_Params_Logs__c log = [Select Id, Product_Name_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                         WHERE Multiloan_Params__c =: params.Id];
            
        system.assertEquals(true, log.Product_Name_Fail__c);
        
    }
    
    @isTest static void checks_ruleFail_for_AssetSaleline_fail() {

        loan__Loan_Account__c contract = creates_contract();
        contract.Asset_Sale_Line__c = 'Stone Ridge';
        update contract;
         
        Multiloan_Params__c params = new Multiloan_Params__c();
        params.contract__c = contract.id;
        params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
        insert params;

        Test.startTest();
        BatchMultiloanFail job = new BatchMultiloanFail(1);
        database.executebatch(job, 1);
        Test.stopTest();
         
        Multiloan_Params_Logs__c log = [Select Id, Asset_Sale_Line_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                         WHERE Multiloan_Params__c =: params.Id];
            
        system.assertEquals(true, log.Asset_Sale_Line_Fail__c);
        
        Note n = [Select Id, CreatedDate, Title FROM Note WHERE ParentId =: contract.Id ORDER BY  CreatedDate DESC LIMIT 1 ]; 
        
        //System.assertEquals('Multi-loan Ineligibility Reasons', n.Title);
        
    }
    @isTest static void checks_ruleFail_for_StrategyType() {
        Contact con = LibraryTest.createContactTH();
        con.MailingState = 'PA';
        update con;
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Maximum_Loan_Amount__c = 4500;
        opp.Status__c = 'Funded';
        Opp.Type = 'Refinance';
        Opp.StrategyType__c = '2';
        Opp.Contact__c = con.Id;
        update opp;
        
        loan__Loan_Account__c contract = creates_contract();
        contract.Opportunity__c = opp.id;
        contract.loan__Loan_Amount__c = 4000;
        update contract;
        
        Multiloan_Params__c params = new Multiloan_Params__c();
        params.contract__c = contract.id;
        params.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-36);
        insert params;
        Test.startTest();
        BatchMultiloanFail job = new BatchMultiloanFail(1);
		database.executebatch(job, 1);
        Test.stopTest();
        
        Multiloan_Params_Logs__c log = [Select Id, Type_Fail__c, Multiloan_Params__c  FROM Multiloan_Params_Logs__c 
                                    WHERE Multiloan_Params__c =: params.Id];
        
        system.assertEquals(true, log.Type_Fail__c);

    }
    
   
    
    

}