global class DL_AccountStatementSF {

    @AuraEnabled public String requestCode { get; set; }
    @AuraEnabled public String customerIdentifier { get; set; }
    @AuraEnabled public String emailAddress { get; set; }
    @AuraEnabled public String institutionName { get; set; }
    @AuraEnabled public String accountName { get; set; }
    @AuraEnabled public String accountType { get; set; }
    @AuraEnabled public String accountNumberEntered { get; set; }
    @AuraEnabled public String accountNumberFoundClean { get; set; }
    @AuraEnabled public String accountNumberFound {
        get {
            return accountNumberFound;
        }
        set {
            accountNumberFound = value;
            accountNumberFoundClean = getNumericValue(getNumericValue(accountNumberFound));
        }
    }
    @AuraEnabled public String accountNumberConfidence { get; set; }
    @AuraEnabled public String nameEntered { get; set; }
    @AuraEnabled public String nameFound { get; set; }
    @AuraEnabled public String nameConfidence { get; set; }
    @AuraEnabled public String amountInput { get; set; }
    @AuraEnabled public String availableBalance { get; set; }
    @AuraEnabled public String averageBalance { get; set; }
    @AuraEnabled public String averageBalanceRecent { get; set; }
    @AuraEnabled public String isVerified { get; set; }
    @AuraEnabled public Integer negativeDays { get; set; }
    @AuraEnabled public Date asOfDateSimple { get; set; }
    @AuraEnabled public String lastMonthDatesMsg {
        get;
        set {
            if(value==null || String.isEmpty(value)){
                lastMonthDatesMsg = '* Last Month period not specified yet';
            }else{
                lastMonthDatesMsg = '* Last Month dates includes the period between '
                        + String.valueOf( Date.valueOf(value).addDays(-30) )
                        + ' and '
                        + String.valueOf( Date.valueOf(value).addDays(-1).addDays(1));
            }
        }
    }
    @AuraEnabled public String activityStartDate { get; set; }
    @AuraEnabled public String activityEndDate { get; set; }
    @AuraEnabled public String totalCredits { get; set; }
    @AuraEnabled public String totalDebits { get; set; }
    @AuraEnabled public String currentBalance { get; set; }
    @AuraEnabled public String isActivityAvailable { get; set; }
    @AuraEnabled public List<TransactionAnalysisSummaries> transactionAnalysisSummaries { get; set; }
    @AuraEnabled public List<TransactionSummaries> transactionSummaries {
        get;
        set{
            transactionSummaries = value;
            //negativeDays = getNegativeDays(transactionSummaries, asOfDate);
        }
    }
    @AuraEnabled public String asOfDate { get;
        set {
            asOfDate = value;
            lastMonthDatesMsg = asOfDate;
            if(!String.isEmpty(asOfDate)){
                asOfDateSimple = Date.valueOf(asOfDate);
            }
            //negativeDays = getNegativeDays(transactionSummaries, asOfDate);
        }
    }
    @AuraEnabled public String processedStatus { get; set; }
    @AuraEnabled public String isStarted { get; set; }
    @AuraEnabled public String isCompleted { get; set; }
    @AuraEnabled public String notes { get; set; }
    @AuraEnabled public String status { get; set; }
    @AuraEnabled public String statusText { get; set; }
    @AuraEnabled public String statusCodeColor { get; set; }
    @AuraEnabled public String lastRefreshErrorMessage { get; set; }
    @AuraEnabled public String isError { get; set; }
    @AuraEnabled public String errorMessage { get; set; }
    @AuraEnabled public String chartsId { get; set; }
    @AuraEnabled public List<AccountExpenses> accountExpenses { get; set; }
    @AuraEnabled public String loginText { get; set; }
    @AuraEnabled public String isLoginValid {
        get {
            return isLoginValid;
        }
        set {
            isLoginValid = value;
            loginText = Boolean.valueOf(isLoginValid == null ? 'false' : isLoginValid) ? 'Successful' : 'Unsuccessful';
        }
    }
    @AuraEnabled public String isACHSupported {
        get {
            return isACHSupported;
        }
        set {
            isACHSupported = value;
            setRoutingNumberEnteredText(this.routingNumberEntered, isACHSupported);
        }
    }
    @AuraEnabled public String routingNumberEnteredClean { get; set; }
    @AuraEnabled public String routingNumberEnteredText { get; set; }
    @AuraEnabled public String routingNumberEntered {
        get {
            return routingNumberEntered;
        }
        set {
            routingNumberEntered = value;
            setRoutingNumberEnteredText(routingNumberEntered, this.isACHSupported);
            this.routingNumberEnteredClean = '';
            if (routingNumberEntered != null)
                this.routingNumberEnteredClean = getNumericValue(routingNumberEntered);
        }
    }
    @AuraEnabled public String negativeDays30DayCount {
        get;
        set{
            negativeDays30DayCount = value;
            negativeDays = Integer.valueOf(negativeDays30DayCount);
        }
    }
    @AuraEnabled public String negativeDays60DayCount { get; set; }
    @AuraEnabled public String negativeDays90DayCount{ get; set; }

    public DL_AccountStatementSF(){
        availableBalance = '';
        asOfDateSimple = date.newinstance(1900, 1, 1);
        currentBalance = '';
        averageBalance = '';
        totalCredits = '';
        averageBalanceRecent = '';
        totalDebits = '';
        negativeDays = 0;
    }

    public void setRoutingNumberEnteredText(String routingNumberEntered, String isACHSupported) {

        this.routingNumberEnteredText = 'Not Specified';
        if (routingNumberEntered != null)
            this.routingNumberEnteredText = routingNumberEntered + ' ' + String.valueOf(Boolean.valueOf(isACHSupported == null ? 'false' : isACHSupported) == true ? '(ACH Supported)' : '(ACH Not Supported)');
    }

    public boolean matchesAccountNumberFound(String accountNumberToCompareWith) {

        try {
            //If either account is null then return false
            if (
                    (this.accountNumberFound != null && accountNumberToCompareWith != null) &&
                            //If account strings are equal then return true
                            (this.accountNumberFound.equals(accountNumberToCompareWith) ||

                                    (!String.isEmpty(getNumericValue(accountNumberToCompareWith)) &&

                                            //In case of leading zeros and cases like "54-987343", remove non-numeric characters in the accounts
                                            ((Decimal.valueOf(getNumericValue(this.accountNumberFound))
                                                    - Decimal.valueOf((getNumericValue(accountNumberToCompareWith)))) == 0) ) ||
                                    //In case DecisionLogic´s account looks like "6737" or "XXX-XX217-5"
                                    // and Salesforce´s account looks like "7434246737" or "2175" or "756482175"
                                    // check if Salesforce´s account contains DecisionLogic´s account
                                    getNumericValue(accountNumberToCompareWith)
                                            .contains(getNumericValue(this.accountNumberFound))
                            )
                    )
                return true;

        } catch (Exception e) {
            System.debug('Exception at DL_AccountStatementSF.matchesAccountNumberFound(' + accountNumberToCompareWith + '): ' + e);
        }

        //unaware of any other scenarios
        return false;
    }

    public boolean matchesEqualsAccountNumberFound(String accountNumberToCompareWith) {

        try{
            //If either account is null then return false
            if (
                    ( (this.accountNumberFound != null && accountNumberToCompareWith != null) &&
                            //If account strings are equal then return true
                            (this.accountNumberFound.equals(accountNumberToCompareWith)) ) ||
                            //In case of leading zeros and cases like "54-987343", remove non-numeric characters in the accounts
                            ( Decimal.valueOf(getNumericValue(this.accountNumberFound))
                                    - Decimal.valueOf(getNumericValue(accountNumberToCompareWith)) == 0 )
                    )
                return true;

        }catch(Exception e){
            System.debug('Exception at DL_AccountStatementSF.matchesAccountNumberFound(' + accountNumberToCompareWith + '): ' + e);
        }
        //unaware of any other scenarios
        return false;
    }

    public boolean matchesContainsAccountNumberFound(String accountNumberToCompareWith) {

        try{
            //If either account is null then return false
            if (    //In case DecisionLogic´s account looks like "6737" or "XXX-XX217-5"
                    // and Salesforce´s account looks like "7434246737" or "2175" or "756482175"
                    // check if Salesforce´s account contains DecisionLogic´s account
                    getNumericValue(accountNumberToCompareWith).contains(getNumericValue(this.accountNumberFound))
                    )
                return true;

        }catch(Exception e){
            System.debug('Exception at DL_AccountStatementSF.matchesAccountNumberFound(' + accountNumberToCompareWith + '): ' + e);
        }
        //unaware of any other scenarios
        return false;
    }

    public String getNumericValue(String accountNumberToClean) {

        String result = '';
        String character;
        for (Integer i = 0; i < accountNumberToClean.length(); i++) {
            if (accountNumberToClean.substring(i, i + 1).isNumeric())
                result = result + accountNumberToClean.substring(i, i + 1);
        }
        return result;

    }

    public Decimal stateTax {
        get {
            return stateTax;
        }
        set {
            stateTax = value;
            stateTax = stateTax != null ? stateTax.abs() : 0;
        }
    }
    public Decimal federalTax {
        get {
            return federalTax;
        }
        set {
            federalTax = value;
            federalTax = federalTax != null ? federalTax.abs() : 0;
        }
    }

    public Decimal defaultAmount{
        get { return defaultAmount; }
        set { defaultAmount= value;
            defaultAmount= defaultAmount!=null? defaultAmount.abs():0; }
    }
    public Decimal lowerTaxSlab{
        get { return lowerTaxSlab; }
        set { lowerTaxSlab= value;
            lowerTaxSlab= lowerTaxSlab!=null? lowerTaxSlab.abs(): 0; }
    }

    /**
* getNegativeDays:
*  From DecisionLogic´s report, take only the most recent month transactions.
*  That period of time is defined as:
*     from the most recent transaction date, all the way back 30 calendar days
*  Return the number of days where the End Of Day balance was negative.
* @param statement
* @return negativeDays number
*/
    public static Integer getNegativeDays(List<DL_AccountStatementSF.TransactionSummaries> txnList, String asOfDate){

        Integer negativeDaysCount = 0;

        //List<DL_AccountStatementSF.TransactionSummaries> txnList = statement.transactionSummaries;
        if(txnList==null || txnList.isEmpty())
            return negativeDaysCount;

        Integer txnPos = 0;
        Date currentTxnDate,
                previousTxnDate;
        Decimal endOfDayBalance;
        DL_AccountStatementSF.TransactionSummaries txn;

        try{

            //Most recent transaction
            txn = txnList.get(txnPos);
            endOfDayBalance = txn.runningBalance;
            if(endOfDayBalance<0)
                negativeDaysCount = negativeDaysCount + 1;
            previousTxnDate = Date.valueOf(txn.transactionDate);

            //Rest of transactions
            do{
                txnPos++;
                txn = txnList.get(txnPos);

                currentTxnDate = Date.valueOf(txn.transactionDate);

                if(currentTxnDate != previousTxnDate){

                    endOfDayBalance = txn.runningBalance;

                    if(endOfDayBalance<0)
                        negativeDaysCount = negativeDaysCount + 1;

                    previousTxnDate = Date.valueOf(txn.transactionDate);

                }


            }while(Date.valueOf(asOfDate).addDays(-30) <= currentTxnDate);

        }catch(Exception e){
            System.debug('Exception at getNegativeDays: ' + e);
            return 0;
        }

        return negativeDaysCount;
    }

    global class TransactionAnalysisSummaries {

        @AuraEnabled public String totalCount { get; set; }
        @AuraEnabled public String typeCode { get; set; }
        @AuraEnabled public Decimal totalAmount {
            get {
                return totalAmount;
            }
            set {
                totalAmount = value;
                totalAmount = totalAmount.abs();
            }
        }
        @AuraEnabled public String recentCount { get; set; }
        @AuraEnabled public String typeName { get; set; }
        @AuraEnabled public Decimal recentAmount {
            get {
                return recentAmount;
            }
            set {
                recentAmount = value;
                recentAmount = recentAmount.abs();
            }
        }
        @AuraEnabled public String popupLink { get; set; }

    }

    public Decimal stateTaxPrevYear{
        get { return stateTaxPrevYear; }
        set { stateTaxPrevYear= value;
            stateTaxPrevYear= stateTaxPrevYear!=null? stateTaxPrevYear.abs():0; }
    }
    public Decimal federalTaxPrevYear{
        get { return federalTaxPrevYear; }
        set { federalTaxPrevYear= value;
            federalTaxPrevYear= federalTaxPrevYear!=null? federalTaxPrevYear.abs(): 0; }
    }

    global class TransactionSummaries {

        @AuraEnabled public String isRefresh { get; set; }
        @AuraEnabled public String status { get; set; }
        @AuraEnabled public Decimal amount { get; set; }
        @AuraEnabled public String description { get; set; }
        @AuraEnabled public String category { get; set; }
        @AuraEnabled public String typeCodes { get; set; }
        @AuraEnabled public Date transactionDateShort { get; set; }
        @AuraEnabled public String transactionDate {
            get {
                return transactionDate;
            }
            set {
                transactionDate = value;
                this.transactionDateShort = Date.valueOf(transactionDate.substring(0, 10));
            }
        }
        @AuraEnabled public Decimal runningBalance { get; set; }

    }

    global class AccountExpenses {

        @AuraEnabled public String category { get; set; }
        @AuraEnabled public Decimal amount { get; set; }
        @AuraEnabled public Decimal percentage { get; set; }

    }

}