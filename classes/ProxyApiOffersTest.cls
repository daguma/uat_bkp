@isTest public class ProxyApiOffersTest {

    @testSetup static void setups_settings() {
        Date brmsDate = Date.newInstance(2017, 08, 21);

        BRMS_Configurable_Settings__c settings = new BRMS_Configurable_Settings__c();
        settings.Name = 'settings';
        settings.Do_Not_Call__c = false;
        settings.Milestone_Day__c = brmsDate;
        settings.Min_Rule_Count__c = 6;
        settings.Show_all_rules__c = true;
        upsert settings;
    }

    @isTest static void gets_offers() {
        Opportunity app = LibraryTest.createApplicationTH();

        Datetime dt = Datetime.newInstanceGmt(2017, 03, 03);
        Test.setCreatedDate(app.id, dt);

        Test.startTest();
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        ProxyApiOffers.offerInfo offers = new ProxyApiOffers.offerInfo();
        List<ProxyApiOfferCatalog> result = ProxyApiOffers.getOffers('B1', '80000',
                                            '5000', 'GA', 'LendingTree', 'Monthly', '700', 852462, '11', 'Fee On Top', 'CreditKarma',
                                            true, 'LP-0002', 'New', 'B1', 24, 0.1999, 5000, '1', app.Id, offers, 'isMrcState',
                                            'merchantState', 'bankName', 'Organic', false, '', String.valueOf(Datetime.now()), '', true,false);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @isTest static void gets_null_offers() {
        Opportunity app = LibraryTest.createApplicationTH();
        ProxyApiOffers.offerInfo offers = new ProxyApiOffers.offerInfo();
        List<ProxyApiOfferCatalog> result = ProxyApiOffers.getOffers('B1', '80000',
                                            '5000', 'GA', 'LendingTree', 'Monthly', '700', 852462, '11', 'Fee On Top', 'CreditKarma',
                                            true, 'LP-0002', 'New', 'B1', 24, 0.1999, 5000, '1', app.Id, offers, 'isMrcState',
                                            'merchantState', 'bankName', 'Organic', false, '', String.valueOf(Datetime.now()), '', false,false);
        System.assertEquals(null, result);
    }

    @isTest static void gets_offers_brms() {
        Opportunity app = LibraryTest.createApplicationTH();
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        ProxyApiOffers.offerInfo offers = new ProxyApiOffers.offerInfo();
        List<ProxyApiOfferCatalog> result = ProxyApiOffers.getOffers('B1', '80000',
                                            '5000', 'GA', 'LendingTree', 'Monthly', '700', 852462, '11', 'Fee On Top', 'CreditKarma',
                                            true, 'LP-0002', 'New', 'B1', 24, 0.1999, 5000, '1', app.Id, offers, 'isMrcState',
                                            'merchantState', 'bankName', 'Organic', false, '', String.valueOf(Datetime.now()), '', true,false);
        System.assertNotEquals(null, result);
    }

    @isTest static void gets_custom_offer() {
        Opportunity app = LibraryTest.createApplicationTH();
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        ProxyApiOffers.offerInfo offers = new ProxyApiOffers.offerInfo();
        offers.requestedType = 'customOffer';
        List<ProxyApiOfferCatalog> result = ProxyApiOffers.getOffers('B1', '80000',
                                            '5000', 'GA', 'LendingTree', 'Monthly', '700', 852462, '11', 'Fee On Top', 'CreditKarma',
                                            true, 'LP-0002', 'New', 'B1', 24, 0.1999, 5000, '1', app.Id, offers, 'isMrcState',
                                            'merchantState', 'bankName', 'Organic', false, '', String.valueOf(Datetime.now()), '', true,false);
        System.assertNotEquals(null, result);
    }

    @isTest static void gets_offers_recalculated_for_payment() {
        Opportunity app = LibraryTest.createApplicationTH();
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        ProxyApiOffers.offerInfo offers = new ProxyApiOffers.offerInfo();
        offers.requestedType = 'recalculateForPayment';
        List<ProxyApiOfferCatalog> result = ProxyApiOffers.getOffers('B1', '80000',
                                            '5000', 'GA', 'LendingTree', 'Monthly', '700', 852462, '11', 'Fee On Top', 'CreditKarma',
                                            true, 'LP-0002', 'New', 'B1', 24, 0.1999, 5000, '1', app.Id, offers, 'isMrcState',
                                            'merchantState', 'bankName', 'Organic', false, '', String.valueOf(Datetime.now()), '', true,false);
        System.assertNotEquals(null, result);
    }

    @isTest static void gets_offers_recalculated_for_principal() {
        Opportunity app = LibraryTest.createApplicationTH();
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        ProxyApiOffers.offerInfo offers = new ProxyApiOffers.offerInfo();
        offers.requestedType = 'recalculateForPrincipal';
        List<ProxyApiOfferCatalog> result = ProxyApiOffers.getOffers('B1', '80000',
                                            '5000', 'GA', 'LendingTree', 'Monthly', '700', 852462, '11', 'Fee On Top', 'CreditKarma',
                                            true, 'LP-0002', 'New', 'B1', 24, 0.1999, 5000, '1', app.Id, offers, 'isMrcState',
                                            'merchantState', 'bankName', 'Organic', false, '', String.valueOf(Datetime.now()), '', true,false);
        System.assertNotEquals(null, result);
    }

}