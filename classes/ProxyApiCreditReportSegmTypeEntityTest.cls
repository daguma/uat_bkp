@isTest public class ProxyApiCreditReportSegmTypeEntityTest {
    @isTest static void validate_instance() {
        Test.startTest();
        ProxyApiCreditReportSegmentTypeEntity ste = new ProxyApiCreditReportSegmentTypeEntity();
        Test.stopTest();

        System.assertEquals(null, ste.id);
        System.assertEquals(null, ste.fieldName);
        System.assertEquals(null, ste.length);
        System.assertEquals(null, ste.startPosition);
        System.assertEquals(null, ste.endPosition);
        System.assertEquals(null, ste.type);
        System.assertEquals(null, ste.description);
        System.assertEquals(null, ste.parentId);
        System.assertEquals(null, ste.isDisplayed);
        System.assertEquals(null, ste.isLengthValue);
        System.assertEquals(null, ste.isApplyLength);
    }

    @isTest static void validate_assign() {

        Test.startTest();
        ProxyApiCreditReportSegmentTypeEntity ste = new ProxyApiCreditReportSegmentTypeEntity();
        Test.stopTest();

        ste.id = 1;
        ste.fieldName = 'name test';
        ste.length = 10;
        ste.startPosition = 1;
        ste.endPosition = 5;
        ste.type = 'type test';
        ste.description = 'description test';
        ste.parentId = 1;
        ste.isDisplayed = true;
        ste.isLengthValue = true;
        ste.isApplyLength = true;

        System.assertEquals(1, ste.id);
        System.assertEquals('name test', ste.fieldName);
        System.assertEquals(10, ste.length);
        System.assertEquals(1, ste.startPosition);
        System.assertEquals(5, ste.endPosition);
        System.assertEquals('type test', ste.type);
        System.assertEquals('description test', ste.description);
        System.assertEquals(1, ste.parentId);
        System.assertEquals(true, ste.isDisplayed);
        System.assertEquals(true, ste.isLengthValue);
        System.assertEquals(true, ste.isApplyLength);
    }

}