public with sharing class RefinancePostSoftPull {

    public static final String ELIGIBILITY_CREDIT_REPORT_QUERY =  'SELECT Id, Contract__c, ' +
            ' FICO__c, Grade__c, ' +
            ' New_FICO__c, New_Grade__c, ' +
            ' Eligible_For_Refinance__c, ' +
            ' IsLowAndGrow__c, ' +
            ' Last_Refinance_Eligibility_Review__c, ' +
            ' Contract__r.loan__Principal_Remaining__c, ' +
            ' Contract__r.loan__Fees_Remaining__c, ' +
            ' Contract__r.loan__Interest_Accrued_Not_Due__c, ' +
            ' Contract__r.loan__Pay_Off_Amount_As_Of_Today__c, ' +
            ' System_Debug__c, ' +
            ' Eligible_For_Soft_Pull_Params__c, ' +
            ' Eligible_For_Soft_Pull__c ' +
            ' FROM loan_Refinance_Params__c ' +
            ' WHERE Eligible_For_Soft_Pull_Params__c != null ' +
            ' AND Eligible_For_Soft_Pull__c = true';

    /**
    * [RefinancePostSoftPull description]
    * @param contactList [description]
    */
    public static void executeProcess(String contractId, Loan_Refinance_Params__c refinanceParams) {

        loan__Loan_Account__c contract = getContract(contractId);
        Contact c = new Contact();
        Boolean newAppSubmitted = false;

        if (contract != null) {

            String appId = '';
            Opportunity newApp = null;
            Opportunity lastApp = getLastRefinanceOpportunity(contract);
            if (lastApp != null) {
                appId = lastApp.Id;
                newApp = lastApp;
            }

            if (appId == '') {
                SubmitApplicationService submit = new SubmitApplicationService();
                appId = submit.submitApplication(contract.loan__Contact__c);
                newApp = getApplication(appId);
                newAppSubmitted = true;
                newApp.Contract_Renewed__c = contractId;
                newApp.Type =  'Refinance';
                newApp.Fee_Handling__c = 'Fee On Top';
                newApp.Partner_Account__c = (OfferCtrlUtil.runningInASandbox() || Test.isRunningTest()) ? [SELECT Id FROM Account WHERE Name = 'Refinance' LIMIT 1].Id : '0010B00001z7hKPQAY'; //Add a validation for sandbox, to avoid exception
                newApp.LeadSource__c = 'Refinance';
                newApp.Lead_Sub_Source__c = (refinanceParams.IsLowAndGrow__c ? 'Refinance Low and Grow' : 'Refinance');
            }

            for (Opportunity fundedApp : [SELECT Estimated_Grand_Total__c
                                          FROM Opportunity
                                          WHERE  Contact__c = : contract.loan__Contact__c
                                                  AND Status__c IN ('Funded')
                                                  ORDER BY CreatedDate DESC
                                                  LIMIT 1]) {

                if (fundedApp.Estimated_Grand_Total__c != null) {
                    c.id = contract.loan__Contact__c;
                    c.Annual_Income__c = fundedApp.Estimated_Grand_Total__c;
                    update c;
                }
            }

            if (newApp != null && newApp.Is_Selected_Offer__c == false) {
                Decimal loanAmount = (((contract.loan__Principal_Remaining__c +
                                        contract.loan__Fees_Remaining__c +
                                        contract.loan__Interest_Accrued_Not_Due__c) / 100)
                                      .round(System.RoundingMode.CEILING)) * 100;
                newApp.Amount = loanAmount > 1000 ? loanAmount : 1000;
            }

            newApp.Next_ACH_Date__c = contract.loan__ACH_Next_Debit_Date__c;
            update newApp;
            if (!String.isEmpty(appId)) {
                /*Long primaryCreditReportId = null;
                if (newAppSubmitted)
                    primaryCreditReportId = 0;
                else {
                    if (newApp.PrimaryCreditReportId__c == null) {
                        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getByEntityId(newApp.Id);
                        primaryCreditReportId = creditReportEntity.Id;
                    } else
                        primaryCreditReportId = Long.valueOf(String.valueOf(newApp.PrimaryCreditReportId__c));
                }*/
                refinanceParams.Eligible_For_Soft_Pull_Params__c =  contract.Opportunity__c + ',' + appId + ',' + contractId + ',' + newApp.Is_Selected_Offer__c; // + ',' + primaryCreditReportId;
                update refinanceParams;
                //checkEligibility(contract.Opportunity__c, appId, contractId);

            }
        }
    }

    public static void eligibilityCheckGetCR(Loan_Refinance_Params__c refinanceParams) {

        try {
            LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
            Decimal newFICO = 0;
            Boolean isEligible = false;
            Scorecard__c newScorecard = null;
            Offer__c oldOffer = null;

            String oldAppId = refinanceParams.Eligible_For_Soft_Pull_Params__c.split(',').get(0);
            String newAppId = refinanceParams.Eligible_For_Soft_Pull_Params__c.split(',').get(1);
            String contractId =  refinanceParams.Eligible_For_Soft_Pull_Params__c.split(',').get(2);
            String isSelectedOffer = refinanceParams.Eligible_For_Soft_Pull_Params__c.split(',').get(3);
            String creditReportData = SalesForceUtil.GetLatestValidCreditReportId(newAppId, true);
            String creditReportId = creditReportData.split(',').get(0);
            String sequenceId = creditReportData.split(',').get(1);

            System.debug('creditReportData ' + creditReportData);
            refinanceParams.Eligible_For_Soft_Pull_Params__c = null;
            refinanceParams.Eligible_For_Soft_Pull__c = false;
            Loan_Refinance_Params_Logs__c refinanceParamsLogs = new Loan_Refinance_Params_Logs__c();

            if (sequenceId != '1' && isSelectedOffer == 'false') {
                ProxyApiCreditReportEntity creditReportEntity = null;
                if (sequenceId == '10' && creditReportId != null) {
                    creditReportEntity = CreditReport.get(newAppId, false, true, true, null, false, Long.valueOf(creditReportId));
                } else {
                    creditReportEntity = CreditReport.get(newAppId, false, true, true);
                }

                Opportunity newApp = getApplication(newAppId);

                if (creditReportEntity != null && creditReportEntity.errorMessage == null && creditReportEntity.brmsApproved) {

                    newScorecard = ScorecardUtil.getScorecard(newAppId);
                    newFICO = creditReportEntity.getDecimalAttribute(CreditReport.FICO);

                    if (refinanceParams != null && newScorecard != null) {

                        if (!refinanceParams.IsLowAndGrow__c) {
                            if ( (isFicoApproved(refinanceParams.FICO__c, newFICO) &&
                                    isGradeApproved(refinanceParams.Grade__c, newScorecard.Grade__c)) ||
                                    isBetterGrade(refinanceParams.Grade__c, newScorecard.Grade__c, refinanceParams.IsLowAndGrow__c) )
                                isEligible = true;
                            else
                                refinanceParamsLogs.Post_Pull_FICO_Grade_Fail__c = true;
                        } else {
                            if (isGradeApproved(refinanceParams.Grade__c, newScorecard.Grade__c) && ((!isBetterFicoLowAndGrow(refinanceParams.FICO__c, newFICO) &&
                                    isBetterGrade(refinanceParams.Grade__c, newScorecard.Grade__c, refinanceParams.IsLowAndGrow__c)) || isBetterFicoLowAndGrow(refinanceParams.FICO__c, newFICO))) {

                                if (isPayoffApproved(newScorecard.Grade__c, refinanceParams.Contract__r.loan__Pay_Off_Amount_As_Of_Today__c)) {
                                    isEligible = true;
                                } else {
                                    refinanceParamsLogs.Payoff_Fail__c = true;
                                }
                            } else {
                                refinanceParamsLogs.Post_Pull_FICO_Grade_Fail__c = true;
                            }
                        }

                        List<Offer__c> newOffers = getOffers(newAppId);

                        Decimal maxOfferAmount = newApp.Maximum_Offer_Amount__c;
                        Decimal tmpMaxAmount = 0;
                        for (Offer__c offr : newOffers) {
                            if (offr.Opportunity__r.Fee_Handling__c == null || offr.Opportunity__r.Fee_Handling__c == FeeUtil.NET_FEE_OUT) {
                                maxOfferAmount = offr.Total_Loan_Amount__c;
                            } else {
                                maxOfferAmount = offr.Loan_Amount__c;
                            }
                            if (maxOfferAmount > tmpMaxAmount) {
                                tmpMaxAmount = maxOfferAmount;
                            }
                        }
                        newApp.Maximum_Offer_Amount__c = tmpMaxAmount;

                        //RD-303: If Payoff is Greater Than the Offer Amount then it is not Eligible.
                        refinanceParamsLogs.Max_Loan_Amount_Check_Fail__c = isPayoffGTOfferAmount(refinanceParams.Contract__r.loan__Pay_Off_Amount_As_Of_Today__c, newApp);
                        if (refinanceParamsLogs.Max_Loan_Amount_Check_Fail__c) { isEligible = false; }

                        if (isEligible) {
                            Boolean hasBetterGrade = !refinanceParams.Grade__c.equalsIgnoreCase(newScorecard.Grade__c);
                            oldOffer = getSelectedOffer(oldAppId);
                            //List<Offer__c> newOffers = getOffers(newAppId);
                            EligibilityCheckPostSoftPull.getRefinanceOffers(oldOffer, newOffers, hasBetterGrade, newScorecard.Grade__c);
                        }
                    }
                } else {
                    System.debug('Soft Pull Decline - update');
                    refinanceParamsLogs.Soft_Pull_Decline__c = true;
                }

                refinanceParams.New_FICO__c = newFICO;
                refinanceParams.New_Grade__c = newScorecard != null ? newScorecard.Grade__c : '-';
                refinanceParams.Eligible_For_Refinance__c = isEligible;

                //Opportunity newApp = getApplication(newAppId);

                System.debug('Final Eligible: ' + isEligible);

                if (isEligible) {

                    if ( String.isEmpty(newApp.Status__c) ||
                            newApp.Status__c == 'Aged / Cancellation' ||
                            newApp.Status__c == 'Refinance Unqualified' ) {
                        newApp.Status__c = 'Refinance Qualified';
                        if (newApp.First_Refi_Qualified_Date__c == null) {
                            newApp.Sub__c = 'Brand New RQ';
                            newApp.First_Refi_Qualified_Date__c = System.now();
                        }
                    }
                    insertNoteOldOffer(newAppId, oldAppId, oldOffer, contractId);
                    if (newApp.Dialer_Status__c != 'ContactDNC') newApp.Dialer_Status__c  = null;
                    newApp.Last_CQ_Date__c = Date.today();
                    newApp.Reason_of_Opportunity_Status__c = null;
                } else {
                    Loan_Refinance_Params_Logs__c lastRefiParalogs  = [SELECT id, Soft_Pull_Decline__c, Post_Pull_FICO_Grade_Fail__c, Payoff_Fail__c, Max_Loan_Amount_Check_Fail__c
                            FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c = :refinanceParams.Id ORDER BY CreatedDate DESC LIMIT 1];

                    String oldValuesParamsLogs = lastRefiParalogs.Payoff_Fail__c + '|' + lastRefiParalogs.Post_Pull_FICO_Grade_Fail__c + '|' + lastRefiParalogs.Soft_Pull_Decline__c + '|' + lastRefiParalogs.Max_Loan_Amount_Check_Fail__c;

                    String newValuesParamsLogs = refinanceParamsLogs.Payoff_Fail__c + '|' + refinanceParamsLogs.Post_Pull_FICO_Grade_Fail__c + '|' + refinanceParamsLogs.Soft_Pull_Decline__c + '|' + refinanceParamsLogs.Max_Loan_Amount_Check_Fail__c;

                    if (lastRefiParalogs == null || newValuesParamsLogs != oldValuesParamsLogs) {
                        if (lastRefiParalogs != null) {
                            lastRefiParalogs.To__c = Date.today();
                            update lastRefiParalogs;
                        }
                        refinanceParamsLogs.Loan_Refinance_Params__c = refinanceParams.Id;
                        refinanceParamsLogs.To__c = null;
                        insert refinanceParamsLogs;
                    }
                    newApp.Status__c = 'Refinance Unqualified';
                }

                update newApp;

            } // sequenceId = '1'

        } catch (Exception e) {
            System.Debug(e.getMessage() + ' ' + e.getStackTraceString());
            refinanceParams.System_Debug__c += '\n' + e.getMessage() + '\n' + e.getStackTraceString();
        }

        update refinanceParams;
    }

    public static String getOldRefinanceAppOwner(String contactId, String newAppId) {

        String ownerId = '';
        LP_Custom__c lpCustom = CustomSettings.getLPCustom();
        List<Id> appIds = new List<Id>();
        appIds.add(newAppId);


        for (Opportunity app : [ SELECT Id, OwnerId
                                 FROM Opportunity
                                 WHERE Contact__c = : contactId
                                         AND Type = 'Refinance'
                                                 AND Id NOT IN : appIds
                                                 AND OwnerId != : lpCustom.WEB_API_User_Id__c
                                                 AND OwnerId != '005U0000003rJ4JIAU'
                                                 ORDER BY CreatedDate DESC LIMIT 1]) {
            if ([SELECT COUNT() FROM User WHERE Id = : app.OwnerId AND IsActive = true] > 0)
                ownerId = app.OwnerId;
        }
        return ownerId;
    }

    public static void insertNoteOldOffer (String parentId, String oldAppId, Offer__c oldOffer, String contractId) {
        if (oldOffer == null || String.isEmpty(parentId) || String.isEmpty(oldAppId)) return;

        loan__Loan_Account__c contract = getContract(contractId);

        String appName = 'Id: ' + oldAppId;
        for (Opportunity app : [SELECT id, Name FROM Opportunity WHERE Id = : oldAppId]) {
            appName = app.Name;
        }
        Note newNote = new Note ();
        newNote.parentid = parentId;
        newNote.title = 'Offers in Funded Application [' + appName + ']';
        NewNote.body =  'Interest Rate: ' + String.valueOf(oldOffer.Interest_Rate__c) + '%\n' +
                        'Term: ' + String.valueOf(oldOffer.Term__c) + '\n' +
                        'Repayment Frequency: ' + String.valueof(oldOffer.Repayment_Frequency__c) + '\n' +
                        'Installments: ' + String.valueOf(oldOffer.Installments__c) + '\n' +
                        'Payment Amount: ' + SalesForceUtil.formatMoney(String.valueof(oldOffer.Payment_Amount__c.format())) + '\n' +
                        'Disbursal Amount: ' + SalesForceUtil.formatMoney(String.valueOf(oldOffer.Customer_Disbursal__c.format())) + '\n' +
                        'Fee: ' + SalesForceUtil.formatMoney(String.valueof(oldOffer.fee__c.format())) + '\n' +
                        'Effective APR: ' + String.valueof(oldOffer.Effective_APR__c) + '%\n' +
                        'Current Due Day: ' + String.valueof(contract.loan__Due_Day__c) + '\n';
        insert newNote;
    }

    public static Boolean isFicoApproved(Decimal previousFico, Decimal newFico) {
        // SM-336
        // RD-101 Changing New FICO base value to custom setting
        Refinance_Settings__c refinanceSettings = Refinance_Settings__c.getValues('settings');
        return ( (newFico >= Integer.valueOf(refinanceSettings.FICO_Approved__c)) && (newFico >= previousFico) );
    }

    public static Boolean isBetterFicoLowAndGrow(Decimal previousFico, Decimal newFico) {
        Refinance_Settings__c refinanceSettings = Refinance_Settings__c.getValues('settings');
        return ( (newFico >= Integer.valueOf(refinanceSettings.FICO_Approved__c)) && (newFico > previousFico) );
    }

    public static Boolean isPayoffApproved(String newGrade, Decimal payoff) {//RD-260
        Boolean result = false;
        Refinance_Settings__c refinanceSettings = Refinance_Settings__c.getValues('settings');

        if (newGrade.equalsIgnoreCase('A1')) {
            result = payoff < refinanceSettings.LGA1__c;
        } else if (newGrade.equalsIgnoreCase('A2')) {
            result = payoff < refinanceSettings.LGA2__c;
        } else if (newGrade.equalsIgnoreCase('B1')) {
            result = payoff < refinanceSettings.LGB1__c;
        } else if (newGrade.equalsIgnoreCase('B2')) {
            result = payoff < refinanceSettings.LGB2__c;
        } else if (newGrade.equalsIgnoreCase('C1')) {
            result = payoff < refinanceSettings.LGC1__c;
        }
        return result;
    }
    public static Boolean isBetterGrade(String previousGrade, String newGrade, Boolean isLowAndGrow) {

        Boolean result = false;

        if (previousGrade.equalsIgnoreCase('A1')) {
            result = newGrade.equalsIgnoreCase(previousGrade);
        } else if (previousGrade.equalsIgnoreCase('A2')) {
            result = newGrade.equalsIgnoreCase(previousGrade) ||
                     newGrade.equalsIgnoreCase('A1');
        } else if (previousGrade.equalsIgnoreCase('B1')) {
            result = newGrade.equalsIgnoreCase(previousGrade) ||
                     newGrade.equalsIgnoreCase('A2') ||
                     newGrade.equalsIgnoreCase('A1');
        } else if (previousGrade.equalsIgnoreCase('B2')) {
            result = newGrade.equalsIgnoreCase(previousGrade) ||
                     newGrade.equalsIgnoreCase('B1') ||
                     newGrade.equalsIgnoreCase('A2') ||
                     newGrade.equalsIgnoreCase('A1');
        } else if (previousGrade.equalsIgnoreCase('C1')) {
            result = (newGrade.equalsIgnoreCase(previousGrade) && !isLowAndGrow ) ||
                     newGrade.equalsIgnoreCase('B2') ||
                     newGrade.equalsIgnoreCase('B1') ||
                     newGrade.equalsIgnoreCase('A2') ||
                     newGrade.equalsIgnoreCase('A1');
        } else if (previousGrade.equalsIgnoreCase('C2') ||
                   previousGrade.equalsIgnoreCase('D') ||
                   previousGrade.equalsIgnoreCase('E') ||
                   previousGrade.equalsIgnoreCase('F')) {
            result = (newGrade.equalsIgnoreCase('C1') && !isLowAndGrow ) ||
                     newGrade.equalsIgnoreCase('B2') ||
                     newGrade.equalsIgnoreCase('B1') ||
                     newGrade.equalsIgnoreCase('A2') ||
                     newGrade.equalsIgnoreCase('A1');
        }

        return result;
    }

    public static Boolean isGradeApproved(String previousGrade, String newGrade) {
        // SM-336
        return (newGrade.equalsIgnoreCase('C1') ||
                newGrade.equalsIgnoreCase('B2') ||
                newGrade.equalsIgnoreCase('B1') ||
                newGrade.equalsIgnoreCase('A2') ||
                newGrade.equalsIgnoreCase('A1'));
    }

    /**
    * [getContract description]
    * @param  contractId [description]
    * @return            [description]
    */
    public static loan__Loan_Account__c getContract(String contractId) {

        loan__Loan_Account__c result = null;

        if (!String.isEmpty(contractId)) {
            for (loan__Loan_Account__c contract : [SELECT Id, loan__Contact__c, Opportunity__c,
                                                   loan__Principal_Remaining__c,
                                                   loan__Fees_Remaining__c,
                                                   loan__Interest_Accrued_Not_Due__c,
                                                   loan__Due_Day__c,
                                                   loan__ACH_Next_Debit_Date__c
                                                   FROM loan__Loan_Account__c
                                                   WHERE Id = : contractId]) {
                result = contract;
            }
        }

        return result;
    }

    /**
    * [getRefinanceParams description]
    * @param  contractId [description]
    * @return            [description]
    */
    public static loan_Refinance_Params__c getRefinanceParams(String contractId) {

        loan_Refinance_Params__c result = null;

        if (!String.isEmpty(contractId)) {
            for (loan_Refinance_Params__c params : [SELECT Id, Contract__c,
                                                    FICO__c, Grade__c,
                                                    New_FICO__c, New_Grade__c,
                                                    Eligible_For_Refinance__c,
                                                    Last_Refinance_Eligibility_Review__c,
                                                    Contract__r.loan__Principal_Remaining__c,
                                                    Contract__r.loan__Fees_Remaining__c,
                                                    Contract__r.loan__Interest_Accrued_Not_Due__c,
                                                    System_Debug__c
                                                    FROM loan_Refinance_Params__c
                                                    WHERE Contract__c = : contractId]) {
                result = params;
            }
        }

        return result;
    }


    /**
    * [getApplication description]
    * @param  appId [description]
    * @return       [description]
    */
    public static Opportunity getApplication(String appId) {

        Opportunity result = null;

        if (!String.isEmpty(appId)) {
            for (Opportunity app : [SELECT Id, Name, Type, OwnerId,
                                    Fee_Handling__c, Amount, Dialer_Status__c,
                                    Status__c, Contract_Renewed__c, Last_CQ_Date__c,
                                    Contact__c, Partner_Account__c,
                                    LeadSource__c, Lead_Sub_Source__c,
                                    Next_ACH_Date__c, First_Refi_Qualified_Date__c, PrimaryCreditReportId__c,
                                    Maximum_Loan_Amount__c, Maximum_Offer_Amount__c, Is_Selected_Offer__c
                                    FROM Opportunity
                                    WHERE Id = : appId]) {
                result = app;
            }
        }

        return result;
    }

    /**
    * [getOffers description]
    * @param  appId [description]
    * @return       [description]
    */
    public static List<Offer__c> getOffers(String appId) {

        List<Offer__c> result = new List<Offer__c>();

        if (!String.isEmpty(appId)) {
            for (Offer__c offer : [SELECT Id,
                                   Repayment_Frequency__c,
                                   APR__c,
                                   Term__c,
                                   Payment_Amount__c,
                                   IsSelected__c,
                                   Effective_APR__c,
                                   Loan_Amount__c,
                                   Total_Loan_Amount__c,
                                   Fee__c,
                                   IsCustom__c,
                                   Fee_Percent__c,
                                   FirstPaymentDate__c,
                                   Installments__c,
                                   Opportunity__c,
                                   RefinanceFundedOffer__c,
                                   Customer_Disbursal__c,
                                   Interest_Rate__c,
                                   Opportunity__r.Fee_Handling__c
                                   FROM Offer__c
                                   WHERE Opportunity__c = : appId]) {
                result.add(offer);
            }
        }

        return result;
    }

    /**
    * [getOffers description]
    * @param  appId [description]
    * @return       [description]
    */
    public static Offer__c getSelectedOffer(String appId) {

        Offer__c result = new Offer__c();

        if (!String.isEmpty(appId)) {
            for (Offer__c offer : [SELECT Id,
                                   Repayment_Frequency__c,
                                   APR__c,
                                   Term__c,
                                   Payment_Amount__c,
                                   IsSelected__c,
                                   Effective_APR__c,
                                   Loan_Amount__c,
                                   Total_Loan_Amount__c,
                                   Fee__c,
                                   IsCustom__c,
                                   Fee_Percent__c,
                                   FirstPaymentDate__c,
                                   Installments__c,
                                   Opportunity__c,
                                   Customer_Disbursal__c,
                                   Interest_Rate__c
                                   FROM Offer__c
                                   WHERE Opportunity__c = : appId
                                           AND IsSelected__c = true
                                                   LIMIT 1]) {
                result = offer;
            }
        }

        return result;
    }

    public static Opportunity getLastRefinanceOpportunity(loan__Loan_Account__c contract) {
        Opportunity foundApp;

        Opportunity lastApp = [SELECT CreatedDate
                               FROM Opportunity
                               WHERE  Contact__c = : contract.loan__Contact__c
                                       AND Type != Null
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1];

        for (Opportunity lastValidRefiApp : [SELECT Id, Name, Type, OwnerId,
                                             Fee_Handling__c, Amount,
                                             Status__c, Contract_Renewed__c,
                                             Contact__c,
                                             Reason_of_Opportunity_Status__c, PrimaryCreditReportId__c,
                                             Is_Selected_Offer__c
                                             FROM Opportunity
                                             WHERE  Contact__c = : contract.loan__Contact__c
                                                     AND Type = 'Refinance'
                                                             AND Status__c NOT IN ('Funded')
                                                             AND CreatedDate = :lastApp.CreatedDate //MAINT-1494
                                                                     ORDER BY CreatedDate DESC
                                                                     LIMIT 1]) {
            foundApp = lastValidRefiApp;
        }

        return foundApp;
    }

    private static Boolean isPayoffGTOfferAmount(Decimal payOffAmount, Opportunity newApp) {
        if ( (newApp.Maximum_Loan_Amount__c == null && newApp.Maximum_Offer_Amount__c == null) || payOffAmount == null) {
            return true;
        } else {
            if ( newApp.Maximum_Offer_Amount__c != null ) {
                if ( payOffAmount > newApp.Maximum_Offer_Amount__c) {
                    return true;
                }
            } else {
                if ( payOffAmount > newApp.Maximum_Loan_Amount__c) {
                    return true;
                }
            }
        }
        return false;
    }

}