@isTest(SeeAllData = true)

public class SubmitApplicationServiceTest {

    @isTest static void submits_app_from_contact() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c = [SELECT id, name FROM Contact WHERE id = : app.Contact__c LIMIT 1];

        Campaign camp = new Campaign();
        camp.UW_Bureau__c = 'Experian';
        camp.Name = 'Campaign Test';
        insert camp;

        CampaignMember nCam = new CampaignMember();
        nCam.CampaignID = camp.Id;
        nCam.ContactId = c.Id;
        nCam.Status = 'Open';
        insert nCam;

        String result = SubmitApplicationService.submitAppFromContact(c.id);

        System.assertEquals('Application has been submitted', result);

        Boolean resultB = SubmitApplicationService.doBridgerContactHit(c.id);

        System.assertEquals(true, resultB);

        //resultB = SubmitApplicationService.verifyBridger(c.id);
        resultB = SubmitApplicationService.verifyWatchDog(c.id);
        
        SubmitApplicationService.checkAndGetExistanceByContact(c.id, 'LendingPoint');
  
        System.assertEquals(true, resultB);

        resultB = SubmitApplicationService.checkExistanceByContact(c.id);

        //System.assertEquals(false, resultB);

        Contact newC = LibraryTest.createContactTH();

        resultB = SubmitApplicationService.checkExistanceByContact(newC.id);

        System.assertNotEquals(null, resultB);

        /*result = SubmitApplicationService.submitAppFromContact('0');

        System.assertEquals('Error while submitting Application.', result);*/
    }

    @isTest static void submits_app_from_contact_with_account() {

        Opportunity app = LibraryTest.createApplicationTH();

        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'TransUnion';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        acc.Default_Payment_Frequency__c = '28 days';
        acc.Partner_Sub_Source__c = 'Test';
        acc.Type = 'Partner';
        acc.Is_Parent_Partner__c = true;
        insert acc;

        Contact c = [SELECT id, name FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c.Lead_Sub_Source__c = 'Test';
        c.AccountId = acc.id;
        update c;

        BridgerWDSFLookup__c bridger = new BridgerWDSFLookup__c();
        bridger.review_status__c = 'No match';
        bridger.Contact__c = c.id;
        bridger.Lookup_By__c = 'test';
        bridger.Name_Searched__c = 'test';
        bridger.Number_of_Hits__c = 0;
        insert bridger;

        String result = SubmitApplicationService.submitAppFromContact(c.id);

        System.assertEquals('Application has been submitted', result);

        acc.Bureau__c = 'Experian';
        update acc;

        result = SubmitApplicationService.submitAppFromContact(c.id);

        System.assertEquals('Application has been submitted', result);

        Boolean resultB = SubmitApplicationService.doBridgerContactHit(c.id);

        System.assertEquals(true, resultB);

        resultB = SubmitApplicationService.verifyBridger(c.id);

        System.assertEquals(true, resultB);

        result = SubmitApplicationService.submitAppFromContact(c.id);

        System.assertEquals('Application has been submitted', result);
    }

    @isTest static void gets_payment_frequency_30_days() {
        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'Experian';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        acc.Partner_Sub_Source__c = 'Test';
        acc.Type = 'Partner';
        acc.Is_Parent_Partner__c = true;
        acc.Default_Payment_Frequency__c = '30 days';
        insert acc;

        String result = SubmitApplicationService.getPaymentFrequency('30 days');

        System.assertEquals('M', result);
    }

    @isTest static void gets_payment_frequency_28_days() {
        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'Experian';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        acc.Partner_Sub_Source__c = 'Test';
        acc.Type = 'Partner';
        acc.Is_Parent_Partner__c = true;
        acc.Default_Payment_Frequency__c = '28 days';
        insert acc;

        String result = SubmitApplicationService.getPaymentFrequency('28 Days');

        System.assertEquals('T', result);
    }

    @isTest static void gets_payment_frequency_Bi_Weekly() {
        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'Experian';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        acc.Partner_Sub_Source__c = 'Test';
        acc.Type = 'Partner';
        acc.Is_Parent_Partner__c = true;
        acc.Default_Payment_Frequency__c = 'Bi-weekly';
        insert acc;

        String result = SubmitApplicationService.getPaymentFrequency('Bi-weekly');

        System.assertEquals('B', result);
    }

    @isTest static void gets_payment_frequency_Monthly() {
        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'Experian';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        acc.Partner_Sub_Source__c = 'Test';
        acc.Type = 'Partner';
        acc.Is_Parent_Partner__c = true;
        acc.Default_Payment_Frequency__c = 'Monthly';
        insert acc;

        String result = SubmitApplicationService.getPaymentFrequency('Monthly');

        System.assertEquals('M', result);
    }

    @isTest static void gets_payment_frequency_Semi_Monthly() {
        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'Experian';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        acc.Partner_Sub_Source__c = 'Test';
        acc.Type = 'Partner';
        acc.Is_Parent_Partner__c = true;
        acc.Default_Payment_Frequency__c = 'Semi-Monthly';
        insert acc;

        String result = SubmitApplicationService.getPaymentFrequency('Semi-Monthly');

        System.assertEquals('S', result);
    }

    @isTest static void gets_payment_frequency_Weekly() {
        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'Experian';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        acc.Partner_Sub_Source__c = 'Test';
        acc.Type = 'Partner';
        acc.Is_Parent_Partner__c = true;
        acc.Default_Payment_Frequency__c = 'Weekly';
        insert acc;

        String result = SubmitApplicationService.getPaymentFrequency('Weekly');

        System.assertEquals('W', result);
    }

    @isTest static void gets_bureau_ex() {
        Contact c = LibraryTest.createContactTH();

        Campaign camp = new Campaign();
        camp.UW_Bureau__c = 'Experian';
        camp.Name = 'Campaign Test';
        insert camp;

        CampaignMember nCam = new CampaignMember();
        nCam.CampaignID = camp.Id;
        nCam.ContactId = c.Id;
        nCam.Status = 'Open';
        insert nCam;

        String result = SubmitApplicationService.getBureau('Partner', c.Id);

        System.assertEquals('EX', result);
    }

    @isTest static void gets_bureau_tu() {
        Contact c = LibraryTest.createContactTH();

        Campaign camp = new Campaign();
        camp.UW_Bureau__c = 'TransUnion';
        camp.Name = 'Campaign Test';
        insert camp;

        CampaignMember nCam = new CampaignMember();
        nCam.CampaignID = camp.Id;
        nCam.ContactId = c.Id;
        nCam.Status = 'Open';
        insert nCam;

        String result = SubmitApplicationService.getBureau('Partner', c.Id);

        System.assertEquals('TU', result);
    }

    @isTest static void verifies_Bridger_with_contact() {

        Contact c = LibraryTest.createContactTH();

        Boolean resultB = SubmitApplicationService.verifyBridger(c.id);

        System.assertEquals(true, resultB);
    }

    @isTest static void changeAppstatusTest() {

        Opportunity lApps = LibraryTest.createApplicationTH();
        
        List<Opportunity> statusApps = new List<Opportunity>();

        statusApps = [Select Id, CreatedDate, Status__c, Contact__c From Opportunity
                      Where Id = : lApps.Id order By CreatedDate Desc];

        SubmitApplicationService.changeAppstatus(statusApps);
    }
    
   /* @isTest static void getPaymentFrequencyTypeTest() {
        string result = SubmitApplicationService.getPaymentFrequencyType('30 days');
        System.assertEquals('Monthly', result);
        
        string resultOne = SubmitApplicationService.getPaymentFrequencyType('Weekly');
        System.assertEquals('Weekly', resultOne );
        
        string resultTwo = SubmitApplicationService.getPaymentFrequencyType('');
        System.assertEquals('28 Days', resultTwo );
    } */
}