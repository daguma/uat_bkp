public class DL_UpdateActivityByUserReport
{
	public DL_UpdateActivityByUserReport()
	{
		DL_ActivityByUserReport result;
		try
		{            
			//Get report from Proxy database
			string jsonResponse = ProxyApiUtil.ApiGetResult('/v101/decision-logic/get-activity-user-report/', '');
			if(Test.isRunningTest())
			{
				jsonResponse = '{"reportLines":[{"salesforceUserId":"005U0000005kJqbIAE","total":85,"not_Started":85,"started_not_Completed":0,"bank_Error":0,"account_Error":0,"login_not_Verified":0,"login_Verified":0,"dateRange":365}]}';
			}
			System.debug('jsonResponse: ' + jsonResponse);
            
			//Deserialize Proxy database report into Salesforce object DL_ActivityByUserReport
			result = (DL_ActivityByUserReport)JSON.deserializeStrict(jsonResponse, DL_ActivityByUserReport.class);
		}
		catch(Exception e)
		{
			// Process exception here
			System.debug('First part exception: ' + e.getStackTraceString());
		}
        
		if(result != null)
		{
			List<DL_ActivityByUserReport.ActivityByUserReportLine> reportLines = result.reportLines;
			System.debug('reportLines: ' + reportLines);
			//Insert new report lines
			for(DL_ActivityByUserReport.ActivityByUserReportLine reportLine: reportLines)
			{
				if(reportLine.salesforceUserId != 'Web')
				{
					try
					{
						DL_ActivityByUserReport__c cd = new  DL_ActivityByUserReport__c();
						cd.user__c = reportLine.salesforceUserId;
						cd.total__c = reportLine.total;
						cd.not_Started__c = reportLine.not_Started;
						cd.Started_Not_Completed__c = reportLine.started_not_Completed;
						cd.Bank_Error__c = reportLine.bank_Error;
						cd.Account_Error__c = reportLine.account_Error;
						cd.Login_Not_Verified__c = reportLine.login_not_Verified;
						cd.Login_Verified__c = reportLine.login_Verified;
						cd.dateRange__c = reportLine.dateRange;
						System.debug('Report line: ' + cd);
						insert cd; 
					}
					catch(Exception e)
					{
                        // Process exception here
					}
				}
			}
		}
	}
}