global class UniversalNotesCtrl {

    public String dialogHTML { get; set; }

    public String domainUri {
        get
        {
            return System.Url.getSalesforceBaseURL().toExternalForm() + '/';
        }
    }

    public String recordId {get; set;}
    public List<Note> consolidatedNotes {get; set;}
    private String contactId {get; set;}
    private List<Opportunity> applicationIds {get; set;}
    private List<loan__Loan_Account__c> loanContractIds {get; set;}
    private List<Member_Services_Notes__c> memberServiceNotesIds {get; set;}
    private List<Member_Services_Notes__c> memberServiceNotes {get; set;}
    private List<Id> allIDs = new List<Id>();


    public void initialize() {

        if (recordId == null || recordId == '')
            recordId = ApexPages.currentPage().getParameters().get('id');

        String objectName = findObjectNamaByRecordId(recordId);


        if (objectName == 'Opportunity')
            contactId = [SELECT Contact__c FROM Opportunity WHERE Id = : recordId].Contact__c;
        else if (objectName == 'loan__Loan_Account__c')
            contactId = [SELECT loan__Contact__c FROM loan__Loan_Account__c WHERE Id = : recordId].loan__Contact__c;
        else if (objectName == 'Member_Services_Notes__c')
            contactId = [SELECT Contact__c FROM Member_Services_Notes__c WHERE Id = : recordId].Contact__c;
        else
            contactId = recordId;

        getRelatedNotesOld();

        dialogHTML = parseForLayout();

    }

    Webservice static String getAllNotes(String thisId) {
        UniversalNotesCtrl u = new UniversalNotesCtrl();
        u.recordId = thisId;
        u.initialize();
        return u.dialogHTML;
    }

    private void getRelatedNotesOld() {

        applicationIds = [SELECT Id FROM Opportunity WHERE Contact__c = : contactId];
        loanContractIds = [SELECT Id FROM loan__Loan_Account__c WHERE loan__Contact__c = : contactId];
        memberServiceNotesIds = [SELECT Id FROM Member_Services_Notes__c WHERE Contact__c = : contactId];
        allIDs.add(contactId);
        allIDs.addAll(new List<Id>(new Map<Id, Opportunity>(applicationIds).keySet()));
        allIDs.addAll(new List<Id>(new Map<Id, loan__Loan_Account__c>(loanContractIds).keySet()));
        allIDs.addAll(new List<Id>(new Map<Id, Member_Services_Notes__c>(memberServiceNotesIds).keySet()));

        System.debug('allIDs: ' + allIDs);

        consolidatedNotes = [SELECT Id,
                             Title,
                             ParentId,
                             Parent.Name,
                             CreatedDate,
                             LastModifiedDate,
                             CreatedBy.FirstName,
                             CreatedBy.LastName
                             FROM    Note
                             WHERE   ParentId in: allIDs
                             ORDER BY CreatedDate DESC];
    }

    public String parseForLayout() {
        String divHeader = '<div id="universalNotesPopup" title="Universal Notes"><div id="universalNotesContent"><table>';
        String divTableHeaders = '<thead>' +
                                 '<th>Actions</th>' +
                                 '<th>Title</th>' +
                                 '<th>Note Parent</th>' +
                                 '<th>Related To</th>' +
                                 '<th>Creation Date</th>' +
                                 '<th>Last Modification Date</th>' +
                                 '<th>Created By</th>' +
                                 '</thead>';
        String divTableRows = '<tbody>';
        String divTableFooter = '</table></div></div>';

        for (Member_Services_Notes__c n : [SELECT Id, Name, Title__c, CreatedBy.FirstName, CreatedBy.LastName, CreatedDate, LastModifiedDate FROM Member_Services_Notes__c WHERE   Id in: allIDs ORDER BY CreatedDate DESC]) {

            ApexPages.StandardController cont = new ApexPages.StandardController(n);
            String toEdit = domainUri + String.valueOf(cont.edit()).remove('System.PageReference[/');
            List<String> toEditList = toEdit.split('retURL');
            toEdit = toEditList.get(0) + 'retURL=%2F' + recordId;

            String toDelete = domainUri + String.valueOf(cont.delete()).remove('System.PageReference[/').remove('retURL=%2F002]') + 'retURL=%2F' + recordId;
            String createdByName = n.CreatedBy.FirstName == null && n.CreatedBy.LastName == 'HAL' ? 'HAL' : n.CreatedBy.FirstName + ' ' + n.CreatedBy.LastName;

            divTableRows += '<tr>';
            divTableRows += '<td><a class="actionLink" href="' + toEdit + '">Edit</a> | <a href="' + toDelete + '" class="actionLink" onclick="return confirmDelete();">Delete</a></td>';
            divTableRows += '<td><a href="' + domainUri + n.Id + '" target="_blank">' + n.Title__c + '</a></td>';
            divTableRows += '<td>' + getParentObjNameById(n.Id) + '</td>';
            divTableRows += '<td><a href="' + domainUri + n.Id + '" target="_blank">' + n.Name + '</a></td>';
            divTableRows += '<td>' + n.CreatedDate.format('MM/dd/yyyy h:mm a', 'EST') + '</td>';
            divTableRows += '<td>' + n.LastModifiedDate.format('MM/dd/yyyy h:mm a', 'EST') + '</td>';
            divTableRows += '<td>' + createdByName + '</td>';
            divTableRows += '</tr>';

        }

        for (Note n : consolidatedNotes) {
            ApexPages.StandardController cont = new ApexPages.StandardController(n);
            String toEdit = domainUri + String.valueOf(cont.edit()).remove('System.PageReference[/');
            List<String> toEditList = toEdit.split('retURL');
            toEdit = toEditList.get(0) + 'retURL=%2F' + recordId;

            String toDelete = domainUri + String.valueOf(cont.delete()).remove('System.PageReference[/').remove('retURL=%2F002]') + 'retURL=%2F' + recordId;
            String createdByName = n.CreatedBy.FirstName == null && n.CreatedBy.LastName == 'HAL' ? 'HAL' : n.CreatedBy.FirstName + ' ' + n.CreatedBy.LastName;

            divTableRows += '<tr>';
            divTableRows += '<td><a class="actionLink" href="' + toEdit + '">Edit</a> | <a href="' + toDelete + '" class="actionLink" onclick="return confirmDelete();">Delete</a></td>';
            divTableRows += '<td><a href="' + domainUri + n.Id + '" target="_blank">' + n.Title + '</a></td>';
            divTableRows += '<td>' + getParentObjNameById(n.ParentId) + '</td>';
            divTableRows += '<td><a href="' + domainUri + n.ParentId + '" target="_blank">' + n.Parent.Name + '</a></td>';
            divTableRows += '<td>' + n.CreatedDate + '</td>';
            divTableRows += '<td>' + n.LastModifiedDate + '</td>';
            divTableRows += '<td>' + createdByName + '</td>';
            divTableRows += '</tr>';
        }

        divTableRows += '</tbody>';

        return divHeader + divTableHeaders + divTableRows + divTableFooter;
    }


    @AuraEnabled
    public static List<UniversalNotes> getRelatedNotes(String recordId) {

        List<UniversalNotes> universalNotes = new List<UniversalNotes>();
        List<SObject> allNotes = new List<SObject>();

        if (String.isNotBlank(recordId)) {
            String contactId = '';

            Set<Id> allIDs = new Set<Id>();

            String objectName = findObjectNamaByRecordId(recordId);

            if (objectName == 'Opportunity')
                contactId = [SELECT Contact__c FROM Opportunity WHERE Id = : recordId].Contact__c;
            else if (objectName == 'loan__Loan_Account__c')
                contactId = [SELECT loan__Contact__c FROM loan__Loan_Account__c WHERE Id = : recordId].loan__Contact__c;
            else if (objectName == 'Member_Services_Notes__c')
                contactId = [SELECT Contact__c FROM Member_Services_Notes__c WHERE Id = : recordId].Contact__c;
            else
                contactId = recordId;

            allIDs = getAllIds(contactId);
            System.debug(allIDs.size());

            allNotes.addAll([SELECT Id, Name, Title__c, CreatedBy.FirstName, CreatedBy.LastName, Contact__c, CreatedDate, LastModifiedDate FROM Member_Services_Notes__c WHERE Id in: allIDs ORDER BY CreatedDate DESC]);
            allNotes.addAll([SELECT Id, Title, ParentId, Parent.Name, CreatedDate, LastModifiedDate, CreatedBy.FirstName, CreatedBy.LastName FROM Note WHERE ParentId in: allIDs ORDER BY CreatedDate DESC]);
            System.debug(allNotes.size());

            if (allNotes.size() > 0) {
                for (Sobject note : allNotes) {
                    UniversalNotes uniNote = new UniversalNotes();
                    uniNote.setNoteObj(note);
                    universalNotes.add(uniNote);
                }
            }
        }

        return universalNotes;
    }

    public static String findObjectNamaByRecordId(String recordId) {
        String objectName = '';
        try {
            //Get prefix from record ID
            //This assumes that you have passed at least 3 characters
            String myIdPrefix = String.valueOf(recordId).substring(0, 3);

            //Get schema information
            Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe();

            //Loop through all the sObject types returned by Schema
            for (Schema.SObjectType stype : gd.values()) {
                Schema.DescribeSObjectResult r = stype.getDescribe();
                String prefix = r.getKeyPrefix();

                //Check if the prefix matches with requested prefix
                if (prefix != null && prefix.equals(myIdPrefix)) {
                    objectName = r.getName();
                    System.debug('Object Name! ' + objectName);
                    break;
                }
            }
        } catch (Exception e) {
            System.debug(e);
        }
        return objectName;
    }

    public static Set<Id> getAllIds(String contactId) {

        Set<Id> allIDs = new Set<Id>();
        Map<Id, SObject> noteMap = new Map<Id, SObject>();

        noteMap.putAll(new Map<Id, SObject>([SELECT Id FROM Opportunity WHERE Contact__c = : contactId]));
        noteMap.putAll(new Map<Id, SObject>([SELECT Id FROM loan__Loan_Account__c WHERE loan__Contact__c = : contactId]));
        noteMap.putAll(new Map<Id, SObject>([SELECT Id FROM Member_Services_Notes__c WHERE Contact__c = : contactId]));

        allIDs.add(contactId);
        allIDs.addAll(noteMap.keySet());

        return allIDs;
    }

    public static String getParentObjNameById(String recordId) {
        String objName = findObjectNamaByRecordId(recordId);
        if (objName == 'Opportunity')
            return 'Application';
        else if (objName == 'loan__Loan_Account__c')
            return 'CL Contract';
        else if (objName == 'Note')
            return 'Note';
        else if (objName == 'Member_Services_Notes__c')
            return 'Member Services Notes';
        else
            return 'Contacts';
    }

    public Class UniversalNotes {
        @AuraEnabled public Sobject noteObj { get; set; }
        @AuraEnabled public String parentObj { get; set; }
        @AuraEnabled public String createdName { get; set; }

        public void setNoteObj(SObject note) {
            this.noteObj = note;
            String objectName = getParentObjNameById((String) note.get('Id'));
            if (objectName == 'Note') this.parentObj = getParentObjNameById((String) note.get('ParentId'));
            if (objectName == 'Member Services Notes') this.parentObj = getParentObjNameById((String) note.get('Contact__c'));

            this.createdName = (String) note.getSobject('CreatedBy').get('FirstName') == null &&
            (String) note.getSobject('CreatedBy').get('LastName') == 'HAL' ? 'HAL' : note.getSobject('CreatedBy').get('FirstName') + ' ' + note.getSobject('CreatedBy').get('LastName');
        }
    }

}