@isTest public class DecisioningCtrlTest {

    @isTest static void validate_initialize() {

        LibraryTest.createBrmsConfigSettings(-1, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        insert ProxyApiUtil.settings;

        TestHelper.createEmailAgeSettings();
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7', Enable_Finwise__c = false,
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;

        opportunity app = LibraryTest.createApplicationTH();
        app.Email_Age_Manual_Attempt__c = true;
        update app;

        String jsonMockResponse = TestHelper.sendEmailAgeMockFailResponse(app.Id);

        EmailAgeStorageController.saveEmailAgeDetails(TestHelper.sendEmailAgeMockFailResponse(app.id));

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Acceptance__c = 'Y';
        score.High_Risk__c = true;
        score.opportunity__c = app.id;
        insert score;

        DecisioningCtrl decisioning = new DecisioningCtrl(app.Id);
        PageReference pr = decisioning.initialize();

        System.assertEquals(null, pr);
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        System.debug(msgs);
        System.assert(decisioning.hasWarning, 'Warning not set');

    }

    @isTest static void fetch_new_soft_pull_brms() {

        LibraryTest.createBrmsConfigSettings(-1, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7', Enable_Finwise__c = false,
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;

        opportunity app = LibraryTest.createApplicationTH();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        String jsonMockResponse = TestHelper.sendEmailAgeMockFailResponse(app.Id);

        EmailAgeStorageController.saveEmailAgeDetails(TestHelper.sendEmailAgeMockFailResponse(app.id));

        decisioning.genesisApp = app;

        decisioning.fetchNewSoftCreditPull();
    }

    @isTest static void fetch_new_soft_pull() {
        LibraryTest.createBrmsConfigSettings(5, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7', Enable_Finwise__c = false,
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;

        opportunity app = LibraryTest.createApplicationTH();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        decisioning.genesisApp = app;
        decisioning.fetchNewSoftCreditPull();
    }

    @isTest static void fetch_new_hard_pull() {

        LibraryTest.createBrmsConfigSettings(-1, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7', Enable_Finwise__c = false,
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;

        opportunity app = LibraryTest.createApplicationTH();
        app.Review_Reason__c = 'Test';
        update app;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        decisioning.genesisApp = app;

        decisioning.fetchNewHardCreditPull();
    }

    @isTest static void overrides_disqualifier() {

        LibraryTest.createBrmsConfigSettings(0, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();


        opportunity app = LibraryTest.createApplicationTH();
        app.Term__c = 24;
        update app;

        Offer__c off = new Offer__c();
        off.Opportunity__c = app.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        insert off;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        decisioning.genesisApp = app;

        decisioning.overrideDisqualifier();

        app = [select id, Status__c, Contact__c, Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : app.id];

        System.assertEquals('Credit Qualified', app.Status__c);


    }

    @isTest static void assigns_notes() {
        LibraryTest.createBrmsConfigSettings(-1, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        opportunity app = LibraryTest.createApplicationTH();

        ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        Note newNote = new Note();
        decisioning.notes = newNote;

        System.assertEquals(newNote, decisioning.notes);
    }

    @isTest static void is_meridian() {

        LibraryTest.createBrmsConfigSettings(-1, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        TestHelper.createEmailAgeSettings();

        opportunity app = LibraryTest.createApplicationTH();

        Test.setCreatedDate(app.id, Date.newInstance(2016, 05, 10));

        Test.startTest();

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);
        LP_Custom__c lpCustom = LP_Custom__c.getOrgDefaults();
        lpCustom.DecisioningTab_Restriction_By_ProfileId__c = 'N/A';
        PageReference pr = decisioning.initialize();

        Test.stopTest();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 2) : 0;
        if (!decisioning.hasCRInformation) System.assert((msgs.get(index)).getDetail() == 'Showing Pre-ATB Meridian Information! Click on the Soft Credit Pull button to retrieve a new one from ATB.');
        // Test is not good since Library Test is now returning valid test credit report.
//        System.assertEquals(false, decisioning.hasCRInformation);
    }


    @isTest static void fetch_new_email_age_result() {

        LibraryTest.createBrmsConfigSettings(0, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();


        TestHelper.createEmailAgeSettings();

        opportunity app = LibraryTest.createApplicationTH();

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        decisioning.fetchNewEmailAgeResult();
    }

    @isTest static void fetch_new_email_age_result_two() {

        LibraryTest.createBrmsConfigSettings(0, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        opportunity app = LibraryTest.createApplicationTH();

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        decisioning.displayOldView =  false;

        decisioning.fetchNewEmailAgeResult();
    }

    @isTest static void sort_on_rule_priority() {

        LibraryTest.createBrmsConfigSettings(0, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();
        list<DisplayGDSResponseWrapper> gdsList = new list<DisplayGDSResponseWrapper>();

        DisplayGDSResponseWrapper gds = new DisplayGDSResponseWrapper();
        gds.showSoftPullEmptyBox = false;
        gds.showHardPullEmptyBox = false;
        gds.ruleName = 'test';
        gds.ruleNumber = '1';
        gds.softPullRuleValue = '123';
        gds.deQueueAssignment = 'level 1';
        gds.colorCode = '';
        gds.isSoftPass = true;
        gds.isHardPass = false;
        gds.actionInstructions = 'test';
        gds.rulePriority = 123;
        gds.approvedBy = 'system';
        gds.sequenceCategory = '4';
        gdsList.add(gds);

        opportunity app = LibraryTest.createApplicationTH();

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        decisioning.sortOnRulePriority(gdsList);
    }

    @isTest static void generate_display_list() {

        LibraryTest.createBrmsConfigSettings(0, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        opportunity app = LibraryTest.createApplicationTH();

        TestHelper.createEmailAgeSettings();
        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        decisioning.automatedRuleDecision = 'Yes';
        decisioning.latestIdoResStr = 'test';
        decisioning.latestPayfnResStr = 'test';
        decisioning.latestKountScr = 'test';
        decisioning.crBrmsAutomatedDecision = 'Yes';
        decisioning.crBrmsApprovedByName = 'system';
        decisioning.attchmtRefresh = false;
        decisioning.appStatus = 'test';

        String softPullResponse = LibraryTest.fakeBrmsRulesResponse(app.Id, true, '1', true);
        ProxyApiCreditReportEntity cr = (ProxyApiCreditReportEntity) JSON.deserializeStrict(softPullResponse, ProxyApiCreditReportEntity.class);

        String hardPullResponse = LibraryTest.fakeBrmsRulesResponseHPextra(app.Id, false, '3', true);
        ProxyApiCreditReportEntity crHard = (ProxyApiCreditReportEntity) JSON.deserializeStrict(hardPullResponse, ProxyApiCreditReportEntity.class);

        List<DisplayGDSResponseWrapper> gdsList = decisioning.generateDisplayList(cr, crHard);

        System.assertEquals(true, gdsList.size() > 0);

        String softPullResponsefail = LibraryTest.fakeBrmsFailRulesResponse(app.Id, true, '1', true);
        ProxyApiCreditReportEntity cr1 = (ProxyApiCreditReportEntity) JSON.deserializeStrict(softPullResponsefail , ProxyApiCreditReportEntity.class);

        String hardPullResponsefail = LibraryTest.fakeBrmsFailRulesResponse(app.Id, false, '3', true);
        ProxyApiCreditReportEntity crHard1 = (ProxyApiCreditReportEntity) JSON.deserializeStrict(hardPullResponsefail , ProxyApiCreditReportEntity.class);

        List<DisplayGDSResponseWrapper> gdsList1 = decisioning.generateDisplayList(cr1, crHard1);

        System.assertEquals(true, gdsList1.size() > 0);
    }

    @isTest static void display_gds_response() {

        LibraryTest.createBrmsConfigSettings(0, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        opportunity app = LibraryTest.createApplicationTH();


        TestHelper.createEmailAgeSettings();
        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        String softPullResponse = LibraryTest.fakeBrmsRulesResponse(app.Id, true, '1', true);
        decisioning.cr = (ProxyApiCreditReportEntity) JSON.deserializeStrict(softPullResponse, ProxyApiCreditReportEntity.class);

        String hardPullResponse = LibraryTest.fakeBrmsRulesResponse(app.Id, false, '3', true);
        decisioning.crHard = (ProxyApiCreditReportEntity) JSON.deserializeStrict(hardPullResponse, ProxyApiCreditReportEntity.class);

        decisioning.displayGDSResponse(true);
    }


    @isTest static void show_all() {

        LibraryTest.createBrmsConfigSettings(0, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7', Enable_Finwise__c = false,
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;

        opportunity app = LibraryTest.createApplicationTH();

        Test.startTest();

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        PageReference pr = decisioning.showAll();

        PageReference pr1 = decisioning.showAllRules();

        Test.stopTest();
    }

    @isTest static void show_less() {

        LibraryTest.createBrmsConfigSettings(0, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7', Enable_Finwise__c = false,
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;

        opportunity app = LibraryTest.createApplicationTH();

        Test.startTest();

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        PageReference pr = decisioning.showLess();

        Test.stopTest();
    }

    @isTest static void show_rules_on_tab() {

        LibraryTest.createBrmsConfigSettings(0, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();

        TestHelper.createEmailAgeSettings();

        opportunity app = LibraryTest.createApplicationTH();

        Test.startTest();

        boolean ruleShow = DecisioningCtrl.showRulesOnTab('Decision', '4');

        System.assertEquals(true, ruleShow);

        Test.stopTest();
    }

    @isTest static void fetch_last_kyc_results() {

        LibraryTest.createBrmsConfigSettings(0, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        opportunity app = LibraryTest.createApplicationTH();

        List<contact> conLst = VerifyPhoneNumberHelper.getContactRec(app.contact__c);
        Contact con = conLst[0];

        Payfone_Settings__c payfoneSet = LibraryTest.createPayfoneSettings('test1', 'test2', 'test3');
        String response = LibraryTest.payfoneResponse(con.Phone, 700, 90, 90, 90, 90, 90, true);
        con = VerifyPhoneNumberHelper.parseVerifyResult(response, con);

        String payfoneresult = VerifyPhoneNumberHelper.decisionUsingPayfone(con);

        VerifyPhoneNumberHelper.InsertPayfoneHistory(con, con.Phone, 'Success', 'Pass', 'token request', response);

        IDology_Request__c requestst = new IDology_Request__c();
        requestst.Name = 'Test';
        requestst.Result_Message__c = '';
        requestst.Contact__c = con.Id ;
        insert requestst;


        Logging__c log = new Logging__c();
        log.contact__c = app.Contact__c;
        log.Webservice__c = 'KOUNT';
        log.api_response__c = 'PASS SCOR=9';
        insert log;

        Test.startTest();


        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        decisioning.fetchLastKYCResults();

        Test.stopTest();
    }

    @isTest static void validate_initialize_brms() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7', Enable_Finwise__c = false,
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;

        opportunity app = LibraryTest.createApplicationTH();
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        String jsonMockResponse = TestHelper.sendEmailAgeMockFailResponse(app.Id);

        EmailAgeStorageController.saveEmailAgeDetails(TestHelper.sendEmailAgeMockFailResponse(app.id));
        //EmailAgeStorageController.storeEmailAgeInSfObject(TestHelper.sendEmailAgeMockFailResponse(app.id),app.genesis__Contact__c,app.Id,false);

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        PageReference pr = decisioning.initialize();

        System.assertEquals(null, pr);

//        System.assertEquals(false, decisioning.hasAllCRInformation);

        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"DEV_Default_URL_New__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Acceptance__c = 'Y';
        score.High_Risk__c = true;
        score.opportunity__c = app.id;
        insert score;

        //controller = new ApexPages.StandardController(app);

        decisioning = new DecisioningCtrl(app.id);

        System.assertEquals(true, decisioning.isHighRisk);

        pr = decisioning.initialize();

    }

    @isTest static void gets_note_information() {

        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        TestHelper.createEmailAgeSettings();

        opportunity app = LibraryTest.createApplicationTH();

        //ApexPages.StandardController controller = new ApexPages.StandardController(app);

        DecisioningCtrl decisioning = new DecisioningCtrl(app.id);

        Note nt1 = new Note();

        Note nt = decisioning.getNoteInformation();
        System.assertEquals(nt1, nt);

        Integer countNotes = [SELECT COUNT() FROM Note];

        nt.title = 'Test';
        nt.body = 'Test Body';

        decisioning.SaveNote();

        System.assert([SELECT COUNT() FROM Note] > countNotes, 'Save Note');
    }
}