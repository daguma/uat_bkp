public with sharing class BrmsRulesWrapper {
    @AuraEnabled public String ruleName {get;set;}
    @AuraEnabled public boolean rulePass {get;set;}    
    @AuraEnabled public string ruleValue {get; set;}
    @AuraEnabled public String ruleNumber {get;set;}
}