@isTest public class ProxyApiScorecardEntityTest {

    @isTest static void validate() {

        ProxyApiScorecardEntity pase = new ProxyApiScorecardEntity();
        pase.annualIncome = 1.1;
        pase.dtiRatio = 1.1;
        pase.selfEmployed = 1.1;
        pase.creditScore = 1.1;
        pase.inquiries = 1.1;
        pase.totalTradeLines = 1.1;
        pase.highestBalance = 1.1;
        pase.totalOpenInstallments = 1.1;
        pase.logit = 1.1;
        pase.unitBadProb = 1.1;
        pase.dollarRateBadProb = 1.1;
        pase.maximumPaymentAmount = 1.1;
        pase.grade = 'D';
        pase.acceptance = 'N';

        System.assertEquals(1.1, pase.annualIncome);
        System.assertEquals(1.1, pase.dtiRatio);
        System.assertEquals(1.1, pase.selfEmployed);
        System.assertEquals(1.1, pase.creditScore);
        System.assertEquals(1.1, pase.inquiries);
        System.assertEquals(1.1, pase.totalTradeLines);
        System.assertEquals(1.1, pase.highestBalance);
        System.assertEquals(1.1, pase.totalOpenInstallments);
        System.assertEquals(1.1, pase.logit);
        System.assertEquals(1.1, pase.unitBadProb);
        System.assertEquals(1.1, pase.dollarRateBadProb);
        System.assertEquals(1.1, pase.maximumPaymentAmount);
        System.assertEquals('D', pase.grade);
        System.assertEquals('N', pase.acceptance);

    }
}