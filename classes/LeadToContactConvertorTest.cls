@isTest public class LeadToContactConvertorTest {

    @istest static void testLeadConversion() {
        //LibraryTest.createGenesisOrgParametersTH();
//        LibraryTest.createMLCustomSettingsTH();
        LibraryTest th = new LibraryTest();
        //th.createSeedDataForTestTH();
        th.createLendingProductName('Term Loan');
        LibraryTest.createLPCustom('Term Loan', 'XXXSN');

        LP_Custom__c lp = [select id, Email_Alert_Portal_Apps__c from LP_Custom__c limit 1];
        lp.Email_Alert_Portal_Apps__c = 'test@gmail.com';
        update lp;


        Lead l = LibraryTest.createLeadTH();

        Logging__c log = new Logging__c();
        log.Lead__c = l.id;
        log.API_Request__c = 'Test';
        insert log;

        contact c = LibraryTest.createContactTH();
        c.lead__c = l.id;
        update c;

        String reply = LeadToContactConvertor.LeadConversion(l.Id);

        System.debug('TestConvertLeadToContact.testLEadConversion Normal: ' + reply);

        l = LibraryTest.createLeadUniqueTH();

        c = LibraryTest.createContactTH();
        c.lead__c = l.id;
        update c;

        if (c.AccountId == null) {
            Account a = new Account();
            a.Name = 'Test2';
            a.Billingstreet = l.street;
            a.BillingCity = l.City;
            a.BillingState = l.state;
            a.BillingPostalCode = l.Postalcode;
            a.BillingCountry = l.Country;
            a.Phone = l.Phone;
            a.Type = 'Customer';

            insert a;

            c.AccountId = a.Id;
            update c;
        }
        reply = LeadToContactConvertor.LeadConversionWeb(l.Id);

        System.debug('TestConvertLeadToContact.testLEadConversion Web: ' + reply);
        
        LeadToContactConvertor.SendLeadEmail(l);

        //SubmitApplicationService.checkExistanceByLead(l.Id);
        SubmitApplicationService.submitAppFromContact('contactID');
        SubmitApplicationService.submitAppFromContact(c.Id);
        SubmitApplicationService.doWatchDogContactHit(c.Id);

        System.debug(SubmitApplicationService.checkExistanceByContact(c.Id));
        

    }

    @istest static void testLeadFutureTest() {
        //LibraryTest.createGenesisOrgParametersTH();
        Lead l = LibraryTest.createLeadTH();
        Contact c = LibraryTest.createContactTH();
        c.lead__c = l.id;
        c.AccountId = null;
        update c;

        LeadToContactConvertor.doLeadConversion(l.Id, null, c.Id);

    }
   
     @istest static void testContactConversionMinIncome() {
        LibraryTest th = new LibraryTest();
        th.createLendingProductName('Term Loan');
        LibraryTest.createLPCustom('Term Loan', 'XXXSN');

        LP_Custom__c lp = [select id, Email_Alert_Portal_Apps__c from LP_Custom__c limit 1];
        lp.Email_Alert_Portal_Apps__c = 'test@gmail.com';
        update lp;

        contact c = LibraryTest.createContactTH();
c.Annual_Income__c = 5000;
         update c;
        String reply = LeadToContactConvertor.LeadConversion(c.Id);

        System.debug('TestConvertLeadToContact.testLEadConversion Normal: ' + reply);

        
    }
   
    
    @istest static void testContactConversion() {
        LibraryTest th = new LibraryTest();
        th.createLendingProductName('Term Loan');
        LibraryTest.createLPCustom('Term Loan', 'XXXSN');

        LP_Custom__c lp = [select id, Email_Alert_Portal_Apps__c from LP_Custom__c limit 1];
        lp.Email_Alert_Portal_Apps__c = 'test@gmail.com';
        update lp;

        contact c = LibraryTest.createContactTH();

        String reply = LeadToContactConvertor.LeadConversion(c.Id);

        System.debug('TestConvertLeadToContact.testLEadConversion Normal: ' + reply);

        
    }
    
    @istest static void after_Lead_Insert_Handler_Test() {
        LibraryTest th = new LibraryTest();
        th.createLendingProductName('Term Loan');
        LibraryTest.createLPCustom('Term Loan', 'XXXSN');

        LP_Custom__c lp = [select id, Email_Alert_Portal_Apps__c from LP_Custom__c limit 1];
        lp.Email_Alert_Portal_Apps__c = 'test@gmail.com';
        update lp;

        contact c = LibraryTest.createContactTH();
        c.Lead_Sub_Source__c = 'LPCUSTOMERPORTAL';
        update c;
        
        LeadToContactConvertor.SendLeadEmail(c);

        List<contact> cons = new List<Contact> ();
        cons.add(c);
        LeadToContactConvertor.afterLeadInsertHandler(cons,cons);
  
    }
    
    @istest static void testLeadMinIncome() {
        //LibraryTest.createGenesisOrgParametersTH();
//        LibraryTest.createMLCustomSettingsTH();
        LibraryTest th = new LibraryTest();
        //th.createSeedDataForTestTH();
        th.createLendingProductName('Term Loan');
        LibraryTest.createLPCustom('Term Loan', 'XXXSN');

        LP_Custom__c lp = [select id, Email_Alert_Portal_Apps__c from LP_Custom__c limit 1];
        lp.Email_Alert_Portal_Apps__c = 'test@gmail.com';
        update lp;


        Lead l = LibraryTest.createLeadTH();

        Logging__c log = new Logging__c();
        log.Lead__c = l.id;
        log.API_Request__c = 'Test';
        insert log;

        contact c = LibraryTest.createContactTH();
        c.lead__c = l.id;
        c.Annual_Income__c = 1000;
        c.Lead_Sub_Source__c = 'LPCUSTOMERPORTAL';
        update c;

        String reply = LeadToContactConvertor.LeadConversion(l.Id);

        System.debug('TestConvertLeadToContact.testLeadMinIncome Normal: ' + reply);


    }


}