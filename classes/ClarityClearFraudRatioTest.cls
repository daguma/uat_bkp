@isTest
public class ClarityClearFraudRatioTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearFraudRatio ccf = new ClarityClearFraudRatio();
        Test.stopTest();
        
        system.assertEquals(null, ccf.name );
        system.assertEquals(null, ccf.one_minute_ago );
        system.assertEquals(null, ccf.ten_minutes_ago );
        system.assertEquals(null, ccf.one_hour_ago );
        system.assertEquals(null, ccf.twentyfour_hours_ago );
        system.assertEquals(null, ccf.seven_days_ago );
        system.assertEquals(null, ccf.fifteen_days_ago );
        system.assertEquals(null, ccf.thirty_days_ago );
        system.assertEquals(null, ccf.ninety_days_ago );
        system.assertEquals(null, ccf.threesixtyfive_days_ago );
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearFraudRatio ccf = new ClarityClearFraudRatio();
        Test.stopTest();
        
        ccf.name = 'test';
        ccf.one_minute_ago = 1;
        ccf.ten_minutes_ago = 1;
        ccf.one_hour_ago = 1;
        ccf.twentyfour_hours_ago = 1;
        ccf.seven_days_ago = 1;
        ccf.fifteen_days_ago = 1;
        ccf.thirty_days_ago = 1;
        ccf.ninety_days_ago = 1;
        ccf.threesixtyfive_days_ago = 1;
        
        system.assertEquals('test', ccf.name );
        system.assertEquals(1, ccf.one_minute_ago );
        system.assertEquals(1, ccf.ten_minutes_ago );
        system.assertEquals(1, ccf.one_hour_ago );
        system.assertEquals(1, ccf.twentyfour_hours_ago );
        system.assertEquals(1, ccf.seven_days_ago );
        system.assertEquals(1, ccf.fifteen_days_ago );
        system.assertEquals(1, ccf.thirty_days_ago );
        system.assertEquals(1, ccf.ninety_days_ago );
        system.assertEquals(1, ccf.threesixtyfive_days_ago );
    }
}