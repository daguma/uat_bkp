@isTest
private class RefinanceAlertCtrlTest {

    @isTest static void gets_new_app_by_contact() {

        Opportunity app = LibraryTest.createApplicationTH();
        RefinanceAlertCtrl.RefinanceAlertEntity rae = RefinanceAlertCtrl.getRefiAppByContact(app.contact__c);

        System.assertEquals('',rae.refiAppsCSV);
        System.assertEquals(false,rae.hasError);
    }
    
    @isTest static void gets_refi_app_by_contact() {

        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B1';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;

        RefinancePostSoftPull.executeProcess(contract.Id,refinanceParams);
        Opportunity refiOpp = [SELECT Id, Name, Contact__c, Status__c FROM Opportunity WHERE Contract_Renewed__c =: contract.Id LIMIT 1];
        refiOpp.Status__c = 'Funded';
        update refiOpp;
        RefinancePostSoftPull.executeProcess(contract.Id,refinanceParams);
        
        RefinanceAlertCtrl.RefinanceAlertEntity rae = RefinanceAlertCtrl.getRefiAppByContact(refiOpp.contact__c);

        System.assertNotEquals('',rae.refiAppsCSV);
        System.assertEquals(false,rae.hasError);
    }
    
    private static loan__loan_Account__c creates_contract() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        contract.loan__Interest_Accrued_Not_Due__c = 500;
        update contract;

        Offer__c off = new Offer__c();
        off.IsSelected__c = true;
        off.Opportunity__c = contract.Opportunity__c;
        off.APR__c = 0.2;
        off.Fee__c = 100;
        off.Fee_Percent__c = 0.04;
        off.FirstPaymentDate__c = Date.today().addDays(-29);
        off.Installments__c = 36;
        off.Loan_Amount__c = 5000;
        off.Term__c = 36;
        off.Payment_Amount__c = 230;
        off.Repayment_Frequency__c = 'Monthly';
        off.Preferred_Payment_Date__c = Date.today().addDays(-23);
        insert off;

        return contract;
    }
    
}