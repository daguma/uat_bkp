@isTest public class CustomSettingsTest {
    @isTest static void getLPCustom() {
         CustomSettings.getLPCustom();
          LP_Custom__c sett = new LP_Custom__c(SupportedStates__c = 'CA,GA,MO');
          insert sett;
          CustomSettings.getLPCustom();
    }
    @isTest static void getEZVerifyCustom() {
          CustomSettings.getEZVerifyCustom();
          ezVerify_Settings__c ez=new ezVerify_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Source__c='EzVerify', EZVerify_Base_URL__c='https://test.ezverify.me/api/lend',Product_Name_Dev__c='Test',Default_Interest_Rate__c=3.99,DL_Income_Variance__c=10);
        ez.EZVerify_Link_Expiration_Hours__c = 24;  
        insert ez;
        CustomSettings.getEZVerifyCustom();
    }
    @isTest static void getcapexMDNotification() {
        Opportunity sampleApp = TestHelper.createApplication();
        Account partnerAccount = new Account(Name='Samplepartner',Type='Partner',Partner_Sub_Source_Key__c='Capex_Pharma',ParterType_ForIndexing__C='Partner',Active_Partner__c=true,Source__c='Online Aggregator',Is_Parent_Partner__c=true);
        insert partnerAccount;
        Opportunity sappNew=new Opportunity(Id=sampleApp.Id,Status__c='Funded', Deal_is_Ready_to_Fund__c=True, Lead_Sub_Source__c = 'Capex_Pharma');
        update sappNew;
 
         List<ID> applicationIds= new List<id>();
          applicationIds.add(sampleApp.id);
          CustomSettings.capexMDNotification(applicationIds);
    }
    @isTest static void getcapexMDNotificationWithoutFunded() {
        Opportunity sampleApp = TestHelper.createApplication();
        Account partnerAccount = new Account(Name='Samplepartner',Type='Partner',Partner_Sub_Source_Key__c='Capex_Pharma',ParterType_ForIndexing__C='Partner',Active_Partner__c=true,Source__c='Online Aggregator',Is_Parent_Partner__c=true);
        insert partnerAccount;
        Opportunity sappNew=new Opportunity(Id=sampleApp.Id,Status__c='Approved, Pending Funding', Deal_is_Ready_to_Fund__c=True, Lead_Sub_Source__c = 'Capex_Pharma');
        update sappNew;
        Provider_Payments__c samplePro = new Provider_Payments__c(Opportunity__c=sappNew.id,Payment_Amount__c=100,Procedure_Date__c=system.today(),Provider_Name__c=partnerAccount.id);
        insert samplePro ;
        Provider_Payments__c samplePro2 = new Provider_Payments__c(Opportunity__c=sappNew.id,Payment_Amount__c=200,Procedure_Date__c=system.today(),Provider_Name__c=partnerAccount.id);
        insert samplePro2 ;
        List<ID> applicationIds= new List<id>();
        applicationIds.add(sampleApp.id);
        CustomSettings.capexMDNotification(applicationIds);
    }
}