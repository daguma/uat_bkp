public with sharing class DealAutomaticFicoRangeCtrl {
    public DealAutomaticFicoRangeCtrl() {

    }

    /**
    * Get all grades from the Deal Automatic Attributes
    * @return List of Deal_Automatic_Attributes__c
    */
    public static String getFicoRanges() {
        return JSON.serialize([SELECT Id__c, Name, Display_Name__c, Usage__c
                               FROM Deal_Automatic_Attributes__c
                               WHERE Usage__c LIKE :DealAutomaticAssignmentUtils.SOFT_FICO_RANGE_USAGE
                               ORDER BY Display_Name__c]);
    }

    /**
        * [saveUsers description]
        * @param  attribute to save the selected users
        * @param  usersGroups    to save in the specific category
        * @return Save the selected users in the Deal_Automatic_Assignment_Data__c object
        */
    @RemoteAction
    public static List<DealAutomaticUserGroup> saveUsers(Deal_Automatic_Attributes__c attribute,
            List<DealAutomaticUserGroup> newUsersGroups,
            List<DealAutomaticUserGroup> oldUserGroups) {
        return DealAutomaticAssignmentUtils.saveUsers(attribute, newUsersGroups, oldUserGroups);
    }

    /**
    * [getUsersGroupsBySource description]
    * @param  grade [description]
    * @return  List of DealAutomaticUserGroup
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> getUsersGroupsByFicoRange(Deal_Automatic_Attributes__c ficoRange) {
        return DealAutomaticAssignmentUtils.getUsersGroupsByCategory(ficoRange);
    }
}