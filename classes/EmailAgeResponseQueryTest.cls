@isTest public class EmailAgeResponseQueryTest {
     @isTest static void validate() {
         
         EmailAgeResponseQuery ea = new EmailAgeResponseQuery();
         
         ea.email = 'test';
         System.assertEquals('test', ea.email);
         
         ea.queryType = 'test';
         System.assertEquals('test', ea.queryType);
         
         ea.count= 1;
         System.assertEquals(1, ea.count);
         
         ea.created = 'test';
         System.assertEquals('test', ea.created);
         
         ea.lang = 'test';
         System.assertEquals('test', ea.lang);
         
         ea.responseCount= 2;
         System.assertEquals(2, ea.responseCount);
         
         ea.results = new List<EmailAgeResponseResults>();
         System.assertEquals(0, ea.results.size());
     }
}