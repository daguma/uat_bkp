global class FactorTrust{
    
    Webservice static string FactorTrust(string LeadId, string contactId){
        
        Map<string,string> RespMap = new Map<string,string>();
        Map<string,string> FTRespMap = new Map<string,string>();
        Contact c;
        Lead l;
        boolean FactorTrustDone = false, isSandbox = runningInASandbox();
        
        String ApplicationID = '',  Phone = '', FirstName = '', LastName = '', Street = '', ZipCode = '', City = '';
        String state = '', Email = '', Country = 'US', FTResponse,  Msg = 'Failed';
        String SSN = '', EmployerName = '', xmlResponse = '', xmlStringxmlRes,  response, ReturnVal = 'N';
        Decimal  RequestedAmount = 0,AnnualIncome = 0;
        integer StatusCode = 0;
        Date DOB = null;
     
        
        try{
            
            FTResponse =FactorTrustUtil.FTService(LeadId,contactId);
            FTRespMap = (Map<String,String>) JSON.deserialize(FTResponse, Map<String,String>.class); 
            xmlStringxmlRes=(FTRespMap.containsKey('request'))?FTRespMap.get('request'):null;
            response=(FTRespMap.containsKey('response'))?FTRespMap.get('response'):null;
            StatusCode =(FTRespMap.containsKey('statusCode'))?Integer.valueOf(FTRespMap.get('statusCode')):0;
            
            if(String.isNotEmpty(contactId) ){
                c  = [select id, Loan_Amount__c, Annual_Income__c, MailingCountry, Employer_Name__c, SSN__c, Email, birthdate, FirstName, LastName, MailingStreet, MailingPostalCode, MailingCity, MailingState, Phone_Clean__c, Factor_Trust_Verification__c,Factor_Trust_Acceptance__c from Contact where id=: contactId];              
            }
            if(String.isNotEmpty(leadId)  && !FactorTrustDone){
                l = [select id,Loan_Amount__c,  Annual_Income__c, Country, Employer_Name__c, SSN__c, Date_of_Birth__c, FirstName, LastName, Street, PostalCode, City, State, Phone_Clean__c, Email, LeadSource,Factor_Trust_Verification__c,Authorization_to_pull_credit__c,IsConverted,Factor_Trust_Acceptance__c,Demyst_Email_Acceptance__c,Demyst_Income_Acceptance__c,Demyst_Employer_Acceptance__c, ConvertedContactID from Lead where id=: leadId];
            }
            
  
            if(StatusCode == 200){
                ReturnVal =  'Y'; 
                Msg = 'Passed';             
            }
            
            //Updating result in Lead/Contact
            MelisaCtrl.TextFieldName = 'Factor_Trust_Verification__c';
            MelisaCtrl.updateResult('Factor_Trust_Acceptance__c',l,c,ReturnVal);
            
            system.debug('================ReturnVal ============'+ReturnVal);
            
            if(ReturnVal == 'N')  ReturnVal = getConfigResult('factorTrust','AllowProceed');
            
            
            if((l <> null && c == null) && (l.Authorization_to_pull_credit__c == false) && (l.LeadSource != 'LPCustomerPortal')){
                
                //Fetching Configuration values for Auto Assignment
                string FactorTrust  = (ReturnVal == 'N') ? getConfigResult('factorTrust','AllowAutoAssign') : ReturnVal;
                string EmailDemyst  = (l.Demyst_Email_Acceptance__c) ? 'Y' : getConfigResult('email','AllowAutoAssign');
                string IncomeDemyst = (l.Demyst_Income_Acceptance__c) ? 'Y' : getConfigResult('income','AllowAutoAssign');
                
            }    
            
            
            System.debug('============Response============================='+xmlResponse);
            
            RespMap.put('Status',String.ValueOf(StatusCode));              
        }Catch(Exception Exc){
            system.debug('======exc======================'+Exc.GetMessage());
            ReturnVal = getConfigResult('factorTrust','AllowProceed');
            WebToSFDC.notifyDev('FactorTrust ERROR', 'ERROR = ' + Exc.getMessage() + '\n Line = ' + Exc.getLineNumber() + '\n ' + Exc.getStackTraceString());
            RespMap.put('Status','error'); 
            msg = Exc.GetMessage(); 
        }
            RespMap.put('Acceptance',ReturnVal);       
        RespMap.put('Message',Msg);  
        return JSON.SerializePretty(RespMap);
        
    }
    
    
    public static string getConfigResult(string API, string FieldCheck){
        string ConfigReslt =  ThirdPartyDetails.fetchConfigurations(API,FieldCheck);
        Map<string,string> ConfigMap = (Map<String,String>) JSON.deserialize(ConfigReslt, Map<String,String>.class);
        return ConfigMap.get('acceptance');       
    }
    
    
    public static Boolean runningInASandbox() {
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    
    
}