public class DealAutomaticStatusCategoryCtrl {

    public DealAutomaticStatusCategoryCtrl() {

    }

    /**
     * Get all the categories and statuses from the Deal Automatic Attributes
     * @return List of Deal_Automatic_Attributes__c
     */
    public static String getCategories() {
        return JSON.serialize([SELECT Id__c, Name, Display_Name__c, Usage__c
                               FROM Deal_Automatic_Attributes__c
                               WHERE Usage__c LIKE :DealAutomaticAssignmentUtils.CATEGORY_USAGE
                               ORDER BY Display_Name__c]);
    }

    /**
     * [getUsersGroupsByCategory description]
     * @param  category [description]
     * @return          [description]
     */
    @RemoteAction
    public static List<DealAutomaticUserGroup> getUsersGroupsByCategory(Deal_Automatic_Attributes__c category) {
        return DealAutomaticAssignmentUtils.getUsersGroupsByCategory(category);
    }

    /**
    * [saveUsers description]
    * @param  attribute to save the selected users
    * @param  usersGroups    to save in the specific category
    * @return Save the selected users in the Deal_Automatic_Assignment_Data__c object
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> saveUsers(Deal_Automatic_Attributes__c attribute,
            List<DealAutomaticUserGroup> newUsersGroups,
            List<DealAutomaticUserGroup> oldUserGroups) {
        return DealAutomaticAssignmentUtils.saveUsers(attribute, newUsersGroups, oldUserGroups);
    }
}