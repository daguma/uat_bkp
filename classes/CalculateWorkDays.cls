public class CalculateWorkDays {
    
    public static Date dateWorkDays(Date endDate, Integer noOfDays){
        return dateWorkDays(endDate, noOfDays, false);
    }
    
    //If the user wants the nearest workday from a specified date they can pass in 0 for noOfDays, or -1 if they want nearest prev workday
    public static Date dateWorkDays(Date endDate, Integer noOfDays, Boolean weekendsOnly){// noOfDays should work with positive or negetive integers
		system.debug('endDate noOfDays weekendsOnly');
        system.debug(endDate + ' ' + noOfDays + ' ' + weekendsOnly);
        Boolean cont = true, itsAHoliday, weekend;
        Integer increment, days = (noOfDays < 0 ? -1 * noOfDays : noOfDays);
        if(noOfDays == 0) {
            increment = 1; 
        }else{
        	increment = (noOfDays < 0 ? -1 : 1);
        }
        system.debug('increment ' + increment);
        Date newEndDate = Date.newInstance(endDate.addDays(increment).year(), endDate.addDays(increment).month(), endDate.addDays(increment).day());
        Date sundaySameWeek;
        Integer numberDays;
        while(days > 0 ){
            weekend = false;
            itsAHoliday = false;
			sundaySameWeek = newEndDate.toStartofWeek();
        	numberDays = sundaySameWeek.daysBetween(newEndDate);
        	system.debug('newEndDate ' + newEndDate + ' sundaySameWeek ' + sundaySameWeek + ' numberDays ' + numberDays);
            // 6 == Saturday, 0 == Sunday
            if (numberDays == 6 || numberDays == 0){ 
                newEndDate = newEndDate.addDays(increment);
                weekend = true;
                system.debug('Weekend');
            }else{
                weekend = false;
            }
            if(!weekendsOnly && !weekend){
                List<loan__Holiday__c> hldays = [SELECT Id, loan__Date__c from loan__Holiday__c];
                for (loan__Holiday__c hlday : hldays){
                    if(newEndDate == hlday.loan__Date__c){
                        itsAHoliday = true;
                        system.debug('Holiday');
                        break;
                    }
                }
                if(itsAHoliday){
                    newEndDate = newEndDate.addDays(increment);
                    cont = true;
                }
            }
            if(!weekend && !itsAHoliday){
                if(days > 1) newEndDate = newEndDate.addDays(increment);
                days--;
            }
            system.debug('Weekend = ' + weekend + ' Holiday = '+ itsAHoliday + ' days = ' + days);
        } 
        return newEndDate;
    }    
}