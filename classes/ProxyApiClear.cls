public class ProxyApiClear extends ProxyApiBRMSUtil{

    private static final String GET_CLEAR_COURT_URL = '/v101/clear/get-court/';

    /**
     * Calls the proxy API for tax liens and judgements search.
     * @param  inputData
     * @return clearCourtEntity
     */
    public static ProxyApiClearCourtEntity getCourt(String inputData) {

        ProxyApiClearCourtEntity clearCourtEntity = null;

        Boolean validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
		System.debug('validatedAPI: ' + validatedAPI);
        if (validatedAPI) {
            System.debug('input data = ' + inputData);
            String body = 'input-data=' + EncodingUtil.urlEncode(inputData, 'UTF-8');
            String jsonResponse = null;

            if (Test.isRunningTest())
                jsonResponse = LibraryTest.fakeProxyApiClearCourtEntity('');
            else
                jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GET_CLEAR_COURT_URL, body);

            if (jsonResponse != null)
                clearCourtEntity = (ProxyApiClearCourtEntity)JSON.deserializeStrict(jsonResponse, ProxyApiClearCourtEntity.class);
        }
        return clearCourtEntity;
    }
}