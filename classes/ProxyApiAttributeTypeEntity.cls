global class ProxyApiAttributeTypeEntity {
    @AuraEnabled public Integer id {get; set;}
    @AuraEnabled public String conceptType {get; set;}
    @AuraEnabled public String displayName {get; set;}
    @AuraEnabled public String description {get; set;}
    @AuraEnabled public Boolean isDisplayed {get; set;}

    public ProxyApiAttributeTypeEntity() {
    }

    public ProxyApiAttributeTypeEntity(String displayName, String description) {
        this.displayName = displayName;
        this.description = description;
        this.isDisplayed = true;
    }
}