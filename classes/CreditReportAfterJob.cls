global class CreditReportAfterJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {

  global final String query;
  global final String option;
  global final Integer minutesFromNow;

  global CreditReportAfterJob(String option) {
    this(option, 1);
  }
  global CreditReportAfterJob(String option, Integer minutesFromNow) {
    this.option = option;
    this.minutesFromNow = minutesFromNow;

    if (option == 'LexisNexis') {
      this.query = 'SELECT Id, Opportunity__c, AttachmentRefreshed__c, Declined__c, IsProcessed__c, ErrorMessage__c FROM LexisNexisResponse__c WHERE IsProcessed__c = false ORDER BY CreatedDate DESC';
    } else if (option == 'AcceptanceModel') {
      this.query = 'SELECT Id, Opportunity__c,  Decile__c, PA__c, Score__c, IsProcessed__c, ErrorMessage__c FROM AcceptanceModelRequest__c WHERE IsProcessed__c = false ORDER BY CreatedDate DESC';
    }
  }

  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator(query);
  }

  global void execute(Database.BatchableContext BC, List<sObject> scope) {

    if (option == 'LexisNexis') {
      for (sobject s : scope) {
        LexisNexisResponse__c ln = (LexisNexisResponse__c) s;
        ln.IsProcessed__c = true;
        CreditReport.getTaxLiensFromLexisNexis(ln);
      }
    } else if (option == 'AcceptanceModel') {
      for (sobject s : scope) {
        AcceptanceModelRequest__c am = (AcceptanceModelRequest__c) s;
        am.IsProcessed__c = true;
        CreditReport.getAcceptanceModel(am);
      }
    }
  }

  global void finish(Database.BatchableContext BC) {

    CreditReportAfterJob job = new CreditReportAfterJob(option, minutesFromNow);
    System.scheduleBatch(job, 'AfterGetCreditReportJob_' + option, minutesFromNow, 1);  //Each 1 minute, process batches of 1 by 1

  }

  global void execute(SchedulableContext sc) {

  }

}

//To run:
/*
CreditReportAfterJob job1 = new CreditReportAfterJob('LexisNexis', 30);
database.executebatch(job1, 1);
 // OR:
CreditReportAfterJob job2 = new CreditReportAfterJob('AcceptanceModel', 30);
database.executebatch(job2, 1);
*/