public class GiactProcessor {
    
    @future (callout=true)
    public static void ProcessFundsConfirmationUpdates() {
        Giact_Update_Request__c retVal = new Giact_Update_Request__c();
        retVal.ProcessDate__c = Date.Today();
        
        string requestBody = GiactRequestHelper.GenerateFundsConfirmationRequest();
        retVal.RequestXml__c = requestBody;
        string result = '';
        try {
            //SD: may need to scale out at high volume
            retVal.ResponseXml__c = new GiactApi().sendGetFundsConfirmationRequest(requestBody);
        } catch (GiactException ex) {
            retVal.ErrorMessage__c = ex.getMessage().abbreviate(131071);
            insert retVal;
            return;
        }
        
        List<string> results = GiactResponseHelper.getNodeArray('GiactSearchResult_5_8', retVal.ResponseXml__c);
        retVal.NumberOfReferences__c = results.size();
        Map<string, string> resultMap = new Map<string, string>();
        for (string item : results) {
            resultMap.put(GiactResponseHelper.getNodeValue('ItemReferenceId', item), item);
        }
        
        List<GIACT__c> itemsToUpdate = new List<GIACT__c>();
        List<GIACT__c> giactRecords = [SELECT AccountAddedDate__c,AccountClosedDate__c,AccountLastUpdatedDate__c,AccountNumber__c,AccountResponseCode__c,BankName__c,CheckAmount__c,CreatedById,CreatedDate,CreatedDate__c,CustomerResponseCode__c,ErrorMessage__c,FundConfirmationAttempts__c,FundsConfirmationResult__c,Id,IsDeleted,ItemReferenceID__c,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,MerchantAccount__c,Name,RequestXml__c,ResponseXml__c,RoutingNumber__c,Status__c,SystemModstamp,Unresolved__c,VerificationResponse__c FROM GIACT__c WHERE ItemReferenceID__c IN :resultMap.keySet()];
        retVal.MatchedReferences__c = giactRecords.size();
        for (string item : resultMap.keySet()) {
            for (GIACT__c giactItem : giactRecords) {
                /*if (giactItem.ItemReferenceID__c == item && giactItem.Status__c != GiactConstants.STATUS_REVIEW_COMPLETE) {
                
                    try {
                        PopulateGiactFieldsFromResponse(resultMap.get(item), giactItem, false);
                        if (giactItem.FundConfirmationAttempts__c > GIACT_Integration_Settings__c.getInstance().RetryAttempts__c) {
                            giactItem.Status__c = GiactConstants.STATUS_FAIL;
                        }
                    } catch(Exception ex) {
                        giactItem.ErrorMessage__c = ex.getMessage().abbreviate(131071);
                    }
                    
                    itemsToUpdate.add(giactItem);
                }*/
                
                //ONLY update the FundsConfirmationResult
                if (giactItem.ItemReferenceID__c == item) {
                
                    try {
                        giactItem.FundsConfirmationResult__c = GiactResponseHelper.getNodeValue('FundsConfirmationResult', resultMap.get(item));                        
                    } catch(Exception ex) {
                        giactItem.ErrorMessage__c = ex.getMessage().abbreviate(131071);
                    }
                    
                    itemsToUpdate.add(giactItem);
                }
            }
        }
        
        retVal.ResponseXml__c = retVal.ResponseXml__c.abbreviate(131071);
        insert retVal;
        update itemsToUpdate;
    }
    
    private static void UpdateGiactFromResponseBody(GIACT__c item, String body) {
    
    }
    
    @future (callout=true)
    public static void SubmitGiact(string giactId) {
        //get giact record
        GIACT__c record = [SELECT RoutingNumber__c, AccountNumber__c, CheckAmount__c, MerchantAccount__r.Name, MerchantAccount__r.MaxLoanAmountAvailable__c, MerchantAccount__r.DBA_Doing_Business_As__c FROM GIACT__c WHERE id = :giactId];

        //make callout
        string result = MakeInquiryCallout(record, false);
        if (result == 'Error')
            return;
            
        //if we get a mis-matched name, try DBA name
        if (GiactResponseHelper.getNodeValue('CustomerResponseCode', result) == GiactConstants.NameMismatch)
            result = MakeInquiryCallout(record, true);
        
        //update record
        PopulateGiactFieldsFromResponse(result, record, true);        
    } 
    
    private static string MakeInquiryCallout(GIACT__c record, boolean dba) {
        string requestBody = GenerateInquiryFromGiact(record, dba);
        record.RequestXml__c = requestBody;
        string result = '';
        try {
            return result = new GiactApi().sendInquiryRequest(requestBody);
        } catch (GiactException ex) {
            //prevent field overflow on long responses, ensure we see something        
            record.ErrorMessage__c = ex.getMessage().abbreviate(131071);
            update record;
            return 'Error';
        }  
    }
    
    private static string GenerateInquiryFromGiact(GIACT__c record, boolean dba) {
        return string.format(GiactRequestHelper.InquiryRequestStringFormat, GiactRequestHelper.GenerateStringArrayFromGiact(record, dba));
    }
    
    private static string ProcessResultCode(string accountResponseCode, string fundsConfirmationResult, string customerResponseCode) {        
        System.debug('accountResponseCode:' + accountResponseCode);
        System.debug('customerResponseCode:' + customerResponseCode);
        System.debug('fundsConfirmationResult:' + fundsConfirmationResult);     
        
        System.debug('GiactConstants.AcceptableAccountResponseCodes:' + GiactConstants.AcceptableAccountResponseCodes);
        System.debug('GiactConstants.AcceptableCustomerResponseCode:' + GiactConstants.AcceptableCustomerResponseCode);
        System.debug('GiactConstants.PositiveFundsConfirmation:' + GiactConstants.PositiveFundsConfirmation);        

        System.debug('AccountResponseCodes.contains():' + GiactConstants.AcceptableAccountResponseCodes.contains(accountResponseCode));
        System.debug('CustomerResponseCode.contains():' + GiactConstants.AcceptableCustomerResponseCode.contains(customerResponseCode));
        System.debug('fundsConfirmationResult == GiactConstants.PositiveFundsConfirmation:' + fundsConfirmationResult == GiactConstants.PositiveFundsConfirmation);                
        
        /*//Basic Pass -- all three valid
        if (GiactConstants.AcceptableAccountResponseCodes.contains(accountResponseCode)
            && GiactConstants.AcceptableCustomerResponseCode.contains(customerResponseCode)
            && fundsConfirmationResult == GiactConstants.PositiveFundsConfirmation)
            return GiactConstants.STATUS_REVIEW_COMPLETE;
        
        //Non-Participating Bank -- ignore funds confirmation result    
        if (fundsConfirmationResult == GiactConstants.NonParticipatingBank
            && GiactConstants.AcceptableAccountResponseCodes.contains(accountResponseCode)
            && GiactConstants.AcceptableCustomerResponseCode.contains(customerResponseCode))
            return GiactConstants.STATUS_REVIEW_COMPLETE;
        
        //All values present, and not pass status -- fail immediately
        if (string.isNotBlank(fundsConfirmationResult) && string.isNotBlank(accountResponseCode) && string.isNotBlank(customerResponseCode))
            return GiactConstants.STATUS_FAIL;*/
            
        //simplified model, pass/fail based on Account & Customer code only, update FundsConfirmation if available
        if (GiactConstants.AcceptableAccountResponseCodes.contains(accountResponseCode)
            && GiactConstants.AcceptableCustomerResponseCode.contains(customerResponseCode))
            return GiactConstants.STATUS_REVIEW_COMPLETE;
            
        if (string.isNotBlank(accountResponseCode) && string.isNotBlank(customerResponseCode))
            return GiactConstants.STATUS_FAIL;
        
        return GiactConstants.STATUS_IN_PROGRESS;
    }    
    
    private static void PopulateGiactFieldsFromResponse(string responseBody, GIACT__c record, boolean doUpdate) {
        try {
            string accountResponseCode = GiactResponseHelper.getNodeValue('AccountResponseCode', responseBody);
            string fundsConfirmationResult = GiactResponseHelper.getNodeValue('FundsConfirmationResult', responseBody);
            string customerResponseCode = GiactResponseHelper.getNodeValue('CustomerResponseCode', responseBody);
            record.ResponseXml__c = responseBody.abbreviate(131071);
            
            record.ItemReferenceID__c = GiactResponseHelper.getNodeValue('ItemReferenceId', responseBody);
            record.VerificationResponse__c = GiactResponseHelper.getNodeValue('VerificationResponse', responseBody);
            record.AccountResponseCode__c = accountResponseCode;
            record.BankName__c = GiactResponseHelper.getNodeValue('BankName', responseBody);                                                
            record.FundsConfirmationResult__c = fundsConfirmationResult ;            
            record.CustomerResponseCode__c = GiactResponseHelper.getNodeValue('CustomerResponseCode', responseBody);   

            record.CreatedDate__c = GiactResponseHelper.tryGetDate(GiactResponseHelper.getNodeValue('CreatedDate', responseBody));
            record.AccountAddedDate__c = GiactResponseHelper.tryGetDate(GiactResponseHelper.getNodeValue('AccountAddedDate', responseBody));            
            record.AccountLastUpdatedDate__c = GiactResponseHelper.tryGetDate(GiactResponseHelper.getNodeValue('AccountLastUpdatedDate', responseBody));            
            record.AccountClosedDate__c = GiactResponseHelper.tryGetDate(GiactResponseHelper.getNodeValue('AccountClosedDate', responseBody));                        
            
            record.Status__c = ProcessResultCode(accountResponseCode, fundsConfirmationResult, customerResponseCode);
        } catch (Exception ex) {
            //prevent field overflow on long responses, ensure we see something
            record.ErrorMessage__c = responseBody.abbreviate(131071);
        }
        if (record.FundConfirmationAttempts__c == null) 
            record.FundConfirmationAttempts__c = 0;
            
        record.FundConfirmationAttempts__c++;
        
        if (doUpdate)
            update record;
    }
}