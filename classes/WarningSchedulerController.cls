/**
 * Author : Shweta Awasthi - Created on 06/10/2016
 * Helper Class to send a warning message if any API stops working
 * Initialy for Payfone API
*/
global class WarningSchedulerController implements Schedulable {
    
    public Map<String,API_List_For_Alerts__c> apiListForAlertsMap = new Map<String,API_List_For_Alerts__c>();
    public Map<String,Idology_Request__c> idologyReqMap = new Map<String,Idology_Request__c>();
    public integer payfoneFailureProbability;
    public integer payfoneNotRespondingProbability;
    public static string EMAIL_TEMPLATE_NAME = 'PayfoneWarningTemplate1';
    
    global void execute(SchedulableContext ctx) {
        
        payfoneFailureProbability = 0;
        payfoneNotRespondingProbability  = 0;
        
        Payfone_Settings__c payfoneSettings = Payfone_Settings__c.getValues('Payfone Request Params');      
        
        //get idology results created today and are the first idology attempts along with payfone
        List<Idology_Request__c> idologyResults = new List<Idology_Request__c>();
        
        for(Idology_Request__c ido : [select id,Contact__c,Contact__r.Process_Date__c,CreatedDate from Idology_Request__c 
                                                    where CreatedDate >=: date.today() and (Contact__r.Process_Date__c >=: date.today() or Contact__r.Process_Date__c = null)]) {
            idologyReqMap.put(ido.Contact__c,ido);
        }
        system.debug('--idologyReqMap---'+idologyReqMap);
        idologyResults = idologyReqMap.values();
        system.debug('--idologyResults ---'+idologyResults );
        
        //get the custom setting list for the APIs for which the alert needs to be send and are active
        for(API_List_For_Alerts__c activeAPIlist : API_List_For_Alerts__c.getall().values()) {
            if(activeAPIlist.Is_Active__c)
                apiListForAlertsMap.put(activeAPIlist.Name,activeAPIlist);
        }
        
        //check if payfone exists in the map
        if(apiListForAlertsMap.containskey('Payfone') && payfoneSettings.call_payfone__c == true) {
            API_List_For_Alerts__c apilst = apiListForAlertsMap.get('Payfone');
            //get the payfone run history records for the day
            List<Payfone_Run_History__c> payfoneResults = new List<Payfone_Run_History__c>();
            
            payfoneResults = [select id,Contact__c,Name,GetIntelligence_Response__c,Verify_API_Response__c,Verification_Result__c,MatchCRM_Response__c
                                 from Payfone_Run_History__c where createddate >=: date.today()];
            
            system.debug('----payfoneResults---'+payfoneResults);
            Integer totalPayfoneResponseFailure = 0;
            Integer payfoneResponseNotReceivedCount = 0;
            
            //iterate over payfone get the count of not responding results
            for(Payfone_Run_History__c payfoneRes : payfoneResults) {
                //if payfone not receiving response get the count
                if(string.isempty(payfoneRes.Verify_API_Response__c)) {
                    payfoneResponseNotReceivedCount ++;
                }
            }
            
            system.debug('--payfoneResponseNotReceivedCount--'+payfoneResponseNotReceivedCount);
            
            //check the failling probability of payfone
            if(payfoneResults.size() != idologyResults.size()) {
                //get the difference in the sizes    
                Integer diff = payfoneResults.size() - idologyResults.size();
                system.debug('--diff --'+diff );
                
                if(diff < 0) {
                    diff = diff  * (-1);
                    if(payfoneResults.size() > 0)
                        payfoneFailureProbability = (diff/payfoneResults.size()) * 100;
                    else 
                        payfoneFailureProbability = 100;
                    
                    if(payfoneFailureProbability > 0) totalPayfoneResponseFailure += payfoneFailureProbability;
                }
            }
            
            system.debug('---payfoneFailureProbability---'+payfoneFailureProbability);
            //if payfone not receiving response
            if(payfoneResponseNotReceivedCount > 0) {
                payfoneNotRespondingProbability = (payfoneResponseNotReceivedCount/payfoneResults.size()) * 100; 
                totalPayfoneResponseFailure += payfoneNotRespondingProbability;
            }
            
            system.debug('---payfoneNotRespondingProbability ---'+payfoneNotRespondingProbability);
            String subject = apilst.Name+ ' - Warning Message';
            String Body = 'It seems like something wrong with payfone. Payfone not responding appropriately..!!';
            String UserId = Label.PayfoneWarningRecipient;
            
            
            if(totalPayfoneResponseFailure > 95) {
                //get the email template 
                EmailTemplate emailtemp = [select Id,DeveloperName,Subject,HTMLValue from EmailTemplate where DeveloperName = :EMAIL_TEMPLATE_NAME];
                WebToSFDC.notifyDev(emailtemp.Subject, emailtemp.HTMLValue , UserId);  // Notify specific user
            }
        }
        
    }
}