@isTest
public class TestTestHelperForManaged {
    public static testMethod void testHelperForManaged(){
        //Date d = TestHelperForManaged.getBranchDate();
        Account b1 = TestHelperForManaged.createBorrower('ShoeString');
        b1.peer__National_Id__c = '1223';
        update b1;
        loan__Bank_Account__c dummyBank = TestHelperForManaged.createBankAccount(b1,'Advance Trust Account');

        loan__Loan_Purpose__c dummyL = TestHelperForManaged.createLoanPurpose();
        loan__Currency__c curr = TestHelperForManaged.createCurrency();
        TestHelperForManaged.createSeedDataForTesting();
        TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.setupORGParameters();
        TestHelperForManaged.setupORGParametersActive();
        TestHelperForManaged.setupMetadataSegments();
        Id Branchid = TestHelperForManaged.getRootBranchRecordTypeID();
        loan__Loan_Purpose__c purp = TestHelperForManaged.createLoanPurpose('test','testing','tested');
        //Contact dummyClient = TestHelperForManaged.createContactWithoutInsert('khush',);
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest', '10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest', '30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr, dummyIncAccount, dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee, dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        loan__Loan_Account__c dummylaMonthly = TestHelperForManaged.createLoanAccountForAccountObj(dummyLP,
                                        b1,
                                        dummyFeeSet,
                                        dummyL,
                                        dummyOffice,
                                        true,
                                        loan.TestHelper.systemDate.addDays(1),
                                        null,
                                        loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY);
        loan__Loan_Account__c dummylaweekly = TestHelperForManaged.createLoanAccountForAccountObj(dummyLP,
                                        b1,
                                        dummyFeeSet,
                                        dummyL,
                                        dummyOffice,
                                        false,
                                        loan.TestHelper.systemDate.addDays(1),
                                        null,
                                        loan.LoanConstants.LOAN_PAYMENT_FREQ_WEEKLY);
    }
}