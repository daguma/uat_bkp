@isTest
public class ACHScreenCtrl_Test {
    testmethod static void test1(){
        DL_AccountStatementSF dlasf = new DL_AccountStatementSF();
        dlasf.availableBalance = '10'; 
        dlasf.currentBalance = '20';
        dlasf.averageBalance = '20';
        dlasf.totalCredits = '20';
        dlasf.averageBalanceRecent = '20';
        dlasf.totalDebits = '20';
		
		List<DL_AccountStatementSF.TransactionSummaries> dtlst = new List<DL_AccountStatementSF.TransactionSummaries>();
		DL_AccountStatementSF.TransactionSummaries dt = new DL_AccountStatementSF.TransactionSummaries();
		dt.runningBalance = 20;
		dt.transactionDate = '2015-12-31';
		dtlst.add(dt);
		dlasf.transactionSummaries = dtlst;
		
		Contact TestContact = LibraryTest.createContactTH();
		Opportunity opp = TestHelper.createApplicationFromContact(TestContact,true);
        ACHScreenCtrl.handleDLFields(dlasf,opp);
        ACHScreenCtrl.EWSNoteRecord(opp.id,'test','test','tesrt');
		
    }
}