@isTest
private class AONDisplayLogsTest {

    @isTest static void displays_logs() {
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        
        Logging__c log = new Logging__c();
        log.Webservice__c = 'Billing';
        log.API_Request__c = 'Request';
        log.API_Response__c = 'Response';
        log.CL_Contract__c = contract.Id;
        insert log;
        
        AONDisplayLogs dispLogs = new AONDisplayLogs();
        String result = AONDisplayLogs.getAllLogs(contract.Id);
        
        System.assert(result.containsIgnoreCase('Billing'), 'Display Logs');
    }
    
}