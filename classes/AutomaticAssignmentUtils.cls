public class AutomaticAssignmentUtils {
    /**
     * gets mean
     * @param  m list
     * @return  mean
     */
    public static Integer getMean(List<Integer> m) {
        if (m.size() > 0) {
            Integer sum = 0;
            for (Integer i = 0; i < m.size(); ++i) {
                sum += m[i];
            }
            return (Integer) sum / m.size();
        }
        return 0;
    }

    /**
     * suffle users algorithm
     * @param  dealAutomaticUserGroup
     * @return List of DealAutomaticUserGroup
     */
    public static List<DealAutomaticUserGroup> shuffleUsers(List<DealAutomaticUserGroup> dealAutomaticUserGroup) {
        Integer N = dealAutomaticUserGroup.size();

        for (Integer i = 0; i < N; i++) {
            Integer r = i + (Integer)(Math.random() * (N - i));
            exchangeUsers(dealAutomaticUserGroup, i, r);
        }
        return dealAutomaticUserGroup;
    }

    /**
     * exchange Users for shuffle algorithm
     * @param dealAutomaticUserGroup
     * @param i index i
     * @param j index j
     */
    private static void exchangeUsers(List<DealAutomaticUserGroup> dealAutomaticUserGroup, Integer i, Integer j) {
        DealAutomaticUserGroup user = dealAutomaticUserGroup[i];
        dealAutomaticUserGroup[i] = dealAutomaticUserGroup[j];
        dealAutomaticUserGroup[j] = user;
    }

    /**
     * Checks if list of user groups contains a specific user group
     * @param  userGroup     to search in the list
     * @param  userGroupList to check if contains the specific user group
     * @return true, if the list contains the user group, otherwise false
     */
    public static Boolean userGroupContains(DealAutomaticUserGroup userGroup,
                                            List<DealAutomaticUserGroup> userGroupList) {
        for (DealAutomaticUserGroup user : userGroupList) {
            if (user.userId == userGroup.userId &&
                    user.groupId == userGroup.groupId) {
                return true;
            }
        }

        return false;
    }

    /**
     * [combineList description]
     * @param  l1 [description]
     * @param  l2 [description]
     * @return    [description]
     */
    public static List<DealAutomaticUserGroup> combineList(List<DealAutomaticUserGroup> l1,
            List<DealAutomaticUserGroup> l2) {

        List<DealAutomaticUserGroup> result = new List<DealAutomaticUserGroup>();

        for (DealAutomaticUserGroup item : l1) {
            if (userGroupContains(item, l2)) {
                result.add(item);
            }
        }

        return result;
    }

    /**
    * gets moda
    * @param  dealAutomaticUserGroup
    * @return moda value
    */
    public static Integer getModa(List<Integer> listTotals) {
        Integer maxValue = 0, maxCount = 0;

        for (Integer i = 0; i < listTotals.size(); ++i) {
            Integer count = 0;

            for (Integer j = 0; j < listTotals.size(); ++j) {
                if (listTotals[j] == listTotals[i]) ++count;
            }
            if (count > maxCount) {
                maxCount = count;
                maxValue = listTotals[i];
            }
        }

        if (maxCount == 1)  maxValue = getMean(listTotals);

        return maxValue;
    }

    /**
     * [saveUserOnlineMode description]
     * @param userId   [description]
     * @param isOnline [description]
     */
    public static void saveUserOnlineMode(String userId, Boolean isOnline) {
        for (User currentUser : [Select Id, isOnline__c From User Where Id = :userId LIMIT 1]) {
            currentUser.isOnline__c = isOnline;
            update currentUser;
        }
    }

    /**
     * [removeUserFromList description]
     * @param  userGroupList [description]
     * @param  userId        [description]
     * @return               [description]
     */
    public static List<DealAutomaticUserGroup> removeUserFromList(List<DealAutomaticUserGroup> userGroupList, String userId) {
        Integer index = 0;
        for (DealAutomaticUserGroup user : userGroupList) {
            if (user.userId == userId) {
                userGroupList.remove(index);
                break;
            }
            index++;
        }
        return userGroupList;
    }

    /**
     * [getUserFromGroupByName description]
     * @param  userName      [description]
     * @param  userGroupList [description]
     * @return               [description]
     */
    public static DealAutomaticUserGroup getUserFromGroupByName(String userName,
            List<DealAutomaticUserGroup> userGroupList) {

        if (String.isEmpty(userName)) userName = '';

        for (DealAutomaticUserGroup user : userGroupList) {
            if (user.userName == userName) {
                return user;
            }
        }

        return null;
    }

    /**
     * Get the next user to assign a lead
     * @param  dealAutomaticUserGroup a list of user groups
     * @return A valid user to assign a lead
     */
    public static DealAutomaticUserGroup getNextUser(Map<Id, Integer> totalMap,
            List<DealAutomaticUserGroup> dealAutomaticUserGroup) {

        DealAutomaticUserGroup result = new DealAutomaticUserGroup();
        Map<DealAutomaticUserGroup, Integer> userTotals = new Map<DealAutomaticUserGroup, Integer>();
        List<DealAutomaticUserGroup> shuffleUserList = shuffleUsers(dealAutomaticUserGroup);

        for (DealAutomaticUserGroup user : shuffleUserList) {
            Integer total = totalMap.get(user.userId);
            userTotals.put(user, total == null ? 0  : total);
        }

        Set<DealAutomaticUserGroup> users = new Set<DealAutomaticUserGroup>();
        users = userTotals.keySet();

        Integer avg = getMean(userTotals.values());

        for (DealAutomaticUserGroup user : users) {
            Integer total = userTotals.get(user);

            if (total <= avg) {
                result = user;
                break;
            }
        }

        return result;
    }

    /**
     * [isSandBox description]
     * @return [description]
     */
    public static Boolean isSandBox() {
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
}