@isTest public class CreditReportTest {

    @isTest static void gets_credit_report_from_server() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();


        Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        ProxyApiCreditReportEntity creditReportEntity =
            CreditReport.get(application.id, true, true, true);
			CreditReport.testingRequest(); 
        	CreditReport.testingRequest1();
        System.assertNotEquals(creditReportEntity, null);
    }

    
    @isTest static void gets_credit_report_not_from_server() {

        Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.get(application.id, true, false, true);

        System.assertNotEquals(creditReportEntity, null);
    }

    @isTest static void gets_credit_report_from_attachment() {
        
        Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getByEntityId(application.id);

        System.assertNotEquals(creditReportEntity, null);

        Blob response = Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(LibraryTest.fakeCreditReportData(application.id))));

        LibraryTest.createAttachment(application.id, response);

        creditReportEntity = CreditReport.getByEntityId(application.id);

        System.assertNotEquals(creditReportEntity, null);
    }

    @isTest static void updates_credit_report_attachment() {
         Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.get(application.id, true, false, true);

        CreditReport.set(application.id, JSON.serialize(creditReportEntity));

        System.assertNotEquals(creditReportEntity, null);
    }

    @isTest static void maps_request_parameters_application_true() {
        Opportunity application = LibraryTest.createApplicationTH();
        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByEntity(application.id, true, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_application_bureau_true() {
        Opportunity application = LibraryTest.createApplicationTH();
        application.Bureau__c = 'TU';
        update application;

        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByEntity(application.id, true, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_application_false() {
        Opportunity application = LibraryTest.createApplicationTH();

        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByEntity(application.id, true, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_application_bureau_false() {
        Opportunity application = LibraryTest.createApplicationTH();
        application.Bureau__c = 'TU';
        update application;

        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByEntity(application.id, true, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_contact_true() {
        Contact application = LibraryTest.createContactTH();
        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByEntity(application.id, true, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_contact_false() {
        Contact application = LibraryTest.createContactTH();
        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByEntity(application.id, false, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_lead_true() {
        Lead application = LibraryTest.createLeadTH();
        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByEntity(application.id, true, false);

        System.assertEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_lead_false() {
        Lead application = LibraryTest.createLeadTH();
        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByEntity(application.id, false, false);

        System.assertEquals(requestParameter, null);
    }

    @isTest static void gets_application_by_id() {
        Opportunity newApplication = LibraryTest.createApplicationTH();

        Opportunity application =
            CreditReport.getOpportunity(newApplication.id);

        System.assertNotEquals(application, null);
    }

    @isTest static void gets_income_by_application() {
        Opportunity application = LibraryTest.createApplicationTH();

        Decimal income =
            CreditReport.getIncomeByApplication(application);

        System.assertNotEquals(income, null);
    }

    @isTest static void getRegradeBureauTest() {
        Contact newContact = LibraryTest.createContactTH();
        CreditReport.leadExists(newContact.id);
        CreditReport.applicationExists(newContact.id);
    }


    @isTest static void validate_lead() {
        Lead newLead = LibraryTest.createLeadTH();

        Boolean leadExists = CreditReport.leadExists(newLead.id);

        System.assertNotEquals(leadExists, null);
    }

    @isTest static void validate_contact() {
        Contact newContact = LibraryTest.createContactTH();

        Boolean contactExists = CreditReport.contactExists(newContact.id);

        System.assertNotEquals(contactExists, null);
    }

    @isTest static void validate_application() {
        Opportunity newApplication = LibraryTest.createApplicationTH();

        Boolean appExists = CreditReport.applicationExists(newApplication.id);

        System.assertNotEquals(appExists, null);
    }

    @isTest static void validate_attachment() {
        Opportunity newApplication = LibraryTest.createApplicationTH();

        Boolean hasAttachment = CreditReport.hasAttachmentByEntityId(newApplication.id);

        System.assertNotEquals(hasAttachment, null);
    }

    @isTest static void maps_request_parameters_application_contact_false() {
        Opportunity application = LibraryTest.createApplicationTH();
        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByOpportunity(application.id, true, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_application_contact_true() {
        Opportunity application = LibraryTest.createApplicationTH();
        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByOpportunity(application.id, true, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_request_parameters_application_lead_bday() {

        Opportunity application = LibraryTest.createApplicationLeadTH();

        CreditReportRequestParameter requestParameter = CreditReport.mapProxyApiRequestParameterByOpportunity(application.id, true, false);

        System.assertNotEquals(requestParameter, null);
    }

    @isTest static void maps_decisioning_parameters_contact() {
        
        Contact newContact = LibraryTest.createContactTH();
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = newContact.Id;
        insert partnerAct;
        
        DecisioningParameter decisionParameter = CreditReport.mapDecisioningParameter(app.id, Null);

        System.assertNotEquals(decisionParameter, null);
    } 

    @isTest static void maps_decisioning_parameters_application() {
        
        Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        DecisioningParameter decisionParameter = CreditReport.mapDecisioningParameter(application.id, Null);
        System.assertNotEquals(decisionParameter, null);
        
        loan__Loan_Account__c cc =  LibraryTest.createContractTH();
        
        loan_Refinance_Params__c pr = New loan_Refinance_Params__c();
        pr.Contract__c = cc.Id;
        Insert pr;
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Contract_Renewed__c = cc.Id;
        app.Type = 'Refinance';
        update app;
        
        DecisioningParameter decisionParameter2 = CreditReport.mapDecisioningParameter(app.id, Null);
        
        Contact con = LibraryTest.createContactTH();
        
        DecisioningParameter decisionParameter3 = CreditReport.mapDecisioningParameter(con.id, Null);

        
        
    }

    @isTest static void maps_credit_report_contact_true() {
        
        Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        CreditReportInputData cr = CreditReport.mapCreditReportInputData(application.id, true, false);

        System.assertNotEquals(cr, null);
        
    }

    @isTest static void maps_credit_report_contact_false() {
        
        Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        CreditReportInputData cr = CreditReport.mapCreditReportInputData(application.id, false, false);

        System.assertNotEquals(cr, null);
    }

    @isTest static void gets_attachment() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        Opportunity application = LibraryTest.createApplicationTH();

        Test.startTest();

        CreditReport.set(application.id, LibraryTest.fakeBrmsRulesResponse(application.Id, true, '1', true));

        Test.stopTest();

        Attachment attach = CreditReport.getAttachment(application.id);

        System.assertNotEquals(attach, null);
    }

    @isTest static void gets_list_credit_report_by_id() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        Opportunity application = LibraryTest.createApplicationTH();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        List<ProxyApiCreditReportEntityLite> listCreditReportEntity = CreditReport.getAllByEntityId(application.id);

        System.assertNotEquals(listCreditReportEntity, null);
    }

    @isTest static void validate_approved_attributes() {

        Opportunity application = LibraryTest.createApplicationTH2();

        ProxyApiCreditReportEntity creditReportEntity1 = LibraryTest.fakeCreditReportEntity(application.id);

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.approvedAttributes('test', creditReportEntity1.id);

        System.assertNotEquals(null, creditReportEntity);
    }

    @isTest static void gets_info_from_api() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getInfoFromAPI(application.id, true, true, true);

        System.assertNotEquals(creditReportEntity, null);
    }

    @isTest static void gets_credit_report_by_id() {

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getCreditReportById(1);

        System.assertNotEquals(creditReportEntity, null);
    }

    @isTest static void gets_credit_report_ws() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        //Opportunity application = LibraryTest.createApplicationTH();
      
        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;

        Test.startTest();

        CreditReport.set(app.id, LibraryTest.fakeBrmsRulesResponse(app.Id, true, '1', true));
       
        CreditReport.set(app.id, LibraryTest.fakeBrmsRulesResponse(app.Id=null, true, '1', true));

        Test.stopTest();

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getCreditReportWS(app.id, true);
        
        CreditReport.notifyApplicationSubmitted(creditReportEntity);

        //System.assertNotEquals(Null, creditReportEntity);
        
        
    }
    
    @isTest static void gets_credit_report_ws1() {

        LibraryTest.createBrmsConfigSettings(1, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        //Opportunity application = LibraryTest.createApplicationTH();
      
        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = false;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;

        Test.startTest();
        
        CreditReport.set(app.id, LibraryTest.fakeBrmsRulesResponse(app.Id, false, '1', true));

        Test.stopTest();

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getCreditReportWS(app.id, true);
        
        CreditReport.notifyApplicationSubmitted(creditReportEntity);

        //System.assertNotEquals(Null, creditReportEntity);
        
        
    }

    
    
    

    @isTest static void gets_credit_report_ws_acceptance_model() {

        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        LibraryTest.createBrmsConfigSettings(-1, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        BRMS_Sequence_Ids__c brmsAM_SeqIdsSettings = new BRMS_Sequence_Ids__c();
        brmsAM_SeqIdsSettings.Name = 'Acceptance Model';
        brmsAM_SeqIdsSettings.Sequence_Id__c = '11';
        upsert brmsAM_SeqIdsSettings;

        Opportunity application = LibraryTest.createApplicationTH();

        Test.startTest();

        CreditReport.set(application.id, LibraryTest.fakeBrmsRulesResponse(application.Id, true, '1', true));

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getCreditReportWS(application.id, true);

        Test.stopTest();

        //System.assertNotEquals(null, creditReportEntity);
    }

    @isTest static void gets_credit_report_ws_with_error() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        Opportunity application = LibraryTest.createApplicationTH();

        Test.startTest();

        CreditReport.set(application.id, LibraryTest.fakeBrmsRulesResponse(application.Id, true, '1', true));

        Test.stopTest();

        LibraryTest.errorMessage = true;
        LibraryTest.decisionPA = false;

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getCreditReportWS(application.id, false);

        //System.assertEquals(null, creditReportEntity);
        //System.assertEquals('No', creditReportEntity.automatedDecision);
    }

    @isTest static void regrade() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;
        ProxyApiCreditReportEntity creditReportEntity = LibraryTest.fakeCreditReportEntity(app.id);

        AcceptanceModelRequest__c acceptModel = new AcceptanceModelRequest__c();
        acceptModel.Opportunity__c = app.id;
        acceptModel.Decile__c = 10;
        acceptModel.PA__c = 0.12312;
        acceptModel.Score__c = 0.12312;
        insert acceptModel;

        ProxyApiCreditReportEntity result = CreditReport.reGrade(creditReportEntity);

        System.assertNotEquals(null, result);
    }

    @isTest static void regrade_created_date_higher_30days() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;

        DateTime createdDate = DateTime.now().addDays(-35);
        ProxyApiCreditReportEntity creditReportEntity = LibraryTest.fakeCreditReportEntity(app.id, createdDate.getTime());

        AcceptanceModelRequest__c acceptModel = new AcceptanceModelRequest__c();
        acceptModel.Opportunity__c = app.id;
        acceptModel.Decile__c = 10;
        acceptModel.PA__c = 0.12312;
        acceptModel.Score__c = 0.12312;
        insert acceptModel;

        Test.startTest();
        ProxyApiCreditReportEntity result = CreditReport.reGrade(creditReportEntity);
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @isTest static void gets_hard_pull() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Opportunity app = LibraryTest.createApplicationTH();

        ProxyApiCreditReportEntity result = CreditReport.getHardPullByEntityId(app.id);
        result = CreditReport.getHardPullByEntity(app.id);

        System.assertNotEquals(null, result);
    }

    @isTest static void is_meridian() {
        Opportunity app = LibraryTest.createApplicationTH();

        Test.setCreatedDate(app.id, Date.newInstance(2016, 05, 10));

        Test.startTest();

        Boolean result = CreditReport.isMeridianCreditReport(app.id);

        System.assertEquals(true, result);

        Test.stopTest();
    }

    @isTest static void gets_meridian_by_entity_id() {

        Opportunity app = LibraryTest.createApplicationTH();

        Test.setCreatedDate(app.id, Date.newInstance(2016, 05, 10));

        Test.startTest();

        ProxyApiCreditReportEntity result = CreditReport.getByEntityId(app.id);

        String NO_CREDIT_REPORT_ERROR = 'No credit report exists for this Application. Please click on the Soft Credit Pull pull button on Decisioning Tab to retrieve one from ATB';

        System.assertEquals(NO_CREDIT_REPORT_ERROR, result.errorMessage);

        Test.stopTest();
    }

    @isTest static void gets_info_from_api_with_military_designation() {
        LibraryTest.isMilitary = true;
        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        Opportunity application = LibraryTest.createApplicationTH();
        application.LeadSource__c = 'EZVerify';
        update application;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = application.Contact__c;
        insert partnerAct;

        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getInfoFromAPI(application.id, true, true, true);

        Opportunity app = [SELECT Id, Contact__r.Military__c FROM Opportunity WHERE Id = : application.id];

        System.assertNotEquals(app.Contact__r.Military__c, true);
    }

    @isTest static void gets_info_from_api_with_hawk_alerts() {
        LibraryTest.isHawkAlert = true;
        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        //Opportunity application = LibraryTest.createApplicationTH();

        //Opportunity app = [SELECT Id, Hawk_Alert_Codes__c FROM Opportunity WHERE Id = : application.id];
        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        app.Hawk_Alert_Codes__c = Null;
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;
        
        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getInfoFromAPI(app.id, true, true, true);

        System.assertEquals(app.Hawk_Alert_Codes__c, Null);
    }

    @isTest static void regrade_employment_details() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;

        Employment_Details__c empDetails = new Employment_Details__c();
        empDetails.Hire_Date__c = Date.today().addYears(-3);
        empDetails.Previous_Employment_End_Date__c = Date.today().addYears(-3);
        empDetails.Previous_Employment_Start_Date__c = Date.today().addYears(-5);
        empDetails.Opportunity__c = app.id;

        insert empDetails;

        ProxyApiCreditReportEntity creditReportEntity = LibraryTest.fakeCreditReportEntity(app.id);

        ProxyApiCreditReportEntity result = CreditReport.reGrade(creditReportEntity);

        System.assertNotEquals(null, result);
    }

    @isTest static void maps_decisioning_parameters_employment_details() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;

        Employment_Details__c empDetails = new Employment_Details__c();
        empDetails.Hire_Date__c = Date.today().addYears(-3);
        empDetails.Previous_Employment_End_Date__c = Date.today().addDays(-5).addYears(-3);
        empDetails.Previous_Employment_Start_Date__c = Date.today().addYears(-5);
        empDetails.Opportunity__c = app.id;

        insert empDetails;

        DecisioningParameter result = CreditReport.mapDecisioningParameter(app.id, Null);

        System.assertNotEquals(null, result);

        System.assertNotEquals('0', result.employmentDuration);
        System.assertNotEquals('0', result.employmentGapDurationDays);
        System.assertNotEquals('0', result.priorEmploymentDuration);
    }

    @isTest static void regrade_employment_details_with_contact() {

        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;

        Employment_Details__c empDetails = new Employment_Details__c();
        empDetails.Opportunity__c = app.id;

        insert empDetails;

        ProxyApiCreditReportEntity creditReportEntity = LibraryTest.fakeCreditReportEntity(app.id);

        ProxyApiCreditReportEntity result = CreditReport.reGrade(creditReportEntity);
        System.assertNotEquals(null, result);
    }

    @isTest static void maps_decisioning_parameters_employment_details_with_contact() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Opportunity app = LibraryTest.createApplicationTH();
        app.LeadSource__c = 'EZVerify';
        update app;
        
        Account partnerAct = new Account(name = 'PartnerAccount', Partner_Sub_Source__c = 'PartnerSubSource', Type = 'Partner');
        partnerAct.Is_Parent_Partner__c = true;
        partnerAct.Mailing_State_Province__c = 'AL';
        partnerAct.contact__c = app.Contact__c;
        insert partnerAct;

        Employment_Details__c empDetails = new Employment_Details__c();
        empDetails.Opportunity__c = app.id;

        insert empDetails;

        DecisioningParameter result = CreditReport.mapDecisioningParameter(app.id, Null);

        System.assertNotEquals(null, result);

        System.assertNotEquals('0', result.employmentDuration);
        System.assertEquals('0', result.employmentGapDurationDays);
        System.assertEquals('0', result.priorEmploymentDuration);
    }

    @isTest static void is_sandbox_assignmentutils() {
        Boolean result = AutomaticAssignmentUtils.isSandbox();
    }

    @isTest static void email_age_input_param() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        Opportunity app = LibraryTest.createApplicationTH();

        EmailAgeRequestParameter result = CreditReport.mapEmailAgeRequestParameter(app.Id);

        System.assertNotEquals(null, result);

        System.assertNotEquals('', result.email);
        System.assertNotEquals('', result.ip_address);
        System.assertEquals('', result.emailAgeStatusCode);

        EmailAgeRequestParameter contactResult = CreditReport.mapEmailAgeRequestParameter(app.Contact__c);

        System.assertNotEquals(null, contactResult);

        System.assertNotEquals('', contactResult.email);
        System.assertNotEquals('', contactResult.ip_address);
        System.assertEquals('', contactResult.emailAgeStatusCode);
        
        CreditReport.mapGDSRequestParameter(app.contact__c,false,false);

    }

    @isTest static void is_sandbox() {
        CreditReport.isSandBox();
    }

    @isTest static void sets_soft_pull_address() {

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        Opportunity app = LibraryTest.createApplicationTH();

        Contact cont = [SELECT id, MailingAddress FROM Contact WHERE id = : app.contact__c];

        cont.SoftPull_Address__c = JSON.serialize(cont.MailingAddress, true);
        update cont;

        CreditReportRequestParameter result = CreditReport.setRequestAddressParameter(cont, true);

        System.assertNotEquals(null, result);
    }

    @isTest static void gets_tax_liens_from_lexis_nexis() {
        Opportunity app = LibraryTest.createApplicationTH();
        CreditReport.prepareGetTaxLiensFromLexisNexis(app.id);
        LexisNexisResponse__c ln = [SELECT Id, Opportunity__c, AttachmentRefreshed__c, Declined__c, IsProcessed__c, ErrorMessage__c FROM LexisNexisResponse__c WHERE IsProcessed__c = false ORDER BY CreatedDate DESC LIMIT 1];
        CreditReport.getTaxLiensFromLexisNexis(ln);
    }

    @isTest static void gets_AcceptanceModel() {
        Opportunity app = LibraryTest.createApplicationTH();
        CreditReport.prepareGetAcceptanceModel(app.id);
        AcceptanceModelRequest__c am = [SELECT Id, Opportunity__c,  Decile__c, PA__c, Score__c, IsProcessed__c, ErrorMessage__c FROM AcceptanceModelRequest__c WHERE IsProcessed__c = false ORDER BY CreatedDate DESC LIMIT 1];
        CreditReport.getAcceptanceModel(am);
    }
    
    @isTest static void get_HardPullBy_Entity_Test() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        Opportunity app = LibraryTest.createApplicationTH();
        CreditReport.getHardPullByEntity(app.Id);
        
        CreditReport.getCreditReportWSfromContact(app.Id,false,false);
    } 
    @isTest static void getTaxLiensFromLexisNexisTest() {
        
        Opportunity app = LibraryTest.createApplicationTH();
        CreditReport.prepareGetTaxLiensFromLexisNexis(app.id);
        LexisNexisResponse__c ln = [SELECT Id, Opportunity__c, AttachmentRefreshed__c, Declined__c, IsProcessed__c, ErrorMessage__c FROM LexisNexisResponse__c WHERE IsProcessed__c = false ORDER BY CreatedDate DESC LIMIT 1];
        CreditReport.getTaxLiensFromLexisNexis(ln);
        CreditReport.getIncomeFromClarity(app.Id);
        CreditReport.getRegradeBureauByContactid(app.contact__c);
        CreditReport.getRegradeBureauByLeadid(app.contact__c);
        CreditReport.insertLexisNexisNote(app.Id,'Test','One');
        String ss;
        ss=CreditReport.DECISION_YES;
        ss=CreditReport.TRANSUNION;
        ss=CreditReport.EXPERIAN;
        ss=CreditReport.DECISION_NO;
        ss=CreditReport.DECISION_REVIEW;
        ss=CreditReport.NEW_APP_TYPE;
        ss=CreditReport.ATTACHMENT_FILE_NAME;
        ss=CreditReport.NO_CREDIT_REPORT_ERROR;
        ss=CreditReport.HARD_PULL_ATTACHMENT_FILE_NAME;
        ss=CreditReport.SEQUENCE_ID_HARD_PULL;
        ss=CreditReport.SEQUENCE_ID_SOFT_PULL;
        ss=CreditReport.SEQUENCE_ID_EMAIL_AGE;
        ss=CreditReport.SEQUENCE_ID_CLARITY;
        ss=CreditReport.SEQUENCE_ID_H2O;
        ss=CreditReport.SEQUENCE_ID_REGRADE;
        ss=CreditReport.SEQUENCE_ID_ACCEPTANCE_MODEL;
        ss=CreditReport.SEQUENCE_ID_LN_TL;
        ss=CreditReport.SEQUENCE_ID_LH;
    }

}