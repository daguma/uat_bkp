public with sharing class CSIWatchListCheckCtrl {
    
    
    public class CSICR_Entity {
        @AuraEnabled public string name {get;set;}
        @AuraEnabled public DateTime createdDate {get;set;}
        @AuraEnabled public string CSIRCDReview_Status {get;set;}
        @AuraEnabled public Decimal CSIRCDNumber_of_Hits {get;set;}
        @AuraEnabled public string CSIRCDComment {get;set;}
        @AuraEnabled public string CSIRCDListDescription {get;set;}
        @AuraEnabled public Date CSIRCDListDatec {get;set;}
        @AuraEnabled public string CSIRCDStatusc {get;set;}
        
    }
    
    @AuraEnabled
    public static List<CSICR_Entity> getCSIDetails(Id oppId) {
        List<CSICR_Entity> resultlist = new List<CSICR_Entity>();
        CSIWatchListCheckCtrl.CSICR_Entity bl = new CSIWatchListCheckCtrl.CSICR_Entity();
        bl.CSIRCDComment = '';
        
        if (String.isNotEmpty(oppId)) {
            
            for (CSIRCD__ATTUSWDSFLookup__c res : [SELECT   name
                                                   , createdDate
                                                   , CSIRCD__Review_Status__c
                                                   , CSIRCD__Number_of_Hits__c
                                                   , CSIRCD__Comment__c
                                                   , CSIRCD__List_Description__c
                                                   , CSIRCD__List_Date__c
                                                   , CSIRCD__Status__c
                                                   FROM CSIRCD__ATTUSWDSFLookup__c
                                                   WHERE CSIRCD__Contact__c IN (SELECT Contact__c
                                                                                 FROM Opportunity
                                                                                 WHERE id = : oppId )
                                                   ORDER BY CreatedDate DESC]){
                                                       bl = new CSICR_Entity();
                                                       bl.name = res.name;
                                                       bl.createdDate = res.createdDate;
                                                       bl.CSIRCDReview_Status = res.CSIRCD__Review_Status__c;
                                                       bl.CSIRCDNumber_of_Hits = res.CSIRCD__Number_of_Hits__c;
                                                       bl.CSIRCDComment = res.CSIRCD__Comment__c;
                                                       bl.CSIRCDListDescription = res.CSIRCD__List_Description__c;
                                                       bl.CSIRCDListDatec = res.CSIRCD__List_Date__c;
                                                       bl.CSIRCDStatusc = res.CSIRCD__Status__c;
                                                       resultList.add(bl);
                                                   }
        }
        System.debug('CSI ' + resultList);
        if (resultList.isEmpty()) resultList.add(bl);
        return resultList;
        
    }
    
}