@isTest
private class CaseAutomaticSummaryCtrlTest {

	@isTest static void gets_all_users() {

		CaseAutomaticSummaryCtrl CASC = new CaseAutomaticSummaryCtrl();
		DealAutomaticUserGroup group1 = new DealAutomaticUserGroup('005U0000004YXGiIAO', 'Kimberly Gardner', '00GU0000002ccJvMAI', 'Kennesaw Sales Center', true, false);
		List<DealAutomaticUserGroup> DAUG = CaseAutomaticSummaryCtrl.getAllUsers();
		System.assert(DAUG.size() > 0);
	}

	@isTest static void saves_User_On_line_Mode() {

		List<User> user1 = [Select Id, Name, IsActive, isOnline__c From User LIMIT 1];
		CaseAutomaticSummaryCtrl.saveUserOnlineMode(user1.get(0).id, true);
		User user2 = [SELECT isOnline__c FROM User WHERE id = : user1.get(0).id LIMIT 1];
		System.assertEquals(true, user2.isOnline__c);

	}

}