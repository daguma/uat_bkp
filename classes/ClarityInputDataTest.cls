@isTest
private class ClarityInputDataTest {
	
	@isTest static void sets_variables(){
        
        ClarityRequestParameter clarityRequest = new ClarityRequestParameter();
        
        clarityRequest.bank_account_number = '1234';
        clarityRequest.bank_account_type = 'ACH';
        
        ClarityInputData clarityInput = new ClarityInputData();
        
        clarityInput.clarityRequestParameter = clarityRequest;
        clarityInput.creditReportId = 123;
        
        System.assertNotEquals(null, clarityInput.clarityRequestParameter);
        System.assertEquals(123, clarityInput.creditReportId);
        
    }
	
}