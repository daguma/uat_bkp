@isTest public class ProxyApiCreditReportEntityTest {

    @isTest static void validate_instance() {

        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        Test.stopTest();

        cre.id = 10;
        System.assertEquals(10, cre.id);

        cre.entityId = 'test data';
        System.assertEquals('test data', cre.entityId);

        cre.entityName = 'test data';
        System.assertEquals('test data', cre.entityName);

        cre.isSoftPull = true;
        System.assertEquals(true, cre.isSoftPull);

        cre.isSoftPull = false;
        System.assertEquals(false, cre.isSoftPull);

        cre.datasourceName = 'test data';
        System.assertEquals('test data', cre.datasourceName);

        cre.createdDate = 1182592983000L;
        System.assertEquals(1182592983000L, cre.createdDate);

        cre.reportText = 'test data';
        System.assertEquals('test data', cre.reportText);

        cre.requestData = 'test data';
        System.assertEquals('test data', cre.requestData);

        cre.errorMessage = 'test data';
        System.assertEquals('test data', cre.errorMessage);

        cre.errorDetails = 'test data';
        System.assertEquals('test data', cre.errorDetails);

        cre.transactionId = 'transactionId';
        System.assertEquals('transactionId', cre.transactionId);
        
        List<ProxyApiCreditReportAttributeEntity> attributesListTest = new List<ProxyApiCreditReportAttributeEntity>();
        List<ProxyApiCreditReportSegmentEntity> segmentsListTest = new List<ProxyApiCreditReportSegmentEntity>();

        System.assertEquals(attributesListTest, cre.attributesList);
        System.assertEquals(segmentsListTest, cre.segmentsList);
    }

    @isTest static void validate_name() {

        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        Test.stopTest();

        cre.id = 10;

        System.assertEquals('CR-00000010', cre.name);

        cre.id = null;
        System.assertEquals(' - ', cre.name);
    }

    @isTest static void validate_automated_decision() {

        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        cre.decision = true;

        System.assertEquals('Yes', cre.automatedDecision);

        List<ProxyApiCreditReportAttributeEntity> attributesListTest = LibraryTest.fakeCreditReportAttributeEntity();
        List<ProxyApiCreditReportSegmentEntity> segmentsListTest = LibraryTest.fakeCreditReportSegmentEntity();
        List<ClarityClearRecentHistory> clearRecentHistoryTest = LibraryTest.fakeCreditReportClarityClearRecentHistory();
        

        cre.attributesList = attributesListTest;

        System.assertEquals('Yes', cre.automatedDecision);

        attributesListTest = LibraryTest.fakeCreditReportAttributeEntityFalse();

        cre.attributesList = attributesListTest;
        cre.decision = false;

        System.assertEquals('No', cre.automatedDecision);
    }

    @isTest static void validate_attributes() {
        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        Test.stopTest();

        System.assertEquals('Automated', cre.attributesApprovedByName);

        List<ProxyApiCreditReportAttributeEntity> attributesListTest = LibraryTest.fakeCreditReportAttributeEntity();
        List<ProxyApiCreditReportSegmentEntity> segmentsListTest = LibraryTest.fakeCreditReportSegmentEntity();

        cre.attributesList = attributesListTest;
        cre.segmentsList = segmentsListTest;

        System.assertEquals('Automated', cre.attributesApprovedByName);

        attributesListTest = LibraryTest.fakeCreditReportAttributeEntityDigDeeper();
        cre.attributesList = attributesListTest;

        System.assertEquals('Automated by Dig Deeper', cre.attributesApprovedByName);

        attributesListTest = LibraryTest.fakeCreditReportAttributeEntityOther();
        cre.attributesList = attributesListTest;

        System.assertEquals('Manual by Other', cre.attributesApprovedByName);

    }

    @isTest static void get_other_attribute_list() {
        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        Test.stopTest();

        System.assertNotEquals(null, cre.getOtherAttributeList());
    }

    @isTest static void validate_creation_date() {
        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        Test.stopTest();

        cre.createdDate = null;
        System.assertEquals(null, cre.creationDate);

        cre.createdDate = 1182592983000L;
        DateTime myDate = DateTime.newInstance(cre.createdDate);
        System.assertEquals(myDate, cre.creationDate);
    }

    @isTest static void validate_error_response() {
        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        Test.stopTest();

        cre.errorMessage = 'error details';
        System.assertEquals(true, cre.errorResponse);

        cre.errorMessage = null;
        System.assertEquals(false, cre.errorResponse);
    }

    @isTest static void validate_get_attribute() {
        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        
        Test.stopTest();

        String getAttValue = cre.getAttributeValue(0);
        System.assertEquals('0', getAttValue);

        List<ProxyApiCreditReportAttributeEntity> attributesListTest = LibraryTest.fakeCreditReportAttributeEntity();
        List<ProxyApiCreditReportSegmentEntity> segmentsListTest = LibraryTest.fakeCreditReportSegmentEntity();

        cre.attributesList = attributesListTest;
        cre.segmentsList = segmentsListTest;

        getAttValue = cre.getAttributeValue(0);
        System.assertEquals('0', getAttValue);

        getAttValue = cre.getAttributeValue(1);
        System.assertEquals('720', getAttValue);

        Decimal decValue = cre.getDecimalAttribute(1);
        System.assertEquals(720, decValue);

        cre.attributesList = null;
        getAttValue = cre.getAttributeValue(0);
        System.assertEquals(' - ', getAttValue);

        decValue = cre.getDecimalAttribute(0);
        System.assertEquals(0, decValue);

        ProxyApiCreditReportAttributeEntity result = cre.getAttributeEntity(0);
        System.assertEquals(null, result);

        cre.attributesList = attributesListTest;
        BRMS_And_ATB_Rules_Mapping__c settings= new BRMS_And_ATB_Rules_Mapping__c(Attributes_List_Id__c='1', BRMS_Rule_Number__c='2', name='FICO');
        result = cre.getAttributeEntity(0);
        System.assertEquals(null, result);

        result = cre.getAttributeEntity(1);
        System.assertNotEquals(null, result);
        
        cre.setAttributeValue(1,'700');
    }

    @isTest static void validate_get_created_date() {
        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        Test.stopTest();

        Datetime dateValue = cre.getCreatedDate();
        System.assertEquals(null, dateValue);

        cre.createdDate = 1182592983000L;
        Datetime cd = Datetime.newInstance(cre.createdDate);
        System.assertEquals(cd, cre.getCreatedDate());
    }

    @isTest static void validate_created_last() {
        Test.startTest();
        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        Test.stopTest();

        System.assertEquals(false, cre.isCreateDateOnLast30Days());

        DateTime myDateTime = DateTime.now();
        DateTime subsDate = myDateTime.addDays(-20);

        cre.createdDate = subsDate.getTime();
        System.assertEquals(true, cre.isCreateDateOnLast30Days());

        subsDate = myDateTime.addDays(-40);
        cre.createdDate = subsDate.getTime();
        System.assertEquals(false, cre.isCreateDateOnLast30Days());
    }

    @isTest static void validate_init() {

        DateTime myDateTime = DateTime.now();
        DateTime subsDate = myDateTime.addDays(-15);

        ProxyApiCreditReportEntity pacre = new ProxyApiCreditReportEntity(2, 'name test value', true, 'Yes', subsDate);
        pacre.decision = true;

        System.assertEquals(2, pacre.id);
        System.assertEquals('CR-00000002', pacre.name);
        System.assertEquals(true, pacre.isSoftPull);
        System.assertEquals('Yes', pacre.automatedDecision);
        System.assertEquals(null, pacre.creationDate);

        pacre.createdDate = subsDate.getTime();
        System.assertEquals(subsDate, pacre.creationDate);

        System.assertEquals(' - ', pacre.attributesApprovedByName);
        String getAttValue = pacre.getAttributeValue(0);
        System.assertEquals(' - ', getAttValue);
    }

    @isTest static void validate_offerCatalogList() {

        ProxyApiOfferCatalog off = new ProxyApiOfferCatalog();
        off.offerCatalogId = 1;
        off.term = 1;
        off.loanAmount = 100;
        off.grade = 'D';
        off.maxPti = 1;
        off.annualIncome = 1000;
        off.paymentAmount = 1;
        off.state = 'OH';
        off.apr = 1;
        off.feeAmount = 1;
        off.maxFundingAmount = 120;
        off.partner = 1;

        List<ProxyApiOfferCatalog> loff = new List<ProxyApiOfferCatalog>();
        loff.add(off);

        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        cre.offerCatalogList = loff;

        System.assertEquals(loff, cre.offerCatalogList);
    }
   

    @isTest static void validate_scorecardEntity() {

        ProxyApiScorecardEntity score = new ProxyApiScorecardEntity();
        score.annualIncome = -1;
        score.dtiRatio = 0.1;
        score.selfEmployed = 1;
        score.inquiries = 1;
        score.creditScore = 10;
        score.dollarRateBadProb = 0;
        score.totalTradeLines = 10;
        score.grade = 'D';
        score.acceptance = 'N';
        score.highestBalance = 1;
        score.totalOpenInstallments = 1;
        score.logit = 0;
        score.unitBadProb = 0;
        score.maximumPaymentAmount = 200;

        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity();
        cre.scorecardEntity = score;

        System.assertEquals(score, cre.scorecardEntity);
    }

    @isTest static void validate_error() {

        ProxyApiCreditReportEntity cre = new ProxyApiCreditReportEntity('test error message', 'test error details');
        System.assertEquals('test error message', cre.errorMessage);
        System.assertEquals('test error details', cre.errorDetails);
    }
    /* @isTest static void validate_get_attributeBRMSAttri() {
        BRMS_And_ATB_Rules_Mapping__c settings= new BRMS_And_ATB_Rules_Mapping__c(Attributes_List_Id__c='1', BRMS_Rule_Number__c='2', name='FICO');
        Test.startTest();
       // List<ProxyApiCreditReportEntity> cre = LibraryTest.fakeCreditReportAttributeEntitybrms();
        
        Test.stopTest();

       // ProxyApiCreditReportAttributeEntity result = cre[0].getAttributeEntity(0);

        ProxyApiCreditReportEntity.BRMSATBMapping();
        
       // result = cre[0].getAttributeEntity(0);

       // result = cre[0].getAttributeEntity(1);
    }
*/
}