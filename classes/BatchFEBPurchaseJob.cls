global class BatchFEBPurchaseJob implements Database.Batchable<sObject>,Schedulable , Database.Stateful{
    DateTime cutOffDate = null, newDate = DateTime.now();
    Integer count =0, daysToPurchase = 3;
    String query = '', purchaseLine = '2018 Funding Trust';
	FEB_Details__c FEBParams = FEB_Details__c.getInstance();   
    Decimal totalAmount = 0;

    public void calculateDate() {
        integer bd = daysToPurchase;
        Date toda = CalculateWorkDays.dateWorkDays(Date.today(), -bd);
        cutOffDate = DateTime.newInstance(toda.year(), toda.month(), toda.day());
        System.debug('CutOff Date = ' + cutOffDate );
        
    }
  	public void execute(SchedulableContext SC){
         BatchFEBPurchaseJob batchapex=new BatchFEBPurchaseJob();
        Id batchId=Database.executeBatch(batchapex, 10);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        calculateDate();
        query = 'select id, CreatedDate, Asset_Sale_Date__C, Asset_Sale_line__c, loan__Principal_remaining__c from Loan__Loan_Account__c where ASset_Sale_Line__C = \'FEB\' and loan__Loan_Status__c like \'Active%\' and Asset_Sale_Date__C <= : cutOffDate ';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC,LIST<loan__Loan_Account__c> scope){
        List<loan__Loan_Account__c> clUpdate = new List<loan__Loan_Account__c>();
        
        for(loan__Loan_Account__c clContract:scope){
            clContract.Asset_Sale_Line__c = purchaseLine;
            clContract.Asset_Sale_Date__c = newDate;
            clCOntract.Asset_Sale_Balance__c = clContract.loan__Principal_remaining__c;
            totalAmount = totalAmount  + clContract.loan__Principal_remaining__c;
            clUpdate.add(clContract);
            count++;
        }
        try{
            if (clUpdate.size() > 0) update clUpdate;
        }
        catch(exception e){
            WebToSFDC.notifyDev('BatchFEBPurchaseJob Error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
    }
    global void finish(Database.BatchableContext BC){
         WebToSFDC.notifyDev('BatchFEBPurchaseJob Finished', 'Deals purchased = ' + count + '\nTotal Amount = ' + totalAmount.setScale(2) + '\n\n',  UserInfo.getUserId() );
 
    }

}