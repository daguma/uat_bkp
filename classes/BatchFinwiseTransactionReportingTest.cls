@IsTest(seeAllData=false)
public class BatchFinwiseTransactionReportingTest {
    public static loan__Payment_Mode__c createPaymentMode(){
        loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c(name='ACH');
        insert paymentMode;
        return paymentMode;
    }
    public static FinwiseDetails__c createFinwiseDetails(){
        FinwiseDetails__c finwiseDetails=new FinwiseDetails__c();
        finwiseDetails.Name='finwise';
        finwiseDetails.No_of_Days_back_Transaction_batch__c=2;
        finwiseDetails.Transaction_Type_Id__c=5;
        finwiseDetails.Transaction_Reversed_Status_Id__c=6;
        finwiseDetails.Transaction_Cleared_Status_Id__c=3;
        finwiseDetails.Finwise_product_Key__c='151478testingbatchFinwiseTransactionReport151478';
        finwiseDetails.Finwise_Base_URL__c='https://mpl-lab.finwisebank.com/Services/';
        insert finwiseDetails;
        return finwiseDetails;
    }
    public static loan__Loan_Payment_Transaction__c createClearedLoanTransactions(loan__Loan_Account__c cl,loan__Payment_Mode__c payMode){
        loan__Loan_Payment_Transaction__c payTrans=new loan__Loan_Payment_Transaction__c();
        payTrans.loan__Payment_Mode__c=payMode.id;
        payTrans.loan__Loan_Account__c=cl.id;
        payTrans.loan__Transaction_Amount__c=200;
        payTrans.loan__Principal__c=1500;
        payTrans.loan__Fees__c=50;
        payTrans.loan__Interest__c=200;
        payTrans.loan__Cleared__c=true;
        payTrans.loan__Clearing_Date__c=datetime.now().addDays(-2);
        insert payTrans;
        return payTrans;
    }
    public static loan__Loan_Payment_Transaction__c createReversedLoanTransactions(loan__Loan_Account__c cl,loan__Payment_Mode__c payMode){
        loan__Loan_Payment_Transaction__c payTrans=new loan__Loan_Payment_Transaction__c();
        payTrans.loan__Payment_Mode__c=payMode.id;
        payTrans.loan__Loan_Account__c=cl.id;
        payTrans.loan__Transaction_Amount__c=200;
        payTrans.loan__Principal__c=1500;
        payTrans.loan__Fees__c=50;
        payTrans.loan__Interest__c=200;
        payTrans.loan__Reversed__c=true;
        payTrans.Reporting_Reversed_Date__c=datetime.now().addDays(-2);
        insert payTrans;
        return payTrans;
    }
    
    static testmethod void batchTesting(){
        loan__Payment_Mode__c pm=BatchFinwiseTransactionReportingTest.createPaymentMode();
        FinwiseDetails__c fd= BatchFinwiseTransactionReportingTest.createFinwiseDetails();
        loan__Loan_Account__c clContract = TestHelper.createContract();
        clContract=[select id,Opportunity__r.id,loan__Disbursal_Date__c,loan__Interest_Rate__c,loan__Fees_Paid__c,loan__Principal_Paid__c,loan__Principal_Remaining__c,loan__Loan_Status__c,loan__Disbursed_Amount__c from loan__Loan_Account__c where id=:clContract.Id];
        clContract.loan__Loan_Status__c='Active - Matured';
        clContract.loan__Disbursed_Amount__c=150;
        clContract.loan__Principal_Remaining__c=500;
        clContract.loan__Principal_Paid__c=1000;
        clContract.loan__Fees_Paid__c=50;
        clContract.loan__Interest_Rate__c=5;
        clContract.loan__Disbursal_Date__c=date.valueOf(datetime.now().addDays(-5));
        upsert clContract;
        Opportunity app=new Opportunity(id=clContract.Opportunity__r.id,Finwise_Approved__c=true);
        upsert app;
        LIST<loan__Loan_Payment_Transaction__c> loanPaymentTrans=new LIST<loan__Loan_Payment_Transaction__c>();
        loanPaymentTrans.add(BatchFinwiseTransactionReportingTest.createReversedLoanTransactions(clContract,pm));
        loanPaymentTrans.add(BatchFinwiseTransactionReportingTest.createClearedLoanTransactions(clContract,pm));
        Test.startTest();
            BatchFinwiseTransactionReporting b=new BatchFinwiseTransactionReporting();
            Database.executeBatch(b);
        Test.stopTest();
    }
}