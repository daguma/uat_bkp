public class Decisioning {
    public static void applicationStatusAutomation(Opportunity theApp, String decision) {
        if (theApp != null) {
            system.debug('---applicationStatusAutomation--'+theApp.status__c);
            if (theApp.status__c == 'Pending Credit Data' && CreditReport.getAttachment(theApp.Id) == null) return;

            String appStatus = theApp.status__c;
            
            if (decision.equalsIgnoreCase('No')) {
                theApp.status__c = 'Declined (or Unqualified)';
                theApp.Reason_of_Opportunity_Status__c = 'Did not pass disqualifiers';
                theApp.Probability = 0;
                theApp.StageName = 'Closed Lost';
            } else if (decision.equalsIgnoreCase('Review')) {
                theApp.status__c = 'Credit Review';
                theApp.Probability = 35;
                theApp.StageName = 'Needs Analysis';
            } else if (decision.equalsIgnoreCase('Yes')) {
                Scorecard__c scorecard = ScorecardUtil.getScorecard(theApp.Id);

                if (scorecard != null) {
                    if (scorecard.Acceptance__c == 'Y') {
                        theApp.status__c = 'Credit Qualified';
                        if(theApp.sub__c!=null) theApp.sub__c = null;

                        if (theApp.Partner_Account__c != null && String.isEmpty(theApp.Selected_Offer_Id__c) && theApp.Partner_Account__r.Contact_Restricted_Partner__c) {
                            theApp.status__c = 'Credit Approved - No Contact Allowed';
                        }
                    } else {
                        theApp.status__c = 'Declined (or Unqualified)';
                        theApp.Reason_of_Opportunity_Status__c = 'Updated Scoring – No Offer';
                        if(theApp.sub__c!=null) theApp.sub__c = null;
                        theApp.Probability = 0;
                        theApp.StageName = 'Closed Lost';
                    }
                }
            }
        }
    }

    public static void applicationStatusAutomation(String appId, String decision) {
        try {
            Opportunity theApp = [ SELECT  Id,
                    status__c,
                    Reason_of_Opportunity_Status__c,
                    Partner_Account__c,
                    Selected_Offer_Id__c,
                    Partner_Account__r.Contact_Restricted_Partner__c,
                    Lending_Product__c,
                    Lending_Product__r.Name,
                    contact__c,
                    contact__r.Product__c,StageName,Probability
            FROM    Opportunity
            WHERE   Id = : appId];

            applicationStatusAutomation(theApp, decision);

            try {
                update theApp;
                system.debug('---applicationStatusAutomation after update--'+theApp.status__c);
            } catch (Exception e) {
                Insert new Logging__c(Webservice__c = 'Decisioning.applicationStatusAutomation error',
                        API_Request__c = String.valueOf(theApp),
                        API_Response__c = e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString(),
                        Opportunity__c = theApp.id);
            }
        } catch (Exception e) {
            system.debug('DECISIONING automatedAppDecision: ' + e.getMessage());
        }
    }

    public static void declineByRiskBand(String appId, ProxyApiCreditReportEntity creditReportEntity) {
        try {
            Opportunity theApp = [ SELECT  Id,
                    status__c,
                    contact__c,
                    Reason_of_Opportunity_Status__c
            FROM    Opportunity
            WHERE   Id = : appId];
            if (theApp != null) {
                Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
                if (emailAgeSettings != null) {
                    if (emailAgeSettings.Call_Email_age__c) {
                        String status = EmailAgeStorageController.getStatusCode(theApp.contact__c);

                        if ((status == '2' || status == '1') && creditReportEntity.automatedDecision.equalsIgnoreCase('No')) {
                            List<Email_Age_Details__c> emailAgeDetails = EmailAgeStorageController.getEmailAgeRecord(theApp.id);

                            if (emailAgeDetails.size() > 0) {
                                if (creditReportEntity.decision && !emailAgeDetails[0].Email_Age_Status__c && emailAgeDetails[0].errorCode__c == '0') {
                                    theApp.status__c = 'Declined (or Unqualified)';
                                    theApp.Reason_of_Opportunity_Status__c = 'Cannot Verify Information Provided';
                                    update theApp;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            system.debug('DECISIONING declineByRiskBand: ' + e.getMessage());
        }
    }

    /**
    * Checks if the FICO score decrease from a soft pull to hard pull
    * @param  softCreditReportEntity [description]
    * @param  hardCreditReportEntity [description]
    * @return true if the FICO score decrease, otherwise false
    */
    public static Boolean ficoScoreDecrease(ProxyApiCreditReportEntity softCreditReportEntity,
            ProxyApiCreditReportEntity hardCreditReportEntity) {

        Boolean result = false;

        if (softCreditReportEntity != null && softCreditReportEntity.errorMessage == null &&
                hardCreditReportEntity != null && hardCreditReportEntity.errorMessage == null) {
            Decimal softFICO = softCreditReportEntity.getDecimalAttribute(CreditReport.FICO);
            Decimal hardFICO = hardCreditReportEntity.getDecimalAttribute(CreditReport.FICO);

            Map<String, FICO_Drop_Rules__c> dropRulesMap = FICO_Drop_Rules__c.getAll();

            for (FICO_Drop_Rules__c rule : dropRulesMap.values()) {
                if (softFICO >= rule.Min_Limit__c && softFICO <= rule.Max_Limit__c) {
                    result = (softFICO - hardFICO) >= rule.Drop_Points__c ;
                    if (result) break;
                }
            }
        }

        return result;
    }

    /**
    * Evaluate clarity api
    * @param hardCreditReportEntity [description]
    * @param clarityInquiryEntity [description]
    * @param [application [description]
    */
    public static void EvalutateClarity(ProxyApiCreditReportEntity hardCreditReportEntity,
            ProxyApiClarityInquiryEntity clarityInquiryEntity,
            Opportunity application) {

        if (clarityInquiryEntity != null && clarityInquiryEntity.errorMessage == null) {
            application.Clarity_Status__c = clarityInquiryEntity.isApproved;
            application.Clarity_Fraud_Score__c = clarityInquiryEntity.fraudScore;
            application.Clarity_Fraud_Crosstab_Multiple__c = clarityInquiryEntity.fraudCrosstabMultiple;
            application.Clarity_Fraud_Reason_Code__c = clarityInquiryEntity.reasonCodeDescription;
            application.Clarity_Submitted__c = true;

            if (!clarityInquiryEntity.isApproved) {

                application.status__c = 'Declined (or Unqualified)';
                application.Reason_of_Opportunity_Status__c = 'Cannot Verify Information Provided';

                String product = string.isNotEmpty(application.ProductName__c) ? application.ProductName__c : (application.contact__c != null ? application.contact__r.Product__c : ''); //4673
                String declinestatus = 'Application is Declined (or Unqualified)';

                if (((generalPurposeWebServices.isPointOfNeed(product) && generalPurposeWebServices.isEZVerify(application.LeadSource__c)) || generalPurposeWebServices.isSelfPayProduct(application.productName__c)) && (System.isFuture() || System.isBatch())) //SM-438 //API-148
                    generalPurposeWebServices.ezVerifyNotificationInSync(new List<ID>{application.id}, 'Declined', 'Application is Declined (or Unqualified)');

            }

            try {
                update application;
            } catch (Exception e) {
                Insert new Logging__c(Webservice__c = 'Decisioning.EvalutateClarity error',
                        API_Request__c = String.valueOf(application),
                        API_Response__c = e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString(),
                        Opportunity__c = application.id);
            }
        }
    }

    /**
    * Decline an application because the FICO score decrease
    * @param application [description]
    */
    public static void declineByFicoScoreDecrease(String appId) {
        declineApplication(appId, 'FICO Score Decrease');
        ScorecardUtil.deleteOffers(appId);
    }

    /**
    * Decline an application because the disqualifiers not pass at hard pull
    * @param app [description]
    */
    public static void declineByHardSecondLook(Opportunity app) {
        declineApplication(app.Id, 'Did not pass disqualifiers at Hard Pull');
    }

    /**
    * Declines an application by a specific reason
    * @param appId         [description]
    * @param declineReason [description]
    */
    public static void declineApplication(String appId, String declineReason) {
        Opportunity app = null;

        try {
            app = [ SELECT  Id,
                    Name,
                    status__c,
                    Reason_of_Opportunity_Status__c
            FROM    Opportunity
            WHERE   Id = : appId];

            app.status__c = 'Declined (or Unqualified)';
            app.Reason_of_Opportunity_Status__c = declineReason;
            update app;
        } catch (Exception e) {
            Insert new Logging__c(Webservice__c = 'Decisioning.declineApplication error',
                    API_Request__c = String.valueOf(app),
                    API_Response__c = e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString(),
                    Opportunity__c = app.id);

            String body = 'Error when trying to decline the application: ' + appId + ', with decline reason: ' + declineReason +
                    '\n \n' + 'Message: ' + e.getMessage() + '\n \n' + 'Stack Trace: ' + e.getStackTraceString();

            WebToSFDC.notifyDev('Error when trying to decline the application: ' + app.Name, body, SalesForceUtil__c.getOrgDefaults().LPTeamId__c);
        }
    }

    public static Opportunity applicationStatusAutomationThruBRMS(String appId,
            ProxyApicreditReportEntity creditReportEntity, Opportunity app) {
        Opportunity theApp = app;
        try {
            if (app == null)
                theApp = CreditReport.getOpportunity(appId);

            if (theApp != null) {
                if (theApp.status__c == 'Pending Credit Data' && creditReportEntity == null)
                    return theApp;
                system.debug('==applicationStatusAutomationThruBRMS==' );
                // CNR-3
                //system.debug('==creditReportEntity.clearRecentHistory==' + creditReportEntity.clearRecentHistory);
                if(creditReportEntity != null && creditReportEntity.clearRecentHistory !=null && !isClarityClearRecentHistoryPassed(creditReportEntity.clearRecentHistory)){
                    theApp.status__c = 'Declined (or Unqualified)';
                    theApp.Reason_of_Opportunity_Status__c = 'Does not meet program parameters';
                    theApp.sub__c= 'Clear Recent History Decline';
                }
                else{
                    /*List<Opportunity> opps = [  SELECT  Id,
                            StageName
                    FROM    Opportunity
                    WHERE   id = : theApp.Id];

                    Opportunity_Users__c cuSettings = Opportunity_Users__c.getOrgDefaults();

                    Opportunity theOpp = (opps.size() > 0) ? opps[0] : null; */

                    Map<String, list<ProxyApiCreditReportRuleEntity>> ruleMap = new Map<String, list<ProxyApiCreditReportRuleEntity>>();
                    List<ProxyApiCreditReportRuleEntity> ruleList;

                    if (creditReportEntity.brms != null && creditReportEntity.brms.rulesList != null && !creditReportEntity.brms.autoDecision.equalsIgnoreCase(' ')) {
                        for (ProxyApiCreditReportRuleEntity item : creditReportEntity.brms.rulesList) {
                            if (ruleMap.containsKey(item.deRuleStatus)) {
                                ruleList = ruleMap.get(item.deRuleStatus);
                                ruleList.add(item);
                            } else {
                                ruleList = new List<ProxyApiCreditReportRuleEntity>();
                                ruleList.add(item);
                            }
                            ruleMap.put(item.deRuleStatus, ruleList);
                        }
                    }

                    if (creditReportEntity.brms != null) {

                        theApp.StrategyType__c = creditReportEntity.brms.deStrategy_Type != null ? creditReportEntity.brms.deStrategy_Type : '';
                        if (ruleMap.containsKey('Fail')) {
                            ProxyApiCreditReportRuleEntity brmsRule = returnHighestPriorityRule(ruleMap.get('Fail'));
                            if (!String.isBlank(creditReportEntity.brms.deBRMS_ContractStatus)) {
                                if (creditReportEntity.brms.deBRMS_ContractStatus.containsIgnoreCase('Review')) {
                                    theApp.status__c = creditReportEntity.brms.deBRMS_ContractStatus;
                                    theApp.Review_Reason__c = brmsRule.deBRMS_ReasonCode;
                                    theApp.Reason_of_Opportunity_Status__c = '';
                                } else if (creditReportEntity.brms.deBRMS_ContractStatus.containsIgnoreCase('Declined')) {
                                    theApp.status__c = creditReportEntity.brms.deBRMS_ContractStatus;
                                    theApp.Reason_of_Opportunity_Status__c = brmsRule.deBRMS_ReasonCode;
                                    theApp.Decline_Note__c = brmsRule.deBRMS_ReasonCode;
                                    theApp.Review_Reason__c = '';
                                    if (!creditReportEntity.isSoftPull) {
                                        if (!String.isBlank(brmsRule.deBRMS_ReasonCode) && brmsRule.deBRMS_ReasonCode.containsIgnoreCase('Did not pass disqualifiers'))
                                            theApp.Reason_of_Opportunity_Status__c = brmsRule.deBRMS_ReasonCode + ' at Hard Pull';
                                    }
                                } else if (creditReportEntity.brms.deBRMS_ContractStatus.containsIgnoreCase('Qualified')) {
                                    theApp.Reason_of_Opportunity_Status__c = '';
                                    theApp.Review_Reason__c = '';

                                    if (creditReportEntity.isSoftPull) {
                                        theApp.status__c = creditReportEntity.brms.deBRMS_ContractStatus;
                                        if (theApp.Partner_Account__c != null &&
                                                theApp.OwnerId == Label.Do_Not_Contact_White_listed_User &&
                                                theApp.Partner_Account__r.Contact_Restricted_Partner__c)
                                            theApp.status__c = 'Credit Approved - No Contact Allowed';
                                        if(theApp.sub__c!=null) theApp.sub__c = null;
                                    }
                                }
/*
                                if (creditReportEntity.isSoftPull && cuSettings.User_Ids__c != null &&  cuSettings.User_Ids__c.contains(UserInfo.getUserId()) && theOpp != null) {
                                    if (creditReportEntity.brms.deBRMS_ContractStatus.containsIgnoreCase('Decline')) {
                                        theApp.StageName = 'Closed Lost';
                                        theApp.Probability = 0;
                                    } else if (creditReportEntity.brms.deBRMS_ContractStatus.containsIgnoreCase('Review')) {
                                        theApp.StageName = 'Needs Analysis';
                                        theApp.Probability = 35;
                                    }
                                    //FIXupdate theOpp;
                                }
*/
                            }
                        } else if (ruleMap.containsKey('Pass')) {
                            if (creditReportEntity.isSoftPull) {
                                theApp.status__c = creditReportEntity.brms.deBRMS_ContractStatus;
                                theApp.Reason_of_Opportunity_Status__c = '';
                                theApp.Review_Reason__c = '';
                                if (theApp.Partner_Account__c != null &&
                                        theApp.OwnerId == Label.Do_Not_Contact_White_listed_User &&
                                        theApp.Partner_Account__r.Contact_Restricted_Partner__c)
                                    theApp.status__c = 'Credit Approved - No Contact Allowed';
                                if(theApp.sub__c!=null) theApp.sub__c = null;
                            }
                        } else {
                            theApp.status__c = 'Declined (or Unqualified)';
                            theApp.Reason_of_Opportunity_Status__c = 'Did not pass disqualifiers';
                            if(theApp.sub__c!=null) theApp.sub__c = null;
                        }
                    } else {
                        theApp.status__c = 'Declined (or Unqualified)';
                        theApp.Reason_of_Opportunity_Status__c = 'Did not pass disqualifiers';
                        if(theApp.sub__c!=null) theApp.sub__c = null;
                    }
                }
            } else {
                System.debug('Decisioning->applicationStatusAutomation() ' +
                        'There is no credit report related to this application id = ' + appId);
            }
        } catch (Exception e) {
            system.debug('DECISIONING automatedAppDecision: ' + e.getMessage());
        }
        return theApp;
    }

    private static ProxyApiCreditReportRuleEntity returnHighestPriorityRule(list<ProxyApiCreditReportRuleEntity> ruleList) {
        ProxyApiCreditReportRuleEntity rule;
        Map<Integer, ProxyApiCreditReportRuleEntity> ruleMapByPriority = new Map<Integer, ProxyApiCreditReportRuleEntity>();
        for (ProxyApiCreditReportRuleEntity failedRule : ruleList) {
            if (String.isNotBlank(failedRule.deIsDisplayed) && failedRule.deIsDisplayed.equalsignorecase('Yes'))
                ruleMapByPriority.put(integer.valueOF(failedRule.rulePriority), failedRule);
        }

        List<Integer> priorityList = new List<Integer> ();
        priorityList.addAll(ruleMapByPriority.keySet());
        priorityList.sort();

        if (priorityList.size() > 0)
            rule = ruleMapByPriority.get(priorityList[priorityList.size() - 1]);

        return rule;
    }
    public static boolean isClarityClearRecentHistoryPassed(List<ClarityClearRecentHistory> clearRecentHistoryList){
        boolean ruleStatus = true;
        if(clearRecentHistoryList !=null){
            for(ClarityClearRecentHistory clearRecentHistory: clearRecentHistoryList)
                if(clearRecentHistory.isRulePass!=null && !boolean.Valueof(clearRecentHistory.isRulePass)) { ruleStatus= false; break;}
        }
        return ruleStatus ;

    }

}