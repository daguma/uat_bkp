//This test class includes coverage for AutoAgingAppsJob and AutoAgingAppsBatch

@isTest private class AutoAgingAppsJobTest {

    @testSetup static void setups_setting() {
        Auto_Aging_Logic__c set1 = new Auto_Aging_Logic__c(Name = 'Aging_1', Status__c = 'Credit Approved - No Contact Allowed', Based_on__c = 'Created Date', Days__c = 31, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Approval Expired', Decline_Note__c = '', Use_Business_Days__c = false);
        Auto_Aging_Logic__c set2 = new Auto_Aging_Logic__c(Name = 'Aging_2', Status__c = 'Credit Qualified', Based_on__c = 'Created Date', Days__c = 31, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Approval Expired', Decline_Note__c = '', Use_Business_Days__c = false);
        Auto_Aging_Logic__c set3 = new Auto_Aging_Logic__c(Name = 'Aging_3', Status__c = 'Offer Accepted', Based_on__c = 'Created Date', Days__c = 31, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Approval Expired', Decline_Note__c = '', Use_Business_Days__c = false);
        Auto_Aging_Logic__c set4 = new Auto_Aging_Logic__c(Name = 'Aging_4', Status__c = 'Refinance Qualified', Based_on__c = 'Created Date', Days__c = 31, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Approval Expired', Decline_Note__c = '', Use_Business_Days__c = false);
        Auto_Aging_Logic__c set5 = new Auto_Aging_Logic__c(Name = 'Aging_5', Status__c = 'Offer Accepted', Based_on__c = 'Status Change Date', Days__c = 7, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Additional Docs not Provided', Decline_Note__c = 'No Response from Customer', Use_Business_Days__c = true);
        Auto_Aging_Logic__c set6 = new Auto_Aging_Logic__c(Name = 'Aging_6', Status__c = 'Additional Items Requested', Based_on__c = 'Status Change Date', Days__c = 5, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Additional Docs not Provided', Decline_Note__c = 'No Response from Customer', Use_Business_Days__c = true);
        Auto_Aging_Logic__c set7 = new Auto_Aging_Logic__c(Name = 'Aging_7', Status__c = 'Incomplete Package', Based_on__c = 'Status Change Date', Days__c = 7, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Additional Docs not Provided', Decline_Note__c = 'No Response from Customer', Use_Business_Days__c = true);
        Auto_Aging_Logic__c set8 = new Auto_Aging_Logic__c(Name = 'Aging_8', Status__c = 'Credit Qualified', Based_on__c = 'Status Change Date', Days__c = 7, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Customer Ceased Responding', Decline_Note__c = 'No Response from Customer', Use_Business_Days__c = true);
        Auto_Aging_Logic__c set9 = new Auto_Aging_Logic__c(Name = 'Aging_9', Status__c = 'Documents Requested', Based_on__c = 'Status Change Date', Days__c = 10, Move_to__c = 'Aged / Cancellation', Reject_Offer_Reason__c  = 'Customer Ceased Responding', Decline_Note__c = 'No Response from Customer', Use_Business_Days__c = true);
        insert set1; insert set2; insert set3; insert set4; insert set5; insert set6; insert set7; insert set8; insert set9;
    }

    @isTest static void executes_job() {
        Test.startTest();
        
            AutoAgingAppsBatch autoAgingJob = new AutoAgingAppsBatch();
            String cron = '0 0 23 * * ?';
            system.schedule('Test Auto Aging', cron, autoAgingJob);

        Test.stopTest();
    }

    @isTest static void changes_status_created_date() {

        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Credit Qualified';
        update app;

        Datetime newDate = Datetime.now().addDays(-32);
        Date expiredDate = Date.today().addDays(30);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        System.debug('Apps: ' + appsList.size());

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);

        Opportunity result = [SELECT Status__c, Reason_of_Opportunity_Status__c
                                           FROM Opportunity
                                           WHERE id = : app.id LIMIT 1];

        System.assertEquals('Aged / Cancellation', result.Status__c);
        System.assertEquals('Approval Expired', result.Reason_of_Opportunity_Status__c);
        Test.stopTest();
    }

    @isTest static void no_change_status_docs_requested_created_date() {

        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Documents Requested';
        update app;

        Datetime newDate = Datetime.now().addDays(-40);
        Date expiredDate = Date.today().addDays(30);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);
        
        Test.stopTest();

        System.assertEquals('Documents Requested', [SELECT Status__c FROM Opportunity WHERE id = : app.id LIMIT 1].Status__c);
    }

    @isTest static void no_change_status_recent_app_modified_date() {

        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Credit Qualified';
        update app;

        Date newModifiedDate = Date.today().addDays(-5);
        app.Status_Modified_Date__c = newModifiedDate;
        update app;

        Datetime newDate = Datetime.now().addDays(-10);
        Date expiredDate = Date.today().addDays(30);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);
        
        Test.stopTest();

        System.assertEquals('Credit Qualified', [SELECT Status__c FROM Opportunity WHERE id = : app.id LIMIT 1].Status__c);
    }

    @isTest static void no_changes_status_app_credit_approved_created_date() {

        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Credit Approved - No Contact Allowed';
        update app;

        Datetime newDate = Datetime.now().addDays(-25);
        Date expiredDate = Date.today().addDays(30);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);
        
        Test.stopTest();

        Opportunity result = [SELECT Status__c, Reason_of_Opportunity_Status__c
                                           FROM Opportunity
                                           WHERE id = : app.id LIMIT 1];

        System.assertEquals('Credit Approved - No Contact Allowed', result.Status__c);
    }

    @isTest static void changes_status_app_credit_approved_created_date() {

        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Credit Approved - No Contact Allowed';
        update app;

        Datetime newDate = Datetime.now().addDays(-35);
        Date newModifiedDate = Date.today().addDays(-30);
        Date expiredDate = Date.today().addDays(30);

        app.Status_Modified_Date__c = newModifiedDate;
        update app;

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);
        
        Test.stopTest();

        Opportunity result = [SELECT Status__c, Reason_of_Opportunity_Status__c
                                           FROM Opportunity
                                           WHERE id = : app.id LIMIT 1];

        System.assertEquals('Aged / Cancellation', result.Status__c);
        System.assertEquals('Approval Expired', result.Reason_of_Opportunity_Status__c);
    }

    @isTest static void changes_status_app_documents_requested_modified_date() {

        Database.BatchableContext bc;

        Date newCreatedDate = Date.today().addDays(-25);
        Date newModifiedDate = Date.today().addDays(-16);
        Date expiredDate = Date.today().addDays(30);

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Documents Requested';
        update app;

        app.Status_Modified_Date__c = newModifiedDate;
        update app;

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);

        Test.stopTest();
        
        Opportunity result = [SELECT Status__c, Reason_of_Opportunity_Status__c
                                           FROM Opportunity
                                           WHERE id = : app.id LIMIT 1];

        System.assertEquals('Aged / Cancellation', result.Status__c);
        System.assertEquals('Customer Ceased Responding', result.Reason_of_Opportunity_Status__c);
    }

    @isTest static void no_changes_status_app_documents_requested_modified_date() {

        Database.BatchableContext bc;

        Date newCreatedDate = Date.today().addDays(-12);
        Date newModifiedDate = Date.today().addDays(-11);
        Date expiredDate = Date.today().addDays(30);

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Documents Requested';
        update app;

        app.Status_Modified_Date__c = newModifiedDate;
        update app;

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);
        
        Test.stopTest();

        Opportunity result = [SELECT Status__c, Reason_of_Opportunity_Status__c
                                           FROM Opportunity
                                           WHERE id = : app.id LIMIT 1];

        System.assertEquals('Documents Requested', result.Status__c);
    }

    @isTest static void no_changes_status_additional_docs_modified_date() {

        Database.BatchableContext bc;

        Date newCreatedDate = Date.today().addDays(-10);
        Date newModifiedDate = Date.today().addDays(-5);
        Date expiredDate = Date.today().addDays(30);

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Additional Items Requested';
        update app;

        app.Status_Modified_Date__c = newModifiedDate;
        update app;

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);
        
        Test.stopTest();

        Opportunity result = [SELECT Status__c, Reason_of_Opportunity_Status__c
                                           FROM Opportunity
                                           WHERE id = : app.id LIMIT 1];

        System.assertEquals('Additional Items Requested', result.Status__c);
    }

    @isTest static void changes_status_app_incomplete_package_modified_date() {

        Database.BatchableContext bc;

        Date newCreatedDate = Date.today().addDays(-25);
        Date newModifiedDate = Date.today().addDays(-12);
        Date expiredDate = Date.today().addDays(30);

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Incomplete Package';
        update app;

        //System.assertEquals(Datetime.now().date(), [SELECT Status_Modified_Date__c FROM Opportunity WHERE id = : app.id LIMIT 1].Status_Modified_Date__c);

        app.Status_Modified_Date__c = newModifiedDate;
        update app;

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);
        
        Test.stopTest();

        Opportunity result = [SELECT Status__c, Reason_of_Opportunity_Status__c
                                           FROM Opportunity
                                           WHERE id = : app.id LIMIT 1];

       // System.assertEquals('Aged / Cancellation', result.Status__c);
        //System.assertEquals('Additional Docs not Provided', result.Reason_of_Opportunity_Status__c);
    }

    @isTest static void no_changes_status_app_incomplete_package_modified_date() {

        Database.BatchableContext bc;

        Date newCreatedDate = Date.today().addDays(-15);
        Date newModifiedDate = Date.today().addDays(-4);
        Date expiredDate = Date.today().addDays(30);

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Incomplete Package';
        update app;

        app.Status_Modified_Date__c = newModifiedDate;
        update app;

        Test.startTest();

        Date MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-7);
        Date CQ_MODIFIED_DATE_LAST_N_DAYS = System.today().addDays(-5);

        List<Opportunity> appsList = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Decline_Note__c, CreatedDate, Status_Modified_Date__c
                FROM Opportunity WHERE (Status__c IN ('Credit Qualified', 'Offer Accepted', 'Credit Approved - No Contact Allowed', 'Refinance Qualified')
                        AND (CreatedDate < :expiredDate OR Status_Modified_Date__c < :CQ_MODIFIED_DATE_LAST_N_DAYS) )
                OR (Status__c IN ('Documents Requested', 'Incomplete Package', 'Additional Items Requested') AND Status_Modified_Date__c < :MODIFIED_DATE_LAST_N_DAYS)
                ORDER BY CreatedDate DESC LIMIT 30];

        AutoAgingAppsBatch autoAgingBatch = new AutoAgingAppsBatch();
        autoAgingBatch.execute(bc, appsList);
        
        Test.stopTest();

        Opportunity result = [SELECT Status__c, Reason_of_Opportunity_Status__c
                                           FROM Opportunity
                                           WHERE id = : app.id LIMIT 1];

        System.assertEquals('Incomplete Package', result.Status__c);
    }

}