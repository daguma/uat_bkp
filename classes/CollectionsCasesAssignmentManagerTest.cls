@isTest
public class CollectionsCasesAssignmentManagerTest {
	@isTest static void validate_getQueryforJob() {
        
        Test.startTest();
        
        String myTestQuery = CollectionsCasesAssignmentManager.getQueryforJob(CollectionsCasesAssignmentManager.DELETE_JOB);
        System.assertEquals(CollectionsCasesAssignmentManager.DELETE_JOB_QUERY, myTestQuery);

        myTestQuery = CollectionsCasesAssignmentManager.getQueryforJob(CollectionsCasesAssignmentManager.ANALYZE_JOB);
        System.assertEquals(CollectionsCasesAssignmentManager.ANALYZE_JOB_QUERY, myTestQuery);
        
        Test.stopTest();
    }
    
    @isTest static void validate_executeBatchableContext(){
        Test.startTest();
 
        Database.BatchableContext BC = null;
        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        CollectionsCasesAssignment__c cca = new CollectionsCasesAssignment__c(CL_Contract__c = myContract.Id);
        insert cca;
        CollectionsCasesAssignmentJob job = new CollectionsCasesAssignmentJob();
        List<Object> scope = [SELECT Id FROM CollectionsCasesAssignment__c];
        System.assert(!scope.isEmpty());
        CollectionsCasesAssignmentManager.executeBatchableContext(BC, scope, job);
        List<CollectionsCasesAssignment__c> ccaList1 = [SELECT Id FROM CollectionsCasesAssignment__c];
        System.assert(ccaList1.isEmpty());

        Test.stopTest();
    }

    @isTest static void validate_executeSchedulableContext(){
        Test.startTest();

        SchedulableContext sc = null;
        CollectionsCasesAssignmentJob job = new CollectionsCasesAssignmentJob();
        CollectionsCasesAssignmentManager.executeSchedulableContext(sc, job);

        Test.stopTest();
    }

    @isTest static void validate_finish(){
        Test.startTest();

        Database.BatchableContext BC = null;
        CollectionsCasesAssignmentJob job = new CollectionsCasesAssignmentJob();
        CollectionsCasesAssignmentManager.finish(BC, job);

        Test.stopTest();
    }

    @isTest static void validate_constructors(){
        Test.startTest();

        Database.BatchableContext BC = null;
        CollectionsCasesAssignmentJob job = new CollectionsCasesAssignmentJob(CollectionsCasesAssignmentManager.DELETE_JOB,false);

        job.execute(BC, new List<Object>());

        SchedulableContext sc = null;
        job.execute(sc);

        Test.stopTest();
    }

    @isTest static void validate_scheduleAnalyzeJobForFailures(){
        Test.startTest();

        //loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        //loan__Loan_Account__c myContract2 = LibraryTest.createContractTH();

        CollectionsCasesAssignmentJob job = new CollectionsCasesAssignmentJob();
        //job.auditProcess.failedAnalyzedContracts.add(myContract.Id);
        //job.auditProcess.failedAnalyzedContracts.add(myContract2.Id);
        job.scheduleAnalyzeJobForFailures();

        Test.stopTest();
    }

}