@isTest
 public Class TestMelisaCtrl{
    
 static testMethod void TestDoRequest(){
    
      MelisaCtrl.USPS('17002');
      
      //Insert Dummy Lead
      Lead ld= new Lead();
      ld.LastName='Test';
      ld.FirstName = 'Test';
      ld.Company='test';
      ld.State = 'GA';
      ld.Use_Of_funds__c = 'Wedding';
      insert ld;
       
      Map<string,string> testmap = new map<string,string>();
      testmap.put('FN','Angel'); 
      testmap.put('LN','McMillan'); 
      testmap.put('Street','4090 Hammock Trce'); 
      testmap.put('City','Atlanta'); 
      testmap.put('State','GA'); 
      testmap.put('Zip','30349'); 
      testmap.put('Country','US'); 
      testmap.put('email','angelstranger@yahoo.com'); 
      testmap.put('leadId',ld.id);
      testmap.put('check','income');
      testmap.put('leadId',ld.id);
      testmap.put('contactId','');
      
      
      string test = JSON.SerializePretty(testmap);
      MelisaCtrl.DymistNew(test);
      string leadId = ld.id;
      MelisaCtrl.DoLogging('FactorTrust','DummyRequest','DummyResponse',leadId,'');
      MelisaCtrl.updateResult('Demyst_Employer_Acceptance__c',ld,null,'Y');
       
   
 }
       
   static testMethod void TestContactRequest(){
       MelisaCtrl.USPS('17002'); 
       Opportunity app = LibraryTest.createApplicationTH();
       Map<string,string> testmap = new map<string,string>();
      //Insert Dummy contact
      //contact cnt= LibraryTest.createContactTH();
      /*cnt.LastName='Test';
      cnt.FirstName = 'test';
      cnt.Email = 'notest@blanck.com';
      insert cnt; */
      
      testmap.clear();
      testmap.put('FN','Angel'); 
      testmap.put('LN','McMillan'); 
      testmap.put('Street','4090 Hammock Trce'); 
      testmap.put('City','Atlanta'); 
      testmap.put('State','GA'); 
      testmap.put('Zip','30349'); 
      testmap.put('Country','US'); 
      testmap.put('email','angelstranger@yahoo.com'); 
      testmap.put('contactId',app.contact__c);
      testmap.put('check','employer');
      testmap.put('leadId','');
      testmap.put('contactId',app.contact__c);
      
      string testCont = JSON.SerializePretty(testmap);
      MelisaCtrl.DymistNew(testCont );
      
      string ContactId= app.contact__c;
      MelisaCtrl.DoLogging('FactorTrust','DummyRequest','DummyResponse','',ContactId);
      
      string emailResponse='{"profile":{"websearch_fraud_hit_v2":"0","invalid_email_deluxe":"0","email_name_inconsistent":"1","email_address_age_less_than_six_months":"1","email_address_age_less_than_one_month":"1","email_address_age_less_than_one_week":"1","sanctionrisk":"0","red_flags_deluxe":5,"name_address_nohit_v3":"1","address_inconsistent":"0","internal_id":"d51ff628-fad8-11e4-aabe-6fe7b1a99907","client_id":"B44ks52346Bz3"},"exec_time":{"websearch_fraud_hit_v2":0.309712146,"invalid_email_deluxe":1.473933656,"email_name_inconsistent":0.657091554,"email_address_age_less_than_six_months":2.221482016,"email_address_age_less_than_one_month":2.222184986,"email_address_age_less_than_one_week":2.033276779,"sanctionrisk":1.517047699,"red_flags_deluxe":2.259491063,"name_address_nohit_v3":1.617592478,"address_inconsistent":1.653889087,"internal_id":0.469586104,"client_id":0.361289813},"sources_queried":{"websearch_fraud_hit_v2":"","invalid_email_deluxe":"email history","email_name_inconsistent":"","email_address_age_less_than_six_months":"email history","email_address_age_less_than_one_month":"email history","email_address_age_less_than_one_week":"email history","sanctionrisk":"watch lists","red_flags_deluxe":"email history, web search, marketing databases, watch lists","name_address_nohit_v3":"marketing databases","address_inconsistent":"","internal_id":"","client_id":""},"keyhash":{"email":"msqa051415.329@lendingtree.com","firstname":"mark","lastname":"harmon","street":"200 blue way","city":"charlotte","state":"nc","postcode":"28277","country":"us","testing":"true"},"subscription_id":914,"transaction_id":"d51ff628-fad8-11e4-aabe-6fe7b1a99907","timestamp":"2015-05-15T08:03:15+00:00","requesttime":2.739379475,"user_email":"ffatras@lendingpoint.com","ip":"96.43.148.8"}';
      JSONParser parser = JSON.createParser(emailResponse); 
       
       app.ACH_Bank_Name__c = 'Test0';
       
       update app;
       List<Id> apps = new List<Id>();
       apps.add(app.Id);
       MelisaCtrl.notifySuperMoney(apps);
       
       MelisaCtrl.SendRequest();
       MelisaCtrl.Respns = 'test';
    }
    
     static testMethod void getMaximumOfferAmountTest(){
        
        //Dummy Contact
        contact cntct = new contact();
        cntct.Lastname = 'Test';
        cntct.Firstname = 'Test First';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 10000;
        cntct.MailingState = 'GA';
        insert cntct;
        
        Account dummyAccount = new Account();
        dummyAccount.Name = 'DummyAccount';
        dummyAccount.c2g__CODABankName__c = 'Test Bank';
        dummyAccount.c2g__CODABankAccountNumber__c = '123456';
        dummyAccount.c2g__CODAPaymentMethod__c = 'Cash';
        
        insert dummyAccount;
        cntct.AccountId = dummyAccount.Id;
        update cntct; 
        
        //Dummy Application
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000;
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification';
        aplcn.Type = 'New';
        insert aplcn;
                
        list<id> apidset = new list<id>();
        apidset.add(aplcn.id);
        
        //Dummy offers
        Offer__c off = new Offer__c();
        off.opportunity__c = aplcn.id;
        off.Fee__c = 500;
        off.APR__c = 0.0050;
        off.Payment_Amount__c = 1200;
        off.Loan_Amount__c = 13000;
       // off.Loan_Amount__c = 4655;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Term__c = 1234;
        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;
        
        MelisaCtrl.getMaximumOfferAmount(apidset);
     
      }
 }