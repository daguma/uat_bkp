//Test the following classes
//QueueTimeOutsScrubbingJob
//QueueTimeOutsScrubbingBatch

@isTest private class QueueTimeOutsScrubbingJobTest {

    @isTest static void executes_batch_from_job() {

        Test.startTest();

        QueueTimeOutsScrubbingJob timeOutsScrubbingJob = new QueueTimeOutsScrubbingJob();
        String cron = '0 0 23 * * ?';
        system.schedule('Test QueueTimeOutsScrubbingJob', cron, timeOutsScrubbingJob);

        Test.stopTest();

    }

    @isTest static void updates_application_owner() {

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__Status__c = 'Credit Qualified';
        app.OwnerId = [SELECT id FROM User WHERE Name = 'HAL' LIMIT 1].id;
        update app;

        app.AssignmentDate__c = Datetime.now().addDays(-2);
        app.OwnerId = UserInfo.getUserId();
        update app;

        TimeoutWarnings__c timeoutWarnings = [SELECT id, Application__c, AssignmentDate__c, 

Last_Assigned_To__c,
                                              Application__r.ownerId, 

Application__r.genesis__Status__c
                                              FROM TimeoutWarnings__c
                                              WHERE Application__c = : app.id LIMIT 1];
        timeoutWarnings.AssignmentDate__c = Datetime.now().addDays(-2);
        timeoutWarnings.Last_Assigned_To__c = UserInfo.getName();
        update timeoutWarnings;

        genesis__applications__c app2 = LibraryTest.createApplicationTH2();
        app2.genesis__Status__c = 'Credit Qualified';
        app2.OwnerId = [SELECT id FROM User WHERE Name = 'HAL' LIMIT 1].id;
        update app2;

        app2.AssignmentDate__c = Datetime.now().addDays(-20);
        app2.OwnerId = UserInfo.getUserId();
        update app2;

        TimeoutWarnings__c timeoutWarnings2 = [SELECT id, Application__c, AssignmentDate__c, 

Last_Assigned_To__c,
                                              Application__r.ownerId, 

Application__r.genesis__Status__c
                                              FROM TimeoutWarnings__c
                                              WHERE Application__c = : app2.id LIMIT 1];
        timeoutWarnings2.AssignmentDate__c = Datetime.now().addDays(-20);
        timeoutWarnings2.Last_Assigned_To__c = UserInfo.getName();
        update timeoutWarnings2;
        
        List<TimeoutWarnings__c> timeoutWarningsList = [SELECT id, Application__c, 

AssignmentDate__c, Last_Assigned_To__c,
                                              Application__r.ownerId, 

Application__r.genesis__Status__c
                                              FROM TimeoutWarnings__c
                                              WHERE BusinessDays__c > 10];

        Database.BatchableContext bc;

        Test.startTest();

        QueueTimeOutsScrubbingBatch timeoutsBatch = new QueueTimeOutsScrubbingBatch();
        timeoutsBatch.execute(bc, timeoutWarningsList);

        Test.stopTest();

        app = [SELECT id, ownerId FROM genesis__applications__c WHERE id = : app.id LIMIT 1];

        System.assertEquals(UserInfo.getUserId(), app.OwnerId);
        
        app2 = [SELECT id, ownerId FROM genesis__applications__c WHERE id = : app2.id LIMIT 

1];

        System.assertNotEquals(UserInfo.getUserId(), app2.OwnerId);
        System.assertEquals(Label.TimeOutsScrubbingOwner, app2.OwnerId);
    }

    @isTest static void test_executes() {

        SchedulableContext sc;

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__Status__c = 'Credit Qualified';
        app.OwnerId = [SELECT id FROM User WHERE Name = 'HAL' LIMIT 1].id;
        update app;

        app.AssignmentDate__c = Datetime.now().addDays(-12);
        app.OwnerId = UserInfo.getUserId();
        update app;

        TimeoutWarnings__c timeoutWarnings = [SELECT id, Application__c, AssignmentDate__c, 

Last_Assigned_To__c,
                                              Application__r.ownerId, 

Application__r.genesis__Status__c, BusinessDays__c
                                              FROM TimeoutWarnings__c WHERE Application__c = 

: app.id LIMIT 1];
        timeoutWarnings.AssignmentDate__c = Datetime.now().addDays(-12);
        timeoutWarnings.Last_Assigned_To__c = UserInfo.getName();
        update timeoutWarnings;

        QueueTimeOutsScrubbingBatch timeoutsBatch = new QueueTimeOutsScrubbingBatch();
        timeoutsBatch.execute(sc);
        
        app = [SELECT id, ownerId FROM genesis__applications__c WHERE id = : app.id LIMIT 1];
        
        if (timeoutWarnings.BusinessDays__c > 10){
            System.assertNotEquals(UserInfo.getUserId(), app.OwnerId);
        }
        else{
            System.assertEquals(UserInfo.getUserId(), app.OwnerId);
        }
        
    }
}