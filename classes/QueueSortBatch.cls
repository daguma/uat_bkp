global class QueueSortBatch implements Database.Batchable<sObject> 
{
    global List<Contact> listToUpdate = new List<Contact>();
    String query;

    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        //query = 'SELECT Email, Work_Email_Address__c, Phone, MobilePhone, Work_Phone_Number__c, HomePhone, OtherPhone, AssistantPhone, Other_Use_Of_Funds__c FROM Contact';
        query = 'SELECT Email, Work_Email_Address__c, Phone, MobilePhone, Work_Phone_Number__c, HomePhone, OtherPhone, AssistantPhone, genesis__SSN__c FROM Contact';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Contact> scope) 
    {
        Integer count = 0;
        if(scope.size() > 0)
        {
            MaskSensitiveDataSettings__c settings = [SELECT MaskAllData__c FROM MaskSensitiveDataSettings__c];
            settings.MaskAllData__c = true;
            Database.update(settings);

            for (Contact c : scope) 
            {   
                c.Email = 'sandbox@test.com';
                c.Work_Email_Address__c = 'sandbox@test.com';
                c.Phone = '**********';
                c.MobilePhone = '**********';
                c.Work_Phone_Number__c = '**********';
                c.HomePhone = '**********';
                c.OtherPhone = '**********';
                c.AssistantPhone = '**********';
                //c.Other_Use_Of_Funds__c = 'Test';
                c.genesis__SSN__c = shuffle(c.genesis__SSN__c);
                listToUpdate.add(c);
            }

            if(listToUpdate.size() > 0 ) 
                Database.update(listToUpdate);

            settings.MaskAllData__c = false;
            Database.update(settings);
        }
    }

    global void finish(Database.BatchableContext BC) {}

    global void execute(SchedulableContext sc) {
        QueueSortBatch conInstance = new QueueSortBatch();
        database.executebatch(conInstance, 100);
    }
    
    public String shuffle(String input)
    {
        if (input != null && input != '') 
        {
            List<String> chars = input.split('');
            String output = '';
            if (chars.size() > 0) 
            {
                output += chars.remove(3);
                output += chars.remove(4);
                output += chars.remove(5);
                for (Integer i = 0; i < chars.size(); i++)
                {
                    output += chars[i];
                }
                System.debug(output);
            }
            return output;    
        }
        else
            return null;
    }
}