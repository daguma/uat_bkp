@isTest
public class ClarityXmlResponseTest {
     static testmethod void validate_instance() {
        Test.startTest();
        ClarityXmlResponse ccf = new ClarityXmlResponse();
        Test.stopTest();
        
        system.assertEquals(null, ccf.tracking_Number);
        system.assertEquals(null, ccf.action);
        system.assertEquals(null, ccf.deny_codes);
        system.assertEquals(null, ccf.deny_descriptions);
        system.assertEquals(null, ccf.exception_descriptions);
        system.assertEquals(null, ccf.filter_codes);
        system.assertEquals(null, ccf.filter_descriptions);
        system.assertEquals(null, ccf.identity_theft_prevention);
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityXmlResponse ccf = new ClarityXmlResponse();
        Test.stopTest();
        
        ccf.tracking_Number = 'test';
        ccf.action = 'test';
        ccf.deny_codes = 'test';
        ccf.deny_descriptions = 'test';
        ccf.exception_descriptions = 'test';
        ccf.filter_codes = 'test';
        ccf.filter_descriptions = 'test';
        ccf.identity_theft_prevention = 'test';
        ccf.clear_products_request = null;
        ccf.inquiry = null;
        ccf.clear_fraud = null;
        
        system.assertEquals('test', ccf.tracking_Number);
        system.assertEquals('test', ccf.action);
        system.assertEquals('test', ccf.deny_codes);
        system.assertEquals('test', ccf.deny_descriptions);
        system.assertEquals('test', ccf.exception_descriptions);
        system.assertEquals('test', ccf.filter_codes);
        system.assertEquals('test', ccf.filter_descriptions);
        system.assertEquals('test', ccf.identity_theft_prevention);
    }
}