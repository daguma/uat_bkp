public class DecisioningAttributeEntity {

    @AuraEnabled public ProxyApiCreditReportAttributeEntity soft {get; set;}
    @AuraEnabled public ProxyApiCreditReportAttributeEntity hard {get; set;}

    public Boolean showHardIsApproved {
        get {
            return hard != null;
        }
        set;
    }

    public DecisioningAttributeEntity(ProxyApiCreditReportAttributeEntity softCreditReportAttributeEntity,
                                      ProxyApiCreditReportAttributeEntity hardCreditReportAttributeEntity) {

        this.soft = softCreditReportAttributeEntity;
        this.hard = hardCreditReportAttributeEntity;
    }
}