@isTest public class CreditReportScoreFactorsTest {

    @isTest static void getScoreFactorStatements() {
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentEntityScores();

        String result = CreditReportScoreFactors.getScoreFactorStatements(segmentList, 'TU');
        String expected = 'Amount owed on accounts is too high\nLevel of delinquency on accounts\nProportion of loan balances to loan amounts is too high\nLack of recent installment loan information\n';
        System.assertEquals(expected, result);

        result = CreditReportScoreFactors.getScoreFactorStatements(segmentList, 'EX');
        expected = 'Amount owed on accounts is too high\nLevel of delinquency on accounts\nToo few bank/national revolving accounts\nToo many bank/national revolving accounts\n';
        System.assertEquals(expected, result);

    }

}