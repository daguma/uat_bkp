/***************************************************************************
*
*
*
*
****************************************************************************/
@isTest
public class Batch_NOIANotificationOnOpportunity_Test {
    Public static testMethod void testMethodOne() 
    {
        string AAN_TEMPLATE = 'NOIA Notifications';
        List<contact> lstcontact= new List<contact>();
        contact c = new contact();
        c.lastname ='Test Contact';
        c.FirstName = 'test First';
        c.MailingState = 'GA';
        c.Email = 'shiva.kumar@test.com';
        EmailTemplate et = [SELECT Id, Name FROm EmailTemplate WHERE Name = 'NOIA Notifications'];
        
        lstcontact.add(c);
        insert lstcontact;
        
        insert new FinwiseDetails__c(Whitelisted_States__c = 'CA');
        
        Account a = new Account();
        a.name = 'Refinance';
        a.Is_FEB__c  = false;
        a.Cobranding_Name__c = 'test';
        insert a;
        
        List<opportunity> lstopty= new List<opportunity>();
        opportunity o = new opportunity();

        o.Contact__c = lstcontact[0].id;
        o.Lead_Sub_Source__c = 'LPCustomerPortal';
        //o.Status_Modified_Date__c =date.today()-2;
        o.type ='Refinance';
        o.Status__c ='Incomplete Package';
        o.LeadSource__c ='Organic';
        o.IsTestBucket__c =false;
        o.CloseDate = date.today();
        o.Amount = 100;
        //o.is_FEB__c = false;
        o.IsTestBucket__c= false;
        o.Name = 'Testing Oppty';
        o.stagename = 'Qualification';
        o.Partner_Account__c = a.id;
        o.NOIA_Sent_Date__c  = null;
        o.Status_Modified_Date__c = Date.today().addDays(-30);
        o.Encrypted_AppId_AES__c = '0Rqz1C8iCkMoWaLHHbw2uWP0rvZxNJ5fovbeGHMd33c%3D';
        //o.Is_Finwise__c = true;
        lstopty.add(o);
        
        insert lstopty;
        
        
        Attachment Att = new Attachment();
        Att.name = 'system_credit_data_management_HP - Do Not Open';
        Att.body = Blob.valueof('Test Record please ignore');
        Att.ParentId = lstopty[0].id;
        
        insert Att;
         
         EmailTemplate e = new EmailTemplate();
         e.Name = 'NOIA Notifications';
         e.body = 'Test';
         e.Subject = 'Your re-financing options are still available but will soon expire' ;
         //insert e;
        //EmailTemplate eTemplate = [select id, name,body,Subject,HtmlValue from EmailTemplate where id =:e.id];
        
        Test.startTest();
        String HtmlValue = 'To:' + lstcontact[0].Email + '\n\n';
        String Subject = 'Your re-financing options are still available but will soon expire';
        String tName = 'NOIA Notifications';
        EmailTemplate eTemplate = [select id, name,body,Subject,HtmlValue from EmailTemplate where name = 'NOIA Notifications'];
        Batch_NOIANotificationOnOpportunity noibatch = new Batch_NOIANotificationOnOpportunity();
        Batch_NOIANotificationOnOpportunity.createNote(HtmlValue ,Subject ,lstopty[0].id);
        noibatch.replaceValuesFromData(eTemplate.body, lstopty[0].Contact__r, lstopty[0]);
        DataBase.executeBatch(noibatch); 
        String CRON_EXP = '0 0 0 3 9 ? 2019';
        String jobId = System.schedule('test', CRON_EXP, new Batch_NOIANotificationOnOpportunity());
      
        String htmlBody = 'To:' + lstcontact[0].Email + '\n\n';
        String subject1 = 'test';
        Id OpportunityID = lstopty[0].id;
        list<note> nlst = new list<Note>();
        Note not1 = new Note();
        not1.ParentId = OpportunityID ;
        not1.Title = Subject1;
        not1.Body = htmlBody ;
        nlst.add(not1);
        insert nlst;
        
       
        
        Test.stopTest();
        
        
    }



}