public class applicationfundingAutomation{

    //*******************************************************************************************************
    //**This method contains functionality to create contract and update releted fields on Application record
    //*******************************************************************************************************
    public static void contractCreation(List<Opportunity> appList, Map<id,string> expDisDateMap, List<Logging__c> logList){
        List<Opportunity> updateAppList= new List<Opportunity>();
        Map<string,string> userIdMap= new Map<string,string>();
        set<string> userSet= new set<string>();
        string contactId;      

        try{    
            if(!appList.IsEmpty()){
                for(Opportunity app: appList){
                    string retmsg;
                    contactId=app.Contact__c;
                    try{
                      if(!test.isrunningtest()){
                        //genesis.ConvertapplicationCtrl ctrl = new genesis.ConvertapplicationCtrl();
                        //retMsg = ctrl.convertapplication(app.ID);
                        retMsg = convertOpportunityToContractCtrl.createContract(app.ID);
                      }
                    logList.add(EmailUtility.getLoggingRecord('ContractGeneration',app.ID,retMsg,app.Contact__c));
                  }catch(Exception e){
                      system.debug('====Exception=='+e.getMessage());
                      logList.add(EmailUtility.getLoggingRecord('ContractGeneration',null,e.getMessage(),contactId));                  
                  }
                  if(app.FS_Assigned_To_Val__c <> null && app.FS_Assigned_To_Val__c <> 'Not Assigned'){
                      userSet.add(app.FS_Assigned_To_Val__c);
                  }
                   
                }
                
            }
            
            for(user us: [select id, name from user where name IN: userSet]){
                userIdMap.put(us.name, us.id);    
            }
            updateAppList=[select id, Contact__c, Status__c, Lending_Account__c,Lending_Account__r.Payment_Frequency_Masked__c, Lending_Account__r.name,StrategyType__c, Customer_Name__c, FS_Assigned_To_Val__c, Amount, Name, Expected_Disbursal_Date__c,AON_Enrollment__c,AON_Fee__c,AON_FeesTotal__c,Term__c  from Opportunity where id IN: appList];
            /*
            Date halDate = [SELECT loan__Current_Branch_s_System_Date__c FROM User WHERE Name = 'HAL' LIMIT 1].loan__Current_Branch_s_System_Date__c;
            String aonFeeId = [SELECT Id,Name FROM loan__Fee__c WHERE Name = 'AON' LIMIT 1].Id;
             */
            for(Opportunity appUp: updateAppList){
                contactId=appUp.Contact__c;
                if(appUp.Lending_Account__c <> null){
                    string sub= 'FUNDED: '+ appUp.Lending_Account__r.name+ ' - '+ appUp.Customer_Name__c+' - '+appUp.Name+' - $'+appUp.Amount;
                    string emailBod='The following deal has been funded on '+ expDisDateMap.get(appUp.id)+'\n'+'Contract ID: '+ appUp.Lending_Account__r.name+'\n'+ 'Application ID: '+ appUp.Name+'\n'+'Applicant Name: '+ appUp.Customer_Name__c+'\n'+'Loan Amount: $'+appUp.Amount;
                    //appUp.Expected_Disbursal_Date__c = Date.valueOf(expDisDateMap.get(appUp.id));
                  //  appUp.Status__c ='Funded';
                   // appUp.Funded_Date__c = system.today();
                    
                    if(appUp.FS_Assigned_To_Val__c <> null && appUp.FS_Assigned_To_Val__c <> 'Not Assigned' && userIdMap.get(appUp.FS_Assigned_To_Val__c) <> null){
                      system.debug('====us====='+userIdMap.get(appUp.FS_Assigned_To_Val__c));
                      WebToSFDC.notifyDev(sub,emailBod,userIdMap.get(appUp.FS_Assigned_To_Val__c)); 
                    }
                    /*if(appUp.AON_Enrollment__c){
                      //Modify the AON Enrollment flag in the contract. A process builder will insert the enrollment record after this change.
                      //loan__Loan_Account__c loan = [select id,AON_Enrollment__c FROM loan__Loan_Account__c WHERE id =: appUp.Lending_Account__c];
                      update new loan__Loan_Account__c(id = appUp.Lending_Account__c,AON_Enrollment__c = true,AON_Fee__c = appUp.AON_Fee__c);
                      //loan.AON_Enrollment__c = true;
                      //loan.AON_Fee__c = appUp.AON_Fee__c;
                      //update loan;
                      //Insert the new periodic fee in the loan__Periodic_Fee_Setup__c object and calculate the fee amount. Update the contract with the loan__fee__c that comes from the application's aon_fee
                      //if(appUp.AON_Fee__c > 0) AONPeriodicFeesConfig(appUp);
                    }     

					if(appUp.AON_Enrollment__c && appUp.AON_Fee__c > 0){
                      //Insert the new periodic fee in the loan__Periodic_Fee_Setup__c object and calculate the fee amount. Update the contract with the loan__fee__c that comes from the application's aon_fee
                      AONPeriodicFeesConfig(appUp, halDate, aonFeeId);
                    }*/               
                    
               } else
                System.debug('Application does not appear funded');
            } 
            
          /*  if(!updateAppList.isEmpty()){
                database.update(updateAppList);
            }    */ 
            
            if(!logList.isEmpty()){
                database.insert(logList);
            }       
          
           }catch(Exception e){

               WebToSFDC.notifyDev('ApplicationFUnding.contractCreation Error',e.getMessage() + '\n' + e.getStackTraceString()); 

               system.debug('====Exception=='+e.getMessage());
               logList.add(EmailUtility.getLoggingRecord('ContractGeneration',null,e.getMessage(),contactId)); 
               if(!logList.isEmpty()){
                   insert logList;
               }   
           }
    } 
    
    //***************************************************************************************
    //**This method is called from process builder for application funding automation process
    //***************************************************************************************
    @invocableMethod
    public static void FundingAutomation(list<Opportunity> AppList){
    
        Date expectedDisbursalDate;
        string contactId;
        Map<id,string> expDisDateMap= new Map<id, string>();
        List<Logging__c> logList= new List<Logging__c>();
        CutOff_time_on_automated_funding__c cutOffParam= CutOff_time_on_automated_funding__c.getInstance('CutOff time');
        list<Opportunity> gappToUpdate = new list<Opportunity>();
        set<id> CLContractIdSet= new Set<id>();
        list<loan__Loan_Account__c> CLContractList= new List<loan__Loan_Account__c>();
        Map<id,datetime> CLContractMap= new Map<id,datetime>();
        Map<id,string> CLContractStatusMap= new Map<id,string>();
        List<Opportunity> toBeUpdateAppList= new List<Opportunity>();
        applicationFundingAutomationValidatio__c fundAutoCheck = applicationFundingAutomationValidatio__c.getInstance('fundingAutomation24hrCheck');
        
    try{
        //To collect all the CL contract related to application records
        for(Opportunity appnew: appList){
            if(appnew.Lending_Account__c <> null){
                CLContractIdSet.add(appnew.Lending_Account__c);
            }
         
        }
        if (CLContractIdSet.size() >0) CLContractList=[select id, createddate, loan__Loan_Status__c from loan__Loan_Account__c where id IN:CLContractIdSet];
        system.debug('===CL Contract for applications==='+CLContractList);
        
        for(loan__Loan_Account__c cont: CLContractList){
            CLContractMap.put(cont.id,cont.createddate);
            CLContractStatusMap.put(cont.id,cont.loan__Loan_Status__c);
        }
        system.debug('===contract Map=='+CLContractMap); 
        
        for(Opportunity appObj: appList){
          contactId= appObj.Contact__c;
          
          if(appObj.Status__c == 'Contract Pkg approved' && appObj.Deal_is_Ready_to_Fund__c == True){                   
               
                DateTime gappTime = System.Now(); 
                String dummyEDValue = gappTime.format('h:mm a');
                integer dummyHours = integer.ValueOf(DummyEDValue.substringBefore(':'));
                integer colonIndex = DummyEDValue.indexOf(':');
                string dummyMinutesValue = DummyEDValue.substring(colonIndex+1, colonIndex+3);
                integer dummyMinutes = integer.ValueOf(dummyMinutesValue) ;  
                String dayOfWeek = gappTime.format('EEEE');  
                system.debug('dayOfWeek >>>> ' +dayOfWeek);
                system.debug('Hours >>>> ' +dummyHours);
                system.debug('Minutes >>>> ' +dummyMinutes);

                expectedDisbursalDate= system.today();
                
                 if(dayOfWeek == 'Saturday'){
                      expectedDisbursalDate= system.today()+2;
                 }
                 else if(dayOfWeek == 'Sunday'){
                     expectedDisbursalDate= system.today()+1;
                 }
                else{
                    if(dummyEDValue.contains(cutOffParam.AM_PM__c) && ((dummyHours > integer.valueOf(cutOffParam.Hours__c) && dummyHours <> 12 ) 
                                                       || (dummyHours == integer.valueOf(cutOffParam.Hours__c) && dummyMinutes > integer.valueOf(cutOffParam.Minutes__c))) ){
                        if(dayOfWeek == 'Friday'){  expectedDisbursalDate= system.today()+3; }
                        else if(dayOfWeek == 'Saturday'){ expectedDisbursalDate= system.today()+2; }
                        else if(dayOfWeek == 'Sunday'){  expectedDisbursalDate= system.today()+1; }
                        else{ expectedDisbursalDate= system.today()+1; }
                    
                    } 
                }
                // temp until we figure out why Cloudlending does not allow disbursals on future dates.
                expectedDisbursalDate= system.today();
            
            if(appObj.Lending_Account__c==NULL && expectedDisbursalDate!=NULL){ 
                gappToUpdate.add(appObj);  
                expDisDateMap.put(appObj.id, string.valueOf(expectedDisbursalDate));          
                
            }else if(appObj.Lending_Account__c!=NULL){
                if(CLContractMap.containsKey(appObj.Lending_Account__c)){
                    datetime contCrtDate= CLContractMap.get(appObj.Lending_Account__c), currentdate= system.now();
                    
                    decimal decHours = ((decimal.valueof(currentdate.getTime()) - decimal.valueof(contCrtDate.getTime()))/1000/60/60).setscale(2);
                    system.debug('====Time difference=='+ decHours);
                    
                    //Allow to create new CL Contract if a previous Contract exists that is older than 24 hours
                    system.debug('====CL Contract Status===='+CLContractStatusMap.get(appObj.Lending_Account__c));
                    if((CLContractStatusMap.get(appObj.Lending_Account__c) == 'Canceled') || (fundAutoCheck.Allow_24_hrs_check__c && decHours >fundAutoCheck.Time__c)){
                        system.debug('===More than 24 hrs====');  
                        gappToUpdate.add(appObj);  
                        expDisDateMap.put(appObj.id, string.valueOf(expectedDisbursalDate));
                         
                        Opportunity toBeUpdateApp= new Opportunity(id= appObj.id);
                        toBeUpdateApp.Lending_Account__c= null;
                        toBeUpdateAppList.add(toBeUpdateApp);
                    }else{
                        system.debug('===Within 24 hrs===');
                        string res= 'Contract '+appObj.Lending_Account__c+' is already present for this application';
                        logList.add(EmailUtility.getLoggingRecord('ContractGeneration',appObj.ID,res,appObj.Contact__c));
                    }
                }             
               
                    
            }  
             
          }
       }
       
       if(!toBeUpdateAppList.isEmpty()){
           update toBeUpdateAppList;
       }
       
       if(!gappToUpdate.isEmpty()){
           contractCreation(gappToUpdate,expDisDateMap,logList);
       }
       
       if(gappToUpdate.isEmpty() && !logList.isEmpty()){
           insert logList;
       }
       
       }catch(Exception e){
           
           system.debug('====Exception=='+e.getMessage());
           logList.add(EmailUtility.getLoggingRecord('ContractGeneration',null,e.getMessage(),contactId)); 
           if(!logList.isEmpty()){
               insert logList;
           } 
       }
    }
    private static void AONPeriodicFeesConfig(Opportunity opp){

      try {

        loan__Periodic_Fee_Setup__c periodicFee = new loan__Periodic_Fee_Setup__c();
        //periodicFee.loan__Lending_Account__c = loan.id;
        periodicFee.loan__Lending_Account__c = opp.Lending_Account__c;
        periodicFee.loan__Clear__c = false;
        periodicFee.loan__Sequence__c = 1.00;

        //periodicFee.loan__Fee__c = [SELECT Name FROM loan__Fee__c WHERE Name = 'AON' LIMIT 1].Id;
        for(loan__Fee__c fee : [SELECT Name FROM loan__Fee__c WHERE Name = 'AON' LIMIT 1])periodicFee.loan__Fee__c = fee.Id;

        periodicFee.loan__Amount__c = opp.AON_FeesTotal__c / opp.Term__c;

        //periodicFee.loan__Next_Recurring_Fee_Date__c = [SELECT loan__Current_Branch_s_System_Date__c FROM User WHERE Name = 'HAL' LIMIT 1].loan__Current_Branch_s_System_Date__c;
        for(User user : [SELECT loan__Current_Branch_s_System_Date__c FROM User WHERE Name = 'HAL' LIMIT 1]) periodicFee.loan__Next_Recurring_Fee_Date__c = user.loan__Current_Branch_s_System_Date__c;

        periodicFee.loan__Active__c = true;

        insert periodicFee;


      } catch(DmlException e) {
        System.debug(e.getMessage());
      }
    }
    /*
    private static void AONPeriodicFeesConfig(Opportunity app, Date halCurrDate, String aonFeeId){

      try {

        loan__Periodic_Fee_Setup__c periodicFee = new loan__Periodic_Fee_Setup__c();
        periodicFee.loan__Lending_Account__c = app.Lending_Account__c;
        periodicFee.loan__Clear__c = false;
        periodicFee.loan__Sequence__c = 1.00;
        periodicFee.loan__Fee__c = aonFeeId;
        periodicFee.loan__Amount__c = app.AON_FeesTotal__c / app.Term__c;
        periodicFee.loan__Next_Recurring_Fee_Date__c = halCurrDate;
        periodicFee.loan__Active__c = true;

        insert periodicFee;


      } catch(DmlException e) {
        System.debug(e.getMessage());
      }
    }
	*/
    
}