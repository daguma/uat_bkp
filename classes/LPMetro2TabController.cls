public class LPMetro2TabController {

    public LPMetro2TabController(ApexPages.StandardController controller) {
        headerSet1 = [SELECT Name, loan__Date_Created__c FROM loan__CR_Org_Definition__c ORDER BY CreatedDate Desc];
    }
    
    public LPMetro2TabController(){
        headerSet1 = [SELECT Name, loan__Date_Created__c FROM loan__CR_Org_Definition__c ORDER BY CreatedDate Desc];
    }
    
    public List<loan__CR_Org_Definition__c> headerSet1 {get; set;}
    
    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }
    

    public PageReference generateFile() {
        loan.M2QueryGen queryGen = null;
        loan__Metro2_Parameters__c m2p = loan__Metro2_Parameters__c.getInstance();
        m2p = m2p == null ? loan__Metro2_Parameters__c.getOrgDefaults() : m2p; // defaulting
        
        try {
            if (m2p.loan__Metro2_Query_Class__c != null) {
                Type t = Type.forName(null, m2p.loan__Metro2_Query_Class__c);
                if (t == null) {
                    t = Type.forName('loan', m2p.loan__Metro2_Query_Class__c);
                }
                queryGen = (loan.M2QueryGen) t.newInstance();
            }
            String query=queryGen.getLoanQuery().replace(' ORDER BY loan__Contact__r.LastName',' ORDER BY loan__Contact__r.LastName limit ' + m2p.loan__Records_Per_File__c.intValue());
            System.debug(query);

            loan.M2FileGenerationJob job = new loan.M2FileGenerationJob(query);
            Database.executeBatch(job, m2p.loan__Batch_Size_For_Metro2_Jobs__c.intValue());
            createMessage(ApexPages.Severity.INFO, 'Metro2 generation job submitted. Refresh this page to see results or check status of apex job. ' + 
                                                        'The file will be generated in your Personal Documents folder.');
        } catch (Exception e) {
            createMessage(ApexPages.Severity.ERROR, 'Metro2 generation job submission failed. ' + e.getMessage());
        }
        return null;
    }

    
}