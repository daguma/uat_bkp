global class ApplicationToContractConverter implements ints.IProcessContract{
    public Opportunity application = new Opportunity();
    public loan__Loan_Account__c loanAccount = new loan__Loan_Account__c();
    private Map<String,String> paymentFrequencyMap = new Map<String,String>();
    public LP_Custom__c lpCustom;


    global virtual void setContracts(Map<SObject,SObject> objectMap){
        Set<SObject> applicationSet = objectMap.keySet();
        for(SObject obj:applicationSet){
            application = (Opportunity)obj;
        }
        this.loanAccount = (loan__Loan_Account__c)objectMap.get(application);
        system.debug('====loanAccount==='+loanAccount );
        lpCustom = CustomSettings.getLPCustom();
    }

    global virtual String processContract(){
        String retMsg = '';
        loan__Office_Name__c office;
        loan__Loan_Product__c product;
 
        try{

            ezVerify_Settings__c ezVerifyCustom = CustomSettings.getEZVerifyCustom();
            application = getApplicationDetails(application.id);
            system.debug('=productType=' + application.Product_Type__c );
            if(application.Lending_Account__c == null) { 
                if(application.Company__c != null)
                    office = getOffice(application.Company__r.Name);
                else if(lpCustom.Default_Company__c != null)
                    office = getOffice(lpCustom.Default_Company__c); 
                else
                    return 'Provide Company information in Application or Custom Settings.';
                       
                system.debug('-------------productName---------'+application.ProductName__c);
                
                if(application.Lending_Product__c != null) //s replacing Lending_Product__c  with Loan_Product__c
                    product = getLoanProductDetails(application.Lending_Product__r.Loan_Lending_Product__r.Name);
                
                else if(generalPurposeWebServices.isPointOfNeed(application.Contact__r.Product__c) && ezVerifyCustom.Product_Name__c != null) 
                    product  = getLoanProductDetails(ezVerifyCustom.Product_Name__c);
                
                else if(generalPurposeWebServices.isSelfPayProduct(application.Contact__r.Product__c) && ezVerifyCustom.Product_Name_SelfPay__c!=null)    
                    product  = getLoanProductDetails(ezVerifyCustom.Product_Name_SelfPay__c);
                
                else if(lpCustom.Default_Lending_Product__c != null) {
                    product = getLoanProductDetails(lpCustom.Default_Lending_Product__c); 
                
                }
                else
                    return 'Provide Lending Product information in Application or Custom Settings.';
                
                application.Product_Type__c = application.Product_Type__c == null ? 'LOAN' : application.Product_Type__c; 
                application.Company__c = office.Id;
                /* ALPHA Changes */
                Lending_Product__c custLendingProduct = [SELECT id from Lending_Product__c WHERE Loan_Lending_Product__c =: product.Id LIMIT 1];
                If(custLendingProduct != null)
                    application.Lending_Product__c = custLendingProduct.Id; //s replacing Lending_Product__c  with Loan_Product__c
                /* ALPHA Changes */
                application.ProductName__c = product.Name;  
                system.debug('=productFound=' + product);
                retMsg = appchecker();
                if(retMsg.Equals('1')) {
                   // update application;
                    
                    if(application.Product_Type__c.equals('LOAN')){
                        CreateLoanAccountForLoanTypeHandler loanHandler = new CreateLoanAccountForLoanTypeHandler(application,loanAccount,product);
                        retMsg = loanHandler.createLoanAccount();
                    }
                }
            } else
                return 'Application has already been converted to Loan Account';
        }catch(Exception e){
            retMsg = 'message:'+e.getMessage() +',line number:'+e.getLineNumber() + '\n' + e.getStackTraceString(); 
        }
        system.debug('=retMsg='+ retMsg );
        return retMsg;
            
    }
    
    public static Opportunity getApplicationDetails(String appId){
        Opportunity application = [select id,CreatedDate,
                                                        Amount,
                                                        Status__c,
                                                        Lending_Product__c,
                                                        Company__c,Lending_Account__c,
                                                        Fee__c,
                                                        Lending_Product__r.Name,
                                                        Company__r.Name,
                                                        Contact__r.MailingState,
                                                        Contact__c,
                                                        Contact__r.Id,
                                                        Contact__r.Annual_Income__c,
                                                        Contact__r.Product__c,
                                                        Payment_Amount__c,
                                                        Term__c,//s genesis__Credit_Limit__c,
                                                        Payment_Frequency__c,
                                                        Payment_Frequency_Multiplier__c,
                                                        Payment_Frequency_Masked__c,
                                                        Interest_Rate__c,Product_Type__c,
                                                        Days_Convention__c,
                                                        Expected_First_Payment_Date__c  ,
                                                        Expected_Start_Date__c,
                                                        Interest_Only_Period__c,
                                                        ACH_Account_Number__c,
                                                        ACH_Account_Type__c,
                                                        ACH_Bank_Name__c,
                                                        ACH_Routing_Number__c,
                                                        Total_Loan_Amount__c,
                                                        Payment_Method__c, 
                                                        FICO__c, 
                                                        is_Finwise__c, 
                                                        Expected_Disbursal_Date__c, 
                                                        DTI__c, Effective_APR__c,
                                   Fee_Handling__c,AccountId,
                                                        Estimated_Grand_Total__c, 
                                                        Payroll_Frequency__c,Partner_Account__c,
                                                        Expected_Close_Date__c,LeadSource__c,
                                                        Refinance_Pay_Off_Amount__c,
                                                        Finwise_Approved__c, ProductName__c,
                                                        Partner_Account__r.Do_not_use_bank_charter__c,
                                                        Partner_Account__r.ACH_Requirement_Override__c,
                                                        Partner_Account__R.Accrue_Interest_On_Disbursal__c, 
                                                        Lending_Product__r.Loan_Lending_Product__r.Name,
                                                        Partner_Account__r.Est_Interest_Start_Grace_Period__c,
                                                        Partner_Account__r.Use_Promotion_Fields__c,
                                                        Partner_Account__r.Channel__c,
                                                        Unique_Code_Hit_Id__c, Scorecard_Grade__c,
                                                        Lead_Sub_Source__c ,
                                                        Contact__r.Promotion_Active__c, 
                                                        Contact__r.Promotion_Expiration_Date__c,
                                                        Contact__r.Promotion_Term_In_Months__c,
                                                        Contact__r.Promotion_Rate__c,
                                                        Partner_Account__r.Promotion_Duration__c,
                                                        Is_FEB__c, 
                                                        Promotion_Rate__c,
                                                        Promotion_Active__c,
                                                        Promotion_Expiration_Date__c,
                                                        AON_Enrollment__c,
                                                        AON_Fee__c,
                                                        Offer_Code__c //API-317                                 
                                                        from Opportunity where Id=:appId];
                                                        
        return application;
    }
    
    public String appChecker(){
        if(application.Term__c == null || application.Term__c == 0)
            return 'Term cannot be null';
        if(application.Amount ==null || application.Amount == 0)
            return 'Loan Amount cannot be null';
        if(application.Contact__c ==null)
            return 'Contact cannot be null';
        if(application.Payment_Amount__c == null || application.Payment_Amount__c == 0)
            return 'Payment Amount cannot be null';
        if(application.Expected_First_Payment_Date__c   ==null)
            return 'Expected First Payment Date cannot be null';
        if(application.Expected_Start_Date__c ==null)
            return 'Expected Start Date cannot be null';
        if(application.Interest_Rate__c ==null || application.Interest_Rate__c == 0)
            return 'Interest Rate cannot be null';
        if(application.Payment_Frequency__c ==null)
            return 'Payment Frequency cannot be null';
        if(!application.Partner_Account__r.ACH_Requirement_Override__c || application.Partner_Account__c == null){
            //===SM-805====
            if((string.isNotEmpty(application.LeadSource__c) && !generalPurposeWebServices.isEzVerify(application.LeadSource__c)) && (application.Lead_Sub_Source__c != 'LoanHero' ||  (application.Lead_Sub_Source__c == 'LoanHero' && application.Payment_Method__c != 'Certified Check')) &&(application.ACH_Account_Number__c == null || application.ACH_Account_Type__c == null ||
                    application.ACH_Bank_Name__c == null || application.ACH_Routing_Number__c == null))
                return 'ACH information is missing or missing ACH Account Type';
        }
        if (application.is_Finwise__c && FinWise.isEnabled() && !application.Finwise_Approved__c )
            return 'Contract must be submitted to FINWISE First';
        return '1';
    }
     
    public static loan__Loan_Product__c getLoanProductDetails(String loanProductName) {
        loan__Loan_Product__c product = [select ID, Name,
            loan__Default_Interest_Rate__c,
            loan__Default_Number_of_Installments__c,
            loan__Fee_Set__c,
            loan__Draw_Term_Payment_Percent__c,
            loan__Repayment_Billing_Method__c,
            loan__Repayment_Term_Payment_Percent__c,
            loan__Maximum_Draw_Amount__c,
            loan__Minimum_Draw_Amount__c,
            loan__Default_Overdue_Interest_Rate__c,
            loan__Loan_Cycle_Setup__c,
            loan__Loan_Product_Type__c,
            loan__Repayment_Procedure__c,
            loan__Interest_Calculation_Method__c,
            loan__Frequency_Of_Loan_Payment__c,
            loan__Funder__c,
            loan__Funding_in_Tranches__c,
            loan__Interest_Only_Period__c,
            loan__Draw_Billing_Method__c,
            loan__Time_Counting_Method__c,
            loan__Write_off_Tolerance_Amount__c,
            loan__Pre_Bill_Days__c,
            loan__Delinquency_Grace_Days__c,
            loan__Late_Charge_Grace_Days__c,
            loan__Amortize_Balance_type__c,
            loan__Amortization_Frequency__c,
            loan__Amortization_Enabled__c,
            loan__Amortization_Calculation_Method__c,
            loan__Grace_Period_for_Repayments__c,
            (select ID,Name,
            loan__Default_Interest_Rate__c,
            loan__Default_Number_of_Installments__c,
            loan__Default_Overdue_Interest_Rate__c,
            loan__Fee_Set__c,
            loan__Cycle_Number__c 
            from loan__Loan_Product_Cycle__r)
            from loan__Loan_Product__c where Name =: loanProductName];
        system.debug(Logginglevel.INFO,'Loan product '+ product );   
        return product;                             

    }      
    
    public static loan__Office_Name__c getOffice(String officeName){
        loan__Office_Name__c ofc = [Select Id,Name 
                                        from loan__Office_Name__c
                                        where Name =: officeName];
        return ofc;
    }
    
    
}