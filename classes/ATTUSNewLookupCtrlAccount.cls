public with sharing class ATTUSNewLookupCtrlAccount {
public class MyException extends Exception{}
public string errMsg {get;set;}
public Boolean isError {get;set;}
public Boolean autolookup {get;set;}
private final ATTUSWDSAccount__c tmpData;
private final Account acct;
private boolean isTest = false;
private boolean isErrorTest = false;
private list<ATTUSWDSAccount__c> rslt;

      public ATTUSNewLookupCtrlAccount(ApexPages.StandardController stdController) 
      {
          tmpData= (ATTUSWDSAccount__c) stdController.getRecord();
          
          acct = [select id, Name, billingstreet, billingcity, billingpostalcode, billingstate, billingcountry,
          shippingstreet, shippingcity, shippingpostalcode, shippingstate, shippingcountry from Account where id =
          :tmpData.Account__c];
           autolookup = true;     
           errMsg = '';
           isError = false;
           rslt = new list<ATTUSWDSAccount__c>();
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please wait while the ATTUS Watchlist Check is performed.....');
           ApexPages.addMessage(myMsg);
           
      }

      public ATTUSNewLookupCtrlAccount(ApexPages.StandardController stdController, boolean testRun, boolean errorTestRun) 
      {
          tmpData= (ATTUSWDSAccount__c) stdController.getRecord();
          
          acct = [select id, Name, billingstreet, billingcity, billingpostalcode, billingstate, billingcountry,
          shippingstreet, shippingcity, shippingpostalcode, shippingstate, shippingcountry from Account where id =
          :tmpData.Account__c];
           autolookup = true;     
           errMsg = '';
           isError = false;
           isTest = testRun;
           isErrorTest = errorTestRun;
           rslt = new list<ATTUSWDSAccount__c>();
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please wait while the ATTUS Watchlist Check is performed.....');
           ApexPages.addMessage(myMsg);
      }
      
      private boolean addressMatch(Account acct)
      {
          boolean retVal = true; //default to matching
          if(acct.billingstreet != acct.shippingstreet)
          {
              retval = false;
          }
          if(acct.billingcity != acct.shippingcity)
          {
              retval = false;
          }
          if(acct.billingstate != acct.shippingstate)
          {
              retval = false;
          }
          if(acct.billingpostalcode != acct.shippingpostalcode)
          {
              retval = false;
          }
          if(acct.billingcountry != acct.shippingcountry)
          {
              retval = false;
          }
          
          return retVal;
      }

      

      public pageReference checkList()
      {
      PageReference pageRef = null;
      string results='';
      httpRequest req = null;
      boolean gotBilling = false;
      boolean gotShipping = false;
      boolean sentShipping = false;
      autolookup = false;
      try
      {
        ATTUSWatchDOGSFLookup lu = null;
        
        if(acct.billingstreet != null || acct.billingcity != null || acct.billingstate != null || acct.billingpostalcode != null || acct.billingcountry != null)
        {
            gotBilling = true;
        }
        
        if(acct.shippingstreet != null || acct.shippingcity != null || acct.shippingstate != null || acct.shippingpostalcode != null || acct.shippingcountry != null)
        {
            gotShipping = true;
        }        
        
        //if(acct.billingstreet != null || acct.billingcity != null || acct.billingstate != null || acct.billingpostalcode != null || acct.billingcountry != null)
        //{
        lu = new ATTUSWatchDOGSFLookup();
        //setup address parameters
        
        if(gotBilling == true)
        {
        if(acct.billingstreet != null)
            {
            if(acct.billingstreet.length() <= 100)
                {
                lu.street = acct.billingstreet;
                }
            else
                {
                lu.street = acct.billingstreet.substring(0,100);
                }
            }
        if(acct.billingcity != null)
            {
            lu.city = acct.billingcity;
            }
        if(acct.billingstate != null)
            {
            lu.state = acct.billingstate;
            }
        if(acct.billingpostalcode != null && (acct.billingcountry != null && acct.billingcountry != '')) //if country is blank, do not send zip code
            {
            //if us country, send only left 5 of zip
            if(acct.billingcountry == 'US' || acct.billingcountry == 'USA' || acct.billingcountry == 'U.S.' || acct.billingcountry == 'U.S.A.' || acct.billingcountry.contains('United States'))
                {
                if(lu.postalcode.length() <= 5)
                    {
                    lu.postalcode = acct.billingpostalcode;
                    }
                else
                    {
                    lu.postalcode = acct.billingpostalcode.substring(0,5);
                    }
                }
            else
                {
                lu.postalcode = acct.billingpostalcode;
                }
            }
        if(acct.billingcountry != null)
            {
            lu.country = acct.billingcountry;
            }
         }
         else if(gotBilling == false && gotShipping == true)
         {
            if(acct.shippingstreet != null)
                {
                if(acct.shippingstreet.length() <= 100)
                    {
                    lu.street = acct.shippingstreet;
                    }
                else
                    {
                    lu.street = acct.shippingstreet.substring(0,100);
                    }
                }
            if(acct.shippingcity != null)
                {
                lu.city = acct.shippingcity;
                }
            if(acct.shippingstate != null)
                {
                lu.state = acct.shippingstate;
                }
            if(acct.shippingpostalcode != null && (acct.shippingcountry != null && acct.shippingcountry != '')) //if country is blank, do not send zip code
                {
                //if us country, send only left 5 of zip
                if(acct.shippingcountry == 'US' || acct.shippingcountry == 'USA' || acct.shippingcountry == 'U.S.' || acct.shippingcountry == 'U.S.A.' || acct.shippingcountry.contains('United States'))
                    {
                    if(acct.shippingpostalcode.length() <= 5)
                        {
                        lu.postalcode = acct.shippingpostalcode;
                        }
                    else
                        {
                        lu.postalcode = acct.shippingpostalcode.substring(0,5);
                        }
                    }
                else
                    {
                    lu.postalcode = acct.shippingpostalcode;
                    }
                }
            if(acct.shippingcountry != null)
                {
                lu.country = acct.shippingcountry;
                }
            sentShipping = true;
         } 
               
        req = lu.createRequestBus(acct.Name);
        if(isTest == false)
            {
            results = lu.checkList(req);
            }
        else        
            {
                if(isErrorTest == false)
                {            
                results = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ATTUSWATCHDOGRESPONSE tranid="317302038" trandate="2011-04-01 09:11:10"><BILLINGCODE partnerid="0f066555-caee-439f-88d7-7ce72767592b " billingid="0f066555-caee-439f-88d7-7ce72767592b" clientdefined1="SF Testing" clientdefined2="" clientdefined3="" /><LISTS><LIST code="OFAC" /></LISTS><QUERY><ITEM operator="" threshold=".85" surname="" givenname="" unparsedname="" othername="" allentities="ACEFROSTY SHIPPING" id="" state="" city="" postalcode="" country="" blockedcountryname="" /></QUERY><SEARCHCRITERIA><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-03-31 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-03-31" /><ENTITIES><ENTITY entityid="5" entitynum="25" thisentitysmaxscore="0.980"><LASTNAME /><FIRSTNAME /><OTHERNAME>ACEFROSTY SHIPPING CO., LTD.</OTHERNAME><WHOLENAME /><ISALIASHIT>0</ISALIASHIT><SCORE>0.980</SCORE><TYPE>other</TYPE><PROGRAMS>CUBA(Cuba)</PROGRAMS><REMARKS>Modified On or Before: 11/15/2002; </REMARKS><ALIASES /><ADDRESSES><ADDRESS streetaddress="171 Old Bakery Street" city="Valletta" country="Malta" remark="" state="" zip="" /></ADDRESSES></ENTITY></ENTITIES></SEARCHCRITERIA></ATTUSWATCHDOGRESPONSE></soap:Body></soap:Envelope>';
                }
                else
                {
                results = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><soap:Fault><faultcode>soap:Client</faultcode><faultstring>Server did not recognize the value of HTTP Header SOAPAction: ProcessNameListMessage_An.</faultstring><detail /></soap:Fault></soap:Body></soap:Envelope>';
                }
            }
        lu.parseDOC(results);

        if(lu.retErrMsg.size() > 0)
        {
            string errMsgs = '';
            for(string s: lu.retErrMsg)
            {
             if(s<> '')
                 {
                     if(s.contains('ERR-67001') || s.contains('ERR-67007') || s.contains('ERR-67008')  || s.contains('ERR-67010')  || s.contains('ERR-67012') || s.contains('ERR-67013') || s.contains('ERR-67014') || s.contains('ERR-67021') || s.contains('ERR-67023') || s.contains('ERR-67024') || s.contains('ERR-67301'))
                     {
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'The watch list check could not be completed – ATTUS Error:  Client is unauthorized.  Please have your Salesforce administrator contact ATTUS Support at 888-494-8449 option 3.');
                     ApexPages.addMessage(myMsg);
                     }
                     else
                     {
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,s);
                     ApexPages.addMessage(myMsg);
                     }
                    
                     errMsgs = errMsgs + ' : ' + s;    
                 }
             }
             isError = true;
             errMsg = errMsg + ' : ' + errMsgs;  

        }
        
        for (Integer x = 0; x < lu.retXmlData.size() ; x++ ) 
        {
            ATTUSWDSAccount__c layout = new ATTUSWDSAccount__c();
            layout.account__c = acct.id;
            layout.List_Date__c = Date.valueOf(lu.retListDate[x]);
            layout.Number_of_Hits__c = lu.retHitCount[x];
            layout.List_Description__c = lu.retListName[x];
            layout.Result_Node__c = lu.retXmlData[x];
            layout.name_searched__c = acct.Name;
            if(gotBilling == true)
            {
            layout.address_type__c = 'Billing';
            if(acct.billingstreet != null)
            {
                if(acct.billingstreet.length() <= 100)
                    {
                    layout.search_street__c = acct.billingstreet;
                    }
                else
                    {
                    layout.search_street__c = acct.billingstreet.substring(0,100);
                    }
            }
            else
            {
             layout.search_street__c = '';
            }
            layout.search_city__c = acct.billingcity;
            layout.search_state__c = acct.billingstate;
            layout.search_postal_code__c = acct.billingpostalcode;
            layout.search_country__c = acct.billingcountry;
            }
            else if(gotBilling == false && gotShipping == true)
            {
            layout.address_type__c = 'Shipping';
            if(acct.shippingstreet != null)
            {
                if(acct.shippingstreet.length() <= 100)
                    {
                    layout.search_street__c = acct.shippingstreet;
                    }
                else
                    {
                    layout.search_street__c = acct.shippingstreet.substring(0,100);
                    }
            }
            else
            {
             layout.search_street__c = '';
            }
            layout.search_city__c = acct.shippingcity;
            layout.search_state__c = acct.shippingstate;
            layout.search_postal_code__c = acct.shippingpostalcode;
            layout.search_country__c = acct.shippingcountry;            
            }
            layout.lookup_by__c = userinfo.getname() + ' ' + datetime.now().formatlong();
            if(layout.number_of_hits__c > 0)
                {
                layout.review_status__c = 'Not Reviewed';
                }
            else
                {
                layout.review_status__c = 'No match';
                }            
            rslt.add(layout);
        }   
        //}  //end of billing address
        
        if(gotShipping == true && sentShipping == false && addressMatch(acct) == false)
        {
        lu = new ATTUSWatchDOGSFLookup();
        //setup address parameters
        if(acct.shippingstreet != null)
            {
            if(acct.shippingstreet.length() <= 100)
                {
                lu.street = acct.shippingstreet;
                }
            else
                {
                lu.street = acct.shippingstreet.substring(0,100);
                }            
            }
        if(acct.shippingcity != null)
            {
            lu.city = acct.shippingcity;
            }
        if(acct.shippingstate != null)
            {
            lu.state = acct.shippingstate;
            }
        if(acct.shippingpostalcode != null && (acct.shippingcountry != null && acct.shippingcountry != '')) //if country is blank, do not send zip code
            {
            //if us country, send only left 5 of zip
            if(acct.shippingcountry == 'US' || acct.shippingcountry == 'USA' || acct.shippingcountry == 'U.S.' || acct.shippingcountry == 'U.S.A.' || acct.shippingcountry.contains('United States'))
                {
                if(acct.shippingpostalcode.length() <= 5)
                    {
                    lu.postalcode = acct.shippingpostalcode;
                    }
                else
                    {
                    lu.postalcode = acct.shippingpostalcode.substring(0,5);
                    }
                }
            else
                {
                lu.postalcode = acct.shippingpostalcode;
                }
            }
        if(acct.shippingcountry != null)
            {
            lu.country = acct.shippingcountry;
            }
                
        req = lu.createRequestBus(acct.Name);
        if(isTest == false)
            {
            results = lu.checkList(req);
            }
        else        
            {
                if(isErrorTest == false)
                {            
                results = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ATTUSWATCHDOGRESPONSE tranid="317302038" trandate="2011-04-01 09:11:10"><BILLINGCODE partnerid="0f066555-caee-439f-88d7-7ce72767592b " billingid="0f066555-caee-439f-88d7-7ce72767592b" clientdefined1="SF Testing" clientdefined2="" clientdefined3="" /><LISTS><LIST code="OFAC" /></LISTS><QUERY><ITEM operator="" threshold=".85" surname="" givenname="" unparsedname="" othername="" allentities="ACEFROSTY SHIPPING" id="" state="" city="" postalcode="" country="" blockedcountryname="" /></QUERY><SEARCHCRITERIA><NUMRECORDS totalhits="1" totalentityhits="1" totalblockedcountryhits="0" /><SEARCHLISTINFORMATION name="Office of Foreign Assets Control" description="2011-03-31 OFAC Source Data" listcode="OFAC" threshold="0.85" listdate="2011-03-31" /><ENTITIES><ENTITY entityid="5" entitynum="25" thisentitysmaxscore="0.980"><LASTNAME /><FIRSTNAME /><OTHERNAME>ACEFROSTY SHIPPING CO., LTD.</OTHERNAME><WHOLENAME /><ISALIASHIT>0</ISALIASHIT><SCORE>0.980</SCORE><TYPE>other</TYPE><PROGRAMS>CUBA(Cuba)</PROGRAMS><REMARKS>Modified On or Before: 11/15/2002; </REMARKS><ALIASES /><ADDRESSES><ADDRESS streetaddress="171 Old Bakery Street" city="Valletta" country="Malta" remark="" state="" zip="" /></ADDRESSES></ENTITY></ENTITIES></SEARCHCRITERIA></ATTUSWATCHDOGRESPONSE></soap:Body></soap:Envelope>';
                }
                else
                {
                results = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><soap:Fault><faultcode>soap:Client</faultcode><faultstring>Server did not recognize the value of HTTP Header SOAPAction: ProcessNameListMessage_An.</faultstring><detail /></soap:Fault></soap:Body></soap:Envelope>';
                }
            }
        lu.parseDOC(results);

        if(lu.retErrMsg.size() > 0)
        {
            string errMsgs = '';
            for(string s: lu.retErrMsg)
            {
             if(s<> '')
                 {
                     if(s.contains('ERR-67001') || s.contains('ERR-67007') || s.contains('ERR-67008')  || s.contains('ERR-67010')  || s.contains('ERR-67012') || s.contains('ERR-67013') || s.contains('ERR-67014') || s.contains('ERR-67021') || s.contains('ERR-67023') || s.contains('ERR-67024') || s.contains('ERR-67301'))
                     {
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'The watch list check could not be completed – ATTUS Error:  Client is unauthorized.  Please have your Salesforce administrator contact ATTUS Support at 888-494-8449 option 3.');
                     ApexPages.addMessage(myMsg);
                     }
                     else
                     {
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,s);
                     ApexPages.addMessage(myMsg);
                     }
                 }
             }
             isError = true;
             errMsg = errMsg + ' : ' + errMsgs;  

        }
        
        for (Integer x = 0; x < lu.retXmlData.size() ; x++ ) 
        {
            ATTUSWDSAccount__c layout = new ATTUSWDSAccount__c();
            layout.account__c = acct.id;
            layout.List_Date__c = Date.valueOf(lu.retListDate[x]);
            layout.Number_of_Hits__c = lu.retHitCount[x];
            layout.List_Description__c = lu.retListName[x];
            layout.Result_Node__c = lu.retXmlData[x];
            layout.name_searched__c = acct.Name;
            layout.address_type__c = 'Shipping';
            if(acct.shippingstreet != null)
            {
                if(acct.shippingstreet.length() <= 100)
                    {
                    layout.search_street__c = acct.shippingstreet;
                    }
                else
                    {
                    layout.search_street__c = acct.shippingstreet.substring(0,100);
                    }
            }  
            else
            {
             layout.search_street__c = '';
            }
            layout.search_city__c = acct.shippingcity;
            layout.search_state__c = acct.shippingstate;
            layout.search_postal_code__c = acct.shippingpostalcode;
            layout.search_country__c = acct.shippingcountry;
            layout.lookup_by__c = userinfo.getname() + ' ' + datetime.now().formatlong();
            if(layout.number_of_hits__c > 0)
                {
                layout.review_status__c = 'Not Reviewed';
                }
            else
                {
                layout.review_status__c = 'No match';
                }            
            rslt.add(layout);
        }   
        }  //end of shipping address
             

        if(isError == true)
            {
                throw new MyException('');
            }
          
        }
        catch (MyException ex)
        {
         return null;
        }
        
        finally
        {
        pageRef = new PageReference('/' + acct.id);  
        pageRef.setRedirect(true);  
        if(rslt.size() > 0)
            {
            insert rslt;      
            }
        }
        return pageRef;
      }
      

      public PageReference goBack()
      {
        PageReference pageRef = new PageReference('/' + acct.id);  
        pageRef.setRedirect(true);
        return pageRef;  
      }

}