Global Class LeadToContactConvertor {
    Webservice Static String LeadConversionWeb(String leadId) {
        return ExecuteConversion(leadId, true, true);
    }
    
    Webservice Static String LeadConversion(String leadId) {
        return ExecuteConversion(leadId, false, false);
    }
    
    public Static String ExecuteConversion(String leadId, Boolean doSoftPull, Boolean verifyBeforeCP) {
        
        String ContactId ='', prodName = 'Lending Point';
        Decimal AnnualIncome = 0;
        Boolean bLFound = false, doCreditPull = false, bCreditReportExists = false, bIsSandBox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        Boolean bOverIncome = false ;
        Map<String, String> inputInfo = new Map<String, String>();
        List<Contact> lCon = null;
        
        if (String.isEmpty(leadId)) return 'Error: LeadID was blank';
        
        id ConLdId = leadId;
        String objType = ConLdId.getSObjectType().getDescribe().getName(); //get object name from record id
        
        if(objType == 'Lead') {
            for ( Lead l : [Select Id, IsConverted, ConvertedContactId, 
                Lead_Sub_Source__c, LeadSource, Name, Authorization_to_pull_credit__c , 
                Point_Code__c, Email, FirstName, LastName, Annual_Income__c,
                Loan_Amount__c, Account__c, Contact__c 
                from Lead
                where Id = : leadId]) {
                
                Contact c;
                AnnualIncome = l.Annual_Income__c;
                bLFound = true;     
                                        
                if (!l.IsConverted) {
                    lcon    = [Select  Lead_Sub_Source__c, LeadSource, Id, AccountId, Authorization_to_pull_credit__c,
                               Firstname, Lastname, Email, Point_Code__c, Annual_Income__c, Product__c
                               from Contact
                               where Lead__c = : l.Id
                               order by ID desc
                               Limit 1];
                } else {
                    lcon = [Select  Lead_Sub_Source__c, Unit__c, LeadSource, Id, AccountId, Authorization_to_pull_credit__c,
                               Firstname, Lastname, Email, Point_Code__c, Annual_Income__c, Product__c
                               from Contact
                               where Id = : l.ConvertedContactId];
                }
                
                if (l.IsConverted) ContactId = l.ConvertedContactId;
                
                if (lCon.size() >  0) {
                    c =  lCon[0];
                    doCreditPull = c.Authorization_to_pull_credit__c != null && c.Authorization_to_pull_credit__c;
                    AnnualIncome = c.Annual_Income__c;
                    ContactId = c.Id;
                    System.debug('Found Existing Contact.');
                    if (String.isNotEmpty(c.Product__c)) prodName = c.Product__c;
                    if (!prodName.equals('Lending Point')) bOverIncome = true;
                } 
                
                if (!verifyBeforeCP || (l.Authorization_to_pull_credit__c != null && l.Authorization_to_pull_credit__c) || doCreditPull ) {
                    doCreditPull = true;
                } else {
                    System.debug('Credit pull not authorized for lead ' + l.FirstName);
                }
                
                if (doCreditPull && String.isNotEmpty(ContactId) && !bIsSandBox && !SubmitApplicationService.checkExistanceByContact(ContactId)) {
                        // we will always say true. Application not created. But that way, process continues, but will not return offers.
                        System.debug('Already has a contract, or was recently declined');
                        doCreditPull = false;
                }
                    
                if (doCreditPull) {
                    try {
                        if (AnnualIncome >= 20000 || bOverIncome  ) {
                            SubmitApplicationService submit2 = new SubmitApplicationService();                                    
                            SubmitApplicationService submit = new SubmitApplicationService();
                            String appID = submit2.submitApplication(ContactId);
                            System.debug('App created Id = ' + appID);
                        } else {
                            System.debug('Income is Less than 20K.. Reported Income = ' + AnnualIncome);
                            ContactCtrl.markAsIncomeNotEnough(ContactId);
                            Return 'Income is less than 20K';
                        }
                    } catch (Exception e) {
                        WebToSFDC.notifyDev('LeadToContactConvertor Error - App submission', 'Lead Id = ' + leadId + ' - ' + e.getMessage() + ' line ' + e.getLineNumber() + '\n' + e.getStackTraceString());
                        SendLeadEmail(l);
                    
                        return 'Error in Application submission. ' + e.getMessage();
                    }
                }
            }    
        }  else if(objType == 'Contact') {
               for(Contact c : [Select  Lead_Sub_Source__c, Unit__c, LeadSource, Id, AccountId, Authorization_to_pull_credit__c,
                               Firstname, Lastname, Email, Point_Code__c, Annual_Income__c, Product__c
                               from Contact where Id = : leadId] ) {
                
                doCreditPull = c.Authorization_to_pull_credit__c != null && c.Authorization_to_pull_credit__c;
                AnnualIncome = c.Annual_Income__c;
                ContactId = c.Id;
                System.debug('Found Existing Contact.');
                if (String.isNotEmpty(c.Product__c)) prodName = c.Product__c;
                if (!prodName.equals('Lending Point')) bOverIncome = true;
                
                if (!verifyBeforeCP || (c.Authorization_to_pull_credit__c != null && c.Authorization_to_pull_credit__c) || doCreditPull ) {
                    doCreditPull = true;
                } else {
                    System.debug('Credit pull not authorized for Contact' + c.FirstName);
                }
                
                if (doCreditPull && String.isNotEmpty(ContactId) && !bIsSandBox && !SubmitApplicationService.checkExistanceByContact(ContactId)) {
                    // we will always say true. Application not created. But that way, process continues, but will not return offers.
                    System.debug('Already has a contract, or was recently declined');
                    doCreditPull = false;
                }
                    
                if (doCreditPull) {
                    try {
                        if (AnnualIncome >= 20000 || bOverIncome  ) {
                            SubmitApplicationService submit2 = new SubmitApplicationService();                                    
                            SubmitApplicationService submit = new SubmitApplicationService();
                            String appID = submit2.submitApplication(ContactId);
                            System.debug('App created Id = ' + appID);
                        } else {
                            System.debug('Income is Less than 20K.. Reported Income = ' + AnnualIncome);
                            ContactCtrl.markAsIncomeNotEnough(ContactId);
                            Return 'Income is less than 20K';
                        }
                    } catch (Exception e) {
                        WebToSFDC.notifyDev('LeadToContactConvertor Error - App submission', 'Lead Id = ' + leadId + ' - ' + e.getMessage() + ' line ' + e.getLineNumber() + '\n' + e.getStackTraceString());
                        SendLeadEmail(c);
                    
                        return 'Error in Application submission. ' + e.getMessage();
                    }
                }
            }
        }

        Return bLFound ? 'Application created' : 'Error: LeadId not found';
    }
    
    @future
    public static void doLeadConversion(String leadId, String AccountID, String ContactID){
        try {
            for (Contact c : [Select Id, AccountId, LastName, FirstName, Mailingstreet, MailingCity,
                              MailingState, MailingPostalCode, MailingCountry, Phone, Lead__c
                              From Contact where Id =: ContactId]){
                                  
                AccountID= c.AccountId;
                
                if (c.AccountId == null ) {
                    Account a = new Account();
                    a.Name = c.FirstName + ' ' + c.LastName;
                    a.Billingstreet = c.Mailingstreet;
                    a.BillingCity = c.MailingCity;
                    a.BillingState = c.Mailingstate;
                    a.BillingPostalCode = c.MailingPostalCode;
                    a.BillingCountry = c.MailingCountry;
                    a.Phone = c.Phone;
                    a.Type = 'Customer';
                    a.Contact__c = c.Id;    
                    insert a;
                    c.AccountId = a.Id;
                    update c;
                    
                    AccountId = a.Id;    
                  
                }
                
              /*  for (Opportunity opp : [Select Id, AccountId from Opportunity where AccountId = null AND contact__c =: c.Id ])  {
                    opp.AccountID = AccountID;
                    update opp;
                }*/
            }
        } catch (Exception e) {
            System.debug('ERROR ' + e.getMessage());
            WebToSFDC.notifyDev('doLeadConversion Error - Conversion', 'Lead Id = ' + leadId + ' - ' + e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
            
        }
    }
    
    public static void SendLeadEmail(Contact ld) {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        WebToSFDC.notifyDev('Web Lead Submitted : ' + ld.FirstName + ' ' + ld.LastName, ' A customer(' + ld.FirstName + ' ' + ld.LastName + ') has just applied via the Web. \n\n Please follow this link to reach the Contact): \n \n https://' + System.Url.getSalesforceBaseUrl().getHost() + '/' + ld.Id , cuSettings.Email_Alert_Portal_Apps__c );
    }
    
    public static void SendLeadEmail(Lead ld) {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        WebToSFDC.notifyDev('Web Lead Submitted : ' + ld.FirstName + ' ' + ld.LastName, ' A customer(' + ld.FirstName + ' ' + ld.LastName + ') has just applied via the Web. \n\n Please follow this link to reach the Contact): \n \n https://' + System.Url.getSalesforceBaseUrl().getHost() + '/' + ld.Id , cuSettings.Email_Alert_Portal_Apps__c );
    }
    
    public static void afterLeadInsertHandler(List<Contact> newRecs, List<Contact> oldRecs){
         try {
        System.debug('LeadDuplicateCheck_After');
        List<CampaignMember> newCam = new List<CampaignMember>();
        
        for (Contact myContact : newRecs) {
            // Web LEad
            String subSource = myContact.Lead_Sub_Source__c == null ? '' : myContact.Lead_Sub_Source__c.trim().toUpperCase() ;
            
            if (subSource.indexOf('LPCUSTOMERPORTAL') >= 0 && String.isNotEmpty(myContact.DuplicateContactToDelete__c) ) {
                // Let's copy over all Campaign Data and then delete the Lead
                for (Contact exL : [SELECT Id  FROM Contact where Id =: myContact.DuplicateContactToDelete__c LIMIT 1 ]) {
                    CampaignMember lastCamp = null, tLastCamp = null;
                    for (CampaignMember me : [Select Id, LeadID, CampaignID, ContactID,  Status, HasResponded, Point_Code__c from CampaignMember where ContactID =: exL.Id Order by CreatedDate asc]) {
                        CampaignMember nCam = new CampaignMember( CampaignID = me.CampaignID, ContactID = myContact.Id, Status = me.Status);
                        nCam.Point_Code__c = me.Point_Code__c;
                        if (String.isNotEmpty(nCam.Point_Code__c) && lastCamp == null && nCam.Point_Code__c == myContact.Point_Code__c ) lastCamp = nCam;
                        newCam.add(nCam);
                        tLastCamp = nCam;
                    }
                    if (lastCamp == null ) lastCamp= tLastCamp;
                    if (lastCamp!= null) lastCamp.Status = 'Responded' ;
                    
                    try { delete exL; } 
                    catch (Exception e) {}
                }
            }
        }
        if (newCam.size() > 0) upsert newCam;   
        
    } catch (Exception e){}
    }
    
}