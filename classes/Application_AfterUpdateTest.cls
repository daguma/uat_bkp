@isTest
private class Application_AfterUpdateTest {

    @isTest static void inserts_application() {

        Integer appsBefore = [SELECT COUNT() FROM genesis__applications__c];

        genesis__applications__c app = LibraryTest.createApplicationTH();

        System.assert([SELECT COUNT() FROM genesis__applications__c] > appsBefore);
    }

    @isTest static void updates_application_fico() {

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__status__c = 'Documents In Review';
        app.FICO__c = '730';
        app.Fico_Reviewed__c = false;
        update app;

        System.assertEquals('Credit Review', [SELECT genesis__status__c FROM genesis__applications__c WHERE id = : app.id LIMIT 1].genesis__status__c);
    }

    @isTest static void updates_application_timeout_warnings() {

        Integer timeoutBefore = [SELECT COUNT() FROM TimeoutWarnings__c];

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__status__c = 'Credit Qualified';
        app.OwnerId = [SELECT id FROM User WHERE Name = 'HAL' LIMIT 1].id;
        update app;

        System.assert([SELECT COUNT() FROM TimeoutWarnings__c] > timeoutBefore);

    }

    @isTest static void updates_application_clear() {
        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__status__c = 'Offer Accepted';
        update app;

        //System.assertNotEquals(null, [SELECT Clear_Relevance__c FROM Contact WHERE id =: app.genesis__contact__c LIMIT 1].Clear_Relevance__c);
    }

    @isTest static void updates_application_idology() {

        genesis__applications__c app = LibraryTest.createApplicationTH();

        Logging__c testLog = new Logging__c();
        testLog.Webservice__c = 'Kount';
        testLog.api_response__c = '{MOBILE_DEVICE=VMAX=0 MODE=Q IP_CITY= RULE_DESCRIPTION_0=Email Whitelist Approval IP_ORG= TRAN=PM5M0W06W7JV ' +
                                  'CARDS=1 PC_REMOTE= PIP_COUNTRY= SESS=5E32234FB75F447F064D8AF59DDF6C19 ORDR= FINGERPRINT= COUNTRY= SCOR=31 ' +
                                  'MASTERCARD= VERS=0630 IP_COUNTRY= BRND=NONE VOICE_DEVICE= TIMEZONE= PIP_REGION= LANGUAGE= PIP_LAT= IP_REGION= ' +
                                  'DSR= IP_IPAD= DEVICE_LAYERS=.... FLASH= REGION= AUTO=A IP_LON= PIP_IPAD= JAVASCRIPT= BROWSER= IP_LAT= PROXY= ' +
                                  'WARNING_COUNT=0 UAS= LOCALTIME=  RULES_TRIGGERED=1 RULE_ID_0=772856 EMAILS=1 HTTP_COUNTRY= MOBILE_FORWARDER= ' +
                                  'PIP_CITY= DDFS= VELO=0 COUNTERS_TRIGGERED=0 NETW=N SITE=DEFAULT COOKIES= OS= PIP_ORG= GEOX=IN REASON_CODE= ' +
                                  'MERC=146300 PIP_LON= KAPT=N DEVICES=1 REGN=IN_05 MOBILE_TYPE=}';
        testLog.Contact__c = app.genesis__contact__c;
        insert testLog;

        IDology_Request__c testIdology = new IDology_Request__c();
        testIdology.Qualifiers__c = '<li>DOB/YOB Not Available</li>';
        testIdology.Contact__c = app.genesis__contact__c;
        insert testIdology;

        app.genesis__status__c = 'Documents In Review';
        app.FICO__c = '730';
        app.Fico_Reviewed__c = false;
        update app;

        app = [SELECT Review_Reason__c, genesis__Status__c FROM genesis__applications__c WHERE id = : app.Id LIMIT 1];

        System.assertEquals('Identify/KYC,Credit Profile', app.Review_Reason__c);
        System.assertEquals('Credit Review', app.genesis__Status__c);
    }

    @isTest static void updates_application_expected_first_payment() {
        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.Selected_Offer_Term__c = 24;
        app.genesis__Expected_First_Payment_Date__c = Date.today().addDays(20);
        app.genesis__Expected_Start_Date__c = Date.today().addDays(10);
        app.genesis__Payment_Amount__c = 300;
        app.genesis__Term__c = 24;
        app.genesis__Payment_Frequency__c = 'Monthly';
        app.genesis__Loan_Amount__c = 5000;
        update app;

        app = [SELECT genesis__APR__c, genesis__Expected_Close_Date__c FROM genesis__applications__c WHERE id = : app.id LIMIT 1];

        System.assertNotEquals(null, app.genesis__APR__c);
        System.assertNotEquals(null, app.genesis__Expected_Close_Date__c);

    }

}