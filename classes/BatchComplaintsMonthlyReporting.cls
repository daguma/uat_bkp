global class BatchComplaintsMonthlyReporting implements Database.Batchable<sObject>, Schedulable,Database.AllowsCallouts{

    TimeZone tZone = UserInfo.getTimeZone();
    FinwiseDetails__c FinwiseParams = FinwiseDetails__c.getInstance();       
    
    public void execute(SchedulableContext sc) { 
       BatchComplaintsMonthlyReporting batchapex = new BatchComplaintsMonthlyReporting();
       id batchprocessid = Database.executebatch(batchapex,integer.valueOf(FinwiseParams.Complaints_Batch_Size__c));
       system.debug('Process ID: ' + batchprocessid);   
              
    }
    
    global Database.queryLocator start(Database.BatchableContext BC){  
           
       string query = 'select id,Criticality__c,Opportunity__c, Opportunity__r.name, Opportunity__r.ACH_Account_Number__c, Opportunity__r.ProductName__c, OwnerId,ClosedDate,Category__c,createdDate';
              query += ',CaseNumber,Credit_Product_Program_Line__c, Contact.Name,Description,Reason__c,Complain_reported_to_Finwise__c,';             
              query += 'Source_of_Complaint__c,Summary_of_Resolution__c, Status from Case where createdDate = LAST_MONTH and Opportunity__r.Finwise_Approved__c = true';
               
       system.debug('=========query============'+query);
       return database.getQueryLocator(query);
    }    
    
    global void execute(Database.BatchableContext BC, List<Case> scope){
       system.debug('=========scope============'+scope); 
        list<logging__c> loggingList = new list<logging__c>(); 
        list<Case> caseListToBeUpdated = new list<Case>();
        Case currentCase;     
        try{
        //Finwise_Details__c FinwiseParams = Finwise_Details__c.getInstance('finwise'); 
        list<finwiseComplaintResponse> finwiseComplaintResponseList =  new list<finwiseComplaintResponse>();       
        list<logging__c> logsList = new list<logging__c>();
        string reqBody;         
      
       for(Case c: scope){ 
           currentCase = c;                                              
            integer createdDatetZoneOffset = tZone.getOffset(c.createdDate);            
            JSONGenerator generator = JSON.createGenerator(false); 
                                                      
            generator.writeStartObject();                                             
            generator.writeStringField('AccountNumber', (c.Opportunity__r.ACH_Account_Number__c <> null)?c.Opportunity__r.ACH_Account_Number__c:'');
            generator.writeStringField('ApplicationID', (c.Opportunity__r.Name <> null)?c.Opportunity__r.Name:'');
            generator.writeStringField('AssignedTo', c.OwnerId);
            if(c.ClosedDate <> null)  generator.writeStringField('ClosedDate', '\\/Date('+c.ClosedDate.getTime()+tZone.getOffset(c.ClosedDate)+')\\/');                     
             
            generator.writeNumberField('ComplaintCategoryId', getCategoryId(c.Category__c));
            generator.writeStringField('ComplaintKey', c.CaseNumber);                             
            generator.writeNumberField('ComplaintSourceId',getSourceId(c.Source_of_Complaint__c));               
            generator.writeNumberField('ComplaintTierId', getTierId(c.Criticality__c));
            generator.writeStringField('CreationDate', '\\/Date('+c.CreatedDate.getTime()+createdDatetZoneOffset+')\\/');
            generator.writeStringField('CreditProduct',(c.Credit_Product_Program_Line__c <> null)?c.Credit_Product_Program_Line__c:'');
            generator.writeStringField('CustomerName', (c.Contact.Name <> null)?c.Contact.Name:'');                 
            generator.writeStringField('Description',(c.Description <> null)?c.Description.replace('\n', ' ').replace('\r', ' ').replace('\"', ' '):'');                                
            generator.writeNumberField('Id', 0);
            
           /**New Changes received on 8-8-2017**/ 
            generator.writeNumberField('ComplaintStatusId', getComplaintStatusId(c.Status)); 
           // generator.writeBooleanField('IsOpen', (c.ClosedDate == null));  
            /**New Changes received on 8-8-2017**/ 
            
           /***4743/4741 -  Product ID Modification for FinWise API - Start***/
            string ProductID = FinwiseParams <> null ? ((generalPurposeWebServices.isPointOfNeed(c.Opportunity__r.ProductName__c) || generalPurposeWebServices.isSelfPayProduct(c.Opportunity__r.ProductName__c)) ? FinwiseParams.Finwise_MedicalProductId__c : FinwiseParams.Finwise_LendingPartnerProductId__c) : ''; //SM-438
            generator.writeStringField('LendingPartnerProductId',string.isNotEmpty(ProductID) ? ProductID  : '');
            /***4743/4741 -  Product ID Modification for FinWise API - END***/
            
            generator.writeStringField('Merchant','' );
            generator.writeStringField('Notes', (c.Reason__c <> null)?c.Reason__c:'');
            generator.writeStringField('ReceivedDate', '\\/Date('+c.CreatedDate.getTime()+createdDatetZoneOffset+')\\/');                
            generator.writeStringField('Resolution', (c.Summary_of_Resolution__c <> null)?c.Summary_of_Resolution__c:'');
            generator.writeEndObject();                              
                         
            reqBody = generator.getAsString(); 
            system.debug('============reqBody ====='+reqBody );
            reqBody = reqBody.unescapeJava();             
            HttpRequest req = new HttpRequest();     
                                      
            string Url = FinwiseParams.Finwise_Base_URL__c+'/monthlyreporting/'+FinwiseParams.Finwise_product_Key__c+'/complaint';
            req.setEndpoint(url);
            req.setMethod('POST'); 
            req.SetBody(reqBody);  
            req.setHeader('Content-Type', 'application/json');                     
            Http http = new Http();
            HttpResponse response;// = http.send(req);
            
            if(!Test.isRunningTest()){
                response = http.send(req);
            }else{
                response = new HTTPResponse(); 
                //response.SetBody(reqBody);
                response.setBody('{"Status":"Success"}');
                response.setStatusCode(200);
            }
            system.debug('============response====='+response);
            system.debug('============body====='+response.getbody());
            string RespTOLog = 'Response: '+response+'\n Response Body: '+response.getBody();
                      
           
           if(response.getStatusCode() == 200){              
               if(Finwise.parseStatus((String)response.getBody()) == 'Success'){                                       
                   c.Complain_reported_to_Finwise__c = true;
                   caseListToBeUpdated.add(c);                                                     
               }else{
                   loggingList.add(Finwise.getLoggingForApp('BatchComplaintsMonthlyReporting Unsuccessful for Case: '+c.CaseNumber+' and Application: '+c.Opportunity__r.name, c.Opportunity__c, reqBody, RespToLog));
               }
           }else{                             
               loggingList.add(Finwise.getLoggingForApp('BatchComplaintsMonthlyReporting Error for Case: '+c.CaseNumber+' and Application: '+c.Opportunity__r.name, c.Opportunity__c, reqBody,RespToLog));
           }
           
         }                
       }catch(Exception exc){ 
           system.debug('===error=========='+exc.getmessage() + ' at line no '+exc.getlineNumber());                            
           string ExcpNoteBody = 'BatchComplaintsMonthlyReporting Exception:  '+exc+' at Line No. '+exc.getLineNumber();                  
           loggingList.add(Finwise.getLoggingForApp('BatchComplaintsMonthlyReporting Exception for Case: '+currentCase.CaseNumber+' and Application: '+currentCase.Opportunity__r.name, currentCase.Opportunity__c,'', ExcpNoteBody));                            
           WebToSFDC.notifyDev('BatchComplaintsMonthlyReporting Exception',  exc.getMessage() + ' line ' + (exc.getLineNumber()) + '\n' + exc.getStackTraceString());            
       }
       finally{
             if(loggingList.size() > 0){ insert loggingList; } 
             if(caseListToBeUpdated.size() > 0){ update caseListToBeUpdated; } 
       }
      
    }
    
    global void finish(Database.BatchableContext BC){            
       WebToSFDC.notifyDev('Finwise BatchComplaintsMonthlyReporting Completed', 'Finwise BatchComplaintsMonthlyReporting Completed');        
    }
    
    private static integer getSourceId(string sourceVal){        
       map<string,Finwise_Complaint_Source__c> sourceIdMapping = Finwise_Complaint_Source__c.getAll();
       
       if(sourceVal <> null && sourceVal <> ''){                                                          
            for(Finwise_Complaint_Source__c sId: sourceIdMapping.values()){
                 if(sId.Source__c <> null && (sId.Source__c.toLowerCase() == sourceVal.trim().toLowerCase())){                       
                    return integer.valueOf(sId.Id__c);                    
                 }                         
            }                                                                        
        }                
        return (sourceIdMapping.containskey('default'))? integer.valueOf(sourceIdMapping.get('default').Id__c):0;       
    }
    
    
     private static integer getTierId(string criticalityVal){ 
       map<string,Finwise_Tier_Id_mapping__c> tierIdMapping = Finwise_Tier_Id_mapping__c.getAll();
       
       if(criticalityVal <> null && criticalityVal <> ''){                                                          
            for(Finwise_Tier_Id_mapping__c tId: tierIdMapping.values()){
                 if(tId.Name <> null && (tId.Name.toLowerCase() == criticalityVal.trim().toLowerCase())){                       
                    return integer.valueOf(tId.Id__c);                    
                 }                         
            }                                                                        
        }                
        return (tierIdMapping.containskey('Low'))? integer.valueOf(tierIdMapping.get('Low').Id__c):0;       
    }
    
    
     private static integer getCategoryId(string categoryVal){ 
       map<string,Finwise_Complaint_Categories__c> categoryIdMapping = Finwise_Complaint_Categories__c.getAll(); 
       if(categoryVal <> null && categoryVal <> ''){                                                          
            for(string ctg: categoryIdMapping.keySet()){
                 if(ctg.toLowerCase() == categoryVal.trim().toLowerCase()){                       
                    return integer.valueOf(categoryIdMapping.get(ctg).Id__c);                    
                 }                         
            }                                                                        
        }                
        return (categoryIdMapping.containskey('Application'))? integer.valueOf(categoryIdMapping.get('Application').Id__c):0;       
    }
   
   
   private static integer getComplaintStatusId(string statusVal){ 
       map<string,Finwise_Complaint_Status_Id__c> statusIdMapping = Finwise_Complaint_Status_Id__c.getAll(); 
       if(statusVal <> null && statusVal <> ''){                                                          
            for(string sts: statusIdMapping.keySet()){
                 if(sts.toLowerCase() == statusVal.trim().toLowerCase()){                       
                    return integer.valueOf(statusIdMapping.get(sts).Id__c);                    
                 }                         
            }                                                                        
        }                
        return (statusIdMapping.containskey('New'))? integer.valueOf(statusIdMapping.get('New').Id__c):0;       
    }
   
   
  private static Datetime convertDateToDateTime(Date inputDate){       
        return datetime.newInstance(inputDate.year(),inputDate.month(),inputDate.day());
    }
 
 
  public class finwiseComplaintResponse{        
        string[] Messages;
        string RecordKey, Status;                 
    } 

}