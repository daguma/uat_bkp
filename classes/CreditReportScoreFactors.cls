public with sharing class CreditReportScoreFactors {

    private static map<String, map<String, String>> scoreFactorReasonsMap = new map<String, map<String, String>> {
        '01' => new map<String, String>
        {
            'TU' => 'Amount owed on accounts is too high',
            'EX' => 'Amount owed on accounts is too high'
        },
        '02' => new map<String, String>
        {
            'TU' => 'Level of delinquency on accounts',
            'EX' => 'Level of delinquency on accounts'
        },
        '03' => new map<String, String>
        {
            'TU' => 'Proportion of loan balances to loan amounts is too high',
            'EX' => 'Too few bank/national revolving accounts'
        },
        '04' => new map<String, String>
        {
            'TU' => 'Lack of recent installment loan information',
            'EX' => 'Too many bank/national revolving accounts'
        },
        '05' => new map<String, String>
        {
            'TU' => 'Too many accounts with balances',
            'EX' => 'Too many accounts with balances'
        },
        '06' => new map<String, String>
        {
            'TU' => 'Too many consumer finance company accounts',
            'EX' => 'Too many consumer finance company accounts'
        },
        '07' => new map<String, String>
        {
            'TU' => 'Account payment history is too new to rate',
            'EX' => 'Account payment history is too new to rate'
        },
        '08' => new map<String, String>
        {
            'TU' => 'Too many inquiries last 12 months',
            'EX' => 'Too many inquiries last 12 months'
        },
        '09' => new map<String, String>
        {
            'TU' => 'Too many accounts recently opened',
            'EX' => 'Too many accounts recently opened'
        },
        '10' => new map<String, String>
        {
            'TU' => 'Proportion of balances to credit limits on bank/national revolving or other revolving accounts is too high',
            'EX' => 'Ratio of balance to limit on bank revolving or other rev accts too high'
        },
        '11' => new map<String, String>
        {
            'TU' => 'Amount owed on revolving accounts is too high',
            'EX' => 'Amount owed on revolving accounts is too high'
        },
        '12' => new map<String, String>
        {
            'TU' => 'Length of time revolving accounts have been established',
            'EX' => 'Length of time revolving accounts have been established'
        },
        '13' => new map<String, String>
        {
            'TU' => 'Time since delinquency is too recent or unknown',
            'EX' => 'Time since delinquency is too recent or unknown'
        },
        '14' => new map<String, String>
        {
            'TU' => 'Length of time accounts have been established',
            'EX' => 'Length of time accounts have been established'
        },
        '15' => new map<String, String>
        {
            'TU' => 'Lack of recent bank/national revolving information',
            'EX' => 'Lack of recent bank/national revolving information'
        },
        '16' => new map<String, String>
        {
            'TU' => 'Lack of recent revolving account information',
            'EX' => 'Lack of recent revolving account information'
        },
        '17' => new map<String, String>
        {
            'TU' => 'No recent non-mortgage balance information',
            'EX' => 'No recent non-mortgage balance information'
        },
        '18' => new map<String, String>
        {
            'TU' => 'Number of accounts with delinquency',
            'EX' => 'Number of accounts with delinquency'
        },
        '19' => new map<String, String>
        {
            'TU' => 'Date of last inquiry too recent',
            'EX' => 'Too few accounts currently paid as agreed'
        },
        '20' => new map<String, String>
        {
            'TU' => 'Time since derogatory public record or collection is too short',
            'EX' => 'Time since derogatory public record or collection is too short'
        },
        '21' => new map<String, String>
        {
            'TU' => 'Amount past due on accounts',
            'EX' => 'Amount past due on accounts'
        },
        '24' => new map<String, String>
        {
            'TU' => 'No recent revolving balances',
            'EX' => 'No recent revolving balances'
        },
        '25' => new map<String, String>
        {
            'TU' => '',
            'EX' => 'Length of time installment loans have been established'
        },
        '26' => new map<String, String>
        {
            'TU' => 'Number of bank/national revolving or other revolving accounts',
            'EX' => 'Number of revolving accounts'
        },
        '27' => new map<String, String>
        {
            'TU' => 'Too few accounts currently paid as agreed',
            'EX' => ''
        },
        '28' => new map<String, String>
        {
            'TU' => 'Number of established accounts',
            'EX' => 'Number of established accounts'
        },
        '29' => new map<String, String>
        {
            'TU' => 'No recent bank/national revolving balances',
            'EX' => 'No recent bank/national revolving balances'
        },
        '30' => new map<String, String>
        {
            'TU' => 'Time since most recent account opening is too short',
            'EX' => 'Time since most recent account opening is too short'
        },
        '31' => new map<String, String>
        {
            'TU' => 'Amount owed on delinquent accounts',
            'EX' => 'Too few accounts with recent payment information'
        },
        '32' => new map<String, String>
        {
            'TU' => '',
            'EX' => 'Lack of recent installment loan information'
        },
        '33' => new map<String, String>
        {
            'TU' => '',
            'EX' => 'Proportion of loan balances to loan amounts is too high'
        },
        '34' => new map<String, String>
        {
            'TU' => '',
            'EX' => 'Amount owed on delinquent accounts'
        },
        '36' => new map<String, String>
        {
            'TU' => 'Length of time open installment loans have been established',
            'EX' => 'Length of time open installment loans have been established'
        },
        '37' => new map<String, String>
        {
            'TU' => '',
            'EX' => 'Number of finance co accts established relative to length of finance hist'
        },
        '38' => new map<String, String>
        {
            'TU' => 'Serious delinquency, and public record or collection filed',
            'EX' => 'Serious delinquency, and public record or collection filed'
        },
        '39' => new map<String, String>
        {
            'TU' => 'Serious delinquency',
            'EX' => 'Serious delinquency'
        },
        '40' => new map<String, String>
        {
            'TU' => 'Derogatory public record or collection filed',
            'EX' => 'Derogatory public record or collection filed'
        },
        '53' => new map<String, String>
        {
            'TU' => 'Amount paid down on open mortgage loans is too low',
            'EX' => 'Amount paid down on open mortgage loans is too low'
        },
        '55' => new map<String, String>
        {
            'TU' => 'Amount paid down on open installment loans is too low',
            'EX' => 'Amount paid down on open installment loans is too low'
        },
        '58' => new map<String, String>
        {
            'TU' => 'Proportion of balances to loan amounts on mortgage accounts is too high',
            'EX' => 'Proportion of balances to loan amounts on mortgage accounts is too high'
        },
        '59' => new map<String, String>
        {
            'TU' => 'Lack of recent revolving HELOC information',
            'EX' => 'Lack of recent revolving HELOC information'
        },
        '62' => new map<String, String>
        {
            'TU' => 'Proportion of balances to credit limits on revolving HELOC accounts is too high',
            'EX' => 'Ratio of balances to credit limits on revolving HELOC accts is too high'
        },
        '64' => new map<String, String>
        {
            'TU' => 'Proportion of revolving HELOC balances to total revolving balances is too high',
            'EX' => 'Ratio of revolving HELOC balances to total revolving balances is too high'
        },
        '65' => new map<String, String>
        {
            'TU' => 'Length of time bank/national revolving accounts have been established',
            'EX' => 'Length of time bank/national revolving accounts have been established'
        },
        '67' => new map<String, String>
        {
            'TU' => 'Length of time open mortgage loans have been established',
            'EX' => 'Length of time open mortgage loans have been established'
        },
        '70' => new map<String, String>
        {
            'TU' => 'Amount owed on mortgage loans is too high',
            'EX' => 'Amount owed on mortgage loans is too high'
        },
        '71' => new map<String, String>
        {
            'TU' => 'Too many recently opened installment accounts',
            'EX' => 'Too many recently opened installment accounts'
        },
        '77' => new map<String, String>
        {
            'TU' => 'Proportion of balances to loan amounts on auto accounts is too high',
            'EX' => 'Proportion of balances to loan amounts on auto accounts is too high'
        },
        '78' => new map<String, String>
        {
            'TU' => 'Length of time reported mortgage accounts have been established',
            'EX' => 'Length of time reported mortgage accounts have been established'
        },
        '79' => new map<String, String>
        {
            'TU' => 'Lack of recent reported mortgage loan information',
            'EX' => 'Lack of recent reported mortgage loan information'
        },
        '81' => new map<String, String>
        {
            'TU' => 'Frequency of delinquency',
            'EX' => 'Frequency of delinquency'
        },
        '85' => new map<String, String>
        {
            'TU' => 'Too few active accounts',
            'EX' => 'Too few active accounts'
        },
        '96' => new map<String, String>
        {
            'TU' => 'Too many mortgage loans with balances',
            'EX' => 'Too many mortgage loans with balances'
        },
        '97' => new map<String, String>
        {
            'TU' => 'Lack of recent auto loan information',
            'EX' => ''
        },
        '98' => new map<String, String>
        {
            'TU' => 'Length of time consumer finance company loans have been established',
            'EX' => 'Lack of recent auto loan information'
        },
        '99' => new map<String, String>
        {
            'TU' => 'Lack of recent consumer finance company account information',
            'EX' => 'Lack of recent consumer finance company account information'
        }
    };

    private static final String EX_SCORE_FACTOR_FIELD_NAME = 'Score Factor Codes';
    private static final String TU_DEROG_INQUE_FIELD_NAME = 'Score Derog and Inquiry Alert Flag';
    private static final String TU_SCORE_FIRST_FACTOR_FIELD_NAME = 'First Factor';
    private static final String TU_SCORE_SECOND_FACTOR_FIELD_NAME = 'Second Factor';
    private static final String TU_SCORE_THIRD_FACTOR_FIELD_NAME = 'Third Factor';
    private static final String TU_SCORE_FOURTH_FACTOR_FIELD_NAME = 'Fourth Factor';

    private static final String EX_MESSAGE_SEGMENT = '361';
    private static final String EX_MESSAGE_TEXT = 'Message Text';
    private static final String EX_INQUERY_PREFIX_TEXT = '0335'; 
    private static final String TOO_MANY_INQUIRIES = 'TOO MANY INQUIRIES';
    private static final String EX = 'EX';


    public static String getTooManyInquiries(List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {
           String result = '';
           if(crSegmentsList!=null){

            for (ProxyApiCreditReportSegmentEntity creditReportSegmentEntity : crSegmentsList) {
                 if (bureau.equalsIgnoreCase(EX) &&
                           creditReportSegmentEntity.prefix == EX_MESSAGE_SEGMENT &&
                           creditReportSegmentEntity.segmentTypeEntity.fieldName.equalsIgnoreCase(EX_MESSAGE_TEXT) &&
                           creditReportSegmentEntity.value.trim().StartsWith(EX_INQUERY_PREFIX_TEXT)) {
                    result = TOO_MANY_INQUIRIES;
                    break;
                }
            }
            }
        return result;
    }


    /**
     * Gets the score factor statements string
     * @param  crSegmentsList credit report segments
     * @param  bureau
     * @return Returns the string to insert in the email of the AAN with the score factor statements
     */
    public static String getScoreFactorStatements(List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {

        CreditReportSegmentsUtil.Segment riskSegment = CreditReportSegmentsUtil.extractScores(crSegmentsList, bureau);
        String codesString = '', tooManyInqui = bureau == 'EX' ? getTooManyInquiries(crSegmentsList, bureau) : extractTooManyInqueriesTU(riskSegment);

        if (bureau == 'TU') {
            codesString = extractScoreFactorCodesTU(riskSegment);
        } else {
            codesString = extractScoreFactorCodesEX(riskSegment);
        }

        return convertFactorCodesToStatements(codesString, bureau) + (tooManyInqui.length() > 0 ? tooManyInqui + '\n': '') ;
    }

    /**
     * Gets the score factors from the EX specific segment
     * @param  segment
     * @return Returns a string with the score factors (four score factor with 2 digits each). Example: "30150809"
     */
    private static String extractScoreFactorCodesEX(CreditReportSegmentsUtil.Segment segment) {

        String result = '';

        if (segment != null) {
            Integer length = segment.headersList.size();

            for (Integer i = 0; i < length; i++) {
                if (segment.headersList[i].equals(EX_SCORE_FACTOR_FIELD_NAME)) {
                    result = segment.contentList[0].valuesList[i];
                    break;
                }
            }
        } else {
            System.debug('The risk score segment does not exists.');
        }

        System.debug('EX Score Factors = ' + result);

        return result;
    }

        /**
     * Gets the score factors from the TU specific segment
     * @param  segment
     * @return Returns a string with the score factors (four score factor with 2 digits each). Example: "30150809"
     */
    private static String extractTooManyInqueriesTU(CreditReportSegmentsUtil.Segment segment) {

        String result = '';

        if (segment != null) {
            for (Integer i = 0; i < segment.headersList.size(); i++) {
                if ( segment.headersList[i].equals(TU_DEROG_INQUE_FIELD_NAME)  &&
                            segment.contentList[0].valuesList[i] != null &&
                            segment.contentList[0].valuesList[i].contains('I')
                      ) {
                    result = TOO_MANY_INQUIRIES  ;
                }
            }
        } else {
            System.debug('The risk score segment does not exists.');
        }

        System.debug('TU Too Many inquiries = ' + result);

        return result;
    }

    /**
     * Gets the score factors from the TU specific segment
     * @param  segment
     * @return Returns a string with the score factors (four score factor with 2 digits each). Example: "30150809"
     */
    private static String extractScoreFactorCodesTU(CreditReportSegmentsUtil.Segment segment) {

        String result = '';

        if (segment != null) {
            Integer length = segment.headersList.size();

            for (Integer i = 0; i < length; i++) {
                if (segment.headersList[i].equals(TU_SCORE_FIRST_FACTOR_FIELD_NAME) ||
                        segment.headersList[i].equals(TU_SCORE_SECOND_FACTOR_FIELD_NAME) ||
                        segment.headersList[i].equals(TU_SCORE_THIRD_FACTOR_FIELD_NAME) ||
                        segment.headersList[i].equals(TU_SCORE_FOURTH_FACTOR_FIELD_NAME) 
                        ) {
                    result +=  segment.contentList[0].valuesList[i].substring(1);
                }
            }
        } else {
            System.debug('The risk score segment does not exists.');
        }

        System.debug('TU Score Factors = ' + result);

        return result;
    }

    /**
     * Converts the string with the score factors to a string with the statements
     * @param  scoreFactorCodes
     * @param  bureau
     * @return
     */
    private static String convertFactorCodesToStatements(String scoreFactorCodes, String bureau) {

        String result = '';

        List<String> codeList = convertStringFactorCodesToList(scoreFactorCodes);

        Integer length = codeList.size();

        for (Integer i = 0; i < length; i++) {
            String code = codeList[i];

            if (scoreFactorReasonsMap.containsKey(code)) {
                result += scoreFactorReasonsMap.get(code).get(bureau) + '\n';
            }
        }

        return result;
    }

    /**
     * Converts the string with the score factors to a list
     * @param  scoreFactorCodes
     * @return
     */
    private static List<String> convertStringFactorCodesToList(String scoreFactorCodes) {

        List<String> codeList = new List<String>();
        Integer length = scoreFactorCodes.length();

        for (Integer i = 0; i < length; i += 2) {
            codeList.add(scoreFactorCodes.substring(i, i + 2));
        }

        return codeList;
    }
}