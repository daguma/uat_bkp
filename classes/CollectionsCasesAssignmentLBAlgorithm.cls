public class CollectionsCasesAssignmentLBAlgorithm {

    public static CollectionsCasesAssignmentLBEntity process(Integer totalInflow, Map<Id,Integer> currentIndividualsLoadMap){

        if(totalInflow == null || currentIndividualsLoadMap == null) return null;

        if(totalInflow <= 0 || currentIndividualsLoadMap.isEmpty()) return new CollectionsCasesAssignmentLBEntity(new Map<Id,Long>(), null);

        Integer maxCurrentIndividualLoad = 0;
        Decimal maxCurrentIndividualLoadModified = 0.00, individualLoadByMaxModifiedAbsSum = 0.00;
        Map<Id, Decimal> individualLoadByMaxModifiedMap, individualLoadByMaxModifiedAbsMap;
        Map<Id, Long> individualFutureLoadMap;
        Id individualWithLowestFutureLoad, individualWithHighestFutureLoad;

        //Get the maximum amount of cases that are already assigned to a user in the group
        Integer individualLoad = 0;
        for ( Id individualId : currentIndividualsLoadMap.keySet() ){
            individualLoad = currentIndividualsLoadMap.get(individualId) != null ? currentIndividualsLoadMap.get(individualId) : 0;
            if(maxCurrentIndividualLoad < individualLoad){
                maxCurrentIndividualLoad = individualLoad;
            }
        }
        
        //Modify that maximum by a factor of 105%
        maxCurrentIndividualLoadModified = Double.valueOf(maxCurrentIndividualLoad) * 105 / 100;
        
        //Get the quotient(1), where the dividend is the current load and the divisor is the modified max
        //Then get the inverse of that quotient(1) and add up all this numbers as a "group total"
        individualLoadByMaxModifiedMap = new Map<Id, Decimal>();
        individualLoadByMaxModifiedAbsMap = new Map<Id, Decimal>();
        Decimal individualLoanByMaxModified;
        for(Id individualId : currentIndividualsLoadMap.keySet() ){
            individualLoanByMaxModified = currentIndividualsLoadMap.get(individualId) / maxCurrentIndividualLoadModified;
            individualLoadByMaxModifiedMap.put(individualId,individualLoanByMaxModified);

            individualLoadByMaxModifiedAbsMap.put(individualId, math.abs(1-individualLoanByMaxModified) );

            individualLoadByMaxModifiedAbsSum += math.abs(1-individualLoanByMaxModified);
        }
        
        //Calculate the quotient(2) where the dividend is the "inverse" and the divisor is the "group total"
        //The future load is calculated by: Original Load * quotient(2)
        individualFutureLoadMap = new Map<Id, Long>();
        Long lowestFutureLoad, highestFutureLoad; Boolean firstIteration = true;
        Long totalAssignedLoad = 0;
        for(Id individualId : currentIndividualsLoadMap.keySet() ){
            
            Decimal temp = individualLoadByMaxModifiedAbsMap.get(individualId) / individualLoadByMaxModifiedAbsSum;
            temp = temp * totalInflow;
            individualFutureLoadMap.put(individualId,temp.round(System.RoundingMode.HALF_UP));
            totalAssignedLoad = totalAssignedLoad + temp.round(System.RoundingMode.HALF_UP);
            if(firstIteration){
                firstIteration = false;
                lowestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                highestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                individualWithLowestFutureLoad = individualId;
                individualWithHighestFutureLoad = individualId;
            }else{
                if(temp.round(System.RoundingMode.HALF_UP) < lowestFutureLoad){
                    lowestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                    individualWithLowestFutureLoad = individualId;
                }
                if(temp.round(System.RoundingMode.HALF_UP) > highestFutureLoad){
                    highestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                    individualWithHighestFutureLoad = individualId;
                }
            }
        }

        //If the algorithm assigned more or less items than the inflow, then fix it here
        Long loadDifference = (long)totalInflow - totalAssignedLoad;
        if(loadDifference < 0){
            //This means that the algorithm assigned more load than the totalInflow
            Long assignedLoad = individualFutureLoadMap.get(individualWithHighestFutureLoad);
            individualFutureLoadMap.remove(individualWithHighestFutureLoad);
            individualFutureLoadMap.put(individualWithHighestFutureLoad, assignedLoad + loadDifference);
        }else if(loadDifference > 0) {
            //This means that the algorithm assigned less load than the totalInflow
            Long assignedLoad = individualFutureLoadMap.get(individualWithLowestFutureLoad);
            individualFutureLoadMap.remove(individualWithLowestFutureLoad);
            individualFutureLoadMap.put(individualWithLowestFutureLoad, assignedLoad + loadDifference);
        }

        return new CollectionsCasesAssignmentLBEntity(individualFutureLoadMap, individualWithLowestFutureLoad);
    }
}