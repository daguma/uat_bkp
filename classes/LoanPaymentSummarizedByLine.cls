global class LoanPaymentSummarizedByLine implements Schedulable {
    public DateTime targetDate = Date.today();

    global void execute (SchedulableContext sc) {
        try {
            targetDate = Date.today();
            System.debug('Transaction Date = ' + targetDate);
            this.generateUnsold();
            this.generateAres();
            this.generateSPE2();
            this.generateGugg();
            this.generateGugg2();
            this.generateFundingTrust();
        } catch (Exception e) {
            WebToSFDC.notifyDev('LoanPaymentSummarizedByLine.execute Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() ) ;
        }
    }
    
    public boolean generateUnsold(){
        List<loan__Loan_Payment_Transaction__c> lt = [select Id, loan__Loan_Account__c from loan__Loan_Payment_Transaction__c 
                                                  where loan__ACH_Filename__c <> null and CreatedDate >: targetDate and CreatedDate <: targetDate.addDays(1)   and
                                                      loan__Loan_Account__c in (Select Id
                                                                                 from loan__Loan_Account__c
                                                                                 where  Asset_Sale_Line__c NOT IN ('ARES', '2018 Funding Trust' , 'SPE2',  'GUGG', 'GUGG 2018-1') 
                                                   )];    
        generateFile(lt, 'LP');
        
        return true;
    }  

    public boolean generateAres(){
        generateFile(getPayments('ARES'), 'ARES'); 
        return true;
    }  
    

    public boolean generateSPE2(){
        generateFile(getPayments('SPE2'), 'SPE2'); 
        return true;
    }  


    public boolean generateFundingTrust(){
        generateFile(getPayments('2018 Funding Trust'), 'FND18'); 
        return true;
    }  


    public boolean generateGugg(){
  
        generateFile(getPayments('GUGG'), 'GUGG');        
        return true;
    }  
    
    
    public boolean generateGugg2(){
        generateFile(getPayments('GUGG 2018-1'), 'GUGG2-1');
        return true;
    }  
    
    public List<loan__Loan_Payment_Transaction__c> getPayments(String line) {
        return [select Id, loan__Loan_Account__c from loan__Loan_Payment_Transaction__c 
                                                  where loan__ACH_Filename__c <> null and CreatedDate >: targetDate and CreatedDate <: targetDate.addDays(1)   and
                                                        loan__Loan_Account__c in (Select Id
                                                                                 from loan__Loan_Account__c
                                                                                 where Asset_Sale_Line__c = : line )
                                                  ]; 
    }

    public void generateFile( List<loan__Loan_Payment_Transaction__c> lPayments, String fileName) {
        
        System.debug(fileName + ' File Payments = ' + lPayments.size());
        
        try {
            String fileContent = '';
            loan.TransactionSweepToACHState st = new loan.TransactionSweepToACHState();
            st.counter = 1;
            LoanPaymentTxnPayPointGen pg = new LoanPaymentTxnPayPointGen();
            pg.currentLine = fileName;
            pg.bIncludeFees = false;
            for (String i : pg.getEntries(st, lPayments)) {
                fileContent += i + '\r\n';
            }

            fileContent = pg.getHeader(st, lPayments).replace(DateTime.now().addDays(-1).format('YYMMdd')  + '0190', DateTime.now().format('YYMMddhhmm') ) + fileContent + pg.getTrailer(st, lPayments);
            System.debug(fileContent);
            
            
            Folder folder = [select id from Folder where name='Shared Folder for ACH' LIMIT 1];
            Document d = new Document(ContentType = 'text/plain', FolderId = folder.Id, Type = 'txt' ); 
            d.Name = fileName + '_LoanPayments_' + DateTime.now(); 
            d.Body = Blob.valueOf(fileContent); 
            
            insert d;
        } catch (Exception e ) {
            System.debug(e.getMessage() + '-' + e.getStacktraceString());
            WebToSFDC.notifyDev('LoanPaymentSummarizedByLine.generateFile Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() ) ;
        }


        
    }

    Webservice static String executePaymentSweep() {
        List<AsyncApexJob> jbs = [SELECT Id, CreatedDate , Status, ApexClassID,  JobItemsProcessed , TotalJobItems FROM AsyncApexJob WHERE ApexClassID = '01pU0000001Byge' and Status = 'Processing' and CreatedDate >= YESTERDAY];
        // has the AUTO process run today? If not, DON"t allow execution
        List<AsyncApexJob> executedjbs = [SELECT Id, CreatedDate , Status, ApexClassID,  JobItemsProcessed , TotalJobItems FROM AsyncApexJob WHERE ApexClassID = '01pU0000001Byge' and CreatedDate = TODAY];

        if (jbs.size() == 0 && (executedjbs.size() > 0 || Test.isRunningTest() )) {
            loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();
            loan.LoanPaymentTxnSweepToACHJob j = new loan.LoanPaymentTxnSweepToACHJob(false);
            Database.executeBatch(j, acha.loan__Loan_Payment_Creation_Job_Batch_Size__c == null ? 100 :acha.loan__Loan_Payment_Creation_Job_Batch_Size__c.intValue());
            return 'The process has been started. You should receive an email soon!';
        }

        if (executedjbs.size() == 0)
            return 'The AUTO daily process has not initiated yet. You must wait until it starts and finishes to use this button.';
        else
            return 'The process is already running. ' + jbs[0].JobItemsProcessed + ' out of ' + jbs[0].TotalJobItems + ' batches run.';
    }

    Webservice static String executeSummarizedLineFile(){

        LoanPaymentSummarizedByLine sl = new LoanPaymentSummarizedByLine();
        sl.execute(null);
        return 'Process ended! Please look for the files.';
    }

    Webservice static String deleteTodaysACHFiles(){
       List<Document> doc = [select Id, Name from Document where Name like '%- Loan_Payments%' and CreatedDate = TODAY];
       integer docs = doc.size();
       if (docs > 0) delete doc; 
       return docs + ' deleted!';
    }
    
}