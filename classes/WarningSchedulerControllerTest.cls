@isTest
public class WarningSchedulerControllerTest {
    public static string cron_exp = '0 '+system.now().minute()+' * * * ?';
    public static testMethod void testPayfoneNotResponding() {
        
        API_List_For_Alerts__c api = new API_List_For_Alerts__c();
        api.Name = 'Payfone';
        api.Is_Active__c = true;
        insert api;
        
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        
        Idology_Request__c ido = new Idology_Request__c ();
        ido.Contact__c = cntct.id;
        insert ido;
        
        Test.StartTest();
        String jobId = System.schedule('Payfone Warning System Test', cron_exp, new WarningSchedulerController());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(cron_exp, ct.CronExpression);
        
        Test.StopTest();
        
        List<CronJobDetail> cronjobs = [select Name from CronJobDetail where Name = 'Payfone Warning System Test'];
        System.assertEquals(cronjobs .size(),1);
    }
    
    public static testMethod void testPayfoneRespondingNull() {
        
        API_List_For_Alerts__c api = new API_List_For_Alerts__c();
        api.Name = 'Payfone';
        api.Is_Active__c = true;
        insert api;
        
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        
        Idology_Request__c ido = new Idology_Request__c ();
        ido.Contact__c = cntct.id;
        insert ido;
        
        Payfone_Run_History__c pyfn = new Payfone_Run_History__c();
        pyfn.Contact__c = cntct.id;
        insert pyfn;
        
        Test.StartTest();
        String jobId = System.schedule('Payfone Warning System Test', cron_exp, new WarningSchedulerController());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(cron_exp, ct.CronExpression);
        Test.StopTest();
        
        List<CronJobDetail> cronjobs = [select Name from CronJobDetail where Name = 'Payfone Warning System Test'];
        System.assertEquals(cronjobs .size(),1);
    }
    
    public static testMethod void testPayfoneResponding() {
        
        API_List_For_Alerts__c api = new API_List_For_Alerts__c();
        api.Name = 'Payfone';
        api.Is_Active__c = true;
        insert api;
        
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        
        Idology_Request__c ido = new Idology_Request__c ();
        ido.Contact__c = cntct.id;
        insert ido;
        
        Payfone_Run_History__c pyfn = new Payfone_Run_History__c();
        pyfn.Contact__c = cntct.id;
        pyfn.Verify_API_Response__c = 'test';
        insert pyfn;
        
        Test.StartTest();
        String jobId = System.schedule('Payfone Warning System Test', cron_exp, new WarningSchedulerController());
        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(cron_exp, ct.CronExpression);    
        Test.StopTest();
        
        List<CronJobDetail> cronjobs = [select Name from CronJobDetail where Name = 'Payfone Warning System Test'];
        System.assertEquals(cronjobs .size(),1);
    }
}