public with sharing class DecisionLogicGDSResponse {
    
    @AuraEnabled public String sessionId {get;set;}
    
    @AuraEnabled public String sequenceId {get;set;}
    
    @AuraEnabled public String productId {get;set;}
    
    @AuraEnabled public String channelId {get;set;}
    
    @AuraEnabled public String executionType {get;set;}
    
    @AuraEnabled public String ruleVersion {get;set;}
    
    @AuraEnabled public String deApplicationRisk {get;set;}
    
    @AuraEnabled public String deRuleViolated {get;set;}
    
    @AuraEnabled public String deStrategy_Type {get;set;}
    
    @AuraEnabled public List<DecisionLogicRule> rulesList {get;set;}
    
    @AuraEnabled public String scorecardAcceptance {get;set;}
    
    @AuraEnabled public List<GDSGenericObjectAttributes.Error> errors {get;set;}
    
    public DecisionLogicGDSResponse() {}
    
    public virtual class DecisionLogicRule{
        
        @AuraEnabled public String deRuleNumber {get;set;}
        
        @AuraEnabled public String deRuleName {
            get{
                return this.deRuleName != null ? this.deRuleName.replace('_',' ') : '';
            }
            set;
        }
        
        @AuraEnabled public String rulePriority {get;set;}
        
        @AuraEnabled public String deRuleValue {get;set;}
        
        @AuraEnabled public String deQueueAssignment {get;set;}
        
        @AuraEnabled public String deRuleStatus {get;set;}
        
        @AuraEnabled public String deBRMS_ReasonCode {get;set;}
        
        @AuraEnabled public String deIsDisplayed {get;set;}
        
        @AuraEnabled public String deSequenceCategory {get;set;}
        
        @AuraEnabled public String deRuleCategory {get;set;}
        
        @AuraEnabled public String deActionInstructions {get;set;}
        
        @AuraEnabled public String deHumanReadableDescription {get;set;}
        
        @AuraEnabled public Boolean override_required {
            get{
                
                return (this.deRuleStatus != null && this.deRuleStatus == 'Fail');
                
            }set;
        }
        
        @AuraEnabled public Boolean overridden {
            get{ 
                return this.overridden != null ? this.overridden : false;
            } 
            set;
        }
        
        @AuraEnabled public String overridden_by {get;set;} 
        
        public DecisionLogicRule(){}
        
    }
    
    public  DecisionLogicGDSResponse(String decisionLogicGDSResponse){
        
        Object tempObject =  System.JSON.deserialize(decisionLogicGDSResponse, DecisionLogicGDSResponse.class);
        
        this.sessionId = ((DecisionLogicGDSResponse) tempObject).sessionId;
        
        this.sequenceId = ((DecisionLogicGDSResponse) tempObject).sequenceId;
        
        this.productId = ((DecisionLogicGDSResponse) tempObject).productId;
        
        this.channelId = ((DecisionLogicGDSResponse) tempObject).channelId;
        
        this.executionType = ((DecisionLogicGDSResponse) tempObject).executionType;
        
        this.ruleVersion = ((DecisionLogicGDSResponse) tempObject).ruleVersion;
        
        this.deApplicationRisk = ((DecisionLogicGDSResponse) tempObject).deApplicationRisk;
        
        this.deRuleViolated = ((DecisionLogicGDSResponse) tempObject).deRuleViolated;
        
        this.deStrategy_Type = ((DecisionLogicGDSResponse) tempObject).deStrategy_Type;
        
        this.rulesList = ((DecisionLogicGDSResponse) tempObject).rulesList;
        
        this.scorecardAcceptance = ((DecisionLogicGDSResponse) tempObject).scorecardAcceptance;
        
        this.errors = ((DecisionLogicGDSResponse) tempObject).errors;
        
        tempObject = null;
    }
}