@isTest
public class CustomerPortalPaymentInfoWSDL_Test {
	static testmethod void test1() {
		loan__Loan_Account__c clContr = TestHelper.createContract();
		clContr.loan__ACH_Next_Debit_Date__c =  system.today() + 5;  
		update clContr;
		
		String req = '{'+
			'"contractId":"'+clContr.Id+'"'+
			'"paymentDate":"'+(system.today() + 10)+'"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getREStatusOrUpdateDueDate(req);
		CustomerPortalPaymentInfoWSDL.generateResponse(clContr);
		CustomerPortalPaymentInfoWSDL.getAmortizationFromContract(req);
		CustomerPortalPaymentInfoWSDL.getLoanPaymentTransactionsFromContract(req);
		//CustomerPortalPaymentInfoWSDL.getBankAccountInfo(req);
		//CustomerPortalPaymentInfoWSDL.saveBankDetails(req);
		CustomerPortalPaymentInfoWSDL.getAttachementsForContract(req);
		//CustomerPortalPaymentInfoWSDL.sendEmail(req);
		
		/* 
		EmailTemplate e = new EmailTemplate (developerName = 'Payment_Email', TemplateType= 'Text', Name = 'Payment_Email'); // plus any other fields that you want to set

		insert e;
		
		
		req = '{'+
        '"contactId":"876543",'+
        '"email":"test",'+
        '"transactionID":"76543",'+
        '"paymentAmount":"76543",'+
        '"cardNumber":"76543",'+
        '"paymentType":"76543",'+
        '"paymentDate":"76543"'+
        '}'; 
		CustomerPortalPaymentInfoWSDL.sendEmail(req);
		 */
		
		
        
	}
	static testmethod void test2() {
		loan__Loan_Account__c clContr = TestHelper.createContract();
		clContr.loan__ACH_Next_Debit_Date__c =  system.today() + 5;  
		clContr.loan__ACH_Routing_Number__c =  '7415241';  
		clContr.loan__ACH_Account_Number__c =  '7415241';  
		clContr.loan__ACH_Debit_Amount__c =  7415241;  
		clContr.loan__ACH_On__c = true;
		update clContr;
		
		Datetime yourDate = Datetime.now();
		String dateOutput = yourDate.format('MM/dd/yyyy');
		String request = '{'+
        '"contractId":"'+clContr.Id+'",'+
        '"paymentDate":"'+dateOutput+'"'+
        '}';
		String ss = CustomerPortalPaymentInfoWSDL.getREStatusOrUpdateDueDate(request);
        
		loan__Repayment_Schedule__c repaymentSchedule = new loan__Repayment_Schedule__c();
		repaymentSchedule.loan__Loan_Account__c = clContr.Id;
		repaymentSchedule.loan__Due_Date__c = Date.today();
		insert repaymentSchedule;
        request = '{'+
        '"contractId":"'+clContr.Id+'"'+
        '}';
		CustomerPortalPaymentInfoWSDL.getAmortizationFromContract(request);
		CustomerPortalPaymentInfoWSDL.getAmortizationFromContract('{"contactId":null}');
        CustomerPortalPaymentInfoWSDL.getAmortizationFromContract('{"contactId":"54545445"}');
		 Integer notesBefore = [SELECT Count() FROM Note];
        Contact con = new Contact(lastName = 'test');
		insert con;
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Disbursal_Date__c = Date.today().addDays(-2);
        contract.loan__Disbursal_Status__c = 'Completed';
        contract.loan__Disbursal_Amount__c = 5000;
        contract.loan__loan_status__c = 'Active - Good Standing';
       
        contract.loan__ACH_Debit_Day__c = 1;
        //contract.loan__Contact__c = con.id;
        contract.loan__Next_Installment_Date__c = Date.today().addMonths(1);
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__First_Installment_Date__c = Date.today().addMonths(-2);                            
        contract.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addMonths(1);
        contract.loan__ACH_On__c = true;
        contract.loan__ACH_Debit_Amount__c = 100;
        contract.loan__ACH_Start_Date__c = Date.today().addMonths(-2);
        contract.loan__ACH_End_Date__c = Date.today().addMonths(6);
        contract.loan__ACH_Bank_Name__c = 'Pacific Western Bank';
        contract.loan__ACH_Account_Type__c = 'Checking';
        contract.loan__ACH_Account_Number__c = '123456';
        contract.loan__ACH_Routing_Number__c = '123456123';
        contract.loan__ACH_Relationship_Type__c = 'PRIMARY';
        contract.Reporting_Fee__c = 300;
        contract.Reporting_Fee_Balance__c = 300;
        contract.Reporting_Interest__c = 1300;
        contract.PaymentDue7DayReminderDate__c = Date.today().addMonths(-1);
		
        update contract;
        
        Contact c = [SELECT Id, AccountId FROM Contact WHERE Id =: contract.loan__Contact__c];

        loan__Bank_Account__c bank = new loan__Bank_Account__c();
        bank.loan__Account__c = c.AccountId;
        bank.loan__Account_Type__c = 'Checking';
        bank.loan__Account_Usage__c = 'Test Dummy Account';
        bank.loan__Active__c = true;
        bank.loan__Bank_Account_Number__c = '098765433';
        bank.loan__Bank_Name__c = '99';
        bank.Debit_Card_Expiration_Date__c = Date.today();
        bank.Name_on_the_Card__c = '99 dfdfd';
        bank.peer__Branch_Code__c = '998';
        bank.loan__Routing_Number__c = '98776631';
        bank.loan__Contact__c = con.id;
        insert bank;

        contract.loan__Borrower_ACH__c = bank.Id;
        update contract;
        
        Test.startTest();
        
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name = 'ACH'];
        
        loan__Loan_Payment_Transaction__c payment = new loan__Loan_Payment_Transaction__c();
        payment.loan__balance__c = 100;
        payment.loan__Loan_Account__c = contract.id;
        payment.loan__Transaction_Amount__c = 100;
        payment.loan__Principal__c = 100;
        payment.loan__Interest__c = 10;
        payment.loan__Reversed__c = false;
        payment.loan__Cleared__c = true;
        payment.loan__Payment_Mode__c = pm.Id;
        insert payment;
		request = '{'+
        '"contractId":"'+contract.Id+'"'+
        '}';
        CustomerPortalPaymentInfoWSDL.getLoanPaymentTransactionsFromContract(request);
        CustomerPortalPaymentInfoWSDL.getLoanPaymentTransactionsFromContract('{"contactId":null}');
        CustomerPortalPaymentInfoWSDL.getLoanPaymentTransactionsFromContract('{"contactId":"54545445"}');
		request = '{'+
        '"contactId":"'+bank.loan__Contact__c+'"'+
        '}';
        //CustomerPortalPaymentInfoWSDL.getBankAccountInfo(request);
        //CustomerPortalPaymentInfoWSDL.getBankAccountInfo('{"contactId":null}');
        //CustomerPortalPaymentInfoWSDL.getBankAccountInfo('{"contactId":"54545445"}');
		request = '{'+
        '"contractId":"'+contract.Id+'"'+
        '}';
		CustomerPortalPaymentInfoWSDL.getAttachementsForContract(request);
		String bodyStr ='\n\n\n' + System.now();
   
		 Attachment att=new Attachment();
		 att.Body=Blob.valueOf(bodyStr);
		 att.Name='Note_' + System.now().format('yyyy_MM_dd_hh_mm_ss') + '.txt';
		 att.parentId=contract.Id;
		 insert att;
		 
		CustomerPortalPaymentInfoWSDL.getAttachementsForContract(request);
		CustomerPortalPaymentInfoWSDL.getAttachementsForContract('{"contactId":null}');
		request = '{'+
        '"contactId":"876543",'+
        '"email":"test@test.com",'+
        '"transactionID":"76543",'+
        '"paymentAmount":"76543",'+
        '"cardNumber":"76543",'+
        '"paymentType":"76543",'+
        '"paymentDate":"12/27/2015"'+
        '}'; 
		CustomerPortalPaymentInfoWSDL.sendEmail(request);
		
		
		
		
		Date da1 = date.newInstance(2017, 1, 31);
        loan__Repayment_Transaction_Adjustment__c reversal = new loan__Repayment_Transaction_Adjustment__c();
        reversal.loan__Reason_Code__c = 'Other';
        reversal.loan__Loan_Payment_Transaction__c = payment.id;
        reversal.loan__Adjustment_Txn_Date__c = da1;
        reversal.loan__Cleared__c = TRUE;
        insert reversal;
		
		request = '{'+
		'"transactionId":"'+payment.id+'"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getLoanRepaymentTransactionAdjustment(request); 
		request = '{'+
		'"transactionId":"'+payment.id+'",'+
		'}';
		CustomerPortalPaymentInfoWSDL.getLoanRepaymentTransactionAdjustment(request); 
		
		request = '{'+
		'"transactionId":""'+
		'}';
		CustomerPortalPaymentInfoWSDL.getLoanRepaymentTransactionAdjustment(request); 
		request = '{'+
		'"transactionId":"23232323"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getLoanRepaymentTransactionAdjustment(request); 
		
		
        loan__Fee__c fee = new loan__Fee__c();
        fee.loan__Amount__c = 25;
        fee.loan__Default_fees__c = true;
        fee.loan__Fee_Calculation_Method__c = 'Fixed';
        fee.loan__Fee_Category__c = 'Loan';
        //fee.Revenue_GLA__c = gla.Id;
        insert fee;
		
		loan__Charge__c loanCharge = new loan__Charge__c();
        loanCharge.loan__Loan_Account__c = contract.id;
        loanCharge.loan__Charge_Type__c = 'Other';
        loanCharge.loan__Date__c = Date.today().addDays(-15);
        loanCharge.loan__Fee__c = fee.Id;
        loanCharge.loan__Original_Amount__c = 25;
        loanCharge.loan__Paid_Amount__c = 0;
        loanCharge.loan__Paid__c = false;
        loanCharge.loan__Principal_Due__c = 25;
        loanCharge.loan__Balance__c = 5000;
        loanCharge.loan__Payoff_Balance__c = 8500;
        insert loanCharge;

		
		
		
		request = '{'+
		'"contractId":"'+contract.id+'"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getloanCharges(request); 
		
		request = '{'+
		'"contractId":"'+contract.id+'",'+
		'}';
		CustomerPortalPaymentInfoWSDL.getloanCharges(request); 
		
		request = '{'+
		'"contractId":""'+
		'}';
		CustomerPortalPaymentInfoWSDL.getloanCharges(request); 
		
		request = '{'+
		'"contractId":"112121212"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getloanCharges(request); 
		
		request = '{'+
		'"contractId":"112121212"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getOnlinePaymentDetails(request); 
		CustomerPortalPaymentInfoWSDL.getOnlinePaymentDetailsCPPI(request); 
		CustomerPortalPaymentInfoWSDL.checkrunningEndOfDayProcess(); 
		
		request = '{'+
		'"contractId":"'+contract.id+'"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getLoanAccounts(request); 
		
		contract.loan__Contact__c = con.id;
		update contract;
		request = '{'+
		'"contractId":"'+contract.id+'",'+
		'"contactId":"'+con.id+'"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getLoanAccounts(request); 
		request = '{'+
		'"contractId":"'+contract.id+'"'+
		'"contactId":"'+con.id+'"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getLoanAccounts(request); 
		
		request = '{'+
		'"contractId":"123445",'+
		'"contactId":"'+con.id+'"'+
		'}';
		CustomerPortalPaymentInfoWSDL.getLoanAccounts(request); 
		
		
		
		request = '{'+
        '"contactId":"876543",'+
        '"email":"test@test.com",'+
        '"transactionID":"76543",'+
        '"paymentAmount":"76543",'+
        '"cardNumber":"76543",'+
        '"paymentType":"76543",'+
        '"paymentDate":"12/27/2015"'+
        '}'; 
		CustomerPortalPaymentInfoWSDL.sendEmail(request);
		
		/* 
		loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
        paymentMode.Name = 'ACH';
        insert paymentMode;

        loan__Loan_Payment_Transaction__c pmt = new loan__Loan_Payment_Transaction__c();
        pmt.loan__Loan_Account__c = contract.Id;
        pmt.Payment_Processing_Complete__c = false;
        pmt.loan__Transaction_Amount__c = 100.00;
        pmt.loan__Principal__c = 100.00;
        pmt.loan__Interest__c = 100.00;
        pmt.loan__Fees__c = 100.00;
        pmt.loan__excess__c = 100.00;
        pmt.loan__Transaction_Date__c = Date.today();
        pmt.loan__Skip_Validation__c = true;
        pmt.loan__Payment_Mode__c = paymentMode.Id;
        pmt.loan__Cleared__c = true;
        pmt.loan__Balance__c = 100;
        insert pmt;
		 */
		request = '{'+
        '"contractId":"'+contract.Id+'",'+
        '"days":"300"'+
        '}';
        CustomerPortalPaymentInfoWSDL.getLoanPaymentTransactionsFromContract(request);
		
        Test.stopTest();
		
	}
}