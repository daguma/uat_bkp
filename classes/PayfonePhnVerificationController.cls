public class PayfonePhnVerificationController {
    public Contact con {get;set;}
    public PayfonePhnVerificationController(ApexPages.StandardController stdController) {
        con = (Contact)stdController.getRecord();
        
    }
    //method to call th payfone APIs to get the verification response and save it to DB
    public void doPhoneVerification() {
       con = VerifyPhoneNumberHelper.allOperations(con.Id);
    }
}