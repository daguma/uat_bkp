global with sharing class LeadContInterface {
    

    Webservice  static string InterfacetoLdCnt(string fnlid) {
        return InterfacetoLdCnt(fnLid, null);
    }

     //********************************************//
    //**Method to get object type from record Id**//
    //**And Call service acc to obj type**********//
    //********************************************//
    public static string InterfacetoLdCnt(string fnlid, Account pAcc){
        Map<string,string> JsonRes= new Map<string,string>(),  pInfoMap;
        string  email,ssn,phone, applicationId, partnerId = '', contactId,fromWeb, leadId = 'null', status = '200', subSrc,res, objType, product = 'Lending Point' ,relationship,coordinatorName,coordinatorEmail;
        Decimal annualIncome,requestedPaymentAmount; //SM-942
        Date procedureDate ; //API-193
        Boolean CreditAuth, authToPullCredit,inNetworkOutOfNetwork, isEzVerify = false,patientApplied = false, bApplicationExists;
        try{
            pInfoMap = (Map<String, String>) JSON.deserialize(fnlid, Map<String, String>.class);
            System.debug('============pInfoMap =============================' + pInfoMap );
            if (pInfoMap.ContainsKey('annualIncome')) {annualIncome = !string.isEmpty(pInfoMap.get('annualIncome')) ? decimal.ValueOf(pInfoMap.get('annualIncome')) : null;}
            if (pInfoMap.ContainsKey('ssn')) {ssn = !string.isEmpty(pInfoMap.get('ssn')) ? pInfoMap.get('ssn') : '';}
            if (pInfoMap.ContainsKey('email')) {email = !string.isEmpty(pInfoMap.get('email'))?pInfoMap.get('email').trim(): '';}
            if (pInfoMap.ContainsKey('phone')) {phone = !string.isEmpty(pInfoMap.get('phone')) ? pInfoMap.get('phone') : '';}
            if (pInfoMap.ContainsKey('authToPullCredit')) {authToPullCredit = !string.isEmpty(pInfoMap.get('authToPullCredit'))? Boolean.ValueOf(pInfoMap.get('authToPullCredit')): False;}            
            if (pInfoMap.ContainsKey('id')) {fnlid= String.isNotEmpty(pInfoMap.get('id'))?pInfoMap.get('id').trim(): '';}
            if (pInfoMap.ContainsKey('relationship')) {relationship = String.isNotEmpty(pInfoMap.get('relationship'))?pInfoMap.get('relationship').trim(): '';} // EZ-20
            if (pInfoMap.ContainsKey('procedureDate')) {procedureDate = String.isNotEmpty(pInfoMap.get('procedureDate'))? Date.Parse(pInfoMap.get('procedureDate')): null;} // API-193
            if (pInfoMap.ContainsKey('patientApplied')) {patientApplied = !string.isEmpty(pInfoMap.get('patientApplied'))? Boolean.ValueOf(pInfoMap.get('patientApplied')): False;}    
        }
        catch(exception ex){
            if(pInfoMap==null)  fnlid=fnlid;
        }
        system.debug('==pInfoMap==' +  pInfoMap); system.debug('==fnlid==' +  fnlid);
        //variables Declaration
        ezVerify_Settings__c EZVerify = CustomSettings.getEZVerifyCustom();
        try{
            if(String.isNotEmpty(fnlid)){
                id ConLdId = fnlid;
                leadId = ConLdId; //FIX for showOffers not being called
                objType = ConLdId.getSObjectType().getDescribe().getName(); //get object name from record id
                system.debug('===Object type==='+objType);
                if(String.isNotEmpty(objType)){
                    if(objType == 'Contact'){
                        contact con = [select id,Patient_Applied__c ,Authorization_to_pull_credit__c, BirthDate, Lead_Sub_Source__c,LeadSource,ints__SSN__c,phone,Email,Annual_Income__c,ints__Social_Security_Number__c,product__c,In_Network_Out_of_Network__c,Relationship__c,CoordinatorEmail__c,CoordinatorName__c,Requested_Payment_Amount__c  from Contact where id=:fnlid]; //SM-942
                        subSrc = con.Lead_Sub_Source__c;
                        CreditAuth = con.Authorization_to_pull_credit__c || (authToPullCredit <> null && authToPullCredit);
                        if (String.isNotEmpty(con.product__c)) product = con.product__c;
                        inNetworkOutOfNetwork = con.In_Network_Out_of_Network__c; 
                        coordinatorName = con.CoordinatorName__c ;
                        coordinatorEmail= con.CoordinatorEmail__c ;
                        requestedPaymentAmount = con.Requested_Payment_Amount__c; //SM-942
                        string response = updateJsonParamsinContact(objType,con,authToPullCredit,annualIncome,ssn,phone,email,leadId,EZVerify,relationship,patientApplied );
                        system.debug('==response==' + response);
                        if(!string.IsEmpty(response)) return response;                        
                        /*------------------------------------------------------------------------
                        Need to check this to restrict creation of duplicate Application
                        ------------------------------------------------------------------------*/
                            bApplicationExists = !SubmitApplicationService.checkExistanceByContact(ConLdId, product, pAcc);
                            
                            system.debug('=====bApplicationExists ========='+bApplicationExists );
                            
                            contactId = ConLdId;
                            //EZ-65 
                            if(string.isnotEmpty(con.LeadSource)) isEzVerify = generalPurposeWebServices.isEzVerify(con.LeadSource);
                            DOB_Validation__c Dob = DOB_Validation__c.getInstance('Allow18');
                            //EZ-65
                            if(!bApplicationExists && CreditAuth){
                                if(isEzVerify && !Dob.AllowBelow18__c  && con.BirthDate <> null && (System.TODAY() < con.BirthDate.addYears(Integer.valueof(label.minimum_birth_age)))){    //EZ-65
                                    status = 'ERR';
                                    res = 'Offers are declined due to age under 18 years';
                                }//EZ-65
                                else {
                                    res = ContactToApplicationConverter.ContactToApp(ConLdId, false, pAcc);
                                    system.debug('==res==' + res);
                                    if(String.isnotempty(res) && res.contains('006')) applicationID = res;
                                    if(String.isnotempty(res) && res.contains('006')) res = 'Application Created'; 
                                } 
                            }else{
                                res= (bApplicationExists)?'Application Already Exists or reached the limit of funded applications.':'Offers cannot be generated as authorization to pull credit is false';
                            }                       
                        system.debug('===contactToapplicationconverter==='+res);
                    }else if(objType == 'Lead'){
                            Lead theL;
                            leadId =  fnlid;
                            List<Lead> lLeads = [select Authorization_to_pull_credit__c, IsConverted, ConvertedContactID,Lead_Sub_Source__c,LeadSource,isResolve360Pass__c, Request_Source__c, DuplicateLeadToDelete__c, CreatedDate, Status, Point_Code__c,
                                             OwnerId, Automatically_Assigned__c,Use_of_Funds__c, Referred_By__c, Campaign_Id__c, Tracking_Number__c, Lead_Provider_Source_Code__c, City, State, Street, Email, SSN__c, Loan_Amount__c, Annual_Income__c, IP__C, Unit__c, Loan_Purpose__c, Employer_Name__c, Employer_Street__c,
                                             Employer_City__c, Employer_State__c, Date_of_Birth__c, Employment_Start_Date__c, Time_at_Current_Address__c, ApiHitDate__c,
                                             Employment_Type__c, Previous_Employment_End_Date__c, Previous_Employment_Start_Date__c, Employment_Duration__c, Company, PostalCode, Country, Phone, Residence_Type__c, Monthly_Rent_Mortgage__c,  FirstName, LastName,
                                             Contact__c  from Lead where id=:fnlid];
                            if(lLeads.size()>0)
                                theL = lLeads[0];
                            else
                                return ValidationMessage('ERR',null,null,authToPullCredit,'No record found for the Lead ID:' + fnlid,leadId,null,objType);
                            system.debug('=========theL================'+theL);
                            system.debug('=========authToPullCredit================'+authToPullCredit);
                            CreditAuth = theL.Authorization_to_pull_credit__c || (authToPullCredit <> null && authToPullCredit);
                            subSrc = theL.Lead_Sub_Source__c;
                            contactId= (theL <> null && theL.IsConverted) ? theL.ConvertedContactID: null;
                            system.debug('==contactId==' + contactId);
                            system.debug('==earlierCreditAuth==' + CreditAuth );
                            Contact cnt ;
                            if (contactId == null) {
                                for (Contact c : [Select Id, Patient_Applied__c ,Authorization_to_pull_credit__c,LeadSource,ints__SSN__c,phone,Email,Annual_Income__c,ints__Social_Security_Number__c,product__c,In_Network_Out_of_Network__c,Relationship__c,CoordinatorEmail__c,CoordinatorName__c,Lead_Sub_Source__c  from Contact where Lead__c =: fnlid order by CreatedDate DESC LIMIT 1]){
                                    cnt = c;
                                    system.debug('==cnt==' + cnt);
                                    contactId = c.Id;                                
                                }
                            } 
                            else{
                                cnt =[select id,Patient_Applied__c ,Authorization_to_pull_credit__c,LeadSource,ints__SSN__c,Email,phone,Annual_Income__c,ints__Social_Security_Number__c,product__c,In_Network_Out_of_Network__c,Relationship__c,CoordinatorEmail__c,CoordinatorName__c  from Contact where id=: contactId];
                            }
                            
                            system.debug('==cnt ==' + cnt );
                            if(cnt!=null){ 
                                string response=updateJsonParamsinContact(objType,cnt, authToPullCredit,annualIncome,ssn,phone,email,leadId,EZVerify,relationship,patientApplied );
                                system.debug('==response==' + response);
                                if(!string.IsEmpty(response)) return response; 
                                CreditAuth = cnt.Authorization_to_pull_credit__c;
                                if (String.isNotEmpty(cnt.product__c)) product=cnt.product__c;
                                inNetworkOutOfNetwork=cnt.In_Network_Out_of_Network__c;
                                coordinatorName = cnt.CoordinatorName__c ;
                                coordinatorEmail= cnt.CoordinatorEmail__c ;
                            }
                            system.debug('==authToPullCredit==' + authToPullCredit);
                            system.debug('==CreditAuth==' + CreditAuth );
                            /*------------------------------------------------------------------------
                            Need to check this to restrict creation of duplicate Application
                            ------------------------------------------------------------------------*/
                            system.debug('==Earlier ContactID==' + ContactID);
                            bApplicationExists = (contactId <> null)?!SubmitApplicationService.checkExistanceByContact(contactId, pAcc):false;
                            system.debug('=====bApplicationExists ========='+bApplicationExists );                       
                            
                            if(!bApplicationExists && CreditAuth){
                                system.debug('==ConLdId==' + ConLdId);
                                res = LeadToContactConvertor.ExecuteConversion(ConLdId, true, true);
                                system.debug('==res==' + res);
                                if (contactId == null) {
                                    for(Lead ld: [select Authorization_to_pull_credit__c, IsConverted, ConvertedContactID from Lead where id=:fnlid]){                              
                                        contactId = (ld.IsConverted) ? ld.ConvertedContactID :contactId;                                 
                                    }   
                                }
                            }else{
                                res= (bApplicationExists)?'Application Already Exists or reached the limit of funded applications.':'Offers cannot be generated as authorization to pull credit is false';                                                  
                            }                                                                                      
                        }
                }
            }
        }
        catch(Exception e){
            res    = e.getMessage();  
            status = 'ERR';
            WebToSFDC.notifyDev('InterfacetoLdCnt Error - ', res + ' \n Objtype= ' + objType + '\n line ' + e.getLineNumber() + '\n' + e.getStackTraceString());
        }
        if(String.isNotEmpty(contactId)  ){
            Account acc = pAcc != null ? pAcc : WebToSFDC.getPartnerAccount(subSrc);
            LP_Custom__c lp = CustomSettings.getLPCustom();
            String pName =generalPurposeWebServices.isPointOfNeed(product) ? product : '';
            if (generalPurposeWebServices.isSelfPayProduct(product)) pName = EZVerify.Product_Name_SelfPay__c;
            if (String.isEmpty(pName)) pName = lp.Default_Lending_Product__c;

            for (Opportunity thA :  [select id, Patient_Applied__c ,Partner_Account__c, In_Network_Out_of_Network__c,Relationship__c,CoordinatorEmail__c,CoordinatorName__c,Procedure_Date__c  from Opportunity where Name like 'APP%' AND Contact__c=:contactId and ProductName__c =: pName Order By createdDate DESC Limit 1]){               
                applicationId = thA.id;
                partnerId = thA.Partner_Account__c;
                if( inNetworkOutOfNetwork!=null && thA.In_Network_Out_of_Network__c <> inNetworkOutOfNetwork)
                    thA.In_Network_Out_of_Network__c = inNetworkOutOfNetwork;
                if(relationship!=null) thA.Relationship__c = Relationship; // EZ-20
                if(coordinatorName!=null) thA.CoordinatorName__c= coordinatorName;
                if(coordinatorEmail!=null) thA.CoordinatorEmail__c = coordinatorEmail;  
                if(procedureDate!=null) thA.Procedure_Date__c = procedureDate; //API-193  
                 thA.Patient_Applied__c = patientApplied; //Ez-95
                if(requestedPaymentAmount!=null) thA.Requested_Payment_Amount__c = requestedPaymentAmount; //SM-942
                system.debug('--thA.Procedure_Date__c---'+thA.Procedure_Date__c);
                if (generalPurposeWebServices.isPointOfNeed(product) || generalPurposeWebServices.isSelfPayProduct(product)) update thA;   //API-148
                
            }                            
        }
        
        
        JsonRes.put('status',status);         
        If(!String.isEmpty(partnerId)){JsonRes.put('partnerId',partnerId);}
        JsonRes.put('applicationId',applicationId);
        JsonRes.put('authToPullCredit',string.valueOf(CreditAuth));        
        JsonRes.put('message',res); 
        JsonRes.put('leadId', leadId);
        JsonRes.put('contactId', contactId);
        JsonRes.put('objectType',objType);
        
        system.debug('===JsonRes================'+JsonRes);
        return JSON.serializePretty(JsonRes); //Convert response in Json and return      
        
    }
    /* FPP-7 To Check the open Contracts - If either there are open contracts less than mentioned in Merchant or nothing mentioned in that Field then return false i.e. Proceed further */
    public static boolean hasOpenContracts(string ContactId,Boolean bIsMultipleOpp ,Decimal openContractCount ){
        Integer count=0;  
        if(openContractCount !=null) openContractCount =  Integer.ValueOf(openContractCount);
        if (openContractCount == null) openContractCount = 1;
         string[] loanStatus=new string[]{'Active - Good Standing','Active - Bad Standing', 'Approved'};      
         count = [select count() from loan__loan_Account__c where loan__Contact__c = :ContactId and loan__Loan_Status__c IN :(loanStatus)];// AND Lead_Sub_Source__c = :subsource ];
         //count = [select count() from loan__loan_Account__c where loan__Contact__c = :ContactId and (loan__Loan_Status__c like ('%Approv%') or loan__Loan_Status__c like ('%Active%') or loan__Loan_Status__c like ('%Written%') or loan__Loan_Status__c like ('%Partial%') ) ];
         count = 0; //Testing purposes
         if(bIsMultipleOpp && openContractCount !=null && count >= openContractCount) return true;
         else if(!bIsMultipleOpp && count >= 1) return true;
         return false;
        
    }
    private static string ValidationMessage(string error,string partnerId,string applicationId,Boolean authToPullCredit,string message,string leadId,string contactId,string objType){
        map<string,string> JsonRes=new map<string,string>(); 
        JsonRes.put('status',error);         
        If(!String.isEmpty(partnerId)){JsonRes.put('partnerId',partnerId);}
        JsonRes.put('applicationId',applicationId);
        JsonRes.put('authToPullCredit',string.valueOf(authToPullCredit));        
        JsonRes.put('message',message); 
        JsonRes.put('leadId', leadId);
        JsonRes.put('contactId', contactId);
        JsonRes.put('objectType',objType);
        
        system.debug('===JsonRes================'+JsonRes);
        return JSON.serializePretty(JsonRes);
    } 
    
    private static string updateDMLContact(contact cnt){
        string message;Boolean bContactUpdated=false;String conId;
         try{
            
            Database.DMLOptions dml = new Database.DMLOptions(); 
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;
            Database.SaveResult result = Database.update(cnt, dml);
            bContactUpdated = result.isSuccess();
            List<string> chRes = WebToSFDC.CommonErrorHandling(result);
            if (chRes.size() > 0) conId = chRes[0];
            system.debug('=bContactUpdated in method=='+ bContactUpdated );
        } catch (Exception e) {
          WebToSFDC.notifyDev('LeadContInterface Error - Contact Insert',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
            
       }
        if(conId!=null){
            insert new Logging__c(Webservice__c= 'LeadContInterface  - updateDMLContact', API_Request__c= 'Update Contact', API_Response__c = 'Contact update Failed, information matched with existing Contact ID:'+ ' ' +  conId , Contact__c =cnt.id);
            return 'Contact Already exists with the given data. ContactID :' + conId ;
        }

        return string.Valueof(bContactUpdated);
    }
    
    /* #3186 To check validations and update the contact*/
    private static string updateJsonParamsinContact(string objType,contact cnt, Boolean authToPullCredit,Decimal annualIncome,string ssn,string phone,string email,string leadId,ezVerify_Settings__c EZVerify,String relationship, boolean patientApplied){
        // #3186 changes start To update email, SSN, AuthPull and AnnualIncome if authToPullCreditcomes true
        if(cnt !=null && authToPullCredit!=null && authToPullCredit){
            LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults(); //EZ-102
            List<String> subSrcExclude = cuSettings != null && cuSettings.SubsourceDefaultIncomeExcluded__c != null ? cuSettings.SubsourceDefaultIncomeExcluded__c.split(',') : new List<String>();
            Boolean ExcludeDefaultIncome = false;
            for(String s:subSrcExclude){
                if(cnt.Lead_Sub_Source__c == s){
                    ExcludeDefaultIncome = true; 
                    break; 
                }
            }
            Decimal finalIncome = annualIncome == null ? cnt.Annual_Income__c : ((annualIncome >= cuSettings.DefaultPONAnnualIncome__c) ? annualIncome : ExcludeDefaultIncome ? annualIncome : cuSettings.DefaultPONAnnualIncome__c); //EZ-102
            if(cnt.ints__Social_Security_Number__c==null && string.IsEmpty(ssn)){
                string respone=ValidationMessage('VER',null,null,authToPullCredit,'SSN is Required Field.',leadId,cnt.id,objType);
                return respone; //Convert response in Json and return
            }
            if(string.IsEmpty(cnt.phone) && string.isEmpty(phone) && authToPullCredit){
                string respone=ValidationMessage('VER',null,null,authToPullCredit,'Phone is Required Field.',leadId,cnt.id,objType);
                return respone; //Convert response in Json and return
            }
            if(string.IsEmpty(cnt.email) && string.isEmpty(email) && authToPullCredit){
                string respone=ValidationMessage('VER',null,null,authToPullCredit,'Email is Required Field.',leadId,cnt.id,objType);
                return respone; //Convert response in Json and return
            }
            else  
                if(finalIncome==null || (finalIncome!=null && finalIncome < EZVerify.Annual_Income__c)){
                    string respone=ValidationMessage('VER',null,null,authToPullCredit,'Income is Less than 20K.',leadId,cnt.id,objType);
                    return respone; //Convert response in Json and return
                }
                else{
                    if(!string.IsEmpty(ssn)){
                        cnt.SSN__c=ssn;
                        cnt.ints__Social_Security_Number__c=ssn;
                    }
                    cnt.Authorization_to_pull_credit__c=authToPullCredit;
                     cnt.Patient_Applied__c  = patientApplied;  //Ez-95
                    if(!string.IsEmpty(email)) cnt.Email=email;
                    if(!string.IsEmpty(phone)) cnt.phone=phone;
                    cnt.Annual_Income__c = (annualIncome != null && annualIncome >= cuSettings.DefaultPONAnnualIncome__c) ? annualIncome : ExcludeDefaultIncome ? annualIncome : cuSettings.DefaultPONAnnualIncome__c; //EZ-102
                    cnt.Relationship__c = relationship; // EZ-20
                    //update cnt;
                    string contactUpdated= updateDMLContact(cnt);
                    system.debug('=contactUpdated=='+ contactUpdated);
                    system.debug('==cnt==' + cnt);
                    if(!string.IsEmpty(contactUpdated) && !contactUpdated.equalsIgnoreCase('true')){
                        return ValidationMessage('DUP_STOP',null,null,authToPullCredit,contactUpdated,leadId,cnt.id,objType);
                    }
                }
        }
        return '';
    }
    
}