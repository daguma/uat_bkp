global class EmailAge {
    
    public static final String SEQUENCE_ID_EMAIL_AGE = 'Email Age';
    
    /**
    *Map all input data related to email age
    *call the email age api method
    **/
    public static void getEmailAgeResult(String entityId,boolean isManualRun) {
       getEmailAgeResult(entityId, isManualRun, null);
    }

    /**
    *Map all input data related to email age
    *call the email age api method
    **/
  webservice static String getEmailAgeResult(String entityId,boolean isManualRun, String pEMail) {
        String result = 'No Data';
        EmailAgeRequestParameter inputData = mapEmailAgeInputData(entityId, pEMail);
        
        //send email age status code as 0 if this is a manual run
        if(isManualRun)
            inputData.emailAgeStatusCode = '0';
        
        if (inputData != null) {
            result = ProxyApiEmailAge.getEmailAgeResult(JSON.serialize(inputData),entityId);
            System.debug('Result = ' + result);
        }

        return result;
    }

    @future(callout=true)
    public static void getEmailAgeResultFuture(String entityId,boolean isManualRun, String pEMail) {
       getEmailAgeResult(entityId, isManualRun, pEMail);
    }    

    /**
    * Maps loan application record into EmailAgeRequestParameter
    * @param  email The email of the entity to be passed further to Email Age API
    * @param  ip_address The IP address of the entity to be passed further to Email Age API
    * @param  emailAgeStatusCode The status code is to identify to call email age or not
    * @return EmailAgeRequestParameter well formed
    */
    public static EmailAgeRequestParameter mapEmailAgeInputData(String entityId) {
        return mapEmailAgeInputData(entityId, null);
    }

    public static EmailAgeRequestParameter mapEmailAgeInputData(String entityId, String pEmail) {
        EmailAgeRequestParameter emailAgeRequestParameter = CreditReport.mapEmailAgeRequestParameter(entityId, pEmail);        
        return emailAgeRequestParameter != null ? emailAgeRequestParameter : null;
    }
    
     /**
    *Map all input data related to email age
    *call the email age api method
    **/
    public static void getEmailAgeResultUsingSeqId(String entityId,String crId,boolean isManualRun) {
        
        Opportunity app = CreditReport.getOpportunity(entityId);
        
        GDSRequestParameter gdsRequestParameter = CreditReport.mapGDSRequestParameter(entityId, false);
        
        //add Email age sequence Id
        BRMS_Sequence_Ids__c sequenceId_EA = BRMS_Sequence_Ids__c.getInstance(SEQUENCE_ID_EMAIL_AGE);
        
        gdsRequestParameter.sequenceId = (sequenceId_EA <> null) ? sequenceId_EA.Sequence_Id__c : '4';
        
        gdsRequestParameter.executionType = '';
          
        String result = null;
        
        if (gdsRequestParameter != null) {
            if (Test.isRunningTest()) {
                result = LibraryTest.fakeEmailAgeRulesResponse(app.Id);
            } else {
                result = ProxyApiEmailAge.getEmailAgeResultUsingSeqId(JSON.serialize(gdsRequestParameter),crId);
            }
            
            //dump email age result in SF
            if(result != null) {
                
                //parse the expected response
                ProxyApiCreditReportEntity creditReportEntity =
                    (ProxyApiCreditReportEntity)JSON.deserialize(result, ProxyApiCreditReportEntity.class);
                
                if(creditReportEntity <> null) {
                    
                    //update the attachment with details
                    if(!Test.isRunningTest())
                        CreditReport.set(entityId, JSON.serialize(creditReportEntity));
                    
                    if(creditReportEntity.brms <> null && creditReportEntity.brms.emailAgeResponse <> null) {
                        EmailAgeStorageController.storeEmailAgeInSfObject(creditReportEntity.brms.emailAgeResponse,app.Contact__c,entityId,isManualRun); 
                        
                        //adding logging for the Email Age request made
                        MelisaCtrl.DoLogging('BRMS logging- Email Age',JSON.serialize(gdsRequestParameter),JSON.serialize(creditReportEntity.brms.emailAgeResponse),null,app.Contact__c);
                    }
                }
            }
        }
    }
}