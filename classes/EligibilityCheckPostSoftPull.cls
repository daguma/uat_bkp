public class EligibilityCheckPostSoftPull {

    @invocableMethod
    public static void checkEligibility(List<String> contractList) {
        Double thisIncome = 0;
        List<loan_Refinance_Params__c> params = new List<loan_Refinance_Params__c>();
        List<Multiloan_Params__c> multiloanparams = new List<Multiloan_Params__c>();
        loan_Refinance_Params__c refiParams = null;
        Multiloan_Params__c multiParams = null;
        for (String contractId : contractList) {
            List<loan__Loan_Account__c> contract = [SELECT  Id,
                                                    Opportunity__r.Estimated_Grand_Total__c,
                                                    loan__Contact__r.Annual_Income__c,
                                                    loan__Contact__r.MailingStreet,
                                                    loan__Contact__r.MailingCity,
                                                    loan__Contact__r.MailingState,
                                                    Originated_FICO__c,
                                                    Originated_Grade__c
                                                    FROM    loan__Loan_Account__c
                                                    WHERE   Id = : contractId
                                                            LIMIT 1];

            if (contract.size() > 0) {
                loan__Loan_Account__c thisContract = contract.get(0);

                List<loan_Refinance_Params__c> param = [SELECT Id,
                                                        FICO__c,
                                                        Grade__c,
                                                        Eligible_For_Refinance__c,
                                                        Last_Refinance_Eligibility_Review__c,
                                                        On_Past_Due__c,
                                                        On_Payment_Plan__c,
                                                        Contract__c,
                                                        ValidatedIncome__c,
                                                        ContactAddress__c
                                                        FROM    loan_Refinance_Params__c
                                                        WHERE   Contract__c = : thisContract.Id
                                                                ORDER BY CreatedDate DESC
                                                                LIMIT 1];

                if (param.size() == 0) {
                    refiParams = new loan_Refinance_Params__c();
                    thisIncome = (thisContract.Opportunity__r.Estimated_Grand_Total__c == 0 ? thisContract.loan__Contact__r.Annual_Income__c : thisContract.Opportunity__r.Estimated_Grand_Total__c);
                    refiParams.FICO__c = thisContract.Originated_FICO__c;
                    refiParams.Grade__c = thisContract.Originated_Grade__c;
                    refiParams.Eligible_For_Refinance__c = false;
                    refiParams.Last_Refinance_Eligibility_Review__c = Date.today();
                    refiParams.On_Past_Due__c = false;
                    refiParams.On_Payment_Plan__c = false;
                    refiParams.Contract__c = thisContract.Id;
                    refiParams.ValidatedIncome__c = thisIncome;
                    refiParams.ContactAddress__c = thisContract.loan__Contact__r.MailingStreet + ', ' + thisContract.loan__Contact__r.MailingCity + ', ' + thisContract.loan__Contact__r.MailingState;
                } else {
                    refiParams = param.get(0);
                    thisIncome = (thisContract.Opportunity__r.Estimated_Grand_Total__c == 0 ? thisContract.loan__Contact__r.Annual_Income__c : thisContract.Opportunity__r.Estimated_Grand_Total__c);
                    refiParams.FICO__c = thisContract.Originated_FICO__c;
                    refiParams.Grade__c = thisContract.Originated_Grade__c;
                    refiParams.Eligible_For_Refinance__c = false;
                    refiParams.Last_Refinance_Eligibility_Review__c = Date.today();
                    refiParams.On_Past_Due__c = false;
                    refiParams.On_Payment_Plan__c = false;
                    refiParams.ValidatedIncome__c = thisIncome;
                    refiParams.ContactAddress__c = thisContract.loan__Contact__r.MailingStreet + ', ' + thisContract.loan__Contact__r.MailingCity + ', ' + thisContract.loan__Contact__r.MailingState;
                }
                params.add(refiParams);


                 List<Multiloan_Params__c> param2 = [SELECT Id,
                                                        FICO__c,
                                                        Grade__c,
                                                        Eligible_For_Multiloan__c,
                                                        Last_MultiLoan_Eligibility_Review__c,
                                                        Contract__c
                                                        FROM    Multiloan_Params__c
                                                        WHERE   Contract__c = : thisContract.Id
                                                                ORDER BY CreatedDate DESC
                                                                LIMIT 1];
                 if (param2.size() == 0) {
                    multiParams = new Multiloan_Params__c();
                    multiParams.FICO__c = thisContract.Originated_FICO__c;
                    multiParams.Grade__c = thisContract.Originated_Grade__c;
                    multiParams.Eligible_For_Multiloan__c = false;
                    multiParams.Last_MultiLoan_Eligibility_Review__c = Date.today();                
                    multiParams.Contract__c = thisContract.Id;
                    } else {
                    multiParams = param2.get(0);
                    multiParams.FICO__c = thisContract.Originated_FICO__c;
                    multiParams.Grade__c = thisContract.Originated_Grade__c;
                    multiParams.Eligible_For_Multiloan__c = false;
                    multiParams.Last_MultiLoan_Eligibility_Review__c = Date.today();
                }
                multiloanparams.add(multiParams);
            }

        }
        if (params.size() > 0)
            upsert params;
        if (multiloanparams.size() > 0)
            upsert multiloanparams;
    }

    public static List<Offer__c> getRefinanceOffers(Offer__c selectedOffer, List<Offer__c> newOffers, Boolean hasBetterGrade, String newGrade) {
        List<Offer__c> updateList = new List<Offer__c>();
        List<Offer__c> deleteList = new List<Offer__c>();
        List<RefinanceSnapshotOffer__c> newRefiOffers = new List<RefinanceSnapshotOffer__c>();
        List<Offer__c> oldRefiOffers = new List<Offer__c>();
        Boolean saveRefiOffersObj = false;
        Decimal r = 0;
        Decimal po = 0;
        Decimal por = 0;
        Date firstPaymentDate = Date.today();
        Decimal newAPR = 0;
        Double thisIncome = 0;
        String oldAppId = selectedOffer.Opportunity__c;

        if (newOffers.size() > 0) {
            Opportunity newApp = [ SELECT  Id,
                                   Payment_Frequency__c,
                                   Payment_Frequency_Multiplier__c,
                                   Payment_Amount__c,
                                   Contract_Renewed__c,
                                   Contact__c,
                                   Fee_Handling__c,
                                   Estimated_Grand_Total__c,
                                   Amount
                                   FROM    Opportunity
                                   WHERE   Id = : newOffers.get(0).Opportunity__c];

            Decimal currentLoanPayoff = getCurrentContractPayOffBalance(newApp.Contact__c, newApp.Contract_Renewed__c);
            currentLoanPayoff = currentLoanPayoff < 500 ? 500 : currentLoanPayoff;
            System.debug('currentLoanPayoff: ' + currentLoanPayoff);
            Contact c = [SELECT id, Annual_Income__c, MailingState FROM Contact WHERE Id = : newApp.Contact__c];
            thisIncome = ((newApp.Estimated_Grand_Total__c != null && newApp.Estimated_Grand_Total__c > 0) ? newApp.Estimated_Grand_Total__c : c.Annual_Income__c);

            if (newOffers.get(0).id == null)
                newOffers = [SELECT Id FROM Offer__c WHERE Opportunity__c = : newOffers.get(0).Opportunity__c];

            for (Offer__c newOff : newOffers) {
                Offer__c tmpOffer = newOff;
                System.debug('tmpOffer.RefinanceFundedOffer__c = ' + tmpOffer.RefinanceFundedOffer__c);

//                if (!tmpOffer.RefinanceFundedOffer__c)
                tmpOffer.RefinanceFundedOffer__c = (tmpOffer.Loan_Amount__c == newApp.Amount && tmpOffer.Term__c == selectedOffer.Term__c);

                //tmpOffer = FeeUtil.recalculateRefinanceFee(tmpOffer, c.MailingState, newApp.Fee_Handling__c);
                
                //show OR delete the offer:
                if (checkCurrentLoanPayoff(currentLoanPayoff, tmpOffer, newApp.Fee_Handling__c))
                    updateList.add(tmpOffer);
                else
                    deleteList.add(tmpOffer);
            }

            if (updateList.size() > 0)
                update updateList;

            if (deleteList.size() > 0)
                delete deleteList;
        }
        return updateList;
    }

    private static Boolean ptiWithinTolerance(Offer__c theOffer, Double income, String grade) {
        Boolean inTolerance = false;
        Decimal ptiValue = 0;
        String paymentFreq = theOffer.Repayment_Frequency__c;
        Decimal paymentAmt = theOffer.Payment_Amount__c;
        //If refi payment amount is within PTI, show the offer:
        if (paymentFreq != null && (income != null && income > 0))
            ptiValue = ((paymentAmt * OfferCtrlUtil.getFrequencyUtil(paymentFreq)) / income) * 100;
        
        if ((ptiValue <= 12 && (grade.equalsIgnoreCase('C2') || grade.equalsIgnoreCase('C1'))) || ptiValue < 18)
            inTolerance = true;

        return inTolerance;
    }

    @testVisible
    private static Boolean checkCurrentLoanPayoff(Decimal payoff, Offer__c theOffer, String feeHandling) {
        //If refi loan amount is greater than current loan amount:
        System.debug('payoff = ' + payoff);
        System.debug('theOffer.Total_Loan_Amount__c = ' + theOffer.Total_Loan_Amount__c);
        System.debug('theOffer.Loan_Amount__c = ' + theOffer.Loan_Amount__c);
        if (feeHandling.equalsIgnoreCase(FeeUtil.NET_FEE_OUT))
            return (theOffer.Total_Loan_Amount__c >= payoff.setscale(2, RoundingMode.DOWN));
        else
            return (theOffer.Loan_Amount__c >= payoff.setscale(2, RoundingMode.DOWN));
    }

    public static Decimal getCurrentContractPayOffBalance(String contactId, String contractId) {
        Decimal currentLoanPayoff = 0;
        List<loan__Loan_Account__c> contract = [SELECT  Id, loan__Pay_Off_Amount_As_Of_Today__c
                                                FROM    loan__Loan_Account__c
                                                WHERE   loan__Contact__c = : contactId
                                                AND     Id = :contractId 
                                                        ORDER BY CreatedDate DESC
                                                        LIMIT 1];
        if (contract.size() > 0)
            currentLoanPayoff = contract.get(0).loan__Pay_Off_Amount_As_Of_Today__c;


        return currentLoanPayoff;
    }
}