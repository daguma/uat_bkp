@isTest public class ProxyApiUtilTest {

    @isTest static void validates_before_call_api_false() {
        ProxyApiUtil.settings = null;

        Boolean validate = ProxyApiUtil.ValidateBeforeCallApi();
        System.assertEquals(false, validate);
    }

    @isTest static void validates_before_call_api_true() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        Boolean validate = ProxyApiUtil.ValidateBeforeCallApi();
        System.assertEquals(true, validate);
    }

    @isTest static void refresh_api_token() {
        Proxy_Api__c settings = LibraryTest.fakeSettings();
        insert settings;

        ProxyApiUtil.settings = settings;

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'TestCookie');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiUtil.ApiRefreshToken();

        Test.stopTest();

        Proxy_API__c proxySettings = [SELECT Id, Proxy_API_Token__c, Token_New__c FROM Proxy_API__c WHERE Id = : settings.Id LIMIT 1];

        System.assertEquals('tokentest2', proxySettings.Proxy_API_Token__c);
        System.assertEquals('tokentest2', proxySettings.Token_New__c);
    }

    @isTest static void refresh_api_token_with_cookie() {
        Proxy_Api__c settings = LibraryTest.fakeSettings();
        insert settings;

        ProxyApiUtil.settings = settings;

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT; path=/; domain=.lendingpoint.com; HttpOnly');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ProxyApiUtil.ApiRefreshToken();

        Test.stopTest();

        Proxy_API__c proxySettings = [SELECT Id, Proxy_API_Token__c, Token_New__c FROM Proxy_API__c WHERE Id = : settings.Id LIMIT 1];

        System.assertEquals('tokentest2', proxySettings.Proxy_API_Token__c);
        System.assertEquals('tokentest2', proxySettings.Token_New__c);
    }

    @isTest static void gets_new_token() {
        Proxy_Api__c settings = LibraryTest.fakeSettings();
        settings.Proxy_API_Token__c = '';
        settings.Proxy_API_TokenExpiresTime__c = Datetime.now().addDays(-1).getTime();

        ProxyApiUtil.settings = settings;

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'TestCookie');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Boolean result = ProxyApiUtil.ExistsApiToken();

        Test.stopTest();

        System.assertEquals(true, result);
        System.assertEquals('tokentest2', ProxyApiUtil.settings.Proxy_API_Token__c);
    }

    @isTest static void gets_new_token_with_cookie() {
        Proxy_Api__c settings = LibraryTest.fakeSettings();
        settings.Proxy_API_Token__c = '';
        settings.Proxy_API_TokenExpiresTime__c = Datetime.now().addDays(-1).getTime();

        ProxyApiUtil.settings = settings;

        Test.startTest();

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Set-Cookie', 'AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT; path=/; domain=.lendingpoint.com; HttpOnly');

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Boolean result = ProxyApiUtil.ExistsApiToken();

        Test.stopTest();

        System.assertEquals(true, result);
        System.assertEquals('tokentest2', ProxyApiUtil.settings.Proxy_API_Token__c);
    }

    @isTest static void gets_api_result_401() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(401,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiUtil.ApiGetResult('http://somethingtotest.com');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiUtil.settings.Proxy_API_Token__c);
    }

    @isTest static void gets_api_result_201() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiUtil.ApiGetResult('http://somethingtotest.com');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiUtil.settings.Proxy_API_Token__c);
    }

    @isTest static void gets_api_result_200() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiUtil.ApiGetResult('http://somethingtotest.com');
        System.assertEquals('[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]', jsonResult);
    }

    @isTest static void gets_api_result_417() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(417,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiUtil.ApiGetResult('http://somethingtotest.com');
        System.assertEquals('[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]', jsonResult);
    }

    @isTest static void gets_api_result_401_with_body() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(401,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiUtil.ApiGetResult('http://somethingtotest.com', 'testbody');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiUtil.settings.Proxy_API_Token__c);
    }

    @isTest static void gets_api_result_201_with_body() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiUtil.ApiGetResult('http://somethingtotest.com', 'testbody');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiUtil.settings.Proxy_API_Token__c);
    }

    @isTest static void gets_api_result_200_with_body() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiUtil.ApiGetResult('http://somethingtotest.com', 'testbody');
        System.assertEquals('[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]', jsonResult);
    }

    @isTest static void gets_api_result_417_with_body() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(417,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        String jsonResult = ProxyApiUtil.ApiGetResult('http://somethingtotest.com', 'testbody');
        System.assertEquals('[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]', jsonResult);
    }

    @isTest static void gets_binary_result_200() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Blob jsonResult = ProxyApiUtil.ApiGetBinaryResult('http://somethingtotest.com', 'testbody');
        System.assertNotEquals(null, jsonResult);
    }

    @isTest static void gets_binary_result_401() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(401,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},' +
                '{"refresh_token":"test"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Blob jsonResult = ProxyApiUtil.ApiGetBinaryResult('http://somethingtotest.com', 'testbody');
        System.assertEquals(null, jsonResult);

        System.assertEquals('tokentest', ProxyApiUtil.settings.Proxy_API_Token__c);
    }

    @isTest static void gets_binary_result_0() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(0,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Blob jsonResult = ProxyApiUtil.ApiGetBinaryResult('http://somethingtotest.com', 'testbody');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_api_bank_info_mail() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        ProxyApiBankInformation jsonResult = ProxyApiUtil.ApiGetBankInformationByEmail('test2@testmail2.com');
        System.assertNotEquals(null, jsonResult);
    }

    @isTest static void gets_null_api_bank_info_mail() {
        ProxyApiUtil.settings = null;

        ProxyApiBankInformation jsonResult = ProxyApiUtil.ApiGetBankInformationByEmail('test2@testmail2.com');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_api_bank_info_empty_mail() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();

        ProxyApiBankInformation jsonResult = ProxyApiUtil.ApiGetBankInformationByEmail('');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_api_bank_info_pdf() {
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Blob jsonResult = ProxyApiUtil.ApiGetBankInformationPdfByEmail('test2@testmail2.com');
        System.assertNotEquals(null, jsonResult);

        ProxyApiUtil.settings = null;
        jsonResult = ProxyApiUtil.ApiGetBankInformationPdfByEmail('test2@testmail2.com');
        System.assertEquals(null, jsonResult);
    }

    @isTest static void gets_null_api_bank_info_pdf() {
        ProxyApiUtil.settings = null;
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        Blob jsonResult = ProxyApiUtil.ApiGetBankInformationPdfByEmail('test2@testmail2.com');
        System.assertEquals(null, jsonResult);
    }

}