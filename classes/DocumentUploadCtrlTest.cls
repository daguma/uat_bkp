@isTest public with sharing class DocumentUploadCtrlTest {

    static testMethod void test1() {

        Contact TestContact = LibraryTest.createContactTH();
        Opportunity opp = TestHelper.createApplicationFromContact(TestContact,true);

        Id fileId = DocumentUploadCtrl.saveChunk(false,opp.Id,'myFile','bXkgZmlsZQ%3D%3D','text/plain','');
        system.assertNotEquals(null,fileId);

        Id fileId2 = DocumentUploadCtrl.saveChunk(false,opp.Id,'myFile','bXkgZmlsZQ%3D%3D','text/plain',fileId);
        system.assertNotEquals(null,fileId2);

    }

}