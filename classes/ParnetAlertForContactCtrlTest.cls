@isTest public class ParnetAlertForContactCtrlTest {
    
    @isTest static void EZVerifysegmentation(){
        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Documents Requested';
        app.LeadSource__c = 'EZVerify';
        app.Sub__c = '';
        update app;
        
        Contact c = [SELECT Id FROM Contact WHERE Id =: app.contact__c LIMIT 1];
        
        Test.startTest();

        Apexpages.StandardController controller = new Apexpages.StandardController(c);
        
        ParnetAlertForContactCtrl m = new ParnetAlertForContactCtrl(controller);
        
        Test.stopTest();
    }
    
    @isTest static void LoanHero(){
        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Documents Requested';
        app.Sub__c = '';
        app.Lead_Sub_Source__c = 'LoanHero';
        update app;
        
        Contact c = [SELECT Id FROM Contact WHERE Id =: app.contact__c LIMIT 1];
        
        Test.startTest();

        Apexpages.StandardController controller = new Apexpages.StandardController(c);
        
        ParnetAlertForContactCtrl m = new ParnetAlertForContactCtrl(controller);
        
        Test.stopTest();
    }
    
    @isTest static void LendingPoint(){
        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Documents Requested';
        app.Sub__c = '';
        app.Lead_Sub_Source__c = '';
        update app;
        
        Contact c = [SELECT Id FROM Contact WHERE Id =: app.contact__c LIMIT 1];
        
        Test.startTest();

        Apexpages.StandardController controller = new Apexpages.StandardController(c);
        
        ParnetAlertForContactCtrl m = new ParnetAlertForContactCtrl(controller);
        
        Test.stopTest();
    }


}