global class LoanPaymentTxnPayPointGen extends loan.FileGenerator {
    
    private loan__Bank_Account__c bank;
    Integer linecount=0, totalentry = 0;
    Double routingHash = 0, blocks =0.0;
    public String bankAccountUsage =  'collections trust account', currentLine = '';
    private loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();
    Decimal debitTotal = 0;
   // Map<id,loan__Loan_Payment_Transaction__c> pmtsMap = new Map<id,loan__Loan_Payment_Transaction__c>();
    public boolean bIncludeFees = true;
    private static String[] abc = new String[] {'A','B','C','D','E','F','G','H','I','J','K',
                                    'L','M','N','O','P','Q','R','S','T','U','V',
                                    'W','X','Y','Z'};
                                    
    public List<loan__Loan_Payment_Transaction__c> requeryScope(List<SObject> scope) {
        Set<ID> ids = new Set<ID>();    
        List<loan__Loan_Payment_Transaction__c> ptms = (List<loan__Loan_Payment_Transaction__c>) scope;
        Map<Id,loan__Loan_Payment_Transaction__c> pmtMap = new Map<Id, loan__Loan_Payment_Transaction__c>();
        for (loan__Loan_Payment_Transaction__c pmt : ptms){
            ids.add(pmt.Id);
            pmtMap.put(pmt.loan__Loan_Account__c, pmt);
        }
        if (currentLine == 'GUGG2-1') bankAccountUsage = 'Bofi Collections Account';

        try{
            bank = [select Id,Name,loan__Bank_Account_Number__c,loan__Account__c,loan__Account__r.Name,
                                                loan__Account_Type__c,  loan__ACH_Code__c,
                                                loan__Account_Usage__c,
                                                loan__Bank_Name__c,
                                                loan__Contact__c,loan__Contact__r.Name,
                                                loan__Routing_Number__c,
                                                ACH_code__c
                                                from loan__Bank_Account__c
                                                where loan__Account_Usage__c =: bankAccountUsage
                                                and loan__Active__c =true limit 1];
        }catch(Exception e){
            
        }

        
        mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
        if (ec.getObject('LoanPaymentsForACH') != null) ec.deleteObject('LoanPaymentsForACH');
        mfiflexUtil.ObjectCache pmtOC = ec.createObject('LoanPaymentsForACH',
                                                    'Loan_Payment_Transaction__c',
                                                     loan.CustomSettingsUtil.getOrgParameters().loan__Namespace_Prefix__c);
        
        
        pmtOC.addFields('Name,'+
                        'Transaction_Amount__c,'+
                        'Loan_Account__c,'+
                        'Loan_Account__r.Name,'+
                        'Loan_Account__r.ACH_Relationship_Type__c,'+ 
                        'Loan_Account__r.Borrower_ACH__c,'+
                        'Loan_Account__r.Borrower_ACH__r.Account__c,'+
                        'Loan_Account__r.Borrower_ACH__r.Account_Type__c,'+
                        'Loan_Account__r.Borrower_ACH__r.Account_Usage__c,'+
                        'Loan_Account__r.Borrower_ACH__r.Active__c,'+
                        'Loan_Account__r.Borrower_ACH__r.Bank_Account_Number__c,'+
                        'Loan_Account__r.Borrower_ACH__r.Bank_Name__c,'+
                        'Loan_Account__r.Borrower_ACH__r.Contact__c,'+
                        'Loan_Account__r.Borrower_ACH__r.Routing_Number__c,'+
                        'Loan_Account__r.Loan_Amount__c,'+
                        'Loan_Account__r.Account__c,'+
                        'Loan_Account__r.Account__r.Name,'+
                        'Loan_Account__r.Contact__c,'+
                        'Loan_Account__r.Contact__r.Name,'+
                        'Loan_Account__r.ACH_Bank__c,'+
                        'Loan_Account__r.ACH_Bank__r.Name,'+
                        'Loan_Account__r.ACH_Bank__r.ACH_Code__c,'+
                        'Loan_Account__r.ACH_Bank__r.Routing_Number__c,'+
                        'Loan_Account__r.ACH_On__c,'+
                        'Loan_Account__r.ACH_Account_Number__c,'+
                        'Loan_Account__r.ACH_Account_Type__c,'+
                        'Loan_Account__r.ACH_Debit_Amount__c,'+
                        'Loan_Account__r.Ach_Debit_Day__c,'+
                        'Loan_Account__r.ACH_Drawer_Name__c,'+
                        'Loan_Account__r.ACH_End_Date__c,'+
                        'Loan_Account__r.ACH_Frequency__c,'+
                        'Loan_Account__r.ACH_Next_Debit_Date__c,'+
                        'Loan_Account__r.ACH_Routing_Number__c,'+
                        'Loan_Account__r.ACH_Start_Date__c,'+
                        'Loan_Account__r.OT_ACH_Account_Number__c,'+
                        'Loan_Account__r.OT_ACH_Account_Type__c,'+
                        'Loan_Account__r.OT_ACH_Bank_Name__c,'+
                        'Loan_Account__r.OT_ACH_Debit_Date__c,'+
                        'Loan_Account__r.OT_ACH_Drawer_Address1__c,'+
                        'Loan_Account__r.OT_ACH_Drawer_Address2__c,'+
                        'Loan_Account__r.OT_ACH_Drawer_City__c,'+
                        'Loan_Account__r.OT_ACH_Drawer_Name__c,'+
                        'Loan_Account__r.OT_ACH_Drawer_State__c,'+
                        'Loan_Account__r.OT_ACH_Drawer_Zip__c,'+
                        'Loan_Account__r.OT_ACH_Fee_Amount__c,'+
                        'Loan_Account__r.OT_ACH_Payment_Amount__c,'+
                        'Loan_Account__r.OT_ACH_Relationship_Type__c,'+
                        'Loan_Account__r.OT_ACH_Routing_Number__c,' +
                        'Loan_Account__r.OT_Borrower_ACH__r.Account__c,'+
                        'Loan_Account__r.OT_Borrower_ACH__r.Account_Type__c,'+
                        'Loan_Account__r.OT_Borrower_ACH__r.Account_Usage__c,'+
                        'Loan_Account__r.OT_Borrower_ACH__r.Active__c,'+
                        'Loan_Account__r.OT_Borrower_ACH__r.Bank_Account_Number__c,'+
                        'Loan_Account__r.OT_Borrower_ACH__r.Bank_Name__c,'+
                        'Loan_Account__r.OT_Borrower_ACH__r.Contact__c,'+
                        'Loan_Account__r.OT_Borrower_ACH__r.Routing_Number__c');
                        
        pmtOC.addNamedParameter('ids', ids);
        pmtOC.setWhereClause('ID IN :ids');
        pmtOC.executeQuery();
        //return (List<loan__Loan_Payment_Transaction__c>) pmtOC.getRecords();
    


        List<loan__Loan_Payment_Transaction__c>loanPmts = (List<loan__Loan_Payment_Transaction__c>) pmtOC.getRecords();
        System.debug('Payments = ' + loanPmts.size());
        if (bIncludeFees) {
            for (loan__Loan_Payment_Transaction__c pmt : loanPmts){
                pmtMap.get(pmt.loan__Loan_Account__c).loan__Transaction_Amount__c = pmt.loan__Transaction_Amount__c;
            }
        }


        system.debug(loggingLevel.Debug,'-------'+bank);

        return loanPmts;
    }

    public override String getSimpleFileName() {
        return 'Loan_Payments';
    }
    
    public override List<String> getEntries(loan.TransactionSweepToACHState state, List<SObject> scope) {
        List<String> retVal = new List<String>();
        List<loan__Loan_Payment_Transaction__c> pmts = requeryScope(scope);
        List<SObject> objects = new List<SObject>();
        for(loan__Loan_Payment_Transaction__c pmt : pmts){
            if(pmt.loan__Loan_Account__r.loan__ACH_Routing_Number__c != null || pmt.loan__Loan_Account__r.loan__ACH_Account_Number__c !=null || pmt.loan__Loan_Account__r.loan__Loan_Amount__c != null ){
                addToValidScope(pmt);
                objects.add(generateEntryDetailRecord(pmt.loan__Loan_Account__r,pmt));
           }
            else{
                addToInvalidScope(pmt,'Borrower routing no, Loan account ACH Account number or Loan amount is missing');
            }
        }
        
        filegen.CreateSegments segments = new filegen.CreateSegments(objects);
        retVal = segments.retString();
        for(String line:retVal){
            addEntry(line + '\n');
        }
        return retVal;
    }
    
    public override String getHeader(loan.TransactionSweepToACHState state, List<SObject> scope) {
        List<String> retVal = new List<String>();
        List<SObject> headerRecs = new List<SObject>();
        headerRecs.add(generateFileHeaderRecord(state));
        headerRecs.add(generateBatchHeaderRecord());
        filegen.CreateSegments segments =new filegen.CreateSegments(headerRecs);
        retVal = segments.retString();
 
        return retVal[0]+'\r\n' + retVal[1] + '\r\n';
    }
    
    public override String getTrailer(loan.TransactionSweepToACHState state, LIST<SObject> scope) {
        String achFileTrailerContent = '';
        List<String> retVal = new List<String>();
        List<SObject> trailerRecs = new List<SObject>();
        trailerRecs.add(generateBatchControlRecord(scope));
        trailerRecs.add(generateFileControlRecord(scope));
        filegen.CreateSegments segments =new filegen.CreateSegments(trailerRecs);
        retVal = segments.retString();
        for(String s : retVal){
            achFileTrailerContent = achFileTrailerContent + s + '\r\n';
        }
        
        Integer fillersToAdd = Integer.valueOf( blocks * 10 ) - linecount;
        achFileTrailerContent = achFileTrailerContent.subString( 0 , achFileTrailerContent.length() - 2 );
             
        //fillers are added to make number of characters in generated file multiple of 940
        for(Integer i = 0 ; i < fillersToAdd ; i++ ) {
            achFileTrailerContent = achFileTrailerContent + '\r\n'+ loan.StringUtils.rightPadString('', 94,'9');
        }
        
        return achFileTrailerContent;
    }
    
    //File header
    
    private filegen__File_Header_Record__c generateFileHeaderRecord(loan.TransactionSweepToACHState state){
        Datetime currentSystemDateTime = Date.today();
        Date csd = Date.newInstance(currentSystemDateTime.year(), currentSystemDateTime.month(), currentSystemDateTime.day());
        String myTime = String.valueOf(currentSystemDateTime.hour()) + String.valueOf(currentSystemDateTime.minute());
        filegen__File_Header_Record__c fHR = new filegen__File_Header_Record__c();
        String banksRoutingNo ='' ;
        if(bank !=null && bank.loan__Routing_Number__c != null){
            banksRoutingNo =loan.StringUtils.leftPadString(''+bank.loan__Routing_Number__c,9,'0');
        }
        
        

        fHR.filegen__Immediate_Destination__c = banksRoutingNo;
        fHR.filegen__Immediate_Origin__c =   loan.StringUtils.leftPadString(bank.loan__Ach_Code__c,10,' ') ;    //   (acha.loan__ACH_Id__c!=null?acha.loan__ACH_Id__c:'');
        fHR.filegen__Creation_Date__c = csd;
        fHR.filegen__Creation_Time__c = myTime;
        fHR.filegen__ID_Modifier__c = abc[state.counter] ;
        
        fHR.filegen__Immediate_Destination_Name__c = '';
        if(bank != null && bank.loan__Bank_Name__c!= null){
            fHR.filegen__Immediate_Destination_Name__c = bank.loan__Bank_Name__c;
        }
        
        fHR.filegen__Immediate_Origin_Name__c = acha.loan__Organization_Name__c;
        fHR.filegen__Reference_Code__c = bank.ACH_Code__c;
        
        return fHR;
    }
     
     //Batch Header
     private filegen__Batch_Header_Record__c generateBatchHeaderRecord(){
        Datetime currentSystemDateTime = Date.today();
        Date csd = Date.newInstance(currentSystemDateTime.year(), currentSystemDateTime.month(), currentSystemDateTime.day());
        filegen__Batch_Header_Record__c bHR = new filegen__Batch_Header_Record__c();
        
        bHR.filegen__Service_Class_Code__c = '200';
        bHR.filegen__Company_Name__c = acha.loan__Organization_Name__c;
        bHR.filegen__Company_Discretionary_Data__c = 'ACH_RECIEPTS';
        bHR.filegen__Company_Identification__c = bank.ACH_Code__c;
        
        bHR.filegen__SEC_Code__c = 'PPD';
        bHR.filegen__Company_Entry_Description__c = 'PAYMENT';
        bHR.filegen__Company_Descriptive_Date__c = csd;
        bHR.filegen__Effective_Entry_Date__c = csd;
        String banksRoutingNo = '';
        if(bank != null && bank.loan__Routing_Number__c != null){
            banksRoutingNo = String.valueOf(bank.loan__Routing_Number__c);
        }
        
        if(banksRoutingNo.length()<9){
            banksRoutingNo=loan.StringUtils.leftPadString(''+bank.loan__Routing_Number__c,9,'0');
        }
        bHR.filegen__Originating_DFI_Identification__c = banksRoutingNo;
        bHR.filegen__Batch_Number__c = '0000123';
        
        return bHR;
    }
    
    //Batch Control
    private filegen__Batch_Control_Record__c generateBatchControlRecord(LIST<SObject> scope){
        filegen__Batch_Control_Record__c bCR = new filegen__Batch_Control_Record__c();
        
        bCR.filegen__Service_Class_Code__c = '200';
        bCR.filegen__Entry_Addenda_Count__c = String.valueOf(scope.size());
        bCR.filegen__Entry_Hash__c = routingHash.format().remove(',');
        bCR.filegen__Total_Debit_Entry_Dollar_Amount__c = String.valueOf(Integer.valueOf(debitTotal.setScale(2)*100));
        String identity = '';
        if(bank != null && bank.ACH_Code__c != null){
            identity = loan.stringutils.leftpadString(bank.ACH_Code__c,10,'');
        }
        bCR.filegen__Company_Identification__c = identity;
        
        String banksRoutingNo = '';
        if(bank != null && bank.loan__Routing_Number__c != null){
            banksRoutingNo = String.valueOf(bank.loan__Routing_Number__c);
        }
        if(banksRoutingNo.length()<9){
            banksRoutingNo=loan.StringUtils.leftPadString(''+bank.loan__Routing_Number__c,9,'0');
        }
        bCR.filegen__Originating_DFI_Identification__c = banksRoutingNo;
        bCR.filegen__Batch_Number__c = '0000123';
        
        return bCR;
    }
    
    //File Control
    private filegen__File_Control_Record__c generateFileControlRecord(LIST<SObject> scope){
        filegen__File_Control_Record__c fCR = new filegen__File_Control_Record__c();
        
        linecount = scope.size()+4;
        blocks = math.ceil((double.valueOf(linecount))/10);
        fCR.filegen__Batch_Count__c = '000001';
        fCR.filegen__Block_Count__c = String.valueOf(Integer.valueOf(blocks));
        fCR.filegen__Entry_Addenda_Count__c = String.valueOf(scope.size());
        fCR.filegen__Entry_Hash__c = routingHash.format().remove(',');
        fCR.filegen__total_debit_entry_dollar_amount_in_fil__c = String.valueOf(Integer.valueOf(debitTotal.setScale(2)*100));
        
        return fCR;
    }
    
    //Entry Detail
    public filegen__Entry_Detail_Record__c generateEntryDetailRecord(loan__Loan_Account__c loanAccount,loan__Loan_Payment_Transaction__c pmt){
        filegen__Entry_Detail_Record__c eDR = new filegen__Entry_Detail_Record__c();
        totalentry++;
        String customerRoutingNo = '', checkDigit ='', banksrouting = '', customerName = '', accountNumber, ACHAccountType, lName = loanAccount.Name ;
        Double routingHashNo = 0;
        if (String.isEmpty(lName)) lName = '';
        if(loanAccount.loan__ACH_Routing_Number__c != null){
            customerRoutingNo = String.valueOf(loanAccount.loan__ACH_Routing_Number__c);
        }else if(loanAccount.loan__Borrower_ACH__c != null && loanAccount.loan__Borrower_ACH__r.loan__Routing_Number__c != null){
            customerRoutingNo = String.valueOf(loanAccount.loan__Borrower_ACH__r.loan__Routing_Number__c);
        }

        accountNumber = loanAccount.loan__ACH_Account_Number__c != null ? loanAccount.loan__ACH_Account_Number__c :(loanAccount.loan__Borrower_ACH__c != null && loanAccount.loan__Borrower_ACH__r.loan__Bank_Account_Number__c != null ? loanAccount.loan__Borrower_ACH__r.loan__Bank_Account_Number__c : '');
        ACHAccountType = loanAccount.loan__ACH_Account_Type__c != null ? loanAccount.loan__ACH_Account_Type__c : (loanAccount.loan__Borrower_ACH__c != null ? loanAccount.loan__Borrower_ACH__r.loan__Account_Type__c : '');

        if(bank != null && bank.loan__Routing_Number__c != null){
            banksRouting = String.valueOf(bank.loan__Routing_Number__c);
        }
    
        if(banksRouting.length()<9){
            banksRouting=loan.StringUtils.leftPadString(banksRouting,9,'0');
        }

        customerName = loanAccount.loan__ACH_Drawer_Name__c;
        if(loanAccount.loan__ACH_Relationship_Type__c == null || loanAccount.loan__ACH_Relationship_Type__c == loan.LoanConstants.ACH_RELATIONSHIP_TYPE_PRIMARY){ 
            if(loanAccount.loan__Account__c != null){
                customerName = loanAccount.loan__Account__r.Name;
            }
            else if(loanAccount.loan__Contact__c != null){
                customerName = loanAccount.loan__Contact__r.Name;
            }
        }
            
        if (!loanAccount.loan__ACH_On__c){
            ACHAccountType = !String.isEmpty(loanAccount.loan__OT_ACH_Account_Type__c) ? loanAccount.loan__OT_ACH_Account_Type__c : 
                                    (loanAccount.loan__OT_Borrower_ACH__c != null && !String.IsEmpty(loanAccount.loan__OT_Borrower_ACH__r.loan__Account_Type__c ) ? loanAccount.loan__OT_Borrower_ACH__r.loan__Account_Type__c : ACHAccountType);
        
            customerName = (!String.isEmpty(loanAccount.loan__OT_ACH_Drawer_Name__c) ? loanAccount.loan__OT_ACH_Drawer_Name__c : customerName);
            if(loanAccount.loan__OT_ACH_Relationship_Type__c == null || loanAccount.loan__OT_ACH_Relationship_Type__c == loan.LoanConstants.ACH_RELATIONSHIP_TYPE_PRIMARY){ 
                if(loanAccount.loan__Account__c != null){
                    customerName = loanAccount.loan__Account__r.Name;
                }
                else if(loanAccount.loan__Contact__c != null){
                    customerName = loanAccount.loan__Contact__r.Name;
                }
            }
            
            if(!String.IsEmpty(loanAccount.loan__OT_ACH_Routing_Number__c)){
                customerRoutingNo = String.valueOf(loanAccount.loan__OT_ACH_Routing_Number__c);
            }else if(loanAccount.loan__OT_Borrower_ACH__c != null && !String.isEmpty(loanAccount.loan__OT_Borrower_ACH__r.loan__Routing_Number__c)){
                customerRoutingNo = String.ValueOf(loanAccount.loan__OT_Borrower_ACH__r.loan__Routing_Number__c);
            }
            
            
            accountNumber = !String.IsEmpty(loanAccount.loan__OT_ACH_Account_Number__c)  ? loanAccount.loan__OT_ACH_Account_Number__c :
                                    (loanAccount.loan__OT_Borrower_ACH__c != null && !String.isEmpty(loanAccount.loan__OT_Borrower_ACH__r.loan__Bank_Account_Number__c) ? loanAccount.loan__OT_Borrower_ACH__r.loan__Bank_Account_Number__c : accountNumber);
            
        }

  
        if(String.isNotEmpty(customerRoutingNo) && customerRoutingNo.length()>8){
            customerRoutingNo=loan.StringUtils.leftPadString(''+customerRoutingNo,9,'0');
            checkDigit = customerRoutingNo.substring(8);
            routingHashNo = Double.valueOf( customerRoutingNo.left(8));
        }

        if( loan.LoanConstants.ACCOUNT_TYPE_SAVING.equalsIgnoreCase(ACHAccountType) 
            || loan.LoanConstants.ACCOUNT_TYPE_SAVINGS.equalsIgnoreCase(ACHAccountType) ){
            eDR.filegen__Transaction_Code__c = '37';
        }else if(loan.LoanConstants.ACCOUNT_TYPE_CHECKING.equalsIgnoreCase(ACHAccountType )){
            eDR.filegen__Transaction_Code__c = '27';
        }

        eDR.filegen__RDFI_Identification__c = customerRoutingNo;
        eDR.filegen__Check_Digit__c = checkDigit;
        eDR.filegen__RDFI_Account_Number__c = accountNumber;
        eDR.filegen__Amount__c = pmt.loan__Transaction_Amount__c != null?String.valueOf(pmt.loan__Transaction_Amount__c.setScale(2)):'';
        eDR.filegen__Individual_Identification_Number__c = loan.StringUtils.rightPadString(lName.replace('-','0'),15, ' ');
        eDR.filegen__Individual_Name__c = customerName;
        eDR.filegen__Addenda_Record_Indicator__c='0';
        eDR.filegen__Trace_Number__c = banksrouting.substring(0,8)+loan.StringUtils.leftPadString(string.valueof(totalentry),7,'0');
        routingHash =+ routingHashNo;
        debitTotal = debitTotal +( pmt.loan__Transaction_Amount__c != null ?pmt.loan__Transaction_Amount__c:0);

        return eDR;
    }
     
    

}