@isTest
private class DealAutomaticSalesQueueCtrlTest {

    @isTest static void gets_sales_queue(){
        Deal_Automatic_Attributes__c attr = new Deal_Automatic_Attributes__c();
        attr.Id__c = 1;
        attr.Name = 'Test';
        attr.Display_Name__c = 'Test';
        attr.Usage__c = 'Sales_Category';
        attr.Key_Name__c = 'SalesCategory';
        attr.Display_Order__c = 1;
        insert attr;
        
        DealAutomaticSalesQueueCtrl dasq = new DealAutomaticSalesQueueCtrl();
        
        String result = DealAutomaticSalesQueueCtrl.getSalesQueues();
        
        System.assertNotEquals(null, result);
    }
    
    @isTest static void saves_user(){
        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'CreditQualified';
        attribute.Display_Name__c = 'Credit Qualified';
        attribute.Key_Name__c = 'Credit Qualified';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 2;
        attribute.Display_Order__c = 1;
        insert attribute;

        Integer listBef = [SELECT COUNT() FROM Deal_Automatic_Assignment_Data__c];

        DealAutomaticUserGroup daug = new DealAutomaticUserGroup('userId_1000', 'userName_Test', 'groupId_2000', 'groupName_Test', true, false);

        List<DealAutomaticUserGroup> listDaug = new List<DealAutomaticUserGroup>();
        listDaug.add(daug);

        List<DealAutomaticUserGroup> list1 = DealAutomaticSalesQueueCtrl.saveUsers(attribute, listDaug, new List<DealAutomaticUserGroup>());

        System.assertNotEquals(null, list1);
    }

    @isTest static void gets_user_group_by_sales_queue(){
        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'CreditQualified';
        attribute.Display_Name__c = 'Credit Qualified';
        attribute.Key_Name__c = 'Credit Qualified';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 2;
        attribute.Display_Order__c = 1;
        insert attribute;

        Deal_Automatic_Assignment_Data__c data = new Deal_Automatic_Assignment_Data__c();
        data.Attribute_Id__c = 2;
        data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                 '"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"},{"userName":"Joe Valeo","userId":"005U0000005k9wOIAQ",' +
                                 '"selected":true,"groupName":"Kennesaw Sales Center","groupId":"00GU0000002ccJvMAI"}]';
        insert data;

        List<DealAutomaticUserGroup> list1 = DealAutomaticSalesQueueCtrl.getUsersGroupsBySalesQueue(attribute);

        System.assert(list1.size() > 0);
        System.assertNotEquals(null, list1);
    }
    
}