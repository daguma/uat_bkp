global class ClContractJob implements Database.Batchable<sObject>, Database.Stateful, Schedulable {//quité el allow callouts

	global final String query;
	global final String option;


	global ClContractJob(String option) {
		this.option = option;
		if (option == 'MarkedAsModifiedAlert') {
			this.query = ClContractJobHelper.MODIFICATION_TRACKING_QUERY;
		} 
	}
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		if (option == 'MarkedAsModifiedAlert') {
			for (sobject s : scope) {
				OtherTransactionLogs__c param = (OtherTransactionLogs__c) s;
				ClContractJobHelper.SendAlerts(param);
			}
		} 
	}

	global void finish(Database.BatchableContext BC) {
	}

	global void execute(SchedulableContext sc) {
		if (option == 'MarkedAsModifiedAlert') {
			ClContractJob job = new ClContractJob(option);
			database.executebatch(job, 1);
		}
	}

}

//To run:
/*
	System.schedule('ClContractJob - MarkedAsModifiedAlert', '0 30 7 1/1 * ? *', new ClContractJob('MarkedAsModifiedAlert'));
*/