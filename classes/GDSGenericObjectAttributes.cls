public with sharing class GDSGenericObjectAttributes {


	public GDSGenericObjectAttributes() {}

	public virtual class GDSGenericCallResponse{

		public String http_status_code {get;set;}

		public String b64Response {get;set;}

		public String isValidResponse {get;set;}

		public String errorMessage {get;set;}

		public String errorDetails {get;set;}

		public String fullErrorMessage {get;set;}

		public GDSGenericCallResponse(){}

		public String get_Response() {
			return  EncodingUtil.base64Decode(this.b64Response).toString();
		}

	}

	public virtual class Error{

		@AuraEnabled public String errorMessage {get;set;}

		@AuraEnabled public String errorDetails {get;set;}

		public Error(){}
	}

}