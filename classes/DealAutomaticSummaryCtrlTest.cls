@isTest public class DealAutomaticSummaryCtrlTest {

    @isTest static void getAllUsersGroups() {

        TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimesAllAssignedToKennesaw();

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__status__c = 'Credit Qualified';
        app.OwnerId = Label.Default_Lead_Owner_ID;
        app.Automatically_Assigned__c = false;
        update app;

        DealAutomaticUserGroup group1 = new DealAutomaticUserGroup('005U0000004YXGiIAO', 'Kimberly Gardner', '00GU0000002ccJvMAI', 'Kennesaw Sales Center', true, false);

        DealAutomaticSummaryCtrl dasc = new DealAutomaticSummaryCtrl();

        try {

            List<DealAutomaticUserGroup> result = DealAutomaticSummaryCtrl.getAllUsersGroups(true);

            System.assert(result.size() > 0);

        } catch (Exception e) {}

        try {

            List<DealAutomaticUserGroup> result2 = DealAutomaticSummaryCtrl.getAllUsersGroups(false);

            System.assert(result2.size() > 0);

        } catch (Exception e) {}

        DealAutomaticUserDetails user1 = new DealAutomaticUserDetails();

    }

    @isTest static void getAttributesBySelectedUser() {
        TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimesAllAssignedToKennesaw();

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__status__c = 'Credit Qualified';
        app.OwnerId = Label.Default_Lead_Owner_ID;
        app.Automatically_Assigned__c = false;
        update app;

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'TodayLeads';
        attribute.Display_Name__c = 'Today\'s Leads';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;
        insert attribute;

        Deal_Automatic_Assignment_Data__c data = new Deal_Automatic_Assignment_Data__c();
        data.Attribute_Id__c = 1;
        data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                 '"groupName":"Kennesaw Sales Center","groupId":"' + Label.Kennesaw_Group_Id + '"}]';
        insert data;

        List<Deal_Automatic_Attributes__c> attributeList = new List<Deal_Automatic_Attributes__c>();
        attributeList.add(attribute);

        DealAutomaticUserDetails user1 = new DealAutomaticUserDetails('005U0000004YXGiIAO', attributeList);

        try {

            List<DealAutomaticUserDetails> result = DealAutomaticSummaryCtrl.getAttributesBySelectedUser('005U0000004YXGiIAO');

            System.assert(result.size() > 0);

        } catch (Exception e) {}

        try {

            List<DealAutomaticSummaryList>  result2 = DealAutomaticSummaryCtrl.getSummaryListBySelectedUser('005U0000004YXGiIAO');
        } catch (Exception e) {}

    }

    @isTest static void reAssignDeals() {

        TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimesAllAssignedToKennesaw();

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__status__c = 'Credit Qualified';
        app.OwnerId = Label.Default_Lead_Owner_ID;
        app.Automatically_Assigned__c = false;
        update app;

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'TodayLeads';
        attribute.Display_Name__c = 'Today\'s Leads';
        attribute.Usage__c = 'Category';
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;
        insert attribute;

        Deal_Automatic_Assignment_Data__c data = new Deal_Automatic_Assignment_Data__c();
        data.Attribute_Id__c = 1;
        data.Assigned_Users__c = '[{"userName":"Kimberly Gardner","userId":"005U0000004YXGiIAO","selected":true,' +
                                 '"groupName":"Kennesaw Sales Center","groupId":"' + Label.Kennesaw_Group_Id + '"}]';
        insert data;

        attribute = new Deal_Automatic_Attributes__c();
        attribute.Name = 'LendingTree';
        attribute.Display_Name__c = 'LendingTree';
        attribute.Usage__c = 'Deal_Source';
        attribute.Id__c = 14;
        attribute.Display_Order__c = 2;
        insert attribute;

        data = new Deal_Automatic_Assignment_Data__c();
        data.Attribute_Id__c = 14;
        data.Assigned_Users__c = '[{"userName":"Joe Valeo","userId":"005U0000005k9wOIAQ",' +
                                 '"selected":true,"groupName":"Kennesaw Sales Center","groupId":"' + Label.Kennesaw_Group_Id + '"}]';
        insert data;

        List<Deal_Automatic_Attributes__c> attributeList = new List<Deal_Automatic_Attributes__c>();
        attributeList.add(attribute);

        DealAutomaticUserDetails user1 = new DealAutomaticUserDetails('005U0000004YXGiIAO', attributeList);

        user1 = new DealAutomaticUserDetails('005U0000005k9wOIAQ', attributeList);

        try {

            DealAutomaticSummaryCtrl.reAssignDeals('005U0000004YXGiIAO', '005U0000005k9wOIAQ', '07/14/2016 11:46 AM', '07/31/2016 11:46 AM');

        } catch (Exception e) {}

    }

}