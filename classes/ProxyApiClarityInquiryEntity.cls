global class ProxyApiClarityInquiryEntity {

    public Long id {get; set;}
    public Long creditReportId {get; set;}
    public Integer fraudScore {get; set;}
    public Double fraudCrosstabMultiple {get; set;}
    public Boolean isApproved {get; set;}
    public String trackingNumber {get; set;}
    public Long createdDate {get; set;}
    public String errorMessage {get; set;}
    public String errorDetails {get; set;}
    public String reasonCodeDescription {get; set;}

    public Boolean errorResponse {
        get { return errorMessage != null; }
        set;
    }

    public DateTime creationDate {
        get {
            if (createdDate == null) return null;

            return DateTime.newInstance(createdDate);
        }
        set;
    }
}