@isTest public class CaseAfterCreateTest {

    @isTest static void case_Default_Owner() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c1.Email = 'Test258@MyCompany.COM';
        update c1;

        Case c = new Case();
        c.Description = 'something to test test@lendingpoint.com test test test test test';
        c.SuppliedEmail = 'test@test.com';
        c.Subject = 'New Case Request Complete, 003 fdf Statements Available in ACH Information Tab';
        c.ContactId = c1.Id;
        c.Opportunity__c = app.Id ;
        insert c;
    }

    @isTest static void case_DE_Owner() {
        Opportunity app = LibraryTest.createApplicationTH();
        app.DE_Assigned_To_Doc__c = 'Chris Watts';
        app.Name='APP-0001111111';
        update app;
        
        Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c1.Email = 'Test258@MyCompany.COM';
        update c1;
        
        Case c = new Case();
        c.Description = 'something to test Test258@MyCompany.COM test test test test docs@lendingpoint.com test';
        c.SuppliedEmail = 'docs@lendingpoint.com';
        c.Subject = 'APP-0001111111 Documents Have Been Uploaded from Web';
        c.Opportunity__c = app.Id;
        
        insert c;
    }

    @isTest static void assigns_contact_to_case() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c1.Email = 'Test258@MyCompany.COM';
        app.Name = 'APP-00038888';
        update c1;
        update app;

        Case c = new Case();
        c.Description = 'something to test test@lendingpoint.com test test test test258@mycompany.com test test test';
        c.SuppliedEmail = 'test@test.com';
        c.Subject = 'New Case';
        insert c;

        c = [SELECT Id, OwnerId, Description, ContactId FROM Case WHERE Id = : c.id LIMIT 1];

        System.assertEquals(c1.id, c.ContactId);
        System.assertNotEquals(c.OwnerId, null);
    }

    @isTest static void assigns_automatically() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c1.Email = 'test258@mycompany.com';
        update c1;

        Case c = new Case();
        c.Description = 'something to test test@lendingpoint.com test test test test258@mycompany.com test docs@lendingpoint.com';
        c.SuppliedEmail = 'test@test.com';
        c.Subject = 'New Case';
        insert c;

        c = [SELECT Id, OwnerId, Description, ContactId, Automatic_Assignment__c FROM Case WHERE Id = : c.id LIMIT 1];

       // System.assertEquals(true, c.Automatic_Assignment__c);
    }
    
    @isTest static void assigns_automatically_app() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c1.Email = 'test258@mycompany.com';
        update c1;
        Opportunity tapp = [Select Id, Name from Opportunity where Id =: app.Id];
        
        Case c = new Case();
        c.Description = 'something to test test@lendingpoint.com test test test te@mycompany.com test docs@lendingpoint.com';
        c.SuppliedEmail = 'test@test.com';
        c.Subject = 'New Case ' + tapp.name + ' ';
        insert c;

        c = [SELECT Id, OwnerId, Description, ContactId, Automatic_Assignment__c FROM Case WHERE Id = : c.id LIMIT 1];

     //   System.assertEquals(true, c.Automatic_Assignment__c);
    }
    
    @isTest static void assigns_to_same_user() {
        
        User usr = [SELECT Id, Name, ProfileId, Profile.Name FROM User where Profile.Name = 'Customer Service' AND IsActive = true Limit 1];
        
        System.runAs(usr) {
            
            Opportunity app = LibraryTest.createApplicationTH();
            Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
            c1.Email = 'Test258@MyCompany.COM';
            update c1;
    
            Case case1 = new Case();
            case1.Description = 'something to test test@lendingpoint.com test test test test258@mycompany.com test test test';
            case1.SuppliedEmail = 'test@test.com';
            case1.Subject = 'New Case 1';
            insert case1;
    
            Case case2 = new Case();
            case2.Description = 'something to test test@lendingpoint.com test test test test258@mycompany.com test test test';
            case2.SuppliedEmail = 'test@test.com';
            case2.Subject = 'New Case 2';
            insert case2;
    
            case1 = [SELECT Id, OwnerId, Description, ContactId, Automatic_Assignment__c FROM Case WHERE Id = : case1.id LIMIT 1];
    
            case2 = [SELECT Id, OwnerId, Description, ContactId, Automatic_Assignment__c FROM Case WHERE Id = : case2.id LIMIT 1];
            
            system.debug('case1.OwnerId: ' + case1.OwnerId + ' :case2.OwnerId : ' + case2.OwnerId);
    
            System.assertEquals(case1.OwnerId, case2.OwnerId);
             
          }   

    }

    
    @isTest static void changeStatus_OwnerHal_Opp() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.OwnerId = '005U0000003pdKWIAY';
        app.Status__c = 'Credit Qualified';
        app.DE_Assigned_To_Doc__c = 'Chris Watts';
        app.Name='APP-0001111111';
        update app;
        
        Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c1.Email = 'Test258@MyCompany.COM';
        update c1;
        
        Case c = new Case();
        c.Description = 'something to test Test258@MyCompany.COM test test test test docs@lendingpoint.com test';
        c.SuppliedEmail = 'docs@lendingpoint.com';
        c.Subject = 'APP-0001111111 Documents Have Been Uploaded from Web';
        c.Opportunity__c = app.Id;
        
        insert c;
        
    }

    
    @isTest static void case_for_customerservice() {

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c1.Email = 'Test258@MyCompany.COM';
        update c1;

        Case case1 = new Case();
        case1.Description = 'something to test test@lendingpoint.com test test test test258@mycompany.com test test test';
        case1.SuppliedEmail = 'customerservice@lendingpoint.com';
        case1.Subject = 'New Case 1';
        insert case1;

        case1 = [SELECT Id, OwnerId, Description, ContactId, SuppliedEmail, Automatic_Assignment__c FROM Case WHERE Id = : case1.id LIMIT 1];

        System.assertEquals(case1.SuppliedEmail, 'customerservice@lendingpoint.com');


    }
    
     
    
    @isTest static void case_for_assing_contac_withoutApplication(){
       Contact c = LibraryTest.createContactTH();
       Contact c1 = [SELECT Firstname, Lastname, Email, BirthDate, Point_Code__c, ints__Social_security_Number__c, Time_at_current_Address__c, Phone, Mailingstreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Annual_Income__c, Loan_Amount__c, Employment_Start_date__c FROM Contact WHERE Firstname = 'David' LIMIT 1];
       c1.Email = 'Test258@MyCompany.COM';
       update c1;

       Case caseTest1 = new Case();
       caseTest1.Description = 'something to test test@lendingpoint.com test test test test258@mycompany.com test test test';
       caseTest1.SuppliedEmail = 'customerservice@lendingpoint.com';
       caseTest1.Subject = 'New Case Test1';
       insert caseTest1;

       caseTest1 = [SELECT Id, OwnerId, Description, ContactId, SuppliedEmail, Automatic_Assignment__c FROM Case WHERE Id = : caseTest1.id LIMIT 1];

       System.assertEquals(caseTest1.OwnerId, caseTest1.OwnerId );
    }


      @isTest static void case_for_decision_logic(){
       Contact c = LibraryTest.createContactTH();
       Contact c1 = [SELECT Firstname, Lastname, Email, BirthDate, Point_Code__c, ints__Social_security_Number__c, Time_at_current_Address__c, Phone, Mailingstreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Annual_Income__c, Loan_Amount__c, Employment_Start_date__c FROM Contact WHERE Firstname = 'David' LIMIT 1];
       c1.Email = 'Test258@MyCompany.COM';
       update c1;

       Case caseTest1 = new Case();
       caseTest1.Description = 'something to test test@lendingpoint.com test test test test258@mycompany.com test test test';
       caseTest1.SuppliedEmail = 'momeeoxo@lendingpoint.com';
       caseTest1.Subject = 'FW: Request Complete for TEST, Customer Id: 00QU000000eXRF1MAO, Request';
       insert caseTest1;

       caseTest1 = [SELECT Id, OwnerId, Description, ContactId, SuppliedEmail, Automatic_Assignment__c FROM Case WHERE Id = : caseTest1.id LIMIT 1];

       System.assertEquals(caseTest1.OwnerId, caseTest1.OwnerId );
    }
    
    @isTest static void caseExeption(){
                try{
        Contact c = LibraryTest.createContactTH();
        Contact c1 = [SELECT Firstname, Lastname, Email, BirthDate, Point_Code__c, ints__Social_security_Number__c, Time_at_current_Address__c, Phone, Mailingstreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Annual_Income__c, Loan_Amount__c, Employment_Start_date__c FROM Contact WHERE Firstname = 'David' LIMIT 1];
        c1.Email = 'Test258@MyCompany.COM';
        update c1;

        Case caseTest2 = new Case();
        caseTest2.Description = 'something to test test@lendingpoint.com test test test test258@mycompany.com test test test';
        caseTest2.SuppliedEmail = 'customerservice@lendingpoint.com';
        caseTest2.Subject = 'New Case Test1';
        insert caseTest2;

        caseTest2 = [SELECT Id, OwnerId, Description, ContactId, SuppliedEmail, Automatic_Assignment__c FROM Case WHERE Id = : caseTest2.id LIMIT 1];

        System.assertEquals(caseTest2.SuppliedEmail, 'customerservice@lendingpoint.com');

        }
        
        catch (Exception e) {
        System.debug('CaseAfterCreate Exception = ' + e);
        
        }
    }
    
    @isTest static void Case_Assign_for_CollectionGroupFrom1to29(){
        loan__Loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.loan__Oldest_Due_Date__c = Date.Today().adddays(-10);
        update cnt;

        Case case1 = new Case();
        case1.RecordTypeId = Label.RecordType_Collections_Id;
        case1.Subject = 'A collection case has been created for LAI-00000000';
        case1.Status = 'First Payment Default';
        case1.Origin = 'Collections';
        case1.CL_Contract__c = cnt.Id;
        insert case1;
        
        case1 = [SELECT Id, OwnerId, Description, ContactId, SuppliedEmail, Automatic_Assignment__c FROM Case WHERE Id = : case1.id LIMIT 1];

        System.assertEquals(case1.OwnerId, case1.OwnerId);
        
        
    }
    
   @isTest static void Case_Assign_for_OverdueDays_equals_Zero(){
       loan__Loan_Account__c cnt = LibraryTest.createContractTH();
       cnt.loan__Oldest_Due_Date__c = Date.Today().adddays(-3);
       update cnt;

       Case case1 = new Case();
       case1.RecordTypeId = Label.RecordType_Collections_Id;
       case1.Subject = 'A collection case has been created for LAI-00000000';
       case1.Status = 'Auto Second Presentment';
       case1.Origin = 'Collections';
       case1.CL_Contract__c = cnt.Id;
       insert case1;
       
       case1 = [SELECT Id, OwnerId, Description, ContactId, SuppliedEmail, Automatic_Assignment__c FROM Case WHERE Id = : case1.id LIMIT 1];

    }
    
   @isTest static void Case_Assign_for_OverdueDays_Notequals_Zero(){
       loan__Loan_Account__c cnt = LibraryTest.createContractTH();
       cnt.loan__Oldest_Due_Date__c = Date.Today().adddays(+3);
       update cnt;

       Case case1 = new Case();
       case1.RecordTypeId = Label.RecordType_Collections_Id;
       case1.Subject = 'A collection case has been created for LAI-00000000';
       case1.Status = 'Auto Second Presentment';
       case1.Origin = 'Collections';
       case1.CL_Contract__c = cnt.Id;
       insert case1;
       
       case1 = [SELECT Id, OwnerId, Description, ContactId, SuppliedEmail, Automatic_Assignment__c FROM Case WHERE Id = : case1.id LIMIT 1];
       
       System.assertNotEquals(case1.OwnerId, null);
        
        
    }
    
    @isTest static void caseLMS(){
        Opportunity app = LibraryTest.createApplicationTH();
        case case1 = new Case();
        case1.Subject = 'LPMS Welcome Call';
        case1.Opportunity__c = app.Id;
        insert case1;
    }
    
    @isTest static void statusChangeBasedOnDMCPlan(){
        loan__Loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.DMC__c='test';
        update cnt;
        
        case case1 = new Case();
        case1.RecordTypeId=Label.RecordType_Collections_Id;
        case1.Description = 'test case';
        case1.Subject = 'Test';
        case1.Opportunity__c = cnt.Opportunity__c;
        case1.contactId=cnt.loan__Contact__c;
        case1.CL_contract__C=cnt.id;
        insert case1;
    }

    @isTest static void caseEzVerifyFS(){
        Opportunity app = LibraryTest.createApplicationTH();
        app = [SELECT Id, name FROM Opportunity WHERE Id=:app.Id];
        
        case case1 = new Case();
        case1.Subject = 'Credit Review - Opportunity # ' + app.name;
        case1.Opportunity__c = app.Id;
        case1.OwnerId = '005U0000003pdKWIAY'; //User on Salesforce: HAL
        insert case1;

        case1 = [SELECT Id, OwnerId, Automatic_Assignment__c, Opportunity__r.name, subject FROM Case WHERE Id = : case1.id LIMIT 1];
        system.assertNotEquals(null, case1.OwnerId);
        system.assertNotEquals('005U0000003pdKWIAY', case1.OwnerId);
        system.assertEquals(true, case1.Automatic_Assignment__c);
    }
    
    @isTest static void case_ChangeStatusToDocReceived() {
        
        Group grp = [SELECT Id, Name FROM Group where Name = 'Data Entry']; 
        GroupMember grpm = [SELECT Id, GroupId, UserOrGroupId FROM GroupMember where GroupId =: grp.Id LIMIT 1];
        User usr = [Select Id, Name, IsActive FROM user where Id =: grpm.UserOrGroupId LIMIT 1];

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Incomplete Package';
        app.DE_Assigned_To_Doc__c = usr.Name;
        app.Name='APP-0001111111';
        update app;
        
        Contact c1 = [SELECT id, name, Email FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c1.Email = 'Test258@MyCompany.COM';
        update c1;
        
        Case c = new Case();
        c.Description = 'something to test Test258@MyCompany.COM test test test test docs@lendingpoint.com test';
        c.SuppliedEmail = 'docs@lendingpoint.com';
        c.Subject = 'APP-0001111111 Documents Have Been Uploaded from Web';
        c.Opportunity__c = app.Id;
        
        insert c;
        
        Opportunity oppstatus = [Select Id, name, status__c FROM Opportunity where Id =: app.Id];
        
        system.assertEquals('Documents Received', oppstatus.Status__c);
    }
    
    @isTest static void Closed_case_LoginNotVerified() {
        
        Contact con = LibraryTest.createContactTH();
        
        Case cs = new Case();
        cs.ContactId = con.Id;
        cs.Description = 'Request Results Company LendingPoint, LLC Name Katelyn Wietzema Customer Identifier 0034O00002V9i2MQAR Profile LendingPoint Live Request Code NJ4K8J Verification Status LOGIN NOT VERIFIED ';
        cs.SuppliedEmail = 'docs@lendingpoint.com';
        cs.Subject = 'FW: Request Complete for Katelyn Wietzema , Customer Id: 0034O00002V9i2MQAR, Request Code: NJ4K8J';
        insert cs;
        
        cs = [Select Id, CaseNumber, Status FROM Case WHERE Id =: cs.id];
        
        System.assertEquals('Closed', cs.Status);
        
        
    }
    

}