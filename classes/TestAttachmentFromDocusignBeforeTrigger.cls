/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAttachmentFromDocusignBeforeTrigger {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Opportunity app = TestHelper.createApplication();

        dsfs__DocuSign_Status__c docStat = new dsfs__DocuSign_Status__c();
        docStat.dsfs__Subject__c = 'test Subject';
        insert docStat;
        
    dsfs__DocuSign_Recipient_Status__c docRe = new dsfs__DocuSign_Recipient_Status__c();
    docRe.dsfs__Parent_Status_Record__c = docStat.Id;
    docRe.dsfs__Contact__c = app.contact__c;
    docRe.dsfs__DocuSign_Recipient_Id__c = '5495B485-214B-4D83-B319-E14571F0D793';
    //docRe.dsfs__DocuSign_Recipient_Email__c = 'daniel.almazan@jazzlabsllc.com';
    insert docRe;        
    System.debug('Contact for Application and insert = ' + docRe.dsfs__Contact__c);
    
    Attachment att = new Attachment();
    att.ParentId = docStat.Id;
    att.Name = 'TEST DOC';
    att.Body = Blob.valueOf('Unit Test Attachment Body');
    System.debug('ID before Docusign Trigger Insert = ' + att.ParentId);
    insert att;
    
    List<Attachment> atts = [Select Id, ParentID from Attachment order by createdDate desc ];
    System.debug('Attachments in systems = ' + atts.size());
    System.debug('ID After Docusign Trigger Insert = ' + atts.get(0).ParentId);
    
    
    
    }
}