@IsTest
public class TestEmailUtility {  
   

  @IsTest public static void sendEmailDetailNew(){
       Java_API_Settings__c JS =  new Java_API_Settings__c(Name='JavaSettings',Mail_Receive_EndPoint__c = 'http://testapi.lendingpoint.com/CustomerPortalAPI/api/mail/receive');
       insert JS;
      String emails = '{"contactId": "456","from": "sample@sample.com","to": "sample2@sample.com","subject": "Sample Subject","body": "Sample Body","date": "10/22/2015 10:15:00 AM"}';
      EmailUtility.sendEmailDetailNew('Sample Subject',emails);
  }
  
  @IsTest public static void ExceptionHandle(){
       Java_API_Settings__c JS =  new Java_API_Settings__c(Name='JavaSettings',Mail_Receive_EndPoint__c = 'http://testapi.lendingpoint.com/CustomerPortalAPI/api/mail/receive');
       insert JS;
       EmailUtility.sendEmailDetailNew('Sample Subject','test eml');
  }
  
  
  @IsTest public static void hitMailRecieveAPI(){
  String emailDetails= '{"contactId": "456","from": "sample@sample.com","to": "sample2@sample.com","subject": "Sample Subject","body": "Sample Body","date": "10/22/2015 10:15:00 AM"}';
  EmailUtility.hitMailRecieveAPI('1232131',emailDetails,'http://testapi.lendingpoint.com/CustomerPortalAPI/api/mail/receive','testing');
  }
  
  @IsTest public static void hitAccessTokenAPI(){
  Java_API_Settings__c JS =  new Java_API_Settings__c(Name='JavaSettings',Mail_Receive_EndPoint__c = 'http://testapi.lendingpoint.com/CustomerPortalAPI/api/mail/receive',clientId__c='testclient', clientSecret__c ='testing',Access_Token_EndPoint__c ='http://testapi.lendingpoint.com/CustomerPortalAPI/oauth/token?grant_type=password',Username__c = 'test',Password__c= 'test123');
   insert JS;
  
  EmailUtility.hitAccessTokenAPI(JS);
  }
  
  
   @IsTest public static void getLoggingRecord(){
     contact cntct= new contact();
     cntct.Lastname='Test';
     cntct.Annual_Income__c =10000;
     insert cntct;
     EmailUtility.getLoggingRecord('DummytestService','http://test.sfdc.com','success', cntct.id);
  }
  
  @IsTest public static void getLoggingRecordAcc(){
     Account acc = new Account ();
     acc.name='Test';
     insert acc;
     EmailUtility.getLoggingRecordAcc('DummytestService','http://test.sfdc.com','success', acc.id);
  }
  
  
   @IsTest public static void getAccessTokenVal(){
     String xmlString = '<access_token>12345</access_token>';
      XmlStreamReader xsr = new XmlStreamReader(xmlString);
     EmailUtility.getAccessTokenVal(xsr);
  }
  @IsTest public static void getAccessTokenValNull(){
     String xmlString = '<access_token></access_token>';
      XmlStreamReader xsr = new XmlStreamReader(xmlString);
     EmailUtility.getAccessTokenVal(xsr);
  }
  
    
   
}