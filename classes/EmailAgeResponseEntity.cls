global class EmailAgeResponseEntity {
    public EmailAgeResponseQuery query;
    public EmailAgeResponseStatus responseStatus;
}