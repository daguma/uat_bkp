@isTest(SeeAllData=false)
public class MuleServices_Test {
    
    static testMethod void testGetAccountLocationsById(){ 
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.Location_Min_Limit__c = 0;
        amcoSetting.Location_Max_Limit__c = 10;
        insert amcoSetting;
        
        Account parentAccount = new Account();
        parentAccount.Name = 'Test Parent Account';
        parentAccount.Type = 'Merchant';
        parentAccount.Active_Partner__c = True;
        parentAccount.Cobranding_Name__c = 'Test Name1';
        parentAccount.Partner_Sub_Source_Key__c = 'TestName1';
        insert parentAccount;
        
        Account childAccount = new Account();
        childAccount.Name = 'Test Child Account';
        childAccount.Type = 'Merchant';
        childAccount.Active_Partner__c = True;
        childAccount.ParentId = parentAccount.Id;
        childAccount.Cobranding_Name__c = 'Test Name2';
        childAccount.Partner_Sub_Source_Key__c = 'TestName2';
        insert childAccount;
        
        Account parentAccountWithoutChild = new Account();
        parentAccountWithoutChild.Name = 'Test Parent Account';
        parentAccountWithoutChild.Type = 'Merchant';
        parentAccountWithoutChild.Active_Partner__c = True;
        parentAccountWithoutChild.Cobranding_Name__c = 'Test Name';
        parentAccountWithoutChild.Partner_Sub_Source_Key__c = 'TestName';
        insert parentAccountWithoutChild;
        
        /*LP_Custom__c cuSettings = new LP_Custom__c();
cuSettings.SetupOwnerId = UserInfo.getOrganizationId();
cuSettings.AAMCO_ID__c = parentAccount.Id;
cuSettings.Days_Before_New_App_Submission_Retry__c = 10;
insert cuSettings;*/
        Account  acc = [select id from account where Type='Merchant' AND Active_partner__c = True Limit 1];
        Account  acc1 = [select id from account Limit 1];
        
        Test.StartTest();
        String requestJson = '{"accountId": "'+acc.Id+'","lowerLimit":"0","upperLimit":"10"}';
        String responseJson = MuleServices.getAccountLocationsById(requestJson);
        String responseJson1 = MuleServices.getAccountLocationsById('{"accountId": "'+acc1.id+'"}');
        String responseJson2 = MuleServices.getAccountLocationsById('{"accountId": "'+acc.id+'"}');
        String responseJson3 = MuleServices.getAccountLocationsById('{"accountId": "'+acc.Id+'","lowerLimit":"0","upperLimit":"10","searchBy":"am"}');
        AccountLocationsWrapper objWrapper = (AccountLocationsWrapper)JSON.deserialize(responseJson, AccountLocationsWrapper.class);
        AccountLocationsWrapper objWrapper1 = (AccountLocationsWrapper)JSON.deserialize(responseJson1, AccountLocationsWrapper.class);
        AccountLocationsWrapper objWrapper2 = (AccountLocationsWrapper)JSON.deserialize(responseJson2, AccountLocationsWrapper.class);
        AccountLocationsWrapper objWrapper3 = (AccountLocationsWrapper)JSON.deserialize(responseJson3, AccountLocationsWrapper.class);
        AccountLocationsWrapper objWrapper4 = null;
    
        MuleServices.getAccountLocationsById('{"accountId": ""}');
        //System.assertEquals('200', objWrapper.status);
        //System.assertEquals('Locations found Successfully', objWrapper.message);
        
        //Updating custom setting field AAMCO_ID__c, with Account which have no child Account in heirarchy  
        //cuSettings.AAMCO_ID__c = parentAccountWithoutChild.Id;
        //update cuSettings;
        
        requestJson = '{"accountId": "'+parentAccount.Id+'"}';
        responseJson = MuleServices.getAccountLocationsById(requestJson);
        objWrapper = (AccountLocationsWrapper)JSON.deserialize(responseJson, AccountLocationsWrapper.class);
        //System.assertEquals('Err', objWrapper.status);
        //System.assertEquals('No Locations found', objWrapper.message);
        Test.stopTest();
    }
    
    
    static testMethod void testGenerateNote(){ 
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'Qualification';
        insert opp;
        
        Contact con = new Contact();
        con.LastName = 'Test Contact';
        insert con;
        
        Test.StartTest();
        //Test when Application and Contact Id's are present
        String requestJson = '{"appId": "'+opp.Id+'", "contactId": "'+con.Id+'", "consent": "test", "fullName": "testFullName"}';
        String responseJson = MuleServices.GenerateNoteMethod(requestJson);
        Map<string, string> responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        //System.assertEquals('200', responseMap.get('status'));
        //System.assertEquals('Note Inserted Successfully', responseMap.get('message'));
        
        //Test when Application and Contact Id both are not present
        requestJson = '{"appId": "", "contactId": "", "consent": "test", "fullName": "testFullName"}';
        responseJson = MuleServices.GenerateNoteMethod(requestJson);
        responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        //System.assertEquals('Err', responseMap.get('status'));
        //System.assertEquals('Application Id and Contact Id are null', responseMap.get('message'));
        
        //Test when Application is blank and Contact Id not blank
        requestJson = '{"appId": "", "contactId": "'+con.Id+'", "consent": "test", "fullName": "testFullName"}';
        responseJson = MuleServices.GenerateNoteMethod(requestJson);
        responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        //System.assertEquals('200', responseMap.get('status'));
        //System.assertEquals('Note Inserted Successfully', responseMap.get('message'));
        requestJson = '{"appId": "'+opp.Id+'", "contactId": "", "consent": "", "fullName": ""}';
        responseJson = MuleServices.GenerateNoteMethod(requestJson);
        //responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class);
        Test.stopTest();
    }
    
    static testMethod void testReturnsingleOpp(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Child Account';
        objAccount.Type ='Merchant';
        objAccount.Active_Partner__c = True;
        objAccount.Cobranding_Name__c='Test Name';        
        insert objAccount;
        
        Contact con = New Contact();
        con.firstName = 'jony';
        con.lastName = 'Paul';
        con.createddate = Date.today();
        con.Lead_Sub_Source__c = 'Aamco';        
        insert con;
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id;
        opp.Name = 'Test OppName';
        opp.CloseDate = Date.today();
        opp.StageName = 'Qualification';
        opp.Partner_Account__c = objAccount.Id;
        opp.status__c = 'Funded';
        insert opp;
        
        IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
        Insert idology; 
        
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credited Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        
        Test.StartTest();
        //Test when Opp status and FilterBy__c both are same
        String requestJson = '{"opportunityId": "'+opp.id+'"}';
        String responseJson = MuleServices.returnSingleRecord(requestJson);
        String responseJsonn = MuleServices.returnSingleRecord('{"opportunityId": ""}');
        String requestJsonContact = '{"contactId": "'+con.id+'"}';
        String responseJsonContact = MuleServices.returnSingleRecord(requestJsonContact);
        String responseJsonContactt = MuleServices.returnSingleRecord('{"contactId": ""}');
        //System.assertEquals(true, requestJsonContact.contains('200'));
        //System.assertEquals(true, responseJsonContact.contains('Success'));        
        Test.stopTest();
    }
    
    static testMethod void testReturnAamcoOpp(){ 
        Account objAccount = New Account(Name = 'Test Child Account',My_Apps_Only__c=false,Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco', Cobranding_Name__c='Test Name');        
        insert objAccount;
        
        Account objAccount1 = New Account(Name = 'Test Child Account',My_Apps_Only__c=True,Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco1', Cobranding_Name__c='Test Name');        
        insert objAccount1;
        
        Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',Partner_Account__c = objAccount.Id,status__c = 'Credit Qualified');
        insert opp;
        Opportunity opp1 = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',Partner_Account__c = objAccount1.Id,status__c = 'Credit Qualified');
        insert opp1;
        
        Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
        Insert con;
        IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
        Insert idology;        
        
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credited Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        
        Aamco_Status__c cuSettings = new Aamco_Status__c();
        cuSettings.Name = 'funded';       
        cuSettings.FilterBy__c = 'Funded';
        insert cuSettings;
        
        Aamco_Status__c cuSettings1 = new Aamco_Status__c();
        cuSettings1.Name = 'expired';       
        cuSettings1.FilterBy__c = 'Expired';
        insert cuSettings1;
        
        Aamco_Status__c cuSettings2 = new Aamco_Status__c();
        cuSettings2.Name = 'fundedExternally';       
        cuSettings2.FilterBy__c = 'Funded Externally';
        insert cuSettings2;
        
        Aamco_Status__c cuSettings3 = new Aamco_Status__c();
        cuSettings3.Name = 'declined';       
        cuSettings3.FilterBy__c = 'Declined';
        insert cuSettings3;
        
        Aamco_Status__c cuSettings4 = new Aamco_Status__c();
        cuSettings4.Name = 'canceled';       
        cuSettings4.FilterBy__c = 'Canceled';
        insert cuSettings4;
        
        Aamco_Status__c cuSettings5 = new Aamco_Status__c();
        cuSettings5.Name = 'loanApproved';       
        cuSettings5.FilterBy__c = 'Loan Approved';
        insert cuSettings5;
        
        Aamco_Status__c cuSettings6 = new Aamco_Status__c();
        cuSettings6.Name = 'pendingApproval';       
        cuSettings6.FilterBy__c = 'Pending Approval';
        insert cuSettings6;
        
        Aamco_Status__c cuSettings7 = new Aamco_Status__c();
        cuSettings7.Name = 'readyToFund';       
        cuSettings7.FilterBy__c = 'Ready To Fund';
        insert cuSettings7;
        
        
        Test.StartTest();
        MuleServices.returnAamcoOpp('{"accountId": ""}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"funded"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"declined","searchBy":"Test","sortBy":"daysuntilexpire","sortType":"desc"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"expired"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"loanApproved"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"pendingApproval"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"readyToFund"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"approvedPendingFunding"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"canceled"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"awaitingFulfillment"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"declined"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","lowerLimit":"1","upperLimit":"10","filterBy":"expired,canceled,funded,applicationSent,pendingApproval,loanApproved,readyToFund","searchBy":"Test","sortBy":"daysuntilexpire","sortType":"desc"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","lowerLimit":"1","upperLimit":"10","filterBy":"funded"}');
        MuleServices.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","lowerLimit":"1","upperLimit":"10","filterBy":"applicationSent"}');
        
        //Test when Opp status and FilterBy__c are different
        /*cuSettings.Name = 'Credit Qualified';
cuSettings.FilterBy__c = 'Credit Qualified';
update cuSettings;      
*/
        
        Test.stopTest();
    }
    
    static testMethod void testgetCount(){        
        Aamco_Status__c cuSettings = new Aamco_Status__c();
        cuSettings.Name = 'funded';       
        cuSettings.FilterBy__c = 'Funded';
        insert cuSettings;
        
        Aamco_Status__c cuSettings1 = new Aamco_Status__c();
        cuSettings1.Name = 'expired';       
        cuSettings1.FilterBy__c = 'Expired';
        insert cuSettings1;
        
        Aamco_Status__c cuSettings2 = new Aamco_Status__c();
        cuSettings2.Name = 'fundedExternally';       
        cuSettings2.FilterBy__c = 'Funded Externally';
        insert cuSettings2;
        
        Aamco_Status__c cuSettings3 = new Aamco_Status__c();
        cuSettings3.Name = 'declined';       
        cuSettings3.FilterBy__c = 'Declined';
        insert cuSettings3;
        Aamco_Status__c cuSettings4 = new Aamco_Status__c();
        cuSettings4.Name = 'canceled';       
        cuSettings4.FilterBy__c = 'Canceled';
        insert cuSettings4;
        
        Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',My_Apps_Only__c=True,Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco', Cobranding_Name__c='Test Name');        
        insert objAccount; 
        Account objAccount1 = New Account(Name = 'Test Child Account',Type='Merchant',My_Apps_Only__c=False,Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco1', Cobranding_Name__c='Test Name1');        
        insert objAccount1; 
        
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Ready To Fund,Canceled,Funded,Credited Qualified,Pending Approval,Loan Approved,Expired,FundedExternally';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
        Insert con;
        //Map<String,Aamco_Status__c> custom_status = Aamco_Status__c.getall(); 
         
        
       
        
        IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
        Insert idology; 
        
        Test.startTest();       
        MuleServices.getCount('{"accountId": "'+objAccount.Id+'"}');
        MuleServices.getCount('{"accountId": "'+objAccount.Id+'","myApp":"True","searchBy":"test"}');
         MuleServices.getCount('{"accountId": "'+objAccount1.Id+'","myApp":"false","searchBy":"test"}');
        MuleServices.getCount('{"accountId": ""}');
        Test.stopTest();
    }
    
    static testMethod void AccountLocationsWrapperTest(){
        Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco',phone = '7852669444',Source__c='Aamco', Cobranding_Name__c='Test Name');        
        insert objAccount;  
        
        AccountLocationsWrapper.Locations loc = New AccountLocationsWrapper.Locations();
        loc.accountId = objAccount.Id;
        loc.accountName = objAccount.Name;
        loc.phone = '7852669444';
        loc.source = 'Aamco';
        loc.subProvider = 'Aamco';
        loc.subSource = 'Aamco';
        
        AccountLocationsWrapper l = new AccountLocationsWrapper();
        l.addLocation(objAccount);
    }
    
    static testMethod void statusReturnTest(){
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credited Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        
        Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco', Cobranding_Name__c = 'Test Name');        
        insert objAccount;
        Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',Partner_Account__c = objAccount.Id,status__c = 'Credit Qualified');
        insert opp;
        Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
        Insert con;
        IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
        Insert idology;
        set<id> setidology = New set<id>();
        setidology.add(Con.id);
        MuleServices.returnStatus(opp,setidology);
    }
    static testMethod void reassignLocation(){
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credit Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        Merchant_Portal_Welcome_Email_Mapping__c mpwm = new Merchant_Portal_Welcome_Email_Mapping__c();
        mpwm.Name = 'reassignmentTemplate';
        mpwm.Email_Template__c = 'test_email';
        insert mpwm;
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
        System.RunAs(usr) {
            test.startTest();
            EmailTemplate eTemplate=new EmailTemplate();
            eTemplate.Name='test_email';
            eTemplate.Body='Hello';
            eTemplate.Subject='Reassign Notification';
            eTemplate.HtmlValue='<html><body>Hello</body></html>';
            eTemplate.DeveloperName='test_email';
            eTemplate.FolderId=UserInfo.getUserId();
            eTemplate.TemplateType='Custom';
            insert eTemplate;
            Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco', Cobranding_Name__c='Test Name', Threshold_Loan_Amount__c = 0);        
            insert objAccount;
            Account objAccount1 = New Account(Name = 'Test Child Account2', RMC_Email__c='test.re@gmail.com', Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco2', Cobranding_Name__c='Test Name');        
            insert objAccount1;
            
            Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
            Insert con;
            Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',Partner_Account__c = objAccount.Id,status__c = 'Credit Qualified', Contact__c = con.id, Amount = 10000, Fee__c = 10000);
            insert opp;
            IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
            Insert idology;
            set<id> setidology = New set<id>();
            setidology.add(Con.id);
            
            MuleServices.reAssignLocation('{"assignedLocationId": "'+objAccount1.id+'", "opportunityId": "'+opp.Id+'", "contactId": "'+con.Id+'"}');
            test.stopTest();
        }
    }
    static testMethod void reassignLocationOnContactId(){
       
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credit Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        Merchant_Portal_Welcome_Email_Mapping__c mpwm = new Merchant_Portal_Welcome_Email_Mapping__c();
        mpwm.Name = 'reassignmentTemplate';
        mpwm.Email_Template__c = 'Reassignment_Template';
        mpwm.Sender_Email__c = 'test@gmail.com';
        insert mpwm;
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
        System.RunAs(usr) {
            test.startTest();
            EmailTemplate eTemplate=new EmailTemplate();
            eTemplate.Name='test_email';
            eTemplate.Body='Hello';
            eTemplate.Subject='Reassign Notification';
            eTemplate.HtmlValue='<html><body>Hello</body></html>';
            eTemplate.DeveloperName='Reassignment_Template';
            eTemplate.FolderId=UserInfo.getUserId();
            eTemplate.TemplateType='Custom';
            insert eTemplate;
            Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco', Cobranding_Name__c='Test Name');        
            insert objAccount;
            Account objAccount1 = New Account(Name = 'Test Child Account2', RMC_Email__c='test.re@gmail.com', Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco2', Cobranding_Name__c='Test Name');        
            insert objAccount1;
            
            Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
            Insert con;
            Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(), StageName = 'Qualification', Partner_Account__c = objAccount.Id, status__c = 'Credit Qualified', Contact__c = con.id);
            insert opp;
            IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
            Insert idology;
            set<id> setidology = New set<id>();
            setidology.add(Con.id);
            
            MuleServices.reAssignLocation('{"opportunityId": "", "assignedLocationId": "'+objAccount1.id+'", "contactId": "'+con.Id+'"}');
            
            test.stopTest();
        }        
        
    }
    
    static testmethod void testgetAccountsForReassignment(){
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        
        amcoSetting.SearchBy_Min_Limit__c = '0';
        amcoSetting.SearchBy_Max_Limit__c = '10';
        insert amcoSetting; 
        
        Account acobj = New Account(Name = 'grandParentAccount',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco', Cobranding_Name__c='Test Name');        
        insert acobj;
        Account acobj1 = New Account(parentid = acobj.id, Name = 'parent Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco1', Cobranding_Name__c='Test Name');        
        insert acobj1;
        Account acobj2 = New Account(parentid = acobj1.id , Name = 'Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco2', Cobranding_Name__c='Test Name');        
        insert acobj2;  
        
        Test.StartTest();
        MuleServices.getAccountsForReassignment('{"accountId":" "}');
        MuleServices.getAccountsForReassignment('{"accountId": "'+acobj.Id+'"}');
        MuleServices.getAccountsForReassignment('{"accountId": "'+acobj.Id+'","lowerLimit":"2001","upperLimit":"2010"}');
        MuleServices.getAccountsForReassignment('{"accountId": "'+acobj2.Id+'"}');
        MuleServices.getAccountsForReassignment('{"accountId": "'+acobj.Id+'","lowerLimit":"5","upperLimit":"10"}');
        MuleServices.getAccountsForReassignment('{"accountId": "'+acobj.Id+'","lowerLimit":"5","upperLimit":"10","searchBy":"aamco"}');
        Test.stopTest();    
        
    }
    
    static testmethod void testgetPermissionsByAccountid(){
        MuleServices.getPermissionsByAccountid('{"accountId":""}');     
        Account acc = new Account(Name = 'testAccount',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco', 
                                     Cobranding_Name__c='Test Name',Start_Application__c = True,
                                     Payment_Calculator__c = True,
                                     Reporting__c = True,
                                     Marketing_Store_Merchant_Center__c = True
                                     ,DrillDown__c=True,Enable_Assignment__c = True);
        insert acc;
        Account acc1 = new Account(Name = 'testAccount',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco1', 
                                     Cobranding_Name__c='Test Name',Start_Application__c = false,
                                     Payment_Calculator__c = false,
                                     Reporting__c = false,
                                     Marketing_Store_Merchant_Center__c = false
                                     ,DrillDown__c=false,Enable_Assignment__c = false);
        insert acc1;
         Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',status__c = 'Credit Qualified');
        insert opp;
        Test.StartTest();
        MuleServices.getPermissionsByAccountid('{"accountId":"'+opp.id+'"}');
        MuleServices.getPermissionsByAccountid('{"accountId":"'+acc.id+'"}');
        MuleServices.getPermissionsByAccountid('{"accountId":"'+acc1.id+'"}');
               
        Test.StopTest();
    }
     static testMethod void testadjustFundingAmount(){ 
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'Qualification';
        opp.Is_Selected_Offer__c=false;
        insert opp;
        
       
        Test.StartTest();
        //Test when loan Amount is 0
        String requestJson = '{"applicationId": "'+opp.Id+'", "loanAmount": "0"}';
        String responseJson = MuleServices.adjustFundingAmount(requestJson);
        Map<string, string> responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        System.assertEquals('ver', responseMap.get('status'));
        System.assertEquals('loan Amount cannot be blank or zero.', responseMap.get('message'));
        
         //Test when application id is missing
        requestJson = '{"loanAmount": "1000"}';
        responseJson = MuleServices.adjustFundingAmount(requestJson);
        responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        System.assertEquals('ver', responseMap.get('status'));
        System.assertEquals('application Id is missing.', responseMap.get('message'));
        
        
        //Test when loan Amount is greater than 0 and offer is not selected
        requestJson = '{"applicationId": "'+opp.Id+'", "loanAmount": "1000"}';
        responseJson = MuleServices.adjustFundingAmount(requestJson);
        responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        System.assertEquals('ver', responseMap.get('status'));
        System.assertEquals('A contract should be signed and an offer must be selected for the amount of the contract.', responseMap.get('message'));
        
        
        opp.Payment_Frequency__c='MONTHLY';
        
        update opp;
        
        //Test when loan Amount is greater than 0 and offer is selected
        requestJson = '{"applicationId": "'+opp.Id+'", "loanAmount": "1000"}';
        responseJson = MuleServices.adjustFundingAmount(requestJson);
        responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        System.assertEquals('err', responseMap.get('status'));
        System.assertEquals('Attempt to de-reference a null object', responseMap.get('message'));
        
        Opportunity sampleApp = TestHelper.createApplication();
        Opportunity sapp=new Opportunity(Id=sampleApp.Id,Status__c='Credit Approved - No Contact Allowed', Deal_is_Ready_to_Fund__c = True);
        update sapp;
        
        Offer__c offer = new Offer__c();
        offer.Loan_Amount__c = 1000;
        offer.Payment_Amount__c = 100;
        offer.Opportunity__c = sapp.id;
        offer.IsSelected__c = true;
        offer.Fee_Percent__c = 0.2;
        offer.Selected_Date__c = Date.Today().addDays(-1);
        offer.Installments__c = 12;
        offer.Term__c = 12;
        offer.APR__c = 0.1;
        offer.Fee__c = 0.2;
        offer.Repayment_Frequency__c = 'Monthly';
        offer.Total_Loan_Amount__C = 1200;
        offer.NeedsApproval__c = false;
        offer.AON_Fee__c = 0;
        offer.AON_Payment_Amount__c = 0;
        offer.Lead_Sub_Source__c='test';
        insert offer;
        
        List<Offer__c> offers =  [SELECT Total_Loan_Amount__C, Loan_Amount__c, Payment_Amount__c, IsSelected__c, Selected_date__c, SelectedByID__c,
                              Fee_Percent__c, Installments__c, Term__c, APR__c, Fee__c, Repayment_Frequency__c,
                              NeedsApproval__c, IsCustom__c, AON_Fee__c, AON_Payment_Amount__c
                              FROM Offer__c WHERE id = : offer.id];
                              
        OfferCtrlUtil.SelectOfferUtil(offer.Id, offers, sapp);
         //Test when loan Amount is greter than 0 and offer is selected
        requestJson = '{"applicationId": "'+sapp.Id+'", "loanAmount": "1000"}';
        responseJson = MuleServices.adjustFundingAmount(requestJson);
        responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        //System.assertEquals('200', responseMap.get('status'));
        //System.assertEquals('opportunity updated successfully.', responseMap.get('message'));
        
        Test.stopTest();
    }
   
  
   /* static testMethod void sendMail(){

        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credit Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        Merchant_Portal_Welcome_Email_Mapping__c mpwm = new Merchant_Portal_Welcome_Email_Mapping__c();
        mpwm.Name = 'reassignmentTemplate';
        mpwm.Email_Template__c = 'Send_Reassignment_Notification_To_Merchant';
        mpwm.Sender_Email__c = 'test@gmail.com';
        insert mpwm;
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
        System.RunAs(usr) {
            test.startTest();
            EmailTemplate eTemplate=new EmailTemplate();
            eTemplate.Name='test_email';
            eTemplate.Body='Hello';
            eTemplate.Subject='Reassign Notification';
            eTemplate.HtmlValue='<html><body>Hello</body></html>';
            eTemplate.DeveloperName='Send_Reassignment_Notification_To_Merchant';
            eTemplate.FolderId=UserInfo.getUserId();
            eTemplate.TemplateType='Custom';
            insert eTemplate;
            Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',RMC_Email__c='test.re@gmail.com',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco', Cobranding_Name__c='Test Name');        
            insert objAccount;
            Account objAccount1 = New Account(Name = 'Test Child Account2', RMC_Email__c='test.re@gmail.com', Type='Merchant',Active_Partner__c=True,Partner_Sub_Source_Key__c='Aamco2', Cobranding_Name__c='Test Name');        
            insert objAccount1;
            
            Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
            Insert con;
            Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(), StageName = 'Qualification', Partner_Account__c = objAccount.Id, status__c = 'Credit Qualified', Contact__c = con.id);
            insert opp;
            IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
            Insert idology;
            set<id> setidology = New set<id>();
            setidology.add(Con.id);
            
            MuleServices.reAssignLocation('{"opportunityId": "' + opp.id + '", "assignedLocationId": "'+objAccount1.id+'", "contactId": "'+con.Id+'"}');
            
            test.stopTest();
        }
    } */

}