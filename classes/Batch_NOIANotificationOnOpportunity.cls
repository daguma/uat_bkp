global class Batch_NOIANotificationOnOpportunity  implements Database.Batchable<sObject>, Database.stateful, Schedulable{
    // Local Variables 
    Public String status = 'Incomplete Package';
    public String optytype = 'Refinance';
    public string query ;
    
    string AAN_TEMPLATE = 'NOIA Notifications';
    Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    Integer iRecsProcessed = 0;
    
    global void execute (SchedulableContext sc) {
        system.debug('cuSettings    ' +cuSettings);
        Integer iNumReportsToProcess = cuSettings != null && cuSettings.AAN_To_Send_Per_Batch__c == null ? 9 : cuSettings.AAN_To_Send_Per_Batch__c.intValue();
        
        Batch_NOIANotificationOnOpportunity job = new Batch_NOIANotificationOnOpportunity();
        System.debug('Starting Batch_NOIANotificationOnOpportunity at ' + Date.today());
        Database.executeBatch(job, iNumReportsToProcess);
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        Date dt = date.Today().addDays(-19);
        Date cutOffDate = date.valueof(Label.RefiNoticesCutOffDate);
        system.debug('-->'+dt);  
        query = 'SELECT id,name,Status__c,Contact_Email__c,Partner_Account__r.Name,Contact__c,Status_Modified_Date_Time__c,Status_Modified_Date__c,Is_Finwise__c,Permission_to_Get_Hard_Pull__c,KeyFactors__c  FROM Opportunity WHERE Status__c =:status AND Type =:optytype and Partner_Account__r.Name =:optytype AND Is_Finwise__c = True and NOIA_Sent_Date__c = null and createdDate >= : cutOffDate and Status_Modified_Date__c <=:dt';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> opportunityListToProcess){
        system.debug('===opportunityListToProcess======'+opportunityListToProcess);
        Set<Id> appIds = new Set<Id>();
        if(!opportunityListToProcess.isEmpty()){
            for(opportunity opp: opportunityListToProcess){
                appIds.add(opp.id); 
            } 
            try{
                if(appIds != null && appIds.size() > 0){
                    System.debug('Apps to Send NOIA for = ' + appIds.size());
                    NOIANotification(appIds);
                }
            } catch(exception ex){
                System.debug('Exception:'+ ex.getMessage());
                WebToSFDC.notifyDev('Batch_NOIANotificationOnOpportunity.QueryLocator ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
            }           
            
        }
    }
    
    public void NOIANotification(Set<Id> appIds) {
        try {
            List<Outbound_Emails_Temp__c> AppEmails = new List<Outbound_Emails_Temp__c>();
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            List<Opportunity> updatedApps = new List<Opportunity>();
            LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
            List<Note> notesEmails = new List<Note>();
            
            Map< Id, Opportunity> apps;
            String plainBody = '';
            String HtmlValue = '', Subject = '', tName = '';
            Contact theContact = null;
            list<string> sendTo;
            tName = AAN_TEMPLATE ;
            EmailTemplate eTemplate = [select id, name,body,Subject,HtmlValue from EmailTemplate where name = :tName];
            
            apps = new Map<Id, Opportunity>(
                [SELECT ID,CreditPull_Date__c, Lead_Sub_Source__c, LexisNexisDeclined__c,  Contact__r.Email, Contact__r.Id,Contact_Email__c,Status_Modified_Date__c,Permission_to_Get_Hard_Pull__c, 
                 Contact__r.Name,Contact__r.FirstName, Contact__r.MailingStreet, Contact__r.MailingCity, Is_Feb__C,
                 Contact__r.MailingState, Contact__r.MailingPostalCode,   CreatedDate, 
                 Status__c, Is_Finwise__c, Reason_of_Opportunity_Status__c, Contact__c, AAN_Sent_Date__c, 
                 KeyFactors__c, Clarity_Status__c, Partner_Account__r.Disable_AAN__c, Partner_Account__r.Name, 
                 Partner_Account__r.AAN_Email_Template__c, FICO__c, Type, Contract_Renewed__c,Items_Requested__c,Encrypted_AppId_AES__c
                 FROM Opportunity WHERE Id in : appIds]);
            
            System.debug('Applications to Send Notification too ' + apps.size());
            
            for (Opportunity theApp : apps.values()) {
                try {
                    theContact = theApp.Contact__r;
                    if (theContact != null ) {                                                  
                        System.debug('Template Name To Use = ' + tName + ' appID = ' + theApp.Id + ' - Created on ' + theApp.CreatedDate);
                        plainBody = replaceValuesFromData(eTemplate.Body, theContact, theApp);
                        HtmlValue = replaceValuesFromData(eTemplate.HtmlValue, theContact, theApp);
                        Subject = replaceValuesFromData(eTemplate.Subject , theContact, theApp);                                                
                        
                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            sendTo = new List<String>();
                            sendTo.add(theContact.Email);
                                email.setWhatId(theApp.id);
                                email.setTargetObjectId(theApp.Contact__c);
                                email.setToAddresses(sendTo);                    
                                email.setHtmlBody(HtmlValue);
                                email.setPlainTextBody(plainBody);
                                email.setSubject(Subject);
                                email.saveAsActivity = false;
                                email.setTreatTargetObjectAsRecipient(False);
                            system.debug('**email****'+email);
                        mails.add(email);
                        
                            theApp.NOIA_Sent_Date__c = Date.today();
                        updatedApps.add(theApp);                            
                        //AppEmails.add(createEmail(HtmlValue, Subject, theContact.Id ));
                        notesEmails.add(createNote( 'To: ' + theContact.Email + '\n\n' + plainBody, Subject, theApp.Id ));
                        iRecsProcessed++;
                    } 
                    else System.debug('No Contact for v1 AppId = ' + theApp.Id);
                } 
                catch (Exception e) {
                    System.debug('Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString());
                }
            }            
            
            System.debug('Sending App Emails # : ' + AppEmails.size());
            
            if(notesEmails.size() > 0){
                insert notesEmails;                
            }
            //send Email
            if(!mails.isEmpty() && theContact.Email != null){
                try{
                     Messaging.sendEmail(mails);
                     if(updatedApps  != null && updatedApps.size() > 0)
                         Update updatedApps;
                 }
                 Catch(Exception e){
                     System.debug('--------------Exception in Sending emails--------------------'+e.getMessage());
                 }
             }
        } 
        catch (Exception e) {
            WebToSFDC.notifyDev('NOIANotification Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString(), UserInfo.getUserId() ) ;
        }
    }
    
    public Outbound_Emails_Temp__c createEmail(String htmlBody, String subject, Id ContactID ){       
        return new Outbound_Emails_Temp__c(Contact__c = ContactID,  Body__c= htmlBody, Subject__c = subject);
    }
    
    public string replaceValuesFromData(String theValue, Contact theContact, Opportunity theOpp) {
        
        theValue = theValue.replace('{!today()}', Datetime.now().format('MMMM d,  yyyy'));
        
        if (theContact != null) {
            theValue = theValue.replace('{!Contact.FirstName}', theContact.FirstName);
        }
        
        String itemsRequested = '';
        itemsRequested = theOpp.Items_Requested__c;
        theValue = theValue.replace('{!Opportunity.Items_Requested__c}', ((String.isnotempty(itemsRequested) ) ? itemsRequested.replace(';','\r\n') : ''));
        theValue = theValue.replace('{!Opportunity.Encrypted_AppId_AES__c}', theOpp.Encrypted_AppId_AES__c);
        return theValue;
    }
    
    global void finish(Database.BatchableContext BC){
        WebToSFDC.notifyDev('Batch_NOIANotificationOnOpportunity Finished', 'Records processed = ' + iRecsProcessed, UserInfo.getUserId() );
        System.debug('Batch_NOIANotificationOnOpportunity Finished!');
    }
    
    public static Note createNote(String htmlBody, String subject, Id OpportunityID ) {
        return new Note(ParentId = OpportunityID, Body = htmlBody, Title = subject);
    }
    
}