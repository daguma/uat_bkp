@istest
public Class TestHelperForManaged{
    public static Date systemDate = Date.today().addDays(-16);
    public static Date branchSysDate;
    private static ID rootBranchRecordTypeId = null;
    
    public static Date getBranchDate(){
    
        branchSysDate = loan.BranchUtil.getBranch().loan__Current_System_Date__c;
        return branchSysDate;
    }
    
    public static loan__Bank_Account__c createBankAccount(Account acc,String accountUsage) {
        loan__Bank_Account__c bank = new  loan__Bank_Account__c();
        bank.loan__Account__c = acc.ID;
        bank.loan__Account_Type__c = 'Checking';
        bank.loan__Account_Usage__c = accountUsage;
        bank.loan__Active__c = true;
        bank.loan__Bank_Account_Number__c = '098765433';
        //loan__Bank_Name__c = 'Bank Of America',
        bank.loan__Bank_Name__c = '99';
        bank.peer__Branch_Code__c = '998';
        //bank.Bank_Number__c = '56';
       // bank.Branch_Address__c = 'XYZ';
        bank.loan__Routing_Number__c = '98776631';
        //bank.Suffix_Details__c = '67';
        insert bank;
        return bank;
    }
     //Create Contact
    public static Contact createContact(String LastN,String other, String postal, String mailingState){
        
        Contact con = new Contact();
        con.LastName = LastN;
        con.FirstName = LastN + '123';
        con.Other_Use_Of_Funds__c = other;
        String SSN = '111-11';
        //24 byte string. since characters used are ascii, each char is 1 byte.
        Blob key = Blob.valueOf('123456789012345678901234');
        Blob cipherText = Crypto.encryptWithManagedIV('AES192', key, Blob.valueOf(SSN));
        String encodedCipherText = EncodingUtil.base64Encode(cipherText);
        String subString = encodedCipherText.subString(0,10);
        
        Date birth = date.newInstance(1990, 12, 31);
        date endDate = date.newInstance(2016, 12, 1);
        date startDate = date.newInstance(2016, 10, 1);
        
        con.Previous_Employment_End_Date__c = endDate;
        con.Previous_Employment_Start_Date__c = startDate;
        con.Birthdate = birth;
        con.Email ='test@gmail.com';
        con.Employment_Start_Date__c = startDate;
        //con.ints__Social_Security_Number__c = SSN;
        con.MailingPostalCode = postal;
        con.MailingState = mailingState;
        insert con;
        return con;    
    }
     public static Opportunity  createApplication(Contact con,clcommon__CL_Product__c product,loan__Office_Name__c dummyCompany){
        Opportunity gapp = new Opportunity();
       // Opportunity gapp = genesis.TestHelper.createApplication();
        //gapp.CL_Product__c = product.Id;
        gapp.Amount = 12000.00;
        gapp.Payment_Frequency__c = 'Monthly';
        gapp.Term__c = 72;                                                       
        gapp.Status__c = 'INTAKE'; 
        gapp.Company__c = dummyCompany.Id;
        gapp.Contact__c = con.Id;
        gapp.Type = 'New';
        gapp.Name = con.FirstName + ' ' + con.LastName;
        gapp.CloseDate = system.today();
        gapp.StageName = 'Qualification';
        gapp.Product_Type__c = 'LOAN';
        insert gapp;
        return gapp;
    
    }
    //Create Originate Company
    public static loan__Office_Name__c createCompany(String companyName){
        loan__Office_Name__c company = new loan__Office_Name__c();
        //company.Enabled_Flag__c=true;
        company.loan__Office_Short_Name__c = 'CLI';
        company.Name = companyName;
       
        
        insert company;
        return company;
    
    }
   
    public static Account createBorrower(String name) {
        Account a = new Account(Name=name);
        a.loan__Borrower__c = true;
        insert a;
        return a;
    }
    public static loan__Loan_Purpose__c createLoanPurpose() {
        loan__Loan_Purpose__c dummyLoanPurpose = new loan__Loan_Purpose__c(Name='XXXLPForTest',
                                                    loan__Loan_Purpose_Code__c='XXXLPCForTest',
                                                    loan__Description__c = 'XXXLPDescForTest');
        insert dummyLoanPurpose;
        return dummyLoanPurpose;
    }
    public static loan__Currency__c createCurrency() {
        loan__Currency__c curr = new loan__Currency__c(Name='dINR');
        insert curr;
        return curr;
    }
    public static void setupApprovalProcessForTxn(){
        if ([select Id from loan__Transaction_Approval_Config__c].size() == 0) {
            loan__Transaction_Approval_Config__c c = new loan__Transaction_Approval_Config__c();
            c.loan__Payment__c = true;
            c.loan__Payment_Reversal__c = true;
            c.loan__Funding__c = true;
            c.loan__Funding_Reversal__c = true;
            c.loan__Write_off__c = true;
            c.loan__Accounting__c = true;
          
            insert c;
        }
    }
    
    public static void setupMetadataSegments() {
  //  public static List<filegen__Metadata_Segments__c> metaData() {
        List<filegen__Metadata_Segments__c> MetaDataList = filegen__Metadata_Segments__c.getAll().values();
        MetaDataList.add(new filegen__Metadata_Segments__c (Name = 'Batch Control Record', filegen__Segment__c = 'filegen__Batch_Control_Record__c' ));
        MetaDataList.add(new filegen__Metadata_Segments__c (Name = 'Batch Header Record', filegen__Segment__c = 'filegen__Batch_Header_Record__c' ));
        MetaDataList.add(new filegen__Metadata_Segments__c (Name = 'Entry Detail Record', filegen__Segment__c = 'filegen__Entry_Detail_Record__c' ));
        MetaDataList.add(new filegen__Metadata_Segments__c (Name = 'File Control Record', filegen__Segment__c = 'filegen__File_Control_Record__c' ));
        MetaDataList.add(new filegen__Metadata_Segments__c (Name = 'File Header Record', filegen__Segment__c = 'filegen__File_Header_Record__c' ));
        insert MetaDataList;
       // return MetaDataList;
    }
    
    public static void setupACHParameters() {
        loan__ACH_Parameters__c acha = loan__ACH_Parameters__c.getInstance();
        acha.loan__Organization_Name__c = 'Lending Point';
        acha.loan__ACH_ID__c = '4TGD2212-B';
        acha.loan__Fed_Tax_Id__c = '4TGD2211-A';
        acha.loan__Days_In_Advance_To_Create_File__c = 0;
        acha.loan__Lock_Period_for_Loan_Payments__c = 0;
        
        acha.loan__Folder_Name__c='Shared Folder for ACH';
        
        acha.loan__Use_Lock_Based_ACH__c=true;
        acha.Append_Fees_Created_After__c = Date.today();
        acha.Fees_To_Add_to_Payment__c = '';
        
       /* acha.loan__Investor_Payment_Transaction_Filegen__c = 'CustomInvestorPaymentTxnNACHAGen';
        acha.loan__Investor_Deposit_Transaction_Filegen__c = 'CustomInvestorTxnNachaGen';
        acha.loan__Investor_Withdrawal_Transaction_Filegen__c = 'CustomInvestorTxnNachaGen';*/
        acha.loan__Loan_Disbursal_Transaction_Filegen__c = 'LoanDisbursalTxnPayPointGen';
        acha.loan__Loan_Payment_Transaction_Filegen__c = 'LoanPaymentTxnPayPointGen';
        upsert acha;
    }
    
    public static void setupORGParametersActive() {
        loan__Org_Parameters__c orgParams = loan__Org_Parameters__c.getInstance();
        orgParams.loan__Disable_Triggers__c = true;
        upsert orgParams;
    }
      
    public static void setupORGParameters() {
        loan__Org_Parameters__c orgParams = loan__Org_Parameters__c.getInstance();
        orgParams.loan__Disable_Triggers__c = true;
        upsert orgParams;
    }
    public static void createSeedDataForTesting() {
        List<loan__Payment_Mode__c> pModes = new List<loan__Payment_Mode__c>();
        loan__Payment_Mode__c dummyPM = new loan__Payment_Mode__c(name='dummyPM');
        pModes.add(dummyPM);
        loan__Payment_Mode__c dummyPM2 = new loan__Payment_Mode__c(name='Check');
        pModes.add(dummyPM2);
        loan__Payment_Mode__c dummyPM3 = new loan__Payment_Mode__c(name='Cash');
        pModes.add(dummyPM3);
        loan__Payment_Mode__c dummyPM4 = new loan__Payment_Mode__c(name='ACH');
        pModes.add(dummyPM4);
        loan__Payment_Mode__c dummyPM5 = new loan__Payment_Mode__c(name='Wire');
        pModes.add(dummyPM5);
        loan__Payment_Mode__c dummyPM6 = new loan__Payment_Mode__c(name='Excess');
        pModes.add(dummyPM6);
        loan__Payment_Mode__c dummyPM7 = new loan__Payment_Mode__c(name='Mobile');
        pModes.add(dummyPM7);
        insert pModes;
        
        loan__Journal__c  dummyJournal = new loan__Journal__c (name = 'dummyJournal',loan__Description__c='dummyJDesc');
        insert dummyJournal;
        
        loan__Business_Activity__c dummyBActivity = new loan__Business_Activity__c(name='dummBusinessActivity ');
        insert dummyBActivity;
            
        loan__Loan_Purpose__c dummyLoanPurpose = new loan__Loan_Purpose__c(name='dummyLoanPurpose',
                                                            loan__Description__c='Loan P Desc',
                                                            loan__Loan_Purpose_Code__c='LPDummyCode');
        insert dummyLoanPurpose;                                                            
        
        loan__Fiscal_Year__c[] years = [select Id,
                                    loan__Start_Date__c,
                                    loan__End_Date__c,
                                    loan__Fiscal_Year_Setting_Id__c,
                                    loan__Status__c
                         from loan__Fiscal_Year__c where loan__Status__c = :loan.AccountingConstants.FISCAL_YEAR_STATUS_OPEN];
        if(years.size() == 0){
            Date today = systemDate;
            loan__Fiscal_Year__c f = new loan__Fiscal_Year__c(
                    loan__Start_Date__c = date.newInstance(today.year(), 1, 1),
                    loan__End_Date__c = date.newInstance(today.year(), 12, 31),
                    loan__Status__c = loan.AccountingConstants.FISCAL_YEAR_STATUS_OPEN
                );
            
            insert f;
        }                         
        
    }

    public static ID getRootBranchRecordTypeID() {
        if (rootBranchRecordTypeId != null) {
            return rootBranchRecordTypeId;
        }
        try{
            rootBranchRecordTypeId = [SELECT ID FROM RecordType WHERE Name='Root Branch'
                                    AND DeveloperName='Root_Branch'
                                    AND NamespacePrefix='loan' LIMIT 1].Id;
        }catch(Exception e){
            System.assertEquals(e.getMessage(),null);
        }
        System.debug('rootBranchRecordTypeId: ' + rootBranchRecordTypeId);
        return rootBranchRecordTypeId;
    }
    
    /*public static Contact createContactWithoutInsert(String fName,loan__Office_Name__c branch){
        Contact dummyClient = new Contact(LastName = fName,loan__Branch__c = branch.Id);
        
        return dummyClient;
    }*/
    
   
    
    public static loan__Loan_Purpose__c createLoanPurpose(String purpose,String description, String shortCode){
    
        loan__Loan_Purpose__c  lPurpose = new loan__Loan_Purpose__c(Name = purpose);
        lPurpose.loan__Loan_Purpose_Code__c = shortCode;
        lPurpose.loan__Description__c = description;
        insert lPurpose;
        return lPurpose;
    }
   
     public static loan__Loan_Account__c createLoanAccountForAccountObj(loan__Loan_Product__c dummyLP,
                                                    Account b1,
                                                    loan__Fee_Set__c dummyFeeSet,
                                                    loan__Loan_Purpose__c dummyLoanPurpose,
                                                    loan__Office_name__c dummyOffice,
                                                    boolean ACH_on, 
                                                    Date nextDebitDate,
                                                    Date otNextDebitDate,
                                                    String pmtFrequency) {
        
        loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccountForAccountObj(dummyLP,
                                                    b1,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
        loanAccount.loan__Loan_Status__c = loan.LoanConstants.LOAN_STATUS_ACTIVE_GOOD_STANDING;
        loanAccount.loan__Fees_Remaining__c  = 100;
        loanAccount.loan__Interest_Remaining__c = 500;
        loanAccount.loan__Principal_Remaining__c = 1000;
        loanAccount.loan__Pay_off_Amount_As_Of_Today__c = loanAccount.loan__Fees_Remaining__c + loanAccount.loan__Principal_Remaining__c;
        loanAccount.loan__Last_Accrual_Date__c = loan.TestHelper.systemDate;
        loanAccount.loan__Frequency_of_Loan_Payment__c = pmtFrequency;
        loanAccount.loan__ACH_on__c = ACH_on;
        If(ACH_on) {
            loanAccount.loan__ACH_Account_Number__c = '123456789';
            loanAccount.loan__ACH_Account_Type__c = 'Checking';
            loanAccount.loan__ACH_Bank_Name__c = 'Bank Of America';
            loanAccount.loan__ACH_Routing_Number__c = '999999';
            loanAccount.loan__ACH_Debit_Amount__c = 100;
            loanAccount.loan__OT_ACH_Debit_Date__c = otNextDebitDate;
            loanAccount.loan__Ach_Debit_Day__c = nextDebitDate.Day();
            loanAccount.loan__ACH_Start_Date__c = loan.TestHelper.systemDate.addDays(-10);
            loanAccount.loan__ACH_Next_Debit_Date__c = nextDebitDate;
            loanAccount.loan__ACH_Start_Date__c = loan.TestHelper.systemDate.addDays(-10);
            loanAccount.loan__ACH_Frequency__c =loanAccount.loan__Frequency_of_Loan_Payment__c;
        }else {
            
            loanAccount.loan__OT_ACH_Account_Number__c = '123456789';
            loanAccount.loan__OT_ACH_Account_Type__c = 'Checking';
            loanAccount.loan__OT_ACH_Bank_Name__c = 'Bank Of America';
            loanAccount.loan__OT_ACH_Debit_Date__c = otNextDebitDate;
            loanAccount.loan__OT_ACH_Drawer_Address1__c = 'Drawer Address1';
            loanAccount.loan__OT_ACH_Drawer_Address2__c = 'Drawer Address2';
            loanAccount.loan__OT_ACH_Drawer_City__c = 'Drawer City';
            loanAccount.loan__OT_ACH_Drawer_Name__c = 'Drawer Name';
            loanAccount.loan__OT_ACH_Drawer_State__c = 'Drawer State';
            loanAccount.loan__OT_ACH_Drawer_Zip__c = '987654';
            loanAccount.loan__OT_ACH_Fee_Amount__c = 20;
            loanAccount.loan__OT_ACH_Payment_Amount__c = 100;
            loanAccount.loan__OT_ACH_Relationship_Type__c = 'Primary';
            loanAccount.loan__OT_ACH_Routing_Number__c = '999999';
            
            
        }
        update loanAccount;
        return loanAccount;
    }
}