public  with sharing class Controller_AllSupport {
    
    public ApexPages.StandardController myAccountController {get; set;}
    public Account Account {get; set;}
    
    
    public ApexPages.StandardController myContactController {get; set;}
    public Contact myContact {get; set;}
    
    public ApexPages.StandardController myBankDetailController {get; set;}
    public loan__Bank_Account__c bank {get; set;}
    
    public ApexPages.StandardController myOriginateController {get; set;}
    public genesis__applications__c application {get; set;}
    
    //   public list<genesis__applications__c> application = new list<genesis__applications__c>();
    
    public ApexPages.StandardController mycollectionController {get; set;}
    public Collection__c collections {get; set;}
    
    
    public ApexPages.StandardController myUserController {get; set;}
    public User User {get; set;} 
    public ApexPages.StandardController standardContactController;
    
    public ApexPages.StandardController myClContract {get; set;}
    public loan__loan_account__c mycontract {get; set;}
    
    /*public ApexPages.StandardController scorecard{get; set;}
    public Scorecard__c score {get; set;}
    public String generalStatus{get; set;}
    public List<SelectOption> options;
    */
    
    //public Map<String, Set<String>> subStatus = new Map<String, Set<String>>();
    
    
    public Controller_AllSupport(ApexPages.StandardController cntrl) {
        
        
        standardContactController = cntrl;
        
        Contact con = (Contact)cntrl.getRecord();
        
        //application
        application = [SELECT id,
                       Name, 
                       genesis__contact__c, 
                       Funded_Date__c, 
                       genesis__Term__c, 
                       genesis__Payment_Amount__c, 
                       genesis__Payment_Frequency__c, 
                       genesis__Bank_Account_Type__c,
                       genesis__Expected_First_Payment_Date__c,
                       genesis__Bank_Account_Number__c,
                       genesis__Routing_Number__c,
                       Employment_Time_at_Job_Accepted__c,
                       genesis__Bank_Name__c,
                       FICO__c  
                       from  genesis__applications__c 
                       where genesis__contact__c =: con.id limit 1];
        System.debug('App id = ' + application.id); 
        
        //account
        Account = [select id, Name,
                   peer__Amount_In_Funding__c, 
                   c2g__CODABankName__c, 
                   c2g__CODABankAccountNumber__c,
                   c2g__CODAPaymentMethod__c 
                   from Account 
                   where id = :con.AccountId][0];
        
        //contact
        /*myContact=[select AccountId,OwnerId,Application__c, Employer_Name__c,ints__Employer__c,Office_Address_Street__c,Office_Address_City__c,Office_Address_State__c,Office_Phone_Number__c,
Employment_Type__c,Is_Number_Verified__c from Contact where Application__c =: application.id];
*/
try{
bank = [select loan__Account_Type__c,loan__Account__c from loan__Bank_Account__c where  loan__Contact__c=:con.id ];
myBankDetailController = new ApexPages.StandardController(bank);
} catch(exception e){
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please input proper Transaction From & To Date Rage.'));
return;
}
        
        User = [select FirstName from User where id=:con.OwnerId][0];
        
        System.debug('Contact id = ' + con.id);
        
        //list<genesis__applications__c> appId = [select id from genesis__applications__c where id = :ApexPages.currentPage().getParameters().get('id')];
        //try{
        
        mycontract = [select id, 
                      name, 
                      loan__contact__c,
                      loan__First_Installment_Date__c , 
                      loan__Principal_Remaining__c, 
                      loan__Total_Amount_Overdue__c, 
                      loan__Fees_Remaining__c, 
                      loan__Pay_Off_Amount_As_Of_Today__c, 
                      loan__Previous_Installment_Date__c, 
                      loan__Last_Billed_Amount__c, 
                      loan__Oldest_Due_Date__c, 
                      loan__Frequency_of_Loan_Payment__c,
                      loan__Next_Installment_Date__c,
                      Number_of_Payments_Missed__c,
                      Agreement_Modified_Date__c,
                      Bankruptcy_Case__c,
                      loan__Disbursed_Amount__c,
                      Proof_of_Claim_Filed__c 
                      from Loan__loan_account__c 
                      where loan__contact__c =: con.id limit 1];
        System.debug('CL Contract id --- ' + mycontract.id);
        try{
          collections = [select id,name,
                           Missed_Payment_Reason__c,
                           General_Status__c,
                           Sub_Status__c
                           from Collection__c 
                           where CL_Contract__c=:mycontract.id];
            
            mycollectionController = new ApexPages.StandardController(collections);
            
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No Contract found associated with Contact '));
            
        }
        /* }
catch(Exception e){
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No CL Contract found associated with Contact '));

}*/
        
        //myContactController = new ApexPages.StandardController(myContact);  
        myClContract = new ApexPages.StandardController(mycontract);
        myAccountController = new ApexPages.StandardController(Account);
        myUserController = new ApexPages.StandardController(User);
        myOriginateController = new ApexPages.StandardController(application); 
        
    }
    /*public void updateDropdown() {
        if(generalStatus == 'Contact_Attempted'){
            options = new List<SelectOption>();
            options.add(new SelectOption('Contact_Made','Contact Made'));
            options.add(new SelectOption('Contact_Failed','Contact Failed'));
        }else if(generalStatus == 'Manual_Escalation'){
            options = new List<SelectOption>();
            options.add(new SelectOption('Requested','Loan Modification Requested'));
            options.add(new SelectOption('Refusal','Refusal to Address'));
            options.add(new SelectOption('Unable','Unable to Address'));
            options.add(new SelectOption('Payment','Payment Plan Requested'));
        }    
    }
    public List<SelectOption> getItems() {
        return options;
    }*/
    
    public pageReference doSaveAll() {
        
        standardContactController.save();
        
        myAccountController.save();
        mycollectionController.save();
        myUserController.save();
        myOriginateController.save();
        //try{
        myClContract.save();
        //myContactController.save();
        //myBankDetailController.save();
        /* } catch(Exception e){
System.debug('Error--- ' + e.getMessage());
}
*/
        return null;
        
    }
    /*   public PageReference ViewPayment()
{
// Apex code for handling record from a Detail page goes here
Id recordId = Controller_AllSupport.getId();
// Test__c record = (Test__c) Controller_AllSupport.getRecord(); 
return null;
}  */
    public PageReference goBack() {
        
        //    return new PageReference(URL.getSalesforceBaseUrl().toExternalForm().replace('c.', 'loan.') + '/apex/tabbedLoanAccount?id=' + this.contract.Id);        
        PageReference pg = new PageReference('/apex/Loan_Correspondence?id=' + this.myContract.Id);
        pg.setredirect(true);
        return pg;
        //  return new PageReference(URL.getSalesforceBaseUrl().toExternalForm().replace('c.', 'loan.') + '/apex/ContactInformation?id=' + this.contract.Id);  
    }
    
    
}