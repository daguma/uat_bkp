global class ProxyApiCreditReportEntityLite {
    @AuraEnabled public Long id {get; set;}
    @AuraEnabled public String appId {get; set;}
    @AuraEnabled public String appName {get; set;}
    @AuraEnabled public Boolean isSoftPull {get; set;}
    @AuraEnabled public String sequenceId {get; set;}
    @AuraEnabled public String grade {get; set;}
    @AuraEnabled public String model {get; set;}
    @AuraEnabled public String datasourceName {get; set;}
    @AuraEnabled public Long createdDate {get; set;}
    @AuraEnabled public String errorMessage {get; set;}
    @AuraEnabled public Boolean decision {get; set;}
    @AuraEnabled public Boolean isPrimary {get; set;}

    @AuraEnabled public String name {
        get {
            if (this.id == null) return ' - ';

            String text = String.valueOf(this.id);
            while (text.length() < 8) text = '0' + text;
            return 'CR-' + text;
        }
        set;
    }

    @AuraEnabled public String sequenceName {
        get {
            for (BRMS_Sequence_Ids__c sequence : [SELECT Id, IsDeleted, Name, Sequence_Id__c
            FROM BRMS_Sequence_Ids__c
            WHERE IsDeleted = false
            AND Sequence_Id__c = : sequenceId
            LIMIT 1]) {
                return sequence.Name;
            }

            return ' - ';
        }
        set;
    }

    @AuraEnabled public String automatedDecision {
        get {
            return decision ? CreditReport.DECISION_YES : CreditReport.DECISION_NO;
        }
        set;
    }

    @AuraEnabled public DateTime creationDate {
        get {
            if (createdDate == null) return null;

            return DateTime.newInstance(createdDate);
        }
        set;
    }

    @AuraEnabled public Boolean errorResponse {
        get {
            return errorMessage != null;
        }
        set;
    }

    @AuraEnabled public String appUrl {
        get {
            return URL.getSalesforceBaseUrl().toExternalForm() + '/apex/newAppDetails?id=' + appId;
        }
    }

    /**
     * Get the credit report create date in Datetime format
     * @result  create date of the credit report
     */
    public Datetime getCreatedDate() {
        if (createdDate == null) return null;

        return Datetime.newInstance(createdDate);
    }

    /**
     * Get the credit report create date in Datetime format
     * @result  true if the credit report create date is on the last 30 days, otherwise false
     */
    public Boolean isCreateDateOnLast30Days() {
        if (createdDate == null) return false;

        return (Datetime.now() - 30) <= getCreatedDate();
    }
}