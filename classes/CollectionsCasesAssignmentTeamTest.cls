@isTest
public class CollectionsCasesAssignmentTeamTest {

    @isTest static void validate_constructor() {
        Test.startTest();

        List<CollectionsCasesAssignmentUsersGroup> usersGroupsList = new List<CollectionsCasesAssignmentUsersGroup>{
                  new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2,31,90)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3,91,180)
        };
        CollectionsCasesAssignmentTeam team = new CollectionsCasesAssignmentTeam.CollectionsTeam(usersGroupsList);
        System.assertNotEquals(null,team);

        Test.stopTest();
    }

    @isTest static void validate_getGroupNameByDpd() {
        Test.startTest();

        List<CollectionsCasesAssignmentUsersGroup> usersGroupsList = new List<CollectionsCasesAssignmentUsersGroup>{
                  new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2,31,90)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3,91,180)
        };
        CollectionsCasesAssignmentTeam team = new CollectionsCasesAssignmentTeam.CollectionsTeam(usersGroupsList);

        for(CollectionsCasesAssignmentUsersGroup usersGroup : team.usersGroupsMap.values()){
            if(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1.equals(usersGroup.getGroupName())){
                for(Integer i = usersGroup.minimumDPD; i <= usersGroup.maximumDPD; i++){
                    System.assertEquals(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1, team.getGroupNameByDpd(i));
                }
            }
            if(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2.equals(usersGroup.getGroupName())){
                for(Integer i = usersGroup.minimumDPD; i <= usersGroup.maximumDPD; i++){
                    System.assertEquals(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2, team.getGroupNameByDpd(i));
                }
            }
            if(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3.equals(usersGroup.getGroupName())){
                for(Integer i = usersGroup.minimumDPD; i <= usersGroup.maximumDPD; i++){
                    System.assertEquals(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3, team.getGroupNameByDpd(i));
                }
            }
        }

        Test.stopTest();
    }

    @isTest static void validate_getAllTeamMembersIds() {
        Test.startTest();

        List<CollectionsCasesAssignmentUsersGroup> usersGroupsList = new List<CollectionsCasesAssignmentUsersGroup>{
                  new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2,31,90)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3,91,180)
        };
        CollectionsCasesAssignmentTeam team = new CollectionsCasesAssignmentTeam.CollectionsTeam(usersGroupsList);
        System.assert(!team.getAllTeamMembersIds().isEmpty());

        List<GroupMember> groupMembersList = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1];
        List<Id> userIdsList = new List<Id>();
        for (GroupMember member: groupMembersList) userIdsList.add(member.UserOrGroupId);
        List<User> collectionsGroupLevel1UsersList = [SELECT Id FROM User WHERE Id IN :userIdsList AND IsActive = true];
        for(User testUser : collectionsGroupLevel1UsersList){
            //System.assert(team.getAllTeamMembersIds().contains(testUser.Id));
        }

        Test.stopTest();
    }

    @isTest static void validate_collections_isTriggerHit() {
        Test.startTest();

        List<CollectionsCasesAssignmentUsersGroup> usersGroupsList = new List<CollectionsCasesAssignmentUsersGroup>{
                  new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2,31,90)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3,91,180)
        };
        CollectionsCasesAssignmentTeam team = new CollectionsCasesAssignmentTeam.CollectionsTeam(usersGroupsList);

        GroupMember collectionsLevel1GroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1];
        System.assert(team.isTriggerHit(collectionsLevel1GroupMember.UserOrGroupId,35));
        System.assert(team.isTriggerHit(collectionsLevel1GroupMember.UserOrGroupId,105));
        System.assert(!team.isTriggerHit(collectionsLevel1GroupMember.UserOrGroupId,2));

        GroupMember collectionsLevel2GroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel2 LIMIT 1];
        System.assert(!team.isTriggerHit(collectionsLevel2GroupMember.UserOrGroupId,5));
        System.assert(!team.isTriggerHit(collectionsLevel2GroupMember.UserOrGroupId,35));
        System.assert(team.isTriggerHit(collectionsLevel2GroupMember.UserOrGroupId,95));

        GroupMember collectionsLevel3GroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel3 LIMIT 1];
        System.assert(!team.isTriggerHit(collectionsLevel3GroupMember.UserOrGroupId,5));
        System.assert(!team.isTriggerHit(collectionsLevel3GroupMember.UserOrGroupId,35));
        System.assert(!team.isTriggerHit(collectionsLevel3GroupMember.UserOrGroupId,95));

        Test.stopTest();
    }

    @isTest static void validate_lms_isTriggerHit() {
        Test.startTest();

        List<CollectionsCasesAssignmentUsersGroup> usersGroupsList = new List<CollectionsCasesAssignmentUsersGroup>{
                  new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_1,1,180)
        };
        CollectionsCasesAssignmentTeam team = new CollectionsCasesAssignmentTeam.LmsTeam(usersGroupsList);

        GroupMember lmsLevel1GroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1];
        System.assert(!team.isTriggerHit(lmsLevel1GroupMember.UserOrGroupId,35));

        Test.stopTest();
    }

    @isTest static void validate_dmc_isTriggerHit() {
        Test.startTest();

        List<CollectionsCasesAssignmentUsersGroup> usersGroupsList = new List<CollectionsCasesAssignmentUsersGroup>{
                  new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_1,1,30)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_2,31,90)
                , new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_3,91,180)
        };
        CollectionsCasesAssignmentTeam team = new CollectionsCasesAssignmentTeam.DmcTeam(usersGroupsList);

        GroupMember dmcLevel1GroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsDMCLevel1 LIMIT 1];
        System.assert(!team.isTriggerHit(dmcLevel1GroupMember.UserOrGroupId,2));

        GroupMember dmcLevel2GroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsDMCLevel2 LIMIT 1];
        System.assert(!team.isTriggerHit(dmcLevel2GroupMember.UserOrGroupId,5));
        System.assert(!team.isTriggerHit(dmcLevel2GroupMember.UserOrGroupId,35));
        System.assert(team.isTriggerHit(dmcLevel2GroupMember.UserOrGroupId,95));

        GroupMember dmcLevel3GroupMember = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsDMCLevel3 LIMIT 1];
        System.assert(!team.isTriggerHit(dmcLevel3GroupMember.UserOrGroupId,5));
        System.assert(team.isTriggerHit(dmcLevel3GroupMember.UserOrGroupId,35));
        System.assert(!team.isTriggerHit(dmcLevel3GroupMember.UserOrGroupId,95));

        Test.stopTest();
    }
}