public with sharing class BridgerNewLookupCtrl {

    public boolean autolookup {get; set;}
    private final BridgerWDSFLookup__c bridgerEntity;

    public BridgerNewLookupCtrl(ApexPages.StandardController stdController) {

        //if (!Test.isRunningTest()) stdController.addFields(new List<String> {'Contact__c'});
        bridgerEntity = (BridgerWDSFLookup__c)stdController.getRecord();
        autolookup = true;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please wait while the Bridger Check is performed.....');
        ApexPages.addMessage(myMsg);

    }

    public PageReference createBrigerCall() {
        autolookup = false;
        Contact cont = [SELECT  Id,
		                        Name,
		                        SSN__c,
		                        BirthDate,
		                        MailingStreet,
		                        MailingState,
		                        MailingPostalCode,
		                        MailingCountry,
		                        MailingCity,
		                        FirstName,
		                        LastName,
		                        AccountID
		                        FROM Contact
		                        WHERE Id = : bridgerEntity.Contact__c
                               ORDER by CreatedDate
                               LIMIT 1];
        
        String xmlResponse = '';

        if (!Test.isRunningTest()) {
        	BridgerSFLookup bridger = new BridgerSFLookup(cont);
        	if (String.isEmpty(bridger.bridgerError)) {
	            xmlResponse = bridger.connect(bridger.createRequest());
	            bridger.parseDOC(xmlResponse);
        	}
        }

        PageReference pageRef = new PageReference('/' + bridgerEntity.Contact__c);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public PageReference goBack() {
        PageReference pageRef = new PageReference('/' + bridgerEntity.Contact__c);
        pageRef.setRedirect(true);
        return pageRef;
    }
}