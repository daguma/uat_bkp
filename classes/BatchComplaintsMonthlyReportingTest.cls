@isTest
public Class BatchComplaintsMonthlyReportingTest{    
        
    @testSetup
    static void setupTestFinwiseData(){
        //Create Custom setting data
       FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name='finwise',Finwise_Base_URL__c='https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/',Apply_State_Restriction__c=false,
                                      Finwise_LendingPartnerId__c='5',Finwise_LendingPartnerProductId__c='7',
                                      Finwise_LoanStatusId__c='1',finwise_partner_key__c='testKey',Finwise_product_Key__c='testkey',
                                      Finwise_RateTypeId__c='1',Whitelisted_States__c='MO',No_of_Days_to_Add_Daily_Interest_Batch__c = 1);
       insert FinwiseParams; 
       Finwise_Complaint_Categories__c finWiseComplaintCategories = new Finwise_Complaint_Categories__c(Name= 'Application', id__c = 11, SFDC_Value__c = true);
       insert finWiseComplaintCategories;
       
       Finwise_Tier_Id_mapping__c finTier = new Finwise_Tier_Id_mapping__c(Name= 'Low', id__c = 11, SFDC_Value__c = '');
       insert finTier;
       
       Finwise_Complaint_Source__c fComplaintSource = new Finwise_Complaint_Source__c(Name= 'Application', Source__c = 'Consumer Verbal',id__c = 11, SFDC_Value__c = true);
       insert fComplaintSource;
    }
    
    static testMethod void ExecutePositiveCase(){ 
        opportunity app = TestHelper.createApplication();
        TestHelper.createComplaint(-15, app, true); 
        Test.startTest();
            BatchComplaintsMonthlyReporting btchP = new BatchComplaintsMonthlyReporting();
            Database.executeBatch(btchP);
        Test.stopTest(); 
        List<Case> caseList = [select id, Complain_reported_to_Finwise__c, createdDate, ClosedDate from Case where Complain_reported_to_Finwise__c = true]; 
        system.assertEquals(caseList.size(), 1);
    }
    static testMethod void ExecuteNegativeCase(){ 
        opportunity app = TestHelper.createApplication();
        TestHelper.createComplaint(0, app, false); 
        Test.startTest();
            BatchComplaintsMonthlyReporting btchP = new BatchComplaintsMonthlyReporting();
            Database.executeBatch(btchP);
        Test.stopTest(); 
        List<Case> caseList = [select id, Complain_reported_to_Finwise__c, createdDate, ClosedDate from Case where Complain_reported_to_Finwise__c = true]; 
        system.assertEquals(caseList.size(), 0); 
    }
}