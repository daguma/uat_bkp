public with sharing class CoborrowerScreenCtrl extends genesis.ApplicationWizardBaseClass {
    public genesis__Applications__c application {get; set;}
    public note notes {get; set;}
    public boolean showpanel {get; set;}
    public contact c {get; set;}
    public boolean hideattach {get; set;}
    public string LastNameInput {get; set;}
    public boolean ShowPullButton {get; set;}

    public CoborrowerScreenCtrl(ApexPages.StandardController controller) {
        super(controller);
        showpanel = false;
        hideattach = true;
        c = new Contact();
        this.application = (genesis__Applications__c)controller.getRecord();

        if (this.application.Id <> null) {
            this.application = [Select Id, Coborrower__c,
                                Coborrower__r.Firstname,
                                Coborrower__r.Lastname,
                                Coborrower__r.MailingStreet,
                                Coborrower__r.MailingCity,
                                Coborrower__r.MailingState,
                                Coborrower__r.MailingPostalCode,
                                Coborrower__r.ints__Social_Security_Number__c,
                                Coborrower__r.Annual_Income__c
                                from genesis__Applications__c
                                where id = : this.application.Id];
        }

        if (this.application.coborrower__c <> null) {
            ShowPullButton = true;
            PopulateInfo();
        }
    }

    public void PopulateInfo() {
        system.debug('==========this.application.coborrower__c========' + this.application.coborrower__c);

        if (this.application.coborrower__c <> null) {
            c = [Select id, Firstname, Lastname, MailingStreet,
                 MailingCity, MailingState, MailingPostalCode,
                 ints__Social_Security_Number__c, phone, MobilePhone,
                 Time_at_Current_Address__c, Email, Employer_Name__c,
                 Postal_Code__c,
                 Salary_Cycle__c,
                 Annual_Income__c,
                 Last_Pay_Date__c,
                 Employment_Start_Date__c,
                 Office_Address_Street__c,
                 Office_Address_City__c,
                 Office_Address_State__c,
                 Office_Phone_Number__c,
                 Employment_Verified_With__c,
                 Employment_Verified_Other__c   from Contact
                 where id = :this.application.coborrower__c];

            LastNameInput = c.lastname;
        }
    }


    public void creditPull() {
        try {
            ProxyApiCreditReportEntity creditReportEntity = CreditReport.get(application.Id, true, false, true);

            if (creditReportEntity != null) {
                if (creditReportEntity.errorMessage != null) {
                    createMessage(ApexPages.Severity.Error, creditReportEntity.errorMessage);
                } else {
                    createMessage(ApexPages.Severity.Confirm, 'Credit pull successful.');
                }
            } else {
                createMessage(ApexPages.Severity.Error, 'Error in pulling Credit Report.');
            }

        } catch (Exception e) {
            createMessage(ApexPages.Severity.Error, 'Error in pulling Credit Report. ' + e.getMessage());
        }
    }

    public void saveApp() {
        try {
            if (this.c.Employment_Verified_With__c == 'Other' && String.isEmpty(this.c.Others__c)) {
                createMessage(ApexPages.Severity.Error, 'Error : Please input value in Other field as Employment Verified Checklist is Others');
                return;
            }

            c.lastname = LastNameInput;
            upsert c;

            this.application.coborrower__c = c.Id;
            update this.application;

            ShowPullButton = true;
        } catch (Exception e) {
            createMessage(ApexPages.Severity.Error, 'Error : ' + e.getMessage());
        }

        createMessage(ApexPages.Severity.Confirm, 'Coborrower information saved');
    }

    public Note mynote;

    Public Note getmynote() {
        mynote = new Note();
        return mynote;
    }

    Public Pagereference Savedoc() {
        String accid = this.application.id;
        Note a = new Note(parentId = accid, title = mynote.title, body = mynote.body);
        /* insert the attachment */
        insert a;
        return NULL;
    }

    Public Pagereference hidepanel() {
        showpanel = true;
        hideattach = false;
        return null;
    }

    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }

}