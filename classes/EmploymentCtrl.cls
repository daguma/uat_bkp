public class EmploymentCtrl {
    
    @AuraEnabled
    public static EmploymentEntity getEmploymentEntity(Id appId) {
        EmploymentEntity empEntity = new EmploymentEntity();
        Employment_Details__c empDetail = new Employment_Details__c();
        
        if(String.isEmpty(appId))  return empEntity;

        try {  
            empDetail.Opportunity__c = appId;
            //get the opportunity record
            Opportunity opportunity = getOpportunity(appId);
            if(opportunity != null) {
                empEntity.setOpportunity(opportunity);
                if(opportunity.Contact__c == null) {
                    empEntity.setEmployementDetails(empDetail);
                }else {                    
                    //get the employement detail record
                    empDetail = getEmploymentDetails(opportunity, empDetail);
                    empEntity.setEmployementDetails(empDetail);
                    //EmploymentCtrl.initialize(empEntity);
                    
                    Boolean showsWarning = (opportunity.Contact__r.Employment_Type__c == 'Employee' || opportunity.Contact__r.Second_Income_Type__c == 'Employee');
                    
                    if (showsWarning && empDetail.Employment_Verification_Method__c == null || EmpDetail.Employment_Verification_Method__c == 'Not Verified') {
                        empEntity.setWarnings('The employment must be Verified for this type of employment.');
                    }
                }
            }else {
                empEntity.setErrorMessage('Opportunity is null.');
            }
            
        }catch(Exception e) {
            system.debug('Employement Entity : ' + e.getMessage());
        }       
        return empEntity;
    }
    
    @AuraEnabled
    public static String verifyWorkEmail(String conId, String appId) {
        String response = '';
        try {
            List<Contact> conList = [SELECT Id, Name, Work_Email_Address__c  FROM Contact WHERE Id =: conId];
            
                response = 'Warning: There is work contact on record for this customer';
            if (conList.size() > 0) {
                Contact con = conList[0]; 
                String email = con.Work_Email_Address__c;
                String oppId = appId;
                Boolean bHasEmail = email != null && email.indexOf('@') > 0;
                
                response = 'Warning: There is no work email on record for this customer';
                if (bHasEmail) {
                    EmailAge.getEmailAgeResult(oppId, false, email);
                    response = 'Success: Verification completed for email ' + email + '.';
                }  
            }  
        }catch(Exception e) {
            response = e.getMessage();
        } 
        return response;
    }
      
    private static Opportunity getOpportunity(Id oppId) {
        return [
            SELECT Id, Name, Contact__c, Contact__r.Id, Contact__r.Employer_Name__c, Contact__r.Employment_Type__c
            , Contact__r.Second_Income_Type__c, Contact__r.name, Contact__r.Work_Email_Address__c
            , Contact__r.Work_Phone_Number__c, Contact__r.Office_Phone_Number__c, Payroll_Frequency__c, Type, FS_Assigned_To_Val__c
            FROM Opportunity 
            WHERE Id = :oppId
        ];
    }
    
    private static Employment_Details__c getEmploymentDetails(Opportunity opp, Employment_Details__c empDetail) {
        
        String queryStr = generateDynamicSOQL('Employment_Details__c', 'WHERE Opportunity__c = \'' + opp.Id + '\' ORDER BY CreatedDate DESC LIMIT 1', 'Completed_By__r.Name, SJ_Completed_By__r.Name, TJ_Completed_By__r.Name');
        List<Employment_Details__c> empDetailList = Database.query(queryStr);
        
        if(empDetailList.size() > 0) {
            empDetail = empDetailList[0];               
        }
        
        if (String.IsBlank(empDetail.Salary_Cycle__c)) {
            empDetail.Salary_Cycle__c = opp.Payroll_Frequency__c;
        }
        if (String.IsBlank(EmpDetail.Employer_Phone_Number__c)) {
            EmpDetail.Employer_Phone_Number__c = opp.Contact__r.Office_Phone_Number__c;
        }
        return empDetail;
    }
    
    private static String generateDynamicSOQL(String SobjectApiName, String WhereClause, String commaSepratedFields) {
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        } 
        return 'SELECT ' + commaSepratedFields + ' FROM ' + SobjectApiName + ' ' + WhereClause; 
    }
    
    @AuraEnabled
    public static List <sObject> fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List <sObject> returnList = new List <sObject>();
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        List <sObject> lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static AuraProcessingMessage saveApp(Employment_Details__c EmpDetail) {
        system.debug(EmpDetail); 
        AuraProcessingMessage returnMessage = validate(EmpDetail);
        system.debug(EmpDetail);       
        if (returnMessage.isSuccess) {              
            if (!EmpDetail.Less_than_1_year__c ) {
                EmpDetail.Prior_Company_Name__c = null;
                EmpDetail.Prior_Employment_Confirmed_By__c = null;
                EmpDetail.Prior_Third_Party__c = false;
                EmpDetail.Prior_Company_Phone__c = null;
                EmpDetail.Previous_Employment_Start_Date__c = null;
                EmpDetail.Previous_Employment_End_Date__c = null;
                EmpDetail.Prior_Employment_Contact_Number__c = null;
            }
            
            if (!EmpDetail.Second_Job__c ) {
                EmpDetail.SJ_Employment_Confirmed_By__c = null;
                EmpDetail.SJ_Hire_Date__c = null;
                EmpDetail.SJ_Last_Pay_Date__c = null;
                EmpDetail.SJ_Position__c = null;
                EmpDetail.SJ_Salary_Cycle__c = null;
                EmpDetail.SJ_Third_Party__c = false;
                
                EmpDetail.TJ_Employment_Confirmed_By__c = null;
                EmpDetail.TJ_Hire_Date__c = null;
                EmpDetail.TJ_Last_Pay_Date__c = null;
                EmpDetail.TJ_Position__c = null;
                EmpDetail.TJ_Salary_Cycle__c = null;
                EmpDetail.TJ_Third_Party__c = false;
                EmpDetail.Third_Job__c = false;
            }
            
            if (!EmpDetail.Third_Job__c ) {
                EmpDetail.TJ_Employment_Confirmed_By__c = null;
                EmpDetail.TJ_Hire_Date__c = null;
                EmpDetail.TJ_Last_Pay_Date__c = null;
                EmpDetail.TJ_Position__c = null;
                EmpDetail.TJ_Salary_Cycle__c = null;
                EmpDetail.TJ_Third_Party__c = false;
            }
            
            try {
                upsert EmpDetail;
                returnMessage.isSuccess = true;  
                System.debug(EmpDetail);
                returnMessage.message = 'Employment Details Updated';
            }
            catch (Exception ex) {
                returnMessage.isSuccess = false;           
                returnMessage.message =  ex.getMessage();
            }
        }
        return returnMessage;
    }     
    
    private static AuraProcessingMessage validate(Employment_Details__c EmpDetail) {
        
        AuraProcessingMessage returnMessage = new AuraProcessingMessage();
        
        if (EmpDetail.Hire_Date__c > Datetime.now().date()) {
            returnMessage.isSuccess = false;
            returnMessage.message = 'Hire Date cannot be greater than today';
        }
        
        if (EmpDetail.Last_Pay_Date__c > Datetime.now().date()) {
            returnMessage.isSuccess = false;
            returnMessage.message = 'Last Pay Date cannot be greater than today';       
        }
        
        return returnMessage;    
    }
    
    public class EmploymentEntity {
        @AuraEnabled public Boolean hasErrorMessage { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public Opportunity opportunity { get; set; }
        @AuraEnabled public Employment_Details__c empDetail { get; set; }
        @AuraEnabled public List<String> warningMsgs { get; set; }
        @AuraEnabled public boolean isEmploymentTabReadOnly { get; set; }
        @AuraEnabled public Boolean isFSReadOnly { get; set; }
        @AuraEnabled public Boolean isReadOnly { get; set; }
        
        public EmploymentEntity() {
            this.warningMsgs = new List<String>();
        }
        
        public void setErrorMessage(String errorMessage ){
            this.errorMessage = errorMessage;
            this.hasErrorMessage = true;
        }
        
        public void setOpportunity(Opportunity opportunity) {
            this.opportunity = opportunity;
        }
        
        public void setEmployementDetails(Employment_Details__c empDetail) {
            this.empDetail = empDetail;
        }
        
        public void setWarnings(String warnMsgs) {
            this.warningMsgs.add(warnMsgs); 
        }
        
        public void setReadOnly(String readOnly) {
            if(readOnly == 'Read')
                this.isReadOnly = true;
            if(readOnly == 'FSRead')
                this.isFSReadOnly = true;
        }
    }
    
    public class AuraProcessingMessage { 
        
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public String message;
        
        public AuraProcessingMessage(){
            isSuccess = true;
            message = '';
        }
    }
    
    @AuraEnabled
    public static string saveFSassigned(String oppId, String newFS){
        try{
            opportunity opp = [Select Id, FS_Assigned_To_Val__c
                               FROM Opportunity
                               WHERE  Id =: oppId];
            
            opp.FS_Assigned_To_Val__c = newFS;
			update opp;
            return 'Success';    
        }
        catch (exception e){
            return 'Exception: ' + e;
        }
    }
}