public class AssignThreadToLoanContracts implements Database.Batchable<SObject>{
    //public Map<Integer, Set<ID>> loanAccountMap;
    public Set<ID> loanAccountList;
    public Integer batchNumber = (Integer)loan.CustomSettingsUtil.getOrgParameters().loan__Concurrent_BatchJobs__c;
    public Boolean noMoreExecution = False;
    public Date sodDate;
    
    public AssignThreadToLoanContracts(){
        //loanAccountMap = new Map<Integer, Set<ID>>();
        if(this.batchNumber == null){
            this.batchNumber = 1;
        }
    }
    
    private String getQuery(){
        String AllowabledLoanStatuses = '\'' + loan.LoanConstants.LOAN_STATUS_ACTIVE_GOOD_STANDING + '\'' + ',' +
                                        '\''+ loan.LoanConstants.LOAN_STATUSACTIVE_BAD_STANDING + '\'' + ',' +
                                        '\''+ loan.LoanConstants.LOAN_STATUS_ACTIVE_MATURED + '\''+ ',' +
                                        '\''+ loan.LoanConstants.LOAN_STATUS_CLOSED_OBLIGATIONS_MET +'\''+ ','+
                                        '\''+ loan.LoanConstants.LOAN_STATUS_CLOSED_RESCHEDULED +'\''+','+
                                        '\''+ loan.LoanConstants.LOAN_STATUS_APPROVED +'\''+','+
                                        '\''+ loan.LoanConstants.LOAN_STATUS_CLOSED_WRITTEN_OFF +'\'';
        return 'SELECT ID,loan__LoanCnt__c FROM loan__Loan_Account__c' 
           + ' WHERE loan__Loan_Status__c in (' +AllowabledLoanStatuses+')';
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(getQuery());
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> scope){
        List<loan__Loan_Account__c> loanIDs = (List<loan__Loan_Account__c>) scope;
        if(batchNumber == null) {
            batchNumber = 1;
        }
        Integer batchCount = 1;
        for(loan__Loan_Account__c loanId : loanIDs){
            
            if(batchCount == (batchNumber+1)){
                    batchCount = 1;
                }
                loanId.loan__LoanCnt__c = batchCount;
                batchCount++;
        }
        update loanIDs;
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
    
    
}