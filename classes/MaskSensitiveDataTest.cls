@isTest
public class MaskSensitiveDataTest
{
    public static @isTest void runTest()
    {   
        Account acc = new Account();
		acc.Name ='Test';
		acc.Email__C = 'Test@test.com';
		insert acc;
		
		Test.startTest();
		
        // Using any Ids as orgId and sandboxId for test, e.g. Account Ids 
        // Id possible pass valid id
        Test.testSandboxPostCopyScript(
            new MaskSensitiveData(), 
            String.valueOf(acc.Id), 
            String.valueOf(acc.Id), 
            'MySandboxName'
        ); 
			
		Test.stopTest();
    }
}