global class CollectionsCasesAssignmentUsersStructure {

    public CollectionsCasesAssignmentTeam dmcTeam, lmsTeam, collectionsTeam;
    public List<Id> dmcUsersIdsList, lmsUsersIdsList, collectionsUsersIdsList;
    public Map<String, CollectionsCasesAssignmentTeam> teamsMap;
    public Map<String, List<Id>> teamUsersIdsMap;
    public Map<String, String> groupsInTeamsMap;

    public CollectionsCasesAssignmentUsersStructure() {
        this.teamsMap = new Map<String,CollectionsCasesAssignmentTeam>();
        this.teamUsersIdsMap = new Map<String,List<Id>>();
        this.groupsInTeamsMap = new Map<String, String>();
        initCollectionsTeam();
        initLMSTeam();
        initDMCTeam();
    }

    public void initCollectionsTeam(){
        this.collectionsTeam = new CollectionsCasesAssignmentTeam.CollectionsTeam(
                new List<CollectionsCasesAssignmentUsersGroup>{
                        new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,1,30)
                        ,new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2,31,60)
                        ,new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3,61,90)
                        ,new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_4,91,120)
                        ,new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_5,121,180)
                }
        );
        this.teamsMap.put(CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS,collectionsTeam);
        this.collectionsUsersIdsList = this.collectionsTeam.getAllTeamMembersIds();
        this.teamUsersIdsMap.put(CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS,this.collectionsUsersIdsList);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_1,CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_2,CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_3,CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_4,CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LEVEL_5,CollectionsCasesAssignmentTeam.TEAM_COLLECTIONS);
    }

    public void initLMSTeam(){
        this.lmsTeam = new CollectionsCasesAssignmentTeam.LmsTeam(
                new List<CollectionsCasesAssignmentUsersGroup>{
                        new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_1,1,30),
                        new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_2,31,60),
                        new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_3,61,90),
                        new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_4,91,120),
                        new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_5,121,180)
                }
        );
        this.teamsMap.put(CollectionsCasesAssignmentTeam.TEAM_LMS,lmsTeam);
        this.lmsUsersIdsList = this.lmsTeam.getAllTeamMembersIds();
        this.teamUsersIdsMap.put(CollectionsCasesAssignmentTeam.TEAM_LMS,this.lmsUsersIdsList);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_1,CollectionsCasesAssignmentTeam.TEAM_LMS);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_2,CollectionsCasesAssignmentTeam.TEAM_LMS);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_3,CollectionsCasesAssignmentTeam.TEAM_LMS);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_4,CollectionsCasesAssignmentTeam.TEAM_LMS);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_LMS_LEVEL_5,CollectionsCasesAssignmentTeam.TEAM_LMS);
    }

    public void initDMCTeam(){
        this.dmcTeam = new CollectionsCasesAssignmentTeam.DmcTeam(
                new List<CollectionsCasesAssignmentUsersGroup>{
                        new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_1,1,30)
                        ,new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_2,31,90)
                        ,new CollectionsCasesAssignmentUsersGroup(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_3,91,180)
                }
        );
        this.teamsMap.put(CollectionsCasesAssignmentTeam.TEAM_DMC,dmcTeam);
        this.dmcUsersIdsList = this.dmcTeam.getAllTeamMembersIds();
        this.teamUsersIdsMap.put(CollectionsCasesAssignmentTeam.TEAM_DMC,this.dmcUsersIdsList);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_1,CollectionsCasesAssignmentTeam.TEAM_DMC);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_2,CollectionsCasesAssignmentTeam.TEAM_DMC);
        this.groupsInTeamsMap.put(CollectionsCasesAssignmentUsersGroup.COLLECTIONS_GROUP_DMC_LEVEL_3,CollectionsCasesAssignmentTeam.TEAM_DMC);
    }

    public Boolean isUserMemberOf(Id theUserId, String teamName){
        if(String.isEmpty(theUserId) || String.isEmpty(teamName)) return false;
        return this.teamUsersIdsMap.get(teamName) != null ? new Set<Id>(this.teamUsersIdsMap.get(teamName)).contains(theUserId) : false;
    }

    public Boolean isUserMemberOfAnyTeam(Id theUserId){
        if(String.isEmpty(theUserId)) return false;
        return new Set<Id>(this.collectionsUsersIdsList).contains(theUserId)
                || new Set<Id>(this.lmsUsersIdsList).contains(theUserId)
                || new Set<Id>(this.dmcUsersIdsList).contains(theUserId);
    }

    public String getTeamGroupNameByDpd(Integer dpd, String teamName){
        if(dpd == null || dpd == 0 || String.isEmpty(teamName)) return null;
        return this.teamsMap.get(teamName) != null ? this.teamsMap.get(teamName).getGroupNameByDpd(dpd) : null;
    }

    public Boolean isTriggerHit(String teamName, Id theUserId, Integer dpd){
        if(String.isEmpty(teamName) || String.isEmpty(theUserId) || dpd == null) return false;
        return this.teamsMap.get(teamName) != null ? this.teamsMap.get(teamName).isTriggerHit(theUserId, dpd) : false;
    }
}