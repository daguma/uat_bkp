@isTest public class DecisioningParameterTest {

    @isTest static void validate() {

        DecisioningParameter dp = new DecisioningParameter();
        dp.entityId = 'test';
        System.assertEquals('test', dp.entityId);

        dp.entityName = 'test';
        System.assertEquals('test', dp.entityName);

        dp.income = 'test';
        System.assertEquals('test', dp.income);

        dp.employmentType = 'test';
        System.assertEquals('test', dp.employmentType);

        dp.employmentDuration = 'test';
        System.assertEquals('test', dp.employmentDuration);

        dp.employmentGapDurationDays = 'test';
        System.assertEquals('test', dp.employmentGapDurationDays);

        dp.priorEmploymentDuration = 'test';
        System.assertEquals('test', dp.priorEmploymentDuration);

        dp.useOfFunds = 'test';
        System.assertEquals('test', dp.useOfFunds);

        dp.isPartner = 'test';
        System.assertEquals('test', dp.isPartner);

        dp.loanAmount = 'test';
        System.assertEquals('test', dp.loanAmount);

        dp.paymentFrequency = 'test';
        System.assertEquals('test', dp.paymentFrequency);

        dp.analyticRandomNumber = 'test';
        System.assertEquals('test', dp.analyticRandomNumber);

        dp.feeHandling = 'test';
        System.assertEquals('test', dp.feeHandling);

        dp.partner = 'test';
        System.assertEquals('test', dp.partner);

        dp.isFinwise = true;
        System.assertEquals(true, dp.isFinwise);

        dp.productCode = 'test';
        System.assertEquals('test', dp.productCode);

        dp.applicationType = 'test';
        System.assertEquals('test', dp.applicationType);

        dp.previousGrade = 'test';
        System.assertEquals('test', dp.previousGrade);

        dp.previousTerm = 'test';
        System.assertEquals('test', dp.previousTerm);
        
        dp.fundedApr = 'test';
        System.assertEquals('test', dp.fundedApr);
        
        dp.currentPayoffBalance = 'test';
        System.assertEquals('test', dp.currentPayoffBalance);
        
        dp.isTest = True;
        System.assertEquals(True, dp.isTest);
        
        dp.isMrcState = True;
        System.assertEquals(True, dp.isMrcState);
        
        dp.merchantState = 'AL';
        System.assertEquals('AL', dp.merchantState);
        
        dp.bankName = 'Test Bank';
        System.assertEquals('Test Bank', dp.bankName);
        
        dp.tbsLogic = 'Testing';
        System.assertEquals('Testing', dp.tbsLogic);
        
        
    }
}