@isTest
private class BatchOfferRegenerationTest {

    @isTest static void deletes_offers(){
     
        Opportunity opp = LibraryTest.createApplicationTH();
        Offer__c off = new Offer__c();
        off.Opportunity__c = opp.Id;
        off.Loan_Amount__c = 500;
        insert off;
        
		Offer_Regeneration_List__c offList = new Offer_Regeneration_List__c();
        offList.OpportunityId__c = opp.Id;
        insert offList;
        
        Integer offBefore = [SELECT COUNT() FROM Offer__c WHERE Opportunity__c =: opp.Id];
        
        Test.startTest();
        BatchOfferRegeneration job = new BatchOfferRegeneration('deleteOffers');
    	database.executebatch(job, 10);
        Test.stopTest();
        
        System.assert(offBefore > [SELECT COUNT() FROM Offer__c WHERE Opportunity__c =: opp.Id], 'Delete Offers');        
    }
    
    @isTest static void creates_offers_no_scorecard(){
     
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.PrimaryCreditReportId__c = 999;
        opp.FICO__c = '600';
        update opp;
        
		Offer_Regeneration_List__c offList = new Offer_Regeneration_List__c();
        offList.OpportunityId__c = opp.Id;
        insert offList;
        
        Test.startTest();
        BatchOfferRegeneration job = new BatchOfferRegeneration('generateOffers');
    	database.executebatch(job, 10);
        Test.stopTest();
    }
    
    @isTest static void creates_offers_with_score(){
     
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.PrimaryCreditReportId__c = 999;
        opp.FICO__c = '600';
        update opp;
        
        Scorecard__c scr = new Scorecard__c();
        scr.Acceptance__c = 'Y';
        scr.Grade__c = 'B1';
        scr.Opportunity__c = opp.Id;
        insert scr;
        
		Offer_Regeneration_List__c offList = new Offer_Regeneration_List__c();
        offList.OpportunityId__c = opp.Id;
        insert offList;
        
        Test.startTest();
        BatchOfferRegeneration job = new BatchOfferRegeneration('generateOffers');
    	database.executebatch(job, 10);
        Test.stopTest();
    }
    
    @isTest static void schedule_job() {
        Test.startTest();

        BatchOfferRegeneration offRegJob = new BatchOfferRegeneration();
        String cron = '0 0 23 * * ?';
        system.schedule('Test Offer Regeneration Job', cron, offRegJob);

        Test.stopTest();
    }
}