public class GDSExternalParamsMapper {

    public static map<String, String> mappInformation(String opportunityId, String contactId) {
        String query = 'SELECT ';
        String objectForQuery = '';
        String lastObject = '';
        Integer counter = 1;
        String whereCondition = '';
        String afterFrom = '';
        List<sObject> records = null;
        List<GDS_Request_Params__c> recordsPerObj = null;
        map<String, String> ExternalParams = new map<String, String>();
        List<GDS_Request_Params__c> requestParameters = [SELECT Id, Name, ObjectName__c, PropertyName__c, ExternalName__c, IsDuplicated__c, IsActive__c
                FROM GDS_Request_Params__c
                WHERE IsActive__c = TRUE
                ORDER BY ObjectName__c ASC, PropertyName__c ASC];

        for (GDS_Request_Params__c requestParam : requestParameters) {
            objectForQuery = requestParam.ObjectName__c;

            if (!lastObject.equalsIgnoreCase(objectForQuery))
                recordsPerObj = getValuesPerObject(objectForQuery, requestParameters);
            
            if (!((counter) == recordsPerObj.size())) {
                query += ((requestParam.IsDuplicated__c ) ? '' : (query.length() <= 8 ? requestParam.PropertyName__c :  ', ' + requestParam.PropertyName__c));
            } else {
                afterFrom = ' FROM ' + objectForQuery + getWhereCondition(objectForQuery, opportunityId, contactId);
                query += (requestParam.IsDuplicated__c) ? '' + afterFrom :  (query.length() <= 8) ? requestParam.PropertyName__c + afterFrom :  ', ' + requestParam.PropertyName__c + afterFrom;

                records =  Database.query(query);

                if (records.size() > 0) {
                    Time t = Time.newInstance(0,0,0,0);
                    for (sObject obj : records)
                        for (GDS_Request_Params__c param : recordsPerObj){
                            Object f = obj.get(param.PropertyName__c);
                            if ( f != null && f instanceOf Date)
                                ExternalParams.put(param.ExternalName__c,  Datetime.newInstanceGmt((Date)f, t).format('yyyy-MM-dd'));
                            else
                                ExternalParams.put(param.ExternalName__c, String.valueOf(f));
                        }           
                } else {
                    for (GDS_Request_Params__c param : recordsPerObj)
                        ExternalParams.put(param.ExternalName__c, null);
                }
                query = 'SELECT ';
                counter = 0;
            }

            lastObject = objectForQuery;
            counter++;
        }
        return ExternalParams;
    }

    /*
    	Getting the properties by objects,
    	It's necessary every time that
    	we execute a query.
    */

    public static List<GDS_Request_Params__c> getValuesPerObject(String objectName, List<GDS_Request_Params__c> params) {
        List<GDS_Request_Params__c> objValues = new List<GDS_Request_Params__c>();
        for (GDS_Request_Params__c param : params) {
            if (param.ObjectName__c == objectName) {
                objValues.add(param);
            }
        }
        return objValues;
    }

    /*
    	Since we're working with many objects,
    	we need a way to filter the records
    	in each one of them.
    */
    public static String getWhereCondition(String objectName, String oppId, String contactId) {
        String whereCondition = ' WHERE ';
        String auxId = '';

        if (objectName.equalsIgnoreCase('offer__c') || objectName.equalsIgnoreCase('employment_details__c'))
            whereCondition += 'Opportunity__c = \'' + oppId + '\'';
        if (objectName.equalsIgnoreCase('opportunity'))
            whereCondition  += 'id = \'' + oppId + '\'';
        if (objectName.equalsIgnoreCase('contact'))
            whereCondition += 'id = \'' + contactId + '\'';
        if (objectName.equalsIgnoreCase('account')) {
            for (Opportunity opp : [SELECT Partner_Account__c FROM Opportunity WHERE Id = :oppId])
                whereCondition += 'id = \'' + (opp.Partner_Account__c == null ? '' : opp.Partner_Account__c ) + '\'';
        }

        return whereCondition;
    }

}