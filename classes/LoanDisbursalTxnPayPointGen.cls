global class LoanDisbursalTxnPayPointGen extends loan.FileGenerator {
    private Map<Id,loan__Loan_Disbursal_Transaction__c> disbMap = new Map<Id,loan__Loan_Disbursal_Transaction__c>();
    private loan__Bank_Account__c bank;
    Integer linecount=0, totalentry=0, iCurrent =0;
    Double blocks =0.0, routingHash =0; 
    public String fileDestination = '';
    private loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();
    private List<Partner_Product_Bank_Account__c> accountBanks ;
    private List<Provider_Payments__c> providerPay;
    private Set<Id> accsWProv = new Set<Id>();
    Decimal creditTotal = 0;
    private static String[] abc = new String[] {'A','B','C','D','E','F','G','H','I','J','K',
                                    'L','M','N','O','P','Q','R','S','T','U','V',
                                    'W','X','Y','Z'};
    
    
    public override String getSimpleFileName() {
        return 'Loan_Disbursals';
    }

    public List<filegen__Entry_Detail_Record__c> processProviderPayments(loan__Loan_Disbursal_Transaction__c txn) {
        loan__Loan_Account__c loanAccount = txn.loan__Loan_Account__r;
        Account pAcc = loanAccount.Original_Partner_Account__r;
        Decimal amnt = ((loanAccount.loan__Loan_Amount__c - ( loanAccount.Reporting_Fee__c == null ? 0 : loanAccount.Reporting_Fee__c) - (txn.Refinance_Payoff_Amount__c != null ? txn.Refinance_Payoff_Amount__c: 0)).setScale(2));
        if (pAcc != null && pAcc.Disburse_without_Discount__c && loanAccount.LoanHero_Merchant_Discount_Amount__c != null  && loanAccount.LoanHero_Merchant_Discount_Amount__c < 0) amnt = amnt + loanAccount.LoanHero_Merchant_Discount_Amount__c;
        System.debug('Full Amount to Disburse = ' + amnt);
        List<filegen__Entry_Detail_Record__c> ds = new List<filegen__Entry_Detail_Record__c>();
        for (Provider_Payments__c p : providerPay) {
            if (p.Opportunity__r.Lending_Account__c ==  txn.loan__Loan_Account__c && p.Payment_Amount__c > 0 && amnt > 0) {
                Decimal paym = p.Payment_Amount__c > amnt ? amnt : p.Payment_Amount__c;
                ds.add(generateEntryDetailRecord(txn,  p.Provider_Name__c , paym));
                amnt =  amnt - paym;
            }
        }
        if (amnt > 0) ds.add(generateEntryDetailRecord(txn,  null, amnt));
        return ds;
    }

    public override List<String> getEntries(loan.TransactionSweepToACHState state, List<SObject> scope) {
        List<String> retVal = new List<String>();
        try {
            system.debug(loggingLevel.DEBUG,'----:'+'inside entries');
            List<loan__Loan_Disbursal_Transaction__c> disbursals = requeryScope(scope);
            List<SObject> objects = new List<SObject>();
            for(loan__Loan_Disbursal_Transaction__c dTxn : disbursals){
                iCurrent++;
                 loan__Loan_Account__c loanAccount = dTxn.loan__Loan_Account__r;
 
                if(!dTxn.loan__Reversed__c && 
                   ((loanAccount.loan__Borrower_ACH__r.loan__Routing_Number__c != null && 
                     loanAccount.loan__Borrower_ACH__r.loan__Bank_Account_Number__c !=null) || 
                    (loanAccount.Opportunity__c != null &&  loanAccount.Opportunity__r.Is_Direct_Pay_On__c ) ) && 
                    loanAccount.loan__Loan_Amount__c != null ){
                    addToValidScope(disbMap.get(dTxn.loan__Loan_Account__c));
                    if((String.isEmpty(fileDestination) && !loanAccount.Finwise_Originated__c ) || (fileDestination != null && fileDestination == 'FINWISE' && dTxn.loan__Loan_Account__r.Finwise_Originated__c)) {
                           if (accsWProv.contains(dTxn.loan__Loan_Account__c) && acha.Disburse_to_Providers__c ) {
                                System.debug('Inside because has provider payment');
                                for (filegen__Entry_Detail_Record__c d : processProviderPayments(dTxn) ) {
                                    objects.add(d);
                                }
                            } else
                                objects.add(generateEntryDetailRecord(dTxn, dTxn.loan__Loan_Account__r.Opportunity__r.Partner_Account__c, null));

                            if ((fileDestination != null && fileDestination == 'FINWISE') && dTxn.Refinance_Payoff_Amount__c != null && dTxn.Refinance_Payoff_Amount__c > 0 ){
                                totalentry++;
                                objects.add(createDetailRecord('22', acha.Finwise_Origin_ABA__c, String.valueOf(acha.Finwise_Refinance_Account__c), dTxn.Refinance_Payoff_Amount__c, loan.StringUtils.rightPadString(dTxn.loan__Loan_Account__r.Name.replace('-','0'),15, ' '), 'LENDINGPOINT REFI',  acha.Finwise_Origin_ABA__c.left(8)+loan.StringUtils.leftPadString(string.valueof(totalentry),7,'0')));
                                creditTotal += dTxn.Refinance_Payoff_Amount__c.setScale(2);
                                routingHash += Double.valueOf( acha.Finwise_Origin_ABA__c.left(8)) ;
                            }
                        }           
                }
                else{
                    addToInvalidScope(disbMap.get(dTxn.loan__Loan_Account__c),'Borrower Ach routing no, Loan account ACH Account number, Disbursal Reversed or Loan amount is missing');
                    System.debug('Invalid scope for Disbursal = ' + dTxn.id);
                }
            }
            
            // If Finwise, add finwise record debit
             if (fileDestination != null && fileDestination == 'FINWISE'){
                 totalentry++;
                 filegen__Entry_Detail_Record__c eDR = createDetailRecord('27', acha.Finwise_Origin_ABA__c, String.valueOf(acha.FINWISE_debit_Account__c), creditTotal, 
                                                    loan.StringUtils.rightPadString(' ',15, ' '), acha.loan__Organization_Name__c, acha.Finwise_Origin_ABA__c.left(8)+loan.StringUtils.leftPadString(string.valueof(totalentry),7,'0'));
                 routingHash += Double.valueOf( acha.Finwise_Origin_ABA__c.left(8)) ;
                 System.debug('Accumulated total = ' + creditTotal);
                objects.add(eDR);
            }
            filegen.CreateSegments segments = new filegen.CreateSegments(objects);
            retVal = segments.retString();
            for(String line:retVal){
                line += '\n';
                addEntry(line);
            }
            
        } catch (Exception e) {
             WebToSFDC.notifyDev('LoanDisbursalTxnPayPointGen ERROR', e.getMessage() + '\n' + e.getStackTraceString() + '\n' + e.getLineNumber() );
        }
        return retVal;
    }
    @TestVisible
    private filegen__Entry_Detail_Record__c createDetailRecord(String tCode, String aba, String debitAccount, Decimal amount, String idNumber, String iName, String tNumber) {
         filegen__Entry_Detail_Record__c eDR = new filegen__Entry_Detail_Record__c();
         eDR.filegen__Transaction_Code__c = tCode;
         eDR.filegen__RDFI_Identification__c = aba;
         eDR.filegen__Check_Digit__c = aba.right(1);
         eDR.filegen__RDFI_Account_Number__c = debitAccount;
         eDR.filegen__Amount__c = String.valueOf(amount.setScale(2));
         eDR.filegen__Individual_Identification_Number__c = idNumber;
         eDR.filegen__Individual_Name__c = iName;
         eDR.filegen__Addenda_Record_Indicator__c='0';
         eDR.filegen__Trace_Number__c = tNumber;
         return eDR;
    }

    public override String getHeader(loan.TransactionSweepToACHState state, List<SObject> scope) {
        List<String> retVal = new List<String>();
        List<SObject> headerRecs = new List<SObject>();
        headerRecs.add(generateFileHeaderRecord(state));
        headerRecs.add(generateBatchHeaderRecord());
        filegen.CreateSegments segments =new filegen.CreateSegments(headerRecs);
        retVal = segments.retString();
        return retVal[0]+'\r\n' + retVal[1] + '\r\n';
    }
    
    public override String getTrailer(loan.TransactionSweepToACHState state, LIST<SObject> scope) {
        String achFileTrailerContent = '';
        List<SObject> trailerRecs = new List<SObject>();
        trailerRecs.add(generateBatchControlRecord(scope));
        trailerRecs.add(generateFileControlRecord(scope));
        filegen.CreateSegments segments =new filegen.CreateSegments(trailerRecs);

        for(String s : segments.retString()){
            achFileTrailerContent += s + '\r\n';
        }
        
        achFileTrailerContent = achFileTrailerContent.subString( 0 , achFileTrailerContent.length() - 2 );
        //fillers are added to make number of characters in generated file multiple of 940
        for(Integer i = 0 ; i < ( Integer.valueOf( blocks * 10 ) - linecount) ; i++ ) {
            achFileTrailerContent += '\r\n'+ rightPadString('', 94,'9');
        }
        
        return achFileTrailerContent;
    }
    //File header
    
    private filegen__File_Header_Record__c generateFileHeaderRecord(loan.TransactionSweepToACHState state){
        Datetime currentSystemDateTime = DateTime.now();
        String banksRoutingNo = '', myTime = String.valueOf(currentSystemDateTime.hour()) + String.valueOf(currentSystemDateTime.minute());
        filegen__File_Header_Record__c fHR = new filegen__File_Header_Record__c();

        if(bank !=null && bank.loan__Routing_Number__c != null){
            banksRoutingNo =loan.StringUtils.leftPadString(''+bank.loan__Routing_Number__c,9,'0');
        }
        if (String.isEmpty(fileDestination)) {
            fHR.filegen__Immediate_Origin__c = (acha.loan__ACH_Id__c!=null?acha.loan__ACH_Id__c:'');
            fHR.filegen__Immediate_Origin_Name__c = acha.loan__Organization_Name__c;
            fHR.filegen__Immediate_Destination__c = banksRoutingNo;
            fHR.filegen__Immediate_Destination_Name__c = (bank != null && bank.loan__Bank_Name__c != null)? bank.loan__Bank_Name__c: '';
        } else if (fileDestination == 'FINWISE'){
            fHR.filegen__Immediate_Origin__c = acha.Finwise_Origin_ABA__c;
            fHR.filegen__Immediate_Origin_Name__c = acha.Finwise_Origin_Name__c;
            fHR.filegen__Immediate_Destination__c = acha.FINWISE_Destination_Number__c;
            fHR.filegen__Immediate_Destination_Name__c = acha.FINWISE_Destination_Name__c;
        }
        fHR.filegen__Creation_Date__c = currentSystemDateTime.date();
        fHR.filegen__Creation_Time__c = myTime;
        fHR.filegen__ID_Modifier__c = abc[state.counter] ;
        fHR.filegen__Reference_Code__c = acha.loan__ACH_Id__c;
        return fHR;
    }
    
    //Batch Header
     private filegen__Batch_Header_Record__c generateBatchHeaderRecord(){
        String banksRoutingNo = '';
        Date csd = Date.today();
        filegen__Batch_Header_Record__c bHR = new filegen__Batch_Header_Record__c();
        
        bHR.filegen__Service_Class_Code__c = fileDestination == 'FINWISE' ? '200' : '220';
        bHR.filegen__Company_Name__c = loan.CustomSettingsUtil.getACHParameters().loan__Organization_Name__c;
        bHR.filegen__Company_Discretionary_Data__c = 'ACH_RECEIPTS';

        bHR.filegen__Company_Identification__c = acha.loan__ACH_Id__c;
        bHR.filegen__SEC_Code__c = 'PPD';
        bHR.filegen__Company_Entry_Description__c = 'PAYMENT';
        bHR.filegen__Company_Descriptive_Date__c = csd;
        bHR.filegen__Effective_Entry_Date__c = csd;
        if(bank != null && bank.loan__Routing_Number__c != null){
            banksRoutingNo = String.valueOf(bank.loan__Routing_Number__c);
        }
        
        if(banksRoutingNo.length()<9){
            banksRoutingNo=loan.StringUtils.leftPadString(''+bank.loan__Routing_Number__c,9,'0');
        }
        if (String.isEmpty(fileDestination)) {
            bHR.filegen__Originating_DFI_Identification__c = banksRoutingNo;
        } else if (fileDestination == 'FINWISE'){
            bHR.filegen__Originating_DFI_Identification__c = acha.Finwise_Origin_Number__c;
        }
        bHR.filegen__Batch_Number__c = '0000123';
        return bHR;
    }
    
    
    //Entry Detail
    @TestVisible
    private filegen__Entry_Detail_Record__c generateEntryDetailRecord(loan__Loan_Disbursal_Transaction__c disbursalTxn, Id ParnerAccId, Decimal overAmount){
        loan__Loan_Account__c loanAccount = disbursalTxn.loan__Loan_Account__r;
        Account pAcc = loanAccount.Original_Partner_Account__r;

        String checkDigit = '', banksrouting, customerRoutingNo = '', customerAccountNo = '', customerLoanName, accountType =  loanAccount.loan__ACH_Account_Type__c;
        Double customerHashNo = 0;
        System.debug('PParnerAccId = ' + ParnerAccId);
        System.debug('overAmount + ' + overAmount);

        if(loanAccount.loan__ACH_Routing_Number__c != null) customerRoutingNo = String.valueOf(loanAccount.loan__ACH_Routing_Number__c);
        customerAccountNo = loanAccount.loan__ACH_Account_Number__c != null ?loanAccount.loan__ACH_Account_Number__c:'';
        
        String customerName = loanAccount.loan__ACH_Drawer_Name__c;
        if(loanAccount.loan__ACH_Relationship_Type__c == null || loanAccount.loan__ACH_Relationship_Type__c == loan.LoanConstants.ACH_RELATIONSHIP_TYPE_PRIMARY){ 
            if(loanAccount.loan__Contact__c != null){
                customerName = loanAccount.loan__Contact__r.Name;
            }
        } 

        for ( Partner_Product_Bank_Account__c aBank : accountBanks) {
            System.debug('Product : ' +  aBank.Product__c + ' = ' +  loanAccount.loan__Loan_Product_Name__c );
            System.debug('ParnerAccId : ' +  aBank.Account__c + ' = ' +  ParnerAccId );
            if (ParnerAccId != null && aBank.Account__c == ParnerAccId &&
                aBank.Product__c ==  loanAccount.loan__Loan_Product_Name__c ) {
                customerRoutingNo = aBank.Routing_Number__c;
                customerAccountNo = aBank.Bank_Account_Number__c;
                accountType = aBank.Account_Type__c;
                customerName = aBank.Account__r.Name + ' ' + String.valueOf(iCurrent);
                System.debug('Using Direct Vendor Pay account = ' + loanAccount.Name);

                break;
            }
        }

        customerRoutingNo=loan.StringUtils.leftPadString(''+customerRoutingNo,9,'0');

        totalentry++;
        if(customerRoutingNo != null && customerRoutingNo.length() >=8 ){
            checkDigit = customerRoutingNo.right(1);
        }
        
        if (fileDestination == 'FINWISE')
            banksRouting = acha.Finwise_Origin_Number__c;
        else if(bank != null && bank.loan__Routing_Number__c != null){
            banksRouting = String.valueOf(bank.loan__Routing_Number__c);
        }    

        if(banksRouting.length()<9) banksRouting=loan.StringUtils.leftPadString(''+banksRouting,9,'0');
        Decimal amnt = ((loanAccount.loan__Loan_Amount__c - ( loanAccount.Reporting_Fee__c == null ? 0 : loanAccount.Reporting_Fee__c) - (disbursalTxn.Refinance_Payoff_Amount__c != null ? disbursalTxn.Refinance_Payoff_Amount__c: 0)).setScale(2));
        if (overAmount != null && overAmount > 0) amnt = overAmount;
        else if (overAmount == null ) {
            if (pAcc != null && pAcc.Disburse_without_Discount__c && loanAccount.LoanHero_Merchant_Discount_Amount__c != null && loanAccount.LoanHero_Merchant_Discount_Amount__c < 0) amnt = amnt + loanAccount.LoanHero_Merchant_Discount_Amount__c;

        }

        if( customerRoutingNo != null && customerRoutingNo.length() >=8){
            customerHashNo = Double.valueOf( customerRoutingNo.left(8));
        }
        routingHash += customerHashNo ;
        creditTotal += amnt;

        filegen__Entry_Detail_Record__c eDR = createDetailRecord('22', customerRoutingNo,customerAccountNo , amnt,  loan.StringUtils.rightPadString(loanAccount.Name.replace('-','0'),15, ' '), customerName, banksRouting.left(8)+loan.StringUtils.leftPadString(string.valueof(totalentry),7,'0'));
        return eDR;
    }
    

    //Batch Control
    private filegen__Batch_Control_Record__c generateBatchControlRecord(LIST<SObject> scope){
        String identity = '', banksRoutingNo = '';
        filegen__Batch_Control_Record__c bCR = new filegen__Batch_Control_Record__c();
        
        bCR.filegen__Service_Class_Code__c = fileDestination == 'FINWISE' ? '200' : '220';
        bCR.filegen__Entry_Addenda_Count__c = String.valueOf(totalentry);
        bCR.filegen__Entry_Hash__c = routingHash.format().remove(',');
        bCR.filegen__Total_Credit_Entry_Dollar_Amount__c = String.valueOf(Integer.valueOf(creditTotal.setScale(2)*100));
        if (fileDestination == 'FINWISE') bCR.filegen__Total_Debit_Entry_Dollar_Amount__c = String.valueOf(Integer.valueOf(creditTotal.setScale(2)*100));
        if(bank != null && bank.ACH_Code__c != null){
            identity = loan.stringutils.leftpadString(bank.ACH_Code__c,10,'');
        }
        bCR.filegen__Company_Identification__c = (fileDestination == 'FINWISE') ? acha.Finwise_Origin_ABA__c : identity ;
        
        if(bank != null && bank.loan__Routing_Number__c != null){
            banksRoutingNo = String.valueOf(bank.loan__Routing_Number__c);
        }
        if(banksRoutingNo.length()<9){
            banksRoutingNo=loan.StringUtils.leftPadString(''+bank.loan__Routing_Number__c,9,'0');
        }
        if (fileDestination == 'FINWISE') banksRoutingNo = acha.Finwise_Origin_Number__c;
        bCR.filegen__Originating_DFI_Identification__c = banksRoutingNo;
        bCR.filegen__Batch_Number__c = '0000123';
        return bCR;
    }
    
    //File Control
    private filegen__File_Control_Record__c generateFileControlRecord(LIST<SObject> scope){
        filegen__File_Control_Record__c fCR = new filegen__File_Control_Record__c();
        
        linecount = totalentry+4;
        blocks = math.ceil((double.valueOf(linecount))/10);
        fCR.filegen__Batch_Count__c = '000001';
        fCR.filegen__Block_Count__c = String.valueOf(Integer.valueOf(blocks));
        fCR.filegen__Entry_Addenda_Count__c = String.valueOf(totalentry);
        fCR.filegen__Entry_Hash__c =  routingHash.format().remove(',');
        fCR.filegen__Total_Credit_Entry_Dollar_Amount_in_Fi__c = String.valueOf(Integer.valueOf(creditTotal.setScale(2)*100));
        if (fileDestination == 'FINWISE') fCR.filegen__Total_Debit_Entry_Dollar_Amount_in_Fil__c = String.valueOf(Integer.valueOf(creditTotal.setScale(2)*100));
        
        return fCR;
    }
    
    //requery scope
    private List<loan__Loan_Disbursal_Transaction__c> requeryScope(List<SObject>scope){
        Set<ID> ids = new Set<ID>();
        for(SObject s : scope) {
            ids.add(s.Id);
        }
        System.debug('Disbursal records to process = ' + scope.size());
        List<loan__Loan_Disbursal_Transaction__c> disbTxns = [select Id,loan__Disbursed_Amt__c,loan__Loan_Account__c,loan__Loan_Account__r.Name,
                                                                    loan__Loan_Account__r.Opportunity__r.Partner_Account__c,
                                                                    loan__Loan_Account__r.loan__ACH_Bank__c,
                                                                    loan__Loan_Account__r.Finwise_Originated__c,
                                                                    loan__Loan_Account__r.loan__Loan_Product_Name__c,
                                                                    loan__Loan_Account__r.loan__ACH_Bank__r.Name,
                                                                    loan__Loan_Account__r.loan__ACH_Routing_Number__c
                                                                    from loan__Loan_Disbursal_Transaction__c
                                                                    where Id in :ids];
        Set<Id> loanIds = new Set<Id>(), pAccs = new Set<Id>();

        for (loan__Loan_Disbursal_Transaction__c tran : disbTxns){
            loanIds.add(tran.loan__Loan_Account__c);
            disbMap.put(tran.loan__Loan_Account__c,tran);
            if (tran.loan__Loan_Account__r.Opportunity__r.Partner_Account__c != null && !pAccs.contains(tran.loan__Loan_Account__r.Opportunity__r.Partner_Account__c )) {
                pAccs.add(tran.loan__Loan_Account__r.Opportunity__r.Partner_Account__c);
            }
        }


        providerPay = [Select Id, Opportunity__c, Opportunity__r.Lending_Account__c,   Payment_Amount__c, Procedure_Date__c, Provider_Name__c  from Provider_Payments__c where Opportunity__c in (select Opportunity__c from loan__Loan_Account__c where Id in :  loanIds)];
        for (Provider_Payments__c p : providerPay) {
            if (p.Provider_Name__c !=null && !pAccs.contains(p.Provider_Name__c)) {
                pAccs.add(p.Provider_Name__c);
            }
            if (!accsWProv.contains(p.Opportunity__r.Lending_Account__c)) accsWProv.add(p.Opportunity__r.Lending_Account__c);
        }
        System.debug('Provider Payments = ' + providerPay.size());
        System.debug('Accounts = ' + pAccs.size());

        accountBanks =  [select Id, Account__c, Account_Type__c, Bank_Account_Number__c, Deposit_To_vendor__c, Product__c, Routing_Number__c, Account__r.Name from Partner_Product_Bank_Account__c 
                        where   Deposit_to_Vendor__c = true and     
                                Bank_Account_Number__c <> '' and 
                                Routing_Number__c <> '' and
                                Product__c <> null and Account__c in : pAccs
                        order by Account__r.Disburse_without_Discount__c asc ];  
        System.debug('Partner_Product_Bank_Account__c = ' + accountBanks.size());

        try{
            bank = [select Id,Name,loan__Bank_Account_Number__c,loan__Account__c,loan__Account__r.Name,
                                                loan__Account_Type__c,
                                                loan__Account_Usage__c,
                                                loan__Bank_Name__c,
                                                //ACH_Code__c,
                                                loan__Contact__c,loan__Contact__r.Name,
                                                loan__Routing_Number__c,
                                                ACH_code__c
                                                from loan__Bank_Account__c
                                                where loan__Account_Usage__c =: 'Advance Trust Account'
                                                and loan__Active__c = true limit 1];
        }catch(Exception e){
            //throw new TransactionException('Advance Trust Account is not available');
        }

        mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
       
        if (ec.getObject('disbursementForACH') != null) ec.deleteObject('disbursementForACH');
        mfiflexUtil.ObjectCache dsbOC = ec.createObject('disbursementForACH',
                                'loan__Loan_Disbursal_Transaction__c','');
        dsbOC.addFields('Id,loan__Disbursed_Amt__c,loan__Loan_Account__c,loan__Reversed__c, '+
                        'loan__Loan_Account__r.Finwise_Originated__c,' + 
                        'loan__Loan_Account__r.Name, '+
                        'loan__Loan_Account__r.loan__ACH_Drawer_Name__c,'+
                        'loan__Loan_Account__r.loan__ACH_Relationship_Type__c,'+ 
                        'loan__Loan_Account__r.loan__Borrower_ACH__c,'+
                        'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Account__c,'+
                        'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Account_Type__c,'+
                        'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Account_Usage__c,'+
                        'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Active__c,'+
                        'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Bank_Account_Number__c,'+
                        'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Bank_Name__c,'+
                        'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Contact__c,'+
                        'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Routing_Number__c,'+
                        'loan__Loan_Account__r.loan__ACH_Account_Number__c,'+
                        'loan__Loan_Account__r.loan__ACH_Account_Type__c,'+
                        'loan__Loan_Account__r.loan__ACH_Debit_Amount__c,'+
                        'loan__Loan_Account__r.loan__Ach_Debit_Day__c,'+
                        'loan__Loan_Account__r.loan__ACH_Drawer_Name__c,'+
                        'loan__Loan_Account__r.loan__ACH_End_Date__c,'+
                        'loan__Loan_Account__r.loan__ACH_Frequency__c,'+
                        'loan__Loan_Account__r.loan__ACH_Next_Debit_Date__c,'+
                        'loan__Loan_Account__r.loan__ACH_Routing_Number__c,'+
                        'loan__Loan_Account__r.loan__Loan_Amount__c,'+
                        'loan__Loan_Account__r.loan__Account__c,'+
                        'loan__Loan_Account__r.loan__Account__r.Name,'+
                        'loan__Loan_Account__r.loan__Contact__c,'+
                        'loan__Loan_Account__r.loan__Contact__r.Name,'+
                        'loan__Loan_Account__r.loan__ACH_Bank__c,'+
                        'loan__Loan_Account__r.loan__Loan_Product_Name__c,'+
                        'loan__Loan_Account__r.loan__ACH_Bank__r.Name,'+
                        'loan__Loan_Account__r.loan__ACH_Bank__r.loan__ACH_Code__c,'+
                        'loan__Loan_Account__r.loan__ACH_Bank__r.loan__Routing_Number__c,' +
                        'loan__Loan_Account__r.LoanHero_Merchant_Discount_Amount__c,' +
                        'loan__Loan_Account__r.Original_File_Disbursal_Amount__C,' +
                        'loan__Loan_Account__r.Opportunity__r.Amount,'+
                        'loan__Loan_Account__r.Opportunity__r.Finwise_Approved__c,' + 
                        'loan__Loan_Account__r.Opportunity__r.Is_Direct_Pay_On__c,' + 
                        'loan__Loan_Account__r.Opportunity__r.Partner_Account__c,' + 
                        'loan__Loan_Account__r.Opportunity__r.Remaining_Refinance_Amount__c,' + 
                        'loan__Loan_Account__r.Original_Partner_Account__c,' +
                        'loan__Loan_Account__r.Original_Partner_Account__r.Disburse_without_Discount__c,' +
                        'loan__Loan_Account__r.Reporting_Fee__c,'+
                        'Refinance_Payoff_Amount__c');
        
        
        dsbOC.addNamedParameter('loanIds', loanIds);
        dsbOC.setWhereClause('loan__Loan_Account__c IN :loanIds');
        dsbOC.executeQuery();
        return (List<loan__Loan_Disbursal_Transaction__c>) dsbOC.getRecords();
    }
    
    String rightPadString(String str, Integer size, String padString) {    
        if (str == null) return null;        
       
        if(str.length()>size) return str.mid(0,size);
        
        return str.rightPad(size,padString);
    }
}