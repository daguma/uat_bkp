/**
 * Class OpportunityHandler
 *
 * Trigger Handler for the Opportunity SObject. This class implements the TriggerInterface
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
public without sharing class OpportunityHandler implements TriggerInterface
{   
    
    // Constructor
    public OpportunityHandler(){
    }

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public Map<Id,Boolean> oppMap = new Map<Id,Boolean>(); 
    public Map<Id,Account> accountMerMap = new Map<Id,Account>();
    public void bulkBefore()
    { 
        Map<Id,Boolean> attachmentFoundMap = new Map<Id,Boolean>(); 
        Map<Id,Opportunity> accIdsMap = new Map<Id,Opportunity>();
        Map<Id,Id> accOppsMap = new Map<Id,Id>();
        for(Opportunity opp : (List<Opportunity>)trigger.new){
            if(opp.id != null){
                oppMap.put(opp.id,false);
                if(opp.Partner_Account__c <> null){
                    accIdsMap.put(opp.Partner_Account__c,opp);
                    accOppsMap.put(opp.Id,opp.Partner_Account__c);
                }
            }
        }
        
        for(Account acc : [Select id,Merchant_Invoice_Required_To_Fund__c from Account  where Id IN: accIdsMap.keyset() and type = 'Merchant' and Active_Partner__c = true]){
            accountMerMap.put(acc.Id,acc);
            //if(!acc.Merchant_Invoice_Required_To_Fund__c)
            //oppMap.put(acc.Id,true);  
        }
        
        for(Attachment att : [SELECT Id,Description,parentId FROM Attachment WHERE Description LIKE '%Merchant Invoice%' and parentId IN: oppMap.keyset()]){
            //if(accOppsMap.containskey(att.parentId) &&  accountMerMap.containskey(accOppsMap.get(att.parentId)) && !accountMerMap.get(accOppsMap.get(att.parentId)).Merchant_Invoice_Required_To_Fund__c)
            oppMap.put(att.parentId,true);
        
        
            //attachmentFoundMap(att.parentId,true);
        }
    }

    public void bulkAfter()
    {
        
    }

    public void beforeInsert(SObject so){ 
      
    }
        

    public void beforeUpdate(SObject oldSo, SObject so){
        Opportunity opp = (Opportunity) so;
        Opportunity oldOpp = (Opportunity) oldSo;
        
        /* if(opp.Lead_Sub_Source__c == 'LoanHero' && oppMap.get(opp.Id) && (opp.Status__c <> oldOpp.Status__c) && (opp.Status__c == 'Funded' || opp.Status__c == 'Approved, Pending Funding')){
            opp.adderror('Merchant invoice needed to fund this application');
        } */
        
        if((opp.Status__c <> oldOpp.Status__c) && (opp.Status__c == 'Approved, Pending Funding')  && accountMerMap.containskey(opp.Partner_Account__c) && accountMerMap.get(opp.Partner_Account__c).Merchant_Invoice_Required_To_Fund__c && !oppMap.get(opp.Id)){
            opp.adderror('A valid invoice or certificate of completion must be uploaded before you can request funds.');
        }
         
      
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so)
    {
        
    }

    public void afterInsert(SObject so)
    {
        
         
    }

    public void afterUpdate(SObject oldSo, SObject so){
        Opportunity opp = (Opportunity) so;
        Opportunity oldOpp = (Opportunity) oldSo;
        
        
    }
    
    

    public void afterDelete(SObject so){
        
    }

    public void afterUndelete(SObject so){
        
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally(){
        
    }
}