global class LoanDisbursalSummarizedByLine implements Schedulable  {
    public DateTime cutOffDate = DateTime.now();
    public DateTime startDate = DateTime.now();
    public boolean bDoUpdate = false;
    global void execute (SchedulableContext sc) {
        try {

            calculateDates();
            this.generateNonFinwise();
            this.generateFinwise();
            this.generateFEB();
        } catch (Exception e) {
            WebToSFDC.notifyDev('LoanDisbursalSummarizedByLine.execute Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() ) ;
        }
    }

    public LoanDisbursalSummarizedByLine(){
           calculateDates();
    }
    
    public void calculateDates() {
        loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();
        Time newT = Time.newInstance(acha.Disbursal_File_Cutoff_Hour__c.intValue(), 0, 0, 0);
        Date t = Date.today();
        Integer dow = t.toStartOfWeek().daysBetween(t); 
        cutOffDate = DateTime.newInstance(t, newT);
        startDate = DateTime.newInstance(t.addDays( dow == 1 ? -3 : -1 ),newT.addSeconds(1));
        System.debug('Transaction Date = ' + cutOffDate + ' to ' + startDate);

    }

    public boolean generateNonFinwise(){
        List<loan__Loan_Disbursal_Transaction__c> lt = [select Id from loan__Loan_Disbursal_Transaction__c 
                                                  where loan__Loan_Account__r.Finwise_Originated__c = false and
                                                        loan__Loan_Account__r.FEB_Originated__c = false and
                                                        loan__Reversed__C = false and
                                                        (  
                                                            loan__Loan_Account__r.Original_Sub_Source__c  = null or
                                                            loan__Loan_Account__r.Original_Sub_Source__c  <> 'LoanHero'  
                                                        ) AND
                                                        CreatedDate >=: startDate AND
                                                        CreatedDate <=: cutOffDate];
        generateFile(lt, 'REG', '', bDoUpdate);
        return true;
    }

    public boolean generateFinwise(){
        List<loan__Loan_Disbursal_Transaction__c> lt = [select Id from loan__Loan_Disbursal_Transaction__c 
                                                  where loan__Loan_Account__r.Finwise_Originated__c = true and
                                                        loan__Reversed__C = false and
                                                        CreatedDate >=: startDate AND
                                                        CreatedDate <=: cutOffDate];
        generateFile(lt, 'FIN', 'FINWISE', bDoUpdate);
        return true;
    }
 
    public boolean generateFEB(){
        List<loan__Loan_Disbursal_Transaction__c> lt = [select Id from loan__Loan_Disbursal_Transaction__c 
                                                  where loan__Loan_Account__r.FEB_Originated__c = true and
                                                        loan__Reversed__C = false and
                                                        (  
                                                            loan__Loan_Account__r.Original_Sub_Source__c  = null or
                                                            loan__Loan_Account__r.Original_Sub_Source__c  <> 'LoanHero'  
                                                        ) AND
                                                        CreatedDate >=: startDate AND
                                                        CreatedDate <=: cutOffDate];
        generateFile(lt, 'FEB', '', bDoUpdate);
        return true;
    } 

    Webservice static String executeSummarizedLineFile(){
        loan__ACH_Parameters__c acha = loan.CustomSettingsUtil.getACHParameters();
        LoanDisbursalSummarizedByLine sl = new LoanDisbursalSummarizedByLine();
        sl.cutOffDate = DateTime.newInstance(Date.today().year(), Date.today().month(), Date.today().day(), acha.Disbursal_File_Cutoff_Hour__c.intValue() , 0, 0);
        sl.execute(null);
        return 'Process ended! Please look for the files.';
    }
    
    public void generateFile( List<loan__Loan_Disbursal_Transaction__c> lDisbursals, String fileName, String pTargetBank, boolean doUpdate) {
        System.debug(fileName + ' File Disbursals = ' + lDisbursals.size());
        System.Savepoint sp1;
        try {
            String fileContent = '';
            Folder folder = [select id from Folder where name='Shared Folder for ACH' LIMIT 1];
            loan.TransactionSweepToACHState st = new loan.TransactionSweepToACHState();
            st.counter = 1;
            LoanDisbursalTxnPayPointGen pg = new LoanDisbursalTxnPayPointGen();
            pg.fileDestination = pTargetBank;
            for (String i : pg.getEntries(st, lDisbursals)) {
                fileContent += i + '\r\n';
            }
            fileContent = pg.getHeader(st, lDisbursals) + fileContent;
            fileContent = fileContent + pg.getTrailer(st, lDisbursals);
            System.debug(fileContent);
            
            Document d = new Document(); 
            d.Name = fileName + '_LoanDisbursals_' + DateTime.now(); 
            d.Body = Blob.valueOf(fileContent); 
            d.ContentType = 'text/plain';
            d.FolderId = folder.Id;
            d.Type = 'txt';
            
            system.debug(d.Name);
            if(pTargetBank == 'FINWISE') FinwiseNACHAUpload.uploadFile(d.Body, d.Name);
            
            sp1 = Database.setSavepoint();

            insert d;			
            
            for (loan__Loan_Disbursal_Transaction__c p : lDisbursals) {
                p.loan__Sent_To_ACH__c = true;
                p.loan__Sent_To_ACH_On__c = Date.today();
                p.loan__ACH_Filename__c = d.Name;
            }

            if (doUpdate && lDisbursals != null && lDisbursals.size() > 0) update lDisbursals;
            
        } catch (Exception e ) {
            WebToSFDC.notifyDev('LoanDisbursalSummarizedByLine.generateFile Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() ) ;
            if (sp1 != null) Database.rollBack(sp1);
            System.debug(e.getMessage() + '-' + e.getStacktraceString());
        }
    }
}