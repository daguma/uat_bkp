@isTest(seeAllData=false)
public class BatchResetPromotionalInterestRateTest {
    @testSetup
    static void TestData(){
        Opportunity testApp = TestHelper.createApplication();
        testApp.Funded_Date__c=Date.today().addDays(-90);
        testApp.LeadSource__c='LoanHero';
        testApp.Lead_Sub_Source__c = 'LoanHero';
        testApp.ProductName__c = 'Medical';
        update testApp;
        system.debug(testApp.Lending_Product__c);
        ezVerify_Settings__c ez=new ezVerify_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),EZVerify_Base_URL__c='https://test.ezverify.me/api/lend',Promotional_Period__c=90,Loan_Product_ID__c=testApp.Lending_Product__c,Default_Interest_Rate__c=3.99);
        insert ez;
        loan__Loan_Account__c CLContract = TestHelper.createContractFromApp(testApp);
        loan__Loan_Account__c testCLContract = new loan__Loan_Account__c(id=CLContract.Id,loan__Contractual_Interest_Rate__c=3.99,Interest_Rate_After_Promotional_Period__c=23,loan__loan_status__c='Active - Good Standing');
		testCLContract.Is_On_Discount_Promotion__c = true;
        testCLContract.Discount_End_Date__c = Date.today().addDays(-10);
        testCLContract.Opportunity__c = testApp.Id;
        update testCLContract;
        
        CLContract.Is_On_Discount_Promotion__c = true;
        CLContract.Discount_End_Date__c = Date.today().addDays(-10);
        update CLContract;
        
    }
    
    static testmethod void batchTesting(){
        BatchResetPromotionalInterestRate batchapex=new BatchResetPromotionalInterestRate();
        Id batchId=Database.executeBatch(batchapex, 1);               
    }
    
    static testmethod void SchedulableJobTesting(){
        BatchResetPromotionalInterestRate scheduleBatch = new BatchResetPromotionalInterestRate();
        SchedulableContext SC;
        scheduleBatch.execute(SC);               
    }
     
}