public class CustomerProfileCtrl {
    
    @AuraEnabled public Decimal debt;
    @AuraEnabled public Decimal income;
    @AuraEnabled public Decimal paymentAmt;
    @AuraEnabled public Decimal pti;
    @AuraEnabled public String achAcctNbr;
    @AuraEnabled public String achAcctType;
    @AuraEnabled public String achBankName;
    @AuraEnabled public String achMemberAcct;
    @AuraEnabled public String achRoutNbr;
    @AuraEnabled public String achOthers;
    @AuraEnabled public String achVerifChk;
    @AuraEnabled public String avgDaily;
    @AuraEnabled public String clarityPass;
    @AuraEnabled public String crDTI;
    @AuraEnabled public String crFICO;
    @AuraEnabled public String crId;
    @AuraEnabled public String dti;
    @AuraEnabled public String emailAgePass;
    @AuraEnabled public String fico;
    @AuraEnabled public String ftDesc;
    @AuraEnabled public String grade;
    @AuraEnabled public String kountPass;
    @AuraEnabled public String negDays;
    @AuraEnabled public String ofacPass;
    @AuraEnabled public String ofacStatus;
    @AuraEnabled public String payfonePass;
    @AuraEnabled public String paymentFreq;
    @AuraEnabled public String ptiStr;
    @AuraEnabled public String ssn;
    @AuraEnabled public String totalDep;
    @AuraEnabled public String idologyPass;
    @AuraEnabled public String applicantName;
    @AuraEnabled public String dob;
    @AuraEnabled public String applicantAddress;
    @AuraEnabled public String offerSelectedPayment;
    @AuraEnabled public String EmployerName;
    @AuraEnabled public String HireDate;
    @AuraEnabled public String PriorEmploymentStarted;
    @AuraEnabled public String PriorEmploymentEnded;
    
    public Offer__c off;
    
    public CustomerProfileCtrl() {
        debt = 0;
        income = 0;
        paymentAmt = 0;
        pti = 0;
        off = null;
        achAcctNbr = '--';
        achAcctType = '--';
        achBankName = '--';
        achMemberAcct = '--';
        achRoutNbr = '--';
        achOthers = '--';
        achVerifChk = '--';
        avgDaily = '--';
        clarityPass = '--';
        crDTI = '--';
        crFICO = '--';
        dti = '--';
        emailAgePass = 'Fail';
        fico = '--';
        ftDesc = '--';
        grade = '--';
        kountPass = 'Not done';
        negDays = '--';
        ofacPass = '';
        ofacStatus = '';
        payfonePass = '';
        paymentFreq = '';
        ptiStr = '--';
        ssn = '--';
        totalDep = '--';
        idologyPass = '';
        crId = '--';
    }
    
    @AuraEnabled
    public static CustomerProfileCtrl initializeCustomerProfile(String appId) {
        CustomerProfileCtrl CustomerProfile = new CustomerProfileCtrl();
        if(String.isNotBlank(appId)) {		
            Opportunity app = [SELECT   ACH_Account_Number__c, 
                            ACH_Account_Type__c, 
                            ACH_Bank_Name__c, 
                            ACH_Member_Account_Number__c, 
                            ACH_Routing_Number__c, 
                            Average_Daily_Balance_Amount__c, 
                            Clarity_Status__c, 
                            Clarity_Submitted__c, 
                            DTI__c, 
                            Estimated_Grand_Total__c, 
                            Contact__c,
                            Contact__r.Annual_Income__c, 
                            Payment_Amount__c, 
                            Payment_Frequency_Masked__c,
                            Payment_Frequency__c, 
                            Payment_Frequency_Multiplier__c, 
                            FT_Description__c, 
                            Id, 
                            Manual_Grade__c, 
                            Number_of_Negative_Days_Number__c, 
                            Others__c, 
                            Verified_Checklist__c, 
                            Total_Deposits_Amount__c,
                            Bankruptcy_Found__c
                    FROM    Opportunity 
                    WHERE   Id =: appId];
            
            Contact c = [SELECT Birthdate, 
                                Id, 
                                ints__Social_Security_Number__c, 
                                MailingCity, 
                                MailingState, 
                                MailingStreet, 
                                Name,
                                Employer_Name__c,
                                Employment_Start_Date__c,
                                Previous_Employment_Start_Date__c,
                                Previous_Employment_End_Date__c
                         FROM   Contact 
                         WHERE  Id =: app.Contact__c];

            ProxyApiCreditReportEntity cr = CreditReport.getByEntityId(appId);

            for (Offer__c offers : [SELECT Id, 
                                            Payment_Amount__c 
                                     FROM   Offer__c 
                                     WHERE  Opportunity__c =: appId 
                                     AND    IsSelected__c = true]){
                CustomerProfile.off = offers;
            }

            for (Logging__c kountLogs : [SELECT Id 
                                          FROM   Logging__c 
                                          WHERE  Contact__c =: c.Id 
                                          AND    Webservice__c = 'Kount'
                                          ORDER BY CreatedDate DESC]){
                CustomerProfile.kountPass = 'Done';
            }

            for (Email_Age_Details__c emailAge : [SELECT   Id 
                                                   FROM     Email_Age_Details__c 
                                                   WHERE    Contact__c =: c.Id 
                                                   AND      Email_Age_Status__c = true
                                                   ORDER BY CreatedDate DESC]){
                CustomerProfile.emailAgePass = 'Pass';

            }

            List<IDology_Request__c> idology = [SELECT  Id, 
                                                        CreatedDate, 
                                                        Verification_Result__c 
                                                FROM    IDology_Request__c 
                                                WHERE   Contact__c =: c.Id 
                                                AND     (Verification_Result__c = 'Pass' OR Verification_Result__c = 'Fail')
                                                ORDER BY CreatedDate DESC 
                                                LIMIT 1];

            List<BridgerWDSFLookup__c> ofac = [SELECT Id, 
                                                            Review_Status__c 
                                                     FROM   BridgerWDSFLookup__c 
                                                     WHERE  Contact__c =: c.Id 
                                                     ORDER BY CreatedDate DESC 
                                                     LIMIT 1];

            

            for (Payfone_Run_History__c payFone : [SELECT  Id, 
                                                            Verification_Result__c
                                                    FROM    Payfone_Run_History__c 
                                                    WHERE   (Verification_Result__c = 'Pass' OR Verification_Result__c = 'Fail')
                                                    AND     Contact__c =: c.Id 
                                                    ORDER BY CreatedDate DESC 
                                                    LIMIT 1]){

               CustomerProfile.payfonePass = payFone.Verification_Result__c;

            }

            if (cr != null && cr.errorMessage == null) {
                    CustomerProfile.crId = String.valueOf(cr.id);
                    CustomerProfile.crDTI = cr.getAttributeValue(CreditReport.DTI) + '%';
                    CustomerProfile.crFICO = cr.getAttributeValue(CreditReport.FICO);
                    if (cr.getAttributeValue(CreditReport.TOTAL_ANNUAL_DEBT) != '')
                        CustomerProfile.debt = cr.getDecimalAttribute(CreditReport.TOTAL_ANNUAL_DEBT);
            }


            for (Scorecard__c sc : [SELECT DTI__c, FICO_Score__c
                                            FROM   Scorecard__c 
                                            WHERE  Opportunity__c =: appId 
                                            ORDER BY CreatedDate DESC 
                                            LIMIT 1]){
                CustomerProfile.fico = sc.FICO_Score__c != null ? String.valueOf(sc.FICO_Score__c) : CustomerProfile.crFICO;
                if (app.Estimated_Grand_Total__c != null && app.Estimated_Grand_Total__c > 0 && CustomerProfile.debt > 0 ) 
                    CustomerProfile.dti = String.valueOf(((CustomerProfile.debt / app.Estimated_Grand_Total__c * 12) * 100).format()) + '%';
                else
                    CustomerProfile.dti = sc.DTI__c != null ? String.valueOf(sc.DTI__c) + '%' : CustomerProfile.crDTI;
            }           
                
            CustomerProfile.paymentAmt = (app.Payment_Amount__c == null ? 0 : app.Payment_Amount__c);
            CustomerProfile.income = (app.Estimated_Grand_Total__c != null && app.Estimated_Grand_Total__c > 0) ? app.Estimated_Grand_Total__c : app.Contact__r.Annual_Income__c;
            CustomerProfile.paymentFreq = app.Payment_Frequency_Masked__c;

            if (CustomerProfile.paymentFreq != null  && (CustomerProfile.income != null && CustomerProfile.income > 0)) 
                CustomerProfile.pti = ((CustomerProfile.paymentAmt * OfferCtrlUtil.getFrequencyUtil(CustomerProfile.paymentFreq)) / CustomerProfile.income) * 100;

            CustomerProfile.ptiStr = String.ValueOf(CustomerProfile.pti.setscale(2)) + '%';
            CustomerProfile.idologyPass = idology.size() > 0 ? idology[0].Verification_Result__c : '--';
            CustomerProfile.ofacPass = ofac.size() > 0 ? ofac[0].Review_Status__c : '--';
            CustomerProfile.clarityPass = app.Clarity_Submitted__c ? (app.Clarity_Status__c ? 'Pass' : 'Fail') : '--';
            CustomerProfile.achAcctNbr = String.isEmpty(app.ACH_Account_Number__c) ? '--' : app.ACH_Account_Number__c;
            if (app.Average_Daily_Balance_Amount__c != null) CustomerProfile.avgDaily =  String.valueOf(app.Average_Daily_Balance_Amount__c); 
            if ( app.Number_of_Negative_Days_Number__c != null) CustomerProfile.negDays = String.valueOf(app.Number_of_Negative_Days_Number__c); 
            CustomerProfile.totalDep = app.Total_Deposits_Amount__c == null ? '--' : String.valueOf(app.Total_Deposits_Amount__c);                                                    
            CustomerProfile.achAcctType = String.isEmpty(app.ACH_Account_Type__c) ? '--' : app.ACH_Account_Type__c;
            CustomerProfile.achMemberAcct = String.isEmpty(app.ACH_Member_Account_Number__c)  ? '--' : app.ACH_Member_Account_Number__c;
            CustomerProfile.achRoutNbr = String.isEmpty(app.ACH_Routing_Number__c) ? '--' : app.ACH_Routing_Number__c;
            CustomerProfile.achBankName = String.isEmpty(app.ACH_Bank_Name__c) ? '--' : app.ACH_Bank_Name__c;
            CustomerProfile.achOthers = String.isEmpty(app.Others__c)  ? '--' : app.Others__c;
            CustomerProfile.achVerifChk = app.Verified_Checklist__c == null ? '--' : app.Verified_Checklist__c;
            CustomerProfile.grade = String.isEmpty(app.Manual_Grade__c)  ? '--' : app.Manual_Grade__c;
            CustomerProfile.ofacStatus = CustomerProfile.ofacPass == 'False Positive' ||  CustomerProfile.ofacPass == 'No match' ? ' (PASS)' : ' (FAIL)';
            CustomerProfile.ssn = c.ints__Social_Security_Number__c == null ? '--' : '***-**-' + c.ints__Social_Security_Number__c.substring(5, 9);
            CustomerProfile.ftDesc = String.isEmpty(app.FT_Description__c)  ? '--' : app.FT_Description__c;
            CustomerProfile.applicantName = c.Name;
            CustomerProfile.dob = c.BirthDate.format();
            CustomerProfile.applicantAddress = c.MailingStreet + ', ' + c.MailingCity + ', ' + c.MailingState;
            CustomerProfile.offerSelectedPayment = CustomerProfile.off != null ? '$' + String.valueOf(CustomerProfile.off.Payment_Amount__c) : '$0.00';
            CustomerProfile.EmployerName = c.Employer_Name__c;
            CustomerProfile.HireDate = c.Employment_Start_Date__c != null ? c.Employment_Start_Date__c.format() : 'N/A';
			CustomerProfile.PriorEmploymentStarted = c.Previous_Employment_Start_Date__c != null ? c.Previous_Employment_Start_Date__c.format() : 'N/A';
            CustomerProfile.PriorEmploymentEnded = c.Previous_Employment_End_Date__c != null ? c.Previous_Employment_End_Date__c.format() : 'N/A';
            
        }
        
    	return CustomerProfile;
    }
    
}