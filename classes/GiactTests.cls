@IsTest
public class GiactTests {
        
    static testMethod void TestResponses() {
        GiactResponseHelper.tryGetDate('2007-10-03T00:00:00');
        GiactResponseHelper.tryGetDate('sdasd00:00');        
        
        GiactResponseHelper.getNodeArray('GiactSearchResult_5_8', '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><GetFundsConfirmationResultsResponse xmlns="http://api.giact.com/verificationservices/v5"><GetFundsConfirmationResultsResult><GiactSearchResult_5_8><Inquiry><Check><RoutingNumber>122105278</RoutingNumber><AccountNumber>0000000016</AccountNumber><CheckAmount>980.0000</CheckAmount><AccountType>Checking</AccountType></Check><Customer><EntityType>UnknownNotApplicable</EntityType><BusinessName>Smith and Associates Consulting</BusinessName><AddressLine1>1923 Lake Forest Rd</AddressLine1><City>Grapevine</City><State>TX</State><ZipCode>76051</ZipCode><Country>US</Country><PhoneNumber>2145551212</PhoneNumber><TaxId>123456543</TaxId><DateOfBirth xsi:nil="true" /><MobileConsentRecordId xsi:nil="true" /><AltIdType xsi:nil="true" /></Customer><GVerifyEnabled>true</GVerifyEnabled><FundsConfirmationEnabled>true</FundsConfirmationEnabled><GAuthenticateEnabled>true</GAuthenticateEnabled><VoidedCheckImageEnabled>false</VoidedCheckImageEnabled><GIdentifyEnabled>false</GIdentifyEnabled><GIdentifyKbaEnabled>false</GIdentifyKbaEnabled><GIdentifyEsiEnabled>false</GIdentifyEsiEnabled><CustomerIdEnabled>false</CustomerIdEnabled><OfacScanEnabled>false</OfacScanEnabled><IpAddressInformationEnabled>false</IpAddressInformationEnabled><DomainWhoisEnabled>false</DomainWhoisEnabled><MobileVerifyEnabled>false</MobileVerifyEnabled><MobileIdentifyEnabled>false</MobileIdentifyEnabled><MobileLocationEnabled>false</MobileLocationEnabled></Inquiry><Result><ItemReferenceId>5001044170</ItemReferenceId><CreatedDate>2017-10-25T09:56:58.0751891-05:00</CreatedDate><VerificationResponse>Declined</VerificationResponse><AccountResponseCode>_1111</AccountResponseCode><BankName>WELLS FARGO BANK NA (ARIZONA)</BankName><AccountAddedDate>2008-04-09T00:00:00</AccountAddedDate><AccountLastUpdatedDate>2010-06-23T00:00:00</AccountLastUpdatedDate><AccountClosedDate xsi:nil="true" /><FundsConfirmationResult>SufficientFunds</FundsConfirmationResult><CustomerResponseCode>CA01</CustomerResponseCode><MatchedPersonData /><ConsumerAlertMessages /><MatchedBusinessData /><OfacListPotentialMatches /><GIdentifyKbaResult /></Result></GiactSearchResult_5_8><GiactSearchResult_5_8><Inquiry><Check><RoutingNumber>122105278</RoutingNumber><AccountNumber>0000000019</AccountNumber><CheckAmount>980.0000</CheckAmount><AccountType>Savings</AccountType></Check><Customer><EntityType>UnknownNotApplicable</EntityType><BusinessName>Smith and Associates Consulting</BusinessName><AddressLine1>1113 Shade Tree Ln</AddressLine1><City>Allen</City><State>TX</State><ZipCode>75013</ZipCode><Country>US</Country><PhoneNumber>2145551212</PhoneNumber><TaxId>123456543</TaxId><DateOfBirth xsi:nil="true" /><MobileConsentRecordId xsi:nil="true" /><AltIdType xsi:nil="true" /></Customer><GVerifyEnabled>true</GVerifyEnabled><FundsConfirmationEnabled>true</FundsConfirmationEnabled><GAuthenticateEnabled>true</GAuthenticateEnabled><VoidedCheckImageEnabled>false</VoidedCheckImageEnabled><GIdentifyEnabled>false</GIdentifyEnabled><GIdentifyKbaEnabled>false</GIdentifyKbaEnabled><GIdentifyEsiEnabled>false</GIdentifyEsiEnabled><CustomerIdEnabled>false</CustomerIdEnabled><OfacScanEnabled>false</OfacScanEnabled><IpAddressInformationEnabled>false</IpAddressInformationEnabled><DomainWhoisEnabled>false</DomainWhoisEnabled><MobileVerifyEnabled>false</MobileVerifyEnabled><MobileIdentifyEnabled>false</MobileIdentifyEnabled><MobileLocationEnabled>false</MobileLocationEnabled></Inquiry><Result><ItemReferenceId>5001044211</ItemReferenceId><CreatedDate>2017-10-25T09:58:32.0396193-05:00</CreatedDate><VerificationResponse>Pass</VerificationResponse><AccountResponseCode>_5555</AccountResponseCode><BankName>WELLS FARGO BANK NA (ARIZONA)</BankName><AccountAddedDate>2007-10-03T00:00:00</AccountAddedDate><AccountLastUpdatedDate>2007-10-03T00:00:00</AccountLastUpdatedDate><AccountClosedDate xsi:nil="true" /><FundsConfirmationResult>SufficientFunds</FundsConfirmationResult><CustomerResponseCode>CA11</CustomerResponseCode><MatchedPersonData /><ConsumerAlertMessages /><MatchedBusinessData /><OfacListPotentialMatches /><GIdentifyKbaResult /></Result></GiactSearchResult_5_8><GiactSearchResult_5_8><Inquiry><Check><RoutingNumber>122105278</RoutingNumber><AccountNumber>0000000019</AccountNumber><CheckAmount>980.0000</CheckAmount><AccountType>Savings</AccountType></Check><Customer><EntityType>UnknownNotApplicable</EntityType><BusinessName>Smith and Associates Consulting</BusinessName><AddressLine1>1113 Shade Tree Ln</AddressLine1><City>Allen</City><State>TX</State><ZipCode>75013</ZipCode><Country>US</Country><PhoneNumber>2145551212</PhoneNumber><TaxId>123456543</TaxId><DateOfBirth xsi:nil="true" /><MobileConsentRecordId xsi:nil="true" /><AltIdType xsi:nil="true" /></Customer><GVerifyEnabled>true</GVerifyEnabled><FundsConfirmationEnabled>true</FundsConfirmationEnabled><GAuthenticateEnabled>true</GAuthenticateEnabled><VoidedCheckImageEnabled>false</VoidedCheckImageEnabled><GIdentifyEnabled>false</GIdentifyEnabled><GIdentifyKbaEnabled>false</GIdentifyKbaEnabled><GIdentifyEsiEnabled>false</GIdentifyEsiEnabled><CustomerIdEnabled>false</CustomerIdEnabled><OfacScanEnabled>false</OfacScanEnabled><IpAddressInformationEnabled>false</IpAddressInformationEnabled><DomainWhoisEnabled>false</DomainWhoisEnabled><MobileVerifyEnabled>false</MobileVerifyEnabled><MobileIdentifyEnabled>false</MobileIdentifyEnabled><MobileLocationEnabled>false</MobileLocationEnabled></Inquiry><Result><ItemReferenceId>5001044325</ItemReferenceId><CreatedDate>2017-10-25T10:13:11.6850966-05:00</CreatedDate><VerificationResponse>Pass</VerificationResponse><AccountResponseCode>_5555</AccountResponseCode><BankName>WELLS FARGO BANK NA (ARIZONA)</BankName><AccountAddedDate>2007-10-03T00:00:00</AccountAddedDate><AccountLastUpdatedDate>2007-10-03T00:00:00</AccountLastUpdatedDate><AccountClosedDate xsi:nil="true" /><FundsConfirmationResult>SufficientFunds</FundsConfirmationResult><CustomerResponseCode>CA11</CustomerResponseCode><MatchedPersonData /><ConsumerAlertMessages /><MatchedBusinessData /><OfacListPotentialMatches /><GIdentifyKbaResult /></Result></GiactSearchResult_5_8><GiactSearchResult_5_8><Inquiry><Check><RoutingNumber>122105278</RoutingNumber><AccountNumber>0000000019</AccountNumber><CheckAmount>980.0000</CheckAmount><AccountType>Savings</AccountType></Check><Customer><EntityType>UnknownNotApplicable</EntityType><BusinessName>Smith and Associates Consulting</BusinessName><AddressLine1>1113 Shade Tree Ln</AddressLine1><City>Allen</City><State>TX</State><ZipCode>75013</ZipCode><Country>US</Country><PhoneNumber>2145551212</PhoneNumber><TaxId>123456543</TaxId><DateOfBirth xsi:nil="true" /><MobileConsentRecordId xsi:nil="true" /><AltIdType xsi:nil="true" /></Customer><GVerifyEnabled>true</GVerifyEnabled><FundsConfirmationEnabled>true</FundsConfirmationEnabled><GAuthenticateEnabled>true</GAuthenticateEnabled><VoidedCheckImageEnabled>false</VoidedCheckImageEnabled><GIdentifyEnabled>false</GIdentifyEnabled><GIdentifyKbaEnabled>false</GIdentifyKbaEnabled><GIdentifyEsiEnabled>false</GIdentifyEsiEnabled><CustomerIdEnabled>false</CustomerIdEnabled><OfacScanEnabled>false</OfacScanEnabled><IpAddressInformationEnabled>false</IpAddressInformationEnabled><DomainWhoisEnabled>false</DomainWhoisEnabled><MobileVerifyEnabled>false</MobileVerifyEnabled><MobileIdentifyEnabled>false</MobileIdentifyEnabled><MobileLocationEnabled>false</MobileLocationEnabled></Inquiry><Result><ItemReferenceId>5001045547</ItemReferenceId><CreatedDate>2017-10-25T11:36:45.6799355-05:00</CreatedDate><VerificationResponse>Pass</VerificationResponse><AccountResponseCode>_5555</AccountResponseCode><BankName>WELLS FARGO BANK NA (ARIZONA)</BankName><AccountAddedDate>2007-10-03T00:00:00</AccountAddedDate><AccountLastUpdatedDate>2007-10-03T00:00:00</AccountLastUpdatedDate><AccountClosedDate xsi:nil="true" /><FundsConfirmationResult>SufficientFunds</FundsConfirmationResult><CustomerResponseCode>CA11</CustomerResponseCode><MatchedPersonData /><ConsumerAlertMessages /><MatchedBusinessData /><OfacListPotentialMatches /><GIdentifyKbaResult /></Result></GiactSearchResult_5_8></GetFundsConfirmationResultsResult></GetFundsConfirmationResultsResponse></soap:Body></soap:Envelope>');
        try {
            GiactResponseHelper.getNodeArray('GiactSearchResult_5_8', 'dsadasdasdsadope>');                    
        }
        catch (Exception ex) {
        
        }
    }
    
    static testMethod void TestException() {
        GiactException gc = new GiactException(''); 
        gc.dummycall();
    }
    
    static testMethod void TestRequests() {
        Settings();
        List<string> ses = GiactRequestHelper.GenerateStringArrayFromGiact(new GIACT__c(), false);
        string s = GiactRequestHelper.GenerateFundsConfirmationRequest();
    }    
        
    static testMethod void TestConstants() {
        Settings();
        String s = GiactConstants.STATUS_REVIEW_COMPLETE;
        s = GiactConstants.STATUS_FAIL;        
        s = GiactConstants.STATUS_IN_PROGRESS;
        s = GiactConstants.PositiveFundsConfirmation;
        Set<string> ses = GiactConstants.AcceptableAccountResponseCodes;
        ses = GiactConstants.AcceptableCustomerResponseCode;                        
    }
    
    static testMethod void TestApis() {
        Settings();
        Account grandParentAccount = new Account(Name = 'Invisalign', Channel__c = 'Direct');
        insert grandParentAccount;
        
        Account m = new Account(Name = 'Merchant', Channel__c = 'Direct', GrandparentAccount__c = grandParentAccount.Id);
        insert m;
        
        GIACT__c g = new GIACT__c(MerchantAccount__c = m.Id, ItemReferenceId__c = '5001044170', AccountNumber__c = '5001044170', RoutingNumber__c = '5001044170', CheckAmount__c = 25, Status__c = 'Under Review');
        insert g;
        
        GiactProcessor.ProcessFundsConfirmationUpdates(); 
        GiactProcessor.SubmitGiact(g.Id);            
    }
    
    private static void Settings() {
      insert new GIACT_Integration_Settings__c(ApiPassword__c = 'Value', ApiUrl__c = 'https://www.google.com', ApiUsername__c = 'Value', PositiveAccountResponseCode__c = 'Value', PositiveCustomerResponseCode__c = 'Value', PositiveFundsConfirmation__c = 'Value', RetryAttempts__c = 2, STATUS_FAIL__c = 'Fail', STATUS_IN_PROGRESS__c = 'Under Review', STATUS_REVIEW_COMPLETE__c = 'Review Complete', NonParticipatingBank__c = '_3333', NameMismatch__c = 'CA21');
    } 
    
    static testMethod void GiactAPITest() {
        Settings();
        GiactApi giact = new GiactApi();
        giact.sendGetFundsConfirmationRequest('Testing body content');
        giact.sendRequest('Testing body content', 'Create');
    }
    
    static testMethod void GiactSchedulerTest() {
        Test.startTest();
        GiactScheduler obj = new GiactScheduler();
        obj.execute(null);
        Test.stopTest();
    }
    
    static testMethod void GiactAsyncTriggerTest() {
        List<GIACT__c> giactList = new List<GIACT__c>();
        
        Account m = new Account(Name = 'Merchant', Channel__c = 'Direct');
        insert m;
        
        GIACT__c g1 = new GIACT__c(MerchantAccount__c = m.Id, ItemReferenceId__c = '5001044170', AccountNumber__c = '5001044170', RoutingNumber__c = '5001044170', CheckAmount__c = 25, Status__c = 'Under Review');
        giactList.add(g1);
        
        GIACT__c g2 = new GIACT__c(MerchantAccount__c = m.Id, ItemReferenceId__c = '5001044170', AccountNumber__c = '5001044170', RoutingNumber__c = '5001044170', CheckAmount__c = 25, Status__c = 'Under Review');
        giactList.add(g2);
        
        insert giactList;       
    }
}