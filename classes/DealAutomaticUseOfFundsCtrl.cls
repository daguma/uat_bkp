public with sharing class DealAutomaticUseOfFundsCtrl {
    public DealAutomaticUseOfFundsCtrl() {

    }

    /**
    * Get all deal sources from the Deal Automatic Attributes
    * @return List of Deal_Automatic_Attributes__c
    */
    public static String getUseOfFunds() {
        return JSON.serialize([SELECT Id__c, Name, Display_Name__c, Usage__c
                               FROM Deal_Automatic_Attributes__c
                               WHERE Usage__c LIKE :DealAutomaticAssignmentUtils.USE_OF_FUNDS_USAGE
                               ORDER BY Display_Name__c]);
    }

    /**
    * [saveUsers description]
    * @param  attribute to save the selected users
    * @param  usersGroups    to save in the specific category
    * @return Save the selected users in the Deal_Automatic_Assignment_Data__c object
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> saveUsers(Deal_Automatic_Attributes__c attribute,
            List<DealAutomaticUserGroup> newUsersGroups,
            List<DealAutomaticUserGroup> oldUserGroups) {
        return DealAutomaticAssignmentUtils.saveUsers(attribute, newUsersGroups, oldUserGroups);
    }

    /**
    * [getUsersGroupsBySource description]
    * @param  useOfFund [description]
    * @return  List of DealAutomaticUserGroup
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> getUsersGroupsByUseOfFunds(Deal_Automatic_Attributes__c useOfFund) {
        return DealAutomaticAssignmentUtils.getUsersGroupsByCategory(useOfFund);
    }
}