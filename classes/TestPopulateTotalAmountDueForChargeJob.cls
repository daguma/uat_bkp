@isTest
public class TestPopulateTotalAmountDueForChargeJob {
    
    @testSetup
        static void setup() {
            Loan.TestHelper.createSeedDataForTesting();
            Loan.TestHelper.systemDate = Date.newInstance(2015, 01, 01);
            Loan__Currency__c curr = Loan.TestHelper.createCurrency();
           
            //Create a Fee Set
            Loan__Fee__c dummyFee = Loan.TestHelper.createFee(curr);
            Loan__Fee_Set__c dummyFeeSet = Loan.TestHelper.createFeeSet();
            Loan__Fee_Junction__c dummyFeeJunction = Loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
            
            Integer noOfInstallments = 6;
            //Create a dummy MF_Account
            Loan__MF_Account__c dummyAccount = Loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
            Contact dummyContact = new Contact(FirstName = 'XYZ', LastName = 'Contact');
            insert dummyContact;
    
            Loan__Office_Name__c dummyOffice = Loan.TestHelper.createOffice();
            Loan__Client__c dummyClient = Loan.TestHelper.createClient(dummyOffice);
            Loan__Loan_Product__c flexiAmzBased = Loan.TestHelper.createLoanProductwithProductType('XYZ@Loan',
                                                                                        dummyOffice,
                                                                                        dummyAccount,
                                                                                        curr,
                                                                                        dummyFeeSet,
                                                                                        'Declining Balance',
                                                                                        12,
                                                                                        10,
                                                                                        null,
                                                                                        Loan.LoanConstants.FLEXIBLE_AMZ);
            
            Account account = Loan.TestHelper.createSFCRMAccount();
            Loan__Loan_Account__c testloanAccount = new Loan__Loan_Account__c(Loan__Loan_Product_Name__c = flexiAmzBased.Id, 
                                                                Loan__Account__c = account.Id, 
                                                                Loan__Loan_Amount__c = 16000, 
                                                                Loan__Number_of_Installments__c = 8, 
                                                                Loan__Protect_Fee_Percent__c = 20, 
                                                                Loan__Protect_Waiver_type__c = 'Principal and Interest', 
                                                                Loan__Loan_Status__c = 'Approved',
                                                                Loan__Write_off_Tolerance_Amount__c = 50, 
                                                                Loan__Client__c = dummyClient.Id,
                                                                Loan__Is_Interest_Posting_Enabled__c = true,
                                                                Loan__Interest_Posting_Frequency__c = Loan.LoanConstants.LOAN_PAYMENT_FREQ_MONTHLY);
            Loan.BorrowerAPI1 bAPI = Loan.APIFactory.getBorrowerAPI1();
            Loan__Loan_Account__c loanAccount = bAPI.createContract(testloanAccount, null, null);
        }
        
        @isTest
        static  void testTaxableCharges() {
            Loan.TestHelper.systemDate = Date.newInstance(2015, 01, 01);
            Loan__Fee__c fee = [SELECT id,
                                 Loan__Include_In_Dues__c
                            FROM Loan__fee__c
                            LIMIT 1];
            
            Loan__Loan_Account__c loanAccount = Loan.TestHelper2.getLoanAccount(null);
            
            List<Loan__Repayment_Schedule__c> schedules = [SELECT Id,
                                                            Loan__Due_Principal__c,
                                                            Loan__Due_Interest__c,
                                                            Loan__isPaid__c,
                                                            Loan__Due_Date__c
                                                       FROM Loan__Repayment_Schedule__c
                                                       WHERE Loan__Loan_Account__c =:loanAccount.id
                                                       ORDER BY Loan__Due_Date__c ASC LIMIT 4];
    
            Loan__Trigger_parameters__c chargeTriggerParameter = new Loan__Trigger_Parameters__c();
            chargeTriggerParameter.loan__Disable_Charge_Trigger__c = true;
            insert chargeTriggerParameter;
            
            Loan__Charge__c charge = new Loan__Charge__c(Loan__Loan_Account__c = loanAccount.Id,
                                             Loan__Original_Amount__c = 20,
                                             Loan__Date__c = schedules.get(0).Loan__Due_Date__c,
                                             Loan__Fee__c = fee.Id,
                                             Loan__Status__c ='Processed',
                                             Loan__total_Amount_Due__c = null);
            insert charge;
            
            Test.startTest();
                system.assert(charge.Loan__Total_Amount_Due__c == null);
                Database.executeBatch(new PopulateTotalAmountDueForChargeJob());
            Test.stopTest();
            
            Loan__Charge__c updatedcharge =[SELECT id,
                                      Loan__Total_Amount_Due__c,
                                      Loan__Total_Due_Amount__c 
                                      FROM Loan__Charge__c
                                      WHERE Id= :charge.Id];
            system.assert(updatedcharge.Loan__Total_Amount_Due__c != null);
            system.assert(updatedcharge.Loan__Total_Amount_Due__c == updatedcharge.Loan__Total_Due_Amount__c);
        }
}