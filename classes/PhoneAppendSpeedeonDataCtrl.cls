public class PhoneAppendSpeedeonDataCtrl {
    @InvocableMethod
    public static void callSpeedeonData(List<id> appIds) {
        System.debug('appIds.....>>>>>>> '+appIds);
        //API-211
        if(!System.isFuture() && !System.isBatch()) 
            callASyncAPI(appIds);
        else
            callAPI(appIds,true);
        //API-211
    }
  
    public static string createPhoneAppendXMLNodes(Opportunity app, Phone_Append_SpeedeonData__c pas){
        String customerId = pas.customerId__c, username = pas.username__c, password = pas.password__c,messageType=pas.messageType__c; 
        XmlStreamWriter w = new XmlStreamWriter(); 
        w.writeStartDocument(null, '1.0'); //Start the XML document
        w.writeStartElement(null, 'SpeedeonInquiry', null);  
        addCol(w,'customerId',customerId);
        addCol(w,'username',username);
        addCol(w,'password',password);
        addCol(w,'messageType',messageType);
        addCol(w,'transactionId',app.Contact__c);
        addCol(w,'firstName',app.Contact__r.firstName); 
        addCol(w,'lastName',app.Contact__r.lastName); 
        addCol(w,'addressLine1',app.Contact__r.mailingstreet); 
        addCol(w,'city',app.Contact__r.mailingcity); 
        addCol(w,'state',app.Contact__r.mailingstate); 
        addCol(w,'zip5',app.Contact__r.mailingpostalcode); 
        //addCol(w,'zip4',app.Contact__r.mailingpostalcode);
        w.writeEndElement(); 
        w.writeEndDocument(); 
        String request = w.getXmlString(); 
        w.close(); 
        System.debug('request..>>>>  '+request);
        return request;
    }
  
    //API-211
    @future(Callout=true)
    public static void callASyncAPI(List<id> appIds){
        callAPI(appIds,true);
    }
    //API-211
  
    public static Map<Id,Opportunity> callAPI(List<id> appIds,Boolean updateOpp) { //API-211
        map<id,Opportunity> returnAppMap = new map<id,Opportunity>(); //API-211
        try{
            List<contact> conList = new List<contact>();
            string Reqbody, body, response;
            map<id,Opportunity> appMap = new map<id,Opportunity>();
            
            for(Opportunity app:[Select id,Status__c, Contact__c, Contact__r.firstName, Contact__r.lastName, Contact__r.mailingcity, Contact__r.mailingpostalcode, Contact__r.mailingstate, Contact__r.mailingstreet from Opportunity where id IN :appIds]){
              appMap.put(app.Contact__c,app);
            }
               
            Phone_Append_SpeedeonData__c pas = Phone_Append_SpeedeonData__c.getInstance('PhoneAppendSettings');
            List<Opportunity> appList = new List<Opportunity>();
            for(Opportunity app1 : appMap.values()){
              
                Reqbody=createPhoneAppendXMLNodes(app1, pas);
        
                body = 'requestData=' + EncodingUtil.urlEncode(Reqbody, 'UTF-8');
                System.debug('pas.Phone_Append_End_url__c..>>>>  '+pas.Phone_Append_End_url__c);
                System.debug('body..>>>>  '+body);
                response= ProxyApiUtil.ApiGetResult(pas.Phone_Append_End_url__c,body);
                
                System.debug('response..>>>>  '+response);
                if(response <> null){
                    DOM.Document doc=new DOM.Document();
                    doc.load(response);
                    DOM.XmlNode rootNode=doc.getRootElement();
                    String phone = parseXML(rootNode);
                    System.debug('..>>phone..>>  '+phone);
                
                    if(phone <> null){
                        contact  conToUpdate = new Contact();
                        conToUpdate.id = app1.Contact__c;
                        conToUpdate.phone = phone;
                        conList.add(conToUpdate);
                        app1.Status__c = 'Credit Qualified';
                        appList.add(app1);
                        returnAppMap.put(app1.Id,app1); //API-211
                    }
                }
            }
            
            if(conList.size() > 0){
                update conList;
                System.debug('con update appList size..>>  '+appList.size());
            }
            
            if(appList.size() > 0 && updateOpp){
                System.debug('Before update appList size..>>  '+appList.size());
                update appList;
                System.debug('Update update appList size..>>  '+appList);
            }
        }catch (Exception e){
            System.debug(e.getMessage());
            //response = e.getMessage()+'  error at line '+e.getLineNumber();
            WebToSFDC.notifyDev('PhoneAppendSpeedeonDataCtrl Error - Callout error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
    
        return returnAppMap;
    }
  
    static void addCol(XmlStreamWriter w, String sel, String cel ) {
        w.writeStartElement(null, sel, null); 
        w.writeCharacters(cel); 
        w.writeEndElement();
    }
    
    public static string parseXML(DOM.XMLNode node) {
        String phone,nomatch,error;
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            system.debug(node.getName());
            
            for (Dom.XMLNode child: node.getChildElements()) {
                if(child.getName()=='noMatch'){
                    system.debug('noMatch..>>>>  '+node.getText());
                    nomatch = node.getText();
                }
            
                if(child.getName()=='error'){
                    system.debug('error..>>>>  '+node.getText());
                    error = node.getText();
                }
            
                if(child.getName()=='attributes' && nomatch == null && error == null){
                    for (Dom.XMLNode child1: child.getChildElements()) {
                    
                        if(child1.getName() == 'phone' && child1.getText() <> null){
                            system.debug('phone..>> '+child1.getText());
                            phone = child1.getText();
                        }
                    }
                }
            }
        
        }
        return phone;
    }
}