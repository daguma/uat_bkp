public with sharing class DL_TransactionsScreenCtrl {
    
	public String transactionsType {get; set;}
    public DL_AccountStatementSF statement {get; set;}
        
    public DL_TransactionsScreenCtrl(ApexPages.StandardController controller) {

        this.transactionsType = apexpages.currentpage().getparameters().get('description') + ' transactions';
        
        String customerId = apexpages.currentpage().getparameters().get('contact-id');
        String applicationId = apexpages.currentpage().getparameters().get('application-id');
        String accountNumber = apexpages.currentpage().getparameters().get('account-number');
        String typeCode = apexpages.currentpage().getparameters().get('type-code');
                
        if(String.isEmpty(customerId) || String.isEmpty(applicationId) || String.isEmpty(accountNumber))
            this.statement = null;
        else{

        	List<DL_AccountStatementSF> statementsList = ProxyApiBankStatements.ApiGetSpecificTransactions(customerId,applicationId,accountNumber,typeCode);
            
        	this.statement = statementsList.get(0);
    	}
        
    }
    
}