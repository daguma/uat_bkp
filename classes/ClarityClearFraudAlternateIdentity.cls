global class ClarityClearFraudAlternateIdentity {
    
    public boolean found_with_bank_account{get;set;}
    public String fraud_risk{get;set;}
    public String identity_type_code{get;set;}
    public String identity_type_description{get;set;}
    public String credit_risk{get;set;}
    public String total_number_of_fraud_indicators{get;set;}
    public String input_ssn_issue_date_cannot_be_verified{get;set;}
    public String input_ssn_recorded_as_deceased{get;set;}
    public String input_ssn_invalid{get;set;}
    public String best_on_file_ssn_issue_date_cannot_be_verified{get;set;}
    public String best_on_file_ssn_recorded_as_deceased{get;set;}
    public String high_probability_ssn_belongs_to_another{get;set;}
    public String ssn_reported_more_frequently_for_another{get;set;}
    public String inquiry_age_younger_than_ssn_issue_date{get;set;}
    public String credit_established_before_age_18{get;set;}
    public String credit_established_prior_to_ssn_issue_date{get;set;}
    public String more_than_3_inquiries_in_the_last_30_days{get;set;}
    public String inquiry_on_file_current_address_conflict{get;set;}
    public String inquiry_address_first_reported_in_lt_90_days{get;set;}
    public String inquiry_current_address_not_on_file{get;set;}
    public String inquiry_address_high_risk{get;set;}
    public String inquiry_address_non_residential{get;set;}
    public String inquiry_address_cautious{get;set;}
    public String on_file_address_high_risk{get;set;}
    public String on_file_address_non_residential{get;set;}
    public String on_file_address_cautious{get;set;}
    public String current_address_reported_by_new_trade_only{get;set;}
    public String current_address_reported_by_trade_open_lt_90_days{get;set;}
    public String driver_license_inconsistent_with_on_file{get;set;}
    public String telephone_number_inconsistent_with_address{get;set;}
    public String telephone_number_inconsistent_with_state{get;set;}
    public String work_phone_previously_listed_as_cell_phone{get;set;}
    public String work_phone_previously_listed_as_home_phone{get;set;}
    public String max_number_of_ssns_with_any_bank_account{get;set;}
    
}