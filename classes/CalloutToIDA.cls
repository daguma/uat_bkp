public class CalloutToIDA {
/* INC 1153 */
@InvocableMethod
    public static void invokeNetworkG_IDA(List<Contact> contacts){
        for(Contact con : contacts){
           if (!Test.isRunningTest()) ThirdPartyDetails.networkG_IDAnalytics(con.MailingPostalCode, con.SSN__c, con.FirstName, con.LastName, String.valueOf(con.Birthdate), con.MailingStreet, con.MailingCity, con.MailingState,con.Phone,con.Email,con.Lead__c,con.Id);
        }
        
    }
/* INC 1153 */
}