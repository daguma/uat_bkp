public with sharing class AchInformationCtrl {

    @AuraEnabled
    public static AchInformationEntity getAchInformationEntity(Id oppId, Opportunity opp) {

        AchInformationEntity aie = new AchInformationEntity();

        try {

            if (opp != null)
                aie.opportunity = opp; else if (oppId != null)
                aie.opportunity = [
                        SELECT ACH_Account_Number__c, ACH_Account_Type__c
                                ,ACH_Member_Account_Number__c
                                ,ACH_Routing_Number__c, ACH_Bank_Name__c, Others__c
                                ,Verified_Checklist__c, DE_Assigned_To_ACH__c
                                ,Invitation_Link__c
                        FROM Opportunity
                        WHERE id = :oppId
                        LIMIT 1
                ];

            if (aie.opportunity != null) {

                //Set the picklist values for ACH_Account_Type
                aie.achAccountTypesList =
                        alphaHelperCls.getPicklistItems(
                                Opportunity.ACH_Account_Type__c.getDescribe(),
                                aie.opportunity.ACH_Account_Type__c,
                                false
                        );

                //Set the picklist values for Verified_Checklist
                aie.verifiedChecklist =
                        alphaHelperCls.getPicklistItems(
                                Opportunity.Verified_Checklist__c.getDescribe(),
                                aie.opportunity.Verified_Checklist__c,
                                false
                        );

                //Set the picklist values for Data Entry Assigned
                aie.dataEntryAssignedList =
                        alphaHelperCls.getPicklistItems(
                                Opportunity.DE_Assigned_To_ACH__c.getDescribe(),
                                aie.opportunity.DE_Assigned_To_ACH__c,
                                true
                        );

            }


        } catch (Exception e) {
            system.debug(e.getMessage() + ' - ' + e.getStackTraceString());
        }
        return aie;

    }

    @AuraEnabled
    public static AchInformationSaveResult saveOpportunity(Opportunity opportunity) {
        AchInformationSaveResult aisr = new AchInformationSaveResult();
        try {
            if (opportunity != null) {

                /*************** Let's look for Contracts that use this account *********/
                Opportunity dbOpp = [
                        select contact__c, status__c, name, ACH_Routing_Number__c, ACH_Account_Number__c
                        ,Contact__r.Name, OwnerId
                        from opportunity
                        where id = : opportunity.id
                ];
                if (opportunity.ACH_Account_Number__c != '' && opportunity.ACH_Routing_Number__c != '') {
                    List<loan__Loan_Account__c> conts = [
                            Select Id, Name, loan__Contact__r.Name
                            from loan__Loan_Account__c
                            where loan__ACH_Account_Number__c = :opportunity.ACH_Account_Number__c
                            AND loan__ACH_Routing_Number__c = :opportunity.ACH_Routing_Number__c
                            AND loan__Contact__c <> :dbOpp.contact__c
                    ];
                    if (conts.size() > 0) {
                        aisr.warning = 'There is another funding with this bank account for a different person.'
                                + 'Please double check for Fraud: '
                                + conts.get(0).Name
                                + '(' + conts.get(0).loan__Contact__r.Name + ')';
                    }
                }

                List<Note> notesLst = [select id, title from Note where Parentid = : opportunity.id];
                boolean EWSNote = false;
                if (!notesLst.isempty() && notesLst.size() > 0) {
                    for (Note notelist : notesLst) {
                        if ((notelist.Title).contains('EWS Call Made on')) {
                            EWSNote = true;
                            break;
                        }
                    }
                }
                if (opportunity.status__c != 'Funded'
                        && String.isNotEmpty(opportunity.ACH_Account_Number__c )
                        && String.isNotEmpty(opportunity.ACH_Routing_Number__c)
                        && ((!EWSNote)
                        || (dbOpp.ACH_Routing_Number__c <> opportunity.ACH_Routing_Number__c
                        || dbOpp.ACH_Account_Number__c <> opportunity.ACH_Account_Number__c))) {
                    //****************************************
                    //*call  FT-Store2 asynchronously
                    //****************************************
                    FactorTrustUtil.FactorTrustEWS(opportunity.id);
                    Note noteDetails = new Note(ParentId = opportunity.id);
                    noteDetails.body = 'At ' + datetime.now().format('hh:mm:ss')
                            + ' on ' + date.today().format()
                            + ',' + UserInfo.GetName()
                            + ' made a call to EWS with the Bank Account Number ' + opportunity.ACH_Account_Number__c
                            + ' and Routing Number ' + opportunity.ACH_Routing_Number__c;
                    noteDetails.Title = 'EWS Call Made on ' + dbOpp.name;
                    insert noteDetails;
                } else {
                    //Saving the update status value from EWS call
                    opportunity.status__c = dbOpp.status__c;
                }

                Opportunity oppToSave = new Opportunity();
                oppToSave.Id = opportunity.id;
                oppToSave.ACH_Account_Number__c = opportunity.ACH_Account_Number__c;
                oppToSave.ACH_Account_Type__c = opportunity.ACH_Account_Type__c;
                oppToSave.ACH_Member_Account_Number__c = opportunity.ACH_Member_Account_Number__c;
                oppToSave.ACH_Routing_Number__c = opportunity.ACH_Routing_Number__c;
                oppToSave.ACH_Bank_Name__c = opportunity.ACH_Bank_Name__c;
                oppToSave.Others__c = opportunity.Others__c;
                oppToSave.Verified_Checklist__c = opportunity.Verified_Checklist__c;

                //Notify Docusign and create Note If there´s Not a Case already created
                handleNotifications(dbOpp);

                update oppToSave;
            }
        } catch (Exception e) {
            aisr.error = e.getMessage() + ' - ' + e.getStackTraceString();
        }
        return aisr;
    }

        /**
* handleNotifications:
*  This method handles the notifications:
*  - DocusignGroup
*  - Notes
*/
    private static void handleNotifications(Opportunity opp) {
        boolean existsCase = false;
        for (Case cases : [
                SELECT CaseNumber
                FROM Case
                WHERE ContactId = :opp.contact__c
                AND IsClosed = false
                limit 1
        ]) {
            existsCase = true;
        }
        if (!existsCase) {
            String subject = opp.Name + ' - Statements Available in ACH Information Tab';
            Datetime dateTimetemp = system.now();
            Date onlyDate = Date.newInstance(dateTimetemp.year(), dateTimetemp.month(), dateTimetemp.day());
            String onlyDateStr = onlyDate.format();
            String Body = opp.Contact__r.Name + ' has shared bank statements on ' + onlyDateStr + ' at ' + dateTimetemp.format('hh:mm:ss');
            LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
            String userId = cuSettings.Docusign_Notification_Email__c;

            WebToSFDC.notifyDev(subject, Body, userId);  // Notify DocusignGroup
            /* INC- 714 */ // Notify Application owner
            if (opp.OwnerId != null) {
                if (opp.OwnerId.getSObjectType() == User.SObjectType) { // Owner is a user
                    userId = opp.OwnerId;
                    //WebToSFDC.notifyDev(subject, Body, userId);
                } else if (opp.OwnerId.getSobjectType() == Group.SObjectType) { // Owner is a Queue
                    Group ownersQueue = [select Id from Group where id = :opp.OwnerId];
                    for (GroupMember gm : [Select UserOrGroupId From GroupMember where GroupId = :ownersQueue.Id]) {
                        WebToSFDC.notifyDev(subject, Body, gm.UserOrGroupId);
                    }
                }
            }

            /* INC- 714 */
            Note statementsNotification = new AANNotificationJob().createNote(Body, subject, opp.Id);
            insert statementsNotification;
        }
    }

    public class AchInformationEntity {
        @AuraEnabled public Opportunity opportunity { get; set; }
        //AuraEnabled does not support SelectOption; use PicklistItem instead of SelectOption
        @AuraEnabled public List<alphaHelperCls.PicklistItem> achAccountTypesList { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> verifiedChecklist { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> dataEntryAssignedList { get; set; }

        public AchInformationEntity() {
        }
    }

    public class AchInformationSaveResult{
        @AuraEnabled public Boolean hasError;
        @AuraEnabled public Boolean hasWarning;
        @AuraEnabled public String warning {
            get;
            set {
                warning = value;
                hasWarning = true;
            }
        }
        @AuraEnabled public String error {
            get;
            set {
                error = value;
                hasError = true;
            }
        }

        public AchInformationSaveResult(){
            hasError = false;
            hasWarning = false;
        }

    }
}