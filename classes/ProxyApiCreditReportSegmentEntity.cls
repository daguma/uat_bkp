global class ProxyApiCreditReportSegmentEntity {
    public Integer id {get; set;}
    public String value {get; set;}
    public Integer parentId {get; set;}
    public String prefix {get; set;}
    public ProxyApiCreditReportSegmentTypeEntity segmentTypeEntity {get; set;}
}