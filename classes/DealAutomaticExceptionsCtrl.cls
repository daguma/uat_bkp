public with sharing class DealAutomaticExceptionsCtrl {

    public DealAutomaticExceptionsCtrl() {
    }

    public static String getDelayExceptions() {
        return JSON.serialize([SELECT Name, Lead_Source__c,
                               Sub_Source__c, Point_Code__c, Delay__c,
                               Available_States__c, Selected_States__c,
                               FICO_Available__c, FICO_Selected__c,
                               Amount_Available__c, Amount_Selected__c,
                               Is_Active__c
                               FROM Deal_Automatic_Time_Delay_Exception__c
                               ORDER BY Name]);
    }

    @RemoteAction
    public static Boolean saveDelayExceptionData(Deal_Automatic_Time_Delay_Exception__c delayException) {

        Boolean result = false;

        if (delayException != null) {
            for (Deal_Automatic_Time_Delay_Exception__c delay : [SELECT Name, Lead_Source__c,
                    Sub_Source__c, Point_Code__c, Delay__c,
                    Available_States__c, Selected_States__c,
                    FICO_Available__c, FICO_Selected__c,
                    Amount_Available__c, Amount_Selected__c,
                    Is_Active__c
                    FROM Deal_Automatic_Time_Delay_Exception__c
                    WHERE Name = :delayException.Name]) {
                delay.Delay__c = delayException.Delay__c;
                delay.Available_States__c = delayException.Available_States__c;
                delay.Selected_States__c = delayException.Selected_States__c;
                delay.FICO_Available__c = delayException.FICO_Available__c;
                delay.FICO_Selected__c = delayException.FICO_Selected__c;
                delay.Amount_Available__c = delayException.Amount_Available__c;
                delay.Amount_Selected__c = delayException.Amount_Selected__c;
                delay.Is_Active__c = delayException.Is_Active__c;

                update delay;
                result = true;
            }
        }

        return result;
    }
}