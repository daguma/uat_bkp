@isTest
public class TestEmploymentCtrl {
    public static testMethod void testDocumentCtrl1() {
        Contact c = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.genesis__Contact__c = c.ID;
        insert a;

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        EmploymentCtrl ctrl  = new EmploymentCtrl(sc);
        ctrl.saveApp();
        // ctrl.getmynote();
        // ctrl.Savedoc();
        ctrl.hidepanel();
        ctrl.ShowSJSection();
        ctrl.ShowEmployNotes();
        ctrl.ShowTJSection();
        ctrl.CalcYear();
        ctrl.CalcYearForSJ();
        ctrl.Lessthan1render();
        ctrl.CalcYearForTJ();
        ctrl.EmployerRender();

        Employment_Details__c EmpDetail = new Employment_Details__c();
        EmpDetail.Application__c = a.Id;
        EmpDetail.Employment_Verification_Method__c = 'Not Verified';
        insert EmpDetail;

        c.Employment_Type__c = 'Employee';
        update c;

        sc = new ApexPages.StandardController(a);
        ctrl  = new EmploymentCtrl(sc);

    }
    public static Contact createContact() {
        Contact c = new Contact();
        c.LastName = 'Test';
        insert c;
        return c;
    }

}