@isTest(SeeAllData=false)
public class AttachmentTriggerTest {
    
    static testMethod void fileUploadTestMethod() {
        Opportunity app = TestHelper.createApplication();
        
        Attachment att1 = new Attachment();
        att1.ParentId = app.Id;
        att1.Name = 'TEST DOC 1';
        att1.Body = Blob.valueOf('Unit Test Attachment Body');
        att1.Description = 'File from APIautoworkflow: First Pay Stubs for Last 30 Days';
        insert att1;
        
        Attachment att = [SELECT Id FROM Attachment WHERE Id =: att1.Id];
        
        Attachment att2 = new Attachment();
        att2.ParentId = app.Id;
        att2.Name = 'TEST DOC 2';
        att2.Body = Blob.valueOf('Unit Test Attachment Body');
        att2.Description = 'File from APIautoworkflow: First Pay Stubs for Last 30 Days Delete AttachmentId: ' + att.Id;
        insert att2;
    }
    
}