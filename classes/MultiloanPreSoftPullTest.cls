@isTest public class MultiloanPreSoftPullTest {
    
    @testSetup static void initializes_objects(){
        MultiLoan_Settings__c multiSet = new MultiLoan_Settings__c();
        multiSet.name = 'settings';
        multiSet.Check_Frequency__c = 30;
        multiSet.Maximum_Loan_Amount__c = 5000;
        multiSet.Minimum_Loan_Amount__c = 1000;
        multiSet.Days_To_First_Check__c = 60;
        multiSet.AccountId__c = '0011700000t45FtAAI';
        multiSet.Allowed_Multiloan_States__c = 'PA,RI,SC,SD,TN,TX,UT,VA,WA,GA';
        insert multiSet;
        
        Account multiLoanAcc = new Account();
        multiLoanAcc.Name = 'Multi-Loan';
        insert multiLoanAcc;
        
    }
    
    @isTest static void get_eligibility_review_query_test(){
        
        System.assertEquals(true, String.isNotEmpty(MultiloanPreSoftPull.getEligibilityReviewQuery(5)));
    }
    @isTest static void passes_eligibility_check() {
        Multiloan_Params__c params = new Multiloan_Params__c();
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
		contract.loan__ACH_On__c = true;
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 1;
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(1);
        contract.loan__Loan_Amount__c = 4000;
        contract.loan__Principal_Paid__c = 2000;
        contract.loan__ACH_Routing_Number__c = '123456789';
        contract.loan__ACH_Account_Number__c = '0987654321';
        contract.loan__ACH_Debit_Amount__c = 50;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(6);
        contract.loan__Principal_Remaining__c = 1000;
        update contract;
        
        params.contract__c = contract.id;
        insert params;
        
        
        Opportunity opp = new Opportunity(Id = contract.Opportunity__c, Maximum_Loan_Amount__c = 6000);
        update opp;
        
        Integer appsBefore = [SELECT COUNT() FROM Opportunity where Contact__c = :contract.loan__Contact__c];
        MultiloanPreSoftPull.EligibilityCheck(params);
       // System.assert(appsBefore < [SELECT count() FROM  Opportunity where Contact__c = :contract.loan__Contact__c], 'Multiloan Opp created');
        
        
    }
    
    @isTest static void gets_payment_limit_by_frequency() {
        Integer result = MultiloanPreSoftPull.getLimitByFrequency('Monthly');

        System.assertEquals(6, result);

        result = MultiloanPreSoftPull.getLimitByFrequency('28 Days');

        System.assertEquals(6, result);

        result = MultiloanPreSoftPull.getLimitByFrequency('Semi-Monthly');

        System.assertEquals(12, result);

        result = MultiloanPreSoftPull.getLimitByFrequency('Bi-Weekly');

        System.assertEquals(13, result);

        result = MultiloanPreSoftPull.getLimitByFrequency('Weekly');

        System.assertEquals(24, result);
    }
    
    @isTest static void calculates_consecutive_payments_more_than_six_payments_for_monthly() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 6;
        update contract;
        
        Multiloan_Params__c ML = New Multiloan_Params__c();
        ML.Contract__c = contract.Id;
        insert ML;

        loan__Loan_account_Due_Details__c bill1 = new loan__Loan_account_Due_Details__c();
        bill1.loan__Payment_Satisfied__c = true;
        bill1.loan__Loan_Account__c = contract.Id;
        bill1.loan__DD_Primary_Flag__c = true;
        insert bill1;

        loan__Loan_account_Due_Details__c bill2 = new loan__Loan_account_Due_Details__c();
        bill2.loan__Payment_Satisfied__c = true;
        bill2.loan__Loan_Account__c = contract.Id;
        bill2.loan__DD_Primary_Flag__c = true;
        insert bill2;

        loan__Loan_account_Due_Details__c bill3 = new loan__Loan_account_Due_Details__c();
        bill3.loan__Payment_Satisfied__c = true;
        bill3.loan__Loan_Account__c = contract.Id;
        bill3.loan__DD_Primary_Flag__c = true;
        insert bill3;

        loan__Loan_account_Due_Details__c bill4 = new loan__Loan_account_Due_Details__c();
        bill4.loan__Payment_Satisfied__c = true;
        bill4.loan__Loan_Account__c = contract.Id;
        bill4.loan__DD_Primary_Flag__c = true;
        insert bill4;

        loan__Loan_account_Due_Details__c bill5 = new loan__Loan_account_Due_Details__c();
        bill5.loan__Payment_Satisfied__c = true;
        bill5.loan__Loan_Account__c = contract.Id;
        bill5.loan__DD_Primary_Flag__c = true;
        insert bill5;

        loan__Loan_account_Due_Details__c bill6 = new loan__Loan_account_Due_Details__c();
        bill6.loan__Payment_Satisfied__c = true;
        bill6.loan__Loan_Account__c = contract.Id;
        bill6.loan__DD_Primary_Flag__c = true;
        insert bill6;

        contract = [SELECT Id, Payment_Frequency_Masked__c, Opportunity__r.StrategyType__c FROM loan__Loan_Account__c where id = :contract.id];
        Boolean result = MultiloanPreSoftPull.overduedayEligibility(ML.Id, contract);

        System.assertEquals(true, result);
    }
    
    @isTest static void createIneligibilityNoteConsecutive_Payment_Fail() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 6;
        update contract;
        
        Multiloan_Params__c ML = New Multiloan_Params__c();
        ML.Contract__c = contract.Id;
        insert ML;

        loan__Loan_account_Due_Details__c bill1 = new loan__Loan_account_Due_Details__c();
        bill1.loan__Payment_Satisfied__c = true;
        bill1.loan__Loan_Account__c = contract.Id;
        bill1.loan__DD_Primary_Flag__c = true;
        insert bill1;

        loan__Loan_account_Due_Details__c bill2 = new loan__Loan_account_Due_Details__c();
        bill2.loan__Payment_Satisfied__c = false;
        bill2.loan__Loan_Account__c = contract.Id;
        bill2.loan__DD_Primary_Flag__c = true;
        insert bill2;

        loan__Loan_account_Due_Details__c bill3 = new loan__Loan_account_Due_Details__c();
        bill3.loan__Payment_Satisfied__c = true;
        bill3.loan__Loan_Account__c = contract.Id;
        bill3.loan__DD_Primary_Flag__c = true;
        insert bill3;

        loan__Loan_account_Due_Details__c bill4 = new loan__Loan_account_Due_Details__c();
        bill4.loan__Payment_Satisfied__c = true;
        bill4.loan__Loan_Account__c = contract.Id;
        bill4.loan__DD_Primary_Flag__c = true;
        insert bill4;

        loan__Loan_account_Due_Details__c bill5 = new loan__Loan_account_Due_Details__c();
        bill5.loan__Payment_Satisfied__c = true;
        bill5.loan__Loan_Account__c = contract.Id;
        bill5.loan__DD_Primary_Flag__c = true;
        insert bill5;

        loan__Loan_account_Due_Details__c bill6 = new loan__Loan_account_Due_Details__c();
        bill6.loan__Payment_Satisfied__c = true;
        bill6.loan__Loan_Account__c = contract.Id;
        bill6.loan__DD_Primary_Flag__c = true;
        insert bill6;

        contract = [SELECT Id, Payment_Frequency_Masked__c, Opportunity__r.StrategyType__c FROM loan__Loan_Account__c where id = :contract.id];
        Boolean result = MultiloanPreSoftPull.overduedayEligibility(ML.Id, contract);

        System.assertEquals(false, result);
    }
    
    @isTest static void refinanceEligibility_Fail() {
        
        Contact cnt = LibraryTest.createContactTH();
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Contact__c = cnt.Id;
        opp.Type = 'Refinance';
        opp.Status__c = 'Refinance Qualified';
        update opp;
        
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Contact__c = cnt.Id;
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 6;
        update contract;
        

        
        Multiloan_Params__c ML = New Multiloan_Params__c();
        ML.Contract__c = contract.Id;
        insert ML;

        contract = [SELECT Id, loan__Contact__c, Payment_Frequency_Masked__c, Opportunity__r.StrategyType__c FROM loan__Loan_Account__c where id = :contract.id];
        Boolean result = MultiloanPreSoftPull.refinanceEligibility(ML.Id, contract);

        System.assertEquals(false, result);
    }
    
    @isTest static void passes_eligibility_check_rules() {
        Multiloan_Params__c params = new Multiloan_Params__c();
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Type = 'New';
        opp.status__c = 'Funded';
        opp.Maximum_Loan_Amount__c = 12500;
        update opp;
        
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Opportunity__c = opp.Id;
		contract.loan__ACH_On__c = true;
        contract.loan__Frequency_of_Loan_Payment__c  = 'Monthly';
        contract.loan__Payment_Frequency_Cycle__c = 1;
        contract.loan__Oldest_Due_Date__c = Date.today().addDays(1);
        contract.loan__Loan_Amount__c = 4000;
        contract.loan__Principal_Paid__c = 2000;
        contract.loan__ACH_Routing_Number__c = '123456789';
        contract.loan__ACH_Account_Number__c = '0987654321';
        contract.loan__ACH_Debit_Amount__c = 50;
        contract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(6);
        contract.loan__Principal_Remaining__c = 1000;
        contract.loan__Loan_Amount__c = 4000;
        update contract;
        
        params.contract__c = contract.id;
        insert params;

        loan__Loan_account_Due_Details__c bill1 = new loan__Loan_account_Due_Details__c();
        bill1.loan__Payment_Satisfied__c = true;
        bill1.loan__Loan_Account__c = contract.Id;
        bill1.loan__DD_Primary_Flag__c = true;
        insert bill1;

        loan__Loan_account_Due_Details__c bill2 = new loan__Loan_account_Due_Details__c();
        bill2.loan__Payment_Satisfied__c = true;
        bill2.loan__Loan_Account__c = contract.Id;
        bill2.loan__DD_Primary_Flag__c = true;
        insert bill2;

        loan__Loan_account_Due_Details__c bill3 = new loan__Loan_account_Due_Details__c();
        bill3.loan__Payment_Satisfied__c = true;
        bill3.loan__Loan_Account__c = contract.Id;
        bill3.loan__DD_Primary_Flag__c = true;
        insert bill3;

        loan__Loan_account_Due_Details__c bill4 = new loan__Loan_account_Due_Details__c();
        bill4.loan__Payment_Satisfied__c = true;
        bill4.loan__Loan_Account__c = contract.Id;
        bill4.loan__DD_Primary_Flag__c = true;
        insert bill4;

        loan__Loan_account_Due_Details__c bill5 = new loan__Loan_account_Due_Details__c();
        bill5.loan__Payment_Satisfied__c = true;
        bill5.loan__Loan_Account__c = contract.Id;
        bill5.loan__DD_Primary_Flag__c = true;
        insert bill5;

        loan__Loan_account_Due_Details__c bill6 = new loan__Loan_account_Due_Details__c();
        bill6.loan__Payment_Satisfied__c = true;
        bill6.loan__Loan_Account__c = contract.Id;
        bill6.loan__DD_Primary_Flag__c = true;
        insert bill6;
        // Verificar xq se cae si no tiene un ACCM
        AccountManagementHistory__c accM = New AccountManagementHistory__c();
        accM.Opportunity__c = opp.Id;
        accM.CreditReportId__c = opp.PrimaryCreditReportId__c;
        accM.FicoValue__c = 729;
        accM.Contract__c = contract.Id;
        insert accM;
        
        Integer notesBefore = [SELECT COUNT() FROM Note WHERE ParentId = :contract.id];
        Integer appsBefore = [SELECT COUNT() FROM Opportunity where Contact__c = :contract.loan__Contact__c];
		
        MultiloanPreSoftpull.EligibilityCheck(params);

        //System.assert([SELECT COUNT() FROM Note WHERE ParentId = :contract.id] == notesBefore, 'Is Eligible - No note');
        //System.assert(appsBefore < [SELECT count() FROM  Opportunity where Contact__c = :contract.loan__Contact__c], 'Multiloan Opp created');
        
        MultiloanPreSoftpull.EligibilityCheck(params);
    }
    
}