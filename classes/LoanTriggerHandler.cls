public class LoanTriggerHandler {
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    loan__Office_Name__c lpOffice = null;
    Set<String> noDaysPastDueStatuses = new Set<String> {'Closed - Obligations met', 'Closed - Sold Off', 'Canceled'};

    public void onInsert(List<loan__loan_Account__c> newRecs, Map<Id, loan__loan_Account__c> oldRecs, Boolean isBefore) {
        Map<Id, loan__Loan_Account__c> mAcc = new Map<Id, loan__Loan_Account__c>();
        
        for (loan__Loan_Account__c theAcc : newRecs){
            if (theAcc.Opportunity__c != null) mAcc.put(theAcc.Opportunity__c, theAcc);
        }               
        
        Map<Id, Opportunity> mApps = new Map<Id, Opportunity>([Select Id, Payment_Frequency__c, Payment_Frequency_Masked__c, Payment_Frequency_Multiplier__c from Opportunity where id in: mAcc.keySet() LIMIT 1]);
        for ( loan__Loan_Account__c loan : mAcc.values()){
            if (loan.Opportunity__c != null) {
                Opportunity app = mApps.get(loan.Opportunity__c);
                System.debug(' ********PAYMENT FREQUENCY******  ' + app.Payment_Frequency__c);
                if (app != null && loan.Original_Term_Months__c == null) loan.Original_Term_Months__c = (loan.loan__Number_of_Installments__c != null ) ? ((loan.loan__Number_of_Installments__c / OfferCtrlUtil.getFrequencyUtil(app.Payment_Frequency_Masked__c)) * 12).setScale(1) : loan.Original_Term_Months__c;
            }
            
            if (isBefore)  calculateAPY(loan);
            
        }
    }
    
    public void onUpdate(List<loan__loan_Account__c> newRecs, Map<Id, loan__loan_Account__c> oldRecs, Boolean isBefore){
        boolean bAddOneDayInterest = cuSettings != null && ( cuSettings.Add_Day_of_Interest__c || cuSettings.Add_Day_Interest_to_Pay_Off__c);
        boolean bAddOneDayAll = cuSettings != null && ( cuSettings.Add_Day_of_Interest__c );
        
        for (loan__Office_Name__c lp :[select id,Name, loan__Current_System_Date__c  from 	loan__Office_Name__c LIMIT 1]){
            lpOffice = lp;
        }
        Date oldestDate;
        Set<Id> mWriteOffList = new Set<Id>();
        Map<Id, loan__loan_Account__c> newMap = new Map<Id, loan__loan_Account__c>();
        for(loan__loan_Account__c newAcc: newRecs){
            loan__loan_Account__c oldLoan = oldRecs.get(newAcc.Id);
            if (!isBefore && !oldLoan.Active_Charge_Off__c && newAcc.Active_Charge_Off__c  ) 
                mWriteOffList.add(newAcc.Id);
            if (isBefore )  {
                newMap.put(newAcc.Id, newAcc);
            }
            if (isBefore && newAcc.loan__Loan_Status__c != null && newAcc.loan__Loan_Status__c.contains('Active') && newAcc.Payment_Frequency_Masked__c != null)  newAcc.Current_Term_Months__c = (newAcc.loan__Term_Cur__c / OfferCtrlUtil.getFrequencyUtil(newAcc.Payment_Frequency_Masked__c)) * 12 ;
            if (isBefore && newAcc.loan__Loan_Status__c != null && newAcc.loan__Loan_Status__c.contains('Active') && (newAcc.loan__Principal_Remaining__c != oldLoan.loan__Principal_Remaining__c ||
                                                                                                                      newAcc.loan__Interest_Paid__c != oldLoan.loan__Interest_Paid__c ||
                                                                                                                      newAcc.Asset_Sale_Line__c != oldLoan.Asset_Sale_Line__c ||
                                                                                                                      newAcc.loan__Pmt_Amt_Cur__c != oldLoan.loan__Pmt_Amt_Cur__c ||
                                                                                                                      newAcc.loan__Term_Cur__c != oldLoan.loan__Term_Cur__c ||
                                                                                                                      newAcc.SCRA__c != oldLoan.SCRA__c ||
                                                                                                                      newAcc.Merchant_Disbursal_Amount__c != oldLoan.Merchant_Disbursal_Amount__c ||
                                                                                                                      newAcc.Is_On_Discount_Promotion__c != oldLoan.Is_On_Discount_Promotion__c ||
                                                                                                                      newAcc.loan__Interest_Rate__c != oldLoan.loan__Interest_Rate__c ||
                                                                                                                      newAcc.loan__Maturity_Date_Current__c != oldLoan.loan__Maturity_Date_Current__c ||
                                                                                                                      newAcc.Apy__c == null || newAcc.Apy__c == 0)) {
                                                                                                                          calculateAPY(newAcc);
                                                                                                                          
                                                                                                                      }
        }
        if (isBefore ){
            if (newMap.size() > 0) {
                Map<id,loan__loan_Account__c> mLoans = new  Map<id,loan__loan_Account__c>([SELECT ID, Payment_Frequency_Masked__c, loan__ACH_On__c, Is_Debit_Card_On__c,
                                                                                           	Debit_Card_Debit_Amount__c, Debit_Card_Debit_Day__c, Debit_Card_Start_Date__c,
											   												Debit_Card_Frequency__c, Debit_Card_End_Date__c,
                                                                                           (select Id, name, loan__Balance_Amount__c, loan__Due_Amt__c, loan__Due_Date__c, loan__Due_Type_Description__c, loan__Due_Type__c, loan__Payment_Amt__c,  loan__DD_Primary_Flag__c , loan__Payment_Date__c, 
                                                                                            loan__Payment_Satisfied__c, loan__Transaction_Date__c 
                                                                                            FROM loan__Dues_Details__r 
                                                                                            where loan__DD_Primary_Flag__c = true and loan__Payment_Satisfied__c = false 
                                                                                            order by loan__Due_Date__c asc LIMIT 1), 
                                                                                           (Select Id, loan__loan_Account__c, Payment_Pending_End_Date__c, loan__Interest__c 
                                                                                            from loan__Loan_Payment_Transactions__r where  loan__Reversed__c = false and (Payment_Pending_End_Date__c <> NULL OR loan__Interest__c <> 0) )
                                                                                           
                                                                                           From loan__Loan_Account__c 
                                                                                           where  Id in: newMap.keySet()]);
                
                for (loan__loan_Account__c newAcc : newMap.values()) {
                    boolean pPending = false;
                    Decimal totalInt = 0;
                    loan__loan_Account__c oldLoan = oldRecs.get(newAcc.Id);
                    
                    for (loan__Loan_Payment_Transaction__c pmt : mLoans.get(newAcc.Id).loan__Loan_Payment_Transactions__r){
                        if (pmt.Payment_Pending_End_Date__c <> null) newAcc.loan__Loan_Status__c = oldLoan.loan__Loan_Status__c ; 
                        if (pmt.Payment_Pending_End_Date__c <> null) pPending = true;
                        totalInt += pmt.loan__Interest__c != null ? pmt.loan__Interest__c : 0;
                    }
                    oldestDate = newAcc.loan__Next_Installment_Date__c;
                    for (loan__Loan_account_Due_Details__c bill : mLoans.get(newAcc.Id).loan__Dues_Details__r) {
                        if ( (oldestDate == null || (oldestDate != null && oldestDate > bill.loan__Due_Date__c))) oldestDate = bill.loan__Due_Date__c;
                    }
                    
                    if (!pPending && oldestDate != null ) {
                        newAcc.loan__Oldest_Due_Date__c = oldestDate;
                        newAcc.loan__Number_of_Days_Overdue__c = (Date.today().daysBetween(newAcc.loan__Oldest_Due_Date__c ) * -1);
                        if (newAcc.loan__Number_of_Days_Overdue__c <= newAcc.loan__Delinquency_Grace_Days__c) newAcc.loan__Number_of_Days_Overdue__c = 0;
                    }
                    
                    newAcc.Is_Payment_Pending__c = pPending;
                    if ((pPending || newAcc.loan__Charged_Off_Date__c != null) &&  newAcc.loan__Number_of_Days_Overdue__c <> oldLoan.loan__Number_of_Days_Overdue__c ) {
                        newAcc.loan__Oldest_Due_Date__c = oldLoan.loan__Oldest_Due_Date__c  ; 
                        newAcc.loan__Number_of_Days_Overdue__c = oldLoan.loan__Number_of_Days_Overdue__c  ; 
                        newAcc.loan__Delinquency_Aging_Interval__c = oldLoan.loan__Delinquency_Aging_Interval__c ; 
                    }
                    if (newAcc.Is_On_Discount_Promotion__c != oldLoan.Is_On_Discount_Promotion__c && newAcc.Is_On_Discount_Promotion__c == false
                        && newAcc.Promotion_Bust_Date__c == null) {
                            newAcc.Promotion_Bust_Date__c = Date.today();
                        }
                    
                    if (newAcc.Test_Bucket_Date__c != null && oldLoan.Test_Bucket_Date__c  == null){
                        newAcc.Inelligible_Date__c = null;
                        newAcc.Inelligible_Reason__c = null;
                    }
                    
                    if (newAcc.loan__Loan_Status__c != null && ( newAcc.loan__Loan_Status__c == 'Active - Marked for Closure' || newAcc.loan__Loan_Status__c == 'Closed - Obligations met') &&
                        newAcc.loan__Loan_Status__c != oldLoan.loan__Loan_Status__c && newAcc.Is_On_Discount_Promotion__c == true) {
                            newAcc.loan__Interest_Paid__c = newAcc.Promotion_Period_Rate__c == 0 ? 0 : totalInt;
                            newAcc.loan__Interest_Remaining__c = 0;
                        }
                    Integer AccrualPeriod = oldLoan.loan__Time_Counting_Method__c == clcommon.CLConstants.TIME_COUNTING_ACTUAL_360 ? 360 : oldLoan.loan__Time_Counting_Method__c == loan.LoanConstants.TIME_COUNTING_ACTUAL_DAYS ? 365 : 360;
                    Decimal perDiem = newAcc.loan__Interest_Rate__c != null ? (((newAcc.loan__Interest_Rate__c / 100) / AccrualPeriod) *
                                                                               newAcc.loan__Principal_Remaining__c) : 0;
                    
                    if ( lpOffice.loan__Current_System_Date__c !=null && newAcc.loan__Last_Accrual_Date__c != null &&
                        newAcc.loan__Last_Accrual_Date__c.daysBetween(lpOffice.loan__Current_System_Date__c) >= 0 &&
                        newAcc.loan__Accrual_Stop_Indicator__c == false && newAcc.loan__Charged_Off_Date__c == null){
                            newAcc.loan__Interest_Accrued_Not_Due__c = (newAcc.loan__Last_Accrual_Date__c.daysBetween(lpOffice.loan__Current_System_Date__c)  + (bAddOneDayAll ? 1 : 0)) * perDiem;
                            newAcc.loan__Interest_Accrued_On_Investment_Loans__c = (newAcc.loan__Last_Accrual_Date__c.daysBetween(lpOffice.loan__Current_System_Date__c.AddDays(-1))  + (bAddOneDayAll ? 1 : 0)) * perDiem;
                        }
                    
                    if (newAcc.Arbitration_Opt_Out__c != oldLoan.Arbitration_Opt_Out__c && newAcc.Arbitration_Opt_Out__c) newAcc.Arbitration_Opt_Out_Date__c = DateTime.now();
                    
                    if (newAcc.loan__Due_Day__c != oldLoan.loan__Due_Day__c) newAcc.Due_Date_Changed_Date__c = Date.today();
                    
                    newAcc.loan__Pay_Off_Amount_As_Of_Today__c = newAcc.loan__Principal_Remaining__c +
                        newAcc.loan__Interest_Accrued_Not_Due__c + 	
                        newAcc.loan__Interest_Remaining__c +
                        newAcc.loan__Fees_Remaining__c + (!bAddOneDayAll && bAddOneDayInterest ? perDiem : 0);
                    
                    if (newAcc.Is_Debit_Card_On__c != oldLoan.Is_Debit_Card_On__c && newAcc.Is_Debit_Card_On__c){
                        newAcc.loan__ACH_On__c = false;
                        newAcc.ACH_Flag_Reason__c = 'Customer Authorized';
                        newAcc.PaymentMethod__c = 'Debit Card';
                    }
                    if (newAcc.loan__ACH_On__c != oldLoan.loan__ACH_On__c && newAcc.loan__ACH_On__c){
                        newAcc.Is_Debit_Card_On__c = false;
                        newAcc.PaymentMethod__c = 'ACH';
                    }
                    if (newAcc.Is_Debit_Card_On__c != oldLoan.Is_Debit_Card_On__c){
                        loan__Other_Transaction__c OtherTrans = New loan__Other_Transaction__c();
                        OtherTrans.loan__Loan_Account__c = newAcc.Id;
                        OtherTrans.loan__Txn_Date__c = Date.today();                        
                        if(newAcc.Is_Debit_Card_On__c){
                            OtherTrans.loan__Transaction_Type__c = 'Debit Card Auto-Pay Enrollment';   
                            OtherTrans.Debit_Card_Debit_Amount__c = newAcc.Debit_Card_Debit_Amount__c;
                            OtherTrans.Debit_Card_Debit_Day__c = newAcc.Debit_Card_Debit_Day__c;
                            OtherTrans.Debit_Card_Start_Date__c = newAcc.Debit_Card_Start_Date__c;
                            OtherTrans.Debit_Card_Frequency__c = newAcc.Debit_Card_Frequency__c;
                            OtherTrans.Debit_Card_End_Date__c = newAcc.Debit_Card_End_Date__c;
                        }else{
                            OtherTrans.loan__Transaction_Type__c = 'Debit Card Auto-Pay Un-Enrollment';   
                        }
                        OtherTrans.RecordTypeId = Label.Payment_Method_Change_Record_Type;
                        insert OtherTrans;
                    }
                    if(!newAcc.Is_Debit_Card_On__c && !newAcc.loan__ACH_On__c) newAcc.PaymentMethod__c = 'Certified Check';
                    if(newAcc.loan__Loan_Status__c == 'Active - Marked for Closure' && newAcc.loan__Pay_Off_Amount_As_Of_Today__c < 1) newAcc.loan__Loan_Status__c = 'Closed - Obligations met';

                    if(noDaysPastDueStatuses.contains(newAcc.loan__Loan_Status__c)) newAcc.loan__Number_of_Days_Overdue__c = 0;
                }
            }
        } else if ( mWriteOffList.size() > 0) {
            
            System.debug('Writting off ' + mWriteOffList.size() + ' loans ');
            loan.WriteOffFactory wf = new loan.WriteOffFactory();
            wf.getWriteOffAPI().writeOffLoans(mWriteOffList);
        }
    }
    
    public void calculateAPY(loan__Loan_Account__c newAcc) {
        try {
            Decimal apy = null;
            Double npv_balance = 0, merchant_balance = null, mLeftDisc = 0, newRate = 0.0999;
            //  Boolean bUseSCRADiscount = cuSettings != null && cuSettings.SCRA_Discount__c;
            String line =  String.isNotEmpty(newAcc.Asset_Sale_Line__c) ? newAcc.Asset_Sale_Line__c : ''; 
            if (newAcc.Discount_End_date__c != null && lpOffice != null && lpOffice.loan__Current_System_Date__c != null) mLeftDisc = Math.round(Math.abs(newAcc.Discount_End_Date__c.daysBetween(lpOffice.loan__Current_System_Date__c)) / 30.4) ;
            
            if (cuSettings != null && cuSettings.Gugg1_minimum_APY__c  != null && cuSettings.Gugg2_minimum_APY__c != null ) newRate = (line == 'GUGG'  ? cuSettings.Gugg1_minimum_APY__c : cuSettings.Gugg2_minimum_APY__c) ;
            if (newRate == null) newRate = 0.0999 ;
            if (newAcc.Reporting_Fee__c == null) newAcc.Reporting_Fee__c = 0;
            // Let's calculate APY
            if (newAcc.Is_On_Discount_Promotion__c    && mLeftDisc > 0  && newAcc.loan__Principal_Remaining__c > 0 &&  newAcc.loan__Pmt_Amt_Cur__c != null && newAcc.loan__Pmt_Amt_Cur__c > 0 ) {
                Double numPayByPmt = (newAcc.loan__Principal_Remaining__c / newAcc.loan__Pmt_Amt_Cur__c).setScale(1) ;
                Double installRem = Math.ceil((Math.log(1 - ( 
                    (newAcc.loan__Principal_Remaining__c * 
                     (newAcc.loan__Interest_Rate__c / 100) / 
                     OfferCtrlUtil.daysPerPeriod(newAcc.Payment_Frequency_Masked__c))
                    / newAcc.loan__Pmt_Amt_Cur__c ))
                                               / Math.log(1 + ((newAcc.loan__Interest_Rate__c / 100)
                                                               / OfferCtrlUtil.daysPerPeriod(newAcc.Payment_Frequency_Masked__c)
                                                              )
                                                         )) * -1);
                
                
                /* npv_balance = (newAcc.loan__Pmt_Amt_Cur__c / (
newRate  / 12  ) )
* (1-(1/Math.pow(1 + (newRate / 12 ), remProMonth))) ; */
                
                npv_balance =  ((( newAcc.loan__Pmt_Amt_Cur__c/(newRate/12)*
                                  ( 1 - (1/( Math.pow((double)(1+(newRate/12)),Math.min(Math.min(numPayByPmt, mLeftDisc - 1), installRem) ))))
                                 ) + (   ( newAcc.loan__Principal_Remaining__c - Math.min(Math.min(numPayByPmt, mLeftDisc -1), installRem) * newAcc.loan__Pmt_Amt_Cur__c ) / 
                                      Math.pow((double)(1+(newRate/12)),Math.min(Math.min(numPayByPmt, mLeftDisc), installRem)))) * 1).setScale(2);
                
                Integer iter  = 0;
                for (apy = 0; apy < newRate * 100 && npv_balance > 0 && newAcc.loan__Principal_Remaining__c > 0 && iter < 5; iter++ ) {
                    apy = newAcc.loan__Principal_Remaining__c == 0 || npv_balance == 0  ? 0 : ((( newAcc.loan__Principal_Remaining__c - npv_balance) / npv_balance) / 
                                                                                               (( 
                                                                                                   (((( Math.min(Math.min(numPayByPmt, mLeftDisc), installRem) - 1) / 2) * ( 1 + ( Math.min(Math.min(numPayByPmt, mLeftDisc), installRem) - 1)))*   
                                                                                                    newAcc.loan__Pmt_Amt_Cur__c )+((newAcc.loan__Principal_Remaining__c -((Math.min(Math.min(numPayByPmt, mLeftDisc), installRem)-1)* newAcc.loan__Pmt_Amt_Cur__c))*
                                                                                                                                   Math.min(Math.min(numPayByPmt, mLeftDisc), installRem)))/ newAcc.loan__Principal_Remaining__c/12) ) * 100;
                    if (apy < newRate * 100) npv_balance = (npv_balance * .9999).setScale(2);
                }
            } else if (newAcc.loan__Maturity_Date_Current__c != null) {
                Boolean useOriginalInt = newAcc.Asset_Sale_Line__C != null && newAcc.Asset_Sale_Line__c == 'GUGG' ;
                Integer AccrualPeriod = newAcc.loan__Time_Counting_Method__c == clcommon.CLConstants.TIME_COUNTING_ACTUAL_360 ? 360 : newAcc.loan__Time_Counting_Method__c == loan.LoanConstants.TIME_COUNTING_ACTUAL_DAYS ? 365 : 360;
                Integer curMonth = useOriginalInt ? newAcc.Original_Term_Months__c.intValue() : Math.abs(newAcc.loan__Maturity_Date_Current__c.daysBetween(Date.valueOf(newAcc.CreatedDate)) / (AccrualPeriod/12.0) ).intValue();
                Decimal feePer = (newAcc.Reporting_Fee__c / (newacc.Original_Fee_Handling__c == 'Net Fee Out' ? newAcc.loan__Loan_Amount__c : newAcc.loan__Loan_Amount__c - newAcc.Reporting_Fee__c ) ) * 100;
                apy = ((feePer * 12) / curMonth) + ( useOriginalInt ? newAcc.loan__Contractual_Interest_Rate__c : newAcc.loan__Interest_Rate__c);
                if (newAcc.Merchant_Disbursal_Amount__c > 0 && 
                    (useOriginalInt ? newAcc.loan__Contractual_Interest_Rate__c : newAcc.loan__Interest_Rate__c) < 
                    (newRate * 100)) {
                        apy += (((newAcc.loan__Loan_Amount__c - newAcc.Merchant_Disbursal_Amount__c ) / newAcc.Merchant_Disbursal_Amount__c) / curMonth) * 100;
                    }
                
                /*
apy =   (Math.pow((double)(1 + 
(
(  (newAcc.loan__Interest_Rate__c / 100) + 
( feePer / 10000  )
) 
/ 
curMonth)), 
curMonth
) - 1
) * 100; */
                
            }
            
            
            if ((newAcc.Merchant_Disbursal_Amount__c != null && newAcc.Merchant_Disbursal_Amount__c > 0 && newAcc.Original_Sub_Source__c == 'LoanHero') ||
                ((newAcc.Original_Sub_Source__c == null || newAcc.Original_Sub_Source__c != 'LoanHero') && 
                 (newAcc.Is_On_Discount_Promotion__c || newAcc.Product_Name__c == 'Point of Need' || newAcc.FEB_Originated__c ) )) {
                     Decimal mDisAmt = newAcc.Original_Sub_Source__c == 'LoanHero' ? newAcc.Merchant_Disbursal_Amount__c : newAcc.loan__Loan_Amount__c;
                     merchant_balance =  (mDisAmt  - 
                                          ((( newAcc.Is_On_Discount_Promotion__c ? (newAcc.loan__Principal_Paid__c + newAcc.loan__Interest_Paid__c ) : newAcc.loan__Principal_Paid__c ) *
                                            (mDisAmt /  newAcc.loan__Loan_Amount__c) 
                                           ) * (newAcc.loan__Principal_Remaining__c > 0 && npv_balance > 0 && newAcc.Is_On_Discount_Promotion__c ? ( npv_balance / newAcc.loan__Principal_Remaining__c ) : 1))).setScale(2) ;
                 }
            
            newAcc.Apy__c = apy !=null ? apy.setScale(4) : apy ;
            newAcc.NPV_Balance__c = npv_balance  ;
            newAcc.merchant_balance__c = merchant_balance  ;
            System.debug('APY = ' + newAcc.Apy__c);
        } catch (Exception e) {
            System.debug( e.getMessage() + '\n' + e.getStackTraceString());
        }
        
    }
}