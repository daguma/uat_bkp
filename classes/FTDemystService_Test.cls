@isTest
private class FTDemystService_Test {

    @isTest static void calls_ft_service_no_pass() {
        opportunity app = LibraryTest.createApplicationLeadTH();

        String leadId = [SELECT Lead__c FROM Contact WHERE Id = : app.contact__c LIMIT 1].Lead__c;

        Map<string, string> PersonInfoMap = new Map<string, string>();
        PersonInfoMap.put('contactId', app.contact__c);
        PersonInfoMap.put('leadId', leadId);
        String req = JSON.serializePretty(PersonInfoMap);

        FTDemystService.FTDemystCall(req);

        Contact cUpdated = [SELECT Demyst_Email_Verification__c, Demyst_Email_Acceptance__c FROM Contact WHERE Id = : app.contact__c LIMIT 1];
    }

    @isTest static void calls_ft_service_with_settings_pass() {
        //FT configuration settings
        Factor_Trust_Configuration__c FT = new Factor_Trust_Configuration__c();
        FT.Name = 'FactorTrust';
        FT.Allow_to_proceed_on_failure__c = true;
        insert FT;

        opportunity app = LibraryTest.createApplicationTH();

        BridgerWDSFLookup__c bridger = new BridgerWDSFLookup__c();
        bridger.review_status__c = 'No match';
        bridger.Contact__c = app.contact__c;
        bridger.Lookup_By__c = 'test';
        bridger.Name_Searched__c = 'test';
        bridger.Number_of_Hits__c = 0;
        insert bridger;

        Map<string, string> PersonInfoMap = new Map<string, string>();
        PersonInfoMap.put('contactId', app.contact__c);
        string req = JSON.serializePretty(PersonInfoMap);

        FTDemystService.FTDemystCall(req);

        Contact cUpdated = [SELECT FT_Demyst_Response__c, Demyst_Email_Verification__c, Demyst_Email_Acceptance__c FROM Contact WHERE Id = : app.contact__c LIMIT 1];
    }

    @isTest static void calls_ofac_service() {

        Factor_Trust_Configuration__c FT = new Factor_Trust_Configuration__c();
        FT.Name = 'FactorTrust';
        FT.Allow_to_proceed_on_failure__c = true;
        insert FT;

        opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Credit Qualified';
        update app;

        FTDemystService.callOFACService(new LIST<opportunity> {app});
    }

    @isTest static void calls_ofac_service_null_product() {

        Factor_Trust_Configuration__c FT = new Factor_Trust_Configuration__c();
        FT.Name = 'FactorTrust';
        FT.Allow_to_proceed_on_failure__c = true;
        insert FT;

        opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Credit Qualified';
        app.Lending_Product__c = null;
        update app;

        FTDemystService.callOFACService(new LIST<opportunity> {app});
    }

    @isTest static void calls_ofac_service_catch_exception() {

        Integer logBefore = [SELECT COUNT() FROM Logging__c];

        opportunity app = LibraryTest.createApplicationTH();

        app = [SELECT Id, Name, contact__c FROM opportunity WHERE Id = : app.Id LIMIT 1];

        Test.startTest();

        FTDemystService.callOFACService(new LIST<opportunity> {app});

        Test.stopTest();

        System.assert([SELECT COUNT() FROM Logging__c] > logBefore, 'Catch Exception Logging');
    }

}