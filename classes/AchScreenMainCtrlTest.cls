@isTest
public class AchScreenMainCtrlTest {
    testmethod static void  test(){
                Contact con = new Contact(LastName = 'TestContact', Firstname = 'David', Email = 'test@gmail2.com', MailingState = 'GA');
        insert con;

        Opportunity app = new Opportunity(Name = 'Test Opportunity', CloseDate = System.today(), StageName = 'Qualification', Contact__c = con.Id, Status__c = 'NEW - ENTERED');
        insert app;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Acceptance__c = 'Y';
        score.Opportunity__c = app.id;
        score.ProdAnnualIncome__c = -1.041877;
        score.ProdCreditScore__c = 0;
        score.ProdDTI_Ratio__c = 0.1;
        score.ProdHCNC__c = 1;
        score.ProdInquiries_last_6_months__c = 1;
        score.ProdInst_Trades_Opened__c = 1;
        score.ProdSelfEmployed__c = 1;
        score.ProdTotal_Trade_Lines__c = 10;
        score.Logit__c = -1.79;
        score.Unit_Bad_Probability__c = 0.143;
        score.Dollar_Rate_Bad_Probability__c = 7.15;
        score.FICO_Score__c = 600;
        score.Contact__c = con.id;
        insert score;
AchScreenMainCtrl.AchScreenEntity t = AchScreenMainCtrl.getAchScreenEntity(app.id);
    }
}