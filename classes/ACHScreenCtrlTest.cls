@isTest
private class ACHScreenCtrlTest {

    @isTest static void gets_and_saves_note() {

        Contact contact = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.ACH_Account_Number__c = '12345678';
        a.ACH_Account_Type__c = 'Saving';
        a.ACH_Bank_Name__c = 'hdfc';
        a.ACH_Drawer_Name__c = 'khushboo';
        a.ACH_Routing_Number__c = '123456789';
        a.Bank_Statements_Information_XML__c = '<xml></xml>';
        a.Bank_Statements_From_Yodlee_Is_Ready__c = false;
        a.genesis__Contact__c = contact.id;
        insert a;

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        Integer notesBefore = [SELECT COUNT() FROM Note WHERE parentid = : a.id];

        ctrl.getmynote();
        ctrl.mynote.title = 'testing';
        ctrl.mynote.body = 'sdfsdfsf';
        ctrl.Savedoc();

        Integer notesAfter = [SELECT COUNT() FROM Note WHERE parentid = : a.id];

        System.assert(notesAfter > notesBefore, 'Insert a note');
    }

    @isTest static void hides_panel() {

        ApexPages.StandardController sc = new ApexPages.StandardController(new genesis__Applications__c());
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.hidepanel();

        System.assertEquals(true, ctrl.showpanel);
        System.assertEquals(false, ctrl.hideattach);
    }

    @isTest static void gets_period() {

        ApexPages.StandardController sc = new ApexPages.StandardController(new genesis__Applications__c());
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        List<SelectOption> initialPeriods = new List<SelectOption>();

        List<SelectOption> result = ctrl.getPeriods();

        System.assertNotEquals(initialPeriods, result);
    }

    @isTest static void saves_app() {

        Contact contact = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.ACH_Account_Number__c = '12345678';
        a.ACH_Account_Type__c = 'Saving';
        a.ACH_Bank_Name__c = 'hdfc';
        a.ACH_Drawer_Name__c = 'khushboo';
        a.ACH_Routing_Number__c = '123456789';
        a.Bank_Statements_Information_XML__c = '<xml></xml>';
        a.Bank_Statements_From_Yodlee_Is_Ready__c = false;
        a.genesis__Contact__c = contact.id;
        insert a;

        Integer notesBefore = [SELECT COUNT() FROM Note WHERE parentid = : a.id];

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);
        //Changes account number
        ctrl.application.ACH_Account_Number__c = '12345688';
        ctrl.application.ACH_Routing_Number__c = '123456788';

        ctrl.saveApp();

        Integer notesAfter = [SELECT COUNT() FROM Note WHERE parentid = : a.id];

        System.assert(notesAfter > notesBefore, 'Insert a note');

        //Check if there are no changes

        a.genesis__status__c = 'Credit Review';
        update a;

        ctrl.saveApp();

        System.assertEquals('Credit Review', ctrl.application.genesis__Status__c);
    }

    @isTest static void checks_verified_checklist_error() {
        Contact contact = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.ACH_Account_Number__c = '12345678';
        a.ACH_Account_Type__c = 'Saving';
        a.ACH_Bank_Name__c = 'hdfc';
        a.ACH_Drawer_Name__c = 'khushboo';
        a.ACH_Routing_Number__c = '123456789';
        a.Bank_Statements_Information_XML__c = '<xml></xml>';
        a.Bank_Statements_From_Yodlee_Is_Ready__c = false;
        a.genesis__Contact__c = contact.id;
        a.Verified_Checklist__c = 'Other';
        insert a;

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.saveApp();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Error : Please input value in Other field as Verified Checklist is Others', (msgs.get(index)).getDetail());
    }

    @isTest static void checks_account_and_routing_number_not_numeric_error() {
        Contact contact = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.ACH_Account_Number__c = 'abc';
        a.ACH_Account_Type__c = 'Saving';
        a.ACH_Bank_Name__c = 'hdfc';
        a.ACH_Drawer_Name__c = 'khushboo';
        a.ACH_Routing_Number__c = 'abc';
        a.Bank_Statements_Information_XML__c = '<xml></xml>';
        a.Bank_Statements_From_Yodlee_Is_Ready__c = false;
        a.genesis__Contact__c = contact.id;
        a.Confirm_ACH_Account_Number__c = 'abc';
        a.Confirm_ACH_Routing_Number__c = 'abc';
        insert a;

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.saveApp();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Error: The Account Number must be numeric. Please change accordingly and try again.', (msgs.get(index)).getDetail());

        //Changes account number to a valid number
        ctrl.application.ACH_Account_Number__c = '12345688';
        ctrl.hiddenACHAcctNbr = '12345688';
        ctrl.saveApp();

        msgs = ApexPages.getMessages();
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Error: The Routing Number must be numeric. Please change accordingly and try again.', (msgs.get(index)).getDetail());

        //Changes routing number to a valid number
        ctrl.application.ACH_Routing_Number__c = '123456788';
        ctrl.hiddenRoutingNbr = '123456788';
        ctrl.saveApp();

        msgs = ApexPages.getMessages();
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Error: The Confirmation ACH Routing Number must be numeric. Please change accordingly and try again.', (msgs.get(index)).getDetail());

        //Changes routing number to a valid number
        ctrl.application.Confirm_ACH_Routing_Number__c = '123456788';
        ctrl.hiddenConfRoutingNbr = '123456788';
        ctrl.saveApp();

        msgs = ApexPages.getMessages();
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Error: The Confirmation ACH Account Number must be numeric. Please change accordingly and try again.', (msgs.get(index)).getDetail());
    }

    @isTest static void notifies_statement_present() {
        Contact contact = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.ACH_Account_Number__c = '1111';
        a.ACH_Account_Type__c = 'Saving';
        a.ACH_Bank_Name__c = 'hdfc';
        a.ACH_Drawer_Name__c = 'khushboo';
        a.ACH_Routing_Number__c = '123456789';
        a.genesis__Contact__c = contact.id;
        a.Confirm_ACH_Account_Number__c = '1111';
        a.Confirm_ACH_Routing_Number__c = '123456789';
        insert a;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);
        ctrl.NotifyStatementsPresent();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Decision Logic report(s) available', (msgs.get(index)).getDetail());
    }

    @isTest static void loads_account_statement_number_empty() {
        genesis__Applications__c app = LibraryTest.createApplicationTH();
        app.ACH_Account_Number__c = '';
        update app;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ApexPages.StandardController sc = new ApexPages.StandardController(app);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);
        ctrl.NotifyStatementsPresent();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 2) : 0;
        System.assertEquals('Statement loaded', (msgs.get(index)).getDetail());
    }

    @isTest static void notifies_statement_present_null() {
        Contact contact = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.ACH_Account_Number__c = '1111';
        a.ACH_Account_Type__c = 'Saving';
        a.ACH_Bank_Name__c = 'hdfc';
        a.ACH_Drawer_Name__c = 'khushboo';
        a.ACH_Routing_Number__c = '123456789';
        a.genesis__Contact__c = contact.id;
        a.Confirm_ACH_Account_Number__c = '1111';
        a.Confirm_ACH_Routing_Number__c = '123456789';
        a.Bank_Statements_Information_XML__c = '<xml></xml>';
        insert a;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);
        ctrl.customerIdentifier = '';
        ctrl.NotifyStatementsPresent();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Decision Logic report(s) not available', (msgs.get(index)).getDetail());
    }

    @isTest static void sends_invitation_email() {

        Contact contact = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.ACH_Account_Number__c = '12345678';
        a.ACH_Account_Type__c = 'Saving';
        a.ACH_Bank_Name__c = 'hdfc';
        a.ACH_Drawer_Name__c = 'khushboo';
        a.ACH_Routing_Number__c = '123456789';
        a.genesis__Contact__c = contact.id;
        a.Confirm_ACH_Account_Number__c = '12345678';
        a.Confirm_ACH_Routing_Number__c = '123456789';
        insert a;

        ApexPages.StandardController sc = new ApexPages.StandardController(a);

        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);
        ctrl.sendInvitationEmail();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        System.debug('Messages: ' + msgs);
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'The invitation link for the email is not valid', (msgs.get(index)).getDetail());

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ctrl.sendInvitationEmail();

        genesis__applications__c app = [SELECT id, invitation_Link__c, invitation_Sent__c FROM genesis__applications__c WHERE id = : a.id LIMIT 1];

        System.assertEquals(false, app.Invitation_Sent__c);
        System.assertNotEquals(null, app.Invitation_Link__c);
        System.assertNotEquals('', app.Invitation_Link__c);

        msgs = ApexPages.getMessages();
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'The invitation email has been sent. A Note has been attached to this application.', (msgs.get(index)).getDetail());
    }

    @isTest static void re_sends_invitation_email() {

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.Invitation_Link__c = 'Re-send Email';
        update app;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ApexPages.StandardController sc = new ApexPages.StandardController(app);

        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);
        ctrl.sendInvitationEmail();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'The invitation email has been re-sent. A Note has been attached to this application.');

        System.assertEquals(false, [SELECT Invitation_Sent__c FROM genesis__Applications__c WHERE id = : app.id].Invitation_Sent__c);
    }

    @isTest static void uses_selected_account() {

        Contact contact = createContact();

        genesis__Applications__c a = new genesis__Applications__c();
        a.ACH_Account_Number__c = '1111';
        a.ACH_Account_Type__c = 'Saving';
        a.ACH_Bank_Name__c = 'hdfc';
        a.ACH_Drawer_Name__c = 'khushboo';
        a.ACH_Routing_Number__c = '123456789';
        a.genesis__Contact__c = contact.id;
        a.Confirm_ACH_Account_Number__c = '1111';
        a.Confirm_ACH_Routing_Number__c = '123456789';
        a.DL_Account_Number_Found__c = '1111';
        a.Verified_Checklist__c = 'Call the bank';
        insert a;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.useSelectedAccount();

        a = [SELECT Id, Verified_Checklist__c FROM genesis__applications__c WHERE Id = : a.Id LIMIT 1];

        System.assertEquals('DecisionLogic', a.Verified_Checklist__c);
    }

    @isTest static void search_by_bank_name() {

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.ACH_Bank_Name__c = 'Test';
        update app;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ApexPages.StandardController sc = new ApexPages.StandardController(app);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.getBankInformationByBankName();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Decision Logic is available by Bank Name entered', (msgs.get(index)).getDetail());
    }

    @isTest static void search_by_bank_name_not_available() {

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.ACH_Bank_Name__c = 'Test';
        update app;

        ApexPages.StandardController sc = new ApexPages.StandardController(app);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.getBankInformationByBankName();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Decision Logic is Not available by Bank Name, please try with a Routing Number.', (msgs.get(index)).getDetail());
    }

    @isTest static void search_by_routing_number() {
        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.ACH_Routing_Number__c = '123456789';
        update app;

        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        ApexPages.StandardController sc = new ApexPages.StandardController(app);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.getBankInformationByRoutingNumber();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().contains('Decision Logic is available, this bank "Chase - Bank" was found by the routing number entered.'), (msgs.get(index)).getDetail());
    }

    @isTest static void search_by_routing_number_no_call() {
        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.ACH_Routing_Number__c = '123456789';
        update app;

        ApexPages.StandardController sc = new ApexPages.StandardController(app);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.getBankInformationByRoutingNumber();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().contains('The Proxy API service is not available, please contact your system administrator.'), (msgs.get(index)).getDetail());
    }

    @isTest static void checks_decision_logic_availability() {
        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.ACH_Routing_Number__c = '123456789';
        app.ACH_Bank_Name__c = 'Test';
        update app;

        ApexPages.StandardController sc = new ApexPages.StandardController(app);
        ACHScreenCtrl ctrl  = new ACHScreenCtrl(sc);

        ctrl.getAvailability = '1';
        ctrl.checkDecisionLogicAvailability();

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail().contains('The Proxy API service is not available, please contact your system administrator.'), (msgs.get(index)).getDetail());

        ctrl.getAvailability = '2';
        ctrl.checkDecisionLogicAvailability();

        msgs = ApexPages.getMessages();
        index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Decision Logic is Not available by Bank Name, please try with a Routing Number.', (msgs.get(index)).getDetail());
    }

    private static Contact createContact() {
        if ([select Id from MaskSensitiveDataSettings__c].size() == 0 ) {
            MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();
            maskSettings.MaskAllData__c = false;
            insert maskSettings;
        }

        Contact contact = new Contact();
        contact.FirstName = 'Test';
        contact.LastName = 'TestCase';
        contact.Phone = '9876543210';
        contact.Email = 'test@test.com';
        contact.Use_of_Funds__c = 'Debt Consolidation';
        contact.MailingState = 'GA';
        contact.MailingPostalCode = '30145';
        contact.ints__Social_Security_Number__c = '123456789';
        insert contact;
        return contact;
    }

}