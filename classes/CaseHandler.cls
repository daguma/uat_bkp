/**
 * Class CaseHandler
 *
 * Trigger Handler for the Case SObject. This class implements the TriggerInterface
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
public without sharing class CaseHandler implements TriggerInterface
{   
    
    Map<String,String> contactMap = new Map<String,String>();
    Map<String,String> contactemailMap = new Map<String,String>();
	String lai = 'LAI-';
    // Constructor
    public CaseHandler(){
    }

    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    public void bulkBefore()
    {
		
		Set<String> subjects = new Set<String>();
        for(Case cs : (List<Case>)trigger.new){
			if(cs.Subject != null){
				subjects.add(cs.Subject);
			}
			
		} 
		System.debug('subjects..>>  '+subjects);
		Set<String> contractNames = new Set<String>();
		for(String ss:subjects){
			//System.debug('left..>>  '+ss.split('LAI-')[1].left(8));
			if(!String.isBlank(ss) && ss.containsIgnoreCase(lai) && ss.indexOf(lai) == 0 && ss.split(lai).size() > 0)
			contractNames.add(lai+ss.split(lai)[1].left(8));
		}
		System.debug('contractNames..>>  '+contractNames);

		for(loan__Loan_Account__c loanaccount : [Select id,Name,loan__Contact__c,loan__Contact__r.email from loan__Loan_Account__c where Name IN: contractNames]){
			System.debug('la..>>  '+loanaccount);
			contactMap.put(loanaccount.Name,loanaccount.loan__Contact__c);
		}

    }

    public void bulkAfter()
    {
        
    }

    public void beforeInsert(SObject so){
        Case cs = (Case)so;
        System.debug('case email..>> '+cs.Subject);
        System.debug('contactMap size..>> '+contactMap.size());
        
		if(!String.isBlank(cs.Subject) && cs.ContactId == null && cs.Subject.containsIgnoreCase(lai)&& cs.Subject.indexOf(lai) == 0 && cs.Subject.split(lai).size() > 0 && contactMap.containsKey(lai+cs.Subject.split(lai)[1].left(8))){
			cs.ContactId = contactMap.get(lai+cs.Subject.split(lai)[1].left(8));
        }
			
    }
        

    public void beforeUpdate(SObject oldSo, SObject so){
        Case cs = (Case)so;
        System.debug('case email..>> '+cs.Subject);
        System.debug('contactMap size..>> '+contactMap.size());
         
			
    }

    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    public void beforeDelete(SObject so)
    {
       System.debug('beforedelete ' + ((Case)so).OwnerId);
    }

    public void afterInsert(SObject so)
    {
       System.debug('afterInsert ' + ((Case)so).OwnerId);
         
    }

    public void afterUpdate(SObject oldSo, SObject so){
       System.debug('afterInsert old ' + ((Case)oldSo).OwnerId);
       System.debug('afterInsert ' + ((Case)so).OwnerId);
        
    }

    public void afterDelete(SObject so){
        
    }

    public void afterUndelete(SObject so){
        
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally(){
        
    }
}