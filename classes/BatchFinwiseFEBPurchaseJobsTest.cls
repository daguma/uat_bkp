@isTest public class BatchFinwiseFEBPurchaseJobsTest {
    
    @isTest static Void BatchFinwisePurchaseJobsTest(){
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        Date toda = CalculateWorkDays.dateWorkDays(Date.today(), -2);
        contract.ASset_Sale_Line__C = 'Finwise';
        contract.loan__Loan_Status__c = 'Active - Good Standing';
        contract.Asset_Sale_Date__C = toda;
        update contract;
        
        loan__ACH_Parameters__c ach = New loan__ACH_Parameters__c();
        ach.Name = 'ACH Test';
        ach.Disbursal_File_Cutoff_Hour__c = 16;
        ach.loan__Organization_Name__c = 'LENDINGPOINT LLC';
        insert ach;
        
        Test.startTest();
        
        BatchFinwisePurchaseJob job = new BatchFinwisePurchaseJob();
        database.executebatch(job);
        
        Test.stopTest();
    }
    
    @isTest static Void BatchFEBPurchaseJobsTest(){
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        Date toda = CalculateWorkDays.dateWorkDays(Date.today(), -3);
        contract.ASset_Sale_Line__C = 'FEB';
        contract.loan__Loan_Status__c = 'Active - Good Standing';
        contract.Asset_Sale_Date__C = toda;
        update contract;
        
        Test.startTest();
        
        BatchFEBPurchaseJob job = new BatchFEBPurchaseJob();
        database.executebatch(job);
        
        Test.stopTest();
    }
}