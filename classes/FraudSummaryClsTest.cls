@isTest class FraudSummaryClsTest {
    static testMethod void test(){
        
                LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
                ProxyApiUtil.settings = LibraryTest.fakeSettings();

         Email_Age_Settings__c emailAgeSetting = new Email_Age_Settings__c (Name='settings', Call_Email_age__c = True, Expiry_Days__c = 30, API_Key__c='1EV583CCvE0C96EFB5E08GBD045F0ED0', API_URL__c = 'https://test.emailage.com/emailagevalidator/', Auth_Token__c='B85HD3G6DF954E20BED7681964A5JE92');
         insert emailAgeSetting;
        
        Contact con = new Contact(LastName='TestContact',Firstname = 'David',Email = 'test@gmail2.com',MailingState = 'GA');
        insert con;
        
       /* Opportunity app = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id,Hawk_Alert_Codes__c='test,test');
        insert app;
        app.FT_Status__c = 'Unknown';
        app.Clarity_Submitted__c = true;
        app.Clarity_Fraud_Reason_Code__c = 'Pass';
        update app; */
        Opportunity app = LibraryTest.createApplicationTH();
        Scorecard__c sc = new Scorecard__c();
        sc.Grade__c = 'B1';
        sc.Acceptance__c = 'Y';
        sc.Opportunity__c = app.id;
        sc.High_Risk__c = true;
        insert sc;

        
        
        FraudSummaryCls t = FraudSummaryCls.getFraudSummaryEntity(app.id);
        t.setErrorMessage('Test');
        FraudSummaryCls.updateOpportunity(app);
       
        
    }

  static testMethod void test2(){
        
                LibraryTest.createBrmsConfigSettings(1, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
                ProxyApiUtil.settings = LibraryTest.fakeSettings();

         Email_Age_Settings__c emailAgeSetting = new Email_Age_Settings__c (Name='settings', Call_Email_age__c = True, Expiry_Days__c = 30, API_Key__c='1EV583CCvE0C96EFB5E08GBD045F0ED0', API_URL__c = 'https://test.emailage.com/emailagevalidator/', Auth_Token__c='B85HD3G6DF954E20BED7681964A5JE92');
         insert emailAgeSetting;
        
        Contact con = new Contact(LastName='TestContact',Firstname = 'David',Email = 'test@gmail2.com',MailingState = 'GA');
        insert con;
        
       /* Opportunity app = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id,Hawk_Alert_Codes__c='test,test');
        insert app;
        app.FT_Status__c = 'Unknown';
        app.Clarity_Submitted__c = true;
        app.Clarity_Fraud_Reason_Code__c = 'Pass';
        update app; */
        Opportunity app = LibraryTest.createApplicationTH();
        app.Hawk_Alert_Codes__c='test,test';
        update app;
        Scorecard__c sc = new Scorecard__c();
        sc.Grade__c = 'B1';
        sc.Acceptance__c = 'Y';
        sc.Opportunity__c = app.id;
        sc.High_Risk__c = true;
        insert sc;

        
        
        FraudSummaryCls t = FraudSummaryCls.getFraudSummaryEntity(app.id);
        t.setErrorMessage('Test');
        FraudSummaryCls.updateOpportunity(app);
       
        
    }

}