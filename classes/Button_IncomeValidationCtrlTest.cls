@isTest public with sharing class Button_IncomeValidationCtrlTest {

    static testMethod void test1() {
    ProxyApiUtil.settings = LibraryTest.fakeSettings();
      
        Button_IncomeValidationCtrl.IncomeValidationEntity da = Button_IncomeValidationCtrl.validateIncome(null);
        Button_IncomeValidationCtrl.IncomeValidationEntity btnInnerCls = new Button_IncomeValidationCtrl.IncomeValidationEntity();
        btnInnerCls.successMessage = 'Success';              

    }
    static testMethod void test2() {
    ProxyApiUtil.settings = LibraryTest.fakeSettings();
     string productID = Label.ProdIDPoN;//'a250B00000CZr3BQAT';
      User hal = [SELECT Id, Name FROM User WHERE Name like '%HAL%' LIMIT 1];
       ezVerify_Settings__c ezVerifyCustom = new ezVerify_Settings__c(SetupOwnerId=hal.Id, EZVerify_Base_URL__c='https://test.ezverify.me/api/lend', Product_Name__c = 'Point Of Need', Default_Interest_Rate__c = 3.99, Promotional_Period__c = 90, Product_Name_Dev__c = 'Point Of Need',Loan_Product_ID__c=productID,DL_Income_Variance__c=10,SocialMedicareTaxRate__c=7);
        insert ezVerifyCustom;
        //Contact TestContact = LibraryTest.createContactTH();
        
         Opportunity opp= TestHelper.createApplication();
        
        Button_IncomeValidationCtrl.IncomeValidationEntity da1 = Button_IncomeValidationCtrl.validateIncome(opp.Id);
               

    }
    static testMethod void test3() {
    ProxyApiUtil.settings = LibraryTest.fakeSettings();
      string productID = Label.ProdIDPoN;//'a250B00000CZr3BQAT';
       User hal = [SELECT Id, Name FROM User WHERE Name like '%HAL%' LIMIT 1];
       ezVerify_Settings__c ezVerifyCustom = new ezVerify_Settings__c(SetupOwnerId=hal.Id, EZVerify_Base_URL__c='https://test.ezverify.me/api/lend', Product_Name__c = 'Point Of Need', Default_Interest_Rate__c = 3.99, Promotional_Period__c = 90, Product_Name_Dev__c = 'Point Of Need',Loan_Product_ID__c=productID,DL_Income_Variance__c=10,SocialMedicareTaxRate__c=7);
        insert ezVerifyCustom;        
         Opportunity opp= TestHelper.createApplication();
        opp.LeadSource__c='EZverify';
        opp.ProductName__c='Point Of Need';
        update opp;
        Button_IncomeValidationCtrl.IncomeValidationEntity da1 = Button_IncomeValidationCtrl.validateIncome(opp.Id);
               

    }
}