@isTest public class CreditReportPreATBTest {

    @isTest static void getByEntityId() {
        genesis__applications__c app = LibraryTest.createApplicationTH();
        ProxyApiCreditReportEntity result = CreditReportPreATB.getByEntityId(app.id);

        System.assertEquals('No disqualifiers Pre-ATB information was found.', result.errorMessage);

        Disqualifiers_Pre_ATB__c disqualifiers = new Disqualifiers_Pre_ATB__c();
        disqualifiers.AnnualIncome__c = 80000.0;
        disqualifiers.AppId__c = app.id;
        disqualifiers.DTI__c = 20;
        disqualifiers.EmploymentType__c = 'Employee';
        disqualifiers.EmploymentDurationYrs__c = 2;
        disqualifiers.PriorEmploymentDurationYears__c = 3;
        disqualifiers.CreditScore__c = 20;
        insert disqualifiers;

        result = CreditReportPreATB.getByEntityId(app.id);

        System.assertNotEquals(null, result);
        System.assertNotEquals('No disqualifiers Pre-ATB information was found.', result.errorMessage);
    }

    @isTest static void getDecisionByEntityId() {
        genesis__applications__c app = LibraryTest.createApplicationTH();
        String result = CreditReportPreATB.getDecisionByEntityId(app.id);

        System.assertEquals('', result);

        Disqualifiers_Pre_ATB__c disqualifiers = new Disqualifiers_Pre_ATB__c();
        disqualifiers.AnnualIncome__c = 80000.0;
        disqualifiers.AppId__c = app.id;
        disqualifiers.DTI__c = 20;
        disqualifiers.EmploymentType__c = 'Employee';
        disqualifiers.EmploymentDurationYrs__c = 2;
        disqualifiers.PriorEmploymentDurationYears__c = 3;
        disqualifiers.CreditScore__c = 20;
        disqualifiers.Decision__c = 'Y';
        insert disqualifiers;

        result = CreditReportPreATB.getDecisionByEntityId(app.id);

        System.assertEquals('Y', result);
    }
}