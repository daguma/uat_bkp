/*This job would populate Total_Amount_Due__c 
field value with Total_Due_Amount__c in charges*/

global class PopulateTotalAmountDueForChargeJob implements Database.Batchable<sObject>{
    
    Date chargeDueDate;
    Boolean isChargetriggerDisabled = false;
    
    /*If charges from specific date to be picked*/
    global PopulateTotalAmountDueForChargeJob(Date chargeDueDate){
        this.chargeDueDate = chargeDueDate;
        disableChargeTrigger();
    }
    
    /*If Charges upto six months old only to be picked*/
    global PopulateTotalAmountDueForChargeJob(){
        Loan.GlobalLoanUtilFacade globalLoanUtilFacade = new Loan.GlobalLoanUtilFacade();
        this.chargeDueDate = globalLoanUtilFacade.getCurrentSystemDate();
        this.chargeDueDate = this.chargeDueDate.addMonths(-6);
        disableChargeTrigger();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'SELECT id,';
        query +=              'Loan__Total_Due_Amount__c,';
        query +=              'Loan__Total_Amount_Due__c ';
        query +=        'FROM Loan__Charge__c ';
        query +=        'WHERE Loan__Total_Amount_Due__c = null AND ';
        query +=              'loan__Date__c >= : chargeDueDate';
        
        return Database.getQueryLocator(query) ;
    }
    
    global void execute(Database.BatchableContext bc, List<Loan__Charge__c> charges){
        try{
            for(Loan__Charge__c charge : charges){
                charge.Loan__Total_Amount_Due__c = charge.Loan__Total_Due_Amount__c;
            }
            update charges;
        }
        catch(Exception e){
            system.debug(LoggingLevel.ERROR, 'Error occured while updating charges : '+ e.getMessage());
            throw e;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        /*If charge trigger not already disabled by user then only enable it*/
        Loan__Trigger_Parameters__c triggerParameter = Loan__Trigger_Parameters__c.getInstance();
        if(!isChargeTriggerDisabled){
            enableChargeTrigger();
        }
    }
    
    public void disableChargeTrigger(){
        Loan__Trigger_Parameters__c triggerParameter = Loan__Trigger_Parameters__c.getInstance();
        isChargetriggerDisabled = triggerParameter.loan__Disable_Charge_Trigger__c;
        triggerParameter.loan__Disable_Charge_Trigger__c = true;
        upsert triggerParameter;
    }
    
    public void enableChargeTrigger(){
        Loan__Trigger_parameters__c chargeTriggerParameter = Loan__Trigger_Parameters__c.getInstance();
        chargeTriggerParameter.loan__Disable_Charge_Trigger__c = false;
        update  chargeTriggerParameter;
    }
}