@isTest
public class ClarityClearFraudAlternateIdentityTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearFraudAlternateIdentity ccf = new ClarityClearFraudAlternateIdentity();
        Test.stopTest();
        
        System.assertEquals(null, ccf.found_with_bank_account);
        System.assertEquals(null, ccf.fraud_risk);
        System.assertEquals(null, ccf.identity_type_code);
        System.assertEquals(null, ccf.identity_type_description);
        System.assertEquals(null, ccf.credit_risk);
        System.assertEquals(null, ccf.total_number_of_fraud_indicators);
        System.assertEquals(null, ccf.input_ssn_issue_date_cannot_be_verified);
        System.assertEquals(null, ccf.input_ssn_recorded_as_deceased);
        System.assertEquals(null, ccf.input_ssn_invalid);
        System.assertEquals(null, ccf.best_on_file_ssn_issue_date_cannot_be_verified);
        System.assertEquals(null, ccf.best_on_file_ssn_recorded_as_deceased);
        System.assertEquals(null, ccf.high_probability_ssn_belongs_to_another);
        System.assertEquals(null, ccf.ssn_reported_more_frequently_for_another);
        System.assertEquals(null, ccf.inquiry_age_younger_than_ssn_issue_date);
        System.assertEquals(null, ccf.credit_established_before_age_18);
        System.assertEquals(null, ccf.credit_established_prior_to_ssn_issue_date);
        System.assertEquals(null, ccf.more_than_3_inquiries_in_the_last_30_days);
        System.assertEquals(null, ccf.inquiry_on_file_current_address_conflict);
        System.assertEquals(null, ccf.inquiry_address_first_reported_in_lt_90_days);
        System.assertEquals(null, ccf.inquiry_current_address_not_on_file);
        System.assertEquals(null, ccf.inquiry_address_high_risk);
        System.assertEquals(null, ccf.inquiry_address_non_residential);
        System.assertEquals(null, ccf.inquiry_address_cautious);
        System.assertEquals(null, ccf.on_file_address_high_risk);
        System.assertEquals(null, ccf.on_file_address_non_residential);
        System.assertEquals(null, ccf.on_file_address_cautious);
        System.assertEquals(null, ccf.current_address_reported_by_new_trade_only);
        System.assertEquals(null, ccf.current_address_reported_by_trade_open_lt_90_days);
        System.assertEquals(null, ccf.driver_license_inconsistent_with_on_file);
        System.assertEquals(null, ccf.telephone_number_inconsistent_with_address);
        System.assertEquals(null, ccf.telephone_number_inconsistent_with_state);
        System.assertEquals(null, ccf.work_phone_previously_listed_as_cell_phone);
        System.assertEquals(null, ccf.work_phone_previously_listed_as_home_phone);
        System.assertEquals(null, ccf.max_number_of_ssns_with_any_bank_account);
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearFraudAlternateIdentity ccf = new ClarityClearFraudAlternateIdentity();
        Test.stopTest();
        
        ccf.found_with_bank_account= false;
        ccf.fraud_risk= 'test';
        ccf.identity_type_code= 'test';
        ccf.identity_type_description= 'test';
        ccf.credit_risk= 'test';
        ccf.total_number_of_fraud_indicators= 'test';
        ccf.input_ssn_issue_date_cannot_be_verified= 'test';
        ccf.input_ssn_recorded_as_deceased= 'test';
        ccf.input_ssn_invalid= 'test';
        ccf.best_on_file_ssn_issue_date_cannot_be_verified= 'test';
        ccf.best_on_file_ssn_recorded_as_deceased= 'test';
        ccf.high_probability_ssn_belongs_to_another= 'test';
        ccf.ssn_reported_more_frequently_for_another= 'test';
        ccf.inquiry_age_younger_than_ssn_issue_date= 'test';
        ccf.credit_established_before_age_18= 'test';
        ccf.credit_established_prior_to_ssn_issue_date= 'test';
        ccf.more_than_3_inquiries_in_the_last_30_days= 'test';
        ccf.inquiry_on_file_current_address_conflict= 'test';
        ccf.inquiry_address_first_reported_in_lt_90_days= 'test';
        ccf.inquiry_current_address_not_on_file= 'test';
        ccf.inquiry_address_high_risk= 'test';
        ccf.inquiry_address_non_residential= 'test';
        ccf.inquiry_address_cautious= 'test';
        ccf.on_file_address_high_risk= 'test';
        ccf.on_file_address_non_residential= 'test';
        ccf.on_file_address_cautious= 'test';
        ccf.current_address_reported_by_new_trade_only= 'test';
        ccf.current_address_reported_by_trade_open_lt_90_days= 'test';
        ccf.driver_license_inconsistent_with_on_file= 'test';
        ccf.telephone_number_inconsistent_with_address= 'test';
        ccf.telephone_number_inconsistent_with_state= 'test';
        ccf.work_phone_previously_listed_as_cell_phone= 'test';
        ccf.work_phone_previously_listed_as_home_phone= 'test';
        ccf.max_number_of_ssns_with_any_bank_account= 'test';
        
        System.assertEquals(false, ccf.found_with_bank_account);
        System.assertEquals('test', ccf.fraud_risk);
        System.assertEquals('test', ccf.identity_type_code);
        System.assertEquals('test', ccf.identity_type_description);
        System.assertEquals('test', ccf.credit_risk);
        System.assertEquals('test', ccf.total_number_of_fraud_indicators);
        System.assertEquals('test', ccf.input_ssn_issue_date_cannot_be_verified);
        System.assertEquals('test', ccf.input_ssn_recorded_as_deceased);
        System.assertEquals('test', ccf.input_ssn_invalid);
        System.assertEquals('test', ccf.best_on_file_ssn_issue_date_cannot_be_verified);
        System.assertEquals('test', ccf.best_on_file_ssn_recorded_as_deceased);
        System.assertEquals('test', ccf.high_probability_ssn_belongs_to_another);
        System.assertEquals('test', ccf.ssn_reported_more_frequently_for_another);
        System.assertEquals('test', ccf.inquiry_age_younger_than_ssn_issue_date);
        System.assertEquals('test', ccf.credit_established_before_age_18);
        System.assertEquals('test', ccf.credit_established_prior_to_ssn_issue_date);
        System.assertEquals('test', ccf.more_than_3_inquiries_in_the_last_30_days);
        System.assertEquals('test', ccf.inquiry_on_file_current_address_conflict);
        System.assertEquals('test', ccf.inquiry_address_first_reported_in_lt_90_days);
        System.assertEquals('test', ccf.inquiry_current_address_not_on_file);
        System.assertEquals('test', ccf.inquiry_address_high_risk);
        System.assertEquals('test', ccf.inquiry_address_non_residential);
        System.assertEquals('test', ccf.inquiry_address_cautious);
        System.assertEquals('test', ccf.on_file_address_high_risk);
        System.assertEquals('test', ccf.on_file_address_non_residential);
        System.assertEquals('test', ccf.on_file_address_cautious);
        System.assertEquals('test', ccf.current_address_reported_by_new_trade_only);
        System.assertEquals('test', ccf.current_address_reported_by_trade_open_lt_90_days);
        System.assertEquals('test', ccf.driver_license_inconsistent_with_on_file);
        System.assertEquals('test', ccf.telephone_number_inconsistent_with_address);
        System.assertEquals('test', ccf.telephone_number_inconsistent_with_state);
        System.assertEquals('test', ccf.work_phone_previously_listed_as_cell_phone);
        System.assertEquals('test', ccf.work_phone_previously_listed_as_home_phone);
        System.assertEquals('test', ccf.max_number_of_ssns_with_any_bank_account);
    }
}