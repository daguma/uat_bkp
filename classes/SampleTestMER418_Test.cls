@isTest(SeeAllData=false)
public class SampleTestMER418_Test {
    
    static testMethod void testGetAccountLocationsById(){ 
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.Location_Min_Limit__c = 0;
        amcoSetting.Location_Max_Limit__c = 10;
        insert amcoSetting;
        
        Account parentAccount = new Account();
        parentAccount.Name = 'Test Parent Account';
        parentAccount.Type = 'Merchant';
        parentAccount.Active_Partner__c = True;
        parentAccount.Cobranding_Name__c = 'Test Name1';
        parentAccount.Partner_Sub_Source__c = 'TestName1';
        insert parentAccount;
        
        Account childAccount = new Account();
        childAccount.Name = 'Test Child Account';
        childAccount.Type = 'Merchant';
        childAccount.Active_Partner__c = True;
        childAccount.ParentId = parentAccount.Id;
        childAccount.Cobranding_Name__c = 'Test Name2';
        childAccount.Partner_Sub_Source__c = 'TestName2';
        insert childAccount;
        
        Account parentAccountWithoutChild = new Account();
        parentAccountWithoutChild.Name = 'Test Parent Account';
        parentAccountWithoutChild.Type = 'Merchant';
        parentAccountWithoutChild.Active_Partner__c = True;
        parentAccountWithoutChild.Cobranding_Name__c = 'Test Name';
        parentAccountWithoutChild.Partner_Sub_Source__c = 'TestName';
        insert parentAccountWithoutChild;
        
        
        Account  acc = [select id from account where Type='Merchant' AND Active_partner__c = True Limit 1];
        Account  acc1 = [select id from account Limit 1];
        
        Test.StartTest();
        String requestJson = '{"accountId": "'+acc.Id+'","lowerLimit":"0","upperLimit":"10"}';
        String responseJson = SampleTestMER418.getAccountLocationsById(requestJson);
        String responseJson1 = SampleTestMER418.getAccountLocationsById('{"accountId": "'+acc1.id+'"}');
        String responseJson2 = SampleTestMER418.getAccountLocationsById('{"accountId": "'+acc.id+'"}');
        String responseJson3 = SampleTestMER418.getAccountLocationsById('{"accountId": "'+acc.Id+'","lowerLimit":"0","upperLimit":"10","searchBy":"am"}');
        AccountLocationsWrapper objWrapper = (AccountLocationsWrapper)JSON.deserialize(responseJson, AccountLocationsWrapper.class);
        AccountLocationsWrapper objWrapper1 = (AccountLocationsWrapper)JSON.deserialize(responseJson1, AccountLocationsWrapper.class);
        AccountLocationsWrapper objWrapper2 = (AccountLocationsWrapper)JSON.deserialize(responseJson2, AccountLocationsWrapper.class);
        AccountLocationsWrapper objWrapper3 = (AccountLocationsWrapper)JSON.deserialize(responseJson3, AccountLocationsWrapper.class);
        SampleTestMER418.getAccountLocationsById('{"accountId": ""}');
        //System.assertEquals('200', objWrapper.status);
        //System.assertEquals('Locations found Successfully', objWrapper.message);
        
        //Updating custom setting field AAMCO_ID__c, with Account which have no child Account in heirarchy  
        //cuSettings.AAMCO_ID__c = parentAccountWithoutChild.Id;
        //update cuSettings;
        
        requestJson = '{"accountId": "'+parentAccount.Id+'"}';
        responseJson = SampleTestMER418.getAccountLocationsById(requestJson);
        objWrapper = (AccountLocationsWrapper)JSON.deserialize(responseJson, AccountLocationsWrapper.class);
        //System.assertEquals('Err', objWrapper.status);
        //System.assertEquals('No Locations found', objWrapper.message);
        Test.stopTest();
    }
    
    
    static testMethod void testGenerateNote(){ 
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'Qualification';
        insert opp;
        
        Contact con = new Contact();
        con.LastName = 'Test Contact';
        insert con;
        
        Test.StartTest();
        //Test when Application and Contact Id's are present
        String requestJson = '{"appId": "'+opp.Id+'", "contactId": "'+con.Id+'", "consent": "test", "fullName": "testFullName"}';
        String responseJson = SampleTestMER418.GenerateNoteMethod(requestJson);
        Map<string, string> responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        //System.assertEquals('200', responseMap.get('status'));
        //System.assertEquals('Note Inserted Successfully', responseMap.get('message'));
        
        //Test when Application and Contact Id both are not present
        requestJson = '{"appId": "", "contactId": "", "consent": "test", "fullName": "testFullName"}';
        responseJson = SampleTestMER418.GenerateNoteMethod(requestJson);
        responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        //System.assertEquals('Err', responseMap.get('status'));
        //System.assertEquals('Application Id and Contact Id are null', responseMap.get('message'));
        
        //Test when Application is blank and Contact Id not blank
        requestJson = '{"appId": "", "contactId": "'+con.Id+'", "consent": "test", "fullName": "testFullName"}';
        responseJson = SampleTestMER418.GenerateNoteMethod(requestJson);
        responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class); 
        //System.assertEquals('200', responseMap.get('status'));
        //System.assertEquals('Note Inserted Successfully', responseMap.get('message'));
        requestJson = '{"appId": "'+opp.Id+'", "contactId": "", "consent": "", "fullName": ""}';
        responseJson = SampleTestMER418.GenerateNoteMethod(requestJson);
        //responseMap = (Map<String, String>) JSON.deserialize(responseJson, Map<String, String>.class);
        Test.stopTest();
    }
    
    static testMethod void testReturnsingleOpp(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Child Account';
        objAccount.Type ='Merchant';
        objAccount.Active_Partner__c = True;
        objAccount.Cobranding_Name__c='Test Name';        
        insert objAccount;
        
        Contact con = New Contact();
        con.firstName = 'jony';
        con.lastName = 'Paul';
        con.createddate = Date.today();
        con.Lead_Sub_Source__c = 'Aamco';        
        insert con;
        Opportunity opp = new Opportunity();
        opp.Contact__c = con.id;
        opp.Name = 'Test OppName';
        opp.CloseDate = Date.today();
        opp.StageName = 'Qualification';
        opp.Partner_Account__c = objAccount.Id;
        opp.status__c = 'Funded';
        insert opp;
        
        IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
        Insert idology; 
        
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credited Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        
        Test.StartTest();
        //Test when Opp status and FilterBy__c both are same
        String requestJson = '{"opportunityId": "'+opp.id+'"}';
        String responseJson = SampleTestMER418.returnSingleRecord(requestJson);
        String responseJsonn = SampleTestMER418.returnSingleRecord('{"opportunityId": ""}');
        String requestJsonContact = '{"contactId": "'+con.id+'"}';
        String responseJsonContact = SampleTestMER418.returnSingleRecord(requestJsonContact);
        String responseJsonContactt = SampleTestMER418.returnSingleRecord('{"contactId": ""}');
        //System.assertEquals(true, requestJsonContact.contains('200'));
        //System.assertEquals(true, responseJsonContact.contains('Success'));        
        Test.stopTest();
    } 
    
    static testMethod void testReturnAamcoOpp(){ 
        Account objAccount = New Account(Name = 'Test Child Account',My_Apps_Only__c=false,Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', Cobranding_Name__c='Test Name');        
        insert objAccount;
        
        Account objAccount1 = New Account(Name = 'Test Child Account',My_Apps_Only__c=True,Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', Cobranding_Name__c='Test Name');        
        insert objAccount1;
        
        Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',Partner_Account__c = objAccount.Id,status__c = 'Credit Qualified');
        insert opp;
        Opportunity opp1 = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',Partner_Account__c = objAccount1.Id,status__c = 'Credit Qualified');
        insert opp1;
        
        Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
        Insert con;
        IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
        Insert idology;        
        
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credited Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        
        Aamco_Status__c cuSettings = new Aamco_Status__c();
        cuSettings.Name = 'funded';       
        cuSettings.FilterBy__c = 'Funded';
        insert cuSettings;
        
        Aamco_Status__c cuSettings1 = new Aamco_Status__c();
        cuSettings1.Name = 'expired';       
        cuSettings1.FilterBy__c = 'Expired';
        insert cuSettings1;
        
        Aamco_Status__c cuSettings2 = new Aamco_Status__c();
        cuSettings2.Name = 'fundedExternally';       
        cuSettings2.FilterBy__c = 'Funded Externally';
        insert cuSettings2;
        
        Aamco_Status__c cuSettings3 = new Aamco_Status__c();
        cuSettings3.Name = 'declined';       
        cuSettings3.FilterBy__c = 'Declined';
        insert cuSettings3;
        
        Aamco_Status__c cuSettings4 = new Aamco_Status__c();
        cuSettings4.Name = 'canceled';       
        cuSettings4.FilterBy__c = 'Canceled';
        insert cuSettings4;
        
        Aamco_Status__c cuSettings5 = new Aamco_Status__c();
        cuSettings5.Name = 'loanApproved';       
        cuSettings5.FilterBy__c = 'Loan Approved';
        insert cuSettings5;
        
        Aamco_Status__c cuSettings6 = new Aamco_Status__c();
        cuSettings6.Name = 'pendingApproval';       
        cuSettings6.FilterBy__c = 'Pending Approval';
        insert cuSettings6;
        
        Aamco_Status__c cuSettings7 = new Aamco_Status__c();
        cuSettings7.Name = 'readyToFund';       
        cuSettings7.FilterBy__c = 'Ready To Fund';
        insert cuSettings7;
        
        wrapperLocations wr = new wrapperLocations();
        wr.myApps = 'false';
        wr.accountId = objAccount1.Id;
        wr.filterBy = 'expired,canceled,funded,pendingApproval,loanApproved,readyToFund';
        wr.lowerLimit = '1';
        wr.upperLimit = '10';
        wr.searchBy = 'Test';
        wr.sortType = 'desc';
        wr.accountList = new List<String>();
        wr.accountList.add(objAccount1.Id);
        wr.subSourceList = new List<String>();
        wr.subSourceList.add(objAccount1.Partner_Sub_Source__c);
        
        Test.StartTest();
        SampleTestMER418.returnAamcoOpp(JSON.serialize(wr));
        //SampleTestMER418.returnAamcoOpp('{"accountId": ""}');
        //SampleTestMER418.returnAamcoOpp('{"accountId": "'+objAccount.Id+'"}');
        //SampleTestMER418.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"funded"}');
        // SampleTestMER418.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"declined","searchBy":"Test","sortBy":"daysuntilexpire","sortType":"desc"}');
       // SampleTestMER418.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","filterBy":"expired"}');
        //SampleTestMER418.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","lowerLimit":"1","upperLimit":"10","filterBy":"expired,canceled,funded,applicationSent,pendingApproval,loanApproved,readyToFund","searchBy":"Test","sortBy":"daysuntilexpire","sortType":"desc"}');
       // SampleTestMER418.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","lowerLimit":"1","upperLimit":"10","filterBy":"funded"}');
        //SampleTestMER418.returnAamcoOpp('{"accountId": "'+objAccount.Id+'","lowerLimit":"1","upperLimit":"10","filterBy":"applicationSent"}');
        
        //Test when Opp status and FilterBy__c are different
        /*cuSettings.Name = 'Credit Qualified';
cuSettings.FilterBy__c = 'Credit Qualified';
update cuSettings;      
*/
        
        Test.stopTest();
    }
    
    static testMethod void testgetCount(){        
        Aamco_Status__c cuSettings = new Aamco_Status__c();
        cuSettings.Name = 'funded';       
        cuSettings.FilterBy__c = 'Funded';
        insert cuSettings;
        
        Aamco_Status__c cuSettings1 = new Aamco_Status__c();
        cuSettings1.Name = 'expired';       
        cuSettings1.FilterBy__c = 'Expired';
        insert cuSettings1;
        
        Aamco_Status__c cuSettings2 = new Aamco_Status__c();
        cuSettings2.Name = 'fundedExternally';       
        cuSettings2.FilterBy__c = 'Funded Externally';
        insert cuSettings2;
        
        Aamco_Status__c cuSettings3 = new Aamco_Status__c();
        cuSettings3.Name = 'declined';       
        cuSettings3.FilterBy__c = 'Declined';
        insert cuSettings3;
        Aamco_Status__c cuSettings4 = new Aamco_Status__c();
        cuSettings4.Name = 'canceled';       
        cuSettings4.FilterBy__c = 'Canceled';
        insert cuSettings4;
        
        Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',My_Apps_Only__c=True,Active_Partner__c=True,Partner_Sub_Source__c='Aamco', Cobranding_Name__c='Test Name');        
        insert objAccount; 
        Account objAccount1 = New Account(Name = 'Test Child Account',Type='Merchant',My_Apps_Only__c=False,Active_Partner__c=True,Partner_Sub_Source__c='Aamco1', Cobranding_Name__c='Test Name1');        
        insert objAccount1; 
        
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Ready To Fund,Canceled,Funded,Credited Qualified,Pending Approval,Loan Approved,Expired,FundedExternally';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
        Insert con;
        //Map<String,Aamco_Status__c> custom_status = Aamco_Status__c.getall(); 
         
        
       
        
        IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
        Insert idology; 
        
        Test.startTest();       
        SampleTestMER418.getCount('{"accountId": "'+objAccount.Id+'"}');
        SampleTestMER418.getCount('{"accountId": "'+objAccount.Id+'","myApp":"True","searchBy":"test"}');
         SampleTestMER418.getCount('{"accountId": "'+objAccount1.Id+'","myApp":"false","searchBy":"test"}');
        SampleTestMER418.getCount('{"accountId": ""}');
        Test.stopTest();
    }
    
    static testMethod void AccountLocationsWrapperTest(){
        Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco',phone = '7852669444',Source__c='Aamco', Cobranding_Name__c='Test Name');        
        insert objAccount;  
        
        AccountLocationsWrapper.Locations loc = New AccountLocationsWrapper.Locations();
        loc.accountId = objAccount.Id;
        loc.accountName = objAccount.Name;
        loc.phone = '7852669444';
        loc.source = 'Aamco';
        loc.subProvider = 'Aamco';
        loc.subSource = 'Aamco';
        
        AccountLocationsWrapper l = new AccountLocationsWrapper();
        l.addLocation(objAccount);
    }
    
    static testMethod void statusReturnTest(){
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credited Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        
        Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', Cobranding_Name__c = 'Test Name');        
        insert objAccount;
        Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',Partner_Account__c = objAccount.Id,status__c = 'Credit Qualified');
        insert opp;
        Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
        Insert con;
        IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
        Insert idology;
        set<id> setidology = New set<id>();
        setidology.add(Con.id);
        SampleTestMER418.returnStatus(opp,setidology);
    }
    static testMethod void reassignLocation(){
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credit Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        Merchant_Portal_Welcome_Email_Mapping__c mpwm = new Merchant_Portal_Welcome_Email_Mapping__c();
        mpwm.Name = 'reassignmentTemplate';
        mpwm.Email_Template__c = 'test_email';
        insert mpwm;
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
        System.RunAs(usr) {
            test.startTest();
            EmailTemplate eTemplate=new EmailTemplate();
            eTemplate.Name='test_email';
            eTemplate.Body='Hello';
            eTemplate.Subject='Reassign Notification';
            eTemplate.HtmlValue='<html><body>Hello</body></html>';
            eTemplate.DeveloperName='test_email';
            eTemplate.FolderId=UserInfo.getUserId();
            eTemplate.TemplateType='Custom';
            insert eTemplate;
            Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', Cobranding_Name__c='Test Name', Threshold_Loan_Amount__c = 0);        
            insert objAccount;
            Account objAccount1 = New Account(Name = 'Test Child Account2', RMC_Email__c='test.re@gmail.com', Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco2', Cobranding_Name__c='Test Name');        
            insert objAccount1;
            
            Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
            Insert con;
            Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',Partner_Account__c = objAccount.Id,status__c = 'Credit Qualified', Contact__c = con.id, Amount = 10000, Fee__c = 10000);
            insert opp;
            IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
            Insert idology;
            set<id> setidology = New set<id>();
            setidology.add(Con.id);
            
            SampleTestMER418.reAssignLocation('{"assignedLocationId": "'+objAccount1.id+'", "opportunityId": "'+opp.Id+'", "contactId": "'+con.Id+'"}');
            test.stopTest();
        }
    }
    static testMethod void reassignLocationOnContactId(){
       
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credit Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        Merchant_Portal_Welcome_Email_Mapping__c mpwm = new Merchant_Portal_Welcome_Email_Mapping__c();
        mpwm.Name = 'reassignmentTemplate';
        mpwm.Email_Template__c = 'Reassignment_Template';
        mpwm.Sender_Email__c = 'test@gmail.com';
        insert mpwm;
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
        System.RunAs(usr) {
            test.startTest();
            EmailTemplate eTemplate=new EmailTemplate();
            eTemplate.Name='test_email';
            eTemplate.Body='Hello';
            eTemplate.Subject='Reassign Notification';
            eTemplate.HtmlValue='<html><body>Hello</body></html>';
            eTemplate.DeveloperName='Reassignment_Template';
            eTemplate.FolderId=UserInfo.getUserId();
            eTemplate.TemplateType='Custom';
            insert eTemplate;
            Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', Cobranding_Name__c='Test Name');        
            insert objAccount;
            Account objAccount1 = New Account(Name = 'Test Child Account2', RMC_Email__c='test.re@gmail.com', Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco2', Cobranding_Name__c='Test Name');        
            insert objAccount1;
            
            Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
            Insert con;
            Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(), StageName = 'Qualification', Partner_Account__c = objAccount.Id, status__c = 'Credit Qualified', Contact__c = con.id);
            insert opp;
            IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
            Insert idology;
            set<id> setidology = New set<id>();
            setidology.add(Con.id);
            
            SampleTestMER418.reAssignLocation('{"opportunityId": "", "assignedLocationId": "'+objAccount1.id+'", "contactId": "'+con.Id+'"}');
            
            test.stopTest();
        }        
        
    }
    
    static testmethod void testgetAccountsForReassignment(){
        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        
        amcoSetting.SearchBy_Min_Limit__c = '0';
        amcoSetting.SearchBy_Max_Limit__c = '10';
        insert amcoSetting; 
        
        Account acobj = New Account(Name = 'grandParentAccount',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', Cobranding_Name__c='Test Name');        
        insert acobj;
        Account acobj1 = New Account(parentid = acobj.id, Name = 'parent Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco1', Cobranding_Name__c='Test Name');        
        insert acobj1;
        Account acobj2 = New Account(parentid = acobj1.id , Name = 'Child Account',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco2', Cobranding_Name__c='Test Name');        
        insert acobj2;  
        
        Test.StartTest();
        SampleTestMER418.getAccountsForReassignment('{"accountId":" "}');
        SampleTestMER418.getAccountsForReassignment('{"accountId": "'+acobj.Id+'"}');
        SampleTestMER418.getAccountsForReassignment('{"accountId": "'+acobj.Id+'","lowerLimit":"2001","upperLimit":"2010"}');
        SampleTestMER418.getAccountsForReassignment('{"accountId": "'+acobj2.Id+'"}');
        SampleTestMER418.getAccountsForReassignment('{"accountId": "'+acobj.Id+'","lowerLimit":"5","upperLimit":"10"}');
        SampleTestMER418.getAccountsForReassignment('{"accountId": "'+acobj.Id+'","lowerLimit":"5","upperLimit":"10","searchBy":"aamco"}');
        Test.stopTest();    
        
    }
    
    static testmethod void testgetPermissionsByAccountid(){
        SampleTestMER418.getPermissionsByAccountid('{"accountId":""}');     
        Account acc = new Account(Name = 'testAccount',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', 
                                     Cobranding_Name__c='Test Name',Start_Application__c = True,
                                     Payment_Calculator__c = True,
                                     Reporting__c = True,
                                     Marketing_Store_Merchant_Center__c = True
                                     ,DrillDown__c=True,Enable_Assignment__c = True);
        insert acc;
        Account acc1 = new Account(Name = 'testAccount',Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', 
                                     Cobranding_Name__c='Test Name',Start_Application__c = false,
                                     Payment_Calculator__c = false,
                                     Reporting__c = false,
                                     Marketing_Store_Merchant_Center__c = false
                                     ,DrillDown__c=false,Enable_Assignment__c = false);
        insert acc1;
         Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(),StageName = 'Qualification',status__c = 'Credit Qualified');
        insert opp;
        Test.StartTest();
        SampleTestMER418.getPermissionsByAccountid('{"accountId":"'+opp.id+'"}');
        SampleTestMER418.getPermissionsByAccountid('{"accountId":"'+acc.id+'"}');
        SampleTestMER418.getPermissionsByAccountid('{"accountId":"'+acc1.id+'"}');
               
        Test.StopTest();
    } 
   
  
   /* static testMethod void sendMail(){

        Merchant_Portal_Settings__c amcoSetting = Merchant_Portal_Settings__c.getInstance();
        amcoSetting.preQualifyStatus__c = 'Funded,Credit Qualified';
        amcoSetting.OppExpiryDays__c ='31';
        amcoSetting.minLimit__c = '0';
        amcoSetting.maxLimit__c = '10';
        insert amcoSetting;
        list<string> pre = amcoSetting.preQualifyStatus__c.split(',');
        Merchant_Portal_Welcome_Email_Mapping__c mpwm = new Merchant_Portal_Welcome_Email_Mapping__c();
        mpwm.Name = 'reassignmentTemplate';
        mpwm.Email_Template__c = 'Send_Reassignment_Notification_To_Merchant';
        mpwm.Sender_Email__c = 'test@gmail.com';
        insert mpwm;
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
        System.RunAs(usr) {
            test.startTest();
            EmailTemplate eTemplate=new EmailTemplate();
            eTemplate.Name='test_email';
            eTemplate.Body='Hello';
            eTemplate.Subject='Reassign Notification';
            eTemplate.HtmlValue='<html><body>Hello</body></html>';
            eTemplate.DeveloperName='Send_Reassignment_Notification_To_Merchant';
            eTemplate.FolderId=UserInfo.getUserId();
            eTemplate.TemplateType='Custom';
            insert eTemplate;
            Account objAccount = New Account(Name = 'Test Child Account',Type='Merchant',RMC_Email__c='test.re@gmail.com',Active_Partner__c=True,Partner_Sub_Source__c='Aamco', Cobranding_Name__c='Test Name');        
            insert objAccount;
            Account objAccount1 = New Account(Name = 'Test Child Account2', RMC_Email__c='test.re@gmail.com', Type='Merchant',Active_Partner__c=True,Partner_Sub_Source__c='Aamco2', Cobranding_Name__c='Test Name');        
            insert objAccount1;
            
            Contact con = New Contact(lastName = 'Sam',Lead_Sub_Source__c = 'Aamco');       
            Insert con;
            Opportunity opp = New Opportunity(Name = 'Test Opp',CloseDate = Date.today(), StageName = 'Qualification', Partner_Account__c = objAccount.Id, status__c = 'Credit Qualified', Contact__c = con.id);
            insert opp;
            IDology_Request__c idology = New IDology_Request__c(Contact__c = con.id,CreatedDate=Date.today(),Verification_Result__c='Pass');
            Insert idology;
            set<id> setidology = New set<id>();
            setidology.add(Con.id);
            
            SampleTestMER418.reAssignLocation('{"opportunityId": "' + opp.id + '", "assignedLocationId": "'+objAccount1.id+'", "contactId": "'+con.Id+'"}');
            
            test.stopTest();
        }
    } */
    
     public class wrapperLocations {
        public string myApps;
        public string accountId;
        public string filterBy;
        public string lowerLimit;
        public string upperLimit;
        public string searchBy;
        public string startDate;
        public string endDate;
        public string sortType;
        public string sortBy;
        public List<String> accountList;
        public List<String> subSourceList;
    }
    

}