public class CLCustomValuesHistoryCtrl {
  public loan__loan_Account__c acc {get; set;}
	public String dateTimeFormat {get; set;}
	public Boolean isProfileAllowed {get; set;}
	public Boolean hasRecords {get; set;}

	public CLCustomValuesHistoryCtrl(Apexpages.StandardController controller) {
		acc = (loan__loan_Account__c)controller.getRecord();

		isProfileAllowed = (label.Asset_History.contains(UserInfo.getProfileId()))?true:false;
		if (getUserHistory().size() > 0) 
			this.hasRecords = true;
	}

	public List<assetEntity> getAssetHistory(){
		List<assetEntity> listAssetsHistory = new List<assetEntity>(); 
		for (Asset_Sale_History__c assetHistory : [SELECT Id, Name, CL_Contract__c, Field_Name__c, Prior_Value__c, New_Value__c, Modified_by_User__r.Name, createdDate 
												   FROM Asset_Sale_History__c
		    									   WHERE CL_Contract__c = :acc.Id]){
			assetEntity t = new assetEntity();

			t.fieldName=assetHistory.Field_Name__c;
			t.priorValue=assetHistory.Prior_Value__c;
      t.newValue=assetHistory.New_Value__c;
      t.userName=assetHistory.Modified_by_User__r.Name;
      t.date_Time=assetHistory.createdDate.format('MM/dd/yyyy hh:mm:ss');

			listAssetsHistory.add(t);
		}
		return listAssetsHistory;
	}

	public List<userEntity> getUserHistory(){
		List<userEntity> listUsersHistory = new List<userEntity>(); 
		for (Securitization_User_Approve__c userHistory : [SELECT Id, userId__r.Name, createdDate 
			                  							   FROM Securitization_User_Approve__c 
															WHERE accountId__c = :acc.Id]){
			userEntity t = new userEntity();

      t.userName=userHistory.userId__r.Name;
      t.date_Time=userHistory.createdDate.format('MM/dd/yyyy hh:mm:ss');

			listUsersHistory.add(t);
		}
		return listUsersHistory;
	}

	public class assetEntity{
    public String fieldName {get;set;} 
    public String priorValue {get;set;} 
    public String newValue{get;set;}
    public String userName{get;set;}
    public String date_Time {get;set;}
	}

  public class userEntity{
    public String userName{get;set;}
    public String date_Time {get;set;}
  } 

}