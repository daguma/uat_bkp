@RestResource(urlMapping='/AutoPayPaymentRestResource/*')
global with sharing class AutoPayPaymentRestResource {
    
    public static Integer BAD_REQUEST = 400;
    public static Integer NO_ACTION = 202;
    public static Integer ERROR = 501;
    public static Integer SUCCESS = 200;
    
    public AutoPayPaymentRestResource(){}
    
    @HttpPost
    global static void switchPaymentMethod(String inputData) {
        RestResponse response = RestContext.response;
        Map<String, Object> res = new Map<String, Object>();  
        Boolean updateContract = false;
        List<loan__Bank_Account__c> bankAccRecord = new List<loan__Bank_Account__c>();
        try{
            RequestParameters requestData = (RequestParameters) JSON.deserialize(EncodingUtil.base64Decode(inputData).toString(), RequestParameters.class); 
            
            res.put('success',false);
            if(String.isNotEmpty(requestData.contractId) && String.isNotEmpty(requestData.contactId)) {
                List<loan__Loan_Account__c> clContract = [  SELECT  Id, 
                                                          loan__Contact__r.Name,Card_Next_Debit_Date__c,
                                                          Name,Is_Debit_Card_On__c,loan__ACH_Next_Debit_Date__c,
                                                          loan__Contact__c,loan__ACH_On__c,loan__ACH_Account_Number__c,
                                                          loan__ACH_Routing_Number__c,loan__ACH_Bank_Name__c,
                                                          Payment_Frequency_Masked__c,loan__Next_Installment_Date__c,
                                                          loan__ACH_Account_Type__c,loan__Borrower_ACH__c,PaymentMethod__c,Debit_Card_Account_Id__c 
                                                          FROM    loan__Loan_Account__c 
                                                          WHERE   Id =:requestData.contractId and loan__Contact__c =: requestData.contactId];
                
                if(!clContract.isEmpty()) {   
                    loan__Loan_Account__c contractToBeUpdated = (!clContract.isEmpty()) ? clContract[0] : null;
                    
                    if(String.isnotEmpty(requestData.bankId)) { 
                        for(loan__Bank_Account__c bankAccs :[SELECT   loan__Bank_Account_Number__c,Id,Name,Is_Default__c,From_CP__c,loan__Account_Type__c,loan__Routing_Number__c,loan__Bank_Name__c FROM    loan__Bank_Account__c WHERE loan__Contact__c =: requestData.contactId and Id =: requestData.bankId]) {
                            bankAccRecord.add(bankAccs);
                        }
                    }
                    
                    if(String.isNotEmpty(requestData.paymentType)){
                        if(requestData.paymentType == 'ACH') {  
                            if(bankAccRecord.size() > 0) {
                                contractToBeUpdated = assigningPaymentMethodValues(bankAccRecord[0].loan__Bank_Account_Number__c,bankAccRecord[0].loan__Routing_Number__c,bankAccRecord[0].loan__Bank_Name__c ,bankAccRecord[0].loan__Account_Type__c,bankAccRecord[0].Id,false,null,(contractToBeUpdated.Card_Next_Debit_Date__c <> null ? contractToBeUpdated.Card_Next_Debit_Date__c : contractToBeUpdated.loan__Next_Installment_Date__c),true,contractToBeUpdated,requestData.paymentType,null);
                                updateContract = true;
                            } else { res.put('message','Bank Details Not Provided.');  response.statusCode = BAD_REQUEST; }
                        } else if(requestData.paymentType == 'Debit Card') {
                            if(String.isNotempty(requestData.cardNo) && String.isNotempty(requestData.accountId)) {
                                contractToBeUpdated = assigningPaymentMethodValues(null,null,null,null,null,true,(contractToBeUpdated.loan__ACH_Next_Debit_Date__c <> null ? contractToBeUpdated.loan__ACH_Next_Debit_Date__c : contractToBeUpdated.loan__Next_Installment_Date__c),null,false,contractToBeUpdated,requestData.paymentType,requestData.accountId);
                                updateContract = true;
                            } else { res.put('message','Debit Card Details Not Provided.'); response.statusCode = BAD_REQUEST; }
                        } else { res.put('message','Payment Type not supported.'); response.statusCode = BAD_REQUEST; }
                        
                        if(updateContract) {
                            update contractToBeUpdated; 
                            res.put('success', true); res.put('message','Payment Method switched successfully');
                            response.statusCode = SUCCESS;
                        }
                    } else{
                        res.put('message','Payment Type cannot be empty.'); 
                        response.statusCode = BAD_REQUEST;
                    } 
                } else {
                    res.put('message','Details not found with given Contract Id.');
                    response.statusCode = NO_ACTION;
                }
            } else{
                res.put('message','Required Parameters are missing.');
                response.statusCode = BAD_REQUEST;   
            }
        } catch(Exception e) {
            res.put('message',e.getMessage() + ' at Line No: ' + e.getLineNumber()); 
            response.statusCode = ERROR; 
        }       
        
        response.responseBody = blob.valueof(JSON.serializePretty(res, true));
    }
    
    @HttpGet
    global static void getAutoPayAccountDetails() {
        Map<String, Object> responseMap = new Map<String, Object>();    
        RestRequest req = RestContext.request;
        RestResponse response = RestContext.response;
        String contractId = req.params.get('contractId');
        String contactId = req.params.get('contactId');
        responseMap.put('success',false);
        response.statusCode = NO_ACTION;
        responseMap.put('message','Details not found Or, the customer belongs to Non-Auto Pay');
        try {
            if(string.isnotempty(contactId) && string.isnotempty(contractId)) {
                for(loan__loan_Account__c contract : [select Id,loan__ACH_On__c,Is_Debit_Card_On__c,loan__Borrower_ACH__c ,loan__ACH_Account_Number__c,loan__ACH_Routing_Number__c,Debit_Card_Account_Id__c from loan__loan_Account__c where Id =: contractId and loan__Contact__c =: contactId limit 1]) {
                    if(contract.loan__ACH_On__c) {
                        for(loan__Bank_Account__c bankAccount : [SELECT loan__Bank_Account_Number__c,
                                                                 Id,Name,Is_Default__c,From_CP__c,loan__Account_Type__c, 
                                                                 loan__Routing_Number__c, 
                                                                 loan__Bank_Name__c FROM loan__Bank_Account__c 
                                                                 WHERE loan__Contact__c =: contactId]) {
                             if(contract.loan__Borrower_ACH__c == bankAccount.Id && string.isnotempty(bankAccount.loan__Bank_Account_Number__c) && string.isnotempty(bankAccount.loan__Routing_Number__c)) {
                                 responseMap.put('success',true);
                                 responseMap.put('message','Record Found Successfully!!');
                                 responseMap.put('bankId', bankAccount.id);
                                 responseMap.put('bankName', bankAccount.loan__Bank_Name__c);
                                 responseMap.put('fundingAccount', !bankAccount.From_CP__c);
                                 responseMap.put('default', bankAccount.Is_Default__c);
                                 responseMap.put('accountingNumber', bankAccount.loan__Bank_Account_Number__c);
                                 responseMap.put('routingNumber', bankAccount.loan__Routing_Number__c);
                                 responseMap.put('accountType', bankAccount.loan__Account_Type__c);
                                 response.statusCode = SUCCESS; 
                             }
                         }
                     } 
                     
                     if(contract.Is_Debit_Card_On__c && string.isnotempty(contract.Debit_Card_Account_Id__c)) {
                         responseMap.put('success',true);
                         responseMap.put('message','Record Found Successfully!!');
                         responseMap.put('accountId', contract.Debit_Card_Account_Id__c );
                         response.statusCode = SUCCESS; 
                     }
                }
            } else {
                responseMap.put('message','Required Parameters are missing.');
                response.statusCode = BAD_REQUEST; 
            }
        } catch (exception e) {
            responseMap.put('message', e.getMessage() + ' at Line No: ' + e.getLineNumber());  
            response.statusCode = ERROR; 
        }
        
        response.responseBody = blob.valueof(JSON.serializePretty(responseMap, true));
    }
    
    public static loan__Loan_Account__c assigningPaymentMethodValues(String accNumber,String routingNumber,String bankName,String accountType,String bankAccountId,Boolean DebitCardOn,Date CardNextDebitDate,Date ACHnextDebitDate,Boolean ACHOn,loan__Loan_Account__c contractToBeUpdated,String paymentMethod,String accountId) {
        contractToBeUpdated.loan__ACH_On__c = ACHOn;
        contractToBeUpdated.loan__ACH_Next_Debit_Date__c = ACHnextDebitDate;
        contractToBeUpdated.loan__ACH_Start_Date__c = ACHnextDebitDate;
        contractToBeUpdated.loan__Ach_Debit_Day__c = (ACHnextDebitDate <> null) ? ACHnextDebitDate.day() : null;
        contractToBeUpdated.Card_Next_Debit_Date__c = CardNextDebitDate;
        contractToBeUpdated.Is_Debit_Card_On__c = DebitCardOn;
        contractToBeUpdated.loan__ACH_Account_Number__c = accNumber;
        contractToBeUpdated.loan__ACH_Routing_Number__c = routingNumber;
        contractToBeUpdated.loan__ACH_Bank_Name__c = bankName;
        contractToBeUpdated.loan__ACH_Account_Type__c = accountType;
        contractToBeUpdated.loan__Borrower_ACH__c = bankAccountId;
        contractToBeUpdated.PaymentMethod__c = paymentMethod;
        contractToBeUpdated.Debit_Card_Account_Id__c = accountId;
        return contractToBeUpdated;
    }
    
    public class RequestParameters {
        public string contactId;
        public string contractId;             
        public string paymentType;
        public string cardNo;
        public string accountId;     
        public string bankId;
    }
   
}