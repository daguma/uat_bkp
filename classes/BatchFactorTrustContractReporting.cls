global class BatchFactorTrustContractReporting implements Database.Batchable<sObject>, Schedulable,Database.AllowsCallouts{
    
    public void execute(SchedulableContext sc) { 
       BatchFactorTrustContractReporting batchapex = new BatchFactorTrustContractReporting();
       id batchprocessid = Database.executebatch(batchapex,100);
       system.debug('Process ID: ' + batchprocessid);
    }
   
    global Database.QueryLocator start(Database.BatchableContext BC){  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
         String query = 'SELECT CreatedDate, loan__Contact__r.Phone_Clean__c, Opportunity__c, loan__Loan_Status__c,Id,FT_Past_Due_Reporting__c,FT_Write_Off_Reporting__c,Name,loan__Pmt_Amt_Cur__c,FT_Reporting__c, loan__Due_Day__c,loan__Next_Installment_Date__c,loan__Contact__r.BirthDate,loan__Contact__r.SSN__c,loan__Loan_Effective_Date__c,loan__Principal_Remaining__c,loan__ACH_Account_Number__c,loan__Contact__r.FirstName,loan__Contact__r.LastName,loan__Contact__r.MailingCity,loan__Contact__r.MailingStreet,loan__Contact__r.MailingState,loan__Contact__r.MailingPostalCode,loan__Contact__r.MailingCountry  FROM loan__loan_Account__c where (CreatedDate = TODAY AND FT_Reporting__c = false) OR  (loan__Loan_Status__c =\'Closed- Written Off\' AND FT_Write_Off_Reporting__c = false)';           
         return Database.getQueryLocator(query);
    }
   
    
    global void execute(Database.BatchableContext BC, List<loan__loan_Account__c> scope){
               date DOB;
               string fName, lName,street, city, state, Zip, country, phone, LAccId;               
               list<Logging__c> logList = new list<Logging__c>();
        try{                  
             for(loan__loan_Account__c a : scope){
                   Contact c = a.loan__Contact__r;
                   LAccId = a.id; 
                   fName  = c.FirstName;
                   lName  = c.LastName;
                   street = c.MailingStreet;
                   city   = c.MailingCity;
                   state  = c.MailingState;
                   Zip    = c.MailingPostalCode;
                   country= c.MailingCountry;
                   DOB    = c.BirthDate;
                   phone  = c.Phone_Clean__c;
                   
                  if(a.CreatedDate.Date() == system.today() &&  !a.FT_Reporting__c){ 
                        logList.add(FTReportingUtil.FactorTrustReporting('LA',LAccId,'Contract Creation Report','NL',c.SSN__c,String.ValueOf(a.loan__Loan_Effective_Date__c),String.ValueOf(a.loan__Next_Installment_Date__c),String.ValueOf(a.loan__Pmt_Amt_Cur__c),String.ValueOf(a.loan__Principal_Remaining__c),a.loan__ACH_Account_Number__c,'',a.Id,a.Opportunity__c,fName,lname,DOB,city,street,state,Zip,country,phone, null));                      
                        a.FT_Reporting__c = true;  
                   }                                                   
                 
                   if(a.loan__Loan_Status__c == 'Closed- Written Off' && !a.FT_Write_Off_Reporting__c){
                       logList.add(FTReportingUtil.FactorTrustReporting('LA',LAccId,'Contract WrittenOff Report','VO',c.SSN__c,String.ValueOf(a.loan__Loan_Effective_Date__c),String.ValueOf(a.loan__Next_Installment_Date__c),String.ValueOf(a.loan__Pmt_Amt_Cur__c),String.ValueOf(a.loan__Principal_Remaining__c),a.loan__ACH_Account_Number__c,'',a.Id,a.Opportunity__c,fName,lname,DOB,city,street,state,Zip,country,phone, null));                     
                       a.FT_Write_Off_Reporting__c = true;
                   }          
                          
              }
                    
             update scope;
             insert logList;
         }catch(Exception e){
            insert logList;                      
            WebToSFDC.notifyDev('Error while submitting factor Trust report for Contract - '+LAccId, e.getMessage() + ' line ' + (e.getLineNumber()));        
         }         
    }   
    
    
    global void finish(Database.BatchableContext BC) {            
        WebToSFDC.notifyDev('Factor Trust Contract Batch Completed', 'Factor Trust CL Contract Reporting completed for Today.');        
    }
}