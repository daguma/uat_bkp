public virtual class DealAutomaticAssignmentManager {

  public static String getQueryOppInCQ() {
    String query =  ' SELECT Id, Contact__c, Contact__r.Name, Contact__r.Phone, Contact__r.OwnerId, OwnerId, Lead__c, Automatically_Assigned__c, Automatically_Assigned_Date__c, AssignmentDate__c, ' +
                    '        Contact__r.Employment_Type__c, Lead_Sub_Source__c, Scorecard_Grade__c, Selected_Offer_Id__c, Status__c, name, ' +
                    '        Lead__r.Use_of_Funds__c, Contact__r.Use_of_Funds__c, LeadSource__c, Point_Code__c, State__c, CreatedDate, Last_Page_Submitted__c, Type, ' +
                    '        Contact__r.Loan_Amount__c, FICO__c, StrategyType__c, Decile_by_SalesCategory__c, ProductName__c  ' +
                    ' FROM Opportunity ' +
                    ' WHERE Status__c IN (\'Credit Qualified\')' +
                    (Test.isRunningTest() ? '' : '   AND CreatedDate >= ' + DateTime.newInstance(Date.Today().year(), Date.Today().month(), Date.Today().day()).addDays(-30).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'GMT')) +
                    '   AND Last_CQ_Date__c < ' + DateTime.newInstance(Date.Today().year(), Date.Today().month(), Date.Today().day()).addDays(-10).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', 'GMT') +
                    '   AND Status__c != NULL ' +
                    '   AND OwnerId = \'' + Label.Default_Lead_Owner_ID  + '\' ' +
                    ' ORDER BY CreatedDate';
    //system.debug(query);
    return query;
  }
    
  public static String getQueryOppInOAandIP() {
    String query =  'SELECT Id, Name, OwnerId, Status__c, Total_Calls_Made__c' +
                    ' FROM Opportunity' +
                    ' WHERE Status__c != NULL' +
                    ' AND Status__c IN (\'Offer Accepted\', \'Incomplete Package\')' + 
                    ' AND Total_Calls_Made__c > 2' + 
                    ' AND LeadSource NOT IN (Null, \'Merchant\', \'Invisalign\')' +
                    ' AND (Lead_Sub_Source__c != null' +
                    ' AND (Lead_Sub_Source__c != \'UMC_NoStips\'' +
                    ' OR (Lead_Sub_Source__c = \'UMC_NoStips\' AND Total_Amount__c > 10000.00) ) )' + 
                    ' AND ProductName__c != \'Point Of Need\'' +
                    ' AND Type = \'New\'' +
                    ' AND OwnerId = \'' + Label.HAL_Id  + '\'' +
                    ' AND Name like \'APP%\'' +                   
                    ' LIMIT 40';
    //system.debug(query);
    return query;
  }   

  public static String getQueryOppInRQ() {
    String query =  ' SELECT Id, OwnerId, Status__c' +
                    ' FROM Opportunity ' +
                    ' WHERE Status__c != NULL' +
                    ' AND Status__c = \'Refinance Qualified\''+ 
                    ' AND OwnerId = \'' + Label.HAL_Id  + '\' ' +
                    ' AND Name like \'APP%\'';
    //system.debug(query);
    return query;
  } 

  public static void assignUserProcess(Opportunity application, DealAutomaticAssignmentManager.GlobalParameters globalParams) {

    DealAutomaticUserGroup ownerUser = AutomaticAssignmentUtils.getNextUser(globalParams.userAppTotalsMap, globalParams.getUsersListByCriteria(application));
    if (ownerUser != null && ownerUser.userId != null) {
      System.debug(application.Name + ' ' + application.OwnerId);
      application.OwnerId = ownerUser.userId;
      application.Automatically_Assigned__c = true;
      application.Automatically_Assigned_Date__c = Datetime.now();
      application.AssignmentDate__c = Datetime.now();

      if (application.Contact__r != null) {
        application.Contact__r.OwnerId = ownerUser.userId;
      }

      if (!globalParams.isDebugMode) {
        update application;

        insert appNewNote(application.id, application.Contact__r.Name, application.name, application.Contact__r.Phone);
      } else {
        System.debug('Next User: ' + ownerUser);
      }
      globalParams.updateUserAppTotalsMap(ownerUser.userId);

    } else
      System.debug('No user found to assign too');
  }


  public void assignExistingLeadFromWeb(Lead lead) {}
  @future
  public static void assignExistingAppFromWeb(String appId) {}
  public static void assignExistingAppFromWeb(String appId, Boolean isDebugMode) {}


  //Method for inserting the notes
  @TestVisible
  private static Note appNewNote(id parent, String Name, String AppName, String Phone) {
    Note NoteRow = new note();
    NoteRow.parentid = parent;
    NoteRow.title = 'New Deal Assigned To You - ' + Name;
    NoteRow.body = 'The deal for ' + Name + ' - ' + AppName + ' was just assigned to you at ' + System.Now().format()
                   + '\nPhone contact Information: ' + Phone
                   + '\n\nApplication URL: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/apex/newOffer?id=' + parent
                   + '\n\n\nThis email has been auto-generated.';
    return NoteRow;
  }

  public virtual class GlobalParameters {
    public Boolean isDebugMode {get; set;}
    public Map<Id, Integer> userAppTotalsMap {get; set;}
    public List<DealAutomaticAssignmentManager.UsersByChannel> channelsList  {get; set;}

    public GlobalParameters(Boolean isDebugMode) {
      this.isDebugMode = isDebugMode;
      //SalesForceUtil.SetOfflineUserByGroupId(Label.Kennesaw_Group_Id);
      channelsList = new List<DealAutomaticAssignmentManager.UsersByChannel>();
      for ( DAA_Logic__c c : [SELECT Id, Name, propName__c, propValues__c, groupId__c, priority__c FROM DAA_Logic__c ORDER BY priority__c]) {
        channelsList.add(new DealAutomaticAssignmentManager.UsersByChannel(c.Name, c.propName__c, c.propValues__c, c.groupId__c));
      }
      userAppTotalsMap = DealAutomaticAssignmentUtils.getTotalAssignedAppsForUserGroupList(getAllUsersList());
    }

    public void updateUserAppTotalsMap(Id userId) {
      Integer total = userAppTotalsMap.get(userId);
      userAppTotalsMap.put(userId, total == null ? 1  : ++total);
      if (isDebugMode) System.debug('UserId: ' + userId + ', total: ' + total);
    }

    public List<DealAutomaticUserGroup> getUsersListByCriteria(Opportunity opp) {
      for (DealAutomaticAssignmentManager.UsersByChannel uc : channelsList) {
        if (uc.propValues.containsIgnoreCase(String.valueOf(opp.get(uc.propName))) || uc.propValues == '*') {
          return uc.users;
        }
      }
      return new List<DealAutomaticUserGroup>();
    }

    private List<DealAutomaticUserGroup> getAllUsersList() {
      List<DealAutomaticUserGroup> result = new List<DealAutomaticUserGroup>();
      for (DealAutomaticAssignmentManager.UsersByChannel uc : channelsList) {
        for ( DealAutomaticUserGroup u : uc.users) {
          result.add(u);
        }
      }
      return result;
    }
  }//class

  public virtual class UsersByChannel {
    public List<DealAutomaticUserGroup> users {get; set;}
    public String channel {get; set;}
    public String propName {get; set;}
    public String propValues {get; set;}

    public UsersByChannel(String channel, String propName, String propValues, String groupId) {
      this.users = DealAutomaticAssignmentUtils.getUsersByGroupId(groupId);
      this.channel = channel;
      this.propName = propName;
      this.propValues = propValues;
    }
  }
    
  public static void assignUserByAvailableState(Opportunity opp, List<String> userList, Integer counter){
    String userId = DealAutomaticAssignmentUtils.getNextUserIdInList(userList, counter);
    if(String.isNotEmpty(userId)){  
      opp.OwnerId = userId;
      update opp;
      insert new Sales_Reps_Assignments_Log__c(OppId__c = opp.Id, 
                                               Opp_Status__c = opp.Status__c,
                                               UserId__c = userId);
      String oppURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id;
      SalesForceUtil.EmailByUserId('New Deal Assigned To You - ' + opp.Name,
                                   'The deal for ' + opp.Name + ' was just assigned to you at ' + System.Now().format() +
                                   '.<BR><BR>Application URL: <a href="' + oppURL +'">'+oppURL+ '</a>.' ,
                                   userId);
    }
  }
    
  public virtual class AssignmentParams {
    public list<String> userIds {get; set;}
    public Sales_Reps_Assignments__c salesCustom {get; set;}
	  public Integer counterTMP = 0;
      
    public AssignmentParams(String groupType){  
      this.salesCustom = Sales_Reps_Assignments__c.getOrgDefaults();
      if (this.salesCustom != null){
          if(test.isRunningTest()){  
          	this.userIds = LibraryTest.fakeIncontactResult();
        	this.counterTMP = 2;
          }
          else if (String.isNotEmpty(groupType)){
            this.userIds = (groupType == 'OA_IP_No_Assigned') ? 
                           DealAutomaticAssignmentUtils.getUsersListByState(this.salesCustom.Sales_Assignments_GroupId__c) : 
                           DealAutomaticAssignmentUtils.getUsersListBySchedule(this.salesCustom.Sales_Refinance_Assignments_GroupId__c);
            this.counterTMP =  Integer.valueOf(this.salesCustom.Sales_Assignments_Counter__c); 
          }         
      }  
    }
      
    public void incrementCounter(Integer counter){
      this.salesCustom.Sales_Assignments_Counter__c = counter;
      update this.salesCustom;
    }  
  }  
}