@isTest
public Class TestApplicationToContractConverter{
    
    static testMethod void testLoanConversion1(){
        
        TestHelper th = new TestHelper();
        th.createLendingProductName('Term Loan');
        loan__Office_Name__c offc = [Select Id,name from loan__Office_Name__C limit 1];
        Testhelper.createLPCustom('Term Loan',offc.Name);
        
        genesis__Applications__c app = createLoanApplication();
        loan__Loan_Account__c contract = new loan__Loan_Account__c();
        contract.loan__Loan_Amount__c = app.genesis__Loan_Amount__c;
        contract.loan__Disbursal_Date__c = app.genesis__Expected_Start_Date__c;
        contract.loan__First_Installment_Date__c = app.genesis__Expected_First_Payment_Date__c;
        //contract.loan__Days_Convention__c = app.genesis__Days_Convention__c;
        contract.loan__Product_Type__c = app.genesis__Product_Type__c;
        
        Map<SObject,Sobject> objMap = new Map<SObject,SObject>();
        objMap.put(app,contract);
        ApplicationToContractConverter converter = new ApplicationToContractConverter();
        converter.setContracts(objMap);
        String message = converter.processContract();
        system.Debug(logginglevel.error,'Message ' + message);
        app.Fico__C = '600';
        app.DTI__c = '0.40';
        update app;
        CreateLoanAccountForLoanTypeHandler loanHandler = new CreateLoanAccountForLoanTypeHandler(converter.application,converter.loanAccount,ApplicationToContractConverter.getLoanProductDetails('Term Loan'));
        String retMsg = loanHandler.createLoanAccount();
        Boolean check = retMsg.contains('Application converted to Loan');
        System.debug(retMsg);
        system.assertEquals(check,true);
    }
    static testMethod void testLoanConversion2(){
        
        TestHelper th = new TestHelper();
        th.createLendingProductName('Term Loan');
        loan__Office_Name__c offc = [Select Id,name from loan__Office_Name__C limit 1];
        Testhelper.createLPCustom('Term Loan',offc.Name);

        genesis__Applications__c app = createLoanApplication2();
        app.genesis__Expected_Start_Date__c = Date.today();
        update app;
        loan__Loan_Account__c contract = new loan__Loan_Account__c();
        contract.loan__Loan_Amount__c = app.genesis__Loan_Amount__c;
        contract.loan__Disbursal_Date__c = app.genesis__Expected_Start_Date__c;
        contract.loan__First_Installment_Date__c = app.genesis__Expected_First_Payment_Date__c;
        //contract.loan__Days_Convention__c = app.genesis__Days_Convention__c;
        contract.loan__Product_Type__c = app.genesis__Product_Type__c;
        app.Fico__C = '600';
        app.DTI__c = '0.40';
        update app;

        Map<SObject,Sobject> objMap = new Map<SObject,SObject>();
        objMap.put(app,contract);
        ApplicationToContractConverter converter = new ApplicationToContractConverter();
        converter.setContracts(objMap);
        String message = converter.processContract();
        system.Debug(logginglevel.error,'Message ' + message);
        CreateLoanAccountForLoanTypeHandler loanHandler = new CreateLoanAccountForLoanTypeHandler(converter.application,converter.loanAccount,ApplicationToContractConverter.getLoanProductDetails('Term Loan'));
        String retMsg = loanHandler.createLoanAccount();
        Boolean check = retMsg.contains('Application converted to Loan');
        system.debug('retmsg = ' +  retMsg);
        system.assertEquals(check,true);
    }
    
    static genesis__Applications__c createLoanApplication(){
        genesis__Applications__c app = new genesis__Applications__c();
        
        //app.Name = 'Test Application';
        List<RecordType> rtList = [Select Id from RecordType
                                    where SObjectType = 'genesis__Applications__c' and DeveloperName =: 'Loan'];
        app.RecordTypeId = rtList[0].Id;
        app.genesis__Contact__c = Testhelper.createContact().Id;
        app.genesis__Product_Type__c = 'LOAN';           
        app.genesis__Term__c = 10;
        app.genesis__Loan_Amount__c = 1000;
        app.genesis__Interest_Rate__c = 20;
        app.genesis__Days_Convention__c = '365/365';
        app.genesis__Expected_Start_Date__c = System.today();
        app.genesis__Expected_Close_Date__c = System.Today().addMonths(10);
        app.genesis__Expected_First_Payment_Date__c = system.today().addMonths(1);
        app.genesis__Interest_Calculation_Method__c = 'Declining Balance';
        app.genesis__Payment_Frequency__c = 'Monthly';
        app.genesis__Payment_Amount__C = 100;
        app.ACH_Account_Number__c = '10000';
        app.ACH_Account_Type__c = 'Checking';
        app.ACH_Bank_Name__c = 'ABC';
        app.ACH_Routing_Number__c = '999999';
        
        
        app.genesis__Lending_Product__c = [Select Id from genesis__Loan_Product__c where Name =: 'Term Loan'][0].id;
   //     app.Lending_Product__c = app.genesis__Lending_Product__c;
        system.debug('lending prod 1 ' + app.genesis__Lending_Product__c);
        
        insert app;
        return app;
    }
    
    static genesis__Applications__c createLoanApplication2(){
        genesis__Applications__c app = new genesis__Applications__c();
        
        //app.Name = 'Test Application';
        List<RecordType> rtList = [Select Id from RecordType
                                    where SObjectType = 'genesis__Applications__c' and DeveloperName =: 'Loan'];
        app.RecordTypeId = rtList[0].Id;
        app.genesis__Contact__c = Testhelper.createContact().Id;
        app.genesis__Product_Type__c = 'LOAN';           
        app.genesis__Term__c = 10;
        app.genesis__Loan_Amount__c = 1000;
        app.genesis__Interest_Rate__c = 20;
        app.genesis__Days_Convention__c = '365/365';
        app.genesis__Expected_Start_Date__c = System.today();
        app.genesis__Expected_First_Payment_Date__c = system.today();
        app.genesis__Interest_Calculation_Method__c = 'Declining Balance';
        app.genesis__Payment_Frequency__c = 'Monthly';
        app.genesis__Payment_Amount__c = 200;
        app.genesis__Expected_Close_Date__c = System.Today().addMonths(10);

        app.genesis__Lending_Product__c = [Select Id from genesis__Loan_Product__c where Name =: 'Term Loan'][0].id;
    //    app.Lending_Product__c = app.genesis__Lending_Product__c;
        system.debug('lending prod 1 ' + app.genesis__Lending_Product__c);
        
        insert app;
        return app;
    }
     

}