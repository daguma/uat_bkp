@isTest public class CustomOtherTransactionsTriggerTest {
    
    @isTest static void inserts_OtherTransaction(){
        
        loan__Loan_Account__c theAccount = LibraryTest.createContractTH();
        theAccount.loan__Pmt_Amt_Cur__c = 120.00;
        update theAccount;
        
        loan__Other_Transaction__c OtherTrans = New loan__Other_Transaction__c();
        OtherTrans.loan__Loan_Account__c = theAccount.Id;
        OtherTrans.loan__Txn_Date__c = Date.today();
        OtherTrans.loan__OT_ACH_Payment_Amount__c = theAccount.loan__Pmt_Amt_Cur__c;
        OtherTrans.loan__OT_ACH_Debit_Date__c = Date.today();
        OtherTrans.loan__Transaction_Type__c = 'Payment Change';
        OtherTrans.loan__Next_Due_Generation_Date__c = Date.today();
        insert OtherTrans;
        
        
        OtherTransactionLogs__c log = [SELECT Id, Name, Other_Loan_Transaction__c, Created_By_Name__c, Modification_Type__c, Email_Body__c, CL_Contract__c 
                                       FROM OtherTransactionLogs__c 
                                       WHERE CL_Contract__c =: theAccount.Id];
        
		boolean result = SalesForceUtil.EmailByUserId('Modification Made Without Tracking', 'Body Testing', '0053B000000ejaeQAA');
        
                
        System.assertEquals(log.Other_Loan_Transaction__c, OtherTrans.Id);
        System.assertEquals('Payment Change', log.Modification_Type__c);
        System.assertEquals(true, result);
    }
         
    

}