global class RefinanceManualEligibilityCheck 
{
    Webservice static String doRefinance(String cntId) 
    {
        List<loan__Loan_Account__c> contractList = [Select id from loan__Loan_Account__c WHERE loan__Contact__c =: cntId ORDER BY CreatedDate DESC LIMIT 1]; 
        Map <String, Object> params = new Map<String, Object>();

        if (contractList.size() > 0) 
        {
            String contractId = contractList.get(0).Id;
            params.put('inputContractId', contractId);

            List<loan_Refinance_Params__c> refParamsBefore = [  SELECT  Id, 
                                                                        Eligible_For_Soft_Pull__c,
                                                                        ManualEligibilityCheck__c
                                                                FROM    loan_Refinance_Params__c 
                                                                WHERE   Contract__c =: contractId 
                                                                ORDER BY CreatedDate DESC 
                                                                LIMIT 1];
            
            if (refParamsBefore.size() > 0) 
            {
                loan_Refinance_Params__c beforeParams = refParamsBefore.get(0);
                
                if (beforeParams.Eligible_For_Soft_Pull__c) 
                {
                    beforeParams.Last_Refinance_Eligibility_Review__c = Date.today();
                    update beforeParams;
                    return 'This customer already has a Refinance application open. You cannot create a new application at this time.';
                }
                    
                
                beforeParams.ManualEligibilityCheck__c = true;
                update beforeParams;
            }

            Flow.Interview.Refinance_Eligibility_Check refiFlow = new Flow.Interview.Refinance_Eligibility_Check(params);
            
            refiFlow.start();

            List<loan_Refinance_Params__c> refParam = [ SELECT  Id, 
                                                                Eligible_For_Soft_Pull__c,
                                                                ManualEligibilityCheck__c
                                                        FROM    loan_Refinance_Params__c 
                                                        WHERE   Contract__c =: contractId 
                                                        ORDER BY CreatedDate DESC 
                                                        LIMIT 1];

            if (refParam.size() > 0) 
            {   
                loan_Refinance_Params__c theseParams = refParam.get(0);

                if (theseParams.Eligible_For_Soft_Pull__c) 
                {
                    return 'This customer is eligible for a refinance and a new application has been created.';
                }
                else
                {
                    theseParams.ManualEligibilityCheck__c = false;
                    theseParams.Last_Refinance_Eligibility_Review__c = Date.today();
                    update theseParams;
                    return 'This customer is NOT eligible for a refinance at this time.';
                }
            }
            else
                return 'The current contract does not have any Refinance parameters.';
        }
        else
        {
            return 'This customer does not have a contract.';
        }
    }
}