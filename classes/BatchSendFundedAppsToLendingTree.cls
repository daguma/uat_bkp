global class BatchSendFundedAppsToLendingTree implements Database.Batchable<sObject>, Schedulable,  
                                                    Database.AllowsCallouts, Database.Stateful{
   String query = '';
   Map<String,String> reqLogPerAppMap = new Map<String,String>();
   List<Logging__c> apiLogList = new List<Logging__c>();
   Partner_Configurations__c lendingTree = Partner_Configurations__c.getInstance('LendingTree');
   Map<String,Opportunity> appIdBasedOnPointCode = new Map<String,Opportunity>();
   List<Opportunity> appsToUpdList = new List<Opportunity>();
   
   global BatchSendFundedAppsToLendingTree(){}
   
   global void execute (SchedulableContext sc) {
        Integer iNumAppsToProcess = lendingTree != null && lendingTree.Apps_To_Send_Per_Batch__c != null ? lendingTree.Apps_To_Send_Per_Batch__c.intValue() : 200;
        BatchSendFundedAppsToLendingTree job = new BatchSendFundedAppsToLendingTree();
        System.debug('Starting BatchSendFundedAppsToLendingTree at ' + Date.today());
        Database.executeBatch(job, iNumAppsToProcess);
    }


   global Database.QueryLocator start(Database.BatchableContext BC){
      try{
      String appStatus = 'Funded', partnerSS = lendingTree.name;

      Date fundDate =lendingTree.Funded_Day__c;
      
      query = 'Select id, Point_Code__c, Recall_Fundings_API__c, Amount, Interest_Rate__c, Effective_APR__c, Term__c, Funded_Date__c '+
                     ' FROM Opportunity '+
                     ' WHERE   Lead_Sub_Source__c = \'LendingTree\' and Status__c = \'Funded\' and (Recall_Fundings_API__c = true OR Funded_Date__c >= :fundDate  )';
      
      }catch(exception ex){
          System.debug('Exception:'+ ex.getMessage());
          WebToSFDC.notifyDev('BatchSendFundedAppsToLendingTree.QueryLocator ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
      }
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Opportunity> scope){
     
        system.debug('Testing scope==== ' +scope);
        try{
        String reqBody = generateRequest(scope);
        If(reqBody.contains('DATAROW') ){
            Logging__c apiLog = new Logging__c(API_Request__c = reqBody);            
            apiLog.Webservice__c = 'LendingTree Funding API dated - '+system.today().format();
           
            Http h = new Http();
            HttpRequest req = Finwise.getReq(lendingTree.End_Point__c, 'POST');
            req.setHeader('Content-Type','application/xml');
            req.setBody(reqBody);
            
            HttpResponse res;
            
             if(!Test.isRunningTest())
                res = h.send(req);
            else {
                res = new HttpResponse();
                res.setBody(reqBody);
            } 
           // HttpResponse res  = h.send(req);             
                        
            apiLog.API_Response__c = res.getBody();
            apiLogList.add(apiLog);
            parseResponse(res.getBody());

            
        }
        }catch(exception ex){
            System.debug('Exception:'+ ex.getMessage());
            WebToSFDC.notifyDev('BatchSendFundedAppsToLendingTree.Execute ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
            system.debug('Testing ex.getTypeName()==== ' +ex.getTypeName());
            If(ex.getTypeName() == 'System.CalloutException'){

                for(Opportunity app : scope){app.Recall_Fundings_API__c = true;appsToUpdList.add(app); }                                                        
            }
            
       }
       
       try{
                  If(!apiLogList.isEmpty() )insert apiLogList;               

           If(!appsToUpdList.isEmpty() )
               update appsToUpdList;
       }catch(exception ex){
            System.debug('Exception:'+ ex.getMessage());
            WebToSFDC.notifyDev('BatchSendFundedAppsToLendingTree.Finish ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
       }    
		apiLogList.clear();
       appsToUpdList.clear();
    }

   global void finish(Database.BatchableContext BC){       
       try{
           If(lendingTree != null){
               lendingTree.Funded_Day__c = system.today(); update lendingTree;               
           }
       }catch(exception ex){
            System.debug('Exception:'+ ex.getMessage());
            WebToSFDC.notifyDev('BatchSendFundedAppsToLendingTree.Finish ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
       }    
   }
   
   public String generateRequest(List<Opportunity> fundedAppList) {
        Dom.Document reqDoc = new Dom.Document();
        Dom.XMLNode rootEle = reqDoc.createRootElement('AUTOMATEDFUNDING', null, null);
        for(Opportunity app : fundedAppList){
            /* request */
            If(String.isNotBlank(app.Point_Code__c) && app.Point_Code__c.contains('LT') && String.isNotBlank(app.Point_Code__c.remove('LT')) ) {
            Dom.XMLNode dataRow = rootEle.addChildElement('DATAROW', null, null);
            String loanAppId = app.Point_Code__c.remove('LT');
            String loanAmt = app.Amount <> null ? String.valueOf(app.Amount) : '';
            String interestRate = app.Interest_Rate__c <> null ? String.valueOf(app.Interest_Rate__c) : '';
            String fundDate = app.Funded_Date__c <> null ? app.Funded_Date__c.format() : '';
            String term = app.Term__c <> null ? String.valueOf(app.Term__c.intValue()) : '';
            String apr = app.Effective_APR__c <> null ? String.valueOf(app.Effective_APR__c) : '';

            addCol(dataRow, 'QF Name', app.Point_Code__c.remove('LT') );
            appIdBasedOnPointCode.put(loanAppId,app); // store appID based on tracking number/point code
            
            addCol(dataRow, 'Funded Product Type', '3' );
            addCol(dataRow, 'Funded Loan Purpose', '1' );
            addCol(dataRow, 'Amount', loanAmt);
            addCol(dataRow, 'Rate', interestRate);
            addCol(dataRow, 'APR', apr);
            addCol(dataRow, 'Term', term);
            addCol(dataRow, 'Date Funded', fundDate);
            
            /* store request per application */
            DOM.Document doc = new DOM.Document();
            dom.XmlNode dataRowReq = doc.createRootElement('DATAROW', null, null);
            addCol(dataRowReq, 'QF Name', loanAppId);
            addCol(dataRowReq, 'Funded Product Type', '3');
            addCol(dataRowReq, 'Funded Loan Purpose', '1');
            addCol(dataRowReq, 'Amount', loanAmt);
            addCol(dataRowReq, 'Rate', interestRate);
            addCol(dataRowReq, 'APR', apr);
            addCol(dataRowReq, 'Term', term);
            addCol(dataRowReq, 'Date Funded', fundDate);
            
            reqLogPerAppMap.put(loanAppId, doc.toXmlString()); // store request per application
        }   
        }
        Dom.XMLNode validation = rootEle.addChildElement('VALIDATION', null, null);
        validation.addChildElement('USERNAME', null, null).addTextNode(lendingTree.Username__c);
        validation.addChildElement('PASSWORD', null, null).addTextNode(lendingTree.Password__c);
        validation.addChildElement('LENDERUID', null, null).addTextNode(lendingTree.LenderUID__c);
              
        return reqDoc.toXmlString();
   }

   void addCol(Dom.XMLNode dataRow, String att, String val ) {
      dom.XmlNode columnReq7 = dataRow.addChildElement('COLUMN', null, null);
      columnReq7.setAttribute('NAME', att);
      columnReq7.addTextNode(val);
   }
   
   public void parseResponse(String resBody) {
     // Start storing response
      Dom.Document docx = new Dom.Document();
      docx.load(resBody);
      system.debug('docx  ==== ' +docx.toXmlString());
      
      Dom.XMLNode[] errStatus = docx.getrootelement().getchildelements();
      for (dom.XmlNode errStatusChild : errStatus)  {                  
          String QFNAME = '', ERRORNUM = '', ERRORDESCRIPTION = '';
          for(dom.XMLNode errStatusChildren : errStatusChild.getchildren() ){              
              If(errStatusChildren.getname() == 'QFNAME') QFNAME = errStatusChildren.gettext();                  
              If(errStatusChildren.getname() == 'ERRORNUM') ERRORNUM = errStatusChildren.gettext();                  
              If(errStatusChildren.getname() == 'ERRORDESCRIPTION') ERRORDESCRIPTION = errStatusChildren.gettext();                  
          }
          // Create the request envelope
          DOM.Document doc = new DOM.Document();
          dom.XmlNode errStatusNode = doc.createRootElement('ERRSTATUS', null, null);
          dom.XmlNode body1= errStatusNode.addChildElement('QFNAME', null, null).addTextNode(QFNAME);
          dom.XmlNode body2= errStatusNode.addChildElement('ERRORNUM', null, null).addTextNode(ERRORNUM);
          dom.XmlNode body3= errStatusNode.addChildElement('ERRORDESCRIPTION', null, null).addTextNode(ERRORDESCRIPTION);
          system.debug('doc xml ==== ' +doc.toXmlString());
            
          Logging__c log = new Logging__c();
          If(String.isNotBlank(QFNAME) && appIdBasedOnPointCode.containsKey(QFNAME) )
          log.Opportunity__c = appIdBasedOnPointCode.get(QFNAME).ID;
          log.API_Response__c = doc.toXmlString();
          log.Webservice__c = 'LendingTree Funding API';
          If(reqLogPerAppMap.containsKey(QFNAME) )log.API_Request__c = reqLogPerAppMap.get(QFNAME);                
          apiLogList.add(log);
           
          Opportunity appToUpdate = appIdBasedOnPointCode.get(QFNAME);
          appToUpdate.Recall_Fundings_API__c = !ERRORDESCRIPTION.contains('SUCCESS') ;

          appsToUpdList.add(appToUpdate);
      }
   }
}