public with sharing class CLServicerActionsCtrl{
  public loan__loan_Account__c acc {get; set;}
  public String segment {get; set;}
  public String dpdGroup {get; set;}
  public Decimal fico {get; set;}
  public Decimal dti {get; set;} 
  public List<Securitization_Action__c> actionList {get; set;}
  
  public Boolean userAllowToRequestApproval {get; set;}
  public Boolean alreadyInProcess {get; set;}
  public Boolean msgApprovalRequested {get; set;}
  public Boolean msgOverrideRequested {get; set;}
  public Boolean hasActions {get; set;}
  public Boolean hasInitValues {get; set;}

  public Boolean btnOverrideApprovalDisable {get; set;}
  public Boolean btnRequestApprovalDisable {get; set;}

  public CLServicerActionsCtrl(Apexpages.StandardController controller){

    this.fico = null;
    this.dti = null;
    this.hasInitValues = true;
    this.msgApprovalRequested = false;
    this.userAllowToRequestApproval = true;
    this.actionList = new List<Securitization_Action__c>();

    if (!Test.isRunningTest()) controller.addFields(new List<String>{'Overdue_Days__c', 'Opportunity__c', 'Originated_FICO__c', 'Originated_DTI__c'});
    acc = (loan__loan_Account__c)controller.getRecord();
    
    if (String.isNotEmpty(acc.Opportunity__c) && acc.Overdue_Days__c != null){
      for (AccountManagementHistory__c accHistory : [SELECT Id, FicoValue__c, DTIValue__c
                                                         FROM AccountManagementHistory__c
                                                         WHERE Opportunity__c =: acc.Opportunity__c
                                                         ORDER BY CreatedDate DESC
                                                         LIMIT 1]){
          this.fico = accHistory.FicoValue__c;
          this.dti = accHistory.DTIValue__c;
      }

      if (this.fico == null || this.dti == null){
        this.fico = acc.Originated_FICO__c;
        this.dti = acc.Originated_DTI__c;
      }

      if (this.fico != null && this.dti != null){

        actionList = actionsByBucketAndSegment();

        if (actionList != null && actionList.size() > 0){

          this.dpdGroup = actionList.get(0).BucketId__r.Name;
          this.segment = actionList.get(0).SegmentId__r.Name;
          this.hasActions = true; 

          if (String.isNotEmpty(getWorkItemId(acc.Id))){
            this.btnOverrideApprovalDisable = false;
            this.alreadyInProcess = true;
            this.btnRequestApprovalDisable = true;
          }
          else{
            this.btnOverrideApprovalDisable = true;
            this.alreadyInProcess = false;
            this.btnRequestApprovalDisable = false;
          }
        }else{
          this.segment = 'N/A';
          dpdGroup = 'N/A';
          this.hasActions = false;
          this.btnOverrideApprovalDisable = true;
        }
      }
      else
        this.hasInitValues = false;
    } 
    
    if (!label.Securitization_Allowed_ProfileId_ApprovalProc.contains(UserInfo.getProfileId())){
      this.userAllowToRequestApproval = false;
      this.btnRequestApprovalDisable = true;
    }
  }

  public List<Securitization_Action__c> actionsByBucketAndSegment(){
    return [SELECT Id, Name, BucketId__r.Name, Manager_Approval__c, Strategy_Ops__c, SegmentId__r.Name
            FROM Securitization_Action__c
            WHERE SegmentId__r.Fico_Min__c <=: this.fico
            AND SegmentId__r.Fico_Max__c >=: this.fico
            AND SegmentId__r.DTI_Min__c <=: this.dti
            AND SegmentId__r.DTI_Max__c >=: this.dti
            AND BucketId__r.Max__c >=: acc.Overdue_Days__c
            AND BucketId__r.Min__c <=: acc.Overdue_Days__c
            AND IsActive__c = true
            ];
  }

  public void requestApproval(){
    if (!this.alreadyInProcess){
      Approval.ProcessSubmitRequest reqApproval = new Approval.ProcessSubmitRequest();
      reqApproval.setComments('Requesting from CLbutton.');
      reqApproval.setObjectId(acc.id);
      reqApproval.setSubmitterId(UserInfo.getUserId());
      reqApproval.setProcessDefinitionNameOrId('Collection_Approvals');
      reqApproval.setSkipEntryCriteria(true);
      Approval.process(reqApproval);

      this.btnRequestApprovalDisable = true;
      this.msgApprovalRequested = true;
      this.btnOverrideApprovalDisable = false;
      this.alreadyInProcess = true;
    }
  }

  public void overrideApproval(){
    Approval.ProcessWorkitemRequest overrideApproval = new Approval.ProcessWorkitemRequest();
    overrideApproval.setAction('Approve');
    overrideApproval.setWorkitemId(getWorkItemId(acc.Id));
    if (this.alreadyInProcess){
      if (UserInfo.getProfileId() == label.System_Admin_ProfileId){
        overrideApproval.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        overrideApproval.setComments('Approving request from CLbutton.');
      }
      else{
        overrideApproval.setNextApproverIds(new Id[] {Label.Securitization_Default_User_Assigned});
        overrideApproval.setComments('Approving request from CLbutton without rights access.');

        insert new Securitization_User_Approve__c(userId__c=UserInfo.getUserId(), accountId__c=acc.Id);
      }
      Approval.process(overrideApproval);

      this.alreadyInProcess = false;
      this.msgOverrideRequested = true;
      this.btnOverrideApprovalDisable = true;
      if (userAllowToRequestApproval) 
        this.btnRequestApprovalDisable = false;
    }
  }

  private String getWorkItemId(Id targetObjectId){
    String workItemId = '';
    for(ProcessInstanceWorkitem workItem :[Select Id from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =: targetObjectId]){
      workItemId  =  workItem.Id;
    }
    return workItemId;
  }
}















/*if (initFico != null || initDTI != null){
          //find segment
          if(initFico > 600 && initDTI < 30){
              this.segment = 'Segment 1';
          }
          else if(initFico < 600 && initDTI < 30){
              this.segment = 'Segment 2';
          }
          else if(initFico > 600 && initDTI > 30){
              this.segment = 'Segment 3';
          }
          else if(initFico < 600 && initDTI > 30){
              this.segment = 'Segment 4';
          }
          else 
              this.segment = 'N/A';*/