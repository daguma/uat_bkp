@isTest public class DecisioningEntityTest {

    @isTest static void checkConstructor() {

        BRMS_Configurable_Settings__c settings = new BRMS_Configurable_Settings__c();
        settings.name = 'settings';
        settings.Milestone_Day__c = Date.today().addDays(600);
        settings.Do_Not_Call__c = true;
        settings.Min_Rule_Count__c = 6;
        settings.Show_all_rules__c = true;
        insert settings;

        TestHelper.createEmailAgeSettings();

        Contact TestContact = LibraryTest.createContactTH();
        Opportunity app = TestHelper.createApplicationFromContact(TestContact, true);

        DecisioningEntity de = new DecisioningEntity(new DecisioningCtrl(app.Id));

        system.assertNotEquals(null,de);
    }

    @isTest static void checkCR(){

        BRMS_Configurable_Settings__c settings = new BRMS_Configurable_Settings__c();
        settings.name = 'settings';
        settings.Milestone_Day__c = Date.today().addDays(600);
        settings.Do_Not_Call__c = true;
        settings.Min_Rule_Count__c = 6;
        settings.Show_all_rules__c = true;
        insert settings;

        TestHelper.createEmailAgeSettings();

        Contact TestContact = LibraryTest.createContactTH();
        Opportunity app = TestHelper.createApplicationFromContact(TestContact, true);

        DecisioningEntity de = new DecisioningEntity(new DecisioningCtrl(app.Id));

        ProxyApiCreditReportEntity cr = LibraryTest.fakeCreditReportEntity();
        de.setCreditReportDetails(cr);

        system.assertNotEquals(null,de);

    }

}