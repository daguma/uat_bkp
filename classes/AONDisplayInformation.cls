global class AONDisplayInformation {

	public String dialogHTML { get; set; }

 	public String recordId {get; set;}
    private List<loan_AON_Information__c> enrollments {get; set;}


    Webservice static String getAllInfo(String thisId) 
    {
        AONDisplayInformation u = new AONDisplayInformation();
        u.recordId = thisId;
        u.initialize();
        return u.parseForLayout();
    }
  
   public void initialize() 
    {
        System.debug('recordId: ' + recordId);
        
        enrollments = [SELECT 
        				Id, 
        				Product_Code__c,
        				Enrollment_Date__c,
                        Cancellation_Date__c, 
        				Claim_Number__c, 
        				Claim_Status__c, 
        				Claim_Disposition__c,
                        Claim_Status_Date__c
        		  FROM loan_AON_Information__c 
        		  WHERE Contract__c = : recordId
        		  ORDER BY CreatedDate DESC];

        dialogHTML = parseForLayout();

        System.debug('dialogHTML: ' + dialogHTML);
    }

   
     public String parseForLayout() 
    {
        String enrolled,
               producttype,
               enrollmentdate,
               cancellationdate,
               claimnumber,
               claimstatus,
               claimdisposition,
               claimstatusdate;
        String divHeader = '<div id="AONInfoPopup" title="AON Payment Safeguard Information"><div id="AONInfoContent"><table>';

        String divTableHeaders =    '<thead>' + 
                                    '<th>Enrollment Status</th>' + 
                                    '<th>Product Type</th>' +
                                    '<th>Effective Date</th>' + 
                                    '<th>Cancellation Date</th>' + 
                                    '<th>Enrollment Channel</th>' + 
                                    '<th>Claim Number</th>' + 
                                    '<th>Claim Status</th>' +
                                    '<th>Claim Disposition</th>' +
                                    '<th>Claim Status Date</th>' +   
                                    '</thead>';

        String divTableRows = '<tbody>';
        String divTableFooter = '</table></div></div>';


        for(loan_AON_Information__c enrollment : enrollments){     
            enrolled = (enrollment.Cancellation_Date__c != null) ? 'Cancelled' : (enrollment.Enrollment_Date__c != null && enrollment.Cancellation_Date__c == null) ? 'Enrolled' : 'Not Enrolled';
            producttype =  (enrollment.Product_Code__c == null) ? ' - ' : (enrollment.Product_Code__c.containsIgnoreCase('Option')) ? 'Optional' : 'Bundled';
            enrollmentdate = (enrollment.Enrollment_Date__c == null) ? ' - ' : String.valueOf(enrollment.Enrollment_Date__c);
            cancellationdate = (enrollment.Cancellation_Date__c == null) ? ' - ' : String.valueOf(enrollment.Cancellation_Date__c);
            claimnumber = (enrollment.Claim_Number__c == null) ? ' - ' : enrollment.Claim_Number__c;
            claimstatus = (enrollment.Claim_Status__c == null) ? ' - ' : enrollment.Claim_Status__c;
            claimdisposition = (enrollment.Claim_Disposition__c == null) ? ' - ' : enrollment.Claim_Disposition__c;
            claimstatusdate = (enrollment.Claim_Status_Date__c == null) ? ' - ' : String.valueOf(enrollment.Claim_Status_Date__c);
            divTableRows += '<tr>';
            divTableRows += '<td>' +  enrolled + '</td>';
            divTableRows += '<td weight="20%">' + producttype + '</td>';
            divTableRows += '<td>' + enrollmentdate + '</td>';
            divTableRows += '<td>' + cancellationdate + '</td>';
            divTableRows += '<td> INT </td>';
            divTableRows += '<td>' + claimnumber + '</td>';
            divTableRows += '<td>' + claimstatus + '</td>';
            divTableRows += '<td>' + claimdisposition + '</td>';
            divTableRows += '<td>' + claimstatusdate + '</td>';
            divTableRows += '</tr>';
        }

        divTableRows += '</tbody>';

        return divHeader + divTableHeaders + divTableRows + divTableFooter;
    }

	public AONDisplayInformation() {
		
	}
}