@isTest
private class ClarityTest {

  @isTest static void gets_inquiries_without_employment_details() {
    Opportunity  app = LibraryTest.createApplicationTH();
    ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);

    ProxyApiClarityInquiryEntity result = Clarity.getInquiries(creditReport);

    System.assertEquals(null, result);
  }
  @isTest static void gets_Clarity_Income() {
    Opportunity  app = LibraryTest.createApplicationTH();    

    ProxyApiClarityClearIncomeAttributes result = Clarity.getClarityIncome(app.id);

    //System.assertNotEquals(null, result);
  }
  @isTest static void gets_inquiries() {

    Opportunity app = LibraryTest.createApplicationTH();
    ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);

    Employment_Details__c empDetails = new Employment_Details__c();
    empDetails.Hire_Date__c = System.today().addYears(-3);
    empDetails.Position__c = 'Assistant';
    empDetails.Payroll_Type__c = 'Direct Deposit';
    empDetails.Salary_Cycle__c = 'Semi-Monthly';
    empDetails.Last_Pay_Date__c = System.today().addDays(-15);
    empDetails.Employer_Phone_Number__c = '555';
    empDetails.Opportunity__c = app.id;
    insert empDetails;

    ProxyApiUtil.settings = LibraryTest.fakeSettings();

    ProxyApiClarityInquiryEntity result = Clarity.getInquiries(creditReport);

    System.assertNotEquals(null, result);
  }

}