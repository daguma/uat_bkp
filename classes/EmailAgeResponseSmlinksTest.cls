@isTest public class EmailAgeResponseSmlinksTest {
     @isTest static void validate() {
         
         EmailAgeResponseSmlinks ea = new EmailAgeResponseSmlinks();
         
         ea.source = 'source';
         System.assertEquals('source', ea.source);
         
         ea.link = 'link';
         System.assertEquals('link', ea.link);
     }
}