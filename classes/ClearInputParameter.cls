global class ClearInputParameter {

    public String contactId {get; set;}

    public String phone {get; set;}

    public String dob {get; set;}

    public ClearAddress address {get; set;}
}