@isTest
private class FactorTrustUtilTest {

    @isTest static void FT_service_response() {

        Lead LeadTest = LibraryTest.createLeadTH();
        Contact ContactTest = LibraryTest.createContactTH();

        String FTServiceTest = FactorTrustUtil.FTService(LeadTest.id, ContactTest.id);

        System.assert(FTServiceTest != null);
    }

    @isTest static void FT_service_response_with_logging() {

        Lead LeadTest = LibraryTest.createLeadTH();
        Contact ContactTest = LibraryTest.createContactTH();

        Logging__c log = new Logging__c();
        log.API_Response__c = '';
        log.Lead__c = LeadTest.Id;
        log.Webservice__c = 'FactorTrust';

        String FTServiceTest = FactorTrustUtil.FTService(LeadTest.id, ContactTest.id);

        System.assert(FTServiceTest != null);
    }

    @isTest static void FT_store2Service_response() {

        opportunity appTest = LibraryTest.createApplicationTH();
        Lead LeadTest = LibraryTest.createLeadTH();

        String FTStore2ServiceTest = FactorTrustUtil.FTStore2Service(LeadTest.id, appTest.contact__c, appTest.id);

        System.assert(FTStore2ServiceTest != null);
    }

    @isTest static void factor_trust_early_warning_system() {

        opportunity appTest = LibraryTest.createApplicationTH();
        Lead LeadTest = LibraryTest.createLeadTH();

        Test.startTest();

        FactorTrustUtil.FactorTrustEWS(AppTest.id);

        Test.stopTest();

        appTest = [SELECT Id, FT_Status__c, Ft_Code__c, Ft_Description__c FROM opportunity WHERE Id = : appTest.Id LIMIT 1];

        System.assertEquals(null, appTest.FT_Status__c);
    }
    
    @isTest static void updates_early_warning_system_response_200() {

        String xmlResponse = '<ApplicationResponse><LoginInfo>'+
            '<ChannelIdentifier></ChannelIdentifier><MerchantIdentifier>40020</MerchantIdentifier>'+
            '<StoreIdentifier>0002</StoreIdentifier></LoginInfo><ApplicationInfo><ClientTransactionID>110089844</ClientTransactionID>'+
            '<TransactionDate>10/3/2017 11:48 AM</TransactionDate><ApplicationID>0030B000025lIrSQAU</ApplicationID>'+
            '<TransactionStatus>A</TransactionStatus><ApprovedAmount>8,000.00</ApprovedAmount><TransactionScore>0</TransactionScore>'+
            '<LendProtectScore>500</LendProtectScore><ProcessedServicesStatus>ALL</ProcessedServicesStatus>'+
            '<ReportLink>https://www.factortrust.com/Misc/TransactionReport.aspx?ReportKey=e69e89b0-daf9-4ca7-8643-8fe6a7d5677e</ReportLink>'+
            '<ResponseCodes></ResponseCodes><DDAPlus><DDAPlusResponse>LOW RISK APPROVAL</DDAPlusResponse></DDAPlus></ApplicationInfo></ApplicationResponse>';
        
        String response = '{"statusCode":"200","response":"' + xmlResponse + '"}';
    FT_Store2_ReponseDetail__c n = new FT_Store2_ReponseDetail__c();
        n.Name = 'LOW RISK APPROVAL';
        n.Status__c = 'Negative';
        n.Code__c = '200';
        insert n;
        
        opportunity appTest = LibraryTest.createApplicationTH();
        appTest.FT_Status__c = 'Negative';
        update appTest;

        Test.startTest();

        FactorTrustUtil.UpdateEWSResponse(appTest, response);

        Test.stopTest();

    }

}