global class LoanDisbursalSummarizedRegularJob  implements Schedulable  {
    global void execute (SchedulableContext sc) {
        try {
            LoanDisbursalSummarizedByLine t = new LoanDisbursalSummarizedByLine();
            t.calculateDates();
            t.generateNonFinwise();
            WebToSFDC.notifyDev('Regular Disbursal file generation', 'File generate succesfully');
        }catch (Exception e) {  WebToSFDC.notifyDev('Regular Disbursal file generation', e.getMessage() + '\n' + e.getLineNumber() + '\n' + e.getStackTraceString());}
    }
}