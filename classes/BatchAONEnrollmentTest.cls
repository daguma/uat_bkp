@isTest
private class BatchAONEnrollmentTest {

    @isTest static void processes_aon_enrollment() {
				Librarytest.createBrmsConfigSettings(10, true);

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
		contract.AON_Fee__c = 0;
        contract.loan__Pmt_Amt_Cur__c = 200;
        update contract;
        
        AON_Fee__c feeSetting = new AON_Fee__c();
        feeSetting.Name = 'AON Fee';
        feeSetting.Fee__c = 6.31;
        insert feeSetting;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = contract.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        enrollment.IsCancellation__c = false;
        enrollment.IsAIMCancellation__c = false;
        enrollment.RetryCount__c = 0;
        insert enrollment;
		System.assert([SELECT AON_Enrollment__c FROM loan__Loan_Account__c WHERE Id =: contract.Id].size() > 0, 'Enrollment Not Inserted');
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
		insert ProxyApiBRMSUtil.settings;

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"aonResponse":null,"errorMessage":null,"errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        BatchAONEnrollment batchapex = new BatchAONEnrollment();
        Database.executebatch(batchapex, 1);

        //System.assert([SELECT AON_Enrollment__c FROM loan__Loan_Account__c WHERE Id =: contract.Id].AON_Enrollment__c);
    }

    @isTest static void processes_aon_enrollment_with_error() {
		Librarytest.createBrmsConfigSettings(10, true);

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
		contract.AON_Fee__c = 0;
        contract.loan__Pmt_Amt_Cur__c = 200;
        update contract;
        Opportunity app = [SELECT Id, Name, Payment_Frequency__c FROM Opportunity WHERE Id =: contract.Opportunity__c LIMIT 1];
        app.Payment_Frequency__c = 'Monthly';
        app.StrategyType__c = '1';
        app.Term__c = 10;
        update app;
        
        
        AON_Fee__c feeSetting = new AON_Fee__c();
        feeSetting.Name = 'AON Fee';
        feeSetting.Fee__c = 6.31;
        insert feeSetting;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = contract.Id;
        enrollment.IsProcessed__c = false;
        enrollment.Product_Code__c = 'LPOPTION01';
        enrollment.IsCancellation__c = false;
        enrollment.IsAIMCancellation__c = false;
        enrollment.RetryCount__c = 0;
        insert enrollment;
		System.assert([SELECT AON_Enrollment__c FROM loan__Loan_Account__c WHERE Id =: contract.Id].size() > 0, 'Enrollment Not Inserted');

        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
		insert ProxyApiBRMSUtil.settings;
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"aonResponse":null,"errorMessage":"test error","errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        BatchAONEnrollment batchapex = new BatchAONEnrollment();
        Database.executebatch(batchapex, 1);

        //System.assertNotEquals(null, [SELECT ServiceError__c FROM loan_AON_Information__c WHERE Id =: enrollment.Id].ServiceError__c);
    }

    @isTest static void schedule_job() {
        		Librarytest.createBrmsConfigSettings(10, true);

        Test.startTest();

        AON_Fee__c feeSetting = new AON_Fee__c();
        feeSetting.Name = 'AON Fee';
        feeSetting.Fee__c = 6.31;
        insert feeSetting;

        BatchAONEnrollment enrollJob = new BatchAONEnrollment();
        String cron = '0 0 23 * * ?';
        system.schedule('Test AON Enrollment Job', cron, enrollJob);

        Test.stopTest();
    }
    
    @isTest static void processes_aon_enrollment_without_product_code() {
		Librarytest.createBrmsConfigSettings(10, true);

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
		contract.AON_Fee__c = 0;
        contract.loan__Pmt_Amt_Cur__c = 200;
        update contract;
        Opportunity app = [SELECT Id, Name, Payment_Frequency__c FROM Opportunity WHERE Id =: contract.Opportunity__c LIMIT 1];
        app.Payment_Frequency__c = 'Monthly';
        app.StrategyType__c = '1'; 
        app.Term__c = 10;
        update app;
        
    AON_Product_Codes__c prodCode = new AON_Product_Codes__c();
        prodCode.Name = 'OPTIONAL MONTHLY';
        prodCode.ProductCode__c = 'LPOPTION01';
        insert prodCode;

        AON_Fee__c feeSetting = new AON_Fee__c();
        feeSetting.Name = 'AON Fee';
        feeSetting.Fee__c = 6.31;
        insert feeSetting;

        loan_AON_Information__c enrollment = new loan_AON_Information__c();
        enrollment.Contract__c = contract.Id;
        enrollment.IsProcessed__c = false;
          enrollment.IsCancellation__c = false;
        enrollment.IsAIMCancellation__c = false;
        enrollment.RetryCount__c = 0;
        insert enrollment;
		System.assert([SELECT AON_Enrollment__c FROM loan__Loan_Account__c WHERE Id =: contract.Id].size() > 0, 'Enrollment Not Inserted');


        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
		insert ProxyApiBRMSUtil.settings;

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"aonResponse":null,"errorMessage":null,"errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        BatchAONEnrollment batchapex = new BatchAONEnrollment();
        Database.executebatch(batchapex, 1);
    }
    
}