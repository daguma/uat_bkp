@isTest
 public class UpdateCompletedBySectionWise_Test{
    
      public static testMethod void UpdateCompletedBySectionWise(){
          Opportunity  App= TestHelper.createApplication();
          
          Employment_Details__c EmpObj= new Employment_Details__c();
          EmpObj.Opportunity__c = App.id;
          EmpObj.Hire_Date__c= system.today();
          insert EmpObj;
          
          EmpObj.Last_Pay_Date__c=system.today();
          update EmpObj;
      }

 }