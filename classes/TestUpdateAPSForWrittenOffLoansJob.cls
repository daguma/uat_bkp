//ND-2421
@isTest
public class TestUpdateAPSForWrittenOffLoansJob {
    @isTest
    static void testWriteOffForAps() {
        Date systemDate = Date.newInstance(2013, 03, 01);
        loan.TestHelper.systemDate = systemDate;
        
        loan.TestHelper.createSeedDataForTesting();
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        
        //As a result of moving validation from code to validation rule, updating org Params
        loan.TestHelper.useCLLoanCRM();
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);

        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);

        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount,
                            curr,
                            dummyFeeSet);
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        
        //Create a dummy loan account
        loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccount(dummyLP,
                                                                   dummyClient,
                                                                   dummyFeeSet,
                                                                   dummyLoanPurpose,
                                                                   dummyOffice);
        //Create Bank Account
        loan__Bank_Account__c bank = new loan__Bank_Account__c(loan__Account_usage__c = 'Borrower/Investor Account',
                                                   loan__Bank_name__c = 'Chase',
                                                   loan__Active__c = true,
                                                   loan__Account_Type__c = 'Checking',
                                                   loan__Bank_Account_Number__c = String.valueOf(100000000),
                                                   loan__Routing_Number__c = '111111111');
         insert bank;
        
        loan__Payment_Mode__c pMode = [SELECT Id
                                       FROM loan__Payment_Mode__c
                                       WHERE Name = 'ACH'
                                      ];
        //Disburse the loan
        loan__Loan_Disbursal_Transaction__c disTxn = new loan__Loan_Disbursal_Transaction__c();
        disTxn.loan__Loan_Account__c = loanAccount.Id;
        disTxn.loan__Disbursal_Date__c = systemDate;
        disTxn.loan__Disbursed_Amt__c = 10000;
        disTxn.loan__Mode_Of_Payment__c = pMode.Id;
        
        loan.LoanDisbursalActionAPI action = new loan.LoanDisbursalActionAPI(disTxn);
        action.disburseLoanAccount();
        
        //Create APS for the loan.
        loan.TestHelper2.createAutomatedPaymentSetup('RECURRING',
                                                     'FIXED AMOUNT',
                                                      bank,
                                                      loanAccount,
                                                     'Monthly',
                                                      pMode,
                                                      systemDate.addMonths(1)
                                                     );
        
        loanAccount = [SELECT Id,
                              loan__Number_of_Days_Overdue__c,
                              loan__Loan_Status__c
                       FROM loan__Loan_Account__c
                      ];
        loanAccount.loan__Number_of_Days_Overdue__c = 91;
        loanAccount.loan__Loan_Status__c = 'Active - Bad Standing';
        update loanAccount;
        
        //Write off the loan.
        loan.WriteOff writeoffaloan = new loan.WriteOffFactory().getWriteOffAPI();
        writeoffaloan.writeOffALoan(loanAccount.Id);
        
        loan__Automated_Payment_Setup__c aps = [SELECT Id,
                                                       loan__Active__c,
                                                       loan__Transaction_Amount__c
                                                FROM loan__Automated_Payment_Setup__c
                                                WHERE loan__CL_Contract__C = :loanAccount.Id
                                               ];
        //Activating the APS
        aps.loan__Active__c = true;
        update aps;
        
        Test.startTest();
        
        UpdateAPSForWrittenOffLoansJob job = new UpdateAPSForWrittenOffLoansJob();
        DataBase.executeBatch(job);
        
        Test.stopTest();
        
        aps = [SELECT Id,
                      loan__Active__c
               FROM loan__Automated_Payment_Setup__c
               WHERE loan__CL_Contract__C = :loanAccount.Id
              ];
        
        //APS should be deactivated after write off
        system.assertEquals(aps.loan__Active__c, false);
    }
}