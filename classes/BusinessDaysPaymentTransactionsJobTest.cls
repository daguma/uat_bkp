@isTest
private class BusinessDaysPaymentTransactionsJobTest {

    @isTest static void executes_job() {
        Test.startTest();

        BusinessDaysPaymentTransactionsJob businessJob = new BusinessDaysPaymentTransactionsJob();
        String cron = '0 0 23 * * ?';
        system.schedule('Test Business Days Transaction Process', cron, businessJob);

        Test.stopTest();
    }

    @isTest static void updates_business_days() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        loan__Loan_Payment_Transaction__c pmt = null;

        System.runAs(thisUser) {

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            update testLoanAccount;

            pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = testLoanAccount.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today().addDays(-10);
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Payment_Mode__c = paymentMode.Id;
            insert pmt;
        }

        Test.startTest();

        BusinessDaysPaymentTransactionsJob b = new BusinessDaysPaymentTransactionsJob();
        database.executebatch(b);

        Test.stopTest();

        loan__Loan_Payment_Transaction__c trans = [SELECT Id, loan__Transaction_Date__c, Business_Day_Count__c, Payment_Pending__c
                FROM loan__Loan_Payment_Transaction__c
                WHERE Id = : pmt.Id LIMIT 1];

        System.assert(trans.Business_Day_Count__c > 0);
    }

    @isTest static void updates_payment_to_cleared() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        loan__Loan_Payment_Transaction__c pmt = null;

        System.runAs(thisUser) {

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            update testLoanAccount;

            pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = testLoanAccount.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today().addDays(-15);
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Payment_Mode__c = paymentMode.Id;
            insert pmt;
        }

        Test.startTest();

        BusinessDaysPaymentTransactionsJob b = new BusinessDaysPaymentTransactionsJob();
        database.executebatch(b);

        Test.stopTest();

        loan__Loan_Payment_Transaction__c trans = [SELECT Id, loan__Transaction_Date__c, Business_Day_Count__c, Payment_Pending__c
                FROM loan__Loan_Payment_Transaction__c
                WHERE Id = : pmt.Id LIMIT 1];

        System.assertEquals('Cleared', trans.Payment_Pending__c);
    }

    @isTest static void updates_payment_to_pending() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        loan__Loan_Payment_Transaction__c pmt = null;

        System.runAs(thisUser) {

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            update testLoanAccount;

            pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = testLoanAccount.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today().addDays(-5);
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Payment_Mode__c = paymentMode.Id;
            insert pmt;
        }

        Test.startTest();

        BusinessDaysPaymentTransactionsJob b = new BusinessDaysPaymentTransactionsJob();
        database.executebatch(b);

        Test.stopTest();

        loan__Loan_Payment_Transaction__c trans = [SELECT Id, loan__Transaction_Date__c, Business_Day_Count__c, Payment_Pending__c
                FROM loan__Loan_Payment_Transaction__c
                WHERE Id = : pmt.Id LIMIT 1];

        System.assertEquals('Pending', trans.Payment_Pending__c);
    }

    @isTest static void updates_payment_to_bounced() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        loan__Loan_Payment_Transaction__c pmt = null;

        System.runAs(thisUser) {

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            update testLoanAccount;

            pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = testLoanAccount.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today().addDays(-5);
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Payment_Mode__c = paymentMode.Id;
            pmt.loan__Reversed__c = true;
            insert pmt;
        }

        Test.startTest();

        BusinessDaysPaymentTransactionsJob b = new BusinessDaysPaymentTransactionsJob();
        database.executebatch(b);

        Test.stopTest();

        loan__Loan_Payment_Transaction__c trans = [SELECT Id, loan__Transaction_Date__c, Business_Day_Count__c, Payment_Pending__c
                FROM loan__Loan_Payment_Transaction__c
                WHERE Id = : pmt.Id LIMIT 1];

        System.assertEquals('Bounced', trans.Payment_Pending__c);
    }

    @isTest static void does_not_update_payment_pending() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        loan__Loan_Payment_Transaction__c pmt = null;

        System.runAs(thisUser) {

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            update testLoanAccount;

            pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = testLoanAccount.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today().addDays(-20);
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Payment_Mode__c = paymentMode.Id;
            pmt.loan__Reversed__c = true;
            insert pmt;
        }

        Test.startTest();

        BusinessDaysPaymentTransactionsJob b = new BusinessDaysPaymentTransactionsJob();
        database.executebatch(b);

        Test.stopTest();

        loan__Loan_Payment_Transaction__c trans = [SELECT Id, loan__Transaction_Date__c, Business_Day_Count__c, Payment_Pending__c
                FROM loan__Loan_Payment_Transaction__c
                WHERE Id = : pmt.Id LIMIT 1];

        System.assertEquals('Bounced', trans.Payment_Pending__c);
    }
}