public with sharing class OffersListCtrl {

    @AuraEnabled
    public static OffersListEntity getOffersList(Id oppId) {

        OffersListEntity ole = new OffersListEntity();

        try {

            Opportunity opp = [SELECT Fee_Handling__c FROM Opportunity WHERE Id = :oppId LIMIT 1];

            if (opp != null) {

                //Set the picklist values for Fee Handling
                ole.feeHandlingOptions =
                        alphaHelperCls.getPicklistItems(
                                Opportunity.Fee_Handling__c.getDescribe(),
                                opp.Fee_Handling__c,
                                false
                        );

            } else {
                ole.errorMessage = 'Opportunity data is invalid';
                return ole;
            }

            List<Offer__c> offersList = [
                    Select Id, Offer_Select_Source__c, Name, Repayment_Frequency__c,
                            APR__c, Term__c, Payment_Amount__c, IsSelected__c, Selected_Date__c, Loan_Amount__c, Createddate,
                            Total_Loan_Amount__c, Fee__c, APR_percent__c, IsCustom__c, Fee_Percent__c,
                            FirstPaymentDate__c, Installments__c, SelectedByID__c,
                            Effective_APR__c, Preferred_Payment_Date_String__c,
                            Offer_Expiry_Date__c, Customer_Disbursal__c, RefinanceFundedOffer__c,NeedsApproval__c
                    from Offer__c
                    where Opportunity__c = :oppId
                    order by Term__c desc
            ];

            Offer__c offer = new Offer__c();
            offer.Repayment_Frequency__c = '28 Days';
            offer.Term__c = 36;
            offer.IsSelected__c = true;

            Offer__c offer2 = new Offer__c();
            offer2.Repayment_Frequency__c = 'Semi-Monthly';
            offer2.Term__c = 24;
            offer2.IsSelected__c = false;

            offersList = new List<Offer__c>{offer};
            offersList.add(offer2);

            //Add the Picklist values to the offers list
            List<OfferEntity> offerEntities = new List<OfferEntity>();
            for (Offer__c anOfferFromTheList : offersList) {
                offerEntities.add(
                        new OfferEntity(
                                anOfferFromTheList,
                                alphaHelperCls.getPicklistItems(
                                        Offer__c.Repayment_Frequency__c.getDescribe(),
                                        anOfferFromTheList.Repayment_Frequency__c,
                                        false
                                )
                        )
                );
            }

            ole.offerEntities = offerEntities;

        } catch (Exception e) {
            String exceptionMessage = e.getMessage() + ' - ' + e.getStackTraceString();
            system.debug('OffersListCtrl.getOffersListEntity: ' + exceptionMessage);
            ole.errorMessage = exceptionMessage;
            return ole;
        }

        return ole;
    }

    public class OfferEntity {
        @AuraEnabled public List<alphaHelperCls.PicklistItem> repaymentFrequencyOptions;
        @AuraEnabled public Offer__c offer;

        public OfferEntity(Offer__c pOffer, List<alphaHelperCls.PicklistItem> pRepaymentFrequencyOptions) {
            offer = pOffer;
            repaymentFrequencyOptions = pRepaymentFrequencyOptions;
        }
    }

    public class OffersListEntity {
        @AuraEnabled public Boolean hasError;
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = !String.isEmpty(errorMessage);
            }
        }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> feeHandlingOptions;
        @AuraEnabled public List<OfferEntity> offerEntities;

        public OffersListEntity() {
            hasError = false;
            offerEntities = new List<OfferEntity>();
        }
    }

}