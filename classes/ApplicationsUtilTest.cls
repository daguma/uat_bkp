@isTest public class ApplicationsUtilTest {

    @isTest static void validates_partner_credit_karma() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Lead_Sub_Source__c = 'Credit Karma';
        app.Status__c = 'Credit Qualified';
        update app;

        ApplicationsUtil appUtil = new ApplicationsUtil(app);
        String result = appUtil.ValidatePartnerForApplication(); 
		System.assertEquals('Please be aware that you cannot contact this customer in any way unless there is already an offer accepted by the customer with us.', result);
        
    }

    @isTest static void gets_read_only() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Lead_Sub_Source__c = 'Credit Karma';
        app.Status__c = 'Credit Qualified';
        update app;

        ApplicationsUtil appUtil = new ApplicationsUtil(app);
        String result = appUtil.getReadOnlyClass(UserInfo.getUserId());

        System.assertEquals('', result);
    }

    @isTest static void is_declined_by_hard_pull() {

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Declined (or Unqualified)';
        app.Reason_of_Opportunity_Status__c = 'Did not pass disqualifiers at Hard Pull';
        update app;

        ApplicationsUtil appUtil = new ApplicationsUtil(app);
        Boolean result = appUtil.isDeclineByHardPull();

        System.assertEquals(true, result);
    }

    @isTest static void inserts_note() {

        Integer notesBefore = [SELECT COUNT() FROM Note];

        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Declined (or Unqualified)';
        app.Reason_of_Opportunity_Status__c = 'Did not pass disqualifiers at Hard Pull';
        update app;

        ApplicationsUtil appUtil = new ApplicationsUtil(app);
        appUtil.InsertNote(app.id, 'Test', 'Test');

        Integer notesAfter = [SELECT COUNT() FROM Note];

        System.assert(notesAfter > notesBefore);
    }

    @isTest static void is_default_deployment_app() {

        Opportunity app = LibraryTest.createApplicationTH();

        Test.setCreatedDate(app.Id, Date.newInstance(2017, 01, 01));
        update app;

        Opportunity newApp = [SELECT Id, Contact__r.Employment_Start_Date__c, CreatedDate,
                                           Contact__r.Employment_Type__c, Employment_Defaulted_Value__c
                                           FROM Opportunity WHERE Id = :app.Id];
        newApp.Contact__r.Employment_Start_Date__c = Date.newInstance(2016, 01, 01);
        newApp.Employment_Defaulted_Value__c = false;
        update newApp;

        ApplicationsUtil appUtil = new ApplicationsUtil(newApp);
        Boolean result = appUtil.isDefaultEmploymentApp();
    }
}