global class BatchFinwiseDailyInterestReporting implements Database.Batchable<sObject>,Schedulable, Database.AllowsCallouts{

    TimeZone tZone = UserInfo.getTimeZone();
    FinwiseDetails__c FinwiseParams = FinwiseDetails__c.getInstance(); 
    public date cutOffdate = null;       
    
   public void execute(SchedulableContext sc) { 
       cutOffdate = system.today() + integer.ValueOf(FinwiseParams.No_of_Days_to_Add_Daily_Interest_Batch__c);       
       execute(sc, cutOffDate);    
    }

     public void execute(SchedulableContext sc, Date pDate) { 
       cutOffdate = pDate; 
       BatchFinwiseDailyInterestReporting batchapex = new BatchFinwiseDailyInterestReporting();
       id batchprocessid = Database.executebatch(batchapex, integer.valueOf(FinwiseParams.Daily_Interest_Batch_Size__c));       
    }
   
    global Database.queryLocator start(Database.BatchableContext BC){
       if (cutOffdate == null) cutOffdate = system.today() + integer.ValueOf(FinwiseParams.No_of_Days_to_Add_Daily_Interest_Batch__c);       
       
       string query = 'select id, Opportunity__r.name, loan__Interest_Accrued_Not_Due__c, Loan__Loan_Balance__c  ';
              query += ',loan__Contact__r.Customer_Number__c, loan__Loan_Status__c,loan__Disbursed_Amount__c, loan__Principal_Remaining__c,'; 
              query += 'loan__Principal_Paid__c, loan__Fees_Paid__c, loan__Interest_Rate__c from loan__Loan_Account__c where loan__Next_Due_Generation_Date__c =: cutOffdate AND Opportunity__r.Finwise_Approved__c = true';
     
       return database.getQueryLocator(query);
    }    
    
    global void execute(Database.BatchableContext BC, List<loan__Loan_Account__c> scope){          
        list<logging__c> logsList = new list<logging__c>();
        string reqBody;
        try{
        system.debug('======scope=================='+scope);
        list<finwiseDailyInterestResponse> finwiseDailyInterestResponseList =  new list<finwiseDailyInterestResponse>();
        Map<string,string> apNameContractMap = new Map<string,string>();
        
      
        JSONGenerator generator = JSON.createGenerator(false);      
       generator.writeStartArray();  
      
       for(loan__Loan_Account__c loanAcnt: scope){                       
            apNameContractMap.put(loanAcnt.Opportunity__r.name,loanAcnt.id);
                                                                
            generator.writeStartObject();                                  
                generator.writeStringField('LoanNumber', loanAcnt.Opportunity__r.name);                                
                              
             generator.writeFieldName('DailyInterestTransactions');
             generator.writeStartArray(); 
                 generator.writeStartObject(); 
                 generator.writeNumberField('Id',0);  
                 generator.writeNumberField('LoanId',0);                 
                 DateTime tdt = System.today();                 
                 generator.writeStringField('CreationDate','\\/Date('+tdt.getTime()+tZone.getOffset(tdt)+')\\/');
                 generator.writeNumberField('Balance',(loanAcnt.Loan__Loan_Balance__c  <> null)?loanAcnt.Loan__Loan_Balance__c .setScale(2):0.00);  
                 generator.writeNumberField('InterestAccrued',(loanAcnt.loan__Interest_Accrued_Not_Due__c <> null)?loanAcnt.loan__Interest_Accrued_Not_Due__c.setScale(2):0.00); 
                 generator.writeEndObject();   
             generator.writeEndArray(); 
                              
             generator.writeFieldName('Service');        
                generator.writeStartObject();                   
                    generator.writeNumberField('LoanId',0);
                    generator.writeBooleanField('ArePaymentsActive',(loanAcnt.loan__Loan_Status__c.contains('Active')));                                  
                    generator.writeNumberField('AmountDisbursed',(loanAcnt.loan__Disbursed_Amount__c <> null)?loanAcnt.loan__Disbursed_Amount__c.setScale(2):0.00);
                    generator.writeNumberField('PrincipalRemaining',(loanAcnt.loan__Principal_Remaining__c <> null)?loanAcnt.loan__Principal_Remaining__c.setScale(2):0.00);  
                    generator.writeNumberField('PrincipalPaid',(loanAcnt.loan__Principal_Paid__c <> null)?loanAcnt.loan__Principal_Paid__c.setScale(2):0.00);
                    generator.writeNumberField('FeesPaid',(loanAcnt.loan__Fees_Paid__c <> null)?loanAcnt.loan__Fees_Paid__c.setScale(2):0.00);
                    generator.writeNumberField('InterestPaid',(loanAcnt.loan__Interest_Rate__c <> null)?loanAcnt.loan__Interest_Rate__c.setScale(2):0.00);                                               
                generator.writeEndObject();
              generator.writeEndObject();                                                                        
           }  
          
            generator.writeEndArray();           
                         
            reqBody = generator.getAsString(); 
            system.debug('============reqBody ====='+reqBody );
            reqBody = reqBody.unescapeJava();             
            HttpRequest req = new HttpRequest();          
            string Url = FinwiseParams.Finwise_Base_URL__c+'/loanservicing/'+FinwiseParams.Finwise_product_Key__c+'/loans';
            req.setEndpoint(url);
            req.setMethod('POST'); 
            req.SetBody(reqBody);  
            req.setHeader('Content-Type', 'application/json');                     
            Http http = new Http();
            HttpResponse response;
            
            if(!Test.isRunningTest()){
              response = http.send(req);
                
            }else{
              response = new HTTPResponse();                    
              response.SetBody(TestHelper.createDomDocNew().toXmlString());               
            }   
            system.debug('============response====='+response);
            system.debug('============body====='+response.getbody());
            string RespOLog = 'Response: '+response+'\n Response Body: '+response.getBody();
             
           if(response.getStatusCode() == 200){ 
                JSONParser parser = JSON.createParser(response.getBody()); 
                finwiseDailyInterestResponseList = (list<finwiseDailyInterestResponse>) parser.readValueAs(list<finwiseDailyInterestResponse>.class);                            
           }else                                       
                logsList.add(CustomerPortalWSDLUtil.getLogging('Finwise DailyInterest Batch Error', null,reqBody, RespOLog));                   
           
           
           for(finwiseDailyInterestResponse batchResp : finwiseDailyInterestResponseList){
               if(batchResp.Status <> 'Success')
                   logsList.add(CustomerPortalWSDLUtil.getLogging('Finwise DailyInterest Reporting Unsuccessful for '+batchResp.RecordKey, apNameContractMap.get(batchResp.RecordKey), reqBody, String.join(batchResp.Messages,', ')));                   
           }  
                             
       }catch(Exception exc){ 
           system.debug('===error=========='+exc.getmessage() + ' at line no '+exc.getlineNumber());                            
           string ExcpNoteBody = 'Finwise Service Processing Exception:  '+exc+' at Line No. '+exc.getLineNumber();                  
           logsList.add(CustomerPortalWSDLUtil.getLogging('Finwise DailyInterest Processing Exception', null,reqBody, ExcpNoteBody));                            
           WebToSFDC.notifyDev('BatchFinwiseDailyInterestReporting Exception',  exc.getMessage() + ' line ' + (exc.getLineNumber()) + '\n' + exc.getStackTraceString());            
       }
       finally{
         system.debug('=======logsList==========='+logsList);
           if(logsList.size() > 0)insert logsList;       
       }
   
   
    }
    
    global void finish(Database.BatchableContext BC){            
       WebToSFDC.notifyDev('BatchFinwiseDailyInterestReporting Batch Completed', 'Finwise Batch for daily interest Reporting is completed for Today. Please check loggings for any failures.');        
    }
    
    
    
  public class finwiseDailyInterestResponse{        
        string[] Messages;
        string RecordKey, Status;                 
    } 

}