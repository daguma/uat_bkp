public with sharing class GDSGenericCallProcess {
    
    private static final String GENERIC_GDS_CALL_URL = '/v101/gds/generic-gds-call/';
    private static final String SEQUENCE_ID_DECISION_LOGIC = 'Decision Logic';
    private static final String PRODUCTION_ORG_URL = 'https://na48.salesforce.com/services/Soap/c/40.0/0DF0B0000001EuX/00DU0000000LpAk';
    private static final String PRODUCTION_ORG_ID = '00DU0000000LpAk';
    private static GDSGenericObjectAttributes gdsGenericObjectAttributes = new GDSGenericObjectAttributes();
    
    public GDSGenericCallProcess() {}
    
    
    public static String get_DecisionLogicGDSParameters(String entityId) {
        
        return DecisionLogicGDSRequestParameter.get_JSONDecisionLogicGDSRequest(new DecisionLogicGDSRequestParameter(GDSGenericCallProcess.gdsRequestParameterInitialize(entityId))); 
        
    }
    
    public static GDSGenericObjectAttributes.GDSGenericCallResponse get_GDSGenericCallResult(String jsonInputData,String entityId,String sequenceId) {
        
        String jsonResponse;
        if (ProxyApiBRMSUtil.ValidateBeforeCallApi() && jsonInputData != null) {
            jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GENERIC_GDS_CALL_URL, 'input-data=' + EncodingUtil.urlEncode(jsonInputData, 'UTF-8') + '&opportunity-id=' + entityId + '&sequence-id=' + sequenceId);
        }
        
        if (jsonResponse != null){
            return (GDSGenericObjectAttributes.GDSGenericCallResponse) System.JSON.deserialize(jsonResponse, GDSGenericObjectAttributes.GDSGenericCallResponse.class);
        } 
        
        return  null;
    } 
    
    public static GDSRequestParameter gdsRequestParameterInitialize(String entityId) {
        
        GDSRequestParameter gdsRequestParameter = new GDSRequestParameter();
        GDS_Configuration__c setting = GDS_Configuration__c.getOrgDefaults();
        BRMS_Sequence_Ids__c sequenceId = BRMS_Sequence_Ids__c.getInstance(SEQUENCE_ID_DECISION_LOGIC);
        
        if (sequenceId != null) gdsRequestParameter.sequenceId = sequenceId.Sequence_Id__c;
        
        gdsRequestParameter.apiSessionId = (setting != null) ? setting.SF_Token__c : 'token'; 
        gdsRequestParameter.organizationId = (setting != null) ? setting.Org_Id__c : PRODUCTION_ORG_ID;
        gdsRequestParameter.organizationURL = (setting != null) ? setting.Org_Native_API_URL__c + '/' + setting.Org_Id__c : PRODUCTION_ORG_URL;
        
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        gdsRequestParameter.sessionId = h.SubString(0, 8) + '-' + h.SubString(8, 12) + '-' + h.SubString(12, 16) + '-' + h.SubString(16, 20) + '-' + h.substring(20);
 
        Opportunity app = CreditReport.getOpportunity(entityId); 
        
        if (app != null ) {
            gdsRequestParameter.applicationId = entityId;
        }
        
        gdsRequestParameter.contactId =  app.Contact__c; 	
        
        return gdsRequestParameter;   
    }
}