@isTest(seeAllData = true)
public class TestForCustomSpread
{
    
    testMethod static void checkLPTSpread()
    {
        loan.GlobalLoanUtilFacade GLUF = new loan.GlobalLoanUtilFacade();
        Date systemDate = GLUF.getCurrentSystemDate();
        List<loan__Payment_Mode__c> PM = [select id from loan__Payment_Mode__c where name  = 'cash'];
        List<loan__Loan_Account__c> LA = [Select id from loan__Loan_Account__c where loan__Loan_Status__c like 'Active%' LIMIT 1];
        loan__Loan_Payment_Transaction__c LPT = new loan__Loan_Payment_Transaction__c();
        LPT.loan__Loan_Account__c = LA[0].id;
        LPT.loan__Transaction_Date__c = systemDate;
        LPT.loan__Transaction_Amount__c = 100;
        LPT.loan__Payment_Mode__c = PM[0].id;
        insert LPT;
    }
}