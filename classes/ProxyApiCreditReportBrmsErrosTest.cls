@isTest 
public class ProxyApiCreditReportBrmsErrosTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ProxyApiCreditReportBrmsErros err = new ProxyApiCreditReportBrmsErros();
        Test.stopTest();
        
        System.assertEquals(null,err.errorMessage);
        System.assertEquals(null,err.errorDetails);
    }
    static testmethod void validate_assign() {
        Test.startTest();
        ProxyApiCreditReportBrmsErros err = new ProxyApiCreditReportBrmsErros();
        Test.stopTest();
        
        err.errorMessage = 'salesforce communication error';
        err.errorDetails = 'No Record Found';
        
        System.assertEquals('salesforce communication error',err.errorMessage);
        System.assertEquals('No Record Found',err.errorDetails);
    }
     
}