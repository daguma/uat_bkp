public with sharing class ReEligibilityAlertforBorrowingBase {
	
    public static List <loan__Loan_Account__c> contractsUpdate = new  List <loan__Loan_Account__c>();
	
    @invocableMethod
	public static void checkEligibility(List<String> contractList) {
		for (String contractId : contractList) {	
			evaluateFields(contractId);
		}
        if(contractsUpdate.size()>0){
            sendEmail(contractsUpdate);
            update contractsUpdate;
        }
	
	}

	private static void  evaluateFields(String contractId){
		List<loan__Loan_account_Due_Details__c> paymentList = [SELECT Id,
																 loan__Due_Date__c, 
																 CreatedDate,
																 loan__Payment_Date__c,
																 loan__Payment_Satisfied__c
														  FROM  loan__Loan_account_Due_Details__c
														  WHERE loan__Loan_Account__c =: contractId 
                                                               AND loan__DD_Primary_Flag__c = true 
														  ORDER BY CreatedDate DESC 
														  LIMIT 3];
	     loan__Loan_Account__c contract = [SELECT Id,
		 										 Name,
	     										 First_Payment_Ineligibility_Cleared__c
	     								  FROM loan__Loan_Account__c
	     								  WHERE Id =: contractId];

		if (paymentList.size() == 3 && !contract.First_Payment_Ineligibility_Cleared__c){ 	
			boolean eligibility = true;
			for(loan__Loan_account_Due_Details__c payment : paymentList){
                
				if((payment.loan__Payment_Date__c > payment.loan__Due_Date__c) || !payment.loan__Payment_Satisfied__c){
					eligibility = false;
                    break;	
				}
			}
            if(eligibility){	
                contract.First_Payment_Ineligibility_Cleared__c = eligibility;
                contract.First_Payment_Ineligibility_Cleared_Date__c = Date.today();
				contractsUpdate.add(contract);    
            }  
		}
	}
	public static void sendEmail(List<loan__Loan_Account__c> contracts){
		string subject ='Contracts No Longer Ineligible due to First Payment Missed';
        string body = 'These contracts are no longer ineligible due to First Payment Missed\n ';
		for(loan__Loan_Account__c c : contracts){
			body += c.Name + '\n';
		}
        
        List <GroupMember> dataEntries = [SELECT Id, UserOrGroupId, GroupId FROM GroupMember WHERE group.Name='Capital Markets'];
        Set <Id> userIds = new Set <Id> ();
    
        for (GroupMember member: dataEntries) {
            userIds.add(member.UserOrGroupId);
        }
                
        for (Id userId: userIds) { 
             salesForceUtil.EmailByUserId(subject, body, userId);
        } 
	}

}