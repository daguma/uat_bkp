@isTest public class CreditReportSegmentsUtilTest {

    @isTest static void pair_values() {
        String key = 'key';
        String value = 'value';
        String type = 'type';
        String displayText = 'display';

        CreditReportSegmentsUtil.PairValues pv2 = new CreditReportSegmentsUtil.PairValues(key, displayText);
        System.assertEquals(key, pv2.key);
        System.assertEquals(displayText, pv2.displayText);
        System.assertEquals('TEXT', pv2.type);

        Boolean result = pv2.isText;
        System.assertEquals(true, result);
        pv2.type = type;
        result = pv2.isText;
        System.assertEquals(false, result);

        result = pv2.isAnchor;
        System.assertEquals(false, result);
        pv2.type = 'ANCHOR';
        result = pv2.isAnchor;
        System.assertEquals(true, result);

        CreditReportSegmentsUtil.PairValues pv1 = new CreditReportSegmentsUtil.PairValues(key, value, displayText, type);
        System.assertEquals(key, pv1.key);
        System.assertEquals(value, pv1.value);
        System.assertEquals(displayText, pv1.displayText);
        System.assertEquals(type, pv1.type);

    }

    @isTest static void segment() {
        CreditReportSegmentsUtil.Segment s = new CreditReportSegmentsUtil.Segment();
        System.assertNotEquals(null, s.headersList);
        System.assertNotEquals(null, s.contentList);

    }

    @isTest static void segment_content() {
        CreditReportSegmentsUtil.SegmentContent sc = new CreditReportSegmentsUtil.SegmentContent();
        System.assertNotEquals(null, sc.valuesList);
        System.assertNotEquals(null, sc.subSegmentList);
        System.assertNotEquals(null, sc.subSegmentsPrefix);
        System.assertEquals('', sc.subSegmentsLink);

        sc.subSegmentsLink = 'link';
        System.assertEquals('link', sc.subSegmentsLink);

        String result = sc.subSegmentDetails;
        String html = '<table style="width:100%">';
        html += '</table>';
        String expected = EncodingUtil.base64Encode(Blob.valueOf(html));
        System.assertEquals(expected, result);

        List<String> test = new List<String> {'one', 'two'};
        sc.valuesList = test;
        System.assertEquals(test, sc.valuesList);

        sc.subSegmentsPrefix = test;
        System.assertEquals(test, sc.subSegmentsPrefix);

    }

    @isTest static void sub_segment() {
        CreditReportSegmentsUtil.SubSegment ss = new CreditReportSegmentsUtil.SubSegment();
        System.assertNotEquals(null, ss.headersList);
        System.assertNotEquals(null, ss.contentList);

        List<String> test = new List<String> {'one', 'two'};
        ss.headersList = test;
        System.assertEquals(test, ss.headersList);

    }

    @isTest static void fraud_indicators() {
        CreditReportSegmentsUtil.Segment segment = new CreditReportSegmentsUtil.Segment();
        List<String> values = new List<String>();
        values.add('03');
        values.add('27');

        CreditReportSegmentsUtil.SegmentContent content = new CreditReportSegmentsUtil.SegmentContent();
        content.valuesList = values;

        List<CreditReportSegmentsUtil.SegmentContent> lcontent = new List<CreditReportSegmentsUtil.SegmentContent>();
        lcontent.add(content);

        segment.headersList.add('Indicator 1');
        segment.headersList.add('Indicator 2');
        segment.contentList = lcontent;

        String fraudIndicatorsString = CreditReportSegmentsUtil.extractFraudIndicatorsString(segment);

        CreditReportSegmentsUtil.FraudIndicators fi = new CreditReportSegmentsUtil.FraudIndicators(fraudIndicatorsString);

        System.assertEquals('No Match', fi.indicator03);
        System.assertEquals('Yes', fi.indicator27);

        List<String> values2 = new List<String>();
        values2.add('01');
        values2.add('02');

        content.valuesList = values2;
        lcontent.clear();
        lcontent.add(content);

        segment.contentList = lcontent;

        fraudIndicatorsString = CreditReportSegmentsUtil.extractFraudIndicatorsString(segment);

        fi = new CreditReportSegmentsUtil.FraudIndicators(fraudIndicatorsString);

        System.assertEquals('Match', fi.indicator03);
        System.assertEquals('No', fi.indicator27);

    }

    @isTest static void extract_main_information() {
        List<CreditReportSegmentsUtil.PairValues> result = new List<CreditReportSegmentsUtil.PairValues>();
        ProxyApiCreditReportEntity cr = LibraryTest.fakeCreditReportEntity();
        result = CreditReportSegmentsUtil.extractMainInformation(cr);
        System.assertNotEquals(result, null);
    }

    @isTest static void extract_main_information2() {

        opportunity app = LibraryTest.createApplicationTH();

        List<CreditReportSegmentsUtil.PairValues> result = new List<CreditReportSegmentsUtil.PairValues>();
        ProxyApiCreditReportEntity cr = LibraryTest.fakeCreditReportEntity(app.Id);
        result = CreditReportSegmentsUtil.extractMainInformation(cr);
        System.assertNotEquals(result, null);
    }

    @isTest static void extract_liabilities() {
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentEntityEX();
        CreditReportSegmentsUtil.Segment result = CreditReportSegmentsUtil.extractLiabilities(segmentList, 'EX');
        System.assertNotEquals(result, null);
        segmentList = LibraryTest.fakeCreditReportSegmentEntityTU();
        result = CreditReportSegmentsUtil.extractLiabilities(segmentList, 'TU');
        System.assertNotEquals(result, null);
    }

    @isTest static void extract_scores() {
        CreditReportSegmentsUtil.Segment result = new CreditReportSegmentsUtil.Segment();
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentEntity();
        result = CreditReportSegmentsUtil.extractScores(segmentList, 'TU');
        System.assertNotEquals(result, null);
        result = CreditReportSegmentsUtil.extractScores(segmentList, 'EX');
        System.assertNotEquals(result, null);
    }

    @isTest static void extract_inquiries() {
        CreditReportSegmentsUtil.Segment result = new CreditReportSegmentsUtil.Segment();
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentEntity();
        result = CreditReportSegmentsUtil.extractInquiries(segmentList, 'TU');
        System.assertNotEquals(result, null);
        result = CreditReportSegmentsUtil.extractInquiries(segmentList, 'EX');
        System.assertNotEquals(result, null);
    }

    @isTest static void extract_summaries() {
        CreditReportSegmentsUtil.Segment result = new CreditReportSegmentsUtil.Segment();
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentEntity();
        result = CreditReportSegmentsUtil.extractSummaries(segmentList, 'EX');
        System.assertNotEquals(result, null);
        result = CreditReportSegmentsUtil.extractSummaries(segmentList, 'TU');
        System.assertNotEquals(result, null);
    }

    @isTest static void extract_fraud_indicators() {
        List<ProxyApiCreditReportSegmentEntity> segmentList = LibraryTest.fakeCreditReportSegmentEntity();
        CreditReportSegmentsUtil.FraudIndicators result = CreditReportSegmentsUtil.extractFraudIndicators(segmentList);
        System.assertNotEquals(result, null);
    }
    /* BRMS Change */
    @isTest static void evaluate_fraud_indicators_thr_brms() {
        list<ProxyApiCreditReportRuleEntity> crList = LibraryTest.fakeBrmsFraudRuleEntity();
        CreditReportSegmentsUtil.FraudIndicators result = CreditReportSegmentsUtil.extractFraudIndicatorsThrBRMS(crList);
    }
    /* BRMS Change */
    
}