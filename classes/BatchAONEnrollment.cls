global class BatchAONEnrollment  implements Database.Batchable<sObject>, Schedulable,  Database.AllowsCallouts {

  private Static Final String ENROLLMENT = 'Enrollment';
  private Static Final String AON_FEE = 'AON Fee';
  private Static Final String OPTIONAL = 'Optional ';

  public BatchAONEnrollment() {
  }

  public void execute(SchedulableContext sc) {
    BatchAONEnrollment  batchapex = new BatchAONEnrollment();
    id batchprocessid = Database.executebatch(batchapex, 1);
    system.debug('Process ID: ' + batchprocessid);
  }

  global Database.QueryLocator start(Database.BatchableContext BC) {
    String query = 'SELECT Contract__c, Id, Enrollment_Date__c, Company__c, Product_Code__c, IsReinstallment__c, Sale_Type__c, Status_Code__c, IsProcessed__c, ServiceError__c, ' +
                   'RetryCount__c FROM loan_AON_Information__c WHERE IsProcessed__c = false AND IsCancellation__c = false AND IsAIMCancellation__c = false AND RetryCount__c <= 3';
    return Database.getQueryLocator(query);
  }

  global void execute(Database.BatchableContext BC, List<loan_AON_Information__c > scope) {

    String jsonRequest;
    String jsonResponse;
    Contact enrollmentCnt;
    Boolean isBundle = false;
    loan__Loan_Account__c enrollmentContract;
    AONObjectsParameters params;
    AONObjectsParameters.AONEnrollmentInputData aonEnrollmentInputData;
    String productFrequency;
    for (loan_AON_Information__c CL : scope) {
      enrollmentContract = [SELECT Id, Name, AON_Enrollment__c, loan__Contact__c, loan__Pmt_Amt_Cur__c, loan__Next_Installment_Date__c, AON_Fee__c, loan__Pay_Off_Amount_As_Of_Today__c,
                            opportunity__r.Payment_Frequency_Masked__c, opportunity__r.StrategyType__c, /*opportunity__r.AON_Fees_Total__c,*/ opportunity__r.AON_FeesTotal__c, 
                            opportunity__r.Term__c, Opportunity__r.AON_Military__c, Opportunity__r.Contact__r.Military__c 
                            FROM loan__Loan_Account__c WHERE Id = : CL.Contract__c];
      enrollmentCnt = [SELECT FirstName, LastName, ints__Middle_Name__c, MailingStreet, MailingCity, MailingState, MailingPostalCode, Email, Phone, Phone_Clean__c  
                       FROM Contact WHERE Id = : enrollmentContract.loan__Contact__c];
      isBundle = OfferCtrlUtil.isAonBundle(enrollmentContract.opportunity__r);
      if (CL.Product_Code__c == null) {
        //productFrequency = OfferCtrlUtil.isAonBundle(enrollmentContract.opportunity__r)?enrollmentContract.opportunity__r.Payment_Frequency_Masked__c : OPTIONAL +  enrollmentContract.opportunity__r.Payment_Frequency_Masked__c;
        productFrequency = isBundle ? enrollmentContract.opportunity__r.Payment_Frequency_Masked__c : OPTIONAL +  enrollmentContract.opportunity__r.Payment_Frequency_Masked__c;
        AON_Product_Codes__c aonProduct = AON_Product_Codes__c.getValues(productFrequency.toUpperCase());
        CL.Product_Code__c = String.valueOf(aonProduct.ProductCode__c);
      }
      aonEnrollmentInputData = new AONObjectsParameters.AONEnrollmentInputData(enrollmentCnt, enrollmentContract.Name, CL.Product_Code__c);

      jsonRequest  = new AONObjectsParameters.AONEnrollmentInputData().get_JSON(aonEnrollmentInputData);

      jsonResponse =  ProxyApiAON.enrollment(jsonRequest);

      AONObjectsParameters.AonResponse aonResponse = new AONObjectsParameters.AonResponse(jsonResponse);

      if (aonResponse != null) {
          
        if (aonResponse.statusCode != 201 && aonResponse.statusCode != 200) {
          if (String.isNotEmpty(aonResponse.errorMessage))
            CL.ServiceError__c = (aonResponse.errorMessage.length() > 255) ? aonResponse.errorMessage.substring(0, 254) : aonResponse.errorMessage;
          CL.RetryCount__c += 1;
          update CL;
        } else {

          params = new AONObjectsParameters();
          CL.Enrollment_Date__c = params.convertTimestamp(aonEnrollmentInputData.effectiveDate);
          CL.Company__c = aonEnrollmentInputData.company;
          CL.IsReinstallment__c = Boolean.valueOf(aonEnrollmentInputData.isReinstatement);
          CL.Sale_Type__c = aonEnrollmentInputData.saleType;
          CL.Status_Code__c = aonEnrollmentInputData.statusCode;
          CL.IsProcessed__c = true;
          CL.RetryCount__c = 0;
          update CL;

          //enrollmentContract.AON_Enrollment__c = true;
          //if (enrollmentContract.AON_Fee__c == null && (enrollmentContract.opportunity__r.StrategyType__c == '2' || enrollmentContract.opportunity__r.StrategyType__c == '3'))
          if (enrollmentContract.AON_Fee__c == null && isBundle)
            enrollmentContract.AON_Fee__c = 0;
          update enrollmentContract;
          // insert a record into the billing object for the first time call to that service (a different batch will process it)
          AON_Fee__c aonFee = AON_Fee__c.getValues(AON_FEE);
          loan_AON_Billing__c billing = new loan_AON_Billing__c();
          billing.loan_AON_Information__c = CL.Id;
          //billing.Billing_Date__c = params.convertTimestamp(aonEnrollmentInputData.effectiveDate);
          billing.Billing_Date__c = System.now();
          billing.Company__c = CL.Company__c;
          billing.Contract__c = CL.Contract__c;
          billing.Loan_Balance__c = enrollmentContract.loan__Pay_Off_Amount_As_Of_Today__c.setScale(2, RoundingMode.HALF_UP);
          billing.Minimum_Monthly_Payment__c = enrollmentContract.loan__Pmt_Amt_Cur__c.setScale(2, RoundingMode.HALF_UP);
          billing.Payment_Due_Date__c = enrollmentContract.loan__Next_Installment_Date__c;
          billing.Product_Code__c = CL.Product_Code__c;
          //billing.Product_Fee__c = (enrollmentContract.AON_Fee__c == 0) ? ((aonFee.Fee__c / 100) * enrollmentContract.loan__Pmt_Amt_Cur__c).setScale(2, RoundingMode.HALF_UP) : (enrollmentContract.opportunity__r.AON_Fees_Total__c / enrollmentContract.opportunity__r.Term__c).setScale(2, RoundingMode.HALF_UP);
          billing.Product_Fee__c = (enrollmentContract.AON_Fee__c == 0) ? ((aonFee.Fee__c / 100) * enrollmentContract.loan__Pmt_Amt_Cur__c).setScale(2, RoundingMode.HALF_UP) : 
          (enrollmentContract.opportunity__r.AON_FeesTotal__c / enrollmentContract.opportunity__r.Term__c).setScale(2, RoundingMode.HALF_UP);
          billing.IsEnrollment__c = true;
          billing.IsProcessed__c = false;
          insert billing;
        }
      }

      AONProcesses.setContactLoggging((aonResponse.callType != null) ? aonResponse.callType : ENROLLMENT,
                                      jsonRequest, jsonResponse, CL.Contract__c);

      enrollmentCnt = null;
      enrollmentContract = null;
      aonEnrollmentInputData = null;
      jsonRequest = null;
      jsonResponse = null;
    }
  }

  global void finish(Database.BatchableContext BC) {
  }
}