public with sharing class GenReportCtrl {

    public String reportText {get; set;}
    public String errorMessage {get; set;}
    public Boolean renderAsPDF {get; set;}
    public String reportName {get; set;}

    /**
     * Generic Report controller
     * @return [description]
     */
    public GenReportCtrl() {
        String type = ApexPages.currentPage().getParameters().get('t');

        this.renderAsPDF = (!String.isEmpty(type) && type == 'PDF') ? true : false;

        reportName = String.valueOf(ApexPages.currentPage().getParameters().get('n'));

        if (String.isEmpty(reportName)) {
            this.errorMessage =  'Invalid Parameters - Missing Name';
        }
    }
}