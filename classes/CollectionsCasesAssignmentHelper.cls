global class CollectionsCasesAssignmentHelper {

    public List<String> dmc1ExclusionsList;

    public CollectionsCasesAssignmentHelper(){
        try{
            List<String> myContractsList = new List<String>();
            StaticResource doc = [SELECT id, body FROM StaticResource WHERE name = 'dmc1ExclusionsList' LIMIT 1];
            if(doc != null){
                String myContracts = doc.Body.toString();
                if(!String.isEmpty(myContracts)){
                    myContractsList = myContracts.split(',');
                }
            }
            this.dmc1ExclusionsList = myContractsList;
        }catch(Exception e){
            system.debug('CollectionsCasesAssignmentHelper exception: ' + e.getMessage());
            this.dmc1ExclusionsList = new List<String>();
        }
    }

    public Boolean ignoreContract(String contractId){
        return new Set<String>(this.dmc1ExclusionsList).contains(contractId);
    }
}