public class DealAutomaticUserGroup {

    public String groupId {get; set;}
    public String groupName {get; set;}
    public String userId {get; set;}
    public String userName {get; set;}
    public Boolean selected {get; set;}
    public Boolean isOnline {get; set;}

    public DealAutomaticUserGroup() {

    }

    public DealAutomaticUserGroup(String userId,
                                  String userName,
                                  String groupId,
                                  String groupName,
                                  Boolean selected,
                                  Boolean isOnline) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.userId = userId;
        this.userName = userName;
        this.selected = selected;
        this.isOnline = isOnline;
    }
}