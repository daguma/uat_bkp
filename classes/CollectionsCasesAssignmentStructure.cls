global class CollectionsCasesAssignmentStructure {

    public Map<String,CollectionsCasesAssignmentLBEntity> loadBalancedMap;
    public Map<String,Map<Id,Integer>> assignedPerGroupBeforeProcessMap; //This is persistent because the AuditReport includes it
    public Map<String,Integer> totalInflowCasesToAssignPerGroupMap; //This is persistent because the AuditReport includes it

    public CollectionsCasesAssignmentStructure(Map<String, CollectionsCasesAssignmentTeam> teamsMap){

        this.loadBalancedMap = new Map<String, CollectionsCasesAssignmentLBEntity>();
        this.assignedPerGroupBeforeProcessMap = new Map<String,Map<Id,Integer>>();
        this.totalInflowCasesToAssignPerGroupMap = new Map<String,Integer>();

        Map<string,AggregateResult> currentAssignmentAggregateResultMap = new Map<string,AggregateResult>([ SELECT OwnerId Id, count(Id) myCount FROM Case WHERE RecordTypeId =: Label.RecordType_Collections_Id AND CL_Contract__c != null AND isClosed = false GROUP BY OwnerId]);
        Map<string,AggregateResult> inflowLoadAggregateResultMap = new Map<string,AggregateResult>([SELECT loadBalanceGroup__c Id, count(id) myCount FROM CollectionsCasesAssignment__c WHERE loadBalanceGroup__c != '' GROUP BY loadBalanceGroup__c]);

        Map<Id,Integer> currentAssignmentMap;
        for(CollectionsCasesAssignmentTeam team : teamsMap.values()){
            for(CollectionsCasesAssignmentUsersGroup usersGroup : team.usersGroupsMap.values()){
                currentAssignmentMap = new Map<Id,Integer>();
                for(Id userId : usersGroup.getAllGroupMembersIds()){
                    currentAssignmentMap.put(
                              userId
                            , currentAssignmentAggregateResultMap.get(userId) != null
                                    ? (Integer) currentAssignmentAggregateResultMap.get(userId).get('myCount')
                                    : 0
                    );
                }
                this.assignedPerGroupBeforeProcessMap.put(usersGroup.getGroupName(),currentAssignmentMap);
                this.totalInflowCasesToAssignPerGroupMap.put(
                        usersGroup.getGroupName()
                        ,inflowLoadAggregateResultMap.get(usersGroup.getGroupName()) != null
                                ? (Integer) inflowLoadAggregateResultMap.get(usersGroup.getGroupName()).get('myCount')
                                : 0
                );
                this.loadBalancedMap.put(
                          usersGroup.getGroupName()
                        , CollectionsCasesAssignmentLBAlgorithm.process(
                                  inflowLoadAggregateResultMap.get(usersGroup.getGroupName()) != null
                                        ? (Integer) inflowLoadAggregateResultMap.get(usersGroup.getGroupName()).get('myCount')
                                        : 0
                                , currentAssignmentMap
                        )
                );
            }
        }
    }

    public Id getNextAssigneeId(String groupName){
        if(String.isEmpty(groupName)) return null;
        return this.loadBalancedMap.get(groupName) != null ? this.loadBalancedMap.get(groupName).getNextAssigneeId() : null;
    }
}