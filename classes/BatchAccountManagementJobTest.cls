@isTest
private class BatchAccountManagementJobTest {

    @testSetup static void creates_required_objects(){
        Account_Management_Settings__c amSetting = new Account_Management_Settings__c();
        amSetting.Loan_Status__c = '\'Active - Good Standing\', \'Active - Bad Standing\', \'Active - Marked for Closure\','+
            ' \'Closed - Written Off\'';
        amSetting.ChargeOff_Reason__c = '\'Fraud\', \'Deceased\'';
        insert amSetting;
        
        BRMS_Sequence_Ids__c brmsAM_SeqIdsSettings = new BRMS_Sequence_Ids__c();
        brmsAM_SeqIdsSettings.Name = 'Account Management';
        brmsAM_SeqIdsSettings.Sequence_Id__c = '10';
        upsert brmsAM_SeqIdsSettings;
    }
    
    @isTest static void schedules_account_management_job(){
        Test.startTest();
        BatchAccountManagementJob AMJob = new BatchAccountManagementJob();
		String cron = '0 0 23 * * ?';
        system.schedule('Test Account Management', cron, AMJob);
        Test.stopTest();
    }
    
    @isTest static void runs_account_management_without_valid_contracts(){
        BatchAccountManagementJob job = new BatchAccountManagementJob();
		database.executebatch(job, 1);
    }
    
    @isTest static void runs_account_management_with_valid_contracts(){
        
        loan__loan_account__c clContract = LibraryTest.createContractTH();
        clContract.loan__loan_status__c = 'Active - Good Standing';
        update clContract;
        
        Opportunity opp = new Opportunity();
        opp.Id = clContract.Opportunity__c;
        opp.Account_Management_Pull_Date__c = Date.today().addDays(-40);
        opp.Lending_Account__c = clContract.Id;
        update opp;
        
        Test.startTest();
        BatchAccountManagementJob job = new BatchAccountManagementJob();
		database.executebatch(job, 1);
        Test.stopTest();
        
        System.assertEquals(Date.today(), [SELECT Account_Management_Pull_Date__c FROM Opportunity WHERE Id=:opp.Id].Account_Management_Pull_Date__c);
    }
    
    @isTest static void runs_account_management_for_deceased(){
        
        loan__loan_account__c clContract = LibraryTest.createContractTH();
        clContract.loan__loan_status__c = 'Active - Good Standing';
        clContract.Charged_Off_Reason__c = 'Deceased';
        update clContract;
        
        Opportunity opp = new Opportunity();
        opp.Id = clContract.Opportunity__c;
        opp.Account_Management_Pull_Date__c = Date.today().addDays(-40);
        opp.Lending_Account__c = clContract.Id;
        update opp;
        
        Test.startTest();
        BatchAccountManagementJob job = new BatchAccountManagementJob();
		database.executebatch(job, 1);
        Test.stopTest();
        
        System.assertNotEquals(Date.today(), [SELECT Account_Management_Pull_Date__c FROM Opportunity WHERE Id=:opp.Id].Account_Management_Pull_Date__c);
    }
    
}