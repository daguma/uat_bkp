@isTest
public class RefinanceCreditReviewRulesTest
{
  @isTest static void needs_credit_review()
    {
        loan__Loan_Account__c testContract = LibraryTest.createContractTH();
        testContract.loan__Principal_Remaining__c = 3000;
        testContract.loan__Fees_Remaining__c = 200;
        testContract.loan__Interest_Accrued_Not_Due__c = 100;
        update testContract;

        List<String> ids = new List<String>();
        List<Opportunity> apps = new List<Opportunity>();

        ids.add(testContract.id);
        
        EligibilityCheckPostSoftPull.checkEligibility(ids);
        
        Opportunity app = [SELECT Id, Status__c, Contact__c, Contact__r.Loan_Amount__c, Type, FICO__c FROM Opportunity LIMIT 1];
        
        app.FICO__c = '720';
        app.Type = 'New';
        app.Status__c = 'Credit Qualified';
        app.Lending_Account__c = testContract.Id;
        update app;
        
        Contact cc = LibraryTest.createContactTH();
        cc.Kyc_Mode__c = 'Test';
        update cc;
        
        LIST<loan_Refinance_Params__c> refiParams = [SELECT Id FROM loan_Refinance_Params__c
                                               WHERE Contract__c = : testContract.Id LIMIT 1];
        String rid = '';
        if (refiParams.size() == 0){
            loan_Refinance_Params__c refiParam = new loan_Refinance_Params__c();
            refiParam.Contract__c = testContract.Id;
            refiParam.FICO__c = 600;
            refiParam.Grade__c = 'C1';
            refiParam.New_FICO__c = 610;
            refiParam.New_Grade__c = 'B1';
            refiParam.Eligible_For_Refinance__c = true;
            refiParam.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-18);
            refiParam.System_Debug__c = '';
            refiParam.ValidatedIncome__c = 50000;
            insert refiParam;
            rid = refiParam.id;
        }

        app.Contact__c = cc.Id;
        app.Status__c = 'Documents In Review';
        update app;
        
        app = [SELECT Id, Status__c, Contact__c, Contact__r.Loan_Amount__c, Contact__r.MailingStreet, Contact__r.MailingCity, Contact__r.MailingState, 
                Type, FICO__c FROM Opportunity LIMIT 1];

        System.assertEquals('Credit Review', app.Status__c);

        Opportunity appRefi = new Opportunity();
        appRefi.Name = 'John Smith';
        appRefi.StageName = 'Underwriting';
        appRefi.CloseDate = System.today();
        appRefi.Contact__c = app.Contact__c;
        appRefi.Lending_Product__c = [Select Id from Lending_Product__c where Name = 'Sample Loan Product' limit 1].Id;
        appRefi.Product_Type__c = 'LOAN';
        appRefi.Amount = app.Contact__r.Loan_Amount__c;
        appRefi.Days_Convention__c = '30/360';
        appRefi.Fee__c = 0.04 * (app.Contact__r.Loan_Amount__c == null ? 0.0 : app.Contact__r.Loan_amount__c);
        appRefi.Interest_Calculation_Method__c = 'Declining Balance';
        appRefi.Type = 'Refinance';
        appRefi.FICO__c = '730';
        appRefi.RecordType = [Select ID from RecordType where DeveloperName like '%LMS%'
                          and SObjectType = : 'Opportunity'];
        appRefi.Status__c = 'Credit Qualified';
        appRefi.Estimated_Grand_Total__c = 205000;
        appRefi.Contract_Renewed__c = testContract.Id;
        insert appRefi;

        appRefi.Status__c = 'Documents In Review';
        update appRefi;
        
        System.debug(LoggingLevel.INFO, 'refinance app id: ' + appRefi.Id);

        apps.add(appRefi);
    
               loan_Refinance_Params__c refiParam = new loan_Refinance_Params__c(id = [select id from loan_Refinance_Params__c limit 1].id);
            refiParam.FICO__c = 600;
            refiParam.ValidatedIncome__c = 50000;
            upsert refiParam;
    
        
        
       RefinanceCreditReviewRules.needsCreditReview(apps);

       appRefi = [  SELECT   Id, 
                Lending_Product__c, 
                Product_Type__c,
                Status__c, 
                Contact__c, 
                Type, 
                FICO__c,
                Amount,
                Days_Convention__c,
                Fee__c,
                Interest_Calculation_Method__c
          FROM   Opportunity 
          WHERE Id =: appRefi.Id];  
        System.debug(' params created = ' + [select id from loan_Refinance_Params__c].size());

    System.debug(LoggingLevel.INFO, 'status: ' + appRefi.Status__c);
       System.assertEquals(appRefi.Status__c, 'Credit Review'); 
    }
}