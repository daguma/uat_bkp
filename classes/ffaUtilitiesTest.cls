@isTest private class ffaUtilitiesTest {

    @testSetup static void setups_data() {

        c2g__codaGeneralLedgerAccount__c testGLA = ffaTestUtilities.create_IS_GLA();

        c2g__codaCompany__c testCompany = null;
        Group newGroup = null;

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {

            newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            // Create Company Queue
            c2g.CODACompanyWebService.createQueue(testCompany.Id, 'USD', testCompany.Name);
            // Activate the Company
            c2g.CODAYearWebService.calculatePeriods(null); // Workaround to bug in company API's, safe to remain once fixed
            c2g.CODACompanyWebService.activateCompany(testCompany.Id, 'USD', testCompany.Name);
            // Assign the User to the Company
            c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c();
            userCompany.c2g__Company__c = testCompany.Id;
            userCompany.c2g__User__c = UserInfo.getUserId();
            insert userCompany;

            GroupMember newGroupMember = new GroupMember();
            newGroupMember.GroupId = newGroup.Id;
            newGroupMember.UserOrGroupId = thisUser.Id;
            insert newGroupMember;
        }

        // Create Year and Periods
        c2g__codaYear__c yr = new c2g__codaYear__c();
        yr.Name = String.valueOf(Date.today().year());
        yr.OwnerId = newGroup.Id;
        yr.c2g__NumberOfPeriods__c = 12;
        yr.c2g__OwnerCompany__c = testCompany.id;
        yr.c2g__AutomaticPeriodList__c = true;
        yr.c2g__StartDate__c = Date.valueOf(Date.today().year() + '-01-01 00:00:00');
        yr.c2g__EndDate__c = Date.valueOf(Date.today().year() + '-12-31 00:00:00');
        yr.c2g__PeriodCalculationBasis__c = 'Month End';
        insert yr;
        c2g.CODAYearWebService.calculatePeriods(yr.Id);
        // Create Accounting Currency?
        if (UserInfo.isMultiCurrencyOrganization()) {
            c2g__codaAccountingCurrency__c testCurrency = new c2g__codaAccountingCurrency__c();
            testCurrency.Name = 'USD';
            testCurrency.c2g__DecimalPlaces__c = 2;
            testCurrency.c2g__Home__c = true;
            testCurrency.c2g__Dual__c = true;
            insert testCurrency;
        }

        c2g__codaAccountingCurrency__c testcurr = [SELECT Id FROM c2g__codaAccountingCurrency__c WHERE c2g__ownerCompany__c = : testCompany.Id LIMIT 1];
        Account testAccount = ffaTestUtilities.createAccount('Test Account');

        c2g__codaJournal__c testJournal = ffaTestUtilities.createJNL(Date.today(), testGLA.id, 100, null, null, null, null, testCompany.id, newGroup.id);
    }

    @isTest static void gets_company() {

        List<c2g__codaCompany__c> companies = ffaUtilities.getCurrentCompanies();

        System.assertEquals([SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id, companies[0].id);
    }

    @isTest static void gets_company_in_group() {

        Id companyId = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;

        Map<Id, Id> result = ffaUtilities.getCompanyToFFAGroupIDMap();

        System.assertEquals([SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1].Id, result.get(companyId));
    }

    @isTest static void discards_journal() {

        List<Id> jnlIdList = new List<Id> {[SELECT Id FROM c2g__codaJournal__c ORDER BY CreatedDate DESC LIMIT 1].Id};
        String result = ffaUtilities.bulkDiscardJournal_WS(jnlIdList);

        System.assert(result.contains('Success'), result);
    }

    @isTest static void no_discard_journal() {

        Id companyId = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;

        List<Id> jnlIdList = new List<Id> {companyId};
        String result = ffaUtilities.bulkDiscardJournal_WS(jnlIdList);

        System.assertEquals('Attention: No Journals were found for discarding, please ensure you have selected at least 1 In Progress Journal Entry.', result);
    }

    @isTest static void creates_loan_capitalization_records() {

        loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
        paymentMode.Name = 'testName';
        insert paymentMode;

        loan__loan_Account__c testLoanAccount = LibraryTest.createContractTH();
        testLoanAccount.loan__Disbursal_Amount__c = 200;
        testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
        testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
        testLoanAccount.loan__Disbursed_Amount__c = 200;
        testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
        testLoanAccount.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
        update testLoanAccount;

        loan__Loan_Payment_Transaction__c pmt = new loan__Loan_Payment_Transaction__c();
        pmt.loan__Loan_Account__c = testLoanAccount.Id;
        pmt.Payment_Processing_Complete__c = false;
        pmt.loan__Transaction_Amount__c = 100.00;
        pmt.loan__Principal__c = 100.00;
        pmt.loan__Interest__c = 100.00;
        pmt.loan__Fees__c = 100.00;
        pmt.loan__excess__c = 100.00;
        pmt.loan__Transaction_Date__c = Date.today();
        pmt.loan__Skip_Validation__c = true;
        pmt.loan__Payment_Mode__c = paymentMode.Id;
        insert pmt;

        loan__Loan_Disbursal_Transaction__c loanDisbursal = ffaTestUtilities.createLoanDisbursal(testLoanAccount.id, paymentMode.id);

        Set<Id> loanDisbursalIds = new Set<Id>();
        loanDisbursalIds.add(loanDisbursal.id);

        Integer countBefore = [SELECT COUNT() FROM Loan_Capitalization__c];

        try {
            ffaUtilities.createLoanCapitalizationRecords(loanDisbursalIds);
        } catch (Exception e) {
            System.debug(e.getMessage());
        }

        System.assert([SELECT COUNT() FROM Loan_Capitalization__c] > countBefore, countBefore);
    }

}