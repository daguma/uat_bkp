@isTest
public class DecisionCtrlTest {
    
    @testSetup static void creates_settings(){
        Email_Age_Settings__c emAge = new Email_Age_Settings__c();
        emAge.API_Key__c = '3EB963CCFE0C46EFB5E08CBD945F9ED0';
        emAge.API_URL__c = 'https://test.emailage.com/emailagevalidator/';
        emAge.Auth_Token__c = 'C85CD3F6DF954E20BED7681864A5FE92';
        emAge.Call_Email_age__c = true;
        emAge.Expiry_Days__c = 30;
        emAge.Name = 'settings';
        insert emAge;
    }
    
    @isTest static void gets_decisioning_entity() {
        Opportunity opp = LibraryTest.createApplicationTH();
        
        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
       
        Proxy_Api__c proxy = LibraryTest.fakeSettings();
        insert proxy;
        DecisioningEntity decEntity = DecisionCtrl.getDecisioningEntity(opp.Id);
        
        System.assertNotEquals(null, decEntity);
    }
    
    @isTest static void fetch_hard_credit_pull() {
        Opportunity opp = LibraryTest.createApplicationTH();
        CreditReport.set(opp.id, LibraryTest.fakeBrmsRulesResponse(opp.Id, true, '3', true));
        DecisionCtrl.fetchNewHardCreditPull(opp.Id);
    }
    
    @isTest static void fetch_soft_credit_pull() {
        Opportunity opp = LibraryTest.createApplicationTH();
        
        Contact co = [Select Id, Email from Contact where Id =: opp.Contact__c];
        
        Email_Age_Details__c obj = new Email_Age_Details__c();
        obj.is_Email_Age_Timed_Out__c = true;
        obj.Email__c = EmailAgeStorageController.encodeQueryParams(co.Email);
        obj.Contact__c = opp.Contact__c;
        insert obj;
        
        CreditReport.set(opp.id, LibraryTest.fakeBrmsRulesResponse(opp.Id, true, '1', true));
        DecisionCtrl.fetchNewSoftCreditPull(opp);
     
        System.assertEquals('Credit Qualified', [SELECT Status__c FROM Opportunity WHERE Id =: opp.Id].Status__c);
    }
    
    
    @isTest static void fetch_soft_credit_pull_decision_no() {
        Opportunity opp = LibraryTest.createApplicationTH();
        
        Contact co = [Select Id, Email from Contact where Id =: opp.Contact__c];
        
        Email_Age_Details__c obj = new Email_Age_Details__c();
        obj.is_Email_Age_Timed_Out__c = true;
        obj.Email__c = EmailAgeStorageController.encodeQueryParams(co.Email);
        obj.Contact__c = opp.Contact__c;
        insert obj;
        
        CreditReport.set(opp.id, LibraryTest.fakeBrmsFailRulesResponse(opp.Id, true, '1', false));
        DecisionCtrl.fetchNewSoftCreditPull(opp);
    }
    
    @isTest static void overrides_disqualifiers() {
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Status__c = 'Declined (or Unqualified)';
        update opp;
        
        Proxy_Api__c proxy = LibraryTest.fakeSettings();
        insert proxy;
        
        LibraryTest.createBrmsConfigSettings(1, true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        CreditReport.set(opp.id, LibraryTest.fakeBrmsRulesResponse(opp.Id, true, '1', true));
        
        DecisionCtrl.overrideDisqualifier(opp, 1, false);
        
        //System.assertEquals('Credit Qualified', [SELECT Status__c FROM Opportunity WHERE Id =: opp.Id].Status__c);
    }
    
    @isTest static void fetch_email_age_result() {
        Opportunity opp = LibraryTest.createApplicationTH();
        DecisionCtrl.fetchNewEmailAgeResult(opp, 32000001, false);
    }
    
    @isTest static void creates_funding_approval() {
        String Description = 'Test Funding Approval';
        Opportunity opp = LibraryTest.createApplicationTH();
        
        Test.startTest();
        DecisionCtrl.createFundingApproval(opp.Id, opp.Name, Description);
        Test.stopTest();
        
        Case cs = [Select Id, Description, Subject FROM CASE WHERE Opportunity__c =: opp.id Limit 1];
        
        System.assertEquals('Funding Approval' , cs.Subject);
    }
    
}