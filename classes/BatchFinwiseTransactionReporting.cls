global class BatchFinwiseTransactionReporting implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts{

    TimeZone tZone = UserInfo.getTimeZone();
    Map<Opportunity,List<loan__Loan_Payment_Transaction__c>> contractMap = new Map<Opportunity,List<loan__Loan_Payment_Transaction__c>>();
    FinwiseDetails__c FinwiseParams = FinwiseDetails__c.getInstance(); 
     
    public void execute(SchedulableContext sc) { 
      // Finwise_Details__c FinwiseParams = Finwise_Details__c.getInstance('finwise'); 
       BatchFinwiseTransactionReporting batchapex = new BatchFinwiseTransactionReporting();
       id batchprocessid = Database.executebatch(batchapex, integer.valueOf(FinwiseParams.Transaction_Batch_Size__c));       
    }
    
    global BatchFinwiseTransactionReporting(){
    try{
       date cutOffDate = (system.today() - integer.ValueOf(FinwiseParams.No_of_Days_back_Transaction_batch__c));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
       String query = 'Select Name,loan__Principal__c,loan__Transaction_Amount__c,loan__Fees__c,';
        query += 'loan__Interest__c,createddate, loan__Cleared__c,';
        query += 'loan__Transaction_Date__c, loan__Loan_Account__r.Opportunity__r.id,';
        query += 'loan__Loan_Account__r.Opportunity__r.name,';
        query += 'loan__Clearing_Date__c,loan__Reversed__c, ';
        query += 'Reporting_Reversed_Date__c,';
        query += 'loan__Loan_Account__r.Opportunity__r.createdDate,loan__Loan_Account__c,';
        query += 'loan__Loan_Account__r.Opportunity__r.Expected_Start_Date__c,loan__Loan_Account__r.loan__Disbursed_Amount__c,';
        query += 'loan__Loan_Account__r.loan__Principal_Remaining__c, loan__Loan_Account__r.loan__Principal_Paid__c, '; 
        query += 'loan__Loan_Account__r.loan__Fees_Paid__c, loan__Loan_Account__r.loan__Interest_Rate__c,';
        query += 'loan__Loan_Account__r.loan__Loan_Status__c, loan__Loan_Account__r.Opportunity__r.Contact__r.Customer_Number__c,';
        query += 'Finwise_Clear_Reported__c, Finwise_Reverse_Reported__c ';
        query += 'from loan__Loan_Payment_Transaction__c ';
        query += 'where loan__Loan_Account__r.Opportunity__r.Finwise_Approved__c = true AND'; 
        query += '((loan__Cleared__c = true AND loan__Reversed__c = false AND Finwise_Clear_Reported__c = false ) OR ';
        query += ' (loan__Reversed__c = true AND (Finwise_Clear_Reported__c = true) AND Finwise_Reverse_Reported__c = false))';
        query += 'order by loan__Loan_Account__r.Opportunity__r.Name, CreatedDate';

        System.debug('Query = ' + query);
       list<loan__Loan_Payment_Transaction__c> transList;       
        
       for(loan__Loan_Payment_Transaction__c trans: Database.Query(query)){                               
           Opportunity loan = trans.loan__Loan_Account__r.Opportunity__r;
          
           if(contractMap.containsKey(loan)){
              transList = contractMap.get(loan);
           }else
              transList = new list<loan__Loan_Payment_Transaction__c>(); 
           
           transList.add(trans);
           contractMap.put(loan,transList);       
       } 
        System.debug('Contracts = ' + transList.size());
       }catch(Exception exc){          
           WebToSFDC.notifyDev('Finwise Service Transaction Processing Exception',  exc.getMessage() + ' line ' + (exc.getLineNumber()) + '\n' + exc.getStackTraceString());            
       } 
    } 
    
    
    global list<Opportunity> start(Database.BatchableContext BC){          
      list<Opportunity> listForScope = new list<Opportunity>();
      listForScope.addAll(contractMap.keyset());
        System.debug('Contracts = ' + listForScope.size());
      return listForScope;
    }
    
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){       
       list<finwiseTransResponse> finwiseTransResponseList =  new list<finwiseTransResponse>();
       list<logging__c> logsList = new list<logging__c>();
       List<loan__Loan_Payment_Transaction__c> payToUp = new List<loan__Loan_Payment_Transaction__c>(); 
       Map<string,string> apNameContractMap = new Map<string,string>();
       string reqBody;
       system.debug('======scope============'+scope);
       try{
      
        JSONGenerator generator = JSON.createGenerator(false);
    
       generator.writeStartArray();  
     
       for(Opportunity loanRef: scope){           
            loan__Loan_Account__c loanAcnt;                                                                           
            generator.writeStartObject();                                  
                generator.writeStringField('LoanNumber', loanRef.name);
                generator.writeFieldName('Transactions');
            generator.writeStartArray(); 
            
            for(loan__Loan_Payment_Transaction__c trans: contractMap.get(loanRef)){ 
                apNameContractMap.put(loanRef.name, trans.loan__Loan_Account__c);
                
                loanAcnt = trans.loan__Loan_Account__r;
                integer createdDatetZoneOffset = tZone.getOffset(trans.createdDate); 
                
                generator.writeStartObject();                                 
                    generator.writeNumberField('Id',0);
                    generator.writeNumberField('LoanId',0);
                    generator.writeNumberField('LoanTransactionTypeId',FinwiseParams.Transaction_Type_Id__c);
                    generator.writeNumberField('LoanTransactionStatusId',(trans.loan__Reversed__c)?FinwiseParams.Transaction_Reversed_Status_Id__c:FinwiseParams.Transaction_Cleared_Status_Id__c);
                    generator.writeStringField('CreationDate','\\/Date('+trans.createddate.getTime()+createdDatetZoneOffset+')\\/');
                    generator.writeStringField('TransactionKey',trans.Name);
                    generator.writeStringField('TransactionType','Payment'); 
                    
                    if(trans.loan__Transaction_Date__c <> null){ 
                        DateTime Tdt = Finwise.convertDateToDateTime(trans.loan__Transaction_Date__c);                 
                        generator.writeStringField('PostedDate','\\/Date('+Tdt.getTime()+tZone.getOffset(Tdt)+')\\/');
                    }                     
                    generator.writeStringField('Username','LendingPoint');
                    generator.writeStringField('Description','Customer Payment');                   
                    generator.writeNumberField('Amount',(trans.loan__Transaction_Amount__c <> null)?trans.loan__Transaction_Amount__c.setScale(2):0.00);   
                    generator.writeNumberField('Principal',(trans.loan__Principal__c <> null)?trans.loan__Principal__c.setScale(2):0.00);  
                    generator.writeNumberField('Fee',(trans.loan__Fees__c <> null)?trans.loan__Fees__c.setScale(2):0.00);  
                    generator.writeNumberField('Interest',(trans.loan__Interest__c <> null)?trans.loan__Interest__c.setScale(2):0.00);                                                                   
                generator.writeEndObject();
                if (trans.loan__Reversed__c) trans.Finwise_Reverse_Reported__c = true;
                if (trans.loan__Cleared__c) trans.Finwise_Clear_Reported__c = true;
                payToUp.add(trans);
            }  
            
            generator.writeEndArray(); 
                         
             generator.writeFieldName('Service');        
                generator.writeStartObject();                   
                    generator.writeNumberField('LoanId',0);
                    generator.writeBooleanField('ArePaymentsActive',(loanAcnt.loan__Loan_Status__c.contains('Active')));                                  
                    generator.writeNumberField ('AmountDisbursed',(loanAcnt.loan__Disbursed_Amount__c <> null)?loanAcnt.loan__Disbursed_Amount__c.setScale(2):0.00);
                    generator.writeNumberField('PrincipalRemaining',(loanAcnt.loan__Principal_Remaining__c <> null)?loanAcnt.loan__Principal_Remaining__c.setScale(2):0.00);  
                    generator.writeNumberField('PrincipalPaid',(loanAcnt.loan__Principal_Paid__c <> null)?loanAcnt.loan__Principal_Paid__c.setScale(2):0.00);
                    generator.writeNumberField('FeesPaid',(loanAcnt.loan__Fees_Paid__c <> null)?loanAcnt.loan__Fees_Paid__c.setScale(2):0.00);
                    generator.writeNumberField('InterestPaid',(loanAcnt.loan__Interest_Rate__c <> null)?loanAcnt.loan__Interest_Rate__c.setScale(2):0.00);                                                   
                generator.writeEndObject();
              generator.writeEndObject();                                                                        
           
           }  
           
            generator.writeEndArray();          
                         
            reqBody = generator.getAsString(); 
            system.debug('============reqBody ====='+reqBody );
            reqBody = reqBody.unescapeJava();             
            HttpRequest req = new HttpRequest();       
            string Url = FinwiseParams.Finwise_Base_URL__c+'/loanservicing/'+FinwiseParams.Finwise_product_Key__c+'/loans';   
            req.setEndpoint(url);
            req.setMethod('POST'); 
            req.SetBody(reqBody);  
            req.setHeader('Content-Type', 'application/json');                     
            Http http = new Http();
            HttpResponse response;
            
            if(!Test.isRunningTest()){
              response = http.send(req);
                
            }else{
              response = new HTTPResponse();                    
              response.SetBody(TestHelper.createDomDocNew().toXmlString());               
            }       
            system.debug('============response====='+response);
            system.debug('============body====='+response.getbody());
            string RespOLog = 'Response: '+response+'\n Response Body: '+response.getBody();                             
                
           if(response.getStatusCode() == 200){ 
              JSONParser parser = JSON.createParser(response.getBody()); 
              finwiseTransResponseList = (list<finwiseTransResponse>) parser.readValueAs(list<finwiseTransResponse>.class);   
              for(finwiseTransResponse batchResp : finwiseTransResponseList){
                if(batchResp.Status <> 'Success') logsList.add(CustomerPortalWSDLUtil.getLogging('Finwise Transaction Reporting Unsuccessful for '+batchResp.RecordKey, apNameContractMap.get(batchResp.RecordKey), reqBody, String.join(batchResp.Messages,', ')));                   
              }  
              if (payToUp.size() > 0) update payToUp;                         
           }else                                       
              logsList.add(CustomerPortalWSDLUtil.getLogging('Finwise Transaction Batch Error', null,reqBody, RespOLog));                   
           
           

           
                             
       }catch(Exception exc){ 
           system.debug('===error=========='+exc.getmessage() + ' at line no '+exc.getlineNumber());                            
           string ExcpNoteBody = 'Finwise Transaction Reporting Exception:  '+exc+' at Line No. '+exc.getLineNumber();                             
           logsList.add(CustomerPortalWSDLUtil.getLogging('Finwise Transaction Reporting Exception', null,reqBody, ExcpNoteBody));                           
           WebToSFDC.notifyDev('Finwise Transaction Reporting Exception',  exc.getMessage() + ' line ' + (exc.getLineNumber()) + '\n' + exc.getStackTraceString());            
       }finally{
       
           if(logsList.size() > 0)insert logsList;   
           system.debug('===logsList=========='+logsList);    
       }
      
    }
    
    global void finish(Database.BatchableContext BC){            
       WebToSFDC.notifyDev('BatchFinwiseTransactionReporting Batch Completed', 'Finwise Batch for cleared and reversed transcation Reporting completed for Today. Please check loggings for any failures.');        
    }
    
    
   
    private static Datetime convertDateToDateTime(Date inputDate){       
        return datetime.newInstance(inputDate.year(),inputDate.month(),inputDate.day());
    }
    
    
    public class finwiseTransResponse{        
        string[] Messages;
        string RecordKey, Status;                 
    } 
  
}