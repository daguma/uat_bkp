global class BatchOfferRegeneration implements Database.Batchable<sObject>,Schedulable, Database.AllowsCallouts, Database.Stateful {

	global Integer count = 0;
	global final String option;

	public void execute(SchedulableContext sc) { 
       BatchOfferRegeneration  batchapex = new BatchOfferRegeneration();
       id batchprocessid = Database.executebatch(batchapex,10);
       system.debug('Process ID: ' + batchprocessid);
    }  

    global Database.QueryLocator start(Database.BatchableContext BC){                  
       String query = 'SELECT OpportunityId__c FROM Offer_Regeneration_List__c ';
       System.debug('Query = ' + query);
       return Database.getQueryLocator(query);
    }

    global BatchOfferRegeneration(){
    }

    global BatchOfferRegeneration(String option) {
    	this.option = option;
	}

	
	global void execute(Database.BatchableContext BC, List<Offer_Regeneration_List__c> scope){
		 if (this.option == 'generateOffers') {
			 for(Offer_Regeneration_List__c orgl : scope){
			 		generateOffers(orgl);
			 		count++;
		        }           
         }

         if (this.option == 'deleteOffers') {
			 for(Offer_Regeneration_List__c orgl : scope){
			 		deleteOffers(orgl);
			 		count++;
		        }           
         }
	}

	global void finish(Database.BatchableContext BC){        
            WebToSFDC.notifyDev('Offers Regeneration Batch Completed', 'Offers Process Completed. \n\nTotal processed = ' + count);        
    }

    private void deleteOffers(Offer_Regeneration_List__c orgl){
		ScorecardUtil.deleteOffers(orgl.OpportunityId__c);
    }


    private void generateOffers(Offer_Regeneration_List__c orgl) {
        Opportunity opp = OfferCtrl.getApplication(orgl.OpportunityId__c);
        System.debug ('Opportunity - ' + opp.Id);
        if (opp.Fico__c != null && opp.PrimaryCreditReportId__c != null) {
            string fico = opp.Fico__c;
            Decimal ficoValue = string.isNotEmpty(fico) && !fico.trim().contains('-') ? Decimal.valueOf(fico) : null;
            Long creditReportId = opp.PrimaryCreditReportId__c.longValue();
            Scorecard__c score = ScorecardUtil.getScorecard(opp.Id);
            if (score == null) {
                System.debug ('Opportunity - Regenerando Score ' + opp.Id);
                Opportunity theApp = CreditReport.getOpportunity(opp.Id);
                ProxyApiCreditReportEntity creditReportEntity = CreditReport.getByEntityId(opp.Id);
                if (creditReportEntity != null) {
                    ScorecardUtil.scorecardProcess(creditReportEntity, theApp);
                    score = ScorecardUtil.getScorecard(opp.Id);
                }
                //List<Offer__c> offers = [SELECT Id FROM Offer__c WHERE Opportunity__c = :opp.Id];
                //delete offers;
            } else {
                String grade = '';
                if (score != null && String.isNotEmpty(score.Acceptance__c))
                    grade = score.Grade__c;
                List<ProxyApiOfferCatalog> newOffers = OfferCtrlUtil.getOffers(opp, string.ValueOf(grade), creditReportId, ficoValue != null ? string.valueof(ficoValue) : null, null, true);
                system.debug('==newOffers==' + newOffers);
                if (newOffers != null && newOffers.size() > 0)
                    ScorecardUtil.generateOffers(opp.Id, newOffers);    //INSERT OFFERS RECEIVED FROM SP
                if (opp.Type == 'Refinance') {
                    ScorecardUtil.regenerateOffersRefinance(opp.Id, false);
                    //opp.Refinance_Payment_Difference__c = OfferCtrlUtil.calculateRefinancePaymentDifference(newOffers,opp.Contract_Renewed__r.loan__Pmt_Amt_Cur__c);
                }
            }
        }
    }
}