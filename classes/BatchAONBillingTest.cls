@isTest
private class BatchAONBillingTest {

    @isTest static void processes_aon_billing() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan_AON_Billing__c aonBilling = new loan_AON_Billing__c();
        aonBilling.IsProcessed__c = false;
        aonBilling.IsEnrollment__c = true;
        aonBilling.Contract__c = contract.id;
        aonBilling.Product_Code__c = 'LPOPTION01';
        aonBilling.Billing_Date__c = System.now();
        aonBilling.Product_Fee__c = 14.51;
        aonBilling.Minimum_Monthly_Payment__c = 230;
        aonBilling.Loan_Balance__c = 5000;
        aonBilling.Payment_Due_Date__c = System.today().addDays(2);
        insert aonBilling;

        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"aonResponse":null,"errorMessage":null,"errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        BatchAONBilling batchapex = new BatchAONBilling();
        Database.executebatch(batchapex, 1);
    }

    @isTest static void processes_aon_billing_with_error() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();

        loan_AON_Billing__c aonBilling = new loan_AON_Billing__c();
        aonBilling.IsProcessed__c = false;
        aonBilling.IsEnrollment__c = true;
        aonBilling.Contract__c = contract.id;
        aonBilling.Product_Code__c = 'LPOPTION01';
        aonBilling.Billing_Date__c = System.now();
        aonBilling.Product_Fee__c = 14.51;
        aonBilling.Minimum_Monthly_Payment__c = 230;
        aonBilling.Loan_Balance__c = 5000;
        aonBilling.Payment_Due_Date__c = System.today().addDays(2);
        insert aonBilling;

        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(400,
                'OK',
                '{"aonResponse":null,"errorMessage":"test error","errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        BatchAONBilling batchapex = new BatchAONBilling();
        Database.executebatch(batchapex, 1);
    }

    @isTest static void schedule_job() {
        Test.startTest();

        BatchAONBilling billingJob = new BatchAONBilling();
        String cron = '0 0 23 * * ?';
        system.schedule('Test AON Billing Job', cron, billingJob);

        Test.stopTest();
    }

    @isTest static void processes_aon_billing_from_payment() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
		contract.loan__Pay_Off_Amount_As_Of_Today__c = 4569;
        update contract;
        
        loan_AON_Billing__c aonBilling = new loan_AON_Billing__c();
        aonBilling.IsProcessed__c = false;
        aonBilling.IsEnrollment__c = false;
        aonBilling.Contract__c = contract.id;
        aonBilling.Product_Code__c = 'LPOPTION01';
        aonBilling.Billing_Date__c = System.now().addHours(-73);
        aonBilling.Product_Fee__c = 14.51;
        aonBilling.Minimum_Monthly_Payment__c = 230;
        aonBilling.Loan_Balance__c = 5000;
        aonBilling.Payment_Due_Date__c = System.now().addDays(2);
        aonBilling.RetryCount__c = 0;
        insert aonBilling;

        Database.BatchableContext bc;

        Datetime last24h = System.now().addDays(-3);        
        List<loan_AON_Billing__c> billingsList = [SELECT Id,Contract__c,Contract__r.Name,Contract__r.loan__Pay_Off_Amount_As_Of_Today__c,Billing_Date__c,Company__c,IsProcessed__c,loan_AON_Information__c,ServiceError__c,Product_Code__c,Loan_Balance__c,
                       Minimum_Monthly_Payment__c,Payment_Due_Date__c,Product_Fee__c,IsEnrollment__c,RetryCount__c,Contract__r.loan__Next_Installment_Date__c 
        		       FROM loan_AON_Billing__c 
                       WHERE IsProcessed__c = false AND (Billing_Date__c < : last24h OR IsEnrollment__c = true) AND RetryCount__c <= 3];
        
        System.debug('24 hours ' + billingsList);
        
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '{"aonResponse":null,"errorMessage":null,"errorDetails":null}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        BatchAONBilling batchapex = new BatchAONBilling();
        batchapex.execute(bc, billingsList);
        
        loan_AON_Billing__c CLBilling = [SELECT Id, Loan_Balance__c, IsProcessed__c FROM loan_AON_Billing__c WHERE Id =: aonBilling.Id LIMIT 1];
        
        //System.assertEquals(4569, CLBilling.Loan_Balance__c);
    }

    @isTest static void processes_aon_billing_from_payment_before_24_hours() {

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Pay_Off_Amount_As_Of_Today__c = 4569;
        update contract;

        loan_AON_Billing__c aonBilling = new loan_AON_Billing__c();
        aonBilling.IsProcessed__c = false;
        aonBilling.IsEnrollment__c = false;
        aonBilling.Contract__c = contract.id;
        aonBilling.Product_Code__c = 'LPOPTION01';
        aonBilling.Billing_Date__c = System.now().addHours(-24);
        aonBilling.Product_Fee__c = 14.51;
        aonBilling.Minimum_Monthly_Payment__c = 230;
        aonBilling.Loan_Balance__c = 5000;
        aonBilling.Payment_Due_Date__c = System.now().addDays(2);
        insert aonBilling;

        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();

        Database.BatchableContext bc;

        Datetime last24h = System.now().addDays(-3);
        List<loan_AON_Billing__c> billingsList = [SELECT Id,Contract__c,Contract__r.Name,Contract__r.loan__Pay_Off_Amount_As_Of_Today__c,Billing_Date__c,Company__c,IsProcessed__c,loan_AON_Information__c,ServiceError__c,Product_Code__c,Loan_Balance__c,
                       Minimum_Monthly_Payment__c,Payment_Due_Date__c,Product_Fee__c,IsEnrollment__c,RetryCount__c,Contract__r.loan__Next_Installment_Date__c 
        		       FROM loan_AON_Billing__c 
                       WHERE IsProcessed__c = false AND (Billing_Date__c < : last24h OR IsEnrollment__c = true) AND RetryCount__c <= 3];
        
        BatchAONBilling batchapex = new BatchAONBilling();
        batchapex.execute(bc, billingsList);
        
        loan_AON_Billing__c CLBilling = [SELECT Id, Loan_Balance__c, IsProcessed__c FROM loan_AON_Billing__c WHERE Id =: aonBilling.Id LIMIT 1];
        
        System.assertEquals(5000, CLBilling.Loan_Balance__c);
        System.assertEquals(false, CLBilling.IsProcessed__c);
    }
    
}