@isTest public class DL_AccountStatementSFTest {

    public static final String JSON_RESPONSE = TestHelper.JSON_RESPONSE;
    
    @isTest static void parses_statement() {

        String jsonResponse = String.escapeSingleQuotes(JSON_RESPONSE);
        jsonResponse = jsonResponse.replaceAll('\'', '\\\\\'');

        JSONParser parser = JSON.createParser(jsonResponse);
        List<DL_AccountStatementSF> dl_AccountStatementSFList = (List<DL_AccountStatementSF>) parser.readValueAs(List<DL_AccountStatementSF>.class);

        System.assertNotEquals(null, dl_AccountStatementSFList);

        String requestCode = dl_AccountStatementSFList.get(0).requestCode;
        system.assertNotEquals(null, requestCode);

        String customerIdentifier = dl_AccountStatementSFList.get(0).customerIdentifier;
        system.assertNotEquals(null, customerIdentifier);

        String email = dl_AccountStatementSFList.get(0).emailAddress;
        system.assertNotEquals(null, email);

        String institutionName = dl_AccountStatementSFList.get(0).institutionName;
        system.assertNotEquals(null, institutionName);

        String accountName = dl_AccountStatementSFList.get(0).accountName;
        system.assertNotEquals(null, accountName);

        String accountType = dl_AccountStatementSFList.get(0).accountType;
        system.assertNotEquals(null, accountType);

        String accountNumberEntered = dl_AccountStatementSFList.get(0).accountNumberEntered;
        system.assertNotEquals(null, accountNumberEntered);

        String accountNumberFoundClean = dl_AccountStatementSFList.get(0).accountNumberFoundClean;
        system.assertNotEquals(null, accountNumberFoundClean);

        String accountNumberFound = dl_AccountStatementSFList.get(0).accountNumberFound;
        system.assertNotEquals(null, accountNumberFound);

        String accountNumberConfidence = dl_AccountStatementSFList.get(0).accountNumberConfidence;
        system.assertNotEquals(null, accountNumberConfidence);

        String nameEntered = dl_AccountStatementSFList.get(0).nameEntered;
        system.assertNotEquals(null, nameEntered);

        String nameFound = dl_AccountStatementSFList.get(0).nameFound;
        system.assertNotEquals(null,nameFound);

        String nameConfidence = dl_AccountStatementSFList.get(0).nameConfidence;
        system.assertNotEquals(null,nameConfidence);

        String amountInput = dl_AccountStatementSFList.get(0).amountInput;
        system.assertNotEquals(null,amountInput);

        String availableBalance = dl_AccountStatementSFList.get(0).availableBalance;
        system.assertNotEquals(null,availableBalance);

        String averageBalance = dl_AccountStatementSFList.get(0).averageBalance;
        system.assertNotEquals(null,averageBalance);

        String averageBalanceRecent = dl_AccountStatementSFList.get(0).averageBalanceRecent;
        system.assertNotEquals(null,averageBalanceRecent);

        String isVerified = dl_AccountStatementSFList.get(0).isVerified;
        system.assertNotEquals(null,isVerified);

        String asOfDate = dl_AccountStatementSFList.get(0).asOfDate;
        system.assertNotEquals(null,asOfDate);

    	String activityStartDate = dl_AccountStatementSFList.get(0).activityStartDate;
        system.assertNotEquals(null,activityStartDate);

        String activityEndDate = dl_AccountStatementSFList.get(0).activityEndDate;
        system.assertNotEquals(null,activityEndDate);

        String totalCredits = dl_AccountStatementSFList.get(0).totalCredits;
        system.assertNotEquals(null,totalCredits);

        String totalDebits = dl_AccountStatementSFList.get(0).totalDebits;
        system.assertNotEquals(null,totalDebits);

        String currentBalance = dl_AccountStatementSFList.get(0).currentBalance;
        system.assertNotEquals(null,currentBalance);

        String isActivityAvailable = dl_AccountStatementSFList.get(0).isActivityAvailable;
        system.assertNotEquals(null,isActivityAvailable);

        String processedStatus = dl_AccountStatementSFList.get(0).processedStatus;
        system.assertNotEquals(null,processedStatus);

        List<DL_AccountStatementSF.TransactionAnalysisSummaries> transactionAnalysisSummaries
            = dl_AccountStatementSFList.get(0).transactionAnalysisSummaries;
        system.assertNotEquals(null,transactionAnalysisSummaries);

    	String totalCount = transactionAnalysisSummaries.get(0).totalCount;
        system.assertNotEquals(null,totalCount);

        String typeCode = transactionAnalysisSummaries.get(0).typeCode;
        system.assertNotEquals(null,typeCode);

        Decimal totalAmount = transactionAnalysisSummaries.get(0).totalAmount;
        system.assertNotEquals(null,totalAmount);

        String recentCount = transactionAnalysisSummaries.get(0).recentCount;
        system.assertNotEquals(null,recentCount);

        String typeName = transactionAnalysisSummaries.get(0).typeName;
        system.assertNotEquals(null,typeName);

        Decimal recentAmount = transactionAnalysisSummaries.get(0).recentAmount;
        system.assertNotEquals(null,recentAmount);

        transactionAnalysisSummaries.get(0).popupLink = '1111';
        String popupLink = transactionAnalysisSummaries.get(0).popupLink;
        system.assertNotEquals(null,popupLink);

        List<DL_AccountStatementSF.TransactionSummaries> transactionSummaries
            = dl_AccountStatementSFList.get(0).transactionSummaries;
        system.assertNotEquals(null,transactionSummaries);

    	String isRefresh = transactionSummaries.get(0).isRefresh;
        system.assertNotEquals(null,isRefresh);

        String transactionStatus = transactionSummaries.get(0).status;
        system.assertNotEquals(null,transactionStatus);

        Decimal amount = transactionSummaries.get(0).amount;
        system.assertNotEquals(null,amount);

        String description = transactionSummaries.get(0).description;
        system.assertNotEquals(null,description);

        String category = transactionSummaries.get(0).category;
        system.assertNotEquals(null,category);

        Decimal runningBalance = transactionSummaries.get(0).runningBalance;
        system.assertNotEquals(null,runningBalance);

        String typeCodes = transactionSummaries.get(0).typeCodes;
        system.assertNotEquals(null,typeCodes);

        String transactionDate = transactionSummaries.get(0).transactionDate;
        system.assertNotEquals(null,transactionDate);

        String isStarted = dl_AccountStatementSFList.get(0).isStarted;
        system.assertNotEquals(null,isStarted);

        String isCompleted = dl_AccountStatementSFList.get(0).isCompleted;
        system.assertNotEquals(null,isCompleted);

        String notes = dl_AccountStatementSFList.get(0).notes;
        system.assertNotEquals(null,notes);

        String status = dl_AccountStatementSFList.get(0).status;
        system.assertNotEquals(null,status);

        String statusText = dl_AccountStatementSFList.get(0).statusText;
        system.assertNotEquals(null,statusText);

        String statusCodeColor = dl_AccountStatementSFList.get(0).statusCodeColor;
        system.assertNotEquals(null,statusCodeColor);

        String lastRefreshErrorMessage = dl_AccountStatementSFList.get(0).lastRefreshErrorMessage;
        system.assertNotEquals(null,lastRefreshErrorMessage);

        String isError = dl_AccountStatementSFList.get(0).isError;
        system.assertNotEquals(null,isError);

        String errorMessage = dl_AccountStatementSFList.get(0).errorMessage;
        system.assertNotEquals(null,errorMessage);

        String chartsId = dl_AccountStatementSFList.get(0).chartsId;
        system.assertNotEquals(null,chartsId);

        List<DL_AccountStatementSF.AccountExpenses> accountExpenses
            = dl_AccountStatementSFList.get(0).accountExpenses;
        system.assertNotEquals(null,accountExpenses);

 		String transactionCategory = accountExpenses.get(0).category;
        system.assertNotEquals(null,transactionCategory);

        Decimal transactionAmount = accountExpenses.get(0).amount;
        system.assertNotEquals(null,transactionAmount);

        Decimal percentage = accountExpenses.get(0).percentage;
        system.assertNotEquals(null,percentage);

        String isLoginValid = dl_AccountStatementSFList.get(0).isLoginValid;
        system.assertNotEquals(null,isLoginValid);

        //Adding newest fields not included in the report
        DL_AccountStatementSF stmt = dl_AccountStatementSFList.get(0);
        stmt.federalTaxPrevYear = 0.00;
        system.assertNotEquals(null,stmt.federalTaxPrevYear);
        stmt.stateTaxPrevYear = 0.00;
        system.assertNotEquals(null,stmt.stateTaxPrevYear);
        stmt.lowerTaxSlab = 0.00;
        system.assertNotEquals(null,stmt.lowerTaxSlab);
        stmt.defaultAmount = 0.00;
        system.assertNotEquals(null,stmt.defaultAmount);
        stmt.federalTax = 0.00;
        system.assertNotEquals(null,stmt.federalTax);
        stmt.stateTax = 0.00;
        system.assertNotEquals(null,stmt.stateTax);

        system.assert(stmt.matchesEqualsAccountNumberFound(stmt.accountNumberFound));

    }

    @isTest static void checks_other_stmt_fields(){

        DL_AccountStatementSF stmt = new DL_AccountStatementSF();
        stmt.negativeDays30DayCount = '30';
        stmt.negativeDays60DayCount = '60';
        stmt.negativeDays90DayCount = '90';
        system.assertNotEquals(null, stmt.negativeDays);

        stmt.lastMonthDatesMsg = '';
        system.assertEquals(stmt.lastMonthDatesMsg,'* Last Month period not specified yet');

    }

    @isTest static void checks_matches_account_number_found() {

        String jsonResponse = String.escapeSingleQuotes(JSON_RESPONSE);
        jsonResponse = jsonResponse.replaceAll('\'', '\\\\\'');

        JSONParser parser = JSON.createParser(jsonResponse);
        List<DL_AccountStatementSF> dl_AccountStatementSFList = (List<DL_AccountStatementSF>) parser.readValueAs(List<DL_AccountStatementSF>.class);

        system.assertEquals(false, dl_AccountStatementSFList.get(0).matchesAccountNumberFound(null));

        system.assert(dl_AccountStatementSFList.get(0).matchesAccountNumberFound('4810'));

        system.assert(dl_AccountStatementSFList.get(0).matchesAccountNumberFound('4-81-0'));

        system.assert(dl_AccountStatementSFList.get(0).matchesAccountNumberFound('534534zWW4-81-0'));

        system.assertEquals(false,dl_AccountStatementSFList.get(0).matchesAccountNumberFound('534534zWW4-831-02345'));

        system.assertEquals(false,dl_AccountStatementSFList.get(0).matchesAccountNumberFound(''));

    }

    @isTest static void checksGetNegDays(){

        String jsonResponse = String.escapeSingleQuotes(JSON_RESPONSE);
        jsonResponse = jsonResponse.replaceAll('\'', '\\\\\'');
        JSONParser parser = JSON.createParser(jsonResponse);
        List<DL_AccountStatementSF> dl_AccountStatementSFList = (List<DL_AccountStatementSF>) parser.readValueAs(List<DL_AccountStatementSF>.class);

        system.assertEquals(0,DL_AccountStatementSF.getNegativeDays( dl_AccountStatementSFList.get(0).transactionSummaries, dl_AccountStatementSFList.get(0).asOfDate ));

    }

    @isTest static void checkMatchesContainsAccountNumberFound(){
        String jsonResponse = String.escapeSingleQuotes(JSON_RESPONSE);
        jsonResponse = jsonResponse.replaceAll('\'', '\\\\\'');
        JSONParser parser = JSON.createParser(jsonResponse);
        List<DL_AccountStatementSF> dl_AccountStatementSFList = (List<DL_AccountStatementSF>) parser.readValueAs(List<DL_AccountStatementSF>.class);
        DL_AccountStatementSF stmt = dl_AccountStatementSFList.get(0);

        system.assertEquals(true,stmt.matchesContainsAccountNumberFound('4810'));
        system.assertEquals(false,stmt.matchesContainsAccountNumberFound('0987654321'));

    }

}