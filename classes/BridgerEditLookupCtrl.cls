public with sharing class BridgerEditLookupCtrl {
    BridgerWDSFLookup__c tmpData;
    public string prevstatus;

    public BridgerEditLookupCtrl(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) stdController.addFields(new List<String> {'Review_status__c'});
        tmpData = (BridgerWDSFLookup__c) stdController.getRecord();
        prevstatus = tmpData.review_status__c;
    }

    public PageReference save() {
        try {
            if (tmpData.Review_status__c == null) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Review Status cannot be None.');
                ApexPages.addMessage(myMsg);
                return null;
            }
            if (prevstatus != tmpData.Review_status__c) {
                tmpData.last_reviewed__c = userinfo.getname() + ' ' + datetime.now().formatlong();
            }

            update tmpData;
            PageReference pageRef = new PageReference('/' + tmpData.Contact__c);
            pageRef.setRedirect(true);
            return pageRef;
        } catch (exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getmessage());
            ApexPages.addMessage(myMsg);
            return null;
        }
    }
}