global class DealAutomaticAssignmentJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {
	global final String query;
	global final Integer batchRecords;
	global final Boolean isDebugMode;
	global final String option;

	private Boolean validQuery = true;
	private DealAutomaticAssignmentManager.AssignmentParams params;
    
	global DealAutomaticAssignmentJob(Integer batchRecords, Boolean isDebugMode, String option) {
		this.option = option;
        this.batchRecords = batchRecords;
		this.isDebugMode = isDebugMode;

		this.params = new DealAutomaticAssignmentManager.AssignmentParams(option);

		if (params.userIds != null && params.userIds.size() > 0 && params.counterTMP != null){
			if (option == 'OA_IP_No_Assigned')
				this.query = DealAutomaticAssignmentManager.getQueryOppInOAandIP();
			else if (option == 'RQ_No_Assigned')
				this.query = DealAutomaticAssignmentManager.getQueryOppInRQ();	
			else if (option == 'CQ_No_Assigned')
				this.query = DealAutomaticAssignmentManager.getQueryOppInCQ();
        } 
        else 
            this.validQuery = false;
	}
    
	global Database.QueryLocator start(Database.BatchableContext BC) {
        if(this.validQuery)
			return Database.getQueryLocator(query);
		else 
			return Database.getQueryLocator('SELECT Id FROM Opportunity LIMIT 0');   
	}
    
	@TestVisible
	global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
		if (this.validQuery && this.option != 'CQ_No_Assigned'){
				Integer counterTMP = params.counterTMP;
				for (Opportunity opp : scope) {
					DealAutomaticAssignmentManager.assignUserByAvailableState(opp, params.userIds, counterTMP);
					counterTMP ++;
				}
				params.incrementCounter(counterTMP);
		}
		else if (this.option == 'CQ_No_Assigned'){
			DealAutomaticAssignmentManager.GlobalParameters globalParams = new DealAutomaticAssignmentManager.GlobalParameters(this.isDebugMode);
			for (Opportunity opp : scope) {
				DealAutomaticAssignmentManager.assignUserProcess(opp, globalParams);
			}
		}
	}
    @TestVisible
	global void finish(Database.BatchableContext BC) {
        DealAutomaticAssignmentJob job = new DealAutomaticAssignmentJob(this.batchRecords, this.isDebugMode, this.option);
		Integer minutesFromNow = 0;

		if (this.option == 'OA_IP_No_Assigned') {
			minutesFromNow = (Datetime.now().Hour() < Time.newInstance(22, 0, 0, 0).hour())? 15 : (Datetime.now().format('EEEE') == 'Friday')? 3465 : 585;       
		}
		if (this.option == 'RQ_No_Assigned') {
			minutesFromNow = (Datetime.now().Hour() < Time.newInstance(18, 0, 0, 0).hour())? 30 : (Datetime.now().format('EEEE') == 'Friday')? 3675 : 795;
		}
        
		if (!test.isRunningTest()) System.scheduleBatch(job, 'AutomaticAssignmentJob_' + this.option, minutesFromNow, this.batchRecords);
    }    

	global void execute(SchedulableContext sc) {
		/*if (this.option == 'CQ_No_Assigned'){
			DealAutomaticAssignmentJob job = new DealAutomaticAssignmentJob(this.batchRecords, this.isDebugMode, this.option);
			database.executebatch(job, this.batchRecords);
		}*/	
	}

}

//To run:
/*
	System.schedule('AutomaticAssignmentJob_CQ_No_Assigned', '0 30 6 1/1 * ? *', new DealAutomaticAssignmentJob(20, true, 'CQ_No_Assigned'));
	DealAutomaticAssignmentJob job = new DealAutomaticAssignmentJob(15, false, 'OA_IP_No_Assigned');
*/