@isTest public class ProxyApiOfferCatalogTest {

    @isTest static void validate_instance() {

        Test.startTest();
        ProxyApiOfferCatalog oc = new ProxyApiOfferCatalog();
        Test.stopTest();
        System.assertEquals(null, oc.offerCatalogId);
        System.assertEquals(null, oc.term);
        System.assertEquals(null, oc.apr);
        System.assertEquals(null, oc.loanAmount);
        System.assertEquals(null, oc.grade);
        System.assertEquals(null, oc.annualIncome);
        System.assertEquals(null, oc.paymentAmount);
        System.assertEquals(null, oc.feeAmount);
        System.assertEquals(null, oc.maxPti);
        System.assertEquals(null, oc.maxFundingAmount);
        System.assertEquals(null, oc.state);
        System.assertEquals(null, oc.partner);
        system.assertequals(null,oc.criteria);
        system.assertequals(null,oc.message );
    }

    @isTest static void validate() {

        ProxyApiOfferCatalog oc = new ProxyApiOfferCatalog();
        oc.offerCatalogId = 1;
        System.assertEquals(1, oc.offerCatalogId);

        oc.term = 1;
        System.assertEquals(1, oc.term);

        oc.apr = 1.0;
        System.assertEquals(1.0, oc.apr);

        oc.loanAmount  = 1.0;
        System.assertEquals(1.0, oc.loanAmount);

        oc.grade = 'A2';
        System.assertEquals('A2', oc.grade);

        oc.annualIncome  = 1.0;
        System.assertEquals(1.0, oc.annualIncome);

        oc.paymentAmount  = 1.0;
        System.assertEquals(1.0, oc.paymentAmount);

        oc.feeAmount  = 1.0;
        System.assertEquals(1.0, oc.feeAmount);

        oc.maxPti  = 1.0;
        System.assertEquals(1.0, oc.maxPti);

        oc.maxFundingAmount = 1.0;
        System.assertEquals(1.0, oc.maxFundingAmount);

        oc.state = 'CA';
        System.assertEquals('CA', oc.state);

        oc.partner = 1;
        System.assertEquals(1, oc.partner);
        
        oc.criteria = 'test';
        system.assertequals('test',oc.criteria);
        
        oc.message = 'test message';
        system.assertequals('test message',oc.message );
        
        oc.installments = 4;
        system.assertequals(4,oc.installments);
        
        oc.ear = 1.00;
        system.assertequals(1.00,oc.ear);
    }

}