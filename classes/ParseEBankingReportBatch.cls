global class ParseEBankingReportBatch implements Database.Batchable<sObject>,Schedulable{
    
    List<loan__Loan_Payment_Transaction__c> lstPaymentTransactions = new List<loan__Loan_Payment_Transaction__c>();
    set<string> setContractnbr = new set<string>();
    boolean bIsSandBox = OfferCtrlUtil.runningInASandbox(); 
    
    public void execute(SchedulableContext sc) { 
       ParseEBankingReportBatch batchapex = new ParseEBankingReportBatch();
       id batchprocessid = Database.executebatch(batchapex);
       system.debug('Process ID: ' + batchprocessid);
    }
    
    global ParseEBankingReportBatch (){
    try{
        List<Document> objdoc=[Select Body,ContentType,Description,DeveloperName,Name from Document WHERE Folder.DeveloperName =:Label.ACH_Returns_Report and CreatedDate = Today];
        if(objdoc!=null && objdoc.size() > 0){
            map<string,eBanking_Report__c> mapEBankingApiNames=eBanking_Report__c.getAll();                            
            
            for(Document doc : objdoc){  
                    
                string contents =  doc.Body.toString();   
                system.debug('==============contents============='+contents);                            
                contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');              
                contents = contents.replaceAll('""','DBLQT');   
                system.debug('==============contents============='+contents);            
                List<String> lines = new List<String>(); 
                String[] columnNames;   
                try {        
                    lines = contents.split('\n');             
                    columnNames=lines[0].split(',');
                    system.debug('==columnNames==' + columnNames);
                    lines.remove(0);      
                } 
                catch (System.ListException e) {        
                    System.debug('Limits exceeded?' + e.getMessage());    
                }    
                Integer num = 0;    
                for(String line : lines) {   
                system.debug('==============line============='+line);      
                    // check for blank CSV lines (only commas)        
                    if(!String.isBlank(line) && (line.replaceAll(',','').trim().length() <> 0)){
                    
                    loan__Loan_Payment_Transaction__c loanPaymenttrans = new loan__Loan_Payment_Transaction__c();
              
                    List<String> fields = line.split(',');                                      
                    String compositeField;        
                    Boolean makeCompositeField = false, cleanField = false; 
                    integer index = 0;                 
                    
                    for(Integer i=0; i<=fields.size()-1; i++){
                        system.debug('==============fields[i]============='+fields[i]); 
                    
                        cleanField = false;  
                        string field = fields[i];
                        field=field.replace('\'','SGQT');                       
                        if (field.length() > 1 && field.startsWith('"') && field.endsWith('"')){ 
                          index++;  
                          cleanField = true;                                                                    
                        } 
                        else if (field.startsWith('"')) {                
                            makeCompositeField = true;                
                            compositeField = field;            
                        }
                        else if (field.endsWith('"')) {
                            compositeField += ',' + field;
                            index++;  
                            cleanField = true;                                                                              
                            makeCompositeField = false;  
                            field =  compositeField;         
                        }
                        else if (makeCompositeField) {                
                            compositeField +=  ',' + field;            
                        }
                        else { 
                            index++;  
                            cleanField = true;                                                      
                        } 
                       system.debug('==============index============='+index); 
                        if(cleanField && mapEBankingApiNames.containsKey(columnNames[index-1].trim())){
                            string col = columnNames[index-1].trim();
                            system.debug('==columnNames[i]==' + col);
                            field = field.replaceAll('DBLQT','"');
                            if(mapEBankingApiNames.get(col).API_Name__c.toLowerCase().contains('date')){
                                Date st = Date.parse(field.trim());
                                loanPaymenttrans.put(mapEBankingApiNames.get(col).API_Name__c,st);                        
                            }
                            else if (mapEBankingApiNames.get(col).API_Name__c=='loan__Transaction_Amount__c'){
                                field=String.valueOf(field).replace('$','').replace(',','').replace('"','');                                                                                                             
                                loanPaymenttrans.put(mapEBankingApiNames.get(col).API_Name__c,Decimal.valueOf(field.trim()).setScale(2));
                            }
                            else if (mapEBankingApiNames.get(col).API_Name__c=='loan__Loan_Account__c'){
                                string contractnbr;
                                if(string.valueOf(field.trim()).indexOf('-') == -1){
                                    contractnbr= string.valueOf(field.trim());
                                    contractnbr = contractnbr.substring(0,3) + '-' + contractnbr.substring(4);                                           
                                }
                                else
                                    contractnbr=field.trim();  
                                    loanPaymenttrans.put(mapEBankingApiNames.get(col).API_Name__c,contractnbr);
                                    setContractnbr.add(contractnbr);
                            }
                            else 
                                loanPaymenttrans.put(mapEBankingApiNames.get(col).API_Name__c,field.replaceAll('"','').trim());
                            
                        }       
                    }                                   
                   lstPaymentTransactions.add(loanPaymenttrans);
                   }    
                }            
       
            }
        }
    }
        catch(Exception e){
         if(!Test.IsRunningTest() && !bIsSandBox)
            WebToSFDC.notifyDev('ParseEBankingReportBatch.constructor Error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());   
        }       
    }
    // Start Method
    global List<loan__Loan_Payment_Transaction__c> start(Database.BatchableContext BC){
        return lstPaymentTransactions;
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<loan__Loan_Payment_Transaction__c> scope){
        system.debug('==scope==' + scope); 
        
        if(scope!=null){
            map<string,string> mapTransIDandContractContactID= new map<string,string>();
            map<string,string> mapConditionInDB= new map<string,string>(), mapConatctIDName = new map<string,string>();
            List<loan__Repayment_Transaction_Adjustment__c> lstRevesalTransaction=new List<loan__Repayment_Transaction_Adjustment__c>();
            loan__Repayment_Transaction_Adjustment__c objTransactionAdjustment;
            AANNotificationJob objAANNotificationJob =new AANNotificationJob ();
            List<Note> lstNote=new List<Note>();
            Note objContractNote,objContactNote;
            string htmlBody,subject;
            list<logging__c> logList = new list<logging__c>(); 
            Map<string,id> isRecentCheckMap = new Map<string,id>();
            Map<id,string> transactionNameMap = new Map<id,string>();
            set<string> duplicateRecordsinfile=new set<string>();
            Map<string,string> mapOtherTransaction = New Map<string,string>();
            try { 
                 //fetched records from other Loan Payments 
                 for (AggregateResult ar : [select loan__Loan_Account__c,Min(loan__Loan_Account__r.Name) Name,Max(CreatedDate) CDate from loan__Other_Transaction__c where loan__Loan_Account__r.Name IN :setContractnbr  GROUP BY loan__Loan_Account__c])  {
                   mapOtherTransaction.put(string.ValueOf(ar.get('Name')),string.ValueOf(ar.get('CDate') ));
                  }
                 
                //fetched a list with above LAIs and PaymentMode should be ACH                               
                //Created Map with key as combination of conditioned values and value as Transactionid, ContractId and ConatctID
                for(loan__Loan_Payment_Transaction__c paym : [Select Id,createdDate,loan__Reversed__c,loan__Payment_Mode__c,
                                                            Account_Name__c,loan__Transaction_Date__c,loan__Transaction_Amount__c,
                                                            Name,loan__Loan_Account__r.Name,loan__Loan_Account__r.Id,
                                                            loan__Loan_Account__r.Customer_Name__c,loan__Loan_Account__r.loan__Contact__c
                                                             from loan__Loan_Payment_Transaction__c Where 
                                                            loan__Loan_Account__r.Name IN :setContractnbr                                                                                                               
                                                             order by loan__Transaction_Date__c,createddate]) {                  
                  if(paym.loan__Reversed__c == false)                                                                                 
                    isRecentCheckMap.put(paym.loan__Loan_Account__r.Name, paym.id); 
                    paym.loan__Transaction_Amount__c  = paym.loan__Transaction_Amount__c.setScale(2);
                    transactionNameMap.put(paym.id, paym.name);
                    if(paym.loan__Payment_Mode__c == Label.ACH_Payment_Mode){
                        if(paym.Account_Name__c <> null){mapConditionInDB.put(paym.loan__Loan_Account__r.Name + 'DBQT' + paym.loan__Transaction_Date__c + 'DBQT' + paym.loan__Transaction_Amount__c + 'DBQT' + string.valueOf(paym.Account_Name__c.replace('\'','SGQT')).toLowerCase(),paym.Id + 'DBQT' + paym.loan__Loan_Account__r.Id + 'DBQT' + paym.loan__Loan_Account__r.loan__Contact__c);}
                        if(paym.loan__Loan_Account__r.Customer_Name__c <> null){mapConditionInDB.put(paym.loan__Loan_Account__r.Name + 'DBQT' + paym.loan__Transaction_Date__c + 'DBQT' + paym.loan__Transaction_Amount__c + 'DBQT' + string.valueOf(paym.loan__Loan_Account__r.Customer_Name__c.replace('\'','SGQT')).toLowerCase(),paym.Id + 'DBQT' + paym.loan__Loan_Account__r.Id + 'DBQT' + paym.loan__Loan_Account__r.loan__Contact__c);}
                        mapTransIDandContractContactID.put(paym.Id,paym.loan__Loan_Account__r.Id + 'DBQT' + paym.loan__Loan_Account__r.loan__Contact__c + 'DBQT' + paym.loan__Transaction_Date__c + 'DBQT' + paym.loan__Loan_Account__r.Name + 'DBQT' + paym.loan__Transaction_Amount__c + 'DBQT' + paym.Name);   
                        mapConatctIDName.put(paym.loan__Loan_Account__r.Name, paym.loan__Loan_Account__r.Id);
                        
                    }
                    //system.debug('=========mapConditionInDB========='+mapConditionInDB);
                    //system.debug('=========isRecentCheckMap========='+isRecentCheckMap);
                    
                }                            
                
                //Traverse from excel with key as conditioned values and value as return Reason and Date to save in Reversal object
                for(loan__Loan_Payment_Transaction__c obj :scope ){
                   system.debug('==obj==' + obj);
                    string value=obj.loan__Reversal_Reason__c + 'DBQT' + obj.loan__Rejection_Date__c;  
                    //Date OtherPaymentDate;                                   
                    string key1 = getKey(obj,obj.loan__Transaction_Date__c);
                    string[] ContractNames=key1.split('DBQT');
                    string ContractName = ContractNames[0];
                    //#2943 To stop added Duplicate records in lstRevesalTransaction
                    if(duplicateRecordsinfile.contains(key1)==false){
                        duplicateRecordsinfile.add(key1);
                        string key2 = getKey(obj,obj.loan__Transaction_Date__c - 1);
                        string key3 = getKey(obj,obj.loan__Transaction_Date__c - 2);
                        string key4 = getKey(obj,obj.loan__Transaction_Date__c - 3); 
                    
                    if(mapConditionInDB.containsKey(key1) || mapConditionInDB.containsKey(key2) || mapConditionInDB.containsKey(key3) || mapConditionInDB.containsKey(key4)){
                            string finalKey = mapConditionInDB.containsKey(key1)? key1: (mapConditionInDB.containsKey(key2)?key2: ((mapConditionInDB.containsKey(key3)?key3: key4)));                                                                               
                            string[] reversalValue=value.split('DBQT');
                            string[] transactionID=mapConditionInDB.get(finalKey).split('DBQT');
                            string[] finalKeyarray = finalKey.split('DBQT');
                            Date TransactionDate=Date.valueOf(string.valueOf(finalKeyarray[1].trim()));
                            
                               //if(mapOtherTransaction.containsKey(finalKeyarray[0]))
                                 //OtherPaymentDate=Date.valueOf(string.valueOf(mapOtherTransaction.get(finalKeyarray[0])) );
                        
                            //system.debug('==OtherPaymentDate==' + OtherPaymentDate);
                            
                            if(isRecentCheckMap.containsKey(ContractName) && isRecentCheckMap.get(ContractName) == transactionID[0]){
                              //#2943 to stop addind records in lstRevesalTransaction if Other Loan Payment has a new Entry or is modified recently  
                              Date dt=Date.valueOf(string.valueOf(reversalValue[1].trim()));                    
                              objTransactionAdjustment=new loan__Repayment_Transaction_Adjustment__c(loan__Loan_Payment_Transaction__c=transactionID[0], loan__Reason_Code__c=reversalValue[0],loan__Adjustment_Txn_Date__c= dt);
                              if(mapOtherTransaction.containsKey(finalKeyarray[0])){
                                if(Date.valueOf(string.valueOf(mapOtherTransaction.get(finalKeyarray[0])) ) < TransactionDate)                                 
                                   lstRevesalTransaction.add(objTransactionAdjustment);                                 
                                else
                                  logList.add(getLogging('ParseEBankingReportBatch', (string)mapConatctIDName.get(ContractName),'ParseEBankingReportBatch.execute' , 'Transaction '+(string)transactionNameMap.get(transactionID[0])+' cannot be reversed as Other Loan Transaction has a new entry of Contract: ' + ContractName + '.'));            
                               }
                               else lstRevesalTransaction.add(objTransactionAdjustment); 
                            }else{
                                logList.add(getLogging('ParseEBankingReportBatch', (string)mapConatctIDName.get(ContractName),'ParseEBankingReportBatch.execute' , 'Transaction '+(string)transactionNameMap.get(transactionID[0])+' cannot be reversed as either it is not the recent transaction of Contract: ' +ContractName+ ' or is already reversed transaction.'));            
                            }   
                        
                    }else{
                        if(mapConatctIDName.containsKey(ContractName))  
                            logList.add(getLogging('ParseEBankingReportBatch', (string)mapConatctIDName.get(ContractName),'ParseEBankingReportBatch.execute' , 'Record does not match for ' + ContractName + ' .'));            
                        else
                            logList.add(getLogging('ParseEBankingReportBatch', null ,'ParseEBankingReportBatch.execute', 'Record not found for ' + ContractName +'.' ));            
                        
                    }
                  }
                  else logList.add(getLogging('ParseEBankingReportBatch', (string)mapConatctIDName.get(ContractName),'ParseEBankingReportBatch.execute' , 'Transaction cannot be reversed as Dupliacte Records found for the Contract: ' + ContractName + '.'));    
                }
                system.debug('==list to insert in Reversal Object=='+ lstRevesalTransaction);
                //Insert list of reversal payments
                Database.SaveResult[] insertResult = Database.insert(lstRevesalTransaction,false);
                system.debug('=======insertResults==========' + insertResult);
                for(Integer i=0;i < insertResult.size();i++){ 
                    if (!insertResult.get(i).isSuccess()) {
                        Database.Error error = insertResult.get(i).getErrors().get(0);
                        System.debug(error.getStatusCode() + ': ' + error.getMessage());
                        if(mapTransIDandContractContactID.containsKey(lstRevesalTransaction.get(i).loan__Loan_Payment_Transaction__c)){
                            string[] value= mapTransIDandContractContactID.get(lstRevesalTransaction.get(i).loan__Loan_Payment_Transaction__c).split('DBQT');
                            logList.add(getLogging('ParseEBankingReportBatch', (string)value[0],'ParseEBankingReportBatch.execute' ,'Failed Repayment Reversal Transactions for ' + (string)value[5] + ' Description: ' + error.getMessage()));         
                        }
                    }
                    else{
                        if(mapTransIDandContractContactID.containsKey(lstRevesalTransaction.get(i).loan__Loan_Payment_Transaction__c)){
                            string[] value= mapTransIDandContractContactID.get(lstRevesalTransaction.get(i).loan__Loan_Payment_Transaction__c).split('DBQT');
                            
                            Date dt = Date.valueOf(value[2]);
                            string transactionDate = Datetime.newInstance(dt.year(), dt.month(),dt.day(),0,0,0).format('MM/dd/YYYY'); 
                            subject='Payment Has Been Flagged as Reversed';                            
                            htmlBody='The payment for ' + transactionDate + ' on ' + value[3] + ' for $'+ value[4] + ' has been reversed.';
                            
                            objContractNote=objAANNotificationJob.createNote(htmlBody,subject,value[0]);
                            objContactNote=objAANNotificationJob.createNote(htmlBody,subject,value[1]);
                            lstNote.add(objContractNote);
                            lstNote.add(objContactNote);
                        }
                    }
                    
                }
                system.debug('==list to insert in note=='+ lstNote);
                if(lstNote!=null) insert lstNote; 
            }
            catch(exception e){
             if(!Test.IsRunningTest() && !bIsSandBox)
                WebToSFDC.notifyDev('ParseEBankingReportBatch.execute Error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());  
            }
            finally {
                if(logList!=null) insert logList;  
            }
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        // Logic to be Executed at finish 
        if(!Test.IsRunningTest() && !bIsSandBox)
            WebToSFDC.notifyDev('Parse EBanking Report Batch Completed', 'Parse EBanking Report completed for Today.');          
        
    }
    
    public static Logging__c getLogging(string webServiceName,string contractId, string request, string response){
        Logging__c Log = new Logging__c(Webservice__c = webServiceName,API_Request__c = request,API_Response__c = response,CL_Contract__c = contractId); 
        return Log;
    }
    
    public static string getKey(loan__Loan_Payment_Transaction__c trans, date transactionDate){
        string key = trans.loan__Loan_Account__c + 'DBQT' + transactionDate + 'DBQT' + trans.loan__Transaction_Amount__c + 'DBQT' + string.valueOf(trans.Customer_Name__c).toLowerCase();
        return key;
    }

    Webservice static String executeReturnFile() {
        ParseEBankingReportBatch j = new ParseEBankingReportBatch();
        Database.executeBatch(j);
        return 'Started process. Please check the return report after 5 minutes!';
    }
        
}