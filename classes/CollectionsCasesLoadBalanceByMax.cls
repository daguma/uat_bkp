global class CollectionsCasesLoadBalanceByMax {

    public Integer maxCurrentIndividualLoad = 0; 
    public Decimal maxCurrentIndividualLoadModified, individualLoadByMaxModifiedAbsSum = 0;
    public Map<Id, Decimal> individualLoadByMaxModifiedMap, individualLoadByMaxModifiedAbsMap;
    public Map<Id, Long> individualFutureLoadMap;
    public Id individualWithLowestFutureLoad, individualWithHighestFutureLoad;

    public CollectionsCasesLoadBalanceByMax(Integer totalInflow, Map<Id, Integer> currentIndividualsLoadMap) {

        //Get the maximum amount of cases that are already assigned to a user in the group
        Integer individualLoad = 0;
        for ( Id individualId : currentIndividualsLoadMap.keySet() ){
            individualLoad = currentIndividualsLoadMap.get(individualId) != null ? currentIndividualsLoadMap.get(individualId) : 0; 
            if(this.maxCurrentIndividualLoad < individualLoad){
                this.maxCurrentIndividualLoad = individualLoad;
            }
        }

        //Modify that maximum by a factor of 105%
        this.maxCurrentIndividualLoadModified = this.maxCurrentIndividualLoad * 105 / 100;

        //Get the quotient(1), where the dividend is the current load and the divisor is the modified max
        //Then get the inverse of that quotient(1) and add up all this numbers as a "group total"
        this.individualLoadByMaxModifiedMap = new Map<Id, Decimal>();
        this.individualLoadByMaxModifiedAbsMap = new Map<Id, Decimal>();
        Decimal individualLoanByMaxModified;
        for(Id individualId : currentIndividualsLoadMap.keySet() ){
            individualLoanByMaxModified = currentIndividualsLoadMap.get(individualId) / this.maxCurrentIndividualLoadModified;
            this.individualLoadByMaxModifiedMap.put(individualId,individualLoanByMaxModified);

            this.individualLoadByMaxModifiedAbsMap.put(individualId, math.abs(1-individualLoanByMaxModified) );

            this.individualLoadByMaxModifiedAbsSum += math.abs(1-individualLoanByMaxModified);
        }

        //Calculate the quotient(2) where the dividend is the "inverse" and the divisor is the "group total"
        //The future load is calculated by: Original Load * quotient(2)
        this.individualFutureLoadMap = new Map<Id, Long>();
        Long lowestFutureLoad, highestFutureLoad; Boolean firstIteration = true;
        Long totalAssignedLoad = 0;
        for(Id individualId : currentIndividualsLoadMap.keySet() ){
            Decimal temp = this.individualLoadByMaxModifiedAbsMap.get(individualId) / this.individualLoadByMaxModifiedAbsSum;
            temp = temp * totalInflow;
            this.individualFutureLoadMap.put(individualId,temp.round(System.RoundingMode.HALF_UP));
            totalAssignedLoad = totalAssignedLoad + temp.round(System.RoundingMode.HALF_UP);
            if(firstIteration){
                firstIteration = false;
                lowestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                highestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                this.individualWithLowestFutureLoad = individualId;
                this.individualWithHighestFutureLoad = individualId;
            }else{
                if(temp.round(System.RoundingMode.HALF_UP) < lowestFutureLoad){
                    lowestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                    this.individualWithLowestFutureLoad = individualId;
                }
                if(temp.round(System.RoundingMode.HALF_UP) > highestFutureLoad){
                    highestFutureLoad = temp.round(System.RoundingMode.HALF_UP);
                    this.individualWithHighestFutureLoad = individualId;
                }
            }
        }

        //If the algorithm assigned more or less items than the inflow, then fix it here
        Long loadDifference = (long)totalInflow - totalAssignedLoad;
        if(loadDifference < 0){
            //This means that the algorithm assigned more load than the totalInflow
            Long assignedLoad = this.individualFutureLoadMap.get(individualWithHighestFutureLoad);
            this.individualFutureLoadMap.remove(individualWithHighestFutureLoad);
            this.individualFutureLoadMap.put(individualWithHighestFutureLoad, assignedLoad + loadDifference);
        }else if(loadDifference > 0) {
            //This means that the algorithm assigned less load than the totalInflow
            Long assignedLoad = this.individualFutureLoadMap.get(individualWithLowestFutureLoad);
            this.individualFutureLoadMap.remove(individualWithLowestFutureLoad);
            this.individualFutureLoadMap.put(individualWithLowestFutureLoad, assignedLoad + loadDifference);
        }

    }
}