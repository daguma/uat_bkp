@isTest public class DealAutomaticAssignmentManagerTest {

    @isTest static void Execute_User_process_Validate_Assignment() {

        TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimesAllAssignedToKennesaw();
        
        Group gpr = [SELECT Id, Name, Type, Email, OwnerId 
                     FROM Group 
                     Where Name = 'DAA_Channel_PON'];
        
        DAA_Logic__c DAA_Log = New DAA_Logic__c();
        DAA_Log.Name = 'PON';
        DAA_Log.priority__c = 1;
        DAA_Log.propName__c = 'ProductName__c' ;
        DAA_Log.propValues__c = 'Point Of Need';
        DAA_Log.groupId__c = gpr.Id;
        insert DAA_Log;

        Boolean isDebugMode = false;
        
 
        Opportunity app = LibraryTest.createApplicationTH();
        app.Status__c = 'Credit Qualified';
        app.OwnerId = Label.Default_Lead_Owner_ID;
        app.Automatically_Assigned__c = false;
        app.Last_CQ_Date__c = Date.today().addDays(-9);
        app.ProductName__c = 'Point Of Need';
        app.Type = 'New';
        update app;
        
        String query = DealAutomaticAssignmentManager.getQueryOppInCQ();
        System.assert(!String.isBlank(query));
        Test.startTest();
        DealAutomaticAssignmentManager.assignUserProcess(app, new DealAutomaticAssignmentManager.GlobalParameters(isDebugMode));
        Test.stopTest();
        
        system.assertEquals(True, app.Automatically_Assigned__c);
        
        System.assertNotEquals(Null, app.OwnerId);
        
        System.assertNotEquals(Null, app.Automatically_Assigned_Date__c);
                            
        System.assertNotEquals(Null, app.AssignmentDate__c);
        
        

    }

    @isTest static void Insert_Note(){
        
        
        TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimesAllAssignedToKennesaw();
        
        Boolean isDebugMode = false;
        
        Group gpr = [SELECT Id, Name, Type, Email, OwnerId 
                     FROM Group 
                     Where Name = 'DAA_Channel_PON'];

        DAA_Logic__c DAA_Log = New DAA_Logic__c();
        DAA_Log.Name = 'PON';
        DAA_Log.priority__c = 1;
        DAA_Log.propName__c = 'ProductName__c' ;
        DAA_Log.propValues__c = 'Point Of Need';
        DAA_Log.groupId__c = gpr.Id;
        insert DAA_Log;

        Opportunity app2 = LibraryTest.createApplicationTH();
        app2.Status__c = 'Credit Qualified';
        app2.OwnerId = Label.Default_Lead_Owner_ID;
        app2.Automatically_Assigned__c = false;
        app2.Last_CQ_Date__c = Date.today().addDays(-25);
        app2.Lead_Sub_Source__c = 'LendingTree';
        app2.ProductName__c = 'Point Of Need';
        app2.Type = 'New';
        update app2;

        Test.startTest();
        DealAutomaticAssignmentManager.assignUserProcess(app2, new DealAutomaticAssignmentManager.GlobalParameters(isDebugMode));
		Test.stopTest();
        
        Note n = [SELECT Id, Title, Body, ParentId FROM Note where ParentId =: app2.Id];
        
        system.assertEquals('New Deal Assigned To You - null', n.Title);

    }
    
    
    
    @isTest static void assignUser_ByAvailableState(){
        
        Sales_Reps_Assignments__c lp = New Sales_Reps_Assignments__c();
        lp.Sales_Assignments_Counter__c = null;
        insert lp;
        
        Opportunity app2 = LibraryTest.createApplicationTH();
        app2.Status__c = 'Incomplete Package';
        app2.OwnerId = Label.Default_Lead_Owner_ID;
        app2.Automatically_Assigned__c = false;
        app2.Last_CQ_Date__c = Date.today().addDays(-25);
        app2.Lead_Sub_Source__c = 'LendingTree';
        app2.ProductName__c = 'Point Of Need';
        app2.Type = 'New';
        update app2;
        
        DealAutomaticAssignmentManager.AssignmentParams params = new DealAutomaticAssignmentManager.AssignmentParams('OA_IP');
        params.salesCustom.Sales_Assignments_GroupId__c = '000test';
        params.salesCustom.Sales_Assignments_Counter__c = 0;
        Integer counterTMP = params.counterTMP;
        List<id> ids = new list<id>();
        Id test = UserInfo.getUserId();
        ids.add(test);
        params.userIds = ids;
        String query = DealAutomaticAssignmentManager.getQueryOppInOAandIP();
        DealAutomaticAssignmentManager.assignUserByAvailableState(app2, params.userIds, counterTMP);

    }
    @isTest static void codeCoverage_CallEmpty_WebMethods(){
        
        
        TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimesAllAssignedToKennesaw();
        
        Boolean isDebugMode = false;
        
        Group gpr = [SELECT Id, Name, Type, Email, OwnerId 
                     FROM Group 
                     Where Name = 'DAA_Channel_PON'];

        DAA_Logic__c DAA_Log = New DAA_Logic__c();
        DAA_Log.Name = 'PON';
        DAA_Log.priority__c = 1;
        DAA_Log.propName__c = 'ProductName__c' ;
        DAA_Log.propValues__c = 'Point Of Need';
        DAA_Log.groupId__c = gpr.Id;
        insert DAA_Log;
        
        Lead led = LibraryTest.createLeadTH();

        Opportunity app2 = LibraryTest.createApplicationTH();
        app2.Status__c = 'Credit Qualified';
        app2.OwnerId = Label.Default_Lead_Owner_ID;
        app2.Automatically_Assigned__c = false;
        app2.Last_CQ_Date__c = Date.today().addDays(-25);
        app2.Lead_Sub_Source__c = 'LendingTree';
        app2.ProductName__c = 'Point Of Need';
        app2.Type = 'New';
        update app2;

        Test.startTest();
        DealAutomaticAssignmentManager.assignExistingAppFromWeb(app2.Id); //Only for coverage Empty method.
        DealAutomaticAssignmentManager.assignExistingAppFromWeb(app2.Id, true); //Only for coverage Empty method.
        //DealAutomaticAssignmentManager.assignExistingLeadFromWeb(led); //Only for coverage Empty method. ver este no es static
		Test.stopTest();


    }

}