@IsTest
public class DocusignEnvelopeCreateResponse_Test {
    
    static testMethod void testParse() {
        String json = '{'+
        '    \"envelopeId\": \"cfeff4e8-c132-4480-9bb8-affcfe11c322\",'+
        '    \"uri\": \"/envelopes/cfeff4e8-c132-4480-9bb8-affcfe11c322\",'+
        '    \"statusDateTime\": \"2018-01-15T14:50:04.1974714Z\",'+
        '    \"status\": \"sent\"'+
        '}';
        DocusignEnvelopeCreateResponse obj = DocusignEnvelopeCreateResponse.parse(json);
        System.assert(obj != null);
    }
}