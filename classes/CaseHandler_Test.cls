@isTest
global class CaseHandler_Test implements HttpCalloutMock {

    testmethod static void test1(){
        Contact c = LibraryTest.createContactTH();
        c.Email = 'test@test.com';
        update c;
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Contact__c = c.Id;
        contract.loan__loan_status__c = 'Active - Good Standing';
        update contract;
        
        Case cs = new Case();
        cs.SuppliedEmail = 'test@test.com';
        cs.Subject = 'LAI-12345678 is for test';
        insert cs;
        cs.SuppliedEmail = 'test1@test.com';
        update cs;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
         // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"example":"test","accessToken":"test"}');
        res.setStatusCode(200);
        return res;
       
    }
    
   

}