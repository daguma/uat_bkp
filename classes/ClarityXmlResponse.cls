global class ClarityXmlResponse {

    public String tracking_Number{get;set;}
    public String action{get;set;}
    public String deny_codes{get;set;}
    public String deny_descriptions{get;set;}
    public String exception_descriptions{get;set;}
    public String filter_codes{get;set;}
    public String filter_descriptions{get;set;}
    public String identity_theft_prevention{get;set;}
    public ClarityClearProductsRequest clear_products_request{get;set;}
    public ClarityInquiry inquiry{get;set;}
    public ClarityClearFraud clear_fraud{get;set;}
}