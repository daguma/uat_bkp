@isTest
private class RefinancePostSoftPullTest {
 
    @testSetup static void initialize() {
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7',
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO');
        insert FinwiseParams;

        Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name = 'POS', Asset_Id__c = 5, SFDC_Value__c = false);
        insert AsetCls;
        
        Refinance_Settings__c refiSet = new Refinance_Settings__c();
        refiSet.name = 'settings';
        refiSet.Eligibility_Days__c = 30;
        refiSet.FICO_Approved__c = 585;
        refiSet.Days_To_First_Check__c = 30;
        insert refiSet;
        
        Account acc = new Account();
        acc.Name = 'Refinance';
        insert acc;
    }
     
   
    @isTest static void creates_new_application_for_refinance_b2() {

        loan__loan_Account__c contract = creates_contract();

    	Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;

        Integer appsBefore = [SELECT COUNT() FROM Opportunity]; 

        RefinancePostSoftPull.executeProcess(contract.Id,refinanceParams);

        System.assert([SELECT COUNT() FROM Opportunity] > appsBefore);
    }

    @isTest static void creates_new_application_for_refinance_b1() {

        loan__loan_Account__c contract = creates_contract();

        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B1';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];

        RefinancePostSoftPull.executeProcess(contract.Id,refinanceParams);

        System.assert([SELECT COUNT() FROM Opportunity] > appsBefore);
    }

    @isTest static void creates_new_application_for_refinance_a2() {

        loan__loan_Account__c contract = creates_contract();

        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'A2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];

        RefinancePostSoftPull.executeProcess(contract.Id,refinanceParams);

        System.assert([SELECT COUNT() FROM Opportunity] > appsBefore);
    }

    @isTest static void creates_new_application_for_refinance_a1() {

        loan__loan_Account__c contract = creates_contract();
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'A1';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];

        RefinancePostSoftPull.executeProcess(contract.Id,refinanceParams);

        System.assert([SELECT COUNT() FROM Opportunity] > appsBefore);
    }

    @isTest static void creates_new_application_for_refinance_c1() {

        loan__loan_Account__c contract = creates_contract();

        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'C1';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];

        RefinancePostSoftPull.executeProcess(contract.Id,refinanceParams);

        System.assert([SELECT COUNT() FROM Opportunity] > appsBefore);
    }

    @isTest static void creates_new_application_for_refinance_c1_low_grow() {
        
        loan__loan_Account__c contract = creates_contract();

        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Envelope_Status__c = 'Completed';
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        fundedApp.StrategyType__c = '2';
        upsert fundedApp;
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-33);
        upsert refinanceParams;

        Integer appsBefore = [SELECT COUNT() FROM Opportunity]; 

        RefinancePostSoftPull.executeProcess(contract.Id,refinanceParams);

        System.assert([SELECT COUNT() FROM Opportunity] > appsBefore);
    }
    
    @isTest static void is_grade_approved() {
        Boolean result = RefinancePostSoftPull.isGradeApproved('B1', 'A1');
        System.assertEquals(true, result);

        result = RefinancePostSoftPull.isGradeApproved('B2', 'A1');
        System.assertEquals(true, result);

        result = RefinancePostSoftPull.isGradeApproved('C1', 'A1');
        System.assertEquals(true, result);

        result = RefinancePostSoftPull.isGradeApproved('F', 'A1');
        System.assertEquals(true, result);

        result = RefinancePostSoftPull.isGradeApproved('A1', 'A1');
        System.assertEquals(true, result);

        result = RefinancePostSoftPull.isGradeApproved('A2', 'A1');
        System.assertEquals(true, result);
    }
    
    @isTest static void is_better_grade(){
        Boolean result = RefinancePostSoftPull.isBetterGrade('A1', 'A1', false);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isBetterGrade('A2', 'A1', false);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isBetterGrade('B1', 'A1', false);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isBetterGrade('B2', 'A1', false);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isBetterGrade('C1', 'A1', false);
        
        result = RefinancePostSoftPull.isBetterGrade('C1', 'A2', false);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isBetterGrade('C1', 'B2', false);
        System.assertEquals(true, result);

        result = RefinancePostSoftPull.isBetterGrade('C1', 'B2', false);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isBetterGrade('C2', 'D', false);
        System.assertEquals(false, result);
        
        result = RefinancePostSoftPull.isBetterGrade('D', 'C1', false);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isBetterGrade('E', 'B2', false);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isBetterGrade('F', 'A1', false);
        System.assertEquals(true, result);

        System.assert(RefinancePostSoftPull.isBetterGrade('F', 'B1', false));        
    }
    
    @isTest static void is_better_fico_low_and_grow(){
        Boolean result = RefinancePostSoftPull.isBetterFicoLowAndGrow(650, 690);
        System.assertEquals(true, result);
    }

    @isTest static void is_fico_approved() {
        Boolean result = RefinancePostSoftPull.isFicoApproved(600, 700);
        System.assertEquals(true, result);
        
        result = RefinancePostSoftPull.isFicoApproved(650, 580);
        System.assertEquals(false, result);
    }
    
    @isTest static void is_payoff_approved() {
        Boolean result1 = RefinancePostSoftPull.isPayoffApproved('A1', 8000);
        System.assertEquals(false, result1);
        
        Boolean result2 = RefinancePostSoftPull.isPayoffApproved('A2', 7500);
        System.assertEquals(false, result2);
        
        Boolean result3 = RefinancePostSoftPull.isPayoffApproved('B1', 8000);
        System.assertEquals(false, result3);
        
        Boolean result4 = RefinancePostSoftPull.isPayoffApproved('B2', 8600);
        System.assertEquals(false, result4);
        
        Boolean result5 = RefinancePostSoftPull.isPayoffApproved('C1', 8000);
        System.assertEquals(false, result5);
    }

    @isTest static void gets_offers() {

        Opportunity app = LibraryTest.createApplicationTH();

        Offer__c off = new Offer__c();
        off.IsSelected__c = true;
        off.Opportunity__c = app.Id;
        off.APR__c = 0.2;
        off.Fee__c = 100;
        off.Fee_Percent__c = 0.04;
        off.FirstPaymentDate__c = Date.today().addDays(-29);
        off.Installments__c = 34;
        off.Loan_Amount__c = 5000;
        off.Term__c = 34;
        off.Payment_Amount__c = 230;
        off.Repayment_Frequency__c = 'Monthly';
        off.Preferred_Payment_Date__c = Date.today().addDays(-23);
        insert off;

        List<Offer__c> resultList = RefinancePostSoftPull.getOffers(app.Id);

        System.assert(resultList.size() > 0, resultList.size());

        Offer__c result = RefinancePostSoftPull.getSelectedOffer(app.Id);

        System.assertEquals(off.Id, result.Id);
    }
 
    @isTest static void inserts_note_with_funded_offer() {

        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
		
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        contract.loan__Principal_Remaining__c = 2500;
        contract.loan__Pay_Off_Amount_As_Of_Today__c = 2500;
        contract.loan__Principal_Paid__c = 200;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT Id 
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList[0];
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        
        refinanceParams.FICO__c = 650;
        refinanceParams.Grade__c = 'C1';
        refinanceParams.Eligible_For_Refinance__c = true;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-35);
        refinanceParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false';
        upsert refinanceParams;
        
        refinanceParams = [SELECT Id, Contract__c, FICO__c, Grade__c, New_FICO__c, 
			New_Grade__c, Eligible_For_Refinance__c, IsLowAndGrow__c, Last_Refinance_Eligibility_Review__c, 
			Contract__r.loan__Principal_Remaining__c, Contract__r.loan__Fees_Remaining__c, System_Debug__c, 
			Contract__r.loan__Interest_Accrued_Not_Due__c, Contract__r.loan__Pay_Off_Amount_As_Of_Today__c, 
			Eligible_For_Soft_Pull_Params__c, Eligible_For_Soft_Pull__c
                FROM loan_Refinance_Params__c
                WHERE Id = : refinanceParams.Id];
        
        Loan_Refinance_Params_Logs__c refiLogs = new Loan_Refinance_Params_Logs__c();
        refiLogs.Loan_Refinance_Params__c = refinanceParams.Id;
        insert refiLogs;

        Opportunity newApp = [SELECT Id, Name, Status__c, Contact__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.CreditPull_Date__c = Date.today().addDays(-60);
        newApp.Status__c = 'Refinance Unqualified';
        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.RepaymentFrequency__c = '28 Days';

        Decimal loanAmount = (((contract.loan__Principal_Remaining__c +
                                contract.loan__Fees_Remaining__c +
                                contract.loan__Interest_Accrued_Not_Due__c) / 100)
                              .round(System.RoundingMode.CEILING)) * 100;
        newApp.Amount = loanAmount > 3500 ? loanAmount : 3500;
        update newApp;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'A1';
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;
        
        Test.startTest();

	    RefinancePostSoftPull.eligibilityCheckGetCR(refinanceParams);
      
        Test.stopTest();

        Note newNote = [SELECT Id, ParentId, Title FROM Note WHERE ParentId = : newApp.Id AND Title like 'Offers in Funded%' ORDER BY CreatedDate DESC LIMIT 1];
        newApp = [SELECT Id, Name, Status__c, Contact__r.Name FROM Opportunity WHERE Id = : appId LIMIT 1];

        System.assertEquals('Refinance Qualified', newApp.Status__c);
        System.assertNotEquals(null, newNote);
        
        String owner = RefinancePostsoftPull.getOldRefinanceAppOwner(newApp.Contact__c, newApp.Id);
        System.assertEquals('', owner);
    }
     
    @isTest static void fails_fico_grade() {
        
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Envelope_Status__c = 'Completed';
        opp.Status__c = 'Funded';
        opp.StrategyType__c = '1';
        update opp;
        
        loan__loan_Account__c contract = creates_contract();
        contract.Opportunity__c = opp.Id;
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        update contract;
        
        loan__Loan_Account__c cont = [SELECT Id, Name, Opportunity__r.StrategyType__c, loan__Pay_Off_Amount_As_Of_Today__c, Original_Loan_Amount_No_Fee__c, Payment_Frequency_Masked__c, loan__Loan_Status__c, loan__Contact__c, loan__Contact__r.Military__c, loan_Active_Payment_Plan__c, Original_Sub_Source__c, overdue_Days__c, principal_Balance_Paid_Percentage__c, loan__Oldest_Due_Date__c 
            FROM loan__Loan_Account__c 
            where id =: contract.Id];
        
        SubmitApplicationService submit = new SubmitApplicationService();
        String appId = submit.submitApplication(contract.loan__Contact__c);
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 850;
        refinanceParams.Grade__c = 'A1';
        refinanceParams.Eligible_For_Refinance__c = true;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-35);
        refinanceParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false';
        upsert refinanceParams;
        
        system.debug('refiParams: ' + refinanceParams);
        
        Loan_Refinance_Params_Logs__c refiLogs = new Loan_Refinance_Params_Logs__c();
        refiLogs.Loan_Refinance_Params__c = refinanceParams.Id;
        insert refiLogs;

        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.RepaymentFrequency__c = '28 Days';

        Decimal loanAmount = (((contract.loan__Principal_Remaining__c +
                                contract.loan__Fees_Remaining__c +
                                contract.loan__Interest_Accrued_Not_Due__c) / 100)
                              .round(System.RoundingMode.CEILING)) * 100;
        newApp.Amount = loanAmount > 3500 ? loanAmount : 3500;
        update newApp;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B1';
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;
        
        Test.startTest();
        
        RefinancePostSoftPull.eligibilityCheckGetCR(refinanceParams);
      
        Test.stopTest();

        System.assertEquals(true, [SELECT Post_Pull_FICO_Grade_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c =: refinanceParams.Id ORDER BY CreatedDate DESC LIMIT 1].Post_Pull_FICO_Grade_Fail__c);
    }
    
    @isTest static void fails_brms_rules() {

        LibraryTest.createBrmsConfigSettings(3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
		
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        
        refinanceParams.FICO__c = 650;
        refinanceParams.Grade__c = 'C1';
        refinanceParams.Eligible_For_Refinance__c = true;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-35);
        refinanceParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false';
        upsert refinanceParams;
        
        Loan_Refinance_Params_Logs__c refiLogs = new Loan_Refinance_Params_Logs__c();
        refiLogs.Loan_Refinance_Params__c = refinanceParams.Id;
        insert refiLogs;

        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.RepaymentFrequency__c = '28 Days';

        Decimal loanAmount = (((contract.loan__Principal_Remaining__c +
                                contract.loan__Fees_Remaining__c +
                                contract.loan__Interest_Accrued_Not_Due__c) / 100)
                              .round(System.RoundingMode.CEILING)) * 100;
        newApp.Amount = loanAmount > 3500 ? loanAmount : 3500;
        update newApp;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'A1';
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;
        
        Test.startTest();

	    RefinancePostSoftPull.eligibilityCheckGetCR(refinanceParams);
      
        Test.stopTest();

        System.assertEquals(true, [SELECT Soft_Pull_Decline__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c =: refinanceParams.Id ORDER BY CreatedDate DESC LIMIT 1].Soft_Pull_Decline__c);
    }
    
    @isTest static void fails_max_offer_amount() {

        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
		
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, CreditPull_Date__c = Date.today().addDays(-60));
        upsert opp;
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT Id, Contract__c, FICO__c, Grade__c, New_FICO__c, 
			New_Grade__c, Eligible_For_Refinance__c, IsLowAndGrow__c, Last_Refinance_Eligibility_Review__c, 
			Contract__r.loan__Principal_Remaining__c, Contract__r.loan__Fees_Remaining__c, System_Debug__c, 
			Contract__r.loan__Interest_Accrued_Not_Due__c, Contract__r.loan__Pay_Off_Amount_As_Of_Today__c, 
			Eligible_For_Soft_Pull_Params__c, Eligible_For_Soft_Pull__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        
        refinanceParams.FICO__c = 650;
        refinanceParams.Grade__c = 'C1';
        refinanceParams.Eligible_For_Refinance__c = true;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-35);
        refinanceParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false';
        upsert refinanceParams;
        
        Loan_Refinance_Params_Logs__c refiLogs = new Loan_Refinance_Params_Logs__c();
        refiLogs.Loan_Refinance_Params__c = refinanceParams.Id;
        insert refiLogs;

        Opportunity newApp = [SELECT Id, Name, Status__c, Contact__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Status__c = 'Refinance Qualified';
        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.RepaymentFrequency__c = '28 Days';

        Decimal loanAmount = (((contract.loan__Principal_Remaining__c +
                                contract.loan__Fees_Remaining__c +
                                contract.loan__Interest_Accrued_Not_Due__c) / 100)
                              .round(System.RoundingMode.CEILING)) * 100;
        newApp.Amount = loanAmount > 3500 ? loanAmount : 3500;
        update newApp;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'A1';
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;
        
        Test.startTest();

	    RefinancePostSoftPull.eligibilityCheckGetCR(refinanceParams);
      
        Test.stopTest();

        newApp = [SELECT Id, Name, Status__c, Contact__r.Name FROM Opportunity WHERE Id = : appId LIMIT 1];

        System.assertEquals('Refinance Unqualified', newApp.Status__c);
        
        System.assertEquals(true, [SELECT Max_Loan_Amount_Check_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c =: refinanceParams.Id ORDER BY CreatedDate DESC LIMIT 1].Max_Loan_Amount_Check_Fail__c);
        
    }
    
    @isTest static void gets_cr_refinance_low_and_grow() {

        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        contract.loan__Principal_Remaining__c = 1456.09;
        contract.loan__Pay_Off_Amount_As_Of_Today__c = 1456.09;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
        String appId = submit.submitApplication(contract.loan__Contact__c);
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];
        
        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 650;
        refinanceParams.IsLowAndGrow__c = true;
        refinanceParams.Grade__c = 'B1';
        refinanceParams.Eligible_For_Refinance__c = true;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-35);
        refinanceParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id;
        upsert refinanceParams;
        
        Loan_Refinance_Params_Logs__c refiLogs = new Loan_Refinance_Params_Logs__c();
        refiLogs.Loan_Refinance_Params__c = refinanceParams.Id;
        insert refiLogs;

        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.RepaymentFrequency__c = '28 Days';

        Decimal loanAmount = (((contract.loan__Principal_Remaining__c +
                                contract.loan__Fees_Remaining__c +
                                contract.loan__Interest_Accrued_Not_Due__c) / 100)
                              .round(System.RoundingMode.CEILING)) * 100;
        newApp.Amount = loanAmount > 3500 ? loanAmount : 3500;
        update newApp;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'A2';
        score.FICO_Score__c = 720;
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;
        
        Test.startTest();

        RefinancePostSoftPull.eligibilityCheckGetCR(refinanceParams);
      
        Test.stopTest();

        System.assertEquals(false, [SELECT Post_Pull_FICO_Grade_Fail__c FROM Loan_Refinance_Params_Logs__c WHERE Loan_Refinance_Params__c =: refinanceParams.Id ORDER BY CreatedDate DESC LIMIT 1].Post_Pull_FICO_Grade_Fail__c);
    }
    
    @isTest static void evaluates_refinance_opp_after_created_catch_exception() {

        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
		
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 650;
        refinanceParams.Grade__c = 'C1';
        refinanceParams.Eligible_For_Refinance__c = true;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-35);
        refinanceParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id;
        refinanceParams.System_Debug__c = '';
        upsert refinanceParams;

        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.RepaymentFrequency__c = '28 Days';

        Decimal loanAmount = (((contract.loan__Principal_Remaining__c +
                                contract.loan__Fees_Remaining__c +
                                contract.loan__Interest_Accrued_Not_Due__c) / 100)
                              .round(System.RoundingMode.CEILING)) * 100;
        newApp.Amount = loanAmount > 3500 ? loanAmount : 3500;
        update newApp;
        
        Test.startTest();

	    RefinancePostSoftPull.eligibilityCheckGetCR(refinanceParams);
      
        Test.stopTest();

        System.assertNotEquals('', [SELECT System_Debug__c FROM Loan_Refinance_Params__c WHERE Id =: refinanceParams.Id ORDER BY CreatedDate DESC LIMIT 1].System_Debug__c);
    }  

    private static loan__loan_Account__c creates_contract() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        contract.loan__Interest_Accrued_Not_Due__c = 500;
        update contract;

        Offer__c off = new Offer__c();
        off.IsSelected__c = true;
        off.Opportunity__c = contract.Opportunity__c;
        off.APR__c = 0.2;
        off.Fee__c = 100;
        off.Fee_Percent__c = 0.04;
        off.FirstPaymentDate__c = Date.today().addDays(-29);
        off.Installments__c = 34;
        off.Loan_Amount__c = 5000;
        off.Term__c = 34;
        off.Payment_Amount__c = 230;
        off.Repayment_Frequency__c = 'Monthly';
        off.Preferred_Payment_Date__c = Date.today().addDays(-23);
        insert off;

        return contract;
    }
 
}