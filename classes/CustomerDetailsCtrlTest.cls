@isTest
class CustomerDetailsCtrlTest {
	static testMethod void test(){
		Contact con = new Contact(LastName='TestContact',Firstname = 'David',Email = 'test@gmail2.com',MailingState = 'GA');
        insert con;

        Opportunity opp = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id);
        insert opp;

        opp = [select Id, contact__r.name, contact__r.MailingStreet, contact__r.MailingCity,
        			 contact__r.MailingState, contact__r.MailingPostalCode, name, contact__r.Customer_Number__c,
        			 contact__r.Use_of_Funds__c, contact__r.HomePhone, contact__r.MobilePhone, contact__r.Email
        			 from Opportunity where Id =: opp.Id];


        CustomerDetailsCtrl.getCustomerDetails(opp.Id,opp);
	}
}