global class OngoingBankAccountMonitoringJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {
    private String query;
    
    global OngoingBankAccountMonitoringJob(){
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        
    }
    global void finish(Database.BatchableContext BC) {
        
    }
    global void execute(SchedulableContext sc) {}
}