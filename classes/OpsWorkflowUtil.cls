global class OpsWorkflowUtil {

    @TestVisible private Applicants applicantsInstance {get; set;}
    @TestVisible private List<ProxyApiCreditReportSegmentEntity> segmentsList {get; set;}
    @TestVisible private String CUST_PROFILE_KEY = 'CustProfile';
    @TestVisible private String DOC_CHK_KEY = 'DocCheck';
    @TestVisible private String divHeader {get; set;}
    @TestVisible private String divFooter {get; set;}
    @TestVisible private String divTableFooter {get; set;}
    @TestVisible private String divTableHeaders {get; set;}
    @TestVisible private String divTableRows {get; set;}
    public Attachment attach {get; set;}
    
    public Boolean expandedEmployedApplicants {get; set;}
    
    public Boolean expandedMiscellaneousApplicants {get; set;}
    
    public Boolean expandedRetiredApplicants {get; set;}
    public Boolean expandedSelfEmployedApplicants {get; set;}
  
    public Boolean isSignedContract {get; set;}
    public Boolean showsEmployedApplicants2 {get; set;}
    public Boolean showsSelfEmployedApplicants2 {get; set;}
    public Boolean savedOk {get; set;}
    public CreditReportSegmentsUtil.FraudIndicators fraudIndicators {get; set;}
    public Opportunity app {get; set;}
    public List<Applicants.AllApplicants> allApplicantsList {get; set;}
    public List<Applicants.EmployedApplicants> employedApplicants1List {get; set;}
    public List<Applicants.EmployedApplicants> employedApplicants2List {get; set;}
    public List<Applicants.MiscellaneousApplicants> miscellaneousApplicantsList {get; set;}
    public List<Applicants.RetiredApplicants> retiredApplicantsList {get; set;}
    public List<Applicants.SelfEmployedApplicants> selfemployedApplicants1List {get; set;}
    public List<Applicants.SelfEmployedApplicants> selfemployedApplicants2List {get; set;}
    public String appId {get; set;}
    public String custProfileHTML { get; set; }
    public String docChecklistHTML { get; set; }
    public String errorMessage {get; set; }
    public String hideCustProfileButton {get; set;}
    public String hideDocChkButton {get; set;}

    public OpsWorkflowUtil() { }

    public OpsWorkflowUtil(ApexPages.StandardController controller) {
    /*
        app = (genesis__Applications__c)controller.getRecord();
        if (app != null) 
            app = [SELECT   Id, 
                            All_Applicants_XML__c,
                            Employed_Applicants_1_XML__c,
                            Employed_Applicants_2_XML__c,
                            Retired_Applicants_XML__c,
                            Self_Employed_Applicants_1_XML__c,
                            Self_Employed_Applicants_2_XML__c,
                            No_POI_Required__c,
                            Miscellaneous_Applicants_XML__c,
                            Bankruptcy_founds__c,
                            Bank_Info_Source__c
                    FROM    genesis__Applications__c 
                    WHERE   Id =: this.app.Id];
                    */
    }

/*
    Webservice static String getCustProfile(String thisId) {
        OpsWorkflowUtil c = new OpsWorkflowUtil();
        c.appId = thisId;
        c.initializeCustomerProfile();
        return c.custProfileHTML;
    }
*/
    public void initializeCustomerProfile() {
    /*
        if (String.isEmpty(appId))
            appId = ApexPages.currentPage().getParameters().get('id');
        
        if (!isAppFunded(appId) && proceedWithDataLoad(CUST_PROFILE_KEY)) {
            Decimal debt = 0, income = 0, paymentAmt = 0, pti = 0;
            Offer__c off = null;
            String achAcctNbr = '--', achAcctType = '--', achBankName = '--', achMemberAcct = '--', achRoutNbr = '--';
            String achOthers = '--',achVerifChk = '--', avgDaily = '--', clarityPass = '--';
            String crDTI = '--', crFICO = '--', crLink = '#', dti = '--', emailAgePass = 'Fail', fico = '--', ftDesc = '--', grade = '--';
            String kountPass = 'Not done', negDays = '--', ofacPass = '', ofacStatus = '', payfonePass = '', paymentFreq = '';
            String ptiStr = '--', ssn = '--', totalDep = '--', idologyPass = '';


            divHeader = '<div id="customerProfile" title="Customer Profile Summary"><table>'; 
            divTableHeaders =   '<thead>' + 
                                '<th>Applicant Details</th>' + 
                                '<th>Banking Details-ACH</th>' + 
                                '<th>Credit Report Details</th>' + 
                                '<th>API Response Details</th>' + 
                                '</thead>';
            divTableRows = '<tbody>';
            divTableFooter = '</table></div>';

            app = [SELECT   ACH_Account_Number__c, 
                            ACH_Account_Type__c, 
                            ACH_Bank_Name__c, 
                            ACH_Member_Account_Number__c, 
                            ACH_Routing_Number__c, 
                            Average_Daily_Balance_Amount__c, 
                            Clarity_Status__c, 
                            Clarity_Submitted__c, 
                            DTI__c, 
                            Estimated_Grand_Total__c, 
                            genesis__Contact__c,
                            genesis__Contact__r.Annual_Income__c, 
                            genesis__Payment_Amount__c, 
                            Payment_Frequency_Masked__c,
                            genesis__Payment_Frequency__c, 
                            genesis__Payment_Frequency_Multiplier__c, 
                            FT_Description__c, 
                            Id, 
                            Manual_Grade__c, 
                            Number_of_Negative_Days_Number__c, 
                            Others__c, 
                            Verified_Checklist__c, 
                            Total_Deposits_Amount_Amount__c,
                            Bankruptcy_founds__c
                    FROM    genesis__Applications__c 
                    WHERE   Id =: appId];
            
            Contact c = [SELECT Birthdate, 
                                Id, 
                                ints__Social_Security_Number__c, 
                                MailingCity, 
                                MailingState, 
                                MailingStreet, 
                                Name,
                                Employer_Name__c,
                                Employment_Start_Date__c,
                                Previous_Employment_Start_Date__c,
                                Previous_Employment_End_Date__c
                         FROM   Contact 
                         WHERE  Id =: app.genesis__Contact__c];

            ProxyApiCreditReportEntity cr = CreditReport.getByEntityId(app.Id);

            for (Offer__c offers : [SELECT Id, 
                                            Payment_Amount__c 
                                     FROM   Offer__c 
                                     WHERE  Application__c =: appId 
                                     AND    IsSelected__c = true]){
                off = offers;
            }

            for (Logging__c kountLogs : [SELECT Id 
                                          FROM   Logging__c 
                                          WHERE  Contact__c =: c.Id 
                                          AND    Webservice__c = 'Kount'
                                          ORDER BY CreatedDate DESC]){
                kountPass = 'Done';
            }

            for (Email_Age_Details__c emailAge : [SELECT   Id 
                                                   FROM     Email_Age_Details__c 
                                                   WHERE    Contact__c =: c.Id 
                                                   AND      Email_Age_Status__c = true
                                                   ORDER BY CreatedDate DESC]){
                emailAgePass = 'Pass';

            }

            List<IDology_Request__c> idology = [SELECT  Id, 
                                                        CreatedDate, 
                                                        Verification_Result__c 
                                                FROM    IDology_Request__c 
                                                WHERE   Contact__c =: c.Id 
                                                AND     (Verification_Result__c = 'Pass' OR Verification_Result__c = 'Fail')
                                                ORDER BY CreatedDate DESC 
                                                LIMIT 1];

            List<CSIRCD__ATTUSWDSFLookup__c> ofac = [SELECT Id, 
                                                            CSIRCD__Review_Status__c 
                                                     FROM   CSIRCD__ATTUSWDSFLookup__c 
                                                     WHERE  CSIRCD__Contact__c =: c.Id 
                                                     ORDER BY CreatedDate DESC 
                                                     LIMIT 1];

            

            for (Payfone_Run_History__c payFone : [SELECT  Id, 
                                                            Verification_Result__c
                                                    FROM    Payfone_Run_History__c 
                                                    WHERE   (Verification_Result__c = 'Pass' OR Verification_Result__c = 'Fail')
                                                    AND     Contact__c =: c.Id 
                                                    ORDER BY CreatedDate DESC 
                                                    LIMIT 1]){

               payfonePass = payFone.Verification_Result__c;

            }

            if (cr != null && cr.errorMessage == null) {
                    crLink = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/CreditReportAttach?id=' + app.id + '&crid=' + cr.id + '&viewattach=true';
                    crDTI = cr.getAttributeValue(CreditReport.DTI) + '%';
                    crFICO = cr.getAttributeValue(CreditReport.FICO);
                    if (cr.getAttributeValue(CreditReport.TOTAL_ANNUAL_DEBT) != '')
                        debt = cr.getDecimalAttribute(CreditReport.TOTAL_ANNUAL_DEBT);
            }


            for (Scorecard__c sc : [SELECT DTI__c, FICO_Score__c
                                            FROM   Scorecard__c 
                                            WHERE  Application__c =: appId 
                                            ORDER BY CreatedDate DESC 
                                            LIMIT 1]){
                fico = sc.FICO_Score__c != null ? String.valueOf(sc.FICO_Score__c) : crFICO;
                if (app.Estimated_Grand_Total__c != null && app.Estimated_Grand_Total__c > 0 && debt > 0 ) 
                    dti = String.valueOf(((debt / app.Estimated_Grand_Total__c * 12) * 100).format()) + '%';
                else
                    dti = sc.DTI__c != null ? String.valueOf(sc.DTI__c) + '%' : crDTI;
            }

            
                
            paymentAmt = (app.genesis__Payment_Amount__c == null ? 0 : app.genesis__Payment_Amount__c);
            income = (app.Estimated_Grand_Total__c != null && app.Estimated_Grand_Total__c > 0) ? app.Estimated_Grand_Total__c : app.genesis__Contact__r.Annual_Income__c;
            paymentFreq = app.Payment_Frequency_Masked__c;

            if (paymentFreq != null  && (income != null && income > 0)) 
                pti = ((paymentAmt * OfferCtrlUtil.getFrequencyUtil(paymentFreq)) / income) * 100;

            ptiStr = String.ValueOf(pti.setscale(2)) + '%';
            idologyPass = idology.size() > 0 ? idology[0].Verification_Result__c : '--';
            ofacPass = ofac.size() > 0 ? ofac[0].CSIRCD__Review_Status__c : '--';
            clarityPass = app.Clarity_Submitted__c ? (app.Clarity_Status__c ? 'Pass' : 'Fail') : '--';
            achAcctNbr = String.isEmpty(app.ACH_Account_Number__c) ? '--' : app.ACH_Account_Number__c;
            if (app.Average_Daily_Balance_Amount__c != null) avgDaily =  String.valueOf(app.Average_Daily_Balance_Amount__c); 
            if ( app.Number_of_Negative_Days_Number__c != null) negDays = String.valueOf(app.Number_of_Negative_Days_Number__c); 
            totalDep = app.Total_Deposits_Amount_Amount__c == null ? '--' : String.valueOf(app.Total_Deposits_Amount_Amount__c);                                                    
            achAcctType = String.isEmpty(app.ACH_Account_Type__c) ? '--' : app.ACH_Account_Type__c;
            achMemberAcct = String.isEmpty(app.ACH_Member_Account_Number__c)  ? '--' : app.ACH_Member_Account_Number__c;
            achRoutNbr = String.isEmpty(app.ACH_Routing_Number__c) ? '--' : app.ACH_Routing_Number__c;
            achBankName = String.isEmpty(app.ACH_Bank_Name__c) ? '--' : app.ACH_Bank_Name__c;
            achOthers = String.isEmpty(app.Others__c)  ? '--' : app.Others__c;
            achVerifChk = app.Verified_Checklist__c == null ? '--' : app.Verified_Checklist__c;
            grade = String.isEmpty(app.Manual_Grade__c)  ? '--' : app.Manual_Grade__c;
            ofacStatus = ofacPass == 'False Positive' ||  ofacPass == 'No match' ? ' (PASS)' : ' (FAIL)';
            ssn = c.ints__Social_Security_Number__c == null ? '--' : '***-**-' + c.ints__Social_Security_Number__c.substring(5, 9);
            ftDesc = String.isEmpty(app.FT_Description__c)  ? '--' : app.FT_Description__c;

            divTableRows += '<tr>';
            //Applicant Details
            divTableRows += '<td>';
            divTableRows += wrapSpan('Applicant Name',c.Name);
            divTableRows += wrapSpan('Applicant Address',c.MailingStreet + ', ' + c.MailingCity + ', ' + c.MailingState);
            divTableRows += wrapSpan('Applicant Date Of Birth',c.BirthDate.format());
            divTableRows += wrapSpan('Applicant SSN',ssn);
            divTableRows += wrapSpan('Offer Selected Payment',(off != null ? '$' + String.valueOf(off.Payment_Amount__c) : '$0.00'));
            divTableRows += wrapSpan('Grade',grade);
            divTableRows += wrapSpan('Employer name',c.Employer_Name__c);
            divTableRows += wrapSpan('Hire date',( c.Employment_Start_Date__c != null ? c.Employment_Start_Date__c.format() : 'N/A'));
            divTableRows += wrapSpan('Prior Employment Started',( c.Previous_Employment_Start_Date__c != null ? c.Previous_Employment_Start_Date__c.format() : 'N/A'));
            divTableRows += wrapSpan('Prior Employment Ended',( c.Previous_Employment_End_Date__c != null ? c.Previous_Employment_End_Date__c.format() : 'N/A'));
            divTableRows += '</td>';
            //Banking Details-ACH 
            divTableRows += '<td>';
            divTableRows += wrapSpan('Account Number',achAcctNbr);
            divTableRows += wrapSpan('Routing Number',achRoutNbr);
            divTableRows += wrapSpan('Account Type',achAcctType);
            divTableRows += wrapSpan('Member Account Number',achMemberAcct);
            divTableRows += wrapSpan('Bank Name',achBankName);
            divTableRows += wrapSpan('Others',achOthers);
            divTableRows += wrapSpan('Verified Checklist',achVerifChk);
            divTableRows += wrapSpan('AVG Daily Balance', '$' + avgDaily);
            divTableRows += wrapSpan('# Of Negative Days',negDays);
            divTableRows += wrapSpan('# Of Total Deposits',totalDep);
            divTableRows += '</td>';
            //Credit Report Details
            divTableRows += '<td>';
            divTableRows += wrapSpan('FICO', fico);
            divTableRows += wrapSpan('DTI',dti);
            divTableRows += wrapSpan('PTI',ptiStr);
            divTableRows += wrapSpan('Credit Report', '<a href="' + crLink + '" target="_blank">View Credit Report</a>');
            divTableRows += '</td>';
            //API Response Details
            divTableRows += '<td>';
            divTableRows += wrapSpan('IDology',idologyPass);
            divTableRows += wrapSpan('Kount',kountPass);
            divTableRows += wrapSpan('Email Age',emailAgePass);
            divTableRows += wrapSpan('OFAC Check',ofacPass + ofacStatus);
            divTableRows += wrapSpan('Clarity',clarityPass);
            divTableRows += wrapSpan('Payfone',payfonePass);
            divTableRows += wrapSpan('EWS', ftDesc);
            divTableRows += '</td>';

            divTableRows += '</tr>';

            divTableRows += '</tbody>';

            custProfileHTML = divHeader + divTableHeaders + divTableRows + divTableFooter;
        }
        */
    }
/*
    public String wrapSpan(String head, String val) {
        return '<span class="data"><strong>' + head + ': </strong>' + val + '</span>';
    }
*/
    public void initializeDocumentChecklist() {
    /*
        if (String.isEmpty(appId))
            appId = ApexPages.currentPage().getParameters().get('id');

        if (!isAppFunded(appId) && proceedWithDataLoad(DOC_CHK_KEY)) {  
            String requiredChecked = '', yodleeChecked = '', completedByName = '', income = '';
            String applicantsTable = '', employeesTable = '';

            attach = new Attachment();
            applicantsInstance = new Applicants();
            app = [SELECT   Id, 
                            All_Applicants_XML__c, 
                            Employed_Applicants_1_XML__c, 
                            Employed_Applicants_2_XML__c, 
                            Retired_Applicants_XML__c,
                            Self_Employed_Applicants_1_XML__c, 
                            Self_Employed_Applicants_2_XML__c, 
                            No_POI_Required__c, 
                            Miscellaneous_Applicants_XML__c,
                            genesis__Contact__c, 
                            genesis__Contact__r.Employment_Type__c, 
                            genesis__Contact__r.Second_Income_Type__c,
                            Bankruptcy_founds__c,
                            Bank_Info_Source__c 
                    FROM    genesis__Applications__c 
                    WHERE   Id =: appId];

            initializeApplicants();
        }
        */
    }

/*
     private Boolean proceedWithDataLoad(String method) {
        Boolean proceed = false;
        String userProfile = UserInfo.getProfileId();
        List<String> profilesToQuery = new List<String> { 'System Administrator', 'Manager' };
        List<Profile> authProfiles = new List<Profile>();

        if (method == CUST_PROFILE_KEY) {
            profilesToQuery.add('Funding Specialist Underwriter');
            profilesToQuery.add('QTM');
        } 
        else if (method == DOC_CHK_KEY) {
            profilesToQuery.add('Funding Specialist Underwriter');
            profilesToQuery.add('Application Support');
        }
        for (Profile prof : [SELECT Id FROM Profile WHERE Name in :profilesToQuery]) {
            if (userProfile == prof.Id) {
                if (method == CUST_PROFILE_KEY)
                    hideCustProfileButton = 'display:inline;';
                else if (method == DOC_CHK_KEY)
                    hideDocChkButton = 'display:inline;';
                proceed = true;
                break;
            }
            else {
                if (method == CUST_PROFILE_KEY)
                    hideCustProfileButton = 'display:none;';
                else if (method == DOC_CHK_KEY)
                    hideDocChkButton = 'display:none;';
            }
        }
        return proceed;
    }
*/
    public PageReference opsWfSaveApp() {
    /*
        saveApp();
        createMessage(ApexPages.Severity.Confirm, 'Document Checklist updated.');
        */
        return null;
    }
/*
    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }
*/
    public Pagereference attachFiles() 
    {
        string docurl = '/apex/EasyUploadAttachment?parentId=' + app.Id;
        Pagereference pageRef = new Pagereference(docurl);
        return pageRef.setRedirect(true);
    }

    public PageReference processFile() {
    /*
        try {
            if (isSignedContract) {
                String signedContracttitle = 'Signed Contract.' + attach.Name.substringAfterLast('.');
                attach.name = signedContracttitle;
                DocusignService.NoteOnNewContractAfterRegrade(app.Id);
            }
            insertAttachment(app.Id);
            insertDocumentStatus(attach.Name);
            System.debug('Record Inserted');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'File uploaded successsfully'));
        } catch (Exception e) {
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Some exception has occured'));
        }
        */
        return null;
    }

    public void cancelUpload() {
    /*
        attach.name = null;
        attach.body = null;
        */
    }
/*
    private Attachment insertAttachment(Id parentId) {
        attach.ParentId = parentId;
        insert attach;
        System.debug('Success');
        attach.Body = null;
        return attach;
    }

    private void insertDocumentStatus(String title) {
        genesis__Document_Status__c obj = new genesis__Document_Status__c();
        title = title.contains('.') ? title.split('\\.')[0] : title;
        obj.genesis__Application__c = app.Id;
        List<genesis__Document_Master__c> dmList = new List<genesis__Document_Master__c>();
        dmList = [SELECT Name, 
                         Id, 
                         genesis__Doc_Name__c
                  FROM   genesis__Document_Master__c
                  WHERE  genesis__Doc_Name__c = :title ];

        if (dmList != null && dmList.size() == 1) {
            obj.genesis__Doc_Name__c = dmList[0].Id;
            obj.genesis__Attachment_Name__c = title;
            obj.genesis__Status__c = 'CREATED';
        } else {
            obj.genesis__Attachment_Name__c = title;
            obj.genesis__Doc_Name__c = null;
            obj.genesis__Status__c = null;
        }
        obj.genesis__Document_Url__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + attach.id;
        insert obj;
    }

    private void initializeApplicants() {       
        expandedEmployedApplicants = (app.genesis__Contact__r.Employment_Type__c == 'Employee' || app.genesis__Contact__r.Second_Income_Type__c == 'Employee');
        showsEmployedApplicants2 = (app.genesis__Contact__r.Employment_Type__c == 'Employee' && app.genesis__Contact__r.Second_Income_Type__c == 'Employee');
        expandedRetiredApplicants = (app.genesis__Contact__r.Employment_Type__c == 'Retired / Disabled' || app.genesis__Contact__r.Second_Income_Type__c == 'Retired / Disabled') ;
        expandedSelfEmployedApplicants = (app.genesis__Contact__r.Employment_Type__c == 'Self Employed' || app.genesis__Contact__r.Second_Income_Type__c == 'Self Employed');
        showsSelfEmployedApplicants2 = (app.genesis__Contact__r.Employment_Type__c == 'Self Employed' && app.genesis__Contact__r.Second_Income_Type__c == 'Self Employed');
        ProxyApiCreditReportEntity cr = CreditReport.getByEntityId(app.Id);
        CreditReportSegmentsUtil.FraudIndicators fraudIndicators = null;

        fraudIndicators = new CreditReportSegmentsUtil.FraudIndicators('');
        if(CreditReport.validateBrmsFlag(app.Id)) {
            List<ProxyApiCreditReportSegmentEntity> segmentsList = (cr != null && cr.errorMessage == null) ? cr.segmentsList : new List<ProxyApiCreditReportSegmentEntity>();
            fraudIndicators = CreditReportSegmentsUtil.extractFraudIndicators(segmentsList);
        } else {
            if(cr != null && cr.brms != null && cr.brms.rulesList != null && !cr.brms.rulesList.isEmpty())
                fraudIndicators = CreditReportSegmentsUtil.extractFraudIndicatorsThrBRMS(cr.brms.rulesList); 
        }

        if (String.isNotEmpty(app.All_Applicants_XML__c) )
            allApplicantsList = applicantsInstance.InitializeAllApplicantsList(app.All_Applicants_XML__c, UserInfo.GetName(), fraudIndicators);
        else
            allApplicantsList = applicantsInstance.InitializeAllApplicantsList(UserInfo.GetName(), fraudIndicators);

        if (String.isNotEmpty(app.Employed_Applicants_1_XML__c) ) {
            employedApplicants1List = applicantsInstance.InitializeEmployedApplicantsList(app.Employed_Applicants_1_XML__c, UserInfo.GetName());
            expandedEmployedApplicants = expandedEmployedApplicants ? expandedEmployedApplicants : applicantsInstance.ExistsValuesForEmployedApplicants(app.Employed_Applicants_1_XML__c);
        } else
            employedApplicants1List = applicantsInstance.InitializeEmployedApplicantsList(expandedEmployedApplicants, UserInfo.GetName());

        if (String.isNotEmpty(app.Employed_Applicants_2_XML__c) )
            employedApplicants2List = applicantsInstance.InitializeEmployedApplicantsList(app.Employed_Applicants_2_XML__c, UserInfo.GetName());
        else
            employedApplicants2List = applicantsInstance.InitializeEmployedApplicantsList(showsEmployedApplicants2, UserInfo.GetName());

        if (String.isNotEmpty(app.Retired_Applicants_XML__c) ) {
            retiredApplicantsList = applicantsInstance.InitializeRetiredApplicantsList(app.Retired_Applicants_XML__c, UserInfo.GetName());
            expandedRetiredApplicants = expandedRetiredApplicants ? expandedRetiredApplicants : applicantsInstance.ExistsValuesForRetiredApplicants(app.Retired_Applicants_XML__c);
        } else
            retiredApplicantsList = applicantsInstance.InitializeRetiredApplicantsList(this.expandedRetiredApplicants, UserInfo.GetName());

        if (String.isNotEmpty(app.Self_Employed_Applicants_1_XML__c) ) {
            selfemployedApplicants1List = applicantsInstance.InitializeSelfEmployedApplicantsList(app.Self_Employed_Applicants_1_XML__c, UserInfo.GetName());
            expandedSelfEmployedApplicants = expandedSelfEmployedApplicants ? expandedSelfEmployedApplicants : applicantsInstance.ExistsValuesForSelfEmployedApplicants(app.Self_Employed_Applicants_1_XML__c);
        } else
            selfemployedApplicants1List = applicantsInstance.InitializeSelfEmployedApplicantsList(expandedSelfEmployedApplicants, UserInfo.GetName());

        if (String.isNotEmpty(app.Self_Employed_Applicants_2_XML__c) )
            selfemployedApplicants2List = applicantsInstance.InitializeSelfEmployedApplicantsList(app.Self_Employed_Applicants_2_XML__c, UserInfo.GetName());
        else
            selfemployedApplicants2List = applicantsInstance.InitializeSelfEmployedApplicantsList(showsSelfEmployedApplicants2, UserInfo.GetName());

        if (String.isNotEmpty(app.Miscellaneous_Applicants_XML__c) )
            miscellaneousApplicantsList = applicantsInstance.InitializeMiscellaneousApplicantsList(app.Miscellaneous_Applicants_XML__c, UserInfo.GetName());
        else
            miscellaneousApplicantsList = applicantsInstance.InitializeMiscellaneousApplicantsList(false, UserInfo.GetName());
    }
*/
    public List<SelectOption> getYears() {
        List<SelectOption> options = new List<SelectOption>();
        Integer currentYear = System.Today().year() - 10;
        for (Integer i = currentYear; i < currentYear + 20; i++) {
            options.add(new SelectOption(String.valueof(i), String.valueof(i)));
        }
        return options;
    }
/*
    private void insertNoteIncomeEdit(String newXML, String oldXML, String incomeText, String EmpType) {
        //Getting new income list
        List<String> newList = new List<String>(), oldList = new List<String>();
        Dom.Document newdoc = new Dom.Document(), olddoc = new Dom.Document();
        String oldIncome = '', newIncome = '', application_id = '', application_name = '', oldValue = '',  newValue = '', incomeType = '';
        integer idValue = 1000;
        newdoc.load(newXML);

        for (Dom.XMLNode node : newdoc.getRootElement().getChildElements()) {
            if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                newIncome = '0.00';
                if (String.isNotEmpty(node.getChildElement(incomeText, null).getText()))
                    newIncome = node.getChildElement(incomeText , null).getText();

                if (oldXML == null)
                    oldList.add('0.00');

                newList.add(newIncome);
            }
        }
        //Getting old income list
        if (oldXML != null) {
            olddoc.load(oldXML);
            for (Dom.XMLNode node : olddoc.getRootElement().getChildElements()) {
                if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                    oldIncome = '0.00';
                    if (String.isNotEmpty(node.getChildElement(incomeText, null).getText()))
                        oldIncome = node.getChildElement(incomeText , null).getText();

                    oldList.add(oldIncome);
                }
            }
        }
        //Comparing the list and Storing in note
        for (integer i = 0; i < newList.size(); i++) {
            idValue = idValue + 10;
            if (newList[i].equals(oldList[i]) == false) {
                application_id = app.id;
                application_name = app.name;
                oldValue = oldList[i];
                newValue = newList[i];

                if (EmpType == 'Miscellaneous') {
                    Applicants.MiscellaneousApplicants d = new Applicants.MiscellaneousApplicants(idValue, false, UserInfo.GetName());
                    incomeType = d.Label;
                } else if (EmpType == 'Retired') {
                    Applicants.RetiredApplicants d = new Applicants.RetiredApplicants(idValue, false, UserInfo.GetName());
                    incomeType = d.Label;
                } else if (EmpType == 'SelfEmployed') {
                    Applicants.SelfEmployedApplicants d = new Applicants.SelfEmployedApplicants(idValue, false, UserInfo.GetName());
                    incomeType = d.Label;
                } else if (EmpType == 'Employed') {
                    Applicants.EmployedApplicants d = new Applicants.EmployedApplicants(idValue, false, UserInfo.GetName());
                    incomeType = d.Label;
                }

                Note noteDetails = new Note();
                noteDetails.ParentId = application_id;
                noteDetails.body = 'The income values changed in the Documents Tab at ' +
                                   datetime.now().format('hh:mm:ss') + ' on ' + date.today().format() +
                                   ' by ' + UserInfo.GetName() + '. \n ' + incomeType + ': ' + incomeText +
                                   ' - $' + oldValue + ' was changed to $' + newValue;
                noteDetails.Title = 'Income Changed in Documents Tab for ' + application_name;
                insert noteDetails;
            }
        }
    }

    private void saveApp() {
        try {
            String oldRetiredApplicants = app.Retired_Applicants_XML__c;
            String oldEmployedApplicants1List = app.Employed_Applicants_1_XML__c, oldEmployedApplicants2List = app.Employed_Applicants_2_XML__c;
            String oldSelfEmployedApplicants1List = app.Self_Employed_Applicants_1_XML__c, oldSelfEmployedApplicants2List = app.Self_Employed_Applicants_2_XML__c;
            String oldMiscellaneousApplicantsList = app.Miscellaneous_Applicants_XML__c;
            app.All_Applicants_XML__c = applicantsInstance.GetXmlFromAllApplicantsList(allApplicantsList);
            app.Employed_Applicants_1_XML__c =  applicantsInstance.GetXmlFromEmployedApplicantsList(employedApplicants1List);
            app.Employed_Applicants_2_XML__c =  applicantsInstance.GetXmlFromEmployedApplicantsList(employedApplicants2List);
            app.Retired_Applicants_XML__c =  applicantsInstance.GetXmlFromRetiredApplicantsList(retiredApplicantsList);
            app.Self_Employed_Applicants_1_XML__c = applicantsInstance.GetXmlFromSelfEmployedApplicantsList(selfEmployedApplicants1List);
            app.Self_Employed_Applicants_2_XML__c = applicantsInstance.GetXmlFromSelfEmployedApplicantsList(selfEmployedApplicants2List);
            app.Miscellaneous_Applicants_XML__c = applicantsInstance.GetXmlFromMiscellaneousApplicantsList(miscellaneousApplicantsList);

            if ((oldRetiredApplicants == null && app.Retired_Applicants_XML__c != null) || (oldRetiredApplicants != null && oldRetiredApplicants.compareTo(app.Retired_Applicants_XML__c) != 0)) {
                insertNoteIncomeEdit(app.Retired_Applicants_XML__c, oldRetiredApplicants, 'BenefitLetterIncome', 'Retired');
                insertNoteIncomeEdit(app.Retired_Applicants_XML__c, oldRetiredApplicants, 'Form1099Income', 'Retired');
            }
            if ((oldEmployedApplicants1List == null && app.Employed_Applicants_1_XML__c != null) || (oldEmployedApplicants1List != null && oldEmployedApplicants1List.compareTo(app.Employed_Applicants_1_XML__c) != 0))
                insertNoteIncomeEdit(app.Employed_Applicants_1_XML__c, oldEmployedApplicants1List, 'Income', 'Employed');
            if ((oldEmployedApplicants2List == null && app.Employed_Applicants_2_XML__c != null) || (oldEmployedApplicants2List != null && oldEmployedApplicants2List.compareTo(app.Employed_Applicants_2_XML__c) != 0))
                insertNoteIncomeEdit(app.Employed_Applicants_2_XML__c, oldEmployedApplicants2List, 'Income', 'Employed');
            if ((oldSelfEmployedApplicants1List == null && app.Self_Employed_Applicants_1_XML__c != null) || (oldSelfEmployedApplicants1List != null && oldSelfEmployedApplicants1List.compareTo(app.Self_Employed_Applicants_1_XML__c) != 0)) {
                insertNoteIncomeEdit(app.Self_Employed_Applicants_1_XML__c, oldSelfEmployedApplicants1List, 'Income', 'SelfEmployed');
                insertNoteIncomeEdit(app.Self_Employed_Applicants_1_XML__c, oldSelfEmployedApplicants1List, 'NetIncome', 'SelfEmployed');
            }
            if ((oldSelfEmployedApplicants2List == null && app.Self_Employed_Applicants_2_XML__c != null) || (oldSelfEmployedApplicants2List != null && oldSelfEmployedApplicants2List.compareTo(app.Self_Employed_Applicants_2_XML__c) != 0)) {
                insertNoteIncomeEdit(app.Self_Employed_Applicants_2_XML__c, oldSelfEmployedApplicants2List, 'Income', 'SelfEmployed');
                insertNoteIncomeEdit(app.Self_Employed_Applicants_2_XML__c, oldSelfEmployedApplicants2List, 'NetIncome', 'SelfEmployed');
            }
            if ((oldMiscellaneousApplicantsList == null && app.Miscellaneous_Applicants_XML__c<> null) || (oldMiscellaneousApplicantsList.compareTo(app.Miscellaneous_Applicants_XML__c) != 0))
                insertNoteIncomeEdit(app.Miscellaneous_Applicants_XML__c, oldMiscellaneousApplicantsList, 'Income', 'Miscellaneous');
            if (String.isEmpty(app.Contract_Received_Through__c))
                app.Contract_Signed_Date__c = null;
            
            update app;
        } catch (Exception e) {
            createMessage(ApexPages.severity.Error, 'Error : ' + e.getMessage() + 'at' + e.getlinenumber());
        }
    }
*/
    public List<SelectOption> getBankStatementsDispos() {
        List<SelectOption> dispos = new List<SelectOption>();
        /*
        for (String val : Applicants.checkingAcctAndDepositForm) 
            dispos.add(new SelectOption(val,val));
            */
        return dispos;
    }

    public List<SelectOption> getAllAppsOtherDispos() {
        List<SelectOption> dispos = new List<SelectOption>();
        /*
        for (String val : Applicants.allAppsOtherDocs) 
            dispos.add(new SelectOption(val,val));
            */
        return dispos;
    }

    public List<SelectOption> getYtdGrossDispos() {
        List<SelectOption> dispos = new List<SelectOption>();
        /*
        for (String val : Applicants.ytdGrossPaystub) 
            dispos.add(new SelectOption(val,val));
            */
        return dispos;
    }
    
    public List<SelectOption> getEmpAppsOtherDispos() {
        List<SelectOption> dispos = new List<SelectOption>();
        /*
        for (String val : Applicants.empAppsOtherDocs) 
            dispos.add(new SelectOption(val,val));
            */
        return dispos;
    }

    public List<SelectOption> getOtherApplicantsDispos() {
        List<SelectOption> dispos = new List<SelectOption>();
        /*
        for (String val : Applicants.otherApplicantsDispos) 
            dispos.add(new SelectOption(val,val));
            */
        return dispos;
    }
   /* 
    private Boolean isAppFunded(String appId) 
    {
        Boolean appIsFunded = ([SELECT genesis__Status__c FROM genesis__Applications__c WHERE Id =: appId].genesis__Status__c == 'Funded');
        if (!appIsFunded) 
            hideDocChkButton = 'display:inline;';
        else
            hideDocChkButton = 'display:none;';
        return appIsFunded;
    }
    */
}