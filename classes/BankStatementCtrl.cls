public with sharing class BankStatementCtrl {

    @AuraEnabled
    public static BankStatementEntity getBankStatementEntity(Id oppId, String statementToSearch, Boolean isFirstTimeLoading) {
        
        
        BankStatementEntity bse = new BankStatementEntity();
        try {
			
            //OF-631
            opportunity oppRefiCheck = [select Same_Bank_Information_as_Funded__c,Contact__c, Contact__r.Lead__c, DL_Account_Number_Found__c, ACH_Account_Number__c,
                                        Contact__r.Annual_Income__c, Estimated_Gross_Income__c,
                                        Gross_Percentage_To_Declared__c, Decision_Logic_Result__c,
                                        DL_Available_Balance__c, DL_As_of_Date__c, DL_Current_Balance__c,
                                        DL_Average_Balance__c, DL_Deposits_Credits__c, DL_Avg_Bal_Latest_Month__c,
                                        DL_Withdrawals_Debits__c, Number_of_Negative_Days_Number__c, Decision_Logic_Income__c,
                                        Income_Verification_Source__c,Clarity_Verified_Income__c,Contract_Renewed__r.opportunity__c,type from opportunity where id=:oppId limit 1];
            if(oppRefiCheck.type == 'Refinance' && oppRefiCheck.Same_Bank_Information_as_Funded__c==True) oppId = oppRefiCheck.Contract_Renewed__r.opportunity__c;
            
            //OF631
            
            Opportunity opp = [
                    SELECT Id, Contact__c, Contact__r.Lead__c, DL_Account_Number_Found__c, ACH_Account_Number__c,
                            Contact__r.Annual_Income__c, Estimated_Gross_Income__c,
                            Gross_Percentage_To_Declared__c, Decision_Logic_Result__c,
                            DL_Available_Balance__c, DL_As_of_Date__c, DL_Current_Balance__c,
                            DL_Average_Balance__c, DL_Deposits_Credits__c, DL_Avg_Bal_Latest_Month__c,
                            DL_Withdrawals_Debits__c, Number_of_Negative_Days_Number__c, Decision_Logic_Income__c,
                            Income_Verification_Source__c,Clarity_Verified_Income__c
                    FROM Opportunity
                    WHERE Id = :oppId
                    LIMIT 1
            ];

            if (opp == null || String.isEmpty(opp.Id) || String.isEmpty(opp.contact__c)) {
                bse.errorMessage = 'The opportunity, id or contact is missing';
                return bse;
            }

            bse.opportunity = opp;

            List<DL_AccountStatementSF> statementsList = getStmtsFromProxy(opp);
            if (statementsList == null || statementsList.isEmpty())
                bse.errorMessage = 'No statements from Proxy';

            PicklistStatementCombo psc = getSelectedStatement(statementsList, opp.ACH_Account_Number__c, statementToSearch, opp.DL_Account_Number_Found__c);
            if (psc.statement == null){
                bse.errorMessage = 'Statement is null';
                psc.statement = new DL_AccountStatementSF();
            }

            /**
            * If the statement is loading when the screen first loads, then it should check if the summary fields have values.
            * It should only update the summary fields if these are ALL empty!
            * If the statement is loaded at any other stage, go ahead and overwrite every value in the screen.
            **/
            if(Boolean.valueOf(isFirstTimeLoading)){                
                if(oppRefiCheck.type == 'Refinance'  && oppRefiCheck.Same_Bank_Information_as_Funded__c==True){ //OF-631
                         bse.statement = manageSummaryInputFields(psc.statement,oppRefiCheck);    
                    }
                else{
                    if(meetsUpdateCriteria(bse.opportunity)){
                        bse.statement = psc.statement;
                    }else{
                        bse.statement = manageSummaryInputFields(psc.statement,bse.opportunity);
                    }
                }
            }else{
                bse.statement = psc.statement;
            }
            		
            bse.accountsFound = psc.picklistItemList;

        } catch (Exception e) {
            system.debug(e.getMessage() + ' - ' + e.getStackTraceString());
            bse.errorMessage = 'Exception at BankStatementCtrl.getBankStatementEntity';
            return bse;
        }

        return bse;
    }

    /**
    * meetsUpdateCriteria:
    * As per SM-372: When the statement loads at screen startup,
    * update summary fields ONLY if they are all empty
    *
    * @param Opportunity
    * @return - true: if it meets the criteria in order to update the fields
     *        - false: if it does not meet the criteria in order to update the fields
    */
    private static boolean meetsUpdateCriteria(Opportunity application){
        return     String.isEmpty(String.valueOf(application.DL_Available_Balance__c))
                && String.isEmpty(String.valueOf(application.DL_As_of_Date__c))
                && String.isEmpty(String.valueOf(application.DL_Current_Balance__c))
                && String.isEmpty(String.valueOf(application.DL_Average_Balance__c))
                && String.isEmpty(String.valueOf(application.DL_Deposits_Credits__c))
                && String.isEmpty(String.valueOf(application.DL_Avg_Bal_Latest_Month__c))
                && String.isEmpty(String.valueOf(application.DL_Withdrawals_Debits__c))
                && (String.isEmpty(String.valueOf(application.Number_of_Negative_Days_Number__c)) || application.Number_of_Negative_Days_Number__c == 0.00);
    }

    private static DL_AccountStatementSF manageSummaryInputFields(DL_AccountStatementSF statement, Opportunity application){
        statement.availableBalance = String.valueOf(application.DL_Available_Balance__c);
        statement.asOfDateSimple = Date.valueOf(application.DL_As_of_Date__c);
        statement.currentBalance = String.valueOf(application.DL_Current_Balance__c);
        statement.averageBalance = String.valueOf(application.DL_Average_Balance__c);
        statement.totalCredits = String.valueOf(application.DL_Deposits_Credits__c);
        statement.averageBalanceRecent = String.valueOf(application.DL_Avg_Bal_Latest_Month__c);
        statement.totalDebits = String.valueOf(application.DL_Withdrawals_Debits__c);
        statement.negativeDays = Integer.valueOf(application.Number_of_Negative_Days_Number__c);
        return statement;
    }

    private static PicklistStatementCombo getSelectedStatement(List<DL_AccountStatementSF> statementsList, String accountEntered, String statementToSearch, String dl_Account_Number_Found) {

        PicklistStatementCombo pc = new PicklistStatementCombo();
        //Adds the '--None--' option, selected if its value equals Opportunity.DL_Account_Number_Found__c
        pc.picklistItemList.add(
                new alphaHelperCls.PicklistItem(
                        '--None--',
                        '--None--',
                        '--None--'.equalsIgnoreCase(dl_Account_Number_Found)
                )
        );

        try {
            if (statementsList == null || statementsList.isEmpty())
                return pc;

            //If statementToSearch is NOT null, then the stmt was selected from the picklist
            if (!String.isEmpty(statementToSearch)){
                accountEntered = statementToSearch;
            }else{
                //This 'else' processes the statements when the ACH tab is first loaded
                if(!String.isEmpty(dl_Account_Number_Found))
                    accountEntered = dl_Account_Number_Found;
            }

            //Iterate the statements and get the corresponding one
            for (DL_AccountStatementSF stmt : statementsList) {
                alphaHelperCls.PicklistItem pi = new alphaHelperCls.PicklistItem(stmt.accountNumberFound, stmt.accountNumberFound);
                if (stmt.matchesAccountNumberFound(accountEntered)) {
                    pi.isSelected = true;
                    pc.statement = stmt;
                }
                pc.picklistItemList.add(pi);
            }

            //If no statement matched, return the first statement in the list
            if (pc.statement == null && !'--None--'.equalsIgnoreCase(dl_Account_Number_Found)) {
                pc.statement = statementsList.get(0);
                if(pc.picklistItemList.size() > 1){
                    alphaHelperCls.PicklistItem pi = pc.picklistItemList.get(1);
                    pi.isSelected = true;
                    pc.picklistItemList.remove(1);
                    pc.picklistItemList.add(pi);
                }
            }

        } catch (Exception e) {
            system.debug(e.getMessage() + ' --> ' + e.getStackTraceString());
            return pc;
        }
        return pc;
    }

    private static List<DL_AccountStatementSF> getStmtsFromProxy(Opportunity opp) {

        List<DL_AccountStatementSF> statementsFromProxy =
                new List<DL_AccountStatementSF>();

        if (opp == null || opp.contact__c == null || opp.id == null)
            return statementsFromProxy;

        try {
            if(Test.isRunningTest()){
                statementsFromProxy = TestHelper.getFakeDecisionLogicStatements();
            }else{
                /*
                statementsFromProxy = ProxyApiBankStatements.ApiGetStatements(
                        opp.Contact__r.Lead__c == null ? opp.contact__c : opp.Contact__r.Lead__c
                        , opp.id
                );
                */
                statementsFromProxy = ProxyApiBankStatements.ApiGetStatements(opp.contact__c, opp.id);
                if(statementsFromProxy == null || statementsFromProxy.isEmpty()){ //Case for existing contacts prior to Alpha release
                    statementsFromProxy = ProxyApiBankStatements.ApiGetStatements(opp.Contact__r.Lead__c, opp.id);
                }
            }
        } catch (Exception e) {
            system.debug(e.getMessage() + ' - ' + e.getStackTraceString());
            return statementsFromProxy;
        }
        return statementsFromProxy;
    }

    private class PicklistStatementCombo {
        public DL_AccountStatementSF statement;
        public List<alphaHelperCls.PicklistItem> picklistItemList;

        public PicklistStatementCombo() {
            picklistItemList = new List<alphaHelperCls.PicklistItem>();
        }
    }

    public class BankStatementEntity {
        @AuraEnabled public Opportunity opportunity {
            get;
            set;
        }
        @AuraEnabled public DL_AccountStatementSF statement { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> accountsFound {
            get;
            set;
        }
        @AuraEnabled public Boolean hasError {
            get;
            set;
        }
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = true;
            }
        }
    }

}