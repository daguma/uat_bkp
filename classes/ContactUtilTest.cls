@isTest public with sharing class ContactUtilTest {

    static testMethod void testGetLastIdologyResult() {

        Contact TestContact = LibraryTest.createContactTH();

        IDology_Request__c ir = new IDology_Request__c(Contact__c = TestContact.Id, Verification_Result_Front__c = '', IDology_Result__c = '');
        insert ir;
        String lastIdologyResult = ContactUtil.getLastIdologyResult(TestContact.Id);
        system.assertNotEquals(null,lastIdologyResult);

    }

    static testMethod void testGetLatestPayphoneResult() {

        Contact TestContact = LibraryTest.createContactTH();

        String MatchCRM_Response = '{&quot;RequestId&quot;:&quot;cb754f00-2b55-b600-f3ae-8bc1ba0a3633&quot;,&quot;Status&quot;:1012,&quot;Description&quot;:&quot;No CRM data available&quot;}';
        String GetIntelligence_Response = '{&quot;RequestId&quot;:&quot;cb754f00-2b55-b600-f3ae-8bc1ba0a3633&quot;,&quot;Status&quot;:0,&quot;Description&quot;:&quot;Success.&quot;,&quot;Response&quot;:{&quot;PayfoneSignature&quot;:&quot;CC13F60C4VK838473E844F5033D73372E0MEKGUSJ5I9PEA0AFDE9F46F737BBF03C73382D64777E64F6G3427FE5BAEB1B7746A40B73879FBD2B912C49&quot;,&quot;TransactionId&quot;:&quot;1265489486&quot;,&quot;FraudStatus&quot;:&quot;No known fraud&quot;,&quot;Path&quot;:4,&quot;PhoneNumberFlags&quot;:{&quot;Tenure&quot;:5,&quot;Velocity&quot;:5,&quot;Status&quot;:&quot;Ported&quot;,&quot;LineType&quot;:&quot;Mobile&quot;},&quot;OperatorFlags&quot;:{&quot;Tenure&quot;:5,&quot;Velocity&quot;:4,&quot;Name&quot;:&quot;Verizon&quot;},&quot;PayfoneSignatureFlags&quot;:{&quot;Tenure&quot;:5}}}';
        Payfone_Run_History__c prh = new Payfone_Run_History__c(Contact__c = TestContact.Id, MatchCRM_Response__c = MatchCRM_Response, Verification_Result__c = 'Pass', GetIntelligence_Response__c = GetIntelligence_Response, Last_Name_Score__c = 0);
        insert prh;
        String latestPayphoneResult = ContactUtil.getLatestPayphoneResult(TestContact.Id);
        system.assertNotEquals('',latestPayphoneResult);

    }

    static testMethod void testGetKountScore() {

        Contact TestContact = LibraryTest.createContactTH();

        String kscore = '{MOBILE_DEVICE=Y VMAX=1 MODE=Q IP_CITY= IP_ORG=VERIZON WIRELESS TRAN=7K1K0K8KKNT7 CARDS=2 PC_REMOTE=N PIP_COUNTRY= SESS=C2BB3185A4DB19C89D6BA416747298C1 ORDR= FINGERPRINT=EDB94E041925427DB5100ECBD475CC01 COUNTRY=US SCOR=81 MASTERCARD= VERS=0630 IP_COUNTRY=US BRND=NONE VOICE_DEVICE=N TIMEZONE=240 PIP_REGION= LANGUAGE=EN PIP_LAT= IP_REGION=Florida DSR=736x414 IP_IPAD=174.227.128.32 DEVICE_LAYERS=E40ED7C842...9E69DE7C53.39D3A5A5D2 FLASH=N REGION=US_FL AUTO=A IP_LON=-81.6221 PIP_IPAD= JAVASCRIPT=Y BROWSER=Safari 65.0.3325.152 IP_LAT=28.6344 PROXY=N WARNING_COUNT=0 UAS=Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_1 like Mac OS X) AppleWebKit/604.1.34 (KHTML like Gecko) CriOS/65.0.3325.152 Mobile/15C153 Safari/604.1 LOCALTIME=2018-04-05 18:49 RULES_TRIGGERED=0 EMAILS=2 HTTP_COUNTRY=US MOBILE_FORWARDER=N PIP_CITY= DDFS=2017-10-12 VELO=1 COUNTERS_TRIGGERED=0 NETW=N SITE=DEFAULT COOKIES=Y OS=iPhone OS 11.2.1 PIP_ORG= GEOX=US REASON_CODE= MERC=146300 PIP_LON= KAPT=Y DEVICES=2 REGN=US_FL MOBILE_TYPE=iPhone}';
        Logging__c log = new Logging__c(Contact__c = TestContact.Id, api_response__c = kscore, Webservice__c = 'Kount');
        insert log;
        String kountScore = ContactUtil.getKountScore(TestContact.Id);
        system.assertNotEquals(null,kountScore);

    }

}