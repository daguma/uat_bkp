@isTest
public Class TestCoborrowerScreenCtrl{
    
    static testMethod void TestCoborrower(){  
    
         Contact Con = new Contact(Firstname='Test', Lastname = 'LastName',MailingStreet = 'ABC Street',MailingCity = 'ABC City',
                      MailingState = 'CA',MailingPostalCode = '123456',ints__Social_Security_Number__c = '123456789',Annual_Income__c = 500000);
         insert Con;
         
         genesis__Applications__c app = CreateApplication(Con);         
         ApexPages.StandardController Contr = new ApexPages.StandardController(app);
         CoborrowerScreenCtrl CBCtrl = new CoborrowerScreenCtrl(Contr);
         CBCtrl.PopulateInfo();
         CBCtrl.creditPull();
         CBCtrl.saveApp();
         CBCtrl.getmynote();
         
         Note TestNote = new Note();
         TestNote.title = 'test';
         TestNote.parentId = app.id;
         TestNote.body = 'b';
         CBCtrl.mynote= TestNote;
                           
         CBCtrl.Savedoc();
         CBCtrl.hidepanel();
                  
    }  
    
     static genesis__Applications__c CreateApplication(Contact Coborrower){
         genesis__Applications__c app = new genesis__Applications__c(Coborrower__c = Coborrower.id);
         app.genesis__Contact__c = Testhelper.createContact().Id;
         app.genesis__Product_Type__c = 'LOAN';           
         app.genesis__Term__c = 10;
         app.genesis__Loan_Amount__c = 1000;
         app.genesis__Interest_Rate__c = 20;
         app.genesis__Days_Convention__c = '365/365';
         app.genesis__Expected_Start_Date__c = System.today();
         app.genesis__Expected_First_Payment_Date__c = system.today().addMonths(1);
         app.genesis__Interest_Calculation_Method__c = 'Declining Balance';
         app.genesis__Payment_Frequency__c = 'Monthly';
         app.genesis__Payment_Amount__C = 100;
         app.ACH_Account_Number__c = '10000';
         app.ACH_Account_Type__c = 'Checking';
         app.ACH_Bank_Name__c = 'ABC';
         app.ACH_Routing_Number__c = '999999';
         insert app;
         return app;
     }  
}