public with sharing class BankStatement_AllTransactionsCtrl {

    @AuraEnabled
    public static BankStatement_AllTransactionsEntity getTransactionsList(String contactId, String oppId, String accountNumber, String typeCode) {

        BankStatement_AllTransactionsEntity bsate = new BankStatement_AllTransactionsEntity();

        try {

            if(String.isEmpty(contactId) || String.isEmpty(oppId) || String.isEmpty(accountNumber)){
                bsate.errorMessage = 'ContactId, OpportunityId or AccountNumber is empty';
            }
            else{

                List<DL_AccountStatementSF> statementsList =
                        ProxyApiBankStatements.ApiGetSpecificTransactions(contactId,oppId,accountNumber,typeCode);

                if( statementsList != null && statementsList.size() > 0 )
                    bsate.statement = statementsList.get(0);
                else{
                    bsate.errorMessage = 'Statement is null';
                }
            }

        } catch (Exception e) {
            System.debug('BankStatement_AllTransactionsCtrl.getTransactionsList: ' + e.getMessage() + e.getStackTraceString());
            bsate.errorMessage = 'An exception occured while getting the transactions list';
        }

        return bsate;
    }

    public class BankStatement_AllTransactionsEntity{

        @AuraEnabled public Boolean hasError { get; set; }
        @AuraEnabled public String errorMessage {
            get;
            set{
                errorMessage = value;
                hasError = true;
            }
        }
        @AuraEnabled public DL_AccountStatementSF statement { get; set; }

        public BankStatement_AllTransactionsEntity(){
            this.hasError = false;
        }
    }
}