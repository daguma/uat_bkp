global class CollectionsCasesAssignmentJob implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    global String query;
    global String jobName {
        get;
        set {
            jobName = value;
            this.query = CollectionsCasesAssignmentManager.getQueryforJob(jobName);
        }
    }
    global CollectionsCasesAssignmentUsersStructure collectionsUsersStructure;
    global CollectionsCasesAssignmentStructure collectionsAssignmentStructure;
    global CollectionsCasesAssignmentHelper collectionsCasesAssignmentHelper;
    global Boolean runNextProcess;
    global CollectionsCasesAssignmentAudit auditProcess;
    global Boolean hasAlreadyBeenScheduledToday;

    global CollectionsCasesAssignmentJob() {
        this.jobName = CollectionsCasesAssignmentManager.DELETE_JOB;
        this.runNextProcess = true;
        this.hasAlreadyBeenScheduledToday = false;
    }

    global CollectionsCasesAssignmentJob(String jobName, Boolean runNextProcess) {
        this.jobName = jobName;
        this.runNextProcess = runNextProcess;
        this.hasAlreadyBeenScheduledToday = false;
    }

    global void scheduleAnalyzeJobForFailures() {
        this.collectionsUsersStructure = new CollectionsCasesAssignmentUsersStructure();
        this.runNextProcess = true;
        this.jobName = CollectionsCasesAssignmentManager.ANALYZE_JOB;
        String failedContracts = '\'';
        if (this.auditProcess != null && this.auditProcess.failedAnalyzedContracts != null && !this.auditProcess.failedAnalyzedContracts.isEmpty()) {
            for (String myId : this.auditProcess.failedAnalyzedContracts) {
                failedContracts += myId + '\',\'';
            }
            this.query += ' AND Id in (' + failedContracts.left(failedContracts.length() - 2) + ')';
            //Clears the "failed lists" so the next run populates them with NEW errors (if any)
            this.auditProcess.initFailedLists();
            this.hasAlreadyBeenScheduledToday = true;
            System.scheduleBatch(this, 'Analyze failed contracts ' + Date.today().format(), 30);  //Run 60 minutes from current time
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return CollectionsCasesAssignmentManager.start(BC,this);
    }

    global void execute(Database.BatchableContext BC, List<Object> scope) {
        CollectionsCasesAssignmentManager.executeBatchableContext(BC, scope, this);
    }

    global void execute(SchedulableContext sc) {
        CollectionsCasesAssignmentManager.executeSchedulableContext(sc, this);
    }

    global void finish(Database.BatchableContext BC) {
        CollectionsCasesAssignmentManager.finish(BC, this);
    }

}

/*
    From Anonymous:
     
     - To run the full process just execute the following line: 
        database.executebatch(new CollectionsCasesAssignmentJob(), 145);

     - To run a specific process only, just indicate the name of the process in the constructor and "FALSE" for the runNextProcess parameter
       For example, if you just want to run the ANALYZE process, then execute the following line:
        database.executebatch(new CollectionsCasesAssignmentJob(CollectionsCasesAssignmentManager.DELETE_JOB,false), 145);
        database.executebatch(new CollectionsCasesAssignmentJob(CollectionsCasesAssignmentManager.ANALYZE_JOB,false), 145);
        If you need to run the ASSIGN job, just execute the ANALYZE with the second parameter as TRUE
*/