@istest
public class StatusHistoryCtrlTEST {

    public static testmethod void TestController(){        
    
        
        TestHelper tt = new TestHelper();
        contact c =  TestHelper.createContact();        
        lead l = TestHelper.createLeadUnique();         
        genesis__Applications__c g = TestHelper.createApplication();
        g.genesis__Status__c = 'APPROVED - APPROVED';
        Update g;
        
      
        ApexPages.StandardController sc = new ApexPages.StandardController(g);
        StatusHistoryCtrl sg= new StatusHistoryCtrl(sc);
        //sg.Sceneriotest = false; 
        sg.leadid = l.id;
        sg.Appid = g.id;
        sg.getHistories();
        sg.getAppHistory();
        sg.getContHistory();
        sg.getLeadHistory();
        
        //test if no updates are there 
        sg.Sceneriotest = true;        
        sg.getHistories();
        sg.getAppHistory();
        sg.getContHistory();
        sg.getLeadHistory();
        
    }    
    
}