global class ReversalReasonProcessBuilder {
    
    @InvocableMethod
    global Static void ReversalReason(List<Id> idVal) { //List<Id> idVal
        
        loan__Repayment_Transaction_Adjustment__c reversal=[select id,loan__Loan_Payment_Transaction__r.loan__Loan_Account__c, loan__Reason_Code__c from loan__Repayment_Transaction_Adjustment__c where id IN:idVal limit 1 ];
        System.debug('Reversal ---- ' + reversal);
        //loan__Loan_Account__c Contract = [select id,Latest_Reason_Code__c from loan__loan_Account__c where id=:'a1uU0000000A9IS'];//reversal.id]; // idVal.get(0)]
        loan__Loan_Account__c Contract = [select id,Latest_Reason_Code__c from loan__loan_Account__c where id=:reversal.loan__Loan_Payment_Transaction__r.loan__Loan_Account__c ]; //'a1uU0000000A9IS' idVal.get(0)] IN:idVal
        system.debug('Contract id - '+ contract.id); 
        // system.debug('Contract--------------------->'+ Contract);
        
        
        list<loan__Loan_Payment_Transaction__c> Payment = [select id,
                                                           loan__Reversal_Reason__c,
                                                           //loan__Loan_Account__c,
                                                           Is_Latest__c
                                                           from loan__Loan_Payment_Transaction__c 
                                                           where loan__Loan_Account__c=:Contract.id
                                                           //AND loan__Reversal_Reason__c!=null 
                                                           AND loan__reversed__c = True
                                                           order by CreatedDate desc];
        System.debug('Payments --- '+ payment);  
        integer count = 0;
        for(loan__Loan_Payment_Transaction__c pay : Payment)
        {
            
            if(count == 0){
                pay.Is_Latest__c = true;
                count += 1;
                pay.loan__Reversal_Reason__c = reversal.loan__Reason_Code__c;
                contract.Latest_Reason_Code__c = pay.loan__Reversal_Reason__c;
            }
            else{
                pay.Is_Latest__c = false;
            }
            // pay.Is_Latest__c = true;
            
            System.debug('True id ---' + pay);
            
        }
        update Payment;
        
        //contract.Latest_Reason_Code__c = payment.get(0).loan__Reversal_Reason__c;
        update contract;
        
        System.debug('Latest Reason Code ---- ' + contract.Latest_Reason_Code__c);
        
        /*	list<loan__Loan_Payment_Transaction__c > All_is_Latest = [select id,loan__Reversal_Reason__c from loan__Loan_Payment_Transaction__c where Is_Latest__c=true];
for(loan__Loan_Payment_Transaction__c paylist :All_is_Latest)
{
if(All_is_Latest.id != idval.get(0)) {
paylist.
}         
else
{
pay.Is_Latest__c = true; 



} */
        // System.debug('Check-------------------' +All_is_Latest.iterator());
        //  loan__Loan_Account__c IsContract =[select id from loan__Loan_Account__c where id IN:All_is_Latest.id limit 1];
    }
    
}