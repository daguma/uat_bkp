@isTest
public class ClarityClearRecentHistoryTest {
    @isTest static void validate() {
        
        ClarityClearRecentHistory crhParam = new ClarityClearRecentHistory();
        
        crhParam.ruleName = '90_days_ago';
        system.assertEquals('90_days_ago',crhParam.ruleName);
    
        crhParam.ruleDesc = 'test data';
        system.assertEquals('test data',crhParam.ruleDesc );
        
        crhParam.ruleValue= 4;
        system.assertEquals(4,crhParam.ruleValue);
        
        crhParam.ruleCategory= 'test data';
        system.assertEquals('test data',crhParam.ruleCategory);
        
        crhParam.isRulePass = true;
        system.assertEquals(true,crhParam.isRulePass);
        
        
    }
}