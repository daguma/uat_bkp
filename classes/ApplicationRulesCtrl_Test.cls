@isTest
public class ApplicationRulesCtrl_Test {
    testmethod static void test1(){
         Opportunity app = TestHelper.createApplication();
        ApplicationRulesCtrl.getCRApplicationRules(app.id);
        ApplicationRulesCtrl.ApplicationRule aarule = new ApplicationRulesCtrl.ApplicationRule();
        ApplicationRulesCtrl.ApplicationRule aarule1 = new ApplicationRulesCtrl.ApplicationRule('test');
		
		List<ProxyApiCreditReportRuleEntity> pcrList = new List<ProxyApiCreditReportRuleEntity>();
		ProxyApiCreditReportRuleEntity softPullRule = new ProxyApiCreditReportRuleEntity();
		softPullRule.deIsDisplayed = 'Yes';
		softPullRule.deRuleCategory = 'test';
		softPullRule.deRuleName = 'test';
		softPullRule.deRuleStatus = 'Pass';
		//softPullRule.deRuleStatus = 'fail';

		//softPullRule.deRuleValue = 'test';
		softPullRule.deHumanReadableDescription = 'fail';
		softPullRule.deActionInstructions = 'fail';
		pcrList.add(softPullRule);		
		ApplicationRulesCtrl.getApplicationRules(pcrList);
    }
	testmethod static void test2(){
         Opportunity app = TestHelper.createApplication();
        ApplicationRulesCtrl.getCRApplicationRules(app.id);
        ApplicationRulesCtrl.ApplicationRule aarule = new ApplicationRulesCtrl.ApplicationRule();
        ApplicationRulesCtrl.ApplicationRule aarule1 = new ApplicationRulesCtrl.ApplicationRule('test');
		
		
		List<ProxyApiCreditReportRuleEntity> pcrList = new List<ProxyApiCreditReportRuleEntity>();
		ProxyApiCreditReportRuleEntity softPullRule = new ProxyApiCreditReportRuleEntity();
		softPullRule.deIsDisplayed = 'Yes';
		softPullRule.deRuleCategory = 'test';
		softPullRule.deRuleName = 'test';
		//softPullRule.deRuleStatus = 'Pass';
		softPullRule.deRuleStatus = 'fail';

		//softPullRule.deRuleValue = 'test';
		softPullRule.deHumanReadableDescription = 'fail';
		softPullRule.deActionInstructions = 'fail';
		pcrList.add(softPullRule);	
		ApplicationRulesCtrl.getApplicationRules(pcrList);
    }
}