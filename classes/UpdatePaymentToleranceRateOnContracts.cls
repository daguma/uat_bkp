/*This job would populate Payment Tolerance Rate to null value if the previous value is zero. */

global class UpdatePaymentToleranceRateOnContracts implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'SELECT Id,';
        query +=              'loan__Payment_Tolerance_Rate__c ';
        query +=        'FROM loan__Loan_Account__c ';
        query +=        'WHERE loan__Payment_Tolerance_Rate__c = 0';

        return Database.getQueryLocator(query) ;
    }

    global void execute(Database.BatchableContext bc, List<loan__Loan_Account__c> contracts){
        try{
            for(loan__Loan_Account__c contract : contracts){
                contract.loan__Payment_Tolerance_Rate__c = null;
            }
            update contracts;
        }
        catch(Exception e){
            system.debug(LoggingLevel.ERROR, 'Error occured while updating contracts : '+ e.getMessage());
            throw e;
        }
    }

    global void finish(Database.BatchableContext bc){}

}