@isTest public class ContactToApplicationConverterTest {

    @isTest static void converts_contact_to_app_with_idology_verification() {
        try {

            Test.startTest();

            opportunity app = LibraryTest.createApplicationTH();
			Opportunity o = [Select id from Opportunity LIMIT 1];
            o.Name = 'APP-0232232';
            update o;
            Contact c1 = [Select Id, Name from Contact where Id = : app.Contact__c];

            IDology_Request__c idology = new IDology_Request__c();
            idology.Verification_Result__c = 'Pass';
            idology.Contact__c = c1.id;
            insert idology;

            String result = ContactToApplicationConverter.ContactToAppWithIdologyVerification(c1.id);

            System.assert(result.startsWith('You cannot submit a new application at this time because other the customer has had an application'), result);

            Test.stopTest();
        } catch (JSONException jse) {
            System.debug('JSONException' + jse.getMessage());
        } catch (Exception e) {
            System.debug('Catch up an exception: ContactToAppWithIdologyVerification ' + e.getMessage());
        }
    }

    @isTest static void renewals_contact_to_app() {
        Contact cntct = LibraryTest.createContactTH();
        String result = ContactToApplicationConverter.ContactToAppRenewal(cntct.id);
        System.assertEquals('Only Admins can run a renewal for now.\n', result);

    }

    @isTest static void processes_contact_submission() {

        try {

            if ([select Id from MaskSensitiveDataSettings__c].size() == 0 ) {
                MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();
                maskSettings.MaskAllData__c = false;
                insert maskSettings;
            }

            Contact testCntct = new Contact();
            testCntct.FirstName = 'Test Name';
            testCntct.Email = 'test@gmail2.com';
            testCntct.Point_Code__c = '1234AB';
            testCntct.Phone = '1251231233';
            testCntct.MailingCountry = 'US';
            testCntct.Lastname = 'Testcase';
            testCntct.BirthDate = Date.Today().addYears(-30);
            testCntct.Employment_Start_date__c = Date.Today().addYears(-3);
            testCntct.ints__Social_security_Number__c = '999999999';
            testCntct.MailingStreet = 'Test street';
            testCntct.MailingCity = 'City';
            testCntct.MailingState = 'OOO';
            testCntct.MailingPostalCode = '123132';
            testCntct.Loan_Amount__c = 4000;
            testCntct.Annual_Income__c = 100000;
            insert testCntct;

            opportunity app = LibraryTest.createApplicationTH();

            Contact cntct = [Select Id, AccountId, Previous_Employment_End_Date__c,
                             Previous_Employment_Start_Date__c, Firstname, Lastname, Email, birthdate, Point_Code__c,
                             ints__Social_Security_Number__c, Phone, Time_at_current_address__c, SSN__c,
                             MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Annual_Income__c,
                             Use_of_Funds__c, Others__c,
                             Loan_Amount__c, Employment_start_date__c, Lead__c from Contact where Id = : app.Contact__c];

            String result = ContactToApplicationConverter.ProcessContactSubmission(testCntct.id, true, '');
            System.assert(result.containsIgnoreCase('Time at Current Address is Empty'));

            testCntct.MailingState = 'GA';
            testCntct.Time_at_current_Address__c = Date.Today().addYears(-4);

            result = ContactToApplicationConverter.ProcessContactSubmission('0', true, '');
            System.assertEquals('Contact could not be found\n', result);

            result = ContactToApplicationConverter.ProcessContactSubmission(cntct.id, true , '');

            System.assertNotEquals(null, result);
            System.assertNotEquals('Error in Application submission.\n', result);
            System.assert(!result.containsIgnoreCase('Firstname is Empty'));
            System.assert(!result.containsIgnoreCase('Lastname is Empty'));
            System.assert(!result.containsIgnoreCase('Date of Birth is Empty'));
            System.assert(!result.containsIgnoreCase('Employment Start Date is Empty'));
            System.assert(!result.containsIgnoreCase('SSN is Empty'));
            System.assert(!result.containsIgnoreCase('Street in Address Information is Empty'));
            System.assert(!result.containsIgnoreCase('City in Address Information is Empty'));
            System.assert(!result.containsIgnoreCase('State in Address Information is Empty'));
            System.assert(!result.containsIgnoreCase('PostalCode in Address Information is Empty'));
            System.assert(!result.containsIgnoreCase('Loan Amount is Empty'));
            System.assert(!result.containsIgnoreCase('Annual Income is Empty'));
            System.assert(!result.containsIgnoreCase('Time at Current Address is Empty'));
            System.assert(!result.containsIgnoreCase('State is not a permitted one'));
            System.assertNotEquals('Contact could not be found\n', result);
            System.assertNotEquals('You cannot submit a new application at this time because other the customer has had an application\n'  
                    + 'created in the last 30 days, or, an application was declined within the last 180 days..\n', result);

        } catch (Exception e) {
            System.debug('Exception ProcessContactSubmission: ' + e.getMessage());
        }
    }

    @isTest static void validates_contact() {

        Contact cntct = LibraryTest.createContactTH();
        cntct.Email = 'test@gmail2.com';
        cntct.Point_Code__c = '1234AB';
        cntct.Phone = '1251231233';
        cntct.MailingCountry = 'US';
        cntct.FirstName = '';
        cntct.LastName = '';
        cntct.birthdate = null;
        cntct.Employment_Start_Date__c  = null;
        cntct.SSN__c = '';
        cntct.MailingStreet = '';
        cntct.MailingCity = '';
        cntct.MailingState = '';
        cntct.MailingPostalCode = '';
        cntct.Loan_Amount__c = null;
        cntct.Annual_Income__c  = null;
        cntct.Time_at_current_Address__c = null;
        System.assert(ContactToApplicationConverter.validate(cntct).containsIgnoreCase('Firstname is Empty'));
        cntct.FirstName = 'Test Name';
        System.assert(ContactToApplicationConverter.validate(cntct).contains('Lastname is Empty'));
        cntct.Lastname = 'Testcase';
        System.assert(ContactToApplicationConverter.validate(cntct).contains('Date of Birth is Empty'));
        cntct.BirthDate = Date.Today().addYears(-30);
        System.assert(ContactToApplicationConverter.validate(cntct).contains('Employment Start Date is Empty'));
        cntct.Employment_Start_date__c = Date.Today().addYears(-3);
        System.assert(ContactToApplicationConverter.validate(cntct).contains('SSN is Empty'));
        cntct.SSN__c = '999999999';
        System.assert(ContactToApplicationConverter.validate(cntct).contains('Street in Address Information is Empty'));
        cntct.Mailingstreet = 'Test street';
        System.assert(ContactToApplicationConverter.validate(cntct).contains('City in Address Information is Empty'));
        cntct.MailingCity = 'City';
        System.assert(ContactToApplicationConverter.validate(cntct).contains('State in Address Information is Empty'));
        cntct.MailingState = 'TC';
        System.assert(ContactToApplicationConverter.validate(cntct).contains('PostalCode in Address Information is Empty'));
        cntct.MailingPostalCode = '123132';
        System.assert(ContactToApplicationConverter.validate(cntct).contains('Loan Amount is Empty'));
        cntct.Loan_Amount__c = 4000;
        System.assert(ContactToApplicationConverter.validate(cntct).contains('Annual Income is Empty'));
        cntct.Annual_Income__c = 100000;
        System.assert(ContactToApplicationConverter.validate(cntct).contains('Time at Current Address is Empty'));
        cntct.Time_at_current_Address__c = Date.Today().addYears(-3);
        System.assert(ContactToApplicationConverter.validate(cntct).contains('State is not a permitted one'));
        cntct.MailingState = 'GA';
        System.assertEquals('true', ContactToApplicationConverter.validate(cntct));
    }

    @isTest static void creates_contact_refinance() {

        Contact c = LibraryTest.createContactTH();
        LibraryTest.createLPCustom('Sample Loan Product', 'LendingPoint');

        loan__Loan_Product__c lp = LibraryTest.createLendingProductTH('Sample Loan Product');

        opportunity app = new opportunity();
        app.Type = 'New';

        app.Contact__c = c.Id;
        app.Lending_Product__c = [Select Id from  Lending_Product__c where NAme = 'Sample Loan Product' limit 1].Id;
        app.Product_Type__c = 'LOAN';
        app.Amount = c.Loan_Amount__c;
        app.Days_Convention__c = '30/360';
        app.Fee__c = 0.04 * (c.Loan_Amount__c == null ? 0.0 : c.Loan_amount__c);
        app.Interest_Calculation_Method__c = 'Declining Balance';



        app.Expected_First_Payment_Date__c = Date.today().addDays(15);
        app.Term__c = 1;
        app.Expected_Start_Date__c = Date.today().addDays(2);
        app.ACH_Account_Number__c = '1234567890';
        app.ACH_Account_Type__c = 'Test';
        app.ACH_Bank_Name__c = 'Test Bank';
        app.Payment_Amount__c = 200;
        app.Payment_Frequency__c = 'Monthly';
        app.ACH_Routing_Number__c = '123456789';
        app.Payment_Method__c = 'ACH';
        app.Expected_Disbursal_Date__c = Date.today().addMonths(2);
        app.Estimated_Grand_Total__c = 50000;
        app.Payroll_Frequency__c = 'Monthly';
        app.DTI__C = '18.000';
        app.FICO__c = '700';
        app.Name = 'Test Opp';
        app.StageName = 'Closed Lost';
        app.CloseDate = system.today();

        insert app;

        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'TU';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        insert acc;

        c.AccountId = acc.Id;
        update c;

        loan__Loan_Account__c loan = new loan__Loan_Account__c();
        loan.loan__Payment_Amount__c = 250;
        loan.loan__Delinquent_Amount__c = 0;
        loan.loan__Fees_Remaining__c = 0;
        loan.loan__Next_Installment_Date__c = Date.today().addDays(6);
        loan.loan__Contact__c = c.Id;
        loan.Opportunity__c = app.Id;
        loan.loan__Number_of_Installments__c = 18;
        loan.loan__APR__c = 0.2399;

        loan.loan__First_Installment_Date__c = Date.today().addDays(1);
        loan.loan__Frequency_of_Loan_Payment__c = 'Monthly';
        loan.loan__Term_Cur__c = 2;
        loan.loan__Maturity_Date_Current__c = Date.today().addDays(2);
        loan.loan__Loan_Amount__c = c.Loan_Amount__c;

        insert loan;

        Scorecard__c sc = new Scorecard__c();
        sc.Grade__c = 'B1';
        sc.Acceptance__c = 'Y';
        sc.Opportunity__c = app.id;

        insert sc;

        CreateLoanAccountForLoanTypeHandler loanAccount = new CreateLoanAccountForLoanTypeHandler(app, loan, lp);

        String result = loanAccount.createLoanAccount();

        System.debug(result);
        result = ContactToApplicationConverter.ProcessContactSubmission(c.id, true , loan.Id);

    }

    @isTest static void converts_contact_to_app() {
        LP_Custom__c lp = new LP_Custom__c();

        //lp.Name = 'a6gU0000000TN3V';
        //lp.SetupOwnerId = '00D0v0000000MZoEAM';
        lp.Default_Company__c = 'LendingPoint';
        lp.Default_Lending_Product__c = 'Lending Point';
        lp.SupportedStates__c = 'GA,NM,UT,MO,MT,SD,OR,AL,MI,WA,OH,CA,DE,NJ,TX,IL';
        //lp.Email_Alert_Portal_Apps__c = '005U0000005jmmG';
        //lp.Docusign_Notification_Email__c = '005U0000005jmmQ';
        //lp.LendingTree_Offer_Alert__c = '005U0000005jmfZ';
        //lp.Super_Money_Sub_Source__c = 'Supermoney';
        //lp.Super_Money_URL__c = 'http://track.supermoney.com/aff_lsr?offer_id=198&transaction_id=';
        lp.Use_Offer_API__c = true;
        //lp.Developer_Alert__c = '005U0000003q3xB';
        //lp.Offer_Selection_Alert__c = '005U0000005jmmB';
        //lp.System_Admin_Profile_ID__c = '00eU0000000iZ9XIAU';
        //lp.WEB_API_User_Id__c = '005U0000003pdKWIAY';
        //lp.Saaquatch_API_Key__c = 'TElWRV9GZmxCeVJvaDluWG81MVJXOGhhSGs3cDF4TTBheGhLcjo=';
        //lp.SaasQuatch_URL__c = 'https://app.referralsaasquatch.com/api/v1/';
        //lp.Saasquatch_Reward_Amount__c = 100;
        //lp.Saasquatch_Tenant_ID__c = 'akoyozezogjie';
        lp.App_Contract_Start_Offset_days__c = 2;
        lp.Online_Payment_Alert__c = 'test@test.com';
        lp.Days_before_Declined_Retry__c = 180;
        //lp.Offer_Selection_Override_Profiles__c = '00eU0000000iZ9XIAU';
        //lp.Override_Profiles__c = '00eU0000000iZ9XIAU, 00e0B000000uLJkQAM';
        lp.dummy_offers_email_domains__c = 'DONOTDELETETHISINPRODUTION!';
        lp.AAN_To_Send_Per_Batch__c = 40;
        lp.States_To_Assign_Immediately__c = 'GA,NM,UT,MO,MT,SD,OR,AL,MI,WA,OH,CA,DE,NJ,TX,IL';
        insert lp;

        Contact c1 = LibraryTest.createContactTH();

        String result = ContactToApplicationConverter.ContactToApp(c1.id);

        //System.assertEquals(String.valueOf(result).substring(0, 3), 'a3l');
    }

}