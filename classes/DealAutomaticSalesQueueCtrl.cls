public with sharing class DealAutomaticSalesQueueCtrl {

    public DealAutomaticSalesQueueCtrl() {

    }

    /**
     * Get all the categories and statuses from the Deal Automatic Attributes
     * @return List of Deal_Automatic_Attributes__c
     */
    public static String getSalesQueues() {
        return JSON.serialize([SELECT Id__c, Name, Display_Name__c, Usage__c
                               FROM Deal_Automatic_Attributes__c
                               WHERE Usage__c LIKE :DealAutomaticAssignmentUtils.SALES_QUEUE_USAGE
                               ORDER BY Display_Name__c]);
    }

    /**
     * [getUsersGroupsByCategory description]
     * @param  salesQueue [description]
     * @return          [description]
     */
    @RemoteAction
    public static List<DealAutomaticUserGroup> getUsersGroupsBySalesQueue(Deal_Automatic_Attributes__c salesQueue) {
        return DealAutomaticAssignmentUtils.getUsersGroupsByCategory(salesQueue);
    }

    /**
    * [saveUsers description]
    * @param  attribute to save the selected users
    * @param  usersGroups    to save in the specific salesQueue
    * @return Save the selected users in the Deal_Automatic_Assignment_Data__c object
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> saveUsers(Deal_Automatic_Attributes__c attribute,
            List<DealAutomaticUserGroup> newUsersGroups,
            List<DealAutomaticUserGroup> oldUserGroups) {
        return DealAutomaticAssignmentUtils.saveUsers(attribute, newUsersGroups, oldUserGroups);
    }
}