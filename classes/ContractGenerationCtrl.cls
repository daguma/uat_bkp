public with sharing class ContractGenerationCtrl {

    @AuraEnabled
    public static ContractGenerationEntity getPicklistValues(Opportunity opp) {

        ContractGenerationEntity cge = new ContractGenerationEntity();

        try {

            //Set the picklist values
            cge.contractDocumentOptions =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Contract_Document__c.getDescribe(),
                            opp == null ? null : opp.Contract_Document__c,
                            true
                    );
            cge.contractReceivedOptions =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Contract_Received_Through__c.getDescribe(),
                            opp == null ? null : opp.Contract_Received_Through__c,
                            true
                    );
            cge.creditFormOptions =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Credit_Form__c.getDescribe(),
                            opp == null ? null : opp.Credit_Form__c,
                            true
                    );

        } catch (Exception e) {
            cge.errorMessage = e.getMessage() + ' -> ' + e.getStackTraceString();
            return cge;
        }

        return cge;
    }

    public class ContractGenerationEntity {
        @AuraEnabled public Boolean isContractReturnedDateRequired { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> contractDocumentOptions {
            get;
            set {
                contractDocumentOptions = value;
                for (alphaHelperCls.PicklistItem pli : contractDocumentOptions) {
                    if (pli.isSelected) {
                        if (pli.value == 'DocuSign' || pli.value == 'Fax or Scan') {
                            isContractReturnedDateRequired = true;
                        }
                    }
                }
            }
        }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> contractReceivedOptions { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> creditFormOptions { get; set; }
        @AuraEnabled public Boolean hasError { get; set; }
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = false;
            }
        }

        public ContractGenerationEntity() {
            hasError = false;
            isContractReturnedDateRequired = false;
        }
    }

}