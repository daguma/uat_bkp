@isTest public class DL_ActivityByUserReportTest {

    @isTest static void validates_instance() {

        DL_ActivityByUserReport activityReport = new DL_ActivityByUserReport();
        
        DL_ActivityByUserReport.ActivityByUserReportLine activityReportLine = new DL_ActivityByUserReport.ActivityByUserReportLine();
        activityReportLine.salesforceUserId = '1';
        activityReportLine.total = 1;
        activityReportLine.not_Started = 1;
        activityReportLine.started_not_Completed = 1;
        activityReportLine.bank_Error = 1;
        activityReportLine.account_Error = 1;
        activityReportLine.login_not_Verified = 1;
        activityReportLine.login_Verified = 1;
        activityReportLine.dateRange = 1;   
     
        List<DL_ActivityByUserReport.ActivityByUserReportLine> activityRepList = new List<DL_ActivityByUserReport.ActivityByUserReportLine>();

        activityRepList.add(activityReportLine);

        activityReport.reportLines = activityRepList;

        System.assertNotEquals(null, activityReport.reportLines);
        System.assertEquals(activityRepList, activityReport.reportLines);

    }

}