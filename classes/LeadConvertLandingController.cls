public class LeadConvertLandingController{
 
    Id newId = null;
    public LeadConvertLandingController(){
        newId = ApexPages.CurrentPage().getParameters().get('newid');
        system.debug('============newId=============='+newId);
    }
 
    public PageReference redirect(){
       
        Lead convertedLead = [select ConvertedContactId from Lead where id=:newId];
        PageReference pRef = new PageReference('/' + convertedLead.ConvertedContactId);
        pRef.setRedirect(true);
        return pRef;
    }
}