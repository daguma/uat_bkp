@isTest
public Class RefinanceNotificationController_Test{
    
 
    @isTest static void sendRBPNotificationEmailTest2(){
        //try {
            //opportunity aplcn = LibraryTest.createApplicationTH();

            //opportunity aplcnToSelect = [select id, Contact__c, Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id limit 1];

            //Contact cntct = [Select Id, Name from Contact where Id = : aplcnToSelect.Contact__c];
               
              Test.startTest(); 
                
                Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
                
                Contact cntct = new contact();
                cntct.Email = 'test@gmail2.com';
                cntct.Point_Code__c = '1234AB';
                cntct.Phone = '1251231233';
                cntct.MailingCountry = 'US';
                cntct.FirstName = 'Test Name';
                cntct.Lastname = 'Testcase';
                cntct.BirthDate = Date.Today().addYears(-30);
                cntct.Employment_Start_date__c = Date.Today().addYears(-3);
                cntct.SSN__c = '999999999';
                cntct.Mailingstreet = 'Test street';
                cntct.MailingCity = 'City';
                cntct.MailingPostalCode = '123132';
                cntct.Loan_Amount__c = 4000;
                cntct.Annual_Income__c = 100000;
                cntct.Time_at_current_Address__c = Date.Today().addYears(-3);
                cntct.MailingState = 'GA';
            insert cntct;
            
           Email_Age_Settings__c eas = new Email_Age_Settings__c();
               eas.Call_Email_age__c =true;
               eas.API_Key__c = '3EB963CCFE0C46EFB5E08CBD945F9ED0';
               eas.API_URL__c = 'https://sandbox.emailage.com/emailagevalidator/';
               eas.Auth_Token__c = 'C85CD3F6DF954E20BED7681864A5FE92';
               eas.Expiry_Days__c = 30;
               eas.name = 'settings';
           insert eas;
            
            Account acc = new Account();
                acc.name = 'Refinance';
            insert acc;    
            
            Opportunity opp = new Opportunity();
                opp.Name = 'APP-Test Opportunity';
                opp.CloseDate = Date.today().addDays(5);
                opp.StageName = 'Open';
                opp.Partner_Account__c = acc.id;
                opp.Type = 'Refinance';
                opp.Contact__c = cntct.id;
                opp.Clarity_Status__c = true;
                opp.Reason_of_Opportunity_Status__c = 'Cannot Verify Information Provided';
            insert opp;
            
            Email_Age_Details__c ead = new Email_Age_Details__c();
                ead.Contact__c = cntct.id;
                ead.Email_Age_Status__c = false;
            insert ead;
            Map<Id, Email_Age_Details__c> mAges = new Map<Id, Email_Age_Details__c>();
               mAges.put(ead.Contact__c,ead); 
                        
            DateTime testDate = System.now();

            EmailTemplate eTemplate;
            string AAN_TEMPLATE = 'Risk_Based_Pricing_Notice_Finwise_Refinances';

           // try {
                //Test.setMock(HttpCalloutMock.class, new SMSCalloutMock());
                
                //Test.startTest();
                    eTemplate = [select id, name, HtmlValue, Subject, Body from EmailTemplate where developername = : AAN_TEMPLATE];
                    string appid = opp.id;
                    
                    RefinanceNotificationController notification_job = new RefinanceNotificationController();
                    RefinanceNotificationController.sendRBPNotificationEmail(appid);
                    
                    ProxyApiCreditReportEntity creditReportEntity = LibraryTest.fakeCreditReportEntity(opp.id);
                    String result = RefinanceNotificationController.replaceValuesFromData(eTemplate.body, cntct, creditReportEntity, opp,  true);
                    boolean bln = RefinanceNotificationController.isThirdPartyQualify(opp,creditReportEntity,mAges);
                    
                Test.stopTest();
                
                System.assertNotEquals(null, result);
                System.debug(result);
            /*} catch (Exception e) {
                System.debug ('[U-03] Unable to locate EmailTemplate');
            }*/

       /* } catch (Exception e) {
            System.debug('AANNotificationJob: replaceValuesFromData: ' + e.getMessage());
        }*/
    }
}