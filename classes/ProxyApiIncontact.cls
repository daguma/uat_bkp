public class ProxyApiIncontact {
	private static final String GET_AGENTS_STATE = '/v101/in-contact-logic/get-agents-state/';

	public ProxyApiIncontact(){}
	
    public static List<ProxyApiIncontact.AgentsStateEntity> getAgentsState(String body){
        System.debug('Entra::');
		List<AgentsStateEntity> userIdList = new List<AgentsStateEntity>();
		if (ProxyApiBRMSUtil.ValidateBeforeCallApi() || test.isRunningTest()){
			try{
				HttpResponse response = ProxyApiBRMSUtil.ApiGetHttpResponse(GET_AGENTS_STATE, body); 
                if (response.getStatusCode() == 200) {userIdList = (List<AgentsStateEntity>) JSON.deserialize(response.getBody(), List<AgentsStateEntity>.class);}   
                else if (response.getStatusCode() == 400) SalesForceUtil.EmailByUserId('Incontact Bad Request.', String.valueOf(response), '0050B0000089FUNQA2'); else System.debug('Incontact/ No Content.');
			} catch (Exception e) {System.debug('error--> ' +e);}
		}
		return userIdList;	
	}

	public class AgentsStateEntity{
        public String userId {get; set;}
	}
}