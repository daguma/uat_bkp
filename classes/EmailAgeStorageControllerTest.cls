@isTest public class EmailAgeStorageControllerTest {

    @isTest static void saves_email_age_pass_status() {

        TestHelper.createEmailAgeSettings();

        Opportunity app = LibraryTest.createApplicationTH();

        String jsonMockResponse = TestHelper.sendEmailAgeMockPassResponse(app.Id);

        EmailAgeStorageController.saveEmailAgeDetails(jsonMockResponse);
        
        System.debug('Response' + jsonMockResponse);

        String status = EmailAgeStorageController.emailAgeCallStatusCode(app.Id, 'rmuradian@lendingpoint.com');
        //system.assertEquals(status, '3');
    }

    @isTest static void saves_email_age_fail_status() {

        TestHelper.createEmailAgeSettings();

        Opportunity app = LibraryTest.createApplicationTH();

        String jsonMockResponseOne = TestHelper.sendEmailAgeMockFailResponse(app.id);

        EmailAgeStorageController.saveEmailAgeDetails(jsonMockResponseOne);

        String statusOne = EmailAgeStorageController.emailAgeCallStatusCode(app.Id, 'rmuradian@lendingpoint.com');
        //system.assertEquals(statusOne, '2');
    }

    @isTest static void no_email_age_records_found() {

        TestHelper.createEmailAgeSettings();

        Opportunity app = LibraryTest.createApplicationTH();

        String jsonMockResponseTwo = TestHelper.sendEmailAgeMockFailResponse(app.id);

        EmailAgeStorageController.saveEmailAgeDetails(jsonMockResponseTwo);
        
        Contact co = [Select Id, Email from Contact LImit 1];
        Email_Age_Details__c d = [Select Id, Email__C from Email_Age_Details__c limit 1];
        d.Email__c = co.Email;
    update d;
        
        String statusTwo = EmailAgeStorageController.emailAgeCallStatusCode(app.Id);

        List<Email_Age_Details__c> ems = EmailAgeStorageController.getEmailAgeRecord(app.id) ;
        system.assert(ems.size() > 0, 'No Emailage records found');

        ems = EmailAgeStorageController.getEmailAgeRecord(app.Contact__c) ;
        system.assert(ems.size() > 0, 'No Emailage records found');
    }

 
 /*

    @isTest static void encodes_query_params() {
        String targetString = 'test%40test.com%2b10.10.8.124';
        String decodedString = EmailAgeStorageController.encodeQueryParams(targetString);
        system.assertEquals(decodedString, 'test@test.com+10.10.8.124');
    }

    @isTest static void calls_email_age_status_code_with_app() {

        TestHelper.createEmailAgeSettings();

        Opportunity app = LibraryTest.createApplicationTH();

        String status = EmailAgeStorageController.emailAgeCallStatusCode(app.Id);
        system.assertEquals(status, '0');
    }

    @isTest static void calls_email_age_status_code_with_contact() {

        TestHelper.createEmailAgeSettings();

        Opportunity app = LibraryTest.createApplicationTH();

        String status = EmailAgeStorageController.emailAgeCallStatusCode(app.Contact__c);
        system.assertEquals(status, '0');
    } */
}