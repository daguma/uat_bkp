public class ProxyApiBankStatements extends ProxyApiUtil {
    private static final String GET_STAND_ALONE_URL = '/v101/decision-logic/get-stand-alone-url/';
    private static final String GET_STATEMENTS = '/v101/decision-logic/get-statements/';
    private static final String GET_SPECIFIC_TRANSACTIONS = '/v101/decision-logic/get-specific-transactions/';
    private static final String GET_BANK_INFORMATION_BY_ROUTING_NUMBER = '/v101/decision-logic/get-bank-information-by-routing-number/';
    private static final String GET_BANK_INFORMATION_BY_BANK_NAME = '/v101/decision-logic/get-bank-information-by-bank-name/';

    /**
    * ApiGetStandAloneLink Call Proxy Api to get a Decision Logic request code
    * @param contactId
    * @param firstName
    * @param lastName
    * @param accountNumber
    * @param routingNumber
    * @param contentServiceId
    * @param applicationId
    * @param userId
    * @return StandAloneLink
    */
    public static String ApiGetStandAloneLink(String contactId, String firstName, String lastName, String accountNumber, String routingNumber,
            String contentServiceId, String applicationId, String userId) {
        if (!ProxyApiUtil.ValidateBeforeCallApi())
            return null;

        String standAloneLink = null;

        if (Test.isRunningTest()) {
            if (contactId != '' && contactId != null && applicationId != '' && applicationId != null) {
                standAloneLink = 'https://www.lendingpoint.com/apply/#/dl/c/XXXXXX';
            }
        } else {
            standAloneLink = ProxyApiUtil.ApiGetResult(GET_STAND_ALONE_URL, 'contact-id=' + EncodingUtil.urlEncode(contactId, 'UTF-8') +
                             '&first-name=' + EncodingUtil.urlEncode(firstName, 'UTF-8') + '&last-name=' +
                             EncodingUtil.urlEncode(lastName, 'UTF-8') + '&account-number=' + EncodingUtil.urlEncode(accountNumber, 'UTF-8') +
                             '&routing-number=' + EncodingUtil.urlEncode(routingNumber, 'UTF-8') + '&content-service-id=' +
                             EncodingUtil.urlEncode(contentServiceId, 'UTF-8') + '&application-id=' + EncodingUtil.urlEncode(applicationId, 'UTF-8') +
                             '&user-id=' + EncodingUtil.urlEncode(userId, 'UTF-8') );
        }

        return standAloneLink;
    }

    /**
    * ApiGetBankInfotmation Call Proxy Api to get Decision Logic statements
    *  for a contact id and a application id
    * @param contactId [Contact id]
    * @param applicationId [Application id]
    * @return List<DL_AccountStatementSF>
    */
    public static List<DL_AccountStatementSF> ApiGetStatements(String contactId, String applicationId) {
        return ApiGetStatements(contactId, applicationId, null, null);
    }

    /**
    * ApiGetBankInfotmation Call Proxy Api to get Decision Logic statements
    *  for a contact id and a application id
    * @param contactId [Contact id]
    * @param applicationId [Application id]
    * @return List<DL_AccountStatementSF>
    */
    public static List<DL_AccountStatementSF> ApiGetStatements(String contactId, String applicationId, string state, Decimal annualIncome) {

        if (!ProxyApiUtil.ValidateBeforeCallApi())
            return null;

        String jsonResponse = null;

        if (Test.isRunningTest()) {
            if (contactId != '' && contactId != null && applicationId != '' && applicationId != null )
                jsonResponse =  '[{"requestCode":"XXXXXX","customerIdentifier":"00Q0B0000011111111","emailAddress":null,"institutionName":' +
                                '"U.S. \' Bank","accountName":"CHECKING","routingNumberEntered":null,"accountType":"CHECKING","accountNumberEntered"' +
                                ':"1111","accountNumberFound":"1111","accountNumberConfidence":"2","nameEntered":"Don Lon","nameFound":"DON J LON",' +
                                '"nameConfidence":"4","amountInput":"0","availableBalance":"127.26","averageBalance":"1050.7167796610181",' +
                                '"averageBalanceRecent":"83.926666666666662","isLoginValid":"true","isVerified":"true","asOfDate":"2017-01-15T00:20:47.813"' +
                                ',"activityStartDate":"2016-10-24T07:00:00","activityEndDate":"2016-12-21T08:00:00","totalCredits":"300","totalDebits":' +
                                '"-2680.95","currentBalance":"127.26","isActivityAvailable":"true","transactionAnalysisSummaries":[{"totalCount":"0",' +
                                '"typeCode":"py,dp","totalAmount":"0","recentCount":"0","typeName":"Payroll","recentAmount":"0"},{"totalCount":"0",' +
                                '"typeCode":"ld","totalAmount":"0","recentCount":"0","typeName":"Loan Debit","recentAmount":"0"}],"transactionSummaries"' +
                                ':[{"isRefresh":"false","status":"posted","amount":"100","description":"Mobile Banking Transfer Deposit 6492","category":' +
                                '"Transfers","typeCodes":"py,dp","transactionDate":"2018-12-21T08:00:00","runningBalance":"127.26"},{"isRefresh":"false",' +
                                '"status":"posted","amount":"60","description":"Mobile Banking Transfer Withdrawal 6492","category":"Transfers","typeCodes"' +
                                ':"py","transactionDate":"2017-12-20T08:00:00","runningBalance":"27.260000000000005"}],"processedStatus":"1","isStarted":"true"' +
                                ',"isCompleted":"true","notes":"<ul style=&quot;margin-top: 3px; margin-bottom: 0px;&quot;><li><strong>Account number</strong> : ' +
                                'a different account number was returned</li></ul>","status":"3","statusText":"Login, Verified","statusCodeColor":"#228822",' +
                                '"lastRefreshErrorMessage":null,"isError":"false","errorMessage":null,"isACHSupported":"false","chartsId":"14329312",' +
                                '"accountExpenses":[{"category":"ATM/Cash Withdrawals","amount":"1300","percentage":"48.490273970047937"},{"category":' +
                                '"Credit Card Payments","amount":"36.95","percentage":"1.3782427870717473"}]}]';
        } else {
            String parameters = 'contact-id=' + EncodingUtil.urlEncode(contactId, 'UTF-8') + '&application-id=' + EncodingUtil.urlEncode(applicationId, 'UTF-8');

            if (!String.IsEmpty(state) && annualIncome != null)
                parameters = parameters + '&state-code=' + EncodingUtil.urlEncode(state, 'UTF-8') + '&income=' + EncodingUtil.urlEncode(string.ValueOf(annualIncome), 'UTF-8');

            jsonResponse = ProxyApiUtil.ApiGetResult(GET_STATEMENTS, parameters);
        }

        if (jsonResponse == null || jsonResponse.contains('Process Result is null') || jsonResponse.contains('Error processing report'))
            return null;

        //jsonResponse = String.escapeSingleQuotes(jsonResponse);

        jsonResponse = ReplaceSingleQuotes(jsonResponse);

        List<DL_AccountStatementSF> dl_AccountStatementSFList;

        try {
            JSONParser parser = JSON.createParser(jsonResponse);
            dl_AccountStatementSFList = (List<DL_AccountStatementSF>) parser.readValueAs(List<DL_AccountStatementSF>.class);
        } catch (Exception e) {
            System.debug('Exception at ProxyApiBankStatements.ApiGetStatements(): ' + e);
            return null;
        }

        return dl_AccountStatementSFList;
    }

    /**
    * ApiGetSpecificTransactions Call Proxy Api to get Decision Logic specific transactions
    *  for a contact id and a application id
    * @param contactId [Contact id]
    * @param applicationId [Application id]
    * @param accountNumber [Account number]
    * @param typeCode [Type code]
    * @return List<DL_AccountStatementSF>
    */
    public static List<DL_AccountStatementSF> ApiGetSpecificTransactions(String contactId,
            String applicationId, String accountNumber, String typeCode) {
        if (!ProxyApiUtil.ValidateBeforeCallApi())
            return null;

        String jsonResponse = null;

        if (Test.isRunningTest()) {

            jsonResponse =  '[{"requestCode":"XXXXXX","customerIdentifier":"00Q0B0000011111111","emailAddress":null,"institutionName":"U.S. Bank",' +
                            '"accountName":"CHECKING","routingNumberEntered":null,"accountType":"CHECKING","accountNumberEntered":"0000","accountNumberFound":"1111",' +
                            '"accountNumberConfidence":"2","nameEntered":"Don Lon","nameFound":"DON J LON","nameConfidence":"4","amountInput":"0","availableBalance":' +
                            '"127.26","averageBalance":"1050.7167796610181","averageBalanceRecent":"83.926666666666662","isLoginValid":"true","isVerified":"true",' +
                            '"asOfDate":"2017-01-15T00:20:47.813","activityStartDate":"2016-10-24T07:00:00","activityEndDate":"2016-12-21T08:00:00","totalCredits":' +
                            '"300","totalDebits":"-2680.95","currentBalance":"127.26","isActivityAvailable":"true","transactionAnalysisSummaries":[{"totalCount":"0",' +
                            '"typeCode":"py","totalAmount":"0","recentCount":"0","typeName":"Payroll","recentAmount":"0"},{"totalCount":"0","typeCode":"ld","totalAmount"' +
                            ':"0","recentCount":"0","typeName":"Loan Debit","recentAmount":"0"}],"transactionSummaries":[{"isRefresh":"false","status":"posted","amount":' +
                            '"100","description":"Mobile Banking Transfer Deposit 6492","category":"Transfers","typeCodes":"dp","transactionDate":"2016-12-21T08:00:00",' +
                            '"runningBalance":"127.26"},{"isRefresh":"false","status":"posted","amount":"-60","description":"Mobile Banking Transfer Withdrawal 6492",' +
                            '"category":"Transfers","typeCodes":null,"transactionDate":"2016-12-20T08:00:00","runningBalance":"27.260000000000005"}],"processedStatus":' +
                            '"1","isStarted":"true","isCompleted":"true","notes":"<ul style=&quot;margin-top: 3px; margin-bottom: 0px;&quot;><li><strong>Account number' +
                            '</strong> : a different account number was returned</li></ul>","status":"3","statusText":"Login, Verified","statusCodeColor":"#228822",' +
                            '"lastRefreshErrorMessage":null,"isError":"false","errorMessage":null,"isACHSupported":"false","chartsId":"14329312","accountExpenses":' +
                            '[{"category":"ATM/Cash Withdrawals","amount":"1300","percentage":"48.490273970047937"},{"category":"Credit Card Payments","amount":' +
                            '"36.95","percentage":"1.3782427870717473"}]}]';

            if (contactId == '' || contactId == null)
                jsonResponse =  null;
            else if (contactId == '100000') {
                while (jsonResponse.length() <= 100000) {
                    jsonResponse += jsonResponse;
                }
            }
        } else
            jsonResponse = ProxyApiUtil.ApiGetResult(GET_SPECIFIC_TRANSACTIONS, 'contact-id=' + EncodingUtil.urlEncode(contactId, 'UTF-8') + '&application-id=' +
                           EncodingUtil.urlEncode(applicationId, 'UTF-8') + '&account-number=' + EncodingUtil.urlEncode(accountNumber, 'UTF-8') +
                           '&type-code=' + EncodingUtil.urlEncode(typeCode, 'UTF-8'));

        if (jsonResponse == null || jsonResponse.contains('Process Result is null') || jsonResponse.contains('Error processing report'))
            return null;

        //jsonResponse = String.escapeSingleQuotes(jsonResponse);

        jsonResponse = ReplaceSingleQuotes(jsonResponse);

        List<DL_AccountStatementSF> dl_AccountStatementSFList;
        try {
            JSONParser parser = JSON.createParser(jsonResponse);
            dl_AccountStatementSFList = (List<DL_AccountStatementSF>) parser.readValueAs(List<DL_AccountStatementSF>.class);
        } catch (Exception e) {
            System.debug('Exception at ProxyApiBankStatements.ApiGetSpecificTransactions(): ' + e);
            return null;
        }

        return dl_AccountStatementSFList;
    }

    /**
    * Replace by segments of 100000.
    * @param String [beforeReplace]
    * @return String
    */
    private static String ReplaceSingleQuotes (String beforeReplace) {

        if (beforeReplace.length() <= 100000)
            return beforeReplace.replaceAll('\'', '´');

        Integer rounds = beforeReplace.length() / 100000, i = 0, startSubString = 0, endSubString = 100000;
        String afterReplace = '', newSubString = '';

        while (i < rounds) {
            if (i == (rounds - 1))
                newSubString = beforeReplace.substring(startSubString, beforeReplace.length());
            else
                newSubString = beforeReplace.substring(startSubString, endSubString);

            afterReplace += newSubString.replaceAll('\'', '´');

            startSubString = startSubString + 100000;

            endSubString = endSubString + 100000;

            i++;

            newSubString = '';
        }

        return afterReplace;
    }

    /**
    * ApiGetBankInformationByRoutingNumber Call Proxy Api to get Decision Logic Bank Information By RoutingNumber
    * @param contactId
    * @param applicationId
    * @param userId
    * @param routingNumber
    * @return jsonResponse
    */
    public static String ApiGetBankInformationByRoutingNumber(String contactId, String applicationId, String userId, String routingNumber) {
        if (!ProxyApiUtil.ValidateBeforeCallApi())
            return null;

        String jsonResponse = '';

        if (Test.isRunningTest()) {
            if (contactId != '' && contactId != null && applicationId != '' && applicationId != null )
                jsonResponse =  '[{"bankName": "Chase - Bank","logoURL": "https://www.decisionlogic.com/ImageHandler.ashx?contenServiceId=20",' +
                                '"homeURL": "https://www.chase.com","contactPhone": null,"contactURL": "http://www.chase.com/"}';
            else
                jsonResponse = null;
        } else
            jsonResponse = ProxyApiUtil.ApiGetResult(GET_BANK_INFORMATION_BY_ROUTING_NUMBER, 'contact-id=' + EncodingUtil.urlEncode(contactId, 'UTF-8') +
                           '&application-id=' + EncodingUtil.urlEncode(applicationId, 'UTF-8') + '&user-id=' + EncodingUtil.urlEncode(userId, 'UTF-8') +
                           '&routing-number=' + EncodingUtil.urlEncode(routingNumber, 'UTF-8'));

        if (jsonResponse == null || jsonResponse.contains('Process Result is null') || jsonResponse.contains('Error processing report'))
            return null;

        return jsonResponse;
    }

    /**
    * ApiGetBankInformationByBankName Call Proxy Api to get Decision Logic Bank Information By Bank name
    * @param contactId
    * @param applicationId
    * @param userId
    * @param bankName
    * @return jsonResponse
    */
    public static String ApiGetBankInformationByBankName(String contactId, String applicationId, String userId, String bankName) {
        if (!ProxyApiUtil.ValidateBeforeCallApi())
            return null;

        String jsonResponse = '';

        if (Test.isRunningTest()) {
            if (contactId != '' && contactId != null && applicationId != '' && applicationId != null )
                jsonResponse =  '[{"contentServiceId": "2116","contentServiceDisplayName": "1st National Bank of Scotia - Bank",' +
                                '"homeURL": "http://www.firstscotia.com","reliability": "3"}]';
            else
                jsonResponse = null;
        } else
            jsonResponse = ProxyApiUtil.ApiGetResult(GET_BANK_INFORMATION_BY_BANK_NAME, 'contact-id=' + EncodingUtil.urlEncode(contactId, 'UTF-8') +
                           '&application-id=' + EncodingUtil.urlEncode(applicationId, 'UTF-8') + '&user-id=' +
                           EncodingUtil.urlEncode(userId, 'UTF-8') + '&bank-name=' + EncodingUtil.urlEncode(bankName, 'UTF-8'));

        if (jsonResponse == null || jsonResponse.contains('Process Result is null') || jsonResponse.contains('Error processing report'))
            return null;

        return jsonResponse;
    }
}