@isTest
public class EmpInfoHelperTest{
    
    @testSetup
    static void testData(){
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        
       
        EmpInfo_Settings__c settings = new EmpInfo_Settings__c ();
        settings.Verification_Type__c= 'VOI';
        settings.Permissible_Purpose__c= 'Application for Credit';
        settings.Fill_Strategy__c= '';
        settings.Dev_Application_Token__c = 'abc';
        settings.Application_Token__c = 'abc';
        insert settings;
        
        Mule_Settings__c st = new Mule_Settings__c();
        st.Dev_Authorization__c = '123';
        st.Authorization__c = '123';
        st.Dev_Base_URL__c = 'https://google.com';
        st.Base_URL__c = 'https://google.com';
        insert st;
        
        Opportunity sampleApp = TestHelper.createApplication();
        
        }
    @isTest
     public static void callEmpInfoTest(){
     EmpInfoHelper obj=new EmpInfoHelper();
     string pPhone='986754321',email='palvi@trantorinc.com';
     List<Contact> lstContact = new List<Contact>();
      Opportunity app = [select id, Lending_Product__c, Lending_Product__r.Name, Status__c,Contact__c,Contact__r.Name from Opportunity limit 1];
      List<Id> cntId = new List<Id>();
      for(Contact cnt: [select Id,Email,LeadSource,FirstName,LastName,SSN__c,MailingStreet,MailingCity,MailingState,Postal_code__c,Employer_Name__c,
                                EmpInfo_Verification_Called__c,Work_Email_Address__c,Office_Phone_Number__c from Contact where id=: app.Contact__c]){
         cntId.add(cnt.id);
         cnt.EmpInfo_Verification_Called__c=false;
         cnt.Employer_Name__c= 'Trantor';
         cnt.Work_Email_Address__c= 'palvi@trantorinc.com';
         cnt.Office_Phone_Number__c  ='65432178';
         lstContact.add(cnt);
      }
      update lstContact;
      EmpInfoHelper.callEmpInfo(cntId);
     }
    
    @isTest
     public static void getEmpInfoResultTest1(){
         EmpInfoHelper obj=new EmpInfoHelper();
         try {
            EmpInfoHelper.getEmpInfoResult(null,null,null);
        } catch(DMLException e) {
            system.assertEquals(e.getMessage(),e.getMessage());
        }
     //EmpInfoHelper obj=new EmpInfoHelper();
     string pPhone='986754321',email='palvi@trantorinc.com';
     List<Contact> lstContact = new List<Contact>();
      Opportunity app = [select id, Lending_Product__c, Lending_Product__r.Name, Status__c,Contact__c,Contact__r.Name from Opportunity limit 1];
      List<Id> cntId = new List<Id>();
      for(Contact cnt: [select Id,Email,LeadSource,FirstName,LastName,SSN__c,MailingStreet,MailingCity,MailingState,Postal_code__c,Employer_Name__c,
                                EmpInfo_Verification_Called__c,Work_Email_Address__c,Office_Phone_Number__c from Contact where id=: app.Contact__c]){
         cntId.add(cnt.id);
         cnt.EmpInfo_Verification_Called__c=false;
         cnt.Employer_Name__c= 'Trantor';
         cnt.Work_Email_Address__c= 'palvi@trantorinc.com';
         cnt.Office_Phone_Number__c  ='65432178';
         lstContact.add(cnt);
      }
      update lstContact;
      EmpInfoHelper.getEmpInfoResult(cntId,pPhone,email);
     }
     @isTest
    public static void getEmpInfoResultTest2(){
     EmpInfoHelper obj=new EmpInfoHelper();
     string pPhone='986754321',email='palvi@trantorinc.com';
     List<Contact> lstContact = new List<Contact>();
      Opportunity app = [select id, Lending_Product__c, Lending_Product__r.Name, Status__c,Contact__c,Contact__r.Name from Opportunity limit 1];
      List<Id> cntId = new List<Id>();
      for(Contact cnt: [select Id,Email,LeadSource,FirstName,LastName,SSN__c,MailingStreet,MailingCity,MailingState,Postal_code__c,Employer_Name__c,
                                EmpInfo_Verification_Called__c,Work_Email_Address__c,Office_Phone_Number__c from Contact where id=: app.Contact__c]){
         cntId.add(cnt.id);
         cnt.EmpInfo_Verification_Called__c=true;
         lstContact.add(cnt);
      }
      update lstContact;
      EmpInfoHelper.getEmpInfoResult(cntId,pPhone,email);
     }
     @isTest
    public static void cancelVerificationOrderTest(){
     EmpInfoHelper obj=new EmpInfoHelper();
     string pPhone='986754321',email='palvi@trantorinc.com';
     List<Contact> lstContact = new List<Contact>();
      Opportunity app = [select id, Lending_Product__c, Lending_Product__r.Name, Status__c,Contact__c,Contact__r.Name from Opportunity limit 1];
       EmpInfo_Details__c empinfo = new EmpInfo_Details__c ();
        
        empinfo.EmpInfo_ID__c ='kyuhki';
        Contact cnt= [select Id,Email,LeadSource,FirstName,LastName,SSN__c,MailingStreet,MailingCity,MailingState,Postal_code__c,Employer_Name__c,
                                EmpInfo_Verification_Called__c,Work_Email_Address__c,Office_Phone_Number__c from Contact where id=: app.Contact__c];
         empinfo.contact__c = cnt.id;
         insert empinfo;
      EmpInfoHelper.cancelVerificationOrder(cnt.Id);
     }
     @isTest
    public static void queryVerificationOrderTest(){
     
     string pPhone='986754321',email='palvi@trantorinc.com';
     List<Contact> lstContact = new List<Contact>();
      Opportunity app = [select id, Lending_Product__c, Lending_Product__r.Name, Status__c,Contact__c,Contact__r.Name from Opportunity limit 1];
      List<Id> cntId = new List<Id>();
      for(Contact cnt: [select Id,Email,LeadSource,FirstName,LastName,SSN__c,MailingStreet,MailingCity,MailingState,Postal_code__c,Employer_Name__c,
                                EmpInfo_Verification_Called__c,Work_Email_Address__c,Office_Phone_Number__c from Contact where id=: app.Contact__c]){
         cntId.add(cnt.id);
         
      }
      EmpInfoQueryVerificationResponseParams obj=new EmpInfoQueryVerificationResponseParams();
      obj.id='fdeswf';
      obj.verification_type='VOI';
      obj.permissible_purpose='Application for Credit';
      obj.source_id='cdef';
      obj.order_id='cdsfc';
      obj.consent_date='2018-09-09';
      obj.request_fullfillment_time=31312;
      obj.fill_strategy='FILLED';
      obj.message='ABC';
      obj.request_status='New';
      obj.request_status_description='ABC';      
      obj.errorCode='19';
      obj.created=3213;
      obj.employer=mapInputparams();
      mapInputparams2();
      EmpInfoHelper.queryVerificationOrder(cntId);
     }
     private static EmpInfoQueryVerificationResponseParams.employerDetails mapInputparams(){
      EmpInfoQueryVerificationResponseParams.employerDetails obj= new EmpInfoQueryVerificationResponseParams.employerDetails();
       obj.name='ASD';
      obj.email='ASD!gmail.com';
       obj.phone='ASD';
      obj.fax='ASD@gmail.com';
      obj.website='www.dewd.com';
      obj.address=mapInputparams5();
      return obj;
     }
     private static void mapInputparams2(){
      EmpInfoQueryVerificationResponseParams.employeeDetails  obj= new EmpInfoQueryVerificationResponseParams.employeeDetails();
       obj.first_name='ASD';
      obj.last_name='ASD!gmail.com';
       obj.middle_initials='ASD';
      obj.limited_ssn='ASD@gmail.com';
      obj.address=mapInputparams5();
      mapInputParams3();
     }
     private static void mapInputparams3(){
      EmpInfoQueryVerificationResponseParams.employmentDetails obj= new EmpInfoQueryVerificationResponseParams.employmentDetails();
       obj.department='ASD';
      obj.job_title='ASD!gmail.com';
       obj.work_location='ASD';
      obj.employment_status='ASD@gmail.com';
      obj.hire_date='ASD';
      obj.status_start_date='ASD@gmail.com';
      obj.income = mapInputparams4();
      //mapInputParams4();
     }
     private static EmpInfoQueryVerificationResponseParams.incomeDetails mapInputparams4(){
      EmpInfoQueryVerificationResponseParams.incomeDetails obj= new EmpInfoQueryVerificationResponseParams.incomeDetails();
       obj.rate_of_pay=123;
      obj.pay_frequency='112';
       obj.pay_frequency_description='ASD';
      obj.avg_weekly_hours_worked=23;
      obj.last_payraise_ammount=1234;
      obj.next_payraise_ammount=1234;      
       obj.most_recent_pay_date='112';
       obj.earnings = mapInputparams6();
      return obj;
     }
     private static EmpInfoQueryVerificationResponseParams.adressDetails mapInputparams5(){
      EmpInfoQueryVerificationResponseParams.adressDetails obj= new EmpInfoQueryVerificationResponseParams.adressDetails();
       obj.street1='ABC';
       obj.street2='112';
       obj.city='ASD';
       obj.state='23';
       obj.zip='1234';
       obj.county='USA';      
       obj.country='ABC';
       return obj;
       //mapInputParams5();
     }
     private static List<EmpInfoQueryVerificationResponseParams.earningsDetails> mapInputparams6(){
      List<EmpInfoQueryVerificationResponseParams.earningsDetails> listobj= new List<EmpInfoQueryVerificationResponseParams.earningsDetails>();
       EmpInfoQueryVerificationResponseParams.earningsDetails obj = new EmpInfoQueryVerificationResponseParams.earningsDetails();
       obj.year=2018;
       obj.base=2;
       obj.overtime=123;
       obj.bonus=23;
       obj.commission=1234;
       obj.stocks=123;      
       obj.other=123;
       obj.total=12334;
       listobj.add(obj);
       return listobj;
       //mapInputParams5();
     }
     @isTest
     public static void updateCallbackRequestTest(){
     EmpInfoHelper obj=new EmpInfoHelper();
     string pPhone='986754321',email='palvi@trantorinc.com';
     List<Contact> lstContact = new List<Contact>();
      Opportunity app = [select id,Contact__c from Opportunity limit 1];
      Contact cnt= [select Id from Contact where id=: app.Contact__c];
      string inpiutstring =  ' { "event_id": "1f37707b-1353c8", "event_type": "STATUS_UPDATE", "event_time": 1348337752, "event_data": { "id": "c21ff1a0479cd", "verification_type": "Employment", "permissible_purpose": "Application for credit", "source_id": "' + cnt.Id +'", "request_status": "Filled", "request_status_description": "The description provided by employer.", "request_fullfillment_time": 125456566 } }';
      EmpInfoCallBackParameter wrapper = new EmpInfoCallBackParameter();
      wrapper.event_id= '1f37707b-1353c8';
      wrapper.event_type='VOI';
      wrapper.event_time= 1348337752;
      EmpInfoHelper.updateCallbackRequest(inpiutstring);
     }
}