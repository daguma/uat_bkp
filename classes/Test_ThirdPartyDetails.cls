@isTest
public class Test_ThirdPartyDetails
{
    static testmethod void testIDology(){
        MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();             
        maskSettings.MaskAllData__c = false;             
        insert maskSettings;
        LibraryTest.createPayfoneSettings();   
        sObject Config;
        ThirdPartyDetails.fetchConfigurations('email','AllowProceed');
        // ThirdPartyDetails.checkConfigurations(Config);
        
        map<string,string> m = new map<string,string>();
        m.put('ZipCode','77001');
        m.put('SSN4','1234');
        m.put('FirstName','Test');
        m.put('LastName','Demo');
        m.put('DOB','12/10/1994');
        m.put('Address','220');
        m.put('City','TestCity');
        m.put('State','TestState');
        m.put('Phone','4699894');
        m.put('Email','r@gmail.com');
        string s = json.serializepretty(m);
        
        Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'Resolve360',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='RS1.0',Product_Name__c='Resolve360',User_Name__c='LendingPoint/T1');
        insert ResolveSett;
        
        ThirdPartyDetails.Resolve360(s);
        
        //Dummy Contact
        contact cntct= new contact();
        cntct.Lastname='Test';
        cntct.Annual_Income__c =10000;
        insert cntct;
        
        //Dummy Application
        Opportunity aplcn= new Opportunity();
        aplcn.Contact__c= cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000;
        aplcn.Name = cntct.LastName;
        aplcn.Type = 'New';

        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification';
        insert aplcn;
        
        //Dummy offers
        Offer__c off= new Offer__c();
        off.Opportunity__c= aplcn.id;
        off.Fee__c=500;
        off.APR__c =0.0050;
        off.Payment_Amount__c = 1200;
        off.Loan_Amount__c = 13000;
        off.Loan_Amount__c = 4655;         
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Term__c = 1234;         
        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;
        
        
        Map<string,string> ResponseMap = new Map<string,string>();
        ResponseMap.put('offerId',off.id);
        ResponseMap.put('questionResponse','TestQResp');
        ResponseMap.put('questionRequestInTime','TestQReqInTime');
        ResponseMap.put('questionResponseOutTime','TestQRespOutTime');
        ResponseMap.put('answerResponse','testanswerResponse');
        ResponseMap.put('answerRequestInTime','TestQReqInTime');
        ResponseMap.put('answerResponseOutTime','TestARespOutTime');
        ResponseMap.put('questionRequest','QuesReq');
        string req = JSON.SERIALIZEPRETTY(ResponseMap);
        
        ThirdPartyDetails.SaveIDologyRequest(req);
        
        Opportunity app=TestHelper.createApplication();
        
        ResponseMap = new Map<string,string>();  
        ResponseMap.put('questionResponse','<error></error>');   
        ResponseMap.put('appId',app.id);
        ResponseMap.put('questionRequestInTime','TestQReqInTime');
        ResponseMap.put('questionResponseOutTime','TestQRespOutTime');
        ResponseMap.put('answerResponse','testanswerResponse');
        ResponseMap.put('answerRequestInTime','TestQReqInTime');
        ResponseMap.put('answerResponseOutTime','TestARespOutTime');
        ResponseMap.put('questionRequest','QuesReq');
        req = JSON.SERIALIZEPRETTY(ResponseMap);
        
        ThirdPartyDetails.SaveIDologyRequest(req);
    }
    
    static testmethod void testFetchConfigurations(){   
        try {
            sObject Config;
            ThirdPartyDetails.fetchConfigurations('income','AllowAutoAssign');       
            ThirdPartyDetails.fetchConfigurations('employer','AllowAutoAssign');       
            ThirdPartyDetails.fetchConfigurations('factorTrust','AllowAutoAssign');     
            ThirdPartyDetails.fetchConfigurations(null,null);    
        } catch (Exception e) {
            System.assert(false, e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    
    static testmethod void testProcessDetails1(){
        try {
            Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'NetworkG_IDA',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='RS1.0',Product_Name__c='Resolve360',User_Name__c='LendingPoint/T1');
            insert ResolveSett;

            Opportunity app=TestHelper.createApplication();
            app.Status__c='Funded' ;
            update app;
            ThirdPartyDetails.processDetails(app.Contact__c);   
        } catch (Exception e) {
            System.assert(false, e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    static testmethod void testProcessDetails2(){
        try {
            Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'NetworkG_IDA',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='RS1.0',Product_Name__c='Resolve360',User_Name__c='LendingPoint/T1');
            insert ResolveSett;

            Contact theContact = TestHelper.CreateContact();
            ThirdPartyDetails.processDetails(theContact.id);   
        } catch (Exception e) {
            System.assert(false, e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    static testmethod void testResolve360(){
        try {
            Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'NetworkG_IDA',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='RS1.0',Product_Name__c='Resolve360',User_Name__c='LendingPoint/T1');
            insert ResolveSett;
            
            MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();
            maskSettings.MaskAllData__c = false;
            insert maskSettings;
            LibraryTest.createPayfoneSettings();   
            sObject Config;
            ThirdPartyDetails.fetchConfigurations('email','AllowProceed');
            // ThirdPartyDetails.checkConfigurations(Config);
            Opportunity app=TestHelper.createApplication();
            map<string,string> m = new map<string,string>();
            m.put('ZipCode','77001');
            m.put('SSN4','1234');
            m.put('FirstName','Test');
            m.put('LastName','Demo');
            m.put('DOB','1994-12-10');
            m.put('Address','220');
            m.put('City','TestCity');
            m.put('State','TestState');
            m.put('Phone','4699894');
            m.put('Email','r@gmail.com');
            string s = json.serializepretty(m);
            
            ResolveSett = new Resolve_360_Settings__c(Name = 'Resolve360',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='RS1.0',Product_Name__c='Resolve360',User_Name__c='LendingPoint/T1');
            insert ResolveSett;
            
            
            String res = ThirdPartyDetails.Resolve360(s);
            System.assert(res.contains('SSN is not resolved'), '"SSN is not resolved" is present in result - Actual: ' + res);
        } catch (Exception e) {
            System.assert(false, e.getMessage() + '\n' + e.getStackTraceString());
        }
        
    }
    
    static testmethod void testPayfoneMethods() {
        try {
            // Contact theContact = TestHelper.CreateContact();
            MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();
            maskSettings.MaskAllData__c = false;
            insert maskSettings;
            Opportunity app = TestHelper.createApplication();
            
            //Dummy offers
            Offer__c off= new Offer__c();
            off.Opportunity__c= app.id;
            off.Fee__c=500;
            off.APR__c =0.0050;
            off.Payment_Amount__c = 1200;
            off.Loan_Amount__c = 13000;
            off.Loan_Amount__c = 4655;         
            off.IsSelected__c = true;
            off.Selected_date__c = Date.today();
            off.Term__c = 1234;         
            off.Repayment_Frequency__c = 'Monthly';
            off.Fee_Percent__c = 10;
            insert off;
            
            LibraryTest.createPayfoneSettings();
            
            Map<String,String> reqMap = new Map<String,String>();
            reqMap.put('offerId',off.id);

            ThirdPartyDetails.getKYCdecisioning(JSON.serialize(reqMap)); 
            String status = ThirdPartyDetails.getPayfoneDetailsFromSFDC(JSON.serialize(reqMap));
            
            System.assertEquals(status,'2');
            
            String matchCRMRequest ='{"RequestId":"29eee7aa-47be-01c6-ebee-d0d38dcd1d6c","ApiClientId":"Lend7h9r52xB24y7r4tA","SubClientId":"","MobileNumber":"2127290854","AccountChallengeResponse":"30318","ConsentStatus":"notCollected","ConsentCollectedTimestamp":"","ConsentTransactionId":"","ConsentDescription":"","MatchCRMName":{"FirstName":"JOHN","MiddleName":"","LastName":"SMITH"},"MatchCRMAddress":{"Address":"550 S WINCHESTER BLVD   580","City":"ATLANTA","Region":"GA","Country": "","PostalCode":"30318"},"MatchCRMEmail" :{"EmailAddress" : "sample10@sample.com"}}';
            String matchCRMResponse = '{"RequestId":"29eee7aa-47be-01c6-ebee-d0d38dcd1d6c","Status":0,"Description":"Success.","Response":{"MatchScore":{"Qualifiers":["AB"],"NameScore":{"FirstNameScore":100,"LastNameScore":100,"IsFirstInitialMatch":true,"IsLastInitialMatch":true},"AddressScore":{"StreetNumberScore":16,"StreetAndSubPremiseScore":37,"CityScore":100,"StateScore":100,"ZipScore":100},"EmailScore":{"EmailAddressScore":50,"IsValidEmailAddress":true}}}}';
            String getIntelRequest = '{"RequestId":"aac70bee-8034-450f-9bc3-7658dc4f1b25","ApiClientId":"Lend7h9r52xB24y7r4tA","PhoneNumber":"2127290854","ConsentCollectedTimestamp":"2016-11-01T15:08:18.0459","ConsentTransactionId":"8648FCF643CAD672AE0E3191652821C7","ConsentDescription":"Consent collected from Lending point web portal - v1.4"}';
            String getIntelResponse = '{"RequestId":"aac70bee-8034-450f-9bc3-7658dc4f1b25","Status":0,"Description":"Success.","Response":{"PhoneNumber":"12127290854","PayfoneSignature":"E8A02F3C4VKRKKX0A3233C904F18C79FF0MEKNCLE389P9C714964BAA58A064761D0DC200D311C680F6G3985D53321DC39F07099560C07FFE81529DE4","TransactionId":"12064550228","FraudStatus":"No known fraud","Path":4,"StatusIndex":"ec","PhoneNumberFlags":{"Tenure":5,"Velocity":5,"Status":"Ported","LineType":"Mobile"},"DeviceFlags":{"Tenure":4,"Velocity":4},"OperatorFlags":{"Tenure":4,"Velocity":4,"Name":"T-Mobile USA"},"SimFlags":{"Tenure":4,"Velocity":4},"PayfoneSignatureFlags":{"Tenure":5}}}';
            String fortifiedEchoRequest = '';
            String fortifiedEchoResponse = '';
            
            //ThirdPartyDetails.savePayfoneDataToSFDC(off.id,'true','9815555555',fortifiedEchoRequest,fortifiedEchoResponse,matchCRMRequest,matchCRMResponse,getIntelRequest,getIntelResponse);
            
            status = ThirdPartyDetails.getPayfoneDetailsFromSFDC(JSON.serialize(reqMap));
            
            matchCRMRequest = '';
            matchCRMResponse = '';
            getIntelRequest = '{"RequestId":"aac70bee-8034-450f-9bc3-7658dc4f1b25","ApiClientId":"Lend7h9r52xB24y7r4tA","PhoneNumber":"2127290854","ConsentCollectedTimestamp":"2016-11-01T15:08:18.0459","ConsentTransactionId":"8648FCF643CAD672AE0E3191652821C7","ConsentDescription":"Consent collected from Lending point web portal - v1.4"}';
            getIntelResponse = '{"RequestId":"aac70bee-8034-450f-9bc3-7658dc4f1b25","Status":0,"Description":"Success.","Response":{"PhoneNumber":"12127290854","PayfoneSignature":"E8A02F3C4VKRKKX0A3233C904F18C79FF0MEKNCLE389P9C714964BAA58A064761D0DC200D311C680F6G3985D53321DC39F07099560C07FFE81529DE4","TransactionId":"12064550228","FraudStatus":"No known fraud","Path":4,"StatusIndex":"ec","PhoneNumberFlags":{"Tenure":5,"Velocity":5,"Status":"Ported","LineType":"Mobile"},"DeviceFlags":{"Tenure":4,"Velocity":4},"OperatorFlags":{"Tenure":4,"Velocity":4,"Name":"T-Mobile USA"},"SimFlags":{"Tenure":4,"Velocity":4},"PayfoneSignatureFlags":{"Tenure":5}}}';
            fortifiedEchoRequest = '';
            fortifiedEchoResponse = '';
            
            //ThirdPartyDetails.savePayfoneDataToSFDC(off.id,'true','9815555555',fortifiedEchoRequest,fortifiedEchoResponse,matchCRMRequest,matchCRMResponse,getIntelRequest,getIntelResponse);
            
            String req = '{'+
                '"offerId":"'+ off.id +'",'+
                '"phoneNumber":"0987654321",'+
                '"verifyRequest":"test",'+
                '"statusCode":"200"'+
            '}';
            ThirdPartyDetails.savePayfoneDataToSFDC(req);
            status = ThirdPartyDetails.getPayfoneDetailsFromSFDC(JSON.serialize(reqMap));
            //System.assertEquals(status,'3');
            
        } catch (Exception e) {
            System.assert(false, e.getMessage() + '\n' + e.getStackTraceString());
        }
        
    }
    
    static testmethod void testAuthenticateSSN() {
        try {
            Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'NetworkG_IDA',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='RS1.0',Product_Name__c='Resolve360',User_Name__c='LendingPoint/T1');
            insert ResolveSett;
            
            MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();             
            maskSettings.MaskAllData__c = false;             
            insert maskSettings;
            
            contact cntct= new contact();
            cntct.Lastname='Test';
            cntct.Annual_Income__c =10000;
            cntct.ints__Social_Security_Number__c='1234567333';
            cntct.SSNAuthenticationCount__c=1;
            insert cntct;
            
            Opportunity aplcn= new Opportunity();
            aplcn.Contact__c= cntct.id;
            aplcn.Maximum_Loan_Amount__c = 10000;
            aplcn.Maximum_Payment_Amount__c = 12000;
            aplcn.LeadSource__c='test';
            aplcn.Lead_Sub_Source__c='dummy';
            aplcn.Type = 'New';
            aplcn.Name = cntct.LastName;
        	aplcn.CloseDate = system.today();
        	aplcn.StageName = 'Qualification';
            insert aplcn;
            
            map<string,string> m = new map<string,string>();
            m.put('appId',aplcn.id);
            m.put('ssn4','1234');
            m.put('phone','4699894');
            string s = json.serializepretty(m);
            
            map<string,string> m1 = new map<string,string>();
            m1.put('appId',aplcn.id);
            string s1 = json.serializepretty(m1);
            
            
            ThirdPartyDetails.authenticateSSN(s);
            ThirdPartyDetails.getLeadSrcInfo(s1 );
            ThirdPartyDetails.resetSSNAuthenticationCount(cntct.id);
        } catch (Exception e) {
            System.assert(false, e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    static testmethod void testNetworkG(){
        try {
            if ([select Id from MaskSensitiveDataSettings__c].size() == 0 ) {
                MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();
                maskSettings.MaskAllData__c = false;
                insert maskSettings;
            }

            Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'NetworkG_IDA',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='RS1.0',Product_Name__c='Resolve360',User_Name__c='LendingPoint/T1');
            insert ResolveSett;
            
            Contact c = new Contact();
            c.Firstname = 'David';
            c.Lastname = 'Testcase';
            c.Email = 'test@gmail2.com';
            c.BirthDate = Date.Today().addYears(-21);
            c.Point_Code__c = '1234AB';
            c.ints__Social_security_Number__c = '999999999';
            c.SSN__c = '999999999';
            c.Time_at_current_Address__c = Date.Today().addYears(-3);
            c.Phone = '1251231233';
            c.Mailingstreet = 'Test street';
            c.MailingCity = 'City';
            c.MailingState = 'GA';
            c.MailingPostalCode = '30144';
            c.MailingCountry = 'US';
            c.Annual_Income__c = 100000;
            c.Loan_Amount__c = 4000;
            c.Employment_Start_date__c = Date.Today().addYears(-3);
            c.Use_of_Funds__c = 'Wedding';
            insert c;
            Contact con = c;
            ThirdPartyDetails.networkG_IDAnalytics(con.MailingPostalCode, con.SSN__c, con.FirstName, con.LastName, String.valueOf(con.Birthdate), con.MailingStreet, con.MailingCity, con.MailingState,con.Phone,con.Email,con.Lead__c,con.Id);

        } catch (Exception e) {
            System.assert(false, e.getMessage() + '\n' + e.getStackTraceString());
        }
    }
    
    static testmethod void testApplicationDetails() {
        try {
            Resolve_360_Settings__c ResolveSett = new Resolve_360_Settings__c(Name = 'NetworkG_IDA',EndPoint__c='https://idatest1.idanalytics.com/webservice/services/IDSPService',Password__c='vSApx*!4',Product_ID__c='RS1.0',Product_Name__c='Resolve360',User_Name__c='LendingPoint/T1');
            insert ResolveSett;
            MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();             
            maskSettings.MaskAllData__c = false;             
            insert maskSettings;
            
            contact cntct= new contact();
            cntct.Lastname='Test';
            cntct.Annual_Income__c =10000;
            cntct.ints__Social_Security_Number__c='1234567333';
            cntct.SSNAuthenticationCount__c=1;
            insert cntct;
            
            Opportunity aplcn= new Opportunity();
            aplcn.Contact__c= cntct.id;
            aplcn.Maximum_Loan_Amount__c = 10000;
            aplcn.Maximum_Payment_Amount__c = 12000;
            aplcn.LeadSource='test';
            aplcn.Lead_Sub_Source__c='dummy';
            aplcn.Type = 'New';
            aplcn.Name = cntct.LastName;
        	aplcn.CloseDate = system.today();
        	aplcn.StageName = 'Qualification';
            insert aplcn;
            
            map<string,string> m1 = new map<string,string>();
            m1.put('leadId',null);
            string s1 = json.serializepretty(m1);
            
            
            ThirdPartyDetails.getApplicantInfo(s1);
            
            //Dummy offers
            Offer__c off= new Offer__c();
            off.Opportunity__c= aplcn.id;
            off.Fee__c=500;
            off.APR__c =0.0050;
            off.Payment_Amount__c = 1200;
            off.Loan_Amount__c = 13000;
            off.Loan_Amount__c = 4655;         
            off.IsSelected__c = true;
            off.Selected_date__c = Date.today();
            off.Term__c = 1234;         
            off.Repayment_Frequency__c = 'Monthly';
            off.Fee_Percent__c = 10;
            insert off;
            Map<string, string> payfoneMap= new Map<string, string>();
            payfoneMap.put('assuranceRequest','test');
            payfoneMap.put('offerId',off.id);
            payfoneMap.put('assuranceResponse','test');
            payfoneMap.put('fortifiedResponse','test');
            payfoneMap.put('fortifiedRequest','test');
            payfoneMap.put('result','test');
            
            string reqString=JSON.serializePretty(payfoneMap);
            ThirdPartyDetails.SavePayfoneRequest(reqString);
            
            Map<string, string> loggingMap= new Map<string, string>();
            loggingMap.put('offerId',off.id);
            loggingMap.put('webServiceName','test');
            loggingMap.put('request','test');
            loggingMap.put('response','test');
            string reqlogString=JSON.serializePretty(loggingMap);
            ThirdPartyDetails.SaveLoggingFromWeb(reqlogString);
            
        } catch (Exception e) {
            System.assert(false, e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

}