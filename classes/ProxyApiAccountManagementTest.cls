@isTest public class ProxyApiAccountManagementTest {
    
    @testSetup static void inserts_custom_setting() {
        Proxy_API__c proxySettings = new Proxy_API__c();
        proxySettings.Proxy_API_ClientId__c = '10215415630';
        proxySettings.Proxy_API_ClientSecret__c = 'XBSgO86b00557n7HRWMxzXQ4Wmd307C1';
        proxySettings.Proxy_API_DefaultURL__c = 'https://iav.lendingpoint.com/proxy-api-pre-brms/rest';
        proxySettings.Proxy_API_Password__c = 'Password123!';
        proxySettings.Proxy_API_TokenExpiresTime__c =  Datetime.now().addDays(2).getTime();
        proxySettings.Proxy_API_TokenURL__c = 'https://iav.lendingpoint.com/proxy-api-pre-brms/oauth/token';
        proxySettings.Proxy_API_Token__c = '';
        proxySettings.DEV_Default_URL__c = 'http://54.173.150.238:9080/proxy-api-pre-brms/rest';
        proxySettings.Proxy_API_Username__c = 'salesforce@lendingpoint.com';
        proxySettings.DEV_Token_URL__c = 'http://54.173.150.238:9080/proxy-api-pre-brms/oauth/token';
        proxySettings.DEV_Default_URL_New__c = 'http://54.173.150.238:9080/proxy-api-pre-brms/rest';
        proxySettings.DEV_Token_URL_New__c = 'http://54.173.150.238:9080/proxy-api-pre-brms/oauth/token';
        proxySettings.Default_URL_New__c = 'https://iav.lendingpoint.com/proxy-api-post-brms/rest';
        proxySettings.Token_Expires_Time_New__c =  Datetime.now().addDays(2).getTime();
        proxySettings.Token_New__c = '';
        proxySettings.Proxy_API_Cookie__c = 'AWSELB=FF2DF9DB0EA8557E01EBB955EB34220EBCA80CA91E02EE9419FD0BC1428A23FCDC50B8CC4FF2F2A2CCA286F3B87A6CC95CCBFA1927F89BB0FEB063F969B149C0395D3DE204;PATH=/;MAX-AGE=1800;__cfduid=dc04917e36a62f7302f01fa131373c9c01503441630; expires=Wed, 22-Aug-18 22:40:30 GMT';
        proxySettings.Token_URL_New__c = 'https://iav.lendingpoint.com/proxy-api-post-brms/oauth/token';
        proxySettings.Proxy_API_Cookie2__c = '; path=/; domain=.lendingpoint.com; HttpOnly';
 
        insert proxySettings;
    }
    
    @isTest static void gets_account_soft_pull_without_apps() {

      List<String> appList = new List<String>();

        ProxyApiAccountManagement.getAccountManagementSoftPull(appList);
    }
    
    @isTest static void gets_account_soft_pull() {
        LibraryTest.createBrmsSeqIds();
        
        Opportunity app = LibraryTest.createApplicationTH();
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        
        contract.Opportunity__c=app.id;
        
      List<String> appList = new List<String>();
        appList.add(app.Id);
        
        Test.startTest();
        
        ProxyApiAccountManagement.getAccountManagementSoftPull(appList);
        
        Test.stopTest();
        
        System.assertEquals(Date.today(), [SELECT Account_Management_Pull_Date__c FROM Opportunity WHERE Id =: app.Id LIMIT 1].Account_Management_Pull_Date__c);
    }
    
    @isTest static void updates_account_management_pull_date(){
        
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsConfigSettings(1,true);
    
        Opportunity app = LibraryTest.createApplicationTH();
        
        Test.startTest();
        
        ProxyApiAccountManagement.get(app.Id);
        
        Test.stopTest();
        
        System.assertEquals(Date.today(), [SELECT Account_Management_Pull_Date__c FROM Opportunity WHERE Id =: app.Id LIMIT 1].Account_Management_Pull_Date__c);
    }
    
    @isTest static void does_not_update_account_management_pull_date() {
        LibraryTest.createBrmsSeqIds();
        
        Opportunity app = LibraryTest.createApplicationTH();
        app.Account_Management_Pull_Date__c = Date.today().addDays(-15);
        update app;
        
      List<String> appList = new List<String>();
        appList.add(app.Id);
        
        ProxyApiAccountManagement.getAccountManagementSoftPull(appList);
        
        System.assertEquals(Date.today().addDays(-15), [SELECT Account_Management_Pull_Date__c FROM Opportunity WHERE Id =: app.Id LIMIT 1].Account_Management_Pull_Date__c);
    }
    
    @isTest static void gets_Sequence10_Refinance() {
        LibraryTest.createBrmsSeqIds();
        
        TBS_Logic__c tbs = New TBS_Logic__c();
        tbs.Name = '020';
        tbs.Description__c = 'Default Account Management with Month on Book (MOB) > 10 and if FICO > 620.';
        tbs.sequenceId__c = 10;
        tbs.Priority__c = 20;
        tbs.ProxyExecution__c = 'TwoPulls=true,Method=FICO,Condition=FICO_RULE,ConditionValue=720,Alternative=FICO';
        insert tbs;
  
        Opportunity app = LibraryTest.createApplicationTH();

        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Accrual_Start_Date__c = Date.today().addDays(-440);
        contract.Opportunity__c=app.id;
        update contract;

        
      List<String> appList = new List<String>();
        appList.add(app.Id);
        
        Test.startTest();
        
        ProxyApiAccountManagement.getAccountManagementSoftPull(appList);
        
        Test.stopTest();
    }
    
}