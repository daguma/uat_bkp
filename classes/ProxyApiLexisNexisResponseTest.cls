@isTest
public with sharing class ProxyApiLexisNexisResponseTest {
	@isTest static void validate() {
    	ProxyApiLexisNexisResponse response = new ProxyApiLexisNexisResponse();
     	response.riskView2ResponseEx = '123';
		response.lnJdgNote = 'the note';
     	
     	System.assertEquals(response.riskView2ResponseEx, '123');
     	System.assertEquals(response.lnJdgNote, 'the note');
 	}
}