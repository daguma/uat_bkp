@isTest
public Class TestBatchFactorTrustPaymentReporting{
    
    static testMethod void BatchFactorTrustPaymentReporting1(){
        TestHelperForManaged.createSeedDataForTesting();
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];

        loan__Loan_Account__c cl = TestHelper.createContract();
        cl.loan__Disbursal_Date__c=system.today();
        cl.loan__Loan_Status__c= 'Active - Good Standing';
        cl.loan__Pmt_Amt_Cur__c = 200.00;
        update cl;
       
        loan__Loan_Payment_Transaction__c transactionPay= new loan__Loan_Payment_Transaction__c();
        transactionPay.loan__Loan_Account__c= cl.id;
        transactionPay.loan__Transaction_Amount__c=1000;
        transactionPay.loan__Cleared__c=true;
        transactionPay.FT_Payment_Reporting__c=false;
        transactionPay.loan__Payment_Mode__c = pm.Id;

        insert transactionPay;
        
        List<loan__Loan_Payment_Transaction__c> scope = new List<loan__Loan_Payment_Transaction__c>();
        scope.add(transactionPay);
        
        Factor_Trust_Configuration__c n = new Factor_Trust_Configuration__c();
        n.Batch_Payment_Records__c = 1;
        n.Name ='TEst';
        insert n;
        Test.startTest();
        BatchFactorTrustPaymentReporting btch= new BatchFactorTrustPaymentReporting();
        Database.BatchableContext BC;          
        btch.execute(BC, scope) ;  
        
    }
    
    
    
    static testMethod void BatchFactorTrustPaymentReporting2(){
        TestHelperForManaged.createSeedDataForTesting();
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];

        loan__Loan_Account__c cl = TestHelper.createContract();
        cl.loan__Disbursal_Date__c=system.today();
        cl.loan__Loan_Status__c= 'Active - Good Standing';
        cl.loan__Pmt_Amt_Cur__c = 200.00;
         update cl;
       
        Factor_Trust_Configuration__c n = new Factor_Trust_Configuration__c();
        n.Batch_Payment_Records__c = 1;
        n.Name ='TEst';
        insert n;


        loan__Loan_Payment_Transaction__c transactionPay= new loan__Loan_Payment_Transaction__c();
        transactionPay.loan__Loan_Account__c= cl.id;
        transactionPay.loan__Transaction_Amount__c=1000;        
        transactionPay.FT_Payment_Reporting__c=false;
        transactionPay.loan__Rejected__c=true;
        transactionPay.FT_Bounce_Reporting__c=false;
        transactionPay.loan__Payment_Mode__c = pm.Id;

        insert transactionPay;
        
        List<loan__Loan_Payment_Transaction__c> scope = new List<loan__Loan_Payment_Transaction__c>();
        scope.add(transactionPay);
        
        Test.startTest();
        BatchFactorTrustPaymentReporting btch= new BatchFactorTrustPaymentReporting();
        btch.execute(null);  
        Test.stopTest();
    }
    
    static testMethod void BatchFactorTrustPaymentReporting3(){
        TestHelperForManaged.createSeedDataForTesting();
        loan__Payment_Mode__c pm = [SELECT Id FROM loan__Payment_Mode__c WHERE Name='ACH'];

        loan__Loan_Account__c cl = TestHelper.createContract();
        cl.loan__Disbursal_Date__c=system.today();
        cl.loan__Loan_Status__c= 'Active - Good Standing';
        cl.loan__Pmt_Amt_Cur__c = 200.00;
        update cl;

        Factor_Trust_Configuration__c n = new Factor_Trust_Configuration__c();
        n.Batch_Payment_Records__c = 1;
        n.Name ='TEst';
        insert n;

       
        loan__Loan_Payment_Transaction__c transactionPay= new loan__Loan_Payment_Transaction__c();
        transactionPay.loan__Loan_Account__c= cl.id;
        transactionPay.loan__Transaction_Amount__c=1000;        
        transactionPay.FT_Payment_Reporting__c=false;
        transactionPay.loan__Rejected__c=true;
        transactionPay.loan__Payment_Mode__c = pm.Id;
        transactionPay.FT_Bounce_Reporting__c=false;
        insert transactionPay;
        
        List<loan__Loan_Payment_Transaction__c> scope = new List<loan__Loan_Payment_Transaction__c>();
        scope.add(transactionPay);
        
        Test.startTest();
        BatchFactorTrustPaymentReporting btch= new BatchFactorTrustPaymentReporting();
        Database.BatchableContext BC;          
        btch.execute(BC, null) ;
        Test.stopTest();
    }
    
 }