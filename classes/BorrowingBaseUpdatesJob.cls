global class BorrowingBaseUpdatesJob implements Schedulable, Database.Batchable<sObject> , Database.Stateful {
    private Date today = Date.today();
    public Decimal totalElligible = 0, totalInelligible = 0;
    public String message = '', query = '';
    List<loan__Loan_Account__c> toUpdate = new List<loan__Loan_Account__c>(); 
    Integer inelCount = 0, elCount = 0, totCount = 0;
    global void execute(SchedulableContext sc){
        today = Date.today();
        message = 'Today = ' + Today + '\n\r';
        BorrowingBaseUpdatesJob job = new BorrowingBaseUpdatesJob();
        System.debug('Starting BorrowingBaseUpdatesJob at ' + Date.today());
        Database.executeBatch(job, 15);
        
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        query = 'SELECT ID From loan__Loan_Account__c ' +
            ' where     loan__Charged_Off_Date__c = null and ' +
            ' Asset_Sale_Line__c != null AND ' +
            ' (not Asset_Sale_Line__c  like \'FEB%\') AND ' +
            ' (not Asset_Sale_Line__c  like \'LP%\') AND ' +
            ' Asset_Sale_Line__c NOT IN  (\'SPE2\',\'2018 Funding Trust\',\'LendingPoint SPE, LLC\', \'FINWISE\') AND  ' +
            ' Is_Payment_Pending__c = false   AND ' +
            ' Loan__loan_Status__C in (\'Active - Good Standing\',\'Active - Bad Standing\') AND ' +
            ' ( (Inelligible_Date__c <> null AND Is_Inelligible__c = FALSE  AND inelligible_reason__c NOT IN ( \'Excluded by Covenant\', \'Fee\' , \'PTI\', \'Credit Criteria\', \'DTI\', \'Fraud\') ) OR ' +
            '   (Inelligible_Date__c = null AND Is_Inelligible__c = TRUE) )  ';
        
        system.debug('==query============='+query);
        return Database.getQueryLocator(query);    
        
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope) {
        try {
            List<id> ids = new List<Id>();
            for  (loan__Loan_Account__c loan  : (List<loan__Loan_Account__c>)scope){
                ids.add(loan.Id);
                totCount++;
            }
            
            toUpdate = new List<loan__Loan_Account__c>();
            for  (loan__Loan_Account__c loan  : [SELECT ID, Payment_Frequency_Masked__c,  (select Id, name, loan__Balance_Amount__c, loan__Due_Amt__c, loan__Due_Date__c, loan__Due_Type_Description__c, loan__Due_Type__c, loan__Payment_Amt__c,  loan__DD_Primary_Flag__c , loan__Payment_Date__c, 
                                                                                           loan__Payment_Satisfied__c, loan__Transaction_Date__c 
                                                                                           FROM loan__Dues_Details__r 
                                                                                           where loan__DD_Primary_Flag__c = true and loan__Payment_Satisfied__c = true  AND
                                                                                           loan__Due_Date__c = LAST_N_MONTHS:6
                                                                                           order by loan__Due_Date__c desc), 
                                                 Reelligible_Date__c, Asset_Sale_Date__c, Original_Term_Months__c, Asset_Sale_Line__c, Product_Name__c, Test_Bucket_Date__c, loan__First_Installment_Date__c, Name, loan__Interest_Rate__c, loan__Last_Payment_Date__c, Days_Unpaid_Due_Date__c, Day_Past_Oldest_Unpaid_Due_Date__c, loan__Oldest_Due_Date__c, Inelligible_Date__c,  Inelligible_Reason__c, loan__loan_Status__C, Originated_Grade__c,loan__Number_of_Days_Overdue__c,Current_Term_Months__c , Is_Inelligible__c,
                                                 Apy__c, Originated_EAR__c , loan__Principal_Remaining__c, loan__Total_Amount_Paid__c, loan__Last_Disbursal_Date__c
                                                 From loan__Loan_Account__c 
                                                 where  Id in: ids]){
                                                     if (loan.Inelligible_Date__c !=null && !loan.Is_Inelligible__c){
                                                         processElligibles(loan);
                                                     } else if (loan.Inelligible_Date__c ==null && loan.Is_Inelligible__c){
                                                         processInElligibles(loan);
                                                     }
                                                     
                                                 }
            if (toUpdate.size() > 0) update toUpdate;
        } catch (Exception e) {
            WebToSFDC.notifyDev('Borrowing Base ERROR - processElligibles', e.getMessage() + '\n' + e.getLineNumber() + '\n' + e.getStackTraceString());
        }
    }
    
    
    global void finish(Database.BatchableContext BC){
        WebToSFDC.notifyDev('BorrowingBaseUpdatesJob Finished', 'Contracts processed = ' + totCount + ' \n Inelligible Added = ' + inelCount + ' Amount = '  + totalInelligible + '\n ReElligible Added = ' + elCount  + ' Amount = ' + totalElligible +  '\n\nLog: \n\n' + message + '\n\n' , UserInfo.getUserId() );
    }
    
    
    public void processElligibles(loan__Loan_Account__c loan){
        try {
            Integer billPaidCount = 0, daysBetween1st =  loan.loan__First_Installment_Date__c != null  ? loan.loan__First_Installment_Date__c.daysBetween(loan.loan__Last_Payment_Date__c != null ? loan.loan__Last_Payment_Date__c : Date.today()) : 0;
            Date   cutoff = Date.today().addDays(OfferCtrlUtil.daysPerPeriod(loan.Payment_Frequency_Masked__c) * -4);
            System.Debug('Today = ' + today);
            String line = loan.Asset_Sale_Line__c;
            
            if(loan.Inelligible_Reason__c != null && String.isNotEmpty(line)) {
                loan.Inelligible_Date__c =  null;
                loan.Reelligible_Date__c = today;
            
                for  (loan__Loan_account_Due_Details__c d : loan.loan__Dues_Details__r) {
                    if (d.loan__Payment_Satisfied__c && d.loan__Due_Date__c > cutoff) billPaidCount++;
                }
                
                if (loan.Inelligible_Reason__c == 'Late 1st Payment' && ( (daysBetween1st < 15 && Date.today() <= loan.loan__Last_Disbursal_Date__c.addDays(60) && loan.Days_Unpaid_Due_Date__c <= 15) || billPaidCount >= 4)){
                    toUpdate.add(loan);
                    totalElligible += loan.loan__Principal_Remaining__c;
                    elCount++;
                    message += 'Reelligible = Late 1st Payment ' + loan.Name + ' line = ' + line + ' First Installment: ' + loan.loan__First_Installment_Date__c + ' Last Payment: ' + loan.loan__Last_Payment_Date__c + ' Days Between: ' + daysBetween1st + ' Bills Paid: ' + billPaidCount +  '\n\r'   ;
                } else if ('APY|APR'.contains(loan.Inelligible_Reason__c)  && line != 'ARES') {
                    toUpdate.add(loan);
                    elCount++;
                    message += 'Reelligible = APY ' + loan.Name + ' line = ' + line + ' APY: ' + Loan.APY__C + ' APR: ' + Loan.Originated_EAR__c + '\n\r'   ;
                    totalElligible += loan.loan__Principal_Remaining__c;
                } else if (loan.Inelligible_Reason__c == 'Term' && line != ('ARES')) {
                    toUpdate.add(loan);
                    elCount++;
                    message += 'Reelligible = Term ' + loan.Name + ' line = ' + line + ' Months: ' + Loan.Current_Term_Months__c + ' Product: ' + Loan.Product_Name__c + ' TB: ' + loan.Test_Bucket_Date__c + '\n\r'   ;
                    totalElligible += loan.loan__Principal_Remaining__c;
                } else if  ((line == 'ARES' && loan.Inelligible_Reason__c == 'DPD > 30' && loan.loan__Number_of_Days_Overdue__c < 31) ||
                            (line != 'ARES' && loan.Inelligible_Reason__c == 'DPD > 60' && loan.loan__Number_of_Days_Overdue__c < 61) ) {
                                toUpdate.add(loan);
                                elCount++;
                                message += 'Reelligible = DPD > ' + loan.Name + ' line = ' + line + ' Days: ' + loan.loan__Number_of_Days_Overdue__c   + ' Oldest Due Date: ' + loan.loan__Oldest_Due_Date__c + '\n\r';
                                totalElligible += loan.loan__Principal_Remaining__c;
                            }
            }
        } catch (Exception e) {
            WebToSFDC.notifyDev('Borrowing Base ERROR - processElligibles', e.getMessage() + '\n' + e.getLineNumber() + '\n' + e.getStackTraceString());
        }
        
    }
    
    
    
    public void processInelligibles(loan__Loan_Account__c loan){
        try {
            String line = '', product = String.isEmpty(loan.Product_Name__c) ? 'Lending Point' : loan.Product_Name__c;
            
            loan.Inelligible_Date__c = Today;
            loan.Reelligible_Date__c = null;
            line = loan.Asset_Sale_Line__c;
            if (String.isNotEmpty(line)) {
                if ((line == 'ARES' && loan.loan__Interest_Rate__c < 13.99) || 
                    (line != 'GUGG' && line != 'ARES' && (loan.APY__c < 9.99 || loan.Originated_EAR__c > 36.00)) ||
                    (line == 'GUGG' && (loan.APY__c < 10.99 || loan.APY__c > 36.00))) {
                        loan.Inelligible_Reason__c = 'APY';
                        toUpdate.add(loan);
                        totalInelligible += loan.loan__Principal_Remaining__c;
                        inelCount++;
                        message += 'Inelligible = APY/EAR Rate Out of Range ' + loan.Name + ' line = ' + line + ' - APY: ' + loan.APY__c + ' EAR: ' + Loan.Originated_EAR__c + '\n\r';
                    } else if ( (line == 'ARES' && loan.loan__Number_of_Days_Overdue__c  > 30) ||
                               (line != 'ARES' && loan.loan__Number_of_Days_Overdue__c  > 60)) {
                                   loan.Inelligible_Reason__c = 'DPD > ' + (line != 'ARES' ? '60' : '30');
                                   toUpdate.add(loan);
                                   totalInelligible += loan.loan__Principal_Remaining__c;
                                   inelCount++;
                                   message += 'Inelligible = DPD > ' + loan.Name + ' line = ' + line + ' Days: ' + loan.loan__Number_of_Days_Overdue__c + ' Oldest Due Date: ' + loan.loan__Oldest_Due_Date__c + '\n\r';
                               } else if ( loan.loan__Total_Amount_Paid__c == 0 && (loan.Days_Unpaid_Due_Date__c > 15 || Date.today() > loan.loan__Last_Disbursal_Date__c.addDays(60) )  ) {
                                   loan.Inelligible_Reason__c = 'Late 1st Payment';
                                   toUpdate.add(loan);
                                   totalInelligible += loan.loan__Principal_Remaining__c;
                                   inelCount++;
                                   message += 'Inelligible = Late 1st Payment ' + loan.Name + ' line = ' + line + ' Days: ' + loan.Day_Past_Oldest_Unpaid_Due_Date__c  + ' First Installment: ' + loan.loan__First_Installment_Date__c + '\n\r';
                               } else if( ( line == 'ARES' &&
                                           (
                                               (loan.Current_Term_Months__c > 37  && 'B2|C1|C2|D'.contains(loan.Originated_Grade__c) )  ||
                                               (loan.Current_Term_Months__c > 48  && 'A1|A2|B1'.contains(loan.Originated_Grade__c) ) 
                                           )
                                          ) ||
                                         (
                                             line != 'ARES' && 
                                             (
                                                 (loan.Original_Term_Months__c > 48  && loan.Test_Bucket_Date__c == null && product == 'Point Of Need' && 'B2|C1|C2|D|F|E'.contains(loan.Originated_Grade__c) )  ||
                                                 (loan.Original_Term_Months__c > 60  && loan.Test_Bucket_Date__c == null && product == 'Point Of Need' && 'A1|A2|B1'.contains(loan.Originated_Grade__c) ) ||
                                                 (loan.Original_Term_Months__c > 39  && loan.Test_Bucket_Date__c == null && product == 'Lending Point' && 'B2|C1|C2|D|F|E'.contains(loan.Originated_Grade__c))  ||
                                                 (loan.Original_Term_Months__c > 51  && loan.Test_Bucket_Date__c == null && product == 'Lending Point' && 'A1|A2|B1'.contains(loan.Originated_Grade__c) ) ||
                                                 (loan.Original_Term_Months__c > 60  && loan.Test_Bucket_Date__c != null )
                                             )
                                         )
                                        ) {
                                            loan.Inelligible_Reason__c = 'Term';
                                            toUpdate.add(loan);
                                            inelCount++;
                                            totalInelligible += loan.loan__Principal_Remaining__c;
                                            message += 'Inelligible = Term ' + loan.Name + ' line = ' + line + ' Months: ' + Loan.Original_Term_Months__c + ' Product: ' + product + ' TB: ' + loan.Test_Bucket_Date__c + '\n\r'   ;
                                        }
            }
        } catch (Exception e) {
            WebToSFDC.notifyDev('Borrowing Base ERROR - processInelligibles', e.getMessage() + '\n' + e.getLineNumber() + '\n' + e.getStackTraceString());
        }
    }
    
}