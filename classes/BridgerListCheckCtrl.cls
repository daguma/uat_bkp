public with sharing class BridgerListCheckCtrl {
	 public class Bridger_Entity {
        @AuraEnabled public string Name {get;set;}
        @AuraEnabled public DateTime CreatedDate {get;set;}
        @AuraEnabled public string Review_Status {get;set;}
        @AuraEnabled public Decimal Number_of_Hits {get;set;}
        @AuraEnabled public string Comment {get;set;}
        @AuraEnabled public string ListDescription {get;set;}
        @AuraEnabled public Date ListDate {get;set;}
        @AuraEnabled public string Status {get;set;}
        
    }
    
    @AuraEnabled
    public static List<Bridger_Entity> getBridgerDetails(Id oppId) {
        List<Bridger_Entity> resultList = new List<Bridger_Entity>();
        BridgerListCheckCtrl.Bridger_Entity bl = new BridgerListCheckCtrl.Bridger_Entity();
        bl.Comment = '';
        
        if (String.isNotEmpty(oppId)) {
            
            for (BridgerWDSFLookup__c res : [SELECT   Name
                                                   , createdDate
                                                   , Review_Status__c
                                                   , Number_of_Hits__c
                                                   , Comment__c
                                                   , List_Description__c
                                                   , List_Date__c
                                                   , Status__c
                                                   FROM BridgerWDSFLookup__c
                                                   WHERE Contact__c IN (SELECT Contact__c
                                                                                 FROM Opportunity
                                                                                 WHERE id = : oppId )
                                                   ORDER BY CreatedDate DESC]){
                                                       bl = new Bridger_Entity();
                                                       bl.Name = res.Name;
                                                       bl.CreatedDate = res.createdDate;
                                                       bl.Review_Status = res.Review_Status__c;
                                                       bl.Number_of_Hits = res.Number_of_Hits__c;
                                                       bl.Comment = res.Comment__c;
                                                       bl.ListDescription = res.List_Description__c;
                                                       bl.ListDate = res.List_Date__c;
                                                       bl.Status = res.Status__c;
                                                       resultList.add(bl);
                                                   }
        }
        //System.debug('CSI ' + resultList);
        if (resultList.isEmpty()) resultList.add(bl);
        return resultList;
        
    }
}