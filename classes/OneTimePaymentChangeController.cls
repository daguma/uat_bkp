public class OneTimePaymentChangeController {
    Public LP_Custom__c custom;
    Public String errorMessage;
    Public String noDaysOutAllowed;
    Public Date daysOutAllowed;
    Public Date changeDaysButtonDisabled;
    Public Boolean isManager;
    Public String lId;
    Public Date origDueDate;
    Public String productName;
    
    Public String OTPaymentChangeNote{get;set;}
    Public Date newDueDate{get;set;}
    Public String ruleList{get; set;}
    Public loan__loan_account__c theAccount{get; set;}
    Public Date nNextPaymentDueDate {get; set;}
    Public Decimal nPaymentAmount {get; set;}
    Public String paymentMethod {get; set;}
    Public Boolean isFirstPayment {get; set;}
    Public String dateRangeAllowed {get; set;}
    Public String paymentFrequency {get; set;}

    Public OneTimePaymentChangeController(Apexpages.StandardController controller){
		
        lId = '';
        theAccount = (loan__loan_account__c)controller.getRecord();
        if (theAccount != null && theAccount.Id <> null){
            lId = theAccount.Id;
        } else{
            lId = System.currentPageReference().getParameters().get('id');
        }

        //DateStringProperty = Date.Today().format(); // Populating field value by today date. 
        custom = [SELECT Id, PaymentDaysOutAllowed__c, PaymentChangeDaysButtonDisabled__c, OneTimeDueDateChange_LP__c, OneTimeDueDateChange_PON__c FROM LP_Custom__c];
                
        if (lId != null && lId.Length() > 0) {
            theAccount = [Select Id, Name, loan__Contact__c, loan__Contact__r.Name, Customer_Name__c, loan__OT_ACH_Account_Number__c,
                            loan__OT_ACH_Routing_Number__c, loan__OT_ACH_Account_Type__c, loan__OT_ACH_Relationship_Type__c,
                            loan__OT_ACH_Payment_Amount__c, loan__OT_ACH_Debit_Date__c, loan__OT_ACH_Bank_Name__c, loan__OT_ACH_Drawer_Name__c, 
                            loan__OT_ACH_Drawer_Zip__c, loan__ACH_Next_Debit_Date__c, loan__Next_Due_Generation_Date__c, loan__Pmt_Amt_Cur__c,
                          	loan__ACH_Account_Number__c, loan__ACH_Account_Type__c, loan__ACH_Bank_Name__c, loan__ACH_Bank__c, loan__ACH_Debit_Amount__c, 
                          	loan__ACH_Drawer_Name__c, loan__ACH_On__c, loan__ACH_Relationship_Type__c, loan__ACH_Routing_Number__c,  LastDateDueDateChanged__c,
                            Card_Next_Debit_Date__c, Is_Debit_Card_On__c, Opportunity__r.Offer_Selected_Date__c, 
                          	Payment_Frequency_Masked__c, loan__First_Installment_Date__c, Product_Name__c, Current_First_Payment_Date__c
                          	from loan__loan_account__c where Id =:lId ];
			
            isManager = false;
            String currentUser = [SELECT Name FROM Profile WHERE ID IN (SELECT ProfileId FROM User WHERE Id = :UserInfo.getuserid()) LIMIT 1].Name;
            //if (currentUser.contains('System Administrator')) isManager = true;
            if (currentUser.contains('Collections Manager')) isManager = true;
            if (currentUser.contains('Collections Supervisor')) isManager = true;
            if (currentUser.contains('Customer Service Manager - Underwriter')) isManager = true;
            setUpScreenFieldsAccordingToPaymentMethod();
            setupRules();
        }
    }

    @TestVisible
    private void setUpScreenFieldsAccordingToPaymentMethod() {

        if(theAccount.Is_Debit_Card_On__c){
            setUpInitialVariables('Debit Card',TheAccount.loan__Next_Due_Generation_Date__c);
        } else if(theAccount.loan__ACH_On__c) {
            setUpInitialVariables('ACH',TheAccount.loan__ACH_Next_Debit_Date__c);
        } else {
            setUpInitialVariables('Check',TheAccount.loan__Next_Due_Generation_Date__c);
        }
    }

    @TestVisible
    private void setUpInitialVariables(String pPaymentMethod, Date nextPmtDueDate){
        paymentMethod = pPaymentMethod;
        nNextPaymentDueDate = nextPmtDueDate;
        origDueDate = nextPmtDueDate;
        nPaymentAmount = theAccount.loan__Pmt_Amt_Cur__c != null ? theAccount.loan__Pmt_Amt_Cur__c : 0;
    }
    
    private void setupRules(){
        noDaysOutAllowed = '';
        productName = TheAccount.Product_Name__c;
        paymentFrequency = TheAccount.Payment_Frequency_Masked__c;
        isFirstPayment = false;
        if(origDueDate == TheAccount.loan__First_Installment_Date__c){ //compare product_name__c to ('Lending Point', 'Point Of Need')
            isFirstPayment  = true;
            if (productName == 'Lending Point'){ //Monthly:45,28 Days:43,Bi-Weekly:30,Semi-Monthly:30
                switch on TheAccount.Payment_Frequency_Masked__c{
                    when 'Monthly'{
                        noDaysOutAllowed = custom.OneTimeDueDateChange_LP__c.substring(custom.OneTimeDueDateChange_LP__c.indexOf('Monthly:') + 8, custom.OneTimeDueDateChange_LP__c.indexOf(',28 Days'));
                    }when '28 Days', 'Weekly'{
                        noDaysOutAllowed = custom.OneTimeDueDateChange_LP__c.substring(custom.OneTimeDueDateChange_LP__c.indexOf('28 Days:') + 8, custom.OneTimeDueDateChange_LP__c.indexOf(',Bi-Weekly'));
                    }when 'Semi-Monthly'{
                        noDaysOutAllowed = custom.OneTimeDueDateChange_LP__c.substring(custom.OneTimeDueDateChange_LP__c.indexOf('Bi-Weekly:') + 10, custom.OneTimeDueDateChange_LP__c.indexOf(',Semi-Monthly'));
                    }when 'Bi-Weekly'{
                        noDaysOutAllowed = custom.OneTimeDueDateChange_LP__c.substring(custom.OneTimeDueDateChange_LP__c.indexOf('Semi-Monthly:') + 13);
                    }
                }
                ruleList = noDaysOutAllowed;
            }else if (productName == 'Point Of Need'){ //Monthly:45,28 Days:43,Bi-Weekly:30,Semi-Monthly:30
                switch on TheAccount.Payment_Frequency_Masked__c{
                    when 'Monthly'{
                        noDaysOutAllowed = custom.OneTimeDueDateChange_PON__c.substring(custom.OneTimeDueDateChange_LP__c.indexOf('Monthly:') + 8, custom.OneTimeDueDateChange_LP__c.indexOf(',28 Days'));
                    }when '28 Days', 'Weekly'{
                        noDaysOutAllowed = custom.OneTimeDueDateChange_PON__c.substring(custom.OneTimeDueDateChange_LP__c.indexOf('28 Days:') + 8, custom.OneTimeDueDateChange_LP__c.indexOf(',Bi-Weekly'));
                    }when 'Semi-Monthly'{
                        noDaysOutAllowed = custom.OneTimeDueDateChange_PON__c.substring(custom.OneTimeDueDateChange_LP__c.indexOf('Bi-Weekly:') + 10, custom.OneTimeDueDateChange_LP__c.indexOf(',Semi-Monthly'));
                    }when 'Bi-Weekly'{
                        noDaysOutAllowed = custom.OneTimeDueDateChange_PON__c.substring(custom.OneTimeDueDateChange_LP__c.indexOf('Semi-Monthly:') + 13);
                    }                    
                }
                ruleList = noDaysOutAllowed;
            }
        }
        if(String.isBlank(noDaysOutAllowed)) { //if it's not then the standard rules apply
            noDaysOutAllowed = String.valueOf(custom.PaymentDaysOutAllowed__c);
            ruleList = noDaysOutAllowed;
        }
        if (isFirstPayment){
            switch on productName{
                when 'Lending Point'{
                    daysOutAllowed = theAccount.Opportunity__r.Offer_Selected_Date__c.addDays(Integer.valueOf(noDaysOutAllowed));
                    ruleList = ruleList + ' Days from Offer Selected Date ('  + String.valueOf(theAccount.Opportunity__r.Offer_Selected_Date__c.format()) +  ')';
                }when 'Point Of Need'{
                    Date disbursalDate = [SELECT loan__Disbursal_Date__c FROM loan__Loan_Disbursal_Transaction__c WHERE loan__Loan_Account__r.Id = :lId LIMIT 1].loan__Disbursal_Date__c;
                    daysOutAllowed = disbursalDate.addDays(Integer.valueOf(noDaysOutAllowed));
                    ruleList = ruleList + ' Days from Bank Disbursal Date ('  + String.valueOf(disbursalDate.format()) + ')';
                }when else{
                    daysOutAllowed = theAccount.loan__Next_Due_Generation_Date__c.addDays(Integer.valueOf(noDaysOutAllowed));
                    ruleList = ruleList + ' Days from Next Due Date ('  + String.valueOf(theAccount.loan__Next_Due_Generation_Date__c.format()) + ')';
                }
            }
        }else
        {
            daysOutAllowed = theAccount.loan__Next_Due_Generation_Date__c.addDays(Integer.valueOf(noDaysOutAllowed));
            ruleList = ruleList + ' Days from Next Due Date ('  + String.valueOf(theAccount.loan__Next_Due_Generation_Date__c.format()) + ')';
        }
        
        if(theAccount.LastDateDueDateChanged__c == null){
            changeDaysButtonDisabled = Date.today();
        }else{
            changeDaysButtonDisabled = theAccount.LastDateDueDateChanged__c.addDays((Integer.valueOf(custom.PaymentChangeDaysButtonDisabled__c)));
        }    
        system.debug(paymentMethod);
        if(paymentMethod == 'Debit Card'){
            dateRangeAllowed = 'Date Range Allowed: ' + String.valueOf(Date.today().format()) + ' - ' + String.valueOf(daysOutAllowed.format());
        }else{
        	dateRangeAllowed = 'Date Range Allowed: ' + String.valueOf(Date.today().addDays(2).format()) + ' - ' + String.valueOf(daysOutAllowed.format());
        }
    }

    public PageReference save(){
		Boolean isError = false;
        newDueDate = theAccount.loan__Next_Due_Generation_Date__c;
        if((Date.today() < changeDaysButtonDisabled) && !isManager){
            errorMessage = 'The current user cannot save this change until ' + changeDaysButtonDisabled.format() + ', please seek the assistance of a manager to proceed.';
            createMessage(apexpages.Severity.WARNING, errorMessage);
            isError = true;
            return null;
        }
		
        if(paymentMethod == 'ACH'){ 
            if(validateRules()){ 
                paymentChange_ach(); 
            }else{isError = true;}
        }
        else if(paymentMethod == 'Debit Card'){ 
            if(validateRules()){ 
                paymentChange_debitCard(); 
            }else{
                isError = true;
            }
    	}
        else if(paymentMethod == 'Check'){ paymentChange_check(); } //No rules for a Check payment to validate
        if(isError){
            return null;
        }
        system.debug('...Validations Completed returning from function...');
        return new PageReference(URL.getSalesforceBaseUrl().toExternalForm().replace('c.', 'loan.') + '/apex/tabbedLoanAccount?id=' + lId);
    }

    @TestVisible
    private void paymentChange_debitCard(){
        try{
        	insertOTDDChange();
        	insertOtherTrans('One-Time Debit Due Date Change');
        }catch (Exception e){
            system.debug('One-Time Debit Due Date Change ERROR: ' + e);
        }
        theAccount.LastDateDueDateChanged__c = Date.today();
        theAccount.loan__Next_Due_Generation_Date__c = newDueDate;
        theAccount.Card_Next_Debit_Date__c  = newDueDate;
        if(origDueDate == TheAccount.loan__First_Installment_Date__c) TheAccount.Current_First_Payment_Date__c = newDueDate;
        update theAccount;

    }
    
    @TestVisible
    private void paymentChange_check(){
        try{
        	insertOTDDChange();
        	insertOtherTrans('One-Time Check Due Date Change');
        }catch (Exception e){
            system.debug('One-Time Check Due Date Change ERROR : ' + e);
        }
        theAccount.LastDateDueDateChanged__c = Date.today();
        theAccount.loan__Next_Due_Generation_Date__c = newDueDate;
        if(origDueDate == TheAccount.loan__First_Installment_Date__c) TheAccount.Current_First_Payment_Date__c = newDueDate;
        update theAccount;

    }

    @TestVisible
    private void paymentChange_ach(){           
        theAccount.LastDateDueDateChanged__c = Date.today();
        theAccount.loan__Next_Due_Generation_Date__c = newDueDate;
        theAccount.loan__ACH_Next_Debit_Date__c = getNxtAmortizedDate();
        theAccount.loan__OT_ACH_Debit_Date__c = newDueDate;
        theAccount.loan__OT_ACH_Account_Number__c = theAccount.loan__ACH_Account_Number__c;
        theAccount.loan__OT_ACH_Routing_Number__c = theAccount.loan__ACH_Routing_Number__c ;
        theAccount.loan__OT_ACH_Account_Type__c = theAccount.loan__ACH_Account_Type__c;
        theAccount.loan__OT_ACH_Relationship_Type__c = theAccount.loan__ACH_Relationship_Type__c;
        theAccount.loan__OT_ACH_Bank_Name__c = theAccount.loan__ACH_Bank_Name__c;
        theAccount.loan__OT_ACH_Drawer_Name__c = theAccount.loan__ACH_Drawer_Name__c;
        theAccount.loan__OT_ACH_Payment_Amount__c = theAccount.loan__Pmt_Amt_Cur__c;
        if(origDueDate == TheAccount.loan__First_Installment_Date__c) TheAccount.Current_First_Payment_Date__c = newDueDate;
        try{
            insertOtAchNote(theAccount.loan__Pmt_Amt_Cur__c);
            insertOTDDChange();
            insertOtherTrans('One-Time ACH Due Date Change');
        }catch (Exception e){
            system.debug('One-Time ACH Due Date Change ERROR: ' + e);
        }
        update theAccount;
    }
    
    private Boolean validateRules(){
        if(Date.today().addDays(2) > newDueDate && paymentMethod != 'Debit Card'){
            errorMessage = 'You cannot change the due date before two days into the future.';
            createMessage(apexpages.Severity.WARNING, errorMessage);
            return false;
        }
        
        if(newDueDate > daysOutAllowed && !isManager){
            if(paymentMethod == 'Debit Card'){
                errorMessage = 'Date can only be set within ' +  Date.today().format() + ' and ' + daysOutAllowed.format();
            }else{
            	errorMessage = 'Date can only be set within ' +  Date.today().addDays(2).format() + ' and ' + daysOutAllowed.format();
            }
            createMessage(apexpages.Severity.WARNING, errorMessage);
            return false;
        }
        
        if(isManager && Date.today() < changeDaysButtonDisabled && OTPaymentChangeNote == ''){
            //Note is required if manager is changing prior to the changeDaysButtonDisabled limitation
            if(Date.today() < changeDaysButtonDisabled && OTPaymentChangeNote == ''){
                errorMessage = 'A note is required to save this change before ' + changeDaysButtonDisabled.format() + '.';
                createMessage(apexpages.Severity.WARNING, errorMessage);
                return false;
            }
        }
        if(Date.today() < changeDaysButtonDisabled && !isManager){
            errorMessage = 'A manager is required to save this change before ' + changeDaysButtonDisabled.format() + '.';
            createMessage(apexpages.Severity.WARNING, errorMessage);
            system.debug('Failed -- Last Due Date Changed: ' + theAccount.LastDateDueDateChanged__c + ', Button Disabled Days: ' + changeDaysButtonDisabled + ', Is Manager: ' + isManager);
            return false;                
        }                  
        return true;
    }

    @TestVisible
    private Date getNxtAmortizedDate(){
        Integer numberOfDays = Date.daysInMonth(Date.Today().year(), Date.Today().month());
        Date lastDayOfMonth = Date.newInstance(Date.Today().year(), Date.Today().month(), numberOfDays);
        List<loan__Repayment_Schedule__c> nextAmortizedDates = [SELECT Id, loan__Due_Date__c  FROM loan__Repayment_Schedule__c WHERE loan__loan_Account__c  =:lId AND loan__Due_Date__c  > :lastDayOfMonth ORDER BY loan__Due_Date__c LIMIT 5];
        Date nxtAmortizedDate = null;

        for(loan__Repayment_Schedule__c nextDate : nextAmortizedDates){
            if(nextDate.loan__Due_Date__c > newDueDate){
                nxtAmortizedDate = nextDate.loan__Due_Date__c;
                break;
            }
        }
        return nxtAmortizedDate;
    }

    @TestVisible
    private void insertOTDDChange(){
        One_Time_Due_Date_Change__c OTDDChange = New One_Time_Due_Date_Change__c();
        OTDDChange.Original_Due_Date__c = origDueDate;
        OTDDChange.New_Due_Date__c = newDueDate;
        OTDDChange.Payment_Amount__c = theAccount.loan__Pmt_Amt_Cur__c;
        OTDDChange.Note__c = OTPaymentChangeNote;
        OTDDChange.Payment_Method__c = paymentMethod;
        OTDDChange.Next_Amortization_Date__c = getNxtAmortizedDate();
        OTDDChange.CL_Contract__c = lId;
        OTDDChange.Rules_Used__c = ruleList;
        OTDDChange.Payment_Frequency__c = paymentFrequency;
        OTDDChange.Lending_Product__c = productName;
        insert OTDDChange;
    }

    @TestVisible
    private void insertOtherTrans(String txnType){
        loan__Other_Transaction__c OtherTrans = New loan__Other_Transaction__c();
        OtherTrans.loan__Loan_Account__c = lId;
        OtherTrans.loan__Txn_Date__c = Date.today();
        OtherTrans.loan__OT_ACH_Payment_Amount__c = theAccount.loan__Pmt_Amt_Cur__c;
        OtherTrans.loan__OT_ACH_Debit_Date__c = newDueDate;
        OtherTrans.loan__Transaction_Type__c = txnType;
        OtherTrans.OT_Date_Change__c = newDueDate;
        OtherTrans.loan__Next_Due_Generation_Date__c = newDueDate;
        insert OtherTrans;
    }

    public PageReference goBack() {
        return new PageReference(URL.getSalesforceBaseUrl().toExternalForm().replace('c.', 'loan.') + '/apex/tabbedLoanAccount?id=' + lId);
    }
    
    @TestVisible
    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }

    private void insertOtAchNote(Decimal otAchPayment) 
    {
        Note newNote = new Note();
        String noteTime = DateTime.now().format('HH:mm:ss');
        String noteDate = DateTime.now().format('MM/dd/YYYY');
        Integer d = newDueDate.day();
        Integer mo = newDueDate.month();
        Integer yr = newDueDate.year();
        DateTime otDate = DateTime.newInstance(yr, mo, d);
        String otDateFormatted = otDate.format('MM/dd/YYYY');        

        newNote.parentid = lId;
        newNote.title = 'OT Payment Due Date Change';
        newNote.body =  'On ' + noteDate + ' at ' + noteTime + ', the user, ' + UserInfo.getName() + ', changed the due date. The One Time ACH is scheduled for ' + otDateFormatted + 
            '\r\n' + OTPaymentChangeNote;
        insert newNote;
    } 
}