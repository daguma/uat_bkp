@isTest
private class manageNotesPermissionsTest {

    @isTest static void edit_note_with_collections() {
        Opportunity a = LibraryTest.createApplicationTH();
        
        LP_Custom__c lp = [SELECT Id FROM LP_Custom__c ORDER BY CreatedDate DESC LIMIT 1];
        lp.Offer_Selection_Override_Profiles__c = [Select Id from Profile where Name = 'System Administrator'].Id;
        lp.Edit_Note_Override_Profiles__c = [Select Id from Profile where Name = 'Collections'].Id;
        upsert lp;

        Note notes = new Note();
        notes.ParentId = a.id;
        notes.title = 'Test';
        notes.body = 'Testing';
        insert notes;

        User collectionsUser = [SELECT Id, Name FROM User WHERE ProfileId = '00e0B000000uLIhQAM' AND IsActive = true LIMIT 1];

        System.runAs(collectionsUser) {
            notes.title = 'Updated Note';
            update notes;
        }

        System.assertEquals('Updated Note', [SELECT Title FROM Note WHERE Id = : notes.Id LIMIT 1].Title);
    }
    
    @isTest static void Delete_Note_Restriction_collections_Profile() {
        Opportunity a = LibraryTest.createApplicationTH();
        
        LP_Custom__c lp = [SELECT Id FROM LP_Custom__c ORDER BY CreatedDate DESC LIMIT 1];
        lp.Delete_Note_Restriction_By_ProfileId__c = [Select Id from Profile where Name = 'Collections'].Id;
        upsert lp;

        Note notes = new Note();
        notes.ParentId = a.id;
        notes.title = 'Test';
        notes.body = 'Testing';
        insert notes;

        User collectionsUser = [SELECT Id, Name FROM User WHERE ProfileId = '00e0B000000uLIhQAM' AND IsActive = true LIMIT 1];
        
        try{
            System.runAs(collectionsUser) {
                Delete notes;
            }
            
        }catch (Exception e){
            system.Assert((e.getMessage()).contains('Insufficient Privileges To Delete Note Records'), e.getMessage());
            
        }


        System.assertEquals(1, [SELECT COUNT() FROM Note]);
    }

    @isTest static void do_not_edit_note_with_manager() {
        Opportunity a = LibraryTest.createApplicationTH();

        Note notes = new Note();
        notes.ParentId = a.id;
        notes.title = 'Test';
        notes.body = 'Testing';
        insert notes;

        User managerUser = [SELECT Id, Name FROM User WHERE ProfileId = '00eU0000000FMgJIAW' AND IsActive = true LIMIT 1];

        try {
            System.runAs(managerUser) {
                notes.title = 'Updated Note';
                update notes;
            }
        } catch (Exception e) {
            System.assert((e.getMessage()).contains('Insufficient privileges to edit/delete Note records'), e.getMessage());
        }

        System.assertEquals('Test', [SELECT Title FROM Note WHERE Id = : notes.Id LIMIT 1].Title);
    }

    @isTest static void deletes_note() {
        Opportunity a = LibraryTest.createApplicationTH();

        Note notes = new Note();
        notes.ParentId = a.id;
        notes.title = 'Test';
        notes.body = 'Testing';
        insert notes;

        System.assertEquals(1, [SELECT COUNT() FROM Note]);

        delete notes;

        System.assertEquals(0, [SELECT COUNT() FROM Note]);
    }

}