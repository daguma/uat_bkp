global class ClarityClearFraudRatio {

    public String name{get;set;}
    public Double one_minute_ago{get;set;}
    public Double ten_minutes_ago{get;set;}
    public Double one_hour_ago{get;set;}
    public Double twentyfour_hours_ago{get;set;}
    public Double seven_days_ago{get;set;}
    public Double fifteen_days_ago{get;set;}
    public Double thirty_days_ago{get;set;}
    public Double ninety_days_ago{get;set;}
    public Double threesixtyfive_days_ago{get;set;}
}