global with sharing class SalesForceUtil {
    /**
     * Sent an email notification
     * @param subject     [description]
     * @param body        [description]
     * @param userID      [allow "," to split Ids for several emails]
     */
    public static boolean EmailByUserId(String subject, String Body, String userID) {
        return sendEmail(subject, Body, userId, null);
    }

    /**
     * Sent an email notification
     * @param subject     [description]
     * @param body        [description]
     * @param userID      [allow "," to split Ids for several emails]
     * @param OrgWide     [description]
     */
    public static boolean EmailByUserIdWithOrgWide(String subject, String Body, String userID, String OrgWide) {
        return sendEmail(subject, Body, userId, OrgWide);
    }

    private static boolean sendEmail(String subject, String Body, String userID, String OrgWide) {
        boolean result = true;
        for (String uid : userID.split(',')) {
            try {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSenderDisplayName('LendingPoint');
                mail.setTargetObjectId(uid);
                mail.setSubject(subject);
                if (String.isNotEmpty(OrgWide)) mail.setOrgWideEmailAddressId(OrgWide);

                if (Body.contains('<') && Body.contains('>'))
                    mail.setHtmlBody(Body);
                else
                    mail.setPlainTextBody(Body);

                mail.setSaveAsActivity(false);

                if (!Test.isRunningTest()) Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

            } catch (Exception e) {
                result = false;
            }
        }
        return result;
    }


    /**
    * Checks if the date is a holiday.
    * @param {DateTWime} dt [The date/time to be evaluated].
    * @return {String} The date of the holiday or an empty string if there's no match.
    */
    private static String getHoliday(Datetime dt) {
        // Month, day
        String jsonM =  '{' +
                        '"7/4": "Independence Day", ' +
                        '"11/11": "Veterans Day", ' +
                        '"12/25": "Christmas Day", ' +
                        '"1/1": "New Year"' +
                        '}';

        // Month, week, day
        String jsonW =  '{' +
                        '"1/3/1": "Martin Luther King Jr. Day", ' + //Third Monday of January
                        '"2/3/1": "Presidents Day", ' + //Third Monday of February
                        '"5/5/1": "Memorial Day", ' + //Last Monday in May
                        '"9/1/1": "Labor Day", ' + //First Monday in September
                        '"10/2/1": "Columbus Day", ' + //Second Monday in October
                        '"11/4/4": "Thanksgiving Day" ' + //Fourth Thursday in November
                        '}';

        Map<String, Object> holidaysM = (Map<String, Object>)JSON.deserializeUntyped(jsonM);
        Map<String, Object> holidaysW = (Map<String, Object>)JSON.deserializeUntyped(jsonW);

        String month = String.valueOf(dt.monthGmt()), day = String.valueOf(dt.dayGmt());
        String week = String.valueOf(Math.floor(1 + ((dt.dayGmt() - 1) / 7)));
        String extraWeek = (dt.dayGmt() == 1 && (dt.dayGmt() + 7) > 30) ? '5' : null;

        String isHolidayM = String.valueOf(holidaysM.get(month + '/' + day));
        String isHolidayW = String.valueOf(holidaysW.get(month + '/' + Integer.valueOf(extraWeek == '5' ? extraWeek : week) + '/' + String.valueOf(dt.formatGmt('u'))));

        if (isHolidayM == null && isHolidayW == null)
            return '';
        else
            return (isHolidayM != null) ? isHolidayM : isHolidayW;
    }

    /**
    * Calculates the business days between two dates.
    * @param {DateTime} startDate [The starting date/time to be evaluated].
    * @param {DateTime} endDate [The ending date/time to be evaluated].
    * @return {Integer} The business days between the dates evaluated.
    */
    public static Integer calculateBusinessDays(Datetime startDate, Datetime endDate) {
        Integer businessDays = 0;

        while (startDate < endDate) {
            // getDay() returns the number of the day:
            // '6' is Saturday.
            // '0' is Sunday
            if (startDate.formatGmt('E') != 'Sat' && startDate.formatGmt('E') != 'Sun') {
                if (getHoliday(startDate) == '')
                    businessDays++;
            }
            startDate = startDate.addDays(1);
        }
        return businessDays;
    }

    public static Datetime getBusinessDate(Integer numberOfBusinessDays) {

        Integer times = 1, dayCount = 1;
        Datetime startDate = Date.today(), finalDate = Date.today(), nextDate = Date.today();

        while (times < numberOfBusinessDays) {
            nextDate = startDate.addDays(-dayCount);
            if ((getHoliday(nextDate) == '') && (nextDate.formatGmt('E') != 'Sat' && nextDate.formatGmt('E') != 'Sun')) {
                finalDate = nextDate;
                times ++;
            }

            dayCount ++;
        }

        return finalDate;
    }

    public static String formatMoney(String s) {
        s = '$' + s;
        if (!s.contains('.')) {
            s += '.00';
        } else {
            Integer dPos = s.indexOf('.');
            if (s.length() - dPos < 3) { s += '0'; }
        }
        return s;
    }

    /*
    Get Credit reports when atb o database is down.
    {!REQUIRESCRIPT("/soap/ajax/30.0/connection.js")}
    {!REQUIRESCRIPT("/soap/ajax/30.0/apex.js")}
    {!REQUIRESCRIPT("https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js")}
    for (var i = 0; i < 110; i++) {
    var result = sforce.apex.execute("SalesForceUtil","getCreditReportNotGeneratedForApp", {});
    console.log(result[0]);
    }
    alert ('DONE!') ;
     */
    Webservice static String getCreditReportNotGenerated() {
        Opportunity application = [SELECT Id, Status__c, Reason_of_Opportunity_Status__c, Alerts__c
                                   FROM Opportunity
                                   WHERE Alerts__c = 'Queue' LIMIT 1];
        String result = '';
        try {
            if (application != null) {
                result += 'App Id =>' + application.id;
                ProxyApiCreditReportEntity creditReportEntity = CreditReport.getByEntityId(application.Id);
                if (creditReportEntity == null) {
                    CreditReport.getCreditReportWS(application.id, false);
                    application.Alerts__c = 'Changed - New';
                } else {
                    application.Alerts__c = 'Changed - Existing';
                    if (creditReportEntity.automatedDecision.equalsIgnoreCase('Yes')) {
                        Scorecard__c scorecard = ScorecardUtil.getScorecard(creditReportEntity.entityId);
                        if (scorecard != null) {
                            if (scorecard.Acceptance__c == 'Y') {
                                application.Status__c = 'Credit Qualified';
                                application.Reason_of_Opportunity_Status__c = '';
                            } else if (scorecard.Acceptance__c == 'N') {
                                application.Status__c = 'Declined (or Unqualified)';
                                application.Reason_of_Opportunity_Status__c = 'Updated Scoring – No Offer';
                            }
                        }
                    } else if (creditReportEntity.automatedDecision.equalsIgnoreCase('No') && creditReportEntity.decision == false) {
                        application.Status__c = 'Declined (or Unqualified)';
                        application.Reason_of_Opportunity_Status__c = 'Did not pass disqualifiers';
                    } else if (creditReportEntity.automatedDecision.equalsIgnoreCase('No') && creditReportEntity.decision) {
                        Decisioning.declineByRiskBand(application.Id, creditReportEntity);
                    }
                }
                update application;
                result += ' - Status =>' + application.Status__c;
            }
        } catch (Exception e) {
            throw e;
            result += ' - Error =>' + e.getMessage();
        }
        return result;
    }

    /** get Unemployment Rate taking into account the contact state and zip code. If state and zip code combination does not exists we return a default value only by state **/
    public static decimal getUnemploymentRate(Contact c) {
        decimal uRate = -1.00;
        if (c == null) return 0.00;

        for (StateIndexUR__c s : [SELECT UnemploymentRate__c, ZipCode__c FROM StateIndexUR__c WHERE State__c = : c.MailingState AND (ZipCode__c = : c.MailingPostalCode OR ZipCode__c = '') LIMIT 1]) {
            if (String.isNotEmpty(s.ZipCode__c))
                return s.UnemploymentRate__c;
            else
                uRate = s.UnemploymentRate__c;
        }

        return uRate;
    }

    static void accountManagementProcess(loan__Loan_Account__c contract) {
        ProxyApiCreditReportEntity creditReportEntity = null;
        CreditReportInputData inputData = CreditReport.mapCreditReportInputData(contract.Opportunity__c, true, false);

        if (inputData.gdsRequestParameter == null)
            inputData.gdsRequestParameter = CreditReport.mapGDSRequestParameter(contract.Opportunity__c, false);

        inputData.gdsRequestParameter.sequenceId = '10';

        if (inputData != null) {
            try {
                if (Test.isRunningTest())
                    creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(LibraryTest.fakeCreditReportData(contract.Opportunity__c), ProxyApiCreditReportEntity.class);
                else
                    creditReportEntity = ProxyApiCreditReport.getCreditReport(JSON.serialize(inputData), false, true);
            } catch (Exception e) {
                creditReportEntity = null;
            }

            if (creditReportEntity != null && creditReportEntity.errorMessage == null) {
                update new Opportunity(Id = contract.Opportunity__c, Account_Management_Pull_Date__c = Datetime.now().date());
                update new Contact(Id = contract.loan__Contact__c, Other_Use_Of_Funds__c = 'DONE_AC');
            } else
                update new Contact(Id = contract.loan__Contact__c, Other_Use_Of_Funds__c = 'ERROR_AC');
        }
    }

    Webservice static void accountManagementPullsDESC() {
        for (loan__Loan_Account__c contract : [SELECT Id,
                                               Opportunity__c,
                                               loan__Contact__c
                                               FROM loan__Loan_Account__c
                                               WHERE loan__Contact__c  IN (SELECT Id FROM Contact WHERE Other_Use_Of_Funds__c = 'SUBMITTED_AC')
                                               ORDER BY CreatedDate DESC
                                               LIMIT 1]) {
            accountManagementProcess(contract);
        }
    }

    Webservice static void accountManagementPullsASC() {
        for (loan__Loan_Account__c contract : [SELECT Id,
                                               Opportunity__c,
                                               loan__Contact__c
                                               FROM loan__Loan_Account__c
                                               WHERE loan__Contact__c  IN (SELECT Id FROM Contact WHERE Other_Use_Of_Funds__c = 'SUBMITTED_AC')
                                               ORDER BY CreatedDate ASC
                                               LIMIT 1]) {

            accountManagementProcess(contract);
        }
    }

    public static Time GetTimeBetweenDates(DateTime startTime, DateTime endTime) {

        if (startTime == null || endTime == null)
            return Time.newInstance(0, 0, 0, 0);

        Integer iHours = endTime.hour() - startTime.hour();
        Integer iMinutes = endTime.minute() - startTime.minute();
        Integer iSeconds = endTime.second() - startTime.second();
        Integer iMiliseconds = endTime.millisecond() - startTime.millisecond();

        return Time.newInstance(iHours, iMinutes, iSeconds, iMiliseconds);
    }

    public static String QueryByGroupId() {
        return 'Select Id, UserOrGroupId, GroupId From GroupMember Where GroupId IN (\'' + Label.Kennesaw_Group_Id + '\', \'' + Label.DR_Group_Id + '\') ';
    }

    public static void SetOfflineUserByGroupId(String groupId) {
        for (GroupMember gm : [Select Id, UserOrGroupId From GroupMember Where GroupId = : groupId]) {
            for (AuthSession authS : [SELECT Id, lastModifiedDate FROM AuthSession WHERE UsersId = : gm.UserOrGroupId AND LoginType = 'Application' AND SessionType = 'UI' ORDER BY lastModifiedDate DESC LIMIT 1]) {
                if (GetTimeBetweenDates(authS.lastModifiedDate, System.now()).minute() > Integer.valueOf(Label.DAA_Limit_Timed_Out)) {
                    for (User u : [SELECT Id, isOnline__c  FROM User WHERE isOnline__c = true AND isActive = true AND Id = : gm.UserOrGroupId ]) {
                        u.isOnline__c = false;
                        update u;
                        delete authS;
                    }
                }
            }
        }
    }

    public static String GetLatestValidCreditReportId (String opportunityId, Boolean returnSequenceId) {
        Opportunity opp = [SELECT Id, CreditPull_Date__c, PrimaryCreditReportId__c, Contract_Renewed__r.Opportunity__c, Contract_Renewed__r.Opportunity__r.CreditPull_Date__c, Source_Contract__r.Opportunity__c, Type, Status__c FROM Opportunity WHERE Id = : opportunityId];
        ProxyApiCreditReportEntity creditReportEntity = null;
        if (opp.Status__c != null && (opp.CreditPull_Date__c == null || opp.PrimaryCreditReportId__c == null))
            creditReportEntity = CreditReport.getByEntityId(opp.Id);
        String sequenceId = '';
        String oppToUse = opportunityId;
        if (opp.Type == 'Refinance') oppToUse = opp.Contract_Renewed__r.Opportunity__c;
        if (opp.Type == 'Multiloan') oppToUse = opp.Source_Contract__r.Opportunity__c;
        List <AccountManagementHistory__c> accList = [SELECT Id, CreatedDate, CreditReportId__c FROM AccountManagementHistory__c WHERE Opportunity__c = : oppToUse ORDER BY CreatedDate DESC LIMIT 1];
        Date creditPullDate = (opp.CreditPull_Date__c != null) ? opp.CreditPull_Date__c :
                              ((creditReportEntity != null) ? Date.valueOf(DateTime.newInstance(creditReportEntity.CreatedDate)) : Date.today().addDays(-60));
        if (accList.size() > 0) {
            Datetime accDatetime = accList[0].CreatedDate;
            Date accDate = Date.newInstance(accDatetime.year(), accDatetime.month(), accDatetime.day());
            if (creditPullDate > Date.today().addDays(-30) && creditPullDate >= accDate) {
                sequenceId = (returnSequenceId ? ',1' : '' );
                if (opp.PrimaryCreditReportId__c != null)
                    return String.valueOf(opp.PrimaryCreditReportId__c) + sequenceId;
                else {
                    return String.valueOf(creditReportEntity.Id) + sequenceId;
                }
            } else if (accDate > creditPullDate && accDate > Date.today().addDays(-30)) {
                sequenceId = (returnSequenceId ? ',10' : '' );
                return String.valueOf(accList[0].CreditReportId__c) + sequenceId;
            } else
                return (returnSequenceId ? '0,0' : '0' );
        } else {
            if (creditPullDate > Date.today().addDays(-30)) {
                sequenceId = (returnSequenceId ? ',1' : '' );
                if (opp.PrimaryCreditReportId__c != null)
                    return String.valueOf(opp.PrimaryCreditReportId__c) + sequenceId;
                else
                    return String.valueOf(creditReportEntity.Id) + sequenceId;
            } else
                return (returnSequenceId ? '0,0' : '0' );
        }
    }
    
    public static boolean checkValuesForOffer(String state, Double efectiveAPR, Double fee, Double amount, String productName, Boolean isFinwise, Boolean isFEB) {

        Decimal feePercent = 0;

        if (fee > 0){
            feePercent = ((fee / amount) * 100);
            feePercent = feepercent.setscale(2);
        }
        
        System.debug('feePercent:: ' + feePercent);
        
        for (Check_Values_Offer_Selection__c cs : [SELECT Id, Name, check_Product_Name__c, check_Loan_Amount_max__c,
                                                          check_Loan_Amount_min__c, check_Effective_APR_min__c, check_Effective_APR_max__c,
                                                          check_Fee_Percent_min__c, check_Fee_Percent_max__c, check_States_Included__c,
                                                          check_IsFinwise__c, check_IsFEB__c
                                                   FROM Check_Values_Offer_Selection__c]) {

            // This validation(if) has been separated into 2 parts to make it easier to read
            // 
            // Check States is Included and is valid
            //Con-225 Added null check for state and check_States_Included__c
            if ( (state!= null && cs.check_States_Included__c!=null && cs.check_States_Included__c.contains(state)) &&
                    ( ( (cs.check_Product_Name__c <> null)
                        && (cs.check_Product_Name__c == productName) ) ||
                      ( (cs.check_IsFinwise__c)
                        && (isFinwise) ) ||
                      ( (cs.check_IsFEB__c)
                        && (isFEB) ) ) ) {

                // Check if it is within the parameters APR, Fee and Amount
                if ( (cs.check_Loan_Amount_min__c == null || amount >= cs.check_Loan_Amount_min__c)
                        && (cs.check_Loan_Amount_max__c == null || amount <= cs.check_Loan_Amount_max__c)
                        && (
                            !( (cs.check_Fee_Percent_min__c == null && feePercent == 0) || feePercent >= cs.check_Fee_Percent_min__c)
                            || !( (cs.check_Fee_Percent_max__c == null && feePercent == 0) || feePercent <= cs.check_Fee_Percent_max__c)
                            || !(cs.check_Effective_APR_min__c == null || efectiveAPR >= cs.check_Effective_APR_min__c)
                            || !(cs.check_Effective_APR_max__c == null || efectiveAPR <= cs.check_Effective_APR_max__c) ) ) {
					System.debug('CS:: ' + cs);
                    return false;
                }
            }


        }
        return true;
    }

    public static void lookForCasesToClose(String oppId, String contactId){
        if (String.isNotEmpty(oppId) && String.isNotEmpty(contactId)){
            list <case> cases = new list <case>();
    
            for (case c : [SELECT Id, Status, Description, SuppliedEmail, subject  
                           FROM Case
                           WHERE ContactId =: contactId
                           OR Opportunity__c =: oppId]){
    
                if ( (String.isNotEmpty(c.description) && (c.Description.contains('docs@lendingpoint.com') ||
                        c.Description.contains('customersuccess@lendingpoint.com')))
                        || (String.isNotEmpty(c.SuppliedEmail) && (c.SuppliedEmail.contains('docs@lendingpoint.com') ||
                                c.SuppliedEmail.contains('customersuccess@lendingpoint.com')))
                        || (String.isNotEmpty(c.subject) && (c.subject.contains('Documents Have Been Uploaded from Web') ||
                                c.subject.contains('Documents uploaded via the Web for App') )) ) {
                    System.debug('Case:: ' + c);
                    if (c.Status != 'Closed') {
                        c.Status = 'Closed';
                        cases.add(c);
                    }
                }
            }
            if (cases != null) update cases;
        } 
    }

}