public class MultiloanFails {
 Public static final String INELIGIBILITY_REVIEW_QUERY = 'SELECT Id, Contract__c, IsLowAndGrow__c, Contract__r.Opportunity__r.Type, ' +
      'Eligible_For_Soft_Pull__c, Manual_Eligibility_Check__c, Last_MultiLoan_Eligibility_Review__c, Eligible_For_Soft_Pull_Params__c, ' +
      ' LoanAmountCheck__c, Contract__r.SCRA__c, Contract__r.Date_of_Settlement__c, Contract__r.Settlement__c, Contract__r.Is_Material_Modification__c,  '+
      ' Contract__r.First_Payment_Missed__c, Contract__r.First_Payment_Made_after_Due_Date__c, Contract__r.DMC__c, Contract__r.loan__Metro2_Account_pmt_history_date__c, '+
      ' Contract__r.loan__ACH_On__c, Contract__r.Opportunity__r.StrategyType__c, Contract__r.loan__Contact__r.MailingState, Contract__r.Is_Debit_Card_On__c, Contract__r.Asset_Sale_Line__c, Contract__r.Servicing_Only_Asset__c, Contract__r.Product_Name__c '+
      'FROM Multiloan_Params__c ' +
      'WHERE  (Last_MultiLoan_Eligibility_Review__c < {0} OR Last_MultiLoan_Eligibility_Review__c = {4} ) AND ' +
      '  Contract__r.loan__Loan_Status__c = {1} AND ' +
      '  Contract__r.loan__Accrual_Start_Date__c < {2} AND ' +
      ' ( LoanAmountCheck__c < {3} OR' +
      '  Contract__r.SCRA__c = {5} OR' +
      '  Contract__r.Date_of_Settlement__c != {4} OR  ' +
      '  Contract__r.Settlement__c != {4} OR ' +
      '  Contract__r.Is_Material_Modification__c = {5} OR ' +
      '  Contract__r.First_Payment_Missed__c = {5} OR ' +
      '  Contract__r.First_Payment_Made_after_Due_Date__c = {5} OR '+
      '  (Contract__r.loan__ACH_On__c = {6} AND Contract__r.Is_Debit_Card_On__c = {6} ) OR ' +
      '  Contract__r.DMC__c  != {4} OR ' +
      '  Contract__r.loan__Metro2_Account_pmt_history_date__c > {7} OR ' +
      '  Contract__r.loan__Metro2_Account_pmt_history_date__c > {7} OR ' +
      '  Contract__r.Opportunity__r.Type != {8} OR ' + // RD-707 Adding failure condition
      // '  (Contract__r.Opportunity__r.StrategyType__c != {12} OR Contract__r.Opportunity__r.StrategyType__c != {14} ) OR' + -- RD-707 Removing Low and Grow restriction
      '  Contract__r.Product_Name__c != {9} OR ' + 
      '  Contract__r.loan__Contact__r.MailingState not in ({12}) )' +
      '  {13} LIMIT 10';
    
  public static String getIneligibilityReviewQuery(Integer queryLimit) {

    MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');
      
    List<String> fillers = new list<String> {'LAST_N_DAYS:' + (Integer.valueOf(multiloanSettings.Check_Frequency__c) - 1), '\'Active - Good Standing\''}; //0,1
    fillers.add('LAST_N_DAYS:' + (Integer.valueOf(multiloanSettings.Days_To_First_Check__c) - 1)); //2 
    fillers.add('' + Integer.valueOf(multiloanSettings.Minimum_Loan_Amount__c)); //3
    fillers.add('' + null); //4
    fillers.add('' + true); //5
    fillers.add('' + false); //6
    fillers.add('today'); //7
    fillers.add('\'New\''); //8   
    fillers.add('\'Lending Point\''); //9
    fillers.add('\'Stone Ridge\''); //10
    fillers.add('\'GLJ\''); //11
    // fillers.add('\'2\'');  //12
    fillers.add('\'' + [SELECT Allowed_Multiloan_States__c FROM MultiLoan_Settings__c].Allowed_Multiloan_States__c.replace(',', '\',\'') + '\''); ///13 - after RD-707 now 12
    // fillers.add('\'3\'');
      if (queryLimit > 0)
      fillers.add('limit ' + queryLimit);
    else
      fillers.add(''); //13
    return String.format(INELIGIBILITY_REVIEW_QUERY, fillers);
  }
    
  public static void updateMultiloanParamsLogs(List<sObject> scope ){

    List<Multiloan_Params_Logs__c> multiLogsList = new List<Multiloan_Params_Logs__c> ();
    List<Multiloan_Params__c> multiloanList = new List<Multiloan_Params__c>();
    Multiloan_Params_Logs__c multiLogs = new Multiloan_Params_Logs__c();
    MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');
    List<String> rulesFailed = new List<String>();

    for(sobject s : scope){

      Multiloan_Params__c param = (Multiloan_Params__c) s;
      if(param.LoanAmountCheck__c < Integer.valueOf(multiloanSettings.Minimum_Loan_Amount__c)){
         multiLogs.Minimum_Amount_Fail__c = true;
         rulesFailed.add(' - The loan amount is not at least $1,000');

      }
      if(param.Contract__r.SCRA__c){
        multiLogs.SCRA_Fail__c = true;
        rulesFailed.add(' - SCRA History ');
      } 
      if(param.Contract__r.Date_of_Settlement__c != null || param.Contract__r.Settlement__c != null) {
        multiLogs.Settlement_Fail__c = true;
         rulesFailed.add(' - Loan Settlement Modification History');
      }
      if(param.Contract__r.Is_Material_Modification__c){
        multiLogs.Material_Modification_Fail__c = true;
        rulesFailed.add(' - Loan Material Modification History');
      } 
      if(param.Contract__r.First_Payment_Missed__c || param.Contract__r.First_Payment_Made_after_Due_Date__c){
         multiLogs.First_Payment_Fail__c = true;
         rulesFailed.add(' - First Payment Missed');
      }
      if(param.Contract__r.DMC__c != null ){
        multiLogs.DMC_Fail__c  = true;
        rulesFailed.add('- DMC Modification');
      }
      if(param.Contract__r.loan__ACH_On__c == false && param.Contract__r.Is_Debit_Card_On__c == false ){
        multiLogs.ACH_OR_Debit_Card_Fail__c= true;
        rulesFailed.add('- ACH OR Debit Card ');

      }
      if((param.Contract__r.loan__Metro2_Account_pmt_history_date__c > Date.today()) ||(param.Contract__r.loan__Metro2_Account_pmt_history_date__c == null)){
        multiLogs.Metro2_Date_Fail__c= true;
        rulesFailed.add('- Last Reported Metro2 Date fail');

      }
      if(param.Contract__r.Product_Name__c != 'Lending Point'){
        multiLogs.Product_Name_Fail__c = true;
        rulesFailed.add('- Product name fail');
      }
      if((param.Contract__r.Asset_Sale_Line__c == 'Stone Ridge') || (param.Contract__r.Asset_Sale_Line__c == 'GLJ')){
        multiLogs.Asset_Sale_Line_Fail__c= true;
        rulesFailed.add('- Asset Sale line fail');
      }
      /*if(param.Contract__r.Opportunity__r.StrategyType__c != '1'){
        multiLogs.Low_and_Grow_Fail__c = true;
        rulesFailed.add('- Is low and grow ');
      }*/
      if(param.Contract__r.Opportunity__r.Type != 'New'){
        multiLogs.Type_Fail__c = true;
        rulesFailed.add('- Type Fail ');
      }
      
      List< MultiLoan_Settings__c> multiSettings = [SELECT Allowed_Multiloan_States__c FROM MultiLoan_Settings__c where Allowed_Multiloan_States__c =: param.Contract__r.loan__Contact__r.MailingState];

      if(multiSettings.size() == 0){
        multiLogs.States_Fail__c = true; 
        rulesFailed.add('- States');

      }



      param.Last_MultiLoan_Eligibility_Review__c = System.today();
        param.Manual_Eligibility_Check__c = false;
        multiLogs.Multiloan_Params__c  = param.Id;

        multiLogsList.add(multiLogs);

        multiloanList.add(param);
        if(param.Contract__c != null && rulesFailed.size()>0){
          createIneligibilityNote(param.Contract__c, rulesFailed);

        }

    }
    insert multiLogsList;
    update multiloanList; 

  }
    public static void createIneligibilityNote(String parentId, List<String> rulesFailed) {

    String bodyNote =   'On ' + System.now() + ', the eligibility check for this customer resulted in a decline.\n' +
                        'The following items caused the ineligibility: \n';

    for (String s : rulesFailed) bodyNote += s + '\n';

    Note n = new Note();
    n.Body = bodyNote;
    n.Title = 'Multi-loan Ineligibility Reasons';
    n.ParentId = parentId;

    insert n;
  }




}