@isTest public class ProxyApiCreditReportAttributeEntityTest {

    @isTest static void validate_credit_reportid() {
        ProxyApiCreditReportAttributeEntity crae = new ProxyApiCreditReportAttributeEntity();
        crae.creditReportId  = 1;
        System.assertEquals(1, crae.creditReportId);
    }

    @isTest static void validate_value() {
        ProxyApiCreditReportAttributeEntity crae = new ProxyApiCreditReportAttributeEntity();
        crae.value  = 'value test';
        System.assertEquals('value test', crae.value);
    }

    @isTest static void validate_is_approved() {
        ProxyApiCreditReportAttributeEntity crae = new ProxyApiCreditReportAttributeEntity();
        crae.isApproved  = true;
        System.assertEquals(true, crae.isApproved);
    }

    @isTest static void validate_approved_system() {
        ProxyApiCreditReportAttributeEntity crae = new ProxyApiCreditReportAttributeEntity();
        crae.approvedBy = 'system';
        String responseString = crae.approvedByDisplayText;
        String toTest = '';
        System.assertEquals(responseString, toTest);
    }

    @isTest static void validate_approved() {
        ProxyApiCreditReportAttributeEntity crae = new ProxyApiCreditReportAttributeEntity();
        crae.approvedBy = 'test';
        System.assertEquals('test', crae.approvedByDisplayText);
    }

    @isTest static void validate_id() {
        ProxyApiCreditReportAttributeEntity crae = new ProxyApiCreditReportAttributeEntity();
        crae.id  = 1;
        System.assertEquals(1, crae.id);
    }

    @isTest static void validate_attribute() {
        ProxyApiAttributeTypeEntity attr = new ProxyApiAttributeTypeEntity();
        attr.id = 1;
        attr.conceptType = 'test';
        attr.displayName = 'test';
        attr.description = 'test';
        attr.isDisplayed = false;

        ProxyApiCreditReportAttributeEntity crae = new ProxyApiCreditReportAttributeEntity();
        crae.attributeType = attr;
        ProxyApiAttributeTypeEntity response = crae.attributeType;
        System.assertEquals(attr, response);
    }

}