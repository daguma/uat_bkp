@isTest
private class tmpBatchJobTest {
    
    @testSetup static void initialize_objects(){
        Account acc = new Account();
        acc.Name = 'Refinance';
        acc.Is_Parent_Partner__c = true;
        acc.RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'Partner'].Id;
        acc.Type = 'Partner';
        acc.Active_Partner__c = true;
        insert acc;
    }
    
    @isTest static void set_refinance_opp(){
        
        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.isProcessed__c = false;
        insert refi;
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('SetRefiOpps'); 
		database.executebatch(j);
        Test.stopTest();
        
        Refi_MPD__c refiUpd = [SELECT Id, LastRefinanceOppId__c FROM Refi_MPD__c WHERE Id =: refi.Id LIMIT 1];
        //System.assertNotEquals(null, refiUpd.LastRefinanceOppId__c);
        
    }
    
    @isTest static void set_refinance_opp_not_eligible(){
        
        loan__loan_Account__c contract = creates_contract();
        contract.Original_Sub_Source__c = 'LoanHero';
        update contract;
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.isProcessed__c = false;
        insert refi;
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('SetRefiOpps');
		database.executebatch(j);
        Test.stopTest();
        
        Refi_MPD__c refiUpd = [SELECT Id, System_Debug__c, IsActive__c FROM Refi_MPD__c WHERE Id =: refi.Id LIMIT 1];
        System.assertNotEquals(null, refiUpd.System_Debug__c);
        System.assertEquals(false, refiUpd.IsActive__c);
        
    }
    
    @isTest static void set_refinance_opp_debug(){
        
        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.isProcessed__c = false;
        insert refi;
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('SetRefiOpps');
		database.executebatch(j);
        Test.stopTest();
        
        Refi_MPD__c refiUpd = [SELECT Id, System_Debug__c FROM Refi_MPD__c WHERE Id =: refi.Id LIMIT 1];
        
        System.assertNotEquals(null, refiUpd.System_Debug__c);
        
        tmpBatchJob job = new tmpBatchJob('SetRefiOpps', 'A');
        database.executebatch(job, 1);
    }

    @isTest static void executes_sequence_10(){
        
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.Status__c = 'Refinance Unqualified';
        update newApp;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.LastRefinanceOppId__c = newApp.Id;
        refi.isProcessed__c = false;
        insert refi;
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('ExecRefiOppsSeq10');
		database.executebatch(j);
        Test.stopTest();
        
        Refi_MPD__c refiUpd = [SELECT Id, OverrideRequired__c FROM Refi_MPD__c WHERE Id =: refi.Id LIMIT 1];
        
        System.assertEquals(true, refiUpd.OverrideRequired__c);
        
    }
    
    @isTest static void executes_sequence_10_debug(){
        
        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.Status__c = 'Refinance Unqualified';
        update newApp;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.LastRefinanceOppId__c = newApp.Id;
        refi.isProcessed__c = false;
        insert refi;
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('ExecRefiOppsSeq10');
		database.executebatch(j);
        Test.stopTest();
        
        Refi_MPD__c refiUpd = [SELECT Id, System_Debug__c FROM Refi_MPD__c WHERE Id =: refi.Id LIMIT 1];
        
        System.assertNotEquals(null, refiUpd.System_Debug__c);
        
        tmpBatchJob job = new tmpBatchJob('ExecRefiOppsSeq10', 'A');
        database.executebatch(job, 1);
    }
    
    @isTest static void gets_refinance_opps(){
        
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = true;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-32);
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.Status__c = 'Refinance Unqualified';
        update newApp;
        
        Scorecard__c newScore = new Scorecard__c();
        newScore.Acceptance__c = 'Y';
        newScore.Grade__c = 'A1';
        newScore.Opportunity__c = newApp.Id;
        insert newScore;
        
        Opportunity oldApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Contact__c = : contract.loan__Contact__c ORDER BY CreatedDate LIMIT 1];
        
        refinanceParams.Eligible_For_Soft_Pull_Params__c = oldApp.Id + ',' + newApp.Id + ',' + contract.Id;
        upsert refinanceParams;
        
        Loan_Refinance_Params_Logs__c refiLogs = new Loan_Refinance_Params_Logs__c(loan_refinance_params__c = refinanceParams.Id);
        insert refiLogs;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.LastRefinanceOppId__c = newApp.Id;
        refi.isProcessed__c = false;
        insert refi;
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('GetRefiOppsCR');
		database.executebatch(j);
        Test.stopTest();

        System.debug([SELECT System_Debug__c FROM loan_Refinance_Params__c]);
        
		System.assertEquals('Refinance Qualified',[SELECT Status__c FROM Opportunity WHERE Id =: newApp.Id].Status__c);
    }
    
    @isTest static void gets_refinance_opps_debug(){
        
        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.Status__c = 'Refinance Unqualified';
        update newApp;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.LastRefinanceOppId__c = newApp.Id;
        refi.isProcessed__c = false;
        insert refi;
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('GetRefiOppsCR');
		database.executebatch(j);
        Test.stopTest();
        
        Refi_MPD__c refiUpd = [SELECT Id, System_Debug__c FROM Refi_MPD__c WHERE Id =: refi.Id LIMIT 1];
        
        System.assertNotEquals(null, refiUpd.System_Debug__c);
        
        tmpBatchJob job = new tmpBatchJob('GetRefiOppsCR', 'A');
        database.executebatch(job, 1);
        
    }
    
    @isTest static void set_offers(){
        
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        loan__loan_Account__c contract = creates_contract();
        contract.Modification_Type__c = 'Loan Workout Agreement Modification';
        update contract;
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.Status__c = 'Refinance Qualified';
        newApp.Campaign_Id__c = 'RiskRefinanceOpp40M';
        newApp.Scorecard_Grade__c = 'A1';
        update newApp;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOpp40M';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.LastRefinanceOppId__c = newApp.Id;
        refi.additionalLoanAmount__c = 100;
        refi.isProcessed__c = false;
        insert refi;
        
        Integer offersBef = [SELECT COUNT() FROM Offer__c WHERE Opportunity__c =: newApp.Id];
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('SetRefiOppsOffers');
		database.executebatch(j);
        Test.stopTest();
        
        Integer offersAft = [SELECT COUNT() FROM Offer__c WHERE Opportunity__c =: newApp.Id];
        
        System.assert(offersAft > offersBef,'Offers');
        
    }
    
    @isTest static void set_offers_debug(){
        
        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.Status__c = 'Refinance Qualified';
        update newApp;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.LastRefinanceOppId__c = newApp.Id;
        refi.isProcessed__c = false;
        insert refi;
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('SetRefiOppsOffers');
		database.executebatch(j);
        Test.stopTest();
        
        Refi_MPD__c refiUpd = [SELECT Id, System_Debug__c FROM Refi_MPD__c WHERE Id =: refi.Id LIMIT 1];
        
        System.assertNotEquals(null, refiUpd.System_Debug__c);
        
        tmpBatchJob job = new tmpBatchJob('SetRefiOppsOffers', 'A');
        database.executebatch(job, 1);
        
    }
    
    @isTest static void deletes_attachment(){
        
        loan__loan_Account__c contract = creates_contract();
        
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.Status__c = 'Refinance Qualified';
        update newApp;
        
        Blob response = Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(LibraryTest.fakeCreditReportData(newApp.id))));

        LibraryTest.createAttachment(newApp.id, response);
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = true;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.LastRefinanceOppId__c = newApp.Id;
        refi.isProcessed__c = false;
        insert refi;
        
        Integer attachBef = [SELECT COUNT() FROM Attachment];
        
        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('DeleteRefiOppsAttach');
		database.executebatch(j);
        Test.stopTest();
        
        Integer attachAft = [SELECT COUNT() FROM Attachment];
        
        System.assert(attachAft < attachBef,'Delete attachment');
        
        tmpBatchJob job = new tmpBatchJob('DeleteRefiOppsAttach', 'A');
        database.executebatch(job, 1);
        
    }
    
    private static loan__loan_Account__c creates_contract() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = '28 Days';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        contract.loan__Interest_Accrued_Not_Due__c = 500;
        contract.loan__Frequency_of_Loan_Payment__c = 'WEEKLY';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        update contract;

        Offer__c off = new Offer__c();
        off.IsSelected__c = true;
        off.Opportunity__c = contract.Opportunity__c;
        off.APR__c = 0.2;
        off.Fee__c = 100;
        off.Fee_Percent__c = 0.04;
        off.FirstPaymentDate__c = Date.today().addDays(-29);
        off.Installments__c = 36;
        off.Loan_Amount__c = 5000;
        off.Term__c = 36;
        off.Payment_Amount__c = 230;
        off.Repayment_Frequency__c = 'Monthly';
        off.Preferred_Payment_Date__c = Date.today().addDays(-23);
        insert off;

        return contract;
    }
    
    @isTest static void set_refi_opps_normal_offers() {
	  ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
	  SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                'OK',
                '[{"Proxy_API_DefaultURL__c": "http://www.google.com"},{"access_token":"tokentest2"},{"expires_in":"' + Datetime.now().addDays(2).getTime() + '"}]',
                null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
      loan__loan_Account__c contract = creates_contract();
         
        LIST<loan_Refinance_Params__c> refinanceParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Refinance__c, Last_Refinance_Eligibility_Review__c
                FROM loan_Refinance_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        loan_Refinance_Params__c refinanceParams = null;

        if (refinanceParamsList.size() > 0) {
            refinanceParams = refinanceParamsList.get(0);
        } else {
            refinanceParams = new loan_Refinance_Params__c();
            refinanceParams.Contract__c = contract.Id;
        }
        refinanceParams.FICO__c = 600;
        refinanceParams.Grade__c = 'B2';
        refinanceParams.Eligible_For_Refinance__c = false;
        refinanceParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-2);
        upsert refinanceParams;

        SubmitApplicationService submit = new SubmitApplicationService();
        String appId = submit.submitApplication(contract.loan__Contact__c);

        Opportunity newApp = [SELECT Id, Name, Status__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.Contract_Renewed__c = contract.Id;
        newApp.Type = 'Refinance';
        newApp.Fee_Handling__c = 'Fee On Top';
        newApp.Status__c = 'Refinance Qualified';
        update newApp;
		
        Scorecard__c s = new Scorecard__c();
        s.Opportunity__c = newApp.Id;
        s.Grade__c='A1';
        insert s;
        
        Refi_MPD__c refi = new Refi_MPD__c();
        refi.IsActive__c = true;
        refi.Campaign_Id__c = 'RiskRefinanceOppSample';
        refi.Sequence10Grade__c = 'A1';
        refi.OverrideRequired__c = false;
        refi.ContractId__c = contract.Id;
        refi.InterestDiscount__c = 0;
        refi.InterestRate__c = 12.99;
        refi.MinimumInterestRate__c = 10.99;
        refi.Fee__c = 0;
        refi.OwnerId__c = UserInfo.getUserId();
        refi.System_Debug__c = '';
        refi.LastRefinanceOppId__c = newApp.Id;
        refi.isProcessed__c = false;
        insert refi;

        Test.startTest();
        tmpBatchJob j = new tmpBatchJob('SetRefiOppsNormalOffers');
        database.executebatch(j);
        Test.stopTest();
        Refi_MPD__c refiUpd = [SELECT Id, System_Debug__c FROM Refi_MPD__c WHERE Id = : refi.Id LIMIT 1];

        System.assertEquals(null, refiUpd.System_Debug__c);

    }
    
}