global class PaymentPendingProcessBatch implements Database.Batchable<sObject>, Schedulable, Database.Stateful {
    String query = '';
    Integer iRecsProcessed = 0;
    Set<String> noDaysPastDueStatuses = new Set<String> {'Closed - Obligations met', 'Closed - Sold Off', 'Canceled'};
    global Database.QueryLocator start(Database.BatchableContext BC){     
         try{
           String d=Datetime.now().adddays(2).format('yyyy-MM-dd');
            // Add TODAY LIMIT FOR Prod
            string loanId= 'a1uU00000009hGeIAI';
           query = 'select ID, Payment_Pending_End_Date__c, loan__Loan_Account__c from loan__Loan_Payment_Transaction__c where loan__Reversed__c = false and Payment_Pending_End_Date__c <= TODAY AND loan__Loan_Account__c=:loanId Order by Name, CreatedDate desc ';     
            System.debug('Query = ' + query);
             iRecsProcessed = 0;
         }catch(exception ex){
           System.debug('Exception:'+ ex.getMessage());
           WebToSFDC.notifyDev('PaymentPendingProcessBatch.QueryLocator ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage());
       }
       return Database.getQueryLocator(query);    
    }
    
    global void execute (SchedulableContext sc) {
        System.debug('Starting PaymentPendingProcessBatch at ' + Date.today());
        PaymentPendingProcessBatch job = new PaymentPendingProcessBatch();
        Database.executeBatch(job);
    }

    public void execute(Database.BatchableContext BC,List<sObject> scope){
        try { 
            List<loan__Loan_Payment_Transaction__c> pmts = (List<loan__Loan_Payment_Transaction__c>)scope;
            System.debug('Payments to process = ' + pmts.size());
            Set<Id> loanIDs = new Set<Id>();
            
            
            for (loan__Loan_Payment_Transaction__c pmt : pmts) {
                loanIDs.add(pmt.loan__Loan_Account__c);
            }
            System.debug('Loans to process = ' + loanIDs.size());
            Map<Id, loan__Loan_Account__c> mLoans = new Map<Id, loan__Loan_Account__c>([select Id, loan__Oldest_Due_Date__c, loan__Number_of_Days_Overdue__c, loan__Principal_Remaining__c,
                                                                                                    loan__Next_Installment_Date__c, loan__Delinquency_Grace_Days__c, loan__Loan_Status__c , loan__Write_off_Tolerance_Amount__c
                                                                                        from loan__Loan_Account__c where Id in : loanIDs]);
            List<loan__Loan_account_Due_Details__c> bills = [Select Id, loan__Loan_Account__c, loan__Due_Date__c from loan__Loan_account_Due_Details__c where loan__Loan_Account__c in : loanIDs AND loan__DD_Primary_Flag__c = true and loan__Payment_Satisfied__c = false]; 
            Date oldestDate;
            
            for (loan__Loan_Payment_Transaction__c pmt : pmts) {
                loan__Loan_Account__c loan = mLoans.get(pmt.loan__Loan_Account__c);
                oldestDate = loan.loan__Next_Installment_Date__c;
                
                pmt.Payment_Pending_End_Date__c = null;
                pmt.Payment_Pending__c = 'Cleared';
                // Search for oldest Due Date
                for (loan__Loan_account_Due_Details__c bill : bills) {
                    if (bill.loan__Loan_Account__c == loan.Id) {
                        if (oldestDate == null || (oldestDate != null && oldestDate > bill.loan__Due_Date__c)) oldestDate = bill.loan__Due_Date__c;
                    }
                }
                // Update DPD
                loan.loan__Oldest_Due_Date__c = oldestDate;
                loan.loan__Number_of_Days_Overdue__c = (Date.today().daysBetween(oldestDate) * -1);
                if ( (loan.loan__Number_of_Days_Overdue__c <= loan.loan__Delinquency_Grace_Days__c) || (noDaysPastDueStatuses.contains(loan.loan__Loan_Status__c))) loan.loan__Number_of_Days_Overdue__c = 0;
                // Update Status
                if (loan.loan__Number_of_Days_Overdue__c == 0 && !loan.loan__Loan_Status__c.contains('Closed') && loan.loan__Loan_Status__c != 'Canceled') loan.loan__Loan_Status__c = 'Active - Good Standing';
                if (loan.loan__Principal_Remaining__c <= loan.loan__Write_off_Tolerance_Amount__c  && !loan.loan__Loan_Status__c.contains('Closed') && loan.loan__Loan_Status__c != 'Canceled') loan.loan__Loan_Status__c = 'Closed - Obligations Met';
                // Update Aging --- NO NEED because aging migration job takes care of it.
                
            }
            iRecsProcessed += pmts.size();
            update pmts;
            update mLoans.values();
            
        }catch(exception ex){
            System.debug('Exception:'+ ex.getMessage());
            WebToSFDC.notifyDev('PaymentPendingProcessBatch.execute ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
        }
    }
    
    global void finish(Database.BatchableContext BC){
            WebToSFDC.notifyDev('PaymentPendingProcessBatch Finished', 'Payments processed = ' + iRecsProcessed, UserInfo.getUserId() );
        System.debug('PaymentPendingProcessBatch Finished!');
    }
    webservice static string overridePending(Id paymentTrans){
        String retVal = 'This action requires a Collections Manager';
        if ((UserInfo.getProfileId() == [SELECT Id FROM Profile WHERE Name = 'Collections Manager' LIMIT 1].Id) || (UserInfo.getProfileId() == [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id)){
            try{
                Database.BatchableContext BC;
                List<loan__Loan_Payment_Transaction__c> pmts = [SELECT Id, Payment_Pending__c FROM loan__Loan_Payment_Transaction__c WHERE Id = :paymentTrans AND ( Payment_Pending__c = 'Pending' OR Payment_Pending_End_Date__c <> null) LIMIT 1];
                if (!pmts.isEmpty()){
                    PaymentPendingProcessBatch b = new PaymentPendingProcessBatch();
                    b.execute(BC, pmts);
                    retVal = 'Pending Payment Cleared';
                }
                else
                {
                    retVal = 'No Pending Payment to Clear';
                }
            }
            catch (exception ex){
                retVal = ex.getMessage();
            }
        }
        return (retVal);
    }
}