@isTest
private class OpsWorkflowUtilTest {
    
    @testSetup static void testOpsWorkflow() {
        OpsWorkflowUtil ops = new OpsWorkflowUtil();
        ops.applicantsInstance = null;
        ops.segmentsList = null;
        ops.CUST_PROFILE_KEY = 'CustProfile';
        ops.DOC_CHK_KEY = 'DocCheck';
        ops.divHeader = 'div';
        ops.divFooter = 'div';
        ops.divTableFooter = 'div';
        ops.divTableHeaders = 'div';
        ops.divTableRows = 'div';  
        ops.attach = null;
        ops.expandedEmployedApplicants = null;
        ops.expandedMiscellaneousApplicants = null;
        ops.expandedRetiredApplicants = null;
        ops.expandedSelfEmployedApplicants = null;
        ops.isSignedContract = null;
        ops.showsEmployedApplicants2 = null;
        ops.showsSelfEmployedApplicants2 = null;
        ops.savedOk = null;
        ops.fraudIndicators = null;
        ops.app = null;
        ops.allApplicantsList = null;
        ops.employedApplicants1List = null;
        ops.employedApplicants2List = null;
        ops.miscellaneousApplicantsList = null;
        ops.retiredApplicantsList = null;
        ops.selfemployedApplicants1List = null;
        ops.selfemployedApplicants2List = null;
        ops.appId = null;
        ops.custProfileHTML = null;
        ops.docChecklistHTML = null;
        ops.errorMessage = null;
        ops.hideCustProfileButton = null;
        ops.hideDocChkButton = null;
    }
    
    @testSetup static void setup_custom_settings() {
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name='finwise',Finwise_Base_URL__c='https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/',Apply_State_Restriction__c=false,
                                      Finwise_LendingPartnerId__c='5',Finwise_LendingPartnerProductId__c='7',
                                      Finwise_LoanStatusId__c='1',finwise_partner_key__c='testKey',Finwise_product_Key__c='testkey',
                                      Finwise_RateTypeId__c='1',Whitelisted_States__c='MO',Investor__c = 'LendingPoint SPE 2');
        insert FinwiseParams;                                   
                                          
        Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name='POS',Asset_Id__c=5,SFDC_Value__c=false);
        insert AsetCls;
    }
    
    @isTest static void initialize_customer_profile_popup() {
        OpsWorkflowUtil c = new OpsWorkflowUtil();
        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'New';
        update app;
        c.appId = app.Id;
        c.initializeCustomerProfile();
    }

    @isTest static void initialize_documents_checklist_popup() {
        OpsWorkflowUtil c = new OpsWorkflowUtil();
        Opportunity app = LibraryTest.createApplicationTH();
        app.Type = 'New';
        update app;
        c.appId = app.Id;
        c.initializeDocumentChecklist();
        c.opsWfSaveApp();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(app);
        OpsWorkflowUtil planan = new OpsWorkflowUtil(sc);
        
        /*PageReference pageRef = Page.Coborrower;
        pageRef.getParameters().put('id', String.valueOf(app.Id));
        Test.setCurrentPage(pageRef);*/
    }

    @isTest static void get_all_dispositions() {
        OpsWorkflowUtil c = new OpsWorkflowUtil();
        List<SelectOption> bankStatsDispos = c.getBankStatementsDispos();
        List<SelectOption> allAppsOtherDispos = c.getAllAppsOtherDispos();
        List<SelectOption> empAppsOtherDispos = c.getEmpAppsOtherDispos();
        List<SelectOption> ytdGrossDispos = c.getYtdGrossDispos();
        List<SelectOption> otherApplicantsDispos = c.getOtherApplicantsDispos();
    }

    @isTest static void get_years() {
        OpsWorkflowUtil c = new OpsWorkflowUtil();
        List<SelectOption> years = c.getYears();
    }
}