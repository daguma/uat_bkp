Global Class SubmitApplicationService {
 
    //Moved to class level in order to try to run the applications by contact select only once
    static List<Opportunity> lApps;
 
    Webservice static String submitAppFromContact(String contactID) {
        try {
            SubmitApplicationService submit = new SubmitApplicationService();
            String resp = submit.submitApplication(contactID);
        } catch (Exception e) {
            return 'Error while submitting Application. ' + e.getMessage();
        }
 
        return 'Application has been submitted';
    }
 
    Webservice static Boolean verifyBridger(String contactID) {
 
        boolean result = true;
        String xmlResponse = '';
 
        for (BridgerWDSFLookup__c bridgerItem : [SELECT ID,
                Review_status__c
                FROM BridgerWDSFLookup__c
                WHERE Contact__c = : contactID
                                   AND CreatedDate > : system.today() - 30
                                   ORDER BY CreatedDate
                                   LIMIT 1 ]) {
 
            return bridgerItem.Review_status__c  == 'No match';
        }
 
        Contact cont = [SELECT  Id,
                        Name,
                        SSN__c,
                        BirthDate,
                        MailingStreet,
                        MailingState,
                        MailingPostalCode,
                        MailingCountry,
                        MailingCity,
                        FirstName,
                        LastName
                        FROM Contact
                        WHERE Id = : contactID
                                   ORDER by CreatedDate
                                   LIMIT 1];
 
        BridgerSFLookup bridger = new BridgerSFLookup(cont);
 
        if (!test.isRunningTest()) {
            xmlResponse = bridger.connect(bridger.createRequest());
 
            if (String.isEmpty(bridger.bridgerError) ) {
                bridger.parseDOC(xmlResponse);
            }
 
            if (bridger.matchList.size() > 0) {
                result = bridger.matchList[0].Review_status__c  == 'No match';
            }
        }
        return result;
    }
    
    Webservice static Boolean verifyWatchDog(String contactID) {
 
        boolean result = true;
        String xmlResponse = '';
 
        for (BridgerWDSFLookup__c bridgerItem : [SELECT ID,
                Review_status__c
                FROM BridgerWDSFLookup__c
                WHERE Contact__c = : contactID
                                   AND CreatedDate > : system.today() - 30
                                   ORDER BY CreatedDate
                                   LIMIT 1 ]) {
 
            return bridgerItem.Review_status__c  == 'No match';
        }
 
        Contact cont = [SELECT  Id,
                        Name,
                        SSN__c,
                        BirthDate,
                        MailingStreet,
                        MailingState,
                        MailingPostalCode,
                        MailingCountry,
                        MailingCity,
                        FirstName,
                        LastName
                        FROM Contact
                        WHERE Id = : contactID
                                   ORDER by CreatedDate
                                   LIMIT 1];
 
        BridgerSFLookup bridger = new BridgerSFLookup(cont);
 
        if (!test.isRunningTest()) {
            xmlResponse = bridger.connect(bridger.createRequest());
 
            if (String.isEmpty(bridger.bridgerError) ) {
                bridger.parseDOC(xmlResponse);
            }
 
            if (bridger.matchList.size() > 0) {
                result = bridger.matchList[0].Review_status__c  == 'No match';
            }
        }
        return result;
    }
 
    static public boolean doBridgerContactHit(String ContactId) {
        boolean pass = false;
        try {
            pass = verifyBridger(ContactID);
            if (!pass || Test.isRunningTest()) {
                /*FIX
                for ( Opportunity asOpp : [Select Id, Lost_Reason__c, StageName, Probability,Contact__c From Opportunity where Contact__c = :  ContactID order by Id desc limit 1])
                {
                    asOpp.Probability = 0;
                    asOpp.Lost_Reason__c = 'OFAC Failed';
                    asOpp.StageName = 'Closed Lost';
                    upsert asOpp;
                }
                */
            }
        } catch (Exception e) {
            WebToSFDC.notifyDev('SubmitApplicationService Error - doBridgerContactHit', 'ContactId = ' + ContactId + ' - ' + e.getMessage() + ' line ' + e.getLineNumber() + '\n' + e.getStackTraceString());
        }
 
        return pass;
    }

    public String submitApplication(String contactID) {
       return submitApplication(contactId, null);
    }
 
    public String submitApplication(String contactID, Account pAcc) {
        LP_Custom__c lp = CustomSettings.getLPCustom();
        ezVerify_Settings__c ezSettings = ezVerify_Settings__c.getInstance();
        map<ID, Lending_Product__c> productMap = new map<ID, Lending_Product__c> ();
        Account lAcc = new account();
        Contact c = [SELECT  IP__c,
                     ints__Bank_Account_Number__c,
                     ints__Routing_Number__c,
                     Account_Type__c,
                     Payment_Method__c,
                     Monthly_Rent_Mortgage__c,
                     Residence_Type__c,
                     Use_of_Funds__c,
                     Employment_Type__c,
                     Referred_By__c,
                     Campaign_Id__c,
                     Id,
                     Name,
                     Loan_Amount__c,
                     Annual_Income__c,
                     AccountId,
                     LeadSource,
                     Lead_Sub_Source__c,
                     Point_Code__c,
                     Lead_Provider_Source_Code__c,
                     JSessionId__c,
                     Patient_Applied__c,//Ez-95
                     Originating_Source__c, //API-332
                     Total_Financial_Responsibility__c, Care_Risk_Classification__c, Recurring_Treatment__c,
                     Coverage_Amount__c, Coverage_Probability__c, Deductible_Amount__c, Co_Payment__c,
                     Co_Insurance_Coverage__c, Relationship_to_Insured__c, Insurance_Carrier__c, Coverage_Status__c,
                     Coverage_Begin_Date__c, Coverage_End_Date__c, Preferred_Language__c, Product__c , industry__c, Requested_Payment_Amount__c, //SM-942
                     Previous_Employment_End_Date__c, Previous_Employment_Start_Date__c, Previous_Employer_Phone__c,
                     Employer_Name__c, Employment_Start_Date__c, Prior_Employment_Name__c
                     FROM Contact
                     WHERE ID = : contactID];
 
        Opportunity app = new Opportunity();
        app.Patient_Applied__c = c.Patient_Applied__c;//Ez-95
        app.Type = 'New';
        app.Total_Provider_Payment_Amount__c = 0; //FPP-44
        if(c.Originating_Source__c != null) app.Originating_Source__c = c.Originating_Source__c; //API-332
        app.Fee_Handling__c = 'Net Fee Out';
        app.ProductName__c = c.Product__c;
        app.Industry__c = c.Industry__c;
        String product = string.isNotEmpty(c.Product__c) ? c.Product__c : '';
        String pName = generalPurposeWebServices.isPointOfNeed(product) ? ezSettings.Product_Name__c : '';
        if (generalPurposeWebServices.isSelfPayProduct(product)) pName = ezSettings.Product_Name_SelfPay__c;
        if (String.isEmpty(pName)) pName = lp.Default_Lending_Product__c;
        for (Lending_Product__c lpro : [Select Id, Name, Loan_Lending_Product__c, Loan_Lending_Product__r.loan__Default_Interest_Rate__c  from Lending_Product__c where Name = : pName limit 1]) { //EZ-68
            app.Lending_Product__c = lpro.Id;
            if (!generalPurposeWebServices.isPointOfNeed(product)) app.ProductName__c = lpro.Name;
            productMap.put(lpro.id, lpro);
        }
        String AccrualPeriod = 'ACTUAL/360'; //default value
        if(!String.IsBlank(app.ProductName__c)){
            List<loan__Loan_Product__c> loanProduct = [SELECT ID, Name, loan__Time_Counting_Method__c FROM loan__Loan_Product__c WHERE Name = :app.ProductName__c LIMIT 1];
            if (loanProduct.size() > 0){
                if(loanProduct.get(0).loan__Time_Counting_Method__c == loan.LoanConstants.TIME_COUNTING_ACTUAL_DAYS){
                    AccrualPeriod = '365/365';
                }else if (loanProduct.get(0).loan__Time_Counting_Method__c == clcommon.CLConstants.TIME_COUNTING_ACTUAL_360){
                    AccrualPeriod = 'ACTUAL/360';
                }
            }
        }
 
        if (string.isNotEmpty(c.Product__c) && !generalPurposeWebServices.isPointOfNeed(c.Product__c)  && !generalPurposeWebServices.isSelfPayProduct(c.Product__c)) {     //API-148
            app.Fee__c = 0.05 * (c.Loan_Amount__c == null ? 0.0 : c.Loan_amount__c);
        }
        if (!String.isEmpty(c.Lead_Sub_Source__c)) {
            /*1390 - Adding Details value to Last_Page_Submitted__c field when application is created for LP customer Portal*/
            if ((c.LeadSource<> null && c.LeadSource.equalsIgnoreCase('LPCustomerPortal')) || (  c.Lead_Sub_Source__c.equalsIgnoreCase('LPCustomerPortal'))) {
                app.Last_Page_Submitted__c = Label.WP_Details_Step;
            }
            //for (Account lAcc : [SELECT Id,Name,Mailing_State_Province__c, Bureau__c, Default_Fee_Handling__c, Industry,loan__Investor__c,Discount_Fee__c FROM Account WHERE Partner_Sub_Source__c =: c.Lead_Sub_Source__c AND Type IN ('Partner','Merchant') LIMIT 1 ])
            lAcc = pAcc != null ? pAcc : WebToSFDC.getPartnerAccount(c.Lead_Sub_Source__c); //FPP-38 - call the common method to associate Merchant Account
            if (lAcc != null) {
                app.Partner_Account__c = lAcc.id;
                app.Bureau__c = lAcc.Bureau__c;
                app.Fee_Handling__c = lAcc.Default_Fee_Handling__c;
 
                //FPP-11========= START==============
                if (generalPurposeWebServices.isFPPAndPONProduct(lAcc.Product_Type__c, lAcc.Merchant_Partner_Type__c)) {
                    app.Industry__c =  lAcc.Industry;
                    //FPP-16=====START=======
                    app.Merchant_Discount_Pct__c = lAcc.Discount_Fee__c;
                    app.Merchant_Discount_Amount__c = (lAcc.Discount_Fee__c <> null) ? (lAcc.Discount_Fee__c * (c.Loan_Amount__c == null ? 0.0 : c.Loan_amount__c)) / 100 : app.Merchant_Discount_Amount__c;
                    //FPP-16=====END=========
                    app.Merchant_Name__c = lAcc.Name;
                    app.Merchant_State__c = lAcc.Mailing_State_Province__c;
                }
                //FPP-11========= END==============
                app.Is_Direct_Pay_On__c = false;
                for (Partner_Product_Bank_Account__c pBank : [Select Id, Deposit_to_Vendor__c from   Partner_Product_Bank_Account__c Where Deposit_to_Vendor__c = true and Account__c = : lAcc.Id and   Lending_Product__c = :  app.Lending_Product__c LIMIT 1] ) {
                    app.Is_Direct_Pay_On__c = pBank.Deposit_to_Vendor__c;
                }
            }
        }
 
        app.Contact__c = c.ID;
        app.Lead_Sub_Source__c = c.Lead_Sub_Source__c;
        app.Point_Code__c = c.Point_Code__c;
        app.LeadSource__C = c.LeadSource;
        app.Lead_Provider_Source_Code__c = c.Lead_Provider_Source_Code__c;
        app.Amount = c.Loan_Amount__c;
        app.Campaign_Id__c = c.Campaign_Id__c;
        app.Referred_By__c = c.Referred_By__c;
        app.Income_Source__c = c.Employment_Type__c;
        app.Use_of_Funds_Contact__c = c.Use_of_Funds__c;
        app.Residence_Type__c = c.Residence_Type__c;
        app.Monthly_Rent_Mortgage__c = c.Monthly_Rent_Mortgage__c;
        app.JSessionId__c = c.JSessionId__c;
        app.Care_Risk_Classification__c = c.Care_Risk_Classification__c;
        app.Co_Insurance_Coverage__c = c.Co_Insurance_Coverage__c;
        app.Landing_Sequence__c = 2;
        app.Product_Type__c = 'LOAN';
        app.Days_Convention__c = AccrualPeriod;
        app.Interest_Calculation_Method__c = 'Declining Balance';
 
        app.ACH_Account_Number__c = c.ints__Bank_Account_Number__c;
        app.ACH_Routing_Number__c = c.ints__Routing_Number__c;
        app.ACH_Account_Type__c = String.isEmpty(c.Account_Type__c) ? 'Checking' : c.Account_Type__c;
        app.Payment_Method__c = c.Payment_Method__c;
        app.IP__c = c.IP__c;
 
        for (loan__Office_Name__c tOff : [Select Id from loan__Office_Name__c where Name = : lp.Default_Company__c limit 1]) {
            app.Company__c = tOff.Id;
        }
 
        for (RecordType rtList : [Select Id from RecordType
                                  where SObjectType = 'Opportunity' and DeveloperName = : 'LP']) {
            app.RecordTypeId = rtList.Id;
        }
 
        // leave at 10000 default. ScoreCard will set the correct value
        app.Maximum_Loan_Amount__c = 10000;
        app.Maximum_Payment_Amount__c = c.Annual_Income__c * 0.179999 / 12;
        app.AccountId = c.AccountId;
 
 
        if (String.isEmpty(app.Bureau__c)) app.Bureau__c = SubmitApplicationService.getBureau(c.Lead_Sub_Source__c, contactID);
        List<Unique_Code_Hit__c> lstUhits = [Select Id, Opportunity__c from Unique_Code_Hit__c
                                             where LeadSource__c = : c.LeadSource AND Sub_Source__c = :  c.Lead_Sub_Source__c AND
                                                     Contact__c = : app.Contact__c AND Code__c = : c.Point_Code__c and Opportunity__c = null Order by CreatedDate desc LIMIT 1];
 
        if ( lstUhits.size() > 0) app.Unique_Code_Hit_Id__c = lstUhits[0].id; //API-101
 
        if (c.AccountId != null ) app.AccountId = c.AccountId;
        app.LeadSource__c = c.LeadSource;
        app.Name = c.Name;
        app.CloseDate = system.today();
        app.StageName = 'Qualification';
        app.Amount = c.Loan_Amount__c;
        app.Requested_Loan_Amount__c = c.Loan_Amount__c;
        System.debug('APPLICATION STATUS : ' + app.Status__c);
        app.Requested_Payment_Amount__c = c.Requested_Payment_Amount__c ; //Additional Parameter
        // system.debug('==app.Partner_Account__r.Promotion_Duration__c=='+app.Partner_Account__r.Promotion_Duration__c);
        //EZ-68
        if ((generalPurposeWebServices.isPointOfNeed(app.ProductName__c) || generalPurposeWebServices.isSelfPayProduct(app.ProductName__c)) &&
                (  lacc != null && lacc.Promotion_Duration__c != null && lacc.Promotion_Duration__c > 0   ) )  {
            app.Promotion_Active__c = true;
            app.Promotion_Term_In_Months__c = Integer.ValueOf(lAcc.Promotion_Duration__c) / 30 ; //==EZ-84 ==
            Lending_Product__c lProduct = null;
            if (app.Lending_Product__c != null)    {
                if (productMap.containskey(app.Lending_Product__c)) {
                    lProduct = productMap.get(app.Lending_Product__c);
                }
            }
            app.Promotion_Rate__c = (lProduct != null && lProduct.Loan_Lending_Product__c != null) ? lProduct.Loan_Lending_Product__r.loan__Default_Interest_Rate__c : ezSettings.Default_Interest_Rate__c;
            system.debug('----promoRate------' + app.Promotion_Rate__c );
        }
        //EZ-68
        system.debug('==app==' + app);
        insert app;
 
 
        for (Unique_Code_Hit__c uh : lstUhits) {
            uh.Opportunity__c = app.Id;
            upsert uh;
        }
        //Original changes MAINT-1317
        Employment_Details__c emp = New Employment_Details__c();
        
        emp.Opportunity__c = app.Id;
        emp.Employer_Name__c = c.Employer_Name__c;
        emp.Employment_Start_Date__c = c.Employment_Start_Date__c;
        emp.Hire_Date__c = c.Employment_Start_Date__c;
        emp.Previous_Employment_End_Date__c = c.Previous_Employment_End_Date__c;
        emp.Previous_Employment_Start_Date__c = c.Previous_Employment_Start_Date__c;
        emp.Prior_Company_Phone__c = c.Previous_Employer_Phone__c;
        emp.Prior_Employment_Contact_Number__c = c.Previous_Employer_Phone__c;
        insert emp;       
        
        return app.Id;
 
    }
 
    /**
        * Read what the partner is from the Application. If one is available,
        * then use the Bureau from that. If not available, OR, Partner = LPCustomerPortal,
        * then find the last campaign on the contact, and use the bureau from there.
        * If non is found, or is specified, use the default.
        * @param  partner   Lead_Sub_Source__c
        * @param  contactId
        * @return the bureau to get the credit report.
    */
    public static String getBureau(String partner, String contactId) {
        String bureau = '';
        for (Campaign camp : [SELECT UW_Bureau__c
                              FROM Campaign
                              WHERE Id IN (SELECT CampaignID
                                           FROM CampaignMember
                                           WHERE ContactID = : contactId
                                          )
                              ORDER BY CreatedDate DESC
                              LIMIT 1]) {
            bureau = camp.UW_Bureau__c;
        }
 
        return (!String.isEmpty(bureau) && bureau.equalsIgnoreCase('TransUnion')) ? CreditReport.TRANSUNION : CreditReport.EXPERIAN;
    }
 
    /**
    * Return the payment frequency for the offers (30 days, 28 days).
    * @param  partner [description]
    * @return the payment frequency to use in the offers
    */
    public static String getPaymentFrequency(String accountFrequency) {
 
        String result = 'T';
 
        if (String.isNotEmpty(accountFrequency)) {
 
            if (accountFrequency.equalsIgnoreCase('30 days') || accountFrequency.equalsIgnoreCase('Monthly') )
                result = 'M';
            else if (accountFrequency.equalsIgnoreCase('Bi-weekly'))
                result = 'B';
            else if (accountFrequency.equalsIgnoreCase('Semi-Monthly'))
                result = 'S';
            else if (accountFrequency.equalsIgnoreCase('Weekly'))
                result = 'W';
        }
 
        return result;
    }
 
    /*
    * checkAndGetExistanceByContact
    *   - This method calls the existing checkExistanceByContact(contact,'')
    *   Returns:
    *   A CheckExistanceResult, defined as a SubmitApplicationService inner class
    * - CheckExistanceResult.appExists returns true if NO apps are found as per checkExistanceByContact()
    * - CheckExistanceResult.lApps returns the list of apps found in the SELECT statement in checkExistanceByContact()
    * */
    public static CheckExistanceResult checkAndGetExistanceByContact(String contact, String pProduct) {
        return checkAndGetExistanceByContact( contact , pProduct, null);
    }

    public static CheckExistanceResult checkAndGetExistanceByContact(String contact, String pProduct, Account pAcc) {
        Boolean pExists = checkExistanceByContact(contact, pProduct, pAcc);
        return new CheckExistanceResult( pExists , lApps);
    }
 
    public static Boolean checkExistanceByContact(String ContactID) {
        return checkExistanceByContact(ContactID, '', null);
    }

    public static Boolean checkExistanceByContact(String ContactID, Account pAcc) {
        return checkExistanceByContact(ContactID, '', pAcc);
    }


    public static Boolean checkExistanceByContact(String ContactID, String pProduct) {
        return checkExistanceByContact(ContactID, pProduct, null);
    }

 
    // returns true if no apps are found
    public static Boolean checkExistanceByContact(String ContactID, String pProduct, Account pAcc) {
        LP_Custom__c lp = CustomSettings.getLPCustom();
        ezVerify_Settings__c EZVerify = CustomSettings.getEZVerifyCustom();
        Date d = (lp <> null && lp.Days_Before_New_App_Submission_Retry__c <> null && lp.Days_Before_New_App_Submission_Retry__c > 0) ? Date.Today().addDays(-1 * lp.Days_Before_New_App_Submission_Retry__c.intValue()) : Date.Today().addDays(-26); //API-287 changes
        Date dateXDaysAgo = Date.Today().addDays(-1 * lp.Days_before_Declined_Retry__c.intValue());
        Date date90Days =  Date.Today().addDays(-90);
        boolean isActive = false, isLessThan30 = false, declinedLessXDaysAgo = false;
 
        String DqsToSkip = String.isNotEmpty(lp.DQS_for_90_days__c) ? lp.DQS_for_90_days__C : '' ;
        String prodName = 'Lending Point';
        Boolean bIsMultipleOpp = false, declineSuppression = false;
        Decimal openContractCount ;
        List<Id> lIds = new List<Id>();
        List<Opportunity> statusApps = new List<Opportunity>();
 
        lApps = [Select Id, CreatedDate, Status__c, ProductName__c, LeadSource__c, Lending_Product__r.Name, Lead_Provider_Source_Code__c, Items_Requested__c,
                 Contact__r.product__c, OwnerId, Reason_of_Opportunity_Status__c, Partner_Account__c, Partner_Account__r.Assign_Immediately__c,
                 Selected_Offer_Id__c, Lead_Sub_Source__c, Encrypted_AppId_AES__c, Estimated_Grand_Total__C, Contact__r.AccountId, Partner_Account__r.Multiple_Opportunity__c, Contact__r.Lead_Sub_Source__c, Contact__c,Contract_Signed_Date__c  From Opportunity
                 Where Contact__c = : ContactID AND Name like 'APP%' order By CreatedDate Desc];
        if (String.isNotEmpty(pProduct )) prodName = pProduct;
        else {
            for (Contact c : [select id, Name, Product__c from Contact where id = :ContactID LIMIT 1])
                if (String.isNotEmpty(c.Product__c)) prodName = c.Product__c;
        }
        //Boolean bIsEZ = (generalPurposeWebServices.isPointOfNeed(prodName) || generalPurposeWebServices.isSelfPayProduct(prodName));
        //FPP-5 , FPP-6 - Fetch multiple opp flag from Merchant Account
        if (!lApps.IsEmpty() && lApps[0].Contact__c != null && string.isNotEmpty(lApps[0].Contact__r.Lead_Sub_Source__c) ) {
            Account lAcc = pAcc != null ? pAcc : WebToSFDC.getPartnerAccount(lApps[0].Contact__r.Lead_Sub_Source__c); // call the common method to associate Merchant Account
            if (lAcc != null) {
                bIsMultipleOpp = lAcc.Multiple_Opportunity__c;
                openContractCount =  lAcc.Open_Contract_Count__c;
                declineSuppression = lAcc.Decline_Suppression_Inactive__c;
            }
        }
        Boolean isCoreProduct = !generalPurposeWebServices.isPointOfNeed(prodName) && !generalPurposeWebServices.isSelfPayProduct(prodName);
        system.debug('==bIsMultipleOpp ==' + bIsMultipleOpp );
        if (bIsMultipleOpp) {
            system.debug('==Product==' + prodName);
            //Change the status of all the open Applications.
            if (!lApps.IsEmpty()) changeAppstatus(lApps);
        }
        List<Opportunity> lstEZVerifyApps = new List<Opportunity>();
        //Pass the applications only for Core Applications to check 30 Days Condition.
        for (Opportunity app : lApps) {
            system.debug('--app.CreatedDate--' + app.CreatedDate);
            if (String.isEmpty(app.ProductName__c)) app.ProductName__c = 'Lending Point';
            /*if(isCoreProduct && (generalPurposeWebServices.isPointOfNeed(app.ProductName__c) || generalPurposeWebServices.isSelfPayProduct(app.ProductName__c))) {
                lstEZVerifyApps.add(app);
            }*/
            if (!isLessThan30 && !declinedLessXDaysAgo && prodName == app.ProductName__c ) {
                lIds.add(app.Id);
                system.debug('==lIds==' + lIds);
                if (app.CreatedDate > d  && app.Status__c != ('Funded') && !bIsMultipleOpp) {
                    isLessThan30 = true;
                }
 
                if (String.isNotEmpty(app.Status__c) && app.Status__c.toLowerCase().contains('unqualified') && !declineSuppression && app.CreatedDate >= dateXDaysAgo  && app.ProductName__c.equalsIgnoreCase(prodName))  {
                    boolean bAllOPassed = false;
                    Integer failedGoodDQs = 0;
                    if (CreditReport.getAttachment(app.Id) != null) {
                        bAllOPassed = true;
                        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getByEntityId(app.id);
                        if (creditReportEntity != null && creditReportEntity.brms <> null && creditReportEntity.brms.rulesList <> null) {
                            for (ProxyApiCreditReportRuleEntity rule : creditReportEntity.brms.rulesList) {
                                if (DqsToSkip.contains(rule.deRuleNumber)) {
                                    if (rule.deRuleStatus == 'Fail') failedGoodDQs++;
                                } else if (rule.deRuleStatus == 'Fail') {
                                    bAllOPassed = false; break;
                                }
                            }
                        }
                    }
                    system.debug('--bAllOPassed--' + bAllOPassed );
                    system.debug('--failedGoodDQs --' + failedGoodDQs );
 
                    declinedLessXDaysAgo = (bAllOPassed && failedGoodDQs <= 1) ? app.CreatedDate >= date90Days : true;
                }
            } else if (prodName != app.ProductName__c && !bIsMultipleOpp) { // && (generalPurposeWebServices.isPointOfNeed(app.ProductName__c) || generalPurposeWebServices.isSelfPayProduct(app.ProductName__c))){ }
                lstEZVerifyApps.add(app);
            }
            
            //API-287 
            if(!isLessThan30) {
                if (app.CreatedDate > d  && app.Status__c != ('Funded') && !bIsMultipleOpp) {
                    isLessThan30 = true;
                }
                if(!isLessThan30) lstEZVerifyApps.add(app);
            } //API-287 
        }
        system.debug('--declinedLessXDaysAgo --' + declinedLessXDaysAgo );
        system.debug('==lstEZVerifyApps==' + lstEZVerifyApps);
 
        system.debug('--date90Days--' + date90Days);
        if (!lstEZVerifyApps.ISEmpty()) changeAppstatus(lstEZVerifyApps);
        Integer count = 0;
        // Now let's check for fundings
        if (!isLessThan30 && !declinedLessXDaysAgo) {
            /* for (loan__Loan_Account__c loan : [Select ID, Opportunity__c, loan__Loan_Status__c from loan__Loan_Account__c WHERE loan__Contact__c =: ContactID order by CreatedDate Desc]) {
                 string lStatus = loan.loan__Loan_Status__c;
                 if (lStatus.contains('Approv') || lStatus.contains('Active') || lStatus.contains('Written') || lStatus.contains('Partial')) {
                     count++;
                     //isActive = true; //break;
                 }
                 if(bIsMultipleOpp && openContractCount !=null) {if(count >= openContractCount ) isActive = true;}
                 else if(!bIsMultipleOpp && count >= 1) isActive = true;
             }*/
            isActive = LeadContInterface.hasOpenContracts(ContactID, bIsMultipleOpp, openContractCount );
        }
        system.debug('=======islessthan30-->' + isLessThan30 + '====isActive-->' + isActive + '====declinedLessXDaysAgo-->' + declinedLessXDaysAgo);
        return (!isLessThan30 && !isActive && !declinedLessXDaysAgo);
 
    }
 
    //Method to change the status of previous applications to Aged / Cancellation conditionally.
    public static void changeAppstatus(List<Opportunity> lstApp) {
        boolean bIsSandBox = [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
        List<Opportunity> statusApps = new List<Opportunity>();
        for (Opportunity EZapp : lstApp) {
            //#3187 start update status to Aged Cancellation of previous applications which are not funded
            if (EZapp.Status__c != ('Funded') && EZapp.Status__c != ('Aged / Cancellation') && EZapp.Status__c != 'Declined (or Unqualified)') {
                EZapp.Status__c = 'Aged / Cancellation';
                EZapp.Reason_of_Opportunity_Status__c = 'Different Product Application';
                if (bIsSandbox) EZapp.Decline_Note__c = 'Different Product Application';
                if (String.isNotEmpty(EZapp.Items_Requested__c) ) EZapp.Items_Requested__c = null;
                statusApps.add(EZapp);
            }
        }
        system.debug('==statusApps==' + statusApps);
        if (!statusApps.IsEmpty()) { update statusApps; }
    }
 
    /*
       * CheckExistanceResult exists because the method checkExistanceByContact returns false, even
       * when apps exist for that contact.
       * This structure allows to return both the boolean and the list of apps without changing the
       * existing logic in the code.
    */
    public class CheckExistanceResult {
        public Boolean appExists;
        public List<Opportunity> resultApps;
 
        public CheckExistanceResult(Boolean pExists, List<Opportunity> pLApps) {
            appExists = pExists;
            resultApps = pLApps;
        }
    }
    
    /**
    * Return the payment frequency Type for the offers (30 days, 28 days).
    * @param  partner [description]
    * @return the payment frequency to use in the offers
    fci */ //API-335 
    public static String getPaymentFrequencyType(String accountFrequency) {         
        String result = '28 Days';

        if (String.isNotEmpty(accountFrequency)) {

            if (accountFrequency.equalsIgnoreCase('30 days') || accountFrequency.equalsIgnoreCase('Monthly') )
                result = 'Monthly';
            else 
                result = accountFrequency;       
        }
        
        return result;
    }
}