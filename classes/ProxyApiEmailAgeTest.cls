@isTest public class ProxyApiEmailAgeTest {

    @isTest static void get_Email_Age_Result(){
        ProxyApiUtil.settings = LibraryTest.fakeSettings();
        genesis__Applications__c app = LibraryTest.createApplicationTH();
        String result = ProxyApiEmailAge.getEmailAgeResult(LibraryTest.fakeInputDataToEmailAge(),app.Id);
        
        System.assertNotEquals(null, ProxyApiUtil.settings);
        System.assertNotEquals(null, result);
    }
    
    @isTest static void get_Email_Age_Result_Using_SeqId() {
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        genesis__Applications__c app = LibraryTest.createApplicationTH();
        
        GDSRequestParameter gdsRequestParameter = new GDSRequestParameter ();
        gdsRequestParameter.contactId = app.genesis__contact__c;
        gdsRequestParameter.channelId = 'Partner';
        gdsRequestParameter.sequenceId = '4';
        gdsRequestParameter.productId = 'Personal Loan';
        gdsRequestParameter.sessionId = 'abc-dhfj-wji-123-dsfd';
        gdsRequestParameter.executionType = '';
        
        String emailAgeInputData = JSON.serialize(gdsRequestParameter);
        
        ProxyApiEmailAge.getEmailAgeResultUsingSeqId(emailAgeInputData,app.Id);
        
    }
}