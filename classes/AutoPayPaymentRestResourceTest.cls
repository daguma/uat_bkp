@isTest
public class AutoPayPaymentRestResourceTest {
    
    public static loan__Bank_Account__c createBankRecord(loan__Loan_Account__c contract) {
        loan__Bank_Account__c bankAcct = new loan__Bank_Account__c();
        bankAcct.loan__Bank_Name__c = 'test bank';
        bankAcct.loan__Bank_Account_Number__c = '09876532452';
        bankAcct.loan__Routing_Number__c = '098765432';
        bankAcct.loan__Account_Type__c = 'Checking';
        bankAcct.loan__Contact__c = contract.loan__contact__c;
        bankAcct.From_CP__c = true;
        insert bankAcct;
        return bankAcct;
    }
    
    @isTest static void switch_payment_method_test() {
  
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Next_Installment_Date__c = date.today().adddays(10);
        contract.loan__ACH_Next_Debit_Date__c = date.today();
        contract.loan__ACH_Debit_Amount__c = 1000;
        contract.loan__ACH_Routing_Number__c = '12343212434';
        contract.loan__ACH_Account_Number__c = '123456789';
        update contract;
        
        loan__Bank_Account__c bankAcct = AutoPayPaymentRestResourceTest.createBankRecord(contract);
                   
        Map<String,String> request = new Map<String,String> ();
        request.put('contactId',contract.loan__contact__c);
        request.put('contractId',contract.ID);
        request.put('paymentType','ACH');
        request.put('cardNo','');
        request.put('accountId','');
        request.put('bankId',bankAcct.Id);
        
        String inputData = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize(request)));

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
        AutoPayPaymentRestResource.switchPaymentMethod(inputData);
        
        Map<String,String> requestOne = new Map<String,String> ();
        requestOne.put('contactId',contract.loan__contact__c);
        requestOne.put('contractId',contract.ID);
        requestOne.put('paymentType','Debit Card');
        requestOne.put('cardNo','4414');
        requestOne.put('accountId','asjdkit2381mskf');
        requestOne.put('bankId','');
        
        String inputDataOne = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize(requestOne)));
        
        AutoPayPaymentRestResource.switchPaymentMethod(inputDataOne);
    }
    
    @isTest static void switch_payment_method_testOne() {
        
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        
        Map<String,String> request = new Map<String,String> ();
        request.put('contractId',contract.ID);
        request.put('paymentType','ACH');
        request.put('cardNo','');
        request.put('accountId','');
        request.put('bankId','');
        
        String inputData = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize(request)));
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
        AutoPayPaymentRestResource.switchPaymentMethod(inputData);
        
        
        Map<String,String> requestOne = new Map<String,String> ();
        requestOne.put('paymentType','ACH');
        requestOne.put('cardNo','');
        requestOne.put('accountId','');
        requestOne.put('bankId','');
        
        String inputDataOne = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize(requestOne)));
        
        
        AutoPayPaymentRestResource.switchPaymentMethod(inputDataOne);
    }
    
    @isTest static void switch_payment_method_test_Two() {
  
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__Next_Installment_Date__c = date.today().adddays(10);
        contract.loan__ACH_Next_Debit_Date__c = date.today();
        contract.loan__ACH_Debit_Amount__c = 1000;
        contract.loan__ACH_Routing_Number__c = '12343212434';
        contract.loan__ACH_Account_Number__c = '123456789';
        update contract;
        
                   
        Map<String,String> request = new Map<String,String> ();
        request.put('contactId',contract.loan__contact__c);
        request.put('contractId',contract.ID);
        request.put('paymentType','ACH');
        request.put('cardNo','');
        request.put('accountId','');
        request.put('bankId','');
        
        String inputData = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize(request)));

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response= res;
        
        AutoPayPaymentRestResource.switchPaymentMethod(inputData);
        
        Map<String,String> requestOne = new Map<String,String> ();
        requestOne.put('contactId',contract.loan__contact__c);
        requestOne.put('contractId',contract.ID);
        requestOne.put('paymentType','Debit Card');
        requestOne.put('cardNo','');
        requestOne.put('accountId','');
        requestOne.put('bankId','');
        
        String inputDataOne = EncodingUtil.base64Encode(Blob.valueof(JSON.serialize(requestOne)));
        
        AutoPayPaymentRestResource.switchPaymentMethod(inputDataOne);
    }
    
    
    @isTest static void get_autoPay_account_details_test_DebitCard() {
  
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.Is_Debit_Card_On__c = true;
        contract.Debit_Card_Account_Id__c = '4414';
        update contract;
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/AutoPayPaymentRestResource/getAutoPayAccountDetails/';  
        req.addParameter('contactId', contract.loan__contact__c);
        req.addParameter('contractId', contract.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        AutoPayPaymentRestResource.getAutoPayAccountDetails();
        
    }
    
    @isTest static void get_autoPay_account_details_test_ACH() {
  
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        
        loan__Bank_Account__c bankAcct = AutoPayPaymentRestResourceTest.createBankRecord(contract);
        
        contract.loan__ACH_On__c = true;
        contract.loan__Borrower_ACH__c = bankAcct.Id;
        contract.loan__Next_Installment_Date__c = date.today().adddays(10);
        contract.loan__ACH_Next_Debit_Date__c = date.today().adddays(10);
        contract.loan__ACH_Debit_Amount__c = 1000;
        contract.loan__ACH_Routing_Number__c = '12343212434';
        contract.loan__ACH_Account_Number__c = '123456789';
        update contract;
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/AutoPayPaymentRestResource/getAutoPayAccountDetails/';  
        req.addParameter('contactId', contract.loan__contact__c);
        req.addParameter('contractId', contract.Id);

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        AutoPayPaymentRestResource.getAutoPayAccountDetails();
        
    }
    
    @isTest static void assigning_payment_method_values_test() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract = AutoPayPaymentRestResource.assigningPaymentMethodValues('1234567890','987654321','test' ,'Checking',null,false,null,date.today(),true,contract,'ACH',null);
        system.assertequals(contract.PaymentMethod__c,'ACH');
    } 
}