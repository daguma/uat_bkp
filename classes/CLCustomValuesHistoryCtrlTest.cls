@isTest
public class CLCustomValuesHistoryCtrlTest{

    @isTest static void validates_constructor() {
        Test.startTest();
        CLCustomValuesHistoryCtrl customValuesCtrl =
                new CLCustomValuesHistoryCtrl(new ApexPages.StandardController(LibraryTest.createContractTH()));
        customValuesCtrl.dateTimeFormat = 'MyDateTimeFormat';
        Test.stopTest();

        System.assertNotEquals(null, customValuesCtrl);
        System.assertNotEquals(null, customValuesCtrl.dateTimeFormat);
    }

    @isTest static void validates_get_user_history(){
        Test.startTest();
        loan__loan_account__c myContract = LibraryTest.createContractTH();
        loan__loan_account__c myContract2 = LibraryTest.createContractTH();
        Securitization_User_Approve__c secUserApprove1 =
                new Securitization_User_Approve__c(
                        accountId__c = myContract.Id,
                        UserId__c = [SELECT Id FROM User LIMIT 1].Id
                );
        insert secUserApprove1;
        Securitization_User_Approve__c secUserApprove2 =
                new Securitization_User_Approve__c(
                        accountId__c = myContract2.Id,
                        UserId__c = [SELECT Id FROM User LIMIT 1].Id
                );
        insert secUserApprove2;
        CLCustomValuesHistoryCtrl customValuesCtrl = new CLCustomValuesHistoryCtrl(new ApexPages.StandardController(myContract));
        Test.stopTest();

        System.assertNotEquals(null, customValuesCtrl.getUserHistory());
        System.assert(!customValuesCtrl.getUserHistory().isEmpty());
        System.assertEquals(1,customValuesCtrl.getUserHistory().size());
        System.assertNotEquals(null, customValuesCtrl.getUserHistory()[0].userName);
        System.assertNotEquals(null, customValuesCtrl.getUserHistory()[0].date_Time);
    }

    @isTest static void validates_get_asset_history(){
        Test.startTest();
        loan__loan_account__c myContract = LibraryTest.createContractTH();
        loan__loan_account__c myContract2 = LibraryTest.createContractTH();
        Asset_Sale_History__c assetSaleHis1 =
                new Asset_Sale_History__c(
                        CL_Contract__c = myContract.Id,
                        Field_Name__c = 'MyField1',
                        Prior_Value__c = 'PriorValue',
                        New_Value__c = 'NewValue',
                        Modified_by_User__c = [SELECT Id FROM User LIMIT 1].Id
                );
        insert assetSaleHis1;
        Asset_Sale_History__c assetSaleHis2 =
                new Asset_Sale_History__c(
                        CL_Contract__c = myContract2.Id,
                        Field_Name__c = 'MyField1_myContract2',
                        Prior_Value__c = 'PriorValue_myContract2',
                        New_Value__c = 'NewValue_myContract2',
                        Modified_by_User__c = [SELECT Id FROM User LIMIT 1].Id
                );
        insert assetSaleHis2;
        CLCustomValuesHistoryCtrl customValuesCtrl = new CLCustomValuesHistoryCtrl(new ApexPages.StandardController(myContract));
        Test.stopTest();

        System.assertNotEquals(null, customValuesCtrl.getAssetHistory());
        System.assert(!customValuesCtrl.getAssetHistory().isEmpty());
        System.assertEquals(1, customValuesCtrl.getAssetHistory().size());
        System.assertEquals('MyField1', customValuesCtrl.getAssetHistory()[0].fieldName);
        System.assertEquals('PriorValue', customValuesCtrl.getAssetHistory()[0].priorValue);
        System.assertEquals('NewValue', customValuesCtrl.getAssetHistory()[0].newValue);
        System.assertNotEquals(null, customValuesCtrl.getAssetHistory()[0].userName);
        System.assertNotEquals(null, customValuesCtrl.getAssetHistory()[0].date_Time);
    }
}