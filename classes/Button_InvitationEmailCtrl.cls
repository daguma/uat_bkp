public with sharing class Button_InvitationEmailCtrl {

/**
* sendInvitationEmail: 
*  In order to send an invitation email to the customer, a RequestCode needs to be
*  delivered by DecisionLogic. 
*  This method calls the ProxyAPI to get the link with the valid RequestCode.
*  That link is saved in Application.invitation_Link__c and a Flow will take it from there.
*/
    @AuraEnabled
    public static InvitationEmailEntity sendInvitationEmail(Id oppId, String acctNum, String routingNum) {
        InvitationEmailEntity iee = new InvitationEmailEntity();

        try {

            Opportunity opp = [
                    SELECT
                            Id
                            ,invitation_Link__c
                            ,lead__c
                            ,contact__c
                            ,Contact__r.Name
                            ,contact__r.firstname
                            ,contact__r.lastname
                            ,OwnerId
                    FROM
                            Opportunity
                    WHERE Id = :oppId
            ];

            if (String.isEmpty(opp.invitation_Link__c)) {

                String customerIdentifier = opp.Lead__c;
                String firstName = opp.Contact__r.FirstName;
                String lastName = opp.Contact__r.LastName;
                String accountNumber = acctNum;
                String routingNumber = routingNum;
                String contentServiceId = '0';
                String applicationId = opp.id;
                if (customerIdentifier == null)
                    customerIdentifier = opp.contact__c;

                String standAloneURL = ProxyApiBankStatements.ApiGetStandAloneLink(
                        customerIdentifier == null ? '' : customerIdentifier
                        , firstName == null ? '' : firstName
                        , lastName == null ? '' : lastName
                        , accountNumber == null ? '' : accountNumber
                        , routingNumber == null ? '' : routingNumber
                        , contentServiceId == null ? '0' : contentServiceId
                        , applicationId == null ? '' : applicationId
                        , UserInfo.getUserId() == null ? '' : UserInfo.getUserId()
                );

                if (standAloneURL == null) {
                    iee.errorMessage = 'The invitation link for the email is not valid';
                } else {
                    if (standAloneURL.contains('Error')) {
                        iee.errorMessage = standAloneURL.replace('Error: Invalid request code -> Error: ', '');
                    } else {
                        opp.invitation_Link__c = standAloneURL;
                        iee.invitationLink = standAloneURL;
                        sendEmailAndAttachNote(opp);
                        iee.successMessage = 'The invitation email has been sent. A Note has been attached to this opportunity.';
                        update opp;
                    }
                }

            } else if (String.isNotEmpty(opp.invitation_Link__c)) {
                sendEmailAndAttachNote(opp);
                iee.invitationLink = opp.invitation_Link__c;
                iee.successMessage = 'The invitation email has been re-sent. A Note has been attached to this opportunity.';
            } else {
                iee.errorMessage = 'An error occurred while sending Invitation E-Mail.';
            }

        } catch (Exception e) {
            iee.errorMessage = 'Proxy API Exception, Please try again';
            system.debug('Button_InvitationEmailCtrl --> ' + e.getMessage() + ' --> --> --> ' + e.getStackTraceString());
            return iee;
        }

        return iee;
    }

    private static void sendEmailAndAttachNote(Opportunity opp) {

        EmailTemplate DLTemplate = [
                SELECT Id, Name, Subject, HtmlValue, Body
                FROM EmailTemplate
                WHERE Name = 'Decision Logic Email - Alpha'
                LIMIT 1
        ];

        User DLUser = [
                SELECT Id, Name, Title, Email, Phone
                FROM User
                WHERE Id = :opp.OwnerId
                LIMIT 1
        ];

        Map<String, String> keyValues = new Map<String, String>();

        keyValues.put('{!Opportunity.Customer_Name__c}', String.isNotEmpty(opp.Contact__r.Name) ? opp.Contact__r.Name : ' ');
        keyValues.put('{!Opportunity.Invitation_Link__c}', String.isNotEmpty(opp.Invitation_Link__c) ? opp.Invitation_Link__c : ' ');
        keyValues.put('{!Opportunity.OwnerFullName}', String.isNotEmpty(DLUser.Name) ? DLUser.Name : ' ');
        keyValues.put('{!Opportunity.OwnerTitle}', String.isNotEmpty(DLUser.Title) ? DLUser.Title : ' ');
        keyValues.put('{!Opportunity.OwnerPhone}', String.isNotEmpty(DLUser.Phone) ? DLUser.Phone : ' ');
        keyValues.put('{!Opportunity.OwnerEmail}', String.isNotEmpty(DLUser.Email) ? DLUser.Email : ' ');

        for (String key : keyValues.keySet()) {
            DLTemplate.HtmlValue = DLTemplate.HtmlValue.replace(key, keyValues.get(key));
        }

        SalesForceUtil.EmailByUserId(DLTemplate.Subject, DLTemplate.HtmlValue, opp.Contact__c);

        Note newNote = new Note();
        newNote.parentid = opp.Id;
        newNote.title = 'Invitation Email Sent';
        newNote.body = 'Invitation email sent to the customer.';
        insert newNote;
    }

    public class InvitationEmailEntity {

        @AuraEnabled public String invitationLink { get; set; }
        @AuraEnabled public Boolean hasError { get; set; }
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = true;
            }
        }
        @AuraEnabled public String successMessage {
            get;
            set {
                successMessage = value;
                hasError = false;
            }
        }

        public InvitationEmailEntity() {
            errorMessage = 'Empty response';
        }
    }
}