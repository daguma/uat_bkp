global class MaskSensitiveData implements SandboxPostCopy 
{
    global void runApexClass(SandboxContext context) 
    {
        QueueSortBatch b = new QueueSortBatch();
        Database.executeBatch(b);  
    }
}