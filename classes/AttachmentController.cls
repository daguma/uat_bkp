public class AttachmentController {

    @AuraEnabled
    public static List<Attachment> getAttachmentList(String parentId) {       
        
        List<Attachment> attachmentList = [SELECT Id, Name, Owner.Name, LastModifiedDate, ContentType, CreatedDate FROM Attachment 
                                           WHERE ParentId =: parentId ORDER BY CreatedDate];
        return attachmentList;
    }
    
    @AuraEnabled
    public static Integer getTotalCount(String parentId){
        Integer totalCount = [SELECT Count() FROM Attachment 
                                           WHERE ParentId =: parentId];
        return totalCount;
    } 
    
    @AuraEnabled
    public static void deleteAttachments(String ids){
        Delete [Select id from Attachment where  id =: ids];
    } 
}