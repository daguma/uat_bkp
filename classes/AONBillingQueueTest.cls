@isTest
private class AONBillingQueueTest {

    @isTest static void inserts_billing(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        loan__loan_Account__c testLoanAccount = null;
        loan__Loan_account_Due_Details__c bill = null;

        System.runAs(thisUser) {

            AON_Fee__c feeSetting = new AON_Fee__c();
            feeSetting.Name = 'AON Fee';
            feeSetting.Fee__c = 6.31;
            insert feeSetting;
            
            Opportunity opp = LibraryTest.createApplicationTH();
            opp.Term__c = 24;
            update opp;

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            testLoanAccount = LibraryTest.createContractTH();
            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            testLoanAccount.loan__Pmt_Amt_Cur__c = 200;
            update testLoanAccount;

            loan_AON_Information__c enrollment = new loan_AON_Information__c();
            enrollment.Contract__c = testLoanAccount.Id;
            enrollment.IsProcessed__c = false;
            enrollment.Product_Code__c = 'LPOPTION01';
            insert enrollment;
            
            bill = new loan__Loan_account_Due_Details__c();
            bill.loan__Loan_Account__c = testLoanAccount.Id;
            bill.loan__DD_Primary_Flag__c = true;
            bill.loan__Due_Amt__c = 200;
            bill.loan__Due_Date__c = Date.today();
            bill.loan__Due_Type_Description__c = 'BILL / DUE DATE';
            bill.loan__Due_Type__c = 1;
            bill.loan__Payment_Amt__c = 200;
            bill.loan__Payment_Date__c = Date.today();
            bill.loan__Payment_Satisfied__c = false;
            bill.loan__Tolerance__c = 10;
            bill.loan__Transaction_Date__c = Date.today();
            bill.loan__Compounding_Interest_Billed__c = 0;
            bill.loan__Regular_Interest_Billed__c = 0;
            bill.loan__Balance__c = 4000;
            bill.loan__Payoff_Balance__c = 4000;
            insert bill;
          
        }
        
        List<String> contractsList = new List<String>();
        //contractsList.add(testLoanAccount.Id);
        contractsList.add(bill.id);

        Integer billingsBefore = [SELECT COUNT() FROM loan_AON_Billing__c];
        
        AONBillingQueue.insertBillingRecord(contractsList);
        
        Integer billingsAfter = [SELECT COUNT() FROM loan_AON_Billing__c];
        
        System.assert(billingsAfter > billingsBefore,'Insert Billing');
    }
    
    /* 
     * To test Process Builder in Sandbox
	 */
    /* 
    @isTest static void inserts_billing_after_bill_satisfied(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        loan__loan_Account__c testLoanAccount = null;
        loan__Loan_account_Due_Details__c bill = null;

        System.runAs(thisUser) {

            AON_Fee__c feeSetting = new AON_Fee__c();
            feeSetting.Name = 'AON Fee';
            feeSetting.Fee__c = 6.31;
            insert feeSetting;
            
            Opportunity opp = LibraryTest.createApplicationTH();
            opp.Term__c = 24;
            opp.StrategyType__c = '2';
            update opp;

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            testLoanAccount = LibraryTest.createContractTH();
            testLoanAccount.loan__Disbursal_Amount__c = 200;
            testLoanAccount.loan__Disbursal_Date__c = Date.today().addDays(-1);
            testLoanAccount.loan__Disbursal_Status__c = 'Fully Disbursed';
            testLoanAccount.loan__Disbursed_Amount__c = 200;
            testLoanAccount.loan__loan_status__c = 'Active - Good Standing';
            testLoanAccount.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            testLoanAccount.loan__Pmt_Amt_Cur__c = 200;
            testLoanAccount.AON_Enrollment__c = true;
            update testLoanAccount;
          
            bill = new loan__Loan_account_Due_Details__c();
            bill.loan__Loan_Account__c = testLoanAccount.Id;
            bill.loan__DD_Primary_Flag__c = true;
            bill.loan__Due_Amt__c = 200;
            bill.loan__Due_Date__c = Date.today();
            bill.loan__Due_Type_Description__c = 'BILL / DUE DATE';
            bill.loan__Due_Type__c = 1;
            bill.loan__Payment_Amt__c = 200;
            bill.loan__Payment_Date__c = Date.today();
            bill.loan__Payment_Satisfied__c = false;
            bill.loan__Tolerance__c = 10;
            bill.loan__Transaction_Date__c = Date.today();
            bill.loan__Compounding_Interest_Billed__c = 0;
            bill.loan__Regular_Interest_Billed__c = 0;
            bill.loan__Balance__c = 4000;
            bill.loan__Payoff_Balance__c = 4000;
            insert bill;  
        }

        Integer billingsBefore = [SELECT COUNT() FROM loan_AON_Billing__c];
        
        Test.startTest();
        
        loan__Loan_account_Due_Details__c updBill = [SELECT Id, Name FROM loan__Loan_account_Due_Details__c WHERE Id =: bill.Id LIMIT 1];
        updBill.loan__Payment_Satisfied__c = true;
        update updBill;
        
        Test.stopTest();
        
        Integer billingsAfter = [SELECT COUNT() FROM loan_AON_Billing__c];
        
        System.assert(billingsAfter > billingsBefore,'Update Bill to Satisfied');
    }
    */
}