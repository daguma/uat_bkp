public with sharing class ModifiedReasonExt {

    public loan__Loan_Account__c contract {get; set;}
    public String status {get; set;}
    public string RecId;

    public ModifiedReasonExt(ApexPages.StandardController controller) {

        this.contract = (loan__Loan_Account__c)controller.getRecord();
        RecId = ApexPages.currentPage().getParameters().get('id');

        if (RecId <> null && RecId <> '') {
            this.contract = [SELECT id, Modification_Type__c,
                             Modification_Reason__c,
                             Modification_Reason_Text_Line__c,
                             Modification_Made__c,
                             Agreement_Modified_Date__c
                             FROM loan__Loan_Account__c
                             WHERE id = : RecId];
        }

        status = 'NotNull';
    }

    /**
     * [saveReason description]
     */
    public void saveReason() {

        if (this.contract <> null) {

            status = 'Success';

            if (!String.isEmpty(this.contract.Modification_Reason__c) &&
                    this.contract.Modification_Reason__c.equalsIgnoreCase('Other') &&
                    String.isEmpty(this.contract.Modification_Reason_Text_Line__c)) {
                status = 'Error';
                createMessage(ApexPages.Severity.Error, 'You have to add a description if you select \'Other\' as a modification reason.');
                return;
            } else if (!String.isEmpty(this.contract.Modification_Reason__c) &&
                       !this.contract.Modification_Reason__c.equalsIgnoreCase('Other')) {
                this.contract.Modification_Reason_Text_Line__c = null;
            }

            if (status.equalsIgnoreCase('Success')) {
                this.contract.Agreement_Modified_Date__c = system.Now();
                update this.contract;
                insertHistoryRecord();
            }
        }
    }

    @testVisible
    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }

    private void insertHistoryRecord(){
        try{
            insert new LoanModificationHistory__c(Contract__c = this.contract.Id,
                                                  Agreement_Modified_Date__c = date.newinstance(system.Now().year(), system.Now().month(), system.Now().day()), //this.contract.Agreement_Modified_Date__c,
                                                  Modification_Made__c = this.contract.Modification_Made__c,
                                                  Modification_Reason__c = this.contract.Modification_Reason__c,
                                                  Modification_Type__c = this.contract.Modification_Type__c,
                                                  Modified_By__c = UserInfo.getUserId(),
                                                  Modification_Reason_Text_Line__c = this.contract.Modification_Reason_Text_Line__c
                                                  );
        }
        catch(exception ex){
            createMessage(ApexPages.Severity.Error, 'Error while inserting LoanModificationHistory__c (Class ModifiedReasonExt, line 48)::-> ' + ex);
        }
    }
}