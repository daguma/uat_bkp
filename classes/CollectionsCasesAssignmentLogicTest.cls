@isTest
public class CollectionsCasesAssignmentLogicTest {
    static testmethod void validate_analyzePaymentReturnedOr1stMissed() {

        Test.startTest();

        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        
        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id;
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R02 - Account Closed';

        CollectionsCasesAssignmentEntity entity = new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        
        entity.hasCase = true;
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_2, entity.assignedScenario);

        entity.isLMS = true;
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure,entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_1, entity.assignedScenario);

        entity.dpd = 31;
        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;//LMS Collector
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure,entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_3_1, entity.assignedScenario);

        entity.dpd = 29;
        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;//LMS Collector
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure,entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_3_2, entity.assignedScenario);

        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.Customer_Service_Group_Id LIMIT 1].UserOrGroupId;//non LMS Collector
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure,entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_4, entity.assignedScenario);

        entity.isLMS = false;
        entity.dpd = 34;//hits dpd trigger
        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId;//member of Collections1To29
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure,entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_5, entity.assignedScenario);

        entity.dpd = 29;//misses dpd trigger
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure,entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_6, entity.assignedScenario);

        entity.caseId = null;
        entity.caseOwnerId = null;
        entity.caseStatus = null;
        entity.hasCase = false;
        entity.isLMS = true;
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure,entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_7, entity.assignedScenario);

        entity.isLMS = false;
        entity = CollectionsCasesAssignmentLogic.analyzePaymentReturnedOr1stMissed(usersStructure,entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_PAYMENT_RETURNED_8, entity.assignedScenario);
 
        Test.stopTest();
    }

    static testmethod void validate_analyzeZeroDPD() {

        Test.startTest();
        
        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id;
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R02 - Account Closed';

        CollectionsCasesAssignmentEntity entity = new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        entity.dpd = 0;
        entity.hasActivePaymentPlan = true;
        entity.hasCase = true;
        entity.caseOwnerId = Label.HAL_Id;
        CollectionsCasesAssignmentEntity result = CollectionsCasesAssignmentLogic.analyzeZeroDPD(entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_ZERO_DPD_1, entity.assignedScenario);

        User u = [SELECT Id, Name FROM User WHERE Name = 'Daniel Almazan' LIMIT 1];
        entity.caseOwnerId = u.Id;
        entity = CollectionsCasesAssignmentLogic.analyzeZeroDPD(entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_ZERO_DPD_2, entity.assignedScenario);

        entity.hasCase = false;
        entity = CollectionsCasesAssignmentLogic.analyzeZeroDPD(entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_ZERO_DPD_3, entity.assignedScenario);

        entity.hasActivePaymentPlan = false;
        entity.hasCase = true;
        entity = CollectionsCasesAssignmentLogic.analyzeZeroDPD(entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_ZERO_DPD_4, entity.assignedScenario);

        entity.hasCase = false;
        entity = CollectionsCasesAssignmentLogic.analyzeZeroDPD(entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_ZERO_DPD_5, entity.assignedScenario);

        Test.stopTest();
    }

    static testmethod void validate_analyzeDMC() {

        Test.startTest();

        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        
        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id;
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R02 - Account Closed';

        CollectionsCasesAssignmentEntity entity = new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        //User u = [SELECT Id, Name FROM User WHERE Name = 'Tiffany Buchanan' LIMIT 1];
        //Id u = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId;
        entity.isDMC = true;
        entity.hasCase = true;
        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsDMCLevel3 LIMIT 1].UserOrGroupId;
        entity.dpd = 34;
        entity = CollectionsCasesAssignmentLogic.analyzeDMC(usersStructure, entity);
        //System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DMC_1, entity.assignedScenario);

        entity.dpd = 3;
        entity = CollectionsCasesAssignmentLogic.analyzeDMC(usersStructure, entity);
        //System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DMC_2, entity.assignedScenario);

        entity.caseOwnerId = Label.HAL_Id;
        entity.dpd = 0;
        entity = CollectionsCasesAssignmentLogic.analyzeDMC(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DMC_3, entity.assignedScenario);

        entity.hasCase = false;
        entity = CollectionsCasesAssignmentLogic.analyzeDMC(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DMC_4, entity.assignedScenario);

        Test.stopTest();
    }

    static testmethod void validate_analyzeTriggersExceptions() {

        Test.startTest();

        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        
        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id;
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R02 - Account Closed';

        CollectionsCasesAssignmentEntity entity = new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        
        entity.dpd = 31;
        entity.hasActivePaymentPlan = true;
        CollectionsCasesAssignmentEntity result = CollectionsCasesAssignmentLogic.analyzeTriggersExceptions(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_TRIGGERS_EXCEPTIONS, entity.assignedScenario);

        Test.stopTest();
    }

    static testmethod void validate_analyzeDPD1TO5() {

        Test.startTest();

        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        
        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id;
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R02 - Account Closed';

        CollectionsCasesAssignmentEntity entity = new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );

        entity.dpd = 1;
        entity.hasOtACH = true;
        entity.hasCase = true;
        entity.caseOwnerId = Label.HAL_Id;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId;
        CollectionsCasesAssignmentEntity result = CollectionsCasesAssignmentLogic.analyzeDPD1TO5(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_1TO5_1, entity.assignedScenario);

        entity.otACHOwnerId = Label.HAL_Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD1TO5(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_1TO5_2, entity.assignedScenario);

        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD1TO5(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_1TO5_3, entity.assignedScenario);

        User u2 = [SELECT Id, Name FROM User WHERE Name = 'Daniel Almazan' LIMIT 1];
        entity.caseOwnerId = u2.Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD1TO5(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_1TO5_4, entity.assignedScenario);

        entity.hasCase = false;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD1TO5(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_1TO5_5, entity.assignedScenario);

        entity.otACHOwnerId = u2.Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD1TO5(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_1TO5_6, entity.assignedScenario);

        entity.hasOtACH = false;
        entity.hasCase = true;
        result = CollectionsCasesAssignmentLogic.analyzeDPD1TO5(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_1TO5_7, entity.assignedScenario);

        entity.hasCase = false;
        result = CollectionsCasesAssignmentLogic.analyzeDPD1TO5(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_1TO5_8, entity.assignedScenario);

        Test.stopTest();
    }

    static testmethod void validate_analyzeDPD6Plus() {

        Test.startTest();

        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();

        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id;
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R02 - Account Closed';

        CollectionsCasesAssignmentEntity entity = new CollectionsCasesAssignmentEntity(
                myContract,
                myCase,
                paymentPlanOwnerId,
                otAchOwnerId,
                lastReversal
            );

        entity.dpd = 7;
        entity.isLMS = true;
        entity.hasOtACH = true;
        entity.hasCase = true;
        entity.caseOwnerId = Label.HAL_Id;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        CollectionsCasesAssignmentEntity result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_1, entity.assignedScenario);

        entity.otACHOwnerId = Label.HAL_Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_2, entity.assignedScenario);

        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        entity.dpd = 32;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_3_1, entity.assignedScenario);

        entity.dpd = 7;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_3_2, entity.assignedScenario);

        User u2 = [SELECT Id, Name FROM User WHERE Name = 'Daniel Almazan' LIMIT 1];        
        entity.caseOwnerId = u2.Id;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_4, entity.assignedScenario);

        entity.otACHOwnerId = u2.Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_5, entity.assignedScenario);

        entity.hasCase = false;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_6, entity.assignedScenario);

        entity.otACHOwnerId = u2.Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_7, entity.assignedScenario);

        entity.hasOtACH = false;
        entity.hasCase = true;
        entity.caseOwnerId = null;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_8, entity.assignedScenario);

        entity.dpd = 31;
        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_9_1, entity.assignedScenario);

        entity.dpd = 7;
        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_9_2, entity.assignedScenario);

        entity.caseOwnerId = u2.Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_10, entity.assignedScenario);

        entity.hasCase = false;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_11, entity.assignedScenario);

        entity.isLMS = false;
        entity.hasOtACH = true;
        entity.hasCase = true;
        entity.caseOwnerId = Label.HAL_Id;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_12, entity.assignedScenario);

        entity.otACHOwnerId = Label.HAL_Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_13, entity.assignedScenario);

        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId;
        entity.dpd = 34;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_14, entity.assignedScenario);

        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        entity.dpd = 29;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_15, entity.assignedScenario);

        entity.caseOwnerId = u2.Id;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_16, entity.assignedScenario);

        entity.otACHOwnerId = Label.HAL_Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_17, entity.assignedScenario);
        
        entity.hasCase = false;
        entity.otACHOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_18, entity.assignedScenario);

        entity.otACHOwnerId = u2.Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_19, entity.assignedScenario);

        entity.hasOtACH = false;
        entity.hasCase = true;
        entity.caseOwnerId = null;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_20, entity.assignedScenario);

        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsPONLevel1 LIMIT 1].UserOrGroupId;
        entity.dpd = 34;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_21, entity.assignedScenario);

        entity.dpd = 7;
        entity.caseOwnerId = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: Label.CollectionsLevel1 LIMIT 1].UserOrGroupId;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_22, entity.assignedScenario);

        entity.caseOwnerId = u2.Id;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_23, entity.assignedScenario);

        entity.hasCase = false;
        result = CollectionsCasesAssignmentLogic.analyzeDPD6Plus(usersStructure, entity);
        System.assertEquals(CollectionsCasesAssignmentLogic.SCENARIO_DPD_6PLUS_24, entity.assignedScenario);

        Test.stopTest();
    }

    static testmethod void validate_analyzeAssignments() {

        Test.startTest();

        CollectionsCasesAssignmentUsersStructure usersStructure = new CollectionsCasesAssignmentUsersStructure();
        List<loan__loan_account__c> contractsList = new List<loan__loan_account__c>();
        
        loan__Loan_Account__c myContract1 = LibraryTest.createContractTH();
        Case myCase1 = new Case();
        myCase1.status = 'Promise Kept';
        insert myCase1;
        Id paymentPlanOwnerId1 = myCase1.owner.Id;
        Id otAchOwnerId1 = myCase1.owner.Id;
        String lastReversal1 = 'R02 - Account Closed';
        
        contractsList.add(myContract1);

        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id;
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = '';       
        //entity.dpd = 30;
        //entity.hasActivePaymentPlan = true;

        contractsList.add(myContract);

        CollectionsCasesAssignmentLogic.analyzeAssignments(contractsList, usersStructure, new CollectionsCasesAssignmentAudit(), new CollectionsCasesAssignmentHelper());

        List<CollectionsCasesAssignment__c> ccaList = [SELECT Id, AssignedScenario__c FROM CollectionsCasesAssignment__c];
        System.assertEquals(ccaList.size(), contractsList.size());

        Test.stopTest();
    }

}