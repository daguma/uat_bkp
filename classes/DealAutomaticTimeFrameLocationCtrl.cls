public class DealAutomaticTimeFrameLocationCtrl {

    public static final String[] LOCATION_GROUP_OPTIONS = new String[] {'Kennesaw Sales Center', 'DR Sales Center'};

    public integer mondayAssignmentFromTimeKennesaw { get; set; }
    public integer mondayAssignmentToTimeKennesaw { get; set; }
    public integer tuesdayAssignmentFromTimeKennesaw { get; set; }
    public integer tuesdayAssignmentToTimeKennesaw { get; set; }
    public integer wednesdayAssignmentFromTimeKennesaw { get; set; }
    public integer wednesdayAssignmentToTimeKennesaw { get; set; }
    public integer thursdayAssignmentFromTimeKennesaw { get; set; }
    public integer thursdayAssignmentToTimeKennesaw { get; set; }
    public integer fridayAssignmentFromTimeKennesaw { get; set; }
    public integer fridayAssignmentToTimeKennesaw { get; set; }
    public integer saturdayAssignmentFromTimeKennesaw { get; set; }
    public integer saturdayAssignmentToTimeKennesaw { get; set; }
    public integer sundayAssignmentFromTimeKennesaw { get; set; }
    public integer sundayAssignmentToTimeKennesaw { get; set; }

    public integer mondayAssignmentFromTimeDr { get; set; }
    public integer mondayAssignmentToTimeDr { get; set; }
    public integer tuesdayAssignmentFromTimeDr { get; set; }
    public integer tuesdayAssignmentToTimeDr { get; set; }
    public integer wednesdayAssignmentFromTimeDr { get; set; }
    public integer wednesdayAssignmentToTimeDr { get; set; }
    public integer thursdayAssignmentFromTimeDr { get; set; }
    public integer thursdayAssignmentToTimeDr { get; set; }
    public integer fridayAssignmentFromTimeDr { get; set; }
    public integer fridayAssignmentToTimeDr { get; set; }
    public integer saturdayAssignmentFromTimeDr { get; set; }
    public integer saturdayAssignmentToTimeDr { get; set; }
    public integer sundayAssignmentFromTimeDr { get; set; }
    public integer sundayAssignmentToTimeDr { get; set; }

    public List<Sales_Center_Login_Times__c> salesCenterLoginTimes { get; set; }

    public DealAutomaticTimeFrameLocationCtrl() {
        try {
            salesCenterLoginTimes = Sales_Center_Login_Times__c.getAll().values();

            for (Sales_Center_Login_Times__c loginTimes : salesCenterLoginTimes) {
                if (loginTimes.Sales_Center_Id__c == Label.Kennesaw_Group_Id) {
                    mondayAssignmentFromTimeKennesaw = Integer.valueOf(loginTimes.Monday_Time_From__c);
                    mondayAssignmentToTimeKennesaw = Integer.valueOf(loginTimes.Monday_Time_To__c);
                    tuesdayAssignmentFromTimeKennesaw = Integer.valueOf(loginTimes.Tuesday_Time_From__c);
                    tuesdayAssignmentToTimeKennesaw = Integer.valueOf(loginTimes.Tuesday_Time_To__c);
                    wednesdayAssignmentFromTimeKennesaw = Integer.valueOf(loginTimes.Wednesday_Time_From__c);
                    wednesdayAssignmentToTimeKennesaw = Integer.valueOf(loginTimes.Wednesday_Time_To__c);
                    thursdayAssignmentFromTimeKennesaw = Integer.valueOf(loginTimes.Thursday_Time_From__c);
                    thursdayAssignmentToTimeKennesaw = Integer.valueOf(loginTimes.Thursday_Time_To__c);
                    fridayAssignmentFromTimeKennesaw = Integer.valueOf(loginTimes.Friday_Time_From__c);
                    fridayAssignmentToTimeKennesaw = Integer.valueOf(loginTimes.Friday_Time_To__c);
                    saturdayAssignmentFromTimeKennesaw = Integer.valueOf(loginTimes.Saturday_Time_From__c);
                    saturdayAssignmentToTimeKennesaw = Integer.valueOf(loginTimes.Saturday_Time_To__c);
                    sundayAssignmentFromTimeKennesaw = Integer.valueOf(loginTimes.Sunday_Time_From__c);
                    sundayAssignmentToTimeKennesaw = Integer.valueOf(loginTimes.Sunday_Time_To__c);
                } else if (loginTimes.Sales_Center_Id__c == Label.DR_Group_Id) {
                    mondayAssignmentFromTimeDr = Integer.valueOf(loginTimes.Monday_Time_From__c);
                    mondayAssignmentToTimeDr = Integer.valueOf(loginTimes.Monday_Time_To__c);
                    tuesdayAssignmentFromTimeDr = Integer.valueOf(loginTimes.Tuesday_Time_From__c);
                    tuesdayAssignmentToTimeDr = Integer.valueOf(loginTimes.Tuesday_Time_To__c);
                    wednesdayAssignmentFromTimeDr = Integer.valueOf(loginTimes.Wednesday_Time_From__c);
                    wednesdayAssignmentToTimeDr = Integer.valueOf(loginTimes.Wednesday_Time_To__c);
                    thursdayAssignmentFromTimeDr = Integer.valueOf(loginTimes.Thursday_Time_From__c);
                    thursdayAssignmentToTimeDr = Integer.valueOf(loginTimes.Thursday_Time_To__c);
                    fridayAssignmentFromTimeDr = Integer.valueOf(loginTimes.Friday_Time_From__c);
                    fridayAssignmentToTimeDr = Integer.valueOf(loginTimes.Friday_Time_To__c);
                    saturdayAssignmentFromTimeDr = Integer.valueOf(loginTimes.Saturday_Time_From__c);
                    saturdayAssignmentToTimeDr = Integer.valueOf(loginTimes.Saturday_Time_To__c);
                    sundayAssignmentFromTimeDr = Integer.valueOf(loginTimes.Sunday_Time_From__c);
                    sundayAssignmentToTimeDr = Integer.valueOf(loginTimes.Sunday_Time_To__c);
                }
            }
        } catch (Exception e) {
            createMessage(ApexPages.Severity.Error, e.getMessage());
        }
    }

    public List<SelectOption> getAllFromTimeSlots() {
        List<SelectOption> options = new List<SelectOption>();
        for (integer index = 0; index <= 24; index++) {
            if (index == 0) {
                options.add(new SelectOption(String.valueOf(index), 'None'));
            } else if (index <= 10) {
                options.add(new SelectOption(String.valueOf(index), '0' + (index - 1) + ':00 EST'));
            } else {
                options.add(new SelectOption(String.valueOf(index), (index - 1) + ':00 EST'));
            }
        }
        return options;
    }

    public List<SelectOption> getAllToTimeSlots() {
        List<SelectOption> options = new List<SelectOption>();
        for (integer index = 0; index <= 25; index++) {
            if (index == 0) {
                options.add(new SelectOption(String.valueOf(index), 'None'));
            } else if (index != 1 && index <= 10) {
                options.add(new SelectOption(String.valueOf(index), '0' + (index - 1) + ':00 EST'));
            } else if (index != 1) {
                options.add(new SelectOption(String.valueOf(index), (index - 1) + ':00 EST'));
            }
        }
        return options;
    }

    public PageReference saveAutomaticAssignment() {
        try {
            PageReference pageRef = new PageReference('/apex/DealAutomaticTimeFrameLocation');

            if (!checkTimeRangeInSync(mondayAssignmentFromTimeKennesaw, mondayAssignmentToTimeKennesaw, mondayAssignmentFromTimeDr, mondayAssignmentToTimeDr)) {
                createMessage(ApexPages.Severity.Error, 'Error : Please verify the time range for Mondays.');
                return null;
            }
            if (!checkTimeRangeInSync(tuesdayAssignmentFromTimeKennesaw, tuesdayAssignmentToTimeKennesaw, tuesdayAssignmentFromTimeDr, tuesdayAssignmentToTimeDr)) {
                createMessage(ApexPages.Severity.Error, 'Error : Please verify the time range for Tuesdays.');
                return null;
            }
            if (!checkTimeRangeInSync(wednesdayAssignmentFromTimeKennesaw, wednesdayAssignmentToTimeKennesaw, wednesdayAssignmentFromTimeDr, wednesdayAssignmentToTimeDr)) {
                createMessage(ApexPages.Severity.Error, 'Error : Please verify the time range for Wednesdays.');
                return null;
            }
            if (!checkTimeRangeInSync(thursdayAssignmentFromTimeKennesaw, thursdayAssignmentToTimeKennesaw, thursdayAssignmentFromTimeDr, thursdayAssignmentToTimeDr)) {
                createMessage(ApexPages.Severity.Error, 'Error : Please verify the time range for Thursdays.');
                return null;
            }
            if (!checkTimeRangeInSync(fridayAssignmentFromTimeKennesaw, fridayAssignmentToTimeKennesaw, fridayAssignmentFromTimeDr, fridayAssignmentToTimeDr)) {
                createMessage(ApexPages.Severity.Error, 'Error : Please verify the time range for Fridays.');
                return null;
            }
            if (!checkTimeRangeInSync(saturdayAssignmentFromTimeKennesaw, saturdayAssignmentToTimeKennesaw, saturdayAssignmentFromTimeDr, saturdayAssignmentToTimeDr)) {
                createMessage(ApexPages.Severity.Error, 'Error : Please verify the time range for Saturdays.');
                return null;
            }
            if (!checkTimeRangeInSync(sundayAssignmentFromTimeKennesaw, sundayAssignmentToTimeKennesaw, sundayAssignmentFromTimeDr, sundayAssignmentToTimeDr)) {
                createMessage(ApexPages.Severity.Error, 'Error : Please verify the time range for Sundays.');
                return null;
            }

            salesCenterLoginTimes = Sales_Center_Login_Times__c.getAll().values();
            for (Sales_Center_Login_Times__c loginTimes : salesCenterLoginTimes) {
                if (loginTimes.Sales_Center_Id__c == Label.Kennesaw_Group_Id) {
                    loginTimes.Monday_Time_From__c = mondayAssignmentFromTimeKennesaw;
                    loginTimes.Monday_Time_To__c = mondayAssignmentToTimeKennesaw;
                    loginTimes.Tuesday_Time_From__c = tuesdayAssignmentFromTimeKennesaw;
                    loginTimes.Tuesday_Time_To__c = tuesdayAssignmentToTimeKennesaw;
                    loginTimes.Wednesday_Time_From__c = wednesdayAssignmentFromTimeKennesaw;
                    loginTimes.Wednesday_Time_To__c = wednesdayAssignmentToTimeKennesaw;
                    loginTimes.Thursday_Time_From__c = thursdayAssignmentFromTimeKennesaw;
                    loginTimes.Thursday_Time_To__c = thursdayAssignmentToTimeKennesaw;
                    loginTimes.Friday_Time_From__c = fridayAssignmentFromTimeKennesaw;
                    loginTimes.Friday_Time_To__c = fridayAssignmentToTimeKennesaw;
                    loginTimes.Saturday_Time_From__c = saturdayAssignmentFromTimeKennesaw;
                    loginTimes.Saturday_Time_To__c = saturdayAssignmentToTimeKennesaw;
                    loginTimes.Sunday_Time_From__c = sundayAssignmentFromTimeKennesaw;
                    loginTimes.Sunday_Time_To__c = sundayAssignmentToTimeKennesaw;
                } else if (loginTimes.Sales_Center_Id__c == Label.DR_Group_Id) {
                    loginTimes.Monday_Time_From__c = mondayAssignmentFromTimeDr;
                    loginTimes.Monday_Time_To__c = mondayAssignmentToTimeDr;
                    loginTimes.Tuesday_Time_From__c = tuesdayAssignmentFromTimeDr;
                    loginTimes.Tuesday_Time_To__c = tuesdayAssignmentToTimeDr;
                    loginTimes.Wednesday_Time_From__c = wednesdayAssignmentFromTimeDr;
                    loginTimes.Wednesday_Time_To__c = wednesdayAssignmentToTimeDr;
                    loginTimes.Thursday_Time_From__c = thursdayAssignmentFromTimeDr;
                    loginTimes.Thursday_Time_To__c = thursdayAssignmentToTimeDr;
                    loginTimes.Friday_Time_From__c = fridayAssignmentFromTimeDr;
                    loginTimes.Friday_Time_To__c = fridayAssignmentToTimeDr;
                    loginTimes.Saturday_Time_From__c = saturdayAssignmentFromTimeDr;
                    loginTimes.Saturday_Time_To__c = saturdayAssignmentToTimeDr;
                    loginTimes.Sunday_Time_From__c = sundayAssignmentFromTimeDr;
                    loginTimes.Sunday_Time_To__c = sundayAssignmentToTimeDr;
                }
            }
            update salesCenterLoginTimes;


            pageRef.setRedirect(true);
            return pageRef;
        } catch (Exception e) {
            createMessage(ApexPages.Severity.Error, e.getMessage());
            return null;
        }
    }

    @TestVisible
    private boolean checkTimeRangeInSync(integer formerTimeFrom, integer formerTimeTo, integer laterTimeFrom, integer laterTimeTo) {
        boolean timeRangeCheckFormer = checkTimeRange(formerTimeFrom, formerTimeTo);
        boolean timeRangeCheckLater = checkTimeRange(laterTimeFrom, laterTimeTo);
        if (timeRangeCheckFormer && timeRangeCheckLater) {
            if ((formerTimeFrom == 0 && formerTimeTo == 0) || (laterTimeFrom == 0 && laterTimeTo == 0)) {
                return true;
            } else if (formerTimeFrom < laterTimeFrom && formerTimeTo <= laterTimeFrom) {
                return true;
            } else if (formerTimeTo > laterTimeTo && formerTimeFrom >= laterTimeTo) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @TestVisible
    private boolean checkTimeRange(integer timeFromValue, integer timeToValue) {
        if (timeFromValue == 0 && timeToValue == 0) {
            return true;
        } else if (timeFromValue == 0 || timeToValue == 0) {
            return false;
        } else if (timeFromValue < timeToValue) {
            return true;
        } else {
            return false;
        }
    }

    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }
}