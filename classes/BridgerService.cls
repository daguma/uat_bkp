Global Class BridgerService {
    
    Webservice   static string verifyBridger(String BridgerRequest){ 
        string contactid; 
        string leadid; 
        boolean result = false;
        string jsonResponse;
        JSONGenerator gen;
        string Accept;
        
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        
        try{  
            Map<string,string> PersonInfoMap = (Map<String,String>) JSON.deserialize(BridgerRequest, Map<String,String>.class);                                            
            if(PersonInfoMap.ContainsKey('contactId')){contactid = (PersonInfoMap.get('contactId') <> null) ?PersonInfoMap.get('contactId'): null;}    
            if(PersonInfoMap.ContainsKey('leadid')){leadid = (PersonInfoMap.get('leadid') <> null) ?PersonInfoMap.get('leadid'): null;}    
            
            if(contactid == null || contactid == ''){            
                 Contactid = [select ConvertedContactId from Lead where id=:leadid].ConvertedContactId;
            }
                                    
            result = SubmitApplicationService.doBridgerContactHit(contactid);
             
            Accept =  (result)? 'Y':'N';                           
            
            gen.writeStringField('acceptance',Accept); 
            gen.writeStringField('status','200');  
            gen.writeStringField('message','success');                                       
            system.debug('===========genTry============='+gen);
        
        }catch (Exception e) {                   
            gen.writeStringField('acceptance','N'); 
            gen.writeStringField('status','error');  
            gen.writeStringField('message',e.getMessage());     
             
            system.debug('===========genCatch============='+gen);                                                
         }
         
         gen.writeEndObject();
         jsonResponse = gen.getAsString(); 
         system.debug('===========jsonResponse============='+jsonResponse); 
         return jsonResponse;       
    }
}