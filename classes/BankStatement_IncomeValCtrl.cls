public with sharing class BankStatement_IncomeValCtrl {

    @AuraEnabled
    public static IncmValCtrlResult updateFields(Id oppId) {

        IncmValCtrlResult ivcr = new IncmValCtrlResult();

        try {
            ivcr.opportunity = [
                    SELECT id
                            , Contact__r.Annual_Income__c
                            , Gross_Percentage_To_Declared__c
                            , Estimated_Gross_Income__c
                            , Decision_Logic_Result__c
                            , Clarity_Verified_Income__c
                            , Income_Verification_Source__c
                            , Decision_Logic_Income__c
                    FROM Opportunity
                    WHERE id = :oppId
                    LIMIT 1
            ];
        } catch (Exception e) {
            ivcr.errorMessage = e.getMessage() + ' - '+ e.getStackTraceString();
        }
        return ivcr;
    }

    public class IncmValCtrlResult {

        @AuraEnabled public boolean hasError;
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = true;
            }
        }
        @AuraEnabled public Opportunity opportunity {
            get;
            set {
                opportunity = value;
                hasError = false;
            }
        }

        public IncmValCtrlResult() {
            hasError = false;
        }

    }
}