public with sharing class alphaHelperCls {

    public static Map<String, String> getOpportunityFieldLabels() {

        String type = 'Opportunity';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Map<String, String> fieldsLabel = new Map<String, String>();

        for (String fieldName : fieldMap.keySet()) {
            fieldsLabel.put(fieldName, fieldMap.get(fieldname).getDescribe().getLabel());
        }
        //system.debug('Print Opportunity fieldsLabel==== ' + fieldsLabel);
        return fieldsLabel;
    }

    public static Map<String, String> getContactFieldLabels() {

        String type = 'Contact';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Map<String, String> fieldsLabel = new Map<String, String>();

        for (String fieldName : fieldMap.keySet()) {
            fieldsLabel.put(fieldName, fieldMap.get(fieldname).getDescribe().getLabel());
        }
        //system.debug('Print Contact fieldsLabel==== ' + fieldsLabel);
        return fieldsLabel;
    }

    public static List<String> fetchPicklistValues(sObject objObject, string fld) {
        List <String> allOpts = new list <String>();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();

        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();

        // Get a map of fields for the SObject
        map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();

        // Get the list of picklist values for this field.
        list <Schema.PicklistEntry> values =
                fieldMap.get(fld).getDescribe().getPickListValues();

        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a : values) {
            allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
    }

    public static Opportunity getOpportunityDetails(Id opportunityId) {
        Opportunity theOpportunity = [
                                    SELECT
                                    Id 
                                    , Contact__c
                                    , All_Applicants_XML__c
                                    , Employed_Applicants_1_XML__c
                                    , Employed_Applicants_2_XML__c
                                    , Retired_Applicants_XML__c
                                    , Self_Employed_Applicants_1_XML__c
                                    , Self_Employed_Applicants_2_XML__c
                                    , Bank_Statements_Information_XML__c
                                    , Miscellaneous_Applicants_XML__c
                                    , FS_Assigned_To_Val__c
                                    //, UW_Assigned_To_Val__c
                                    , Contact__r.Employment_Type__c
                                    , Contact__r.Second_Income_Type__c
                                    , Contact__r.LeadSource
                                    , Contact__r.Lead_Sub_Source__c
                                    , Payment_Frequency_Multiplier__c
                                    , Estimated_Grand_Total__c
                                    , Is_No_POI_Required__c
                                    , ACH_Account_Type__c
                                    , DTI__c
                                    , FICO__c
                                    , Was_phone_no_verified__c
                                    , Contact__r.Request_Source__c
                                    , Contact__r.Annual_Income__c
                                    , Contact__r.Show_Regrade_Warning__c
                                    , Contract_Signed_Date__c
                                    , Contract_Received_Through__c
                                    , (Select OldValue, NewValue, createddate From Histories where field = 'Status__c' ORDER BY CreatedDate ASC)
                                    , Is_Signed_Contract_Overridden__c
                                    , FT_Status__c
                                    , Lending_Account__c
                                    , Contact__r.MailingState
                                    , Payment_Frequency__c
                                    , Payment_Amount__c
                                    , Offer_Selected_Date__c
                                    , Fico_Reviewed__c
                                    , CreatedDate
                                    , State__c
                                    , Status__c
                                    , Maximum_Payment_Amount__c
                                    , CSI_Watch_List_Approved__c
                                    , Contact__r.SSN__c
                                    , Contact__r.Email
                                    , Reviewed_and_Cleared__c
                                    , Hawk_Alert_Codes__c
                                    , Fraud_Alert_Accepted__c
                                    , Is_Totals_Accepted__c
                                    , Reason_of_Opportunity_Status__c
                                    , DL_Account_Number_Found__c
                                    , Average_Daily_Balance_Amount__c
                                    , Current_Balance_Amount__c
                                    , Number_of_Negative_Days_Number__c
                                    , Total_Deposits_Amount__c
                                    , Type
                                    , is_Finwise__c
                                    , Idology_Accepted__c
                                    , Is_SSNRepMoreFreqforAnother_Overridden__c
                                    , InqCurrAddOnFile_Overridden__c
                                    , BRMS_Rules_Overridden__c
                                    , Average_Daily_Balance_Meets_Tolerance__c
                                    , Average_Daily_Balance_Overridden__c
                                    , Clear_Overridden__c
                                    , Clear_Overridden_By__c
                                    , Clear_Tax_Liens_Judgments_Approved__c
                                    , InqCurrAddOnFile_Meet_Tolerance__c
                                    , InqCurrAddOnFile_Overridden_By__c
                                    , Is_SSNRepMoreFreqAnother_Tolerance_Met__c
                                    , EWS_Call_Meets_Tolerance__c
                                    , EWS_Call_Overidden__c
                                    , SSNRepMoreFreqforAnother_Overridden_By__c
                                    , EWS_Call_Overridden_By__c
                                    , Average_Daily_Balance_Overridden_by__c
                                    , Overdrafts_Number__c
                                    , Overdrafts_Amount__c
                                    , Is_Overdrafts_Tolerance_Met__c
                                    , Overdrafts_Overridden__c
                                    , Overdrafts_Overridden_by__c
                                    , YTD_Overdrafts_Number__c
                                    , YTD_Overdrafts_Amount__c
                                    , Is_YTD_Overdrafts_Tolerance_Met__c
                                    , YTD_Overdrafts_Overridden__c
                                    , YTD_Overdrafts_Overridden_by__c
                                    , NSFs_Number__c
                                    , NSFs_Amount__c
                                    , Is_NSFs_Tolerance_Met__c
                                    , NSFs_Overridden__c
                                    , NSFs_Overridden_by__c
                                    , YTD_NSFs_Number__c
                                    , Is_YTD_NSFs_Tolerance_Met__c
                                    , YTD_NSFs_Overridden__c
                                    , YTD_NSFs_Overridden_by__c
                                    , Overdrafts_Plus_NFSs_Number__c
                                    , Overdrafts_Plus_NSFs_Amount__c
                                    , Is_Overdrafts_Plus_NSFs_Tolerance_Met__c
                                    , OverdraftsPlusNSF_Overridden__c
                                    , OverdraftsPlusNSF_Overridden_by__c
                                    , Is_No_Of_Negative_Days_Tolerance_Met__c
                                    , Is_No_Of_Negative_Days_Overridden__c
                                    , Number_Of_Negative_Days_Overridden_By__c
                                    , Is_Total_Deposits_Amount_Tolerance_Met__c
                                    , Is_Total_Deposits_Amount_Overridden__c
                                    , Total_Deposits_Amount_Overridden_By__c
                                    , Current_Balance_Meets_Tolerance__c
                                    , Current_Balance_Overridden__c
                                    , Current_Balance_Overridden_by__c
                                    , Recurrent_Deposits__c
                                    , Bank_Accepted__c
                                    , Employment_Verification_Meets_Tolerance__c
                                    , Employment_Verification_Overidden__c
                                    , Employment_Time_Meets_Tolerance__c
                                    , Employment_Overidden__c
                                    , Employment_Overridden_By__c
                                    , Employment_Time_at_Job_Accepted__c
                                    , DL_Time_Meets_Tolerance__c
                                    , DL_Overidden__c
                                    , DL_Overridden_By__c
                                    , DL_Accepted__c
                                    , Is_Signed_Contract_Tolerance_Met__c
                                    , Is_Signed_Contract_Overridden_Required__c
                                    , Is_Signed_Contract_Accepted__c
                                    , Signed_Contract_Overridden_By__c
                                    , Employment_Verification_Overridden_By__c
                                    , Is_Totals_Overridden__c
                                    , Estimated_Grand_Total_Meets_Tolerance__c
                                    , Totals_Overridden_By__c
                                    , Was_Application_Re_Graded__c
                                    , Re_Grated_Income_in_Contacts__c
                                    , Re_Graded_By__c
                                    , DTI_Meets_Tolerance__c
                                    , Is_PTI_Tolerance_Met__c
                                    , Is_Maximum_Payment_Meets_Tolerance__c
                                    , Is_Maximum_Payment_Overridden__c
                                    , Maximum_Payment_Overridden_By__c
                                    , Is_Offer_Payment_Amount_Tolerance_Met__c
                                    , Offer_Payment_Amount_Overridden__c
                                    , Offer_Payment_Amount_Overridden_by__c
                                    , Previous_Grade__c
                                    , Is_Scorecard_Grade_Tolerance_Met__c
                                    , Scorecard_Grade_Overridden__c
                                    , Scorecard_Grade_Overridden_by__c
                                    , Manual_Grade__c
                                    , Manual_Grade_Meets_Tolerance__c
                                    , Is_Manual_Grade_Overridden__c
                                    , Manual_Grade_Overridden_By__c
                                    , BRMS_Rules_Overridden_By__c
                                    , BRMS_Rules_Meets_Tolerance__c
                                    , Deal_is_Ready_to_Fund__c
                                    , Has_More_Than_1_Job__c
                                    , Is_Self_Employed_More_Than_1_Job__c
                                    , YTD_NSFs_Amount__c
                                    , Clear_Accepted__c,Direct_Pay_Banner__c,Disbursal_Request_Received__c
                                    FROM Opportunity
                                    WHERE id = :opportunityId
        ];
        return theOpportunity;
    }

    /*
    * Returns a list of PickListItem, with the isSelected option set accordingly if key is found.
    *
    * @param  sdfr  It contains the Schema.DescribeFieldResult for the specific field
    * @param  searchString  It contains the string that should match a key in the PickListItem to set as isSelected.
    * @param  addNoneOption  It defines if the --None-- option should be included in the picklist values or not.
    * @return a list of PickListItem
    */
    public static List<PicklistItem> getPicklistItems(Schema.DescribeFieldResult sdfr, String searchString, Boolean addNoneOption) {

        List<PicklistItem> aatList = new List<PicklistItem>();
        Schema.DescribeFieldResult fieldResult = sdfr;
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        Boolean picklistOptionSelected = false;

        if (ple != null)
            for (Schema.PicklistEntry f : ple) {
                aatList.add(
                        new PicklistItem(
                                f.getLabel(),
                                f.getValue(),
                                searchString != null
                                        ? searchString.equalsIgnoreCase(f.getValue())
                                        : false
                        )
                );
                if(picklistOptionSelected == false)
                    picklistOptionSelected = searchString != null ? searchString.equalsIgnoreCase(f.getValue()) : false;
            }

        if(addNoneOption)
            aatList.add(new PicklistItem('--None--', '--None--', !picklistOptionSelected));
        return aatList;
    }

    /*
    * PickListItem class
    *     Since Lightning does not support SelectOption for picklist fields, this structure might come handy.
    *
    * @attribute label It refers to the label in the picklist options.
    * @attribute value It refers to the value in the picklist options.
    * @attribute isSelected It refers to the selected option in the picklist.
    *
    */
    public class PicklistItem {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public Boolean isSelected;

        public PicklistItem(String pLabel, String pValue) {
            label = pLabel;
            value = pValue;
            isSelected = false;
        }

        public PicklistItem(String pLabel, String pValue, Boolean pIsSelected) {
            label = pLabel;
            value = pValue;
            isSelected = pIsSelected;
        }
    }

}