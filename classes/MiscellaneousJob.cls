global class MiscellaneousJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {
	
	global final String query;
	global final String option;
	global final Integer minutesFromNow;


	global MiscellaneousJob(String option, Integer minutesFromNow) {
		this.option = option;
		this.minutesFromNow = minutesFromNow;

		if (option == 'Bridger') {
			this.query = BridgerSFLookup.ELIGIBILITY_BRIDGER_QUERY;
		}

	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator(this.query);
	}

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

    	if (this.option == 'Bridger'){
    		for (sobject s : scope) {
    			BridgerSFLookup.bridgerNewCall((BridgerWDSFLookup__c) s);
    		}
    	}
	}
	
	global void finish(Database.BatchableContext BC) {
		MiscellaneousJob job = new MiscellaneousJob(this.option, this.minutesFromNow);
		System.scheduleBatch(job, 'MiscellaneousJob_' + this.option, this.minutesFromNow, 1);  //Each 1 minute, process batches of 1 by 1
	}

	global void execute(SchedulableContext sc) {

	}
	
}

/* To run:
MiscellaneousJob job = new MiscellaneousJob('Bridger', 20);
database.executebatch(job, 1);
*/