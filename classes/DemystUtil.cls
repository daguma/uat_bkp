public with sharing class DemystUtil{
    
    //************************
    //**Demyst Service Call
    //************************
    public static string DemystService(string PersonalInfoString){
        
        //variables declaration
        Map<string,string> ResponseMap = new Map<string,string>();   
        Contact c;
        Lead l;
        string leadId, contactId, phon, email, FN, LN, Street, City, State, ZipVar, Country;
        string income, employer, jobtitle, EmailAcceptanceVar,IncomeAcceptanceVar;
        
        try{  
            system.debug('===PersonalInfoString==='+PersonalInfoString);          
            Map<string,string> PersonInfoMap = (Map<String,String>) JSON.deserialize(PersonalInfoString, Map<String,String>.class);                        
            system.debug('===PersonInfoMap==='+PersonInfoMap);        
            if(PersonInfoMap.ContainsKey('leadId')){leadId = (PersonInfoMap.get('leadId') <> null) ?EncodingUtil.urlEncode(PersonInfoMap.get('leadId'), 'UTF-8'): null;}
            if(PersonInfoMap.ContainsKey('contactId')){contactId = (PersonInfoMap.get('contactId') <> null) ?EncodingUtil.urlEncode(PersonInfoMap.get('contactId'), 'UTF-8'): null;}
            
            
            if(String.isNotEmpty(leadId) ){
                l = [select Phone,id,Factor_Trust_Verification__c,Demyst_Income_Verification__c,Demyst_Employer_Verification__c,Demyst_Email_Verification__c,Demyst_Email_Acceptance__c,Factor_Trust_Acceptance__c,Demyst_Employer_Acceptance__c,Demyst_Income_Acceptance__c, firstName, lastName, Street,State,City,IsConverted,PostalCode,Country,Email, Annual_Income__c, Employer_Name__c, Title from Lead where id=: leadId];
                phon = l.Phone;                
                FN = (l.firstName <> null) ? EncodingUtil.urlEncode(l.firstName, 'UTF-8') : '' ;
                LN = (l.lastName <> null) ? EncodingUtil.urlEncode(l.lastName, 'UTF-8') : '' ;
                Street = (l.Street <> null) ? EncodingUtil.urlEncode(l.Street, 'UTF-8') : '' ;
                City =  (l.City <> null) ? EncodingUtil.urlEncode(l.city, 'UTF-8') : '' ;
                State =  (l.State<> null) ? EncodingUtil.urlEncode(l.State, 'UTF-8') : '' ;         
                ZipVar =  (l.PostalCode <> null) ? EncodingUtil.urlEncode(l.PostalCode, 'UTF-8') : '' ;
                Country =  (l.Country <> null) ? EncodingUtil.urlEncode(l.Country, 'UTF-8') : 'US' ;
                Email =  (l.Email <> null) ? EncodingUtil.urlEncode(l.Email, 'UTF-8') : '' ;
                income =  (l.Annual_Income__c <> null) ? EncodingUtil.urlEncode(String.valueOf(l.Annual_Income__c), 'UTF-8') : '' ;
                employer =  (l.Employer_Name__c <> null) ? EncodingUtil.urlEncode(l.Employer_Name__c, 'UTF-8') : '' ;
                jobtitle =  (l.Title <> null) ? EncodingUtil.urlEncode(l.Title, 'UTF-8') : '' ;
            }
            
            
            if(String.isNotEmpty(contactId) ){
                c  = [select Phone,id,Factor_Trust_Verification__c,Demyst_Income_Verification__c,Demyst_Employer_Verification__c,Demyst_Email_Verification__c,Factor_Trust_Acceptance__c,Demyst_Email_Acceptance__c,Demyst_Employer_Acceptance__c,Demyst_Income_Acceptance__c, firstName, lastName, MailingStreet,MailingState,MailingCity,MailingPostalCode,MailingCountry,Email, Annual_Income__c, Employer_Name__c, Title from Contact where id=: contactId];              
                phon = c.Phone;
                
                FN = (c.firstName <> null) ? EncodingUtil.urlEncode(c.firstName, 'UTF-8') : '' ;
                LN = (c.lastName <> null) ? EncodingUtil.urlEncode(c.lastName, 'UTF-8') : '' ;
                Street = (c.MailingStreet <> null) ? EncodingUtil.urlEncode(c.MailingStreet, 'UTF-8') : '' ;
                City =  (c.MailingCity <> null) ? EncodingUtil.urlEncode(c.MailingCity, 'UTF-8') : '' ;
                State =  (c.MailingState <> null) ? EncodingUtil.urlEncode(c.MailingState, 'UTF-8') : '' ;         
                ZipVar =  (c.MailingPostalCode <> null) ? EncodingUtil.urlEncode(c.MailingPostalCode, 'UTF-8') : '' ;
                Country =  (c.MailingCountry <> null) ? EncodingUtil.urlEncode(c.MailingCountry, 'UTF-8') : 'US' ;
                Email =  (c.Email <> null) ? EncodingUtil.urlEncode(c.Email, 'UTF-8') : '' ;
                income =  (c.Annual_Income__c <> null) ? EncodingUtil.urlEncode(String.valueOf(c.Annual_Income__c), 'UTF-8') : '' ;
                employer =  (c.Employer_Name__c <> null) ? EncodingUtil.urlEncode(c.Employer_Name__c, 'UTF-8') : '' ;
                jobtitle =  (c.Title <> null) ? EncodingUtil.urlEncode(c.Title, 'UTF-8') : '' ;
                
            }        
            string APIKeyVal = Label.Demyst_Key, SubscriptionIdVal = Label.Demyst_Subscription_Id;
            String xmlString = '&email='+email+'&firstname='+FN+'&lastname='+LN+'&street='+Street+'&city='+City+'&state='+State+'&postcode='+ZipVar+'&country='+Country+'&max_wait=2'+'&Testing='+Label.Demyst_Testing+'&phone='+phon;
            xmlString += '&income='+income+'&employer='+employer+'&jobtitle='+jobtitle+'&__trace_timing=1'; 
            
            
            HTTPRequest Req = new httpRequest();
            Req.SetMethod('GET');                       
            Req.SetEndPoint('https://demystdata.com/engine/get?apikey='+APIKeyVal+'&subscription='+SubscriptionIdVal+xmlString);               
            Req.setTimeout(120000);
            http httpProtocol = new http();
            HTTPResponse Resp;
            
            
            if(!Test.isRunningTest()){
                Resp = httpProtocol.send(Req); 
            }else{
                Resp = new HTTPResponse(); 
                string dummyBody = '{"profile":{"websearch_fraud_hit_v2":"0","income_inconsistency_with_neighborhood":"0","income_inconsistency_with_model43":"0","invalid_email_deluxe":"1","email_name_inconsistent":"0","email_address_age_less_than_six_months":"unknown","email_address_age_less_than_one_month":"unknown","email_address_age_less_than_one_week":"unknown","sanctionrisk":"0","red_flags_deluxe":2,"name_address_nohit_v3":"1","address_inconsistent":"0","internal_id":"bb70f8e2-d77b-11e4-9d64-4b0f6230a399","income_inconsistency_with_model26":"0","client_id":"3R25l1621393921J","email_age_less_than_one_month":"0"}}';             
                Resp.SetBody(dummyBody);            
            }
            
            system.debug('====resp===================='+Resp.getbody());
            
            
            JSONParser parser = JSON.createParser(Resp.getBody()); 
            string ParseRes= DemystUtil.DemystParsingLogic(parser);
            system.debug('===ParseRes==='+ParseRes);
            
            ResponseMap= (Map<String,String>) JSON.deserialize(ParseRes, Map<String,String>.class); 
            system.debug('===ResponseMap===='+ResponseMap);
            ResponseMap.put('response',Resp.getbody());
            ResponseMap.put('request',String.ValueOf(Req));
            system.debug('==ResponseMap============'+ResponseMap);
            
        }catch(Exception Exc){  
            system.debug('===Exception==='+Exc.getmessage()); 
        }  
        return JSON.SerializePretty(ResponseMap);
    }
    
    
    //*****************************
    //*To Parse Demyst response
    //*****************************
    public static string DemystParsingLogic(JSONParser parser){
        
        string EmailAcceptanceVar = 'N',IncomeAcceptanceVar = 'N',EmpAcceptanceVar,AcceptanceVar, ReturnVal = 'Y';
        decimal FraudVal, RiskVal, RedflagVal, EmailAge;
        boolean InvalidEmail = false, Unknown = false;
        Map<string,string> ResponseMap = new Map<string,string>(); 
        string income_inconsistency_with_model26, income_inconsistency_with_model43, income_inconsistency_with_neighborhood;
        string websearch_fraud_hit_v2, sanctionrisk, red_flags_deluxe, email_age_less_than_one_month;
        
        try{
            decimal Model26, Model43, neighborhood; 
            system.debug('==parser=============='+parser);  
            while(parser.nextToken() <> null) {
                system.debug('==m in================');
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    if (((parser.getText() == 'exec_time') || (parser.getText() == 'sources_queried') || (parser.getText() == 'keyhash')))      break;
                    
                    
                    if ( parser.getText() == 'income_inconsistency_with_model26') {                   
                        parser.nextToken();                                        
                        if(parser.gettext() <> ''  && parser.gettext() <> 'unknown'){
                            if(parser.gettext().isNumeric() ){
                                Model26 = decimal.ValueOf(parser.gettext());  
                            }                                                                    
                        }else{ Unknown = true;}
                        
                        income_inconsistency_with_model26 = parser.gettext();                                     
                        ResponseMap.put('income_inconsistency_with_model26',income_inconsistency_with_model26);
                        
                    }
                    
                    if (parser.getText() == 'income_inconsistency_with_model43') {                   
                        parser.nextToken();                                       
                        if(parser.gettext() <> ''  && parser.gettext() <> 'unknown'){
                            if(parser.gettext().isNumeric() ){
                                Model43 = decimal.ValueOf(parser.gettext()); 
                            }                                                                   
                        }else{ Unknown = true;}
                        
                        income_inconsistency_with_model43 = parser.gettext();   
                        ResponseMap.put('income_inconsistency_with_model43',income_inconsistency_with_model43);                                 
                    }
                    
                    if (parser.getText() == 'income_inconsistency_with_neighborhood') {                   
                        parser.nextToken();                                        
                        if(parser.gettext() <> ''  && parser.gettext() <> 'unknown'){
                            if(parser.gettext().isNumeric() ){
                                neighborhood = decimal.ValueOf(parser.gettext());    
                            }                                                               
                        }else{ Unknown = true;}
                        
                        income_inconsistency_with_neighborhood = parser.gettext();
                        ResponseMap.put('income_inconsistency_with_neighborhood',income_inconsistency_with_neighborhood);    
                    }                                           
                    
                    
                    
                    
                    if (parser.getText() == 'websearch_fraud_hit_v2') {                   
                        parser.nextToken(); 
                        
                        if(parser.gettext() == 'invalid_email'){  InvalidEmail = true;
                            
                        }else if(parser.gettext() <> ''  && parser.gettext() <> 'unknown'){
                            if(parser.gettext().isNumeric() ){
                                FraudVal = decimal.ValueOf(parser.gettext());
                            }                                                                      
                        }
                        
                        websearch_fraud_hit_v2 = parser.gettext();  
                        ResponseMap.put('websearch_fraud_hit_v2',websearch_fraud_hit_v2);                                                                                           
                    }
                    
                    if (parser.getText() == 'sanctionrisk') {                   
                        parser.nextToken(); 
                        
                        if(parser.gettext() == 'invalid_email'){ InvalidEmail = true;
                            
                        }else if(parser.gettext() <> ''  && parser.gettext() <> 'unknown'){
                            if(parser.gettext().isNumeric() ){
                                RiskVal = decimal.ValueOf(parser.gettext());      
                            }                                                              
                        } 
                        
                        sanctionrisk = parser.gettext();    
                        ResponseMap.put('sanctionrisk',sanctionrisk);                                                                                           
                    }
                    
                    if (parser.getText() == 'red_flags_deluxe') {                   
                        parser.nextToken(); 
                        
                        if(parser.gettext() == 'invalid_email'){ InvalidEmail = true;
                            
                        }else if(parser.gettext() <> ''  && parser.gettext() <> 'unknown'){
                            if(parser.gettext().isNumeric() ){
                                RedflagVal = decimal.ValueOf(parser.gettext());  
                            }                                                                 
                        }
                        
                        red_flags_deluxe = parser.gettext();  
                        ResponseMap.put('red_flags_deluxe',red_flags_deluxe);                                    
                    }
                    
                    if (parser.getText() == 'email_age_less_than_one_month') {                   
                        parser.nextToken(); 
                        
                        if(parser.gettext() == 'invalid_email'){InvalidEmail = true;
                            
                        }else if(parser.gettext() <> ''  && parser.gettext() <> 'unknown'){
                            if(parser.gettext().isNumeric()){
                                EmailAge = decimal.ValueOf(parser.gettext()); 
                            }                                                                     
                        }
                        
                        email_age_less_than_one_month= parser.gettext();                                      
                        ResponseMap.put('email_age_less_than_one_month',email_age_less_than_one_month); 
                    }
                }
                
            }
            
            EmailAcceptanceVar = (InvalidEmail || FraudVal == 1 || RiskVal == 1 || RedflagVal > 3 || (emailAge == 1 && RedflagVal > 1)) ? 'N' : 'Y';
            
            system.debug('==EmailAcceptanceVar============'+EmailAcceptanceVar);
            
            
            if((Model26 < 0.20)  &&  (Model43 < 0.20)  &&  (neighborhood < 0.20)){
                IncomeAcceptanceVar = 'Y';
                
            }
            
            system.debug('==IncomeAcceptanceVar============'+IncomeAcceptanceVar); 
            EmpAcceptanceVar = 'Y';
            
            ResponseMap.put('EmailAcceptanceVar',EmailAcceptanceVar);
            ResponseMap.put('IncomeAcceptanceVar',IncomeAcceptanceVar);
            ResponseMap.put('EmpAcceptanceVar',EmpAcceptanceVar);
            
            system.debug('===Parse Response==='+ResponseMap);
            
            Map<string,string> ConfigMap = (Map<String,String>) JSON.deserialize(ThirdPartyDetails.fetchConfigurations('email','AllowProceed'), Map<String,String>.class);
            EmailAcceptanceVar = (EmailAcceptanceVar == 'N') ? ConfigMap.get('acceptance') : EmailAcceptanceVar;
            
            ConfigMap = (Map<String,String>) JSON.deserialize(ThirdPartyDetails.fetchConfigurations('income','AllowProceed'), Map<String,String>.class);
            IncomeAcceptanceVar = (IncomeAcceptanceVar == 'N') ? ConfigMap.get('acceptance') : IncomeAcceptanceVar;
            
            
            AcceptanceVar = (EmailAcceptanceVar == 'N' || IncomeAcceptanceVar == 'N' || EmpAcceptanceVar == 'N') ? 'N' : 'Y';
            ResponseMap.put('AcceptanceVar',AcceptanceVar);
            return JSON.SerializePretty(ResponseMap);
            
        }catch(Exception exc){
            system.debug('=====================exc============'+exc);
            return null;
        }           
    }
    
    //*****************************************
    //*To update result on Lead and Contact
    //*****************************************
    public static void updateResult(Lead leadRow, Contact contactRow, Map<string,string> ResMap){
        
        if(contactRow <> null){ 
            
            if(ResMap.containsKey('EmailAcceptanceVar')) contactRow.Demyst_Email_Verification__c = ResMap.get('EmailAcceptanceVar')=='Y' ? 'Pass' : 'Fail';
            if(ResMap.containsKey('EmailAcceptanceVar')) contactRow.Demyst_Email_Acceptance__c = ResMap.get('EmailAcceptanceVar')=='Y';
            if(ResMap.containsKey('IncomeAcceptanceVar')){if(ResMap.get('IncomeAcceptanceVar')=='Y'){contactRow.put('Demyst_Income_Verification__c','Pass');}else{contactRow.put('Demyst_Income_Verification__c','Fail');}} 
            if(ResMap.containsKey('IncomeAcceptanceVar')){if(ResMap.get('IncomeAcceptanceVar')=='Y'){contactRow.put('Demyst_Income_Acceptance__c',true);}else{contactRow.put('Demyst_Income_Acceptance__c',false);}} 
            if(ResMap.containsKey('EmpAcceptanceVar')){if(ResMap.get('EmpAcceptanceVar')=='Y'){contactRow.put('Demyst_Employer_Verification__c','Pass');}else{contactRow.put('Demyst_Employer_Verification__c','Fail');}} 
            if(ResMap.containsKey('EmpAcceptanceVar')){if(ResMap.get('EmpAcceptanceVar')=='Y'){contactRow.put('Demyst_Employer_Acceptance__c',true);}else{contactRow.put('Demyst_Employer_Acceptance__c',false);}}              
            if(ResMap.containsKey('income_inconsistency_with_model26')){contactRow.put('income_inconsistency_with_model26__c',ResMap.get('income_inconsistency_with_model26'));}
            if(ResMap.containsKey('income_inconsistency_with_model43')){contactRow.put('income_inconsistency_with_model43__c',ResMap.get('income_inconsistency_with_model43'));}
            if(ResMap.containsKey('income_inconsistency_with_neighborhood')){contactRow.put('income_inconsistency_with_neighborhood__c',ResMap.get('income_inconsistency_with_neighborhood'));}
            if(ResMap.containsKey('websearch_fraud_hit_v2')){contactRow.put('websearch_fraud_hit_v2__c',ResMap.get('websearch_fraud_hit_v2'));}
            if(ResMap.containsKey('sanctionrisk')){contactRow.put('sanctionrisk__c',ResMap.get('sanctionrisk'));}
            if(ResMap.containsKey('red_flags_deluxe')){contactRow.put('red_flags_deluxe__c',ResMap.get('red_flags_deluxe'));}    
            if(ResMap.containsKey('email_age_less_than_one_month')){contactRow.put('email_age_less_than_one_month__c',ResMap.get('email_age_less_than_one_month'));}     
            if(ResMap.containsKey('FT_Demyst_Response')){contactRow.put('FT_Demyst_Response__c',ResMap.get('FT_Demyst_Response'));}
            if(ResMap.containsKey('FT_Response')){if(ResMap.get('FT_Response')=='Y'){contactRow.put('Factor_Trust_Verification__c','Pass');}else{contactRow.put('Factor_Trust_Verification__c','Fail');}} 
            if(ResMap.containsKey('FT_Response')){if(ResMap.get('FT_Response')=='Y'){contactRow.put('Factor_Trust_Acceptance__c',true);}else{contactRow.put('Factor_Trust_Acceptance__c',False);}}
            update contactRow;
        }
        
        if(leadRow <> null){
            if(ResMap.containsKey('EmailAcceptanceVar')) leadRow.Demyst_Email_Verification__c = ResMap.get('EmailAcceptanceVar')=='Y' ? 'Pass' : 'Fail';
            if(ResMap.containsKey('EmailAcceptanceVar')) leadRow.Demyst_Email_Acceptance__c = ResMap.get('EmailAcceptanceVar')=='Y';
            if(ResMap.containsKey('IncomeAcceptanceVar')){if(ResMap.get('IncomeAcceptanceVar')=='Y'){leadRow.put('Demyst_Income_Verification__c','Pass');}else{leadRow.put('Demyst_Income_Verification__c','Fail');}} 
            if(ResMap.containsKey('IncomeAcceptanceVar')){if(ResMap.get('IncomeAcceptanceVar')=='Y'){leadRow.put('Demyst_Income_Acceptance__c',true);}else{leadRow.put('Demyst_Income_Acceptance__c',false);}} 
            if(ResMap.containsKey('EmpAcceptanceVar')){if(ResMap.get('EmpAcceptanceVar')=='Y'){leadRow.put('Demyst_Employer_Verification__c','Pass');}else{leadRow.put('Demyst_Employer_Verification__c','Fail');}} 
            if(ResMap.containsKey('EmpAcceptanceVar')){if(ResMap.get('EmpAcceptanceVar')=='Y'){leadRow.put('Demyst_Employer_Acceptance__c',true);}else{leadRow.put('Demyst_Employer_Acceptance__c',false);}}
            if(ResMap.containsKey('income_inconsistency_with_model26')){leadRow.put('income_inconsistency_with_model26__c',ResMap.get('income_inconsistency_with_model26'));}
            if(ResMap.containsKey('income_inconsistency_with_model43')){leadRow.put('income_inconsistency_with_model43__c',ResMap.get('income_inconsistency_with_model43'));}
            if(ResMap.containsKey('income_inconsistency_with_neighborhood')){leadRow.put('income_inconsistency_with_neighborhood__c',ResMap.get('income_inconsistency_with_neighborhood'));}
            if(ResMap.containsKey('websearch_fraud_hit_v2')){leadRow.put('websearch_fraud_hit_v2__c',ResMap.get('websearch_fraud_hit_v2'));}
            if(ResMap.containsKey('sanctionrisk')){leadRow.put('sanctionrisk__c',ResMap.get('sanctionrisk'));}
            if(ResMap.containsKey('red_flags_deluxe')){leadRow.put('red_flags_deluxe__c',ResMap.get('red_flags_deluxe'));}    
            if(ResMap.containsKey('email_age_less_than_one_month')){leadRow.put('email_age_less_than_one_month__c',ResMap.get('email_age_less_than_one_month'));}     
            if(ResMap.containsKey('FT_Demyst_Response')){leadRow.put('FT_Demyst_Response__c',ResMap.get('FT_Demyst_Response'));}
            if(ResMap.containsKey('FT_Response')){if(ResMap.get('FT_Response')=='Y'){leadRow.put('Factor_Trust_Verification__c','Pass');}else{leadRow.put('Factor_Trust_Verification__c','Fail');}}
            if(ResMap.containsKey('FT_Response')){if(ResMap.get('FT_Response')=='Y'){leadRow.put('Factor_Trust_Acceptance__c',true);}else{leadRow.put('Factor_Trust_Acceptance__c',False);}}
            update leadRow;
        }     
        
    }   
}