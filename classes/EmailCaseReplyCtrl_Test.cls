@isTest
public class EmailCaseReplyCtrl_Test {
    @isTest static void testCallout() {
        Contact c = LibraryTest.createContactTH();
        c.Email = 'test@test.com';
        update c;
        Case cs = new Case();
        cs.SuppliedEmail = 'test@test.com';
        cs.contactId = c.Id;
        cs.Description = 'test';
        insert cs;
        
        Java_API_Settings__c jas = new Java_API_Settings__c();
        jas.User_Create_Endpoint__c = 'https://testapi.lendingpoint.com/newCustomerPortalAPI/api/mail/caseReply';
        jas.clientId__c = 'sfdcclient';
        jas.Username__c = 'nikhil';
        jas.Password__c = 'test@123';
        jas.Name = 'CaseReplySettings';
        insert jas;
        
        
        ApexPages.currentPage().getParameters().put('csId',cs.Id);
        EmailCaseReplyCtrl ecr = new EmailCaseReplyCtrl();
        Test.setMock(HttpCalloutMock.class, new CaseHandler_Test());
        EmailCaseReplyCtrl.submitReply(cs.id, cs.Description);
        ecr.reply();
        EmailCaseReplyCtrl.hitCaseReplyAPI('test','test','test',cs);
    }
}