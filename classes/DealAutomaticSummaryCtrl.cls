public with sharing class DealAutomaticSummaryCtrl {
    public DealAutomaticSummaryCtrl() {

    }

    /**
    * Get all users
    * @return List of DealAutomaticUserGroup
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> getAllUsersGroups(Boolean onlyOnlineUsers) {

        List<DealAutomaticUserGroup> result = null;

        if (onlyOnlineUsers)
            result = DealAutomaticAssignmentUtils.getAllUsersGroupsFromCenters();
        else
            result = DealAutomaticAssignmentUtils.getAllUsersGroupsForSummaryPage();

        return result;
    }

    /**
    * Get groups by selected user
    * @return List of DealAutomaticUserDetails
    */
    @RemoteAction
    public static List<DealAutomaticUserDetails> getAttributesBySelectedUser(String userId) {
        List<DealAutomaticUserDetails> dealAutomaticUserDetails = null;

        for (Deal_Automatic_Assignment_Data__c dealAutomaticAssigmentData :
                [SELECT Id, Assigned_Users__c, Attribute_Id__c
                 FROM Deal_Automatic_Assignment_Data__c]) {
            dealAutomaticUserDetails.add(getUsertByAttributeId(String.valueOf(dealAutomaticAssigmentData.Attribute_Id__c)));
        }
        return dealAutomaticUserDetails;
    }

    /**
     * [getSummaryListByUserId description]
     * @param  userId [description]
     * @return        [description]
     */
    @RemoteAction
    public static List<DealAutomaticSummaryList> getSummaryListBySelectedUser(String userId) {
        return DealAutomaticAssignmentUtils.getSummaryListByUserId(userId);
    }

    /**
     * [saveSummaryListBySelectedUser description]
     * @param summaryList [description]
     * @param userId      [description]
     */
    @RemoteAction
    public static void saveSummaryListBySelectedUser(List<DealAutomaticSummaryList> summaryList, String userId) {
        DealAutomaticAssignmentUtils.saveSummaryListByUserId(summaryList, userId);
    }

    /**
     * [saveUserOnlineMode description]
     * @param userId   [description]
     * @param isOnline [description]
     */
    @RemoteAction
    public static void saveUserOnlineMode(String userId, Boolean isOnline) {
        AutomaticAssignmentUtils.saveUserOnlineMode(userId, isOnline);
    }

    /**
     * [reAssignDeals description]
     * @param userId   [description]
     * @param isOnline [description]
     */
    @RemoteAction
    public static void reAssignDeals(String fromUserId, String toUserId, String fromDatetimeString, String toDatetimeString) {

        Datetime fromDatetime = Datetime.parse(fromDatetimeString);
        Datetime toDatetime = Datetime.parse(toDatetimeString);

        DealAutomaticAssignmentUtils.reAssignDeals(fromUserId, toUserId, fromDatetime, toDatetime);
    }

    /**
     * [getUsertByAttributeId description]
     * @param  attribute_Id [description]
     * @return              [description]
     */
    private static DealAutomaticUserDetails getUsertByAttributeId(String attribute_Id) {
        return new DealAutomaticUserDetails();
    }
}