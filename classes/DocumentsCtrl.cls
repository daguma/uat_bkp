public with sharing class DocumentsCtrl {

    @AuraEnabled
    public static DocumentsEntity getDocumentsEntity(Id oppId) {

        DocumentsEntity dee = new DocumentsEntity();

        try {

            if (oppId == null)
                return dee;

            dee.newDocumentCtrl = new NewDocumentCtrl(oppId);
            System.debug(dee.newDocumentCtrl.application);
            //Set the picklist values
            dee.setBankStatementsDispos(Applicants.checkingAcctAndDepositForm);
            dee.setAllAppsOtherDispos(Applicants.allAppsOtherDocs);
            dee.setYtdGrossPaystubDispos(Applicants.ytdGrossPaystub);
            dee.setEmpAppsOtherDocsDispos(Applicants.empAppsOtherDocs);
            dee.setOtherApplicantsDispos(Applicants.otherApplicantsDispos);

            //Data Entry Assigned
            dee.deAtDOptions =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.DE_Assigned_To_Doc__c.getDescribe(),
                            dee.newDocumentCtrl.application.DE_Assigned_To_Doc__c,
                            true
                    );

            //Funding Specialist Assigned
            dee.fsAtDOptions =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.FS_Assigned_To_Val__c.getDescribe(),
                            dee.newDocumentCtrl.application.FS_Assigned_To_Val__c,
                            false
                    );
            //If the FS_Assigned_To_Val__c data is empty we can assume nothing has been selected
            if (String.isEmpty(dee.newDocumentCtrl.application.FS_Assigned_To_Val__c))
                for (alphaHelperCls.PicklistItem pli : dee.fsAtDOptions)
                    if ('Not Assigned'.equalsIgnoreCase(pli.value))
                        pli.isSelected = true;


            //Goverment ID state
            dee.govIdStateOptions =
                    alphaHelperCls.getPicklistItems(
                            Opportunity.Borrower_ID_State__c.getDescribe(),
                            dee.newDocumentCtrl.application.Borrower_ID_State__c,
                            true
                    );

        } catch (Exception e) {
            String errorMessage = e.getMessage() + ' - ' + e.getStackTraceString();
            system.debug('DocumentsCtrl --> getDocumentsEntity --> ' + errorMessage);
            dee.errorMessage = errorMessage;
            return dee;
        }

        return dee;

    }

    @AuraEnabled
    public static NewDocumentCtrl.ProcessResult documentUploadInvite(Opportunity opp){
        NewDocumentCtrl ndc = new NewDocumentCtrl();
        return ndc.documentUploadInvite(opp);
    }
	/*
    @AuraEnabled
    public static NewDocumentCtrl.ProcessResult generateContractDocs(Opportunity opp){
        system.debug('==opp=='+opp);
        NewDocumentCtrl ndc = new NewDocumentCtrl(opp);
        return ndc.genDocument();
    } */
    // Metthod equal to production
    @AuraEnabled
    public static NewDocumentCtrl.ProcessResult generateContractDocs(Opportunity opp,Boolean isSendEmail){
        system.debug('==opp=='+opp);
        NewDocumentCtrl ndc = new NewDocumentCtrl(opp);
        return ndc.genDocument(isSendEmail);
     
    }
    
    
    public static Map<String,Integer> mapCount; //351
    public static Map<String,String> customMap; //351
    public static String keyValue;  //351
    @AuraEnabled
    public static NewDocumentCtrl.QuickSaveResult quickSaveApp(
            Opportunity pOpportunity
            , String pAllApplicantsList
            , String pEmployedApplicants1List
            , String pEmployedApplicants2List
            , String pSelfEmployedApplicants1List
            , String pSelfEmployedApplicants2List
            , String pMiscellaneousApplicantsList
            , String pRetiredApplicantsList) {
        //Start- MAINT-351        
        mapCount = new Map<String,Integer>();
        customMap = new Map<String,String>();
        for(Upload_File_Max_Limits__c ufm : Upload_File_Max_Limits__c.getAll().values()) {
            if(string.isNotEmpty(ufm.Priority_Document__c)) {
                //[SELECT Priority_Document__c,Document_Row_ID__c,Id,XML_Fields__c FROM Upload_File_Max_Limits__c WHERE Use_For_Priority__c = true])
                mapCount.put(ufm.Priority_Document__c,0);
                if(ufm.XML_Fields__c.contains(',')){
                    for(String ss : ufm.XML_Fields__c.split(',')){
                        customMap.put(ufm.Document_Row_ID__c+ss,ufm.Priority_Document__c);
                    }
                    
                }else{
                    customMap.put(ufm.Document_Row_ID__c+ufm.XML_Fields__c,ufm.Priority_Document__c);
                }
            }
        }
        System.debug('mapCount..>>>  '+mapCount);
        System.debug('customMap..>>>  '+customMap);
        // End - MAINT-351
        NewDocumentCtrl ndc = new NewDocumentCtrl();
        ndc.allApplicantsList = mapPivotToApplicants(pAllApplicantsList);
        ndc.employedApplicants1List = mapPivotToEmpApplicants(pEmployedApplicants1List);
        ndc.employedApplicants2List = mapPivotToEmpApplicants(pEmployedApplicants2List);
        ndc.retiredApplicantsList = mapPivotToRetiredApplicants(pRetiredApplicantsList);
        ndc.selfEmployedApplicants1List = mapPivotToSelfEmpApplicants(pSelfEmployedApplicants1List);
        ndc.selfEmployedApplicants2List = mapPivotToSelfEmpApplicants(pSelfEmployedApplicants2List);
        ndc.miscellaneousApplicantsList = mapPivotToMiscApplicants(pMiscellaneousApplicantsList);
        ndc.mapCount = mapCount;    // MAINT-351
        return ndc.quickSaveApp(pOpportunity);
    }

    private static List<Applicants.AllApplicants> mapPivotToApplicants(String pAllApplicantsList) {

        List<Applicants.AllApplicants> resultList = new List<Applicants.AllApplicants>{
        };
        if (String.isEmpty(pAllApplicantsList)) {
            return resultList;
        }

        List<PivotItem> pivotItemsList = (List<PivotItem>) JSON.deserialize(pAllApplicantsList, List<PivotItem>.class);

        for (PivotItem item : pivotItemsList) {
            Applicants.AllApplicants tempApplicants = new Applicants.AllApplicants();
            tempApplicants.id = item.id;
            tempApplicants.required = item.required;
            tempApplicants.received = item.received;    //Maint-351
            tempApplicants.approved = item.approved;
            tempApplicants.receivedDate = item.receivedDate;
            tempApplicants.yodleeFlag = item.yodleeFlag;
            tempApplicants.compledteBy = item.compledteBy;
            tempApplicants.dispositions = item.dispositions;
            resultList.add(tempApplicants);
            //Start- MAINT-351
            keyValue = item.id +'All_Applicants_XML__c';
            if(Boolean.valueOf(item.received) && customMap.containsKey(keyValue)){
                mapCount.put(customMap.get(keyValue),mapCount.get(customMap.get(keyValue))+1);
            }
        //END- MAINT-351
        }

        return resultList;
    }

    private static List<Applicants.EmployedApplicants> mapPivotToEmpApplicants(String pEmployedApplicants2List) {

        List<Applicants.EmployedApplicants> resultList = new List<Applicants.EmployedApplicants>{
        };
        if (String.isEmpty(pEmployedApplicants2List)) {
            return resultList;
        }

        List<PivotItem> pivotItemsList = (List<PivotItem>) JSON.deserialize(pEmployedApplicants2List, List<PivotItem>.class);

        for (PivotItem item : pivotItemsList) {
            Applicants.EmployedApplicants tempApplicants = new Applicants.EmployedApplicants();
            tempApplicants.id = item.id;
            tempApplicants.income = item.income;
            tempApplicants.PaymentPeriod = item.paymentPeriod;
            tempApplicants.required = item.required;
            tempApplicants.approved = item.approved;
            tempApplicants.received = item.received;    //MAINT-351
            tempApplicants.receivedDate = item.receivedDate;
            tempApplicants.compledteBy = item.compledteBy;
            tempApplicants.compledteByIncome = item.compledteByIncome;
            tempApplicants.dispositions = item.dispositions;
            resultList.add(tempApplicants);
            //Start- MAINT-351
            keyValue = item.id +'Employed_Applicants_1_XML__c';
            if(Boolean.valueOf(item.received) && customMap.containsKey(keyValue)){
                mapCount.put(customMap.get(keyValue),mapCount.get(customMap.get(keyValue))+1);
            }
            keyValue = item.id +'Employed_Applicants_2_XML__c';
            if(Boolean.valueOf(item.received) && customMap.containsKey(keyValue)){
                mapCount.put(customMap.get(keyValue),mapCount.get(customMap.get(keyValue))+1);
            }
            //END- MAINT-351
        }

        return resultList;
    }

    private static List<Applicants.RetiredApplicants> mapPivotToRetiredApplicants(String pRetiredApplicantsList) {

        List<Applicants.RetiredApplicants> resultList = new List<Applicants.RetiredApplicants>{
        };
        if (String.isEmpty(pRetiredApplicantsList)) {
            return resultList;
        }

        List<PivotItem> pivotItemsList = (List<PivotItem>) JSON.deserialize(pRetiredApplicantsList, List<PivotItem>.class);

        for (PivotItem item : pivotItemsList) {
            Applicants.RetiredApplicants tempApplicants = new Applicants.RetiredApplicants();
            tempApplicants.id = item.id;
            tempApplicants.required = item.required;
            tempApplicants.form1099Income = item.form1099Income;
            tempApplicants.form1099ReceivedDate = item.form1099ReceivedDate;
            tempApplicants.benefitLetterIncome = item.benefitLetterIncome;
            tempApplicants.benefitLetterReceivedDate = item.benefitLetterReceivedDate;
            tempApplicants.approved = item.approved;
            tempApplicants.received = item.received;    //MAINT-351
            tempApplicants.compledteBy = item.compledteBy;
            tempApplicants.compledteByCurrentYear = item.compledteByCurrentYear;
            tempApplicants.dispositions = item.dispositions;
            resultList.add(tempApplicants);
            //Start- MAINT-351
            keyValue = item.id +'Retired_Applicants_XML__c';
            if(Boolean.valueOf(item.received) && customMap.containsKey(keyValue)){
                mapCount.put(customMap.get(keyValue),mapCount.get(customMap.get(keyValue))+1);
            }
            //End- MAINT-351
        }

        return resultList;
    }

    private static List<Applicants.SelfEmployedApplicants> mapPivotToSelfEmpApplicants(String pSelfEmployedApplicantsList) {

        List<Applicants.SelfEmployedApplicants> resultList = new List<Applicants.SelfEmployedApplicants>{
        };
        if (String.isEmpty(pSelfEmployedApplicantsList)) {
            return resultList;
        }

        List<PivotItem> pivotItemsList = (List<PivotItem>) JSON.deserialize(pSelfEmployedApplicantsList, List<PivotItem>.class);

        for (PivotItem item : pivotItemsList) {
            Applicants.SelfEmployedApplicants tempApplicants = new Applicants.SelfEmployedApplicants();
            tempApplicants.id = item.id;
            tempApplicants.receivedDate = item.receivedDate;
            tempApplicants.income = item.income;
            tempApplicants.netIncome = item.netIncome;
            tempApplicants.calendarValue = item.calendarValue;
            tempApplicants.required = item.required;
            tempApplicants.approved = item.approved;
            tempApplicants.received = item.received;    //MAINT-351
            tempApplicants.compledteBy = item.compledteBy;
            tempApplicants.dispositions = item.dispositions;
            resultList.add(tempApplicants);
            
            //Start- MAINT-351
            keyValue = item.id +'Self_Employed_Applicants_1_XML__c';
            if(Boolean.valueOf(item.received) && customMap.containsKey(keyValue)){
                mapCount.put(customMap.get(keyValue),mapCount.get(customMap.get(keyValue))+1);
            }
            keyValue = item.id +'Self_Employed_Applicants_2_XML__c';
            if(Boolean.valueOf(item.received) && customMap.containsKey(keyValue)){
                mapCount.put(customMap.get(keyValue),mapCount.get(customMap.get(keyValue))+1);
            }
            //END- MAINT-351
        }

        return resultList;
    }

    private static List<Applicants.MiscellaneousApplicants> mapPivotToMiscApplicants(String pMiscApplicantsList) {

        List<Applicants.MiscellaneousApplicants> resultList = new List<Applicants.MiscellaneousApplicants>{
        };
        if (String.isEmpty(pMiscApplicantsList)) {
            return resultList;
        }

        List<PivotItem> pivotItemsList = (List<PivotItem>) JSON.deserialize(pMiscApplicantsList, List<PivotItem>.class);

        for (PivotItem item : pivotItemsList) {
            Applicants.MiscellaneousApplicants tempApplicants = new Applicants.MiscellaneousApplicants();
            tempApplicants.id = item.id;
            tempApplicants.receivedDate = item.receivedDate;
            tempApplicants.income = item.income;
            tempApplicants.required = item.required;
            tempApplicants.received = item.received;    //MAINT-351
            tempApplicants.approved = item.approved;
            tempApplicants.compledteBy = item.compledteBy;
            tempApplicants.dispositions = item.dispositions;
            resultList.add(tempApplicants);
            //Start- MAINT-351
            keyValue = item.id +'Miscellaneous_Applicants_XML__c';
            if(Boolean.valueOf(item.received) && customMap.containsKey(keyValue)){
                mapCount.put(customMap.get(keyValue),mapCount.get(customMap.get(keyValue))+1);
            }
            //End MAINT-351
        }

        return resultList;
    }

    public class PivotItem {
        public String label { get; set; }
        public Boolean showsRequired { get; set; }
        public Boolean isRequired { get; set; }
        public String required { get; set; }
        public Boolean showApproved { get; set; }
        public Boolean isApproved { get; set; }
        public String approved { get; set; } 
        public Boolean showReceived { get; set; }   //MAINT-351
        public Boolean isReceived { get; set; }     //MAINT-351
        public String received { get; set; }        //MAINT-351
        public Boolean isValidated { get; set; }
        public Boolean showsReceivedDate { get; set; }
        public Date receivedDateVal { get; set; }
        public String receivedDate { get; set; }
        public Boolean showsYodleeFlag { get; set; }
        public Boolean isYodleeFlag { get; set; }
        public String yodleeFlag { get; set; }
        public String dispositions { get; set; }
        public Boolean showscompledteBy { get; set; }
        public String compledteBy { get; set; }
        public String defaultName { get; set; }
        public CreditReportSegmentsUtil.FraudIndicators fraudIndicators { get; set; }
        public Boolean mergeCells { get; set; }
        public Integer id { get; set; }
        public Boolean showOtherDocs { get; set; }
        public Boolean showBankStatementsDocs { get; set; }

        public String income { get; set; }
        public String paymentPeriod { get; set; }
        public String compledteByIncome { get; set; }

        public String form1099Income { get; set; }
        public String form1099ReceivedDate { get; set; }
        public String benefitLetterIncome { get; set; }
        public String benefitLetterReceivedDate { get; set; }
        public String compledteByCurrentYear { get; set; }

        public String netIncome { get; set; }
        public String calendarValue { get; set; }

    }

    public class DocumentsEntity {
        @AuraEnabled public NewDocumentCtrl newDocumentCtrl { get; set; }
        @AuraEnabled public String isFSReadOnly { get; set; }
        //AuraEnabled does not support SelectOption; use PicklistItem instead of SelectOption
        @AuraEnabled public List<alphaHelperCls.PicklistItem> bankStatementsDispos { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> allAppsOtherDispos { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> ytdGrossPaystubDispos { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> empAppsOtherDocsDispos { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> otherApplicantsDispos { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> years { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> govIdStateOptions { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> fsAtDOptions { get; set; }
        @AuraEnabled public List<alphaHelperCls.PicklistItem> deAtDOptions { get; set; }
        @AuraEnabled public Boolean hasError { get; set; }
        @AuraEnabled public String errorMessage {
            get;
            set {
                errorMessage = value;
                hasError = true;
            }
        }

        public DocumentsEntity() {
            hasError = false;
            bankStatementsDispos = new List<alphaHelperCls.PicklistItem>();
            allAppsOtherDispos = new List<alphaHelperCls.PicklistItem>();
            ytdGrossPaystubDispos = new List<alphaHelperCls.PicklistItem>();
            empAppsOtherDocsDispos = new List<alphaHelperCls.PicklistItem>();
            otherApplicantsDispos = new List<alphaHelperCls.PicklistItem>();
            setYears();
        }

        public void setBankStatementsDispos(List<String> dispos) {
            if (dispos != null)
                addDispos(this.bankStatementsDispos, dispos);
        }

        public void setAllAppsOtherDispos(List<String> dispos) {
            if (dispos != null)
                addDispos(this.allAppsOtherDispos, dispos);
        }

        public void setYtdGrossPaystubDispos(List<String> dispos) {
            if (dispos != null)
                addDispos(this.ytdGrossPaystubDispos, dispos);
        }

        public void setEmpAppsOtherDocsDispos(List<String> dispos) {
            if (dispos != null)
                addDispos(this.empAppsOtherDocsDispos, dispos);
        }

        public void setOtherApplicantsDispos(List<String> dispos) {
            if (dispos != null)
                addDispos(this.otherApplicantsDispos, dispos);
        }

        private void addDispos(List<alphaHelperCls.PicklistItem> picklist, List<String> dispos) {
            for (String val : dispos) {
                picklist.add(new alphaHelperCls.PicklistItem(val, val));
            }
        }

        public void setYears() {
            years = new List<alphaHelperCls.PicklistItem>();
            Integer currentYear = System.Today().year() - 10;
            for (Integer i = currentYear; i < currentYear + 20; i++) {
                years.add(new alphaHelperCls.PicklistItem(String.valueof(i), String.valueof(i)));
            }
        }
    }
}