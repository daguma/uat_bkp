global class ClarityClearProductsRequest {

    public String username{get;set;}
    public String control_file_substituted{get;set;}
    public String control_file_name{get;set;}
    public Double control_file_version_number{get;set;}
    public String group_name{get;set;}
    public String account_name{get;set;}
    public String location_name{get;set;}
    public String action{get;set;}
    public String deny_codes{get;set;}
    public String deny_descriptions{get;set;}
    public String exception_descriptions{get;set;}
    public String filter_codes{get;set;}
    public String filter_descriptions{get;set;}
}