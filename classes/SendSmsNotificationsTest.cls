@isTest
public class SendSmsNotificationsTest {
    @isTest static void testConstructor() {
        Test.startTest();
        SendSmsNotifications scheduler = new SendSmsNotifications();

        System.assertEquals(True, True);
        Test.stopTest();
    }

    @isTest static void testExecute() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

        SchedulableContext SC;
		createCampaignWithMembers();
        SendSmsNotifications scheduler = new SendSmsNotifications();
        scheduler.execute(SC);
        System.assertEquals(True, True);
        Test.stopTest();
    }
    
    @isTest static void testRetrieveActiveCampaigns() {
        Test.startTest();
        createCampaignWithMembers();

        SendSmsNotifications scheduler = new SendSmsNotifications();
        List<Campaign> campaigns = scheduler.retrieveActiveCampaigns();
        Boolean result = campaigns instanceof List<Campaign>;
        Test.stopTest();

        System.assertEquals(result, True);
    }

    @isTest static void testSendSmsNotificationsForActiveCampaigns() {
        Test.startTest();
		createCampaignWithMembers();

        SendSmsNotifications scheduler = new SendSmsNotifications();
        scheduler.sendSmsNotificationsForActiveCampaigns();
        System.assertEquals(Null, Null);
        Test.stopTest();
    }

    @isTest static void testSendSmsNotificationsForCampaign() {
        Test.startTest();
        SendSmsNotifications scheduler = new SendSmsNotifications();

        // Completed campaign
        Campaign campaign = new Campaign();
        campaign.Name = 'Foo Campaign';
		campaign.SMS_Notification_Status__c = 'Completed';
        insert campaign;

        Boolean resultCompletedCampaign = scheduler.sendSmsNotificationsForCampaign(campaign);
        System.assertEquals(False, resultCompletedCampaign);
        
        // Active campaign
		campaign = createCampaignWithMembers();
        Boolean resultActiveCampaign = scheduler.sendSmsNotificationsForCampaign(campaign);
        System.assertEquals(True, resultActiveCampaign);

        Test.stopTest();
    }

    @isTest static void testRetrieveCampaignById() {
        Test.startTest();
        // Campaign doesn't exist
        SendSmsNotifications scheduler = new SendSmsNotifications();
        Campaign campaign = scheduler.retrieveCampaignById('Foo');
        System.assertEquals(campaign, Null);

        // Campaign exists
        campaign = createCampaignWithMembers();
        campaign = scheduler.retrieveCampaignById(campaign.Id);
        System.assertNotEquals(campaign, Null);
        Test.stopTest();

    }

    @isTest static void testStaticMethodSendSmsNotificationsForCampaign() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Campaign campaign = createCampaignWithMembers();
        SendSmsNotifications.sendSmsNotificationsForCampaign(campaign.Id);
        System.assertEquals(True, True);
        Test.stopTest();
    }

    @isTest static void testStaticMethodStopSmsNotificationsForCampaign() {
        Test.startTest();
        Campaign campaign = createCampaignWithMembers();
        SendSmsNotifications.stopSmsNotificationsForCampaign(campaign.Id);
        
        Campaign updatedCampaign = [SELECT Id, SMS_Notification_Status__c FROM Campaign
                                    WHERE Id= : campaign.Id];

        System.assertEquals('Paused', updatedCampaign.SMS_Notification_Status__c);
        
        campaign.SMS_Notification_Status__c = 'Completed';
        update campaign;

        SendSmsNotifications.startSmsNotificationsForCampaign(campaign.Id);
        updatedCampaign = [SELECT Id, SMS_Notification_Status__c FROM Campaign
                                    WHERE Id= : campaign.Id];

        System.assertEquals('Completed', updatedCampaign.SMS_Notification_Status__c);
        Test.stopTest();
    }

    @isTest static void testStaticMethodStartSmsNotificationsForCampaign() {
        Test.startTest();
        Campaign campaign = createCampaignWithMembers();
        campaign.SMS_Notification_Status__c = 'Paused';
        update campaign;

        SendSmsNotifications.startSmsNotificationsForCampaign(campaign.Id);
        Campaign updatedCampaign = [SELECT Id, SMS_Notification_Status__c FROM Campaign
                                    WHERE Id= : campaign.Id];

        System.assertEquals('Active', updatedCampaign.SMS_Notification_Status__c);
        
        campaign.SMS_Notification_Status__c = 'Completed';
        update campaign;

        SendSmsNotifications.startSmsNotificationsForCampaign(campaign.Id);
        updatedCampaign = [SELECT Id, SMS_Notification_Status__c FROM Campaign
                                    WHERE Id= : campaign.Id];

        System.assertEquals('Completed', updatedCampaign.SMS_Notification_Status__c);
        Test.stopTest();
    }

    @isTest static void testGenerateTwilioMessageRecord() {
        Test.startTest();
        Campaign campaign = createCampaignWithMembers();

        String account = 'ACa5c7e1524c314f67f3528252e014aeb7';
        String token   = 'hBpQyosmdN8PpL36WObI2K2DQhIx8U17';
        String messagingServiceId = 'AC5157b4393187f597c39f17c2db3ca315';

        TwilioSF.TwilioApiClient api = new TwilioSF.TwilioApiClient(account, token);
        api.addUrlPart('Accounts');
        api.addUrlPart(messagingServiceId);
        api.addUrlPart('Messages.json');

        api.addParam('To',   '+18584425066');
        api.addParam('From', '+14702841416');
        api.addParam('Body', 'Test SMS' + System.now().format());

        try {
            TwilioSF.TwilioApiClientResponse response = api.doPost();

            SendSmsNotifications scheduler = new SendSmsNotifications();
            scheduler.generateTwilioMessageRecord(campaign.Id, messagingServiceId, response);
            System.assertEquals(True, True);
        } catch (Exception e) {
            // Note: This test requires further configuration in order to be tested properly
			System.assertEquals(True, True);
        }

        Test.stopTest();
    }
    
    static Campaign createCampaignWithMembers() {
		Contact contact = new Contact(FirstName='John', LastName='Wick', Phone='+18584425066');
        insert contact;

        Campaign campaign = new Campaign();
        campaign.Name = 'Foo Campaign';
        campaign.SMS_Notification_Status__c = 'Active';
        insert campaign;

        CampaignMember member = new CampaignMember();
        member.ContactId = contact.Id;
        member.CampaignId = campaign.Id;
        member.SMS_Notification__c = 'Test SMS Notification: $123.50';
        insert member;
        
        return campaign;
    }
}