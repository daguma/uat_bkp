public class ProxyApiAccountManagement {
    public static final String SEQUENCE_ID_ACCOUNT_MANAGEMENT = 'Account Management';

    @invocableMethod
    public static void getAccountManagementSoftPull(List<String> appList) {

        if (appList.isEmpty()) return;

        String appId = appList[0];


        Opportunity application = CreditReport.getOpportunity(appId);

        if ((application != null &&
                ((Datetime.now() - 30) >= application.Account_Management_Pull_Date__c)) || application.Account_Management_Pull_Date__c == null) {

            if (System.isBatch()) get(appId); else getWithFuture(appId);

        }

    }

    @future (callout = true)
    public static void getWithFuture(String appId) {
        get(appId);
    }
    public static void get(String appId) {
        try {

            ProxyApiCreditReportEntity creditReportEntity = null;
            Opportunity application = CreditReport.getOpportunity(appId);
            CreditReportInputData inputData = CreditReport.mapCreditReportInputData(appId, true, false, application);
            //inputData.decisioningParameter.callAWS = false;
            inputData.decisioningParameter.tbsLogic = ProxyApiAccountManagement.getTbsLogicForAccountManagement(appId);
            if (inputData.gdsRequestParameter == null)
                inputData.gdsRequestParameter = CreditReport.mapGDSRequestParameter(appId, false);

            inputData.gdsRequestParameter.sequenceId = BRMS_Sequence_Ids__c.getInstance(SEQUENCE_ID_ACCOUNT_MANAGEMENT).Sequence_Id__c;

            if (inputData != null) {
                if (Test.isRunningTest())
                    creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(LibraryTest.fakeCreditReportData(application.Id), ProxyApiCreditReportEntity.class);
                else
                    creditReportEntity = ProxyApiCreditReport.getCreditReport(JSON.serialize(inputData), false, true);

                update new Opportunity(Id = application.Id, Account_Management_Pull_Date__c = Datetime.now().date(), Status__c = application.Status__c);
                if (creditReportEntity != null && creditReportEntity.errorMessage == null) {
                    Id ContractId = null;

                    for (loan__Loan_Account__c c : [SELECT Id FROM loan__Loan_Account__c WHERE opportunity__c = : application.Id and (NOT loan__Loan_Status__c like 'Canc%') LIMIT 1]) ContractId = c.Id;
                    if (ContractId != null) {
                        Decimal fico = 0;
                        decimal dti = 0; // OF-915
                        
                        if (creditReportEntity.brms != null && creditReportEntity.brms.rulesList != null && !creditReportEntity.brms.rulesList.isEmpty()) {
                            for (ProxyApiCreditReportRuleEntity rule : creditReportEntity.brms.rulesList) {
                                if (String.isNotBlank(rule.deRuleNumber) && rule.deRuleNumber.equals('2')) {
                                    try {
                                        fico = decimal.valueOf(rule.deRuleValue.Split(',')[0]);
                                    } catch (Exception e) {}
                                }
                                // OF-915
                                if (String.isNotBlank(rule.deRuleName) && rule.deRuleName == 'DTI_Verification' && String.isNotBlank(rule.deRuleValue)) {
                                    try {
                                        dti = decimal.valueOf(rule.deRuleValue);
                                    } catch (Exception e) {}
                                }
                            }
                        }
                        Long creditReport = 0;
                        if (creditReportEntity.attributesList != null) {
                            for (ProxyApiCreditReportAttributeEntity credit : creditReportEntity.attributesList) {
                                creditReport = credit.creditReportId;
                            }
                        }
                        insert new AccountManagementHistory__c(opportunity__c = application.Id, DTIValue__c = dti, Contract__c = ContractId, FicoValue__c = fico, CreditReportId__c = creditReport);
                    }
                }
            }
        } catch (Exception e) {
            WebToSFDC.notifyDev('ProxyApiAccountManagement.get Error ',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString() + '\n app Id = ' + appId + '\n');
        }
    }

    public static String getTbsLogicForAccountManagement(String OppId) {
        Date loanAccrualStartDate = null;
        for (loan__loan_Account__c cl : [SELECT loan__Accrual_Start_Date__c from loan__loan_Account__c WHERE Opportunity__c = : OppId ORDER by CreatedDate DESC LIMIT 1]) {
            loanAccrualStartDate = cl.loan__Accrual_Start_Date__c;
        }
        if (ProxyApiAccountManagement.MonthsDiff(loanAccrualStartDate) > 10) {
            for (TBS_Logic__c tbs : [SELECT ProxyExecution__c
                                     FROM TBS_Logic__c
                                     WHERE SequenceId__c = 10
                                             ORDER BY Priority__c
                                             LIMIT 1]) {
                return tbs.ProxyExecution__c;
            }
        }
        return 'TwoPulls=false';
    }

    public static Integer MonthsDiff(Date a) {
        if (a == null) return 0;
        Date b = Date.Today();
        Integer monthDiff = a.monthsBetween(b);
        if (b.day() < a.day()) monthDiff--;
        return monthDiff;
    }
}