@isTest
private class PaymentPendingProcessBatchTest {
    @isTest static void jobRunTest() {
        
        loan__Loan_Account__c cl = TestHelper.createContract();
        cl.loan__Disbursal_Date__c = System.today();
        cl.loan__Loan_Status__c = 'Active - Good Standing';
        update cl;
        TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.createSeedDataForTesting();
        
        loan__Payment_Mode__c pMode = [select Id from loan__Payment_Mode__c where Name = 'ACH' limit 1];
        
        
        loan__Loan_Payment_Transaction__c transactionPay = new loan__Loan_Payment_Transaction__c();
        transactionPay.loan__Loan_Account__c = cl.id;
        transactionPay.loan__Transaction_Amount__c = 1000;
        transactionPay.loan__Cleared__c = true;
        transactionPay.FT_Payment_Reporting__c = false;
        transactionPay.loan__Payment_Mode__c = pMode.Id;
        insert transactionPay;
        
        transactionPay.Payment_Pending_End_Date__c = Date.today().addDays(-30);
        update transactionPay;
        
        loan__Loan_account_Due_Details__c bill = new loan__Loan_account_Due_Details__c();
        bill.loan__Loan_Account__c = cl.Id;
        bill.loan__Due_Amt__c = 1000;
        bill.loan__DD_Primary_Flag__c = true;
        bill.loan__Due_Date__c = Date.today().addDays(10);
        insert bill;
        
        System.assert([Select Id from loan__Loan_Payment_Transaction__c].size() > 0);
        
        
        test.startTest();
        PaymentPendingProcessBatch job = new PaymentPendingProcessBatch();
        job.execute(null);
        test.stopTest();
        System.assert([Select Id from loan__Loan_Payment_Transaction__c WHERE  Payment_Pending_End_Date__c = null].size() > 0, 'Payment_Pending_End_Date__c not cleared');
        
        System.debug('Testing should have been succesful!');
        
    }
    
     @isTest static void jobOverrideRunTest() {
        
        loan__Loan_Account__c cl = TestHelper.createContract();
        cl.loan__Disbursal_Date__c = System.today();
        cl.loan__Loan_Status__c = 'Active - Good Standing';
        update cl;
        TestHelperForManaged.setupApprovalProcessForTxn(); // 3 day lock period
        TestHelperForManaged.setupACHParameters();
        TestHelperForManaged.createSeedDataForTesting();
        
        loan__Payment_Mode__c pMode = [select Id from loan__Payment_Mode__c where Name = 'ACH' limit 1];
        
        
        loan__Loan_Payment_Transaction__c transactionPay = new loan__Loan_Payment_Transaction__c();
        transactionPay.loan__Loan_Account__c = cl.id;
        transactionPay.loan__Transaction_Amount__c = 1000;
        transactionPay.loan__Cleared__c = true;
        transactionPay.FT_Payment_Reporting__c = false;
        transactionPay.loan__Payment_Mode__c = pMode.Id;
        insert transactionPay;
         
        
        transactionPay.Payment_Pending_End_Date__c = Date.today().addDays(-30);
        update transactionPay;
        
        loan__Loan_account_Due_Details__c bill = new loan__Loan_account_Due_Details__c();
        bill.loan__Loan_Account__c = cl.Id;
        bill.loan__Due_Amt__c = 1000;
        bill.loan__DD_Primary_Flag__c = true;
        bill.loan__Due_Date__c = Date.today().addDays(10);
        insert bill;
        Id paymentId = [Select Id from loan__Loan_Payment_Transaction__c LIMIT 1].Id;
                
        test.startTest();
        String testJob = PaymentPendingProcessBatch.overridePending(paymentId);
        test.stopTest();
        System.assert((testJob == 'Pending Payment Cleared' ), testJob);
        //System.assert([Select Id from loan__Loan_Payment_Transaction__c WHERE Id =:paymentId AND Payment_Pending_End_Date__c = null].size() > 0, 'not cleared');
        
        System.debug('Testing should have been succesful!');
        
    }
}