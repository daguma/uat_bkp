public class CreditReportHawkAlertsResult {

    @AuraEnabled public String alertName {get; set;}
    public Boolean isApproved {get; set;}
    @AuraEnabled public List<String> alertReasons {get; set;}

    @AuraEnabled
    public String hawkResult {
        get {
            return isApproved ? 'Pass' : 'Fail';
        }
        set;
    }
    
    public String hawkStyleClass {
        get {
            return isApproved ? 'green' : 'red';
        }
        set;
    }

    public CreditReportHawkAlertsResult() {

    }

    public CreditReportHawkAlertsResult(String alertName,
                                        Boolean isApproved,
                                        List<String> alertReasons) {
        this.alertName = alertName;
        this.isApproved = isApproved;
        this.alertReasons = alertReasons;
    }
}