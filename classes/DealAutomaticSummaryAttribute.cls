public class DealAutomaticSummaryAttribute {

    public Decimal id {get; set;}
    public String name {get; set;}
    public Boolean isActive {get; set;}

    /**
     * [DealAutomaticSummaryAttribute description]
     * @return [description]
     */
    public DealAutomaticSummaryAttribute() {

    }

    /**
     * [DealAutomaticSummaryAttribute description]
     * @param  id       [description]
     * @param  name     [description]
     * @param  isActive [description]
     * @return          [description]
     */
    public DealAutomaticSummaryAttribute(Decimal id,
                                         String name,
                                         Boolean isActive) {
        this.id = id;
        this.name = name;
        this.isActive = isActive;
    }
}