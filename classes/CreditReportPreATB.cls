global class CreditReportPreATB {

    public static ProxyApiCreditReportEntity getByEntityId(String entityId) {
        ProxyApiCreditReportEntity creditReportEntity = null;
        Disqualifiers_Pre_ATB__c disqualifiers = getDisqualifiersPreATB(entityId);

        if (disqualifiers != null) {
            creditReportEntity = new ProxyApiCreditReportEntity();
            creditReportEntity.id = 0;
            creditReportEntity.entityId = entityId;
            creditReportEntity.entityName = 'application';
            creditReportEntity.isSoftPull = true;
            creditReportEntity.decision = false;
            creditReportEntity.attributesList = getAttributeList(disqualifiers);
        } else {
            creditReportEntity = new ProxyApiCreditReportEntity('No disqualifiers Pre-ATB information was found.', '');
        }

        return creditReportEntity;
    }

    public static String getDecisionByEntityId(String entityId) {
        Disqualifiers_Pre_ATB__c disqualifiers = getDisqualifiersPreATB(entityId);

        if (disqualifiers != null) {
            return disqualifiers.Decision__c;
        }

        return '';
    }

    public static ProxyApiCreditReportAttributeEntity addAtt(String pType, String pADesc, Integer pAId, String pDQ, String value, Boolean isOverride ) {
        ProxyApiAttributeTypeEntity attributeType = new ProxyApiAttributeTypeEntity(pType, pADesc);
        ProxyApiCreditReportAttributeEntity attribute = new ProxyApiCreditReportAttributeEntity();
        if (pAId<> null) attributeType.id = pAId;
        attribute.value = String.valueOf(value);
        attribute.isApproved = pDQ != null && pDQ.equals('P');
        attribute.approvedBy = isOverride && !attribute.isApproved ? 'Overridden' : 'system';
        attribute.attributeType = attributeType;

        return attribute;
    }

    public static List<ProxyApiCreditReportAttributeEntity> getAttributeList(Disqualifiers_Pre_ATB__c disqualifiers) {
        List<ProxyApiCreditReportAttributeEntity> result = new List<ProxyApiCreditReportAttributeEntity>();

        Boolean isOverride = disqualifiers.Override__c != null && disqualifiers.Override__c.equalsIgnoreCase('TRUE');

        result.add(addAtt('FICO ( < 600 )', '', null, disqualifiers.DisqCreditScore__c, String.valueOf(disqualifiers.CreditScore__c), isOverride  ));
        result.add(addAtt('DTI ( >= 45% )', '', CreditReport.DTI, disqualifiers.DisqDTI__c, String.valueOf(disqualifiers.DTI__c), isOverride  ));
        result.add(addAtt('Current Delinquencies ( > 0 )', '', null, disqualifiers.DisqCurrentDelinquencies__c ,  String.valueOf(disqualifiers.Current_Delinquencies__c),  isOverride  ));
        result.add(addAtt('Charge Off ( > 0 )', '', null, disqualifiers.DisqChargeOffs__c, String.valueOf(disqualifiers.ChargeOffs__c), isOverride  ));
        result.add(addAtt('Tax Liens ( > 0 )', '', null, disqualifiers.DisqTaxLiens__c, String.valueOf(disqualifiers.TaxLiens__c) ,  isOverride  ));
        result.add(addAtt('Age of Credit File ( < 2 years )', '', null, disqualifiers.DisqAgeOfCredit__c, String.valueOf(disqualifiers.AgeOfCreditDays__c),  isOverride  ));
        result.add(addAtt('Open Bankruptcies ( > 0 )', '', null, disqualifiers.DisqOpenBankruptcies__c, String.valueOf(disqualifiers.Open_Bankruptcies__c) ,  isOverride  ));
        result.add(addAtt('Inquiries in last 3 months ( > 3 )', '', null, disqualifiers.DisqInqlast3mths__c, String.valueOf(disqualifiers.Inquiries_in_the_last_3_months__c),  isOverride  ));
        result.add(addAtt('Inquiries in last 6 months ( > 6 )', '', null, disqualifiers.DisqInqlast6mths__c, String.valueOf(disqualifiers.Inquiries_in_the_last_6_months__c),  isOverride  ));
        result.add(addAtt('Revolving Utilization ( > 100% )', '', null, disqualifiers.DisqRevUtil__c, String.valueOf(disqualifiers.Revolving_Utilization__c)  ,  isOverride  ));
        result.add(addAtt('Revolving Open To Buy ( < 0 )', '', null, disqualifiers.DisqRevUtil__c, String.valueOf(disqualifiers.Open_to_buy__c),  isOverride  ));
        result.add(addAtt('Annual Income ( < 20k )', '', null, disqualifiers.DisqRevolvingO2B__c, String.valueOf(disqualifiers.AnnualIncome__c),  isOverride  ));

        ProxyApiAttributeTypeEntity attributeType = new ProxyApiAttributeTypeEntity('Employment', '(C:< 1 year; P:<2 years;G:>=90 days)\n C: Current Employment\n P: Prior Employment\n G: Gap between Jobs');
        ProxyApiCreditReportAttributeEntity attribute = new ProxyApiCreditReportAttributeEntity();
        attribute.value = 'C: ' + String.valueOf(disqualifiers.EmploymentDurationYrs__c) +
                          '; P: ' + String.valueOf(disqualifiers.PriorEmploymentDurationYears__c) +
                          '; G: ' + String.valueOf(disqualifiers.EmploymentGapDurationDays__c);
        attribute.isApproved = disqualifiers.DisqEmployment__c != null && disqualifiers.DisqEmployment__c.equals('P');
        attribute.approvedBy = isOverride && !attribute.isApproved ? 'Overridden' : 'system';
        attribute.attributeType = attributeType;

        result.add(attribute);

        return result;
    }

    public static Disqualifiers_Pre_ATB__c getDisqualifiersPreATB(String entityId) {
        for (Disqualifiers_Pre_ATB__c disqualifiers : [SELECT Id, AgeOfCreditDays__c, DisqAgeOfCredit__c,
                AnnualIncome__c, DisqAnnualIncome__c, ChargeOffs__c,
                DisqChargeOffs__c, Current_Delinquencies__c, DisqCurrentDelinquencies__c,
                AppId__c, DTI__c, DisqDTI__c, EmploymentType__c,
                EmploymentDurationYrs__c, PriorEmploymentDurationYears__c,
                EmploymentGapDurationDays__c, DisqEmployment__c, CreditScore__c,
                DisqCreditScore__c, Inquiries_in_the_last_3_months__c, DisqInqlast3mths__c,
                Inquiries_in_the_last_6_months__c, DisqInqlast6mths__c, Open_Bankruptcies__c,
                DisqOpenBankruptcies__c, Open_to_buy__c, DisqRevolvingO2B__c,
                Revolving_Utilization__c, DisqRevUtil__c, Decision__c, Override__c,
                SC_Accpt_Flag__c, LP_Credit_Grade__c, TaxLiens__c, DisqTaxLiens__c,
                TaxLienAmount__c, OverrideEmployment__c
                FROM Disqualifiers_Pre_ATB__c
                WHERE AppId__c = :entityId
                                 LIMIT 1]) {
            return disqualifiers;
        }
        return null;
    }
}