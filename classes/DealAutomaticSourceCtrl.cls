public with sharing class DealAutomaticSourceCtrl {
    public DealAutomaticSourceCtrl() {

    }

    /**
    * Get all deal sources from the Deal Automatic Attributes
    * @return List of Deal_Automatic_Attributes__c
    */
    public static String getDealSources() {
        return JSON.serialize([SELECT Id__c, Name, Display_Name__c, Usage__c
                               FROM Deal_Automatic_Attributes__c
                               WHERE Usage__c LIKE :DealAutomaticAssignmentUtils.DEAL_SOURCE_USAGE
                               ORDER BY Display_Name__c]);
    }

    /**
    * [saveUsers description]
    * @param  attribute to save the selected users
    * @param  usersGroups    to save in the specific category
    * @return Save the selected users in the Deal_Automatic_Assignment_Data__c object
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> saveUsers(Deal_Automatic_Attributes__c attribute,
            List<DealAutomaticUserGroup> newUsersGroups,
            List<DealAutomaticUserGroup> oldUserGroups) {
        return DealAutomaticAssignmentUtils.saveUsers(attribute, newUsersGroups, oldUserGroups);
    }

    /**
    * [getUsersGroupsBySource description]
    * @param  source [description]
    * @return  List of DealAutomaticUserGroup
    */
    @RemoteAction
    public static List<DealAutomaticUserGroup> getUsersGroupsBySource(Deal_Automatic_Attributes__c source) {
        return DealAutomaticAssignmentUtils.getUsersGroupsByCategory(source);
    }
}