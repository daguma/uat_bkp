@isTest
public Class BatchFinwiseDailyInterestReportingTest{    
    
    
    @testSetup
    static void setupTestFinwiseData(){
       FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name='finwise',Finwise_Base_URL__c='https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/',Apply_State_Restriction__c=false,
                                      Finwise_LendingPartnerId__c='5',Finwise_LendingPartnerProductId__c='7',
                                      Finwise_LoanStatusId__c='1',finwise_partner_key__c='testKey',Finwise_product_Key__c='testkey',
                                      Finwise_RateTypeId__c='1',Whitelisted_States__c='MO',No_of_Days_to_Add_Daily_Interest_Batch__c = 1,Daily_Interest_Batch_Size__c = 1);
       insert FinwiseParams;                                                    
    }
    
    
    static testMethod void TestExecuteMethodPositive(){ 
       loan__Loan_Account__c clContr = TestHelper.createContract();
       clContr.loan__Next_Due_Generation_Date__c =  system.today() + 1;  
       update clContr;
       
        Opportunity aplic = new Opportunity(id = clContr.Opportunity__c,Finwise_Approved__c=true);
        update aplic;
        		clContr.Opportunity__c = aplic.Id;
        update clContr; 
        List<loan__Loan_Account__c> ap = new  List<loan__Loan_Account__c>();
        ap.add(clContr);
        
        Test.startTest();
        BatchFinwiseDailyInterestReporting btchP = new BatchFinwiseDailyInterestReporting();
        btchP.execute(null, ap);
        btchP.execute(null);
        Test.stopTest();  
    
    }
    
    static testMethod void TestExecuteMethodNegative(){ 
       loan__Loan_Account__c clContr = TestHelper.createContract();
       clContr.loan__Next_Due_Generation_Date__c = system.today() + integer.ValueOf(1);
       Opportunity aplic = new Opportunity(id = clContr.Opportunity__c,Finwise_Approved__c=true);
        update aplic;
        		clContr.Opportunity__c = aplic.Id;
        update clContr;
        
       Test.startTest();
            BatchFinwiseDailyInterestReporting btchN = new BatchFinwiseDailyInterestReporting();
            btchN.execute(null);
        Test.stopTest();  
    
    }
    
  }