@isTest
class BridgerListCheckCtrlTest {
    static testMethod void test() {
        Contact con = new Contact(LastName='TestContact',Firstname = 'David',Email = 'test@gmail2.com',MailingState = 'GA');
        insert con;

        Opportunity opp = new Opportunity(Name='Test Opportunity', CloseDate=System.today(),StageName='Qualification',Contact__c=con.Id);
        insert opp;
        
        BridgerWDSFLookup__c bri = new BridgerWDSFLookup__c();
        bri.review_status__c = 'No match';
        bri.Contact__c = opp.Contact__c;
        bri.Lookup_By__c = 'test';
        bri.Name_Searched__c = 'test';
        bri.Number_of_Hits__c = 0;
        insert bri;        
        BridgerListCheckCtrl.getBridgerDetails(opp.Id);
    }
}