@isTest
private class BridgerEditLookupCtrlTest {

    public static testMethod void testContactEdit() {

        Contact cont = new contact(firstname = 'Jose', lastname = 'Rodriguez');
        insert cont;

        BridgerWDSFLookup__c prevLayout = new BridgerWDSFLookup__c();
        BridgerWDSFLookup__c posLayout = new BridgerWDSFLookup__c();

        BridgerWDSFLookup__c layout = new BridgerWDSFLookup__c();
        layout.Contact__c = cont.id;
        layout.List_Date__c = date.today();
        layout.Number_of_Hits__c = 0;
        layout.List_Description__c = 'OFAC';
        layout.Result_Node__c = 'test';
        layout.lookup_by__c = 'Joe';
        layout.review_status__c = 'No Match';
        insert layout;

        prevLayout = layout;

        ApexPages.StandardController sc = new ApexPages.standardController(layout);
        BridgerEditLookupCtrl myPageCon = new BridgerEditLookupCtrl(sc);

        layout = new BridgerWDSFLookup__c();
        layout.Contact__c = cont.id;
        layout.List_Date__c = date.today();
        layout.Number_of_Hits__c = 1;
        layout.List_Description__c = 'OFAC';
        layout.Result_Node__c = 'test';
        layout.lookup_by__c = 'Joe';
        layout.review_status__c = 'No Match';
        insert layout;

        sc = new ApexPages.standardController(layout);
        myPageCon = new BridgerEditLookupCtrl(sc);
        myPageCon.save();

        layout = new BridgerWDSFLookup__c();
        layout.Contact__c = cont.id;
        layout.List_Date__c = date.today();
        layout.Number_of_Hits__c = 1;
        layout.List_Description__c = 'OFAC';
        layout.Result_Node__c = 'test';
        layout.lookup_by__c = 'Joe';
        layout.review_status__c = 'True Match';
        insert layout;

        posLayout = layout;

        sc = new ApexPages.standardController(layout);
        myPageCon = new BridgerEditLookupCtrl(sc);
        myPageCon.prevstatus = 'No Match';
        myPageCon.save();

        sc = new ApexPages.standardController(layout);
        myPageCon = new BridgerEditLookupCtrl(sc);
        myPageCon.save();

        layout = new BridgerWDSFLookup__c();
        layout.Contact__c = cont.id;
        layout.List_Date__c = date.today();
        layout.Number_of_Hits__c = 1;
        layout.List_Description__c = 'OFAC';
        layout.Result_Node__c = 'test';
        layout.lookup_by__c = 'Joe';
        layout.review_status__c = null;
        insert layout;

        sc = new ApexPages.standardController(layout);
        myPageCon = new BridgerEditLookupCtrl(sc);
        myPageCon.save();

        system.assertNotEquals(posLayout.review_status__c, prevLayout.review_status__c);
    }
}