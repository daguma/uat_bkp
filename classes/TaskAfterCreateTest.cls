@isTest
private class TaskAfterCreateTest {

	@isTest static void Create_Task_Method() {
		Account a = new Account(Name = 'Sample Account');
		insert a;

		Contact c = LibraryTest.createContactTH();
		c.AccountId = a.Id;
		update c;

		List<Task> tasks = new List<Task>();
		tasks.add(new Task(
		              ActivityDate = Date.today().addDays(7),
		              Subject = 'Call Task',
		              WhatId = a.Id,
		              OwnerId = UserInfo.getUserId(),
		              Status = 'In Progress'));

		insert tasks;

		c = [SELECT TotalCallsMade__c, TotalCallsMadeSinceLastStatusChange__c FROM Contact WHERE id = : c.id LIMIT 1];

		System.assertEquals(1, c.TotalCallsMade__c);

		System.assertEquals(1, c.TotalCallsMadeSinceLastStatusChange__c);
	}
    
    @isTest static void Delete_Task_Method() {
		Account a = new Account(Name = 'Sample Account');
		insert a;

		Contact c = LibraryTest.createContactTH();
		c.AccountId = a.Id;
		update c;

		List<Task> tasks = new List<Task>();
		tasks.add(new Task(
		              ActivityDate = Date.today().addDays(7),
		              Subject = 'Call Task',
		              WhatId = a.Id,
		              OwnerId = UserInfo.getUserId(),
		              Status = 'In Progress'));

		insert tasks;
        
        System.assertEquals(1, [SELECT COUNT() FROM Task]);
        
        delete tasks;
        
        System.assertEquals(0, [SELECT COUNT() FROM Task]);

        
	}

}