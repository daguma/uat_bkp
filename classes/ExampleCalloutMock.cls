@isTest
public class ExampleCalloutMock {
     public HttpResponse respond(HTTPRequest req){
      string body='<Response xmlns="http://www.payleap.com/payments" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><AuthCode></AuthCode><ProcessedAsCreditCard>False</ProcessedAsCreditCard><RespMSG></RespMSG><Result>0</Result><Message></Message><PNRef>400098</PNRef><Exponent>010001</Exponent><GUID>469ccbef-49b3-1c2c-9266-dc44a3a9401e</GUID><Modulus>B90BA703A0F83FFFA0D1BAEC29AE28AD88BDDB1F8A18F04D3F352EC099714D9F0A773FFF09DAE735432723F9F507EECD8D7437E407B1B388248E8517B41E2A9773156965441D0F328490FE6B0C64B03846F41F30CE48257CFBB7EAE7464CCA9562ED2812CDA377DF312516AD94806146062F132B66D9ED3D3C954FFF1388F82B</Modulus></Response>';
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody(body);
        return res;
      }
 }