@isTest
public class CollectionsCasesAssignmentEntityTest {
    static testmethod void validate_constructor() {

        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        //myCase.owner = [SELECT Id FROM User LIMIT 1];
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id; 
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R02 - Account Closed';

        Test.startTest();
        CollectionsCasesAssignmentEntity myTestEntity = 
            new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        Test.stopTest();

        System.assertEquals(myContract.Id ,myTestEntity.contractId);
        System.assertEquals(myContract.Collection_Case_Owner__c, myTestEntity.collectionsCaseOwnerId);
        System.assertEquals(myContract.Overdue_Days__c, myTestEntity.dpd);
        System.assertEquals(myContract.loan_Active_Payment_Plan__c, myTestEntity.hasActivePaymentPlan);
        System.assertEquals(myContract.Is_Payment_Pending__c, myTestEntity.hasPendingPayment);
        System.assertEquals(myContract.First_Payment_Missed__c, myTestEntity.missedFirstPayment);
        System.assertEquals(!String.isEmpty(myContract.DMC__c), myTestEntity.isDMC);
        System.assertEquals('Point Of Need'.equalsIgnoreCase(myContract.loan__Loan_Product_Name__c), myTestEntity.isLMS);
        
        System.assertEquals(myCase.Id, myTestEntity.caseId);
        System.assertEquals(myCase.Owner.Id, myTestEntity.caseOwnerId);
        System.assertEquals(myCase.status, myTestEntity.caseStatus);
        System.assertEquals(myCase != null, myTestEntity.hasCase);
        
        System.assertEquals(paymentPlanOwnerId, myTestEntity.paymentPlanOwnerId);
        System.assertEquals(otAchOwnerId, myTestEntity.otACHOwnerId);
        System.assertEquals(otAchOwnerId != null, myTestEntity.hasOtACH);

        System.assertEquals(lastReversal, myTestEntity.lastPaymentReversalReason);
        System.assertEquals(true, myTestEntity.isLastPaymentReturned);
           
    }

    static testmethod void validate_constructor2() {

        loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        insert myCase;

        Test.startTest();
        CollectionsCasesAssignmentEntity myTestEntity = new CollectionsCasesAssignmentEntity(myContract, myCase, 'my contract is not null');
        system.assert(myTestEntity.assignedScenario != null);

        myTestEntity = new CollectionsCasesAssignmentEntity(null, myCase, 'some exception ocurred...');
        system.assert(myTestEntity.assignedScenario != null);
        Test.stopTest();
        
    }

    static testmethod void validate_lastPaymentAsR03() {  
           loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        //myCase.owner = [SELECT Id FROM User LIMIT 1];
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id; 
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R03 - No Account / Unable to Locate Account';

        Test.startTest();
        CollectionsCasesAssignmentEntity myTestEntity = 
            new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        Test.stopTest();

        System.assertEquals(true, myTestEntity.isLastPaymentReturned);
    }

    static testmethod void validate_lastPaymentAsR01() {  
           loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        //myCase.owner = [SELECT Id FROM User LIMIT 1];
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id; 
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R01 - Insufficient Funds';

        Test.startTest();
        CollectionsCasesAssignmentEntity myTestEntity = 
            new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        Test.stopTest();

        System.assertEquals(true, myTestEntity.isLastPaymentReturned);
    }
    static testmethod void validate_lastPaymentAsR08() {  
           loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        //myCase.owner = [SELECT Id FROM User LIMIT 1];
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id; 
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R08 - Payment Stopped';

        Test.startTest();
        CollectionsCasesAssignmentEntity myTestEntity = 
            new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        Test.stopTest();

        System.assertEquals(true, myTestEntity.isLastPaymentReturned);
    }

    static testmethod void validate_lastPaymentAsR16() {  
           loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        //myCase.owner = [SELECT Id FROM User LIMIT 1];
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id; 
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R16 - Account Frozen';

        Test.startTest();
        CollectionsCasesAssignmentEntity myTestEntity = 
            new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        Test.stopTest();

        System.assertEquals(true, myTestEntity.isLastPaymentReturned);
    }

    static testmethod void validate_lastPaymentNot2_3_8_16() {  
           loan__Loan_Account__c myContract = LibraryTest.createContractTH();
        Case myCase = new Case();
        //myCase.owner = [SELECT Id FROM User LIMIT 1];
        myCase.status = 'Promise Kept';
        insert myCase;
        Id paymentPlanOwnerId = myCase.owner.Id; 
        Id otAchOwnerId = myCase.owner.Id;
        String lastReversal = 'R30';

        Test.startTest();
        CollectionsCasesAssignmentEntity myTestEntity = 
            new CollectionsCasesAssignmentEntity(
                myContract, 
                myCase, 
                paymentPlanOwnerId, 
                otAchOwnerId, 
                lastReversal
            );
        Test.stopTest();

        System.assertEquals(false, myTestEntity.isLastPaymentReturned);
    }
}