@isTest public class ClearTest {

    @isTest static void maps_clear_input_data() {
        Contact c = LibraryTest.createContactTH();

        c.Phone = '1234567890';
        c.MailingStreet = 'Mailing Street';
        c.MailingCity = 'Mailing City';
        c.MailingState = 'GA';
        c.MailingPostalCode = '10000';
        c.SSN__c = '123456789';
        update c;

        ClearInputData result = Clear.mapClearCourtInputData(c);

        System.assertNotEquals(null, result);
    }

    @isTest static void maps_clear_input_parameter() {
        Contact c = LibraryTest.createContactTH();

        c.Phone = '1234567890';
        c.MailingStreet = 'Mailing Street';
        c.MailingCity = 'Mailing City';
        c.MailingState = 'GA';
        c.MailingPostalCode = '10000';
        update c;

        ClearInputParameter result = Clear.mapClearInputParameter(c);

        System.assertNotEquals(null, result);
    }

    @isTest static void gets_court() {

        Integer countBefore = [SELECT COUNT() FROM Clear_Court_Result__c];

        opportunity app = LibraryTest.createApplicationTH();

        User currentUser = [SELECT Id, Name FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];

        System.runAs(currentUser) {

            Test.startTest();

            Clear.getCourt(app.Contact__C, app.Id);

            Test.stopTest();

            System.assert([SELECT COUNT() FROM Clear_Court_Result__c] > countBefore, 'Gets Court');

            Note taxLiensNote = [SELECT Id, Title FROM Note WHERE ParentId = : app.Id LIMIT 1];

            System.assert(taxLiensNote.Title.contains('Tax Liens and/or Judgements Results for'), taxLiensNote.Title);
        }
    }

    @isTest static void gets_court_with_invalid_data() {
        Integer countBefore = [SELECT COUNT() FROM Clear_Court_Result__c];

        User currentUser = [SELECT Id, Name FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];

        System.runAs(currentUser) {

            Test.startTest();

            Clear.getCourt('error', '');

            Test.stopTest();

            System.assertEquals([SELECT COUNT() FROM Clear_Court_Result__c], countBefore);
        }
    }

    @isTest static void gets_court_with_invalid_app() {
        Integer countBefore = [SELECT COUNT() FROM Clear_Court_Result__c];

        Contact c = LibraryTest.createContactTH();

        User currentUser = [SELECT Id, Name FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];

        System.runAs(currentUser) {

            Test.startTest();

            Clear.getCourt(c.Id, '');

            Test.stopTest();

            System.assertEquals([SELECT COUNT() FROM Clear_Court_Result__c], countBefore);
        }
    }

    @isTest static void gets_clear_court_result() {
        opportunity app = LibraryTest.createApplicationTH();

        Clear_Court_Result__c clearCourt = new Clear_Court_Result__c();
        clearCourt.Opportunity__c = app.Id;
        clearCourt.Contact__c = app.Contact__C;
        clearCourt.Open_Tax_Liens__c = 0;
        clearCourt.Closed_Tax_Liens__c = 0;
        clearCourt.Open_Judgments__c = 0;
        clearCourt.Closed_Judgments__c = 0;
        clearCourt.Total_Liens_Judgments__c = 0;
        insert clearCourt;

        Clear_Court_Result__c result = Clear.getClearCourtResult(app.Contact__C, app.Id);

        System.assertNotEquals(null, result);
    }

    @isTest static void court_logic_greater_than_zero() {
        opportunity app = LibraryTest.createApplicationTH();

        Clear_Court_Result__c clearCourt = new Clear_Court_Result__c();
        clearCourt.opportunity__c = app.Id;
        clearCourt.Contact__c = app.Contact__C;
        clearCourt.Open_Tax_Liens__c = 0;
        clearCourt.Closed_Tax_Liens__c = 0;
        clearCourt.Open_Judgments__c = 0;
        clearCourt.Closed_Judgments__c = 0;
        clearCourt.Total_Liens_Judgments__c = 5;
        insert clearCourt;

        Boolean result = Clear.isCourtCountLogicGreaterThanZero(app.Contact__C, app.Id);

        System.assertEquals(true, result);
    }

    @isTest static void court_logic_equals_to_zero() {
        opportunity app = LibraryTest.createApplicationTH();

        Clear_Court_Result__c clearCourt = new Clear_Court_Result__c();
        clearCourt.opportunity__c = app.Id;
        clearCourt.Contact__c = app.Contact__C;
        clearCourt.Open_Tax_Liens__c = 0;
        clearCourt.Closed_Tax_Liens__c = 0;
        clearCourt.Open_Judgments__c = 0;
        clearCourt.Closed_Judgments__c = 0;
        clearCourt.Total_Liens_Judgments__c = 0;
        insert clearCourt;

        Boolean result = Clear.isCourtCountLogicGreaterThanZero(app.Contact__C, app.Id);

        System.assertEquals(false, result);
    }

}