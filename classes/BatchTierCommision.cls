global class BatchTierCommision implements Database.Batchable<sObject>,Schedulable{
    
    public void execute(SchedulableContext sc) { 
       BatchTierCommision  batchapex = new BatchTierCommision();
       id batchprocessid = Database.executebatch(batchapex);
       system.debug('Process ID: ' + batchprocessid);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){       
       string indicator= 'Cost per Funding';  
       date cutOffDate = system.today().addMonths(-1);                      
       string query = 'Select id,Application__c,Application__r.Partner_Account__c,Application__r.Partner_Account__r.Commission_Structure__c,loan__Loan_Amount__c ,Application__r.Partner_Account__r.Commission_Structure__r.Tier_Range_Indicator__c,CREATEDdATE   from loan__Loan_Account__c where  CREATEDdATE >=: cutOffDate and Application__r.Partner_Account__r.Commission_Structure__r.Commission_Cost_Base__c =:indicator and Application__r.Partner_Account__r.Commission_Structure__r.Tier_Range_Indicator__c <> null' ;
       system.debug('===========cutOffDate ================'+cutOffDate);
       return Database.getQueryLocator(query);
    }
    
    public BatchTierCommision(){}
    
    global void execute(Database.BatchableContext BC, List<loan__Loan_Account__c > scope){
      try{ 
      //Variable declarations//
      Integer CommissionScheme=0;
      Integer partialCLSize=0;
      decimal loanAmt=0.00;
      String tierStructure,CS;
      Set<String> PartnerSet= new Set<String>();
      Set<String> CSSet= new Set<String>();
      loan__Loan_Account__c loanAccount;
      List<Tier_Structure__c> CSTierStructureList;      
      Map <String, list<Tier_Structure__c> > CSTierStructure= new Map <String, list<Tier_Structure__c> >();
      Map<String, String> PartnerRangeValueMap= new Map<String, String>();
      Map<String,String> PartnerCSMap = new Map<String, String>();
      Map<String,String> loanAmtMap = new Map<String, String>();
      Map <String,String> CLIdsSizeMap= new Map <String, String>();
      Map<String,Integer>PartnerCSValueMap = new Map<String, Integer>();
      Map<String,loan__Loan_Account__c > finalUpdMap = new Map<String,loan__Loan_Account__c >();
     
        //Storing required value from loan__loan_Account__c  in map as (partner,value)//
       for(loan__loan_Account__c CL : scope){
           
           if (!(PartnerSet.contains(CL.Application__r.Partner_Account__c)) || PartnerSet== null ) {
                      PartnerSet.add(CL.Application__r.Partner_Account__c);
                      CS=CL.Application__r.Partner_Account__r.Commission_Structure__c;                    
                      CSSet.add(CS);
                      loanAmt=0.00;
                      PartnerCSMap.put(CL.Application__r.Partner_Account__c,CS);
                      partialCLSize=0;
                }
             tierStructure = CL.Application__r.Partner_Account__r.Commission_Structure__r.Tier_Range_Indicator__c; 
             if(PartnerSet.contains(CL.Application__r.Partner_Account__c)) {
               if(tierStructure=='Count'){
                 
                 if((CLIdsSizeMap.get(CL.Application__r.Partner_Account__c))<>null){
                   partialCLSize = Integer.ValueOf(CLIdsSizeMap.get(CL.Application__r.Partner_Account__c));
                  } 
                   partialCLSize=partialCLSize+1;
                   CLIdsSizeMap.put(CL.Application__r.Partner_Account__c,String.ValueOf(partialCLSize));
                   PartnerRangeValueMap.put(CL.Application__r.Partner_Account__c, String.ValueOf(partialCLSize));
                   
                 }  
                 
               else if(tierStructure=='Dollar Amount'){

                if((loanAmtMap.get(CL.Application__r.Partner_Account__c))<>null){ 
                  loanAmt = Decimal.ValueOf(loanAmtMap.get(CL.Application__r.Partner_Account__c));
                } 
                  loanAmt=loanAmt+CL.loan__Loan_Amount__c;
                  loanAmtMap.put(CL.Application__r.Partner_Account__c,String.ValueOf(loanAmt));
                  PartnerRangeValueMap.put(CL.Application__r.Partner_Account__c, String.ValueOf(loanAmt));
    
                  
                }
                
              }
            } 
         
           //Storing CS in map as (CS, list of tier ranges related to CS)// 
            list<Tier_Structure__c> tierlist = [Select Id,Commissions_Structure__c, Name,Commission_Scheme_Value__c, Maximum_Range_Value__c, Minimum_Range_Value__c
                                     from Tier_Structure__c 
                                     where Commissions_Structure__c IN:CSSet];
             Map<id,Tier_Structure__c> TierStructureMap = new  Map<id,Tier_Structure__c>(tierlist);                      
             for (ID idKey : TierStructureMap.keyset()) {  
              Tier_Structure__c tierS = TierStructureMap.get(idKey); 
               if(tierS .Commissions_Structure__c<> null){
                 List<Tier_Structure__c> tierRangeList = new List<Tier_Structure__c>();
                 if(CSTierStructure.containskey(tierS .Commissions_Structure__c)) {
                   tierRangeList = CSTierStructure.get(tierS .Commissions_Structure__c);
                 }
                 tierRangeList .add(tierS );
                 CSTierStructure.put(tierS.Commissions_Structure__c,tierRangeList );               
                }  
              }                        
             System.debug(PartnerSet+'..PartnerSet..'+'####PartnerRangeValueMap####'+PartnerRangeValueMap+'...CSTierStructure....'+CSTierStructure);
            /*Getting CSvalue,CSrange and CStierStructure on basis of Partner from Map to get Commission Scheme value of individual partner*/                         
            for(String PartnerValues:PartnerSet){
                 String CSValue=PartnerCSMap.get(PartnerValues);
                 decimal CSrange=decimal.ValueOf(PartnerRangeValueMap.get(PartnerValues));
                 CSTierStructureList =CSTierStructure.get(CSValue);
                 /*Get CommissionScheme value on basis of range for each partner*/
                if(CSTierStructureList<>null){ 
                 for(Tier_Structure__c k:CSTierStructureList){
                     Integer maxRange= Integer.ValueOf(k.get('Maximum_Range_Value__c'));
                     Integer minRange= Integer.ValueOf(k.get('Minimum_Range_Value__c'));
                    if(maxrange>=CSrange && minRange<=CSrange){
                      system.debug('CSrange'+CSrange+'maxrange'+maxrange+'minRange'+minRange+'k.get(Commission_Scheme_Value__c)'+k.get('Commission_Scheme_Value__c'));
                      CommissionScheme=Integer.ValueOf(k.get('Commission_Scheme_Value__c'));
                      PartnerCSValueMap.put(PartnerValues,CommissionScheme);
                     } 
                    }
                 }
            }
            /*Map of CLid and loan__Loan_Account__c object to update CS value*/
            for(loan__loan_Account__c CL : scope) {
                 String CLPartner =  CL.Application__r.Partner_Account__c;
                 Integer CommissionValue= PartnerCSValueMap.get(CLPartner);
                 loanAccount= new loan__Loan_Account__c (ID=CL.id ,Tier_Direct_Sales_Commission__c=CommissionValue);
                 finalUpdMap.put(CL.id,loanAccount);  
            }
            
            If(!finalUpdMap.isEmpty()){
                System.debug(finalUpdMap);
                update finalUpdMap.values();
              }
            
          }catch(Exception e){
          
          system.debug('Exception' +e.getMessage()+' at '+e.getLineNumber());

       }   
    }
    
    global void finish(Database.BatchableContext BC){       
      

    }
   

}