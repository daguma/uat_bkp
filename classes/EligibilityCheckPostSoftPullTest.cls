@isTest public class EligibilityCheckPostSoftPullTest {

    @testSetup static void inserts_finwise_dummy_data() {
        FinwiseDetails__c finW = new FinwiseDetails__c();
        finW.Enable_Finwise__c = false;
        finW.Whitelisted_States__c = 'N/A';
        finW.Name = 'finwise';
        insert finW;
    }

    @isTest static void checks_eligibility_for_refinance() {
        loan__Loan_Account__c testContract = LibraryTest.createContractTH();
        List<String> ids = new List<String>();
        ids.add(testContract.id);
        EligibilityCheckPostSoftPull.checkEligibility(ids);
    }

    @isTest static void updates_loan_refinance_params() {
        loan__Loan_Account__c testContract = LibraryTest.createContractTH();

        loan_Refinance_Params__c refParams = new loan_Refinance_Params__c();
        refParams.Contract__c = testContract.Id;
        refParams.FICO__c = 700;
        refParams.Grade__c = 'C1';
        refParams.Eligible_For_Refinance__c = false;
        refParams.Last_Refinance_Eligibility_Review__c = Date.today().addDays(-40);
        insert refParams;

        List<String> ids = new List<String>();
        ids.add(testContract.id);
        EligibilityCheckPostSoftPull.checkEligibility(ids);
    }

    @isTest static void gets_refinance_offers_better_grade_same_apr() {
        System.debug(LoggingLevel.INFO, 'get_refinance_offers_better_grade_same_apr START');
        Offer__c offerNew = new Offer__c();
        Offer__c offerRefi = new Offer__c();
        List<Offer__c> oldOffers = new List<Offer__c>();
        List<Offer__c> newOffers = new List<Offer__c>();

        loan__Loan_Account__c testContract = LibraryTest.createContractTH();

        testContract.loan__Principal_Remaining__c = 3182.26;
        testContract.loan__Fees_Remaining__c = 75;
        testContract.loan__Interest_Accrued_Not_Due__c = 7.71;
        testContract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(5);
        update testContract;

        Contact c = [SELECT Id, AccountId FROM Contact WHERE Id = : testContract.loan__Contact__c];

        Opportunity testNewApp = [SELECT   Id,
                                               Fee_Handling__c,
                                               Type,
                                               Contact__c
                                               FROM    Opportunity
                                               WHERE   Id = : testContract.Opportunity__c
                                                       LIMIT 1];

        testNewApp.Fee_Handling__c = FeeUtil.NET_FEE_OUT;
        testNewApp.Type = 'New';
        update testNewApp;

        Opportunity testRefiApp = LibraryTest.createApplicationTH();
        testRefiApp.Fee_Handling__c = FeeUtil.NET_FEE_OUT;
        testRefiApp.Type = 'Refinance';
        testRefiApp.Contract_Renewed__c = testContract.Id;
        update testRefiApp;

        // Offer for New App - BEGIN;
        offerNew.Repayment_Frequency__c = 'Monthly';
        offerNew.APR__c = 0.24;
        offerNew.Term__c = 36;
        offerNew.Payment_Amount__c = 190.03;
        offerNew.IsSelected__c = true;
        offerNew.Selected_Date__c = Date.today().addDays(-120);
        offerRefi.Effective_APR__c = 27.690036553792;
        offerNew.Loan_Amount__c = 5000;
        offerNew.Total_Loan_Amount__c = 5250;
        offerNew.Fee__c = 250;
        offerNew.IsCustom__c = false;
        offerNew.Fee_Percent__c = 5;
        offerNew.FirstPaymentDate__c = null;
        offerNew.Installments__c = 36;
        offerNew.Opportunity__c = testNewApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerNew;
        oldOffers.add(offerNew);

        offerNew = new Offer__c();
        offerNew.Repayment_Frequency__c = 'Monthly';
        offerNew.APR__c = 0.24;
        offerNew.Term__c = 36;
        offerNew.Payment_Amount__c = 190.03;
        offerNew.IsSelected__c = true;
        offerNew.Selected_Date__c = Date.today().addDays(-120);
        offerRefi.Effective_APR__c = 27.690036553792;
        offerNew.Loan_Amount__c = 5000;
        offerNew.Total_Loan_Amount__c = 5250;
        offerNew.Fee__c = 250;
        offerNew.IsCustom__c = false;
        offerNew.Fee_Percent__c = 5;
        offerNew.FirstPaymentDate__c = null;
        offerNew.Installments__c = 36;
        offerNew.Opportunity__c = testNewApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerNew;
        oldOffers.add(offerNew);
        // Offer for New App - END;

        // Offer for Refinance App - BEGIN;
        Double apr = 0.24;
        Integer term = 36;
        Double payAmt = 456.07;
        Double effAPR = 27.689701167107;
        Double loanAmt = 12000;
        Double totalLA = 12600;
        Double fee = 600;
        Double feePct = 5;

        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = apr;
        offerRefi.Term__c = term;
        offerRefi.Payment_Amount__c = payAmt;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = effAPR;
        offerRefi.Loan_Amount__c = loanAmt;
        offerRefi.Total_Loan_Amount__c = totalLA;
        offerRefi.Fee__c = fee;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = feePct;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = 36;
        offerRefi.Opportunity__c = testRefiApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerRefi;
        newOffers.add(offerRefi);

        offerRefi = new Offer__c();
        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = apr;
        offerRefi.Term__c = term;
        offerRefi.Payment_Amount__c = payAmt;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = effAPR;
        offerRefi.Loan_Amount__c = loanAmt;
        offerRefi.Total_Loan_Amount__c = totalLA;
        offerRefi.Fee__c = fee;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = feePct;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = 36;
        offerRefi.Opportunity__c = testRefiApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerRefi;
        newOffers.add(offerRefi);
        // Offer for Refinance App - BEGIN;

        List<Offer__c> newRefiOffers = EligibilityCheckPostSoftPull.getRefinanceOffers(oldOffers.get(0), newOffers, true, 'B1');
        //Offer__c refiOffer = new Offer__c();

        System.debug(LoggingLevel.INFO, 'get_refinance_offers_better_grade_same_apr END');

        for (Offer__c refiOffer : newRefiOffers) {
            System.debug(LoggingLevel.INFO, 'old APR: ' + apr + ', new APR: ' + refiOffer.APR__c);
            System.debug(LoggingLevel.INFO, 'old Term: ' + term + ', new Term: ' + refiOffer.Term__c);
            System.debug(LoggingLevel.INFO, 'old Payment_Amount__c: ' + payAmt + ', new Payment_Amount__c: ' + refiOffer.Payment_Amount__c);
            System.debug(LoggingLevel.INFO, 'old Effective_APR__c: ' + effAPR + ', new Effective_APR__c: ' + refiOffer.Effective_APR__c);
            System.debug(LoggingLevel.INFO, 'old Fee_Percent__c: ' + feePct + ', new Fee_Percent__c: ' + refiOffer.Fee_Percent__c);
            System.debug(LoggingLevel.INFO, 'old Fee__c: ' + fee + ', new Fee__c: ' + refiOffer.Fee__c);
            System.debug(LoggingLevel.INFO, 'old Total_Loan_Amount__c: ' + totalLA + ', new Total_Loan_Amount__c: ' + refiOffer.Total_Loan_Amount__c);
            System.debug(LoggingLevel.INFO, 'old Loan_Amount__c: ' + loanAmt + ', new Loan_Amount__c: ' + refiOffer.Loan_Amount__c);

            System.assert(refiOffer != null);
        }
    }

    @isTest static void gets_refinance_offers_better_grade_diff_apr() {
        System.debug(LoggingLevel.INFO, 'get_refinance_offers_better_grade_diff_apr START');
        Scorecard__c scoreNew = new Scorecard__c();
        Scorecard__c scoreRefi = new Scorecard__c();
        Offer__c offerNew = new Offer__c();
        Offer__c offerRefi = new Offer__c();
        Offer__c offerToEval = new Offer__c();
        List<Offer__c> oldOffers = new List<Offer__c>();
        List<Offer__c> newOffers = new List<Offer__c>();
        Boolean hasBetterGrade = false;

        loan__Loan_Account__c testContract = LibraryTest.createContractTH();

        testContract.loan__Principal_Remaining__c = 3182.26;
        testContract.loan__Fees_Remaining__c = 75;
        testContract.loan__Interest_Accrued_Not_Due__c = 7.71;
        testContract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(5);
        update testContract;

        Contact c = [SELECT Id, AccountId FROM Contact WHERE Id = : testContract.loan__Contact__c];

        Opportunity testNewApp = [SELECT   Id,
                                               Fee_Handling__c,
                                               Type,
                                               Contact__c
                                               FROM    Opportunity
                                               WHERE   Id = : testContract.Opportunity__c
                                                       LIMIT 1];

        testNewApp.Fee_Handling__c = FeeUtil.NET_FEE_OUT;
        testNewApp.Type = 'New';
        update testNewApp;

        Opportunity testRefiApp = LibraryTest.createApplicationTH();
        testRefiApp.Fee_Handling__c = FeeUtil.NET_FEE_OUT;
        testRefiApp.Type = 'Refinance';
        testRefiApp.Contract_Renewed__c = testContract.Id;
        update testRefiApp;

        // Offer for New App - BEGIN;
        offerNew.Repayment_Frequency__c = 'Monthly';
        offerNew.APR__c = 0.24;
        offerNew.Term__c = 36;
        offerNew.Payment_Amount__c = 190.03;
        offerNew.IsSelected__c = true;
        offerNew.Selected_Date__c = Date.today().addDays(-120);
        offerRefi.Effective_APR__c = 27.690036553792;
        offerNew.Loan_Amount__c = 5000;
        offerNew.Total_Loan_Amount__c = 5250;
        offerNew.Fee__c = 250;
        offerNew.IsCustom__c = false;
        offerNew.Fee_Percent__c = 5;
        offerNew.FirstPaymentDate__c = null;
        offerNew.Installments__c = 36;
        offerNew.Opportunity__c = testNewApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerNew;
        oldOffers.add(offerNew);
        // Offer for New App - END;

        // Offer for Refinance App - BEGIN;
        Double apr = 0.3264;
        Integer term = 36;
        Double payAmt = 506.07;
        Double effAPR = 32.811546981925;
        Double loanAmt = 11500;
        Double totalLA = 11525;
        Double fee = 25;
        Double feePct = 0.22;

        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = apr;
        offerRefi.Term__c = term;
        offerRefi.Payment_Amount__c = payAmt;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = effAPR;
        offerRefi.Loan_Amount__c = loanAmt;
        offerRefi.Total_Loan_Amount__c = totalLA;
        offerRefi.Fee__c = fee;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = feePct;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = 36;
        offerRefi.Opportunity__c = testRefiApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerRefi;
        newOffers.add(offerRefi);

        offerRefi = new Offer__c();
        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = apr;
        offerRefi.Term__c = term;
        offerRefi.Payment_Amount__c = payAmt;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = effAPR;
        offerRefi.Loan_Amount__c = loanAmt;
        offerRefi.Total_Loan_Amount__c = totalLA;
        offerRefi.Fee__c = fee;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = feePct;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = 36;
        offerRefi.Opportunity__c = testRefiApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerRefi;
        newOffers.add(offerRefi);
        // Offer for Refinance App - END;

        List<Offer__c> newRefiOffers = EligibilityCheckPostSoftPull.getRefinanceOffers(oldOffers.get(0), newOffers, true, 'B1');

        System.debug(LoggingLevel.INFO, 'get_refinance_offers_better_grade_diff_apr END');

        for (Offer__c refiOffer : newRefiOffers) {
            System.debug(LoggingLevel.INFO, 'old APR: ' + apr + ', new APR: ' + refiOffer.APR__c);
            System.debug(LoggingLevel.INFO, 'old Term: ' + term + ', new Term: ' + refiOffer.Term__c);
            System.debug(LoggingLevel.INFO, 'old Payment_Amount__c: ' + payAmt + ', new Payment_Amount__c: ' + refiOffer.Payment_Amount__c);
            System.debug(LoggingLevel.INFO, 'old Effective_APR__c: ' + effAPR + ', new Effective_APR__c: ' + refiOffer.Effective_APR__c);
            System.debug(LoggingLevel.INFO, 'old Fee_Percent__c: ' + feePct + ', new Fee_Percent__c: ' + refiOffer.Fee_Percent__c);
            System.debug(LoggingLevel.INFO, 'old Fee__c: ' + fee + ', new Fee__c: ' + refiOffer.Fee__c);
            System.debug(LoggingLevel.INFO, 'old Total_Loan_Amount__c: ' + totalLA + ', new Total_Loan_Amount__c: ' + refiOffer.Total_Loan_Amount__c);
            System.debug(LoggingLevel.INFO, 'old Loan_Amount__c: ' + loanAmt + ', new Loan_Amount__c: ' + refiOffer.Loan_Amount__c);

            System.assert(refiOffer != null);
        }
    }

    @isTest static void gets_refinance_offers_same_grade_same_term() {
        System.debug(LoggingLevel.INFO, 'get_refinance_offers_same_grade_same_term START');
        Scorecard__c scoreNew = new Scorecard__c();
        Scorecard__c scoreRefi = new Scorecard__c();
        Offer__c offerNew = new Offer__c();
        Offer__c offerRefi = new Offer__c();
        List<Offer__c> oldOffers = new List<Offer__c>();
        List<Offer__c> newOffers = new List<Offer__c>();
        Boolean hasBetterGrade = false;

        loan__Loan_Account__c testContract = LibraryTest.createContractTH();

        testContract.loan__Principal_Remaining__c = 3182.26;
        testContract.loan__Fees_Remaining__c = 75;
        testContract.loan__Interest_Accrued_Not_Due__c = 7.71;
        testContract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(5);
        update testContract;

        Contact c = [SELECT Id, AccountId FROM Contact WHERE Id = : testContract.loan__Contact__c];

        Opportunity testNewApp = [SELECT   Id,
                                               Fee_Handling__c,
                                               Type,
                                               Contact__c
                                               FROM    Opportunity
                                               WHERE   Id = : testContract.Opportunity__c
                                                       LIMIT 1];

        testNewApp.Fee_Handling__c = FeeUtil.NET_FEE_OUT;
        testNewApp.Type = 'New';
        update testNewApp;

        Opportunity testRefiApp = LibraryTest.createApplicationTH();
        testRefiApp.Fee_Handling__c = FeeUtil.NET_FEE_OUT;
        testRefiApp.Type = 'Refinance';
        testRefiApp.Contract_Renewed__c = testContract.Id;
        update testRefiApp;

        // Offer for New App - BEGIN;
        offerNew.Repayment_Frequency__c = 'Monthly';
        offerNew.APR__c = 0.24;
        offerNew.Term__c = 36;
        offerNew.Payment_Amount__c = 190.03;
        offerNew.IsSelected__c = true;
        offerNew.Selected_Date__c = Date.today().addDays(-120);
        offerRefi.Effective_APR__c = 27.690036553792;
        offerNew.Loan_Amount__c = 5000;
        offerNew.Total_Loan_Amount__c = 5250;
        offerNew.Fee__c = 250;
        offerNew.IsCustom__c = false;
        offerNew.Fee_Percent__c = 5;
        offerNew.FirstPaymentDate__c = null;
        offerNew.Installments__c = 36;
        offerNew.Opportunity__c = testNewApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerNew;
        oldOffers.add(offerNew);
        // Offer for New App - END;

        // Offer for Refinance App - BEGIN;
        Double apr = 0.3264;
        Integer term = 36;
        Double payAmt = 506.07;
        Double effAPR = 32.811546981925;
        Double loanAmt = 11500;
        Double totalLA = 11525;
        Double fee = 25;
        Double feePct = 0.22;

        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = apr;
        offerRefi.Term__c = term;
        offerRefi.Payment_Amount__c = payAmt;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = effAPR;
        offerRefi.Loan_Amount__c = loanAmt;
        offerRefi.Total_Loan_Amount__c = totalLA;
        offerRefi.Fee__c = fee;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = feePct;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = 36;
        offerRefi.Opportunity__c = testRefiApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerRefi;
        newOffers.add(offerRefi);

        offerRefi = new Offer__c();
        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = apr;
        offerRefi.Term__c = term;
        offerRefi.Payment_Amount__c = payAmt;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = effAPR;
        offerRefi.Loan_Amount__c = loanAmt;
        offerRefi.Total_Loan_Amount__c = totalLA;
        offerRefi.Fee__c = fee;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = feePct;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = 36;
        offerRefi.Opportunity__c = testRefiApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerRefi;
        newOffers.add(offerRefi);
        // Offer for Refinance App - END;

        List<Offer__c> newRefiOffers = EligibilityCheckPostSoftPull.getRefinanceOffers(oldOffers.get(0), newOffers, false, 'B1');

        System.debug(LoggingLevel.INFO, 'get_refinance_offers_same_grade_same_term END');

        for (Offer__c refiOffer : newRefiOffers) {
            System.debug(LoggingLevel.INFO, 'old APR: ' + apr + ', new APR: ' + refiOffer.APR__c);
            System.debug(LoggingLevel.INFO, 'old Term: ' + term + ', new Term: ' + refiOffer.Term__c);
            System.debug(LoggingLevel.INFO, 'old Payment_Amount__c: ' + payAmt + ', new Payment_Amount__c: ' + refiOffer.Payment_Amount__c);
            System.debug(LoggingLevel.INFO, 'old Effective_APR__c: ' + effAPR + ', new Effective_APR__c: ' + refiOffer.Effective_APR__c);
            System.debug(LoggingLevel.INFO, 'old Fee_Percent__c: ' + feePct + ', new Fee_Percent__c: ' + refiOffer.Fee_Percent__c);
            System.debug(LoggingLevel.INFO, 'old Fee__c: ' + fee + ', new Fee__c: ' + refiOffer.Fee__c);
            System.debug(LoggingLevel.INFO, 'old Total_Loan_Amount__c: ' + totalLA + ', new Total_Loan_Amount__c: ' + refiOffer.Total_Loan_Amount__c);
            System.debug(LoggingLevel.INFO, 'old Loan_Amount__c: ' + loanAmt + ', new Loan_Amount__c: ' + refiOffer.Loan_Amount__c);

            System.assert(refiOffer != null);
        }
    }

    @isTest static void gets_refinance_offers_same_grade_diff_term() {
        System.debug(LoggingLevel.INFO, 'get_refinance_offers_same_grade_diff_term START');
        Scorecard__c scoreNew = new Scorecard__c();
        Scorecard__c scoreRefi = new Scorecard__c();
        Offer__c offerNew = new Offer__c();
        Offer__c offerRefi = new Offer__c();
        List<Offer__c> oldOffers = new List<Offer__c>();
        List<Offer__c> newOffers = new List<Offer__c>();
        Boolean hasBetterGrade = false;

        loan__Loan_Account__c testContract = LibraryTest.createContractTH();

        testContract.loan__Principal_Remaining__c = 3182.26;
        testContract.loan__Fees_Remaining__c = 75;
        testContract.loan__Interest_Accrued_Not_Due__c = 7.71;
        testContract.loan__ACH_Next_Debit_Date__c = Date.today().addDays(5);
        update testContract;

        Contact c = [SELECT Id, AccountId FROM Contact WHERE Id = : testContract.loan__Contact__c];

        Opportunity testNewApp = [SELECT   Id,
                                               Fee_Handling__c,
                                               Type,
                                               Contact__c
                                               FROM    Opportunity
                                               WHERE   Id = : testContract.Opportunity__c
                                                       LIMIT 1];

        testNewApp.Fee_Handling__c = FeeUtil.NET_FEE_OUT;
        testNewApp.Type = 'New';
        update testNewApp;

        Opportunity testRefiApp = LibraryTest.createApplicationTH();
        testRefiApp.Fee_Handling__c = FeeUtil.NET_FEE_OUT;
        testRefiApp.Type = 'Refinance';
        testRefiApp.Contract_Renewed__c = testContract.Id;
        update testRefiApp;

        // Offer for New App - BEGIN;
        System.debug(LoggingLevel.INFO, 'offerNew id: ' + offerNew.Id);
        offerNew.Repayment_Frequency__c = 'Monthly';
        offerNew.APR__c = 0.24;
        offerNew.Term__c = 36;
        offerNew.Payment_Amount__c = 190.03;
        offerNew.IsSelected__c = true;
        offerNew.Selected_Date__c = Date.today().addDays(-120);
        offerRefi.Effective_APR__c = 27.690036553792;
        offerNew.Loan_Amount__c = 5000;
        offerNew.Total_Loan_Amount__c = 5250;
        offerNew.Fee__c = 250;
        offerNew.IsCustom__c = false;
        offerNew.Fee_Percent__c = 5;
        offerNew.FirstPaymentDate__c = null;
        offerNew.Installments__c = 36;
        offerNew.Opportunity__c = testNewApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerNew;
        oldOffers.add(offerNew);
        // Offer for New App - END;

        // Offer for Refinance App - BEGIN;
        Double apr = 0.2812;
        Integer term = 24;
        Double payAmt = 276.12;
        Double effAPR = 28.657231352549;
        Double loanAmt = 5000;
        Double totalLA = 5025;
        Double fee = 25;
        Double feePct = 0.5;

        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = apr;
        offerRefi.Term__c = term;
        offerRefi.Payment_Amount__c = payAmt;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = effAPR;
        offerRefi.Loan_Amount__c = loanAmt;
        offerRefi.Total_Loan_Amount__c = totalLA;
        offerRefi.Fee__c = fee;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = feePct;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = term;
        offerRefi.Opportunity__c = testRefiApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerRefi;
        newOffers.add(offerRefi);

        offerRefi = new Offer__c();
        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = apr;
        offerRefi.Term__c = term;
        offerRefi.Payment_Amount__c = payAmt;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = effAPR;
        offerRefi.Loan_Amount__c = loanAmt;
        offerRefi.Total_Loan_Amount__c = totalLA;
        offerRefi.Fee__c = fee;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = feePct;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = term;
        offerRefi.Opportunity__c = testRefiApp.id;
        offerNew.RefinanceFundedOffer__c = false;
        insert offerRefi;
        newOffers.add(offerRefi);
        // Offer for Refinance App - END;

        List<Offer__c> newRefiOffers = EligibilityCheckPostSoftPull.getRefinanceOffers(oldOffers.get(0), newOffers, false, 'B1');

        System.debug(LoggingLevel.INFO, 'get_refinance_offers_same_grade_diff_term END');

        for (Offer__c refiOffer : newRefiOffers) {
            System.debug(LoggingLevel.INFO, 'old APR: ' + apr + ', new APR: ' + refiOffer.APR__c);
            System.debug(LoggingLevel.INFO, 'old Term: ' + term + ', new Term: ' + refiOffer.Term__c);
            System.debug(LoggingLevel.INFO, 'old Payment_Amount__c: ' + payAmt + ', new Payment_Amount__c: ' + refiOffer.Payment_Amount__c);
            System.debug(LoggingLevel.INFO, 'old Effective_APR__c: ' + effAPR + ', new Effective_APR__c: ' + refiOffer.Effective_APR__c);
            System.debug(LoggingLevel.INFO, 'old Fee_Percent__c: ' + feePct + ', new Fee_Percent__c: ' + refiOffer.Fee_Percent__c);
            System.debug(LoggingLevel.INFO, 'old Fee__c: ' + fee + ', new Fee__c: ' + refiOffer.Fee__c);
            System.debug(LoggingLevel.INFO, 'old Total_Loan_Amount__c: ' + totalLA + ', new Total_Loan_Amount__c: ' + refiOffer.Total_Loan_Amount__c);
            System.debug(LoggingLevel.INFO, 'old Loan_Amount__c: ' + loanAmt + ', new Loan_Amount__c: ' + refiOffer.Loan_Amount__c);

            System.assert(refiOffer != null);
        }
    }
    
    @isTest static void checks_payoff_and_loan_amount(){
        Decimal payoff = 12495.4552;
        String feeHandling = 'Net Fee Out';
        
        Opportunity app = LibraryTest.createApplicationTH();
        
        Offer__c offerRefi = new Offer__c();
        offerRefi.Repayment_Frequency__c = 'Monthly';
        offerRefi.APR__c = 20;
        offerRefi.Term__c = 36;
        offerRefi.Payment_Amount__c = 200;
        offerRefi.IsSelected__c = false;
        offerRefi.Effective_APR__c = 30;
        offerRefi.Loan_Amount__c = 12295.45;
        offerRefi.Total_Loan_Amount__c = 12495.45;
        offerRefi.Fee__c = 200;
        offerRefi.IsCustom__c = false;
        offerRefi.Fee_Percent__c = 5;
        offerRefi.FirstPaymentDate__c = null;
        offerRefi.Installments__c = 36;
        offerRefi.Opportunity__c = app.id;
        insert offerRefi;
        
        Boolean result = EligibilityCheckPostSoftPull.checkCurrentLoanPayoff(payoff, offerRefi, feeHandling);
        
        System.assertEquals(true, result);
        
        feeHandling = 'Fee On Top';
        
        offerRefi.Loan_Amount__c = 12495.45;
        offerRefi.Total_Loan_Amount__c = 12695.45;
        update offerRefi;
        
        result = EligibilityCheckPostSoftPull.checkCurrentLoanPayoff(payoff, offerRefi, feeHandling);
        
        System.assertEquals(true, result);
    }
    
}