public class LPM2BorrowerInfoImpl implements loan.CRBorrowerInfoInterface2 {

    private Contact c;
    private Account a;
    
    public void setBorrower(SObject b){
        if (b instanceof Contact) {
            this.c = (Contact) b;
        } else if(b instanceof Account) {
            this.a = (Account) b;
        }
    }
    
    public loan.CRBorrowerInfo getBorrowerInfo(){
        
        return new loan.CRBorrowerInfo('Borrower',
                                c != null ? (c.FirstName != null ? c.FirstName : c.Name) : '',
                                c != null ? (c.LastName != null ? c.LastName : c.Name) : '',
                                '',
                                c != null ? (c.ints__Social_Security_Number__c != null ? c.ints__Social_Security_Number__c : '') : '' ,
                                c != null ? (c.MailingStreet!=null?c.MailingStreet.replace('\n', ''):'') : '',
                                '',
                                c != null ? (c.MailingCity!=null?c.MailingCity.replace('\n', ''):'') : '',
                                c != null ? (c.MailingState!=null?c.MailingState.replace('\n', '').subString(0,2) :'') : '',
                                c != null ? (c.MailingPostalCode!=null?c.MailingPostalCode.replace('\n', '').leftPad(5).replaceAll(' ', '0') :'')                                : '',
                                c != null ? (c.MailingCountry!=null?c.MailingCountry.replace('\n', ''):'') : '',
                                'US',
                                null,
                                c != null ? c.BirthDate : null,
                                c != null ? sanitizePhone(c.Phone) : '',
                                '',
                                null,  
                                null, 
                                null
                                );                              
    }

    private String sanitizePhone(String s) { 
        return s != null ? s.replaceAll('[^0-9]', '') : null;
    }

    public loan.CRBorrowerInfo getCoBorrowerInfo() {
        return null;                          
    }
   
   
   public String getLoanNumber(loan__Loan_Account__c loan){
       return loan.name.replace('-', '');
    }
  
    public String getLoanAccountType(loan__Loan_Account__c loan){
       return '01';     }
  }