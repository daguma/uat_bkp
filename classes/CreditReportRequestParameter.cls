global class CreditReportRequestParameter {

    public String last_name {get; set;}

    public String first_name {get; set;}

    public String ssn {get; set;}

    public String state {get; set;}

    public String city {get; set;}

    public String zip_code {get; set;}

    public String house_nb {get; set;}

    public String pre_Dir {get; set;}

    public String street_type {get; set;}

    public String street {get; set;}

    public String post_Dir {get; set;}

    public String apt_Nbr {get; set;}

    public String unit_Type {get; set;}

    public String pobNbr {get; set;}

    public String Dob_MMDDYYYY {get; set;}

    public String dob_YYYYMMDD {get; set;}

    public String phone_number {get; set;}

    public String phone_area {get; set;}

    public String bureau {get; set;}
    
    //email Age input params
    public String email {get;set;} 
    
    public String ip_address {get;set;} 
    
    public String emailAgeStatusCode {get;set;} //added for Email Age implementation
    //email Age input params
}