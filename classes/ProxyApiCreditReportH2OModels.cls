global class ProxyApiCreditReportH2OModels {

    public String predictionModelName{get;set;}
    public String predictionModelType{get;set;}
    public ProxyApiCreditReportPredictionResult predictionResult{get;set;}
}