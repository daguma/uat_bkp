public with sharing class ParnetAlertForContactCtrl {
  
  public Contact c {get; set;}
  public String partnerName {get; set;}

  public ParnetAlertForContactCtrl(Apexpages.StandardController controller) {
    partnerName = '';
    c = (Contact)controller.getRecord();
        
        List<Opportunity> query = null;
        
        if (Test.isRunningTest()){
            query = [SELECT Id, LeadSource__c, Lead_Sub_Source__c 
                        FROM Opportunity
                        WHERE Contact__c = : c.Id
                        ORDER BY CreatedDate DESC
                        LIMIT 1];
        }
        else{
             query = [SELECT Id, LeadSource__c, Lead_Sub_Source__c 
                        FROM Opportunity
                        WHERE Contact__c = : c.Id
                        AND Status__c NOT IN ('Aged / Cancellation', 'Declined (or Unqualified)')
                        ORDER BY CreatedDate DESC
                        LIMIT 1];
        }
        
    for (Opportunity apps : query){

      if (String.isNotEmpty(apps.LeadSource__c) && apps.LeadSource__c == 'EZVerify')
        partnerName = 'ezCarePoint';
      else if (String.isNotEmpty(apps.Lead_Sub_Source__c) && apps.Lead_Sub_Source__c == 'LoanHero')  
        partnerName = 'LendingPoint Merchant Solutions';
      else 
          partnerName = 'LendingPoint';

      }  
  }
}