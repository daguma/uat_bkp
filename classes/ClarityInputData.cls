global class ClarityInputData {

    public ClarityRequestParameter clarityRequestParameter {get; set;}

    public Long creditReportId {get; set;}
}