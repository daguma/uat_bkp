global with Sharing class ContactCtrl {

    public ContactCtrl() {

    }

    
    @AuraEnabled public Boolean hasErrorMessage { get; set; }
    @AuraEnabled public String errorMessage { get; set; }
    @AuraEnabled public Boolean hasWarningMessage { get; set; }
    @AuraEnabled public String warningMessage { get; set; }
    @AuraEnabled public String popupMessage { get; set; }
    @AuraEnabled List<IdologyUtil.IDologyQuestion> questions { get; set; }
    @AuraEnabled public Contact cont { get; set; }
    @AuraEnabled public String IdNumber { get; set; }
    @AuraEnabled public String iqResult { get; set; }
    @AuraEnabled public String debug { get; set; }
    @AuraEnabled public Boolean callNewEnterprise { get; set; }
    @AuraEnabled public Boolean questionsProceeded { get; set; }
    @AuraEnabled public Boolean questionsRecovered { get; set; }
    @AuraEnabled public Boolean questionsDisplayed { get; set; }
    @AuraEnabled public Boolean questionsSkipped {get; set;}
    @AuraEnabled public String verificationResult { get; set; }
    @AuraEnabled public String payfoneResult { get; set; }
    @AuraEnabled public Boolean callPayfone { get; set; }
    @AuraEnabled public IDology_Request__c request{ get; set; }
    
    public Payfone_Settings__c payfoneSettings;        
    public List<IDology_Request__c> listRequests;
    
    private static IDology_Settings__c settings = IDology_Settings__c.getOrgDefaults();
    
    
    //PAC-3 ***starts***
    private String verifyRequest {get; set;}
    private String verifyResponse {get; set;}
    private String OAuthToken {get;set;}
    private String tokenRequestLabel,phnNumber;
    public String requestLabel='';
    
    public ContactCtrl(String recordId) {
        verifyRequest = '';
        verifyResponse = '';
        OAuthToken = '';
        request = new IDology_Request__c();
        listRequests = new List<IDology_Request__c>();
        this.questionsRecovered = false;
        this.questionsDisplayed = false;
        this.questionsSkipped = false;
        this.callNewEnterprise = false;
        this.callPayfone = false;
        this.verificationResult = '';
        this.payfoneResult = '';
        this.questions = new List<IdologyUtil.IDologyQuestion>();
        this.cont = getContact(recordId);
        this.payfoneSettings = getPayfoneSettings();
        
        this.listRequests = [SELECT Id, Name, IDology_Result__c, Result_Message__c, Debug__c,
                             Contact__c, Questions_Received__c, Qualifiers__c,
                             Verification_Result__c, Verification_Message__c
                             FROM IDology_Request__c
                             WHERE Contact__c = : this.cont.Id
                             order by CreatedDate desc
                             limit 1];
        
        if (this.listRequests != null && this.listRequests.size() > 0) {
            this.questionsProceeded =  true;
            this.verificationResult = this.listRequests[0].IDology_Result__c ;
            if (this.listRequests[0].Result_Message__c != null)
                this.verificationResult += ' - ' + this.listRequests.get(0).Result_Message__c;
        }
        
        Integer expiryDays = 0;
        if (payfoneSettings.Expiry_Days__c != null) {
            expiryDays = Integer.valueof(payfoneSettings.Expiry_Days__c);
            if (expiryDays == 0) expiryDays = expiryDays + 1;
        }
        
        Datetime beforeThirtyDays = DateTime.Now().addDays(-expiryDays);
        
        //check if there any old payfone result
        for (Payfone_Run_History__c payfonelist : [select id, Contact__c,Trust_Score__c,Verify_API_Response__c, Verification_Result__c, MatchCRM_Response__c, GetIntelligence_Response__c, createdDate from Payfone_Run_History__c //PAC-3
                                                   where Contact__c = :this.cont.Id and CreatedDate > : beforeThirtyDays order by CreatedDate desc limit 1]) {
            payfoneResult = payfonelist.Verification_Result__c;
            //check if the existing result has the payfone the null responses for getIntelligence/matchCRM
            if(payfonelist.Trust_Score__c == null  || String.isEmpty(payfonelist.Verify_API_Response__c)) 
                this.callPayfone = true; //PAC-3
        }
        
    }
    
    public void setErrorMessage(String errorMessage ) {
        this.errorMessage = errorMessage;
        this.hasErrorMessage = true;
    }
    
    public void setWarningMessage(String warnMessage ) {
        this.warningMessage = warnMessage;
        this.hasWarningMessage = true;
    }
    
    public void clearMessage() {
        this.warningMessage = '';
        this.errorMessage = '';
        this.hasWarningMessage = false;
        this.hasErrorMessage = false;
    }
    
    /******** PAYFONE BEGINS *********/
    public void tokenRequest() {
        
        if(payfoneSettings != null) {
            try {
                HttpRequest request = VerifyPhoneNumberHelper.getTokenRequest(payfoneSettings);
                Http http = new Http();
                HTTPResponse response = http.send(request);                    
                this.OAuthToken = VerifyPhoneNumberHelper.parseTokenResponse(response);
                
                if(string.isnotempty(this.OAuthToken))
                    callAsyncPayfone();
                else
                    popupMessage = 'ERROR: OAuth token not found';
            }catch(Exception e) {
                popupMessage = 'ERROR: ' + e.getMessage();
            }
        }else {
            popupMessage = 'ERROR: Payfone settings not found.';
        }
    }
    
    public void callAsyncPayfone() {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
        Integer timeout = 5000;
        if (this.payfoneSettings != null && this.payfoneSettings.API_TimeOut__c != null)
            timeout = (Integer) this.payfoneSettings.API_TimeOut__c * 1000;
        
        //calling payfone asynchronously
        if (this.payfoneSettings.Call_Payfone__c && (String.isEmpty(this.payfoneResult) || this.payfoneSettings.Expiry_Days__c == 0 || this.callPayfone)) {
            String requestId = VerifyPhoneNumberHelper.getGUIDString();                       
            this.phnNumber = '';
            
            if(String.isNotBlank(cont.MobilePhone))
                this.phnNumber = VerifyPhoneNumberHelper.normalizeString(cont.MobilePhone);
            else if(String.isNotBlank(cont.Phone))
                this.phnNumber = VerifyPhoneNumberHelper.normalizeString(cont.Phone);   
            
            this.verifyRequest = VerifyPhoneNumberHelper.getRequestForVerifyAPI(payfoneSettings, phnNumber, this.cont, requestId);
            system.debug('--request--'+this.verifyRequest);
            
            //HTTP Payfone request
            HttpRequest req = new HttpRequest();
            req = VerifyPhoneNumberHelper.getVerifyRequest(this.OAuthToken,requestId,this.verifyRequest,payfoneSettings);
            req.setTimeout(timeout);
            
            Http http = new Http();
            HTTPResponse response;
            if(Test.isRunningTest()){
                response = null;

            }else{
                response = http.send(req);
            }

            if(response!=null){
                this.verifyResponse = response.getBody();
                system.debug('--second response--'+response+'---response body--'+this.verifyResponse);
                //parse the response
                system.debug('--status code--'+response.getStatusCode());

                if(string.isnotempty(this.verifyResponse) && response.getStatusCode() == 200) {
                    this.cont = VerifyPhoneNumberHelper.parseVerifyResult(this.verifyResponse, this.cont);

                    if (payfoneSettings.Call_Payfone__c && (String.isEmpty(payfoneResult) || payfoneSettings.Expiry_Days__c == 0 || this.callPayfone )) {
                        this.payfoneResult = VerifyPhoneNumberHelper.decisionUsingPayfone(this.cont);
                        VerifyPhoneNumberHelper.InsertPayfoneHistory(this.cont,this.phnNumber,this.cont.Verify_Description__c,this.payfoneResult,this.verifyRequest,this.verifyResponse);
                    }
                } else { //failled response logging
                    MelisaCtrl.DoLogging('phone verify',this.verifyRequest,string.valueof(response),null,this.cont.Id);
                }
            }

        }
    }
    /******** PAYFONE ENDS *********/
    
    public void recoverQuestions() {
        try {
            if (CheckSettings()) {
                //PAC-3
                if(VerifyPhoneNumberHelper.payfoneSkipIdologyCriteria(payfoneSettings,this.cont))
                    this.callNewEnterprise = true;
                //PAC-3
                
                HTTPResponse res = CallApiToGetQuestions();
                system.debug(res.getBody());
                this.cont.Kyc_Mode__c = '';
                
                if (res.getBody().contains('<error>'))
                    IdologyUtil.SaveIDologyRequest(idNumber, null, 0, 'ERROR', res.getBody().substring(res.getBody().indexOf('<error>') + 7, res.getBody().indexOf('</error>')), this.cont.Id, this.debug);
                else {
                    ProcessResultXml(res.getBodyDocument().getRootElement());
                    MelisaCtrl.DoLogging('IdologyRecoverQuestions', res.getBodyDocument().toXmlString(), '', '' , String.valueOf(this.cont.Id));
                }
                System.debug('CONTACT : ' + this.cont);
                upsert this.cont;
                this.questionsProceeded = false;
                
                if (this.questionsSkipped == true && this.cont.kyc_mode__c == IdologyUtil.PAYFONE_WO_QUES) //MAINT-708 fix
                    popupMessage = 'INFO: Applicant has passed KYC using Payfone, there are no challenge questions to be answered at this time. ' +
                                      'Please continue further with the application.';
            }else {
                popupMessage = 'WARNING: Check Settings Failed';
            }
        } catch (Exception e) {
            popupMessage = 'ERROR: ' + e.getMessage();
        }
    }
    
    public HTTPResponse CallApiToGetQuestions() {
        addDebugValue('Request to recover IDology Questions initiated at: ' + System.now().format());
        HTTPResponse res;
        if(Test.isRunningTest()){
            res = null;
        }else{
            res = IdologyUtil.CallApiToGetQuestions(this.cont, this.callNewEnterprise);
        }

        if(res!=null){
            if (res.getBody().contains('<error>')) {
                setErrorMessage(res.getBody().substring(res.getBody().indexOf('<error>') + 7, res.getBody().indexOf('</error>')));
                addDebugValue('Request resulted in an error: ' + res.getBody().substring(res.getBody().indexOf('<error>') + 7, res.getBody().indexOf('</error>')));
                addDebugValue('Request ended at: ' + System.now().format());
            }
        }

        return res;
    }
    
    public void ProcessResultXml(Dom.XMLNode response) {
        Boolean updateIdologyRequest = false; //MAINT-606
        if (response != null) {
            IdologyUtil.IdologyResult theRes = IdologyUtil.ProcessResultXML(response);
            System.debug(theRes);
            addDebugValue('Request Summary Result: ' + theRes.summaryResult);
            addDebugValue('Request result: ' + theRes.result);
            addDebugValue('Request questions: ' + theRes.numquestions);
            addDebugValue('Request ID Number: ' + theRes.idNumber);
            addDebugValue('Request Qualifiers: ' + theRes.qualifiers.replaceAll('<li>', '').replaceAll('</li>', ', '));
            addDebugValue('Request Error: "' + theRes.Error + '"');
            addDebugValue('Request ended at: ' + System.now().format());
            this.questions = theRes.Questions;
            this.IdNumber = theRes.IdNumber;
            if (theRes.expectIdResult != '')
                saveExpectIdresult(theRes.expectIdResult, this.cont.Id);
            
            this.request = IdologyUtil.SaveIDologyRequest(theRes.idNumber, theRes.qualifiers,
                                                          theRes.numquestions, theRes.summaryResult,
                                                          'Pending answers', this.cont.Id, this.debug);
            System.debug(this.request);
            
             //MAINT-606
            if(theRes.numquestions == 0) {
                if(this.callNewEnterprise) this.questionsSkipped = true;
            }
            else {
                this.questionsRecovered = true;
                this.questionsDisplayed = true;
            } //MAINT-606
            
            if (theRes.summaryResult == 'Pass' && String.isEmpty(theRes.Error)) {
                if (this.callNewEnterprise && theRes.numquestions == 0) { //MAINT-606
                    this.cont.kyc_mode__c = String.isEmpty(theRes.qualifiers) ? IdologyUtil.PAYFONE_WO_QUES : IdologyUtil.IDOLOGY_WO_QUES;
                   
                    //MAINT-606
                    this.request.Verification_Result__c = this.payfoneResult;
                    this.request.Result_Message__c = '';
                    updateIdologyRequest  = true; //MAINT-606
                } else {
                    
                    //set the kyc mode to payfone with challenge questions
                    this.cont.kyc_mode__c = getKYCPassBy(); //if the idology count is more than one means this is the second attempt
                    
                    if (String.isNotEmpty(this.cont.kyc_mode__c) && this.cont.kyc_mode__c.contains('Payfone')) {
                        //update the result with verification result as pass
                        this.request.Verification_Result__c = this.payfoneResult;
                        updateIdologyRequest = true; //MAINT-606
                    } /*MAINT-606*/ else if(String.isEmpty(this.cont.kyc_mode__c) && theRes.numquestions == 0) {
                        this.cont.kyc_mode__c = IdologyUtil.IDOLOGY_WO_QUES;
                        this.request.Verification_Result__c = 'Pass';
                        updateIdologyRequest = true; //MAINT-606
                    } //MAINT-606
                    
                } //MAINT-606
            } else if(theRes.summaryResult == 'Fail' && String.isEmpty(theRes.Error) && theRes.result.equalsIgnoreCase('ID Located')) {
                if(theRes.numquestions > 0) {
                    this.cont.kyc_mode__c = getKYCPassBy(); //if the idology count is more than one means this is the second attempt
                        
                    if (this.cont.kyc_mode__c.contains('Payfone')) {
                        //update the result with verification result as pass
                        this.request.Verification_Result__c = this.payfoneResult;
                        updateIdologyRequest = true; //MAINT-606
                    }
                } else if(this.callNewEnterprise && theRes.numquestions == 0) {
                    this.cont.kyc_mode__c = IdologyUtil.PAYFONE_WO_QUES;
                    this.request.Verification_Result__c = this.payfoneResult;
                    this.request.Result_Message__c = '';
                    updateIdologyRequest  = true; //MAINT-606
                }
            } //MAINT-606
            else
                popupMessage = 'WARNING: IDology returned a negative result. \n' + 
                               'Reasons: ' + theRes.qualifiers + ' \n' +
                               'Error: ' + theRes.Error;
            if(updateIdologyRequest && this.request <> null) update this.request; //MAINT-606
        } else {
            popupMessage = 'ERROR: Bad response from Idology server';
        }
    }
    
    public String getKYCPassBy() {
        datetime beforeThirtyDays = DateTime.Now().addDays(-30);
        
        List<Idology_Request__c> idologyResults = [select id, Contact__c, CreatedDate, Verification_Result_Front__c, IDology_Result__c from Idology_Request__c where Contact__c = : this.cont.Id and
                                                   (Verification_Result_Front__c = 'Fail' or ((IDology_Result__c = 'Pass' or Questions_Received__c > 0) and Verification_Result_Front__c != 'Pass' )) and
                                                   CreatedDate > :beforeThirtyDays order by CreatedDate desc];
        
        if (idologyResults.size() > 1)
            if (this.PayfoneResult == 'Pass' && payfoneSettings.Payfone_In_Use__c && payfoneSettings.Call_Payfone__c )
            this.cont.Kyc_Mode__c = IdologyUtil.PAYFONE_W_QUES;
        
        return this.cont.Kyc_Mode__c;
    }
    
    public void cancelButton() {
        if (this.questionsRecovered && !this.questionsProceeded && String.isNotEmpty(this.payfoneResult)) {
            if (!this.questionsSkipped && payfoneSettings.Payfone_In_Use__c && payfoneSettings.Call_Payfone__c)
                this.cont.Kyc_Mode__c = getKYCPassBy();
            
            if (String.isnotempty(this.cont.Kyc_Mode__c) && this.cont.Kyc_Mode__c.contains('Payfone') && payfoneSettings.Payfone_In_Use__c && payfoneSettings.Call_Payfone__c) { //MAINT-520 bug fix with MAINT-606
                this.request.Verification_Result__c = this.payfoneResult;
                update this.request;
            }

            if (String.isNotEmpty(this.cont.Kyc_Mode__c))
                update this.cont;
            
        }
    }
    
    public void processQuestions() {
        try {
            if (CheckAnswersSettings(this.IDNumber)) {
                HTTPResponse res = IdologyUtil.CallApiToProcessQuestions(this.IDNumber, this.questions, this.callNewEnterprise);
                if (res != null) {
                    ProcessAnswersResultXml(res.getBodyDocument().getRootElement());
                    MelisaCtrl.DoLogging('IdologyProcessQuestions', res.getBodyDocument().toXmlString(), '', null, this.cont.Id);
                    upsert this.cont;
                }
            } else
                popupMessage = 'ERROR: Idology Questions Issue - CheckAnswersSettings: No Request ID Found';
        } catch (Exception e) {
            setErrorMessage(e.getMessage());
        }
    }
    
    public void ProcessAnswersResultXml(Dom.XMLNode response) {
        try {
            IdologyUtil.IDologyResult theRes = IdologyUtil.ProcessAnswersResultXml(response);
            addDebugValue('Validation Summary Result: ' + theRes.summaryResult);
            addDebugValue('Validation result: ' + theRes.result);
            addDebugValue('Validation ID Number: ' + theRes.idNumber);
            addDebugValue('Validation Answers Received: ' + theRes.noAnswers);
            addDebugValue('Validation IQ Result: ' + theRes.iqResultMessage);
            addDebugValue('Validation Error: ' + theRes.Error);
            addDebugValue('Validation ended at: ' + System.now().format());
            this.iqresult = theRes.iqResult;
            this.cont.Kyc_Mode__c = '';
            this.cont.Kyc_Mode__c = (this.iqResult == 'pass') ? IdologyUtil.IDOLOGY_W_QUES : getKYCPassBy();
            
            if (String.isNotEmpty(theRes.Error) && !theRes.Error.contains('Too few answers submitted for IQ type'))
                popupMessage = 'WARNING: Question Validation Failed \n\n' +
                               'Result: ' + theRes.iqResult + '\n' + 
                               'ID LiveQ Result: ' + theRes.iqResultMessage + '\n' +
                               'ID Error: ' + theRes.Error;
            
            else {
                String errorStr = '';
                if (this.cont.Kyc_Mode__c.contains('Payfone') && payfoneSettings.Payfone_In_Use__c && payfoneSettings.Call_Payfone__c )
                    popupMessage  = 'SUCCESS: Verification Completed \n\n ' +
                                    'Result: KYC ' + this.payfoneResult;
                else {
                    if (theRes.Error.contains('Too few answers submitted for IQ type'))
                        popupMessage = 'WARNINIG: Question Validation Failed \n\n' +
                                       'Result: ' + this.iqResult + ' \n' + 
                                       'ID LiveQ Result: ' + theRes.iqResultMessage + '\n' +
                                       'ID Error: ' + theRes.Error ;
                    else
                        popupMessage = 'SUCCESS: Question Validation Completed \n\n ' +
                                       'Result: ' + this.iqResult + '\n' + 
                                       'ID LiveQ Result: ' + theRes.iqResultMessage;
                }
            }
            
            this.verificationResult = this.iqResult + ' - ' + theRes.iqResultMessage;
            this.questionsProceeded = true;
            this.questionsRecovered = false;
            this.questionsDisplayed = false;
            questions = new List<IdologyUtil.IDologyQuestion>();
            this.request.Verification_Result__c = this.iqResult;
            this.request.Verification_Result_Front__c = this.iqResult;
            if (cont.Kyc_Mode__c.contains('Payfone') && payfoneSettings.Payfone_In_Use__c && payfoneSettings.Call_Payfone__c)
                this.request.Verification_Result__c = this.payfoneResult;
            else
                this.request.Verification_Result__c = this.iqResult;
            
            this.request.Verification_Message__c = theRes.iqResultMessage;
            this.request.Result_Message__c = verificationResult;
            this.request.Debug__c = this.debug;
            
            upsert this.request;
        } catch (Exception e) {
            popupMessage  = 'ERROR: Idology ProcessAnswersResultXml ERROR: ' + e.getMessage();
        }
    }
    
    public void addDebugValue(String value) {
        if (this.debug == null)
            this.debug = '';
        this.debug += (this.debug.length() > 0 ? '\n' : '') + value;
    }
    
    @AuraEnabled
    public static String submitApplicaiton(String contactID, String BirthDate) {
        
        String response = '';
        try {
            if(String.isNotBlank(contactID) && String.isNotBlank(BirthDate)) {
                Date dob = Date.valueOf(BirthDate);
                Integer totalDays = dob.daysBetween(Date.Today());
                Integer age = Integer.valueOf(totalDays/365);
                Integer minAge = Integer.valueOf(System.Label.minimum_birth_age);

                Boolean retVal = SubmitApplicationService.verifyBridger(contactID);

                if (retVal && age >= minAge) {
                    String appID = ContactToApplicationConverter.ContactToApp(contactID);
                    DateTime start = System.now();
                    
                    if (appID.startsWith('006')) {
                        response = '{"hasError": false, "message": "' + appID + '"}';                        
                    }else {
                        response = '{"hasError": true, "message": "' + appID.replaceAll('\\n', '=>') + '"}';
                    }
                } else if (age < minAge) {
                    response = '{"hasError": true, "message": "Minimum age must be "' + minAge + '" years."}';
                } else {
                    response = '{"hasError": true, "message": "OFAC verification failed. Please review the CSI result."}';
                }
            }else {
                response = '{"hasError": true, "message": "Please check Birthdate and Contact."}';
            }
        }catch(Exception e) {
            response = '{"hasError": true, "message": "' + e.getMessage() + '"}';
        }
        System.debug(response);
        return response;
    }
    
    @AuraEnabled
    public static void callSoftPull(String appID,String contactID) {
        string product;
        ProxyApiCreditReportEntity creditReportEntity;
        
        ezVerify_Settings__c ezSettings = ezVerify_Settings__c.getInstance();
        opportunity app= [select id, ProductName__c, Lending_Product__r.Name, Lending_Product__c, Contact__r.Product__c, Contact__c from opportunity where id=:appId];
        if(app <> null)
            product = string.isNotEmpty(app.ProductName__c) ? app.ProductName__c : (app.Lending_Product__c != null && string.isNotEmpty(app.Lending_Product__r.Name) ? app.Lending_Product__r.Name: (app.Contact__c != null ? app.Contact__r.Product__c : ''));
        if(string.isNotEmpty(product) && string.isNotEmpty(ezSettings.Product_Name__c) &&  generalPurposeWebServices.isPointOfNeed(product))
            creditReportEntity = CreditReport.getCreditReportWSfromContact(appID, false,false);
        else
            creditReportEntity = CreditReport.getCreditReportWSfromContact(appID, false,true);
        
        String inputdata = JSON.serialize(CreditReport.mapGDSRequestParameter(appID, false, true));
        
        //MER-133
        List<id> oppList= new List<id>();
        oppList.add(appID);
        MAP<id, decimal> OppMap = new MAP<id, decimal>();
        List<offer__c> offList= new list<Offer__c>();
        OppMap = MelisaCtrl.returnMaxLoanAmount(oppList, offList);
        if(!oppMap.isEmpty() && OppMap.containsKey(appID)){
            Opportunity opp= new opportunity(id=appID,Maximum_Loan_Amount__c =OppMap.get(appID));
            update opp;
            
        }
        
        if(creditReportEntity <> null && creditReportEntity.brms <> null) {
            if(creditReportEntity.brms.emailAgeResponse <> null)
                EmailAgeStorageController.storeEmailAgeInSfObject(creditReportEntity.brms.emailAgeResponse, contactID, appID, false);
            MelisaCtrl.DoLogging('BRMS logging-Soft Pull',inputData, JSON.serialize(creditReportEntity.brms),null,contactID);
        }
    }
    
    @AuraEnabled
    public static String verifyEmail(String contactID) {
        String personalInfo = '{"contactId" :"' + contactID + '","leadId" : null,"check" : "email"}'; 
        String response = MelisaCtrl.DymistNew(personalInfo);
        return response;
    }
    
    @AuraEnabled
    public static String markIncomeNotEnough(String contactID) {
        String response = 'Contact ID not found';
        System.debug(contactID);
        if(String.isNotBlank(contactID)) {
            response = markAsIncomeNotEnough(contactID);
        }
        return response;
    }
    
    @AuraEnabled
    public static void refreshFICOInfo(String contactID) {
        GenerateOffersWSDL.RefreshFicoInfo(contactID);
    }
    
    @AuraEnabled
    public static void resetAuthSSNCount(String contactID) {
        ThirdPartyDetails.resetSSNAuthenticationCount(contactID);
    }
    
    @AuraEnabled
    public static String CreateUserforCustomerPortal(String contactID) {
        String response = ThirdPartyDetails.processDetails(contactID);
        return response;
    }
    
    @AuraEnabled
    public static ContactCtrl getContactEntity(String recordId) {
        ContactCtrl contactEntity = new ContactCtrl(recordId);
        return contactEntity;
    }
    
    @AuraEnabled
    public static String processQuestions(String EntityStr) {
        ContactCtrl conEntity = (ContactCtrl) System.JSON.deserializeStrict(EntityStr, ContactCtrl.class);
        conEntity.cont = conEntity.getContact(conEntity.cont.Id);
        conEntity.payfoneSettings = getPayfoneSettings();
        conEntity.clearMessage(); //clear old errors and warnings
        conEntity.processQuestions();
        return conEntity.popupMessage;
    }
    
    @AuraEnabled
    public static ContactCtrl payfoneRequest(String EntityStr) {
        ContactCtrl conEntity = (ContactCtrl) System.JSON.deserializeStrict(EntityStr, ContactCtrl.class);
        conEntity.cont = conEntity.getContact(conEntity.cont.Id);
        conEntity.payfoneSettings = getPayfoneSettings();
        conEntity.clearMessage(); //clear old errors and warnings
        conEntity.tokenRequest();
        return conEntity;
    }
    
    @AuraEnabled
    public static void cancelQuestionsProcess(String EntityStr) {
        ContactCtrl conEntity = (ContactCtrl) System.JSON.deserializeStrict(EntityStr, ContactCtrl.class);
        conEntity.cont = conEntity.getContact(conEntity.cont.Id);
        conEntity.payfoneSettings = getPayfoneSettings();
        try {
            conEntity.cancelButton(); 
        }Catch(Exception e) {
            throw new AuraHandledException('Error while cancelling : ' + e.getMessage());
        }
    }
    
    @AuraEnabled
    public static ContactCtrl recoverQuestions(String EntityStr) {
        ContactCtrl conEntity = (ContactCtrl) System.JSON.deserializeStrict(EntityStr, ContactCtrl.class);
        conEntity.cont = conEntity.getContact(conEntity.cont.Id);
        conEntity.payfoneSettings = getPayfoneSettings();
        conEntity.clearMessage(); //clear old errors and warnings
        conEntity.recoverQuestions();
        return conEntity;
    }
    
    @AuraEnabled
    public static void SSNPopulate(Contact cont) {
        
        String SSN4, SSN9EnteredByUser;
        String fullSSN = cont.ints__Social_Security_Number__c;
        cont.SSN__c = fullSSN;
        
        if (cont.ints__Social_Security_Number__c <> null && cont.ints__Social_Security_Number__c.length() > 4) {
            if (cont.ints__Social_Security_Number__c.length() == 9)
                SSN9EnteredByUser = cont.ints__Social_Security_Number__c;
            
            SSN4 = cont.ints__Social_Security_Number__c.substring(cont.ints__Social_Security_Number__c.length() - 4, cont.ints__Social_Security_Number__c.length());
        } else
            SSN4 = cont.ints__Social_Security_Number__c;
        
        string FormtedPhone = cont.Phone.replaceAll('[|,|.|\\,||"||:|~|!|@|#|$|%|^|&|*|_|+|=|<|>|?|\\(|\\)|\\{|\\}|\\;|\\\'"-]', ' ');
        string fmtPhone = '';
        for (String s : FormtedPhone.split(' ')) {
            fmtPhone = fmtPhone + s;
        }
        
        Map<string, string> JsonMap = new Map<string, string>();
        JsonMap.put('ZipCode', cont.MailingPostalCode);
        JsonMap.put('SSN4', SSN4);
        JsonMap.put('FirstName', cont.FirstName);
        JsonMap.put('LastName', cont.LastName);
        JsonMap.put('DOB', String.valueOf(cont.Birthdate));
        JsonMap.put('Address', cont.MailingStreet);
        JsonMap.put('City', cont.MailingCity);
        JsonMap.put('State', cont.MailingState);
        JsonMap.put('Phone', fmtPhone);
        JsonMap.put('Email', cont.Email);
        string JSONReq = JSON.serializePretty(JsonMap);
        
        string SSN9 = ThirdPartyDetails.Resolve360(JSONReq);
        Map<string, string> SSNMap = (Map<String, String>) JSON.deserialize(SSN9, Map<String, String>.class);
        
        if (SSNMap.ContainsKey('SSN9'))
            cont.ints__Social_Security_Number__c = SSNMap.get('SSN9');
        
        if (String.isEmpty(cont.ints__Social_Security_Number__c) ) {
            if (String.isNotEmpty(SSN9EnteredByUser))
                cont.ints__Social_Security_Number__c = SSN9EnteredByUser;
            else {
                throw new AuraHandledException('Please enter 9 digit SSN');
            }
        }
        
        try {
            update cont;
        }catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public Boolean CheckAnswersSettings(String idNumber) {
        if (Test.isRunningTest()) {
            settings = new IDology_Settings__c();
            settings.Username__c = 'notEmpty';
            settings.Password__c = 'notEmpty';
        }
        return (String.isNotEmpty(settings.Username__c) &&
                String.isNotEmpty(settings.Password__c) &&
                String.isNotEmpty(idNumber));
    }
    
    public Boolean CheckSettings() {
        if (this.cont.Id != null) {
            if (Test.isRunningTest()) {
                settings = new IDology_Settings__c();
                settings.Username__c = 'notEmpty';
                settings.Password__c = 'notEmpty';
            }
            system.debug('DG TEST: ' + settings);
            if (settings != null) {
                return (String.isNotEmpty(settings.Username__c)
                        && String.isNotEmpty(settings.Password__c)
                        && String.isNotEmpty(this.cont.FirstName)
                        && String.isNotEmpty(this.cont.LastName)
                        && String.isNotEmpty(this.cont.MailingStreet)
                        && String.isNotEmpty(this.cont.MailingCity)
                        && String.isNotEmpty(this.cont.MailingState)
                        && String.isNotEmpty(this.cont.MailingPostalCode));
            } else {
                System.debug('Error');
                setErrorMessage('No IDology settings were found. Please contact your system administrator.');
                this.addDebugValue('No IDology settings were found. Please contact your system administrator.');
            }
        }
        return false;
    }
    
    public static void saveExpectIdresult(String expectIdMessage, String ContactID) {
        List<Opportunity> applist = [SELECT id FROM Opportunity WHERE Contact__c = : ContactID ORDER BY CreatedDate DESC LIMIT 1];
        if (!applist.isEmpty()) {
            Opportunity app = applist[0];
            app.ExpectID_Result__c = expectIdMessage;
            update app;
        }
    }
    
    public Contact getContact(String recordId) {
        Contact con = [SELECT Id, FirstName, LastName, Email, Phone, MobilePhone, Birthdate, MailingStreet, 
                       MailingPostalCode, MailingCity, MailingState, Kyc_Mode__c, SSN__c, ints__Social_Security_Number__c, 
                       Do_Not_Contact__c, Lead_Sub_Source__c, Show_Regrade_Warning__c, Verify_Description__c , Trust_Score__c, 
                       Payfone_Verified_Flag__c, last_Name_score__c, first_name_score__c, address_score__c, Street_Number_Score__c,
                       EmpInfo_Employment_Status__c,EmpInfo_Verification_Called__c
                       FROM Contact WHERE Id =: recordId];
        return con;
    }
    
    public static Payfone_Settings__c getPayfoneSettings() {
        return Payfone_Settings__c.getValues('Payfone Request Params');
    }
    
    Webservice static string  markAsIncomeNotEnough(String ContactID ) {
        String response = 'Contact Not Found!';
        for (Contact con : [Select Id, AccountID from Contact where Id = : ContactID LIMIT 1]) {
            response = 'Opportunity Not Found!';
            for (Opportunity opp :  [Select Id, AccountId, CreatedDate, StageName, Probability, Lost_Reason__c
                                     from Opportunity
                                     where StageName != 'Closed Lost' and AccountID = : con.AccountID
                                             order by CreatedDate Desc LIMIT 1]) {
                opp.Lost_Reason__c = 'Does Not Meet Minimum Income Requirement';
                opp.StageName = 'Closed Lost';
                update opp;
                response = 'Opportunity was closed and marked as Not Enough income!';
            }

            Note nte = new Note();
            nte.title = 'Insufficient Income Button Was Clicked';
            nte.parentId = con.Id;
            insert nte;
        }
        return response;
    }
    @AuraEnabled
    Webservice static String callCancelVerification(String entityId) {
        return EmpInfoHelper.cancelVerificationOrder(entityId);
        
    }
    @invocablemethod(label='callQueryVerificationOrder')
    public static void callQueryVerificationOrder(List<id> cntid){        
        EmpInfoHelper.queryVerificationOrder(cntid);
    }
}