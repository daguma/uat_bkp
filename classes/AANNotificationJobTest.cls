@isTest public class AANNotificationJobTest {

    @isTest static void executes_job() {
        Test.startTest();
        AANNotificationJob notification_job = new AANNotificationJob();
        String cron = '0 0 23 * * ?';
        system.schedule('Test Sched', cron, notification_job);
        Test.stopTest();
  }

    @isTest static void executes_aan_notification() {

        Database.BatchableContext bc;
        
        try {

            OrgWideEmailAddress noRe;
            EmailTemplate eTemplate;
            string AAN_TEMPLATE = 'LP_AAN_Notification';

            try {
                noRe = [select Id, DisplayName from OrgWideEmailAddress where DisplayName = 'LendingPoint-NoReply' ];
                System.assert(true);
            } catch (Exception e) {
                System.debug ('[U-03] Unable to locate OrgWideEmailAddress using name: ' + 'LendingPoint-NoReply' + ' refer to Setup | Email Communcations ' );
                System.assert(true);
            }

            try {
                eTemplate = [select id, name, HtmlValue, Subject, Body from EmailTemplate where developername = : AAN_TEMPLATE];
                System.assert(true);
            } catch (Exception e) {
                System.debug ('[U-03] Unable to locate EmailTemplate using name: ' + AAN_TEMPLATE + ' refer to Setup | Communications Templates ' );
                System.assert(true);
            }

            opportunity aplcn = LibraryTest.createApplicationTH();

            aplcn.status__c = 'Declined (or Unqualified)';
            aplcn.Decline_Note__c = 'Did not pass disqualifiers';
            aplcn.Type = 'New';

            update aplcn;

            opportunity aplcnToSelect = [select id, Contact__c, Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id];
      
             
            List<opportunity> listApps = new List<opportunity>();
            
            Test.setCreatedDate(aplcn.Id, Datetime.now().addDays(-5));
            
            Test.startTest();
            
            listApps.add(aplcn);

            Set<Id> apss = new Set<Id>();
            apss.add(aplcnToSelect.Id);

            AANNotificationJob notification_job = new AANNotificationJob();

            notification_job.execute(null);   
            
            notification_job.execute(bc, listApps);

            Test.stopTest();
            
            List<Note> decNote = [Select Id from Note ];
            //System.assert(decNote.size() > 0, 'No decline notice note was inserted!');


            List<Outbound_Emails_Temp__c> decEmail = [Select Id from Outbound_Emails_Temp__c ];
            //System.assert(decEmail.size() > 0, 'No Outbound Email was inserted!');
            
        } catch (Exception e) {
            System.debug( System.LoggingLevel.ERROR, 'AANNotificationJob: validate: Problems generating the instance ' + e.getMessage() + '\n' + e.getLineNumber() + '\n' + e.getStackTraceString());
            System.assert(false, 'AANNotificationJob: validate: Problems generating the instance ' + e.getMessage() + '\n' + e.getLineNumber() + '\n' + e.getStackTraceString());
        }
    }

    @isTest static void replaces_values_from_data() {
        try {
            opportunity aplcn = LibraryTest.createApplicationTH();

            opportunity aplcnToSelect = [select id, Contact__c, Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id];

            Contact cntct = [Select Id, Name from Contact where Id = : aplcnToSelect.Contact__c];

            cntct.Email = 'test@gmail2.com';
            cntct.Point_Code__c = '1234AB';
            cntct.Phone = '1251231233';
            cntct.MailingCountry = 'US';
            cntct.FirstName = 'Test Name';
            cntct.Lastname = 'Testcase';
            cntct.BirthDate = Date.Today().addYears(-30);
            cntct.Employment_Start_date__c = Date.Today().addYears(-3);
            cntct.SSN__c = '999999999';
            cntct.Mailingstreet = 'Test street';
            cntct.MailingCity = 'City';
            cntct.MailingPostalCode = '123132';
            cntct.Loan_Amount__c = 4000;
            cntct.Annual_Income__c = 100000;
            cntct.Time_at_current_Address__c = Date.Today().addYears(-3);
            cntct.MailingState = 'GA';

            Opportunity opp = new Opportunity();
            opp.Name = 'APP-Test Opportunity';
            opp.CloseDate = Date.today().addDays(5);
            opp.StageName = 'Open';
            opp.Type = 'New';
            insert opp;

            DateTime testDate = System.now();

            EmailTemplate eTemplate;
            string AAN_TEMPLATE = 'LP_AAN_Notification';

            try {
                Test.startTest();
                eTemplate = [select id, name, HtmlValue, Subject, Body from EmailTemplate where developername = : AAN_TEMPLATE];

                AANNotificationJob notification_job = new AANNotificationJob();
                ProxyApiCreditReportEntity creditReportEntity = LibraryTest.fakeCreditReportEntity(aplcn.id);
                String result = notification_job.replaceValuesFromData(eTemplate.body, cntct, creditReportEntity, opp,  true);

                Test.stopTest();
                
                //System.assertNotEquals(null, result);
                System.debug(result);
            } catch (Exception e) {
                System.debug ('[U-03] Unable to locate EmailTemplate');
            }

        } catch (Exception e) {
            System.debug('AANNotificationJob: replaceValuesFromData: ' + e.getMessage());
        }
    }


    @isTest static void creates_an_email() {
        contact cntct = LibraryTest.createContactTH();
        AANNotificationJob notification_job = new AANNotificationJob();
        Outbound_Emails_Temp__c result = notification_job.createEmail('html body', 'test subject', cntct.id);
        Outbound_Emails_Temp__c c = new Outbound_Emails_Temp__c();
        c.Contact__c = cntct.id;
        c.Body__c = 'html body';
        c.Subject__c = 'test subject';
        //System.assertEquals(c, result);
    }

    @isTest static void creates_a_note() {
        contact cntct = LibraryTest.createContactTH();
        AANNotificationJob notification_job = new AANNotificationJob();
        Note result = notification_job.createNote('html body', 'test subject', cntct.id);
        Note c = new Note();
        c.ParentId = cntct.id;
        c.Body = 'html body';
        c.Title = 'test subject';
        //System.assertEquals(c, result);
    }
    
    @isTest static void replaces_values_from_data_brms() {
        try {
            LibraryTest.createBrmsConfigSettings(-1,false);
            LibraryTest.brmsRulesConfig();
            LibraryTest.createBrmsRuleOnScreenConfig();
            
            opportunity aplcn = LibraryTest.createApplicationTH();

            opportunity aplcnToSelect = [select id, Contact__c, Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id];

            Contact cntct = [Select Id, Name from Contact where Id = : aplcnToSelect.Contact__c];

            cntct.Email = 'test@gmail2.com';
            cntct.Point_Code__c = '1234AB';
            cntct.Phone = '1251231233';
            cntct.MailingCountry = 'US';
            cntct.FirstName = 'Test Name';
            cntct.Lastname = 'Testcase';
            cntct.BirthDate = Date.Today().addYears(-30);
            cntct.Employment_Start_date__c = Date.Today().addYears(-3);
            cntct.SSN__c = '999999999';
            cntct.Mailingstreet = 'Test street';
            cntct.MailingCity = 'City';
            cntct.MailingPostalCode = '123132';
            cntct.Loan_Amount__c = 4000;
            cntct.Annual_Income__c = 100000;
            cntct.Time_at_current_Address__c = Date.Today().addYears(-3);
            cntct.MailingState = 'GA';

            Opportunity opp = new Opportunity();
            opp.Name = 'APP-Test Opportunity';
            opp.CloseDate = Date.today().addDays(5);
            opp.StageName = 'Open';
            opp.Type = 'New';

            insert opp;

            DateTime testDate = System.now();

            EmailTemplate eTemplate;
            string AAN_TEMPLATE = 'LP_AAN_Notification';

            try {
                Test.startTest();
                eTemplate = [select id, name, HtmlValue, Subject, Body from EmailTemplate where developername = : AAN_TEMPLATE];

                AANNotificationJob notification_job = new AANNotificationJob();
                String response = LibraryTest.fakeBrmsRulesResponse(aplcn.id,true,'1',true);
                ProxyApiCreditReportEntity creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(response, ProxyApiCreditReportEntity.class);
                String result = notification_job.replaceValuesFromData(eTemplate.body, cntct, creditReportEntity, opp,  true);

                Test.stopTest();
                
                //System.assertNotEquals(null, result);
                System.debug(result);
            } catch (Exception e) {
                System.debug ('[U-03] Unable to locate EmailTemplate');
            }

        } catch (Exception e) {
            System.debug('AANNotificationJob: replaceValuesFromData: ' + e.getMessage());
        }
    }

}