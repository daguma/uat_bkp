@isTest public class GenerateOffersWSDLTest {
    @testSetup
    static void testData(){
        Loan_Hero_Settings__c LHSetting= new Loan_Hero_Settings__c(Default_Bank_Name__c='N/A',Sub_Source__c='LoanHero');
    }
    @isTest static void validate_variables() {
        System.assertEquals(0, GenerateOffersWSDL.RequestedLoanAmount);
        System.assertEquals(0, GenerateOffersWSDL.MaxLoanAmount);
        System.assertEquals(0, GenerateOffersWSDL.MaxPaymentAmount);
        System.assertEquals(0, GenerateOffersWSDL.AnnualIncomeAmount);
        System.assertEquals(null, GenerateOffersWSDL.offers);
        System.assertEquals(null, GenerateOffersWSDL.app);
    }

    @isTest static void OfferList() {

        Decimal ap = 0.1;
        Decimal fe = 0.1;
        Date expDate = Date.today().addDays(2);
        string appName = 'test';
        string offerid = 'id_test';
        Decimal aprPer = 0.1;
        Decimal LoanAmnt = 3500;
        Decimal PayAmnt = 350;
        Decimal trm = 0.1;
        Boolean IsSelctd = true;
        Decimal inRate = 0.1;
        Decimal effectApr = 0.1;
        String estTotalFinanceCharge = '0.1';
        String criteria = 'test';
        String offerCode = 'test';
        String offerType = 'test';
        GenerateOffersWSDL.OfferList offer = new GenerateOffersWSDL.OfferList( ap,  fe,  expDate,  appName,  offerid,  aprPer,  LoanAmnt,  PayAmnt,  trm,  IsSelctd,  inRate,  effectApr,true,estTotalFinanceCharge,criteria,offerCode , offerType );
       
    }

    @isTest static void TrackOffers() {

        MaskSensitiveDataSettings__c ma = new MaskSensitiveDataSettings__c();
        ma.MaskAllData__c = false;
        insert ma;
        
        List<Contact> ContactListBefore;
        List<Contact> ContactListAfter;
        opportunity app = LibraryTest.createApplicationTH();
        contactListBefore = [select id, name, Offer_First_Seen__c, Offer_Last_Seen__c from contact where id in (select Contact__c  from opportunity where id = : app.id)];
        GenerateOffersWSDL.TrackOffers(app.id);
        contactListAfter = [select id, name, Offer_First_Seen__c, Offer_Last_Seen__c from contact where id in (select Contact__c  from opportunity where id = : app.id)];
        System.assertNotEquals(contactListAfter, contactListBefore);

    }

    @isTest static void checkVerification() {

        opportunity app = LibraryTest.createApplicationTH();
        
        Contact cnt =  LibraryTest.createContactTH();
        cnt.Email = 'offersme@gmail.com';

        Offer__c offer = new Offer__c();
        offer.Loan_Amount__c = 1000;
        offer.Payment_Amount__c = 100;
        offer.IsSelected__c = false;
        offer.Fee_Percent__c = 0.2;
        offer.Selected_Date__c = Date.Today().addDays(-1);
        offer.Installments__c = 10;
        offer.Term__c = 12;
        offer.APR__c = 0.1;
        offer.Fee__c = 0.2;
        offer.Repayment_Frequency__c = 'Weekly';        

        insert offer;
        
        GenerateOffersWSDL.createDummyOffer(24,1,100,2000,100,app,'test');
        
        GenerateOffersWSDL.createDummyOffer(26,  0.2283,  22.83,   3500 ,   168.837 ,app,'test');
        
        String result = GenerateOffersWSDL.CheckVerification(offer.Id);
        System.debug(result);

        offer.opportunity__c = app.id;
        update offer;

        result = GenerateOffersWSDL.CheckVerification(offer.Id);
        System.debug(result);

        Contact c = [SELECT Id, Name, FT_Demyst_Response__c from Contact WHERE Id = : app.Contact__c ];

        c.FT_Demyst_Response__c = 'Y';
        update c;

        result = GenerateOffersWSDL.CheckVerification(offer.Id);
        System.debug(result);

        c.FT_Demyst_Response__c = 'N';
        update c;

        result = GenerateOffersWSDL.CheckVerification(offer.Id);
        System.debug(result);

    }

    @isTest static void saveOffer() {
        opportunity app = LibraryTest.createApplicationTH();
        Contact c = [Select Id, MailingState from Contact LIMIT 1];
        c.Clear_Relevance__c = 0;
        update c;
        LP_Custom__c cuSettings = [Select Id from LP_Custom__c limit 1] ;
        cuSettings.States_To_Assign_Immediately__c = c.MailingState;
        update cuSettings;
        Offer__c offer = new Offer__c();
        offer.opportunity__c = app.id;
        offer.Loan_Amount__c = 1000;
        offer.Payment_Amount__c = 100;
        offer.IsSelected__c = false;
        offer.Fee_Percent__c = 0.2;
        offer.Selected_Date__c = Date.Today().addDays(-1);
        offer.Installments__c = 10;
        offer.Term__c = 12;
        offer.APR__c = 0.1;
        offer.Fee__c = 0.2;
        offer.Repayment_Frequency__c = 'Weekly';

        insert offer;

        Map<String, String> offerdetails = new Map<String, String>();
        offerdetails.put('offerId', offer.Id);
        offerdetails.put('offerSelectSrc', '');

        String offerdetailsS = '{"offerId":"' + offer.Id + '","offerSelectSrc":"", "subSource":"partnerSubSource" }';

        String result = GenerateOffersWSDL.SaveOffer(offerdetailsS, true);
        System.debug(result);

        result = GenerateOffersWSDL.SaveOffer(offerdetailsS, false);
        System.debug(result);

        offerdetailsS = '{"offerId":"' + offer.Id + '","offerSelectSrc":"","subSource" : "LPCustomerPortal"}';
        result = GenerateOffersWSDL.SaveOffer(offer.Id, false);
        System.debug(result);

    }

    @isTest static void RefreshFicoInfo() {
        opportunity app = LibraryTest.createApplicationTH();

        Contact c = [SELECT Id, Name from Contact WHERE Id = : app.Contact__c ];

        Scorecard__c sc = new Scorecard__c();
        sc.FICO_Score__c = Decimal.valueOf('0.1');
        sc.DTI__c = Decimal.valueOf('0');
        sc.Revolving_Utilization__c = '0%';
        sc.opportunity__c = app.Id;

        insert sc;

        GenerateOffersWSDL.RefreshFicoInfo(c.Id);

    }
 

    @isTest static void TestGenerateOffers() {

        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';

        insert cuSettings;

        contact cntct = LibraryTest.createContactTH();
        cntct.Lastname = 'Test';
        cntct.FirstName = 'Antoer';
        cntct.Annual_Income__c = 10000;
        cntct.Phone = '5552323232';
        cntct.email = 'test@test42424.com';
        cntct.Loan_Amount__c  = 23000;
        cntct.Prior_Loan_Amount__c = 10000; 
        cntct.Lead_Sub_Source__c = 'LendingPoint';
        upsert cntct;

        Account acc=new Account(Name='test' ,Type='Partner',Allow_Offers__c=true,Display_only_Effective_APR__c=true,URL_AAN_Process__c=true,Suppress_AAN__c=true,Delayed_hours_to_send_AAN__c=12, AAN_Email_Template__c='CK AAN Notification', Is_Parent_Partner__c=true,Is_child_Partner__c=false);
        acc.Partner_Sub_Source__c = 'LPCU';
        acc.Cobranding_Name__c = 'Test TestGenerateOffers';
        insert acc;
        opportunity aplcn = new opportunity();
        aplcn.Contact__c  = cntct.id;
        aplcn.Maximum_Loan_Amount__c = 10000;
        aplcn.Maximum_Payment_Amount__c = 12000;
        aplcn.Status__c = 'Credit Qualified';
        aplcn.Partner_Account__c=acc.id;
        aplcn.Name = cntct.FirstName + ' ' + cntct.LastName;
        aplcn.CloseDate = system.today();
        aplcn.StageName = 'Qualification';
        aplcn.Type = 'New';
        insert aplcn;
        
        GenerateOffersWSDL.updateOpportunity(JSON.serialize(aplcn));        
     
        //opportunity aplcnToSelect = [select id, Contact__c , Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id];

        Offer__c off = new Offer__c();
        off.opportunity__c = aplcn.id;
        off.Fee__c = 500;
        off.APR__c = 0.2399;
        off.Payment_Amount__c = 250;
        off.Loan_Amount__c = 4655;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Effective_APR__c = .3299;
        off.Term__c = 24;
        off.Installments__c = 24;
        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;



        String result = GenerateOffersWSDL.GenerateOffers(null, cntct.id);

        aplcn.Maximum_Loan_Amount__c = 3600;
        aplcn.Maximum_Payment_Amount__c = 200;
        update aplcn;

        result = GenerateOffersWSDL.GenerateOffers(null, cntct.id);

        System.debug(result);

      /***  aplcn.Maximum_Loan_Amount__c = 10000;
        update aplcn;

        result = GenerateOffersWSDL.GenerateOffers(null, cntct.id);

        System.debug(result);

        aplcn.Maximum_Payment_Amount__c = 12000;
        update aplcn;

       // aplcnToSelect = [select id, Contact__c , Contact__r.Annual_Income__c , Maximum_Loan_Amount__c , Maximum_Payment_Amount__c  from opportunity where id = : aplcn.id];
        aplcn.Status__c = 'Credit Qualified';
        update aplcn;***/
        /*Finwise_Details__c FinwiseParams = new Finwise_Details__c(name='finwise',Finwise_Base_URL__c='https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/',Apply_State_Restriction__c=false,
                                      Finwise_LendingPartnerId__c='5',Finwise_LendingPartnerProductId__c='7',
                                      Finwise_LoanStatusId__c='1',finwise_partner_key__c='testKey',Finwise_product_Key__c='testkey',
                                      Finwise_RateTypeId__c='1',Whitelisted_States__c='AL,MO',Investor__c = 'LendingPoint SPE 2');
       insert FinwiseParams;
        result = GenerateOffersWSDL.ShowOffers('{"applicationId":"' + aplcn.id + '"}');

        System.debug(result);
         */

       /* cntct.Prior_Loan_Amount__c = 12999;
        update cntct;
        result = GenerateOffersWSDL.ShowOffers(aplcn.id);

        System.debug(result);*/
        /*aplcn.AAN_Notification_Trigger_Date__c=system.today();
        update aplcn;
        result = GenerateOffersWSDL.ShowOffers(aplcn.id);*/
 
        cntct.Title = 'CONVERTED TO APP 06-03';
        update cntct;
        GenerateOffersWSDL.CreateCRs(); 

    }

    @isTest static void TestGenerateOffers2() {

        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        cuSettings.States_To_Assign_Immediately__c = 'GA, NJ';
        insert cuSettings;
       
        //Create Lead
        Lead Testlead = LibraryTest.createLeadTH();

        Account TestAcc = new Account(name = 'TestCase', Cobranding_Name__c='TestGenerateOffers2');
        insert TestAcc;

        Contact TestContact = LibraryTest.createContactTH();
        TestContact.mailingState = 'GA';
        TestContact.Accountid = TestAcc.id;
        TestContact.Lead__c = TestLead.id;
        TestContact.Point_Code__c='1234'; 
        TestContact.Lead_Sub_Source__c = 'LendingPoint';

        update TestContact;
        
        Campaign c = new Campaign(Name='Test');
        insert c;
        
        CampaignMember newMember = new CampaignMember(CampaignId =c.id, ContactId = testContact.Id, Point_Code__c='1234');
        insert newMember;
        
        testlead.Do_Not_Contact__c=true;
        update testlead;
        
       /* Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(TestLead.id);
        lc.setAccountId(TestAcc.id);
        lc.setContactId(TestContact.Id);
        lc.setConvertedStatus('Application Submitted');
        Database.LeadConvertResult lcr = Database.convertLead(lc);*/

        opportunity a = LibraryTest.createApplicationTH();
        a.Contact__c  = TestContact.id;
       // a.Status__c = 'Credit Qualified';
        a.Status__c = 'Pending Credit Data';
        a.LeadSource__c='LoanHero';
        a.Maximum_Loan_Amount__c = 20000;
        a.Lead_Sub_Source__c = 'LoanHero';
        update a;
        
        
       
        Offer__c off = new Offer__c();
        off.opportunity__c = a.id;
        off.Fee__c = 350;
        off.APR__c = 0.230;
        off.Payment_Amount__c = 250;
        off.Loan_Amount__c = 5000;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Installments__c = 36;
        off.Term__c = 36;
        off.Fee__c = 0;
        off.Effective_APR__c = .3299;

        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;

        string result = GenerateOffersWSDL.GenerateOffers(Testlead.id, null);
        System.debug(result);

        //GenerateOffersWSDL.ShowOffers('{"applicationId":""}');
        GenerateOffersWSDL.ShowOffers('{"applicationId":"' + a.id + '"}');

        Contact cntct = new contact();
        cntct.Firstname = 'TestF';
        cntct.Lastname = 'Test2';
        cntct.MailingState = 'GA';
        cntct.Annual_Income__c = 12000;
        insert cntct;
        
        Campaign c1 = new Campaign(Name='Test');
        insert c1;
        
        CampaignMember newMember1 = new CampaignMember(CampaignId =c.id, ContactId = testContact.Id, Point_Code__c='1234');
        
        result = GenerateOffersWSDL.GenerateOffers(null, cntct.id);
        //system.AssertNotEquals(null,result);        

    }
     @isTest static void TestGenerateOffers3() {

        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        insert cuSettings;
        
        //Create Lead
        Lead Testlead = LibraryTest.createLeadTH();
        
        Account TestAcc =new Account(Name='TestCase' ,Type='Partner',Allow_Offers__c=false,Display_only_Effective_APR__c=true,URL_AAN_Process__c=true,Suppress_AAN__c=true,Delayed_hours_to_send_AAN__c=12, AAN_Email_Template__c='CK AAN Notification', Is_Parent_Partner__c=true,Is_child_Partner__c=false);
        TestAcc.Partner_Sub_Source__c = 'LPCU';
        TestAcc.Cobranding_Name__c = 'TestGenerateOffers3';
        insert TestAcc ;

        Contact TestContact = LibraryTest.createContactTH();
        TestContact.mailingState = 'NJ';
        TestContact.Accountid = TestAcc.id;
        TestContact.Lead__c = TestLead.id;
        TestContact.Point_Code__c='1234';
        TestContact.email='test@sample.com';
        TestContact.Preferred_Language__c = 'spanish';
        update TestContact;
        
        Campaign c = new Campaign(Name='Test');
        insert c;
        
        CampaignMember newMember = new CampaignMember(CampaignId =c.id, ContactId = testContact.Id, Point_Code__c='1234');
        insert newMember;
        
        testlead.Do_Not_Contact__c=true;
        update testlead;
        test.starttest();
        
        /*Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(TestLead.id);
        lc.setAccountId(TestAcc.id);
        lc.setContactId(TestContact.Id);
        lc.setConvertedStatus('Application Submitted');
        Database.LeadConvertResult lcr = Database.convertLead(lc);*/

        opportunity a = LibraryTest.createApplicationTH();
        a.Contact__c  = TestContact.id;
        a.Status__c = 'Aged / Cancellation';
        a.Reason_of_Opportunity_Status__c = 'Test';
        a.Decline_Note__c = 'Testl';
        a.LeadSource__c='LPCustomerPortal';
        a.Maximum_Loan_Amount__c = 20000;
        a.Partner_Account__c=TestAcc.id;
        update a;
        
      //  string result=GenerateOffersWSDL.ShowOffers('{"applicationId":"' + a.id + '"}');  
                     
        string request= '{"desEncryptedAppID":"2SjfaBNmm9BTfPy9F3CRzJiwnaO8gPqs","ezVerifyEmailFlag":"false","applicationId":"' + a.id + '"}'; 
        GenerateOffersWSDL.sendEmailWithLink(TestContact.id, 'test', 'dummyUrl','test',500,3); 
        GenerateOffersWSDL.sendEmailWithLink('test', 'test', 'dummyUrl','test',500,6); 
        //Account act = new Account();
        //act.name = 'Test Partner Account';
        //insert act;
        GenerateOffersWSDL.generatePersonalizedURL(a,'test','test','test','test','test',TestAcc);  
              
        test.stoptest();
        //system.assertNotEquals(null,result);
        }
    @isTest static void TestGenerateOffers4() {
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        insert cuSettings;
        
        //Create Lead
        Lead Testlead = LibraryTest.createLeadTH();
        
        Account TestAcc =new Account(Name='TestCase' ,Type='Partner',Allow_Offers__c=false,Display_only_Effective_APR__c=true,URL_AAN_Process__c=true,Suppress_AAN__c=true,Delayed_hours_to_send_AAN__c=12, AAN_Email_Template__c='CK AAN Notification', Is_Parent_Partner__c=true,Is_child_Partner__c=false);
        TestAcc.Partner_Sub_Source__c = 'LPCU';
        TestAcc.Cobranding_Name__c = 'TestGenerateOffers4';
        insert TestAcc ;

        Contact TestContact = LibraryTest.createContactTH();
        TestContact.mailingState = 'NJ';
        TestContact.Accountid = TestAcc.id;
        TestContact.Lead__c = TestLead.id;
        TestContact.Point_Code__c='1234';
        TestContact.email='test@sample.com';
        update TestContact;
        
    EmailTemplate em = [Select id, Name, HTMLValue, subject, body from EmailTemplate limit 1];
    //Contact TestContact = LibraryTest.createContactTH();
    GenerateOffersWSDL.sendEmailWithLink(String.valueOf(TestContact.id), 'test', 'dummyUrl',em.Name,500,6); 
    
    
        Campaign c = new Campaign(Name='Test');
        insert c;
        
        CampaignMember newMember = new CampaignMember(CampaignId =c.id, ContactId = testContact.Id, Point_Code__c='1234');
        insert newMember;
        
        testlead.Do_Not_Contact__c=true;
        update testlead;
        test.starttest();
        
       /* Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(TestLead.id);
        lc.setAccountId(TestAcc.id);
        lc.setContactId(TestContact.Id);
        lc.setConvertedStatus('Application Submitted');
        Database.LeadConvertResult lcr = Database.convertLead(lc);*/

        opportunity a = LibraryTest.createApplicationTH();
        a.Contact__c  = TestContact.id;
        a.Status__c = 'Aged / Cancellation';
        a.Reason_of_Opportunity_Status__c = 'Test';
        a.Decline_Note__c = 'Testl';
        a.LeadSource__c='LPCustomerPortal';
        a.Maximum_Loan_Amount__c = 20000;
        a.Partner_Account__c=TestAcc.id;
        update a;
        /*
        Offer__c off = new Offer__c();
        off.opportunity__c = a.id;
        off.Fee__c = 350;
        off.APR__c = 0.230;
        off.Payment_Amount__c = 250;
        off.Loan_Amount__c = 5000;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Installments__c = 36;
        off.Term__c = 36;
        off.Fee__c = 0;
        off.Effective_APR__c = .3299;

        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;
        */
       /* Finwise_Details__c FinwiseParams = new Finwise_Details__c(name='finwise',Finwise_Base_URL__c='https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/',Apply_State_Restriction__c=false,
                                      Finwise_LendingPartnerId__c='5',Finwise_LendingPartnerProductId__c='7',
                                      Finwise_LoanStatusId__c='1',finwise_partner_key__c='testKey',Finwise_product_Key__c='testkey',
                                      Finwise_RateTypeId__c='1',Whitelisted_States__c='AL,MO',Investor__c = 'LendingPoint SPE 2');
       insert FinwiseParams;     */
       
        GenerateOffersWSDL.ShowOffers('{"applicationId":"' + a.id + '"}');
        
        test.stoptest();
        
    }
    
    @isTest static void TestGenerateOffers5() {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        cuSettings.States_To_Assign_Immediately__c = 'CA';
        insert cuSettings;
        
        Fintech_Custom_Setting__c fintechsettings = Fintech_Custom_Setting__c.getOrgDefaults();
        fintechsettings.Sub_Source__c = 'PartnerSubSource';
        insert fintechsettings;
        
        //Create Lead
        Lead Testlead = LibraryTest.createLeadTH();
        
        Account TestAcc =new Account(Name='TestCase' ,Type='Partner',Allow_Offers__c=false,Display_only_Effective_APR__c=true,URL_AAN_Process__c=true,Suppress_AAN__c=true,Delayed_hours_to_send_AAN__c=12, AAN_Email_Template__c='CK AAN Notification', Is_Parent_Partner__c=true,Is_child_Partner__c=false);
        TestAcc.Partner_Sub_Source__c = 'LendingTree';
        TestAcc.Force_CQ_if_Phone_Available__c = true;
        TestAcc.Contact_Restricted_Partner__c = false;
        TestAcc.Validate_Work_Phone__c = true;
        TestAcc.Cobranding_Name__c = 'TestGenerateOffers5';
        TestAcc.Return_Grade__c = false;
        TestAcc.Force_CQ_if_Phone_Available__c = true;
        insert TestAcc ;

        
        
        Contact TestContact = LibraryTest.createContactTH();
        TestContact.mailingState = 'NJ';
        TestContact.Accountid = TestAcc.id;
        TestContact.Lead__c = TestLead.id;
        TestContact.Point_Code__c='1234';
        TestContact.Email = 'testoffersme@gmail.com';
        TestContact.Work_Email_Address__c = 'work1q@gmail.com';
        TestContact.Phone = '9988876765';
        TestContact.Lead_Sub_Source__c = 'Google';
        TestContact.Preferred_Language__c = 'spanish';
        TestContact.Lead_Sub_Source__c = 'PartnerSubSource';
        update TestContact;
        
        
        
        EmailTemplate em = [Select id, Name, HTMLValue, subject, body from EmailTemplate limit 1];
        //Contact TestContact = LibraryTest.createContactTH();
        GenerateOffersWSDL.sendEmailWithLink(String.valueOf(TestContact.id), 'notqual', 'dummyUrl',em.Name,500,3); 
        
        GenerateOffersWSDL.sendEmailWithLink(String.valueOf(TestContact.id), 'notqual', 'dummyUrl',em.Name,500,3); 
    
    
        Campaign c = new Campaign(Name='Test');
        insert c;
        
        CampaignMember newMember = new CampaignMember(CampaignId =c.id, ContactId = testContact.Id, Point_Code__c='1234');
        insert newMember;
        
        testlead.Do_Not_Contact__c=true;
        update testlead;
        test.starttest();

        opportunity a = LibraryTest.createApplicationTH();
        a.Contact__c = TestContact.id;
        a.Status__c = 'Credit Approved - No Contact Allowed';
        a.Decline_Note__c = 'Testl';
        a.LeadSource__c='LPCustomerPortal';
        a.Maximum_Loan_Amount__c = 20000;
        a.Partner_Account__c=TestAcc.id;
        a.Deal_is_Ready_to_Fund__c = true;
        a.Partner_Account__c = TestAcc.Id;
        a.ProductName__c = 'Lending Point';
        a.Lead_Sub_Source__c = 'LendingTree';
        update a;
        
        System.debug('Preferred_Language__c:: ' + a.Preferred_Language__c);
        Offer__c off = new Offer__c();
        off.Opportunity__c = a.id;
        off.Fee__c = 350;
        off.APR__c = 0.230;
        off.Payment_Amount__c = 250;
        off.Loan_Amount__c = 5000;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Installments__c = 36;
        off.Term__c = 36;
        off.Fee__c = 0;
        off.Effective_APR__c = .3299;
        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;
        List<id> appList= new List<id>();
        appList.add(a.id);
        Map<String,String> reqMap = new Map<String,String>();
        reqMap.put('applicationId', a.id);
        reqMap.put('desEncryptedAppID', '2SjfaBNmm9BTfPy9F3CRzJiwnaO8gPqs');
        reqMap.put('ezVerifyEmailFlag', 'true');
        GenerateOffersWSDL.ShowOffers(JSON.serialize(reqMap));
        //GenerateOffersWSDL.ShowOffers('{"applicationId":"' + a.id + '"}');
        GenerateOffersWSDL.fundingDecision(appList);
        
        test.stoptest();
        
    }   
    
     @isTest static void TestGenerateOffers6() {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        cuSettings.States_To_Assign_Immediately__c = 'CA';
        insert cuSettings;
        
        Fintech_Custom_Setting__c fintechsettings = Fintech_Custom_Setting__c.getOrgDefaults();
        fintechsettings.Sub_Source__c = 'PartnerSubSource';
        insert fintechsettings;
        
        //Create Lead
        Lead Testlead = LibraryTest.createLeadTH();
        
        Account TestAcc =new Account(Name='TestCase' ,Type='Partner',Allow_Offers__c=false,Display_only_Effective_APR__c=true,URL_AAN_Process__c=true,Suppress_AAN__c=true,Delayed_hours_to_send_AAN__c=12, AAN_Email_Template__c='CK AAN Notification', Is_Parent_Partner__c=true,Is_child_Partner__c=false);
        TestAcc.Partner_Sub_Source__c = 'LendingTree';
        TestAcc.Force_CQ_if_Phone_Available__c = true;
        TestAcc.Contact_Restricted_Partner__c = false;
        TestAcc.Validate_Work_Phone__c = true;
        TestAcc.Cobranding_Name__c = 'TestGenerateOffers5';
        TestAcc.Return_Grade__c = false;
        TestAcc.Force_CQ_if_Phone_Available__c = true;
        TestAcc.Subsequent_offer_not_returned__c = True;
        insert TestAcc ;
        
        Account TestAcc1 =new Account(Name='TestCase2' ,Type='Partner',Allow_Offers__c=false,Display_only_Effective_APR__c=true,URL_AAN_Process__c=true,Suppress_AAN__c=true,Delayed_hours_to_send_AAN__c=12, AAN_Email_Template__c='CK AAN Notification', Is_Parent_Partner__c=true,Is_child_Partner__c=false);
        TestAcc1.Partner_Sub_Source__c = 'MotoLoan';
        TestAcc1.Force_CQ_if_Phone_Available__c = true;
        TestAcc1.Contact_Restricted_Partner__c = false;
        TestAcc1.Validate_Work_Phone__c = true;
        TestAcc1.Cobranding_Name__c = 'TestGenerateOffers5';
        TestAcc1.Return_Grade__c = false;
        TestAcc1.Force_CQ_if_Phone_Available__c = true;
        TestAcc1.Subsequent_offer_not_returned__c = True;
        insert TestAcc1 ;
        
        Contact TestContact = LibraryTest.createContactTH();
        TestContact.mailingState = 'NJ';
        TestContact.Accountid = TestAcc.id;
        TestContact.Lead__c = TestLead.id;
        TestContact.Point_Code__c='1234';
        TestContact.Email = 'testoffersme@gmail.com';
        TestContact.Work_Email_Address__c = 'work1q@gmail.com';
        TestContact.Phone = '9988876765';
        TestContact.Lead_Sub_Source__c = 'Google';
        TestContact.Preferred_Language__c = 'spanish';
        TestContact.Lead_Sub_Source__c = 'PartnerSubSource';
        update TestContact;
        
        EmailTemplate em = [Select id, Name, HTMLValue, subject, body from EmailTemplate limit 1];
        //Contact TestContact = LibraryTest.createContactTH();
        GenerateOffersWSDL.sendEmailWithLink(String.valueOf(TestContact.id), 'notqual', 'dummyUrl',em.Name,500,3); 
        
        GenerateOffersWSDL.sendEmailWithLink(String.valueOf(TestContact.id), 'notqual', 'dummyUrl',em.Name,500,3); 
    
    
        Campaign c = new Campaign(Name='Test');
        insert c;
        
        CampaignMember newMember = new CampaignMember(CampaignId =c.id, ContactId = testContact.Id, Point_Code__c='1234');
        insert newMember;
        
        testlead.Do_Not_Contact__c=true;
        update testlead;
        test.starttest();

        opportunity a = LibraryTest.createApplicationTH();
        a.Contact__c = TestContact.id;
        a.Status__c = 'Aged / Cancellation';
        a.Reason_of_Opportunity_Status__c = 'Test';
        a.Decline_Note__c = 'Testl';
        a.LeadSource__c='LPCustomerPortal';
        a.Maximum_Loan_Amount__c = 20000;
        a.Partner_Account__c=TestAcc.id;
        a.Deal_is_Ready_to_Fund__c = true;
        a.Partner_Account__c = TestAcc.Id;
        a.ProductName__c = 'Lending Point';
        a.Lead_Sub_Source__c = 'MotoLoan';
        update a;
        
        list<Unique_Code_Hit__c> uchList = new List<Unique_Code_Hit__c>();
        Unique_Code_Hit__c uch = new Unique_Code_Hit__c();
        uch.Sub_Source__c = 'Lending Tree';
        uch.Opportunity__c = a.id;
        uchList.add(uch);
        Unique_Code_Hit__c uch1 = new Unique_Code_Hit__c();
        uch1.Sub_Source__c = 'MotoLoan';
        uch1.Opportunity__c = a.id;
        uchList.add(uch1);
        insert uchList; 
        
        TestContact.Lead_Sub_Source__c = 'MotoLoan';
        update TestContact;
        
        a.Lead_Sub_Source__c = 'MotoLoan';
        a.Selected_Offer_Id__c = '';
        a.Partner_Account__c = TestAcc1.id;
        update a;
        
        System.debug('Preferred_Language__c:: ' + a.Preferred_Language__c);
        Offer__c off = new Offer__c();
        off.Opportunity__c = a.id;
        off.Fee__c = 350;
        off.APR__c = 0.230;
        off.Payment_Amount__c = 250;
        off.Loan_Amount__c = 5000;
        off.IsSelected__c = true;
        off.Selected_date__c = Date.today();
        off.Installments__c = 36;
        off.Term__c = 36;
        off.Fee__c = 0;
        off.Effective_APR__c = .3299;
        off.Repayment_Frequency__c = 'Monthly';
        off.Fee_Percent__c = 10;
        insert off;
        List<id> appList= new List<id>();
        appList.add(a.id);
        Map<String,String> reqMap = new Map<String,String>();
        reqMap.put('applicationId', a.id);
        reqMap.put('desEncryptedAppID', '2SjfaBNmm9BTfPy9F3CRzJiwnaO8gPqs');
        reqMap.put('ezVerifyEmailFlag', 'true');
        GenerateOffersWSDL.ShowOffers(JSON.serialize(reqMap));
        //GenerateOffersWSDL.ShowOffers('{"applicationId":"' + a.id + '"}');
        GenerateOffersWSDL.fundingDecision(appList);
        
        test.stoptest();
        
    }   
}