public class DL_DeleteActivityByUserReport
{
	public DL_DeleteActivityByUserReport()
	{
		try
		{
			//Empty current report
			DL_ActivityByUserReport__c[] currentReport = [SELECT
				Id
			FROM
				DL_ActivityByUserReport__c];
			System.debug('Delete report: ' + currentReport);
			Delete currentReport;
		}
		catch(Exception e)
		{
			// Process exception here
			System.debug('Exception: ' + e.getStackTraceString());
		}
	}
}