global class AutoDebitBatchController implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts,HttpCalloutMock
{
    public static final String CRON_EXPR =  '0 0 7 * * ? *';
    String query = ''; 
    global final String option;
    Map<String, Integer> reportResults = new Map<String, Integer>();  

    global AutoDebitBatchController(String option) {
		this.option = option;

		if (option == 'AutoDebitProcess') {
            this.query = AutoDebitBatchControllerHelper.AUTO_DEBIT_PROCESS;
		} else if (option == 'OneTimeDebitProcess') {
            this.query = AutoDebitBatchControllerHelper.ONE_TIME_DEBIT_PROCESS_QUERY;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext ctx) {
        // This is good practice if you are likely to be processing a
        // lot of records at a time

        if (this.option == 'AutoDebitProcess') {
            AutoDebitBatchController b = new AutoDebitBatchController(this.option);
            Database.executeBatch(b,1);
        }
    }
    
    global void execute(Database.BatchableContext BC, List<Object> scope) {
        try{
            if (option == 'AutoDebitProcess') {
               reportResults = AutoDebitBatchControllerHelper.executeAutoDebitProcess((List<loan__loan_account__c>) scope);
            } else if(option == 'OneTimeDebitProcess'){
                for (Object s : scope) {
                    loan__Other_Transaction__c param = (loan__Other_Transaction__c) s;
                    AutoDebitBatchControllerHelper.executeOneTimeDebitProcess(param);
                }          
            }   
        }catch(Exception e){
            System.debug('ERROR '+ e.getMessage());
        }      
    }

    global void finish(Database.BatchableContext bc){
        if(this.option == 'AutoDebitProcess'){
            WebToSFDC.notifyDev('AutoDebitBatchController Finished', 'Payments charged = ' + reportResults.get('count') + '\nErrors = ' +reportResults.get('errors') + '\n\n',  UserInfo.getUserId() );
            System.debug('AutoDebitBatchController Finished!');

            AutoDebitBatchController job = new AutoDebitBatchController('OneTimeDebitProcess');
            System.scheduleBatch(job, 'OneTimeDebitProcess', 1, 1);
        }

    }
     global static HTTPResponse respond(HTTPRequest req) {
         // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"SC":"400","accessToken":"test"}');
        res.setStatusCode(200);
        return res;
       
    }

    global static String scheduleJob(String option) {
        AutoDebitBatchController job = new AutoDebitBatchController(option);
        return System.schedule('AutoDebitBatchController job', CRON_EXPR, job);
    }
    
    /*
    public Messaging.SingleEmailMessage createEmail(String sendTo,loan__Loan_Account__c obLoanobLoan){
        templatelist
        string mailBody = String.isEmpty(template.htmlValue) ? '' : template.htmlValue;
        mailBody = obLoan.Name != null ? mailBody.replace('[LAI]', obLoan.Name) : mailBody;
    
        mailBody = string.valueOf(resMap.get('EM')) != null ? mailBody.replace('[Reason]', string.valueOf(resMap.get('EM'))) : mailBody.replace('[Reason]', '');
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> toAddresses = new List<String>();
        toAddresses.add(obLoan.loan__Contact__r.email);
        toAddresses.add(Label.Customer_Service_Email);
        if (orgWideEmailAddress.size() > 0 ) mail.setOrgWideEmailAddressId(orgWideEmailAddress[0].Id); 
        mail.setToAddresses(toAddresses);
        mail.setSubject('Autodebit transaction fail');
        //mail.setHtmlBody('Your transaction for '+ obLoan.Name+' has been failed due to '+string.valueOf(resMap.get('EM')));
        mail.setHtmlBody(mailBody);
        
        return mail
    }
    */
}