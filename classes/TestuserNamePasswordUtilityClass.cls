@IsTest
public class TestuserNamePasswordUtilityClass { 
 
  @IsTest public static void sendUserToCustomerPortal(){
      Java_API_Settings__c JS =  new Java_API_Settings__c(Name='JavaSettings',User_Create_Endpoint__c='http://testapi.lendingpoint.com/CustomerPortalAPI/api/user');
      insert JS;
   
      Contact TestContact = TestHelper.createContact();
      Opportunity aplcn= new Opportunity();
      aplcn.Contact__c= TestContact.id;
      aplcn.ACH_Bank_Name__c='Dummy Bank';
      aplcn.CloseDate = system.today();
      aplcn.StageName = 'Qualification';
      aplcn.Name = 'test1';
aplcn.Type = 'New';

      insert aplcn;
      userNamePasswordUtilityClass.sendUserToCustomerPortal(aplcn.id);
  }
   @IsTest public static void hitUserCreateAPI(){
      Contact TestContact = TestHelper.createContact();
      TestContact.email='test@test.com';
      TestContact.firstName='testname';
      TestContact.lastName='testlastname'; 
      update TestContact;
      Opportunity aplcn= new Opportunity();
      aplcn.Contact__c= TestContact.id;
      aplcn.ACH_Bank_Name__c='Dummy Bank';
      aplcn.Customer_Portal_Status__c='testing';
      aplcn.Contact__c=TestContact.id;
      aplcn.CloseDate = system.today();
      aplcn.StageName = 'Qualification';
      aplcn.Name = 'test12';
                 aplcn.Type = 'New';

      insert aplcn;
      userNamePasswordUtilityClass.hitUserCreateAPI('1212',aplcn.id,'http://testendpoint.com');
  }
  
  @IsTest public static void updateApp(){
      Contact TestContact = TestHelper.createContact();
      Opportunity aplcn= new Opportunity();
      aplcn.Contact__c= TestContact.id;
      aplcn.ACH_Bank_Name__c='Dummy Bank';
      aplcn.CloseDate = system.today();
      aplcn.StageName = 'Qualification';
      aplcn.Name = 'test123';
                  aplcn.Type = 'New';

      insert aplcn;
      userNamePasswordUtilityClass.updateApp(aplcn,'testing');
  }
  
    @IsTest public static void sendpassword(){
     Contact TestContact = TestHelper.createContact();
      userNamePasswordUtilityClass.sendpassword(TestContact.id,'testsfdc@test.com','test123','http://test.com');
  }
  
   @IsTest public static void getPassword(){
      userNamePasswordUtilityClass.getPassword();
  }
  @IsTest public static void createMerchant(){
    Mule_Settings__c muleDetails = Mule_Settings__c.getInstance();
    muleDetails.Application_Token__c = 'd2aeb73a3a4e48';
    muleDetails.Authorization__c = 'ea798cafbe9142538756f7613a4200a3a15cf44b7cbf400b9040046073f43128';
    muleDetails.Base_URL__c = 'https://io.lendingpoint.com';
    muleDetails.Dev_Application_Token__c = '61a9f06971af096b';
    muleDetails.Dev_Authorization__c = 'f96e4d7ca5654216b2882a7e30275b0fb8c9ee24e5b04d728534c8a6b484a676';
    muleDetails.Dev_Base_URL__c = 'https://iodev.lendingpoint.com';
    muleDetails.Environment__c = 'sandbox';
    muleDetails.Role_Id__c= 14;
    muleDetails.Update_Merchant_EndPoint__c = '/api-manager/users/';
    muleDetails.User_Application_Id__c = 9;
    muleDetails.User_Create_Endpoint__c = '/api-manager/users';
    insert muleDetails;
    
    Profile pf= [Select Id from profile where Name='System Administrator']; 
    String orgId=UserInfo.getOrganizationId();
    String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
    Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000));
    String uniqueName=orgId+dateString+RandomId;
    User uu=new User(firstname = 'ABC',
                     lastName = 'XYZ',
                     email = uniqueName + '@test' + orgId + '.org',
                     Username = uniqueName + '@test' + orgId + '.org',
                     EmailEncodingKey = 'ISO-8859-1',
                     Alias = uniqueName.substring(18, 23),
                     TimeZoneSidKey = 'America/Los_Angeles',
                     LocaleSidKey = 'en_US',
                     LanguageLocaleKey = 'en_US',
                     ProfileId = pf.Id
                    );
    
    insert uu;
    
    Account parentAccount = new Account();
    parentAccount.Name = 'Test Parent Account';
    parentAccount.Type = 'Merchant';
    parentAccount.Active_Partner__c = True;
    parentAccount.Onboarding_Stage__c ='Approved'; 
    parentAccount.RMC_Email__c = 'test@test.com';
    parentAccount.LMS_URL__c = 'test';
    parentAccount.LMS_Username__c = 'test@test.com';
    parentAccount.Account_Manager__c = uu.id;
    parentAccount.Cobranding_Name__c = 'test';
    insert parentAccount;
     
    list<string> emailToAddressess = new list<string>();
    emailToAddressess.add('test@test.com');
    
    string password = 'test', url = 'www.test.com',senderEmail ='test@test.com', Email_TEMPLATE ;
    
    EmailTemplate emp = [Select id,DeveloperName,name, Subject, Body,HtmlValue from EmailTemplate limit 1];
    Email_TEMPLATE = emp.DeveloperName;
    userNamePasswordUtilityClass.sendMerchantLoginCredentials(emailToAddressess,password,url,senderEmail,Email_TEMPLATE);
    userNamePasswordUtilityClass.updateMerchantTomySql(parentAccount.id,'test1@test.com','false',48,parentAccount.name);
  }
  
}