public with sharing class ProxyApiTabaPay {
	
	private static final String TRANSACTION_WITH_SAVED_DETAILS_URL = '/v101/tabapay/transactionWithSavedDetails';
	private static final String RETRIEVE_DATA_URL = '/v101/tabapay/retrieveData';
	private static final String ADD_DATA_URL = '/v101/tabapay/addData';
	private static final String DELETE_DATA_URL = '/v101/tabapay/deleteUserAccount';
	private static final String CHANGE_AUTO_PAYMENT = '/v101/tabapay/setCardAsDefault';
	private static final String AUTO_DEBIT_SAVED_DETAILS_URL = '/v101/tabapay/autoDebitWithSavedDetails';
	private AONObjectsParameters aonObjectsParemeters = new AONObjectsParameters();
    public static String fake_response='';
    
	public ProxyApiTabaPay() {}


	public static ProxyApiTabaPay.AddAccountResponse addAccount(String params) {
		String result = (!Test.isRunningTest()) ? ((ProxyApiBRMSUtil.ValidateBeforeCallApi() && String.isNotEmpty(params)) ? ProxyApiBRMSUtil.ApiGetResult(ADD_DATA_URL, params) : null) : fake_response;//'{"SC":"200","EC":"3C3E1401","EM":"card.accountNumber","message":null}';
        try {
			return (ProxyApiTabaPay.AddAccountResponse) JSON.deserialize(result, ProxyApiTabaPay.AddAccountResponse.class);
		} catch (Exception e) {
			return new ProxyApiTabaPay.AddAccountResponse('-1', '', 'API Exception When Trying to Add a TabaPay Account', 'Error details: ' + e.getMessage());
		}
	}

	public static ProxyApiTabaPay.DeleteAccountResponse deleteAccount(String params) {
		String result = (!Test.isRunningTest()) ? ((ProxyApiBRMSUtil.ValidateBeforeCallApi() && String.isNotEmpty(params)) ? ProxyApiBRMSUtil.ApiGetResult(DELETE_DATA_URL, params) : null): fake_response; //'{"SC":200,"EC":"0"}';
        try {
			return (ProxyApiTabaPay.DeleteAccountResponse) JSON.deserialize(result, ProxyApiTabaPay.DeleteAccountResponse.class);
		} catch (Exception e) {
			return new ProxyApiTabaPay.DeleteAccountResponse('-1', '', 'API Exception When Trying to Delete a TabaPay Account<br/>Error details: ' + e.getMessage());
		}
	}

	public static ProxyApiTabaPay.AccountsInfo retrieveAccounts(String params) {
		String result = (!Test.isRunningTest()) ? ((ProxyApiBRMSUtil.ValidateBeforeCallApi() && String.isNotEmpty(params)) ? ProxyApiBRMSUtil.ApiGetResult(RETRIEVE_DATA_URL, params) : null): fake_response;  // '[{"contactId":"0030B00002HNDd4","contractId":null,"cardNo":"XXXXXXXXXXXX9997","expDate":"202202","accountId":"RvwypjPEMIhxfsHsLkujIg","createdDate":"2018-11-23","active":true,"default":false}]';    
        try {
			if (result.contains('No Data found'))
				return new ProxyApiTabaPay.AccountsInfo(null, 'OK');
			else
				return new ProxyApiTabaPay.AccountsInfo((List<ProxyApiTabaPay.AccountsResponse>) JSON.deserialize(result, List<ProxyApiTabaPay.AccountsResponse>.class), 'OK');
		} catch (Exception e) {
			return new ProxyApiTabaPay.AccountsInfo(null, 'API Exception Getting TabaPay Accounts<br/>Error details: ' + e.getMessage());
		}
	}

	public static ProxyApiTabaPay.PaymentResponse makePayment(String params) {
        String result = (!Test.isRunningTest()) ? ((ProxyApiBRMSUtil.ValidateBeforeCallApi() && String.isNotEmpty(params)) ? ProxyApiBRMSUtil.ApiGetResult(TRANSACTION_WITH_SAVED_DETAILS_URL, params) : null) : fake_response;  //: '{"SC":200,"EC":"0","transactionID":"CrgDl7HEuKlKelbR1uQCwA","network":"Visa","networkRC":"00","status":"COMPLETED","approvalCode":"152458","AVS":{"codeAVS":"Y"}}';
        try {
			return (ProxyApiTabaPay.PaymentResponse) JSON.deserialize(result, ProxyApiTabaPay.PaymentResponse.class);
		} catch (Exception e) {
			return new ProxyApiTabaPay.PaymentResponse('-1', 'API Exception When Trying To Process The Payment<br/>Error details: ' + e.getMessage());
		}
	}

	public static ProxyApiTabaPay.ChangeAutoPayment changeAutoPayment(String params) {
		String result = (!Test.isRunningTest()) ? ((ProxyApiBRMSUtil.ValidateBeforeCallApi() && String.isNotEmpty(params)) ? ProxyApiBRMSUtil.ApiGetResult(CHANGE_AUTO_PAYMENT, params) : null): fake_response; //'{"SC":200,"EC":"0"}';
        try {
			return (ProxyApiTabaPay.ChangeAutoPayment) JSON.deserialize(result, ProxyApiTabaPay.ChangeAutoPayment.class);
		} catch (Exception e) {
			return new ProxyApiTabaPay.ChangeAutoPayment('-1', '', 'API Exception When Trying to change the auto payment<br/>Error details: ' + e.getMessage());
		}
	}

	public static ProxyApiTabaPay.PaymentResponse makeAutoPayment(String params) {
        String result = (!Test.isRunningTest()) ? ((ProxyApiBRMSUtil.ValidateBeforeCallApi() && String.isNotEmpty(params)) ? ProxyApiBRMSUtil.ApiGetResult(AUTO_DEBIT_SAVED_DETAILS_URL, params) : null) : fake_response;  //: '{"SC":200,"EC":"0","transactionID":"CrgDl7HEuKlKelbR1uQCwA","network":"Visa","networkRC":"00","status":"COMPLETED","approvalCode":"152458","AVS":{"codeAVS":"Y"}}';
        try {
			return (ProxyApiTabaPay.PaymentResponse) JSON.deserialize(result, ProxyApiTabaPay.PaymentResponse.class);
		} catch (Exception e) {
			return new ProxyApiTabaPay.PaymentResponse('-1', 'API Exception When Trying To Process The Payment<br/>Error details: ' + e.getMessage());
		}
	}

	public class AccountsResponse {

        public String accountId {get; set;}
        public String contactId {get; set;}
        public String contractId {get; set;}
        public String cardNo {get; set;}

        public String expDate {get; set;}
        public String expDateFormat {
            get{
                return ExpDateCustomFormatted(expDate);
            }
        }
        public String createdDate {get; set;}
        public Boolean active {get; set;}
        public String activeToText {
            get{
                return BooleanExpressionToText(active);
            }
        }
        public Boolean selectedForAutoPay {get; set;}

        public AccountsResponse(){}
		
        private String ExpDateCustomFormatted(String expDate){
            if (expDate != null && expDate.length() == 6)
                return expDate.substring(expDate.length() - 2, expDate.length()) + '/' +
                   expDate.substring(0, 4);
            else 
                return 'N/A';       
        }
        
        private String BooleanExpressionToText(Boolean var){
            if (var != null)
                return (var ? 'Yes':'No');
            else
                return 'N/A';
        }
    }

	public class AccountsInfo {

		public List<ProxyApiTabaPay.AccountsResponse> responseList {get; set;}
		public String result {get; set;}

		public AccountsInfo(List<ProxyApiTabaPay.AccountsResponse> responseList, String result) {
			this.responseList = responseList == null ? new List<ProxyApiTabaPay.AccountsResponse>() : responseList;
			this.result = result;
		}
	}

	public class AddAccountResponse {

		public String SC {get; set;}
		public String EC {get; set;}
		public String EM {get; set;}
		public String message {get; set;}

		public AddAccountResponse(String SC, String EC, String EM, String message) {
			this.SC = SC;
			this.EC = EC;
			this.EM = EM;
			this.message = message;
		}
	}

	public class DeleteAccountResponse {

		public String SC {get; set;}
		public String EC {get; set;}
		public String message {get; set;}

		public DeleteAccountResponse(String SC, String EC, String message) {
			this.SC = SC;
			this.EC = EC;
			this.message = message;
		}
	}

	public class PaymentResponse {
		public String SC {get; set;}
		public String EC {get; set;}
		public String EM {get; set;}
		public String transactionID {get; set;}
		public String network {get; set;}
		public String status {get; set;}
		public String approvalCode {get; set;}
		public String message {get; set;}

		public PaymentResponse(String SC, String message) {
			this.SC = SC;
			this.message = message;
		}
	}

	public class ChangeAutoPayment {

		public String SC {get; set;}
		public String EC {get; set;}
		public String message {get; set;}

		public ChangeAutoPayment(String SC, String EC, String message) {
			this.SC = SC;
			this.EC = EC;
			this.message = message;
		}
	}


}