Global class ScorecardUtil {

    /**
       * [scorecardProcess description]
       */
    public static void scorecardProcess(ProxyApiCreditReportEntity creditReportEntity, Opportunity theApp) {
                    System.debug('scorecardProcess');
                    System.debug(creditReportEntity.scorecardEntity);
       if (creditReportEntity != null &&
                  //  ScorecardUtil.appDecision(creditReportEntity) != 'No' &&
                    creditReportEntity.entityName == 'application') {

                if (creditReportEntity.scorecardEntity != null) {
                    System.debug('scorecardProcess2');
                    Scorecard__c scorecard = getScorecard(creditReportEntity.entityId);

                    if (scorecard == null)
                        score(creditReportEntity, theApp);
                    else
                        reGrade(creditReportEntity, scorecard);
                }
            }
    }

    public static void scorecardProcess(ProxyApiCreditReportEntity creditReportEntity) {
       scorecardProcess(creditReportEntity, null);
    }

    /**
       * Score process
       * @param creditReportEntity [description]
       */
    public static void score(ProxyApiCreditReportEntity creditReportEntity, Opportunity theApp) {
        if (ScorecardUtil.appDecision(creditReportEntity) == 'No') {
            creditReportEntity.scorecardEntity.highRisk = false;
        }
        updateScorecardFromEntity(creditReportEntity.entityId, creditReportEntity);

        if  (creditReportEntity.scorecardEntity.highRisk == false) {
            if (theApp!= null ) generateOffers(theApp, creditReportEntity.offerCatalogList);
            else
                generateOffers(creditReportEntity.entityId, creditReportEntity.offerCatalogList);
            if(theApp == null || (theApp !=null && theApp.Type == 'Refinance') ) regenerateOffersRefinance(creditReportEntity.entityId, false);
        }
        if (theApp != null)
            updateApp(theApp, creditReportEntity.scorecardEntity.grade, creditReportEntity.scorecardEntity, false);
        else
            updateApp(creditReportEntity.entityId, creditReportEntity.scorecardEntity.grade, creditReportEntity.scorecardEntity, false);
    }
    
    public static void score(ProxyApiCreditReportEntity creditReportEntity) {
        score(creditReportEntity, null);

    }

    /**
     * Re-grade process
     * @param creditReportEntity [description]
     * @param previousScorecard  [description]
     */
    public static void reGrade(ProxyApiCreditReportEntity creditReportEntity, Scorecard__c previousScorecard) {
        if (ScorecardUtil.appDecision(creditReportEntity) == 'No') {
            creditReportEntity.scorecardEntity.highRisk = false;
        }
        updateScorecardFromEntity(creditReportEntity.entityId, creditReportEntity);

        createNote(creditReportEntity.entityId);

        deleteOffers(creditReportEntity.entityId);

        if  (creditReportEntity.scorecardEntity.highRisk == false) {
            generateOffers(creditReportEntity.entityId, creditReportEntity.offerCatalogList);
            regenerateOffersRefinance(creditReportEntity.entityId, false);
        }
        updateApp(creditReportEntity.entityId, previousScorecard.Grade__c, creditReportEntity.scorecardEntity, true);
    }

    /**
     * [updateScorecardFromEntity description]
     * @param ProxyApiCreditReportEntity cr [description]
     */
    public static void updateScorecardFromEntity(String appId, ProxyApiCreditReportEntity cr) {
        Scorecard__c sc = getScorecard(appId);
    	System.debug('updateScorecardFromEntity');
        if (sc == null) {
            sc = new Scorecard__c();
            Opportunity application = getApplication(appId);
            sc.Opportunity__c = application.Id;
            sc.Contact__c = application.Contact__c;
        }

        sc.ProdAnnualIncome__c = cr.scorecardEntity.annualIncome;
        sc.ProdDTI_Ratio__c = cr.scorecardEntity.dtiRatio;
        sc.ProdSelfEmployed__c = cr.scorecardEntity.selfEmployed;
        sc.ProdCreditScore__c  = cr.scorecardEntity.creditScore;
        sc.ProdInquiries_last_6_months__c = cr.scorecardEntity.inquiries;
        sc.ProdTotal_Trade_Lines__c = cr.scorecardEntity.totalTradeLines;
        sc.ProdHCNC__c = cr.scorecardEntity.highestBalance;
        sc.ProdInst_Trades_Opened__c = cr.scorecardEntity.totalOpenInstallments;
        sc.Logit__c = cr.scorecardEntity.logit;
        sc.Unit_Bad_Probability__c = cr.scorecardEntity.unitBadProb;
        sc.Dollar_Rate_Bad_Probability__c = cr.scorecardEntity.dollarRateBadProb;
        sc.FICO_Score__c = decimal.valueOf(cr.getAttributeValue(CreditReport.FICO));
        sc.DTI__c = decimal.valueOf(cr.getAttributeValue(CreditReport.DTI));
        sc.Revolving_Utilization__c = cr.getAttributeValue(CreditReport.REVOLVING_UTILIZATION);
        if (cr.scorecardEntity.highRisk == null) {
            cr.scorecardEntity.highRisk = false;
        }
        
         /* BEFORE BRMS * /
        sc.Grade__c = scorecardEntity.grade;
        sc.Acceptance__c = scorecardEntity.acceptance;
        /* BRMS */
        
        /* AFTER BRMS */
        If(CreditReport.validateBrmsFlag(appId) ){
            sc.Grade__c = cr.scorecardEntity.grade;
            sc.Acceptance__c = cr.scorecardEntity.acceptance;
        }else{
            If(cr.brms != null && String.isNotBlank(cr.brms.scAcceptance) )
                sc.Acceptance__c = cr.brms.scAcceptance;
            If(cr.brms != null && String.isNotBlank(cr.brms.finalGrade) )   
                sc.Grade__c = cr.brms.finalGrade;
        }
        /* BRMS */

        sc.High_Risk__c = cr.scorecardEntity.highRisk;

        upsert sc;
    }


    /**
    * This field shall show the acceptance generated for the customer using the data from the hard pull
    * @param creditReportEntity [description]
    */
    public static void updateAcceptanceForHardPull(ProxyApiCreditReportEntity creditReportEntity) {

        Scorecard__c sc = getScorecard(creditReportEntity.entityId);

        if (sc != null) {
            /* BEFORE BRMS * /
            sc.Acceptance_Hard_Pull__c = creditReportEntity.scorecardEntity != null ? creditReportEntity.scorecardEntity.acceptance : 'N';
            /* BRMS */
            
            /* AFTER BRMS */
            If(CreditReport.validateBrmsFlag(creditReportEntity.entityId ) ) // Indicates pre BRMS
                sc.Acceptance_Hard_Pull__c = creditReportEntity.scorecardEntity != null ? creditReportEntity.scorecardEntity.acceptance : 'N';
            else{
                If(creditReportEntity.brms != null && String.isNotBlank(creditReportEntity.brms.scAcceptance) )
                    sc.Acceptance_Hard_Pull__c = creditReportEntity.brms.scAcceptance;
            }
            
            update sc;
            /* BRMS */
        }
    }

    /**
     * Updates the app after re-grade process
     * @param previousGrade
     */
    public static void updateApp(String appId, String previousGrade, ProxyApiScorecardEntity scorecardEntity, Boolean isReGraded) {
        //START - SM-976 | Update Application Fields On Offer Deletion or De-selection
        //Opportunity app = new Opportunity(Id = appId); 
        Opportunity app = [select id, Maximum_Payment_Amount__c, Re_Grated_Income_in_Contacts__c ,  Scorecard_Grade__c, Is_Selected_Offer__c, Selected_Offer_Id__c, Contact__r.Loan_Amount__c from Opportunity where id= :appId];
        //END- SM-976 | Update Application Fields On Offer Deletion or De-selection
        if(app != null) {
            if (String.isEmpty(previousGrade)) previousGrade = app.Scorecard_Grade__c;
            updateApp(app, previousGrade, scorecardEntity, isReGraded);
            System.debug('updateApp + ' + app.Re_Grated_Income_in_Contacts__c);
            update app;
        }
    }
    
    /**
     * Updates the app after re-grade process
     * @param previousGrade
     */
    public static void updateApp(Opportunity app, String previousGrade, ProxyApiScorecardEntity scorecardEntity, Boolean isReGraded) {

        app.Scorecard_Grade__c = scorecardEntity == null ? 'F' : scorecardEntity.grade;
        app.Manual_Grade__c = app.Scorecard_Grade__c;
        //app.Maximum_Payment_Amount__c = scorecardEntity == null ? 0 : scorecardEntity.maximumPaymentAmount;
        
        //START - SM-976 | Update Application Fields On Offer Deletion or De-selection
        if(app.Is_Selected_Offer__c || string.isNotEmpty(app.Selected_Offer_Id__c)) OfferCtrlUtil.changeOppOnOfferDeselect(app);
        //app.Is_Selected_Offer__c = false;
    //END - SM-976 | Update Application Fields On Offer Deletion or De-selection
        if (isReGraded) {
            app.Previous_Grade__c = previousGrade;
            app.Re_Graded_By__c = UserInfo.GetName();
            app.Was_application_re_graded__c = true;
            app.Re_Grated_Income_in_Contacts__c =  scorecardEntity == null ? 0 : scorecardEntity.annualIncome / -0.000010741;
            app.Payment_Amount__c = null;
        }
 
    }



    /**
     * Get a specific application
     * @param  appId The id of the application to get
     * @return Returns an application
     */
    public static Opportunity getApplication(String appId) {
        Opportunity result = null;

        for (Opportunity app : [SELECT Id,
                                             Estimated_Grand_Total__c,
                                             Scorecard_Grade__c,
                                             Previous_Grade__c,
                                             Was_application_re_graded__c,
                                             Manual_Grade__c,
                                             Contact__c,
                                             Contact__r.Employment_Type__c,
                                             Contact__r.Annual_Income__c,
                                             Contact__r.MailingState,
                                             Payment_Amount__c,
                                             Amount, Unique_Code_Hit_Id__c,
                                             Lead_Sub_Source__c,
                                             Re_Graded_By__c, 
                                             Re_Grated_Income_in_Contacts__c,
                                             Maximum_Payment_Amount__c,
                                             Partner_Account__r.Default_Payment_Frequency__c,
                                             Type,Maximum_Loan_Amount__c,Total_Loan_Amount__c,
                                             Contract_Renewed__c, Maximum_Offer_Amount__c,
                                             Contract_Renewed__r.Payment_Frequency_Masked__c,
                                             Source_Contract__c, ProductName__c,Fee_Handling__c,
                                             Source_Contract__r.Payment_Frequency_Masked__c,
                                             Is_Selected_Offer__c, RepaymentFrequency__c, selected_Offer_ID__c,
                                             Contact__r.Lead_Sub_Source__c,Contact__r.Loan_Amount__c //PAR-132
                                             FROM Opportunity 
                                             WHERE Id = :appId
                                                     LIMIT 1]) {
            result = app;
        }

        return result;
    }

    /**
      * Get a specific scorecard
      * @param  appId The id of the scorecard to get
      * @return Returns an scorecard
      */
    public static Scorecard__c getScorecard(String appId) {
        Scorecard__c result = null;

        for (Scorecard__c scorecard : [SELECT Id, Grade__c, Opportunity__c, Acceptance__c, Acceptance_Hard_Pull__c,
                                       ProdAnnualIncome__c, ProdCreditScore__c, DTI__c, Revolving_Utilization__c, 
                                       ProdDTI_Ratio__c, ProdHCNC__c, ProdInquiries_last_6_months__c, ProdInst_Trades_Opened__c,
                                       ProdSelfEmployed__c, ProdTotal_Trade_Lines__c, Logit__c, Unit_Bad_Probability__c,
                                       Dollar_Rate_Bad_Probability__c, FICO_Score__c, Contact__c, High_Risk__c
                                       FROM Scorecard__c
                                       WHERE Opportunity__c = : appId
                                               ORDER BY Id Desc
                                               LIMIT 1]) {
            result = scorecard;
        }

        return result;
    }

    /**
     * Deletes all the offers related to a specific application
     * @param appId The id of the application to delete offers
     */
    public static void deleteOffers(String appId) {
        SavePoint sp = Database.setSavePoint();

        try {
            List<Offer__c> offersList = [SELECT Id, name , IsSelected__c
                                         FROM Offer__c
                                         WHERE Opportunity__c = : appId];

            if (offersList != null && offersList.size() > 0) {
                delete offersList;
                System.debug('Previous Offers Deleted = SUCCESS!');
            }
        } catch (Exception e) {
            Database.RollBack(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getStackTraceString()));
        }
    }

    /**
    * Deletes scorecard related to a specific application
    * @param appId The id of the application to delete offers
    */
    public static void deleteScorecard(String appId) {
        SavePoint sp = Database.setSavePoint();

        try {
            List<Scorecard__c> scorecard = [SELECT Id
                                            FROM Scorecard__c
                                            WHERE Opportunity__c = : appId];

            if (scorecard != null && scorecard.size() > 0) {
                delete scorecard;
                System.debug('Scorecard Deleted = SUCCESS!');
            }
        } catch (Exception e) {
            Database.RollBack(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getStackTraceString()));
        }
    }

    /**
     * Generate offers process
     */
    public static void generateOffers(Opportunity Opp, List<ProxyApiOfferCatalog> offerCatalogList) {    
        String appId = Opp!= null ? String.valueOf(Opp.Id) : '';
        generateOffersr(appId, offerCatalogList, Opp);
        
    }

        /**
     * Generate offers process
     */
    public static void generateOffers(String appId, List<ProxyApiOfferCatalog> offerCatalogList) {    
        
        generateOffersr(appId, offerCatalogList, null);
        
        
    }


  /**
     * Generate offers process returns max pti as string
     */
  public static String generateOffersr(String appId, List<ProxyApiOfferCatalog> offerCatalogList, Opportunity opp) {    
        List<Offer__c> offerList = new List<Offer__c>();
        Double maxLoanAmount=0;
        
    	Opportunity application = opp != null ? opp : getApplication(appId) ;
    	application.Maximum_Offer_Amount__c = 0;
        if (application.Unique_Code_Hit_Id__c == null) {
          for (Unique_Code_Hit__c uh : [Select Id from Unique_Code_Hit__c where Contact__c =: application.Contact__c and Sub_Source__c =: application.Lead_Sub_Source__c  order by CreatedDate desc LIMIT 1]) {
              application.Unique_Code_Hit_Id__c  = uh.Id;
            }
        }

        if (offerCatalogList != null) {
            System.debug('Creating new offers in the system');

            for (ProxyApiOfferCatalog offerCatalog : offerCatalogList) {
                offerList.add(createOffer(offerCatalog, application));
            }
        }
        if (offerList.size() > 0) 
        {
            upsert offerList;
            application.Maximum_Payment_Amount__c = offerCatalogList[0].maxPti;
            System.debug('Max PTI ScorecardUtil ' + application.Maximum_Payment_Amount__c);
            if (opp == null) update application;
            return String.valueOf(application.Maximum_Payment_Amount__c);
            
        }
        return null;
    }

    /**
     * Create a new offer
     * @param  offerCatalog [description]
     * @param  app          [description]
     * @return              [description]
     */
     public static Offer__c createOffer(ProxyApiOfferCatalog offerCatalog, Opportunity app) {
        Offer__c o = new Offer__c();


        
        o.Max_Funding_Amount__c = offerCatalog.maxFundingAmount; //MER-133
        o.Term__c = offerCatalog.term;
        o.Installments__c = offerCatalog.installments;
        o.APR__c = offerCatalog.apr;
        o.Loan_Amount__c = offerCatalog.loanAmount;
        o.Fee__c = offerCatalog.feeAmount;
        o.Total_Loan_Amount__c = offerCatalog.feeAmount + offerCatalog.loanAmount;
        o.Fee_Percent__c = offerCatalog.feePercent;
        //o.Payment_Amount__c = offerCatalog.paymentAmount;
        o.Payment_Amount__c =  offerCatalog.AONPaymentAmount != null || offerCatalog.AONPaymentAmount != 0.00 ? offerCatalog.paymentAmount + offerCatalog.AONPaymentAmount : offerCatalog.paymentAmount;
        o.Effective_APR__c = offerCatalog.ear;
        o.Criteria__c = offerCatalog.criteria; //API-166
        if(string.isNotEmpty(offerCatalog.offerType)) {
            o.Offer_Type__c= offerCatalog.offerType;//PAR-6
            Promo_Information__mdt promoInfo = OfferCtrlUtil.getPromoInfo(offerCatalog.offerType,false);
            if(promoInfo != null)  o.Offer_Code__c= string.isNotempty(promoInfo.Promo_Code__c) ? promoInfo.Promo_Code__c : '';//API-312  
        }
        o.Scope__c = offerCatalog.offerScope;
        o.Opportunity__c = app.Id;
        o.Unique_Code_Hit_Id__c = app.Unique_Code_Hit_Id__c;
        o.AON_Fee__c = offerCatalog.AONFeePct;
        o.AON_Payment_Amount__c = offerCatalog.AONPaymentAmount;

        String frequency = '';
        if (app.Type.equalsIgnoreCase('New'))
            frequency = app.RepaymentFrequency__c <> null && app.RepaymentFrequency__c <> '--None--' ? SubmitApplicationService.getPaymentFrequency(app.RepaymentFrequency__c) : (app.Partner_Account__r == null ? 'T' : SubmitApplicationService.getPaymentFrequency(app.Partner_Account__r.Default_Payment_Frequency__c));   //MAINT-575
        else if (app.Type.equalsIgnoreCase('Refinance'))
            frequency = app.Contract_Renewed__c == null ? 'T' : (string.isNotEmpty(app.Contract_Renewed__r.Payment_Frequency_Masked__c) ? SubmitApplicationService.getPaymentFrequency(app.Contract_Renewed__r.Payment_Frequency_Masked__c) : 'T');

        else if (app.Type.equalsIgnoreCase('Multiloan'))
            frequency = app.Source_Contract__c == null ? 'T' : (string.isNotEmpty(app.Source_Contract__r.Payment_Frequency_Masked__c) ? SubmitApplicationService.getPaymentFrequency(app.Source_Contract__r.Payment_Frequency_Masked__c) : 'T');

        if (frequency.equalsIgnoreCase('M'))
            o.Repayment_Frequency__c = 'Monthly';
        else if (frequency.equalsIgnoreCase('T'))
            o.Repayment_Frequency__c = '28 Days';
        else if (frequency.equalsIgnoreCase('B'))
            o.Repayment_Frequency__c = 'Bi-Weekly';
        else if (frequency.equalsIgnoreCase('S'))
            o.Repayment_Frequency__c = 'Semi-Monthly';
        else if (frequency.equalsIgnoreCase('W'))
            o.Repayment_Frequency__c = 'Weekly';
           
        if (o.Contact__c != app.Contact__c) o.Contact__c = app.Contact__c;
         
        o.Lead_Sub_Source__c = app.ProductName__c == 'Point Of Need' ? app.Lead_Sub_Source__c :  app.Contact__r.Lead_sub_Source__c; //PAR-132
        o.requested_Loan_Amount__c = app.Contact__r.Loan_Amount__c; //PAR-132

        decimal maxOfferAmount = (app.Fee_Handling__c == null || app.Fee_Handling__c == FeeUtil.NET_FEE_OUT) ? o.Total_Loan_Amount__c : o.Loan_Amount__c;  
        if (maxOfferAmount > app.Maximum_Offer_Amount__c) app.Maximum_Offer_Amount__c = maxOfferAmount;

        return o;
    }

    /**
     * [createNote obtains all details about selected offer]
     */
    private static void createNote(String appId) {
        try {
            Note newNote = getNewNote(appId, getNoteBody(appId));

            if (newNote != null) insert newNote;
        } catch (Exception e) {
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Error after trying insert the note for the offer selected!'));
        }
    }

    /**
     * [getNewNote description]
     * @param  appId    [description]
     * @param  textNote [description]
     * @return          [description]
     */
    private static Note getNewNote(String appId, String textNote) {
        Note newNote = null;

        if (textNote != null) {
            newNote = new Note();
            newNote.parentid = appId;
            newNote.title = 'New Offer Generated after Re-Grade' ;
            newNote.body = textNote;
        }

        return newNote;
    }

    /**
     * [getNoteBody description]
     * @return [description]
     */
    private static String getNoteBody(String appId) {

        Offer__c selectedOffer = getSelectedOffer(appId);

        if (selectedOffer != null) {
            Decimal apr = selectedOffer.APR__c * 100;
            apr = (apr <> null ? apr.setscale(2) : apr);

            DateTime dT1 = selectedOffer.Createddate;
            Date dCreateDate = date.newinstance(dT1.year(), dT1.month(), dT1.day());

            DateTime dT2 = selectedOffer.Selected_Date__c;
            Date dSelectedDate = date.newinstance(dT2.year(), dT2.month(), dT2.day());

            DateTime dT3 = selectedOffer.FirstPaymentDate__c;
            Date dFirstPaymentDate = date.newinstance(dT3.year(), dT3.month(), dT3.day());

            return   'Old Offer selected before Re-Grade' + '\n\r' +
                     'APR: ' + apr +  '%' + '\n\r' +
                     'Term: ' + selectedOffer.Term__c + '\n\r' +
                     'Repayment Frequency: ' + selectedOffer.Repayment_Frequency__c + '\n\r' +
                     'Installments: ' + selectedOffer.Installments__c + '\n\r' +
                     'Payment Amount: ' + '$' + selectedOffer.Payment_Amount__c + '\n\r' +
                     'Loan Amount: ' + '$' + selectedOffer.Loan_Amount__c + '\n\r' +
                     'Fee: ' + '$' + selectedOffer.Fee__c + '\n\r' +
                     'Total Loan Amount: ' + '$' + selectedOffer.Total_Loan_Amount__c + '\n\r' +
                     'Created Date: ' + dCreateDate.format() + '\n\r' +
                     'Selected Date: ' + dSelectedDate.format() + '\n\r' +
                     'First Payment Date: ' + dFirstPaymentDate.format() + '\n\r' +
                     'Is Custom: ' + (selectedOffer.IsCustom__c ? 'Yes' : 'No' + '\n\r');
        }

        return null;
    }

    /**
     * [getSelectedOffer description]
     * @return [description]
     */
    public static Offer__c getSelectedOffer(String appId) {

        try {

            List<Offer__c> offer = [SELECT
                                    Id
                                    , Repayment_Frequency__c
                                    , APR__c
                                    , Term__c
                                    , Payment_Amount__c
                                    , IsSelected__c
                                    , Selected_Date__c
                                    , Loan_Amount__c
                                    , Createddate
                                    , Total_Loan_Amount__c
                                    , Fee__c
                                    , APR_percent__c
                                    , IsCustom__c
                                    , Fee_Percent__c
                                    , FirstPaymentDate__c
                                    , Installments__c
                                    , Criteria__c
                                    , Unique_Code_Hit_Id__c
                                    FROM Offer__c
                                    WHERE Opportunity__c = : appId
                                            AND IsSelected__c = true LIMIT 1 ];

            if (offer.size() > 0) return offer.get(0);

        } catch (Exception e) {
            System.debug(e);
        }
        return null;
    }

    /**
     * [maxAmount description]
     * @param  grade [description]
     * @return       [description]
     */
    public static Decimal maxAmount(String grade, String strategyType) {

        User current_user = [SELECT Email
                             FROM User
                             WHERE Id = :UserInfo.getUserId()] ;

        List<OfferOverrideUsers__c> users = OfferOverrideUsers__c.getall().values();

        boolean usrOverride = false;

        for (OfferOverrideUsers__c oo : users) {
            if (oo.Email__c == current_user.Email)
                usrOverride = true;
        }

        if (usrOverride && (grade == 'C2' || grade == 'D' || grade == 'E'))   return 5000;

        if (grade == 'A1' ||  grade == 'A2' || grade == 'B1')      return 25000;
        else if (grade == 'B2') return 17500;
        else if (grade == 'C1' && strategyType == '1') return 10000;
        else if (grade == 'C1' && (strategyType == '2' || strategyType == '3')) return 5000;
        else if (grade == 'C2' || grade == 'D' || grade == 'E') return 5000;

        return 0;
    }

    public static void regenerateOffersRefinance(String appId, Boolean isRecalculation) {
        Boolean hasBetterGrade = false;
        
        Opportunity application = getApplication(appId);

        if (application.Type.equalsIgnoreCase('Refinance')) {                
            loan_Refinance_Params__c params = RefinancePostSoftPull.getRefinanceParams(application.Contract_Renewed__c);

            String oldAppId = [SELECT Opportunity__c FROM loan__Loan_Account__c WHERE Id =: application.Contract_Renewed__c].Opportunity__c;

            Offer__c oldOffer = RefinancePostSoftPull.getSelectedOffer(oldAppId);

            List<Offer__c> newOffers = RefinancePostSoftPull.getOffers(appId);

            Scorecard__c newScorecard = getScorecard(application.id);

            if (RefinancePostSoftPull.isGradeApproved(params.Grade__c, newScorecard.Grade__c))
                hasBetterGrade = !params.Grade__c.equalsIgnoreCase(newScorecard.Grade__c);

            List<Offer__c> refiOffers = EligibilityCheckPostSoftPull.getRefinanceOffers(oldOffer, newOffers, hasBetterGrade, newScorecard.Grade__c);

            upsert refiOffers;
        }
    }

    public static void deleteRefinanceOffers(String appId) {
        SavePoint sp = Database.setSavePoint();

        try {
            List<RefinanceSnapshotOffer__c> refiList = [SELECT Id FROM RefinanceSnapshotOffer__c WHERE Opportunity__c = : appId];

            if (refiList != null && refiList.size() > 0) {
                delete refiList;
                System.debug('Refinance Offers Deleted = SUCCESS!');
            }
        } catch (Exception e) {
            Database.RollBack(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getStackTraceString()));
        }
    }
    
    public static String appDecision(ProxyApiCreditReportEntity creditReportEntity){
        String decision = 'No'; // Set default decision
        If(creditReportEntity != null){
            If(CreditReport.validateBrmsFlag(creditReportEntity.entityId)){ // Pre BRMS
                decision = creditReportEntity.automatedDecision;
            }else{ // Post BRMS
                If(creditReportEntity.scorecardEntity != null)
                    decision  = 'Yes';
            }
        }
        return decision;
    }
}