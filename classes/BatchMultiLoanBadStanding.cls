global class BatchMultiLoanBadStanding implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts {
    global final String query;

    global BatchMultiLoanBadStanding(){
    	this.query ='SELECT Type, Id, Name, Contact__c, Status__c '+
				    'FROM Opportunity '+
				    'WHERE Contact__c in(Select loan__Contact__c FROM loan__Loan_Account__c where loan__Loan_Status__c = \'Active - Bad Standing\' ) '+
    				'AND Type = \'Multiloan\' '+
    				'AND Status__c = \'Credit Qualified\'';
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
			return Database.getQueryLocator(query);
		}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		List<Opportunity> opps = new List<Opportunity>();
		for(sObject s: scope){
			Opportunity app = (Opportunity) s;
			app.Status__c = 'Declined (or Unqualified)';
			opps.add(app);
		}
		System.debug('List of opp' + opps);
		update opps;
        
  }

	global void finish(Database.BatchableContext BC) {
	}

	global void execute(SchedulableContext sc) {
	  BatchMultiLoanBadStanding job = new BatchMultiLoanBadStanding();
		database.executebatch(job, 1);
	}

}