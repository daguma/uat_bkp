global class EmpInfoHelper {
 
    private static string preFix = '/vendors/empinfo/';///api/';//'/verdors/empinfo/';
    private static final String EmpInfo_Get_Token = preFix + 'createToken' ;//'/api/createToken';
    private static final String GET_EmpInfo_CreateOrder_RESULT = preFix + 'createOrder';//'/api/createOrder';//'/api/v1.0/order';
    private static final String DELETE_EmpInfo_CreateOrder = preFix + 'cancelOrder';//'/api/cancelOrder';
    private static final String GET_EmpInfo_QueryOrder_RESULT = preFix + 'queryOrder';//'/api/v1.0/order';
    
    @invocablemethod
    public static void callEmpInfo(List<id> cntList) {
      if(!System.isFuture() && !System.isBatch()) 
        getEmpInfoResult(cntList,'','') ;
    }
    
    //EmpInfo API
    /**
    *Map all input data related to empInfo
    *call the empInfo api method
    **/
    @future(callout=true)
    public static void getEmpInfoResult(List<id> cntIds,String pPhone,string email) {
       String Status = '',error;
        String verifyRequest = '',OauthToken,excep = '',verifyResponse = '';
        EmpInfo_Details__c empInfoResult = new  EmpInfo_Details__c();
        Mule_Settings__c settings = Mule_Settings__c.getOrgDefaults(); 
        HttpResponse res = new HttpResponse();
        List<Contact> listContact = new List<Contact>();
        try { 
                for(Contact contact : [SELECT Id,Email,LeadSource,FirstName,LastName,SSN__c,MailingStreet,MailingCity,MailingState,MailingPostalCode,Employer_Name__c,
                                EmpInfo_Verification_Called__c,Work_Email_Address__c,Office_Phone_Number__c  FROM Contact WHERE id In : cntIds]){ 
                        //string regExp = '[^\\w\\s]'; string regExp2 = '[ ]';                          
                        if(string.isEmpty(pPhone) && contact.Office_Phone_Number__c!=null) {
                         pPhone = VerifyPhoneNumberHelper.normalizeString(contact.Office_Phone_Number__c); //((contact.Office_Phone_Number__c).replaceAll(regExp, '')).replaceAll(regExp2,'');
                        }
                        if(string.isEmpty(email))  email= contact.Work_Email_Address__c!=null ? contact.Work_Email_Address__c : '';
                        empInfoResult.Contact__c = contact.id; 
                        empInfoResult.API_Name__c = 'Create Verification Order'; 
                        empInfoResult.API_RequestTime__c= system.Now();
                          if(!contact.EmpInfo_Verification_Called__c){
                               if(string.isNotBlank(pPhone) && string.isNotBlank(contact.Employer_Name__c)){
                                    Http hp = new http();
                                    HttpRequest req = getCreateVerifyOrderRequest(contact.id,settings,pPhone,contact,email,empInfoResult);
                                    if (Test.isRunningTest()) {
                                        verifyResponse = LibraryTest.fakeEmpInfoCreateOrderResponse();
                                        res.setStatusCode(200);
                                    } else {
                                        res = hp.send(req);
                                        verifyResponse  = res.getBody();
                                    }
                                    empInfoResult.API_ResponseTime__c = system.Now();
                                    empInfoResult.API_Response__c = verifyResponse ;                                     
                                    System.debug('getCreateVerifyOrderRequest API ("' + req.getEndpoint() + '") -> Response: ' + res.getStatusCode() + ' / ' + res.getStatus() );                    
                                    system.debug('==response==' + res.getBody());
                                    
                                    if(res.getStatusCode() == 200) {
                                      empInfoResult.EmpInfo_ID__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'id');
                                      Status  = empInfoResult.Request_Status__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'request_status');
                                      contact.EmpInfo_Employment_Request_Status__c = Status ;
                                      empInfoResult.Request_Status_Description__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'request_status_description');
                                      error = (string.IsNotblank(ProxyApiBRMSUtil.ExtractJsonField(verifyResponse , 'errorCode')) ? ProxyApiBRMSUtil.ExtractJsonField(verifyResponse , 'errorCode') : (string.IsNotblank(ProxyApiBRMSUtil.ExtractJsonField(verifyResponse , 'error'))? ProxyApiBRMSUtil.ExtractJsonField(verifyResponse , 'error') : ''));
                                      if(string.isBlank(ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'message'))) contact.EmpInfo_Verification_Called__c = true;
                                        else empInfoResult.Exception__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'message') + (string.IsNotBlank(error) ? ' Error Code =' + error :'' );   
                                      listContact.add(contact);
                                    } else 
                                         empInfoResult.Exception__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'message') + (string.IsNotBlank(error) ? ' Error Code =' + error :'' );   
                           } else empInfoResult.API_Response__c ='Please provide the Employer Name and Prior Job Phone Number to initiate the EmpInfo';               
                        } else empInfoResult.API_Response__c ='EmpInfo is already initiated for Verification for specific Contact';               
            }            
        }
        Catch(Exception ex) {
            excep = ex.getMessage() + ' at line no ' + ex.getLineNumber();
            if(empInfoResult==null) empInfoResult= new EmpInfo_Details__c();
            empInfoResult.Exception__c = excep ;
        }
        finally{
            system.debug('==empInfoResult==' + empInfoResult);
            system.debug('==listContact==' + listContact);
            if(empInfoResult!=null) Insert empInfoResult;
            if(listContact!=null) update listContact;
        }
    }
    //get the request for EmpInfo Create Verification Order API
    private static HttpRequest getCreateVerifyOrderRequest(String entityId,Mule_Settings__c settings,string pPhone,Contact contact,string email,EmpInfo_Details__c empInfoResult  ) {
    
        String inputData = mapEmpInfoInputData(entityId, pPhone,contact,email);
        system.debug('==inputData== '+ inputData );
        HttpRequest req = new HttpRequest();
        req = mapReuestHeader(req, GET_EmpInfo_CreateOrder_RESULT ,'POST',settings);        
        req.setBody(inputData);
        empInfoResult.API_Request__c = string.valueOf(req)+ ' Input-Data= ' + inputData; 
        return req;
    }
    @TestVisible
    Private static string mapEmpInfoInputData(String entityId, String pPhone,Contact contact,string email) {
        EmpInfoRequestParameter empInfoReqParam = null;
        String inputParams ;
        EmpInfo_Settings__c empInfoSettings = EmpInfo_Settings__c.getOrgDefaults();
            empInfoReqParam = new EmpInfoRequestParameter ();
            empInfoReqParam.source_id = entityId;
            empInfoReqParam.consent_date = String.ValueOf(system.Today());
            empInfoReqParam.verification_type= empInfoSettings.Verification_Type__c;//'VOI';
            empInfoReqParam.permissible_purpose =empInfoSettings.Permissible_Purpose__c;// 'Application for Credit';
            if(FactorTrust.runningInASandbox() && string.isNotBlank(empInfoSettings.Fill_Strategy__c)) empInfoReqParam.fill_strategy = empInfoSettings.Fill_Strategy__c;// 'FILLED';
            empInfoReqParam.employee = mapEmployeeInputData(contact);
            empInfoReqParam.employer =  mapemployerInputData(contact,pPhone,email);
            inputParams = System.Json.serialize(empInfoReqParam );
        return inputParams ;
    }
    @TestVisible
    private static EmpInfoRequestParameter.employeeDetails mapEmployeeInputData(Contact contact){
      EmpInfoRequestParameter.employeeDetails obj= new  EmpInfoRequestParameter.employeeDetails();
            obj.first_name = string.IsNotEmpty(contact.FirstName) ?  contact.FirstName : '';
            obj.last_name = string.IsNotEmpty(contact.LastName) ?  contact.LastName : '';            
            obj.ssn = string.IsNotEmpty(contact.SSN__c) ?  formatSSN(contact.SSN__c) : '';
            obj.email= string.IsNotEmpty(contact.Email) ?  contact.Email : '';
            obj.address = mapEmployeeAddressData(contact);
      return obj;
    }
    @TestVisible
    private static EmpInfoRequestParameter.addressDetails mapEmployeeAddressData(Contact contact){
            EmpInfoRequestParameter.addressDetails obj = new  EmpInfoRequestParameter.addressDetails();            
            obj.street1= string.IsNotEmpty(contact.MailingStreet) ?  contact.MailingStreet : '';
            obj.city= string.IsNotEmpty(contact.MailingCity) ?  contact.MailingCity : '';
            obj.state= string.IsNotEmpty(contact.MailingState) ?  contact.MailingState : '';
            obj.zip= string.IsNotEmpty(contact.MailingPostalCode ) ?  contact.MailingPostalCode : '';
      return obj;
    }
    @TestVisible
    private static EmpInfoRequestParameter.employerDetails mapemployerInputData(Contact contact,string pPhone,string email){
          EmpInfoRequestParameter.employerDetails obj= new  EmpInfoRequestParameter.employerDetails();
             
            obj.name = string.IsNotEmpty(Contact.Employer_Name__c) ?  Contact.Employer_Name__c : '';
            obj.phone= string.IsNotEmpty(pPhone) ?  pPhone : ''; 
            obj.email= email;
      return obj;
    }
    private static string formatSSN(string ssn){
      string nondigits = '[^0-9]';
      string digits;      
      // remove all non numeric
      digits = ssn.replaceAll(nondigits,'');      
      // 10 digit: reformat with dashes
      if (digits.length() == 9) 
        return digits.substring(0,3) + '-' +
               digits.substring(3,5) + '-' +
               digits.substring(5,9);
          
      // if it isn't a 10 or 11 digit number, return the original because
      // it may contain an extension or special information
      return( ssn);
    }
   Webservice static string cancelVerificationOrder(String entityId) {
        String Status = '',message,apiStatus,error;
        String verifyRequest = '',OauthToken,excep = '',verifyResponse = '';
        EmpInfo_Details__c empInfoResult = new  EmpInfo_Details__c(); 
        HttpResponse res= new HttpResponse() ;
        Mule_Settings__c settings = Mule_Settings__c.getOrgDefaults();
        map<string,string> returnMap = new  map<string,string>();
        Contact contact;       
        try {    
            if(CreditReport.contactExists(entityId)){
                contact = CreditReport.getContact(entityId);   
                empInfoResult.API_Name__c = 'Cancel Verification Order';
                empInfoResult.API_RequestTime__c= system.Now();
                message = empInfoResult.API_Response__c = 'No order present to cancel';                
                for(EmpInfo_Details__c empInfo :[select EmpInfo_ID__c,Contact__c from EmpInfo_Details__c where contact__c = :entityId and EmpInfo_ID__c <> null order by CreatedDate desc LIMIT 1]  ) { 
                            Http hp = new http();
                            HttpRequest req = cancelVerifyOrderRequest(empInfo.EmpInfo_ID__c,settings,empInfoResult);
                            empInfoResult.Contact__c = empInfo.Contact__c ;
                            if (Test.isRunningTest()) {
                                verifyResponse = LibraryTest.fakeEmpInfoCancelOrderResponse();
                                res.setStatusCode(200);
                            } else {
                                res = hp.send(req);
                                verifyResponse  = res.getBody();
                            }
                            
                            empInfoResult.API_ResponseTime__c = system.Now();
                            empInfoResult.API_Response__c = verifyResponse ; 
                                apiStatus = string.valueOf(res.getStatusCode());
                                if(res.getStatusCode() == 200) {
                                  empInfoResult.EmpInfo_ID__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'id');
                                  Status  = empInfoResult.Request_Status__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'request_status');
                                  contact.EmpInfo_Employment_Request_Status__c = Status ;
                                  empInfoResult.Request_Status_Description__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'request_status_description');
                                  string cancel_initiated = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'cancel_initiated');
                                  if(cancel_initiated=='true') message ='Cancel request is initiated with the updated status as ' + Status  ;
                                  error = (string.IsNotblank(ProxyApiBRMSUtil.ExtractJsonField(verifyResponse , 'errorCode')) ? ProxyApiBRMSUtil.ExtractJsonField(verifyResponse , 'errorCode') : (string.IsNotblank(ProxyApiBRMSUtil.ExtractJsonField(verifyResponse , 'error'))? ProxyApiBRMSUtil.ExtractJsonField(verifyResponse , 'error') : ''));
                                  if(string.isNotBlank(ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'message')) )
                                      message  = empInfoResult.Exception__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'message') + ' Error Code =' + error  ;
                                } else 
                                     message  = empInfoResult.Exception__c = ProxyApiBRMSUtil.ExtractJsonField(verifyResponse, 'message') + (string.IsNotBlank(error) ? ' Error Code =' + error :'' );                                                                                           
                 } 
              } else message = 'No record found for the given Id';
          }
          catch(Exception e){
             empInfoResult.Exception__c = e.getMessage() + ' at Line no ' + e.getLineNumber();
          }
          finally{
              if(empInfoResult!=null) Insert empInfoResult;
              if(string.isNotBlank(Status)) update Contact;
          }
          ReturnMap.put('status',apiStatus);
          ReturnMap.put('message',message);
          system.debug('==ReturnMap==' + JSON.serializePretty(ReturnMap));
          return JSON.serializePretty(ReturnMap);
    }
    //get the request for EmpInfo Create Verification Order API
    public static HttpRequest cancelVerifyOrderRequest(String id,Mule_Settings__c settings,EmpInfo_Details__c empInfoResult) {
        JSONGenerator gene = JSON.createGenerator(false);
                gene.writeStartObject();
                gene.writeStringField('id', id );
                gene.writeStringField('id_type', 'UNIQUE_ID');
                gene.writeEndObject();
                gene.close();
                string jsonString = gene.getAsString();
        system.debug('==jsonString == '+ jsonString );
        HttpRequest req = new HttpRequest();
        req = mapReuestHeader(req,DELETE_EmpInfo_CreateOrder  ,'DELETE',settings);        
        req.setBody(jsonString );
        empInfoResult.API_Request__c =string.valueOf(req) + ' Input-data= ' + jsonString ;
        return req;
    }
    @future(callout=true)
    Webservice static void queryVerificationOrder(List<id> cntList) {
        String Status = '',error;
        HttpResponse res= new HttpResponse() ;
        String verifyRequest = '',OauthToken,excep = '',verifyResponse = '';
        EmpInfo_Details__c empInfoResult = new  EmpInfo_Details__c();
        List<Employment_Details__c> lstEmpDetails = new List<Employment_Details__c>();
        Mule_Settings__c settings = Mule_Settings__c.getOrgDefaults(); 
        List<Contact> ListCnt = new List<contact>();  
        Map<string,string> mapEmpInfo = new  Map<string,string>(); 
        Map<string,string> mapContactOpp = new  Map<string,string>(); 
        Map<string,Employment_Details__c> mapEmpContact = new Map<string,Employment_Details__c>();   
        try { 
              for(EmpInfo_Details__c empInfo :[select EmpInfo_ID__c,Contact__c from EmpInfo_Details__c where contact__c In :cntList and EmpInfo_ID__c <> null order by CreatedDate desc ])
                mapEmpInfo.put(empInfo.Contact__c,empInfo.EmpInfo_ID__c);
               //E2E-93 - starts
               for(Employment_Details__c obj : [select id,Employment_Verification_Method__c,Opportunity__r.Contact__c,Opportunity__c from Employment_Details__c where Opportunity__r.Contact__c in : cntList order by CreatedDate desc] ){
                   if(!mapEmpContact.containskey(obj.Opportunity__r.Contact__c)) mapEmpContact.put(obj.Opportunity__r.Contact__c,obj);
               } 
               for(Opportunity opp : [Select id,Contact__c from Opportunity where Contact__c IN : cntList order by createdDate Desc]) 
                  if(!mapContactOpp.containskey(opp.Contact__c)) mapContactOpp.put(opp.Contact__c,opp.id);  
               //E2E-93 - ends      
               for(Contact contact: [SELECT Id,Email,LeadSource,FirstName,LastName,SSN__c,MailingStreet,MailingCity,MailingState,MailingPostalCode,Employer_Name__c,
                                EmpInfo_Verification_Called__c,Work_Email_Address__c,Office_Phone_Number__c  FROM Contact WHERE id In :cntList ]){
                empInfoResult.API_RequestTime__c = system.Now();
                empInfoResult.API_Name__c = 'Query Verification Order'; 
                empInfoResult.API_Response__c = 'No order present to query the verification Order'; 
                empInfoResult.Contact__c = contact.Id;               
               
                if(mapEmpInfo.containskey(contact.Id)){
                    string empInfoId = mapEmpInfo.get(contact.Id);
                    Http hp = new http();
                    HttpRequest req = queryVerificationOrderRequest(empInfoId,settings,empInfoResult);
                    if (Test.isRunningTest()) {
                        verifyResponse = LibraryTest.fakeEmpInfoQueryOrderResponse();
                        res.setStatusCode(200);
                    } else {
                        res = hp.send(req);
                         verifyResponse  = res.getBody();
                    }
                    empInfoResult.API_ResponseTime__c = system.Now();
                     empInfoResult.API_Response__c = verifyResponse ; 
                     system.debug('==verifyResponse==' + verifyResponse);
                     EmpInfoQueryVerificationResponseParams wrapper = (EmpInfoQueryVerificationResponseParams)System.JSON.deserialize(verifyResponse, EmpInfoQueryVerificationResponseParams.class);
                     if(res.getStatusCode() == 200) {                                
                         system.debug('==wrapper==' + wrapper);
                         empInfoResult.EmpInfo_ID__c = wrapper.id;
                         contact.EmpInfo_Employment_Request_Status__c  = empInfoResult.Request_Status__c = wrapper.request_status;
                         //E2E-93 - starts
                         if(wrapper.employer!=null)
                             contact.EmpInfo_Employer_Name__c = wrapper.employer.name ;
                         if(wrapper.employee!=null && wrapper.employee.employment!=null){                             
                             //contact.EmpInfo_Employment_Start_Date__c = string.IsNotBlank(wrapper.employee.employment.hire_date) ? date.valueOf(wrapper.employee.employment.hire_date) : null;                             
                             if(string.IsNotBlank(wrapper.employee.employment.hire_date)){
                               try{
                                   string[] arrays = wrapper.employee.employment.hire_date.split('/');
                                   DateTime myDate = DateTime.newInstance(Integer.ValueOF(arrays[2]), Integer.ValueOF(arrays[0]), Integer.ValueOF(arrays[1]));
                                   contact.EmpInfo_Employment_Start_Date__c  = Date.ValueOf(myDate.format('yyyy-MM-dd'));
                               }
                               catch(exception e){
                                    contact.EmpInfo_Employment_Start_Date__c  = Date.ValueOf(wrapper.employee.employment.hire_date);
                               }
                             }
                             contact.EmpInfo_Possition__c = wrapper.employee.employment.job_title ;
                             if(wrapper.employee.employment.income!=null){
                                 contact.EmpInfo_Avg_Hours_Per_Week__c = wrapper.employee.employment.income.avg_weekly_hours_worked;
                                 contact.EmpInfo_Income__c = wrapper.employee.employment.income.rate_of_pay ;
                             }
                             contact.EmpInfo_Employment_Status__c = wrapper.employee.employment.employment_status ; 
                             if(wrapper.employee.employment.employment_status == 'Active'){
                                  Employment_Details__c obj;
                                  if(mapEmpContact.containskey(contact.Id)) obj = mapEmpContact.get(contact.Id);
                                  else {obj = new Employment_Details__c();
                                    if(mapContactOpp.containskey(contact.Id)) obj.Opportunity__c = mapContactOpp.get(contact.Id);
                                  }
                                  if(obj!=null) {obj.Employment_Verification_Method__c = 'EmpInfo';
                                  lstEmpDetails.add(obj);}
                              }
                             
                          }
                          //E2E-93 - ends
                        error = (string.IsNotBlank(wrapper.errorCode)? wrapper.errorCode : (string.IsNotBlank(wrapper.error)?wrapper.error : ''));
                        empInfoResult.Request_Status_Description__c = wrapper.request_status_description;
                        if(string.isNotBlank(wrapper.message) )
                        empInfoResult.Exception__c =  wrapper.message + (string.IsNotBlank(error) ? ' Error Code =' + error :'' );   
                        ListCnt.add(contact);
                    } else 
                       empInfoResult.Exception__c = wrapper.message + (string.IsNotBlank(error) ? ' Error Code =' + error :'' );                        
                 }
              }
          }
          catch(Exception e){
             empInfoResult.Exception__c = e.getMessage() + ' at Line no ' + e.getLineNumber();
          }
          finally{
              if(empInfoResult!=null) Insert empInfoResult;
              if(ListCnt.size()>0) update ListCnt;
              If(lstEmpDetails.size()>0) upsert lstEmpDetails;
          }
    }
    @TestVisible
     //get the request for EmpInfo Create Verification Order API
    public static HttpRequest queryVerificationOrderRequest(String id,Mule_Settings__c settings,EmpInfo_Details__c empInfoResult) {        
        HttpRequest req = new HttpRequest(); 
        req = mapReuestHeader(req,GET_EmpInfo_QueryOrder_RESULT + '/' + id,'GET',settings);        
        empInfoResult.API_Request__c = string.valueOf(req) ;
        return req;
    }
    private static HttpRequest mapReuestHeader(HttpRequest req, string ApiURL,string methodType,Mule_Settings__c settings){      
        EmpInfo_Settings__c empInfoSettings = EmpInfo_Settings__c.getOrgDefaults();  
        req.setMethod(methodType);
        req.setEndpoint((FactorTrust.runningInASandbox()? settings.Dev_Base_URL__c : settings.Base_URL__c) + ApiURL);
        //ApiURL = ApiURL.replace('/verdors/empinfo/','');
        //req.setEndpoint('https://lpempinfoverify.us-e1.cloudhub.io/api/' +ApiURL); 
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(20000);
        //req.setHeader('Authorization', 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik0wVkdPVGxEUWtSRk5VVTBSRFE0TkRBNVJETkZOMEkxTVRFd01qRkJPVVJCUmpCR05VWkNNZyJ9.eyJodHRwczovL2FjY2Vzcy5jb250cm9sLmVtcGluZm8uY29tL3VzZXJEZXRhaWxzIjoie1widXNlcmlkXCI6XCJhdXRoMHw1YjY3ZTYwNTc3NzM3YjVmNWM1N2VmMDNcIixcImNvbXBhbnlJZFwiOlwiZTRkYjQ5MWItMmY2OC00YzNlLWI2NmYtMmRhZDE5MjhjMTNkXCJ9IiwiaXNzIjoiaHR0cHM6Ly9lbXBpbmZvLWRldmVsb3BtZW50LmF1dGgwLmNvbS8iLCJzdWIiOiJSNEJ2YlNrTGNJNlk1YkR0cm8ySmpnb2xHVHFYTndxZUBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly92ZXJpZmljYXRpb25hcGktZGV2ZWxvcG1lbnQuZW1waW5mby5jb20iLCJpYXQiOjE1NDQ2MDk0MDcsImV4cCI6MTU0NDY5NTgwNywiYXpwIjoiUjRCdmJTa0xjSTZZNWJEdHJvMkpqZ29sR1RxWE53cWUiLCJzY29wZSI6IndyaXRlOnZlcmlmaWNhdGlvbiByZWFkOnZlcmlmaWNhdGlvbiBjYW5jZWw6dmVyaWZpY2F0aW9uIGNyZWF0ZTpzdWJzY3JpcHRpb24gcmVhZDpzdWJzY3JpcHRpb24gdXBkYXRlOnZlcmlmaWNhdGlvbiB3cml0ZTpyZXF1ZXN0ZXIgcmVhZDpyZXF1ZXN0ZXIiLCJndHkiOiJjbGllbnQtY3JlZGVudGlhbHMifQ.rBSHTwnNRHoPuY35Pl6iGVQAlazYIlVUgnTC_63XquxjrHFAjEvOWmZDh8pnkuTn9G7XUjzhJG-1ACfmh5QPhyYUvtyIkiW53EKZ860hQ6OdNi49gk8G_kR2UQ0-i5vVAvmOv48wnQa6RuAKvzCq527j2a9HhcY9cthrEWs7spePd1aFYWsPV8Lc8UeUjUR9Zviv49MI7tfG9vSGq35lCXGvIWcJQmqCLxoPQsmsW9d643NPfH599RZ7QIxLQfunnGSpz84hWLuUTeWqBV06fykyU9B8YdVfjIo3K1kWn4X2cGNsnzfBkRaMY0PIvAhOtnWuBmuzv3B2Amne5UIdaw');
        req.setHeader('Authorization', (FactorTrust.runningInASandbox()? settings.Dev_Authorization__c : settings.Authorization__c));
        req.setHeader('Application-Token',(FactorTrust.runningInASandbox()? empInfoSettings.Dev_Application_Token__c : empInfoSettings.Application_Token__c));        
        return req;
    }
    webservice static string updateCallbackRequest(String jsonString) {
        string message='',status='error';
        Contact objContact = new Contact();
        EmpInfo_Details__c objEmpInfo = new EmpInfo_Details__c();
        Map<string,string> returnMap = new Map<string,string>();
        try {
            if(string.isNotBlank(jsonString)){               
                objEmpInfo.API_Request__c = jsonString;
                objEmpInfo.API_Name__c = 'Update Callback Request';
                objEmpInfo.API_RequestTime__c= system.Now();
                EmpInfoCallBackParameter wrapper = (EmpInfoCallBackParameter)System.JSON.deserialize(jsonString, EmpInfoCallBackParameter.class);
                system.debug('==wrapper ==' + wrapper );
                List<Contact> cntList = [SELECT Id,EmpInfo_Employment_Request_Status__c FROM Contact WHERE Id = :wrapper.event_data.source_id LIMIT 1];
                 if(cntList.Size()>0){
                    objContact=cntList[0];                   
                    objContact.EmpInfo_Employment_Request_Status__c = wrapper.event_data.request_status;
                    
                    objEmpInfo.Contact__c = wrapper.event_data.source_id;
                    objEmpInfo.EmpInfo_ID__c = wrapper.event_data.id;
                    objEmpInfo.Request_Status__c = wrapper.event_data.request_status;
                    objEmpInfo.Request_Status_Description__c = wrapper.event_data.request_status_description;
                    objEmpInfo.Third_Party_Name__c = wrapper.event_data.third_party_name;
                    objEmpInfo.API_Response__c = 'success';
                    update objContact;
                    message='success';status='200';
                } else objEmpInfo.API_Response__c = message ='No record present in the system for source Id : ' + wrapper.event_data.source_id;
           } else objEmpInfo.API_Response__c = message ='Please provide the data';
        }
        catch(Exception ex) {
            message = objEmpInfo.Exception__c = ex.getMessage() + ', line no' + ex.getLineNumber() + ', ' + ex.getStackTraceString();
            objEmpInfo.API_Response__c = message;            
        }
        finally{
            returnMap.put('message',message );
            returnMap.put('status',status);
            insert objEmpInfo;
        }
        return JSON.serializePretty(returnMap);
    }
    
}