public with sharing class CustomerDetailsCtrl {

    @AuraEnabled
    public static CustomerDetail getCustomerDetails(Id oppId, Opportunity opportunity) {
        CustomerDetail cd = new CustomerDetail();
        cd.setOpportunityDetails(opportunity);
        return cd;
    }

    public class CustomerDetail {

        @AuraEnabled public string customerName;
        @AuraEnabled public string mailingAddress;
        @AuraEnabled public string opportunityId;
        @AuraEnabled public string customerNum;
        @AuraEnabled public string brmsVersionId;

        @AuraEnabled public string useOfFunds;
        @AuraEnabled public string homePhone;
        @AuraEnabled public string mobilePhone;
        @AuraEnabled public string email;
        @AuraEnabled public string lastKountScore;

        @AuraEnabled public string finalGrade;
        @AuraEnabled public string lastIdologyResult;
        @AuraEnabled public string lastPayphoneResult;
        @AuraEnabled public String[] scoringModelA {get;set;}
        @AuraEnabled public String[] scoringModelB {get;set;}

        @AuraEnabled public string applicationRisk;

        public CustomerDetail() {}

        public void setOpportunityDetails(Opportunity opp){
            if(opp == null){
                customerName = 'Opportunity is null';
            }else{
                Contact co = opp.contact__r;
                customerName = co.name;
                mailingAddress = co.MailingStreet
                        + '\n' + co.MailingCity
                        + ',' + co.MailingState
                        + ' ' + co.MailingPostalCode;
                opportunityId = opp.name;
                customerNum =  co.Customer_Number__c;

                useOfFunds =  co.Use_of_Funds__c;
                homePhone =  co.HomePhone;
                mobilePhone =  co.MobilePhone;
                email =  co.Email;

                lastKountScore = ContactUtil.getKountScore(co.Id);
                lastIdologyResult = ContactUtil.getLastIdologyResult(co.id);
                lastPayphoneResult = ContactUtil.getLatestPayphoneResult(co.id);
            }
        }
    }

}