public with sharing class AONObjectsParameters {

	private static final String COMPANY_CODE = 'LPT0010000';
	private static final String PRODUCT_CODE = '000000OP01';
	

	public AONObjectsParameters() {	
	}

	public virtual class AONEnrollmentInputData{

		public String effectiveDate {get; set;}
	    public String firstName {get; set;}
	    public String middleName {get; set;}
	    public String lastName {get; set;}
	    public String addressLine1 {get; set;}
	    public String addressLine2 {get; set;}
	    public String addressLine3 {get; set;}
	    public String municipalityName {get; set;}
	    public String stateCode {get; set;}
	    public String postalCode {get; set;}
	    public String countryCode {get; set;}
	    public String emailAddress {get; set;}
	    public String locality {get; set;}
	    public String partyType {get; set;}
	    public String phoneUse {get; set;}
	    public String phoneNumber {get; set;}
	    public String statusCode {get; set;}
	    public String saleType {get; set;}
	    public String customerNumber {get; set;}
	    public String isReinstatement {get; set;}
	    public String company {get; set;}
	    public String accountNumber {get; set;}
	    public String productCode {get; set;}

	    public AONEnrollmentInputData(){}

		public AONEnrollmentInputData(Contact contact, String loanAccountNumber, String aonProductCode){
			AONObjectsParameters util = new AONObjectsParameters();
			effectiveDate = util.zuluFormat(system.now());
		    firstName = contact.FirstName;
		    middleName = contact.ints__Middle_Name__c;
		    lastName = contact.LastName;
		    addressLine1= contact.MailingStreet;
		    addressLine2 = '';
		    addressLine3 = '';
		    municipalityName = contact.MailingCity;
		    stateCode = contact.MailingState;
		    postalCode = contact.MailingPostalCode;
		    countryCode = 'US';
		    emailAddress = contact.Email;
		    locality = 'en-US';
		    partyType = 'P';
		    phoneUse = 'Work';
		    phoneNumber = contact.Phone_Clean__c;
		    statusCode = 'A';
		    saleType = 'INT';
		    customerNumber = contact.Id; 
		    isReinstatement = getReinstatementValue(loanAccountNumber);
		    company = COMPANY_CODE;
		    accountNumber = loanAccountNumber;
		    productCode = aonProductCode;
		}

		public String get_JSON(AONEnrollmentInputData aonEnrollmentInputData){
				return JSON.serializePretty(aonEnrollmentInputData, true);
		}

		private string getReinstatementValue(String loanAccountNumber){
			return 'False';
		}
	}

	public virtual class AONCancellationInputData{
		public String company {get; set;}
		public String accountNumber {get; set;}
		public String productCode {get; set;}
		public String cancelReasonCode {get; set;}
		public String effectiveDate {get; set;}
		public String refundAmount {get; set;}

		public AONCancellationInputData(){}

		public AONCancellationInputData(String loanAccountNumber, String aonCancelReasonCode, String aonRefundAmount){
			AONObjectsParameters util = new AONObjectsParameters();
			company = COMPANY_CODE;
			accountNumber = loanAccountNumber;
			productCode = [SELECT Product_Code__c FROM loan_AON_Information__c WHERE Contract__c = : [SELECT Id FROM loan__Loan_Account__c WHERE Name =: loanAccountNumber LIMIT 1].Id LIMIT 1].Product_Code__c; 
			cancelReasonCode = aonCancelReasonCode;
			effectiveDate = util.zuluFormat(system.now());
			refundAmount = aonRefundAmount;
		}

		public String get_JSON(AONCancellationInputData aoncancellationInputData){
				return JSON.serializePretty(aoncancellationInputData, true);
		}
	}

	public virtual class AONBillingInputData{
		public String effectiveDate {get; set;}
		public String minimumMonthlyPayment {get; set;}
		public String loanBalance {get; set;}
		public String paymentDueDate {get; set;}
		public String company {get; set;}
		public String accountNumber {get; set;}
		public String productCode {get; set;}
		public String productFee {get; set;}

		public AONBillingInputData(){}

		public AONBillingInputData(	DateTime billingEffectiveDate, 
									String billingProductFee, 
									String billingMinimumMonthlyPayment, 
									String billingLoanBalance,
									DateTime billingPaymentDueDate,
									String billingLoanAccountNumber,
									String billingProductCode) {
                                        
			AONObjectsParameters util = new AONObjectsParameters();
			effectiveDate = util.zuluFormat(billingEffectiveDate);                  
			productFee = billingProductFee;
			minimumMonthlyPayment = billingMinimumMonthlyPayment;
			loanBalance = billingLoanBalance;
			paymentDueDate = util.zuluFormat(billingPaymentDueDate);
			company = COMPANY_CODE;
			accountNumber = billingLoanAccountNumber;
			productCode = billingProductCode;
		}

		public String get_JSON(AONBillingInputData aonBillingInputData){
				return JSON.serializePretty(aonBillingInputData, true);
		}
	}

	public virtual class AONResponse{
		public Integer statusCode;
		public String http_status;
		public String aonResponse;
		public String errorMessage;
		public String errorDetails;
		public String callType;

		public AONResponse(String jsonResponse){

			Map<String, Object> responseMap;
			if(jsonResponse != null)
				responseMap = (Map<String,Object>)JSON.deserializeUntyped(jsonResponse);
			if(responseMap != null && !responseMap.isEmpty()){
				this.statusCode = (responseMap.get('statusCode') != null) ? (Integer) responseMap.get('statusCode') : null;
				this.http_status = (String) responseMap.get('http_status');
				this.aonResponse = (String) responseMap.get('aonResponse');
				this.errorMessage = (String) responseMap.get('errorMessage');
				this.errorDetails = (String) responseMap.get('errorDetails');
				this.callType = (responseMap.get('callType') != null) ? (String) responseMap.get('callType'): null;

				if(String.isNotEmpty(this.errorDetails)){
					try{
						responseMap = (Map<String,Object>)JSON.deserializeUntyped(this.errorDetails);
						this.errorDetails = (responseMap.get('message') != null) ? (String) responseMap.get('message') : this.errorDetails;
					}catch(JSONException e){
		                System.debug('Error in JSON deserializeUntyped, errorDetails attribute isn \'t a JSON Type');
		            } 
				}	
			}	
		}
	}
    
    public virtual class SalesforceResponse {
	    public String statusCode;
	    public String response;
	    public String errorMessage;
	    public String errorDetails;
	    public String inputData;

	    public SalesforceResponse(){
	      this.response = 'Response from Salesforce';
	      this.statusCode = 'OK';
	    }

	    public SalesforceResponse(String inputData){
	      this.response = 'Response from Salesforce, The param received is: ' + inputData;
	      this.statusCode = 'OK';
	      this.inputData = inputData;
	    }

	    public String get_JSON(SalesforceResponse salesforceResponse){
				return JSON.serializePretty(salesforceResponse, true);
		}
	}

	public virtual class AONPaymentInputData{
	  	public String accountNumber;
	  	public String companyCode;
	  	public String productCode;
	  	public String claimNumber;
	  	public String claimStatus;
	  	public String claimStatusDate;
	  	public String claimDisposition;
	  	public String paymentAmount;
	  	public String paymentDate;
	  	public String paymentType;
	}

	public DateTime convertTimestamp( String iso8601_ts){
		// Create a valid JSON string like '{"t":"2012-07-31T11:22:33.444Z"}'
		JSONParser parser = JSON.createParser( '{"t":"' + iso8601_ts + '"}');
		parser.nextToken();    
		parser.nextValue();    
		return DateTime.newInstance( parser.getDateTimeValue().getTime());
	}

	public String zuluFormat(Datetime dateValue){
		String zulu = 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'';
		return dateValue.format(zulu,'America/New_York');
	}

}