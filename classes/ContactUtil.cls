public with sharing class ContactUtil {

    // Fetch Last Idology Result
    public static String getLastIdologyResult(Id contactId) {

        String latestIdoResStr = 'n/a';
        for (IDology_Request__c latestIdoRes : [
                Select Verification_Result_Front__c,IDology_Result__c
                FROM IDology_Request__c
                WHERE Contact__c = :contactId
                ORDER BY createdDate DESC
                LIMIT 1
        ]) {
            latestIdoResStr = latestIdoRes.IDology_Result__c
                    + '/'
                    + latestIdoRes.Verification_Result_Front__c;
        }
        return latestIdoResStr;

    }

    //Fetch latest payphone result
    public static String getLatestPayphoneResult(Id contactId) {

        String latestPayfnResStr = 'n/a';
        for (Payfone_Run_History__c lastestPayfnRes : [
                Select MatchCRM_Response__c, GetIntelligence_Response__c, Last_Name_Score__c, Verification_Result__c
                FROM Payfone_Run_History__c
                WHERE Contact__c = :contactId
                ORDER BY createdDate DESC
                LIMIT 1
        ]) {
            latestPayfnResStr = lastestPayfnRes.Verification_Result__c;
        }
        return latestPayfnResStr;
    }

    // Fetch Kount Score
    public static String getKountScore(Id contactId) {

        String latestKountScr = 'n/a';
        for (Logging__c log : [
                Select api_response__c
                FROM Logging__c
                WHERE contact__c = :contactId AND Webservice__c = 'Kount'
                ORDER BY createdDate DESC
                LIMIT 1
        ]) {
            If (String.isNotBlank(log.api_response__c)) {
                String[] splitString = log.api_response__c.split(' ');
                for (String st : splitString) {
                    if (st.contains('SCOR')) {
                        String[] scr = st.split('=');
                        latestKountScr = scr[1];
                    }
                }
            }
        }
        return latestKountScr;

    }
}