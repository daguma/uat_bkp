global class ClarityRequestParameter {

    public String bank_account_number {get; set;}

    public String bank_account_type {get; set;}

    public String bank_routing_number {get; set;}

    public String cell_phone {get; set;}

    public String city {get; set;}

    public String date_of_birth {get; set;}

    public String date_of_next_payday {get; set;}

    public String drivers_license_number {get; set;}

    public String drivers_license_state {get; set;}

    public String email_address {get; set;}

    public String employer_address {get; set;}

    public String employer_city {get; set;}

    public String employer_state {get; set;}

    public String first_name {get; set;}

    public String last_name {get; set;}

    public String home_phone {get; set;}

    public String housing_status {get; set;}

    public String net_monthly_income {get; set;}

    public String occupation_type {get; set;}

    public String pay_frequency {get; set;}

    public String paycheck_direct_deposit {get; set;}

    public String reference_first_name {get; set;}

    public String reference_last_name {get; set;}

    public String reference_phone {get; set;}

    public String reference_relationship {get; set;}

    public String social_security_number {get; set;}

    public String state {get; set;}

    public String street_address_1 {get; set;}

    public String street_address_2 {get; set;}

    public String months_at_address {get; set;}

    public String months_at_current_employer {get; set;}

    public String work_fax_number {get; set;}

    public String work_phone {get; set;}

    public String zip_code {get; set;}
    
    public Decimal annual_income {get; set;}
    
    public string entity_id {get ;set;}
    
}