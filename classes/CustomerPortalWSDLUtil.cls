global class CustomerPortalWSDLUtil {

    webService static String getPaymentDetails(String requestParams){    
        return CustomerPortalWSDL.getOnlinePaymentDetails(requestParams);         
    }
    
    public static logging__c AddNote(string contractId, string paymentMethod, string paymentType, decimal paymentAmount , date paymentDate, string contactName, string contractName, string cardType,string transactionID, decimal fee){    
        
        Note contractNote;
         
        try{
            if(!string.isEmpty(contractId)){
                date todayDate=system.today();
                
                string paymentScheduledDate = Datetime.newInstance(paymentDate.year(), paymentDate.month(),paymentDate.day(),0,0,0).format('MM/dd/YYYY'); 
                string currentDate= Datetime.newInstance(todayDate.year(), todayDate.month(),todayDate.day(),0,0,0).format('MM/dd/YYYY'); 
                
                string subject='Online Payment Has Been Processed for ' + contractName;
                string body='On ' + string.valueOf(currentDate) + ', ' + contactName + ' submitted a payment'; 
                if(transactionID  <> null){
                    body+=' with online TransactionID ' + transactionID;
                }
                body+=' for the Contract ' + contractName + ' for the amount of $' + paymentAmount ;
            if(fee>0){
                body+=' (including 2% Debit Card fee)';

            }  
                body+=' with Payment Date of ' + paymentScheduledDate+'.';
                body+='The Payment Type selected is ' + paymentType + ' with the Payment Method ' + paymentMethod;  
                
                if(paymentMethod == 'ACH'){
                    body+='.';
                }else{
                    if(!string.isEmpty(cardType)){
                        body+=' and Card type is ' + cardType + '.';
                    }
                }
                
                contractNote = new Note(parentId=contractId,body=body,title=subject,isPrivate=false);
                system.debug('===Note==='+contractNote);
                insert contractNote;
            }
        }Catch(Exception e){
            WebToSFDC.notifyDev('CustomerPortalWSDLUtil.AddNote Error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
            if(!string.isEmpty(contractId))
              return getLogging('CustomerPortalWSDLUtil.AddNote Error', contractId, Json.SerializePretty(contractNote), e.getMessage());
        }  
        
        return null;      
    }
    
      public static logging__c addPayment(string contractId, string paymentMode, decimal paymentAmount , date paymentDate, date transacDate, string paymentType, decimal fee){    
        loan__Loan_Payment_Transaction__c newPayment;
        boolean addAllowed = false;
        logging__c log= null;
insert new Log__c(debug__c = 'addPayment ' + contractId + ' transacDate ' + transacDate);
        try{                        

            for (loan__Loan_Account__c la : [select id, Day_Past_Oldest_Unpaid_Due_Date__c from loan__Loan_Account__c where Id =: contractId]) {
                if (la.Day_Past_Oldest_Unpaid_Due_Date__c <= 0) addAllowed = true;
            }

            if(!string.isEmpty(contractId)){                
                //adding debit card extra fee for collections
                if(fee>0){
                    addCharges(contractId,fee,transacDate);    
                }               

                if(paymentType == 'Additional Payment' && addAllowed){ 
                    newPayment = new loan__Loan_Payment_Transaction__c(loan__Loan_Account__c = contractId,loan__Transaction_Amount__c = paymentAmount+fee, loan__Payment_Mode__c = paymentMode, loan__Receipt_Date__c = paymentDate, loan__Transaction_Date__c = transacDate, loan__Loan_Payment_Spread__c=label.Additional_payment_spread);                   
                    system.debug('============new Additional Payment============'+newPayment);    
                }else{
                    newPayment = new loan__Loan_Payment_Transaction__c(loan__Loan_Account__c = contractId,loan__Transaction_Amount__c = paymentAmount+fee, loan__Payment_Mode__c = paymentMode, loan__Receipt_Date__c = paymentDate, loan__Transaction_Date__c =  transacDate);                     
                    
                    system.debug('============newPayment============'+newPayment);
                }
               
                insert newPayment;
                insert new Log__c(debug__c = 'newPayment created ' );
            }
        }Catch(Exception e){insert new Log__c(debug__c = 'Error with payment '+ e.getMessage());            
           if(![SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox)WebToSFDC.notifyDev('CustomerPortalWSDLUtil.addPayment Error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());        
           if(!string.isEmpty(contractId)) 
               log= getLogging('CustomerPortalWSDLUtil.addPayment Error', contractId, Json.SerializePretty(newPayment), e.getMessage());
        }
        
        return log;
     }
     
    
    public static void addCharges(string contractId, decimal paymentAmount , date transacDate){    
                          
        if(!string.isEmpty(contractId)){
           loan__Charge__c chargesObj = new loan__Charge__c(loan__Loan_Account__c = contractId,loan__Original_Amount__c = paymentAmount, loan__Date__c =  transacDate, loan__Fee__c= label.DebitCardFee, loan__Interest_Rate__c=0); 
                insert chargesObj;
                system.debug('============chargesObj============'+chargesObj);
        }

   }  
   
     public static string enableNextDebitCardTransaction(string contractId){
         
         Map<string,string> paymentResponse= new Map<string,string>();
         list<loan__Loan_Payment_Transaction__c> transcations = [select id, name, loan__Transaction_Amount__c, loan__Cleared__c, loan__Rejected__c from loan__Loan_Payment_Transaction__c order by createddate desc limit 1];
         loan__Loan_Payment_Transaction__c latestTransaction = (!transcations.isEmpty()) ? transcations[0] : null;
        
            boolean enablePayment= !(latestTransaction <> null && !latestTransaction.loan__Cleared__c && !latestTransaction.loan__Rejected__c);   
             if(enablePayment){
                 paymentResponse.put('enablePayment','true');
                 paymentResponse.put('msg',null);

             }
             else{
             string rtnMsg='Transaction already in process with Transaction ID '+ latestTransaction.name + ' and Payment Amount '+latestTransaction.loan__Transaction_Amount__c;
                 paymentResponse.put('enablePayment','false');
                 paymentResponse.put('msg',rtnMsg);

             }
         return json.serializepretty(paymentResponse);
     }
     
     public static string enableNextACHTransaction(string contractId){
         boolean enablePayment=true;
         string rtnMsg,formattedDate;
         //Date todayDate= system.Today();
         Map<string,string> paymentResponse= new Map<string,string>();
         list<loan__Loan_Account__c> LoanAccountList= new List<loan__Loan_Account__c>();
         
         if(!string.isEmpty(contractId)){
             LoanAccountList=[select id, loan__OT_ACH_Debit_Date__c, loan__OT_ACH_Payment_Amount__c,loan__Last_Accrual_Date__c from loan__Loan_Account__c where id=: contractId];
         }
         loan__Loan_Account__c latestAccount = (!LoanAccountList.isEmpty()) ? LoanAccountList[0] : null;
         if(latestAccount <> null && latestAccount.loan__OT_ACH_Debit_Date__c <> null){
             formattedDate = DateTime.newInstance(latestAccount.loan__OT_ACH_Debit_Date__c.year(),latestAccount.loan__OT_ACH_Debit_Date__c.month(),latestAccount.loan__OT_ACH_Debit_Date__c.day()).format('MM/dd/YYYY');
             enablePayment=false;

             }  
             system.debug('===enablePayment==='+enablePayment); 
             if(enablePayment){
                 paymentResponse.put('enablePayment','true');
                 paymentResponse.put('msg',null);

             }
             else{              
                rtnMsg= 'ACH Transaction already in process with Payment Amount $'+latestAccount.loan__OT_ACH_Payment_Amount__c+ ' on '+ formattedDate;              
                 paymentResponse.put('enablePayment','false');
                 paymentResponse.put('msg',rtnMsg);

             }
            system.debug('==response==' + json.serializepretty(paymentResponse));
         return json.serializepretty(paymentResponse);
     }
     
     public static Logging__c getLogging(string webServiceName,string contractId, string request, string response){
        return new Logging__c(Webservice__c = webServiceName,API_Request__c = request,API_Response__c = response,CL_Contract__c = contractId);        
    }
    //wrapper class for extracting the response
    public class deserializeResponse{
        public Map<String,string> contractDetail{get;set;} 
        public String body{get;set;} 
        public String subject{get;set;}
        public string state{get;set;}   
    }
 
   webService static string customerPortalLoginNote(String requestParams){ 
        string noteSubject, noteBody, noteState;
        Map<string,string> contractDetailsMap= new Map<string,string>();
        List<Note> NoteList= new List<Note>();
        system.debug('===requestParams==='+requestParams);

        //Parsing of Response
        JSONParser parser = JSON.createParser(requestParams);
        deserializeResponse contractNoteReq = (deserializeResponse) parser.readValueAs(deserializeResponse.class); 
        system.debug('===contractNoteReq==='+contractNoteReq);  
        contractDetailsMap= contractNoteReq.contractDetail;
        noteSubject = contractNoteReq.subject;
        noteBody = contractNoteReq.body;
        noteState = contractNoteReq.state;

       
        Map<string,string> contractNoteInsertResponse= new Map<string,string>();
        try{
            if(!contractDetailsMap.isEmpty()){
                for(String fieldName : contractDetailsMap.keySet()){
                   

                    string contractId= contractDetailsMap.get(fieldName);
                    string fullSubject= noteSubject+contractDetailsMap.get(fieldName);
                    system.debug('====fullSubject size=='+ fullSubject.length());
                    fullSubject += '.State-'+noteState+'';
                     system.debug('====fullSubject size=='+ fullSubject.length());
                    Note contractNote = new Note(parentId=fieldName,body=noteBody,title=fullSubject,isPrivate=false);  
                    NoteList.add(contractNote);  
                }
                system.debug('===NoteList=='+NoteList);
                if(!NoteList.isEmpty()){
                    insert NoteList;
                }
            }
            else{
                 contractNoteInsertResponse.put('msg','ContractId is missing'); 
                 contractNoteInsertResponse.put('status','err');    
             } 
             contractNoteInsertResponse.put('msg',null); 
             contractNoteInsertResponse.put('status','200'); 
             return JSON.serializePretty(contractNoteInsertResponse);
         }catch(Exception e){
             contractNoteInsertResponse.put('msg',e.getMessage()); 
             contractNoteInsertResponse.put('status','err');  
             return JSON.serializePretty(contractNoteInsertResponse);  
         }
    }


}