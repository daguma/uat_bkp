global class ProxyApiCreditReportEntity {
    public Long id {get; set;}
    public String entityId {get; set;}
    public String entityName {get; set;}
    public Boolean isSoftPull {get; set;}
    public String datasourceName {get; set;}
    public Long createdDate {get; set;}
    public String reportText {get; set;}
    public String requestData {get; set;}
    public String errorMessage {get; set;}
    public String errorDetails {get; set;}
    public String transactionId {get; set;}
    public Boolean decision {get; set;}
    public List<ProxyApiCreditReportAttributeEntity> attributesList {get; set;}
    public List<ProxyApiCreditReportSegmentEntity> segmentsList {get; set;}
    public List<ProxyApiOfferCatalog> offerCatalogList {get; set;}
    public ProxyApiScorecardEntity scorecardEntity {get; set;}
    public ProxyApiCreditBRMSResponse brms {get; set;} 
    public map<string,string> valueMap {get; set;}
    public map<string,string> statusMap {get; set;}
    public list<BRMS_And_ATB_Rules_Mapping__c> brmsConfigList {get; set;}
    public Map<string,string> brmsConfigMap {get; set;}
    public List<ClarityClearRecentHistory> clearRecentHistory {get;set;} /* CNR - 3 property*/
    
    public ProxyApiEmailAgeResponseEntity emailAgeResponse {get; set;}
    public String name {
        get {
            if (this.id == null) return ' - ';

            String text = String.valueOf(this.id).leftPad(8, '0');
            return 'CR-' + text;
        }
        set;
    }

    public Boolean brmsApproved {
        get {
            if (brms != null && brms.deBRMS_ContractStatus != null && brms.deBRMS_ContractStatus.equalsIgnoreCase('Credit Qualified')) return true;
            else return false;
        }
        set;
    }

    public String automatedDecision {
        get {

            if (decision && isSoftPull) {
                Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
                //get the email age results
                List<Email_Age_Details__c> emailAgeDetails = EmailAgeStorageController.getEmailAgeRecord(entityId);

                if (emailAgeDetails.size() > 0 && !emailAgeDetails[0].Email_Age_Status__c && !emailAgeDetails[0].is_Email_Age_Timed_Out__c  && emailAgeSettings.Call_Email_age__c && emailAgeDetails[0].errorCode__c == '0' ) {
                    system.debug('--automatedDecision :inside decision set to no---');
                    return CreditReport.DECISION_NO;
                }

            }
            system.debug('--automatedDecision :inside decision set to decision---' + decision);
            return decision ? CreditReport.DECISION_YES : CreditReport.DECISION_NO;
        }
        set;
    }

    public String attributesApprovedByName {
        get {
            if (this.attributesList == null) return ' - ';

            List<ProxyApiCreditReportAttributeEntity> criteriaAttributeList = getCriteriaAttributeList();

            String result = 'Automated';

            for (ProxyApiCreditReportAttributeEntity item : criteriaAttributeList) {
                if (item.approvedBy.equals('Dig Deeper')) {
                    result = 'Automated by Dig Deeper';
                    break;
                } else if (!item.approvedBy.equals('system')) {
                    result = 'Manual by ' + item.approvedBy;
                    break;
                }
            }

            return result;
        }
        set;
    }

    public DateTime creationDate {
        get {
            if (createdDate == null) return null;

            return DateTime.newInstance(createdDate);
        }
        set;
    }

    public Boolean errorResponse {
        get {
            return errorMessage != null;
        }
        set;
    }

    /**
    * Get the list of attributes to be displayed in the criteria values
    * section in the credit report details screen
    * @return An attribute entity list
    */
     public List<ProxyApiCreditReportAttributeEntity> getCriteriaAttributeList() {
        if (this.attributesList == null) return null;

        List<ProxyApiCreditReportAttributeEntity> result = new List<ProxyApiCreditReportAttributeEntity>();
        BRMSATBMapping();
        Integer length = attributesList.size();
        
        for (Integer i = 0; i < length; i++) {
            ProxyApiCreditReportAttributeEntity attributeEntity = attributesList[i];
             if(!brmsConfigMap.isEmpty() && brmsConfigMap.Containskey(string.valueOf(attributeEntity.attributeType.id))){
                 if(!valueMap.isEmpty() && valueMap.containsKey(brmsConfigMap.get(string.valueOf(attributeEntity.attributeType.id)))) {
                     //handling for FICO response coming from BRMS
                     if(string.valueOf(attributeEntity.attributeType.id) == '1'){
                         string res= valueMap.get(brmsConfigMap.get(string.valueOf(attributeEntity.attributeType.id)));
                         List<String> FICOList = new list<string>();
                         
                         if(res <> null){ 
                             if(res.contains(',')) FICOList = res.split(',');
                             else FICOList.add(res); 
                         }
                         
                         if(FICOList.size()>1){
                           if(isSoftPull) attributeEntity.value= FICOList[0];
                           else attributeEntity.value= FICOList[1];
                         }else attributeEntity.value= FICOList[0];
                     }else
                     attributeEntity.value= valueMap.get(brmsConfigMap.get(string.valueOf(attributeEntity.attributeType.id)));
                 }
                 
                 if(!statusMap.isEmpty() && statusMap.containsKey(brmsConfigMap.get(string.valueOf(attributeEntity.attributeType.id)))) {
                     if(statusMap.get(brmsConfigMap.get(string.valueOf(attributeEntity.attributeType.id))) == 'Pass') attributeEntity.isApproved=true;
                        else attributeEntity.isApproved=false;
                 }
             }
             system.debug('==attributeEntity after=='+attributeEntity);
            if (attributeEntity.attributeType.isDisplayed) result.add(attributeEntity);
        }
        //system.debug('==result=='+result);
        return result;
    }

    /**
     * Get the list of attributes to be displayed in the other values
     * section in the credit report details screen
     * @return An attribute entity list
     */
    public List<ProxyApiCreditReportAttributeEntity> getOtherAttributeList() {
        List<ProxyApiCreditReportAttributeEntity> result = new List<ProxyApiCreditReportAttributeEntity>();

        result.add(getAttributeEntity(CreditReport.NUMBER_TRADES));
        result.add(getAttributeEntity(CreditReport.INSTALLMENT_TRADES_OPENED));
        result.add(getAttributeEntity(CreditReport.HIGHEST_CONSECUTIVE_BALANCE));
        result.add(getAttributeEntity(CreditReport.TOTAL_ANNUAL_DEBT));

        return result;
    }

    /**
     * Return the decimal value of a specific attribute
     * @param  attributeId The id of the credit report attribute
     * @return value The value of the credit report attribute in decimal format
     */
    public Decimal getDecimalAttribute(Long attributeId) {

        String valueString = getAttributeValue(attributeId);
        Decimal result = 0;

        try {
            result = Decimal.valueOf(valueString);
        } catch (Exception e) {
        }

        return result;
    }

    /**
    * Get a credit report attribute value by id
    * @param  attributeId The id of the credit report attribute
    * @result  value The value of the credit report attribute
    */
    public String getAttributeValue(Long attributeId) {
        if (this.attributesList == null) return ' - ';

        String value = '0';

        Integer length = attributesList.size();

        for (Integer i = 0; i < length; i++) {
            ProxyApiCreditReportAttributeEntity attributeEntity =
                attributesList.get(i);

            if (attributeEntity.attributeType.id == attributeId) {
                value = attributeEntity.value;
                break;
            }
        }

        return value;
    }

    /**
    * Get a credit report attribute value by id
    * @param  attributeId The id of the credit report attribute
    * @result  value The value of the credit report attribute
    */
    public void setAttributeValue(Long attributeId, String value) {

        Integer length = attributesList.size();

        for (Integer i = 0; i < length; i++) {
            ProxyApiCreditReportAttributeEntity attributeEntity =
                attributesList.get(i);

            if (attributeEntity.attributeType.id == attributeId) {
                attributeEntity.value = value;
                break;
            }
        }
    }
    
    public void BRMSATBMapping(){
        brmsConfigMap =new map<string,string>();
        valueMap= new map<string,string>();
        statusMap= new map<string,string>(); 
        
        brmsConfigList= BRMS_And_ATB_Rules_Mapping__c.getall().values();
        //system.debug('==brmsConfigList=='+brmsConfigList);
        for(BRMS_And_ATB_Rules_Mapping__c brmsConfig: brmsConfigList){
            if(string.isNotEmpty(brmsConfig.Attributes_List_Id__c)) brmsConfigMap.put(brmsConfig.Attributes_List_Id__c, brmsConfig.BRMS_Rule_Number__c);
     
        }
        if(this.brms != null && this.brms.rulesList != null && !this.brms.rulesList.isEmpty()){
            for(ProxyApiCreditReportRuleEntity softPullRule : this.brms.rulesList) {
                if(softPullRule.deRuleNumber!=null){
                 valueMap.put(softPullRule.deRuleNumber, softPullRule.deRuleValue);
                 statusMap.put(softPullRule.deRuleNumber, softPullRule.deRuleStatus);
                }
            }
        }
    }
    
    /**
    * Get a credit report attribute entity
    * @param  attributeId The id of the credit report attribute
    * @return result The credit report entity
    */
    public ProxyApiCreditReportAttributeEntity getAttributeEntity(Long attributeId) {
        //variables declaration
        string deRuleValue, deRuleStatus;
        ProxyApiCreditReportAttributeEntity result = null;
        
         //====SM-453==start=======
         BRMSATBMapping();
         if(!brmsConfigMap.isEmpty() && brmsConfigMap.Containskey(string.valueOf(attributeId))){
             if(!valueMap.isEmpty() && valueMap.containsKey(brmsConfigMap.get(string.valueOf(attributeId)))) {
                 //handling for FICO response coming from BRMS
                 if(string.valueOf(attributeId) == '1'){
                         string res= valueMap.get(brmsConfigMap.get(string.valueOf(attributeId)));
                         List<String> FICOList = new list<string>();
                         
                         if(res <> null){ 
                             if(res.contains(',')) FICOList = res.split(',');
                             else FICOList.add(res); 
                         }
                         
                         if(!FICOList.isEmpty() && FICOList.size()>1){
                         if(isSoftPull) deRuleValue= FICOList[0];
                         else deRuleValue= FICOList[1];
                         }else deRuleValue= FICOList[0];
                         
                   }else
                     deRuleValue= valueMap.get(brmsConfigMap.get(string.valueOf(attributeId)));
             }
             if(!statusMap.isEmpty() && statusMap.containsKey(brmsConfigMap.get(string.valueOf(attributeId)))) deRuleStatus= statusMap.get(brmsConfigMap.get(string.valueOf(attributeId)));
         }  
         //====SM-453==End=======   
        system.debug('==attributeList=='+this.attributesList);
        if (this.attributesList == null) return null;

        Integer length = attributesList.size();

        for (Integer i = 0; i < length; i++) {
            ProxyApiCreditReportAttributeEntity attributeEntity =
                attributesList.get(i);

            if (attributeEntity.attributeType.id == attributeId) {
                //====SM-453==start=======
                if(string.isNotEmpty(deRuleValue) && attributeEntity.value != deRuleValue){
                    attributeEntity.value= deRuleValue;
                    if(string.isNotEmpty(deRuleStatus)){
                        if(deRuleStatus == 'Pass') attributeEntity.isApproved=true;
                        else attributeEntity.isApproved=false;
                    }
                }
                //====SM-453==End=======
                result = attributeEntity;
                break;
            }
        }

        return result;
    }

    /**
     * Get the credit report create date in Datetime format
     * @result  create date of the credit report
     */
    public Datetime getCreatedDate() {
        if (createdDate == null) return null;

        return Datetime.newInstance(createdDate);
    }

    /**
     * Get the credit report create date in Datetime format
     * @result  true if the credit report create date is on the last 30 days, otherwise false
     */
    public Boolean isCreateDateOnLast30Days() {

        if (createdDate == null) return false;

        return (Datetime.now() - 30) <= getCreatedDate();
    }

    public ProxyApiCreditReportEntity() {
        this.isSoftPull = false;
        this.decision = false;
        attributesList = new List<ProxyApiCreditReportAttributeEntity>();
        segmentsList = new List<ProxyApiCreditReportSegmentEntity>();
    }

    public ProxyApiCreditReportEntity(Long id, String name, Boolean isSoftPull, String automatedDecision, Datetime creationDate) {
        this.decision = false;
        this.id = id;
        this.name = name;
        this.isSoftPull = isSoftPull;
        this.automatedDecision = automatedDecision;
        this.creationDate = creationDate;
    }

    public ProxyApiCreditReportEntity(String errorMessage, String errorDetails) {
        this.errorMessage = errorMessage;
        this.errorDetails = errorDetails;
        this.isSoftPull = false;
        this.decision = false;
    }
}