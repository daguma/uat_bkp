@isTest public class PersonRequestParameterTest {
    @isTest static void validates(){
        PersonRequestParameter prp = new PersonRequestParameter();
        
        prp.firstName = 'David';
        prp.lastName = 'Testcase';
        prp.ssn = '123456789';
        
        System.assertEquals('David', prp.firstName);
        System.assertEquals('Testcase', prp.lastName);
        System.assertEquals('123456789', prp.ssn);
    }
}