@isTest
private class contactTrigger_Test{

    @isTest static void updates_contact_info(){
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(thisUser) {

            Contact c = LibraryTest.createContactTH();
            
            Offer__c off = new Offer__c();
            off.Contact__c = c.id;
            off.Fee__c = 50.0;
            off.IsSelected__c = true;
            off.Repayment_Frequency__c = 'Monthly';
            off.Term__c = 10;
            off.APR__c = 0.25;
            off.Loan_Amount__c = 5000.0;
            off.Installments__c = 10;
            off.Payment_Amount__c = 200;
            off.Effective_APR__c = 0.2;
            off.NeedsApproval__c = false;
            insert off;
            
            c.Email = 'newmail@test.com';
            c.Annual_Income__c = 150000;
            c.MailingState = 'CA';
            c.employment_Type__c = 'Self Employed';
            update c;
            
            c = [SELECT Email FROM Contact WHERE Id = : c.Id LIMIT 1];
    
            System.assertEquals('newmail@test.com', c.Email);
        }
    }
}