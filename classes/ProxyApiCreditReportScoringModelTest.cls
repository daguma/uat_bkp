@isTest public class ProxyApiCreditReportScoringModelTest {
    @isTest static void validate() {
        
        ProxyApiCreditReportScoringModelEntity models = new ProxyApiCreditReportScoringModelEntity ();
        
        models.modelName = 'test data';
        system.assertEquals('test data', models.modelName);
        
        models.grade = 'test data';
        system.assertEquals('test data', models.grade);
        
        models.coefficientsList = new List<ProxyApiCreditReportCoefficientEntity> ();
        system.assertEquals(0, models.coefficientsList.size());
        
        models.dollarRateBadProb = 'test data';
        system.assertEquals('test data', models.dollarRateBadProb );
        
        models.unitBadProb = 'test data';
        system.assertEquals('test data', models.unitBadProb );
        
        models.score  = 'test data';
        system.assertEquals('test data', models.score);
    }
}