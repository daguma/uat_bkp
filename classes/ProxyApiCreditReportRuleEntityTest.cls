@isTest 
public class ProxyApiCreditReportRuleEntityTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ProxyApiCreditReportRuleEntity  re = new ProxyApiCreditReportRuleEntity ();
        Test.stopTest();

        System.assertEquals(null, re.deRuleNumber);
        System.assertEquals(null, re.deRuleName);
        System.assertEquals(null, re.rulePriority);
        System.assertEquals(null, re.deRuleValue );
        System.assertEquals(null, re.approvedBy);
        System.assertEquals(null, re.deQueueAssignment);
        System.assertEquals(null, re.deRuleStatus);
        System.assertEquals(null, re.deBRMS_ReasonCode);
        System.assertEquals(null, re.deSequenceCategory);
        System.assertEquals(null, re.deRuleCategory);
        System.assertEquals(null, re.deActionInstructions);
        System.assertEquals(null, re.deHumanReadableDescription);
    }

    static testmethod void validate_assign() {
        Test.startTest();
        ProxyApiCreditReportRuleEntity  re = new ProxyApiCreditReportRuleEntity ();
        Test.stopTest();
        re.deRuleNumber = '1';
        re.deRuleName = 'Test rule';
        re.rulePriority = '1';
        re.deRuleValue = '100';
        re.approvedBy = 'System';
        re.deQueueAssignment = 'Level 1';
        re.deRuleStatus = 'Fail';
        re.deBRMS_ReasonCode = 'Did not pass disqualifiers';
        re.deSequenceCategory = '1';
        re.deRuleCategory = 'FIN';
        re.deActionInstructions = 'Test instructions';
        re.deHumanReadableDescription = 'Test description';
        System.assertEquals('1', re.deRuleNumber);
        System.assertEquals('Test rule', re.deRuleName);
        System.assertEquals('1', re.rulePriority);
        System.assertEquals('100', re.deRuleValue);
        System.assertEquals('System', re.approvedBy);
        System.assertEquals('Level 1', re.deQueueAssignment);
        System.assertEquals('Fail', re.deRuleStatus);
        System.assertEquals('Did not pass disqualifiers', re.deBRMS_ReasonCode);
        System.assertEquals('1', re.deSequenceCategory);
        System.assertEquals('FIN', re.deRuleCategory);
        System.assertEquals('Test instructions', re.deActionInstructions);
        System.assertEquals('Test description', re.deHumanReadableDescription);
        
    }
}