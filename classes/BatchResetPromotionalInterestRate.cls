global class BatchResetPromotionalInterestRate implements Database.Batchable<sObject>,Schedulable , Database.Stateful {
    global LIST<Logging__c> exceptionLogs=new LIST<Logging__c>();
    Integer count = 0;

    public void execute(SchedulableContext SC){
        system.debug('---IN---');
        BatchResetPromotionalInterestRate batchapex=new BatchResetPromotionalInterestRate();
        Id batchId=Database.executeBatch(batchapex, 10);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        Date endDate = Date.today();
        //Commenting undeployed changes
        //Removed LoanHero_Industry__c field as it is no longer in use
        //return Database.getQueryLocator('SELECT Id, Is_On_Discount_Promotion__c , Name, Day_Past_Oldest_Unpaid_Due_Date__c, Discount_End_Date__c,Promotion_Grace_Days__c,Promotion_Period_Rate__c FROM loan__Loan_Account__c where (Application__r.Lead_Sub_source__c = \'LoanHero\' OR Application__r.LeadSource__c = \'EZVerify\') and Is_On_Discount_Promotion__c = true and loan__Loan_Status__C like \'Active%\' AND (Day_Past_Oldest_Unpaid_Due_Date__c > 0 OR Discount_End_Date__c <: endDate)  ');
        return Database.getQueryLocator('SELECT Id, Is_On_Discount_Promotion__c , Name, Day_Past_Oldest_Unpaid_Due_Date__c, LoanHero_Merchant_Discount_Amount__c, Discount_End_Date__c, LoanHero_Merchant_Discount_Pct__c,LoanHero_Merchant_State__c,LoanHero_Merchant__c,LoanHero_Promo_Discount_Amount__c,LoanHero_Promo_Discount_Pct__c,Promotion_Bust_Date__c,Promotion_Grace_Days__c,Promotion_Period_Rate__c,Promotion_Type__c FROM loan__Loan_Account__c where  Is_On_Discount_Promotion__c = true and loan__Loan_Status__C like \'Active%\' AND (Day_Past_Oldest_Unpaid_Due_Date__c > 0 OR Discount_End_Date__c < TODAY  ) ');
    }
    
    
    global void execute(Database.BatchableContext BC,LIST<loan__Loan_Account__c> scope){
        List<loan__Loan_Account__c> clUpdate = new List<loan__Loan_Account__c>();
        Date endDate = Date.today();
        
        for(loan__Loan_Account__c clContract:scope){
            Integer iGrace = clContract.Promotion_Grace_Days__c == null ? 0 : clContract.Promotion_Grace_Days__c.intValue();
            System.debug('Check contract ' + clContract.Name + ' dpd = ' + clContract.Day_Past_Oldest_Unpaid_Due_Date__c + ' end date = ' + clContract.Discount_End_Date__c + ' Grace = ' + iGrace);
            if (  endDate > clContract.Discount_End_Date__c || 
                    clContract.Day_Past_Oldest_Unpaid_Due_Date__c > iGrace ) {
               System.debug('Contract busted!');
                clContract.Is_On_Discount_Promotion__c = false;
                clUpdate.add(clContract);
                count++;
            }

        }
        try{
            if (clUpdate.size() > 0) update clUpdate;
        }
        catch(exception e){
            WebToSFDC.notifyDev('BatchResetPromotionalReset Error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
    }
    global void finish(Database.BatchableContext BC){
         WebToSFDC.notifyDev('BatchResetPromotionalInterestRate Finished', 'Deals reset = ' + count,  UserInfo.getUserId() );

        if(exceptionLogs.size()>0) insert exceptionLogs;
    }
}