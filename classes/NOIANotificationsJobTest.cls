@isTest
private class NOIANotificationsJobTest {

    @isTest static void schedule_job() {
        Test.startTest();

        NOIANotificationsJob notificationJob = new NOIANotificationsJob();
        String cron = '0 0 23 * * ?';
        system.schedule('Test NOIA Notification Job', cron, notificationJob);

        Test.stopTest();
    }

    @isTest static void updates_noia_sent_date() {
        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.status__c = 'Credit Qualified';
        app.LeadSource__c = 'Online Aggregator';
        app.Lead_Sub_Source__c = 'LendingTree';
        update app;

        Datetime newDate = Datetime.now().addDays(-26);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        List<Opportunity> appsList = [SELECT Id, Name, Customer_Name__c, Contact__c, CreatedDate, Encrypted_AppId_AES__c, LeadSource__c, Lead_Sub_Source__c
                FROM Opportunity
                WHERE Status__c NOT IN ('Funded', 'Credit Approved - No Contact Allowed', 'Declined (or Unqualified)')];

        NOIANotificationsJob noiaNotification = new NOIANotificationsJob();
        noiaNotification.execute(bc, appsList);

        Test.stopTest();
        
        
        Opportunity result = [SELECT NOIA_Sent_Date__c
                              FROM Opportunity
                              WHERE id = : app.id LIMIT 1];
        
        System.assertEquals(Date.today(), result.NOIA_Sent_Date__c); 
    }

    @isTest static void updates_noia_sent_date_with_lead_source() {
        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.status__c = 'Credit Qualified';
        app.Lead_Sub_Source__c = 'Test';
        app.LeadSource__c = 'Organic';
        app.Type = 'New';
        
        update app;

        Datetime newDate = Datetime.now().addDays(-26);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        List<Opportunity> appsList = [SELECT Id, Name, Customer_Name__c, Contact__c, CreatedDate, Encrypted_AppId_AES__c, LeadSource__c, Lead_Sub_Source__c
                FROM Opportunity
                WHERE Status__c NOT IN ('Funded', 'Credit Approved - No Contact Allowed', 'Declined (or Unqualified)')];

        NOIANotificationsJob noiaNotification = new NOIANotificationsJob();
        noiaNotification.execute(bc, appsList);

        Test.stopTest();

              Opportunity result = [SELECT NOIA_Sent_Date__c
                          FROM Opportunity
                          WHERE id = : app.id LIMIT 1];

        System.assertEquals(Date.today(), result.NOIA_Sent_Date__c);
         
    }

    @isTest static void does_not_update_noia_sent_date() {
        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.status__c = 'Credit Approved - No Contact Allowed';
        app.Lead_Sub_Source__c = 'Test';
        app.LeadSource__c = 'Test';
        update app;

        Datetime newDate = Datetime.now().addDays(-24);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        List<Opportunity> appsList = [SELECT Id, Name, Customer_Name__c, Contact__c, CreatedDate, Encrypted_AppId_AES__c, LeadSource__c, Lead_Sub_Source__c
                FROM Opportunity
                WHERE Status__c NOT IN ('Funded', 'Credit Approved - No Contact Allowed', 'Declined (or Unqualified)')
                AND Contact__r.NOIA_Notification_Sent_Date__c <> TODAY ORDER BY Id DESC];

        NOIANotificationsJob noiaNotification = new NOIANotificationsJob();
        noiaNotification.execute(bc, appsList);

        Test.stopTest();

        Opportunity result = [SELECT NOIA_Sent_Date__c
                          FROM Opportunity
                          WHERE id = : app.id LIMIT 1];

        System.assertEquals(null, result.NOIA_Sent_Date__c);
    }

}