@isTest public class EmailAgeResponseStatusTest {
     @isTest static void validate() {
         
         EmailAgeResponseStatus ea = new EmailAgeResponseStatus();
         
         ea.status = 'success';
         System.assertEquals('success', ea.status);
         
         ea.errorCode = '0';
         System.assertEquals('0', ea.errorCode);
         
         ea.description = 'test';
         System.assertEquals('test', ea.description);
     }
}