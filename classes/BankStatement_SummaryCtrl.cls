public with sharing class BankStatement_SummaryCtrl {

    @AuraEnabled
    public static String saveOpportunity(Opportunity opportunity){
        String result = '';
        try{
            if(opportunity != null){
                Opportunity oppToSave = new Opportunity();
                oppToSave.Id = opportunity.id;
                oppToSave.DL_Available_Balance__c = opportunity.DL_Available_Balance__c;
                oppToSave.DL_As_of_Date__c = opportunity.DL_As_of_Date__c;
                oppToSave.DL_Current_Balance__c = opportunity.DL_Current_Balance__c;
                oppToSave.DL_Average_Balance__c = opportunity.DL_Average_Balance__c;
                oppToSave.DL_Deposits_Credits__c = opportunity.DL_Deposits_Credits__c;
                oppToSave.DL_Avg_Bal_Latest_Month__c = opportunity.DL_Avg_Bal_Latest_Month__c;
                oppToSave.DL_Withdrawals_Debits__c = opportunity.DL_Withdrawals_Debits__c;
                oppToSave.Number_of_Negative_Days_Number__c = opportunity.Number_of_Negative_Days_Number__c;
                
                oppToSave.Average_Daily_Balance_Amount__c = oppToSave.DL_Average_Balance__c;
                oppToSave.Current_Balance_Amount__c = oppToSave.DL_Current_Balance__c;
                oppToSave.Total_Deposits_Amount__c = oppToSave.DL_Deposits_Credits__c;
                
                update oppToSave;
                result = 'success';
            }
        }catch(Exception e){
            result = e.getMessage() + ' - ' + e.getStackTraceString();
        }
        return result;
    }

}