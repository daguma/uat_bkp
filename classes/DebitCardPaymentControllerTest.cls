@isTest 
public class DebitCardPaymentControllerTest {
    
    //static loan__Loan_Account__c loan=TestHelper.createContract();
    static DebitCardPaymentController.deserializeResponse pinPadParams = new DebitCardPaymentController.deserializeResponse(); 
    static string TID;
    @future private static void addTemplte(){  
        List<EmailTemplate> lstEmail = new List<EmailTemplate>();
        EmailTemplate eTemplate = new EmailTemplate(FolderId=UserInfo.getUserId(),TemplateType='Custom',name='Payment_Monthly', HtmlValue='Welcome', Subject='subject',Body='Body',DeveloperName='Payment_Monthly');lstEmail.add(eTemplate);
        eTemplate = new EmailTemplate(FolderId=UserInfo.getUserId(),TemplateType='Custom',Name='Payment_Submission',HtmlValue='Welcome',Subject='subject',Body='Body',DeveloperName='Payment_Submission');lstEmail.add(eTemplate);
        eTemplate = new EmailTemplate(FolderId=UserInfo.getUserId(),TemplateType='Custom',Name='Payment_Failed',HtmlValue='Welcome',Subject='subject',Body='Body',DeveloperName='Payment_Failed');lstEmail.add(eTemplate);
        insert lstEmail;
    }
    
    @isTest static void Proceed1(){ 
        
        loan__Loan_Account__c loan=TestHelper.createContract();
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);        
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        
        objDebitCardPayment.lId = System.currentPageReference().getParameters().put('id',loan.id);
        
        loan.loan__Pay_Off_Amount_As_Of_Today__c=100;
        loan.loan__OT_ACH_Debit_Date__c=Date.Today().addDays(1);
        loan.loan__OT_ACH_Payment_Amount__c=123;
        update loan;
        
        objDebitCardPayment.AdditionalPayment=10; 
        objDebitCardPayment.expirationDate ='1216';
        objDebitCardPayment.selectedExpirationYear='2016';
        objDebitCardPayment.selectedExpirationMonth='12';
        string slectedPaymentTypeID=ApexPages.currentPage().getParameters().put('selected' ,'AdditionalPaymentSelection');
            objDebitCardPayment.amount=objDebitCardPayment.AdditionalPayment;
            
            SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                                                                           'OK',
                                                                           '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                                                                           null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            test.startTest();
            Pagereference pr = objDebitCardPayment.Proceed();
            test.stoptest();
            System.assertEquals(null, pr);
    } 
    
    @isTest static void Proceed2(){
        
        List<PayLeap_Messages__c> lstMsg=new List<PayLeap_Messages__c>();
        PayLeap_Messages__c obj;
        obj = new PayLeap_Messages__c(name='115',Message__c='Please confirm API Login ID (username) and Transaction Key (password) are correct and reattempt call.',description__c='vfd');lstMsg.add(obj);
        obj = new PayLeap_Messages__c(name='19',Message__c='Please Confirm your card details are correct.',description__c='vfv');lstMsg.add(obj);
        obj = new PayLeap_Messages__c(name='310',Message__c='The Transaction is Declined. Please use a different card or contact your bank.',description__c='vf');lstMsg.add(obj);
        insert lstMsg;  
        
        pinPadParams.AuthCode='12324';pinPadParams.sessionId='frt435435';pinPadParams.GetAVSResult='T';pinPadParams.GetCVResult='T';pinPadParams.GetCommercialCard='F';
        pinPadParams.HostCode='7';pinPadParams.InvNum='54gtr76';pinPadParams.ProcessedAsCreditCard='F';pinPadParams.Exponent='012';pinPadParams.GUID='43gfvgfg43';
        pinPadParams.InnerErrorCode='90';pinPadParams.InnerErrorMessage='';pinPadParams.Modulus='kui9879787';pinPadParams.PinReferenceId='9876';pinPadParams.Amount=10.0;pinPadParams.expirationDate='1216';
        
        loan__Loan_Account__c loan=TestHelper.createContract();
        ApexPages.StandardController standardCtrl = new ApexPages.StandardController(loan);        
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        objDebitCardPayment.lId = System.currentPageReference().getParameters().put('id',loan.id);
        loan.loan__Pay_Off_Amount_As_Of_Today__c=0;
        loan.loan__OT_ACH_Debit_Date__c=Date.Today().addDays(1);
        loan.loan__OT_ACH_Payment_Amount__c=123;
        update loan;
        
        objDebitCardPayment.selectedExpirationYear='2016';
        objDebitCardPayment.selectedExpirationMonth='12';
        objDebitCardPayment.debitCardNumber='4111111111111111';
        
        try{
            objDebitCardPayment.AdditionalPayment=100;
            //objDebitCardPayment.expirationDate ='1216';
            string slectedPaymentTypeID=ApexPages.currentPage().getParameters().put('selected' ,'AdditionalPaymentSelection');
            objDebitCardPayment.amount=objDebitCardPayment.AdditionalPayment;
            
            SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                                                                           'OK',
                                                                           '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                                                                           null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            test.startTest();
            Pagereference pr = objDebitCardPayment.Proceed();
            test.stoptest();
            System.assertEquals(null, pr);
        }
        catch(exception e){
            system.debug('Error: Invalid Input.');
        }
        
        
        try{
            objDebitCardPayment.AdditionalPayment=100;
            objDebitCardPayment.expirationDate ='1216';
            objDebitCardPayment.PaymenttowardsPastDueBalance=200;
            
            string slectedPaymentTypeID=ApexPages.currentPage( ).getParameters( ).put( 'selected' , 'PastDueBalancePaymentSelection');
            objDebitCardPayment.amount=objDebitCardPayment.PaymenttowardsPastDueBalance;
            
            SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                                                                           'OK',
                                                                           '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                                                                           null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            
            Pagereference pr = objDebitCardPayment.Proceed();
            System.assertEquals(null, pr);
            
        }
        catch(exception e){
            system.debug('Error: Invalid Input.');
        }
        
        try {
            objDebitCardPayment.AdditionalPayment=100;
            objDebitCardPayment.expirationDate ='1216';
            objDebitCardPayment.PaymenttowardsPastDueBalance=200;
            
            string slectedPaymentTypeID=ApexPages.currentPage( ).getParameters( ).put( 'selected' , 'MonthlyPaymentSelection');
            objDebitCardPayment.amount=loan.loan__ACH_Debit_Amount__c;
            
            SingleRequestMockTest fakeResponse = new SingleRequestMockTest(200,
                                                                           'OK',
                                                                           '[{"Proxy_API_DefaultURL__c": "http://www.google.com"}]',
                                                                           null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            
            Pagereference pr = objDebitCardPayment.Proceed();
            System.assertEquals(null, pr);
        }
        catch(exception e){
            system.debug('Error: Invalid Input.');
        }
        
    } 
    
    @isTest static void getcardType() {
        
        loan__loan_account__c objcontract=TestHelper.createContract();
        
        ApexPages.StandardController standardCtrl= new ApexPages.StandardController(objcontract);
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        
        List<SelectOption> testOptions = objDebitCardPayment.getcardType();
        
        System.assertNotEquals(null, testOptions);
    } 
    
    @isTest static void getMonth() {
        loan__loan_account__c objcontract=TestHelper.createContract();
        ApexPages.StandardController standardCtrl= new ApexPages.StandardController(objcontract);
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        List<SelectOption> testOptions = objDebitCardPayment.getlistMonth();
        System.assertNotEquals(null, testOptions);
    }
    
    @isTest static void getYear() {
        loan__loan_account__c objcontract=TestHelper.createContract();
        ApexPages.StandardController standardCtrl= new ApexPages.StandardController(objcontract);
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        
        List<SelectOption> testOptions = objDebitCardPayment.getlistYear();
        
        System.assertNotEquals(null, testOptions);
    } 
    
    @isTest static void goBack() {
        loan__loan_account__c objcontract=TestHelper.createContract();
        
        ApexPages.StandardController standardCtrl= new ApexPages.StandardController(objcontract);
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        
        Pagereference pr = objDebitCardPayment.goBack();
        
        System.assertNotEquals(null, pr);
        
    } 
    @isTest static void createMessage(){
        loan__loan_account__c objcontract=TestHelper.createContract();
        
        ApexPages.StandardController standardCtrl= new ApexPages.StandardController(objcontract);
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        ApexPages.severity severity=ApexPages.severity.INFO;
        objDebitCardPayment.createMessage(severity,'Success!!');
    }
    
    @isTest static void AddNote1(){
        
        loan__loan_account__c objcontract=TestHelper.createContract();
        
        ApexPages.StandardController standardCtrl= new ApexPages.StandardController(objcontract);
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        
        ApexPages.severity msgSeverity = ApexPages.severity.ERROR;
        string Result=apexpages.currentpage().getparameters().put('Result','0');
        TID=apexpages.currentpage().getparameters().put('TID','75674');
        string innerErrorMessage=apexpages.currentpage().getparameters().put('InnerErrorMessage','');
        string APIRequest=apexpages.currentpage().getparameters().put('APIRequest','Accylynk Request');
        string ID = apexpages.currentpage().getparameters().put('id','424325rf3');
        string TransType=apexpages.currentpage().getparameters().put('TransType','SaleComplete');
        String sResult='0';String sRespMSG='';String sAPIRequest='https://google.com';string sPNRef='12345';
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        objDebitCardPayment.paymentType='Monthlyselection';
        System.RunAs(usr)
        {
            Test.startTest();
            addTemplte();
            Test.stopTest();
        }
        Pagereference pr =objDebitCardPayment.AddNote();
        System.assertEquals(null, pr);
        
    }
    @isTest static void AddNote2(){
        loan__loan_account__c objcontract=TestHelper.createContract();
        
        ApexPages.StandardController standardCtrl= new ApexPages.StandardController(objcontract);
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        
        ApexPages.severity msgSeverity = ApexPages.severity.ERROR;
        string Result=apexpages.currentpage().getparameters().put('Result','2');
        TID=apexpages.currentpage().getparameters().put('TID','75674');
        string innerErrorMessage=apexpages.currentpage().getparameters().put('InnerErrorMessage','');
        string APIRequest=apexpages.currentpage().getparameters().put('APIRequest','Accylynk Request');
        string ID = apexpages.currentpage().getparameters().put('id','424325rf3');
        String sResult='0';String sRespMSG='';String sAPIRequest='https://google.com';string sPNRef='12345';
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        objDebitCardPayment.paymentType='selection'; 
        System.RunAs(usr)
        {
            Test.startTest();
            addTemplte();
            Test.stopTest();
        }
        Pagereference pr =objDebitCardPayment.AddNote();
        System.assertEquals(null, pr);
        
        
    }
    @isTest static void makeSaleCompleteCall(){
        loan__loan_account__c objcontract=TestHelper.createContract();
        
        ApexPages.StandardController standardCtrl= new ApexPages.StandardController(objcontract);
        DebitCardPaymentController objDebitCardPayment = new DebitCardPaymentController(standardCtrl);
        
        String sResult='0';String sRespMSG='';String sAPIRequest='https://google.com';string sPNRef='12345';
        string TransType=apexpages.currentpage().getparameters().put('TransType','SaleComplete');
        string ID = apexpages.currentpage().getparameters().put('id','424325rf3');
        objDebitCardPayment.paymentType='Monthlyselection';  
        Pagereference pr = objDebitCardPayment.makeSaleCompleteCall(); 
        System.assertEquals(null, pr);
        
    }
    
    
    
}