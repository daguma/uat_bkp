@isTest public class ContactCtrlTest {

    @isTest static void testSSNPopulate() {

        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl.SSNPopulate(cntct);
        System.assertEquals('999999999', cntct.ints__Social_Security_Number__c);

        ContactCtrl con = new ContactCtrl(cntct.Id);
        System.assertNotEquals(null, con.cont);

    }

    @isTest static void testCallSoftPull() {

        Opportunity app = LibraryTest.createApplicationTH();
        ContactCtrl.callSoftPull(app.Id,app.Contact__c);

    }

    @isTest static void testLdologyList() {

        LibraryTest.createPayfoneSettings();
        Opportunity app = LibraryTest.createApplicationTH();
        IDology_Request__c idr = new IDology_Request__c();
        idr.Name = 'name';
        idr.Result_Message__c = 'not empty';
        idr.Result_Message__c = 'not empty';
        idr.Contact__c = app.contact__c;
        insert idr;
        ContactCtrl con = new ContactCtrl(app.contact__c);

    }

    @isTest static  void testTokenRequest(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl cnt = new ContactCtrl(cntct.Id);
        try{
            cnt.tokenRequest();
        }catch(Exception e){
            system.debug(e);
        }
        system.assertEquals(null, cnt.errorMessage);

        cnt.payfoneSettings = null;
        try{
            cnt.tokenRequest();
        }catch(Exception e){
            system.debug(e);
        }
    }

    @isTest static  void testOthers(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl cnt = new ContactCtrl(cntct.Id);

        cnt.setErrorMessage('Error');
        system.assertEquals(true,cnt.hasErrorMessage);

        cnt.clearMessage();
        system.assertEquals(false,cnt.hasErrorMessage);

    }

    @isTest static  void testRecoverQuestions(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl src = new ContactCtrl(cntct.Id);

        ContactCtrl res = ContactCtrl.recoverQuestions(JSON.serialize(src));
        system.debug('DG TEST testRecoverQuestions: ' + JSON.serialize(res));

    }

    @isTest static  void testCallAsyncPayfone(){

        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl src = new ContactCtrl(cntct.Id);
        src.callAsyncPayfone();

    }

    @isTest static  void testCheckSettings(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl cnt = new ContactCtrl(cntct.Id);
        system.assertEquals(true, cnt.CheckSettings());

        cntct.MailingPostalCode = '';
        update cntct;
        cnt = new ContactCtrl(cntct.Id);
        system.assertEquals(false, cnt.CheckSettings());

    }

    @isTest static void testCallApiQuestions(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl cnt = new ContactCtrl(cntct.Id);

        //Methods defined as  do not support Web service callouts
        //HTTPResponse res = cnt.CallApiToGetQuestions();
        //system.debug('DG TEST testCallApiQuestions: ' + JSON.serialize(res));

    }

    @isTest static  void testGetKYCPassBy(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl cnt = new ContactCtrl(cntct.Id);

        String res = cnt.getKYCPassBy();
        system.debug('DG TEST testGetKYCPassBy: ' + JSON.serialize(res));

    }

    @isTest static  void testProcessQuestions(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl src = new ContactCtrl(cntct.Id);
        String cnt = ContactCtrl.processQuestions(JSON.serialize(src));
    }

    @isTest static  void testProcessAnswersResultXml(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl cnt = new ContactCtrl(cntct.Id);
        cnt.ProcessAnswersResultXml(LibraryTest.GenerateTestingData4());
        system.debug('DG TEST ProcessAnswersResultXml: ' + JSON.serialize(cnt.errorMessage));

    }

    @isTest static  void testSubmit(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();

        string year = '1976';
        string month = '10';
        string day = '5';
        string hour = '12';
        string minute = '20';
        string second = '20';
        string stringDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
        String res = ContactCtrl.submitApplicaiton(cntct.Id,stringDate);

        system.debug('DG TEST testSubmit: ' + JSON.serialize(res));

    }

    @isTest static  void testVerifyEmail(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        String res = ContactCtrl.verifyEmail(cntct.Id);

        system.debug('DG TEST testVerifyEmail: ' + JSON.serialize(res));

    }

    @isTest static  void testMarkIncomeNotEnough(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        String res = ContactCtrl.markIncomeNotEnough(cntct.Id);

        system.debug('DG TEST testMarkIncomeNotEnough: ' + JSON.serialize(res));

    }

    @isTest static  void testRefreshFICOInfo(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        try{
            ContactCtrl.refreshFICOInfo(cntct.Id);
        }catch(Exception e){
            system.debug('DG TEST testRefreshFICOInfo: ' + e);
        }
    }

    @isTest static  void testResetAuthSSNCount(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl.resetAuthSSNCount(cntct.Id);


    }

    @isTest static  void testCreateUserforCustomerPortal(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        String res = ContactCtrl.CreateUserforCustomerPortal(cntct.Id);

        system.debug('DG TEST testCreateUserforCustomerPortal: ' + JSON.serialize(res));

    }

    @isTest static  void testGetContactEntity(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl res = ContactCtrl.getContactEntity(cntct.Id);

        system.debug('DG TEST testGetContactEntity: ' + JSON.serialize(res));

    }

    @isTest static  void testPayfoneRequest(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl src = new ContactCtrl(cntct.Id);

        try{
            ContactCtrl res = ContactCtrl.payfoneRequest(JSON.serialize(src));
            system.debug('DG TEST payfoneRequest: ' + JSON.serialize(res));
        }catch(Exception e){
            system.debug('DG TEST payfoneRequest: ' + e);
        }

    }

    @isTest static  void testCheckAnswersSettings(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        ContactCtrl src = new ContactCtrl(cntct.Id);
        Boolean res = src.CheckAnswersSettings('2');
        system.debug('DG TEST testCheckAnswersSettings: ' + JSON.serialize(res));

    }

    @isTest static void mark_as_income_not_enough() {

        LibraryTest.createPayfoneSettings();

        Contact c = LibraryTest.createContactTH();
        ContactCtrl lc = new ContactCtrl(c.Id);
        String result = ContactCtrl.markAsIncomeNotEnough(String.valueOf(c.id));
        System.assertEquals('Opportunity Not Found!', result);

        Account acc = new Account();
        acc.Bank_Name__c = 'Test Bank';
        acc.Bureau__c = 'TU';
        acc.Bank_Account_Number__c = '123456789';
        acc.First_Name__c = 'Test';
        acc.Email__c = 'test@test2.com';
        acc.Last_Name__c = 'Testcase';
        acc.Name_of_Bank__c = 'Test Bank';
        acc.Name = 'Test';
        insert acc;
        c.AccountId = acc.id;
        update c;

        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity';
        opp.CloseDate = Date.today().addDays(5);
        opp.StageName = 'Open';
        opp.AccountId = acc.id;
        opp.Type = 'New';

        opp.Contact__c = c.Id;
        opp.Status__c = 'NEW - ENTERED';
        insert opp;

        lc = new ContactCtrl(c.Id);
        
        result = ContactCtrl.markAsIncomeNotEnough(String.valueOf(c.id));
        System.assertEquals('Opportunity was closed and marked as Not Enough income!', result);
    }
    @isTest static void save_expect_id_result() {

        LibraryTest.createPayfoneSettings();

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c = [SELECT id, name FROM Contact WHERE id = : app.Contact__c LIMIT 1];

        ContactCtrl.saveExpectIdresult('Test message', c.id);

        Opportunity appRes = [select id, ExpectID_Result__c from Opportunity where contact__c = : c.id order by CreatedDate desc LIMIT 1];
        System.assertEquals('Test message', appRes.ExpectID_Result__c);
    }

    @isTest static void processes_xml_result() {

        LibraryTest.createPayfoneSettings();

        ExpectId_Email_Settings__c emailSettings = new ExpectId_Email_Settings__c();
        emailSettings.ExpectId_Key__c = 'resultcode.address.does.not.match';
        emailSettings.Name = 'Test ProcessResultXml';
        insert emailSettings;

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c = [SELECT id, name FROM Contact WHERE id = : app.Contact__c LIMIT 1];

        ContactCtrl lc = new ContactCtrl(c.Id);

        lc.ProcessResultXml(LibraryTest.GenerateTestingData4());
        Opportunity appRes = [select id, ExpectID_Result__c from Opportunity where contact__c = : c.id order by CreatedDate desc LIMIT 1];
        System.assertEquals('Address Does Not Match', appRes.ExpectID_Result__c);
    }
    
    @isTest static void processes_xml_result_PASS() {

        LibraryTest.createPayfoneSettings();

        ExpectId_Email_Settings__c emailSettings = new ExpectId_Email_Settings__c();
        emailSettings.ExpectId_Key__c = 'resultcode.address.does.not.match';
        emailSettings.Name = 'Test ProcessResultXml';
        insert emailSettings;

        Opportunity app = LibraryTest.createApplicationTH();
        Contact c = [SELECT id, name, kyc_mode__c, Trust_Score__c, Payfone_Verified_Flag__c, last_Name_score__c,first_name_score__c, address_score__c  FROM Contact WHERE id = : app.Contact__c LIMIT 1];
        c.Trust_Score__c = 700 ;
        c.Payfone_Verified_Flag__c = True;
        c.last_Name_score__c = 90;
        c.first_name_score__c = 90;
        c.address_score__c  = 90;
        c.kyc_mode__c = 'Payfone';
        update c;

        ContactCtrl lc = new ContactCtrl(c.Id);
        LibraryTest.createPayfoneSettings();
        IDology_Request__c idr = new IDology_Request__c();
        idr.Name = 'name';
        idr.Result_Message__c = 'not empty';
        idr.Result_Message__c = 'not empty';
        idr.Contact__c = app.contact__c;
        insert idr;
        lc.recoverQuestions();
        lc.callAsyncPayfone();
        
        lc.ProcessResultXml(LibraryTest.GenerateTestingData2());
    }
    
    @isTest static  void cancel(){
        LibraryTest.createPayfoneSettings();
        Contact cntct = LibraryTest.createContactTH();
        cntct.Trust_Score__c = 700 ;
        cntct.Payfone_Verified_Flag__c = True;
        cntct.last_Name_score__c = 90;
        cntct.first_name_score__c = 90;
        cntct.address_score__c  = 90;
        cntct.kyc_mode__c = 'Payfone';
        update cntct;
        ContactCtrl src = new ContactCtrl(cntct.Id);

        ContactCtrl.cancelQuestionsProcess(JSON.serialize(src));
        
        IDology_Request__c idr = new IDology_Request__c();
        idr.Name = 'name';
        idr.Result_Message__c = 'not empty';
        idr.Result_Message__c = 'not empty';
        idr.Contact__c = cntct.id;
        insert idr;
        src.callAsyncPayfone();
        src.ProcessAnswersResultXml(LibraryTest.GenerateTestingData2());
        src.ProcessResultXml(LibraryTest.GenerateTestingData2());
        src.recoverQuestions();
        src.payfoneResult = 'Pass';
        src.questionsRecovered = true;
        src.questionsProceeded = false;
        src.cancelButton();
    }
}