global class BatchFinwisePurchaseJob implements Database.Batchable<sObject>,Schedulable , Database.Stateful {
    DateTime cutOffDate = null, newDate = DateTime.now();
    Integer count =0, daysToPurchase = 2;
    String query = '', purchaseLine = '2018 Funding Trust';
	FinwiseDetails__c FinwiseParams = FinwiseDetails__c.getInstance();   
    Decimal totalAmount = 0;
    public void calculateDate() {
        if (FinwiseParams != null && FinwiseParams.Days_To_Purchase__c != null) daysToPurchase = FinwiseParams.Days_To_Purchase__c.intValue();
        loan__ACH_Parameters__c acha = loan__ACH_Parameters__c.getInstance();
        system.debug('acha:: ' + acha);
        system.debug('acha.Value:: ' + acha.Disbursal_File_Cutoff_Hour__c);
        Time newT = Time.newInstance(acha.Disbursal_File_Cutoff_Hour__c.intValue(), 0, 0, 0);
        integer bd = daysToPurchase;
        Date toda = CalculateWorkDays.dateWorkDays(Date.today(), -bd);

        cutOffDate = DateTime.newInstance(toda, newT);
        System.debug('CutOff Date = ' + cutOffDate );
        
    }
  	public void execute(SchedulableContext SC){
         BatchFinwisePurchaseJob batchapex=new BatchFinwisePurchaseJob();
        Id batchId=Database.executeBatch(batchapex, 10);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        calculateDate();
        query = 'select id, CreatedDate, Asset_Sale_Date__C, Asset_Sale_line__c, loan__Principal_remaining__c from Loan__Loan_Account__c where ASset_Sale_Line__C = \'Finwise\' and loan__Loan_Status__c like \'Active%\' and Asset_Sale_Date__C <= : cutOffDate ';
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC,LIST<loan__Loan_Account__c> scope){
        List<loan__Loan_Account__c> clUpdate = new List<loan__Loan_Account__c>();
   		if (FinwiseParams != null && FinwiseParams.LP_Purchase_Line__c != null) purchaseLine = FinwiseParams.LP_Purchase_Line__c;
        
        for(loan__Loan_Account__c clContract:scope){
            clContract.Asset_Sale_Line__c = purchaseLine;
            clContract.Asset_Sale_Date__c = newDate;
            clCOntract.Asset_Sale_Balance__c = clContract.loan__Principal_remaining__c;
            totalAmount = totalAmount  + clContract.loan__Principal_remaining__c;
            clUpdate.add(clContract);
            count++;
        }
        try{
            if (clUpdate.size() > 0) update clUpdate;
        }
        catch(exception e){
            WebToSFDC.notifyDev('BatchFinwisePurchaseJob Error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
        }
    }
    global void finish(Database.BatchableContext BC){
         WebToSFDC.notifyDev('BatchFinwisePurchaseJob Finished', 'Deals purchased = ' + count + '\nTotal Amount = ' + totalAmount.setScale(2) + '\n\n',  UserInfo.getUserId() );
 
    }
}