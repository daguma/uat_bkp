global with sharing class MultiloanPreSoftPull {
  private static final String ELIGIBILITY_REVIEW_QUERY = 'SELECT Id, Contract__c, IsLowAndGrow__c, FICO__c, ' +
      'Eligible_For_Soft_Pull__c, Manual_Eligibility_Check__c, Last_MultiLoan_Eligibility_Review__c, Eligible_For_Soft_Pull_Params__c, LoanAmountCheck__c ' +
      'FROM Multiloan_Params__c ' +
      'WHERE  (Last_MultiLoan_Eligibility_Review__c < {0} OR Last_MultiLoan_Eligibility_Review__c = {4} OR Manual_Eligibility_Check__c = {6} ) AND ' +
      '  Contract__r.loan__Loan_Status__c = {1} AND ' +
      '  Contract__r.loan__Accrual_Start_Date__c < {2} AND ' +
      '  LoanAmountCheck__c >= {3} AND' +
      '  Contract__r.SCRA__c = {5} AND' +
      '  Contract__r.Date_of_Settlement__c = {4} AND  ' +
      '  Contract__r.Settlement__c = {4} AND ' +
      '  Contract__r.DMC__c  = {4} AND ' +
      '  Contract__r.Is_Material_Modification__c = {5} AND ' +
      '  Contract__r.First_Payment_Missed__c = {5} AND ' +
      '  Contract__r.First_Payment_Made_after_Due_Date__c = {5} AND ' +
      '  (Contract__r.loan__ACH_On__c ={6} OR Contract__r.Is_Debit_Card_On__c = {6}) AND ' +
      '  (Contract__r.loan__Metro2_Account_pmt_history_date__c != {4} AND Contract__r.loan__Metro2_Account_pmt_history_date__c < {7}) AND ' +
      '  Contract__r.Opportunity__r.Type = {8} AND ' +
      // '  (Contract__r.Opportunity__r.StrategyType__c = {12} OR Contract__r.Opportunity__r.StrategyType__c = {14}) AND ' +  -- RD-707 Removing Low and Grow restriction
      '  Contract__r.Product_Name__c = {9}  AND ' +
      '  (Contract__r.Asset_Sale_Line__c != {10} AND Contract__r.Asset_Sale_Line__c != {11} ) AND ' +
      '  Contract__r.Servicing_Only_Asset__c != {6} AND' + 
      '  Contract__r.loan__Contact__r.MailingState in ({12}) ' + // RD-707 changed from 13 to 12 
      
      //'  AND contract__r.Opportunity__r.Lead_Sub_source__c != {13} ' + // RD-707 changed from 15 to 13 
      
      ' {13} ';  

  public static String getEligibilityReviewQuery(Integer queryLimit) {

    MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');

    List<String> fillers = new list<String> {'LAST_N_DAYS: ' + (Integer.valueOf(multiloanSettings.Check_Frequency__c) - 1), '\'Active - Good Standing\''}; //0,1
    fillers.add('LAST_N_DAYS:' + (Integer.valueOf(multiloanSettings.Days_To_First_Check__c) - 1)); //2
    fillers.add('' + Integer.valueOf(multiloanSettings.Minimum_Loan_Amount__c)); //3
    fillers.add('' + null); //4
    fillers.add('' + false); //5
    fillers.add('' + true); //6
    fillers.add('today'); //7
    fillers.add('\'New\''); //8
    fillers.add('\'Lending Point\''); //9
    fillers.add('\'Stone Ridge\''); //10
    fillers.add('\'GLJ\''); //11
    // fillers.add('\'1\''); // 12
    fillers.add('\'' + [SELECT Allowed_Multiloan_States__c FROM MultiLoan_Settings__c].Allowed_Multiloan_States__c.replace(',', '\',\'') + '\''); //13 - after RD-707 now 12
    // fillers.add('\'4\''); // 14
    //fillers.add('\'RefinanceGUGG1\''); //15 -- after RD-707 now 13
    if (queryLimit > 0)
      	fillers.add('limit ' + queryLimit);
    else
      fillers.add(''); //14
    return String.format(ELIGIBILITY_REVIEW_QUERY, fillers);
  }

  public static void EligibilityCheck(Multiloan_Params__c params) {
    loan__Loan_Account__c contract = getContract(params.Contract__c);
    if (contract == null) return;
    EligibilityCheckDefault(params, contract);
  }


  public static void EligibilityCheckDefault(Multiloan_Params__c params, loan__Loan_Account__c contract) {
    params.Last_MultiLoan_Eligibility_Review__c = System.today();
    params.Manual_Eligibility_Check__c = false;


    if (overduedayEligibility(params.Id, contract) && refinanceEligibility(params.Id, contract) /*&& acountMag(params, contract)*/ ) {
      MultiloanPostSoftPull.executeProcess( contract.Id, params);
      params.Eligible_For_Soft_Pull__c = true;
    } else {
      params.Eligible_For_Soft_Pull__c = false;
    }

    update params;

  }

  public static boolean overduedayEligibility(String paramId, loan__Loan_Account__c contract) {
    Multiloan_Params_Logs__c multiLogs = new Multiloan_Params_Logs__c();
    multiLogs.Multiloan_Params__c = paramId;
    boolean eligibility = true;
    List<String> rulesFailed = new List<String>();
    Integer paymentLimit = getLimitByFrequency(contract.Payment_Frequency_Masked__c);
    List<loan__Loan_account_Due_Details__c> billsList = [SELECT Id,
                                            loan__Due_Date__c,
                                            CreatedDate,
                                            loan__Payment_Date__c,
                                            loan__Payment_Satisfied__c
                                            FROM  loan__Loan_account_Due_Details__c
                                            WHERE loan__Loan_Account__c = : contract.Id
                                                AND loan__DD_Primary_Flag__c = true
                                                    ORDER BY CreatedDate DESC
                                                    LIMIT : paymentLimit];
    system.debug('billsList: ' + billsList);
    if ((billsList.size()  == paymentLimit) && (paymentLimit > 0)) {
      for (loan__Loan_account_Due_Details__c payment : billsList) {
        if ((payment.loan__Payment_Date__c > payment.loan__Due_Date__c) || !payment.loan__Payment_Satisfied__c) {
          eligibility = false;
          rulesFailed.add(' - Consecutive Payments');
          multiLogs.Overdue_Count_Fail__c = true;
          insert multiLogs;
          MultiloanFails.createIneligibilityNote(contract.Id, rulesFailed);
          break;

        }
      }
    } else {
      multiLogs.Overdue_Count_Fail__c = true;
      eligibility = false;
    }

    return eligibility;
  }
  /**
  * [getLimitByFrequency description]
  * @param  frequency [description]
  * @return           [description]
  */
  public static Integer getLimitByFrequency(String frequency) {

    Integer result = 0;

    if (!String.isEmpty(frequency)) {

      if (frequency.equalsIgnoreCase('Monthly')) {
        result = 6;
      } else if (frequency.equalsIgnoreCase('28 Days')) {
        result = 6;
      } else if (frequency.equalsIgnoreCase('Semi-Monthly')) {
        result = 12;
      } else if (frequency.equalsIgnoreCase('Bi-weekly')) {
        result = 13;
      } else if (frequency.equalsIgnoreCase('Weekly')) {
        result = 24;
      }
    }

    return result;
  }

  /* public static boolean acountMag(Multiloan_Params__c params, loan__Loan_Account__c contract){
    MultiLoan_Settings__c multiloanSettings = MultiLoan_Settings__c.getValues('settings');
    boolean accountMa = true;
    AccountManagementHistory__c managemment;
    for (AccountManagementHistory__c managem : [SELECT Id, CreditReportId__c, FicoValue__c FROM AccountManagementHistory__c WHERE Contract__c = : contract.Id order by CreatedDate desc limit 1 ]){
      managemment = managem;
    }
    if(managemment != null){
      if(managemment.FicoValue__c >= Integer.valueOf(multiloanSettings.Minimum_Fico__c) && managemment.FicoValue__c >= params.FICO__c){
        accountMa = true;
      }
      else{
            Multiloan_Params_Logs__c multiloanParamsLogs = new Multiloan_Params_Logs__c();
            multiloanParamsLogs.Multiloan_Params__c = params.Id;
            multiloanParamsLogs.Fico_Fail__c = true;
            params.New_FICO__c  = managemment.FicoValue__c;
            update params;
            insert multiloanParamsLogs;
            accountMa = false;
      }
    }
    return accountMa;
  } */
  public static boolean refinanceEligibility(String paramId, loan__Loan_Account__c contract){
    Multiloan_Params_Logs__c multiLogs = new Multiloan_Params_Logs__c();
    multiLogs.Multiloan_Params__c = paramId;
    boolean eligibility = true;
    List<String> rulesFailed = new List<String>();

    List<Opportunity> oppList = [SELECT Id, Type, Contact__c 
                                FROM Opportunity 
                                WHERE Contact__c = : contract.loan__Contact__c 
                                                    AND Type = 'Refinance' 
                                                    AND Status__c NOT IN ('Aged / Cancellation','Refinance Unqualified')];
      system.debug('Opplist: ' + oppList);
    if(oppList.size() > 0){
      multiLogs.Refinance_Opp_Fail__c = true;
      insert multiLogs;
        rulesFailed.add(' - Contact with Refinance Opportunity Related');
        MultiloanFails.createIneligibilityNote(contract.Id, rulesFailed);
        eligibility =  false;
    } 

    return eligibility;

  }
  private static loan__Loan_Account__c getContract(String contractId) {

    loan__Loan_Account__c result = null;

    if (!String.isEmpty(contractId)) {
      for (loan__Loan_Account__c contract : [SELECT Id,
                                             loan__Contact__c,
                                             Opportunity__c,
                                             loan__Due_Day__c,
                                             loan__ACH_On__c,
                                             overdue_Days__c,
                                             modification_Type__c,
                                             principal_Balance_Paid_Percentage__c,
                                             payment_Frequency_Masked__c,
                                             Opportunity__r.StrategyType__c,
                                             loan__Principal_Remaining__c,
                                             loan_Active_Payment_Plan__c,
                                             Original_Sub_Source__c,
                                             Product_Name__c,
                                             loan__Contact__r.Military__c,
                                             Opportunity__r.Is_Direct_Pay_On__c,
                                             Is_Material_Modification__c,
                                             Original_Loan_Amount_No_Fee__c

                                             FROM loan__Loan_Account__c
                                             WHERE Id = : contractId]) {
        result = contract;
      }
    }

    return result;
  }
}