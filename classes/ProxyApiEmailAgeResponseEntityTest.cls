@isTest
private class ProxyApiEmailAgeResponseEntityTest {
    @isTest static void validate() {
        ProxyApiEmailAgeResponseEntity emailAge = new ProxyApiEmailAgeResponseEntity();
        
        emailAge.id  = 123;
        System.assertEquals(123, emailAge.id);
        
        emailAge.requestData = 'test request';
        System.assertEquals('test request', emailAge.requestData);
        
        emailAge.emailAgeInquiryId = 2148;
        System.assertEquals(2148, emailAge.emailAgeInquiryId);
        
        emailAge.requestTime  = 0;
        System.assertEquals(0, emailAge.requestTime);
        
        emailAge.processTime  = 81;
        System.assertEquals(81, emailAge.processTime );
        
        emailAge.endProcessTime  = 81;
        System.assertEquals(81, emailAge.endProcessTime );
        
        emailAge.responseData   = 'test response';
        System.assertEquals('test response', emailAge.responseData  );
    }
}