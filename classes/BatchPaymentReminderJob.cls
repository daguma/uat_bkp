global class BatchPaymentReminderJob  implements Database.Batchable<sObject>, Schedulable,  Database.Stateful {
	public integer count = 0;
    global void execute (SchedulableContext sc) {
     
        BatchPaymentReminderJob job = new BatchPaymentReminderJob();
        System.debug('Starting BatchPaymentReminderJob at ' + Date.today());
        Database.executeBatch(job, 25);
    }
    String query = '';
    global Database.QueryLocator start(Database.BatchableContext BC) {
        try {
            query = 'SELECT Id, Payment_Due_10_Day_Reminder_Date__c , loan__Next_Installment_Date__c, loan__ACH_Next_Debit_Date__c, PaymentDue7DayReminderDate__c,Card_Next_Debit_Date__c, loan__ACh_On__c , Is_Debit_Card_On__c ' + 
                ' from loan__Loan_Account__c  ' +
                ' WHERE ( ( loan__Next_Installment_Date__c > NEXT_N_DAYS:9 AND loan__Next_Installment_Date__c = NEXT_N_DAYS:10 AND loan__ACh_On__c = false AND Is_Debit_Card_On__c = false) OR ' +
                '         ( Card_Next_Debit_Date__c> NEXT_N_DAYS:9 AND Card_Next_Debit_Date__c= NEXT_N_DAYS:10 AND Is_Debit_Card_On__c= true) OR ' +
                '       ( loan__ACH_Next_Debit_Date__c > NEXT_N_DAYS:9 AND loan__ACH_Next_Debit_Date__c = NEXT_N_DAYS:10 AND loan__ACh_On__c = true) ) AND ' +
                '     Payment_Due_10_Day_Reminder_Date__c  < LAST_N_DAYS:10 AND ' +
                '     loan__Loan_Status__c like \'Active%\' AND ' +
                '     loan__Contact__r.Do_not_Contact__c = false AND ' +
                '     loan__Contact__r.HasOptedOutOfEmail = false and ' +
                '     loan__Charged_Off_Date__c = null ' ;
            
        } catch(exception ex){
            System.debug('Exception:'+ ex.getMessage());
            WebToSFDC.notifyDev('BatchPaymentReminderJob.QueryLocator ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
        }
        return Database.getQueryLocator(query);    
     }
    
     global void execute(Database.BatchableContext BC,List<sObject> scope) {
         
         try { 
             system.debug('==scope============='+scope);
             
             list<loan__Loan_Account__c> theLoans = (List<loan__Loan_Account__c>)scope;
             for (loan__Loan_Account__c loan : theLoans) {
                 loan.PaymentDue7DayReminderDate__c = Date.today();
                 count++;
             }
             
             update theLoans;
             
         } catch(exception ex){
             System.debug('Exception:'+ ex.getMessage());
             WebToSFDC.notifyDev('BatchPaymentReminderJob ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
         }
     }
    
       global void finish(Database.BatchableContext BC){
            WebToSFDC.notifyDev('BatchPaymentReminderJob Finished', 'Reminders Processed = ' + count , UserInfo.getUserId() );
    }
}