global class EmpInfoCallBackParameter{
    public String event_id{get;set;}
    public String event_type{get;set;}
    public Long event_time{get;set;}
    public EventData event_data{get;set;}
    
    public class EventData {
        public String id;
        public String verification_type;
        public String permissible_purpose;
        public String source_id;
        public String request_status;
        public String request_status_description;
        public String third_party_name;
        public Long request_fullfillment_time;
    }
}