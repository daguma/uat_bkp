public with sharing class LoanCorrespondenceCtrl {

    public loan__Loan_Account__c contract {get; set;}
    public String contactName {get; set;}
    public Boolean correspondenceForFinwise;
    public String letterTemplateId {get; set;}
    public String letterTemplateHtmlBody {get; set;}
    public String letterTemplateSubject {get; set;}
    public User currentUser {get; set;}
    public loan__loan_Disbursal_Transaction__c disbursalTransaction {get; set;}

    private Boolean tmpReturnMailValue;
    private String tmpReturnReasonValue;
    private String letterTemplateName {get; set;}
    private Map<String, String> keyValues;

    public LoanCorrespondenceCtrl(ApexPages.StandardController stdController) {
        LoadLoanAccountInformation(string.valueof(stdController.getRecord().get('Id')));
        LoadLoanMapKeyValues();
    }

    public Boolean LoadLoanAccountInformation(String contractId) {

        try {
            contract = [SELECT
                        Id
                        , Name
                        , U_S_Return_Mail__c
                        , U_S_Return_Mail_Reason__c
                        , Other_Reason_Details__c
                        , loan__Pmt_Amt_Cur__c
                        , loan__Oldest_Due_Date__c
                        , loan__Previous_Installment_Date__c
                        , loan__Next_Installment_Date__c
                        , loan__ACH_Start_Date__c
                        , loan__ACH_Debit_Amount__c
                        , loan__ACH_Next_Debit_Date__c
                        , loan__ACH_Bank_Name__c
                        , loan__ACH_Account_Number__c
                        , loan__ACH_Routing_Number__c
                        , loan__Amount_to_Current__c
                        , loan__Contact__r.Id
                        , loan__Contact__r.Name
                        , loan__Contact__r.Email
                        , loan__Contact__r.MailingStreet
                        , loan__Contact__r.MailingCity
                        , loan__Contact__r.MailingState
                        , loan__Contact__r.MailingPostalCode
                        , loan__Contact__r.Phone
                        , Opportunity__r.Finwise_Approved__c
                        , Masked_LAI__c
                        , loan__Payment_Amount__c
                        , loan__First_Installment_Date__c
                        , Due_By_Date__c
                        , loan__Last_Payment_Date__c
                        , loan__Number_of_Days_Overdue__c
                        , Today_7__c
                        , Today_8__c
                        , loan__Frequency_of_Loan_Payment__c
                        , Masked_Bank_Account_Number__c
                        , loan__OT_ACH_Debit_Date__c
                        , loan__Total_Amount_Paid__c	
                        FROM loan__Loan_Account__c
                        WHERE Id = : contractId LIMIT 1];

            contactName = contract.Loan__Contact__r == null ? '' : contract.Loan__Contact__r.Name;

            tmpReturnMailValue = contract.U_S_Return_Mail__c;
            tmpReturnReasonValue = contract.U_S_Return_Mail_Reason__c;

            correspondenceForFinwise = contract.Opportunity__r.Finwise_Approved__c;

            loadUserInfo();
            disbursalInfo();

        } catch (Exception e) {
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Some exception has occured'));
        }

        return true;
    }

    public void LoadLoanMapKeyValues() {

        keyValues = new Map<String, String>();

        if (contract == null || contract.loan__Contact__r == null) return;

        DateTime d = Date.Today() + 1;
        keyValues.put('#CURRENT_DATE#', d.format('MMMMM dd, yyyy'));
        keyValues.put('#CONTRACT_NAME#', contract.loan__Contact__r.Name != null ? contract.loan__Contact__r.Name : '-');
        keyValues.put('#CONTRACT_EMAIL#', contract.loan__Contact__r.Email != null ? contract.loan__Contact__r.Email : '-');
        keyValues.put('#CONTRACT_ACCOUNT#', contract.Name != null ? contract.Name : '-');
        keyValues.put('#CONTRACT_TOTAL_AMOUNT_PAID#', contract.loan__Total_Amount_Paid__c != null ? formatMoney(String.valueOf(contract.loan__Total_Amount_Paid__c.format())) : '-');
        keyValues.put('#CONTRACT_AMOUNT#', contract.loan__ACH_Debit_Amount__c != null ? formatMoney(String.valueOf(contract.loan__ACH_Debit_Amount__c.format())) : '-');
        keyValues.put('#CONTRACT_BANK_NAME#', contract.loan__ACH_Bank_Name__c != null ? contract.loan__ACH_Bank_Name__c : '-');
        keyValues.put('#CONTRACT_ACCOUNT_NUMBERS#', contract.loan__ACH_Account_Number__c != null ? contract.loan__ACH_Account_Number__c : '-');
        keyValues.put('#CONTACT_PHONE#', contract.loan__Contact__r.Phone != null ? contract.loan__Contact__r.Phone : '-');
        keyValues.put('#CONTACT_STREET#', contract.loan__Contact__r.MailingStreet != null ? contract.loan__Contact__r.MailingStreet : '-');
        keyValues.put('#CONTACT_CITY#', contract.loan__Contact__r.MailingCity != null ? contract.loan__Contact__r.MailingCity : '-');
        keyValues.put('#CONTACT_STATE#', contract.loan__Contact__r.MailingState != null ? contract.loan__Contact__r.MailingState : '-');
        keyValues.put('#CONTACT_POSTALCODE#', contract.loan__Contact__r.MailingPostalCode != null ? contract.loan__Contact__r.MailingPostalCode : '-');
        keyValues.put('#CONTRACT_NOW_DUE#', contract.loan__Amount_to_Current__c != null ? formatMoney(String.valueOf(contract.loan__Amount_to_Current__c.format())) : '-');
        keyValues.put('#MASKED_LAI#', contract.Masked_LAI__c != null ? String.valueOf(contract.Masked_LAI__c) : '-');
        keyValues.put('#FUNDED_DATE#', disbursalTransaction.loan__Disbursal_Date__c != null ? String.valueOf(disbursalTransaction.loan__Disbursal_Date__c.format()) : '-');
        keyValues.put('#LOAN_AMOUNT#', disbursalTransaction.loan__Disbursed_Amt__c != null ? formatMoney(String.valueOf(disbursalTransaction.loan__Disbursed_Amt__c.format())) : '-');
        keyValues.put('#DUE_DATE#', contract.loan__Next_Installment_Date__c != null ? String.valueOf(contract.loan__Next_Installment_Date__c.format()) : '-');
        keyValues.put('#PAYMENT_AMOUNT#', contract.loan__Payment_Amount__c != null ? formatMoney(String.valueOf(contract.loan__Payment_Amount__c.format())) : '-');
        keyValues.put('#CURRENT_USER_PHONE#', currentUser.phone != null ? String.valueOf(currentUser.phone) : '-');
        keyValues.put('#CURRENT_USER_EMAIL#', currentUser.email != null ? String.valueOf(currentUser.email) : '-');
        keyValues.put('#FIRST_DUE_DATE#', contract.loan__First_Installment_Date__c != null ? String.valueOf(contract.loan__First_Installment_Date__c.format()) : '-');
        keyValues.put('#AMOUNT_DUE#', contract.loan__Amount_to_Current__c != null ? formatMoney(String.valueOf(contract.loan__Amount_to_Current__c.format())) : '-');
        keyValues.put('#DUE_BY_DATE#', contract.Due_By_Date__c != null ? String.valueOf(contract.Due_By_Date__c.format()) : '-');
        keyValues.put('#PREV_DUE_DATE#', contract.loan__Last_Payment_Date__c != null ? String.valueOf(contract.loan__Last_Payment_Date__c.format()) : '-');
        keyValues.put('#DPD#', contract.loan__Number_of_Days_Overdue__c != null ? String.valueOf(contract.loan__Number_of_Days_Overdue__c) : '-');
        keyValues.put('#TODAY_7#', contract.Today_7__c != null ? String.valueOf(contract.Today_7__c.format()) : '-');
        keyValues.put('#TODAY_8#', contract.Today_8__c != null ? String.valueOf(contract.Today_8__c.format()) : '-');
        keyValues.put('#CURRENT_USER_NAME#', currentUser.Name != null ? String.valueOf(currentUser.Name) : '-');
        keyValues.put('#CURRENT_USER_TITLE#', currentUser.Title != null ? String.valueOf(currentUser.Title) : '-');
        keyValues.put('#PAYMENT_FREQUENCY#', contract.loan__Frequency_of_Loan_Payment__c != null ? String.valueOf(contract.loan__Frequency_of_Loan_Payment__c) : '-');
        keyValues.put('#MASKED_BANK_ACCOUNT_NUMBER#', contract.Masked_Bank_Account_Number__c != null ? String.valueOf(contract.Masked_Bank_Account_Number__c) : '-');
        keyValues.put('#OT_ACH_DEBIT_DATE#', contract.loan__OT_ACH_Debit_Date__c != null ? String.valueOf(contract.loan__OT_ACH_Debit_Date__c.format()) : '-');
        keyValues.put('#ACH_DEBIT_DATE#', contract.loan__ACH_Next_Debit_Date__c != null ? String.valueOf(contract.loan__ACH_Next_Debit_Date__c.format()) : '-');
    }

    public String formatMoney(String s) {
        s = '$' + s;
        if (!s.contains('.')) {
            s = s + '.00';
        } else {
            Integer dPos = s.indexOf('.');
            if (s.length() - dPos < 3) { s = s + '0'; }
        }
        return s;
    }

    public PageReference goBack() {
        return new PageReference(URL.getSalesforceBaseUrl().toExternalForm().replace('c.', 'loan.') + '/apex/tabbedLoanAccount?id=' + this.contract.Id);
    }

    public void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }

    public void saveUSReturnMail() {
        update contract;

        if ( (tmpReturnMailValue != contract.U_S_Return_Mail__c && contract.U_S_Return_Mail__c == true) ||
                (tmpReturnMailValue == contract.U_S_Return_Mail__c && contract.U_S_Return_Mail__c == true && tmpReturnReasonValue != contract.U_S_Return_Mail_Reason__c) ) {
            String mailingStreet = contract.Loan__Contact__r == null ? '' : contract.Loan__Contact__r.MailingStreet;
            String mailingCity  = contract.Loan__Contact__r == null ? '' : contract.Loan__Contact__r.MailingCity;
            String mailingState  = contract.Loan__Contact__r == null ? '' : contract.Loan__Contact__r.MailingState;
            String mailingPostalCode  = contract.Loan__Contact__r == null ? '' : contract.Loan__Contact__r.MailingPostalCode;
            String reason = contract.U_S_Return_Mail_Reason__c != 'Other' ? contract.U_S_Return_Mail_Reason__c : contract.Other_Reason_Details__c;

            insertNoteForMail(contract.Id, contactName, mailingStreet, mailingCity, mailingState, mailingPostalCode, reason);
        }
    }

    private void insertNoteForMail(Id parentId, String contactName, String mailingStreet, String mailingCity, String mailingState, String mailingPostalCode, String reason) {
        Note newNote = new Note();
        newNote.parentid = parentId;
        newNote.title = 'Contract ' + contract.Name + ' Shows Returned U.S. Mail';
        newNote.body = 'U.S. Mail has been returned for ' + contactName + ' with this mailing address: \n\n' + contactName.toUpperCase() + '\n' + mailingStreet + '\n' + mailingCity + ', ' + mailingState + ' ' + mailingPostalCode + '\n\nReason: ' + reason + '\n';

        insert newNote;

        String htmlBody = 'U.S. Mail has been returned for ' + contactName + ' with this mailing address: <br><br>' + contactName.toUpperCase() + '<br>' + mailingStreet + '<br>' + mailingCity + ', ' + mailingState + ' ' + mailingPostalCode + '<br><br>Reason: ' + reason + '<br><br>';

        SalesForceUtil.EmailByUserId(newNote.title, htmlBody, getUserEmails());
    }

    private void notifyFinalUserByEmail(String subject, String htmlBody) {

        SalesForceUtil.EmailByUserId(subject, htmlBody, getUserEmails());
    }

    private String getUserEmails() {

        String userType = Schema.SObjectType.User.getKeyPrefix();
        Set<String> userIds = new Set<String>();

        for (GroupMember m : [SELECT
                              UserOrGroupId
                              FROM GroupMember
                              WHERE GroupId IN (SELECT
                                                Id
                                                FROM Group
                                                WHERE Name like 'US Return Notification')]) {
            if (((String)m.UserOrGroupId).startsWith(userType))
                userIds.add(m.UserOrGroupId);
        }

        List<String> UserIdslList = new List<String>();

        for (User u : [SELECT Id,
                       Email
                       FROM User
                       WHERE Id IN :userIds]) {

            UserIdslList.add(u.Id);
        }
        String IdslList = String.join(UserIdslList, ',');
        return IdslList;
    }

    public List<SelectOption> getLettersTemplates() {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('', '-- Please, select one!  --'));

        if (correspondenceForFinwise)
            for (EmailTemplate template : [SELECT Id, Name, DeveloperName FROM EmailTemplate WHERE FolderId = : String.valueof(Label.Finwise_Collections_Demand_Letters)]) {
                if (!template.DeveloperName.contains('Forms'))
                    options.add(new SelectOption(template.Id, template.Name));
            }
        else
            for (EmailTemplate template : [SELECT Id, Name, DeveloperName FROM EmailTemplate WHERE FolderId = : String.valueof(Label.Member_Services_Correspondence_l_LP)]) {
                options.add(new SelectOption(template.Id, template.Name));
            }
        return options;
    }

    private void LoadKeyTemplateValues() {
        letterTemplateName = '';
        letterTemplateHtmlBody = '';

        if (letterTemplateId == null)
            return;

        for (EmailTemplate template : [SELECT Id, Name, HTMLValue, DeveloperName, Subject
                                       FROM EmailTemplate WHERE Id = :letterTemplateId]) {
            letterTemplateName = template.Name;
            letterTemplateHtmlBody = template.HTMLValue;
            letterTemplateSubject = template.Subject;
        }
        for (String key : keyValues.keySet()) {
            letterTemplateHtmlBody = letterTemplateHtmlBody.replace(key, keyValues.get(key));
        }
    }

    public void onChangeLettersTemplatesOptions() {
        LoadKeyTemplateValues();
    }

    public PageReference sendEmailTemplateAndInsertNote() {
        //Create MSNoteEmail
        Member_Services_Notes__c MSNote = New Member_Services_Notes__c ();
        MSNote.Title__c = this.letterTemplateName;
        MSNote.Body__c = this.letterTemplateHtmlBody.replaceAll('<[^>]*>|&[^;]*;', '');
        MSNote.CL_Contract__c = contract.id;
        MSNote.Interaction_Codes__c = 'Email';
        Insert MSNote;

        //Send mail
        SalesForceUtil.EmailByUserId(this.letterTemplateSubject, this.letterTemplateHtmlBody, this.contract.loan__Contact__r.Id);

        PageReference pg = new PageReference('/apex/Loan_Correspondence?id=' + this.contract.Id);
        pg.setredirect(true);

        return pg;
    }

    public PageReference memberServicesNotePrint () {
        //Create MSNoteLetter
        Member_Services_Notes__c MSNote = New Member_Services_Notes__c ();
        MSNote.Title__c = this.letterTemplateName + '- Correspondence';
        MSNote.Body__c = this.letterTemplateHtmlBody.replaceAll('<[^>]*>|&[^;]*;', '');
        MSNote.CL_Contract__c = contract.id;
        MSNote.Interaction_Codes__c = 'Correspondence';
        Insert MSNote;

        PageReference pg = new PageReference('/apex/Loan_Correspondence?id=' + this.contract.Id);
        pg.setredirect(true);

        return pg;
    }

    public void loadUserInfo() {
        currentUser = [SELECT id
                       , phone
                       , Email
                       , Name
                       , Title
                       FROM User
                       WHERE id = : UserInfo.getUSerId()  ];
    }

    public void disbursalInfo() {
        disbursalTransaction = [SELECT id
                                , loan__Disbursal_Date__c
                                , loan__Disbursed_Amt__c
                                FROM loan__loan_Disbursal_Transaction__c
                                WHERE loan__Loan_Account__c = : contract.id];
    }
}