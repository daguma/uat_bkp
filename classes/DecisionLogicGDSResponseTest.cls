@isTest 
private class DecisionLogicGDSResponseTest {
	
    @isTest static void initialize_gds_response(){
        DecisionLogicGDSResponse gdsResponse = new DecisionLogicGDSResponse();
    }
    
    @isTest static void gets_gds_response(){
        String GDSResponseS = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        DecisionLogicGDSResponse gdsResponse = new DecisionLogicGDSResponse(GDSResponseS);
    }
    
    @isTest static void gets_dl_rule(){
        DecisionLogicGDSResponse.DecisionLogicRule gdsRuleResponse = new DecisionLogicGDSResponse.DecisionLogicRule();
        gdsRuleResponse.deRuleNumber = 'Test';
        gdsRuleResponse.deRuleName = 'Test_Rule';
        gdsRuleResponse.rulePriority = 'Test';
        gdsRuleResponse.deRuleValue = 'Test';
        gdsRuleResponse.deQueueAssignment = 'Test';
        gdsRuleResponse.deRuleStatus = 'Test';
        gdsRuleResponse.deBRMS_ReasonCode = 'Test';
        gdsRuleResponse.deIsDisplayed = 'Test';
        gdsRuleResponse.deSequenceCategory = 'Test';
        gdsRuleResponse.deRuleCategory = 'Test';
        gdsRuleResponse.deActionInstructions = 'Test';
        gdsRuleResponse.deHumanReadableDescription = 'Test';
        gdsRuleResponse.overridden_by = 'Test';
    }
    
}