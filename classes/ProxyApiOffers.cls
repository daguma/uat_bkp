public class ProxyApiOffers extends ProxyApiUtil {

  private static final String GET_OFFERS_URL = '/v101/offer/get-offers/';
  private static final String REFINANCE = 'Refinance';
  private static final String CUSTOMOFFER = 'customOffer';
  private static final String RECALCULATE_PAYMENT = 'recalculateForPayment';
  private static final String RECALCULATE_PRINCIPAL = 'recalculateForPrincipal';

  public static List<ProxyApiOfferCatalog> getOffers(String grade,
      String annualIncome,
      String loanAmount,
      String state,
      String isPartner,
      String frequency,
      String fico,
      Long creditReportId,
      String analyticRandomNumber,
      String feeHandling,
      String partner,
      Boolean isFinwise,
      String productCode,
      String applicationType,
      String previousGrade,
      Decimal previousTerm,
      Decimal fundedAPR,
      Decimal currentPayoffBalance,
      String strategyType,
      String appId,
      offerInfo offer,
      String isMrcState,
      String merchantState,
      String bankName,
      String leadSource,
      Boolean isFoc,
      String promotionTypes,
      String oppCreatedDate,
      String salesCategory,
      Boolean aonEnrollment,
      Boolean threeMonths) {

    List<ProxyApiOfferCatalog> result = null;
    Boolean validatedAPI = false,
            isBrms = !CreditReport.validateBrmsFlag(appId),
            isRefinance = applicationType.equalsIgnoreCase(REFINANCE);

    if (isRefinance || isBrms)
      validatedAPI = ProxyApiBRMSUtil.ValidateBeforeCallApi();
    else
      validatedAPI = ProxyApiUtil.ValidateBeforeCallApi();

    System.debug('validatedAPI: ' + validatedAPI);
    if (validatedAPI) {
      String body = 'grade=' + grade +
                    '&annual-income=' + annualIncome +
                    '&loan-amount=' + loanAmount +
                    '&state=' + state +
                    '&is-partner=' + isPartner +
                    '&frequency=' + frequency +
                    '&fico=' + fico +
                    '&credit-report-id=' + creditReportId +
                    '&analytic-random-number=' + analyticRandomNumber +
                    '&fee-handling=' + feeHandling +
                    '&partner=' + partner +
                    '&is-finwise=' + isFinwise +
                    '&product-code=' + productCode +
                    '&application-type=' + applicationType +
                    '&previous-grade=' + previousGrade +
                    '&previous-term=' + previousTerm +
                    '&funded-apr=' + fundedAPR +
                    '&current-payoff=' + currentPayoffBalance +
                    '&strategy-type=' + strategyType +
                    '&is-merchant-state=' + isMrcState +
                    '&merchant-state=' + merchantState +
                    '&bank-name=' + bankName +
                    '&lead-source=' + leadSource;

      //Start - SM-362 | Modify Fee/Pricing Validations Process
      if (offer <> null && string.isNotEmpty(offer.requestedType)) {
        if (offer.requestedType == CUSTOMOFFER) {
          body += '&requested-amount=' + offer.requestedLoanAmount +
                  '&requested-term=' + offer.requestedTerm +
                  '&requested-apr=' + offer.requestedApr +
                  '&requested-fee=' + offer.requestedFee +
                  '&requested-type=' + offer.requestedType;
        } else if (offer.requestedType == RECALCULATE_PAYMENT) {
          body += '&requested-amount=' + offer.requestedLoanAmount +
                  '&requested-fee=' + offer.requestedFee +
                  '&requested-type=' + offer.requestedType;
        } else if (offer.requestedType == RECALCULATE_PRINCIPAL)   {
          body += '&requested-amount=' + offer.requestedLoanAmount +
                  '&requested-fee=' + offer.requestedFee +
                  '&requested-type=' + offer.requestedType;
        }
      }

      body += '&is-foc=' + isFoc;
      body += '&promotion-types=' + promotionTypes;
      body += '&opp-created-date=' + oppCreatedDate;
      body += '&sales-category=' + salesCategory;
      body += '&aon-enrollment=' + aonEnrollment;
      body += '&three-months-extension=' + threeMonths;

      system.debug('----strbody ----' + body);

      //End - SM-362 | Modify Fee/Pricing Validations Process
      String jsonResponse = null;

      if (Test.isRunningTest()) {
        jsonResponse = LibraryTest.fakeOffersData();
      } else {
        if (isRefinance || isBrms)
          jsonResponse = ProxyApiBRMSUtil.ApiGetResult(GET_OFFERS_URL, body);
        else
          jsonResponse = ProxyApiUtil.ApiGetResult(GET_OFFERS_URL, body);
      }

      system.debug('Json Response' + jsonResponse);

      if (jsonResponse != null) {
        JSONParser parser = JSON.createParser(jsonResponse);

        result = (List<ProxyApiOfferCatalog>)parser.readValueAs(List<ProxyApiOfferCatalog>.class);
      }
    }
    return result;
  } 

  //SM-362
  public class offerInfo {
    public Decimal requestedLoanAmount, requestedApr, requestedFee, requestedTerm;
    public string requestedType;
  }
}