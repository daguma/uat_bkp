global class ProxyApiCreditReportRuleEntity {
    @AuraEnabled public String deRuleNumber {get; set;}
    @AuraEnabled public String deRuleName {get; set;}
    @AuraEnabled public String rulePriority {get; set;}
    @AuraEnabled public String deRuleValue {get; set;}
    @AuraEnabled public String approvedBy {get; set;}
    @AuraEnabled public String deQueueAssignment {get; set;}
    @AuraEnabled public String deRuleStatus {get;set;}
    @AuraEnabled public String deBRMS_ReasonCode{get;set;}
    @AuraEnabled public String deIsDisplayed {get; set;}
    @AuraEnabled public String deSequenceCategory {get; set;}
    @AuraEnabled public String deRuleCategory {get; set;}
    @AuraEnabled public String deActionInstructions {get; set;}
    @AuraEnabled public String deHumanReadableDescription {get;set;}
}