global class EmailAgeStorageController {
    
    Webservice static void saveEmailAgeDetails(String jsonResult) {
        EmailAgeResponseEntity emailAgeResEntity;
        JSONParser parser = JSON.createParser(jsonResult);
        while(parser.nextToken() != null) {
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'response') {
                parser.nextToken();
                emailAgeResEntity = (EmailAgeResponseEntity)parser.readValueAs(EmailAgeResponseEntity.class);
            }
        }
        
        Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(jsonResult);
        
        if(root != null) {
            String Request = (String) root.get('request');
            String Response = JSON.serialize((Object) root.get('response'));
            String appId = (String) root.get('appId');
            Boolean decision;
            if(root.get('creditDecision') != null && root.get('creditDecision') != '')
                decision = (Boolean) root.get('creditDecision');
            String creditDecision = String.valueOf(decision);

            if(appId != null) { 
                for (Opportunity  apps : [ SELECT  Id,
                                                    Contact__c,
                                                    Contact__r.Id,
                                                    LastModifiedDate,
                                                    Bankruptcy_Found__c 
                                                    FROM    Opportunity 
                                                    WHERE   Id =: appId 
                                                    ORDER BY CreatedDate DESC 
                                           LIMIT 1]) {
                    if(emailAgeResEntity != null)
                        storeEmailAgeInSfObject(emailAgeResEntity, apps.Contact__c, apps.Id, false);

                    MelisaCtrl.DoLogging('Email Age', Request, Response, null, apps.Contact__c);
                                           }
            }
        }
    }
    
    public static List<Email_Age_Details__c> getEmailAgeRecord(String entityId) {
        return getEmailAgeRecord(entityId, null);
    }
    
    //method to return email age records
    public static List<Email_Age_Details__c> getEmailAgeRecord(String entityId, String pEmail) {
        List<Email_Age_Details__c> emailAgeDetails = new List<Email_Age_Details__c>();
        Contact co ;
        Id recordId = entityId;
        if (recordId != null) {
            String objType = recordId.getSObjectType().getDescribe().getName();
            if (objType.equalsIgnoreCase('Opportunity')) {
                co = [Select Id, Email from Contact where Id in (SELECT  Contact__c 
                                                            FROM    Opportunity 
                                                            WHERE   Id =: entityId) ];
            } 
            else if (objType.equalsIgnoreCase('Contact')) {
                co = [Select Id, Email from Contact where Id =: recordId ];
            } 
            if (co != null) {
                String em = String.isEmpty(pEmail) ? co.Email : pEmail;
                if (!String.isEmpty(em)) {
                    emailAgeDetails = [ SELECT  Id,
                                                Contact__c,
                                                Actual_Credit_Decision__c,
                                                Email_Age_Status__c,
                                                Is_Email_Age_Timed_Out__c,
                                                ErrorCode__c,
                                                Status__c 
                                        FROM    Email_Age_Details__c 
                                        WHERE   Contact__c =: co.Id and
                                                Email__C =: encodeQueryParams(em)
                                        ORDER BY CreatedDate DESC 
                                        LIMIT 1];
                                    }
                }
        }

        return emailAgeDetails;
    }
    
    public static string encodeQueryParams(string targetString) {
        return EncodingUtil.urlDecode(targetString,'UTF-8');
    }
    
    //method to store the parsed response in SFDC
    public static void storeEmailAgeInSfObject(EmailAgeResponseEntity emailAgeResEntity, String contactId, String appId, Boolean isManualRun) {
        try {
            Map<Integer, Email_Age_Results__c> resultsMap = new Map<Integer, Email_Age_Results__c>();
            system.debug('---IN email age saving---'+emailAgeResEntity+'---contact---'+contactId+'---appId--'+appId);
            Email_Age_Details__c emailAge = new Email_Age_Details__c();
            emailAge.Contact__c = contactId;
            emailAge.is_Email_Age_Timed_Out__c = false;
            
            if(emailAgeResEntity.query != null) {
                EmailAgeResponseQuery queryRes = emailAgeResEntity.query;
                emailAge.Count__c = queryRes.count;
                emailAge.Created__c = queryRes.created;
                
                String queryParams = '';
                if(queryRes.email != null) {
                    queryParams = encodeQueryParams(queryRes.email);
                    queryParams = queryParams.replace('+', ' ');
                    List<String> queryParamsExtractionStr = queryParams.split(' ');
                    queryParams = queryParamsExtractionStr[0];
                }
                
                emailAge.Email__c = queryParams;
                emailAge.lang__c = queryRes.lang;
                emailAge.queryType__c = queryRes.queryType;
                emailAge.responseCount__c = queryRes.responseCount;
                emailAge.Email_Age_Status__c = false;
                emailAge.errorCode__c = '';
                emailAge.status__c = '';
                
                if(isManualRun != null)
                    emailAge.is_Manual_Run__c = isManualRun;
                
                if(queryRes.results != null) {
                    Integer mapKey = 0;
                    for(EmailAgeResponseResults resResult : queryRes.results) {
                        mapKey++;
                        Email_Age_Results__c results = new Email_Age_Results__c();
                        String queryEmail = (resResult.email != null) ? encodeQueryParams(resResult.email) : '';

                        results.email__c = queryEmail;
                        results.ipaddress__c = resResult.ipaddress;
                        results.eName__c = resResult.eName;
                        results.emailAge__c = resResult.emailAge;
                        results.domainAge__c = resResult.domainAge;
                        results.firstVerificationDate__c = resResult.firstVerificationDate;
                        results.lastVerificationDate__c = resResult.lastVerificationDate;
                        results.status__c = resResult.status;
                        results.country__c = resResult.country;
                        results.fraudRisk__c = resResult.fraudRisk;
                        results.EAScore__c = resResult.EAScore;
                        results.EAReason__c = resResult.EAReason;
                        results.EAStatusID__c = resResult.EAStatusID;
                        results.EAReasonID__c = resResult.EAReasonID;
                        results.EAAdviceID__c = resResult.EAAdviceID;
                        results.EAAdvice__c = resResult.EAAdvice;
                        results.EARiskBandID__c = resResult.EARiskBandID;
                        
                        if(String.isNotEmpty(results.EARiskBandID__c) && integer.valueof(results.EARiskBandID__c) < 4)
                            emailAge.Email_Age_Status__c = true;

                        results.EARiskBand__c = resResult.EARiskBand;
                        results.source_industry__c = resResult.source_industry;
                        results.fraud_type__c = resResult.fraud_type;
                        results.dob__c = resResult.dob;
                        results.gender__c = resResult.gender;
                        results.location__c = resResult.location;
                        results.smfriends__c = resResult.smfriends;
                        results.totalhits__c = resResult.totalhits;
                        results.uniquehits__c = resResult.uniquehits;
                        results.imageurl__c = resResult.imageurl;
                        results.emailExists__c = resResult.emailExists;
                        results.domainExists__c = resResult.domainExists;
                        results.company__c = resResult.company;
                        results.title__c = resResult.title;
                        results.domainname__c = resResult.domainname;
                        results.domaincompany__c = resResult.domaincompany;
                        results.domaincountryname__c = resResult.domaincountryname;
                        results.domaincategory__c = resResult.domaincategory;
                        results.domaincorporate__c = resResult.domaincorporate;
                        results.domainrisklevel__c = resResult.domainrisklevel;
                        results.domainrelevantinfo__c = resResult.domainrelevantinfo;
                        results.domainrisklevelID__c = resResult.domainrisklevelID;
                        results.domainrelevantinfoID__c = resResult.domainrelevantinfoID;
                        results.ip_risklevelid__c = resResult.ip_risklevelid;
                        results.ip_risklevel__c = resResult.ip_risklevel;
                        results.ip_riskreasonid__c = resResult.ip_riskreasonid;
                        results.ip_riskreason__c = resResult.ip_riskreason;
                        results.ip_reputation__c = resResult.ip_reputation;
                        results.ip_anonymousdetected__c = resResult.ip_anonymousdetected;
                        results.ip_isp__c = resResult.ip_isp;
                        results.ip_org__c = resResult.ip_org;
                        results.ip_netSpeedCell__c = resResult.ip_netSpeedCell;
                        results.ip_corporateProxy__c = resResult.ip_corporateProxy;
                        results.ip_continentCode__c = resResult.ip_continentCode;
                        results.ip_country__c = resResult.ip_country;
                        results.ip_countryCode__c = resResult.ip_countryCode;
                        results.ip_region__c = resResult.ip_region;
                        results.ip_city__c = resResult.ip_city;
                        results.ip_callingcode__c = resResult.ip_callingcode;
                        results.ip_metroCode__c = resResult.ip_metroCode;
                        results.ip_latitude__c = resResult.ip_latitude;
                        results.ip_longitude__c = resResult.ip_longitude;
                        results.ip_map__c = resResult.ip_map;
                        results.ipcountrymatch__c = resResult.ipcountrymatch;
                        results.ipriskcountry__c = resResult.ipriskcountry;
                        results.ipdistancekm__c = resResult.ipdistancekm;
                        results.ipdistancemil__c = resResult.ipdistancemil;
                        results.ipaccuracyradius__c = resResult.ipaccuracyradius;
                        results.iptimezone__c = resResult.iptimezone;
                        results.ipasnum__c = resResult.ipasnum;
                        results.ipdomain__c = resResult.ipdomain;
                        results.ip_countryconf__c = resResult.ip_countryconf;
                        results.ip_regionconf__c = resResult.ip_regionconf;
                        results.ip_cityconf__c = resResult.ip_cityconf;
                        results.ip_postalcode__c = resResult.ip_postalcode;
                        results.ip_postalconf__c = resResult.ip_postalconf;
                        results.ip_riskscore__c = resResult.ip_riskscore;
                        results.custphoneInbillingloc__c = resResult.custphoneInbillingloc;
                        results.shipforward__c = resResult.shipforward;
                        results.citypostalmatch__c = resResult.citypostalmatch;
                        results.shipcitypostalmatch__c = resResult.shipcitypostalmatch;
                        
                        if(resResult.smlinks != null)
                            results.smlinks__c = string.valueof(resResult.smlinks);

                        resultsMap.put(mapKey,results);
                    }
                }
            }
            
            if(emailAgeResEntity.responseStatus != null) {
                EmailAgeResponseStatus resStatus = emailAgeResEntity.responseStatus;
                emailAge.status__c = resStatus.status;
                emailAge.description__c = resStatus.description;
                emailAge.errorCode__c = resStatus.errorCode;
            }
            
            upsert emailAge;
            
            for(Integer results : resultsMap.keySet()) {
                Email_Age_Results__c result = resultsMap.get(results);
                result.Email_Age_Details__c = emailAge.Id;
                resultsMap.put(results,result);
            }
            
            if(resultsMap != null) 
                upsert resultsMap.values();
        }
        catch (exception e) {
            system.debug('EmailAgeStorageController storeEmailAgeInSfObject: ' + e.getMessage());
        }
    }
    
    Webservice static String emailAgeCallStatusCode(String entityId) {
        return emailAgeCallStatusCode(entityId, null);
    }
    
    public static String emailAgeCallStatusCode(String entityId, String pEmail) {
        String status = '';
         
        try {
            Id recordId = entityId;
            if (recordId != null) { 
                String objType = recordId.getSObjectType().getDescribe().getName();
                 
                if (objType.equalsIgnoreCase('Opportunity'))
                    for (Opportunity app : [SELECT Id,
                                                                Contact__c 
                                                        FROM    Opportunity 
                                                        WHERE   id =: entityId 
                                                        LIMIT 1]) {
                        status = getStatusCode(app.Contact__c, pEmail);
                    } 
                else if(objType.equalsIgnoreCase('Contact'))
                    status = getStatusCode(entityId);
            }
        }
        catch(exception e) {
             system.debug('EmailAgeStorageController emailAgeCallStatusCode: ' + e.getMessage());
        }
        
        return status;
     }
     
    public static string getStatusCode(String recordId) {
        return getStatusCode(recordId, null);
    }
    
    public static string getStatusCode(String recordId, String pEmail) {
        String status = '';
        
        try {
            Contact c = [select Id, Email from Contact where Id =: recordId];
            String em = String.IsEmpty(pEmail) ? c.Email : pEmail;
            Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
             
            Date currentDate = Date.newInstance(datetime.now().year(), datetime.now().month(), datetime.now().day());
             
            if(emailAgeSettings.Call_Email_age__c && !String.isEmpty(em)) {
            
                status = '0';  
                for (Email_Age_Details__c  emailAgeDetails : [  SELECT  Id,
                                                                        Contact__c,
                                                                        CreatedDate,
                                                                        Email_Age_Status__c,
                                                                        Is_Email_Age_Timed_Out__c, 
                                                                        (SELECT Id,
                                                                                EARiskBandID__c 
                                                                        FROM    Email_Age_Results__r) 
                                                                FROM    Email_Age_Details__c 
                                                                WHERE   Contact__c =: c.Id and 
                                                                        email__c =: encodeQueryParams(em)
                                                                ORDER BY CreatedDate DESC 
                                                                LIMIT 1]) {

                    DateTime emailAgeCreatedDate = emailAgeDetails.CreatedDate;
                    Date sencDate = Date.newInstance(emailAgeCreatedDate.year(), emailAgeCreatedDate.month(), emailAgeCreatedDate.day());
                    status = '4';
                    if(emailAgeDetails.Email_Age_Results__r.size () > 0) {
                        if(sencDate.daysBetween(currentDate) >= emailAgeSettings.Expiry_Days__c )
                            status = '1';
                        else
                            status = (emailAgeDetails.Email_Age_Status__c) ? '3' : '2';
                    }

                }
            }
        } 
        catch(exception e) {
             system.debug('EmailAgeStorageController getStatusCode: ' + e.getMessage());
        }
         
        return status;
    }
    
}