global class CollectionsCasesAssignmentAudit {

    List<Id> auditUsersIdsList, auditDebugUsersIdsList;
    public List<String> failedAnalyzedContracts, failedAssignedContracts, failedAssignedCCAs, failedAssignedCases;
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();

    public List<String> myStringsList = new List<String>();

    public CollectionsCasesAssignmentAudit() {
        this.auditUsersIdsList = new List<String> ();
        for( GroupMember gm : [SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: Label.COLLECTIONS_CASES_ASSIGNMENT_AUDIT]){
            auditUsersIdsList.add(gm.UserOrGroupId);
        }
        this.auditDebugUsersIdsList = new List<String> ();
        for( GroupMember gm : [SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: Label.COLLECTIONS_CASES_ASSIGNMENT_AUDIT_DEBUG]){
            auditDebugUsersIdsList.add(gm.UserOrGroupId);
        }

        initFailedLists();
    }

    public void initFailedLists(){
        this.failedAnalyzedContracts = new List<String>();
        this.failedAssignedContracts = new List<String>();
        this.failedAssignedCCAs = new List<String>();
        this.failedAssignedCases = new List<String>();
    }

    private void sendEmail(String subject, String body, String userID, Boolean isHtml, List<Messaging.EmailFileAttachment> attachmentList){
        if(!Test.isRunningTest()) {
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            email.setSubject(subject);
            email.setTargetObjectId(userID);
            if(attachmentList != null && !attachmentList.isEmpty()){
                email.setFileAttachments(attachmentList);
            }
            if(isHtml){
                email.setHtmlBody(body);
            }else{
                email.setPlainTextBody(body);
            }
            email.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
    }

    public void emailHtml(String subject, String body, List<Messaging.EmailFileAttachment> attachmentList ,List<Id> userIdsList){
        for(Id auditUserId : userIdsList){
            sendEmail(subject, body, auditUserId, true, attachmentList);
        }
    }

    public void emailHtml(String subject, String body, List<Id> userIdsList){
        for(Id auditUserId : userIdsList){
            sendEmail(subject, body, auditUserId, true, null);
        }        
    }

    public void emailAuditors(String subject, String body){
        for(Id auditUserId : this.auditUsersIdsList){
            sendEmail(subject, body, auditUserId, false,null);
        }        
    }

    public void emailDebugToAuditors(String subject, String body){
        if(cuSettings.Collections_Cases_Assignment_Debug__c) {
            for(Id auditUserId : this.auditDebugUsersIdsList){
                sendEmail(subject, body, auditUserId, false,null);
            }
        }
    }

    public void auditAnalyzeJob(Database.SaveResult[] saveResultsList){
        CollectionsCasesAssignmentAudit.AuditDmlActionResult auditResult = auditDMLAction(saveResultsList);
        if(auditResult.hasErrors){
            for(String statusCode : auditResult.errorsMap.keySet()){ 
                if('UNABLE_TO_LOCK_ROW'.equals(statusCode)){
                    this.failedAnalyzedContracts.addAll(getIdsFromErrorMessage(auditResult.errorsMap.get('UNABLE_TO_LOCK_ROW')));
                }
            }
        }
    }

    public void auditAssignContractJob(Database.SaveResult[] saveResultsList){
        CollectionsCasesAssignmentAudit.AuditDmlActionResult auditResult = auditDMLAction(saveResultsList);
        if(auditResult.hasErrors){
            for(String statusCode : auditResult.errorsMap.keySet()){ 
                if('UNABLE_TO_LOCK_ROW'.equals(statusCode)){
                    this.failedAssignedContracts.addAll(getIdsFromErrorMessage(auditResult.errorsMap.get('UNABLE_TO_LOCK_ROW')));
                }
            }
        }
    }

    public void auditAssignCCAJob(Database.SaveResult[] saveResultsList){
        CollectionsCasesAssignmentAudit.AuditDmlActionResult auditResult = auditDMLAction(saveResultsList);
        if(auditResult.hasErrors){
            for(String statusCode : auditResult.errorsMap.keySet()){ 
                if('UNABLE_TO_LOCK_ROW'.equals(statusCode)){
                    this.failedAssignedCCAs.addAll(getIdsFromErrorMessage(auditResult.errorsMap.get('UNABLE_TO_LOCK_ROW')));
                }
            }
        }
    }

    public void auditAssignCaseJob(Database.UpsertResult[] upsertTheseCasesResult){
        CollectionsCasesAssignmentAudit.AuditDmlActionResult auditResult = auditDMLAction(upsertTheseCasesResult);
        if(auditResult.hasErrors){
            for(String statusCode : auditResult.errorsMap.keySet()){ 
                if('UNABLE_TO_LOCK_ROW'.equals(statusCode)){
                    this.failedAssignedCases.addAll(getIdsFromErrorMessage(auditResult.errorsMap.get('UNABLE_TO_LOCK_ROW')));
                }
            }
        }
    }

    private List<String> getIdsFromErrorMessage(String errorMessage){
        List<String> words = errorMessage.split(' ');
        return words.get(words.size()-1).split(',');
    }

    public AuditDmlActionResult auditDMLAction(List<Database.SaveResult> saveResultsList){
        AuditDmlActionResult adar = new AuditDmlActionResult(saveResultsList);
        if(!adar.unhandledErrors.isEmpty()){
            for(String unhandledError : adar.unhandledErrors){
                emailAuditors('Collections Cases Assignment: auditDMLAction: unhandled errors',unhandledError);
            }
        }
        return adar;
    }

    public AuditDmlActionResult auditDMLAction(List<Database.UpsertResult> upsertResultsList){
        AuditDmlActionResult adar = new AuditDmlActionResult(upsertResultsList);
        if(!adar.unhandledErrors.isEmpty()){
            for(String unhandledError : adar.unhandledErrors){
                emailAuditors('Collections Cases Assignment: auditDMLAction: unhandled errors',unhandledError);
            }
        }
        return adar;
    } 

    public void sendAuditReport(CollectionsCasesAssignmentUsersStructure usersStructure, CollectionsCasesAssignmentStructure assignmentStructure){

        String reportBody = 'Collections Cases Assignment Audit Report <br/><br/>';
        reportBody += processFailedList('Analyze job failed Contracts',failedAnalyzedContracts);
        reportBody += processFailedList('Assign job failed Cases',failedAssignedCases);
        reportBody += processFailedList('Assign job failed CollectionsCasesAssignments',failedAssignedCCAs);
        reportBody += processFailedList('Assign job failed Contracts',failedAssignedContracts);
        Date reportDate = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().day());
        emailHtml('Collections Cases Assignment Audit Report ' + reportDate.format(), reportBody, this.auditDebugUsersIdsList);

        //CSV
        String header = 'CL Contract: CL Contract ID,dpd,isDmc,lastPaymentReversalReason,missedFirstPayment,hasActivePaymentPlan,PaymentPlanOwnerId: Full Name,hasPendingPayment,OtAchOwnerId: Full Name,CL Contract: Product Name,caseStatus,caseOwnerId: Full Name,collectionsCaseOwnerId: Full Name,caseOwnerNewId: Full Name,loadBalanceGroup,AssignedScenario,AssignedScenarioDescription,UpdateCase,CreateCase,Assignment Result,Assigned_to: Full Name,CL Contract: Originated State,CL Contract: Mailing State \n';
        String finalstr = header ;

        for (CollectionsCasesAssignment__c cca : [
                SELECT Id, CL_Contract__r.name, dpd__c, isDmc__c, lastPaymentReversalReason__c, missedFirstPayment__c, hasActivePaymentPlan__c, PaymentPlanOwnerId__r.name,
                        hasPendingPayment__c, OtAchOwnerId__r.name, CL_Contract__r.Product_Name__c, caseStatus__c, caseOwnerId__r.name, collectionsCaseOwnerId__r.name, caseOwnerNewId__r.name,
                        loadBalanceGroup__c, AssignedScenario__c, AssignedScenarioDescription__c, UpdateCase__c, CreateCase__c, Assignment_Result__c,
                        Assigned_to__r.name, CL_Contract__r.Originated_State__c, CL_Contract__r.Contact_s_Mailing_State__c
                FROM CollectionsCasesAssignment__c
        ]) {
            string recordString = '"' + cca.CL_Contract__r.name + '","' + cca.dpd__c + '","' + cca.isDmc__c + '","' + cca.lastPaymentReversalReason__c + '","' +
                    cca.missedFirstPayment__c + '","' + cca.hasActivePaymentPlan__c + '","' + cca.PaymentPlanOwnerId__r.name + '","' + cca.hasPendingPayment__c + '","' +
                    cca.OtAchOwnerId__r.name + '","' + cca.CL_Contract__r.product_name__c + '","' + cca.caseStatus__c + '","' + cca.caseOwnerId__r.name + '","' +
                    cca.collectionsCaseOwnerId__r.name + '","' + cca.caseOwnerNewId__r.name + '","' + cca.loadBalanceGroup__c + '","' + cca.AssignedScenario__c + '","' +
                    cca.AssignedScenarioDescription__c + '","' + cca.UpdateCase__c + '","' + cca.CreateCase__c + '","' + cca.Assignment_Result__c + '","' +
                    cca.Assigned_to__r.name + '","' + cca.CL_Contract__r.Contact_s_Mailing_State__c + + '","' + cca.CL_Contract__r.Originated_State__c + '"\n';
            finalstr = finalstr + recordString;
        }

        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname=  'CollectionsCasesAssignment_' + String.valueOf(Date.today()) + '.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        List<Messaging.EmailFileAttachment> attachmentsList = new List<Messaging.EmailFileAttachment>{csvAttc};

        //UsersStructure
        Messaging.EmailFileAttachment usersStructureAttachment = new Messaging.EmailFileAttachment();
        blob usersStructureBlob = Blob.valueOf( usersStructure == null ? 'There was an error processing the data' : JSON.serialize(usersStructure));
        string usersStructurename=  'CollectionsCasesAssignment_UsersStructure' + String.valueOf(Date.today()) + '.json';
        usersStructureAttachment.setFileName(usersStructurename);
        usersStructureAttachment.setBody(usersStructureBlob);
        attachmentsList.add(usersStructureAttachment);

        //AssignmentStructure
        Messaging.EmailFileAttachment assignmentStructureAttachment = new Messaging.EmailFileAttachment();
        blob assignmentStructureBlob = Blob.valueOf( assignmentStructure == null ? 'There was an error processing the data' : JSON.serialize(assignmentStructure));
        string assignmentStructureName=  'CollectionsCasesAssignment_AssignmentStructure' + String.valueOf(Date.today()) + '.json';
        assignmentStructureAttachment.setFileName(assignmentStructureName);
        assignmentStructureAttachment.setBody(assignmentStructureBlob);
        attachmentsList.add(assignmentStructureAttachment);

        //reportBody
        Messaging.EmailFileAttachment reportBodyAttachment = new Messaging.EmailFileAttachment();
        blob reportBodyBlob = Blob.valueOf( reportBody == null ? 'There was an error processing the data' : JSON.serialize(reportBody));
        string reportBodyName=  'CollectionsCasesAssignment_ReportBody' + String.valueOf(Date.today()) + '.txt';
        reportBodyAttachment.setFileName(reportBodyName);
        reportBodyAttachment.setBody(reportBodyBlob);
        attachmentsList.add(reportBodyAttachment);

        reportBody = 'The Collections Cases Assignment process has finished running.<br/>';
        reportBody += 'Please find attached the CSV file report for today.<br/>';

        emailHtml('Collections Cases Assignment Audit Report ' + reportDate.format(), reportBody,attachmentsList,this.auditUsersIdsList);
    }

    private String processFailedList(String sectionTitle, List<String> failedList ){
        sectionTitle += '<br/>';
        Boolean isFirstRun = true;
        for(String failedRecord : failedList){
            if(isFirstRun){
                sectionTitle += '\'' + failedRecord;
                isFirstRun = false;
            }else{
                sectionTitle += '\',\'' + failedRecord;
            }
        }
        sectionTitle += isFirstRun ? '<br/><br/>' : '\'<br/><br/>';
        return sectionTitle;
    }

    public class AuditDmlActionResult{

        Boolean hasErrors = false;
        Map<String,String> errorsMap = new Map<String,String>();
        List<String> unhandledErrors = new List<String>();

        public AuditDmlActionResult(List<Database.SaveResult> saveResultsList){
            for(Database.SaveResult mySaveResult : saveResultsList){
                if(!mySaveResult.isSuccess()){
                    for(Database.Error myError : mySaveResult.getErrors()){
                        if(StatusCode.UNABLE_TO_LOCK_ROW == myError.getStatusCode()){
                            //When this error happens, none of the items are inserted/updated/upserted.
                            errorsMap.put('UNABLE_TO_LOCK_ROW',myError.getMessage());
                            hasErrors = true;
                        }else{
                            unhandledErrors.add(myError.getStatusCode() + ' - ' + myError.getMessage() + ' - ' + myError.getFields());
                        }
                    }
                }
            }
        }
        
        public AuditDmlActionResult(List<Database.UpsertResult> upsertResultsList){
            for(Database.UpsertResult myUpsertResult : upsertResultsList){
                if(!myUpsertResult.isSuccess()){
                    for(Database.Error myError : myUpsertResult.getErrors()){
                        if(StatusCode.UNABLE_TO_LOCK_ROW == myError.getStatusCode()){
                            //When this error happens, none of the items are inserted/updated/upserted.
                            errorsMap.put('UNABLE_TO_LOCK_ROW',myError.getMessage());
                            hasErrors = true;
                        }else{
                            unhandledErrors.add(myError.getStatusCode() + ' - ' + myError.getMessage() + ' - ' + myError.getFields());
                        }
                    }
                }
            }
        }

    }

}