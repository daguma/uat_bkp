@isTest
private class EmailAgeRequestParameterTest {
    @isTest static void validate() {
        EmailAgeRequestParameter ea = new EmailAgeRequestParameter();
        
        ea.email = 'test@test.com';
        System.assertEquals('test@test.com', ea.email);
        
        ea.ip_address = '0.0.0.0';
        System.assertEquals('0.0.0.0', ea.ip_address);
        
        ea.emailAgeStatusCode = '0';
        System.assertEquals('0', ea.emailAgeStatusCode);
    }
}