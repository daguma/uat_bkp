@isTest
private class DealAutomaticCtrlTest {

    @isTest static void test_status_category_controller() {
        DealAutomaticStatusCategoryCtrl controller = new DealAutomaticStatusCategoryCtrl();

        //gets categories
        String categories =  DealAutomaticStatusCategoryCtrl.getCategories();
        System.assertEquals(true, categories != null, 'categories is not null');


        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;

        //gets user_groups
        List<DealAutomaticUserGroup> usersGroupsByCategory = DealAutomaticStatusCategoryCtrl.getUsersGroupsByCategory(attribute);
        System.assertEquals(true, usersGroupsByCategory != null, 'usersGroupsByCategory is not null');

        List<DealAutomaticUserGroup> userList = new List<DealAutomaticUserGroup>();

        for (Integer i = 0; i < 5; i++) {
            DealAutomaticUserGroup user = new DealAutomaticUserGroup(String.valueOf(i), 'User Name', String.valueOf(i), 'Group Name', false, false);
            userList.add(user);
        }

        //save Users
        List<DealAutomaticUserGroup> savedUsers =  DealAutomaticStatusCategoryCtrl.saveUsers(attribute, userList, new List<DealAutomaticUserGroup>());
        System.assertEquals(true, savedUsers != null, 'saved users is not null');
    }

    @isTest static void test_grades_controller() {
        DealAutomaticGradeCtrl controller = new DealAutomaticGradeCtrl();

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;

        List<DealAutomaticUserGroup> userList = new List<DealAutomaticUserGroup>();

        for (Integer i = 0; i < 5; i++) {
            DealAutomaticUserGroup user = new DealAutomaticUserGroup(String.valueOf(i), 'User Name', String.valueOf(i), 'Group Name', false, false);
            userList.add(user);
        }

        String grades = DealAutomaticGradeCtrl.getGrades();
        System.assertEquals(false, grades == null, 'grades is not null');

        List<DealAutomaticUserGroup> savedUsers = DealAutomaticGradeCtrl.saveUsers(attribute, userList, new List<DealAutomaticUserGroup>());
        System.assertEquals(false, savedUsers == null, 'grades is not null');

        List<DealAutomaticUserGroup> users = DealAutomaticGradeCtrl.getUsersGroupsByGrade(attribute);
        System.assertEquals(false, users == null, 'grades is not null');
    }

    @isTest static void test_sources_controller() {
        DealAutomaticSourceCtrl controller = new DealAutomaticSourceCtrl();

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;

        List<DealAutomaticUserGroup> userList = new List<DealAutomaticUserGroup>();

        for (Integer i = 0; i < 5; i++) {
            DealAutomaticUserGroup user = new DealAutomaticUserGroup(String.valueOf(i), 'User Name', String.valueOf(i), 'Group Name', false, false);
            userList.add(user);
        }

        String sources = DealAutomaticSourceCtrl.getDealSources();
        System.assertEquals(false, sources == null, 'sources is not null');

        List<DealAutomaticUserGroup> savedUsers = DealAutomaticSourceCtrl.saveUsers(attribute, userList, new List<DealAutomaticUserGroup>());
        System.assertEquals(false, savedUsers == null, 'sources is not null');

        List<DealAutomaticUserGroup> users = DealAutomaticSourceCtrl.getUsersGroupsBySource(attribute);
        System.assertEquals(false, users == null, 'sources is not null');
    }

    @isTest static void test_employment_controller() {
        DealAutomaticEmploymentTypeCtrl controller = new DealAutomaticEmploymentTypeCtrl();

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;

        List<DealAutomaticUserGroup> userList = new List<DealAutomaticUserGroup>();

        for (Integer i = 0; i < 5; i++) {
            DealAutomaticUserGroup user = new DealAutomaticUserGroup(String.valueOf(i), 'User Name', String.valueOf(i), 'Group Name', false, false);
            userList.add(user);
        }

        String employment = DealAutomaticEmploymentTypeCtrl.getEmploymentTypes();
        System.assertEquals(false, employment == null, 'employment is not null');

        List<DealAutomaticUserGroup> savedUsers = DealAutomaticEmploymentTypeCtrl.saveUsers(attribute, userList, new List<DealAutomaticUserGroup>());
        System.assertEquals(true, savedUsers.size() == 5);

        List<DealAutomaticUserGroup> users = DealAutomaticEmploymentTypeCtrl.getUsersGroupsByEmployment(attribute);
        System.assertEquals(false, users == null, 'employment is not null');
    }

    @isTest static void test_useOfFunds_controller() {
        DealAutomaticUseOfFundsCtrl controller = new DealAutomaticUseOfFundsCtrl();

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;

        List<DealAutomaticUserGroup> userList = new List<DealAutomaticUserGroup>();

        for (Integer i = 0; i < 5; i++) {
            DealAutomaticUserGroup user = new DealAutomaticUserGroup(String.valueOf(i), 'User Name', String.valueOf(i), 'Group Name', false, false);
            userList.add(user);
        }

        String useOfFunds = DealAutomaticUseOfFundsCtrl.getUseOfFunds();
        System.assertEquals(false, useOfFunds == null, 'use of funds is not null');

        List<DealAutomaticUserGroup> savedUsers = DealAutomaticUseOfFundsCtrl.saveUsers(attribute, userList, new List<DealAutomaticUserGroup>());
        System.assertEquals(true, savedUsers.size() == 5);

        List<DealAutomaticUserGroup> users = DealAutomaticUseOfFundsCtrl.getUsersGroupsByUseOfFunds(attribute);
        System.assertEquals(false, users == null, 'use of funds is not null');
    }

    @isTest static void test_ficoRange_controller() {
        DealAutomaticFicoRangeCtrl controller = new DealAutomaticFicoRangeCtrl();

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;

        List<DealAutomaticUserGroup> userList = new List<DealAutomaticUserGroup>();

        for (Integer i = 0; i < 5; i++) {
            DealAutomaticUserGroup user = new DealAutomaticUserGroup(String.valueOf(i), 'User Name', String.valueOf(i), 'Group Name', false, false);
            userList.add(user);
        }

        String ficoRanges = DealAutomaticFicoRangeCtrl.getFicoRanges();
        System.assertEquals(false, ficoRanges == null, 'fico range is not null');

        List<DealAutomaticUserGroup> savedUsers = DealAutomaticFicoRangeCtrl.saveUsers(attribute, userList, new List<DealAutomaticUserGroup>());
        System.assertEquals(true, savedUsers.size() == 5);

        List<DealAutomaticUserGroup> users = DealAutomaticFicoRangeCtrl.getUsersGroupsByFicoRange(attribute);
        System.assertEquals(false, users == null, 'fico range is not null');
    }

    @isTest static void test_requestedAmount_controller() {
        DealAutomaticRequestedAmountCtrl controller = new DealAutomaticRequestedAmountCtrl();

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;

        List<DealAutomaticUserGroup> userList = new List<DealAutomaticUserGroup>();

        for (Integer i = 0; i < 5; i++) {
            DealAutomaticUserGroup user = new DealAutomaticUserGroup(String.valueOf(i), 'User Name', String.valueOf(i), 'Group Name', false, false);
            userList.add(user);
        }

        String amounts = DealAutomaticRequestedAmountCtrl.getAmounts();
        System.assertEquals(false, amounts == null, 'requested amount is not null');

        List<DealAutomaticUserGroup> savedUsers = DealAutomaticRequestedAmountCtrl.saveUsers(attribute, userList, new List<DealAutomaticUserGroup>());
        System.assertEquals(true, savedUsers.size() == 5);

        List<DealAutomaticUserGroup> users = DealAutomaticRequestedAmountCtrl.getUsersGroupsByAmount(attribute);
        System.assertEquals(false, users == null, 'requested amount is not null');
    }

    @isTest static void test_strategyType_controller() {
        DealAutomaticStrategyTypeCtrl controller = new DealAutomaticStrategyTypeCtrl();

        Deal_Automatic_Attributes__c attribute = new Deal_Automatic_Attributes__c();
        attribute.Id__c = 1;
        attribute.Display_Order__c = 1;

        List<DealAutomaticUserGroup> userList = new List<DealAutomaticUserGroup>();

        for (Integer i = 0; i < 5; i++) {
            DealAutomaticUserGroup user = new DealAutomaticUserGroup(String.valueOf(i), 'User Name', String.valueOf(i), 'Group Name', false, false);
            userList.add(user);
        }

        String strategies = DealAutomaticStrategyTypeCtrl.getStrategies();
        System.assertEquals(false, strategies == null, 'strategy type is not null');

        List<DealAutomaticUserGroup> savedUsers = DealAutomaticStrategyTypeCtrl.saveUsers(attribute, userList, new List<DealAutomaticUserGroup>());
        System.assertEquals(true, savedUsers.size() == 5);

        List<DealAutomaticUserGroup> users = DealAutomaticStrategyTypeCtrl.getUsersGroupsByStrategy(attribute);
        System.assertEquals(false, users == null, 'strategy type is not null');
    }

//----------------- Time Frame and location test
    @IsTest static void checkValidTimeRange() {
        DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
        boolean correctTimeRange = laaController.checkTimeRange(1, 3);
        system.assertEquals(true, correctTimeRange);
    }

    @IsTest static void checkNoneOptionTimeRangeValid() {
        DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
        boolean correctTimeRange = laaController.checkTimeRange(0, 0);
        system.assertEquals(true, correctTimeRange);
    }

    @IsTest static void checkSingleNoneOptionTimeRangeInValid() {
        DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
        boolean incorrectTimeRange = laaController.checkTimeRange(0, 1);
        system.assertEquals(false, incorrectTimeRange);
        incorrectTimeRange = laaController.checkTimeRange(1, 0);
        system.assertEquals(false, incorrectTimeRange);
    }

    @IsTest static void checkInvalidTimeRange() {
        DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
        boolean incorrectTimeRange = laaController.checkTimeRange(2, 1);
        system.assertEquals(false, incorrectTimeRange);
    }

    @IsTest static void checkValidTimeRangeInSync() {
        DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
        boolean correctTimeRange = laaController.checkTimeRangeInSync(0, 0, 3, 5);
        system.assertEquals(true, correctTimeRange);
        correctTimeRange = laaController.checkTimeRangeInSync(1, 3, 3, 5);
        system.assertEquals(true, correctTimeRange);
        correctTimeRange = laaController.checkTimeRangeInSync(5, 7, 3, 5);
        system.assertEquals(true, correctTimeRange);
    }

    @IsTest static void checkInvalidTimeRangeInSync() {
        DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
        boolean incorrectTimeRange = laaController.checkTimeRangeInSync(3, 7, 5, 8);
        system.assertEquals(false, incorrectTimeRange);
        incorrectTimeRange = laaController.checkTimeRangeInSync(7, 3, 5, 8);
        system.assertEquals(false, incorrectTimeRange);
    }

    @IsTest static void verifyAllFromTimeSlots() {
        DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
        List<SelectOption> options = laaController.getAllFromTimeSlots();
        system.assertEquals(25, options.size());
        SelectOption firstOption = options.get(0);
        SelectOption lastOption = options.get(options.size() - 1);
        system.assertEquals('None', firstOption.getLabel());
        system.assertEquals('0', firstOption.getValue());
        system.assertEquals('23:00 EST', lastOption.getLabel());
        system.assertEquals('24', lastOption.getValue());
    }

    @IsTest static void verifyAllToTimeSlots() {
        DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
        List<SelectOption> options = laaController.getAllToTimeSlots();
        system.assertEquals(25, options.size());
        SelectOption firstOption = options.get(0);
        SelectOption lastOption = options.get(options.size() - 1);
        system.assertEquals('None', firstOption.getLabel());
        system.assertEquals('0', firstOption.getValue());
        system.assertEquals('24:00 EST', lastOption.getLabel());
        system.assertEquals('25', lastOption.getValue());
    }

    @IsTest static void assign_emptyVariables() {
        DealAutomaticTimeFrameLocationCtrl dealTF = new DealAutomaticTimeFrameLocationCtrl();

        dealTF.mondayAssignmentFromTimeKennesaw = 0;
        dealTF.mondayAssignmentToTimeKennesaw = 0;
        dealTF.tuesdayAssignmentFromTimeKennesaw = 0;
        dealTF.tuesdayAssignmentToTimeKennesaw = 0;
        dealTF.wednesdayAssignmentFromTimeKennesaw = 0;
        dealTF.wednesdayAssignmentToTimeKennesaw = 0;
        dealTF.thursdayAssignmentFromTimeKennesaw = 0;
        dealTF.thursdayAssignmentToTimeKennesaw = 0;
        dealTF.fridayAssignmentFromTimeKennesaw = 0;
        dealTF.fridayAssignmentToTimeKennesaw = 0;
        dealTF.saturdayAssignmentFromTimeKennesaw = 0;
        dealTF.saturdayAssignmentToTimeKennesaw = 0;
        dealTF.sundayAssignmentFromTimeKennesaw = 0;
        dealTF.sundayAssignmentToTimeKennesaw = 0;
        dealTF.mondayAssignmentFromTimeDr = 0;
        dealTF.mondayAssignmentToTimeDr = 0;
        dealTF.tuesdayAssignmentFromTimeDr = 0;
        dealTF.tuesdayAssignmentToTimeDr = 0;
        dealTF.wednesdayAssignmentFromTimeDr = 0;
        dealTF.wednesdayAssignmentToTimeDr = 0;
        dealTF.thursdayAssignmentFromTimeDr = 0;
        dealTF.thursdayAssignmentToTimeDr = 0;
        dealTF.fridayAssignmentFromTimeDr = 0;
        dealTF.fridayAssignmentToTimeDr = 0;
        dealTF.saturdayAssignmentFromTimeDr = 0;
        dealTF.saturdayAssignmentToTimeDr = 0;
        dealTF.sundayAssignmentFromTimeDr = 0;
        dealTF.sundayAssignmentToTimeDr = 0;
        //dealTF.fromUsername = 'Test User';

        System.assert(true, true);
    }

    @isTest static void DealAutomaticTimeFrameLocationCtrlTest() {

        TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimesAllAssignedToKennesaw();

        genesis__applications__c app = LibraryTest.createApplicationTH();
        app.genesis__status__c = 'Credit Qualified';
        app.OwnerId = Label.Default_Lead_Owner_ID;
        app.Automatically_Assigned__c = false;
        update app;

        DealAutomaticTimeFrameLocationCtrl datflc = new DealAutomaticTimeFrameLocationCtrl();

        Pagereference res = datflc.saveAutomaticAssignment();
    }

    /* @IsTest static void verifyAutomaticAssignmentSavedWithCategoryExceptions() {
         TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimes(true);
         TestLeadAutomaticAssignmentFactory.createAutomaticAssignmentLeadCategoriesWithExceptions();
         DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
         laaController.saveAutomaticAssignment();
     }

     @IsTest static void verifyAutomaticAssignmentSavedWithoutCategoryExceptions() {
         TestLeadAutomaticAssignmentFactory.createSalesCenterLoginTimes(true);
         TestLeadAutomaticAssignmentFactory.createAutomaticAssignmentLeadCategoriesWithoutExceptions();
         DealAutomaticTimeFrameLocationCtrl laaController = new DealAutomaticTimeFrameLocationCtrl();
         laaController.saveAutomaticAssignment();
     }*/

}