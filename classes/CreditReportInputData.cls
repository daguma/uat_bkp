global class CreditReportInputData {
    public CreditReportRequestParameter creditReportRequestParameter {get; set;}

    public DecisioningParameter decisioningParameter {get; set;}
    
    // BRMS Change
    public GDSRequestParameter gdsRequestParameter {get;set;}
}