//SF token handling 
global class TokenAuthentication{

  //save SF token in custom setting
  WebService Static string updateSfToken(string inputParams){
        system.debug('============SFtoken inputParams============'+inputParams);
        string response='invalid json';
        
        //validate incoming json
        if(!String.isBlank(inputParams)){    
            try{
            //parse incoming json
            Map<string,string> inputParamsMap = (Map<String,String>) JSON.deserialize(inputParams, Map<String,String>.class);
            //get token and update configuration
            if(inputParamsMap.containsKey('token')) 
            {                    
                GDS_Configuration__c setting = GDS_Configuration__c.getOrgDefaults();           
                setting.SF_Token__c =inputParamsMap.get('token');
                upsert setting;
                response='success';
            }
            else{
              response='token is not present';}      
           }
           catch(exception e){
               response='error';
           }       
        } 
        system.debug('============SFtoken response============'+response);   
        
        return response;
     }
}