global class CollectionsCasesAssignmentUsersGroup {

    public static final String COLLECTIONS_GROUP_LEVEL_1 = 'CollectionsLevel1',
            COLLECTIONS_GROUP_LEVEL_2 = 'CollectionsLevel2',
            COLLECTIONS_GROUP_LEVEL_3 = 'CollectionsLevel3',
            COLLECTIONS_GROUP_LEVEL_4 = 'CollectionsLevel4',
            COLLECTIONS_GROUP_LEVEL_5 = 'CollectionsLevel5',
            COLLECTIONS_GROUP_LMS_LEVEL_1 = 'CollectionsPONLevel1',
            COLLECTIONS_GROUP_LMS_LEVEL_2 = 'CollectionsPONLevel2',
            COLLECTIONS_GROUP_LMS_LEVEL_3 = 'CollectionsPONLevel3',
            COLLECTIONS_GROUP_LMS_LEVEL_4 = 'CollectionsPONLevel4',
            COLLECTIONS_GROUP_LMS_LEVEL_5 = 'CollectionsPONLevel5',
            COLLECTIONS_GROUP_DMC_LEVEL_1 = 'CollectionsDMCLevel1',
            COLLECTIONS_GROUP_DMC_LEVEL_2 = 'CollectionsDMCLevel2',
            COLLECTIONS_GROUP_DMC_LEVEL_3 = 'CollectionsDMCLevel3';

    public String publicUserGroupName;
    public Integer minimumDPD, maximumDPD;
    public List<Id> usersList;

    public CollectionsCasesAssignmentUsersGroup(String collectionsPublicUserGroupName, Integer minDPD, Integer maxDPD) {
        this.publicUserGroupName = collectionsPublicUserGroupName;
        this.minimumDPD = minDPD;
        this.maximumDPD = maxDPD;
        this.usersList = getAllGroupUsers(getGroupName());
    }

    public Boolean isUserMemberOfGroup(Id userId){
        if(String.isEmpty(userId)) return false;
        return new Set<Id>(this.usersList).contains(userId);
    }

    public List<Id> getAllGroupMembersIds(){
        return this.usersList;
    }

    public String getGroupName(){
        return this.publicUserGroupName;
    }

    private List<Id> getAllGroupUsers(String groupName){
        List<Id> resultList = new List<Id>();
        if(String.isEmpty(groupName)) return resultList;

        String groupId = '';
        if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LEVEL_1)) {
            groupId = Label.CollectionsLevel1;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LEVEL_2)) {
            groupId = Label.CollectionsLevel2;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LEVEL_3)) {
            groupId = Label.CollectionsLevel3;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LEVEL_4)) {
            groupId = Label.CollectionsLevel4;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LEVEL_5)) {
            groupId = Label.CollectionsLevel5;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_DMC_LEVEL_1)) {
            groupId = Label.CollectionsDMCLevel1;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_DMC_LEVEL_2)) {
            groupId = Label.CollectionsDMCLevel2;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_DMC_LEVEL_3)) {
            groupId = Label.CollectionsDMCLevel3;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LMS_LEVEL_1)) {
            groupId = Label.CollectionsPONLevel1;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LMS_LEVEL_2)) {
            groupId = Label.CollectionsPONLevel2;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LMS_LEVEL_3)) {
            groupId = Label.CollectionsPONLevel3;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LMS_LEVEL_4)) {
            groupId = Label.CollectionsPONLevel4;
        } else if (groupName.equalsIgnoreCase(COLLECTIONS_GROUP_LMS_LEVEL_5)) {
            groupId = Label.CollectionsPONLevel5;
        }
        else {
            return new List<Id>();
        }

        List<Id> usersList = new List<Id>();
        for(GroupMember gm : [SELECT Id, UserOrGroupId, GroupId FROM GroupMember WHERE GroupId =: groupId])usersList.add(gm.UserOrGroupId);

        //Check if the users in the Public Group are active
        for(User groupMemberUser : [SELECT Id FROM User WHERE Id IN :usersList])resultList.add(groupMemberUser.Id);

        return resultList;
    }
}