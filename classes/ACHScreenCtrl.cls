public with sharing class ACHScreenCtrl {
     /**
* handleDLFields: 
*  Sets the value of the DL fields in Application, depending on
*  the DL_AccountStatementSF record loaded.
* @param DL_AccountStatementSF statement
*/ 
     public static void handleDLFields(DL_AccountStatementSF statement,Opportunity application){
        
        application.DL_Available_Balance__c = statement.availableBalance!=null ? Decimal.valueOf(statement.availableBalance) : null ;
        application.DL_As_of_Date__c = statement.asOfDate !=null ? Date.valueOf(statement.asOfDate) : null ;
        application.DL_Current_Balance__c = statement.currentBalance!=null ? Decimal.valueOf(statement.currentBalance) : null;
        application.DL_Average_Balance__c = statement.averageBalance !=null ? Decimal.valueOf(statement.averageBalance) : null;
        application.DL_Deposits_Credits__c = statement.totalCredits!=null ? Decimal.valueOf(statement.totalCredits) : null;
        application.DL_Avg_Bal_Latest_Month__c = statement.averageBalanceRecent!=null ? Decimal.valueOf(statement.averageBalanceRecent) : null;
        application.DL_Withdrawals_Debits__c = statement.totalDebits!=null ? Decimal.valueOf(statement.totalDebits) : null;
        application.Number_of_Negative_Days_Number__c = getNegativeDays(statement);
        
    } 
    
    /**
* getNegativeDays: 
*  From DecisionLogic´s report, take only the most recent month transactions.
*  That period of time is defined as:
*     from the most recent transaction date, all the way back 30 calendar days
*  Return the number of days where the End Of Day balance was negative.
* @param statement
* @return negativeDays number
*/    
    private static Integer getNegativeDays(DL_AccountStatementSF statement){
        
        Integer negativeDaysCount = 0;
        
        if(statement==null)
            return negativeDaysCount;
        
        List<DL_AccountStatementSF.TransactionSummaries> txnList = 
            statement.transactionSummaries;
        if(txnList==null || txnList.isEmpty())
            return negativeDaysCount;
        
        Integer txnPos = 0;
        Date currentTxnDate,
            previousTxnDate;
        Decimal endOfDayBalance;
        DL_AccountStatementSF.TransactionSummaries txn;
        
        try{
            
            //Most recent transaction
            txn = txnList.get(txnPos);
            endOfDayBalance = txn.runningBalance;
            if(endOfDayBalance<0)
                negativeDaysCount = negativeDaysCount + 1;
            previousTxnDate = Date.valueOf(txn.transactionDate);
            
            //Rest of transactions
            do{
                txnPos++;
                txn = txnList.get(txnPos);
                
                currentTxnDate = Date.valueOf(txn.transactionDate);                
                
                if(currentTxnDate != previousTxnDate){                   
                    
                    endOfDayBalance = txn.runningBalance;
                    
                    if(endOfDayBalance<0)
                        negativeDaysCount = negativeDaysCount + 1;
                    
                    previousTxnDate = Date.valueOf(txn.transactionDate);
                    
                }
                
                
            }while(Date.valueOf(statement.asOfDate).addDays(-30) <= currentTxnDate);
            
        }catch(Exception e){
            System.debug('Exception at getNegativeDays: ' + e);
            return 0;
        }
        
        return negativeDaysCount;
    }
    
    public static void EWSNoteRecord(string AppId, string AccNo, string RoutingNo, string AppName){
        Note noteDetails = new Note();
        noteDetails.ParentId = AppId;
        noteDetails.body = 'At ' + datetime.now().format('hh:mm:ss') + ' on ' + date.today().format() + ',' + UserInfo.GetName() + ' made a call to EWS with the Bank Account Number ' + AccNo + ' and Routing Number ' + RoutingNo;
        noteDetails.Title = 'EWS Call Made on ' + AppName;
        insert noteDetails;
    }
}