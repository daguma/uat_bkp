// TODO - Add warning displayed on page whenever a period in which commmitted rev rec transactions is selected 

public class ffaCOAManagerController {
    
    //Variables 
    public c2g__codaInvoice__c dummyInvoice {get; set;}
    public COA_Funnel_Rules_Settings__c coaSettings {get; set;}
    public c2g__codaPeriod__c selectedPeriod {get; set;}
    public List<SelectOption> periodOptions {get; set;}
    public Map<Id, c2g__codaPeriod__c> accountingPeriodMap {get; set;}
    public Indirect_Cost_of_Acquisition_History__c coaHistoryObj {get; set;}

    //COA Display (Calculated) Fields
    public Integer loanCount {get; set;}
    public Map<String, Decimal> apiVendorCOADimensionMap {get; set;}
    public Map<String, Decimal> staffCOADimensionMap {get; set;}
    public List<coaWrapper> apiVendorWrapperList {get; set;}
    public List<coaWrapper> staffWrapperList {get; set;}
    public List<coaWrapper> wrapperList {get; set;}

    //COA Total Variables
    public Decimal totalAPIVendorCOA {
        get{  return totalAPIVendorCOA; }
        set{  totalAPIVendorCOA = value.setScale(2); }
    } 
    public Decimal totalStaffCOA {
        get{  return totalStaffCOA; }
        set{  totalStaffCOA = value.setScale(2); }
    } 
    public Decimal totalAPIVendorInvoiced {
        get{  return totalAPIVendorInvoiced; }
        set{  totalAPIVendorInvoiced = value.setScale(2); }
    } 
    public Decimal totalStaffInvoiced {
        get{  return totalStaffInvoiced; }
        set{  totalStaffInvoiced = value.setScale(2); }
    } 
    public Decimal totalAPIVendorPercent {get; set;}
    public Decimal totalStaffPercent {get; set;}
    public Decimal totalIndirectCOA {
        get{  return totalIndirectCOA; }
        set{  totalIndirectCOA = value.setScale(2); }
    } 
    public Decimal totalAPIVendorCOAPerLoan {
        get{  return totalAPIVendorCOAPerLoan; }
        set{  totalAPIVendorCOAPerLoan = value.setScale(2); }
    } 
    public Decimal totalStaffVendorCOAPerLoan {
        get{  return totalStaffVendorCOAPerLoan; }
        set{  totalStaffVendorCOAPerLoan = value.setScale(2); }
    }
    public Decimal totalIndirectCOAPerLoan {
        get{  return totalIndirectCOAPerLoan; }
        set{  totalIndirectCOAPerLoan = value.setScale(2); }
    }

    //CONSTRUCTOR
    public ffaCOAManagerController() {

        //Instantiate 
        selectedPeriod = new c2g__codaPeriod__c();
        dummyInvoice = new c2g__codaInvoice__c();

        //Get org default settings 
        coaSettings = COA_Funnel_Rules_Settings__c.getOrgDefaults(); 
        
        //Get Accounting Periods 
        accountingPeriodMap = new Map<Id, c2g__codaPeriod__c>(); 
        for (c2g__codaPeriod__c p: [SELECT Id, c2g__StartDate__c, c2g__EndDate__c 
                                    FROM c2g__codaPeriod__c 
                                    WHERE c2g__OwnerCompany__r.Name = 'Lendingpoint LLC'
                                    AND c2g__StartDate__c >= :Date.today().toStartOfMonth().addMonths(-6)
                                    AND c2g__Description__c = 'Trading Period'
                                    ORDER BY c2g__StartDate__c ASC]){
            accountingPeriodMap.put(p.Id, p);
            if (Date.today() >= p.c2g__StartDate__c && Date.today() <= p.c2g__EndDate__c){
                dummyInvoice.c2g__Period__c = p.Id;
            } 
        }
        System.debug('accountingPeriodMap: ' + accountingPeriodMap); 

        //Create/retrieve the COA history object for the current period 
        getCOAHistoryRecordForSelectedPeriod(); 

        //Ensure that the "shell" wrapper objects are available and in the wrapper list variable
        wrapperList = new List<coaWrapper>(); 
        totalIndirectCOAPerLoan = 0.00; 
        totalStaffVendorCOAPerLoan = 0.00; 
        totalAPIVendorCOAPerLoan = 0.00; 

        // 1) Get distinct list of all dimensions, will use to drive display table
        List<c2g__codaDimension4__c> dimensions = getDimension4List(); 

        // 2) Build shell list of wrapper objects, will be updated by the aggregate queries later on
        wrapperList = new List<coaWrapper>(); 
        for (c2g__codaDimension4__c d: dimensions){
            coaWrapper newWrapper = new coaWrapper(); 
            newWrapper.dimensionName = d.Name; 
            wrapperList.add(newWrapper); 
        }
            
        //Pre-Populate the Loan Count Variable Based on Current Period
        refreshCOATotals(); 
    }

    //Update Loan Account Method 
    public PageReference updateLoanAccounts() {
        List<loan__Loan_Disbursal_Transaction__c> disbursalTxs = new List<loan__Loan_Disbursal_Transaction__c>(); 

        if (Test.isRunningTest()){
            disbursalTxs =  [SELECT loan__Loan_Account__c
                               FROM loan__Loan_Disbursal_Transaction__c 
                              WHERE loan__Disbursal_Date__c >= :accountingPeriodMap.get(dummyInvoice.c2g__Period__c).c2g__StartDate__c
                                AND loan__Disbursal_Date__c <= :accountingPeriodMap.get(dummyInvoice.c2g__Period__c).c2g__EndDate__c
                                AND loan__Reversed__c = false
                                LIMIT 5];
        }
        else {
            disbursalTxs =  [SELECT loan__Loan_Account__c
                               FROM loan__Loan_Disbursal_Transaction__c 
                              WHERE loan__Disbursal_Date__c >= :accountingPeriodMap.get(dummyInvoice.c2g__Period__c).c2g__StartDate__c
                                AND loan__Disbursal_Date__c <= :accountingPeriodMap.get(dummyInvoice.c2g__Period__c).c2g__EndDate__c
                                AND loan__Reversed__c = false];
        }   

        Set<Id> loanAccountIds = new Set<Id>(); 
        for (loan__Loan_Disbursal_Transaction__c ldt: disbursalTxs){
            loanAccountIds.add(ldt.loan__Loan_Account__c); 
        }
        System.debug('ffaCOAManagerController.updateLoanAccounts loanAccountIds: ' + loanAccountIds); 

        ffaCOABatch.startBatch(loanAccountIds, totalAPIVendorCOAPerLoan, totalStaffVendorCOAPerLoan); 

        //Grab the current values of the wrapper class input percentages, for each history obj field, and update the current history obj to ensure what's on the screen is saved as a historical rate record
        for (coaWrapper w: wrapperList){
            coaHistoryObj.put(COA_Funnel_Dimension_API_Mapping__c.getAll().get(w.dimensionName).Funnel_Category_Name__c, w.displayPercentage);
        }
        upsert coaHistoryObj; 
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'The Loan Account update batch was succesfully initiated. You will receive an email when the job is complete.')); 

        return null;
    }

    //Method to calculate COA totals for the selected period
    public PageReference previewCOACalcs() {
        getCOAHistoryRecordForSelectedPeriod();  
        refreshCOATotals(); 
        return null; 
    }

    //Helper method to refresh all COA-related totals displayed on screen 
    public PageReference refreshCOATotals() {

        // 1) Build necessary data-sets
        getLoanCountForPeriod(); 
        getAPIVendorCOAForPeriod();
        getStaffCOAForPeriod();

        // 2) Add per-dimension values to wrapper list 
        buildWrapperList(); 

        // 3) Create totals 
        totalIndirectCOA = totalStaffCOA + totalAPIVendorCOA;
        totalIndirectCOAPerLoan = totalAPIVendorCOAPerLoan + totalStaffVendorCOAPerLoan;        
        return null;
    }

    //Get Total Number of Loans Originated in Selected Period
    private Integer getLoanCountForPeriod() {
        System.debug('getLoanCountForPeriod STARTED!');
        System.debug('dummyInvoice: ' + dummyInvoice);  
        AggregateResult[] results = [SELECT count(id) loanCount
                                       FROM loan__Loan_Disbursal_Transaction__c 
                                      WHERE loan__Disbursal_Date__c >= :accountingPeriodMap.get(dummyInvoice.c2g__Period__c).c2g__StartDate__c
                                                                        AND loan__Disbursal_Date__c <= :accountingPeriodMap.get(dummyInvoice.c2g__Period__c).c2g__EndDate__c
                                      AND loan__Reversed__c = false];

        loanCount = (Integer)results[0].get('loanCount'); 

        return loanCount; 
    }

    //Helper method to retrieve all dimension 4 values
    private List<c2g__codaDimension4__c> getDimension4List() {
        return [SELECT Id, Name FROM c2g__codaDimension4__c ORDER BY Name ASC]; 
    }

    private Decimal getAPIVendorCOAForPeriod() {
        totalAPIVendorPercent = 0.00; 
        totalAPIVendorInvoiced = 0.00; 
        totalAPIVendorCOA = 0.00; 
        AggregateResult[] results = [SELECT sum(c2g__Dimension4Value__c) totalAPICost, c2g__Dimension4__r.Name
                                      FROM c2g__codaTransactionLineItem__c
                                      WHERE c2g__Dimension4__c != null 
                                      AND c2g__Transaction__r.c2g__Period__c = :dummyInvoice.c2g__Period__c
                                      AND c2g__Account__r.Staff_Payroll_Vendor__c = False
                                      GROUP BY c2g__Dimension4__r.Name
                                      ORDER BY c2g__Dimension4__r.Name];

        apiVendorCOADimensionMap = new Map<String, Decimal>();
        apiVendorWrapperList = new List<coaWrapper>();  
        for (AggregateResult ar: results){
            apiVendorCOADimensionMap.put((String)ar.get('Name'), ((Decimal)ar.get('totalAPICost') == null ? 0.00 : (Decimal)ar.get('totalAPICost')));
            //coaWrapper wrapper = new coaWrapper((Decimal)ar.get('totalAPICost'), (String)ar.get('Name'), coaHistoryObj); 
            //apiVendorWrapperList.add(wrapper);  
            totalAPIVendorInvoiced += (Decimal)ar.get('totalAPICost');
            //totalAPIVendorPercent += wrapper.displayPercentage; 
            ///totalAPIVendorCOA += wrapper.displayTotalCOA;  
        }

        //Handle divide by zero 
        /*if (loanCount > 0){
            totalAPIVendorCOAPerLoan = totalAPIVendorCOA/loanCount;  
        }*/

        return totalAPIVendorCOA; 
    }

    private Decimal getStaffCOAForPeriod() {
        totalStaffPercent = 0.00; 
        totalStaffInvoiced = 0.00; 
        totalStaffCOA = 0.00; 
        AggregateResult[] results = [SELECT sum(c2g__Dimension4Value__c) totalStaffCost, c2g__Dimension4__r.Name
                                      FROM c2g__codaTransactionLineItem__c
                                      WHERE c2g__Dimension4__c != null 
                                      AND c2g__Transaction__r.c2g__Period__c = :dummyInvoice.c2g__Period__c
                                      AND c2g__Account__r.Staff_Payroll_Vendor__c = True
                                      GROUP BY c2g__Dimension4__r.Name
                                      ORDER BY c2g__Dimension4__r.Name];

        staffCOADimensionMap = new Map<String, Decimal>(); 
        staffWrapperList = new List<coaWrapper>(); 
        for (AggregateResult ar: results){
            staffCOADimensionMap.put((String)ar.get('Name'), ((Decimal)ar.get('totalStaffCost') == null ? 0.00 : (Decimal)ar.get('totalStaffCost')));
            //coaWrapper wrapper = new coaWrapper((Decimal)ar.get('totalStaffCost'), (String)ar.get('Name'), coaHistoryObj); 
            //staffWrapperList.add(wrapper);   
            totalStaffInvoiced += (Decimal)ar.get('totalStaffCost'); 
            //totalStaffPercent += wrapper.displayPercentage; 
            //totalStaffCOA += wrapper.displayTotalCOA; 
        }

        //Handle divide by zero
        /*if (loanCount > 0){ 
            totalStaffVendorCOAPerLoan = totalStaffCOA/loanCount; 
        }*/

        return totalStaffCOA; 
    }
    
    private Indirect_Cost_of_Acquisition_History__c getCOAHistoryRecordForSelectedPeriod(){

        coaHistoryObj = new Indirect_Cost_of_Acquisition_History__c(); 

        //Query the history obj for the currently selected period (assumes that the dummyInvoice.c2g__Period__c field is not null!)
        List<Indirect_Cost_of_Acquisition_History__c> historyObjList = [SELECT Applications__c, Contacts__c, Contract_Package_Sent_Received__c, Credit_Qualified__c, Documents_in_Review__c,
                                                                      id, Name, Period__c, Documents_Requested__c, Funded__c, Funded_Credit_Qualified__c, Funded_Documents_Requested__c
                                                                FROM Indirect_Cost_of_Acquisition_History__c 
                                                                WHERE Period__c = :dummyInvoice.c2g__Period__c];

        //If no record exists, create one                                                                
        if(historyObjList.isEmpty()){
            Indirect_Cost_of_Acquisition_History__c coaHistory = new Indirect_Cost_of_Acquisition_History__c();
            coaHistory.Applications__c = 0.0; 
            coaHistory.Contacts__c = 0.00;
            coaHistory.Contract_Package_Sent_Received__c = 0.00;
            coaHistory.Credit_Qualified__c = 0.00;
            coaHistory.Documents_in_Review__c = 0.00;
            coaHistory.Documents_Requested__c = 0.00;
            coaHistory.Funded__c = 0.00; 
            coaHistory.Funded_Credit_Qualified__c = 0.00; 
            coaHistory.Funded_Documents_Requested__c = 0.00;
            coaHistory.Period__c = dummyInvoice.c2g__Period__c; 
            coaHistoryObj = coaHistory;  
            return coaHistoryObj; 
        }
        else {
            coaHistoryObj = historyObjList[0]; 
            return coaHistoryObj; 
        }
    }

    //Assumes that the COA maps for both vendor and API are populated, then builds single wrapper list for display to user 
    public void buildWrapperList(){

        // 1) Get total invoiced per category, add to wrapper list, calculate running total variables 
        totalAPIVendorCOA = 0.00; 
        totalStaffCOA = 0.00; 
        totalAPIVendorCOAPerLoan = 0.00; 
        totalStaffVendorCOAPerLoan = 0.00; 
        totalAPIVendorInvoiced = 0.00; 
        totalStaffInvoiced = 0.00; 


        for (coaWrapper w: wrapperList){
            //Add API Total Invoiced Amount 
            w.displayTotalAPIInvoicedAmount = apiVendorCOADimensionMap.get(w.dimensionName); 

            //Add Staff Total Invoiced Amount
            w.displayTotalStaffInvoicedAmount = staffCOADimensionMap.get(w.dimensionName); 

            //Set display percentage
            System.debug(w.dimensionName + ': ' + w.displayPercentage); 
            Decimal historyObjPercent = (Decimal)coaHistoryObj.get(COA_Funnel_Dimension_API_Mapping__c.getAll().get(w.dimensionName).Funnel_Category_Name__c);
            w.displayPercentage = w.displayPercentage == 0.00 ? historyObjPercent : w.displayPercentage;

            //Calculate the Dimension-specific Staff COA 
            if (w.displayTotalStaffInvoicedAmount == null || w.displayTotalStaffInvoicedAmount == 0.00){
                w.displayTotalCOAStaff = 0.00; 
                w.displayTotalStaffInvoicedAmount = 0.00; 
            }
            else {
                w.displayTotalCOAStaff = w.displayTotalStaffInvoicedAmount * w.displayPercentage / 100;
                totalStaffCOA += w.displayTotalCOAStaff; 
                totalStaffInvoiced += w.displayTotalStaffInvoicedAmount;  
            }

            //Calculate the Dimension-specific Vendor API COA
            if (w.displayTotalAPIInvoicedAmount == null || w.displayTotalAPIInvoicedAmount == 0.00){
                w.displayTotalCOAAPI = 0.00; 
                w.displayTotalAPIInvoicedAmount = 0.00; 
            }
            else {
                w.displayTotalCOAAPI = w.displayTotalAPIInvoicedAmount * w.displayPercentage / 100;
                totalAPIVendorCOA += w.displayTotalCOAAPI; 
                totalAPIVendorInvoiced += w.displayTotalAPIInvoicedAmount; 
            }

        }

        // 2) Calculate the per-loan COA for both Staff and API Vendor 
        totalAPIVendorCOAPerLoan = loanCount > 0 ? (totalAPIVendorCOA / loanCount) : 0.00; 
        totalStaffVendorCOAPerLoan = loanCount > 0 ? (totalStaffCOA / loanCount) : 0.00; 
    }


    public class coaWrapper {

        //COA Percent Value 
        public Decimal displayPercentage {get; set;}

        //API Invoiced Amount
        public Decimal displayTotalAPIInvoicedAmount {get; set;}

        //Staff Invoiced Amount
        public Decimal displayTotalStaffInvoicedAmount {get; set;}

        //Combined total invoiced
        public Decimal displayTotalInvoiced {get; set;}

        //API COA
        public Decimal displayTotalCOAAPI {get; set;}

        //Staff COA
        public Decimal displayTotalCOAStaff {get; set;}

        //Combined total COA
        public Decimal displayTotalCOA {get; set;}

        //Dimension to custom setting value mapping properties
        public String dimensionName {get; set;}
        public String customSettingFieldAPI {get; set;}

        //User-set percentage value
        public Decimal userDefinedPercent {get;set;}

        public coaWrapper(){
            displayPercentage = 0.00; 
            displayTotalAPIInvoicedAmount = 0.00; 
            displayTotalStaffInvoicedAmount = 0.00;
            displayTotalInvoiced = 0.00; 
            displayTotalCOAAPI = 0.00;
            displayTotalCOAStaff = 0.00;
            displayTotalCOA = 0.00;   
            dimensionName = ''; 
            customSettingFieldAPI = '';   
        }

        /*public coaWrapper(Decimal inputTotalAmount, String inputDimensionName, Indirect_Cost_of_Acquisition_History__c historyObj){
            displayTotalInvoiced = inputTotalAmount;
            dimensionName = inputDimensionName; 
            customSettingFieldAPI = COA_Funnel_Dimension_API_Mapping__c.getAll().get(dimensionName).Funnel_Category_Name__c; 
            displayPercentage = (Decimal)historyObj.get(customSettingFieldAPI); 
            displayTotalCOA = (displayPercentage/100) * displayTotalInvoiced; 
        }*/
    }

}