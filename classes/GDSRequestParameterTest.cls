@isTest
public class GDSRequestParameterTest {
    @isTest static void validate() {
        
        GDSRequestParameter gdsParam = new GDSRequestParameter ();
        
        gdsParam.applicationId = '1234567890';
        system.assertEquals('1234567890',gdsParam.applicationId);
        
        gdsParam.contactId = 'test data';
        system.assertEquals('test data',gdsParam.contactId);
    
        gdsParam.channelId = 'test data';
        system.assertEquals('test data',gdsParam.channelId );
        
        gdsParam.sequenceId = 'test data';
        system.assertEquals('test data',gdsParam.contactId);
        
        gdsParam.productId = 'test data';
        system.assertEquals('test data',gdsParam.productId );
        
        gdsParam.sessionId = 'test data';
        system.assertEquals('test data',gdsParam.contactId);
        
        gdsParam.executionType = 'test data';
        system.assertEquals('test data',gdsParam.executionType );
        
        gdsParam.apiSessionId = 'test';
        system.assertEquals('test',gdsParam.apiSessionId );
        
        gdsParam.organizationId = 'test';
        system.assertEquals('test',gdsParam.organizationId);
        
        gdsParam.organizationURL = 'test';
        system.assertEquals('test',gdsParam.organizationURL);
        
        gdsParam.isClarityValidate = true;
        system.assertEquals(true,gdsParam.isClarityValidate );
        
        gdsParam.ExternalParams = null;
        system.assertEquals(null,gdsParam.ExternalParams );
        
    }
}