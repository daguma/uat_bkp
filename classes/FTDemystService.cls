global class FTDemystService {   
    @Future(callout=true)
    public static void FTDemystCall(String PersonalInfoString) {   
        String  demystResponse, leadId, contactId, EmpAcceptanceVar, AcceptanceVar, EmailAcceptanceVar, 
                IncomeAcceptanceVar, request, response, FTRequest, FTResponse,FTRes, OFACRes;
        Lead l;
        Contact c;
        Map<String, String> OFACMap = new Map<String, String>();
        Map<String, String> PersonInfoMap = (Map<String,String>) JSON.deserialize(PersonalInfoString, Map<String,String>.class); 
        
        if(PersonInfoMap.ContainsKey('leadId'))
            leadId = (PersonInfoMap.get('leadId') != null) ? EncodingUtil.urlEncode(PersonInfoMap.get('leadId'), 'UTF-8') : null;
        
        if(PersonInfoMap.ContainsKey('contactId'))
            contactId = (PersonInfoMap.get('contactId') != null) ? EncodingUtil.urlEncode(PersonInfoMap.get('contactId'), 'UTF-8') : null;

        if(String.isNotEmpty(leadId))
            l = [SELECT Phone, 
                        Id, 
                        Factor_Trust_Verification__c, 
                        Demyst_Income_Verification__c, 
                        Demyst_Employer_Verification__c, 
                        Demyst_Email_Verification__c,
                        Demyst_Email_Acceptance__c,
                        Factor_Trust_Acceptance__c,
                        Demyst_Employer_Acceptance__c,
                        Demyst_Income_Acceptance__c, 
                        FirstName, 
                        LastName, 
                        Street,
                        State,
                        City,
                        IsConverted,
                        PostalCode,
                        Country,
                        Email, 
                        Annual_Income__c, 
                        Employer_Name__c, 
                        Title 
                FROM    Lead 
                WHERE   Id=: leadId];
        
        if(String.isNotEmpty(contactId))
            c = [SELECT  Phone,
                        Id,
                        Factor_Trust_Verification__c,
                        Demyst_Income_Verification__c,
                        Demyst_Employer_Verification__c,
                        Demyst_Email_Verification__c,
                        Factor_Trust_Acceptance__c,
                        Demyst_Email_Acceptance__c,
                        Demyst_Employer_Acceptance__c,
                        Demyst_Income_Acceptance__c, 
                        FirstName, 
                        LastName,
                        MailingStreet,
                        MailingState,
                        MailingCity,
                        MailingPostalCode,
                        MailingCountry,
                        Email, 
                        Annual_Income__c, 
                        Employer_Name__c, 
                        Title 
                FROM    Contact 
                WHERE   Id=: contactId];
        
        try { 
            Map<String, String> ResMap = new Map<String, String>();
            EmailAcceptanceVar = 'Y';
            IncomeAcceptanceVar = 'Y';
            EmpAcceptanceVar = 'Y';
            AcceptanceVar = 'Y';
        
            OFACRes = BridgerService.verifyBridger(PersonalInfoString);
            OFACMap = (Map<String,String>)JSON.deserialize(OFACRes, Map<String,String>.class);
            
            if(OFACMap.containsKey('acceptance') && OFACMap.get('acceptance') == 'Y')
                ResMap.put('FT_Demyst_Response','Y');
            else
                ResMap.put('FT_Demyst_Response','N');   
            
            DemystUtil.updateResult(l, c, ResMap);
        
        } catch(Exception exp) {
            Map<String, String> ResMap = new Map<String, String>();
            String EmailReslt = FactorTrust.getConfigResult('email','AllowProceed');
            String IncomeReslt = FactorTrust.getConfigResult('income','AllowProceed');
            String FTConfig = FactorTrust.getConfigResult('factorTrust','AllowProceed');
            String Acc = (EmailReslt == 'N' || IncomeReslt == 'N' || FTConfig == 'N')? 'N' : 'Y';
            ResMap.put('FT_Demyst_Response',Acc); 
            DemystUtil.updateResult(l, c, ResMap);  
        }
    }
    
     
    @InvocableMethod(label='Call OFAC Service')
    public static void callOFACService(List<Opportunity> apps) {
        List<Logging__c> exceptionLogs = new List<Logging__c>();
        for(Opportunity app : apps) {
            try {
                Map<String, String> reqMap = new Map<String,String>();
                if(app.Contact__c != null) {
                    String productName = app.ProductName__c;
                    //4673 - Changes Start
                    if(string.isEmpty(productName))
                        productName = [SELECT Product__c FROM Contact where Id =: app.Contact__c].Product__c;
                    
                    if((app.Status__c == 'Offer Accepted' && (generalPurposeWebServices.isPointOfNeed(productName) || generalPurposeWebServices.isSelfPayProduct(productName))) || 
                        (app.Status__c=='Documents In Review' && !generalPurposeWebServices.isPointOfNeed(productName) && !generalPurposeWebServices.isSelfPayProduct(productName)))
                    { 
                        reqMap.put('contactId',app.Contact__c);
                        if (!System.isBatch() && !System.isFuture()) FTDemystService.FTDemystCall(JSON.serialize(reqMap));
                    }
                }
            }
            catch(Exception e) {
                exceptionLogs.add(new Logging__c(Opportunity__c = app.Id,
                                                Contact__c = app.Contact__c,
                                                Request__c = 'Call OFAC Service',
                                                Response__c = e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString(),
                                                Webservice__c = 'FTDemystService.callOFACService'));
                WebToSFDC.notifyDev('FTDemystService.callOFACService Error',  e.getMessage() + ' line ' + (e.getLineNumber()) + '\n' + e.getStackTraceString());
            }
        }
        if(!exceptionLogs.isEmpty()) 
            insert exceptionLogs;
    }
}