@isTest
public class TestUpdatePaymentToleranceRate{

    public testMethod static void testPaymentToleranceRate(){
        loan.TestHelper.systemDate = Date.newInstance(2016, 03, 01);
        loan.TestHelper.createSeedDataForTesting();


        loan__Currency__c curr = loan.TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');

            //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);

        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice('TestOffice', false);
        insert loan.TestHelper.createMultipleDayProcessRecs(loan.TestHelper.systemDate, dummyOffice, 20, loan.TestHelper.systemDate);
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                                                        dummyAccount,
                                                        curr,
                                                        dummyFeeSet);
        dummyLP.loan__Payment_Tolerance_Rate__c = 10;
        update dummyLP;
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();

        //Borrower
        Contact dummyContact = new Contact (FirstName = 'Dummy', Lastname = 'Contact');
        insert dummyContact;
        //Loan Contract
        loan__Loan_Account__c loanAccount = new loan__Loan_Account__c(loan__Loan_Amount__c = 10000,
                                                                      loan__Contact__c = dummyContact.Id,
                                                                      loan__Product_Type__c = loan.LoanConstants.LOAN,
                                                                      loan__Loan_Product_Name__c = dummyLP.Id,
                                                                      loan__Interest_Rate__c = 14,
                                                                      loan__Number_Of_Installments__c = 6);
        loan.BorrowerAPI1 bAPI = loan.APIFactory.getBorrowerAPI1();
        loanAccount = bAPI.createContract(loanAccount, null, null);

        loanAccount.loan__Payment_Tolerance_Rate__c = 0;
        update loanAccount;

        Test.startTest();
        Database.executeBatch(new UpdatePaymentToleranceRateOnContracts());
        Test.stopTest();

        loan__Loan_Account__c updatedLoanAccount = [SELECT Id,
                                                           loan__Payment_Tolerance_Rate__c
                                                    FROM loan__Loan_Account__c
                                                    WHERE Id =:loanAccount.Id
                                                    ];
        System.assertEquals(null, updatedLoanAccount.loan__Payment_Tolerance_Rate__c);
    }
}