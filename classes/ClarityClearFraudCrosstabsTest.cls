@isTest
public class ClarityClearFraudCrosstabsTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearFraudCrosstabs ccf = new ClarityClearFraudCrosstabs();
        Test.stopTest();
        
        System.assertEquals(null ,ccf.clear_fraud_crosstab);
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearFraudCrosstabs ccf = new ClarityClearFraudCrosstabs();
        Test.stopTest();
        ClarityClearFraudCrosstab ccf1 = new ClarityClearFraudCrosstab();
        
        ccf1.name = 'test';
        ccf1.ssn = 1;
        ccf1.drivers_license = 1;
        ccf1.bank_account = 1;
        ccf1.home_address = 1;
        ccf1.zip_code = 1;
        ccf1.home_phone = 1;
        ccf1.cell_phone = 1;
        ccf1.email_address = 1;
        
        ccf.clear_fraud_crosstab = new list<ClarityClearFraudCrosstab>();
        ccf.clear_fraud_crosstab.add(ccf1);
        
        System.assertEquals(1,ccf.clear_fraud_crosstab.size());
    }
}