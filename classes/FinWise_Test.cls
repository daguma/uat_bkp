@isTest
public Class FinWise_Test{    
 
    @testSetup
    static void setupTestFinwiseData(){
       FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name='finwise',Finwise_Base_URL__c='https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/',Apply_State_Restriction__c=false,
                                      Finwise_LendingPartnerId__c='5',Finwise_LendingPartnerProductId__c='7',
                                      Finwise_LoanStatusId__c='1',finwise_partner_key__c='testKey',Finwise_product_Key__c='testkey',
                                      Decline_Loan_Status_Id__c = '7', Finwise_RateTypeId__c='1',Whitelisted_States__c='AL,MO',Investor__c = 'LendingPoint SPE 2');
       insert FinwiseParams;                                   
       //Finwise_Whitelisted_States obj= new Finwise_Whitelisted_States(Name='AL');                                 
       Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name='POS',Asset_Id__c=5,SFDC_Value__c=false);
       insert AsetCls;       
       
        LibraryTest.createBrmsConfigSettings(1,true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();    
        
        Account acc=new Account(Name='test',Is_Parent_Partner__c=true,Is_Child_Partner__c=false,type='Partner',Type2__c='Partner',Default_Fee_Handling__c='Fee On Top');
        insert acc;         
        
        
    }
    
    
    static testMethod void TestsubmitLoanInfoToFinwise(){ 
       Contact con=TestHelper.createContact();
       con.MailingState='AL';
       update con; 
       Account acc =[select id from Account limit 1];
       Opportunity ap =TestHelper.createApplicationFromContact(con);
       ap.Expected_First_Payment_Date__c=system.today();
       ap.Amount = 100;
       ap.AccountId = acc.id;
       ap.ProductName__c = 'Medical';
       ap.Use_of_Funds_Contact__c = 'POS';
        ap.KeyFactors__c = 'Test';
        ap.Name = 'APP-10101010';
       update ap;
       CreditReport.set(ap.id, LibraryTest.fakeBrmsRulesResponse(ap.Id, true, '1', true));

       Provider_Payments__c procPay=new Provider_Payments__c(Opportunity__c=ap.id,Payment_Amount__c=200,Procedure_Date__c=system.today(),Provider_Name__c=acc.id);
        insert procPay;
        loan__Loan_Product__c prod = [select id from loan__Loan_Product__c limit 1];
        prod.Name = 'Point Of Need';
        update prod;  
        Partner_Product_Bank_Account__c obj=new Partner_Product_Bank_Account__c (Account__c=acc.id,Account_Type__c='Saving',Bank_Account_Number__c='123456789',Deposit_to_Vendor__c=false,Routing_Number__c='123456789',Product__c=prod.id);
        insert obj;
       Blob response = Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(LibraryTest.fakeCreditReportData(ap.id))));

       LibraryTest.createAttachment(ap.id, response);

       Attachment atch = new Attachment(name='Signed Contract',body=blob.ValueOf('Testing'),contentType='text/plain',parentid=ap.id); 
       insert atch;    
       FinWise.submitLoanInfoToFinwise(ap.id, con.MailingState);           
    }
    
    static testMethod void TestsubmitLoanInfoToFinwise1(){ 
       Contact con=TestHelper.createContact();
       con.MailingState='AL';
       update con; 
       Opportunity ap =TestHelper.createApplicationFromContact(con);
       ap.ACH_Account_Number__c  = '123456789';
       ap.ACH_Routing_Number__c  = '123456789';
       ap.ACH_Bank_Name__c = 'American Test Bank';
       ap.Status__c = 'Declined (or Unqualified)';
       ap.Reason_of_Opportunity_Status__c = 'Cannot Confirm Information Provided';
       ap.Decline_Note__c = 'Cannot confirm Information';
       ap.AAN_Sent_Date__c = System.now();
        ap.Name = 'APP-10101010';
       ap.KeyFactors__c = 'Key factors';
       ap.ProductName__c = 'Medical';
       ap.Use_of_Funds_Contact__c = 'POS';
        ap.KeyFactors__c = 'Test';
       update ap;
       CreditReport.set(ap.id, LibraryTest.fakeBrmsRulesResponse(ap.Id, true, '1', true));
       Account acc =[select id from Account limit 1];
       Provider_Payments__c procPay=new Provider_Payments__c(Opportunity__c=ap.id,Payment_Amount__c=200,Procedure_Date__c=system.today(),Provider_Name__c=acc.id);
       insert procPay;
       loan__Loan_Product__c prod = [select id from loan__Loan_Product__c limit 1];
        prod.Name = 'Point Of Need';
        update prod;  
        Partner_Product_Bank_Account__c obj=new Partner_Product_Bank_Account__c (Account__c=acc.id,Account_Type__c='Saving',Bank_Account_Number__c='123456789',Deposit_to_Vendor__c=false,Routing_Number__c='123456789',Product__c=prod.id);
        insert obj;
       string appState = [select State__c from  Opportunity where id=:ap.id].State__c; 
       Attachment atch = new Attachment(name='Signed Contract',body=blob.ValueOf('Testing'),contentType='text/plain',parentid=ap.id); 
       insert atch;   
        
        Blob response2 = Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(LibraryTest.fakeCreditReportData(ap.id))));

       LibraryTest.createAttachment(ap.id, response2);

       
       IDology_Request__c ir = new IDology_Request__c(Contact__c = ap.Contact__c,Verification_Result__c = 'Pass');
       insert ir;
       
       logging__C ls = new logging__C(contact__c = ap.Contact__c, webservice__c = 'Kount', API_Response__c = 'AUTO=D');
       insert ls;
       
       FinWise.submitLoanInfoToFinwise(ap.id, con.MailingState);
        String response = '{'+
            '"Status":"Success",'+
        '}';
       FinWise.parseStatus(response);  
       FinWise.isFinwise(appState);  

       
    }
    static testMethod void TestsubmitLoanInfoToFinwise2(){ 
       Contact con=TestHelper.createContact();
       con.MailingState='AL';
       update con; 
       Opportunity ap =TestHelper.createApplicationFromContact(con);
       ap.ProductName__c = 'Medical';
       ap.Use_of_Funds_Contact__c = 'POS';
        ap.KeyFactors__c = 'Test';
        ap.Name = 'APP-10101010';

       update ap; 
       Blob response = Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(LibraryTest.fakeCreditReportData(ap.id))));
       CreditReport.set(ap.id, LibraryTest.fakeBrmsRulesResponse(ap.Id, true, '1', true));

       LibraryTest.createAttachment(ap.id, response);
       Account acc =[select id from Account limit 1];
       Provider_Payments__c procPay=new Provider_Payments__c(Opportunity__c=ap.id,Payment_Amount__c=200,Procedure_Date__c=system.today(),Provider_Name__c=acc.id);
       insert procPay;
       loan__Loan_Product__c prod = [select id from loan__Loan_Product__c limit 1];
        prod.Name = 'Point Of Need';
        update prod;  
        Partner_Product_Bank_Account__c obj=new Partner_Product_Bank_Account__c (Account__c=acc.id,Account_Type__c='Saving',Bank_Account_Number__c='123456789',Deposit_to_Vendor__c=false,Routing_Number__c='123456789',Product__c=prod.id);
        insert obj;
       //Opportunity ap = TestHelper.createApplication();
       string appState = [select State__c from  Opportunity where id=:ap.id].State__c; 
       Attachment atch = new Attachment(name='testt',body=blob.ValueOf('Testing'),contentType='text/plain',parentid=ap.id); 
       insert atch;    
       FinWise.submitLoanInfoToFinwise(ap.id, appState);           
    }
    static testMethod void TestgetLoanStatusFromFinwise(){    
       Opportunity apNew = TestHelper.createApplication();           
       FinWise.getLoanStatusFromFinwise(apNew.name);           
    }
    
    static testMethod void TestuploadFile(){    
       Opportunity ap1New = TestHelper.createApplication(); 
       Attachment atch = new Attachment(name='TestFile',body=blob.ValueOf('Testing'),contentType='text/plain',parentid=ap1New.id); 
       insert atch;
       
       Opportunity ap1NewUpdated = [select name,State__c,Expected_Start_Date__c,createdDate from  Opportunity where id=:ap1New.id];  
       ap1NewUpdated.Expected_Start_Date__c = system.today() + 10;
       ap1NewUpdated.Status__c = 'Credit Qualified';
                ap1NewUpdated.Name = 'APP-10101010';

        update ap1NewUpdated;
        Blob response = Blob.valueOf(EncodingUtil.base64Encode(Blob.valueOf(LibraryTest.fakeCreditReportData(ap1NewUpdated.id))));

       LibraryTest.createAttachment(ap1NewUpdated.id, response);
        
       FinWise.submitLoanInfoToFinwise(ap1New.id, ap1NewUpdated.State__c);  
       FinWise.uploadFile(ap1New.Id,ap1NewUpdated.name,ap1NewUpdated.Expected_Start_Date__c ,ap1NewUpdated.createdDate); 
                    
    }
    
}