global class CKPhoneAppendJob implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful {
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    public Integer totalRecsProcessed=0;
    string query = '', CREDIT_KARMA = 'CreditKarma_API', CREDIT_APPROVED_NO_CONTACT = 'Credit Approved - No Contact Allowed';
    Date d = Date.parse(Label.CkPhoneCutOffDate);
    
    global void execute (SchedulableContext sc) {
        CKPhoneAppendJob job = new CKPhoneAppendJob();
        System.debug('Starting CKPhoneAppendJob at ' + Date.today());
        Database.executeBatch(job);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        try {
           
            // Add TODAY LIMIT FOR Prod
            query = 'SELECT ID,Contact__r.SSN__c,Contact__c,status__c FROM Opportunity where Contact__c <> null AND Contact__r.Phone = null AND Contact__r.SSN__c <> null ';
            query += ' AND status__c = ' + '\'' + CREDIT_APPROVED_NO_CONTACT + '\' ' ; 
            query += ' AND Contact__r.Lead_Sub_Source__c = ' + '\'' + CREDIT_KARMA + '\' ' ;
            query += ' AND createdDate >: d' ;//+ '\'' + Date.parse(Label.CkPhoneCutOffDate) + '\' ' ;
            //query += 'order by createdDate desc limit 10 ';
            system.debug('---query--'+ query);
            totalRecsProcessed = 0;
            
        } catch(exception ex){
           System.debug('Exception:'+ ex.getMessage());
           WebToSFDC.notifyDev('CKPhoneAppendJob.QueryLocator ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
        }
        
        return Database.getQueryLocator(query);    
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope) {
        try { 
            list<Opportunity> theApps = (List<Opportunity>)scope;
            Map<Id,Opportunity> appsMap = new Map<Id,Opportunity>();
            Set<Id> conIds = new Set<Id>();
            Set<String> ssnList = new Set<String>();
            for (Opportunity app : theApps) {
                conIds.add(app.Contact__c);
                ssnList.add(app.Contact__r.SSN__c.right(4));
                appsMap.put(app.Contact__c,app);
            }
            CKPhoneAppend(conIds,ssnList,appsMap);

        } catch(exception ex){
            System.debug('Exception:'+ ex.getMessage());
            WebToSFDC.notifyDev('CKPhoneAppendJob.execute ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
        }
    }
    
    public void CKPhoneAppend(Set<Id> conIds,Set<String> ssnList,Map<Id,Opportunity> appsMap) {
         
         Map<String,Lead> ld = new Map<String,Lead>();
         List<Lead> leadsToUpdate = new List<Lead>();
         List<Contact> consToUpdate = new List<Contact>();
         List<Logging__c> logList = new List<Logging__c>();
         List<Opportunity> appsToUpdate = new List<Opportunity>();
         string body='Successfull records :';

         for(Lead l : [SELECT ID, Phone, SSN__c,Lead_sub_source__c,isConverted,CK_Phone_Processed__c FROM Lead 
                         WHERE Phone <> null and Lead_sub_source__c = :CREDIT_KARMA and isConverted = false 
                         and CK_Phone_Processed__c = false order by createddate asc]) {
             if(String.isnotEmpty(l.SSN__c)) {
                 String ssn = l.SSN__c;
                 ld.put(ssn.right(4),l);
             }
         }
         system.debug('---ld--'+ld);
         for(Contact con :  [SELECT ID, Phone, SSN__c
                     FROM Contact WHERE Id in : conIds]) {
             String ssn = con.SSN__c;
             String ssn4 = ssn.right(4);
             if(ld.containskey(ssn4) && appsMap.containskey(con.Id)) {
             
                 Lead l = ld.get(ssn4);
                 l.CK_Phone_Processed__c = true; //mark the lead as CK processed
                 leadsToUpdate.add(l);
                 
                 con.Phone = l.phone; //append the phone on contact
                 consToUpdate.add(con);
                 
                 Opportunity app = appsMap.get(con.Id);
                 app.status__c = 'Credit Qualified'; //set the status to CQ
                 appsToUpdate.add(app);
                 
                 DealAutomaticAssignmentManager.assignExistingAppFromWeb(app.Id, false); //send app for queue assignment
                 body = body +  ' AppId :' +app.id+ ', ContactId : ' + con.id +' , ' ;
                 totalRecsProcessed++;
             }
         }
         
         try{
             if(totalRecsProcessed > 0) logList.add(ParseEBankingReportBatch.getLogging('CK Phone Append Job',null,'CK Phone Append',body ));
             if(consToUpdate.size() > 0) update consToUpdate;
             if(appsToUpdate.size() > 0) update appsToUpdate;
             if(leadsToUpdate.size() > 0) update leadsToUpdate;
         } catch(exception e) {
             logList.add(ParseEBankingReportBatch.getLogging('CK Phone Append Job Error',null,'CK Phone Append',e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString()));
             WebToSFDC.notifyDev('CKPhoneAppendJob Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString(), UserInfo.getUserId() ) ;
         }
         finally{
           if(logList!=null) insert logList;  
         }                
    }
    
    global void finish(Database.BatchableContext BC){
        //WebToSFDC.notifyDev('CKPhoneAppendJob Finished', 'Records processed = ' + totalRecsProcessed, UserInfo.getUserId() );
        //WebToSFDC.notifyDev('CKPhoneAppendJob Finished', 'CK Phone Append Job Finished', UserInfo.getUserId() );
        System.debug('CKPhoneAppendJob Finished!');
    }
     /*Webservice static String executeJobFile() {
        CKPhoneAppendJob j = new CKPhoneAppendJob();
        Database.execute(j);
        return 'Started process. Please check the return report after 10 minutes!';
    }*/
     /*Webservice static void executeReturnFile() {
     CKPhoneAppendJob exeClass = new CKPhoneAppendJob();
     //'0 10 * * * * *';
     //String cronStr = '0 10 * * * ? 2018';
     System.schedule('CK Phone Append Job 1', '0 0 * * * ?', new CKPhoneAppendJob());
     System.schedule('CK Phone Append Job 2', '0 10 * * * ?', new CKPhoneAppendJob());
     System.schedule('CK Phone Append Job 3', '0 20 * * * ?', new CKPhoneAppendJob());
     System.schedule('CK Phone Append Job 4', '0 30 * * * ?', new CKPhoneAppendJob());
     System.schedule('CK Phone Append Job 5', '0 40 * * * ?', new CKPhoneAppendJob());
     System.schedule('CK Phone Append Job 6', '0 50 * * * ?', new CKPhoneAppendJob());
     //System.schedule('CK Phone Append Job', cronStr, exeClass);
     system.debug('Started process. Please check the result after 10 minutes!');
    }*/
}