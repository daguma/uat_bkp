@isTest public class CLServicerActionsCtrlTest {
    
    @testSetup static void initializes_objects() {
        
        Securitization_Segment__c segment = New Securitization_Segment__c();
        segment.Name = 'Segment 1' ;
        segment.Fico_Max__c = 1000;
        segment.Fico_Min__c = 600;
        segment.DTI_Min__c = 0;
        segment.DTI_Max__c = 30;
        insert segment;

        
        Securitization_Bucket__c bucked = New Securitization_Bucket__c();
        bucked.Name = '1-30';
        bucked.Min__c = -1;
        bucked.Max__c = 30;
        insert bucked;
        
        Securitization_Action__c Setting = new Securitization_Action__c();
        Setting.Name = 'Payment Plan';
        Setting.Strategy_Ops__c = '3 months (under $500 Payment) 6 months payment $501 + Test';
        Setting.Manager_Approval__c = false;
        Setting.SegmentId__c = segment.Id;
        Setting.BucketId__c = bucked.Id;
        Setting.IsActive__c = true;
        insert Setting;

    }
    
    @isTest Static void Validate_CLServicerActions_Data(){
        
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.Originated_FICO__c = 600;
        cnt.Originated_DTI__c = 23;
        cnt.loan__Oldest_Due_Date__c = Date.Today().adddays(-10);
        cnt.loan__Loan_Status__c = 'Active - Bad Standing';
        upsert cnt;
        
        loan__loan_Account__c contract = [SELECT Id, Name, loan__Loan_Status__c, loan__Oldest_Due_Date__c, loan__Contact__c, Opportunity__c, Originated_FICO__c, Originated_DTI__c, Overdue_Days__c FROM loan__Loan_Account__c where Id =: cnt.Id];
        
        Test.startTest();
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        CLServicerActionsCtrl Alert = New CLServicerActionsCtrl(controller);
        Test.stopTest();
        
    }
    
    @isTest Static void Validate_CLServicerActions_Data_AM(){
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.Originated_FICO__c = 650;
        cnt.Originated_DTI__c = 23;
        cnt.loan__Loan_Status__c = 'Active - Bad Standing';
        cnt.loan__Oldest_Due_Date__c = Date.Today().adddays(-10);
        update cnt;
        
        AccountManagementHistory__c accM = New AccountManagementHistory__c();
        accM.Opportunity__c = cnt.Opportunity__c;
        //accM.CreditReportId__c = fundedApp.PrimaryCreditReportId__c;
        accM.FicoValue__c = 650;
        accM.DTIValue__c = 28;
        accM.Contract__c = cnt.Id;
        insert accM;

        loan__loan_Account__c contract = [SELECT Id, Name, loan__Contact__c, loan__Oldest_Due_Date__c, Opportunity__c, Originated_FICO__c, Originated_DTI__c, Overdue_Days__c FROM loan__Loan_Account__c where Id =: cnt.Id];
        
        Test.startTest();
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        CLServicerActionsCtrl Alert = New CLServicerActionsCtrl(controller);
        Alert.requestApproval();
        Alert.overrideApproval();
        Test.stopTest();
        
    }

    

}