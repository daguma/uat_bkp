global class CreditDecisionNotificationJob  implements Database.Batchable<sObject>, Schedulable, Database.Stateful  {
  Global String query;
    Global List<Opportunity> appList = new List<Opportunity>();
    global Integer count = 0, withError = 0 , ActuallySent = 0;

    public void execute(SchedulableContext sc) {
        CreditDecisionNotificationJob b = new CreditDecisionNotificationJob();
        database.executebatch(b, 15);
    }

    global CreditDecisionNotificationJob() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        query = 'select id, CreatedDate, Fico__C from Opportunity ' +
                ' where CreatedDate >= 2018-10-23T00:00:00Z And  ' +
                ' RBP_Sent_Date__c = null and status__c <> \'Declined (or Unqualified)\' and' +  //OF-356
                ' Name like \'APP%\' and ' +
                ' FICO__C <> NULL and ' +
                ' ( FICO__c LIKE \'___\' OR FICO__c LIKE \'0___\') AND ' +
                ' Contact__r.Email <> null and ' + 
                ' Contact__r.EmailBouncedDate = null and ' + 
                ' Type = \'New\' AND   ' +
                ' (NOT FICO__c like \'9%\') '+
                ' Order by CreatedDate asc ';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        Set<id> cons = new set<Id>();
        for (Opportunity CurrentApplication : scope) {
            try {
                Map<String, Object> params = new Map<String, Object>();
                
                params.put('appId', CurrentApplication.Id);
                Flow.Interview.Risk_Based_Price_Notification RBP_Notification = new Flow.Interview.Risk_Based_Price_Notification(params);
                RBP_Notification.start();
                count++;
                cons.add(CurrentApplication.Id);
            } catch (Exception e) {
                withError++;
            }
        }
        ActuallySent += [select Id from Opportunity where RBP_Sent_Date__c <> NULL AND Id in : cons].size();
    }

    global void finish(Database.BatchableContext BC) {
        WebToSFDC.notifyDev('RBP Notices Batch Completed', 'RBP Notices batch completed for Today. Total processed : ' + count + '\nWith Error = ' + withError + '\nActually Sent = ' + ActuallySent);        

    }
}