public with sharing class CreditReportDetailsCtrl {
    
    @AuraEnabled
    public static CreditReportDetailEntity getCreditReportDetail(String entityId, String reportId, Boolean viewattach) {
        Integer crId = Integer.valueOf(reportId);
        CreditReportDetailEntity crDetailEntity = new CreditReportDetailEntity();
        Boolean processCreditReport = false;
        if(!String.isEmpty(entityId)) {
            
            if(entityId.startsWith('003')) {
                try {
                	Contact currentContact = [SELECT Id, Name
                                       FROM Contact
                                       WHERE Id = : entityId];
                	processCreditReport = (crId > 0 && currentContact != null && currentContact.id != null);
                }catch(Exception e) {
                    crDetailEntity.setErrorMessage(e.getMessage());
                }
            }
            
            if(entityId.startsWith('006')) {
                try {
                	Opportunity currentApp = [SELECT Id, Name, Contact__c, Contact__r.name
                                   FROM Opportunity
                                   WHERE Id = : entityId];
                	processCreditReport = (crId > 0 && currentApp != null && currentApp.Id != null);
                }catch(Exception e) {
                    crDetailEntity.setErrorMessage(e.getMessage());
                }
            }
            
            if (processCreditReport) {
				ProxyApiCreditReportEntity cr = ProxyApiCreditReport.getCreditReportById(crId, entityId);
                if (cr != null) {
                    if ( !cr.errorResponse) {
                        String bureau = cr.datasourceName.startsWith('Tu') ? 'TU' : 'EX';
                        crDetailEntity.crSegmentsList = cr.segmentsList;
                        crDetailEntity.crScores = CreditReportSegmentsUtil.extractScores(crDetailEntity.crSegmentsList, bureau);
                    }else if ( cr.errorResponse) {
                        crDetailEntity.setErrorMessage(cr.errorMessage);
                    }  
                    crDetailEntity.crDetailsList = CreditReportSegmentsUtil.extractMainInformation(cr);
                    crDetailEntity.crAttributesList = cr.getCriteriaAttributeList();
                    crDetailEntity.crOtherAttributeList = cr.getOtherAttributeList();
                    crDetailEntity.CreditReportText = cr.reportText;
                } else {
                    crDetailEntity.setErrorMessage('Error occurred while pulling Credit Report.');
                }
            }else {
                crDetailEntity.setErrorMessage('Credit report not found.');
            }
                
        }else {
            crDetailEntity.setErrorMessage('No entity ID found.');
        }        
        return crDetailEntity;       
    }
    
    public class CreditReportDetailEntity  {
        @AuraEnabled public List<ProxyApiCreditReportAttributeEntity> crAttributesList {get; set;}
        @AuraEnabled public List<ProxyApiCreditReportAttributeEntity> crOtherAttributeList {get; set;}
        @AuraEnabled public Transient List<ProxyApiCreditReportSegmentEntity> crSegmentsList {get; set;}
        @AuraEnabled public List<CreditReportSegmentsUtil.PairValues> crDetailsList {get; set;}
        @AuraEnabled public CreditReportSegmentsUtil.Segment crLiabilities {get; set;}
        @AuraEnabled public CreditReportSegmentsUtil.Segment crScores {get; set;}
        @AuraEnabled public CreditReportSegmentsUtil.Segment crInquiries {get; set;}
        @AuraEnabled public CreditReportSegmentsUtil.Segment crSummaries {get; set;}
        @AuraEnabled public String CreditReportText {get; set;}
        @AuraEnabled public String errorMessage {get; set;}
        @AuraEnabled public Boolean hasError {get; set;}
        
        public CreditReportDetailEntity() {
            //
        }
        
        public void setErrorMessage(String message) {
            this.hasError = true;
            this.errorMessage = message;
        }
   	}
}