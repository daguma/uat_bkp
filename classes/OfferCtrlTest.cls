@isTest public class OfferCtrlTest {
    @testSetup static void setup_test_data() {
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
                Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7',
                Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
                Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO', Enable_Finwise__c = false);
        insert FinwiseParams;
        
        AON_Fee__c aonFee = new AON_Fee__c();
        aonFee.Fee__c = 6.31;
        aonFee.Name = 'AON Fee';
        insert aonFee;

        if ([select Id from Idology_Settings__c].size() == 0) {
            Idology_Settings__c ido = new Idology_Settings__c();
            ido.Password__c = 'test';
            ido.Username__c = 'te';
            ido.ciq_Password__c = 'test';
            ido.ciq_Username__c = 'te';
            ido.name = 'sgget';
            insert ido;
        }

        Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name = 'POS', Asset_Id__c = 5, SFDC_Value__c = false);
        insert AsetCls;

        LibraryTest.createPayfoneSettings();
    }

    @isTest static void get_UW_OfferEntityTest() {

        Opportunity opp = LibraryTest.createApplicationTH();

        CreditReport.set(opp.Id, LibraryTest.fakeCreditReportDataWithApp(opp.Id));


        Offer__c off = new Offer__c();
        off.Opportunity__c = opp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        insert off;

        //OfferCtrl.UnderWriterOfferEntity ofrctrl1 = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        //String entityString = JSON.serialize(ofrctrl1);
        OfferCtrl.getOffers(opp.Id);

        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 24;
        offcust.APR__c = 0.25;
        offcust.Loan_Amount__c = 5000.0;
        offcust.Installments__c = 24;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = true;


        OfferCtrl.InsertCustOffer(offcust, opp.id);

    }
    @isTest static void get_UW_OfferEntityTest1() {

        Opportunity opp = LibraryTest.createApplicationTH();

        CreditReport.set(opp.Id, LibraryTest.fakeCreditReportDataWithApp(opp.Id));

        Payfone_Run_History__c payfon = new Payfone_Run_History__c();
        payfon.contact__c = opp.contact__c;
        payfon.Verification_Result__c = 'Pass';
        insert payfon;

        Idology_Request__c ido = new Idology_Request__c();
        ido.Contact__c = opp.contact__c;
        ido.Verification_Result__c = 'Pass';
        insert ido;

        Offer__c off = new Offer__c();
        off.Opportunity__c = opp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        insert off;

        OfferCtrl.UnderWriterOfferEntity ofrctrl1 = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        String entityString = JSON.serialize(ofrctrl1);
        OfferCtrl.getUnderWriterOfferEntity(opp.Id);
        OfferCtrl.loadIncPkgReasons(opp.Id);
        OfferCtrl.deleteOffer(off, entityString);
        OfferCtrl.loadOffers(opp.Id);
        OfferCtrl.getOffers(opp.Id);
    }

    @isTest static void kyc_test_method() {

        Opportunity opp = LibraryTest.createApplicationTH();

        OfferCtrl.UnderWriterOfferEntity ofrctrl = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        OfferCtrl.getPayfoneSettings();
        ofrctrl.tokenRequest();
        ofrctrl.recoverQuestions();
        ofrctrl.ProcessResultXml(LibraryTest.GenerateTestingData1());
        ofrctrl.ProcessAnswersResultXml(LibraryTest.GenerateTestingData3());
        ofrctrl.processQuestions();

        Idology_Request__c ido = new Idology_Request__c();
        ido.Contact__c = opp.contact__c;
        ido.Verification_Result_Front__c = 'Fail';
        insert ido;

        ofrctrl.getKYCPassBy();
    }

    @isTest static void kyc_test_method_One() {

        Opportunity opp = LibraryTest.createApplicationTH();

        Payfone_Run_History__c payfoneTrack = new Payfone_Run_History__c();
        payfoneTrack.Contact__c   = opp.contact__c;
        payfoneTrack.Phone_Number__c = '9897868632';
        payfoneTrack.Result_Message__c = 'pass';
        payfoneTrack.Verification_Result__c = 'pass';
        payfoneTrack.First_Name_Score__c =  100;
        payfoneTrack.Last_Name_Score__c =  100;
        payfoneTrack.Middle_Name_Score__c =  100;
        payfoneTrack.Street_Number_Score__c =  100;
        payfoneTrack.Verify_API_Response__c = 'success';
        payfoneTrack.Trust_Score__c = 1000;
        payfoneTrack.Address_Score__c = 100;
        insert payfoneTrack;

        OfferCtrl.UnderWriterOfferEntity ofrctrl = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        OfferCtrl.getPayfoneSettings();
        ofrctrl.tokenRequest();
        ofrctrl.recoverQuestions();
        ofrctrl.callNewEnterprise = true;
        ofrctrl.ProcessResultXml(LibraryTest.GenerateTestingData1());
        ofrctrl.ProcessAnswersResultXml(LibraryTest.GenerateTestingData3());
        ofrctrl.processQuestions();

        Idology_Request__c ido = new Idology_Request__c();
        ido.Contact__c = opp.contact__c;
        ido.Verification_Result_Front__c = 'Fail';
        insert ido;

        ofrctrl.getKYCPassBy();
    }

    @isTest static void test_method_one() {
        
        RecordType rType = [Select Id, SObjectType, DeveloperName  from RecordType where SObjectType = 'Opportunity' and DeveloperName = 'LP' LIMIT 1];
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.RecordTypeId = rType.Id;
        
        Account acc = New Account();
        acc.Name = 'Test';
        acc.Cobranding_Name__c = 'Test Name Test';
        insert acc;

        contact con = [SELECT Firstname, AccountId, Name, Lastname, Email, BirthDate, Point_Code__c, Employment_Duration__c, Annual_Income__c, Loan_Amount__c, Employment_Start_date__c,
                       ints__Social_security_Number__c, Phone, Mailingstreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, createddate, Employment_Type__c,
                       Lead_Sub_Source__c FROM Contact WHERE Id = : opp.contact__c LIMIT 1];
        con.MailingState = 'OR';
        con.AccountId = acc.Id;
        con.Promotion_Active__c = true;
        con.Promotion_Grace_Days__c = 1.0;
        con.Promotion_Term_In_Months__c = 20;
        update con;

        Offer__c off = new Offer__c();
        off.Opportunity__c = opp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        insert off;

        OfferCtrl.UnderWriterOfferEntity ofrctrl = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        ofrctrl.initialize(True);
        OfferCtrl.validatePreviousOfferSelectedForOregon(off.Id, opp);
        OfferCtrl.validatePayrollDate(opp);

        datetime d = system.now().adddays(-2);
        opp.Next_Payroll_Date__c = date.valueof(d);
        opp.Payroll_Frequency__c = 'Monthly';
        opp.Contact__c = con.Id;
        update opp;

        OfferCtrl.validatePayrollDate(opp);
        OfferCtrl.doCalculationStartDate(opp, null);

        String entityString = JSON.serialize(ofrctrl);
        system.debug('---entityString---' + entityString);

        OfferCtrl.payfoneRequest(entityString);
        OfferCtrl.recoverQuestions(entityString);
        OfferCtrl.changePrincipal(entityString);
        OfferCtrl.changePayment(entityString);
       // OfferCtrl.reGenerateOffers(entityString);
        OfferCtrl.saveApp(entityString);
        OfferCtrl.callFinwise(opp.Id, '');
        OfferCtrl.getselectOptions(opp, 'Type');
        //OfferCtrl.rePaymentFrequencyGeneration(json.serialize(ofrctrl));
    }
    
    @isTest static void test_payfone_request_with_mobile_number() {

        Opportunity opp = LibraryTest.createApplicationTH();

        contact con = [SELECT Firstname, Name, MobilePhone, Lastname, Email, BirthDate, Point_Code__c, Employment_Duration__c, Annual_Income__c, Loan_Amount__c, Employment_Start_date__c,
                       ints__Social_security_Number__c, Phone, Mailingstreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, createddate, Employment_Type__c,
                       Lead_Sub_Source__c FROM Contact WHERE Id = : opp.contact__c LIMIT 1];
        con.MailingState = 'OR';
        con.MobilePhone = '5555555555'; 
        update con;

        Offer__c off = new Offer__c();
        off.Opportunity__c = opp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        insert off;

        OfferCtrl.UnderWriterOfferEntity ofrctrl = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        ofrctrl.initialize(true);
        
        String entityString = JSON.serialize(ofrctrl);
        
        OfferCtrl.payfoneRequest(entityString);
    }

    @isTest static void calculate_pti_test() {

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Amount__c = 1245;
        opp.Payment_Frequency_Multiplier__c = 4;
        opp.Payment_Frequency__c = 'Monthly';
        update opp;

        OfferCtrl.UnderWriterOfferEntity ofrctrl = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        ofrctrl.CalculatePTI();
    }

    @isTest static void offer_case_test_low_and_grow_2() {

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Amount__c = 1245;
        opp.Payment_Frequency_Multiplier__c = 4;
        opp.Payment_Frequency__c = 'MONTHLY';
        opp.StrategyType__c = '2';
        update opp;

        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 24;
        offcust.APR__c = 0.25;
        offcust.Loan_Amount__c = 5000;
        offcust.Installments__c = 24;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.isCustom__c = true;

        OfferCtrl.InsertCustOffer(offcust, opp.id);
        OfferCtrl.UnderWriterOfferEntity ofrctrl1 = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        String entityString = JSON.serialize(ofrctrl1);

        OfferCtrl.CalcPaymentAmt(offcust);

    }
    
    @isTest static void offer_case_test_low_and_grow_3() {

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Amount__c = 1245;
        opp.Payment_Frequency_Multiplier__c = 4;
        opp.Payment_Frequency__c = 'MONTHLY';
        opp.StrategyType__c = '3';
        update opp;

        Offer__c offcust1 = new Offer__c();
        offcust1.Opportunity__c = opp.id;
        offcust1.Fee__c = 50.0;
        offcust1.IsSelected__c = false;
        offcust1.Repayment_Frequency__c = 'Monthly';
        offcust1.Term__c = 24;
        offcust1.APR__c = 0.25;
        offcust1.Loan_Amount__c = 2100;
        offcust1.Installments__c = 24;
        offcust1.Payment_Amount__c = 200;
        offcust1.Effective_APR__c = 0.2;
        offcust1.NeedsApproval__c = false;
        offcust1.iscustom__c = false;
        insert offcust1;

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'A2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;

        OfferCtrl.InsertCustOffer(offcust1, opp.id);

    }
    
    @isTest static void dual_list_box(){
        OfferCtrl.DualListboxItem dualList = new OfferCtrl.DualListboxItem('TestLabel', 'TestValue');
    }

    @isTest static void custom_exception_data_wrapper() {
        OfferCtrl.CustomExceptionData c = new OfferCtrl.CustomExceptionData();
        c.setErrorMessage('test');
    }

    @isTest static void offer_select_test() {

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Amount__c = 1245;
        opp.Payment_Frequency_Multiplier__c = 4;
        opp.Payment_Frequency__c = 'MONTHLY';
        opp.StrategyType__c = '2';
        update opp;

        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 24;
        offcust.APR__c = 0.25;
        offcust.Loan_Amount__c = 5000;
        offcust.Installments__c = 24;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = true;

        OfferCtrl.InsertCustOffer(offcust, opp.id);

        opp.StrategyType__c = '3';
        update opp;

        Offer__c offcust1 = new Offer__c();
        offcust1.Opportunity__c = opp.id;
        offcust1.Fee__c = 50.0;
        offcust1.IsSelected__c = false;
        offcust1.Repayment_Frequency__c = 'Monthly';
        offcust1.Term__c = 24;
        offcust1.APR__c = 0.25;
        offcust1.Loan_Amount__c = 2100;
        offcust1.Installments__c = 24;
        offcust1.Payment_Amount__c = 200;
        offcust1.Effective_APR__c = 0.2;
        offcust1.NeedsApproval__c = false;
        offcust1.iscustom__c = false;
        offcust1.Preferred_Payment_Date_String__c = '2018-06-30';
        insert offcust1;

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'a2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;

        OfferCtrl.UnderWriterOfferEntity ofrctrl1 = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        String entityString = JSON.serialize(ofrctrl1);

        OfferCtrl.selectOffer(offcust1, entityString);

    }

    @isTest static void offer_deselect_test() {

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Amount__c = 1245;
        opp.Payment_Frequency_Multiplier__c = 4;
        opp.Payment_Frequency__c = 'MONTHLY';
        opp.StrategyType__c = '2';
        update opp;

        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 24;
        offcust.APR__c = 0.25;
        offcust.Loan_Amount__c = 5000;
        offcust.Installments__c = 24;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = true;

        OfferCtrl.InsertCustOffer(offcust, opp.id);

        opp.StrategyType__c = '3';
        update opp;

        Offer__c offcust1 = new Offer__c();
        offcust1.Opportunity__c = opp.id;
        offcust1.Fee__c = 50.0;
        offcust1.IsSelected__c = true;
        offcust1.Repayment_Frequency__c = 'Monthly';
        offcust1.Term__c = 24;
        offcust1.APR__c = 0.25;
        offcust1.Loan_Amount__c = 2100;
        offcust1.Installments__c = 24;
        offcust1.Payment_Amount__c = 200;
        offcust1.Effective_APR__c = 0.2;
        offcust1.NeedsApproval__c = false;
        offcust1.iscustom__c = false;
        insert offcust1;

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'a2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;

        OfferCtrl.UnderWriterOfferEntity ofrctrl1 = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        String entityString = JSON.serialize(ofrctrl1);

        OfferCtrl.deselectOffer(offcust1, entityString);

    }

    @isTest static void getGrade_test() {
        
        contact con = LibraryTest.createContactTH();

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Contact__c = con.Id;
        opp.Payment_Amount__c = 1245;
        opp.Payment_Frequency_Multiplier__c = 4;
        opp.Payment_Frequency__c = 'MONTHLY';
        opp.StrategyType__c = '2';
        opp.Term__c  = 24;
        opp.Status__c = 'Credit Qualified';
        opp.KYC_Completed__c = true;
        update opp;
        
        OpportunityFieldHistory oppfieldHistory = New OpportunityFieldHistory();
        oppfieldHistory.OpportunityId = opp.Id;
        oppfieldHistory.Field = 'Status__c';
        upsert oppfieldHistory;
        /*
        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Contact__c = con.Id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 24;
        offcust.APR__c = 0.25;
        offcust.Loan_Amount__c = 5000;
        offcust.Installments__c = 24;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = true; */

        //OfferCtrl.InsertCustOffer(offcust, opp.id);

        opp.StrategyType__c = '3';
        update opp;

        Offer__c offcust1 = new Offer__c();
        offcust1.Opportunity__c = opp.id;
        offcust1.Contact__c = con.Id;
        offcust1.Fee__c = 50.0;
        offcust1.IsSelected__c = true;
        offcust1.Repayment_Frequency__c = 'Monthly';
        offcust1.Term__c = 24;
        offcust1.APR__c = 0.25;
        offcust1.Loan_Amount__c = 2100;
        offcust1.Installments__c = 24;
        offcust1.Payment_Amount__c = 200;
        offcust1.Effective_APR__c = 0.2;
        offcust1.NeedsApproval__c = false;
        offcust1.iscustom__c = false;
        insert offcust1;

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'A2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;

        OfferCtrl.UnderWriterOfferEntity ofrctrl1 = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        String entityString = JSON.serialize(ofrctrl1);

        OfferCtrl.GetGrade(entityString);

        OfferCtrl.processQuestions(entityString);

    }

    @isTest static void offer_case_testing() {
        
        contact con = LibraryTest.createContactTH();


        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Status__c = 'Credit Qualified';
        opp.Contact__c = con.Id;
        opp.Payment_Amount__c = 1245;
        opp.Payment_Frequency_Multiplier__c = 4;
        opp.Payment_Frequency__c = 'MONTHLY';
        opp.StrategyType__c = '2';
        update opp;

        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.Id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 36;
        offcust.APR__c = 0.2825;
        offcust.Loan_Amount__c = 2300;
        offcust.Installments__c = 36;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = true;

        //OfferCtrl.InsertCustOffer(offcust, opp.id);

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'a2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;

        OfferCtrl.InsertCustOffer(offcust, opp.id);
        OfferCtrl.UnderWriterOfferEntity ofrctrl1 = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        String entityString = JSON.serialize(ofrctrl1);

        //OfferCtrl.CalcPaymentAmt(offcust1);
        OfferCtrl.DualListboxItem dualList = new OfferCtrl.DualListboxItem('TestLabel', 'TestValue');

    }

    @isTest static void offer_case_testing2() {

        Lending_Product__c lp = New Lending_Product__c();
        lp.Name = 'Medical';
        upsert lp;

        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Payment_Amount__c = 1245;
        opp.Payment_Frequency_Multiplier__c = 4;
        opp.Payment_Frequency__c = 'MONTHLY';
        opp.StrategyType__c = '2';
        opp.Lending_Product__c = lp.Id ;
        update opp;

        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 36;
        offcust.APR__c = 0.2825;
        offcust.Loan_Amount__c = 450;
        offcust.Installments__c = 36;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = true;

        //OfferCtrl.InsertCustOffer(offcust, opp.id);

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'a2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;

        OfferCtrl.InsertCustOffer(offcust, opp.id);
        OfferCtrl.UnderWriterOfferEntity ofrctrl1 = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        String entityString = JSON.serialize(ofrctrl1);

        OfferCtrl.DualListboxItem dualList = new OfferCtrl.DualListboxItem('TestLabel', 'TestValue');

    }

    @isTest static void fills_military_multi_select(){
        Opportunity opp = LibraryTest.createApplicationTH();
        OfferCtrlUtil.DualListboxEntity result = OfferCtrl.loadAonMilitaryOptions(opp.Id);
        System.assertNotEquals(null, result);
    }
    
    @isTest static void validates_refinance_for_oregon(){
        Opportunity opp = createRefinanceApp();
        Contact c = [SELECT Id, MailingState FROM Contact WHERE Id =: opp.Contact__c LIMIT 1];
        c.MailingState = 'OR';
        update c;
        opp = [SELECT Id, Contact__r.MailingState, Contract_Renewed__c FROM Opportunity WHERE Id =: opp.Id LIMIT 1];
        Offer__c newOffer = [SELECT Id, Opportunity__c, APR__c FROM Offer__c WHERE Opportunity__c = : opp.Id LIMIT 1];
        Boolean result = OfferCtrl.validatePreviousOfferSelectedForOregon(newOffer.Id, opp);
        System.assertEquals(true,result);
        
        OfferCtrl.UnderWriterOfferEntity uroEntity = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        
        List<String> resultList = uroEntity.infoMsgs;
        
        Boolean hasPayoff = false;
        
        for(String res : resultList){
            if(res.containsIgnoreCase('The pay-off amount for the existing contract is $')){
                hasPayoff = true;
                break;
            }
        }
        
        System.assert(hasPayoff, 'Current payoff of application');
    } 
    
    @isTest static void is_aon_bundled(){
        Opportunity opp = LibraryTest.createApplicationTH();
        
        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 36;
        offcust.APR__c = 0.2825;
        offcust.Loan_Amount__c = 450;
        offcust.Installments__c = 36;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = false;
        insert offcust;

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'A2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;
        
        OfferCtrl.UnderWriterOfferEntity uroEntity = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        
        try{
          Boolean result = OfferCtrl.isAonBundle(json.serialize(uroEntity));
            System.assertEquals(false, result);
        }
        catch(Exception e){}
    }
    
    @isTest static void is_aon_bundled_catch_exception(){
        
        String underWriterEntity = 'abc';
        
        try{
          Boolean result = OfferCtrl.isAonBundle(underWriterEntity);
        }
        catch(Exception e){}
    }
    
    @isTest static void updates_military_options(){
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Preferred_Payment_Date__c = Date.today().addDays(2);
        update opp;
        
        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 36;
        offcust.APR__c = 0.2825;
        offcust.Loan_Amount__c = 450;
        offcust.Installments__c = 36;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = false;
        insert offcust;

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'A2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;
        
        OfferCtrl.UnderWriterOfferEntity uroEntity = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        
        OfferCtrl.updateAonMilitaryOptions(json.serialize(uroEntity));
    }
    
    @isTest static void updates_military_options_catch_exception(){
        
        String underWriterEntity = 'abc';
        try{
          OfferCtrl.updateAonMilitaryOptions(underWriterEntity);
        }
        catch(Exception e){}
    }
    
    @isTest static void validates_aon_optional_states(){
        
        AON_Configurable_Settings__c aonSettings = new AON_Configurable_Settings__c(name='Settings',Excluded_States__c='CO');
        insert aonSettings;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        Contact c = [SELECT Id, Name, MailingState FROM Contact WHERE Id =: opp.Contact__c LIMIT 1];
        c.MailingState = 'CO';
        update c;
        
        OfferCtrl.UnderWriterOfferEntity uwEntity = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        Boolean result = uwEntity.validateAONOptionalStates();
        System.assertEquals(false, result);
    }
    
    @isTest static void is_aon_enrollment_disabled(){
        
        AON_Configurable_Settings__c aonSettings = new AON_Configurable_Settings__c(name='Settings',Excluded_States__c='CO');
        insert aonSettings;
        
        Opportunity opp = LibraryTest.createApplicationTH();
        Contact c = [SELECT Id, Name, MailingState FROM Contact WHERE Id =: opp.Contact__c LIMIT 1];
        c.MailingState = 'CO';
        update c;
        
        OfferCtrl.UnderWriterOfferEntity uwEntity = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        Boolean result = OfferCtrl.isDisableAONOptionalProduct(JSON.serialize(uwEntity));
        System.assertEquals(true, result);
    }
    
    @isTest static void loads_offers(){
        Opportunity opp = LibraryTest.createApplicationTH();
        
        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 36;
        offcust.APR__c = 0.2822;
        offcust.Loan_Amount__c = 450;
        offcust.Installments__c = 36;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = false;
        insert offcust;
        
        OfferCtrl.showOfferEntity getOffers = OfferCtrl.loadOffers(opp.Id);

    }
    
    @isTest static void gets_offers_exception(){
        Opportunity opp = LibraryTest.createApplicationTH();
        List<Offer__c> offers = OfferCtrl.getOffers('abc');
        System.assertEquals(null, offers);
    }
    
    @isTest static void inserts_custom_offer_opp_exceptions(){
        Opportunity opp = LibraryTest.createApplicationTH();
        
        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 36;
        offcust.APR__c = null;
        offcust.Loan_Amount__c = 450;
        offcust.Installments__c = 36;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = false;

        String result = OfferCtrl.InsertCustOffer(offcust, opp.Id);
        
        System.assertEquals('ERROR: Please enter APR, Loan Amount and Term before generating Offer',result);
        
        opp.Status__c = 'Declined (or Unqualified)';
        update opp;
        
        offcust.APR__c = 28.53;
        
        result = OfferCtrl.InsertCustOffer(offcust, opp.Id);

        System.assertEquals('ERROR: Offers could not be generated',result);
    }
    
    @isTest static void changes_repayment_frequency_offers(){
        
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.RepaymentFrequency__c = '28 Days';
        update opp;
        
        Offer__c offcust = new Offer__c();
        offcust.Opportunity__c = opp.id;
        offcust.Fee__c = 50.0;
        offcust.IsSelected__c = true;
        offcust.Repayment_Frequency__c = 'Monthly';
        offcust.Term__c = 36;
        offcust.APR__c = 0.2825;
        offcust.Loan_Amount__c = 450;
        offcust.Installments__c = 36;
        offcust.Payment_Amount__c = 200;
        offcust.Effective_APR__c = 0.2;
        offcust.NeedsApproval__c = true;
        offcust.iscustom__c = false;
        insert offcust;

        Scorecard__c score = new Scorecard__C();
        score.Opportunity__c = opp.Id;
        score.Contact__c = opp.Contact__c;
        score.Grade__c = 'A2';
        score.Score__c = 712;
        score.Acceptance__c = 'Y';
        insert score;
        
        OfferCtrl.UnderWriterOfferEntity uroEntity = new OfferCtrl.UnderWriterOfferEntity(opp.Id);
        
        try{
            OfferCtrl.UnderWriterOfferEntity res = OfferCtrl.rePaymentFrequencyGeneration(json.serialize(uroEntity));
        }
        catch(Exception e){}
        
    }
    
    private static Opportunity createRefinanceApp() {
        loan__Loan_Account__c testContract = LibraryTest.createContractTH();

        Opportunity app = [SELECT Id, is_Finwise__c, Status__c, Contact__c, Contact__r.Loan_Amount__c, Type, FICO__c FROM Opportunity LIMIT 1];

        app.FICO__c = '720';
        app.Type = 'New';
        app.Status__c = 'Funded';
        app.Manual_Grade__c = 'B1';
        update app;

        Scorecard__c scoreRef = new Scorecard__c();
        scoreRef.Grade__c = 'B1';
        scoreRef.Opportunity__c = app.Id;
        scoreRef.Acceptance__c = 'Y';
        scoreRef.FICO_Score__c = 720;
        scoreRef.Contact__c = app.Contact__c;
        insert scoreRef;
        
        Offer__c off = new Offer__c();
        off.Opportunity__c = app.Id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 36;
        off.APR__c = 0.24;
        off.Loan_Amount__c = 10500;
        off.Installments__c = 36;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.26;
        off.iscustom__c = false;
        insert off;

        ProxyApiCreditReportEntity creditReport = LibraryTest.fakeCreditReportEntity(app.id);

        Opportunity appRefi = new Opportunity();
        appRefi.Contact__c = app.Contact__c;
        appRefi.Lending_Product__c = [Select Id from Lending_Product__c where Name = 'Sample Loan Product' limit 1].Id;
        appRefi.Product_Type__c = 'LOAN';
        appRefi.Amount = app.Contact__r.Loan_Amount__c;
        appRefi.Days_Convention__c = '30/360';
        appRefi.Fee__c = 0.04 * (app.Contact__r.Loan_Amount__c == null ? 0.0 : app.Contact__r.Loan_amount__c);
        appRefi.Interest_Calculation_Method__c = 'Declining Balance';
        appRefi.Type = 'Refinance';
        appRefi.FICO__c = '730';
        appRefi.Manual_Grade__c = 'B1';
        appRefi.Status__c = 'Credit Qualified';
        appRefi.Estimated_Grand_Total__c = 205000;
        appRefi.Contract_Renewed__c = testContract.id;
        appRefi.Name = 'Test Opp';
        appRefi.CloseDate = system.today();
        appRefi.StageName = 'Qualification';
        insert appRefi;

        ProxyApiCreditReportEntity creditReport2 = LibraryTest.fakeCreditReportEntity(appRefi.id);

        if ([SELECT Count() FROM loan_Refinance_Params__c] == 0) {
            loan_Refinance_Params__c refiParams = new loan_Refinance_Params__c();
            refiParams.Contract__c = testContract.Id;
            refiParams.FICO__c = 720;
            refiParams.Grade__c = 'B1';
            refiParams.New_FICO__c = 720;
            refiParams.New_Grade__c = 'B1';
            refiParams.Eligible_For_Refinance__c = false;
            refiParams.Last_Refinance_Eligibility_Review__c = Date.today();
            insert refiParams;
        } else {
            loan_Refinance_Params__c refiParams2 = [SELECT Contract__c, Id, FICO__c, Grade__c, New_FICO__c, New_Grade__c FROM loan_Refinance_Params__c WHERE Contract__c = : testContract.Id LIMIT 1];
            refiParams2.FICO__c = 720;
            refiParams2.Grade__c = 'B1';
            refiParams2.New_FICO__c = 720;
            refiParams2.New_Grade__c = 'B1';
            update refiParams2;
        }

        Scorecard__c scoreRefi = new Scorecard__c();
        scoreRefi.Grade__c = 'B1';
        scoreRefi.Opportunity__c = appRefi.Id;
        scoreRefi.Acceptance__c = 'Y';
        scoreRefi.FICO_Score__c = 720;
        scoreRefi.Contact__c = appRefi.Contact__c;
        insert scoreRefi;
        
        Offer__c offRef = new Offer__c();
        offRef.Opportunity__c = appRefi.Id;
        offRef.Fee__c = 50.0;
        offRef.IsSelected__c = false;
        offRef.Repayment_Frequency__c = 'Monthly';
        offRef.Term__c = 32;
        offRef.APR__c = 0.20;
        offRef.Loan_Amount__c = 4500;
        offRef.Installments__c = 32;
        offRef.Payment_Amount__c = 200;
        offRef.Effective_APR__c = 0.2;
        offRef.iscustom__c = false;
        insert offRef;

        return appRefi;
    }
    
    @isTest static void Test_checkValues_OfferSelection(){
    
    Check_Values_Offer_Selection__c setting2 = New Check_Values_Offer_Selection__c();
        setting2.Name = 'Finwise_GA_-k_3k';
        setting2.check_Product_Name__c = '';
        setting2.check_Loan_Amount_min__c = 0;
        setting2.check_Loan_Amount_max__c = 4700;
        setting2.check_Effective_APR_max__c = 36 ;
        setting2.check_Fee_Percent_min__c = 0;
        setting2.check_Fee_Percent_max__c  = 6 ;
        setting2.check_States_Included__c = 'GA';
        setting2.check_IsFinwise__c = false;
        setting2.check_IsFEB__c = true;
        insert setting2;
        
        Opportunity opp = Librarytest.createApplicationTH();
        opp.Fee__c = 120.00;
        opp.LeadSource__c  = 'EZVERIFY';
        opp.Effective_APR__c = 36.89;
        update opp;
        
        Opportunity app = [SELECT  Id,
                           LeadSource__c,
                           Lending_Product__r.Name,
                           ProductName__c,
                           Effective_APR__c,
                           Lending_Product__c,
                           Status__c,Is_FEB__c,
                           Total_Loan_Amount__c,
                           State__c,
                           Fee__c,
                           Is_Finwise__c
                           FROM    Opportunity
                           WHERE   Id = : opp.Id LIMIT 1];
        
        system.debug('Opp:: ' + app);
        
        Offer__c off = new Offer__c();
        off.Opportunity__c = opp.id;
        off.Fee__c = 50.0;
        off.IsSelected__c = true;
        off.Repayment_Frequency__c = 'Monthly';
        off.Term__c = 24;
        off.APR__c = 0.25;
        off.Loan_Amount__c = 5000.0;
        off.Installments__c = 24;
        off.Payment_Amount__c = 200;
        off.Effective_APR__c = 0.2;
        off.NeedsApproval__c = false;
        off.Total_Loan_Amount__c = 5678.90;
        insert off;
        
        OfferCtrl.checkValuesOfferSelection( off,
                                              String.valueOf(app.State__c), 
                                               String.valueOf(app.Lending_Product__r.Name),
                                                Boolean.valueOf(app.Is_Finwise__c),
                                                 Boolean.valueOf(app.Is_FEB__c) );
    }
    
}