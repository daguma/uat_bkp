global class tmpBatchJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {

    global final String query;
    global final String option;
    global final String execution; // M=Manual || A=Automatic


    global tmpBatchJob(String option) {
        this(option, 'M');
    }
    global tmpBatchJob(String option, String execution) {
        this.execution = execution;
        this.option = option;
        String query = 'SELECT Id, ContractId__c, ContractId__r.loan__Contact__c , ContractId__r.loan__Contact__r.Military__c, ContractId__r.loan_Active_Payment_Plan__c, ' +
                       'ContractId__r.Original_Sub_Source__c, LastRefinanceOppId__c, LastRefinanceOppId__r.Status__c, ' +
                       'Campaign_Id__c, Sequence10Grade__c,  OverrideRequired__c, additionalLoanAmount__c, ' +
                       'InterestRate__c, InterestDiscount__c, MinimumInterestRate__c, Fee__c, OwnerId__c, System_Debug__c, isProcessed__c ' +
                       'FROM Refi_MPD__c ' +
                       'WHERE IsActive__c = true AND isProcessed__c = false ';

        if (this.option == 'SetRefiOpps') {
            this.query = query;
        } else if (this.option == 'GetRefiOppsCR') {
            this.query = query + ' AND LastRefinanceOppId__c != null';
        } else if (this.option == 'ExecRefiOppsSeq10') {
            this.query  = query + ' AND LastRefinanceOppId__c != null AND LastRefinanceOppId__r.Status__c = \'Refinance Unqualified\'';
        } else if (this.option == 'DeleteRefiOppsAttach') {
            this.query  = query + ' AND LastRefinanceOppId__c != null AND LastRefinanceOppId__r.Status__c = \'Refinance Qualified\' ' +
                          ' AND OverrideRequired__c = true';
        } else if (this.option == 'SetRefiOppsOffers') {
            this.query  = query + ' AND LastRefinanceOppId__c != null AND LastRefinanceOppId__r.Status__c = \'Refinance Qualified\'';
        } else if (this.option == 'SetRefiOppsNormalOffers') {
            this.query  = query + ' AND LastRefinanceOppId__c != null AND LastRefinanceOppId__r.Status__c = \'Refinance Qualified\'';
        } else if (this.option == 'reGenerateOffers') {
            this.query  = query + ' AND campaign_id__c = \'Holiday_pricing_2018\'';
        } else if (this.option == 'Regrade') {
            this.query  = query + ' AND campaign_id__c = \'Regrade\'';
        } else if (this.option == 'RegularSoftPull') {
            this.query  = query + ' AND LastRefinanceOppId__c != null';
        }

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Refi_MPD__c> scope) {
        if (this.option == 'SetRefiOpps') {
            for (Refi_MPD__c s : scope) {
                SetRefiOpps(s);
            }
        } else if (this.option == 'GetRefiOppsCR') {
            for (Refi_MPD__c s : scope) {
                GetRefiOppsCR(s);
            }
        } else if (this.option == 'ExecRefiOppsSeq10') {
            for (Refi_MPD__c s : scope) {
                ExecRefiOppsSeq10(s);
            }
        } else if (this.option == 'DeleteRefiOppsAttach') {
            for (Refi_MPD__c s : scope) {
                DeleteRefiOppsAttach(s);
            }
        } else if (this.option == 'SetRefiOppsOffers') {
            for (Refi_MPD__c s : scope) {
                SetRefiOppsOffers(s);
            }
        } else if (this.option == 'SetRefiOppsNormalOffers') {
            for (Refi_MPD__c s : scope) {
                SetRefiOppsNormalOffers(s);
            }
        } else if (this.option == 'reGenerateOffers') {
            for (Refi_MPD__c s : scope) {
                reGenerateOffers(s);
            }
        } else if (this.option == 'Regrade') {
            for (Refi_MPD__c s : scope) {
                Regrade(s);
            }
        }
        else if (this.option == 'RegularSoftPull') {
            for (Refi_MPD__c s : scope) {
                RegularSoftPull(s);
            }
        }
    }

    global void finish(Database.BatchableContext BC) {
        if (this.execution == 'A') {
            if (this.option == 'SetRefiOpps') {
                tmpBatchJob job = new tmpBatchJob('GetRefiOppsCR', 'A');
                System.scheduleBatch(job, 'tmpBatchJob_GetRefiOppsCR', 1, 1);

            } else if (this.option == 'GetRefiOppsCR') {
                tmpBatchJob job = new tmpBatchJob('ExecRefiOppsSeq10', 'A');
                System.scheduleBatch(job, 'tmpBatchJob_ExecRefiOppsSeq10', 1, 1);

            } else if (this.option == 'ExecRefiOppsSeq10') {
                tmpBatchJob job = new tmpBatchJob('DeleteRefiOppsAttach', 'A');
                System.scheduleBatch(job, 'tmpBatchJob_DeleteRefiOppsAttach', 1, 1);

            } else if (this.option == 'DeleteRefiOppsAttach') {
                tmpBatchJob job = new tmpBatchJob('SetRefiOppsOffers', 'A');
                System.scheduleBatch(job, 'tmpBatchJob_SetRefiOppsOffers', 1, 1);
            }
        }
    }

    global void execute(SchedulableContext sc) {

    }

    private void SetRefiOpps(Refi_MPD__c obj) {
        try {
            obj.System_Debug__c = '';
            if (PassEligibilityDefaultRules(obj.ContractId__r, obj.Campaign_Id__c)) {
                loan_Refinance_Params__c param = GetRefinanceParams(obj.ContractId__c);
                param.Eligible_For_Soft_Pull__c = true;
                RefinancePostSoftPull.executeProcess(obj.ContractId__c, param);

                Opportunity lastRefiApp = GetLastRefiOpp(obj.ContractId__r.loan__Contact__c);
                if (lastRefiApp == null) {
                    obj.System_Debug__c += '\nSetRefiOpps: unable to get last Refi Opp';
                }
                obj.LastRefinanceOppId__c = lastRefiApp.Id;
                lastRefiApp.Campaign_Id__c = obj.Campaign_Id__c;
                update lastRefiApp;
            } else {
                obj.System_Debug__c += '\nSetRefiOpps: Fails Rules';
                obj.IsActive__c = false;
            }
        } catch (Exception e) {
            obj.System_Debug__c += '\nSetRefiOpps: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
        } finally {
            update obj;
        }
    }

    private Boolean PassEligibilityDefaultRules(loan__Loan_Account__c contract, String campaign) {

        if (contract.loan__Contact__r.Military__c) return false;
        if (contract.loan_Active_Payment_Plan__c && campaign != 'RiskRefinanceOpp5M') return false;
        if (contract.Original_Sub_Source__c == 'LoanHero') return false;

        return true;
    }

    private void GetRefiOppsCR(Refi_MPD__c obj) {
        try {
            loan_Refinance_Params__c param = GetRefinanceParams(obj.ContractId__c);
            RefinancePostSoftPull.eligibilityCheckGetCR(param);
            Opportunity newOpp = GetOppById(obj.LastRefinanceOppId__c);
            newOpp.OwnerId = obj.OwnerId__c;
            update newOpp;
        } catch (Exception e) {
            obj.System_Debug__c += '\nGetRefiOppsCR: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
        } finally {
            update obj;
        }
    }

    private Opportunity GetOppById(String OppId) {
        Opportunity result = null;
        for (Opportunity opp : [SELECT Id, Amount, Requested_Loan_Amount__c, Contact__r.MailingState,
                                Contact__r.Annual_Income__c,
                                Contract_Renewed__r.loan__Pay_Off_Amount_As_Of_Today__c,
                                Contract_Renewed__r.Opportunity__c,
                                Contract_Renewed__r.loan__Interest_Rate__c,
                                Contract_Renewed__r.loan__Term_Cur__c,
                                Contract_Renewed__r.Modification_Type__c,
                                Contract_Renewed__r.Payment_Frequency_Masked__c,
                                Contract_Renewed__r.Current_Term_Months__c,
                                Fee_Handling__c, CreatedDate, Contract_Renewed__r.CreatedDate,
                                ProductName__c, Campaign_Id__c, Account_Management_Pull_Date__c,
                                Partner_Account__r.Est_Interest_Start_Grace_Period__c,
                                Partner_Account__r.Default_Payment_Frequency__c,
                                Partner_Account__r.Offer_Products__c,
                                Preferred_Payment_Date__c, Scorecard_Grade__c, Status__c, OwnerId,
                                Is_Finwise__c, FICO__c, Estimated_Grand_Total__c, RepaymentFrequency__c,
                                Contract_Renewed__c, Analytic_Random_Number__c, Lending_Product__r.Loan_Lending_Product__r.loan__Loan_Product_Code__c,
                                Lead_Sub_Source__c, Partner_Account__c, Type, Is_FEB__c, LeadSource__c, StrategyType__c, 
                                AON_Enrollment__c, AON_Military__c, Contact__r.Military__c 
                                FROM Opportunity
                                WHERE Id = :OppId limit 1]) {
            result = opp;
        }
        return result;
    }

    private Opportunity GetLastRefiOpp(String contactId) {
        Opportunity result = null;
        Opportunity lastApp = [SELECT CreatedDate
                               FROM Opportunity
                               WHERE  Contact__c = :contactId
                               AND Type != Null
                               ORDER BY CreatedDate DESC
                               LIMIT 1];
        
        for (Opportunity lastValidRefiApp : [SELECT Id, Name, Type, OwnerId,
                                             Fee_Handling__c, Amount,
                                             Status__c, Contract_Renewed__c, Campaign_Id__c,
                                             Contact__c
                                             FROM Opportunity
                                             WHERE  Contact__c = : contactId
                                             AND Type = 'Refinance'
                                             AND Status__c NOT IN ('Funded')
                                             AND CreatedDate = :lastApp.CreatedDate
                                             ORDER BY CreatedDate DESC
                                             LIMIT 1]) {
            result = lastValidRefiApp;
        }
        return result;
    }

    private loan_Refinance_Params__c GetRefinanceParams(String contactId) {
        loan_Refinance_Params__c result = null;
        for (loan_Refinance_Params__c param : [SELECT Id, Contract__c, on_Past_Due__c, Eligible_For_Soft_Pull__c,
                                               ManualEligibilityCheck__c, Last_Refinance_Eligibility_Review__c, Eligible_For_Soft_Pull_Params__c,
                                               Fico__c, Grade__c
                                               FROM loan_Refinance_Params__c
                                               WHERE Contract__c = : contactId  limit 1]) {
            result = param;
        }
        return result;
    }

    private void ExecRefiOppsSeq10(Refi_MPD__c obj) {
        String appId = obj.LastRefinanceOppId__c;
        try {
            ProxyApiCreditReportEntity creditReportEntity = null;
            Opportunity application = CreditReport.getOpportunity(appId);
            CreditReportInputData inputData = CreditReport.mapCreditReportInputData(appId, true, false, application);

            if (inputData.gdsRequestParameter == null)
                inputData.gdsRequestParameter = CreditReport.mapGDSRequestParameter(appId, false);

            inputData.gdsRequestParameter.sequenceId = BRMS_Sequence_Ids__c.getInstance('Account Management').Sequence_Id__c;

            if (inputData != null) {
                if (Test.isRunningTest())
                    creditReportEntity = (ProxyApiCreditReportEntity) JSON.deserializeStrict(LibraryTest.fakeCreditReportData(application.Id), ProxyApiCreditReportEntity.class);
                else
                    creditReportEntity = ProxyApiCreditReport.getCreditReport(JSON.serialize(inputData), false, true);

                if (creditReportEntity != null && creditReportEntity.errorMessage == null) {

                    application = GetOppById(application.Id);

                    application.Account_Management_Pull_Date__c = Datetime.now().date();

                    if (creditReportEntity.brms != null) {
                        if (obj.Campaign_Id__c == 'RiskRefinanceOpp5M') {
                            if (!('A1,A2,B1,B2,C1'.contains(creditReportEntity.brms.finalGrade)))
                                creditReportEntity.brms.finalGrade = 'C1';
                        }
                        application.Scorecard_Grade__c = creditReportEntity.brms.finalGrade;
                        Scorecard__c scorecard = ScorecardUtil.getScorecard(appId);
                        if (scorecard == null) {
                            scorecard = new Scorecard__c(Opportunity__c = appId);
                        }
                        scorecard.Grade__c = application.Scorecard_Grade__c;
                        scorecard.Acceptance__c = 'N';
                        obj.Sequence10Grade__c = application.Scorecard_Grade__c;

                        if ('A1,A2,B1,B2,C1'.contains(application.Scorecard_Grade__c)) {
                            application.Status__c = 'Refinance Qualified';
                            obj.OverrideRequired__c = true;
                            scorecard.Acceptance__c = 'Y';
                        }
                        upsert scorecard;
                    }

                    update application;
                }
            }
        } catch (Exception e) {
            obj.System_Debug__c += '\nExecRefiOppsSeq10: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
        } finally {
            update obj;
        }
    }

    private void DeleteRefiOppsAttach(Refi_MPD__c obj) {
        try {
            for (Attachment attachment : [SELECT Id
                                          FROM Attachment
                                          WHERE ParentId = : obj.LastRefinanceOppId__c AND Name = : CreditReport.ATTACHMENT_FILE_NAME
                                                  LIMIT 1
                                         ]) {
                delete attachment;
            }
            //CreditReport.get(obj.LastRefinanceOppId__c, false, false, false);

        } catch (Exception e) {
            obj.System_Debug__c += '\nDeleteRefiOppsAttach: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
        } finally {
            update obj;
        }
    }

    private void SetRefiOppsOffers(Refi_MPD__c obj) {
        try {
            Opportunity opp = GetOppById(obj.LastRefinanceOppId__c);
            for (Offer__c o : [SELECT id FROM Offer__c WHERE Opportunity__c = : obj.LastRefinanceOppId__c]) {
                delete o;
            }
            generateOffer(opp, obj.InterestRate__c, obj.InterestDiscount__c, obj.MinimumInterestRate__c, obj.additionalLoanAmount__c, obj.Fee__c);
            obj.isProcessed__c = true;
        } catch (Exception e) {
            obj.System_Debug__c += '\nSetRefiOppsOffers: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
        } finally {
            update obj;
        }
    }

    public void SetRefiOppsNormalOffers(Refi_MPD__c obj) {
        try {
            Opportunity opp = GetOppById(obj.LastRefinanceOppId__c);

            ProxyApiCreditReportEntity creditReportEntity;
            creditReportEntity = CreditReport.getByEntityId(opp.Id);

            List<ProxyApiOfferCatalog> newOffers = OfferCtrlUtil.getOffers(opp, opp.Scorecard_Grade__c, creditReportEntity.Id, String.valueOf(opp.FICO__c), null);

            update opp;

            ScorecardUtil.deleteOffers(opp.Id);
            ScorecardUtil.generateOffers(opp.Id, newOffers);
            ScorecardUtil.regenerateOffersRefinance(opp.Id, false);

            obj.isProcessed__c = true;
        } catch (Exception e) {
            obj.System_Debug__c += '\nSetRefiOppsNormalOffers: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
        } finally {
            update obj;
        }
    }

    public void reGenerateOffers(Refi_MPD__c obj) {
        for (Opportunity opp : [Select id FROM opportunity WHERE Id = : obj.LastRefinanceOppId__c AND campaign_id__c = : obj.campaign_id__c]) {

            try {
                obj.System_Debug__c = '';
                opp = GetOppById(obj.LastRefinanceOppId__c);
                ProxyApiCreditReportEntity creditReportEntity;
                creditReportEntity = CreditReport.getByEntityId(opp.Id);

                List<ProxyApiOfferCatalog> newOffers = OfferCtrlUtil.getOffers(opp, opp.Scorecard_Grade__c, creditReportEntity.Id, String.valueOf(opp.FICO__c), null);

                update opp;
                ScorecardUtil.deleteOffers(opp.Id);
                ScorecardUtil.generateOffers(opp.Id, newOffers);
                obj.isProcessed__c = true;
            } catch (Exception e) {
                obj.System_Debug__c += '\nSetRefiOppsOffers: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
            } finally {
                update obj;
            }
        }
    }


    private void Regrade (Refi_MPD__c obj) {
        try {
            Opportunity opp =  [SELECT Id, Status__c FROM Opportunity WHERE Id = : obj.LastRefinanceOppId__c];
            validationTabUtil.regrade(opp, false);
            obj.isProcessed__c = true;
        } catch (Exception e) {
            obj.System_Debug__c += '\nSetRefiOppsOffers: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
        } finally {
            update obj;
        }
    }

    private void RegularSoftPull (Refi_MPD__c obj) {
        try {
            Opportunity opp =  [SELECT Id FROM Opportunity WHERE Id = : obj.LastRefinanceOppId__c];
            ProxyApiCreditReportEntity creditReportEntity = CreditReport.get(opp.Id, false, true, true);
            obj.isProcessed__c = true;
        } catch (Exception e) {
            obj.System_Debug__c += '\nRegularSoftPull: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
        } finally {
            update obj;
        }
    }

    /*

    5M

        • Term: Original Term
        • Loan Amount: Current balance on original loan + $500
        • Interest Rate: 11.99% . Interest rate cannot be below 9.99%
        //Update: 13.99

        If a workout agreement exists on the open LAI:

        • If workout interest rate >=11.99%, then give 11.99% in new refinance opportunity
        • If workout interest rate < 11.99%, then keep current workout rate in refinance opportunity
        • The term on new loan is the same as term on workout agreement

    40M

        • Term: Original Term
        • Loan Amount: Current balance on original loan + $500
        • Interest Rate = Maximum of (Current Interest Rate minus 300 basis points but not below 9.99%)

        If a workout agreement exists:

        • If workout interest rate >= Current Interest Rate minus 300 basis points, then give Current Interest Rate minus 300 basis points in new loan.
        • if workout interest rate < Current Interest Rate minus 300 basis points then keep current workout rate.
        • Term on new loan is the same as term on workout agreement.

    */

    private void generateOffer(Opportunity opp, Decimal interestRate, Decimal interestDiscount, Decimal minimumInterestRate, Decimal additionalLoanAmount, Decimal fee) {

        Decimal po, r, por;
        Offer__c CustomOffer = new Offer__c();

        CustomOffer.Loan_Amount__c = opp.Contract_Renewed__r.loan__Pay_Off_Amount_As_Of_Today__c + additionalLoanAmount;

        CustomOffer.APR__c = interestRate;

        if (opp.Contract_Renewed__r.Modification_Type__c == 'Loan Workout Agreement Modification') {
            CustomOffer.APR__c = (opp.Contract_Renewed__r.loan__Interest_Rate__c < minimumInterestRate) ? minimumInterestRate : (opp.Contract_Renewed__r.loan__Interest_Rate__c > interestRate) ? interestRate : opp.Contract_Renewed__r.loan__Interest_Rate__c;
        }

        CustomOffer.APR__c = (CustomOffer.APR__c / 100);

        CustomOffer.Term__c = (opp.Scorecard_Grade__c == 'C1' || opp.Scorecard_Grade__c == 'B2') ? 36 : 48;

        CustomOffer.Fee__c = CustomOffer.Loan_Amount__c * fee;
        CustomOffer.Contact__c = opp.Contact__c;

        if (opp.Fee_Handling__c == 'Net Fee Out') {
            CustomOffer.Total_Loan_Amount__c = CustomOffer.Loan_Amount__c;
            CustomOffer.Loan_Amount__c = CustomOffer.Loan_Amount__c - CustomOffer.Fee__c;
        } else {
            CustomOffer.Loan_Amount__c = CustomOffer.Loan_Amount__c;
            CustomOffer.Total_Loan_Amount__c = CustomOffer.Fee__c + CustomOffer.Loan_Amount__c;
        } 

        CustomOffer.Repayment_Frequency__c = opp.Contract_Renewed__r.Payment_Frequency_Masked__c;
        Integer TP = OfferCtrlUtil.getFrequencyUtil(CustomOffer.Repayment_Frequency__c);
        CustomOffer.Installments__c = Math.ceil(((CustomOffer.Term__c / 12) * TP).setScale(2));
        r = CustomOffer.APR__c / TP;
        po = (r + 1).pow(Integer.valueOf(CustomOffer.Installments__c)) - 1;
        por = r / po;

        CustomOffer.Payment_Amount__c = (r + por) * (CustomOffer.Loan_Amount__c + CustomOffer.Fee__c);
        CustomOffer.Payment_Amount__c = CustomOffer.Payment_Amount__c.setScale(2);
        CustomOffer.IsCustom__c = true;
        CustomOffer.Opportunity__c = opp.Id;
        Date firstPaymentDate = OfferCtrlUtil.getNextPayment(CustomOffer.Repayment_Frequency__c, Date.today(), 1);
        CustomOffer.FirstPaymentDate__c = firstPaymentDate;
        CustomOffer.Effective_APR__c = OfferCtrlUtil.calculateEffectiveAPR_GovComp(Date.today(), firstPaymentDate, CustomOffer.Loan_Amount__c, CustomOffer.Payment_Amount__c, CustomOffer.Installments__c, CustomOffer.Repayment_Frequency__c);
        CustomOffer.NeedsApproval__c = false;
        CustomOffer.PricingAPR__c = null;
        insert CustomOffer;

        // SECOND OFFER ----------------------------------------

        Offer__c CustomOffer2 = new Offer__c();

        CustomOffer2.Loan_Amount__c = opp.Contract_Renewed__r.loan__Pay_Off_Amount_As_Of_Today__c + additionalLoanAmount;

        CustomOffer2.APR__c = interestRate;

        if (opp.Contract_Renewed__r.Modification_Type__c == 'Loan Workout Agreement Modification') {
            CustomOffer2.APR__c = (opp.Contract_Renewed__r.loan__Interest_Rate__c < minimumInterestRate) ? minimumInterestRate : (opp.Contract_Renewed__r.loan__Interest_Rate__c > interestRate) ? interestRate : opp.Contract_Renewed__r.loan__Interest_Rate__c;
        }

        CustomOffer2.APR__c = (CustomOffer2.APR__c / 100);


        Integer billsSatisfied;

        List<AggregateResult> BillsPaidList = [SELECT COUNT(Id) Total FROM loan__Loan_account_Due_Details__c
                                               where loan__Loan_Account__c = : opp.Contract_Renewed__c
                                                       and loan__Payment_Satisfied__c = true
                                                               and loan__DD_Primary_Flag__c = true];

        for (AggregateResult ar : BillsPaidList)  {
            billsSatisfied = (Integer) ar.get('Total');
        }
		
        if(opp.Contract_Renewed__r.Payment_Frequency_Masked__c == 'Bi-Weekly' || opp.Contract_Renewed__r.Payment_Frequency_Masked__c =='Semi-Monthly'){
           billsSatisfied = billsSatisfied / 2; 
        }
        CustomOffer2.Term__c = Integer.valueOf((opp.Contract_Renewed__r.Current_Term_Months__c - billsSatisfied) < = 60 ? opp.Contract_Renewed__r.Current_Term_Months__c - billsSatisfied : 60);
        CustomOffer2.Fee__c = CustomOffer2.Loan_Amount__c * fee;
        CustomOffer2.Contact__c = opp.Contact__c;

        if (opp.Fee_Handling__c == 'Net Fee Out') {
            CustomOffer2.Total_Loan_Amount__c = CustomOffer2.Loan_Amount__c;
            CustomOffer2.Loan_Amount__c = CustomOffer2.Loan_Amount__c - CustomOffer2.Fee__c;
        } else {
            CustomOffer2.Loan_Amount__c = CustomOffer2.Loan_Amount__c;
            CustomOffer2.Total_Loan_Amount__c = CustomOffer2.Fee__c + CustomOffer2.Loan_Amount__c;
        }

        CustomOffer2.Repayment_Frequency__c = opp.Contract_Renewed__r.Payment_Frequency_Masked__c;
        TP = OfferCtrlUtil.getFrequencyUtil(CustomOffer2.Repayment_Frequency__c);
        CustomOffer2.Installments__c = Math.ceil(((CustomOffer2.Term__c / 12) * TP).setScale(2));
        r = CustomOffer2.APR__c / TP;
        po = (r + 1).pow(Integer.valueOf(CustomOffer2.Installments__c)) - 1;
        por = r / po;

        CustomOffer2.Payment_Amount__c = (r + por) * (CustomOffer2.Loan_Amount__c + CustomOffer2.Fee__c);
        CustomOffer2.Payment_Amount__c = CustomOffer2.Payment_Amount__c.setScale(2);
        CustomOffer2.IsCustom__c = true;
        CustomOffer2.Opportunity__c = opp.Id;
        firstPaymentDate = OfferCtrlUtil.getNextPayment(CustomOffer2.Repayment_Frequency__c, Date.today(), 1);
        CustomOffer2.FirstPaymentDate__c = firstPaymentDate;
        CustomOffer2.Effective_APR__c = OfferCtrlUtil.calculateEffectiveAPR_GovComp(Date.today(), firstPaymentDate, CustomOffer2.Loan_Amount__c, CustomOffer2.Payment_Amount__c, CustomOffer2.Installments__c, CustomOffer2.Repayment_Frequency__c);
        CustomOffer2.NeedsApproval__c = false;
        CustomOffer2.PricingAPR__c = null;
        insert CustomOffer2;
    }



}

//To run:
/*

    // 1
    tmpBatchJob job = new tmpBatchJob('SetRefiOpps');
    database.executebatch(job, 1);


    // 2
    tmpBatchJob job = new tmpBatchJob('GetRefiOppsCR');
    database.executebatch(job, 1);


    // 3
    tmpBatchJob job = new tmpBatchJob('ExecRefiOppsSeq10');
    database.executebatch(job, 1);


    // 4
    tmpBatchJob job = new tmpBatchJob('DeleteRefiOppsAttach');
    database.executebatch(job, 1);


    // 5
    tmpBatchJob job = new tmpBatchJob('SetRefiOppsOffers');
    database.executebatch(job, 1);


*/