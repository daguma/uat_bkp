@isTest
public class TestManageNotesPermissions {
 	public static  testmethod void testManageNotesPermissions(){
        try{
          genesis__Applications__c a = TestHelper.createApplication();
        
           note notes= new note();
           notes.ParentId=a.id;
           notes.title='Test';
           notes.body ='Testing';
           insert notes;
          
           notes.title= 'Updated Note';
           Update notes;
         
            
         }catch(Exception e){
           system.debug('Exception in CustomerPortalWSDL '+e.getStackTraceString());  
         }
   }
    public static  testmethod void testDeleteNotes(){
        try{
          genesis__Applications__c a = TestHelper.createApplication();
        
           note notes= new note();
           notes.ParentId=a.id;
           notes.title='Test';
           notes.body ='Testing';
           insert notes;
          
          List<Note> deleteNotes = [SELECT Id From Note where ParentId =: a.id];
		  delete deleteNotes; 
            
         }catch(Exception e){
           system.debug('Exception in CustomerPortalWSDL '+e.getStackTraceString());  
         }
   }
}