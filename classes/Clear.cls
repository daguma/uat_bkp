global class Clear {
/*
    /**
     * [mapClearCourtInputData]
     * @param  contact
     * @return ClearInputDat
     */
    public static ClearInputData mapClearCourtInputData(Contact contact) {
        ClearInputData clearInputData = new ClearInputData();
        ClearInputParameter clearInputParameter = mapClearInputParameter(contact);
        PersonRequestParameter courtRequestParameter = mapPersonRequestParameter(contact);

        clearInputData.clearInputParameter = clearInputParameter;
        clearInputData.courtRequestParameter = courtRequestParameter;

        return courtRequestParameter != null ? clearInputData : null;
    }

    /**
     * [mapClearInputParameter description]
     * @param  contact [description]
     * @return         [description]
     */
    public static ClearInputParameter mapClearInputParameter(Contact contact) {
        ClearInputParameter clearInputParameter = null;

        if (contact != null) {
            clearInputParameter = new ClearInputParameter();
            clearInputParameter.contactId = contact.Id;
            clearInputParameter.phone = contact.Phone != null ? contact.Phone : '';
            clearInputParameter.address = new ClearAddress(contact.MailingStreet,
                    contact.MailingCity,
                    contact.MailingState,
                    contact.MailingPostalCode);

            if (contact.BirthDate != null) {
                DateTime dt = DateTime.newInstance(contact.BirthDate.year(),
                                                   contact.BirthDate.month(),
                                                   contact.BirthDate.day());

                clearInputParameter.dob = dt.format('yyyy-MM-dd');
            } else {
                clearInputParameter.dob = '';
            }
        }

        return clearInputParameter;
    }

    /**
     * [mapPersonRequestParameter description]
     * @param  contact [description]
     * @return         [description]
     */
    private static PersonRequestParameter mapPersonRequestParameter(Contact contact) {
        PersonRequestParameter personRequestParameter = null;

        if (contact != null && contact.SSN__c != null) {
            personRequestParameter = new PersonRequestParameter();
            personRequestParameter.firstName = contact.FirstName != null ? contact.FirstName : '';
            personRequestParameter.lastName = contact.LastName != null ? contact.LastName : '';
            personRequestParameter.ssn = contact.SSN__c != null ? contact.SSN__c : '';

            if (contact.SSN__c.length() == 9) {
                personRequestParameter.ssn = contact.SSN__c.substring(0, 3) +
                                             '-' +
                                             contact.SSN__c.substring(3, 5) +
                                             '-' +
                                             contact.SSN__c.substring(5, 9);
            } else {
                return null;
            }
        }

        return personRequestParameter;
    }

    /**
     * Gets a Contact by the id
     * @param  contactId  The id of the contact to get
     * @return Contact
     */
    private static Contact getContact(String contactId) {
        Contact result = null;

        for (Contact contact : [SELECT Id,
                                LastName,
                                FirstName,
                                Phone,
                                SSN__c,
                                BirthDate,
                                MailingStreet,
                                MailingCity,
                                MailingState,
                                MailingPostalCode,
                                Clear_Address_Approved__c,
                                Clear_Is_Approved__c,
                                Clear_Phone_Approved__c,
                                Clear_SSN_Approved__c,
                                Clear_DOB_Approved__c,
                                Clear_Relevance__c
                                FROM Contact
                                WHERE id = : contactId
                                           LIMIT 1
                               ]) {
            result = contact;
        }

        return result;
    }

    /**
     * [getPerson description]
     * @param  contactId [description]
     * @return           [description]
     */
    @future(callout = true)
    public static void getCourt(String contactId, String appId) {

        ProxyApiClearCourtEntity clearCourtEntity = null;
        Contact contact = getContact(contactId);
        ClearInputData inputData = mapClearCourtInputData(contact);

        if (Test.isRunningTest() && contactId == 'error')
            contact = LibraryTest.createContactTH();

        if (inputData != null) {
            if (Test.isRunningTest()) {
                if (contactId != 'error') {
                    clearCourtEntity = (ProxyApiClearCourtEntity)JSON.deserializeStrict(LibraryTest.fakeProxyApiClearCourtEntity(contactId), ProxyApiClearCourtEntity.class);
                }
            } else
                clearCourtEntity = ProxyApiClear.getCourt(JSON.serialize(inputData));
        }

        if (clearCourtEntity != null && !clearCourtEntity.errorResponse && clearCourtEntity.clearCourtResults != null) {
            insertTaxLiensNote(clearCourtEntity.clearCourtResults.noteBody, contact, appId);

            Id clearCourtResultId = insertClearCourtResult(clearCourtEntity.clearCourtResults, contactId, appId);

            if (clearCourtResultId != null)
                insertClearCourtReportData(clearCourtEntity.clearCourtResults.clearCourtReportData, clearCourtResultId);

            // WILL DO SOME OTHER UPDATES HERE IF NEEDED.
        }
    }

    private static void insertTaxLiensNote(String noteBody, Contact c, String appId) {
        String title = 'Tax Liens and/or Judgements Results for ' + c.FirstName + ' ' + c.LastName;
        List<Note> notesToInsert = new List<Note>();
        try {
            Note contactNote = new Note();
            Note appNote = new Note();
            contactNote.Title = title;
            contactNote.Body = noteBody;
            contactNote.ParentId = c.Id;
            appNote.Title = title;
            appNote.Body = noteBody;
            appNote.ParentId = appId;
            notesToInsert.add(contactNote);
            notesToInsert.add(appNote);
            insert notesToInsert;
        } catch (Exception e) {
            System.debug('ERROR: ' + e.getMessage() + ' - ' + e.getStackTraceString());
        }
    }
    
    private static Id insertClearCourtResult(ClearCourtResults clearCourtResult, String contactId, String appId) {
        try {
            Clear_Court_Result__c courtResult = new Clear_Court_Result__c();
            courtResult.Opportunity__c = appId;
            courtResult.Contact__c = contactId;
            courtResult.Open_Tax_Liens__c = clearCourtResult.openTaxLiens;
            courtResult.Open_Judgments__c = clearCourtResult.openJudgements;
            courtResult.Closed_Tax_Liens__c = clearCourtResult.closedTaxLiens;
            courtResult.Closed_Judgments__c = clearCourtResult.closedJudgements;
            courtResult.Total_Liens_Judgments__c = clearCourtResult.totalLiensJudgements;
            insert courtResult;
            return courtResult.Id;
        } catch (Exception e) {
            System.debug('ERROR: ' + e.getMessage() + ' - ' + e.getStackTraceString());
            return null;
        }
    }

    private static void insertClearCourtReportData(List<ClearCourtReportData> dataList, Id clearCourtResultId) {
        try {
            List<ClearCourtReportData__c> courtDataList = new List<ClearCourtReportData__c>();
            ClearCourtReportData__c courtData = new ClearCourtReportData__c();
            Decimal theAmount = 0;
            for (ClearCourtReportData data : dataList) {
                theAmount = data.amount == '--' ? 0 : Decimal.valueOf(data.amount.replace('$', '').replace(',', ''));
                courtData = new ClearCourtReportData__c();
                courtData.FiledDate__c = parseDate(data.filedDate);
                courtData.DocumentType__c = data.documentType;
                courtData.Amount__c = theAmount;
                courtData.DebtorName__c = data.debtorName;
                courtData.TypeOfAction__c = data.typeOfAction;
                courtData.Clear_Court_Result__c = clearCourtResultId;
                courtDataList.add(courtData);
            }

            if (courtDataList.size() > 0)
                insert courtDataList;
        } catch (Exception e) {
            System.debug('ERROR: ' + e.getMessage() + ' - ' + e.getStackTraceString());
        }
    }

    private static Date parseDate(String inDate) {
        String[] dst = inDate.split('-');
        Date d = Date.newInstance(Integer.valueOf(dst[0]), Integer.valueOf(dst[1]), Integer.valueOf(dst[2]));
        return d;
    }

    /**
     * [getClearCourtResultByContactId description]
     * @param  contactId [description]
     * @param  appId     [description]
     * @return           [description]
     */
    public static Clear_Court_Result__c getClearCourtResult(String contactId, String appId) {

        Clear_Court_Result__c result = null;

        for (Clear_Court_Result__c courtResult : [SELECT Id, CreatedDate, Opportunity__c,Opportunity__r.ApplicationId__c,
                Contact__c, Open_Tax_Liens__c,
                Closed_Tax_Liens__c, Open_Judgments__c,
                Closed_Judgments__c, Total_Liens_Judgments__c
                FROM Clear_Court_Result__c
                WHERE Contact__c = : contactId
                                   AND (Opportunity__c = : appId or Opportunity__r.ApplicationId__c = :appId)
                                           ORDER BY CreatedDate DESC
                                           LIMIT 1]) {
            result = courtResult;
        }

        return result;
    }

    /**
     * [isCourtCountLogicGreaterThanZero description]
     * @param  contactId [description]
     * @param  appId     [description]
     * @return           [description]
     */
    public static Boolean isCourtCountLogicGreaterThanZero(String contactId, String appId) {

        Clear_Court_Result__c result = getClearCourtResult(contactId, appId);

        return result != null && result.Total_Liens_Judgments__c > 0;
    }
}