global class EmailAgeRequestParameter {    
    
    public String email {get;set;} 
    
    public String ip_address {get;set;} 
    
    public String emailAgeStatusCode {get;set;}
    
}