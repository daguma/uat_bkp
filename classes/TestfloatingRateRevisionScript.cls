@isTest
public class TestfloatingRateRevisionScript {
    @testSetup
    public static void testSetUp() {
        loan.TestHelper.systemDate = Date.newInstance(2015, 3, 01);
        loan.TestHelper.createSeedDataForTesting();
        
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Set the Fiscal Year days
        loan.TestHelper.createDayProcessForFullYear(loan.TestHelper.SystemDate);
        
        //Client__c dummyClient = TestHelper.createClient(dummyOffice);
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();        
        
        loan__Payment_Mode__c pMode = [SELECT Id 
                                       FROM loan__Payment_Mode__c 
                                       LIMIT 1
                                      ];
        
        Contact dummyClient = new Contact();
        dummyClient.LastName = 'TestContact';
        insert dummyClient;
        loan__Loan_Account__c dummyLoanAccount = loan.TestHelper.createLoanAccountForContactObj(dummyLP,
                                                    dummyClient,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
        
        loan__Loan_Disbursal_Transaction__c distxn = new loan__Loan_Disbursal_Transaction__c();
        distxn.loan__Loan_Account__c = dummyLoanAccount.Id;
        distxn.loan__Mode_of_Payment__c = pMode.Id;
        distxn.loan__Disbursed_Amt__c = 10000;
        loan.LoanDisbursalActionAPI a = new loan.LoanDisbursalActionAPI(distxn);
        a.disburseLoanAccount();
    }
    public static testMethod void testfloatingRateChildUpsertion() {
        loan__Loan_Account__c loanAccount = [SELECT Id,
                                                    loan__Interest_Type__c
                                             FROM loan__Loan_Account__c
                                            ];
        loan__Loan_Disbursal_Transaction__c distxn = [SELECT Id, 
                                                             loan__Disbursal_Date__c 
                                                        FROM loan__Loan_Disbursal_Transaction__c
                                                         ];
        
        loan__Floating_Rate_Index__c fri = new loan__Floating_Rate_Index__c();
        fri.loan__Active__c = true;
        insert fri;
        loan__Floating_Rate__c fr = new loan__Floating_Rate__c();
        fr.loan__Rate_Effective_From__c = Date.newInstance(2015, 3, 5);
        fr.loan__Rate_Percentage__c = 1.65;
        fr.loan__Active__c = true;
        fr.loan__Reset_Process_Status__c = 'Not Submitted';
        fr.loan__Floating_Rate_Index__c = fri.Id;
        insert fr;
        //disabling the triggeres
        loan__Org_Parameters__c orgParams = loan.CustomSettingsUtil.getOrgParameters();
        orgParams.loan__Namespace_Prefix__c = 'loan';
        orgParams.loan__Disable_Triggers__c = true;
        upsert orgParams;
        loanAccount.loan__Interest_Type__c = loan.LoanConstants.INTEREST_TYPE_FLOATING;
        loanAccount.loan__Product_Type__c = loan.LoanConstants.LOC;
        update loanAccount;
        distxn.loan__Disbursal_Date__c = Date.newInstance(2015, 3, 10);
        update distxn;
        List<loan__Multi_Step_Loan_Setup__c> rateSetUpList = new List<loan__Multi_Step_Loan_Setup__c>();
        loan__Multi_Step_Loan_Setup__c rateSetUp = new loan__Multi_Step_Loan_Setup__c();
        rateSetUp.loan__Start_Date__c = Date.newInstance(2015, 3, 01);
        rateSetUp.loan__Loan_Account__c = loanAccount.Id;
        rateSetUp.loan__Sequence__c = 1;
        rateSetUp.loan__Floating_Rate_Index__c = fri.Id;
        rateSetup.loan__Margin_Rate__c = 1.5;
        rateSetUpList.add(rateSetUp);
        rateSetUp = new loan__Multi_Step_Loan_Setup__c();
        rateSetUp.loan__Start_Date__c = Date.newInstance(2015, 4, 01);
        rateSetUp.loan__Loan_Account__c = loanAccount.Id;
        rateSetUp.loan__Sequence__c = 2;
        rateSetUp.loan__Floating_Rate_Index__c = fri.Id;
        rateSetup.loan__Margin_Rate__c = 2;
        rateSetUpList.add(rateSetUp);
        insert rateSetUpList;
        List<loan__Other_Transaction__c> otxns = new List<loan__Other_Transaction__c>();
        loan__Other_Transaction__c otxn = new loan__Other_Transaction__c();
        otxn.loan__New_Interest_Rate__c = 2;
        otxn.loan__Txn_Date__c = Date.newInstance(2015, 3, 01);
        otxn.loan__Loan_Account__c = loanAccount.Id;
        otxn.loan__Transaction_Type__c = loan.LoanConstants.LOAN_TRANSACTION_INDEX_RATE_CHANGE;
        otxns.add(otxn);
        otxn = new loan__Other_Transaction__c();
        otxn.loan__New_Interest_Rate__c = 2.5;
        otxn.loan__Txn_Date__c = Date.newInstance(2015, 3, 02);
        otxn.loan__Loan_Account__c = loanAccount.Id;
        otxn.loan__Transaction_Type__c = loan.LoanConstants.LOAN_TRANSACTION_INDEX_RATE_CHANGE;
        otxns.add(otxn);
        otxn = new loan__Other_Transaction__c();
        otxn.loan__New_Interest_Rate__c = 2.75;
        otxn.loan__Txn_Date__c = Date.newInstance(2015, 3, 15);
        otxn.loan__Loan_Account__c = loanAccount.Id;
        otxn.loan__Transaction_Type__c = loan.LoanConstants.LOAN_TRANSACTION_INDEX_RATE_CHANGE;
        otxns.add(otxn);
        otxn = new loan__Other_Transaction__c();
        otxn.loan__New_Interest_Rate__c = 2.15;
        otxn.loan__Txn_Date__c = Date.newInstance(2015, 4, 01);
        otxn.loan__Loan_Account__c = loanAccount.Id;
        otxn.loan__Transaction_Type__c = loan.LoanConstants.LOAN_TRANSACTION_INDEX_RATE_CHANGE;
        otxns.add(otxn);
        otxn = new loan__Other_Transaction__c();
        otxn.loan__New_Interest_Rate__c = 2.35;
        otxn.loan__Txn_Date__c = Date.newInstance(2015, 4, 02);
        otxn.loan__Loan_Account__c = loanAccount.Id;
        otxn.loan__Transaction_Type__c = loan.LoanConstants.LOAN_TRANSACTION_INDEX_RATE_CHANGE;
        otxns.add(otxn);
        otxn = new loan__Other_Transaction__c();
        otxn.loan__New_Interest_Rate__c = 2.85;
        otxn.loan__Txn_Date__c = Date.newInstance(2015, 4, 15);
        otxn.loan__Loan_Account__c = loanAccount.Id;
        otxn.loan__Transaction_Type__c = loan.LoanConstants.LOAN_TRANSACTION_INDEX_RATE_CHANGE;
        otxns.add(otxn);
        insert otxns;
        Test.startTest();
        FloatingRateRevisionScript job = new FloatingRateRevisionScript();
        Database.executeBatch(job, 200);
        Test.stopTest();
        List<loan__Multi_Step_Loan_Setup__c> rateSetUpList1 = [Select Id, 
                                                                     loan__Interest_Rate__c, 
                                                                     loan__Sequence__c, 
                                                                     loan__Parent_Multi_Step_Loan_Setup__c,
                                                                     loan__Margin_Rate__c,
                                                                     loan__Start_Date__c
                                                                FROM loan__Multi_Step_Loan_Setup__c
                                                                WHERE loan__Loan_Account__c = :loanAccount.Id
                                                                AND loan__Parent_Multi_Step_Loan_Setup__c != null
                                                                ORDER BY loan__Start_Date__c];
        
        system.assertEquals(7, rateSetUpList1.size());
        /*for(loan__Multi_Step_Loan_Setup__c rs : rateSetUpList1) {
            system.debug(rs);
            system.assertEquals(7, rs.size());
        }*/
    }    
}