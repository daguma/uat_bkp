@isTest public class LeadContInterfaceTest {

    @isTest static void TestContactVerify() {

        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        cuSettings.SubsourceDefaultIncomeExcluded__c = 'UnitedMedicalCredit,UnitedMedicalCredit_Direc';

        insert cuSettings;

        Lead l = LibraryTest.createLeadTH();
        Contact c = LibraryTest.createContactTH();
       loan__Loan_Product__c lp = LibraryTest.createLendingProductTH('Lending Point');

        c.Lead__c = l.Id;
        c.Authorization_to_pull_credit__c = true;


        Account a = new Account();
        a.Name = 'TEST';
        a.Billingstreet = l.street;
        a.BillingCity = l.City;
        a.BillingState = l.state;
        a.BillingPostalCode = l.Postalcode;
        a.BillingCountry = l.Country;
        a.Phone = l.Phone;
        a.Type = 'Customer';

        insert a;

        c.AccountId = a.Id;

        update c;
        String res = LeadContInterface.InterfacetoLdCnt('{"annualIncome":"15000","ssn":"1458","email":"abc@def.com","id":"'+c.Id+'","authToPullCredit":"true"}');
        Boolean passed = res.contains('applicationId');

    }

    @isTest static void TestLeadVerify() {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        cuSettings.SubsourceDefaultIncomeExcluded__c = 'UnitedMedicalCredit,UnitedMedicalCredit_Direc';

        insert cuSettings;
        
        loan__Loan_Product__c lp = LibraryTest.createLendingProductTH('Lending Point');

        Lead l = LibraryTest.createLeadTH();
        Contact c = LibraryTest.createContactTH();
        c.Lead__c = l.Id;
        c.Authorization_to_pull_credit__c = true;
        c.ints__Social_Security_Number__c='';
        Account a = new Account();
        a.Name = 'TEST';
        a.Billingstreet = l.street;
        a.BillingCity = l.City;
        a.BillingState = l.state;
        a.BillingPostalCode = l.Postalcode;
        a.BillingCountry = l.Country;
        a.Phone = l.Phone;
        a.Type = 'Customer';

        insert a;
        update c;
        l.Authorization_to_pull_credit__c = true;
        update l;
        //string result=LeadContInterface.updateDMLContact(c);
        String res = LeadContInterface.InterfacetoLdCnt('{"annualIncome":"15000","ssn":"","email":"abc@def.com","id":"'+l.Id+'","authToPullCredit":"true"}');
        res = LeadContInterface.InterfacetoLdCnt('{"annualIncome":"","ssn":"4523","email":"abc@def.com","id":"'+l.Id+'","authToPullCredit":"true"}');
        res = LeadContInterface.InterfacetoLdCnt('{"annualIncome":"15000","ssn":"4569","email":"abc@def.com","id":"'+l.Id+'","authToPullCredit":"true"}');
        
        c.Phone =null;
        update c;
        res = LeadContInterface.InterfacetoLdCnt('{"annualIncome":"15000","ssn":"4569","email":"abc@def.com","id":"'+l.Id+'","authToPullCredit":"true"}');
        
       Boolean passed = res.contains('applicationId');
    }
    
    @isTest static void TestLeadVerify1() {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        cuSettings.SubsourceDefaultIncomeExcluded__c = 'UnitedMedicalCredit,UnitedMedicalCredit_Direc';

        insert cuSettings;

                loan__Loan_Product__c lp = LibraryTest.createLendingProductTH('Lending Point');

        Lead l = LibraryTest.createLeadTH();
        Contact c = LibraryTest.createContactTH();
        c.Lead__c = l.Id;
        c.Authorization_to_pull_credit__c = true;
        c.ints__Social_Security_Number__c='';
        Account a = new Account();
        a.Name = 'TEST';
        a.Billingstreet = l.street;
        a.BillingCity = l.City;
        a.BillingState = l.state;
        a.BillingPostalCode = l.Postalcode;
        a.BillingCountry = l.Country;
        a.Phone = l.Phone;
        a.Type = 'Customer';

        insert a;
        update c;
        l.Authorization_to_pull_credit__c = true;
        update l;
        //string result=LeadContInterface.updateDMLContact(c);
       
        c.email=null;
        update c;
        String res = LeadContInterface.InterfacetoLdCnt('{"annualIncome":"15000","ssn":"4569","id":"'+l.Id+'","authToPullCredit":"true"}');
        
        c.Annual_Income__c=null;
        update c;
        res = LeadContInterface.InterfacetoLdCnt('{"annualIncome":"5000","email":"abc@def.com","ssn":"4569","id":"'+l.Id+'","authToPullCredit":"true"}');
        
        Boolean passed = res.contains('applicationId');
    }
    @isTest static void TestLeadVerify2() {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        cuSettings.SubsourceDefaultIncomeExcluded__c = 'UnitedMedicalCredit,UnitedMedicalCredit_Direc';

        insert cuSettings;

                loan__Loan_Product__c lp = LibraryTest.createLendingProductTH('Lending Point');

        Lead l = LibraryTest.createLeadTH();
        Contact c = LibraryTest.createContactTH();
        c.Lead__c = l.Id;
        c.Authorization_to_pull_credit__c = true;
        c.ints__Social_Security_Number__c='';
        c.product__c = 'Point Of Need';
        Account a = new Account();
        a.Name = 'TEST';
        a.Billingstreet = l.street;
        a.BillingCity = l.City;
        a.BillingState = l.state;
        a.BillingPostalCode = l.Postalcode;
        a.BillingCountry = l.Country;
        a.Phone = l.Phone;
        a.Type = 'Customer';

        insert a;
        update c;
        l.Authorization_to_pull_credit__c = true;
        update l;
        
        String res = LeadContInterface.InterfacetoLdCnt(l.Id);
        
        
    }
    
    @isTest static void TestLeadValidation() {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        cuSettings.Email_Alert_Portal_Apps__c = 'WebApps@lendingpoint.com';
        cuSettings.Default_Lending_Product__c = 'Lending Point';
        cuSettings.Default_Company__c = 'LendingPoint';
        cuSettings.SupportedStates__c = 'GA,NM,UT,MN,MO';
        cuSettings.Dummy_Offers_Email_Domains__c = 'sample';
        cuSettings.SubsourceDefaultIncomeExcluded__c = 'UnitedMedicalCredit,UnitedMedicalCredit_Direc';

        insert cuSettings;

                loan__Loan_Product__c lp = LibraryTest.createLendingProductTH('Lending Point');

        Lead l = LibraryTest.createLeadTH();
        Contact c = LibraryTest.createContactTH();
        c.Lead__c = l.Id;
        c.Authorization_to_pull_credit__c = true;
        c.ints__Social_Security_Number__c= null;
        c.product__c = 'Point Of Need';
        c.SSN__c = '';
        Account a = new Account();
        a.Name = 'TEST';
        a.Billingstreet = l.street;
        a.BillingCity = l.City;
        a.BillingState = l.state;
        a.BillingPostalCode = l.Postalcode;
        a.BillingCountry = l.Country;
        a.Phone = l.Phone;
        a.Type = 'Customer';

        insert a;
        update c;
        l.Authorization_to_pull_credit__c = true;
        
        update l;
        
        String res = LeadContInterface.InterfacetoLdCnt(c.Id);
        
        
    }
    



}