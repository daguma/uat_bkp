@isTest
public class CollectionsCasesAssignmentHelperTest{

    @isTest static void validates_constructor() {
        Test.startTest();
        CollectionsCasesAssignmentHelper helper = new CollectionsCasesAssignmentHelper();
        Test.stopTest();

        System.assertNotEquals(null, helper.dmc1ExclusionsList);
    }

    @isTest static void validates_ignore_contract(){
        Test.startTest();
        CollectionsCasesAssignmentHelper helper = new CollectionsCasesAssignmentHelper();
        Test.stopTest();

        System.assert(helper.ignoreContract('a1uU0000002s6CfIAI'));
        System.assert(!helper.ignoreContract('a1uU0000004mUZWIA2_xxxxx'));
    }
}