@isTest
public class PayfonePhnVerificationControllerTest {
    public static testMethod void testPayfonePhnVerification(){
        Contact con = TestHelper.createContact();
        con.Phone = '9815555555';
        update con;
        
        TestHelper.createPayfoneSettings();
        
        ApexPages.StandardController stdCntrlr = new ApexPages.StandardController(con);
        
        PageReference pageRef = Page.PayfonePhoneVerification;
        pageRef.getParameters().put('id', String.valueOf(con.Id));
        Test.setCurrentPage(pageRef);
        
        PayfonePhnVerificationController payfone = new PayfonePhnVerificationController(stdCntrlr);
        payfone.doPhoneVerification();
    }
}