@isTest
public class ProxyApiCreditBRMSResponseTest {
    @isTest static void validate() {
        
        List<ProxyApiCreditReportBrmsErros> errors = new List<ProxyApiCreditReportBrmsErros>();
        ProxyApiCreditReportBrmsErros error = new ProxyApiCreditReportBrmsErros();
        error.errorMessage = 'error msg';
        error.errorDetails = 'error details';
        errors.add(error);
        
        ProxyApiLexisNexisResponse lxsNxs = new ProxyApiLexisNexisResponse();
        
        ProxyApiCreditBRMSResponse brmsRes = new ProxyApiCreditBRMSResponse();
        
        brmsRes.lexisNexisResponse = lxsNxs;
        
        brmsRes.rulesExecutionStartDateTime = 1234567;
        system.assertEquals(1234567, brmsRes.rulesExecutionStartDateTime );
        
        brmsRes.rulesExecutionStopDateTime = 4567678;
        system.assertEquals(4567678, brmsRes.rulesExecutionStopDateTime );
        
        brmsRes.sessionId = '123-abc-456-def-678-ghi';
        system.assertEquals('123-abc-456-def-678-ghi', brmsRes.sessionId );
        
        brmsRes.sequenceId = 'jkl-901-mno-rst';
        system.assertEquals('jkl-901-mno-rst', brmsRes.sequenceId );
        
        brmsRes.productId = 'Loan';
        system.assertEquals('Loan', brmsRes.productId );
        
        brmsRes.channelId = 'Partner';
        system.assertEquals('Partner', brmsRes.channelId );
        
        brmsRes.executionType = 'SP';
        system.assertEquals('SP', brmsRes.executionType );
        
        brmsRes.rulesList = new List<ProxyApiCreditReportRuleEntity> ();
        brmsRes.scoringModels = new List<ProxyApiCreditReportScoringModelEntity> ();
        brmsRes.h2oModels = new List<ProxyApiCreditReportH2OModels>();
        
        brmsRes.scorecardAcceptance = 'NO';
        system.assertEquals('NO', brmsRes.scorecardAcceptance );
        
        brmsRes.finalGrade= 'C1';
        system.assertEquals('C1', brmsRes.finalGrade );
        
        brmsRes.automatedDecision= '';
        //brmsRes.errorMessage= '';
        //brmsRes.errorDetails= '';
        
        brmsRes.ruleVersion= 'AB-F9-19-54-C1-D6-7D-72';
        system.assertEquals('AB-F9-19-54-C1-D6-7D-72', brmsRes.ruleVersion );
        
        brmsRes.emailAgeResponse = new EmailAgeResponseEntity ();
        brmsRes.clarityResponse = new ProxyApiClarityResponse ();
        brmsRes.errors = new List<ProxyApiCreditReportBrmsErros>();
        
        brmsRes.deBRMS_ContractStatus = 'Declined';
        system.assertEquals('Declined', brmsRes.deBRMS_ContractStatus );
        
        brmsRes.deSF_AcceptanceStatus = 'Yes';
        system.assertEquals('Yes', brmsRes.deSF_AcceptanceStatus );
        
        brmsRes.deApplicationRisk= '4';
        system.assertEquals('4', brmsRes.deApplicationRisk );
        
        brmsRes.deRuleViolated = '1';
        system.assertEquals('1', brmsRes.deRuleViolated);
        
        brmsRes.errorMessage = 'test';
        system.assertEquals('test', brmsRes.errorMessage);
        
        brmsRes.errorDetails = 'test';
        system.assertEquals('test', brmsRes.errorDetails);
        
        brmsRes.deStrategy_Type = 'test';
        system.assertEquals('test', brmsRes.deStrategy_Type);
        
    }
    
    
     @isTest Static void GiactResponseParseTest(){
         GiactResponseParse obj1 = new GiactResponseParse();       
         GiactResponseParse.PostInquiryResult obj3 = new GiactResponseParse.PostInquiryResult();
         obj3.AccountResponseCode = 'test'; 
         obj3.CreatedDate = 'test';
         obj3.CustomerResponseCode = 'test';
         obj3.ItemReferenceId = 'test';
         obj3.VerificationResponse = 'test';
         obj3.AccountAddedDate = 'test';
         obj3.AccountLastUpdatedDate = 'test';
         ProxyApiCreditBRMSResponse obj = new ProxyApiCreditBRMSResponse();
         obj.giactResponse = obj1;
         
 
    }
    
    
    @isTest static void validate_ruleList() {
        ProxyApiCreditBRMSResponse brmsRes = new ProxyApiCreditBRMSResponse();
        
        brmsRes.rulesList = new List<ProxyApiCreditReportRuleEntity> ();
        
        brmsRes.errors = new List<ProxyApiCreditReportBrmsErros>();
        ProxyApiCreditReportBrmsErros error = new ProxyApiCreditReportBrmsErros();
        error.errorMessage = 'error msg';
        error.errorDetails = 'error details';
        brmsRes.errors.add(error);
        
        ProxyApiCreditBRMSResponse brmsResErrors = new ProxyApiCreditBRMSResponse();
        
        ProxyApiCreditReportRuleEntity rule = new ProxyApiCreditReportRuleEntity();
        rule.deRuleNumber = '1';
        rule.deRuleName = 'test';
        rule.rulePriority = '333';
        rule.deRuleValue = '0';
        rule.approvedBy = 'system';
        rule.deQueueAssignment = 'test';
        rule.deRuleStatus = 'Fail';
        rule.deBRMS_ReasonCode = 'test';
        rule.deSequenceCategory = '4';
        rule.deRuleCategory = 'test';
        rule.deActionInstructions = 'test';
        rule.deHumanReadableDescription = 'test';
        
        brmsRes.rulesList.add(rule);
        
        ProxyApiCreditReportRuleEntity rule1 = new ProxyApiCreditReportRuleEntity();
        rule1.deRuleNumber = '2';
        rule1.deRuleName = 'test';
        rule1.rulePriority = '333';
        rule1.deRuleValue = '0';
        rule1.approvedBy = 'system';
        rule1.deQueueAssignment = 'test';
        rule1.deRuleStatus = 'Fail';
        rule1.deBRMS_ReasonCode = 'test';
        rule1.deSequenceCategory = '4';
        rule1.deRuleCategory = 'test';
        rule1.deActionInstructions = 'test';
        rule1.deHumanReadableDescription = 'test';
        
        brmsRes.rulesList.add(rule1);
        
        brmsRes.autoDecision = 'No';
        brmsRes.scAcceptance = 'N';
        brmsRes.ruleAttributesApprovedByName = 'system';
        brmsRes.rulesExecutionStartDateTime = 1234567;
        system.assertEquals(1234567, brmsRes.rulesExecutionStartDateTime );
        
        brmsRes.rulesExecutionStopDateTime = 4567678;
        system.assertEquals(4567678, brmsRes.rulesExecutionStopDateTime );
        
        //system.assertEquals(brmsRes.autoDecision,'Yes');
    }
    
    @isTest static void validate_assign() {
        ProxyApiCreditBRMSResponse brmsRes = new ProxyApiCreditBRMSResponse();
        
        brmsRes.rulesList = new List<ProxyApiCreditReportRuleEntity> ();
        
        ProxyApiCreditReportRuleEntity rule = new ProxyApiCreditReportRuleEntity();
        rule.deRuleNumber = '1';
        rule.deRuleName = 'test';
        rule.rulePriority = '333';
        rule.deRuleValue = '0';
        rule.approvedBy = 'system';
        rule.deQueueAssignment = 'test';
        rule.deRuleStatus = 'Fail';
        rule.deBRMS_ReasonCode = 'test';
        rule.deSequenceCategory = '4';
        rule.deRuleCategory = 'test';
        rule.deActionInstructions = 'test';
        rule.deHumanReadableDescription = 'test';
        brmsRes.rulesList.add(rule);
        
        ProxyApiCreditReportRuleEntity rule1 = new ProxyApiCreditReportRuleEntity();
        rule1.deRuleNumber = '2';
        rule1.deRuleName = 'test';
        rule1.rulePriority = '333';
        rule1.deRuleValue = '0';
        rule1.approvedBy = 'system';
        rule1.deQueueAssignment = 'test';
        rule1.deRuleStatus = 'Fail';
        rule1.deBRMS_ReasonCode = 'test';
        rule1.deSequenceCategory = '4';
        rule1.deRuleCategory = 'test';
        rule1.deActionInstructions = 'test';
        rule1.deHumanReadableDescription = 'test';
        
        brmsRes.rulesList.add(rule1);
        
        brmsRes.rulesExecutionStartDateTime = 1234567;
        system.assertEquals(1234567, brmsRes.rulesExecutionStartDateTime );
        
        brmsRes.rulesExecutionStopDateTime = 4567678;
        system.assertEquals(4567678, brmsRes.rulesExecutionStopDateTime );
        
        brmsRes.sessionId = '123-abc-456-def-678-ghi';
        system.assertEquals('123-abc-456-def-678-ghi', brmsRes.sessionId );
        
        brmsRes.sequenceId = 'jkl-901-mno-rst';
        system.assertEquals('jkl-901-mno-rst', brmsRes.sequenceId );
        
        brmsRes.productId = 'Loan';
        system.assertEquals('Loan', brmsRes.productId );
        
        brmsRes.channelId = 'Partner';
        system.assertEquals('Partner', brmsRes.channelId );
        
        brmsRes.executionType = 'SP';
        system.assertEquals('SP', brmsRes.executionType );
        
        brmsRes.scoringModels = new List<ProxyApiCreditReportScoringModelEntity> ();
        brmsRes.h2oModels = new List<ProxyApiCreditReportH2OModels>();
        
        brmsRes.scorecardAcceptance = 'NO';
        system.assertEquals('NO', brmsRes.scorecardAcceptance );
        
        brmsRes.finalGrade= 'C1';
        system.assertEquals('C1', brmsRes.finalGrade );
        
        brmsRes.automatedDecision= '';
        //brmsRes.errorMessage= '';
        //brmsRes.errorDetails= '';
        
        brmsRes.ruleVersion= 'AB-F9-19-54-C1-D6-7D-72';
        system.assertEquals('AB-F9-19-54-C1-D6-7D-72', brmsRes.ruleVersion );
        
        brmsRes.emailAgeResponse = new EmailAgeResponseEntity ();
        brmsRes.clarityResponse = new ProxyApiClarityResponse ();
        brmsRes.errors = new List<ProxyApiCreditReportBrmsErros>();
        
        brmsRes.deBRMS_ContractStatus = 'Declined';
        system.assertEquals('Declined', brmsRes.deBRMS_ContractStatus );
        
        brmsRes.deSF_AcceptanceStatus = 'Yes';
        system.assertEquals('Yes', brmsRes.deSF_AcceptanceStatus );
        
        brmsRes.deApplicationRisk= '4';
        system.assertEquals('4', brmsRes.deApplicationRisk );
        
        brmsRes.deRuleViolated = '1';
        system.assertEquals('1', brmsRes.deRuleViolated);
        
        brmsRes.errorMessage = 'test';
        system.assertEquals('test', brmsRes.errorMessage);
        
        brmsRes.errorDetails = 'test';
        system.assertEquals('test', brmsRes.errorDetails);
        
        brmsRes.ruleExecutionEndDateTime = system.now()-1;
        brmsRes.ruleExecutionStartDateTime = system.now()-1;
        
        DateTime dt = brmsRes.ruleExecutionEndDateTime;
        String name = brmsRes.ruleAttributesApprovedByName;
        DateTime dt1 = brmsRes.ruleExecutionEndDateTime;
        
        String decision = brmsRes.autoDecision;
        String acceptance = brmsRes.scAcceptance;
    }
}