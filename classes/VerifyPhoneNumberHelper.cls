/**
 * Author : Shweta Awasthi - Created on 05/02/2016
 * Helper Class to call the payfone APIs to return the responses wrt the provided phone number
*/
global class VerifyPhoneNumberHelper {
    
    public VerifyPhoneNumberHelper() {
       
    }
    
    //method to call th payfone APIs to get the verification response and save it to DB
    @future(callout=true)
    public static void doPhoneVerification(String contactId) {
        Contact con = allOperations(contactId);
        update con;
    }
    
    
    //get the contact record PAC-3 change in method
    public static List<Contact> getContactRec(String contactId){
        //get the contact record associated to Idology request record     
        return([select Id, ints__Social_Security_Number__c,Title,Time_at_Current_Address_dm__c,
                 Time_at_Current_Address__c, Salutation, ReportsToId, Previous_Employment_Start_Date__c,
                 Reset_Portal_Password__c, CreatePortalAccount__c, Previous_Employment_End_Date__c,
                 Point_Code__c, Others__c, OtherStreet, OtherState,
                 OtherPostalCode, OtherPhone, OtherLongitude, OtherLatitude, OtherCountry, OtherCity,
                 MailingLongitude,MailingLatitude, Loan_Amount__c, Lead__c, LeadSource,
                 Lead_Sub_Source__c, Lead_Provider_Source_Code__c,HomePhone, 
                 Fax, Employment_Start_Date__c, Employment_Duration__c,Department, Birthdate,
                 AssistantPhone, AssistantName, Annual_Income__c, AccountId , Employer_Name__c,
                 Employment_Verified_With__c, Show_Regrade_Warning__c,do_not_contact__c,
                 How_Did_You_Hear_About_Us__c, Part_Time_Job_Income__c, Second_Income_Type__c ,
                 Customer_Number__c, Work_Phone_Number__c, Work_Email_Address__c  , Status__c, Disposition__c,
                 Random_Number__c,Use_of_Funds__c,employment_Type__c,FirstName,LastName,MobilePhone,Address_Score__c,Email_Score__c,Name_Score__c,Qualifiers__c,CreatedDate, Payfone_Decisioning_Check__c,
                 Email,MailingCity,MailingState,Phone,Process_Date__c,Payfone_Response_Code__c,Is_Number_Verified__c,Phone_Processed__c,MailingCountry,MailingPostalCode,
                 MailingStreet,MatchCRM_Description__c,getIntelligence_Description__c,City_Score__c,Device_Tenure__c,
                 Device_Velocity__c,First_Name_Score__c,Is_First_Initial_Match__c,Is_Last_Initial_Match__c,Is_Middle_Initial_Match__c,
                 Is_Valid_Email_Address__c,Last_Name_Score__c,Line_Type__c,Middle_Name_Score__c,Operator_Name__c,Fraud_Status__c,
                 Operator_Tenure__c,Operator_Velocity__c,Path__c,Payfone_Signature__c,Phone_Number_Status__c,Phone_Number_Tenure__c,
                 Phone_Number_Velocity__c,Signature_Tenure__c,Sim_Tenure__c,Sim_Velocity__c,State_Score__c,getIntelligenceStatus__c,
                 matchCRMStatus__c,Status_Index__c,Street_SubPremise_Score__c,Street_Number_Score__c,Transaction_Id__c,Zip_Score__c,Kyc_mode__c,Payfone_Exception__c,
                 Trust_Score__c,Payfone_Reason_Codes__c,Payfone_Verified_Flag__c,Verify_Description__c //PAC-3
                 from Contact where Id = :contactId]);
    }
    
    //remove special characters fromany string on contact
    public static String normalizeString(String phoneNum) {
        String phnNumber = phoneNum.replaceAll('\\(','').replaceAll('\\)','').replaceAll('-','').replaceAll(' ','').trim(); 
        system.debug('--phnNumber--'+phnNumber);
        return phnNumber;
    }
    
    //method to return guid
    public static string getGUIDString() {
        //generate a uniques guid
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        system.debug(guid);
        return guid;
    }
    
    //restructuting of Payfone API
    public static contact allOperations(String contactId) {
        String verifyRequest = '',OauthToken,excep = '',verifyResponse = '';
        
        Contact con = new Contact();
        //get the contact record associated to Idology request record     
        List<Contact> contactDetails = getContactRec(contactId);
        if(contactDetails.size() > 0) con = contactDetails[0]; 
                    
        //get details from custom settings
        Payfone_Settings__c payfoneSettings = Payfone_Settings__c.getValues('Payfone Request Params');
        try {
            //check if there is any record present in custom settings
            if(payfoneSettings != null && con != null && (con.Phone != null || con.MobilePhone != null)) {
                
                OauthToken = getAccessToken(payfoneSettings); //PAC-3
                if(string.isnotempty(OAuthToken) && payfoneSettings.Call_Payfone__c) { //PAC-3
                    
                    String requestId = getGUIDString();
                    
                    //check if mobile number/phone is empty or not
                    String phnNumber = con.MobilePhone != null ? String.valueof(con.MobilePhone) : ( con.Phone != null ? String.valueof(con.Phone) : '');
                    
                    //PAC-3 ***Starts***
                    if(String.isnotempty(phnNumber)) {
                        phnNumber = normalizeString(phnNumber);
                        verifyRequest = getRequestForVerifyAPI(payfoneSettings, phnNumber, con, requestId);
                        Http hp = new http();
                        HttpRequest req = getVerifyRequest(OauthToken,requestId,verifyRequest,payfoneSettings);
                        HttpResponse res = hp.send(req);
                        verifyResponse = res.getBody();
                        
                        if(string.isnotempty(verifyResponse) && res.getStatusCode() == 200) {
                            con = parseVerifyResult(verifyResponse, con);
                            if (payfoneSettings.Call_Payfone__c) {
                                String payfoneResult = decisionUsingPayfone(con);
                                InsertPayfoneHistory(con,phnNumber ,con.Verify_Description__c,payfoneResult,verifyRequest,verifyResponse);
                            }
                        } else {
                            excep = string.valueof(res);
                            MelisaCtrl.DoLogging('phone verify',verifyRequest,string.valueof(res),null,con.Id);
                        }
                    }
                    //PAC-3 ***Ends***
                }
            }
            system.debug('----con---allOperations-'+con);
        }
        Catch(Exception ex) {
            excep = ex.getMessage();
        }
        finally{
            con.Payfone_Exception__c = '';
            //add exception to the field
            if(String.isEmpty(OauthToken) )
                con.Payfone_Exception__c += '/token API: Oauth token not found\n';  //PAC-3
            if(String.isEmpty(verifyResponse))
                con.Payfone_Exception__c += '/verify API: '+excep;   //PAC-3 
        }
        
        return con;
    }
    
    //PAC-3 ***starts***
    //method to return the request for verify API
    public static string getRequestForVerifyAPI(Payfone_Settings__c payfoneSettings,String phnNumber,Contact con,String requestId) {
        String baseURL = '/+1'+phnNumber+'/verify/v1?';
        String RequestURL  = 'firstName='+EncodingUtil.urlEncode(con.firstName,'UTF-8')+'&lastName='+EncodingUtil.urlEncode(con.lastName,'UTF-8')+'&address='+EncodingUtil.urlEncode(con.MailingStreet,'UTF-8')+'&extendedAddress=&city='+EncodingUtil.urlEncode(con.MailingCity,'UTF-8')+'&region='+EncodingUtil.urlEncode(con.MailingState,'UTF-8')+'&postalCode='+con.MailingPostalCode+'&lastVerified=&last=&dob=&emailaddress='+EncodingUtil.urlEncode(con.Email,'UTF-8')+'&details=true';
        return (baseURL + RequestURL);
    }  
    
    
    public static HttpRequest getTokenRequest(Payfone_Settings__c payfoneSettings) {
        HttpRequest request = new HttpRequest();
        request.setEndpoint(RunningInSandbox() ? payfoneSettings.Dev_OAuth_Token_API_URL__c : payfoneSettings.OAuth_Token_API_URL__c);
        request.setMethod('POST');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        
        String username = RunningInSandbox() ? payfoneSettings.Dev_User_Name__c : payfoneSettings.User_Name__c;
        String password = RunningInSandbox() ? payfoneSettings.Dev_Password__c : payfoneSettings.Password__c;
        String apiClientId = RunningInSandbox() ? payfoneSettings.Dev_APIClientId__c : payfoneSettings.APIClientId__c;
        
        request.setBody(
                'grant_type=password' +
                '&username=' + EncodingUtil.urlEncode(username, 'UTF-8') +
                '&password=' + EncodingUtil.urlEncode(password, 'UTF-8') +
                '&client_id=' + EncodingUtil.urlEncode(apiClientId, 'UTF-8')
            );
        system.debug('--token request--'+request);
        return request;
    }
    
    public static string parseTokenResponse(HttpResponse response) {
        if (response <> null && response.getStatusCode() == 200) {
            String JsonResponse = response.getBody();
            Map<String, String> responseParams = (Map<String, String>)JSON.deserialize(JsonResponse, Map<String, String>.class); 
            return (responseParams.containskey('access_token') ? responseParams.get('access_token') : '');
        } else return null;
        
    }
    
    //method to get the access token
    public static string getAccessToken(Payfone_Settings__c payfoneSettings) {
        String OAuthtoken;
        try {
            HttpRequest request = getTokenRequest(payfoneSettings);
            system.debug('--request--'+request); 
            
            if (Test.isRunningTest()) OAuthtoken = 'token';
            else {
                HttpResponse  response = new Http().send(request);
                System.debug('Payfone ("' + request.getEndpoint() + '") -> Response: ' + response.getStatusCode() + ' / ' + response.getStatus() );
                OAuthtoken = parseTokenResponse(response);
            }
        } Catch (Exception e) {
            System.debug('Catch Exception: ' + e.getMessage());
            WebToSFDC.notifyDev('ApiGetNewToken ERROR', 'Catch Exception: ' + e.getMessage() + '\nStack Trace:\n' + e.getStackTraceString());
        }
        
        return OAuthtoken;
    }
    
    //get the request for verify API
    public static HttpRequest getVerifyRequest(String OAuthToken,String requestId,String verifyRequest,Payfone_Settings__c payfoneSettings) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(((RunningInSandbox()) ? payfoneSettings.Dev_Verify_API_URL__c : payfoneSettings.Verify_API_URL__c) + verifyRequest);
        req.setHeader('accept', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + OAuthToken);
        req.setHeader('trust-score', 'true');
        req.setHeader('consent-status', 'optedIn');
        req.setHeader('request-id',requestId);
        return req;
    }
    
    //parsing the /verify result
    public static Contact parseVerifyResult(String respondeBody, Contact con) {
        if (String.isNotEmpty(respondeBody)) {
            con.Name_Score__c = 0;
            con.First_Name_Score__c = 0;
            con.Last_Name_Score__c = 0;
            con.Address_Score__c = 0;
            con.Street_Number_Score__c = 0;
            con.Trust_Score__c = 0;
            
            //Parsing the Json, get the root elements
            Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(respondeBody);
            if(root != null) {
                con.Verify_Description__c = (String) (root.containskey('description') ? root.get('description') : '');
                Integer status = (Integer) (root.containskey('status') ? root.get('status') : '99999');
                Map<String, Object> items = (Map<String, Object>)root.get('response');
                if(items != null) {
                    //get TransactionId
                    con.Transaction_Id__c = (String) (items.containskey('transactionId') ? items.get('transactionId') : '');
                    //get name object items
                    Map<String, Object> nameItems = items.containskey('name') ? (Map<String, Object>)items.get('name') : null;
                    if(nameItems != null) {
                        con.Name_Score__c = (Integer) (nameItems.containskey('nameScore') ? nameItems.get('nameScore') : '0');
                        con.First_Name_Score__c = (Integer) (nameItems.containskey('firstName') ? nameItems.get('firstName') : '0');
                        con.Last_Name_Score__c = (Integer) (nameItems.containskey('lastName') ? nameItems.get('lastName') : '0');
                        system.debug('--name score--'+con.Name_Score__c+'--fNscore--'+con.First_Name_Score__c+'--LNScore--'+con.Last_Name_Score__c);
                    }
                    //get address object items
                    Map<String, Object> addressItems = items.containskey('address') ? (Map<String, Object>)items.get('address') : null;
                    if(addressItems != null) {
                        con.Address_Score__c = (Integer) (addressItems.containskey('addressScore') ? addressItems.get('addressScore') : '0');
                        con.Street_Number_Score__c = (Integer) (addressItems.containskey('streetNumber') ? addressItems.get('streetNumber') : '0');
                        system.debug('--address score--'+con.Address_Score__c+'--street score--'+con.Street_Number_Score__c);
                    }
                    
                    con.Payfone_Verified_Flag__c = (Boolean) (items.containskey('verified') ? items.get('verified') : 'false');
                    con.Trust_Score__c = (Integer) (items.containskey('trustScore') ? items.get('trustScore') : '0');
                    con.Payfone_Reason_Codes__c = JSON.serialize(items.get('reasonCodes'));
                }
            }
        }
        return con;
    }
    
    
    //method for decisioning payfone result
    public static string decisionUsingPayfone(Contact con) {
        string resultMsg,payfoneResult = 'Fail';
        
        //set the last processed phone number and last processed date
        con.Phone_Processed__c = con.MobilePhone != null ? normalizeString(String.valueof(con.MobilePhone)) : ( con.Phone != null ? normalizeString(String.valueof(con.Phone)) : '');
        
        con.Process_Date__c = system.now(); 
        
        if((con.Trust_Score__c >= 700 && con.Payfone_Verified_Flag__c && con.last_Name_score__c >= 90 && con.first_name_score__c >= 90 && con.address_score__c >= 90 && con.Street_Number_Score__c >=90) || (con.Trust_Score__c >= 700 && con.Last_Name_Score__c >= 90)) payfoneResult = 'Pass';
        
        update con;
        return payfoneResult;
    }
    
    public static void InsertPayfoneHistory(Contact con, String phNum ,String resultMsg,String verfResult,String verify_Req,String verify_Res){
        try{
            system.debug('-payfone run history-name score--'+con.Name_Score__c+'--fNscore--'+con.First_Name_Score__c+'--LNScore--'+con.Last_Name_Score__c);
            system.debug('-payfone run history-address score--'+con.Address_Score__c+'--street score--'+con.Street_Number_Score__c);
            Payfone_Run_History__c payfoneTrack = new Payfone_Run_History__c();
            payfoneTrack.Contact__c   = con.Id;
            payfoneTrack.Phone_Number__c = phNum;
            payfoneTrack.Result_Message__c = resultMsg;
            payfoneTrack.Verification_Result__c = verfResult;
            payfoneTrack.First_Name_Score__c =  con.First_Name_Score__c;
            payfoneTrack.Last_Name_Score__c =  con.Last_Name_Score__c;
            payfoneTrack.Middle_Name_Score__c =  con.Name_Score__c;
            payfoneTrack.Street_Number_Score__c =  con.Street_Number_Score__c;
            payfoneTrack.Verify_API_Response__c = verify_Res;
            payfoneTrack.Trust_Score__c = con.Trust_Score__c;
            payfoneTrack.Address_Score__c = con.Address_Score__c;
            insert payfoneTrack;
            
            //do logging
            MelisaCtrl.DoLogging('phone verify',verify_Req,verify_Res,null,con.Id);
        
        }
        catch(Exception e){
            System.debug('Exception occured updating PayfoneRunHistory'+ e.getMessage());
        } 
    }
    
    //method to return payfone 2.0 skip Questions condition
    public static boolean payfoneSkipIdologyCriteria(Payfone_Settings__c payfoneSettings,Contact con) {
        boolean skipIdologyQuestions = false;
        String payfoneResult = '';
        
        Integer expiryDays = 0;
        if (payfoneSettings.Expiry_Days__c != null) {
            expiryDays = Integer.valueof(payfoneSettings.Expiry_Days__c);
            if (expiryDays == 0) expiryDays = expiryDays + 1;
        }
        
        Datetime beforeThirtyDays = DateTime.Now().addDays(-expiryDays);
        
        //check if there any old payfone result
        for (Payfone_Run_History__c payfonelist : [select id, Contact__c,Trust_Score__c,Verify_API_Response__c, Verification_Result__c, MatchCRM_Response__c, GetIntelligence_Response__c, createdDate from Payfone_Run_History__c //PAC-3
                                                   where Contact__c = :con.Id and CreatedDate > : beforeThirtyDays and Verification_Result__c = 'Pass' order by CreatedDate desc limit 1]) {
            payfoneResult = payfonelist.Verification_Result__c;
        }
        if(payfoneSettings.Payfone_In_Use__c && payfoneSettings.Call_Payfone__c && String.isnotempty(payfoneResult) && con.Trust_Score__c >= 700 && con.Payfone_Verified_Flag__c && con.last_Name_score__c >= 90 && con.first_name_score__c >= 90 && con.address_score__c >= 90 && con.Street_Number_Score__c >=90) skipIdologyQuestions = true;
        return skipIdologyQuestions;
    }
    
     /**
     * running In Sandbox
     * @return true is org is sandbox
     */
    public static Boolean RunningInSandbox() {
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
    //PAC-3 ***Ends***
}