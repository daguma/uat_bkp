@isTest
public class ClarityClearFraudStabilitiesTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearFraudStabilities ccf = new ClarityClearFraudStabilities();
        Test.stopTest();
        
        system.assertEquals(null ,ccf.clear_fraud_stability);
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearFraudStabilities ccf = new ClarityClearFraudStabilities();
        Test.stopTest();
        ClarityClearFraudStability ccf1 = new ClarityClearFraudStability();
        
        ccf1.name = 'test';
        ccf1.one_minute_ago  = 1;
        ccf1.ten_minutes_ago  = 1;
        ccf1.one_hour_ago  = 1;
        ccf1.twentyfour_hours_ago  = 1;
        ccf1.seven_days_ago  = 1;
        ccf1.fifteen_days_ago  = 1;
        ccf1.thirty_days_ago  = 1;
        ccf1.ninety_days_ago  = 1;
        ccf1.threesixtyfive_days_ago  = 1;
        
        ccf.clear_fraud_stability = new list<ClarityClearFraudStability>();
        ccf.clear_fraud_stability.add(ccf1);
        
        System.assertEquals(1,ccf.clear_fraud_stability.size());
    }
}