@isTest
public class process_ContactUpdClsTest{
    /* INC 546 */
    public static testMethod void updateApplicationTest(){
    
    Account partnerAct_cs = new Account(name='Credit Smart',Type='Partner',Is_Parent_Partner__c = true,Partner_Sub_Source__c='CreditSmart',Contact_Restricted_Partner__c=true);
    insert partnerAct_cs;
    Account partnerAct_lt = new Account(name='Lending Tree',Type='Partner',Is_Parent_Partner__c = true,Partner_Sub_Source__c='LendingTree',Contact_Restricted_Partner__c=false);
    insert partnerAct_lt;
    Contact con = new Contact(FirstName='Test',LastName='Contact',LeadSource = 'Partner',Old_Lead_Sub_Source__c = 'CreditSmart',Lead_Sub_Source__c = 'LendingTree');
    insert con;
    genesis__Applications__c app = new genesis__Applications__c ();
    app = LibraryTest.createApplicationTH();
    app.genesis__Contact__c = con.Id;
    app.Selected_Offer_Id__c = null;
    update app;
    List<Contact> conList = new List<Contact>();
    conList.add(con);
    process_ContactUpdCls.updateApplication(conList);
    genesis__Applications__c updatedApp = [Select Partner_Account__c,genesis__Status__c from  genesis__Applications__c  where id =: app.Id];
    System.assertEquals(partnerAct_lt.Id,updatedApp.Partner_Account__c);
    System.assertEquals('Credit Qualified',updatedApp.genesis__Status__c);
    }
    
    public static testMethod void updateApplicationBulkTest(){
    
    Account partnerAct_cs = new Account(name='Credit Smart',Type='Partner',Is_Parent_Partner__c = true,Partner_Sub_Source__c='CreditSmart',Contact_Restricted_Partner__c=true);
    insert partnerAct_cs;
    Account partnerAct_lt = new Account(name='Lending Tree',Type='Partner',Is_Parent_Partner__c = true,Partner_Sub_Source__c='LendingTree',Contact_Restricted_Partner__c=false);
    insert partnerAct_lt;
    Contact con1 = new Contact(FirstName='Test',LastName='Contact',LeadSource = 'Partner',Old_Lead_Sub_Source__c = 'CreditSmart',Lead_Sub_Source__c = 'LendingTree');
    insert con1;
    Contact con2 = new Contact(FirstName='Test',LastName='Contact',LeadSource = 'Partner',Old_Lead_Sub_Source__c = 'CreditSmart',Lead_Sub_Source__c = 'LendingTree');
    insert con2;
    genesis__Applications__c app1 = new genesis__Applications__c ();
    app1 = LibraryTest.createApplicationTH();
    app1.genesis__Contact__c = con1.Id;
    app1.Selected_Offer_Id__c = null;
    update app1;
    genesis__Applications__c app2 = new genesis__Applications__c ();
    app2 = LibraryTest.createApplicationTH();
    app2.genesis__Contact__c = con2.Id;
    app2.Selected_Offer_Id__c = null;
    update app2;
    List<Contact> conList = new List<Contact>();
    conList.add(con1);
    conList.add(con2);
    process_ContactUpdCls.updateApplication(conList);
    genesis__Applications__c updatedApp1 = [Select Partner_Account__c,genesis__Status__c from  genesis__Applications__c  where id =: app1.Id];
    System.assertEquals(partnerAct_lt.Id,updatedApp1.Partner_Account__c);
    System.assertEquals('Credit Qualified',updatedApp1.genesis__Status__c);
    genesis__Applications__c updatedApp2 = [Select Partner_Account__c,genesis__Status__c from  genesis__Applications__c  where id =: app2.Id];
    System.assertEquals(partnerAct_lt.Id,updatedApp2.Partner_Account__c);
    System.assertEquals('Credit Qualified',updatedApp2.genesis__Status__c);
    }
    /* INC 546 */
}