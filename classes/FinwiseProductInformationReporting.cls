public with sharing class FinwiseProductInformationReporting{

  @InvocableMethod 
  public static void executeProductInformationAPI(List<string> prodIds){
      executeAPIInternal(prodIds);
  }
   
   @future (callout=true)      
   public static void executeAPIInternal(List<string> prodIds){
       list<logging__c> loggingList = new list<logging__c>();
       TimeZone tZone = UserInfo.getTimeZone(); 
       string prodId;
     try{
       list<loan__Loan_Product__c> products = [select id,name,CreatedDate,loan__Min_Interest_Rate__c,loan__Max_Interest_Rate__c
                                           ,loan__Min_Number_of_Installments__c,loan__Max_Number_of_Installments__c 
                                           from loan__Loan_Product__c where id IN: prodIds];
                                           
        FinwiseDetails__c FinwiseParams = FinwiseDetails__c.getInstance(); 
        for(loan__Loan_Product__c prod: products){
            prodId = prod.id;
            JSONGenerator generator = JSON.createGenerator(false);      
            generator.writeStartObject();                                  
            generator.writeStringField('APRRange', string.ValueOf(prod.loan__Min_Interest_Rate__c)+'-'+string.ValueOf(prod.loan__Max_Interest_Rate__c));
            generator.writeStringField('CompanyName', 'LendingPoint');           
            integer createdDatetZoneOffset = tZone.getOffset(prod.CreatedDate); 
            
            /***8-8-2017 New Changes****/          
          //  generator.writeStringField('CreationDate','\\/Date('+prod.CreatedDate.getTime()+createdDatetZoneOffset+')\\/'); 
           generator.writeNumberField('TargetMonth', Date.Today().month());   
           generator.writeNumberField('TargetYear', Date.Today().year()); 
          /***8-8-2017 New Changes****/  
                     
            generator.writeStringField('DBANames', 'LendingPoint');
            generator.writeStringField('Description', 'LendingPoint core lending product');       
            generator.writeNumberField('Id', 0);
            generator.writeStringField('InterestRateRange', string.ValueOf(prod.loan__Min_Interest_Rate__c)+'-'+string.ValueOf(prod.loan__Max_Interest_Rate__c));
            /***4743/4741 -  Product ID Modification for FinWise API - Start***/
            string ProductID = FinwiseParams <> null ? ((generalPurposeWebServices.isPointOfNeed(prod.name) || generalPurposeWebServices.isSelfPayProduct(prod.name)) ? FinwiseParams.Finwise_MedicalProductId__c : FinwiseParams.Finwise_LendingPartnerProductId__c) : '';   //API-160
            if(string.isNotEmpty(ProductID )) generator.writeNumberField('LendingPartnerProductId', integer.ValueOf(ProductID));
            /***4743/4741 -  Product ID Modification for FinWise API - End***/ 
             
            generator.writeNumberField('MinimumFICO', integer.ValueOf(FinwiseParams.Minimum_FICO__c));                   
            generator.writeStringField('OriginationFeeRange', FinwiseParams.Origination_Fee_Range__c);
            generator.writeStringField('OtherFees', 'Late,NSF');
            generator.writeStringField('ProductsOffered', 'LOAN');
            generator.writeStringField('TermRangeInMonths', string.ValueOf(prod.loan__Min_Number_of_Installments__c)+'-'+string.ValueOf(prod.loan__Max_Number_of_Installments__c));
            generator.writeStringField('WebsiteUsed', 'https://www.lendingpoint.com/');      
 
            string  reqBody = generator.getAsString(); 
            system.debug('============reqBody ====='+reqBody );
            reqBody = reqBody.unescapeJava();             
            HttpRequest req = new HttpRequest();          
            string Url = FinwiseParams.Finwise_Base_URL__c+'/monthlyreporting/'+FinwiseParams.Finwise_product_Key__c+'/productinformation';
            req.setEndpoint(url);
            req.setMethod('POST'); 
            req.SetBody(reqBody);  
            req.setHeader('Content-Type', 'application/json');                     
            Http http = new Http();
            HttpResponse response;
          if(!Test.isRunningTest()){
              response = http.send(req);
                
          }else{
              response = new HTTPResponse();                    
              response.SetBody(TestHelper.createDomDocNew().toXmlString());               
          }
            string RespToLog = 'Response: '+response+'\n Response Body: '+response.getBody(); 
          
           if(response.getStatusCode() == 200){              
               if(Finwise.parseStatus((String)response.getBody()) <> 'Success'){                                                       
                   loggingList.add(getLoggingForProd('FinwiseProductInformationReporting Unsuccessful on Date: '+System.today(), prodId, reqBody, RespToLog));
               }
           }else{                             
               loggingList.add(getLoggingForProd('FinwiseProductInformationReporting Error', prodId, reqBody,RespToLog));
           }
                    
            system.debug('============response====='+response);
            system.debug('============body====='+response.getbody());        
       }    
     }catch(exception exc){
          system.debug('===e============'+exc);
           string ExcpNoteBody = 'FinwiseProductInformationReporting Exception:  '+exc+' at Line No. '+exc.getLineNumber();                  
           loggingList.add(getLoggingForProd('FinwiseProductInformationReporting Exception', prodId,'', ExcpNoteBody));                             
           //WebToSFDC.notifyDev('FinwiseProductInformationReporting Exception',  exc.getMessage() + ' line ' + (exc.getLineNumber()) + '\n' + exc.getStackTraceString());  
      } 
      finally{
             if(loggingList.size() > 0){ insert loggingList; } 
      }
   
   }
   
  
   
     public static Logging__c getLoggingForProd(string webServiceName,string prodId, string request, string response){
        Logging__c Log = new Logging__c(Webservice__c = webServiceName,API_Request__c = request,API_Response__c = response,Product__c = prodId); 
        return Log;
    } 

}