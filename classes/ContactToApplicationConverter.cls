Global class ContactToApplicationConverter {
    
    Webservice static String ContactToAppWithIdologyVerification(String contactID) {

        Boolean bPassedIDology = false;

        List< IDology_Request__c > lIDo = [Select ID,
                                           Verification_Result__c
                                           from IDology_Request__c
                                           where Contact__c = : contactID and Verification_Result__c = 'Pass'
                                                   order by CreatedDate desc LIMIT 1];

        if (lIdo.size() > 0) {
            bPassedIDology = true;
        }

        return ContactToApp(contactID);
    }
    
    public static String ContactToApp(String contactID,boolean check) {
        return ContactToApp(contactID, check, null);
    }
    
    public static String ContactToApp(String contactID,boolean check, Account pAcc) {
        String result = ProcessContactSubmission(contactID, false, '', pAcc);
        if (String.valueOf(result).substring(0,3) == '006' && check)
        {
            Unique_Code_Hit__c uh = new Unique_Code_Hit__c();
            Contact c = [SELECT Id, Point_Code__c, LeadSource, Lead_Sub_Source__c, Campaign_ID__c, Use_of_Funds__c FROM Contact WHERE Id =: contactID];
            uh.Code__c = c.Point_Code__c;
            uh.LeadSource__c = c.LeadSource;
            uh.Sub_Source__c = c.Lead_Sub_Source__c;
            uh.Campaign_Id__c = c.Campaign_ID__c;
            uh.Use_of_Funds__c = c.Use_of_Funds__c;
            uh.ManualSubmission__c = true;
            uh.Contact__c = c.Id;
            uh.Opportunity__c = result;
            system.debug('--ContactToApp:uh--'+uh);
            insert uh;
        }
        return result;
  }
    Webservice static String ContactToApp(String contactID) {
        return ContactToApp(contactID, true);
        
    }
    
    Webservice static String ContactToAppRenewal(String contactID) {
        LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
        if ( UserInfo.getProfileId() == cuSettings.System_Admin_Profile_ID__c) {
            return ProcessContactSubmission(contactID, true, '', null);
        } else
            return 'Only Admins can run a renewal for now.\n';
    }

     Webservice static String ProcessContactSubmission(String contactID, boolean allowRenewal, String LoanAccountID ) {
         return ProcessContactSubmission(contactID, allowRenewal, LoanAccountID, null);
     }

     public static String ProcessContactSubmission(String contactID, boolean allowRenewal, String LoanAccountID, Account pAcc) {
        Contact c;
        boolean CreditPulled = false;
        String appID;
        if (!SubmitApplicationService.checkExistanceByContact(contactID, pAcc) && !allowRenewal) {
            return 'You cannot submit a new application at this time because other the customer has had an application '  
                    + 'created in the last 30 days, or, an application was declined within the last 180 days  or has crossed the limit of funded applications.';
        }

        List<Contact> lcon =  [Select Id, AccountId, Previous_Employment_End_Date__c,
                               Previous_Employment_Start_Date__c, Firstname, Lastname, Email, birthdate, Point_Code__c,
                               ints__Social_Security_Number__c, Phone, Time_at_current_address__c, SSN__c, Product__c,
                               MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Annual_Income__c,
                               Use_of_Funds__c, Others__c, Lead_Sub_Source__c, Loan_Amount__c, Employment_start_date__c, 
                               Lead__c from Contact where id = : contactID ];

        if (lcon.size() > 0) {
            c =  lcon[0];

            String valid = validate(c, pAcc);

            if (!valid.equalsIgnoreCase('true')) {
                return valid;
            }
        } else {
            return 'Contact could not be found\n';
        }

        Savepoint sp = Database.setSavepoint();
        if (c.AccountId == null) LeadToContactConvertor.doLeadConversion(null, null, c.Id);

        try {

            SubmitApplicationService submit = new SubmitApplicationService();

            appID = submit.submitApplication(c.Id, pAcc);

            System.debug('appID: ' + appID);


            if (String.isNotEmpty(appID)  && allowRenewal) {
                List<loan__Loan_Account__c> openContracts = [select Id, Opportunity__c from loan__Loan_Account__c
                                                    where loan__Contact__c =: c.Id and
                                                            loan__Loan_Status__c in ('Active - Good Standing', 'Active - Bad Standing') ORDER BY CreatedDate desc LIMIT 1 ];
                if (!String.isEmpty(LoanAccountID))
                {
                    for (loan__Loan_Account__c oCont:  [select Id, Opportunity__c from loan__Loan_Account__c
                                                        where Id = : LoanAccountID LIMIT 1 ]) {

                            for (Opportunity apps : [Select Id, Contract_Renewed__c from Opportunity
                                                                  where Id =: appId LIMIT 1]) {
                                apps.Contract_Renewed__c =  oCont.id;
                                update apps;
                            }
                    }
                }

            }
            return appID;

        } catch (Exception e) {
            Database.rollback(sp);

            return 'Error in Application submission. ' + e.getMessage() + '\n';
        }
    }
    
    public static String validate(Contact c) {
        return validate (c, null);
    }

    public static String validate(Contact c, Account pAcc) {
        String result = '';
        Account newPartnerAct = pAcc != null ? pAcc : WebToSFDC.getPartnerAccount(c.Lead_Sub_Source__c );
        boolean isPointOfNeed = generalPurposeWebServices.isPointOfNeed(c.product__c, newPartnerAct), //SM-438 changed the methodname and variable Name            
                    isSelfPayProduct = generalPurposeWebServices.isSelfPayProduct(c.product__c),
                    isEzVerify = generalPurposeWebServices.isEzVerify(c.Lead_Sub_Source__c);

        if (String.isEmpty(c.FirstName))
            result += 'Firstname is Empty\n';

        if (String.isEmpty(c.LastName))
            result += 'Lastname is Empty\n';

        if (c.birthdate == null)
            result += 'Date of Birth is Empty\n';

        if (c.Employment_Start_Date__c  == null)
            result += 'Employment Start Date is Empty\n';

        if (String.isEmpty(c.SSN__c))
            result += 'SSN is Empty\n';

        if (String.isEmpty(c.MailingStreet))
            result += 'Street in Address Information is Empty\n';

        if (String.isEmpty(c.MailingCity))
            result += 'City in Address Information is Empty\n';

        if (String.isEmpty(c.MailingState))
            result += 'State in Address Information is Empty\n';

        if (String.isEmpty(c.MailingPostalCode))
            result += 'PostalCode in Address Information is Empty\n';

        if (c.Loan_Amount__c == null)
            result += 'Loan Amount is Empty\n';

        if (c.Annual_Income__c  == null)
            result += 'Annual Income is Empty\n';

        if (c.Time_at_current_Address__c == null)
            result += 'Time at Current Address is Empty\n';

        if (!CustomLPValidations.validState(c.MailingState, c.Lead_Sub_Source__c,  isPointOfNeed || isSelfPayProduct || isEzVerify))
            result += 'State is not a permitted one\n';

        if (String.isEmpty(result))
            result = 'true';

        return result;
    }
}