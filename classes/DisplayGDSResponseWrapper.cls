public with sharing class DisplayGDSResponseWrapper {
    @AuraEnabled public String ruleNumber {get;set;}
    @AuraEnabled public String ruleName {get;set;}
    @AuraEnabled public Boolean isSoftPass {get;set;}
    @AuraEnabled public Boolean isHardPass {get;set;}
    @AuraEnabled public String ruleCategory {get;set;}
    @AuraEnabled public String description {get;set;}
    @AuraEnabled public String actionInstructions {get;set;}
    @AuraEnabled public Integer rulePriority {get;set;}
    @AuraEnabled public Integer applicationRisk {get;set;}
    @AuraEnabled public Boolean showSoftPullEmptyBox {get;set;}
    @AuraEnabled public Boolean showHardPullEmptyBox {get;set;}
    @AuraEnabled public String contractStatus{get;set;}
    @AuraEnabled public String sequenceCategory;
    @AuraEnabled public String approvedBy {get;set;}
    @AuraEnabled public String colorCode {get;set;}
    @AuraEnabled public String colorTheme {get;set;}
    @AuraEnabled public String softPullRuleValue {get;set;}
    @AuraEnabled public String hardPullRuleValue {get;set;}
    @AuraEnabled public String deQueueAssignment {get;set;}
    @AuraEnabled public String approvedByDisplayText {
        get
        {
            String result = '';
           // if (!approvedBy.equals('system'))
                return result = approvedBy;

            return result;
        }
    }
}