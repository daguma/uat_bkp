@isTest 
private class DocusignServiceTest {

        //changes for 3197 
        // here hard pull is executed for eziverify apps if
        // 1) product is 'Medical' as set in custom setting.
        // 2) new parameter 'doHardPull' is true
        // 3) if product exist at application level,use this product
        //else use contact product
        //add test custom setting
        @testSetup static void createTestData() {
        
         ezVerify_Settings__c ezSettings = new ezVerify_Settings__c();
         ezSettings.Default_Interest_Rate__c=3.99;
         ezSettings.Product_Name_Dev__c='MedicalTest';
         ezSettings.EZVerify_Base_URL__c='https://test.ezverify.me/api/lend';
         ezSettings.DL_Income_Variance__c = 10;
         ezSettings.EZVerify_Link_Expiration_Hours__c = 48;
         insert ezSettings ;
         
         if ([select Id from MaskSensitiveDataSettings__c].size() == 0 ) {
            MaskSensitiveDataSettings__c maskSettings = new MaskSensitiveDataSettings__c();
            maskSettings.MaskAllData__c = false;
            insert maskSettings;
         }
         
         //Dummy Contact
        /* contact cntct= new contact();
         cntct.Lastname='Contact-APP-F';
         cntct.Annual_Income__c =10000;
         cntct.Product__c='MedicalTest';
         cntct.Firstname='TestFirst';
         cntct.MailingState='GA';
         insert cntct;
         CreateApplicationWithContactAndProduct(cntct.id,'MedicalTest'); */
          
                   
        }
        //changes for 3197 
        //Create App for contact
        Private static void CreateApplicationWithContactAndProduct(Id contactId,string productName)
        {
           loan__Loan_Product__c prdct=new loan__Loan_Product__c();
           prdct.name=productName;
           //insert prdct;
        
          //Dummy Application
           opportunity aplcn= new opportunity(); 
           aplcn.Expected_Start_Date__c=Date.today();
           aplcn.Expected_Close_Date__c=Date.today();
           aplcn.Term__c=2015;
           aplcn.Payment_Amount__c=2011;
           aplcn.Payment_Frequency__c='MONTHLY';
           aplcn.Expected_First_Payment_Date__c=Date.today();
           aplcn.Amount=7600;
           aplcn.Fee__c=400;
           aplcn.ACH_Bank_Name__c='Dummy Bank Test';
           aplcn.Effective_APR__c = 28.46654;         
           aplcn.Contact__c= contactId;
           aplcn.Name = 'Opp';
           aplcn.CloseDate = system.today();
           aplcn.StageName = 'Qualification';
           aplcn.Type = 'New';
                          
           //aplcn.Lending_Product__c= prdct.id;        
           insert aplcn; 
        }
                               
        
    
        
        
        //changes for 3197 
        //Docusign-ApplicationTags test//
         Private static void docuSign_AppId_Tags_With_AppId(){
             //Dummy Contact
         contact cntct= new contact();
         cntct.Lastname='Contact-APP-F';
         cntct.Annual_Income__c =10000;
         cntct.Product__c='MedicalTest';
         cntct.Firstname='TestFirst';
         cntct.MailingState='GA';
         insert cntct;
         CreateApplicationWithContactAndProduct(cntct.id,'MedicalTest'); 
            
           opportunity aplcn=new opportunity();
           aplcn=[select id,LeadSource__c from opportunity where ACH_Bank_Name__c='Dummy Bank Test' LIMIT 1];
            
           if(aplcn!=null)
           {
           
               Map<string,string> ParamsMap = new Map<string,string>();
               ParamsMap.put('appId',aplcn.id);
               string Inputparam = Json.serializepretty(ParamsMap);
               
               //Case: product='Medical' and no param for doHardPull in request data
               DocusignService.DocuSignAppIdTags(Inputparam);
               
               
               //Case: product='Medical' and doHardPull true
               ParamsMap.put('doHardPull','true');
               Inputparam = Json.serializepretty(ParamsMap);
               DocusignService.DocuSignAppIdTags(Inputparam);
               
               //aplcn.Lending_Product__c=null;
               //update aplcn;
               //DocusignService.DocuSignAppIdTags(Inputparam);            
              
          }
       }
   
       //changes for 3197 
       //Docusign-ApplicationTags test//
       Private static void docuSign_AppId_Tags_With_ContactId(){
            
          contact cntct1= new contact();
         cntct1.Lastname='Contact-APP-F';
         cntct1.Annual_Income__c =10000;
         cntct1.Product__c='MedicalTest';
         cntct1.Firstname='TestFirst';
         cntct1.MailingState='GA';
         insert cntct1;
         CreateApplicationWithContactAndProduct(cntct1.id,'MedicalTest');
         
          Contact cntct=new Contact();
          cntct=[select id from Contact where LastName='Contact-APP-F' LIMIT 1];
           if(cntct!=null){
           
           Map<string,string> ParamsMap = new Map<string,string>();
           ParamsMap.put('contactId',cntct.id);
           string Inputparam = Json.serializepretty(ParamsMap);
           
          
           //Case: product='Medical' and no param for doHardPull in request data
           DocusignService.DocuSignAppIdTags(Inputparam);
       
           
            //Create second app for contact//recent app is selected
            CreateApplicationWithContactAndProduct(cntct.id,'MedicalTest2');
           
           //Case: product='Medical' and no param for doHardPull in request data and more than one app
           //recent created app is selected
           ParamsMap = new Map<string,string>();
           ParamsMap.put('contactId',cntct.id);
           Inputparam = Json.serializepretty(ParamsMap);  
           DocusignService.DocuSignAppIdTags(Inputparam);
           
           //Case: product='Medical' and doHardPull true
           ParamsMap.put('doHardPull','true');
           Inputparam = Json.serializepretty(ParamsMap);
           DocusignService.DocuSignAppIdTags(Inputparam);
           }
           
   }
  
    @isTest static void saves_payment_info_app_null(){
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', null);
        ReqMap.put('paymentMethod', 'Monthly');
        ReqMap.put('bankName', 'Dummy Bank');
        ReqMap.put('prefferdPaymentDate', String.Valueof(Date.today()));
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.savePaymentInfo(inputParam); 
        
        //System.assert(result.containsIgnoreCase('Application Id is null'));
    }
    
    @isTest static void saves_payment_info_invalid_date(){
        
        opportunity app = LibraryTest.createApplicationTH();
        
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', app.id);
        ReqMap.put('paymentMethod', 'Monthly');
        ReqMap.put('bankName', 'Dummy Bank');
        ReqMap.put('prefferdPaymentDate', String.Valueof(Date.today()));
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.savePaymentInfo(inputParam); 
        
        System.assert(result.containsIgnoreCase('Invalid Date'));
    }

    @isTest static void saves_payment_info(){
        
        opportunity app = LibraryTest.createApplicationTH();
        
        Offer__c off = new Offer__c();
        off.Repayment_Frequency__c = 'Monthly';
        off.Effective_APR__c = 20;
        off.Term__c = 24;
        off.Payment_Amount__c = 254;
        off.Selected_Date__c = Date.today();
        off.loan_Amount__c = 5000;
        off.Total_Loan_Amount__c  = 5250;
        off.Fee__c = 250;
        off.IsCustom__c = true;
        off.Fee_Percent__c = 5;
        off.FirstPaymentDate__c = Date.today().addDays(15);
        off.Installments__c = 24;
        off.Effective_APR__c = 3;
        off.opportunity__c = app.Id;
        off.IsSelected__c = True;
        insert off;
        
        Date prefferedDate = Date.today().addDays(15);
        String prefferedDateString = prefferedDate.month() + '/' + prefferedDate.day() + '/' + prefferedDate.year();
        
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', app.id);
        ReqMap.put('paymentMethod', 'Monthly');
        ReqMap.put('bankName', 'Dummy Bank');
        ReqMap.put('prefferdPaymentDate', prefferedDateString);
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.savePaymentInfo(inputParam); 

        System.assert(result.containsIgnoreCase('success'));
    }
    
    @isTest static void inserts_note_after_regrade(){
        
        opportunity app = LibraryTest.createApplicationTH();
        
        app.Was_application_re_graded__c = true;
        update app;
        
        Integer notesBefore = [SELECT COUNT() FROM Note];
        
        DocusignService.NoteOnNewContractAfterRegrade(app.id); 
      
        System.assert([SELECT COUNT() FROM Note] > notesBefore);
    }
    
    @isTest static void saves_docusign(){
        
        Merchant_Portal_Welcome_Email_Mapping__c m = new Merchant_Portal_Welcome_Email_Mapping__c();
        m.Name = 'Onboarding_confirmation_email';
        m.Email_Template__c = 'Onboarding_confirmation_email';
        m.Sender_Email__c = 'noreply@lendingpoint.com';
        insert m;
        
        opportunity app = LibraryTest.createApplicationTH();
        Account acc1 = LibraryTest.createAccount('TEST', 'Test');
        acc1.Channel__c = 'Point Of Need';
        update acc1;
        app.Partner_Account__c = acc1.id;
        update app;
        
        contact cntct= new contact();
        cntct.Lastname='Contact-APP-F';
        cntct.Annual_Income__c =10000;
        cntct.Product__c='MedicalTest';
        cntct.Firstname='TestFirst';
        cntct.MailingState='GA';
        cntct.Is_Business_Owner__c = 'Yes';
        cntct.Email = 'abctest@test.com';
        cntct.AccountID = acc1.id;
        insert cntct;
 
        Blob body = Blob.valueOf('dummybody');
        String dummyBody = EncodingUtil.base64Encode(body);
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', app.id);
        ReqMap.put('recordId', acc1.id);
        ReqMap.put('contentType', 'pdf');
        ReqMap.put('fileName', 'Test file');
        ReqMap.put('fileContent', dummyBody);
        ReqMap.put('description', 'Its a dummy file');
        
        Integer attachsBefore = [SELECT COUNT() FROM Attachment];
        
        String parameters = Json.serializepretty(ReqMap);
        String result = DocusignService.SaveDocusign(parameters);
        
        Map<string,string> ReqMap1 = new Map<string,string>();
        ReqMap1.put('appId', null);
        ReqMap1.put('contentType', 'pdf');
        ReqMap1.put('fileName', 'Test file');
        ReqMap1.put('fileContent', dummyBody);
        ReqMap1.put('description', 'Its a dummy file');
        String parameters1 = Json.serializepretty(ReqMap1);
        String result1 = DocusignService.SaveDocusign(parameters1);
        
        Map<string,string> ReqMap2 = new Map<string,string>();
        Account acc = LibraryTest.createAccount('TestName','TestNameCo');
        acc.Channel__c = 'Point Of Need';
        update acc;
        ReqMap2.put('recordId', acc.Id);
        ReqMap2.put('contentType', 'pdf');
        ReqMap2.put('fileName', 'Test file');
        ReqMap2.put('fileContent', dummyBody);
        ReqMap2.put('description', 'Its a dummy file');
        String parameters2 = Json.serializepretty(ReqMap2);
        String result2 = DocusignService.SaveDocusign(parameters2);
        DocusignService.savePaymentInfo(parameters2);
        
        System.assertEquals('DocuSign', [SELECT Contract_Received_Through__c FROM opportunity WHERE Id =: app.id LIMIT 1].Contract_Received_Through__c);
        System.assert([SELECT COUNT() FROM Attachment] > attachsBefore);
    }
    
    @isTest static void docusign_app_id_tags(){
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', null);
        ReqMap.put('contactId', null);
        ReqMap.put('paymentMethod', 'Monthly');
        ReqMap.put('bankName', 'Dummy Bank');
        ReqMap.put('prefferdPaymentDate', String.Valueof(Date.today()));
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.DocuSignAppIdTags(inputParam); 
        
        System.assert(result.containsIgnoreCase('Loan Application Id/Contact Id/Account Id is null or blank'));
    }
    
    @isTest static void docusign_app_id_tags_app(){
        
        opportunity app = LibraryTest.createApplicationTH();
        
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', app.Id);
        ReqMap.put('contactId', app.contact__c);
        ReqMap.put('paymentMethod', 'Monthly');
        ReqMap.put('bankName', 'Dummy Bank');
        ReqMap.put('prefferdPaymentDate', String.Valueof(Date.today()));
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.DocuSignAppIdTags(inputParam); 
        
        System.assert(result.containsIgnoreCase('success'));
        docuSign_AppId_Tags_With_AppId();
        
        Account acc = LibraryTest.createAccount('TestNameTwo','TestNameCoTwo');
        ReqMap.put('appId', null);
        ReqMap.put('recordId', acc.Id);
        ReqMap.put('contactId', null);
        ReqMap.put('paymentMethod', 'Monthly');
        ReqMap.put('bankName', 'Dummy Bank');
        ReqMap.put('prefferdPaymentDate', String.Valueof(Date.today()));
        String result1 = DocusignService.DocuSignAppIdTags(Json.serializepretty(ReqMap)); 
        System.assert(result.containsIgnoreCase('success'));
    }
    
    @isTest static void docusign_app_id_tags_contact(){
        
        opportunity app = LibraryTest.createApplicationTH();
        
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', null);
        ReqMap.put('contactId', app.contact__c);
        ReqMap.put('paymentMethod', 'Monthly');
        ReqMap.put('bankName', 'Dummy Bank');
        ReqMap.put('prefferdPaymentDate', String.Valueof(Date.today()));
        
        LibraryTest.createBrmsConfigSettings(1,true);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.DocuSignAppIdTags(inputParam); 
        
        System.assert(result.containsIgnoreCase('success'));
        docuSign_AppId_Tags_With_ContactId();
    }
    
    @isTest static void docusign_attachment_tags(){
     
        opportunity app = LibraryTest.createApplicationTH();
        
        Attachment att= new Attachment();
        blob bodyblob = blob.ValueOf('dummybody');
        att.Name= 'Dummy_10001';
        att.Body =bodyblob;
        att.ContentType = 'pdf';
        att.Description='dummyfile';
        att.parentid = app.id;
        insert att; 
        
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', app.Id);
        ReqMap.put('envelopeId', '10001');        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.DocuSignAttachmentTags(inputParam); 
        
        Map<string,string> ReqMap1 = new Map<string,string>();
        ReqMap1.put('appId', '');
        ReqMap1.put('envelopeId', '10001');
        String inputParam1 = Json.serializepretty(ReqMap1);
        String result1 = DocusignService.DocuSignAttachmentTags(inputParam1);       
                
        System.assert(result.containsIgnoreCase('success'));
    }
    
    @isTest static void docusign_attachment_tags_without_attachment(){
     
        opportunity app = LibraryTest.createApplicationTH();
        
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', app.Id);
        ReqMap.put('envelopeId', '10001');
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.DocuSignAttachmentTags(inputParam); 
        
        System.assert(result.containsIgnoreCase('No result found'));
        
        Account acc = LibraryTest.createAccount('TestNameThree','TestNameCoThree');
        ReqMap.put('recordId', acc.Id);
        ReqMap.put('envelopeId', '10001');
        
        String inputParam1 = Json.serializepretty(ReqMap);
        String result1 = DocusignService.DocuSignAttachmentTags(inputParam1); 
        System.assert(result1.containsIgnoreCase('No result found'));
    }
    
    @isTest static void docusign_attachment_tags_app_null(){
     
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', null);
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.DocuSignAttachmentTags(inputParam); 
        
        System.assert(result.containsIgnoreCase('Application Id or Envelope Id is null'));
    }
    
    @isTest static void send_docusign_url_email_test(){
        Account acc = LibraryTest.createAccount('TestNameTwo','TestNameCoTwo');
        acc.RMC_Email__c ='noreply@lendingpoint.com';
        update acc;
        Merchant_Portal_Welcome_Email_Mapping__c m = new Merchant_Portal_Welcome_Email_Mapping__c();
        m.Name = 'Consumer Docusign Template';
        m.Email_Template__c = 'Contract_Email';
        m.Sender_Email__c = 'noreply@lendingpoint.com';
        insert m;
        
        Merchant_Portal_Welcome_Email_Mapping__c m1 = new Merchant_Portal_Welcome_Email_Mapping__c();
        m1.Name = 'Merchant Onboarding Contract Email';
        m1.Email_Template__c = 'Send_Merchant_Onboarding_Docusign_Link';
        m1.Sender_Email__c = 'noreply@lendingpoint.com';
        insert m1;
        
        Map<string,string> ReqMap = new Map<string,string>();
        ReqMap.put('appId', null);
        ReqMap.put('url', 'test.url.com');
        ReqMap.put('encryptedAppId', null);
        
        String inputParam = Json.serializepretty(ReqMap);
        String result = DocusignService.sendDocusignURLviaEmail(inputParam); 
        
        opportunity app = LibraryTest.createApplicationTH();
        Map<string,string> ReqMapOne = new Map<string,string>();
        ReqMapOne.put('appId', app.Id);
        ReqMapOne.put('url', 'test.url.com');
        ReqMapOne.put('encryptedAppId', app.Id);
        ReqMapOne.put('emailAddress', 'test@test.com');
        ReqMapOne.put('encryptedRecordId', acc.Id);
        ReqMapOne.put('recordId', acc.Id);
        
        String inputParamOne = Json.serializepretty(ReqMapOne);
        String resultOne = DocusignService.sendDocusignURLviaEmail(inputParamOne); 
        
        ReqMapOne.put('recordId', acc.Id);
        ReqMapOne.put('emailAddress', 'test@test.com');
        ReqMapOne.put('url', 'test.url.com');
        ReqMapOne.put('encryptedRecordId', app.Id);
        
        String inputParamTwo = Json.serializepretty(ReqMapOne);
        String resultTwo = DocusignService.sendDocusignURLviaEmail(inputParamTwo); 
    }
    
}