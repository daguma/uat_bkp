global class ProxyApiCreditReportSegmentTypeEntity {
    public Integer id {get; set;}
    public String fieldName {get; set;}
    public Integer length {get; set;}
    public Integer startPosition {get; set;}
    public Integer endPosition {get; set;}
    public String type {get; set;}
    public String description {get; set;}
    public Integer parentId {get; set;}
    public Boolean isDisplayed {get; set;}
    public Boolean isLengthValue {get; set;}
    public Boolean isApplyLength {get; set;}
}