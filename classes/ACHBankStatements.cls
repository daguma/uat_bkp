global class ACHBankStatements {

    global class BankStatementsPeriod {

        public String selectedP1 { get; set; }
        public String selectedP2 { get; set; }
        public String selectedP3 { get; set; }
        public String selectedP4 { get; set; }
        public Boolean is60DayStatement { get; set; }

        public Boolean isTitle { get; set; }

        public BankStatementsPeriod(String selectedP1, String selectedP2, String selectedP3, String selectedP4, Boolean is60DayStatement, Boolean isTitle) {

            this.selectedP1 = selectedP1;
            this.selectedP2 = selectedP2;
            this.selectedP3 = selectedP3;
            this.selectedP4 = selectedP4;
            this.is60DayStatement = is60DayStatement;
            this.isTitle = isTitle;
        }


        public BankStatementsPeriod(Boolean isTitle) {

            this.isTitle = isTitle;

            if (isTitle) {
                this.selectedP1 = '0';
                this.selectedP2 = '0';
                this.selectedP3 = '0';
                this.selectedP4 = '0';
                this.is60DayStatement = false;
            } else {
                Integer currentYear = System.Today().year();
                Integer currentMonth = System.Today().month() - 1;


                if (currentMonth == 0) {
                    currentMonth = 12;
                    currentYear--;
                }


                this.selectedP1 = String.valueOf(currentYear + GetMonthLeadingZeros(currentMonth--)); //201501
                if (currentMonth == 0) {
                    currentMonth = 12;
                    currentYear--;
                }

                this.selectedP2 = String.valueOf(currentYear + GetMonthLeadingZeros(currentMonth--)); //201412
                if (currentMonth == 0) {
                    currentMonth = 12;
                    currentYear--;
                }

                this.selectedP3 = String.valueOf(currentYear + GetMonthLeadingZeros(currentMonth--)); //201411
                if (currentMonth == 0) {
                    currentMonth = 12;
                    currentYear--;
                }

                this.selectedP4 = String.valueOf(currentYear + GetMonthLeadingZeros(currentMonth--)); //201410

                this.is60DayStatement = false;
            }
        }



        private String GetMonthLeadingZeros(Integer month) {
            return month < 10 ? '0' + String.valueof(month) : String.valueof(month);
        }

    }

    global class BankStatementsDeposits {

        public String labelName { get; set; }
        public String valForP1 { get; set; }
        public String valForP2 { get; set; }
        public String valForP3 { get; set; }
        public String valForP4 { get; set; }

        public String dateForP1 { get; set; }
        public String dateForP2 { get; set; }
        public String dateForP3 { get; set; }
        public String dateForP4 { get; set; }



        public BankStatementsDeposits(String valForP1, String valForP2, String valForP3, String valForP4, String dateForP1, String dateForP2, String dateForP3, String dateForP4) {
            this.labelName = 'Payroll Deposits';
            this.valForP1 = valForP1;
            this.valForP2 = valForP2;
            this.valForP3 = valForP3;
            this.valForP4 = valForP4;


            this.dateForP1 = dateForP1;
            this.dateForP2 = dateForP2;
            this.dateForP3 = dateForP3;
            this.dateForP4 = dateForP4;

        }



        public BankStatementsDeposits() {
            this.labelName = 'Payroll Deposits';
        }

    }

    public class BankStatementsInfo implements Comparable {

        public String labelName { get; set; }
        public String numForP1 { get; set; }
        public String numForP2 { get; set; }
        public String numForP3 { get; set; }
        public String numForP4 { get; set; }

        public String amtForP1 { get; set; }
        public String amtForP2 { get; set; }
        public String amtForP3 { get; set; }
        public String amtForP4 { get; set; }

        public Boolean showsCurrency { get; set; }
        public Boolean showsNumber { get; set; }

        public Boolean showsInAllP { get; set; }

        public Integer id { get; set; }

        public BankStatementsInfo(Integer id, String numForP1, String numForP2, String numForP3, String numForP4, String amtForP1, String amtForP2, String amtForP3, String amtForP4) {

            SetLabel(id);

            this.numForP1 = numForP1;
            this.numForP2 = numForP2;
            this.numForP3 = numForP3;
            this.numForP4 = numForP4;

            this.amtForP1 = amtForP1;
            this.amtForP2 = amtForP2;
            this.amtForP3 = amtForP3;
            this.amtForP4 = amtForP4;
        }


        public BankStatementsInfo(Integer id) {
            SetLabel(id);
        }


        private void SetLabel(Integer id) {
            this.id = id;

            this.showsCurrency = false;
            this.showsNumber = false;
            this.showsInAllP = true;

            if (id == 1010) {
                this.labelName = 'Average Balance';
                this.showsCurrency = true;
            }
            if (id == 1020) {
                this.labelName = 'Total of Overdrafts';
                this.showsCurrency = true;
                this.showsNumber = true;
            }
            if (id == 1030) {
                this.labelName = 'Total of YTD Overdrafts';
                this.showsNumber = true;
                this.showsCurrency = true;
                this.showsInAllP = false;
            }
            if (id == 1040) {
                this.labelName = 'Total of NSF Fees';
                this.showsCurrency = true;
                this.showsNumber = true;
            }
            if (id == 1050) {
                this.labelName = 'Total of YTD NSF Fees';
                this.showsNumber = true;
                this.showsCurrency = true;
                this.showsInAllP = false;
            }


            if (id == 1060) {
                this.labelName = 'Number of Negative Days';
                this.showsNumber = true;
            }


            if (id == 1070) {
                this.labelName = 'Number of Days with Deposits/Credits';
                this.showsNumber = true;
            }

            if (id == 1080) {
                this.labelName = 'Total Deposits';
                this.showsNumber = true;
                this.showsCurrency = true;
            }
            /*if (id == 1090) {
                this.labelName = 'Total Withdrawals';
                this.showsNumber = true;
                this.showsCurrency = true;
            }*/
            if (id == 2000) {
                this.labelName = 'Available Balance';
                this.showsCurrency = true;
            }

            /*if (id == 2010) {
                this.labelName = 'Current Balance';
                this.showsCurrency = true;
                this.showsInAllP = false;
            }*/

        }


        public Integer compareTo(Object compareTo) {
            BankStatementsInfo compare = (BankStatementsInfo)compareTo;

            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (this.id > compare.id) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (this.id < compare.id) {
                // Set return value to a negative value.
                returnValue = -1;
            }

            return returnValue;
        }
    }

    public List<ACHBankStatements.BankStatementsPeriod> InitializeBankStatementsPeriodList() {
        List<ACHBankStatements.BankStatementsPeriod> result = new List<ACHBankStatements.BankStatementsPeriod>();
        //result.add(new ACHBankStatements.BankStatementsPeriod(true));
        result.add(new ACHBankStatements.BankStatementsPeriod(false));
        return result;
    }

    public List<ACHBankStatements.BankStatementsPeriod> InitializeBankStatementsPeriodList(String xml) {
        try {
            List<ACHBankStatements.BankStatementsPeriod> result = new List<ACHBankStatements.BankStatementsPeriod>();

            Dom.Document doc = new Dom.Document();
            doc.load(xml);

            for (Dom.XMLNode node : doc.getRootElement().getChildElements()) {
                if (node.getName() == 'BankStatementsPeriod') {
                    ACHBankStatements.BankStatementsPeriod item = new ACHBankStatements.BankStatementsPeriod( node.getChildElement('selectedP1', null).getText(),
                            node.getChildElement('selectedP2', null).getText(),
                            node.getChildElement('selectedP3', null).getText(),
                            node.getChildElement('selectedP4', null).getText(),
                            Boolean.valueOf(node.getChildElement('is60DayStatement', null).getText()),
                            Boolean.valueOf(node.getChildElement('isTitle', null).getText())
                                                                                                            );

                    result.add(item);
                }
            }

            return result.size() == 1 ? result : InitializeBankStatementsPeriodList();
        }

        catch (Exception e) {
            return InitializeBankStatementsPeriodList();
        }

    }

    public List<ACHBankStatements.BankStatementsDeposits> InitializeBankStatementsDepositsList() {
        List<ACHBankStatements.BankStatementsDeposits> result = new List<ACHBankStatements.BankStatementsDeposits>();
        result.add(new ACHBankStatements.BankStatementsDeposits());
        return result;
    }

    public List<ACHBankStatements.BankStatementsDeposits> InitializeBankStatementsDepositsList(String xml) {
        try {
            List<ACHBankStatements.BankStatementsDeposits> result = new List<ACHBankStatements.BankStatementsDeposits>();

            Dom.Document doc = new Dom.Document();
            doc.load(xml);

            for (Dom.XMLNode node : doc.getRootElement().getChildElements()) {
                if (node.getName() == 'BankStatementsDeposits') {
                    ACHBankStatements.BankStatementsDeposits item = new ACHBankStatements.BankStatementsDeposits( node.getChildElement('valForP1', null).getText(),
                            node.getChildElement('valForP2', null).getText(),
                            node.getChildElement('valForP3', null).getText(),
                            node.getChildElement('valForP4', null).getText(),
                            node.getChildElement('dateForP1', null).getText(),
                            node.getChildElement('dateForP2', null).getText(),
                            node.getChildElement('dateForP3', null).getText(),
                            node.getChildElement('dateForP4', null).getText());
                    result.add(item);
                }
            }
            return result.size() == 1 ? result : InitializeBankStatementsDepositsList();
        }

        catch (Exception e) {
            return InitializeBankStatementsDepositsList();
        }

    }

    public List<ACHBankStatements.BankStatementsInfo> InitializeBankStatementsInfoList() {
        List<ACHBankStatements.BankStatementsInfo> result = new List<ACHBankStatements.BankStatementsInfo>();
        for (Integer intI : new Integer[]{1010, 1020, 1030, 1040, 1050, 1060, 1070, 1080, 2000}) {
            result.add(new ACHBankStatements.BankStatementsInfo(intI));
        }
        return result;
    }

    public List<ACHBankStatements.BankStatementsInfo> InitializeBankStatementsInfoList(String xml) {
        try {
            List<ACHBankStatements.BankStatementsInfo> result = InitializeBankStatementsInfoList(); // new List<ACHBankStatements.BankStatementsInfo>();


            Dom.Document doc = new Dom.Document();
            doc.load(xml);

            for (Dom.XMLNode node : doc.getRootElement().getChildElements()) {
                if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                    if (node.getName() == 'BankStatementsInfo') {

                        for (ACHBankStatements.BankStatementsInfo item : result) {
                            if (item.id == Integer.valueof(node.getChildElement('id', null).getText())) {
                                item.numForP1 = node.getChildElement('numForP1', null).getText();
                                item.numForP2 = node.getChildElement('numForP2', null).getText();
                                item.numForP3 = node.getChildElement('numForP3', null).getText();
                                item.numForP4 = node.getChildElement('numForP4', null).getText();

                                item.amtForP1 = node.getChildElement('amtForP1', null).getText();
                                item.amtForP2 = node.getChildElement('amtForP2', null).getText();
                                item.amtForP3 = node.getChildElement('amtForP3', null).getText();
                                item.amtForP4 = node.getChildElement('amtForP4', null).getText();
                                break;
                            }
                        }

                    }
                }
            }
            return result;
        }

        catch (Exception e) {
            return InitializeBankStatementsInfoList();
        }
    }


    public boolean IsLast3MonthsBankStatementsPopulated(List<ACHBankStatements.BankStatementsInfo> bankStatementList) {

        try {
            for (ACHBankStatements.BankStatementsInfo bs : bankStatementList) {

                system.debug('=====bs.numForP1.length==================' + bs.numForP1.length());
                if (bs.showsNumber && (bs.numForP1.length() == 0 || 
                        (bs.showsInAllP && (bs.numForP2.length() == 0 || bs.numForP3.length() == 0))
                    )) {
                    return false;
                }

                if (bs.showsCurrency && (bs.amtForP1.length() == 0 || 
                        (bs.showsInAllP && (bs.amtForP2.length() == 0 || bs.amtForP3.length() == 0))
                    )) {
                    return false;
                }
            }

        } catch (Exception e) {}
        return true;
    }

    public boolean IsLast3MonthsBankDepositsPopulated(List<ACHBankStatements.BankStatementsDeposits> bankDepositsList) {
        boolean Response = false;

        try {
            for (ACHBankStatements.BankStatementsDeposits dep : bankDepositsList) {
                if ((dep.valForP1.length() > 0 && dep.valForP2.length() > 0 && dep.valForP3.length() > 0)  && (dep.dateForP1.length() > 0 && dep.dateForP2.length() > 0 && dep.dateForP3.length() > 0)) {
                    Response = true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {}
        return Response;
    }


    public String GetXmlFromACHBankStatements( List<ACHBankStatements.BankStatementsPeriod> statementsPeriodList
            , List<ACHBankStatements.BankStatementsInfo> statementsInfoList
            , List<ACHBankStatements.BankStatementsDeposits> statementsDepositsList) {
        Applicants ac = new Applicants();
        XmlStreamWriter w = new XmlStreamWriter(); // Start preparing the XML
        w.writeStartDocument(null, '1.0'); //Start the <XML>
        w.writeStartElement(null, 'BankStatementsList', null);   // <BankStatementsList>

        for (ACHBankStatements.BankStatementsInfo item : statementsInfoList) {
            w.writeStartElement(null, 'BankStatementsInfo', null);  //   <BankStatementsInfo>

            ac.addElement(w, 'id', String.valueof(item.id));
            ac.addElement(w, 'numForP1', item.numForP1);
            ac.addElement(w, 'numForP2', item.numForP2);
            ac.addElement(w, 'numForP3', item.numForP3);
            ac.addElement(w, 'numForP4', item.numForP4);
            ac.addElement(w, 'amtForP1', item.amtForP1);
            ac.addElement(w, 'amtForP2', item.amtForP2);
            ac.addElement(w, 'amtForP3', item.amtForP3);
            ac.addElement(w, 'amtForP4', item.amtForP4);


            w.writeEndElement();                                  //    </BankStatementsInfo>
        }


        for (ACHBankStatements.BankStatementsPeriod item : statementsPeriodList) {
            w.writeStartElement(null, 'BankStatementsPeriod', null);  //   <BankStatementsPeriod>

            ac.addElement(w, 'selectedP1', item.selectedP1);
            ac.addElement(w, 'selectedP2', item.selectedP2);
            ac.addElement(w, 'selectedP3', item.selectedP3);
            ac.addElement(w, 'selectedP4', item.selectedP4);
            ac.addElement(w, 'is60DayStatement', '' + item.is60DayStatement);
            ac.addElement(w, 'isTitle', '' + item.isTitle);

            w.writeEndElement();                                  //    </BankStatementsPeriod>
        }


        for (ACHBankStatements.BankStatementsDeposits item : statementsDepositsList) {
            w.writeStartElement(null, 'BankStatementsDeposits', null);  //   <BankStatementsDeposits>

            ac.addElement(w, 'valForP1', item.valForP1);
            ac.addElement(w, 'valForP2', item.valForP2);
            ac.addElement(w, 'valForP3', item.valForP3);
            ac.addElement(w, 'valForP4', item.valForP4);
            ac.addElement(w, 'dateForP1', item.dateForP1);
            ac.addElement(w, 'dateForP2', item.dateForP2);
            ac.addElement(w, 'dateForP3', item.dateForP3);
            ac.addElement(w, 'dateForP4', item.dateForP4);

            w.writeEndElement();                                  //    </BankStatementsDeposits>
        }


        w.writeEndElement();                                    // </BankStatementsList>
        w.writeEndDocument();                           //</XML>

        String xmlOutput = w.getXmlString();
        w.close();
        return xmlOutput;
    }

    private Decimal getDecimal(List<ACHBankStatements.BankStatementsInfo> li, Integer pItemId) {
        try {
            for (ACHBankStatements.BankStatementsInfo item : li) {
                if (item.id == pItemId && item.amtForP1.length() > 0)
                    return decimal.valueOf(item.amtForP1.replaceAll(',', ''));
            }
        } catch (exception e) {}
        return null;
    }

    private Integer getInteger(List<ACHBankStatements.BankStatementsInfo> li, Integer pItemId) {
        try {
            for (ACHBankStatements.BankStatementsInfo item : li) {
                if (item.id == pItemId && item.numForP1.length() > 0)
                    return Integer.valueOf(item.numForP1.replaceAll(',', ''));
            }
        } catch (exception e) {}
        return null;
    }

    public Decimal GetCurrentBalance(List<ACHBankStatements.BankStatementsInfo> li) {
        return getDecimal(li, 2000);
    }


    public Integer GetOverdraftsNum(List<ACHBankStatements.BankStatementsInfo> li) {
        return getInteger(li, 1020);
    }
    public Decimal GetOverdraftsAmt(List<ACHBankStatements.BankStatementsInfo> li) {
        return getDecimal(li, 1020);
    }

    public Integer GetYTD_OverdraftsNum(List<ACHBankStatements.BankStatementsInfo> li) {
        return getInteger(li, 1030);
    }
    public Decimal GetYTD_OverdraftsAmt(List<ACHBankStatements.BankStatementsInfo> li) {
        return getDecimal(li, 1030);
    }

    public Decimal GetTotalDepositsAmt(List<ACHBankStatements.BankStatementsInfo> li) {
        return getDecimal(li, 1080);
    }

    public Integer GetNSFsNum(List<ACHBankStatements.BankStatementsInfo> li) {
        return getInteger(li, 1040);
    }
    public Decimal GetNSFsAmt(List<ACHBankStatements.BankStatementsInfo> li) {
        return getDecimal(li, 1040);
    }

    public Integer GetYTD_NSFsNum(List<ACHBankStatements.BankStatementsInfo> li) {
        return getInteger(li, 1050);
    }
    public Decimal GetYTD_NSFsAmt(List<ACHBankStatements.BankStatementsInfo> li) {
        return getDecimal(li, 1050);
    }
    public Decimal GetCurrentAvgDailyBalanceAmt(List<ACHBankStatements.BankStatementsInfo> li) {
        return getDecimal(li, 1010);
    }

    public Decimal GetAvgDailyBalanceAmt(List<ACHBankStatements.BankStatementsInfo> li) {
        try {
            Integer i = 0;
            Decimal avgDBAmt = 0.00;

            for (ACHBankStatements.BankStatementsInfo item : li) {
                if (item.id == 1010 && item.amtForP1.length() > 0) {
                    avgDBAmt += Decimal.valueOf(item.amtForP1.replaceAll(',', ''));
                    i++;
                }
                if (item.id == 1010 && item.amtForP2.length() > 0) {
                    avgDBAmt += Decimal.valueOf(item.amtForP2.replaceAll(',', ''));
                    i++;
                }
                if (item.id == 1010 && item.amtForP3.length() > 0) {
                    avgDBAmt += Decimal.valueOf(item.amtForP3.replaceAll(',', ''));
                    i++;
                }
                if (item.id == 1010 && item.amtForP4.length() > 0) {
                    avgDBAmt += Decimal.valueOf(item.amtForP4.replaceAll(',', ''));
                    i++;
                }
            }
            if (i > 0)
                return (avgDBAmt / i).setScale(2);
        } catch (exception e) {}
        return null;
    }

    public Decimal GetAvgNegativeDaysNum(List<ACHBankStatements.BankStatementsInfo> li) {
        try {
            Integer i = 0, avgNegativeDays = 0;

            for (ACHBankStatements.BankStatementsInfo item : li) {
                if (item.id == 1060 && item.numForP1.length() > 0) {
                    avgNegativeDays += Integer.valueOf(item.numForP1.replaceAll(',', ''));
                    i++;
                }
            }
            if (i > 0)
                return avgNegativeDays / i;
        } catch (exception e) {}
        return null;
    }

    public Boolean HasBankStatementsDeposits(List<ACHBankStatements.BankStatementsDeposits> li) {
        Integer i = 0;
        try {
            for (ACHBankStatements.BankStatementsDeposits item : li) {
                if (item.valForP1.length() > 0 && Decimal.valueOf(item.valForP1.replaceAll(',', '')) > 0.00)
                    i++;
                if (item.valForP2.length() > 0 && Decimal.valueOf(item.valForP2.replaceAll(',', '')) > 0.00)
                    i++;
                if (item.valForP3.length() > 0 && Decimal.valueOf(item.valForP3.replaceAll(',', '')) > 0.00)
                    i++;
                if (item.valForP4.length() > 0 && Decimal.valueOf(item.valForP4.replaceAll(',', '')) > 0.00)
                    i++;
            }
        } catch (exception e) {}
        return (i > 0) ;
    }

}