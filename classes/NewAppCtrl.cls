public with sharing class NewAppCtrl extends genesis.ApplicationWizardBaseClass {
    public genesis__Applications__c application {get; set;}
    public note notes {get; set;}
    public boolean showpanel {get; set;}
    public boolean hideattach {get; set;}
    public boolean HardcreditPullPending {get; set;}
    public Boolean hasCRInformation {get; set;}
    public String isReadOnly {get; set;}

    // #2659 - LEAD MONETIZATION: New Fields: Applications
    public Contact referralInfoCnt {get; set;}
    public genesis__Applications__c referralInfoApp {get; set;}
    public String referralEligibleReadOnly {get; set;}

    public NewAppCtrl(ApexPages.StandardController controller) {
        super(controller);
        showpanel = false;
        hideattach = true;
        isReadOnly = '';
        this.application = (genesis__Applications__c)controller.getRecord();
        // #2659 - LEAD MONETIZATION: New Fields: Applications
        this.referralInfoCnt = [SELECT  Referral_Eligible_Decline__c, 
                            Referral_Eligible_High_FICO__c, 
                            Referral_Status__c 
                    FROM Contact 
                    WHERE Id =: application.genesis__Contact__C ];
        this.referralInfoApp = [SELECT  Referral_APR__c,
                                        Referral_Date__c,
                                        Referral_Fee_Amount__c,
                                        Referral_Funding_Amount__c,
                                        Referral_Funding_Date__c,
                                        FinMkt_LoanId__c,
                                        Referral_Partner__c,
                                        Referral_Term__c,
                                        Referred__c
                                FROM genesis__Applications__c
                                WHERE Id =: application.Id];
        ApplicationsUtil AppUtil = new ApplicationsUtil(this.application);
        AppUtil.ValidatePartnerForApplication();
        isReadOnly = AppUtil.getReadOnlyClass(UserInfo.getProfileId());
        
        if (AppUtil.isDeclineByHardPull()) {
            createMessage(ApexPages.Severity.Warning, 'The application has been declined after the hard credit report was pulled. You cannot move forward with this application.');
        }

        // #2658 - LEAD MONETIZATION: New Fields: Contact
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        referralEligibleReadOnly = UserInfo.getProfileId() == prof.Id ? '' : 'readOnly';

    }

    public pagereference initialize() {
        HardcreditPullPending = true;
        ProxyApiCreditReportEntity creditReportEntity = CreditReport.getHardPullByEntityId(this.application.id);

        if (creditReportEntity != null) {
            if (creditReportEntity.errorMessage != null) {
                createMessage(ApexPages.Severity.Error, creditReportEntity.errorMessage);
                return null;
            } else {
                HardcreditPullPending = creditReportEntity.isCreateDateOnLast30Days() ? false : true;
            }
        }

        if (HardcreditPullPending)
            createMessage(ApexPages.Severity.Warning, 'You must do a Hard Credit Pull before Funding this application.');

        return null;
    }

    public Note mynote;

    Public Note getmynote() {
        mynote = new Note();
        return mynote;
    }

    Public Pagereference Savedoc() {
        String accid = this.application.id;

        Note a = new Note(parentId = accid, title = mynote.title, body = mynote.body);
        /* insert the attachment */
        insert a;
        return NULL;
    }

    Public Pagereference hidepanel() {
        showpanel = true;
        hideattach = false;
        return null;
    }

    Public void PullHardCreditReport() {
        try {
            ProxyApiCreditReportEntity creditReportEntity = CreditReport.get(application.id, true, true, true);

            if (creditReportEntity != null) {
                HardcreditPullPending = false;
            } else {
                createMessage(ApexPages.Severity.ERROR, 'Could not pull credit! <BR> ');
            }
        } catch (Exception e) {
            System.debug('ERROR ' + e.getMessage());
        }
    }

    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }

    // #2659 - LEAD MONETIZATION: New Fields: Applications
    public void saveReferralInfo() 
    {
        try 
        {
            if (this.referralInfoApp.Referred__c)
                this.referralInfoApp.Referral_Date__c = Datetime.now();
            else 
                this.referralInfoApp.Referral_Date__c = null;

            update this.referralInfoCnt;
            update this.referralInfoApp;
            createMessage(ApexPages.Severity.Confirm, 'Referral Partner Information saved!');
        }
        catch (Exception e)
        {
            createMessage(ApexPages.Severity.ERROR, 'An error has occurred while saving the data:<br>' + 
                            e.getMessage() + '<br>' + e.getStackTraceString());
        }
    }
}