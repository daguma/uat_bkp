public class IdologyUtil {

    public static String IDOLOGY_W_QUES = 'IDology; w/ challenge questions';
    public static String PAYFONE_WO_QUES = 'Payfone; challenge questions not asked';
    public static String PAYFONE_W_QUES = 'Payfone; w/out passing challenge questions';
    public static String IDOLOGY_WO_QUES = 'IDology; w/out challenge questions';
    
    public static IDology_Request__c SaveIDologyRequest(String name, String qualifiers, Integer numquestions, String result, String message, String ContactID, String debug) {
        IDology_Request__c request = new IDology_Request__c();
        request.Name = name;
        if (request.Contact__c == null)
            request.Contact__c = ContactID;
        request.Qualifiers__c = qualifiers;
        request.Questions_Received__c = numquestions;
        request.IDology_Result__c = result;
        request.Result_Message__c = message;
        request.Debug__c = debug;
        upsert request;
        return request;
        
        
    }
    
    public static Boolean runningInASandbox() {
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;

    }
    
   public static HTTPResponse CallApiToProcessQuestions(String idNumber, List<IDologyQuestion> IDologyQuestions, Boolean callNewEnterprise) {
    system.debug('--submit Questions--callNewEnterprise---'+callNewEnterprise);        
    HttpRequest req = new HttpRequest();
    req.setEndpoint('https://web.idologylive.com/api/idliveq-answers.svc');
    req.setMethod('POST');
    req.setBody(GetParametersAnswersForApi(idNumber,IDologyQuestions,callNewEnterprise));
    system.debug('--answers req---'+req);
//    addDebugValue('Request to validate IDology Questions initiated at: ' + System.now().format());
    Http http = new Http();
    HTTPResponse res = http.send(req);
    System.debug('--anser res--'+res.getBody());
    return res;
  }
    
     public static HTTPResponse CallApiToGetQuestions(Contact c,boolean callNewEnterprise) {
        System.debug('CallAPiTogetQuestions started!');
        system.debug('--request Questions--callNewEnterprise---'+callNewEnterprise);
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint('https://web.idologylive.com/api/idiq.svc');
        //payfone 2.0 changes END
        
        req.setMethod('POST');
        System.debug('CallApiToGetQuestions = ' + GetParametersForApi(c,callNewEnterprise));
        req.setBody(GetParametersForApi(c,callNewEnterprise));
        
        System.debug('Request to recover IDology Questions initiated at: ' + System.now().format());
        system.debug('--questions req---'+req);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        System.debug(res.getBody());
        return res;
    }


    public static String GetParametersAnswersForApi(String idNumber, List<IDologyQuestion> IDologyQuestions,boolean callNewEnterprise) {
        IDology_Settings__c settings = IDology_Settings__c.getOrgDefaults();
        String parameters = '';

        system.debug('--callNewEnterprise answers?--'+callNewEnterprise);
        
        parameters = 'username=' + settings.CIQ_UserName__c + '&password=' + settings.CIQ_Password__c + '&idNumber=' + idNumber; //MAINT-606 always call CIQ enterprise
        
        Integer i = 1;

        for (IDologyQuestion q : IDologyQuestions) {
            if (q.selectedAnswer != null && q.selectedAnswer != '- Not Answered -' && q.selectedAnswer != ''  && (q.skipped == null || q.skipped == 'no')) {
                parameters += '&question' + String.valueOf(i) + 'Type=' + q.typeQuestion;
                parameters += '&question' + String.valueOf(i++) + 'Answer=' + q.selectedAnswer;
            }
        }
        
        System.debug('Validate Answers parameters: ' + parameters);
        return parameters;
    }
    
     
    
    public class IDologyQuestion {
        @AuraEnabled public Integer no {get; set;}
        @AuraEnabled public String question {get; set;}
        @AuraEnabled public String typeQuestion = '';
        public List<SelectOption> listAnswers {get; set;}
        @AuraEnabled public List<String> AnswersList {get; set;}
        @AuraEnabled public String selectedAnswer {get; set;}
        @AuraEnabled public String skipped {get; set;}
        
        public IDologyQuestion(String question, String typeQuestion, Integer no) {
            this.question = question;
            this.typeQuestion = typeQuestion;
            listAnswers = new List<SelectOption>();
            AnswersList = new List<String>();
            listAnswers.add(new SelectOption('', '- Not Answered -'));
            AnswersList.add('Select Answer');
            this.selectedAnswer = '';
            this.no = no;
            this.skipped = 'no';
        }
    }

    public class IdologyResult {
        
        public String idNumber = '';
        public String summaryResult = '';
        public String result = '';
        public String noAnswers = '';
        public String iqResultMessage = '';
        public String iqResult = '';
        public String expectIdResult = '';
        public Integer numquestions = 0;
        public String qualifiers = '';
        public String error = '';
        public List<IdologyQuestion> Questions = new List<IdologyQuestion>();
        public IDologyResult(){}
        
    }    
    
    public static IDologyResult ProcessAnswersResultXml(Dom.XMLNode response) {
        IdologyResult theRes = new IdologyResult();
        for (Dom.XMLNode node : response.getChildElements()) {
            if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                if (node.getName() == 'summary-result') {
                    theRes.summaryResult = getText(node, 'message');
                } else


                if (node.getName() == 'results') {
                    theRes.result = getText(node, 'message');
                } else


                if (node.getName() == 'id-number') {
                    theRes.idNumber = node.getText();
                } else


                if (node.getName() == 'idliveq-error') {
                    theRes.error = getText(node, 'message');
                } else


                if (node.getName() == 'answers-received') {
                    theRes.noAnswers = node.getText();
                } else

                if (node.getName() == 'error') {
                    theRes.error = node.getText();
                } else

                if (node.getName() == 'idliveq-result') {
                    theRes.iqResultMessage = getText(node, 'message');
                } else


                
                if (node.getName() == 'iq-summary-result') {
                    if (node.getChildElement('message', null) == null) {
                        theRes.iqResult = node.getText();
                    } else {
                        theRes.iqResult = getText(node, 'message');
                    }
                }
            }
        }
        return theRes;
      
    }
    
    static String getText(Dom.XMLNode node, String fi) {
        return node.getChildElement(fi, null).getText();
    }
    
    public static IdologyResult ProcessResultXml(Dom.XMLNode response) {
        IdologyResult theRes = new IdologyResult();
        Set<string> options = new Set<string>();
        Set<string> commaSeparator = new Set<string>();
            for (ExpectId_Email_Settings__c expectEmailId : ExpectId_Email_Settings__c.getAll().values()){
            system.debug(expectEmailId.ExpectId_Key__c);
            options.add(expectEmailId.ExpectId_Key__c);
         }

         
        if (response != null) {

            for (Dom.XMLNode node : response.getChildElements()) {
                if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                    if (node.getName() == 'summary-result') {
                        theRes.summaryResult = getText(node, 'message');
                    } else


                    if (node.getName() == 'idliveq-error') {
                        theRes.error = getText(node, 'message');
                    } else


                    if (node.getName() == 'results') {
                        theRes.result = getText(node, 'message');
                    } else


                    if (node.getName() == 'qualifiers') {
                        for ( Dom.XMLNode qNode : node.getChildElements()) {

                            String key =  getText(qnode, 'key');
                            if(options.contains(key)){
                               
                             
                             if(!commaSeparator.contains('commaSeparator')){
                                commaSeparator.add('commaSeparator');
                                theRes.expectIdResult+=getText(qnode, 'message');
                             }
                             else{
                                theRes.expectIdResult+= ', '+getText(qnode, 'message');
                             }
                             
                            }
                            theRes.qualifiers += '<li>' + getText(qnode, 'message') + '</li>';
                        }
                    } else
                    if (node.getName() == 'error') {
                        theRes.error = node.getText();
                    } else


                    if (node.getName() == 'questions') {
                        for (Dom.XMLNode childNode : node.getChildElements()) {
                            if (childNode.getName() == 'question') {
                                IDologyQuestion q = new IDologyQuestion(childNode.getChildElement('prompt', null).getText(), childNode.getChildElement('type', null).getText(), theRes.Questions.size() + 1);
                                List<SelectOption> listIDologyAnswers = new List<SelectOption>();
                                List<String> AnswersList = new List<String>();
                                for (Dom.Xmlnode answerNodes : childNode.getChildElements()) {
                                    if (answerNodes.getName() == 'answer') {
                                        listIDologyAnswers.add(new SelectOption(answerNodes.getText(), answerNodes.getText()));
                                        AnswersList.add(answerNodes.getText());
                                    }
                                }
                                q.listAnswers = listIDologyAnswers;
                                q.AnswersList.addAll(AnswersList);
                                theRes.numquestions++;
                                theRes.Questions.add(q);
                                system.debug(' theRes.Questions:::'+ theRes.Questions);
                            }
                        }
                    } else


                    if (node.getName() == 'id-number') {
                        theRes.idNumber = node.getText();
                    }
                }
            }
            
      
        }
        return theRes;
    }    
    
    public static String GetParametersForApi(Contact c,Boolean callNewEnterprise) {
        IDology_Settings__c settings = IDology_Settings__c.getOrgDefaults();
        String parameters = '';
        system.debug('---new enterprise ?---'+callNewEnterprise);
        
        parameters = 'username=' + settings.CIQ_UserName__c +  '&password=' + settings.CIQ_Password__c ;  //MAINT-606 always call CIQ enterprise

        String FirstName = (c.FirstName != null) ? c.FirstName.trim() : '';
        if (FirstName.contains(' ')) FirstName = FirstName.substring(0, FirstName.lastIndexOf( ' ') );

        parameters += '&invoice=' + '';
        parameters += '&amount=' + '';
        parameters += '&shipping=' + '';
        parameters += '&tax=' + '';
        parameters += '&total=' + '';
        parameters += '&idType=' + '';
        parameters += '&idIssuer=' + '';
        parameters += '&idNumber=' + '';
        parameters += '&paymentMethod=' + '';
        parameters += '&firstName=' + FirstName;
        parameters += '&lastName=' + c.LastName;
        parameters += '&address=' + c.MailingStreet;
        parameters += '&city=' + c.MailingCity;
        parameters += '&state=' + c.MailingState;
        parameters += '&zip=' + c.MailingPostalCode;
        parameters += '&ssnLast4=' + (c.SSN__c != null && c.SSN__c.length() > 4 ? c.SSN__c.subString(c.SSN__c.length() - 4) : ''); //MAINT-606
        parameters += '&ssn=' + (String.isNotEmpty(c.SSN__c) ? c.SSN__c: '');
        
        parameters += '&dobMonth=' + (c.Birthdate != null ?  String.valueOf(c.Birthdate.month()).leftPad(2, '0') : '');
        parameters += '&dobDay=' + (c.Birthdate != null ? String.valueOf(c.Birthdate.day()).leftPad(2, '0') : '');
        parameters += '&dobYear=' + (c.Birthdate != null ? String.valueOf(c.Birthdate.year()) : '');
        parameters += '&ipAddress=' + '';
        parameters += '&email=' + (String.isNotEmpty(c.Email) ? c.Email : '');

        parameters += '&telephone=' + (c.Phone != null && c.Phone.length() > 0 ? c.Phone : '');
        parameters += '&sku=' + '';
        parameters += '&uid=' + '';
        parameters += '&altAddress=' + '';
        parameters += '&altCity=' + '';
        parameters += '&altState=' + '';
        parameters += '&altZip=' + '';
        return parameters;
    }
    
    
  public static  Dom.XMLNode GenerateTestingData1() {
    Dom.Document doc = new Dom.Document();
    String responseFail = '<?xmlversion="1.0"encoding="UTF-8"?><response><id-number>1207887314</id-number><summary-result><key>id.failure</key><message>FAIL</message></summary-result><results><key>result.match</key><message>IDLocated</message></results><qualifiers><qualifier><key>resultcode.address.does.not.match</key><message>Address Does Not Match</message></qualifier><qualifier><key>resultcode.street.number.does.not.match</key><message>Street Number Does Not Match</message></qualifier><qualifier><key>resultcode.street.name.does.not.match</key><message>Street Name Does Not Match</message></qualifier><qualifier><key>resultcode.state.does.not.match</key><message>State Does Not Match</message></qualifier><qualifier><key>resultcode.zip.does.not.match</key><message>ZIP Code Does Not Match</message></qualifier><qualifier><key>resultcode.yob.does.not.match</key><message>YOB Does Not Match</message></qualifier><qualifier><key>resultcode.ssn.issued.prior.to.dob</key><message>SSN Issued Priorto DOB</message></qualifier><qualifier><key>resultcode.address.location.alert</key><message>Address Location Alert</message></qualifier><qualifier><key>resultcode.data.strength.alert</key><message>Data Strength Alert</message></qualifier></qualifiers><idliveq-error><key>id.not.eligible.for.questions</key><message>Not Eligible For Questions</message></idliveq-error></response>';
    doc.load(responseFail);

    return doc.getRootElement();
  }

  public static  Dom.XMLNode GenerateTestingData2() {
    Dom.Document doc = new Dom.Document();
    String responseOk = '<?xml version="1.0" encoding="UTF-8"?><response><id-number>1208646854</id-number><summary-result><key>id.success</key><message>PASS</message></summary-result><results><key>result.match</key><message>ID Located</message></results><questions><question><prompt>Which of these roads is closest to your address on MALIBU DR?</prompt><type>geo.closest.major.road</type><answer>HILL AVE</answer><answer>CHARLES AVE</answer><answer>CANTON RD CONN</answer><answer>MASSACHUSETTS ST</answer></question><question><prompt>With which name are you associated?</prompt><type>alternate.names.phone</type><answer>QUINN</answer><answer>QUATTRONE</answer><answer>QUINTERO</answer><answer>None of the above</answer></question><question><prompt>How long have you been associated with the property at 1116 MALIBU DR?</prompt><type>time.at.current.address</type><answer>Less than 2 years</answer><answer>2 - 3 years</answer><answer>3 - 4 years</answer><answer>4 - 5 years</answer><answer>Over 5 years</answer><answer>None of the above</answer></question><question><prompt>Which number goes with your address on SW 97TH ST?</prompt><type>street.number</type><answer>16442</answer><answer>10411</answer><answer>2411</answer><answer>None of the above</answer></question><question><prompt>Which of the following people do you know?</prompt><type>fictional</type><answer>Gerardo Valverde</answer><answer>Jose Elizondo</answer><answer>Jorge Porras</answer><answer>Manuel Arias</answer></question></questions></response>';
    doc.load(responseOk);

    return doc.getRootElement();
  }

  public static Dom.XMLNode GenerateTestingData3() {
    Dom.Document doc = new Dom.Document();
    String responseOk = '<?xml version="1.0" encoding="UTF-8"?><response>    <id-number>1208646854</id-number>    <summary-result>        <key>id.success</key>        <message>PASS</message>    </summary-result>    <results>        <key>result.match</key>        <message>ID PASS</message>    </results> <answers-received>        <key></key>        <message>4</message>    </answers-received>  <idliveq-result>        <key></key>        <message>TEST OK</message>    </idliveq-result> <iq-summary-result>        <key></key>        <message>Summary OK</message>    </iq-summary-result> </response>';
    doc.load(responseOk);

    return doc.getRootElement();
  }
    
    
}