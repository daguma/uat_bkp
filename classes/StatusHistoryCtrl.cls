public with sharing class StatusHistoryCtrl extends genesis.ApplicationWizardBaseClass {

    //*********************
    //Variable Declaration
    //*********************
    List<LeadHistory> LeadHistory {get; set;}
    public string leadid {get; set;}
    public string Appid {get; set;}
    public boolean Sceneriotest {get; set;}
    List<LeadHistory> LeadHistoryOwnertemp {get; set;}
    public List<historyOwner> leadHistoryPopulated {get; set;}
    public List<historyOwner> appHistoryPopulated {get; set;}
    List<genesis__Applications__History> AppHistory {get; set;}
    List<genesis__Applications__History> AppHistorytest {get; set;}
    List<string> leadids = new List<string>();
    map<string, string> ownerMap = new map<string, string>();
    map<string, string> OwnerAppmap = new map<string, string>();
    map<string, string> userMap = new map<string, string>();
    map<string, string> userMapID = new map<string, string>();
    List<genesis__Applications__c> AppList = new List<genesis__Applications__c>();
    genesis__Applications__c app;
    public boolean ShowLead {get; set;}
    public boolean ShowApp {get; set;}
    public boolean ShowCont {get; set;}
    public string SFDCURL {get; set;}
    genesis__Applications__c currentApp;

    //***********
    //Constructor
    //***********
    public StatusHistoryCtrl(ApexPages.StandardController controller) {
        super(controller);
        Sceneriotest = false;
        SFDCURL = string.valueOf(system.URL.getSalesforceBaseUrl().toExternalForm());
        app = (genesis__Applications__c)controller.getrecord();

        currentApp = [select genesis__Contact__r.Lead__c, genesis__Contact__c, Owner.name  from genesis__Applications__c where id = :app.id]; //get current application
        Leadid   = currentApp.genesis__Contact__r.Lead__c; //get leadID from Application
        id contactId = currentApp.genesis__Contact__c; //get ContactID from Application

        //segration of logic
        getHistories();

        //get all the users details
        for (User use : [select id , name from user]) {
            userMap.put(use.id, use.name);
            userMapID.put(use.name, use.id);
        }
        system.debug('USERS' + userMap);



        AppList     = [select id, genesis__Contact__c, createddate, name, CreatedById, ownerid from genesis__Applications__c where genesis__Contact__c = : contactId];

        ShowLead  = LeadHistory.size() > 0 ? true : false;
        ShowApp  = AppHistory.size() > 0 ? true : false; //to render Application history section
        ShowCont = AppList.size()    > 0 ? true : false; //to render Contact history section

        ApplicationsUtil AppUtil = new ApplicationsUtil(this.app);
        AppUtil.ValidatePartnerForApplication();

    }


    //*********************
    //Get application history and lead history
    //*********************
    public void getHistories() {
        try {

            if (test.isRunningTest()) {
                if (Sceneriotest) {
                    LeadHistory = null;
                    LeadHistoryOwnertemp = null;
                } else {
                    LeadHistory = new List<LeadHistory>();
                    LeadHistoryOwnertemp = new List<LeadHistory>();
                    LeadHistory.add(new LeadHistory(LeadId = Leadid, Field = 'Status'));
                    LeadHistoryOwnertemp.add(new LeadHistory(LeadId = Leadid, Field = 'Owner'));
                    LeadHistory.add(new LeadHistory(LeadId = Leadid, Field = 'Status'));
                    LeadHistoryOwnertemp.add(new LeadHistory(LeadId = Leadid, Field = 'Owner'));
                }
            } else {
                LeadHistory = [select oldvalue, newvalue, field, createddate, CreatedById, LeadId from LeadHistory where field = 'Status' AND LeadId = :Leadid order by createddate desc]; // get lead history
                LeadHistoryOwnertemp = [SELECT CreatedById, CreatedDate, Field, Id, LeadId, NewValue, oldvalue FROM LeadHistory WHERE field = 'Owner' AND LeadId = :Leadid order by createddate desc];
            }

            if (test.isRunningTest()) {
                AppHistory = new List<genesis__Applications__History>();
                AppHistory.add(new genesis__Applications__History(ParentId = Appid, field = 'genesis__Status__c'));
                AppHistory.add(new genesis__Applications__History(ParentId = Appid, field = 'genesis__Status__c'));
                AppHistorytest = new List<genesis__Applications__History>();
                AppHistorytest.add(new genesis__Applications__History(ParentId = Appid, field = 'owner'));
                AppHistorytest.add(new genesis__Applications__History(ParentId = Appid, field = 'owner'));
            } else {
                AppHistory  = [select oldvalue, newvalue, field, createddate, CreatedById, ParentId   from genesis__Applications__History where field IN('genesis__Status__c', 'Fico_Reviewed__c') AND ParentId = : app.id order by createddate desc]; //get application history
                AppHistorytest = [select oldvalue, newvalue, field, createddate, CreatedById, ParentId   from genesis__Applications__History where field = 'owner' AND ParentId = : app.id order by createddate desc];
            }

            //for getting lead owner if no lead history for owner has changed
            for (LeadHistory ll : LeadHistory) {
                leadids.add(ll.leadId);
            }

            //get all the owner names for al leads records
            for (lead ll : [Select id, ownerid, Owner.name from Lead where id in : leadids] ) {
                Ownermap.put(ll.id, ll.Owner.name);
            }

            /*
              //get all the owner names for all Application records
              for(genesis__Applications__c ll : [SELECT id,Owner.name FROM genesis__Applications__c where id =: app.id]){
                  OwnerAppmap.put(ll.id,ll.Owner.name);
              }
             */
        } catch (Exception ee) {
            system.debug(ee.getCause());
            system.debug(ee.getLineNumber());
            system.debug(ee.getMessage());
            system.debug(ee.getStackTraceString());
        }

    }


    //*********************
    //To show Lead History
    //*********************
    public List<historyOwner> getLeadHistory() {

        leadHistoryPopulated = New List<historyOwner>();
        try {
            if (!LeadHistory.isEmpty()) {
                string leadstatus =   '';
                for (LeadHistory lh : LeadHistory) {
                    //*******************************************
                    //Logic to check previous and current element
                    //*******************************************
                    if (leadstatus == '') {
                        leadstatus = string.valueOf(lh.OldValue);
                        assignment(lh);
                    } else if (leadstatus != '' && leadstatus != string.valueOf(lh.OldValue)) {
                        leadstatus = string.valueOf(lh.oldvalue);
                        assignment(lh);
                    }
                }
            }
        } catch (Exception ee) {
            system.debug(ee.getCause());
            system.debug(ee.getLineNumber());
            system.debug(ee.getMessage());
            system.debug(ee.getStackTraceString());
        }
        return leadHistoryPopulated;
    }

    //*********************************
    //Method to fill the wrapper object
    //*********************************
    private void assignment(LeadHistory lh) {
        try {
            if (!LeadHistoryOwnertemp.isEmpty()) {
                historyOwner lho;
                string lastmodified;
                string lastmodifiedoldval;
                for (LeadHistory LHH : LeadHistoryOwnertemp) {
                    if (lh.LeadId == LHH.LeadId) {
                        if (lh.CreatedDate >= LHH.CreatedDate) {
                            //get the last modifies user
                            lastmodified = string.valueOf(LHH.NewValue);
                            system.debug('LHH.oldValueLHH.oldValueLHH.oldValueLHH.oldValue' + LHH.oldValue);
                            break;
                        }
                    }
                }
                system.debug('!!!!!!!!!!!!!' + LeadHistoryOwnertemp.size());
                lastmodifiedoldval = string.valueOf(LeadHistoryOwnertemp[LeadHistoryOwnertemp.size() - 1].oldValue);

                //****************************
                //Fill other details of map
                //****************************
                lho = new historyOwner();
                lho.field = string.valueOf(lh.Field);
                lho.oldValue = string.valueOf(lh.OldValue);
                lho.newValue = string.valueOf(lh.NewValue);
                lho.timeStamp =  lh.CreatedDate;
                lho.modifiedBy =  userMap.get(lh.CreatedById);
                lho.modifiedByID = userMapID.get(userMap.get(lh.CreatedById));
                system.debug('PPPPP' + lastmodified);
                system.debug('lastmodifiedoldval' + lastmodifiedoldval);

                if (lastmodified == null) {
                    lho.newOwner = userMap.get(lastmodifiedoldval);
                    lho.newOwnerID = userMapID.get(userMap.get(lastmodifiedoldval));
                } else {
                    lho.newOwner = lastmodified;
                    lho.newOwnerID = userMapID.get(lastmodified);
                }

                system.debug('data ' + lho);
                leadHistoryPopulated.add(lho);
                //ShowLead =true; //to render Lead history section

            } else {
                //***************************************************
                //This loop will be executed when no owner is updated
                //***************************************************
                historyOwner lho = new historyOwner();
                lho.field = string.valueOf(lh.Field);
                lho.oldValue = string.valueOf(lh.OldValue);
                lho.newValue = string.valueOf(lh.NewValue);
                lho.timeStamp =  lh.CreatedDate;
                lho.modifiedBy = userMap.get(lh.CreatedById);
                lho.modifiedByID = lh.CreatedById;
                lho.newOwner = Ownermap.get(lh.LeadId);
                lho.newOwnerID = lh.LeadId;
                system.debug('No change' + lho);
                leadHistoryPopulated.add(lho);
                //ShowLead =true; //to render Lead history section
            }
        } catch (exception e) {
            system.debug(e.getCause());
            system.debug(e.getLineNumber());
            system.debug(e.getMessage());
            system.debug(e.getStackTraceString());
        }
    }



    //****************************
    //To Show Application history
    //****************************
    public List<historyOwner> getAppHistory() {
        appHistoryPopulated = new List<historyOwner>();
        try {
            if (!AppHistory.isEmpty()) {
                string appstatus = '';
                for (genesis__Applications__History lh : AppHistory) {

                    //*******************************************
                    //Logic to check previous and current element
                    //*******************************************
                    if (appstatus == '') {
                        system.debug('@@@@@@@@@' + lh.OldValue);
                        system.debug('@@@@@@@@@' + lh.newValue);
                        appstatus = string.valueOf(lh.OldValue);
                        Appassignment(lh);
                    } else if (appstatus != '' && appstatus != string.valueOf(lh.OldValue)) {
                        appstatus = string.valueOf(lh.oldvalue);
                        Appassignment(lh);
                    }
                }
            }
        } catch (Exception ee) {
            system.debug(ee.getCause());
            system.debug(ee.getLineNumber());
            system.debug(ee.getMessage());
            system.debug(ee.getStackTraceString());
        }
        return appHistoryPopulated;
    }

    //*********************************
    //Method to fill the wrapper object
    //*********************************
    private void Appassignment(genesis__Applications__History lh) {
        try {
            system.debug('AppHistorytest' + AppHistorytest);
            if (!AppHistorytest.isEmpty()) {
                historyOwner lho;
                string lastmodified;
                string lastmodifiedoldval;
                for (genesis__Applications__History LHH : AppHistorytest) {
                    if (lh.ParentId  == LHH.ParentId ) {
                        if (lh.CreatedDate >= LHH.CreatedDate) {
                            //get the last modifies user
                            string newval = string.valueOf(LHH.NewValue);
                            if (newval == null )
                                newval = '';
                            lastmodified = string.valueOf(newval);
                            break;
                        }
                    }
                }

                lastmodifiedoldval = string.valueOf(AppHistorytest[AppHistorytest.size() - 1].oldValue);
                //****************************
                //Fill other details of map
                //****************************
                lho = new historyOwner();
                lho.field = string.valueOf(lh.Field);
                lho.oldValue = string.valueOf(lh.OldValue);
                lho.newValue = string.valueOf(lh.NewValue);
                lho.timeStamp =  lh.CreatedDate;
                lho.modifiedBy =  userMap.get(lh.CreatedById);
                lho.modifiedByID = userMapID.get(userMap.get(lh.CreatedById));
                if (lastmodified == null) {
                    lho.newOwner = userMap.get(lastmodifiedoldval);
                    lho.newOwnerID = userMapID.get(userMap.get(lastmodifiedoldval));
                } else {
                    lho.newOwner = lastmodified;
                    lho.newOwnerID = userMapID.get(lastmodified);
                }
                system.debug('data ' + lho);
                appHistoryPopulated.add(lho);
                //ShowLead =true; //to render Lead history section

            } else {
                //***************************************************
                //This loop will be executed when no owner is updated
                //***************************************************
                historyOwner lho = new historyOwner();
                lho.field = string.valueOf(lh.Field);
                lho.oldValue = string.valueOf(lh.OldValue);
                lho.newValue = string.valueOf(lh.NewValue);
                lho.timeStamp = lh.CreatedDate;
                lho.modifiedBy = userMap.get(lh.CreatedById);
                lho.modifiedByID = lh.CreatedById;
                //  lho.newOwner =  OwnerAppmap.get(lh.ParentId);  //Ownermap.get(lh.LeadId);
                lho.newOwner = currentApp.owner.name;
                lho.newOwnerID =  lh.ParentId;
                system.debug('No change' + lho);
                appHistoryPopulated.add(lho);
                //ShowLead =true; //to render Lead history section
            }
        } catch (exception e) {
            system.debug(e.getCause());
            system.debug(e.getLineNumber());
            system.debug(e.getMessage());
            system.debug(e.getStackTraceString());
        }
    }





    //***********************
    //To Show Contact history
    //***********************
    public List<genesis__Applications__c> getContHistory() {
        return AppList;
    }

    //*************************************
    //Wrapper class for Lead history owner.
    //*************************************
    public class historyOwner {

        //*******************
        //Wrapper variables
        //*******************
        public string field {get; set;}
        public string oldValue {get; set;}
        public string newValue {get; set;}
        public datetime timeStamp {get; set;}
        public string modifiedBy {get; set;}
        public string newOwner {get; set;}
        public string modifiedByID {get; set;}
        public string newOwnerID {get; set;}
    }




}