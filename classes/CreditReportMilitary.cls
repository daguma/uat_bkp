public with sharing class CreditReportMilitary {

    private static final String TU = 'TU';
    private static final String EX = 'EX';
    private static final String TU_MILITARY_SEGMENT = 'AO01';
    private static final String EX_MILITARY_SEGMENT = '361';
    private static final String TU_CODE = '07051';
    private static final String TU_SERVICE_CODE = 'Service Code';
    private static final String TU_SEARCH_STATUS = 'Search Status';
    private static final String EX_MESSAGE_TEXT = 'Message Text';
    private static final String EX_MILITARY_PREFIX_TEXT = '120'; 


    private static Set<String> militaryValuesMapper = new Set<String> {
        'M01',
        '1203'
    };

    /**
    * validate if credit report segments is military designation
    * @param  crSegmentsList
    * @return true or false
    */
    public static Boolean isMilitary(List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {
        Boolean isMilitary = false;

        for (ProxyApiCreditReportSegmentEntity creditReportSegmentEntity : crSegmentsList) {
            if (bureau.equalsIgnoreCase(TU) &&
                    creditReportSegmentEntity.prefix == TU_MILITARY_SEGMENT &&
                    creditReportSegmentEntity.segmentTypeEntity.fieldName.equalsIgnoreCase(TU_SERVICE_CODE) &&
                    creditReportSegmentEntity.value.trim().equalsIgnoreCase(TU_CODE)) {
                isMilitary = true;
                break;
            } else if (bureau.equalsIgnoreCase(EX) &&
                       creditReportSegmentEntity.prefix == EX_MILITARY_SEGMENT &&
                       creditReportSegmentEntity.segmentTypeEntity.fieldName.equalsIgnoreCase(EX_MESSAGE_TEXT) &&
                       creditReportSegmentEntity.value.trim().StartsWith(EX_MILITARY_PREFIX_TEXT)) {
                isMilitary = true;
                break;
            }
        }
        return isMilitary;
    }

    /**
    * extract military designation from credit report segments
    * @param  crSegmentsList
    * @return military value
    */
    public static String getMilitaryDesignation(List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {
        String result = '';

        if (isMilitary(crSegmentsList, bureau)) {
            for (ProxyApiCreditReportSegmentEntity creditReportSegmentEntity : crSegmentsList) {
                if (bureau.equalsIgnoreCase(TU) &&
                        creditReportSegmentEntity.prefix == TU_MILITARY_SEGMENT &&
                        creditReportSegmentEntity.segmentTypeEntity.fieldName.equalsIgnoreCase(TU_SEARCH_STATUS) ) {
                    result = creditReportSegmentEntity.value.substring(0, 3);
                    break;
                } else if (bureau.equalsIgnoreCase(EX) &&
                           creditReportSegmentEntity.prefix == EX_MILITARY_SEGMENT &&
                           creditReportSegmentEntity.segmentTypeEntity.fieldName.equalsIgnoreCase(EX_MESSAGE_TEXT) &&
                           creditReportSegmentEntity.value.trim().StartsWith(EX_MILITARY_PREFIX_TEXT)) {
                    result = creditReportSegmentEntity.value.substring(0, 4);
                    break;
                }
            }
        }
        return result;
    }

    /**
     * change contact military flag
     * @param contactId
     * @param crSegmentsList
     * @param bureau
     */
    /*public static void changeContactMilitaryFlag(String contactId, List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {
        for (Contact con : [Select Id,
                           Military__c
                           from Contact
                           where Id = : contactId]){

            if (con.Military__c == false && militaryValuesMapper.contains(getMilitaryDesignation(crSegmentsList, bureau))) {
                con.Military__c = true;
                update con;
            }
        }
    }*/

    public static Contact changeContactMilitaryFlag(String contactId, List<ProxyApiCreditReportSegmentEntity> crSegmentsList, String bureau) {
        for (Contact con : [SELECT Id, Military__c FROM Contact WHERE Id =: contactId LIMIT 1]){
            System.debug('** changeContactMilitaryFlag, is Military before: ' + con.Military__c);
            if (!con.Military__c && militaryValuesMapper.contains(getMilitaryDesignation(crSegmentsList, bureau))){
                con.Military__c = true;
                update con;
            }
            System.debug('** changeContactMilitaryFlag, is Military after: ' + con.Military__c);
            return con;
        }
        return null;
    }
}