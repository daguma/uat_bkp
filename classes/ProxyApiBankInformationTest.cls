@isTest public class ProxyApiBankInformationTest {

    @isTest static void validate_instance() {

        Test.startTest();
        ProxyApiBankInformation bi = new ProxyApiBankInformation();
        Test.stopTest();

        System.assertEquals(null, bi.bankAccountGeneralInformation);
        System.assertEquals(null, bi.bankStatementsPeriods);
        System.assertEquals(null, bi.bankStatementsDetailsList);

        ProxyApiBankInformation.BankAccountGeneralInformation bagi = new ProxyApiBankInformation.BankAccountGeneralInformation();
        bagi.yodleeItemAccountId = 1;
        bagi.yodleeItemId = 1;
        bagi.email = 'test';
        bagi.accountName = 'test';
        bagi.accountAddress = 'test';
        bagi.accountType = 'test';
        bagi.accountNumber = 'test';
        bagi.routingNumber = 'test';
        bagi.currentBalance = 0.0;
        bagi.overdraftProtection = 1;

        bi.bankAccountGeneralInformation = bagi;

        System.assertNotEquals(null, bi.bankAccountGeneralInformation);
        System.assertEquals(bagi, bi.bankAccountGeneralInformation);

        ProxyApiBankInformation.BankStatementsPeriods bsp = new ProxyApiBankInformation.BankStatementsPeriods();

        bsp.selectedP1 = 'test';
        bsp.selectedP2 = 'test';
        bsp.selectedP3 = 'test';
        bsp.selectedP4 = 'test';

        bi.bankStatementsPeriods = bsp;

        System.assertNotEquals(null, bi.bankStatementsPeriods);
        System.assertEquals(bsp, bi.bankStatementsPeriods);

        ProxyApiBankInformation.BankStatementsDetailsList bsdl = new ProxyApiBankInformation.BankStatementsDetailsList();

        bsdl.id = 1;
        bsdl.amtForP1 = 'test';
        bsdl.amtForP2 = 'test';
        bsdl.amtForP3 = 'test';
        bsdl.amtForP4 = 'test';

        bsdl.numForP1 = 'test';
        bsdl.numForP2 = 'test';
        bsdl.numForP3 = 'test';
        bsdl.numForP4 = 'test';

        List<ProxyApiBankInformation.BankStatementsDetailsList> bsdlist = new List<ProxyApiBankInformation.BankStatementsDetailsList>();

        bsdlist.add(bsdl);

        bi.bankStatementsDetailsList = bsdlist;

        System.assertNotEquals(null, bi.bankStatementsDetailsList);
        System.assertEquals(bsdlist, bi.bankStatementsDetailsList);

    }

}