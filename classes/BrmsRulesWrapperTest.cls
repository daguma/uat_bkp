@isTest public class BrmsRulesWrapperTest {
    @isTest static void validate() {
        
        BrmsRulesWrapper ruleWrapper = new BrmsRulesWrapper();
        
        ruleWrapper.ruleName = 'test data';
        system.assertequals('test data',ruleWrapper.ruleName);
        
        ruleWrapper.rulePass = true;
        system.assertequals(true,ruleWrapper.rulePass);
        
        ruleWrapper.ruleValue = 'test data';
        system.assertequals('test data',ruleWrapper.ruleValue);
        
        ruleWrapper.ruleNumber = 'test data';
        system.assertequals('test data',ruleWrapper.ruleNumber);
    }
}