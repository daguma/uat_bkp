public class GiactConstants {
    public static string STATUS_REVIEW_COMPLETE
    {
        get
        {
            return GIACT_Integration_Settings__c.getInstance().STATUS_REVIEW_COMPLETE__c;
        }
    }
    public static string STATUS_FAIL 
    {
        get
        {
            return GIACT_Integration_Settings__c.getInstance().STATUS_FAIL__c;
        }
    }
    public static string STATUS_IN_PROGRESS 
    {
        get
        {
            return GIACT_Integration_Settings__c.getInstance().STATUS_IN_PROGRESS__c;
        }
    }
    
    public static string PositiveFundsConfirmation
    {
        get
        {
            return GIACT_Integration_Settings__c.getInstance().PositiveFundsConfirmation__c;
        }
    }
    
    public static string NameMismatch
    {
        get
        {
            return GIACT_Integration_Settings__c.getInstance().NameMismatch__c;
        }
    }
    
    public static string NonParticipatingBank
    {
        get
        {
            return GIACT_Integration_Settings__c.getInstance().NonParticipatingBank__c;
        }
    }
    
    public static Set<string> AcceptableAccountResponseCodes
    {
        get
        {
            GIACT_Integration_Settings__c settings = GIACT_Integration_Settings__c.getInstance();
            Set<String> retVal = new Set<String>();
            retVal.addAll(settings.PositiveAccountResponseCode__c.split(','));
            return retVal;
        }
    }
    public static Set<string> AcceptableCustomerResponseCode
    {
        get
        {
            GIACT_Integration_Settings__c settings = GIACT_Integration_Settings__c.getInstance();
            Set<String> retVal = new Set<String>();
            retVal.addAll(settings.PositiveCustomerResponseCode__c.split(','));
            return retVal;
        }
    }    
}