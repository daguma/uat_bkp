@isTest public class ProxyApiClarityInquiryEntityTest {

    @isTest static void sets_variables() {

        ProxyApiClarityInquiryEntity pacie = new ProxyApiClarityInquiryEntity();
        pacie.id = 1;
        pacie.creditReportId = 001;
        pacie.fraudCrosstabMultiple = 0.0;
        pacie.fraudScore = 1;
        pacie.isApproved = true;

        DateTime myDateTime = DateTime.now().addDays(-1);
        pacie.createdDate = myDateTime.getTime();

        pacie.trackingNumber = '0123';
        pacie.errorMessage = 'Clarity Error';
        pacie.errorDetails = 'Something went wrong';

        pacie.reasonCodeDescription = 'Test codes';

        System.assertEquals(true, pacie.errorResponse);
        System.assertNotEquals(null, pacie.creationDate);
    }

}