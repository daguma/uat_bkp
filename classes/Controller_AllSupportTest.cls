@isTest
private class Controller_AllSupportTest {

    @isTest static void gets_objects_from_contact() {

        loan__loan_Account__c contract = LibraryTest.createContractTH();

        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        update contract;

        Contact c = [SELECT Id, Firstname, Lastname, MailingState, AccountId, OwnerId
                     FROM Contact WHERE Id = : contract.loan__Contact__c LIMIT 1];

        loan__Bank_Account__c bankAcc = new loan__Bank_Account__c();
        bankAcc.loan__Contact__c = c.id;
        bankAcc.loan__Account__c = c.AccountId;
        bankAcc.loan__Account_Type__c = 'Checking';
        bankAcc.loan__Account_Usage__c = 'TestUsage';
        bankAcc.loan__Active__c = true;
        bankAcc.loan__Bank_Account_Number__c = '1234567890';
        bankAcc.loan__Bank_Name__c = 'Test Bank';
        bankAcc.peer__Branch_Code__c = '998';
        bankAcc.loan__Routing_Number__c = '123456789';
        insert bankAcc;

        Collection__c coll = new Collection__c();
        coll.Missed_Payment_Reason__c = 'Account Frozen';
        coll.General_Status__c = 'Contact Attempted';
        coll.Sub_Status__c = 'Contact Failed';
        coll.CL_Contract__c = contract.Id;
        insert coll;

        ApexPages.StandardController cntrl = new ApexPages.StandardController(c);
        Controller_AllSupport allSupport = new Controller_AllSupport(cntrl);

        allSupport.doSaveAll();

        System.assertNotEquals(null, allSupport.mycollectionController);
        System.assertNotEquals(null, allSupport.myClContract);
        System.assertNotEquals(null, allSupport.myAccountController);
        System.assertNotEquals(null, allSupport.myUserController);
        System.assertNotEquals(null, allSupport.myOriginateController);
    }

    @isTest static void gets_objects_without_bank() {

        loan__loan_Account__c contract = LibraryTest.createContractTH();

        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        update contract;

        Contact c = [SELECT Id, Firstname, Lastname, MailingState, AccountId, OwnerId
                     FROM Contact WHERE Id = : contract.loan__Contact__c LIMIT 1];

        ApexPages.StandardController cntrl = new ApexPages.StandardController(c);
        Controller_AllSupport allSupport = new Controller_AllSupport(cntrl);

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'Please input proper Transaction From & To Date Rage.', 'No Bank');
    }

    @isTest static void gets_objects_without_collections() {

        loan__loan_Account__c contract = LibraryTest.createContractTH();

        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        update contract;

        Contact c = [SELECT Id, Firstname, Lastname, MailingState, AccountId, OwnerId
                     FROM Contact WHERE Id = : contract.loan__Contact__c LIMIT 1];

        loan__Bank_Account__c bankAcc = new loan__Bank_Account__c();
        bankAcc.loan__Contact__c = c.id;
        bankAcc.loan__Account__c = c.AccountId;
        bankAcc.loan__Account_Type__c = 'Checking';
        bankAcc.loan__Account_Usage__c = 'TestUsage';
        bankAcc.loan__Active__c = true;
        bankAcc.loan__Bank_Account_Number__c = '1234567890';
        bankAcc.loan__Bank_Name__c = 'Test Bank';
        bankAcc.peer__Branch_Code__c = '998';
        bankAcc.loan__Routing_Number__c = '123456789';
        insert bankAcc;

        ApexPages.StandardController cntrl = new ApexPages.StandardController(c);
        Controller_AllSupport allSupport = new Controller_AllSupport(cntrl);

        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'No Contract found associated with Contact ', 'No Collections');
    }

    @isTest static void goes_back() {

        loan__loan_Account__c contract = LibraryTest.createContractTH();

        contract.loan__Disbursal_Amount__c = 200;
        contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
        contract.loan__Disbursal_Status__c = 'Fully Disbursed';
        contract.loan__Disbursed_Amount__c = 200;
        contract.loan__loan_status__c = 'Active - Good Standing';
        update contract;

        Contact c = [SELECT Id, Firstname, Lastname, MailingState, AccountId, OwnerId
                     FROM Contact WHERE Id = : contract.loan__Contact__c LIMIT 1];

        loan__Bank_Account__c bankAcc = new loan__Bank_Account__c();
        bankAcc.loan__Contact__c = c.id;
        bankAcc.loan__Account__c = c.AccountId;
        bankAcc.loan__Account_Type__c = 'Checking';
        bankAcc.loan__Account_Usage__c = 'TestUsage';
        bankAcc.loan__Active__c = true;
        bankAcc.loan__Bank_Account_Number__c = '1234567890';
        bankAcc.loan__Bank_Name__c = 'Test Bank';
        bankAcc.peer__Branch_Code__c = '998';
        bankAcc.loan__Routing_Number__c = '123456789';
        insert bankAcc;

        Collection__c coll = new Collection__c();
        coll.Missed_Payment_Reason__c = 'Account Frozen';
        coll.General_Status__c = 'Contact Attempted';
        coll.Sub_Status__c = 'Contact Failed';
        coll.CL_Contract__c = contract.Id;
        insert coll;

        ApexPages.StandardController cntrl = new ApexPages.StandardController(c);
        Controller_AllSupport allSupport = new Controller_AllSupport(cntrl);

        PageReference result = allSupport.goBack();

        System.assertEquals('/apex/Loan_Correspondence?id=' + contract.id, result.getUrl());
    }

}