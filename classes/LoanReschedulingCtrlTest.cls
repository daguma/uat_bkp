@isTest
public class LoanReschedulingCtrlTest {
    @isTest static void save_amortization_schedule() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        loan__Loan_Payment_Transaction__c pmt = null;

        System.runAs(thisUser) {

            Group newGroup = [SELECT Id, Name FROM Group WHERE Name LIKE '%FF Lendingpoint LLC' ORDER BY Id LIMIT 1];

            RecordType recordTy = [SELECT Id, Name, DeveloperName, NamespacePrefix, Description, BusinessProcessId, SobjectType, IsActive FROM RecordType WHERE Name = 'SUT' LIMIT 1];

            c2g__codaCompany__c testCompany = new c2g__codaCompany__c();
            testCompany.Name = 'Lendingpoint LLC';
            testCompany.RecordTypeId = recordTy.Id;
            testCompany.OwnerId = newGroup.Id;
            insert testCompany;

            loan__Payment_Mode__c paymentMode = new loan__Payment_Mode__c();
            paymentMode.Name = 'ACH';
            insert paymentMode;

            loan__Loan_Account__c contract = LibraryTest.createContractTH();
            contract.loan__loan_Status__c = 'Active - Bad Standing';
            contract.loan__Disbursal_Amount__c = 200;
            contract.loan__Disbursal_Date__c = Date.today().addDays(-1);
            contract.loan__Disbursal_Status__c = 'Fully Disbursed';
            contract.loan__Disbursed_Amount__c = 200;
            contract.Company__c = [SELECT Id FROM c2g__codaCompany__c WHERE Name = 'Lendingpoint LLC' ORDER BY CreatedDate DESC LIMIT 1].Id;
            update contract;

            loan__Loan_Payment_Spread__c pymntSpread = new loan__Loan_Payment_Spread__c();
            pymntSpread.Name = 'Recovery Loans';
            insert pymntSpread;

            pmt = new loan__Loan_Payment_Transaction__c();
            pmt.loan__Loan_Account__c = contract.Id;
            pmt.Payment_Processing_Complete__c = false;
            pmt.loan__Transaction_Amount__c = 100.00;
            pmt.loan__Principal__c = 100.00;
            pmt.loan__Interest__c = 100.00;
            pmt.loan__Fees__c = 100.00;
            pmt.loan__excess__c = 100.00;
            pmt.loan__Transaction_Date__c = Date.today().addDays(-10);
            pmt.loan__Skip_Validation__c = true;
            pmt.loan__Reversed__c = false;
            pmt.Payment_Pending_End_Date__c = date.today();
            pmt.loan__Payment_Mode__c = paymentMode.Id;
            insert pmt;

            List<String> paymentIds = new List<String>();
            List<String> balances = new List<String>();
            List<String> paymentAmounts = new List<String>();
            List<String> paymentDates  = new List<String>();
            List<String> interestAmounts  = new List<String>();
            List<String> principalAmounts  = new List<String>();

            Integer day = 11;
            String month = '10';
            String year = '2017';

            for (Integer currentNumber = 1; currentNumber < 6; currentNumber ++) {
                paymentIds.add(String.valueOf(currentNumber));
                balances.add(String.valueOf(currentNumber * 7));
                paymentDates.add(String.valueOf(month + '/' + (day + currentNumber) + '/' + year));
                principalAmounts.add(String.valueOf(currentNumber * 4));
                interestAmounts.add(String.valueOf(currentNumber * 2));
                paymentAmounts.add(String.valueOf((currentNumber * 4) + (currentNumber * 2)));
            }

            loan__Repayment_Schedule__c repaymentSchedule = new loan__Repayment_Schedule__c();
            repaymentSchedule.loan__Loan_Account__c = contract.Id;
            repaymentSchedule.loan__Due_Date__c = Date.today();
            insert repaymentSchedule;

            Integer countBefore = [Select count() from loan__Repayment_Schedule__c];
            String idRS = ((loan__Repayment_Schedule__c)[Select Id from loan__Repayment_Schedule__c where loan__Loan_Account__c = :contract.Id]).Id;
            ApexPages.StandardController controller = new ApexPages.StandardController(contract);
            LoanReschedulingCtrl rescheduling = new LoanReschedulingCtrl(controller);

            LoanReschedulingCtrl.saveAmortizationSchedule(contract.Id, balances, paymentAmounts, paymentDates, interestAmounts, principalAmounts, paymentIds);

            //Old amortization archived
            System.assert(((loan__Repayment_Schedule__c)[Select loan__Is_Archived__c from loan__Repayment_Schedule__c where id = :idRS]).loan__Is_Archived__c == true, 'Old Amortization archived');

            //New amortization inserted
            System.assert([Select count() from loan__Repayment_Schedule__c] > countBefore, 'Amortization inserted');

            //Pending end date annulled
            System.assert(((loan__Loan_Payment_Transaction__c)[Select Payment_Pending_End_Date__c from loan__Loan_Payment_Transaction__c where loan__Loan_Account__c = :contract.Id]).Payment_Pending_End_Date__c == null, 'Pending end date annulled');

            //Change the contract status
            System.assertEquals('Active - Good Standing', ((loan__Loan_Account__c)[Select loan__loan_Status__c from loan__Loan_Account__c where Id = :contract.Id]).loan__Loan_Status__c);
        }
    }

    @isTest static void setup_periodic_fee() {
        loan__Loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__loan_Status__c = 'Active - Bad Standing';
        update contract;

        loan__Fee__c fee = new loan__Fee__c();
        fee.Name = 'Waived Interest Recovery';
        fee.loan__Amount__c = 50;
        insert fee;

        loan__Periodic_Fee_Setup__c periodicFee = new loan__Periodic_Fee_Setup__c();
        periodicFee.loan__Active__c = true;
        periodicFee.loan__Lending_Account__c = contract.Id;
        periodicFee.loan__Fee__c = fee.Id;

        insert periodicFee;

        Integer countBefore = [Select count() from loan__Periodic_Fee_Setup__c];

        LoanReschedulingCtrl.setUpPeriodicFee(contract.Id, String.valueOf(5), ('10/11/2017'));
        //New fee setup inserted
        System.assert([Select count() from loan__Periodic_Fee_Setup__c] > countBefore, 'New fee setup inserted');

        //Old fee setup archived
        System.assert(((loan__Periodic_Fee_Setup__c)[Select loan__Active__c from loan__Periodic_Fee_Setup__c where loan__Lending_Account__c = :contract.Id AND Id = :periodicFee.Id]).loan__Active__c == false, 'Inactive fee');
        System.assert(((loan__Periodic_Fee_Setup__c)[Select loan__Archived__c from loan__Periodic_Fee_Setup__c where loan__Lending_Account__c = :contract.Id AND Id = :periodicFee.Id]).loan__Archived__c == true, 'Archived fee');
        System.assert(((loan__Periodic_Fee_Setup__c)[Select loan__Clear__c from loan__Periodic_Fee_Setup__c where loan__Lending_Account__c = :contract.Id AND Id = :periodicFee.Id]).loan__Clear__c == true, 'Clear fee');


    }
}