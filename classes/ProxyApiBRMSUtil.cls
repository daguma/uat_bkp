public virtual class ProxyApiBRMSUtil {
    @testVisible private static String AON_URL = '/v101/aon/';
    @testVisible private static Proxy_API__c settings = Proxy_API__c.getOrgDefaults();
    @testVisible private static String apiCookie = ''; 
    /**
     * CheckSettings for API
     * @return a boolean value
     */
    private static Boolean CheckSettings() {

        if (settings != null) {
            return true;
        }

        System.debug('ERROR: No Proxy API settings were found. Please contact your system administrator');
        return false;
    }

    /**
     * Call Api To Get a New Token
     * @return void
     */
    private static void ApiGetNewToken() {
        try {
            HttpRequest request = new HttpRequest();
            system.debug('-----settings-----'+settings);
            request.setEndpoint(RunningInSandbox() ? settings.DEV_Token_URL_New__c : settings.Token_URL_New__c);
            request.setMethod('POST');
            request.setTimeout(40000);
            request.setHeader('Accept', 'application/json');
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.setHeader('Authorization', 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(settings.Proxy_API_ClientId__c + ':' + settings.Proxy_API_ClientSecret__c)));
            request.setBody(
                'grant_type=password' +
                '&username=' + EncodingUtil.urlEncode(settings.Proxy_API_Username__c, 'UTF-8') +
                '&password=' + EncodingUtil.urlEncode(settings.Proxy_API_Password__c, 'UTF-8') +
                '&scope=' + EncodingUtil.urlEncode('read write', 'UTF-8')
            );
            HttpResponse response = new Http().send(request);
            System.debug('Proxy API ("' + request.getEndpoint() + '") -> Response: ' + response.getStatusCode() + ' / ' + response.getStatus() + '   . Cookies = ' + apiCookie);
            if (response.getStatusCode() == 200) {
                String JsonResponse = response.getBody();
                apiCookie = response.getHeader('Set-Cookie'); 
                if (apiCookie != null && !String.isEmpty(apiCookie)) {
                        Integer cookieLength = apiCookie.length();
                        if (cookieLength > 255) {
                            settings.Proxy_API_Cookie__c = apiCookie.substring(0,255);
                            settings.Proxy_API_Cookie2__c = apiCookie.substring(255); 
                        }
                        else {
                            settings.Proxy_API_Cookie__c = apiCookie.substring(0,cookieLength); 
                            settings.Proxy_API_Cookie2__c = '';                 
                        }
                    }
                settings.Token_New__c =
                    ExtractJsonField(JsonResponse, 'access_token');

                settings.Token_Expires_Time_New__c =
                    DateTime.now().getTime() +
                    Long.valueOf(ExtractJsonField(JsonResponse, 'expires_in')) * 1000;
            }
       } catch (Exception e) {
            System.debug('Catch Exception: ' + e.getMessage());
        }
    }

    /**
     * Get Api Token
     * @return token
     */
    public static Boolean ExistsApiToken() {
        if (settings.Token_Expires_Time_New__c != null &&
                settings.Token_Expires_Time_New__c > DateTime.now().getTime() &&
                settings.Token_New__c != null &&
                (RunningInSandbox() ? true : settings.Proxy_API_Cookie__c != null)
                ) {
            return true;
        }
        ApiGetNewToken();
        return settings.Token_New__c != null;
    }

    public static Boolean ValidateBeforeCallApi() {
        return CheckSettings() && ExistsApiToken();
    }

    /**
     * [ExtractJsonField description]
     * @param  body  [description]
     * @param  field [description]
     * @return       [description]
     */
    public static String ExtractJsonField(String body, String field) {

        JSONParser parser = JSON.createParser(body);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == field) {
                parser.nextToken();
                return parser.getText();
            }
        }
        return null;
    }

    /**
     * Call Api To Get a Result
     * @return json string
     */
    public static String ApiGetResult(String url) {
        try {
            for (Integer i = 0; i < 2; i++) {
                HttpRequest request = new HttpRequest();
                request.setEndpoint((RunningInSandbox() ? settings.DEV_Default_URL_New__c : settings.Default_URL_New__c) + url);
                request.setMethod('GET');
                request.setTimeout(120000);
                request.setHeader('Accept', 'application/json');
                request.setHeader('Authorization', 'bearer ' + settings.Token_New__c);
                apiCookie = settings.Proxy_API_Cookie__c + settings.Proxy_API_Cookie2__c;
                if (!String.isEmpty(apiCookie)) request.setHeader('Cookie', apiCookie);

                HttpResponse response = new Http().send(request);

                System.debug('Proxy API ("' + request.getEndpoint() + '") ->    Response: ' + response.getStatusCode() + ' / ' + response.getStatus() + '  . Cookies = ' + apiCookie);

                if (response.getStatusCode() == 200 || response.getStatusCode() == 417 || response.getStatusCode() == 201) {
                if (String.isEmpty(apiCookie)) apiCookie = response.getHeader('Set-Cookie');
                    return response.getBody();
                } else {
                    ApiGetNewToken();
                }
            }
        } catch (Exception e) {
            System.debug('Catch Exception ApiGetResult: ' + e.getMessage());
            WebToSFDC.notifyDev('ApiGetResult ERROR', 'Catch Exception: ' + e.getMessage() + '\nStacktrace\n' + e.getStackTraceString());

        }
        return null;
    }

    /**
     * Call Api To Post a Result
     * @return json string
     */
    public static String ApiGetResult(String url, String body) {
        try {
            for (Integer i = 0; i < 2; i++) {
                HttpRequest request = new HttpRequest();

                request.setEndpoint((RunningInSandbox() ? settings.DEV_Default_URL_New__c : settings.Default_URL_New__c) + url);
                request.setMethod('POST');
                request.setTimeout(120000);
                request.setHeader('Accept', 'application/json');
                request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                request.setHeader('Authorization', 'bearer ' + settings.Token_New__c);
                apiCookie = settings.Proxy_API_Cookie__c + settings.Proxy_API_Cookie2__c;
                if (!String.isEmpty(apiCookie)) request.setHeader('Cookie', apiCookie);

                request.setBody(body);

                HttpResponse response = new Http().send(request);

                System.debug('Proxy API ("' + request.getEndpoint() + '") -> Response: ' + response.getStatusCode() + ' / ' + response.getStatus() + '  . Cookies = ' + apiCookie);
                System.debug('Body = ' + request.getBody());
                System.debug('response body = ' + response.getBody());
                if(url.contains(AON_URL)) return AONProcesses.proxyResponseProcess(response);

                if (response.getStatusCode() == 200 || response.getStatusCode() == 417 || response.getStatusCode() == 400) { 
                    if (String.isEmpty(apiCookie)) apiCookie = response.getHeader('Set-Cookie');
                    return response.getBody();
                } else {
                    ApiGetNewToken();
                } 
            }

        } catch (Exception e) {
            System.debug('Catch Exception: ' + e.getMessage());
            WebToSFDC.notifyDev('ApiGetResult ERROR', 'Catch Exception: ' + e.getMessage() + '\nStacktrace\n' + e.getStackTraceString());
        }
        return null;
    }
    
    /**
     * Call Api To Post a Result
     * @return HTTPResponse
     */
    public static HttpResponse ApiGetHttpResponse(String url, String body) {
        try {
            ApiGetNewToken();
            HttpRequest request = new HttpRequest();
            
            request.setEndpoint((RunningInSandbox() ? settings.DEV_Default_URL_New__c : settings.Default_URL_New__c) + url);
            request.setMethod('POST');
            request.setTimeout(120000);
            request.setHeader('Accept', 'application/json');
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.setHeader('Authorization', 'bearer ' + settings.Token_New__c);
            apiCookie = settings.Proxy_API_Cookie__c + settings.Proxy_API_Cookie2__c;
            if (!String.isEmpty(apiCookie)) request.setHeader('Cookie', apiCookie);
            
            request.setBody(body);
            return new Http().send(request);
            
        } catch (Exception e) {
            System.debug('Catch Exception: ' + e.getMessage());
            WebToSFDC.notifyDev('ApiGetResult ERROR', 'Catch Exception: ' + e.getMessage() + '\nStacktrace\n' + e.getStackTraceString());
        }
        return null;
    }

    /**
     * Call Api To Get Post a Result
     * @return json string
     */
    public static Blob ApiGetBinaryResult(String url, String body) {
        try {
            for (Integer i = 0; i < 2; i++) {
                HttpRequest request = new HttpRequest();

                request.setEndpoint((RunningInSandbox() ? settings.DEV_Default_URL_New__c : settings.Default_URL_New__c) + url);
                request.setMethod('POST');
                request.setTimeout(120000);
                request.setHeader('Accept', 'application/json');
                request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                request.setHeader('Authorization', 'bearer ' + settings.Token_New__c);
                if (!String.isEmpty(apiCookie)) request.setHeader('Cookie', apiCookie);

                request.setBody(body);

                HttpResponse response = new Http().send(request);
                System.debug('Proxy API ("' + request.getEndpoint() + '") -> Response: ' + response.getStatusCode() + ' / ' + response.getStatus() + '  . Cookies = ' + apiCookie);
                if (response.getStatusCode() == 200) {
                    if (String.isEmpty(apiCookie)) apiCookie = response.getHeader('Set-Cookie');
                    return response.getBodyAsBlob();
                } else  {
                    ApiGetNewToken();
                } 
            }

        } catch (Exception e) {
            System.debug('Catch Exception: ' + e.getMessage());

            WebToSFDC.notifyDev('ApiGetBinaryResult ERROR', 'Catch Exception: ' + e.getMessage());
        }
        return null;
    }

    /**
     * [ApiGetOffersByLoanAmount description]
     * @param  grade        [description]
     * @param  annualIncome [description]
     * @param  loanAmount   [description]
     * @param  state        [description]
     * @return              [description]
     */
    public static List<ProxyApiOfferCatalog> ApiGetOffersByLoanAmount(String grade, Long annualIncome, Long loanAmount, String state) {
        return ApiGetOffersByLoanAmount(grade, annualIncome, loanAmount, state, 0);
    }

    /**
     * [ApiGetOffersByLoanAmount description]
     * @param  grade        [description]
     * @param  annualIncome [description]
     * @param  loanAmount   [description]
     * @param  state        [description]
     * @return              [description]
     */
    public static List<ProxyApiOfferCatalog> ApiGetOffersByLoanAmount(String grade, Long annualIncome, Long loanAmount, String state, Integer partner) {

        String offersDisplayed = '3';

        if (!ValidateBeforeCallApi()) {
            System.Debug('ApiGetOffersByLoanAmount: Offer API call not validated.');
            return null;
        }

        if (grade == 'C2' || grade == 'D')
            offersDisplayed = '1';

        String jsonResponse = null;
        if (Test.isRunningTest()) {
            jsonResponse = LibraryTest.fakeOffersData(partner);
        } else {
            jsonResponse = ApiGetResult('/v101/offer-catalog/get=' + EncodingUtil.urlEncode(offersDisplayed, 'UTF-8') + '/when/grade=' + EncodingUtil.urlEncode(grade, 'UTF-8') + '&annual-income=' + EncodingUtil.urlEncode('' + annualIncome, 'UTF-8') + '&loan-amount=' + EncodingUtil.urlEncode('' + loanAmount, 'UTF-8') + '&state=' + EncodingUtil.urlEncode(state, 'UTF-8') + '&partner=' + EncodingUtil.urlEncode('' + partner, 'UTF-8'));
        }

        if (jsonResponse == null) {
            System.Debug('ApiGetOffersByLoanAmount: No API response.');
            return null;
        }
        return (List<ProxyApiOfferCatalog>) JSON.deserializeStrict(jsonResponse, List<ProxyApiOfferCatalog>.class);

    }

    /**
     * [ApiGetOffersByPaymentAmount description]
     * @param  grade         [description]
     * @param  annualIncome  [description]
     * @param  paymentAmount [description]
     * @param  state         [description]
     * @return               [description]
     */
    public static List<ProxyApiOfferCatalog> ApiGetOffersByPaymentAmount(String grade, Long annualIncome, Long paymentAmount, String state, Integer partner) {
        if (!ValidateBeforeCallApi()) {
            return null;
        }
        String jsonResponse = null;
        if (Test.isRunningTest()) {
            jsonResponse = LibraryTest.fakeOffersData(partner);
        } else {
            jsonResponse = ApiGetResult('/v101/offer-catalog/get=3/when/grade=' + EncodingUtil.urlEncode(grade, 'UTF-8') + '&annual-income=' + EncodingUtil.urlEncode('' + annualIncome, 'UTF-8') + '&payment-amount=' + EncodingUtil.urlEncode('' + paymentAmount, 'UTF-8') + '&state=' + EncodingUtil.urlEncode(state, 'UTF-8') + '&partner=' + EncodingUtil.urlEncode('' + partner, 'UTF-8'));
        }

        if (jsonResponse == null)
            return null;
        return (List<ProxyApiOfferCatalog>) JSON.deserializeStrict(jsonResponse, List<ProxyApiOfferCatalog>.class);

    }

    /**
     * ApiGetBankInfotmation Call Proxy Api to get Yodlee information for a contact email
     * @param  email [Contact email]
     * @return
     */
    public static ProxyApiBankInformation ApiGetBankInformationByEmail(String email) {
        if (!ValidateBeforeCallApi()) {
            return null;
        }

        String jsonResponse = null;

        if (Test.isRunningTest()) {
            if (email != '' && email != null) {
                jsonResponse = LibraryTest.fakeProxyApiBankInformation();
            }
        } else {
            String body = 'email=' + EncodingUtil.urlEncode(email, 'UTF-8');
            jsonResponse = ApiGetResult('/v101/yodlee-bank-account/get/', body);
        }
        if (jsonResponse == null)
            return null;

        ProxyApiBankInformation result = (ProxyApiBankInformation)JSON.deserializeStrict(jsonResponse, ProxyApiBankInformation.class);

        if (result != null && (result.bankAccountGeneralInformation == null || result.bankStatementsPeriods == null || result.bankStatementsDetailsList == null)) {
            return null;
        }
        return result;
    }

    /**
     * ApiGetBankInfotmation Call Proxy Api to get Yodlee information for a contact email
     * @param  email [Contact email]
     * @return
     */
    public static Blob ApiGetBankInformationPdfByEmail(String email) {
        if (!ValidateBeforeCallApi()) {
            return null;
        }
        String body = 'email=' + EncodingUtil.urlEncode(email, 'UTF-8');
        return ApiGetBinaryResult('/v101/yodlee-bank-account/pdf/', body);
    }

    /**
     * running In Sandbox
     * @return true is org is sandbox
     */
    public static Boolean RunningInSandbox() {
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
}