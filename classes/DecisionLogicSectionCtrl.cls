public with sharing class DecisionLogicSectionCtrl {
    
    private static final String SEQUENCE_ID_DECISION_LOGIC = 'Decision Logic';
    private static final String OPP_STATUS = 'Funded';
    
    public DecisionLogicSectionCtrl() {}
    
    @AuraEnabled
    public static DecisionLogicGDSResponse getDecisionLogicRulesList(String opportunityId) {
        
        String GDSResponse = '';
        Boolean getRulesFromAPI = true;
        
        for(Decision_Logic_Rules__c dlRule : [SELECT id, GDS_Response__c, Opportunity__c, Opportunity__r.Status__c,LastCallDate__c, CreatedDate FROM Decision_Logic_Rules__c WHERE Opportunity__c = : opportunityId ORDER BY Id DESC LIMIT 1]){
            
            if (String.isNotEmpty(dlRule.GDS_Response__c)) {       
                getRulesFromAPI = dlRule.Opportunity__r.Status__c != OPP_STATUS && dlRule.LastCallDate__c <= Date.today().addDays(-7);  
                GDSResponse = dlRule.GDS_Response__c;
               
            }
        }
        return getRulesFromAPI ? getDecisionLogicRulesListFromProxyAPI(opportunityId) : (String.isNotEmpty(GDSResponse) ? new DecisionLogicGDSResponse(GDSResponse) : null);
    }
    
    @InvocableMethod(label='Refresh Decision Logic Section')
    public static void refreshDecisionLogicSection(List<ID> ids) {
        refreshDecisionLogicSection(ids.get(0));
    }
    
    @future (callout=true)
    public static void refreshDecisionLogicSection(String opportunityId) {
        getDecisionLogicRulesListFromProxyAPI(opportunityId);
    }
    
    public static DecisionLogicGDSResponse getDecisionLogicRulesListFromProxyAPI(String opportunityId) {
        
        if (validateDecisionLogicAccountNumber(opportunityId)) {
 
            BRMS_Sequence_Ids__c BRMSequenceIds = BRMS_Sequence_Ids__c.getInstance(SEQUENCE_ID_DECISION_LOGIC);
            String jsonInputData = GDSGenericCallProcess.get_DecisionLogicGDSParameters(opportunityId);
            
            if (String.isNotEmpty(jsonInputData) && BRMSequenceIds != null && String.isNotEmpty(BRMSequenceIds.Sequence_Id__c)) {
                
                GDSGenericObjectAttributes.GDSGenericCallResponse gdsGenericCallResponse = GDSGenericCallProcess.get_GDSGenericCallResult(jsonInputData,opportunityId, BRMSequenceIds.Sequence_Id__c);
 
                if (gdsGenericCallResponse != null && String.isNotEmpty(gdsGenericCallResponse.get_Response())) {
                    
                    upsertDecisionLogiRulesObjectAfterCallout(opportunityId,gdsGenericCallResponse.get_Response());
                    
                    validateDecisionLogicMeetTolerance(opportunityId, gdsGenericCallResponse.get_Response());
                    
                    return new DecisionLogicGDSResponse(gdsGenericCallResponse.get_Response());
                }
            }
        }
        return null;
    }
    
    @AuraEnabled
    public static Boolean validateDecisionLogicAccountNumber(String opportunityId) {
        system.debug('validateDecisionLogicAccountNumber');
        for (Opportunity opp :[SELECT DL_Account_Number_Found__c, Status__c FROM  Opportunity WHERE Id =: opportunityId LIMIT 1]) {
            if (opp.Status__c == OPP_STATUS) {
                return false;
            }
            return opp.DL_Account_Number_Found__c != null && String.isNotEmpty(opp.DL_Account_Number_Found__c) && opp.DL_Account_Number_Found__c != '--None--';
        }
        return true;
    } 
    
    public static void upsertDecisionLogiRulesObjectAfterCallout(String opportunityId, String GDSResponse){
        
        List<Decision_Logic_Rules__c> dlRulesList = [SELECT Id,Opportunity__c,GDS_Response__c,LastCallDate__c FROM Decision_Logic_Rules__c WHERE Opportunity__c = : opportunityId ORDER BY Id DESC LIMIT 1];
        
        if (!dlRulesList.isEmpty()) {
            dlRulesList.get(0).GDS_Response__c = GDSResponse;
            dlRulesList.get(0).LastCallDate__c = Date.today();
            dlRulesList.get(0).ManualCall__c = false; //System.debug('*** Last Call Date Update: ' +  dlRulesList.get(0).LastCallDate__c);
            update dlRulesList;
        }else{
            Decision_Logic_Rules__c DecisionLogicRules = new Decision_Logic_Rules__c();
            DecisionLogicRules.GDS_Response__c = GDSResponse;
            DecisionLogicRules.Opportunity__c = opportunityId;
            DecisionLogicRules.LastCallDate__c = Date.today();
            DecisionLogicRules.ManualCall__c = false;
            insert DecisionLogicRules;
        }
    }
    
    @AuraEnabled
    public static Boolean upsertDecisionLogicRuleObject(String opportunityId, String jsonGDSResponse) {
        
        for(Decision_Logic_Rules__c dlRule : [SELECT id, GDS_Response__c FROM Decision_Logic_Rules__c WHERE Opportunity__c =: opportunityId LIMIT 1]) {
            if (String.isNotEmpty(jsonGDSResponse) ) {
                dlRule.GDS_Response__c = jsonGDSResponse;
                update dlRule;
                return validateDecisionLogicMeetTolerance(opportunityId, jsonGDSResponse);
            }
        }
        return false;
    }
    
    @AuraEnabled
    public static Boolean validateDecisionLogicMeetTolerance(String opportunityId, String jsonGDSResponse) {
    
        System.debug('validateDecisionLogicMeetTolerance ' + jsonGDSResponse);
        
        Opportunity opp = new Opportunity(Id = opportunityId);
        
        if (opp != null) {
            opp.DecisionLogicMeetsTolerance__c = true;
            if (jsonGDSResponse != null && jsonGDSResponse != 'null') {
                DecisionLogicGDSResponse decisionLogicGDSResponse = new DecisionLogicGDSResponse(jsonGDSResponse);
                for (DecisionLogicGDSResponse.DecisionLogicRule rule : decisionLogicGDSResponse.rulesList) {
                    opp.DecisionLogicMeetsTolerance__c  = (rule.deRuleStatus != 'Pass') ? false : opp.DecisionLogicMeetsTolerance__c;
                    break;
                }
                opp.DecisionLogicAccepted__c = false;
            }
            update opp;  
            return opp.DecisionLogicMeetsTolerance__c;
        }
        
        return false;
    }
    
    @AuraEnabled
    public static DecisionLogicGDSResponse getManualCallDecisionLogicRulesFromProxyAPI(String opportunityId) {  
        
        Boolean isNotExpired = false;
            
        for (Decision_Logic_Rules__c dlRule : [SELECT id,Opportunity__r.Status__c, Opportunity__r.DL_Account_Number_Found__c, ManualCall__c, LastCallDate__c FROM Decision_Logic_Rules__c WHERE Opportunity__c =: opportunityId ORDER BY Id LIMIT 1]) {
            
            isNotExpired = (dlRule.LastCallDate__c > Date.today().addDays(-7));             
            if (!dlRule.ManualCall__c && isNotExpired && dlRule.Opportunity__r.Status__c != OPP_STATUS &&  dlRule.Opportunity__r.DL_Account_Number_Found__c != null &&  dlRule.Opportunity__r.DL_Account_Number_Found__c != '--None--') {
                
                DecisionLogicGDSResponse GDSResponse = getDecisionLogicRulesListFromProxyAPI(opportunityId);

                if (GDSResponse != null){
                    dlRule.ManualCall__c = true;
                    dlRule.LastCallDate__c = Date.today();
                    update dlRule; 
                    return GDSResponse;
                }
            }
        }
        return null;
    }
    
    @AuraEnabled
    public static Decision_Logic_Rules__c getDecisionLogicRulesObject(String opportunityId) {
        for (Decision_Logic_Rules__c dl : [SELECT id,Opportunity__r.Status__c, Opportunity__r.DL_Account_Number_Found__c, ManualCall__c, LastCallDate__c FROM Decision_Logic_Rules__c WHERE Opportunity__c =: opportunityId ORDER BY Id LIMIT 1]){
            return dl;
        }
        return null;
    }
    
}