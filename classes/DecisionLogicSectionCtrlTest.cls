@isTest
private class DecisionLogicSectionCtrlTest {

    @testSetup static void set_decision_logic_sequence(){
        BRMS_Sequence_Ids__c brms = new BRMS_Sequence_Ids__c();
        brms.Name = 'Decision Logic';
        brms.Sequence_Id__c = '14';
        insert brms;
        
        Decision_Logic_Rules__c dlCR = new Decision_Logic_Rules__c();
        dlCR.Name = 'CR-Instance';
        dlCR.GDS_Response__c = 'PFNPQVAtRU5WOkVudmVsb3BlIHhtbG5zOlNPQVAtRU5WPSJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy9zb2FwL2VudmVsb3BlLyI+PFNPQVAtRU5WOkhlYWRlci8+PFNPQVAtRU5WOkJvZHk+PG5zMjpDYWxsQVRCUmVzcG9uc2VUeXBlVmVjdG9yIHhtbG5zOm5zMj0idXJuOkFUQldlYlNlcnZpY2UiPjxyZXR1cm4+PHRyYW5zYWN0aW9uX2lkPjQ1Nzk1MzA8L3RyYW5zYWN0aW9uX2lkPjxyZXNwb25zZT48ZGF0YXNvdXJjZV9zZWdtZW50cz48bmFtZT5UdUNvbnNVU1Y0PC9uYW1lPjxzZWdtZW50cz48bmFtZT5UUlBUPC9uYW1lPjxmaWVsZHM+PG5hbWU+dHJwdF90ZXh0PC9uYW1lPjx2YWx1ZXM+ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgVFJBTlNVTklPTiBDUkVESVQgUkVQT1JUICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICZsdDtGT1ImZ3Q7ICAgICAgICAgICZsdDtTVUIgTkFNRSZndDsgICAgICAgICAgJmx0O01LVCBTVUImZ3Q7ICAmbHQ7SU5GSUxFJmd0OyAgICZsdDtEQVRFJmd0OyAgICAgICZsdDtUSU1FJmd0OyAmbHQ7QlImZ3Q7IChJKSBGIEFNNjI4MzM1OCBMRU5ESU5HUE9JTlQgICAgICAgIDI5IEFaICAgICAxMS85MSAgICAgIDA0LzAzLzE4ICAgIDAwOjA1Q1QmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICZsdDtTVUJKRUNUJmd0OyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICZsdDtTU04mZ3Q7ICAgICAgICAmbHQ7QklSVEggREFURSZndDsmbHQ7QlImZ3Q7IEVYTEVZLCBSSUNIQVJEICBBLiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDU3Mi0zMS0zMDQ5ICAgOC83MyAgICAgICAmbHQ7QlImZ3Q7ICZsdDtBTFNPIEtOT1dOIEFTJmd0OyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IERFWExFWSxSSUNIQVJELEEgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICZsdDtDVVJSRU5UIEFERFJFU1MmZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7REFURSBSUFREJmd0OyAmbHQ7QlImZ3Q7IDI4MjUxIE4uIEhPTExZIFJELiwgU0FOIFRBTiBWQUxMRVkgQVouIDg1MTQzICAgICAgICAgICAgICAgICAgICAgMi8xMSAgICAgICAmbHQ7QlImZ3Q7ICZsdDtGT1JNRVIgQUREUkVTUyZndDsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IDEyMDUgNVRIIEFWLiwgVVBMQU5EIENBLiA5MTc4NiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgMi8xMCAgICAgICAmbHQ7QlImZ3Q7IDgzMSBHQUxFIERSLiwgIzI3LiBDQU1QQkVMTCBDQS4gOTUwMDggICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJmx0O1BPU0lUSU9OJmd0OyAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICZsdDtDVVJSRU5UIEVNUExPWUVSIEFORCBBRERSRVNTJmd0OyAgICAgICAgICAgICAgICAgICAgICZsdDtWRVJGJmd0OyAmbHQ7UlBURCZndDsgICAgICAgICAgICAmbHQ7QlImZ3Q7IEFKIExFR0VORFMgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVEVDSCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IEFQQUNIRSBKVU5DVElPTiBBWi4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAzLzExQSAgMy8xMSAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICZsdDtGT1JNRVIgRU1QTE9ZRVIgQU5EIEFERFJFU1MmZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IEtPTEwgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgNS85NSAgICAgICAgICAgICAmbHQ7QlImZ3Q7IC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0mbHQ7QlImZ3Q7IE0gTyBEIEUgTCAgIFAgUiBPIEYgSSBMIEUgICAgICAgICAgKiAqICogQSBMIEUgUiBUICogKiAqICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7ICoqKkZJQ08gU0NPUkUgOSBTQ09SRSArNjQ2ICA6IDAxMCwgMDAzLCAwMTIsIDAxNCoqKiBJTiBBRERJVElPTiBUTyBUSEUgICAgICAmbHQ7QlImZ3Q7ICoqKkZBQ1RPUlMgTElTVEVEIEFCT1ZFLCBUSEUgTlVNQkVSIE9GIElOUVVJUklFUyBPTiBUSEUgQ09OU1VNRVInUyBDUkVESVQgICAmbHQ7QlImZ3Q7ICoqKkZJTEUgSEFTIEFEVkVSU0VMWSBBRkZFQ1RFRCBUSEUgQ1JFRElUIFNDT1JFLiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0mbHQ7QlImZ3Q7IEMgUiBFIEQgSSBUICAgUyBVIE0gTSBBIFIgWSAgICAgICogKiAqICAgIFQgTyBUIEEgTCAgRiBJIEwgRSAgSCBJIFMgVCBPIFIgWSAmbHQ7QlImZ3Q7IFBSPTAgQ09MPTAgIE5FRz0wICBIU1RORUc9MCAgICAgVFJEPTIwIFJWTD0xMiBJTlNUPTggIE1URz0wICBPUE49MCAgSU5RPTEgICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICBISUdIIENSRUQgIENSRUQgTElNICBCQUxBTkNFICBQQVNUIERVRSAgTU5USExZIFBBWSBBVkFJTEFCTEUgICAmbHQ7QlImZ3Q7IFJFVk9MVklORzogICAkNDQuNEsgICAgICQ1Ni4wSyAgICAkMzUuNUsgICAkMCAgICAgICAgJDc0NiAgICAgICAzNiUgICAgICAgICAmbHQ7QlImZ3Q7IElOU1RBTExNRU5UOiAkMzMuOUsgICAgICQgICAgICAgICAkNDMuMksgICAkMCAgICAgICAgJDE0OCAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IFRPVEFMUzogICAgICAkNzguM0sgICAgICQ1Ni4wSyAgICAkNzguOEsgICAkMCAgICAgICAgJDg5NCAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0mbHQ7QlImZ3Q7IFQgUiBBIEQgRSBTICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IFNVQk5BTUUgICAgICBTVUJDT0RFICAgT1BFTkVEICBISUdIQ1JFRCBURVJNUyAgICAgTUFYREVMUSAgUEFZUEFUICAxLTEyIE1PUCAmbHQ7QlImZ3Q7IEFDQ09VTlQjICAgICAgICAgICAgICAgVkVSRklFRCBDUkVETElNICBQQVNURFVFICAgQU1ULU1PUCAgUEFZUEFUIDEzLTI0ICAgICAmbHQ7QlImZ3Q7IEVDT0EgQ09MTEFUUkwvTE9BTlRZUEUgQ0xTRC9QRCBCQUxBTkNFICBSRU1BUktTICAgICAgICAgICAgICAgIE1PIDMwLzYwLzkwICAmbHQ7QlImZ3Q7IENIQVNFIENBUkQgICBCIDI2UUswMDEgIDUvMTQgICAkMTk1NCAgICBNSU41MCAgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDQ2NDAxODIwOTMyMCAgICAgICAgICAgIDMvMThBICAkNDAwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgRkxFWCBTUE5EIENSVCBDUkQgICAgICAgICAkMTc5MyAgICAgICAgICAgICAgICAgICAgICAgICAgNDYgICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IFNZTkNCL0xPVyAgICBMIDIzNTA0MUogIDYvMTMgICAkNzIzOCAgICBNSU4xNzggICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDc5ODE5MjQwMzE0OSAgICAgICAgICAgIDMvMThBICAkODEwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgQ0hBUkdFIEFDQ09VTlQgICAgICAgICAgICAkNzIyOSAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IEFNRVggICAgICAgICBCIDIxV0IwMDEgIDYvMTUgICAkNjg4MyAgICAgICAgICAgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDM0OTk5MjIzODc1MzIyMDMgICAgICAgIDMvMThBICAkNzAwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgQ1JFRElUIENBUkQgICAgICAgICAgICAgICAkNDgyMiAgICAgICAgICAgICAgICAgICAgICAgICAgMzIgICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IENIQVNFIENBUkQgICBCIDI2UUswMDEgIDcvMTcgICAkMTQ4MiAgICBNSU4yNSAgICAgICAgICAgICAgMTExMTExMTEgICAgIFIwMSAmbHQ7QlImZ3Q7IDQyNjY4NDE1MzA1MSAgICAgICAgICAgIDMvMThBICAkMzAwMCAgICAkMCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IEkgICAgQ1JFRElUIENBUkQgICAgICAgICAgICAgICAkMTI5MyAgICAgICAgICAgICAgICAgICAgICAgICAgOCAgICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IFdFTExTIEZBUkdPICBCIDkwOE42NjQgIDMvMTAgICAkNTY2MCAgICBNSU4xMzUgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDQ0NjU0MjA1OTE1MSAgICAgICAgICAgIDMvMThBICAkNzAwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgQ1JFRElUIENBUkQgICAgICAgICAgICAgICAkNTQ1OCAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IFRIRC9DQk5BICAgICBCIDI2SDMwMDUgMTEvMTIgICAkNjM3NiAgICBNSU4xNDEgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDYwMzUzMjAzNzczOSAgICAgICAgICAgIDMvMThBICAkNjAwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgQ0hBUkdFIEFDQ09VTlQgICAgICAgICAgICAkNTM3MSAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IFNZTkNCL0hERlVSTiBGIDk5OTI5NjggIDIvMTQgICAkMzQ4OSAgICAgICAgICAgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDYwMzQ2MTA5MTAyNSAgICAgICAgICAgIDMvMThBICAkNDkwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgQ0hBUkdFIEFDQ09VTlQgICAgMTAvMTcgICAkMCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IERJU0NPVkVSQkFOSyBCIDk2MTYwMDMgIDQvMTQgICAkNTExMyAgICBNSU45NyAgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDYwMTEwMDI1NDA5NCAgICAgICAgICAgIDMvMThBICAkNjUwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgQ1JFRElUIENBUkQgICAgICAgICAgICAgICAkNDgyMyAgICAgICAgICAgICAgICAgICAgICAgICAgNDYgICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IENCTkEgICAgICAgICBCIDI5MkYwMzEgIDkvMTMgICAkMjcxMiAgICBNSU4yNyAgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDQyNjkzODA4NjQ5MyAgICAgICAgICAgIDMvMThBICAkNDAwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgQ1JFRElUIENBUkQgICAgICAgICAgICAgICAkMTg1OCAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IERFU0VSVEZJTkFOQyBRIDI5OTYwMDQgMTIvMTcgICAkODA3NiAgICAwNjNNMTQ4ICAgICAgICAgICAgMTEgICAgICAgICAgIEkwMSAmbHQ7QlImZ3Q7IDE0MDA2OTgwMjBMMjEwMCAgICAgICAgIDMvMThBICAgICAgICAgICAkMCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IEkgICAgQVVUT01PQklMRSAgICAgICAgICAgICAgICAkNzg0NCAgICAgICAgICAgICAgICAgICAgICAgICAgMiAgICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IERFUFRFRE5FTE5FVCBFIDJDTk4wMDEgIDIvMTAgICAkMjkyOCAgICAzMDBNICAgICAgICAgICAgICAgMTExMTExMTExMTExIEkwMSAmbHQ7QlImZ3Q7IDkwMDAwMDEyMjAyNDI0OSAgICAgICAgIDIvMThBICAgICAgICAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgU1RVREVOVCBMT0FOICAgICAgICAgICAgICAkNDg1MiAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IERFUFRFRE5FTE5FVCBFIDJDTk4wMDEgIDEvMTEgICAkNDUwMCAgICAzMDBNICAgICAgICAgICAgICAgMTExMTExMTExMTExIEkwMSAmbHQ7QlImZ3Q7IDkwMDAwMDIwNzY2NDI0OSAgICAgICAgIDIvMThBICAgICAgICAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgU1RVREVOVCBMT0FOICAgICAgICAgICAgICAkNTEyNSAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IERFUFRFRE5FTE5FVCBFIDJDTk4wMDEgIDEvMTEgICAkNjAwMCAgICAzMDBNICAgICAgICAgICAgICAgMTExMTExMTExMTExIEkwMSAmbHQ7QlImZ3Q7IDkwMDAwMDIwNzY2NDM0OSAgICAgICAgIDIvMThBICAgICAgICAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgU1RVREVOVCBMT0FOICAgICAgICAgICAgICAkOTQzNSAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IERFUFRFRE5FTE5FVCBFIDJDTk4wMDEgIDMvMTEgICAkNDUwMCAgICAzMDBNICAgICAgICAgICAgICAgMTExMTExMTExMTExIEkwMSAmbHQ7QlImZ3Q7IDkwMDAwMDIyNDYwMDc0OSAgICAgICAgIDIvMThBICAgICAgICAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgU1RVREVOVCBMT0FOICAgICAgICAgICAgICAkNTEyNSAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IERFUFRFRE5FTE5FVCBFIDJDTk4wMDEgIDMvMTEgICAkNDQ0OCAgICAzMDBNICAgICAgICAgICAgICAgMTExMTExMTExMTExIEkwMSAmbHQ7QlImZ3Q7IDkwMDAwMDIyNDYwMDg0OSAgICAgICAgIDIvMThBICAgICAgICAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgU1RVREVOVCBMT0FOICAgICAgICAgICAgICAkNjgwNiAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IEVMQU4gRklOIFNWQyBCIDI3NDkwMDEgMTIvMTEgICAkMTg1NCAgICBNSU41MCAgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDQwMzc2NjAwMjQ4OCAgICAgICAgICAgIDIvMThBICAkMzUwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgQ1JFRElUIENBUkQgICAgICAgICAgICAgICAkMTk0MCAgICAgICAgICAgICAgICAgICAgICAgICAgNDggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbHQ7QlImZ3Q7IEVMQU4gRklOIFNWQyBCIDI3NDkwMDEgMTAvMTUgICAkMTY3NSAgICBNSU40MyAgICAgICAgICAgICAgMTExMTExMTExMTExIFIwMSAmbHQ7QlImZ3Q7IDM3MjUxMjAwMTM3ICAgICAgICAgICAgIDIvMThBICAkMjAwMCAgICAkMCAgICAgICAgICAgICAgICAgMTExMTExMTExMTExICAgICAmbHQ7QlImZ3Q7IEkgICAgRkxFWCBTUE5EIENSVCBDUkQgICAgICAgICAkMTAwMiAgICAgICAgICAgICAgICAgICAgICAgICAgMjggICAwLyAwLyAwICAmbHQ7QlImZ3Q7ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg';
        insert dlCR;
        
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
    }
    
    @isTest static void gets_decision_logic_rules(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Status__c = 'Documents Requested';
        update opp;
        
        DecisionLogicGDSResponse response = DecisionLogicSectionCtrl.getDecisionLogicRulesList(opp.Id);
        
        System.assertEquals(null, response);
    }
    
    @isTest static void gets_decision_logic_rules_with_account(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Status__c = 'Documents Requested';
        opp.DL_Account_Number_Found__c = '1234';
        update opp;
        
        DecisionLogicGDSResponse response = DecisionLogicSectionCtrl.getDecisionLogicRulesList(opp.Id);
        
        System.debug('DL Rules ' + response);
    }
    
    @isTest static void gets_decision_logic_rules_with_none_account(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Status__c = 'Documents Requested';
        opp.DL_Account_Number_Found__c = '--None--';
        update opp;
        
        DecisionLogicGDSResponse response = DecisionLogicSectionCtrl.getDecisionLogicRulesList(opp.Id);
        
        System.assertEquals(null, response);
    }
    
    @isTest static void gets_decision_logic_rules_with_funded_opp(){
        Opportunity opp = LibraryTest.createApplicationTH();
        opp.Status__c = 'Funded';
        opp.DL_Account_Number_Found__c = '1234';
        update opp;
        
        DecisionLogicGDSResponse response = DecisionLogicSectionCtrl.getDecisionLogicRulesList(opp.Id);
        
        System.assertEquals(null, response);
    }
    
    @isTest static void gets_decision_logic_rules_automatic_expired(){
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        
        Opportunity opp = LibraryTest.createApplicationTH();
        
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Test.startTest();                                                      
        opp.Status__c = 'Documents Requested';
        opp.DL_Account_Number_Found__c = '1234';
        update opp;
        Test.stopTest();
                                                                      
		Decision_Logic_Rules__c dlRules = new Decision_Logic_Rules__c();
        dlRules.Opportunity__c = opp.Id;
        dlRules.GDS_Response__c = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        dlRules.LastCallDate__c = System.today().addDays(-10);
        dlRules.ManualCall__c = false;
        insert dlRules;
        
        DecisionLogicGDSResponse response = DecisionLogicSectionCtrl.getDecisionLogicRulesList(opp.Id);
        
    }
    
    @isTest static void gets_decision_logic_rules_funded_expired(){
        ProxyApiBRMSUtil.settings = LibraryTest.fakeSettings();
        
        Opportunity opp = LibraryTest.createApplicationTH();
        
        SingleRequestMockTest fakeResponse = new SingleRequestMockTest(201,
                'OK',
                '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}',
                null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Test.startTest();                                                      
        opp.Status__c = 'Funded';
        opp.DL_Account_Number_Found__c = '1234';
        update opp;
        Test.stopTest();
                                                                      
		Decision_Logic_Rules__c dlRules = new Decision_Logic_Rules__c();
        dlRules.Opportunity__c = opp.Id;
        dlRules.GDS_Response__c = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        dlRules.LastCallDate__c = System.today().addDays(-10);
        dlRules.ManualCall__c = false;
        insert dlRules;
        
        DecisionLogicGDSResponse response = DecisionLogicSectionCtrl.getDecisionLogicRulesList(opp.Id);
        
    }
    
    @isTest static void validates_funded(){
        Opportunity opp = LibraryTest.createApplicationTH();                               
        opp.Status__c = 'Funded';
        update opp;
        
        Boolean result = DecisionLogicSectionCtrl.validateDecisionLogicAccountNumber(opp.Id);
        
        System.assertEquals(false,result);   
    }
    
    @isTest static void validates_account_number_null(){
        Opportunity opp = LibraryTest.createApplicationTH();                               
        opp.Status__c = 'Documents Requested';
        update opp;
        
        Boolean result = DecisionLogicSectionCtrl.validateDecisionLogicAccountNumber(opp.Id);
        
        System.assertEquals(false,result);   
    }
    
    @isTest static void validates_account_number_none(){
        Opportunity opp = LibraryTest.createApplicationTH();                               
        opp.Status__c = 'Documents Requested';
        opp.DL_Account_Number_Found__c = '--None--';
        update opp;
        
        Boolean result = DecisionLogicSectionCtrl.validateDecisionLogicAccountNumber(opp.Id);
        
        System.assertEquals(false,result);   
    }
    
    @isTest static void validates_account_number(){
        Opportunity opp = LibraryTest.createApplicationTH();                               
        opp.Status__c = 'Documents Requested';
        opp.DL_Account_Number_Found__c = '5645';
        update opp;
        
        Boolean result = DecisionLogicSectionCtrl.validateDecisionLogicAccountNumber(opp.Id);
        
        System.assertEquals(true,result);   
    }
    
    @isTest static void inserts_dl_object(){
        Opportunity opp = LibraryTest.createApplicationTH();                               
        Integer beforeInsert = [SELECT COUNT() FROM Decision_Logic_Rules__c];
        String GDSResponse = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14"}';
        DecisionLogicSectionCtrl.upsertDecisionLogiRulesObjectAfterCallout(opp.Id, GDSResponse);
        Integer afterInsert = [SELECT COUNT() FROM Decision_Logic_Rules__c];
        System.assert(afterInsert > beforeInsert,'Inserts DL Rule Object');
    }
    
    @isTest static void updates_dl_object(){
        Opportunity opp = LibraryTest.createApplicationTH();                               
        Decision_Logic_Rules__c dlRules = new Decision_Logic_Rules__c();
        dlRules.Opportunity__c = opp.Id;
        dlRules.GDS_Response__c = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        dlRules.LastCallDate__c = System.today().addDays(-10);
        dlRules.ManualCall__c = false;
        insert dlRules;
        String GDSResponse = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14"}';
        DecisionLogicSectionCtrl.upsertDecisionLogiRulesObjectAfterCallout(opp.Id, GDSResponse);
        System.assertEquals(System.today(),[SELECT LastCallDate__c FROM Decision_Logic_Rules__c WHERE Opportunity__c =: opp.Id LIMIT 1].LastCallDate__c,'Updates DL Rule Object');
    }
    
    @isTest static void gets_decision_logic_object(){
        Opportunity opp = LibraryTest.createApplicationTH();                               
        Decision_Logic_Rules__c dlRules = new Decision_Logic_Rules__c();
        dlRules.Opportunity__c = opp.Id;
        dlRules.GDS_Response__c = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        dlRules.LastCallDate__c = System.today().addDays(-10);
        dlRules.ManualCall__c = false;
        insert dlRules;
        Decision_Logic_Rules__c result = DecisionLogicSectionCtrl.getDecisionLogicRulesObject(opp.Id);
        System.assertNotEquals(null, result);
    }
    
    @isTest static void gets_decision_logic_object_null(){
        Opportunity opp = LibraryTest.createApplicationTH();
        Decision_Logic_Rules__c result = DecisionLogicSectionCtrl.getDecisionLogicRulesObject(opp.Id);
        System.assertEquals(null, result);
    }
    
    @isTest static void upserts_decision_logic_rule_no_object(){
        Opportunity opp = LibraryTest.createApplicationTH();
        String GDSResponse = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        Boolean result = DecisionLogicSectionCtrl.upsertDecisionLogicRuleObject(opp.Id, GDSResponse);
        System.assertEquals(false, result);
    }
    
    @isTest static void upserts_decision_logic_rule_object(){
        Opportunity opp = LibraryTest.createApplicationTH();
        String GDSResponse = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        Decision_Logic_Rules__c dlRules = new Decision_Logic_Rules__c();
        dlRules.Opportunity__c = opp.Id;
        dlRules.GDS_Response__c = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        dlRules.LastCallDate__c = System.today().addDays(-10);
        dlRules.ManualCall__c = false;
        insert dlRules;
        Boolean result = DecisionLogicSectionCtrl.upsertDecisionLogicRuleObject(opp.Id, GDSResponse);
        System.assertEquals(false, result);
    }
    
    @isTest static void gets_manual_call(){
        Opportunity opp = LibraryTest.createApplicationTH();
        Decision_Logic_Rules__c dlRules = new Decision_Logic_Rules__c();
        dlRules.Opportunity__c = opp.Id;
        dlRules.GDS_Response__c = '{"sessionId":"8accaa93-dbc0-36ed-5ab0-0483de89b9df","sequenceId":"14","productId":"","channelId":"","executionType":"","ruleVersion":"7B-0A-F3-BB-90-1D-F4-EB","deApplicationRisk":"1","deRuleViolated":"1","deStrategy_Type":"1","rulesList":[{"deRuleNumber":"1003","deRuleName":"BankStatements_Error","rulePriority":"100","deRuleValue":"","deQueueAssignment":"Level 1","deRuleStatus":"Fail","deBRMS_ReasonCode":"","deIsDisplayed":"Yes","deSequenceCategory":"5","deRuleCategory":"Higher Risk","deActionInstructions":"We were unable to process EmailAge, consider manually running Bank Statements.","deHumanReadableDescription":"The goal of this rule is to inform the end user if the system was unable to process Bank Statements."}],"scorecardAcceptance":"Yes","errors":[{"errorMessage":"Bank Statement Timeout Error","errorDetails":"True - Timeout Error - Web Request returned error: The operation has timed out"},{"errorMessage":"Bank Statement Communication Error","errorDetails":"True - get-specific-transactions =>Main result object in Process Result is null -> Report not found"},{"errorMessage":"Credit Report Parsing Error","errorDetails":"Unable to process credit report data, an invalid report was received"}]}' ; 
        dlRules.LastCallDate__c = System.today().addDays(-10);
        dlRules.ManualCall__c = false;
        insert dlRules;
        DecisionLogicGDSResponse response = DecisionLogicSectionCtrl.getManualCallDecisionLogicRulesFromProxyAPI(opp.Id);
    }
    
}