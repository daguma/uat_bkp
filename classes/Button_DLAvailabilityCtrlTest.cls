@isTest public with sharing class Button_DLAvailabilityCtrlTest {

    static testMethod void test1() {

        Proxy_Api__c settings = LibraryTest.fakeSettings();
        Contact TestContact = LibraryTest.createContactTH();
        Opportunity opp = TestHelper.createApplicationFromContact(TestContact,true);

        Button_DLAvailabilityCtrl.DLAvailability da = Button_DLAvailabilityCtrl.checkForDLAvailable('021001318', '', opp.Id);
        system.assertNotEquals(null, da.successMessage);

        da = Button_DLAvailabilityCtrl.checkForDLAvailable('', 'My Bank', opp.Id);
        system.assertEquals(null, da.errorMessage);

    }

}