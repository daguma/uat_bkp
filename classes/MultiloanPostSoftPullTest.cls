@isTest public class MultiloanPostSoftPullTest {
    
    @testSetup static void initializes_objects(){
        
        FinwiseDetails__c FinwiseParams = new FinwiseDetails__c(name = 'finwise', Finwise_Base_URL__c = 'https://mpl-lab.finwisebank.com/Services/data/v1.0/json/partner/', Apply_State_Restriction__c = false,
        Finwise_LendingPartnerId__c = '5', Finwise_LendingPartnerProductId__c = '7',
        Finwise_LoanStatusId__c = '1', finwise_partner_key__c = 'testKey', Finwise_product_Key__c = 'testkey',
        Finwise_RateTypeId__c = '1', Whitelisted_States__c = 'MO');
        insert FinwiseParams;

        Finwise_Asset_Class_Ids__c AsetCls = NEW Finwise_Asset_Class_Ids__c(name = 'POS', Asset_Id__c = 5, SFDC_Value__c = false);
        insert AsetCls;
        MultiLoan_Settings__c multiSet = new MultiLoan_Settings__c();
        multiSet.name = 'settings';
        multiSet.Check_Frequency__c = 30;
        multiSet.Maximum_Loan_Amount__c = 5000;
        multiSet.Minimum_Loan_Amount__c = 1000;
        multiSet.Days_To_First_Check__c = 60;
        multiSet.AccountId__c = '0011700000t45FtAAI';
        multiSet.Minimum_Fico__c = 600;
        multiSet.Maximum_Loan_Amount_LG__c = 3000;
        insert multiSet;
        
        Account acc = new Account();
        acc.Name ='Multi-Loan'; 
        insert acc;
    }
    
    @isTest static void createApp_executeProcess(){
    
        loan__loan_Account__c contract = creates_contract();
        update contract;
        
        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Contact__c = contract.loan__Contact__c;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
     
        
         Multiloan_Params__c multiloanParams = null;
         if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        multiloanParams.FICO__c = 727;
        multiloanParams.Grade__c = 'A2';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-90);
        upsert multiloanParams;
        
        Test.startTest();
        MultiloanPostSoftPull.executeProcess(contract.Id, multiloanParams);
		Test.stopTest();
    }
    
    @isTest static void inserts_eligibilityCheckGetCR_funded() {

        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
		
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT Id 
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        Opportunity newApp = [SELECT Id, Name, Status__c, Contact__c, Is_Selected_Offer__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.CreditPull_Date__c = Date.today().addDays(-60);
        newApp.Status__c = 'Declined (or Unqualified)';
        newApp.Type = 'Multiloan';

        
        multiloanParams.FICO__c =730;
        multiloanParams.Grade__c = 'A1';
        multiloanParams.Eligible_For_Multiloan__c = true;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-90);
        multiloanParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id + ','+ newApp.Is_Selected_Offer__c + ',' +newApp.Status__c;
        upsert multiloanParams;
        
        multiloanParams = [SELECT Id, Contract__c, FICO__c, Grade__c, New_FICO__c, 
			New_Grade__c, Eligible_For_Multiloan__c, IsLowAndGrow__c, Last_MultiLoan_Eligibility_Review__c,
			Eligible_For_Soft_Pull_Params__c, Eligible_For_Soft_Pull__c, System_Debug__c
                FROM Multiloan_Params__c
                WHERE Id = : multiloanParams.Id];
        
        Multiloan_Params_Logs__c multiLogs = new Multiloan_Params_Logs__c();
        multiLogs.Multiloan_Params__c = multiloanParams.Id;
        insert multiLogs;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'A1';
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;
        
        Test.startTest();

	    MultiloanPostSoftPull.eligibilityCheckGetCR(multiloanParams);
      
        Test.stopTest();
        newApp = [SELECT Id, Name, Status__c, Contact__r.Name FROM Opportunity WHERE Id = : appId LIMIT 1];

        //System.assertEquals('Credit Qualified', newApp.Status__c);
       
    }
    private static loan__loan_Account__c creates_contract() {
        loan__loan_Account__c contract = LibraryTest.createContractTH();
        contract.loan__ACH_Frequency__c = 'Monthly';
        contract.loan__Principal_Remaining__c = 3000;
        contract.loan__Fees_Remaining__c = 200;
        contract.loan__Interest_Accrued_Not_Due__c = 500;
        contract.loan__Loan_Amount__c = 4000;
        update contract;
		
        Opportunity opp = [Select Id, Maximum_Loan_Amount__c from Opportunity where Id =: contract.Opportunity__c ];
        opp.Maximum_Loan_Amount__c = 8700;
        upsert opp;
        
        Offer__c off = new Offer__c();
        off.IsSelected__c = true;
        off.Opportunity__c = contract.Opportunity__c;
        off.APR__c = 0.2;
        off.Fee__c = 100;
        off.Fee_Percent__c = 0.04;
        off.FirstPaymentDate__c = Date.today().addDays(-29);
        off.Installments__c = 34;
        off.Loan_Amount__c = 5000;
        off.Term__c = 34;
        off.Payment_Amount__c = 230;
        off.Repayment_Frequency__c = 'Monthly';
        off.Preferred_Payment_Date__c = Date.today().addDays(-23);
        off.Total_Loan_Amount__c = 3000;
        insert off;

        return contract;
    }
    
    @isTest static void creates_new_MultiloanOpportunity_AccountManagement() {

        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Maximum_Loan_Amount__c = 8500;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        fundedApp.CreditPull_Date__c = Date.today().addDays(-6);
        upsert fundedApp;
        
        Opportunity opp2 = [Select Id, Name, PrimaryCreditReportId__c FROM Opportunity WHERE id =: fundedApp.Id];
     
		//Contract__r.Opportunity__r.Maximum_Loan_Amount__c - Contract__r.loan__Loan_Amount__c
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        update contract;
        
        AccountManagementHistory__c accM = New AccountManagementHistory__c();
        accM.Opportunity__c = fundedApp.Id;
        accM.CreditReportId__c = fundedApp.PrimaryCreditReportId__c;
        accM.FicoValue__c = 729;
        accM.Contract__c = contract.Id;
        insert accM;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, CreditPull_Date__c = Date.today().addDays(-6));
        upsert opp;

        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, LoanAmountCheck__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 600;
        multiloanParams.Grade__c = 'B2';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false' + ',' + opp.Status__c;
        upsert multiloanParams;
        
        Multiloan_Params__c paramstest = [SELECT id, System_Debug__c, Contract__c, LoanAmountCheck__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c, Eligible_For_Soft_Pull_Params__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id];


        Integer appsBefore = [SELECT COUNT() FROM Opportunity];
        Test.startTest();
        MultiloanPostSoftPull.eligibilityCheckGetCR(paramstest);
        Test.stopTest();

        
        
    }
    
    @isTest static void evaluate_catchExeption() {
     
        
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Maximum_Loan_Amount__c = 12500;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
        Opportunity opp2 = [Select Id, Name, PrimaryCreditReportId__c FROM Opportunity WHERE id =: fundedApp.Id];
     

        loan__loan_Account__c contract = creates_contract();
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        update contract;
        
        AccountManagementHistory__c accM = New AccountManagementHistory__c();
        accM.Opportunity__c = fundedApp.Id;
        accM.CreditReportId__c = fundedApp.PrimaryCreditReportId__c;
        accM.FicoValue__c = 729;
        accM.Contract__c = contract.Id;
        insert accM;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, CreditPull_Date__c = Date.today().addDays(-60));
        upsert opp;

        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, LoanAmountCheck__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 600;
        multiloanParams.Grade__c = 'B2';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id;
        upsert multiloanParams;

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];
        Test.startTest();
        MultiloanPostSoftPull.eligibilityCheckGetCR(multiloanParams);
        Test.stopTest();
  
        System.assertNotEquals('', [SELECT System_Debug__c FROM Multiloan_Params__c WHERE Id =: multiloanParams.Id ORDER BY CreatedDate DESC LIMIT 1].System_Debug__c);
        
        
    }
    
    @isTest static void maximum_difference_maximum(){
        
		loan__loan_Account__c contract = creates_contract();  
        contract.loan__Loan_Amount__c = 4000;
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Contact__c = contract.loan__Contact__c;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        fundedApp.Maximum_Loan_Amount__c = 4500;
        upsert fundedApp;
        
        
        contract.Opportunity__c = fundedApp.Id;
        update contract;
        
        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, FICO__c, Grade__c, LoanAmountCheck__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c 
                                                         FROM Multiloan_Params__c
                                                         WHERE Contract__c = : contract.Id LIMIT 1];
        
         Multiloan_Params__c multiloanParams = null;
         if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        multiloanParams.FICO__c = 727;
        multiloanParams.Grade__c = 'A2';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-90);
        upsert multiloanParams;
        
        Test.startTest();
        MultiloanPostSoftPull.executeProcess(contract.Id, multiloanParams);
		Test.stopTest();
    }
    
    @isTest static void Validate_FicoFail_TenPoints() {
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
		
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT Id 
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        Opportunity newApp = [SELECT Id, Name, Status__c, Contact__c, Is_Selected_Offer__c FROM Opportunity WHERE Id = : appId LIMIT 1];

        newApp.CreditPull_Date__c = Date.today().addDays(-60);
        newApp.Status__c = 'Declined (or Unqualified)';
        newApp.Type = 'Multiloan';

        
        multiloanParams.FICO__c =736;
        multiloanParams.Grade__c = 'A1';
        multiloanParams.Eligible_For_Multiloan__c = true;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-90);
        multiloanParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id + ','+ newApp.Is_Selected_Offer__c + ',' +newApp.Status__c;
        upsert multiloanParams;
        
        multiloanParams = [SELECT Id, Contract__c, FICO__c, Grade__c, New_FICO__c, 
			New_Grade__c, Eligible_For_Multiloan__c, IsLowAndGrow__c, Last_MultiLoan_Eligibility_Review__c,
			Eligible_For_Soft_Pull_Params__c, Eligible_For_Soft_Pull__c, System_Debug__c
                FROM Multiloan_Params__c
                WHERE Id = : multiloanParams.Id];
        
        Multiloan_Params_Logs__c multiLogs = new Multiloan_Params_Logs__c();
        multiLogs.Multiloan_Params__c = multiloanParams.Id;
        insert multiLogs;

        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'A1';
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;
        
        Test.startTest();

	    MultiloanPostSoftPull.eligibilityCheckGetCR(multiloanParams);
      
        Test.stopTest();
        newApp = [SELECT Id, Name, Status__c, Contact__r.Name FROM Opportunity WHERE Id = : appId LIMIT 1];
        
   
    }
    
    @isTest static void Validate_FicoFail_fifteenPoints() {
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
		
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Frequency_of_Loan_Payment__c = 'Weekly';
        contract.loan__Payment_Frequency_Cycle__c = 4;
        update contract;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        
        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT Id 
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        Opportunity newApp = [SELECT Id, Name, Status__c, Contact__c, Is_Selected_Offer__c FROM Opportunity WHERE Id = : appId LIMIT 1];
        
        Scorecard__c score = new Scorecard__c();
        score.Grade__c = 'B2';
        score.Opportunity__c = newApp.Id;
        score.Acceptance__c = 'Y';
        insert score;

        newApp.CreditPull_Date__c = Date.today().addDays(-60);
        newApp.Status__c = 'Declined (or Unqualified)';
        newApp.Type = 'Multiloan';
        newApp.Scorecard_Grade__c = 'B2';
        
        
        Scorecard__c scorecard = [SELECT Id, Grade__c, Opportunity__c, Acceptance__c, FICO_Score__c
                                       FROM Scorecard__c
                                       WHERE Id =: score.Id];
        
        scorecard.Grade__c = 'B2';
        update scorecard;
        
        
        
        
        multiloanParams.FICO__c =736;
        multiloanParams.Grade__c = 'A1';
        multiloanParams.Eligible_For_Multiloan__c = true;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-90);
        multiloanParams.Eligible_For_Soft_Pull_Params__c= contract.Opportunity__c + ',' + appId + ',' + contract.Id + ','+ newApp.Is_Selected_Offer__c + ',' +newApp.Status__c;
        upsert multiloanParams;
        
        multiloanParams = [SELECT Id, Contract__c, FICO__c, Grade__c, New_FICO__c, 
			New_Grade__c, Eligible_For_Multiloan__c, IsLowAndGrow__c, Last_MultiLoan_Eligibility_Review__c,
			Eligible_For_Soft_Pull_Params__c, Eligible_For_Soft_Pull__c, System_Debug__c
                FROM Multiloan_Params__c
                WHERE Id = : multiloanParams.Id];
        
        Multiloan_Params_Logs__c multiLogs = new Multiloan_Params_Logs__c();
        multiLogs.Multiloan_Params__c = multiloanParams.Id;
        insert multiLogs;

       
        
        Test.startTest();

	    MultiloanPostSoftPull.eligibilityCheckGetCR(multiloanParams);
      
        Test.stopTest();
        newApp = [SELECT Id, Name, Status__c, Contact__r.Name FROM Opportunity WHERE Id = : appId LIMIT 1];
        
   
    }
    
    @isTest static void Validate_AgeCAncellation_Status() {
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Maximum_Loan_Amount__c = 7000;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
        Opportunity opp2 = [Select Id, Name, PrimaryCreditReportId__c FROM Opportunity WHERE id =: fundedApp.Id];
     
		//Contract__r.Opportunity__r.Maximum_Loan_Amount__c - Contract__r.loan__Loan_Amount__c
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        update contract;
        
        AccountManagementHistory__c accM = New AccountManagementHistory__c();
        accM.Opportunity__c = fundedApp.Id;
        accM.CreditReportId__c = fundedApp.PrimaryCreditReportId__c;
        accM.FicoValue__c = 729;
        accM.Contract__c = contract.Id;
        insert accM;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, Status__c = 'Aged / Cancellation', CreditPull_Date__c = Date.today().addDays(-60));
        upsert opp;

        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, LoanAmountCheck__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 600;
        multiloanParams.Grade__c = 'B2';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false' + ',' + opp.Status__c;
        upsert multiloanParams;
        
        Multiloan_Params__c parmastest = [SELECT id, Contract__c, LoanAmountCheck__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c, Eligible_For_Soft_Pull_Params__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id];
        

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];
        Test.startTest();
        MultiloanPostSoftPull.eligibilityCheckGetCR(parmastest);
        Test.stopTest();
        
   
    }
    
    @isTest static void Validate_Decline_Status() {
        LibraryTest.createBrmsConfigSettings(-3, false);
        LibraryTest.createBrmsSeqIds();
        LibraryTest.createBrmsRuleOnScreenConfig();
        LibraryTest.brmsRulesConfig();
        
        Opportunity fundedApp = LibraryTest.createApplicationTH();
        fundedApp.Maximum_Loan_Amount__c = 7000;
        fundedApp.PrimaryCreditReportId__c = 8749683;
        fundedApp.Status__c = 'Funded';
        fundedApp.Estimated_Grand_Total__c = 75000;
        upsert fundedApp;
        
        Opportunity opp2 = [Select Id, Name, PrimaryCreditReportId__c FROM Opportunity WHERE id =: fundedApp.Id];
     
		//Contract__r.Opportunity__r.Maximum_Loan_Amount__c - Contract__r.loan__Loan_Amount__c
        loan__loan_Account__c contract = creates_contract();
        contract.loan__Loan_Amount__c = 4000;
        contract.Opportunity__c = fundedApp.Id;
        update contract;
        
        AccountManagementHistory__c accM = New AccountManagementHistory__c();
        accM.Opportunity__c = fundedApp.Id;
        accM.CreditReportId__c = fundedApp.PrimaryCreditReportId__c;
        accM.FicoValue__c = 729;
        accM.Contract__c = contract.Id;
        insert accM;
        
        SubmitApplicationService submit = new SubmitApplicationService();
		String appId = submit.submitApplication(contract.loan__Contact__c);
        Opportunity opp = new Opportunity(Id = appId, Status__c = 'Declined (or Unqualified', CreditPull_Date__c = Date.today().addDays(-6));
        upsert opp;
        
        System.debug('opp');

        LIST<Multiloan_Params__c> multiloanParamsList = [SELECT id, Contract__c, LoanAmountCheck__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id
                                    LIMIT 1];

        Multiloan_Params__c multiloanParams = null;

        if (multiloanParamsList.size() > 0) {
            multiloanParams = multiloanParamsList.get(0);
        } else {
            multiloanParams = new Multiloan_Params__c();
            multiloanParams.Contract__c = contract.Id;
        }
        
        multiloanParams.FICO__c = 600;
        multiloanParams.Grade__c = 'B2';
        multiloanParams.Eligible_For_Multiloan__c = false;
        multiloanParams.Last_MultiLoan_Eligibility_Review__c = Date.today().addDays(-35);
        multiloanParams.Eligible_For_Soft_Pull_Params__c = contract.Opportunity__c + ',' + appId + ',' + contract.Id + ',false' + ',' + opp.Status__c;
        upsert multiloanParams;
        
        Multiloan_Params__c parmastest = [SELECT id, Contract__c, LoanAmountCheck__c, FICO__c, Grade__c, Eligible_For_Multiloan__c, Last_MultiLoan_Eligibility_Review__c, Eligible_For_Soft_Pull_Params__c
                FROM Multiloan_Params__c
                WHERE Contract__c = : contract.Id];
        

        Integer appsBefore = [SELECT COUNT() FROM Opportunity];
        Test.startTest();
        MultiloanPostSoftPull.eligibilityCheckGetCR(parmastest);
        Test.stopTest();
        
   
    }
    

}