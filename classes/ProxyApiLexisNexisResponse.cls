public with sharing class ProxyApiLexisNexisResponse {
	
	public String riskView2ResponseEx {get; set;}

	public String lnJdgNote {get; set;}
}