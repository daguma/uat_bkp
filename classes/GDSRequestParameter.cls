global class GDSRequestParameter {

    public String applicationId {get;set;}

    public String contactId {get;set;}
    
    public String channelId {get;set;}
    
    public String sequenceId {get;set;}
    
    public String productId {get;set;}
    
    public String sessionId {get;set;}
    
    public String executionType {get;set;}
    
    public String apiSessionId{get;set;}
    
    public String organizationId {get;set;}
    
    public String organizationURL {get;set;}
    
    public boolean isClarityValidate {get;set;} //CNR-5

    public map<String,String> ExternalParams {get;set;} //RD-279
}