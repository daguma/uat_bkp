@isTest public class CLContractAlertSectionCtrlTest {
    
    @isTest Static void Validate_CLContractAlersection_Data(){
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        
        loan__loan_Account__c contract = [SELECT Id, Name, loan__Contact__c, Opportunity__c FROM loan__Loan_Account__c where Id =: cnt.Id];
        
        Contact con = [SELECT Id, Name, MailingPostalCode FROM Contact where Id =: contract.loan__Contact__c];
        
        //This code return isProjectTransition = True
        Flag_Accounts__c facc = New Flag_Accounts__c();
        facc.Name = contract.Name;
        insert facc;
        
        //This code return isZipCodeFIL = True
        FIL_Zip_Codes__c FZC = New FIL_Zip_Codes__c();
        FZC.Name = con.MailingPostalCode ;
        Insert FZC;
        
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        CLContractAlertSectionCtrl Alert = New CLContractAlertSectionCtrl(controller);
        
    }
    
    
    @isTest Static void Validate_CLContractAlersection_refiMessage(){
        
        Opportunity opp = LibraryTest.createapplicationTH();
        opp.Type = 'Refinance';
        opp.Status__c = 'Refinance Qualified';
        upsert opp;
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.Opportunity__c = opp.Id;
        update cnt;
        
        loan__loan_Account__c contract = [SELECT Id, Name, loan__Contact__c, Opportunity__c FROM loan__Loan_Account__c where Id =: cnt.Id];
        
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        CLContractAlertSectionCtrl Alert = New CLContractAlertSectionCtrl(controller);
        
        system.assertEquals('This customer is refinance qualified.', 'This customer is refinance qualified.');
        
    }
    
    @isTest Static void Validate_CLContractAlersection_Multiloan(){
        
        Opportunity opp = LibraryTest.createapplicationTH();
        opp.Type = 'Multiloan';
        opp.Status__c = 'Credit Qualified';
        upsert opp;
        
        loan__loan_Account__c cnt = LibraryTest.createContractTH();
        cnt.Opportunity__c = opp.Id;
        update cnt;
        
        loan__loan_Account__c contract = [SELECT Id, Name, loan__Contact__c, Opportunity__c FROM loan__Loan_Account__c where Id =: cnt.Id];
        
        Apexpages.StandardController controller = New Apexpages.StandardController(contract);
        CLContractAlertSectionCtrl Alert = New CLContractAlertSectionCtrl(controller);
        
        system.assertEquals('This customer is eligible for a Multi-Loan opportunity.', 'This customer is eligible for a Multi-Loan opportunity.');
        
    }

}