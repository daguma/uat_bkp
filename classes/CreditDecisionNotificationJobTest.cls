@istest
public class CreditDecisionNotificationJobTest {

    @isTest static void schedule_job() {
        Test.startTest();

        CreditDecisionNotificationJob notificationJob = new CreditDecisionNotificationJob();
        String cron = '0 0 23 * * ?';
        system.schedule('Test RBP Notification Job', cron, notificationJob);

        Test.stopTest();
    }

    @isTest static void updates_rbp_sent_date() {
        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.status__c = 'Credit Qualified';
        app.LeadSource__c = 'Online Aggregator';
        app.Lead_Sub_Source__c = 'LendingTree';
        app.Fico__c = '711';
        app.Payment_Amount__c = 200;
        app.Bureau__c = 'TU';
        update app;

        Datetime newDate = Datetime.now().addDays(-26);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        List<Opportunity> appsList = [SELECT Id, Name, Customer_Name__c, Contact__c, CreatedDate, Encrypted_AppId_AES__c, LeadSource__c, Lead_Sub_Source__c
                FROM Opportunity
                WHERE Status__c NOT IN ('Funded', 'Credit Approved - No Contact Allowed', 'Declined (or Unqualified)')];

        CreditDecisionNotificationJob noiaNotification = new CreditDecisionNotificationJob();
        noiaNotification.execute(bc, appsList);

        Test.stopTest();
        
        
        Opportunity result = [SELECT RBP_Sent_Date__c
                              FROM Opportunity
                              WHERE id = : app.id LIMIT 1];
        
        System.assertEquals(Date.today(), result.RBP_Sent_Date__c); 
    }

    @isTest static void updates_rbp_sent_date_with_lead_source() {
        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.status__c = 'Credit Qualified';
        app.Lead_Sub_Source__c = 'Test';
        app.LeadSource__c = 'Organic';
        app.Type = 'New';
                app.Bureau__c = 'TU';

        app.Fico__c = '711';
        app.Payment_Amount__c = 200;
        update app;

        Datetime newDate = Datetime.now().addDays(-26);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        List<Opportunity> appsList = [SELECT Id, Name, Customer_Name__c, Contact__c, CreatedDate, Encrypted_AppId_AES__c, LeadSource__c, Lead_Sub_Source__c
                FROM Opportunity
                WHERE Status__c NOT IN ('Funded', 'Credit Approved - No Contact Allowed', 'Declined (or Unqualified)')];

        CreditDecisionNotificationJob noiaNotification = new CreditDecisionNotificationJob();
        noiaNotification.execute(bc, appsList);

        Test.stopTest();

              Opportunity result = [SELECT RBP_Sent_Date__c
                          FROM Opportunity
                          WHERE id = : app.id LIMIT 1];

        System.assertEquals(Date.today(), result.RBP_Sent_Date__c);
         
    }

    @isTest static void does_not_update_rbp_sent_date() {
        Database.BatchableContext bc;

        Opportunity app = LibraryTest.createApplicationTH();
        app.status__c = 'Credit Approved - No Contact Allowed';
        app.Lead_Sub_Source__c = 'Test';
        app.LeadSource__c = 'Test';
        app.Fico__c = '711';
                app.Bureau__c = 'TU';

        app.Payment_Amount__c = 200;
        update app;

        Datetime newDate = Datetime.now().addDays(-24);

        Test.setCreatedDate(app.id, newDate);

        Test.startTest();

        List<Opportunity> appsList = [SELECT Id, Name, Customer_Name__c, Contact__c, CreatedDate, Encrypted_AppId_AES__c, LeadSource__c, Lead_Sub_Source__c
                FROM Opportunity
                WHERE Status__c NOT IN ('Funded', 'Credit Approved - No Contact Allowed', 'Declined (or Unqualified)')
                AND Contact__r.NOIA_Notification_Sent_Date__c <> TODAY ORDER BY Id DESC];

        CreditDecisionNotificationJob noiaNotification = new CreditDecisionNotificationJob();
        noiaNotification.execute(bc, appsList);

        Test.stopTest();

        Opportunity result = [SELECT RBP_Sent_Date__c
                          FROM Opportunity
                          WHERE id = : app.id LIMIT 1];

        System.assertEquals(null, result.RBP_Sent_Date__c);
    }

}