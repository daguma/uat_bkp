public with sharing class ClContractJobHelper {

    public static final String MODIFICATION_TRACKING_QUERY =  'SELECT Id, createdDate, CL_Contract__r.Modification_Type__c, CL_Contract__r.Agreement_Modified_Date__c, CL_Contract__c, CreatedById, ' +
        ' Created_By_Name__c, Email_Body__c, Modification_Type__c, CL_Contract__r.Name, ' +
        ' NotMarkedAsModified__c, System_Debug__c, Other_Loan_Transaction__c ' +
        ' FROM OtherTransactionLogs__c ' + 
        ' WHERE NotMarkedAsModified__c = false ' +
        (Test.isRunningTest() ? ' AND  createdDate = TODAY ' : ' AND  createdDate = YESTERDAY') +
        (Test.isRunningTest() ? ' AND CL_Contract__c NOT IN ' +
        ' (SELECT Contract__c ' +
        ' FROM LoanModificationHistory__c ' +
        ' WHERE createdDate = TODAY)' : ' AND CL_Contract__c NOT IN ' +
        ' (SELECT Contract__c ' +
        ' FROM LoanModificationHistory__c ' +
        'WHERE createdDate = YESTERDAY) ');

    public static void SendAlerts(OtherTransactionLogs__c otherTransaction) {
    	String emailBody = '';
        Boolean sendAlert = true;

    	try {
            if(otherTransaction.CL_Contract__r.Agreement_Modified_Date__c != null && 
                otherTransaction.CL_Contract__r.Modification_Type__c != null) {
                    
                if(otherTransaction.CL_Contract__r.Agreement_Modified_Date__c.date() == otherTransaction.createdDate.date())
                    sendAlert = false;
            }
            
            User user = [SELECT Name, ProfileId FROM User WHERE id =: otherTransaction.CreatedById LIMIT 1];
            
            if(Label.Profiles_Mark_Modified_No_Alert.contains(user.ProfileId))
                sendAlert = false;
            

            if(sendAlert){
                List < GroupMember > serivingManagers = [SELECT Id, UserOrGroupId, GroupId
                                FROM GroupMember
                                WHERE GroupId =: Label.Servicing_Managers_Group_Id   
                               ];

                emailBody = 'On ' +otherTransaction.CreatedDate + ', ' + otherTransaction.Created_By_Name__c + '  made a modification for ' +otherTransaction.Modification_Type__c + ' on '+ otherTransaction.CL_Contract__r.Name + '.';
            
                for (GroupMember member: serivingManagers) {
                    salesForceUtil.EmailByUserId('Modification Made Without Tracking', emailBody, member.UserOrGroupId);
                }
                otherTransaction.NotMarkedAsModified__c = true;    
                otherTransaction.Email_Body__c = emailBody;
            }
    	} catch (Exception e) {
    		otherTransaction.System_Debug__c = 'ERROR: ' + e.getMessage() + ' Line: ' + e.getLineNumber();
    	}finally{
            update otherTransaction;
        }
    }
}