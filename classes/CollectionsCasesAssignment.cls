public class CollectionsCasesAssignment {

    public static void assignAndCommitCases(
        List<CollectionsCasesAssignment__c> scope, 
        CollectionsCasesAssignmentStructure assignmentStructure,
        CollectionsCasesAssignmentAudit auditProcess){

        List<Case> upsertTheseCases = new List<Case>();
        List<loan__loan_account__c> updateTheseContracts = new List<loan__loan_account__c>();

        for(CollectionsCasesAssignment__c collectionsCasesAssignment : scope) {

            try{

                if(collectionsCasesAssignment.CreateCase__c == true || collectionsCasesAssignment.UpdateCase__c == true){

                    Id newCaseOwnerId = collectionsCasesAssignment.caseOwnerNewId__c != null || !String.isEmpty(collectionsCasesAssignment.caseOwnerNewId__c)
                                    ? collectionsCasesAssignment.caseOwnerNewId__c 
                                    : assignmentStructure.getNextAssigneeId(collectionsCasesAssignment.loadBalanceGroup__c);

                    if(newCaseOwnerId != null){

                        collectionsCasesAssignment.assigned_to__c = newCaseOwnerId;
                        collectionsCasesAssignment.Assignment_Result__c = 'Assigned';

                        if(collectionsCasesAssignment.CreateCase__c == true){
                            upsertTheseCases.add(getCase(collectionsCasesAssignment, true, newCaseOwnerId));
                        }else if(collectionsCasesAssignment.UpdateCase__c == true){
                            upsertTheseCases.add(getCase(collectionsCasesAssignment, false, newCaseOwnerId));
                        }
                        updateTheseContracts.add(getContract(collectionsCasesAssignment.CL_Contract__c,newCaseOwnerId)); 
                    }
                }else{
                    collectionsCasesAssignment.Assignment_Result__c = 'Did not reassign';
                    if(collectionsCasesAssignment.CL_Contract__r.Collection_Case_Owner__c != collectionsCasesAssignment.Case__r.OwnerId){
                        updateTheseContracts.add(getContract(collectionsCasesAssignment.CL_Contract__c,collectionsCasesAssignment.Case__r.OwnerId));
                    }
                }                

            }catch(Exception e){
                collectionsCasesAssignment.Assignment_Result__c = e.getMessage().left(255);
            }
            
        }

        Database.UpsertResult[] upsertTheseCasesResult;
        try{
            upsertTheseCasesResult = Database.upsert(upsertTheseCases,false);
            auditProcess.auditAssignCaseJob(upsertTheseCasesResult);
        }catch(Exception e){
            auditProcess.emailAuditors('Collections Cases Assignment: exception occurred', ' Var upsertTheseCasesResult: ' + JSON.serialize(upsertTheseCasesResult) + ' Var upsertTheseCases: ' + JSON.serialize(upsertTheseCases) + ' \n\n Exception in code: ' + e.getMessage());
        }
        Map<Id,Id> newCasesIdsForScope = new Map<Id,Id>();
        for(Case myCase : upsertTheseCases){
            newCasesIdsForScope.put(myCase.CL_Contract__c,myCase.Id);        
        }
        
        Database.SaveResult[] updateTheseContractsResult;
        try{
            updateTheseContractsResult = Database.update(updateTheseContracts,false);
            auditProcess.auditAssignContractJob(updateTheseContractsResult);
        }catch(Exception e){
            auditProcess.emailAuditors('Collections Cases Assignment: exception occurred', ' Var updateTheseContractsResult: ' + JSON.serialize(updateTheseContractsResult) + ' Var updateTheseContracts: ' + JSON.serialize(updateTheseContracts) + ' \n\n Exception in code: ' + e.getMessage());
        }
        
        for(CollectionsCasesAssignment__c myScopeItem : scope){
            if(myScopeItem.CreateCase__c == true){
                myScopeItem.CaseCreated__c = newCasesIdsForScope.get(myScopeItem.CL_Contract__c);
            }
        }
        Database.SaveResult[] updateTheScopeResult;
        try{
            updateTheScopeResult = Database.update(scope,false);
            auditProcess.auditAssignCCAJob(updateTheScopeResult);
            auditProcess.emailDebugToAuditors('Collections Cases Assignment: Assign: ',JSON.serialize(new List<Object>{upsertTheseCasesResult,upsertTheseCases,updateTheseContractsResult,updateTheseContracts,updateTheScopeResult,scope}));

        }catch(Exception e){
            auditProcess.emailAuditors('Collections Cases Assignment: exception occurred', 'Var updateTheScopeResult: ' + JSON.serialize(updateTheScopeResult) + ' \n\n Exception in code: ' + e.getMessage());
        }
    }

    private static loan__loan_account__c getContract(Id contractId, Id caseOwner){
        return new loan__Loan_Account__c(id = contractId, Collection_Case_Owner__c = caseOwner);
    }

    private static Case getCase(CollectionsCasesAssignment__c collectionsCasesAssignment, Boolean isNewCase, Id newCaseOwnerId){
        if(isNewCase){
            return new Case(
                OwnerId = newCaseOwnerId, 
                RecordTypeId = Label.RecordType_Collections_Id, 
                CL_Contract__c = collectionsCasesAssignment.CL_Contract__c,
                Subject = 'A collection case has been created for ' + collectionsCasesAssignment.CL_Contract__r.name,
                Description = 'A collection case has been created for ' + collectionsCasesAssignment.CL_Contract__r.name,
                Status = 'New',
                Origin = 'Collections',
                Priority = 'Medium',
                Opportunity__c = collectionsCasesAssignment.CL_Contract__r.Opportunity__c,
                ContactId = collectionsCasesAssignment.CL_Contract__r.loan__Contact__c,
                Automatic_Assignment__c = true,
                Case_Assignment_Date__c = Date.today()
            );
        }else{
            return new Case(
                Id = collectionsCasesAssignment.Case__c, 
                OwnerId = newCaseOwnerId != null ? newCaseOwnerId : collectionsCasesAssignment.caseOwnerNewId__c, 
                Status = 'New',
                Automatic_Assignment__c = true,
                Case_Assignment_Date__c = Date.today()
            );
        }
    }

}