public with sharing class AchScreenMainCtrl {

    @AuraEnabled
    public static AchScreenEntity getAchScreenEntity(Id oppId) {

        if (String.isEmpty(oppId))         return null;

        Opportunity opp = [SELECT Id, Type, contact__c, Decline_Reason__c, status__c, Lead_Sub_Source__c FROM Opportunity WHERE Id = :oppId LIMIT 1];
        Contact con;
        if (opp != null)
            con = [SELECT Id, Do_Not_Contact__c FROM Contact WHERE Id = :opp.Contact__c LIMIT 1];

        AchScreenEntity ase = new AchScreenEntity(opp, con);

        if (!String.isEmpty(opp.status__c) &&
                !String.isEmpty(opp.Decline_Reason__c) &&
                'Declined (or Unqualified)'.equalsIgnoreCase(opp.status__c) &&
                'Did not pass disqualifiers at Hard Pull'.equalsIgnoreCase(opp.Decline_Reason__c)) {
            ase.warningMessagesList.add('The application has been declined after the hard credit report was pulled. You cannot move forward with this application.');
        }

        if (opp.Id != null && 'Credit Karma'.equals(opp.Lead_Sub_Source__c)) {
            ase.warningMessagesList.add('Please be aware that you cannot contact this customer in any way unless there is already an offer accepted by the customer with us.');
        }

        return ase;
    }

    public class AchScreenEntity {
        @AuraEnabled public Boolean isRefinance { get; set; }
        @AuraEnabled public Opportunity opportunity {
            get;
            set {
                opportunity = value;
                if (opportunity != null) {
                    if ('Refinance'.equals(opportunity.Type)) {
                        isRefinance = true;
                    }
                }
            }
        }
        @AuraEnabled public Contact contact { get; set; }
        @AuraEnabled public Boolean hasWarning { get; set; }
        @AuraEnabled public List<String> warningMessagesList {
            get;
            set {
                warningMessagesList = value;
                if (warningMessagesList != null && !warningMessagesList.isEmpty()) {
                    hasWarning = true;
                }
            }
        }

        public AchScreenEntity(Opportunity opp, Contact con) {
            this.isRefinance = false;
            this.hasWarning = false;
            this.warningMessagesList = new List<String>();
            this.opportunity = opp;
            this.contact = con;
        }
    }
}