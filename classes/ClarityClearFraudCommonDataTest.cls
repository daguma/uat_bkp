@isTest
public class ClarityClearFraudCommonDataTest {
    static testmethod void validate_instance() {
        Test.startTest();
        ClarityClearFraudCommonData ccf = new ClarityClearFraudCommonData();
        Test.stopTest();
        
        System.assertEquals(null ,ccf.one_minute_ago);
        System.assertEquals(null ,ccf.ten_minutes_ago);
        System.assertEquals(null ,ccf.one_hour_ago);
        System.assertEquals(null ,ccf.twentyfour_hours_ago);
        System.assertEquals(null ,ccf.seven_days_ago);
        System.assertEquals(null ,ccf.fifteen_days_ago);
        System.assertEquals(null ,ccf.thirty_days_ago);
        System.assertEquals(null ,ccf.ninety_days_ago);
        System.assertEquals(null ,ccf.threesixtyfive_days_ago);
    }
    
    static testmethod void validate_assign() {
        Test.startTest();
        ClarityClearFraudCommonData ccf = new ClarityClearFraudCommonData();
        Test.stopTest();
        
        ccf.one_minute_ago= 1;
        ccf.ten_minutes_ago= 1;
        ccf.one_hour_ago= 1;
        ccf.twentyfour_hours_ago= 1;
        ccf.seven_days_ago= 1;
        ccf.fifteen_days_ago= 1;
        ccf.thirty_days_ago= 1;
        ccf.ninety_days_ago= 1;
        ccf.threesixtyfive_days_ago= 1;
        
        System.assertEquals(1 ,ccf.one_minute_ago);
        System.assertEquals(1 ,ccf.ten_minutes_ago);
        System.assertEquals(1 ,ccf.one_hour_ago);
        System.assertEquals(1 ,ccf.twentyfour_hours_ago);
        System.assertEquals(1 ,ccf.seven_days_ago);
        System.assertEquals(1 ,ccf.fifteen_days_ago);
        System.assertEquals(1 ,ccf.thirty_days_ago);
        System.assertEquals(1 ,ccf.ninety_days_ago);
        System.assertEquals(1 ,ccf.threesixtyfive_days_ago);
        
    }
}