global class AANNotificationJob implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts, Database.Stateful {

    public OrgWideEmailAddress noRe;
    string EMAIL_IGNORE = 'no@lendingpoint.com';
    public Datetime  cutoffDate = Datetime.now().addDays(-90);
    Email_Age_Settings__c emailAgeSettings =  Email_Age_Settings__c.getValues('settings');
    LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();
    String query = '', msgErrors = '';
    Integer iRecsProcessed = 0;
    public Integer count = 0, offCount = 0;    
    private static final string RULE_STATUS_PASS = 'Pass'; //BRMS Change
    
    global void execute (SchedulableContext sc) {
        Integer iNumReportsToProcess = cuSettings != null && cuSettings.AAN_To_Send_Per_Batch__c == null ? 9 : cuSettings.AAN_To_Send_Per_Batch__c.intValue();

        AANNotificationJob job = new AANNotificationJob();
        System.debug('Starting AANNotificationJob at ' + Date.today());
        Database.executeBatch(job, iNumReportsToProcess);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        try {
           Map<Id, Opportunity> apps;
           Map<Id, Contact> mContacts;
           
           boolean isSandbox = OfferCtrlUtil.runningInASandbox();
           cutoffDate = Datetime.now().addDays(-100);
           
            // Add TODAY LIMIT FOR Prod
           query = 'SELECT ID FROM Opportunity WHERE AAN_Sent_Date__c = null AND CreatedDate > ' + cutOffDate.format('yyyy-MM-dd') + 'T00:00:00.000Z AND CreatedDate < YESTERDAY AND Type = \'New\' ';
           query += ' AND Name like \'APP%\' AND (Partner_Account__c = null OR Partner_Account__r.Disable_AAN__c = false) ';

           if (!isSandbox && !Test.isRunningTest()) {
                query += ' AND ( Partner_Account__c = null OR ' +
                        '       (Partner_Account__c <> null and CreatedDate < THIS_WEEK and AAN_Notification_Trigger_Date__c = null) OR ' +
                        '        Partner_Account__r.Suppress_AAN__c = false OR ( ' +
                        '        AAN_Notification_Trigger_Date__c <> null AND ' +
                        '        AAN_Notification_Trigger_Date__c <= TODAY ' +
                        '        ) ) ';
            }
            query += ' AND AAN_Required__c = true ' +
                    ' ORDER BY CreatedDate ASC';

            iRecsProcessed = 0;
        } catch(exception ex){
           System.debug('Exception:'+ ex.getMessage());
           WebToSFDC.notifyDev('AANNotificationJob.QueryLocator ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
        }
        system.debug('==query============='+query);
        return Database.getQueryLocator(query);    
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope) {
        try { 
            system.debug('==scope============='+scope);
        
            list<Opportunity> theApps = (List<Opportunity>)scope;
            Set<Id> appIds = new Set<Id>();
            for (Opportunity app : theApps) {
                appIds.add(app.Id);
            }
            System.debug('Apps to Send AAN for = ' + appIds.size());
            AANNotification(appIds);

        } catch(exception ex){
            System.debug('Exception:'+ ex.getMessage());
            WebToSFDC.notifyDev('AANNotificationJob ERROR', 'Error on ' + ex.getLineNumber() + '\n Detail = ' + ex.getMessage(), UserInfo.getUserId());
        }
    }
 
    public void AANNotification(Set<Id> appIds) {
        String HtmlValue = '', Subject = '', tName = '', lastAppName = '';
        try {
            List<Outbound_Emails_Temp__c> AppEmails = new List<Outbound_Emails_Temp__c>();
            List<Note> notesEmails = new List<Note>();
            List<Opportunity> updatedApps = new List<Opportunity>(), updatedAmountApps = new List<Opportunity>();
            LP_Custom__c cuSettings = LP_Custom__c.getOrgDefaults();

            Map< Id, Opportunity> apps;
            boolean isSandbox = OfferCtrlUtil.runningInASandbox(),  thirdPartyCheck, sendEmail = false;
            Decimal maxLoanAmnt = 0;
            Contact theContact = null;
            EmailTemplate eTemplate;
            Map<String , EmailTemplate> templates = new Map<String, emailTemplate>(); 

            try {
                noRe = [SELECT Id, DisplayName FROM OrgWideEmailAddress WHERE DisplayName = 'LendingPoint-NoReply' ];
            } catch (Exception e) {
                System.debug ('[U-03] Unable to locate OrgWideEmailAddress using name: ' + 'LendingPoint-NoReply' + ' refer to Setup | Email Communcations ' );
                return;
            }

            for (EmailTemplate t : [SELECT name, Subject, Body FROM EmailTemplate where Name like '%AAN%']){
                templates.put(t.Name, t);
            }
            System.debug('Templates for AAN : ' + templates.size());

            apps = new Map<Id, Opportunity>(
                    [SELECT ID,CreditPull_Date__c, Lead_Sub_Source__c, LexisNexisDeclined__c,  Contact__r.Email, Contact__r.Id, 
                     Contact__r.Name, Contact__r.MailingStreet, Contact__r.MailingCity, Is_Feb__C, Contact__r.EmailBouncedDate,
                     Contact__r.MailingState, Contact__r.MailingPostalCode,   CreatedDate, StrategyType__c, ScoreCard_Grade__c,
                     Status__c, Is_Finwise__c, Reason_of_Opportunity_Status__c, Contact__c, AAN_Sent_Date__c, 
                     KeyFactors__c, Clarity_Status__c, Partner_Account__r.Disable_AAN__c, Partner_Account__r.Name, 
                     Partner_Account__r.AAN_Email_Template__c, FICO__c, Type, Contract_Renewed__c, Bureau__c, Name,
                     Requested_Loan_Amount__c,Maximum_Loan_Amount__c , AAN_Template__c, AAN_Required__c, Maximum_Offer_Amount__c
                     FROM Opportunity WHERE Id in : appIds]);
            
            List<Offer__c> listOffers = [Select Id, Loan_Amount__c, Total_Loan_amount__c, Opportunity__c from Offer__c where Opportunity__c in: appIds];

            System.debug('Applications to Send Notification too ' + apps.size());
            

            Map<Id, Email_Age_Details__c> mAges = new Map<Id, Email_Age_Details__c>();
            for (Email_Age_Details__c eAge : [select Id, Contact__c, Actual_Credit_Decision__c, Email_Age_Status__c, is_Email_Age_Timed_Out__c, errorCode__c, status__c from Email_Age_Details__c 
                                               where Contact__c in (select Contact__c from Opportunity where Id in: apps.keySet())  order by Contact__c, CreatedDate desc]) {
                                                   mAges.put(eAge.Contact__c, eAge);
                                               }
            
            ProxyApiCreditReportEntity creditReportEntity;
            for (Opportunity theApp : apps.values() ) {
                loan_Refinance_Params__c thisRefiParam = null;
                lastAppName = theApp.Name;
                if (theApp.Type == 'Refinance') {
                    for (loan_Refinance_Params__c thePara : [ SELECT  ManualEligibilityCheck__c
                                  FROM    loan_Refinance_Params__c 
                                  WHERE   Contract__c =: theApp.Contract_Renewed__c 
                                  ORDER BY CreatedDate DESC LIMIT 1]){
                        thisRefiParam = thePara;
                    }
                }
                if (theApp.Type == 'New' || (theApp.Type == 'Refinance' && thisRefiParam != null && thisRefiParam.ManualEligibilityCheck__c)) {
                    try {
                        try {
                            creditReportEntity = CreditReport.getByEntityId(theApp.Id, true);
                        } catch (Exception ex) {
                            creditReportEntity = null;
                            System.debug('CR Get Exception = ' + ex.getStackTraceString());
                        }

                        theContact = theApp.Contact__r;
                        if (theContact != null ) {
                            maxLoanAmnt = 0;
                            thirdPartyCheck =  isThirdPartyQualify(theApp,creditReportEntity, mAges);
                            tName = theApp.AAN_Template__c ;
                            System.debug('Template Name To Use = ' + tName + ' appID = ' + theApp.Id + ' - Created on ' + theApp.CreatedDate);
                            eTemplate = templates.get(tName);
                            System.debug('Max Maximum_Offer_Amount__c = ' + theApp.Maximum_Offer_Amount__c);
                            System.debug('Max Requested_Loan_Amount__c = ' + theApp.Requested_Loan_Amount__c);
                            if ('Unable to Verify Income|Documents Rejected|Documents Rejected – EWS/FactorTrust Rejected|Declined (or Unqualified)|Refinance Unqualified'.contains(theApp.Status__c) || (theApp.Maximum_Offer_Amount__c != null && theApp.Maximum_Offer_Amount__c > 0 && theApp.Requested_Loan_Amount__c > theApp.Maximum_Offer_Amount__c) ) {
                                
                                HtmlValue = replaceValuesFromData(eTemplate.Body, theContact, creditReportEntity, theApp, thirdPartyCheck);
                                Subject = replaceValuesFromData(eTemplate.Subject, theContact, creditReportEntity, theApp,  thirdPartyCheck);
                                
                                updatedApps.add(theApp);
                                sendEmail = theContact.EmailBouncedDate == null && String.isNotEmpty(theContact.Email)  &&  theContact.Email != EMAIL_IGNORE;
                                theApp.AAN_Sent_Date__c = DateTime.now();
                                theApp.AAN_Sent_By__c = sendEmail ?  'EMAIL' : 'USPS';
                                if (sendEmail) AppEmails.add(createEmail(HtmlValue, Subject, theContact.Id ));
                                notesEmails.add(createNote( (sendEmail ? 'To: ' + theContact.Email + '\n\n' : '') + HtmlValue, Subject, theApp.Id ));
                                iRecsProcessed++;
                                count++;
                            } else {
                                for (Offer__c off : listOffers) {
                                    if (off.Opportunity__c == theApp.Id && maxLoanAmnt > off.Total_Loan_amount__c ) maxLoanAmnt = off.Total_Loan_amount__c;
                                }
                                // MER-133
                                // if ((maxLoanAmnt == 0 || maxLoanAmnt == NULL) && theApp.ScoreCard_Grade__c != null && theApp.StrategyType__c != null ) maxLoanAmnt = ScorecardUtil.maxAmount(theApp.ScoreCard_Grade__c, theApp.StrategyType__c);
                                // System.debug('Max Loan = ' + maxLoanAmnt);
                                // theApp.Maximum_Loan_Amount__c = maxLoanAmnt;
                                // theApp.Maximum_Offer_Amount__c = maxLoanAmnt;
                                updatedAmountApps.add(theApp);
                                offCount++;
                            }
                        } else System.debug('No Contact for v1 AppId = ' + theApp.Id);
                    } catch (Exception e) {
                        System.debug('Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString() + '\nLast App Name = ' + lastAppName);
                    }
                }
            }

            System.debug('Sending App Emails # : ' + AppEmails.size());

            if ((AppEmails.size() > 0) || Test.isRunningTest() ) insert AppEmails;
            if (notesEmails.size() > 0) {
                insert notesEmails;
                update updatedApps;
            } 

            if (updatedAmountApps.size() > 0) update updatedAmountApps;
        } catch (Exception e) {
            WebToSFDC.notifyDev('AANNotification Error ', 'Error ' + e.getMessage() + ' - ' + e.getLineNumber() + ' - ' + e.getStackTraceString()  + '\nLast App Name = ' + lastAppName, UserInfo.getUserId() ) ;
        }
    }

    private boolean isApproved(ProxyApiCreditReportEntity cr, Integer dq){
        return cr.getAttributeEntity(dq) != null && cr.getAttributeEntity(dq).isApproved; 
    }

    public string replaceValuesFromData(String theValue, Contact theContact, ProxyApiCreditReportEntity creditReportEntity, Opportunity theOpp,  boolean bThirdParty) {
        boolean bHasCR = creditReportEntity != null, dontRunBRMS = CreditReport.validateBrmsFlag(theOpp); 
        map<string, BrmsRulesWrapper> brmsRulesMap = new map<string, BrmsRulesWrapper> ();
        if(!dontRunBRMS && bHasCR) {
            bHasCR = creditReportEntity <> null; 
            brmsRulesMap = displayBrmsRulesMap(creditReportEntity);
        }
        
        String sFico = String.isEmpty(theOpp.FICO__c) || theOpp.FICO__c.length() < 3 || theOpp.FICO__c.startsWith('900') ? 'N/A' : theOpp.FICO__c;
        String kFac = theOpp.KeyFactors__c != null ? theOpp.KeyFactors__c : ' ';
        kFac = !bHasCR ||  creditReportEntity.brms == null || sFico == 'N/A' ? 'CREDIT REPORT UNAVAILABLE' : kFac;
        String bureau = !bHasCR || creditReportEntity.datasourceName == null ? '' : ( creditReportEntity.datasourceName.equalsIgnoreCase('ExpConsUSV7') ? 'Experian' : 'Transunion');
        if (string.isBlank(bureau)) bureau = String.isEmpty(theOpp.Bureau__c) ? 'N/A' : ( theOpp.Bureau__c == 'EX' ? 'Experian' : 'Transunion');
            
        theValue = theValue.replace('{!LetterDate}', (theOpp.CreditPull_Date__c != null) ? Datetime.newInstance(theOpp.CreditPull_Date__c, DateTime.now().Time() ).format('MMMM d,  yyyy') : Datetime.now().format('MMMM d,  yyyy'));

        if (theContact != null) {
            theValue = theValue.replace('{!Contact.Name}', theContact.Name);
            theValue = theValue.replace('{!Contact.MailingStreet}', theContact.MailingStreet != null ? theContact.MailingStreet : '' );
            theValue = theValue.replace('{!Contact.MailingCity}', (theContact.MailingCity != null) ? theContact.MailingCity : '');
            theValue = theValue.replace('{!Contact.MailingState}', theContact.MailingState != null ? theContact.MailingState : '');
            theValue = theValue.replace('{!Contact.MailingPostalCode}', theContact.MailingPostalCode != null ? theContact.MailingPostalCode : '');
        }

        theValue = theValue.replace('{!Disqualifier.Open_Credit_Lines__c}', bHasCR && isApproved(creditReportEntity, CreditReport.NUMBER_TRADES) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.Age_of_Credit_File__c}', bHasCR && isApproved(creditReportEntity,CreditReport.AGE_CREDIT_FILE) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.Employment__c}', checkDisqualifiers(bHasCR, creditReportEntity, brmsRulesMap, dontRunBRMS, 'Employment', CreditReport.EMPLOYMENT) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.PTI__c}', '');
        theValue = theValue.replace('{!Disqualifier.DTI__c}', checkDisqualifiers(bHasCR, creditReportEntity, brmsRulesMap, dontRunBRMS, 'DTI', CreditReport.DTI) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.Current_Delinquencies__c}', checkDisqualifiers(bHasCR, creditReportEntity, brmsRulesMap, dontRunBRMS, 'Current Delinquencies', CreditReport.DELINQUENCIES)  ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.Tax_Liens__c}', checkDisqualifiers(bHasCR, creditReportEntity, brmsRulesMap, dontRunBRMS, 'Tax Liens', CreditReport.TAX_LIENS) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.ChargeOff__c}', checkDisqualifiers(bHasCR, creditReportEntity, brmsRulesMap, dontRunBRMS, 'Charge Off', CreditReport.CHARGE_OFF) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.Open_Bankruptcies__c}', checkDisqualifiers(bHasCR, creditReportEntity, brmsRulesMap, dontRunBRMS, 'Open Bankruptcies', CreditReport.OPEN_BANKRUPTCIES) ? '_' : 'X');      
        theValue = theValue.replace('{!Disqualifier.Inquiries_in_last_6_months__c}', checkDisqualifiers(bHasCR, creditReportEntity, brmsRulesMap, dontRunBRMS, 'Inquiries Past 6 Months', CreditReport.INQUIRIES_PAST_6_MONTHS) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.Inquiries_in_last_6_Number}', '');       
        theValue = theValue.replace('{!NoCreditReport}', sFico == 'N/A' ? 'X' : ' ' );       
        theValue = theValue.replace('{!NoKeyFactors}', kFac == 'CREDIT REPORT UNAVAILABLE' ? 'X' : ' ' );       
        
        theValue = theValue.replace('{!Disqualifier.FICO__c}', bHasCR && isApproved(creditReportEntity, CreditReport.FICO) ? '_' : 'X'); //|| (kFactors != null && kFactors.length() > 0) ) ? 'X' : '_');
        
        theValue = theValue.replace('{!Disqualifier.Revolving_Utilization__c}', checkDisqualifiers(bHasCR, creditReportEntity, brmsRulesMap, dontRunBRMS, 'Revolving Utilization', CreditReport.REVOLVING_UTILIZATION) ? '_' : 'X');      
        
        theValue = theValue.replace('{!Disqualifier.Revolving_Open_to_Buy__c}', bHasCR && isApproved(creditReportEntity,CreditReport.REVOLVING_OPEN_TO_BUY) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.Use_of_Funds__c}', bHasCR && isApproved(creditReportEntity,CreditReport.USE_OF_FUNDS) ? '_' : 'X');
        theValue = theValue.replace('{!Disqualifier.Income_Verification}', (theOpp != null && theOpp.Status__c != null && theOpp.Status__c.contains('Unable to Verify Income') ) ? 'X' : '_');

        theValue = theValue.replace('{!CreditReport.CreditBureau}', bureau);
        theValue = theValue.replace('{!CreditReport.CreditBureauAddress}', bureau == 'N/A' ? 'N/A' : ( bureau.equalsIgnoreCase('Experian') ? 'EXPERIAN\nPO Box 2002\nALLEN, TX 75013\n' : 'TRANSUNION LLC\n2 Baldwin Place \nPO Box 1000\nCHESTER, PA 19016\n' ));
        theValue = theValue.replace('{!CreditReport.CreditBureauPhone}', bureau == 'N/A' ? 'N/A' : ( bureau.equalsIgnoreCase('Experian') ? '(888) 397-3742' : 'TEL. (800) 888-4213'));
        theValue = theValue.replace('{!CreditReport.Credit_Score_Value__c}', sFico == 'N/A' ? 'FICO UNAVAILABLE' : sFico);
        theValue = theValue.replace('{!CreditReport.KeyFactors}', kFac);
        theValue = theValue.replace('{!Application.Lead_Sub_Source__c}', ( theOpp.Partner_Account__c != null && String.isNotEmpty(theOpp.Partner_Account__r.Name) ? theOpp.Partner_Account__r.Name : ( String.isNotEmpty(theOpp.Lead_Sub_Source__c) ? theOpp.Lead_Sub_Source__c : 'LendingPoint') ) );

        theValue = theValue.replace('{!ThirdPartyInfo}', bThirdParty ? 'X' : '_' );
        theValue = theValue.replace('{!ThirdPartyDecision}', bThirdParty ? 'was' : 'was not' );

        return theValue;
    }


    public Outbound_Emails_Temp__c createEmail(String htmlBody, String subject, Id ContactID ) {
        return new Outbound_Emails_Temp__c(Contact__c = ContactID, Body__c = htmlBody, Subject__c = subject);

    }
    public Note createNote(String htmlBody, String subject, Id ContactID ) {
        return new Note(ParentId = ContactID, Body = htmlBody, Title = subject);
    }
    
    //Email Age changes
    public boolean isThirdPartyQualify(Opportunity theApp,ProxyApiCreditReportEntity creditReportEntity, Map<Id, Email_Age_Details__c> mAges){
        boolean dontRunBRMS = CreditReport.validateBrmsFlag(theApp.id), thirdPartyCheck = false;
        if(String.isNotEmpty(theApp.Reason_of_Opportunity_Status__c) && theApp.Reason_of_Opportunity_Status__c == 'Cannot Verify Information Provided' && creditReportEntity <> null) {
            thirdPartyCheck = !theApp.Clarity_Status__c;
            if (!thirdPartyCheck && mAges.containsKey(theApp.Contact__c)) {
                Email_Age_Details__c theAge = mAges.get(theApp.Contact__c);
                if(dontRunBRMS)
                    thirdPartyCheck =  emailAgeSettings.Call_Email_age__c && creditReportEntity.decision && creditReportEntity.automatedDecision.equalsIgnoreCase('No') && !theAge.Email_Age_Status__c ; //if application declined due to email age
                else 
                    thirdPartyCheck = creditReportEntity.brms <> null && creditReportEntity.brms.autoDecision.equalsIgnoreCase('No') && !theAge.Email_Age_Status__c ; //if application declined due to email age
            }
        }
        if (!thirdPartyCheck ) thirdPartyCheck = theApp.LexisNexisDeclined__c ;
        if (!thirdPartyCheck && creditReportEntity != null) thirdPartyCheck = !Decisioning.isClarityClearRecentHistoryPassed(creditReportEntity.clearRecentHistory);

        return thirdPartyCheck;
    }

    global void finish(Database.BatchableContext BC){
            WebToSFDC.notifyDev('AANNotification Finished', 'AAN Sent = ' + count + ' Offer data updated = ' + offCount, UserInfo.getUserId() );
    }
    
    private boolean checkDisqualifiers(boolean bHasCR, ProxyApiCreditReportEntity creditReportEntity, map<string,BrmsRulesWrapper> brmsRulesMap, boolean dontRunBRMS, string attributeName, integer attributeNumber) {
        if(!dontRunBRMS && bHasCR && brmsRulesMap.containskey(ValidationTabCtrl.getRuleNumber(attributeName))) {
            BrmsRulesWrapper brmsRuleWrapper = brmsRulesMap.get(ValidationTabCtrl.getRuleNumber(attributeName));
            system.debug('---brmsRuleWrapper ---'+brmsRuleWrapper);
            if(brmsRuleWrapper.ruleValue != null && brmsRuleWrapper.rulePass) return True;
        }
        else if(bHasCR && isApproved(creditReportEntity, attributeNumber))
                return  True;

        return false;
    }
    
    //method to generate the BRMS Wrapper map to indicate the 
    public static Map<String,BrmsRulesWrapper> displayBrmsRulesMap(ProxyApiCreditReportEntity creditReport) {
        Map<String,BrmsRulesWrapper> brmsRulesWrapperMap = new Map<String,BrmsRulesWrapper>();
        if(creditReport <> null && creditReport.brms <> null && creditReport.brms.rulesList <> null) {
            for(ProxyApiCreditReportRuleEntity rule : creditReport.brms.rulesList) {
                system.debug('--rule--'+rule);
                BrmsRulesWrapper brmsRuleWrapper = new BrmsRulesWrapper();
                brmsRuleWrapper.ruleName = rule.deRuleName;
                brmsRuleWrapper.ruleValue = rule.deRuleValue;
                brmsRuleWrapper.ruleNumber = rule.deRuleNumber;
                brmsRuleWrapper.rulePass = rule.deRuleStatus == RULE_STATUS_PASS;
                brmsRulesWrapperMap.put(brmsRuleWrapper.ruleNumber,brmsRuleWrapper);
            }
        }
        return brmsRulesWrapperMap;
    }
}