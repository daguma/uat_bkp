global class NOIANotificationsJob implements Database.Batchable<sObject>, Schedulable, Database.Stateful {

    Global String query;
    Global List<Opportunity> appList = new List<Opportunity>();
    global Integer count = 0, withError = 0 ;

    public void execute(SchedulableContext sc) {
        NOIANotificationsJob b = new NOIANotificationsJob();
        database.executebatch(b, 25);
    }

    global NOIANotificationsJob() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        query = 'SELECT Id, Name, Customer_Name__c, Contact__c, CreatedDate, Encrypted_AppId_AES__c, LeadSource__c, Lead_Sub_Source__c, Contact__r.Do_Not_Contact__c ' +
                'FROM Opportunity ' +
                'WHERE Status__c NOT IN (\'Funded\', \'Credit Approved - No Contact Allowed\', \'Declined (or Unqualified)\') ' +
                'AND CreatedDate >= LAST_N_DAYS:30 AND CreatedDate < LAST_N_DAYS:22 ' +
                'AND Contact__r.Do_Not_Contact__c = false ' +
                'AND Contact__r.Email <> null ' +
            	'AND Contact__r.EmailBouncedDate = null  ' +
                'AND Contact__r.Email <> \'no@lendingpoint.com\' ' +
                'AND Type = \'New\' ' +
                'AND ((Contact__r.NOIA_Notification_Sent_Date__c <> LAST_N_DAYS:10 And CreatedDate < 2018-07-01T00:00:00Z) ' +
                ' OR (NOIA_Sent_Date__c = NULL And CreatedDate >= 2018-07-01T00:00:00Z)) ' +
            	'ORDER BY CreatedDate DESC';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {

        for (Opportunity CurrentApplication : scope) {
            try {
                Map<String, Object> params = new Map<String, Object>();
                
                params.put('CurrentApplication', CurrentApplication);
                Flow.Interview.NOIA_Notification NOIA_Notifications = new Flow.Interview.NOIA_Notification(params);
                NOIA_Notifications.start();
                count++;
            } catch (Exception e) {
                withError++;
            }
        }
    }

    global void finish(Database.BatchableContext BC) {
 		WebToSFDC.notifyDev('NOIA Batch Completed', 'NOIA batch completed for Today. Total processed : ' + count + '\nWith Error = ' + withError);        

    }

}