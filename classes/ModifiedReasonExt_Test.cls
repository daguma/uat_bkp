@isTest
public class ModifiedReasonExt_Test{
    
	@isTest static void updates_agreement_modified_date(){
		loan__Loan_Account__c Contr= TestHelper.createContract();
 
		Test.setCurrentPageReference(new PageReference('Page.myPage'));
		System.currentPageReference().getParameters().put('id', Contr.id);
		ApexPages.StandardController sc = new ApexPages.StandardController(Contr);
		ModifiedReasonExt testAccPlan = new ModifiedReasonExt(sc);
          
		testAccPlan.saveReason();
          
		loan__Loan_Account__c contractUpdated = [SELECT id, Agreement_Modified_Date__c 
                                                 FROM loan__Loan_Account__c 
                                                 WHERE id =: Contr.id LIMIT 1];
          
		System.assertNotEquals(Contr.Agreement_Modified_Date__c, contractUpdated.Agreement_Modified_Date__c);
	}
    
    @isTest static void updates_modified_reason(){
        loan__Loan_Account__c Contr= TestHelper.createContract();
        Contr.Modification_Reason__c = 'Service Members Service Relief Act';
        Contr.Modification_Reason_Text_Line__c = 'Test';
        update Contr;
 
		Test.setCurrentPageReference(new PageReference('Page.myPage'));
		System.currentPageReference().getParameters().put('id', Contr.id);
		ApexPages.StandardController sc = new ApexPages.StandardController(Contr);
		ModifiedReasonExt testAccPlan = new ModifiedReasonExt(sc);
          
		testAccPlan.saveReason();
        
        loan__Loan_Account__c contractUpdated = [SELECT id, Modification_Reason_Text_Line__c 
                                                 FROM loan__Loan_Account__c 
                                                 WHERE id =: Contr.id LIMIT 1];
          
		System.assertEquals(null, contractUpdated.Modification_Reason_Text_Line__c);
    }
    
    @isTest static void checks_error_message(){
        loan__Loan_Account__c Contr= TestHelper.createContract();
 
		Test.setCurrentPageReference(new PageReference('Page.myPage'));
		System.currentPageReference().getParameters().put('id', Contr.id);
		ApexPages.StandardController sc = new ApexPages.StandardController(Contr);
		ModifiedReasonExt testAccPlan = new ModifiedReasonExt(sc);
          
		testAccPlan.createMessage(ApexPages.Severity.Error, 'You have to add a description if you select \'Other\' as a modification reason.');
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        Integer index = (msgs.size() > 0) ? (msgs.size() - 1) : 0;
        System.assert((msgs.get(index)).getDetail() == 'You have to add a description if you select \'Other\' as a modification reason.');
		
    }

 }