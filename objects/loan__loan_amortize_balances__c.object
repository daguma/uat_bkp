<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Amortize balances of the loan account</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>loan__Amortize_Calculation_Method__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortization Method</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the method used to calculate the Interest Amortization</inlineHelpText>
        <label>Amortization Method</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>loan__Balance_Type__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Interest Bearing</fullName>
                    <default>false</default>
                    <label>Interest Bearing</label>
                </value>
                <value>
                    <fullName>Straight Line</fullName>
                    <default>false</default>
                    <label>Straight Line</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Pre Paid Fee</controllingFieldValue>
                <valueName>Straight Line</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Interest</controllingFieldValue>
                <valueName>Interest Bearing</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>loan__Amortize_Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Contract date of the loan account.</inlineHelpText>
        <label>Amortization Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__Archived__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Selection indicates that this record can be archived. This is automatically updated by the system. Do no update it manually.</inlineHelpText>
        <label>Archive</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Balance_Amount__c</fullName>
        <deprecated>false</deprecated>
        <description>Balance amount of amortize</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the estimated Interest on the loan account.</inlineHelpText>
        <label>Balance Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Balance_Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortize Balance type</description>
        <externalId>false</externalId>
        <inlineHelpText>Balance type which will be amortized.</inlineHelpText>
        <label>Balance Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pre Paid Fee</fullName>
                    <default>true</default>
                    <label>Pre Paid Fee</label>
                </value>
                <value>
                    <fullName>Interest</fullName>
                    <default>false</default>
                    <label>Interest</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>loan__Earned_Balance__c</fullName>
        <deprecated>false</deprecated>
        <description>Earned Balance</description>
        <externalId>false</externalId>
        <inlineHelpText>This is zero initially. It is updated when amortization transaction is created.</inlineHelpText>
        <label>Earned</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Enabled_Flag__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If enabled, this balance will be used to compute Interest Amortization</inlineHelpText>
        <label>Enabled</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__First_Amortize_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>First Amortize Date</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the first payment date on the loan account.</inlineHelpText>
        <label>First Amortize Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__Frequency__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortization Frequency</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the frequency of amortization process</inlineHelpText>
        <label>Amortization Frequency</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>loan__Balance_Type__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Monthly</fullName>
                    <default>false</default>
                    <label>Monthly</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Pre Paid Fee</controllingFieldValue>
                <controllingFieldValue>Interest</controllingFieldValue>
                <valueName>Monthly</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>loan__Last_Amortize_Calculation_Method__c</fullName>
        <deprecated>false</deprecated>
        <description>Last Amortization Method.</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the balance earned in the last Amortization process run</inlineHelpText>
        <label>Last Amortization Method</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Last_Earned_Balance__c</fullName>
        <deprecated>false</deprecated>
        <description>Last Earned balance</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the balance earned in the last Amortization process run</inlineHelpText>
        <label>Last Earned Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Loan_Account__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Loan</label>
        <referenceTo>loan__Loan_Account__c</referenceTo>
        <relationshipLabel>Loan Amortize Balances</relationshipLabel>
        <relationshipName>Loan_Amortize_Balances</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>loan__Master_Archive_Object__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Selection indicates that this is a master object record containing one or more child object records. Master object records may be archived but are not deleted by the system. This is automatically updated by the system. Do not update it manually.</inlineHelpText>
        <label>Master Archive Object</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Next_Amortize_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Next Amortization Date</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the next date when Interest Amortize will run</inlineHelpText>
        <label>Next Amortization Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__Rate__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortize Rate</description>
        <externalId>false</externalId>
        <inlineHelpText>Interest rate using which Interest will be calculated.</inlineHelpText>
        <label>Amortize Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Summary_Record_Id__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The ID of the summary transaction record within which this record is summarized. This is automatically generated by the system. Do no update it manually.</inlineHelpText>
        <label>Summary Record Id</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Summary__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Selection indicates that the record is a summary transaction record for data archival purpose. This is automatically updated by the system. Do no update it manually.</inlineHelpText>
        <label>Summary</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Term__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortized Term</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the term of the loan account</inlineHelpText>
        <label>Term</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Writtenoff_Balance__c</fullName>
        <deprecated>false</deprecated>
        <description>WriteOff balance</description>
        <externalId>false</externalId>
        <label>WriteOff Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__last_Term__c</fullName>
        <deprecated>false</deprecated>
        <description>Last Term</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the loan term at the time of last Amortization process run</inlineHelpText>
        <label>Last Term</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_Enabled_Flag__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If enabled, this balance will be used to compute Interest Amortization</inlineHelpText>
        <label>Enabled</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__lmb_amortize_calculation_method__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortization Method</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the method used to calculate the Interest Amortization</inlineHelpText>
        <label>Amortization Method</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_amortize_calculation_method_last__c</fullName>
        <deprecated>false</deprecated>
        <description>Last Amortization Method.</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the balance earned in the last Amortization process run</inlineHelpText>
        <label>Last Amortization Method</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_amortize_frequency__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortization Frequency</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the frequency of amortization process</inlineHelpText>
        <label>Amortization Frequency</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_amortize_start_date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Contract date of the loan account.</inlineHelpText>
        <label>Amortization Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__lmb_balance_amount__c</fullName>
        <deprecated>false</deprecated>
        <description>Balance amount of amortize</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the estimated Interest on the loan account.</inlineHelpText>
        <label>Balance Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_balance_earned__c</fullName>
        <deprecated>false</deprecated>
        <description>Earned Balance</description>
        <externalId>false</externalId>
        <inlineHelpText>This is zero initially. It is updated when amortization transaction is created.</inlineHelpText>
        <label>Earned</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_balance_earned_last__c</fullName>
        <deprecated>false</deprecated>
        <description>Last Earned balance</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the balance earned in the last Amortization process run</inlineHelpText>
        <label>Last Earned Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_balance_type__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortize Balance type</description>
        <externalId>false</externalId>
        <inlineHelpText>Balance type which will be amortized.</inlineHelpText>
        <label>Balance Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>loan__lmb_balance_writeoff__c</fullName>
        <deprecated>false</deprecated>
        <description>WriteOff balance</description>
        <externalId>false</externalId>
        <label>WriteOff Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_first_amortize_date__c</fullName>
        <deprecated>false</deprecated>
        <description>First Amortize Date</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the first payment date on the loan account.</inlineHelpText>
        <label>First Amortize Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__lmb_next_amortize_date__c</fullName>
        <deprecated>false</deprecated>
        <description>Next Amortization Date</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the next date when Interest Amortize will run</inlineHelpText>
        <label>Next Amortization Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__lmb_rate__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortize Rate</description>
        <externalId>false</externalId>
        <inlineHelpText>Interest rate using which Interest will be calculated.</inlineHelpText>
        <label>Amortize Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_term__c</fullName>
        <deprecated>false</deprecated>
        <description>Amortized Term</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the term of the loan account</inlineHelpText>
        <label>Term</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__lmb_term_last__c</fullName>
        <deprecated>false</deprecated>
        <description>Last Term</description>
        <externalId>false</externalId>
        <inlineHelpText>It is the loan term at the time of last Amortization process run</inlineHelpText>
        <label>Last Term</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Loan Amortize Balances</label>
    <nameField>
        <displayFormat>A-{0000000000}</displayFormat>
        <label>Amortization Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Loan Amortize Balances</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
