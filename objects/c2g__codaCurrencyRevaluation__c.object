<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>c2g__codahelpcurrencyrevaluation</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Stores the master details related to a currency revaluation.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>c2g__Analysis__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Analysis</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Detail Level</fullName>
                    <default>false</default>
                    <label>Detail Level</label>
                </value>
                <value>
                    <fullName>Summary Level</fullName>
                    <default>false</default>
                    <label>Summary Level</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>c2g__BreakdownByStandardAnalysisFields__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked your transaction line items for the gains and losses GLA will be broken down by standard analysis fields (Account, Product, Bank Account, Tax Code).</description>
        <externalId>false</externalId>
        <label>Standard Breakdown</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>c2g__BreakdownBySubAnalysisFields__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked your revaluation summary and transaction line items for the gains and losses GLA will be broken down by custom sub-analysis fields.</description>
        <externalId>false</externalId>
        <label>Sub-Analysis Breakdown</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>c2g__CancelPeriod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cancel Period</label>
        <referenceTo>c2g__codaPeriod__c</referenceTo>
        <relationshipLabel>Currency Revaluations (Cancel Period)</relationshipLabel>
        <relationshipName>CurrencyRevaluations4</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__CancellationReason__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cancellation Reason</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>c2g__Dimension1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dimension 1</label>
        <referenceTo>c2g__codaDimension1__c</referenceTo>
        <relationshipLabel>Currency Revaluations</relationshipLabel>
        <relationshipName>CurrencyRevaluations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__Dimension2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dimension 2</label>
        <referenceTo>c2g__codaDimension2__c</referenceTo>
        <relationshipLabel>Currency Revaluations</relationshipLabel>
        <relationshipName>CurrencyRevaluations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__Dimension3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dimension 3</label>
        <referenceTo>c2g__codaDimension3__c</referenceTo>
        <relationshipLabel>Currency Revaluations</relationshipLabel>
        <relationshipName>CurrencyRevaluations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__Dimension4__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dimension 4</label>
        <referenceTo>c2g__codaDimension4__c</referenceTo>
        <relationshipLabel>Currency Revaluations</relationshipLabel>
        <relationshipName>CurrencyRevaluations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__DimensionalAnalysis__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dimensional Analysis On Lines</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Copy from Source</fullName>
                    <default>false</default>
                    <label>Copy from Source</label>
                </value>
                <value>
                    <fullName>Enter Dimensions</fullName>
                    <default>false</default>
                    <label>Enter Dimensions</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>c2g__DiscardReason__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Discard Reason</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>c2g__DocumentCurrency__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Document Currency</label>
        <referenceTo>c2g__codaAccountingCurrency__c</referenceTo>
        <relationshipLabel>Currency Revaluations</relationshipLabel>
        <relationshipName>CurrencyRevaluations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__DocumentDescription__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Document Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>c2g__DocumentReference__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Document Reference</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__DualExchangeRate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dual Exchange Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>9</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__DualValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dual Value</label>
        <summarizedField>c2g__codaCurrencyRevaluationSummary__c.c2g__DualValue__c</summarizedField>
        <summaryForeignKey>c2g__codaCurrencyRevaluationSummary__c.c2g__CurrencyRevaluation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>c2g__ErrorListView__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Error</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>c2g__ErrorText__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Error Text</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>c2g__ExcludeCRVTransactions__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Exclude CRV Transactions</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>c2g__ExcludeRevaluedLines__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Should previously revalued lines be excluded from selection by this revaluation or should they cause the revaluation to fail?</description>
        <externalId>false</externalId>
        <label>Exclude Revalued Lines</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>c2g__GroupDisplayName__c</fullName>
        <deprecated>false</deprecated>
        <description>The currency revaluation group display name.</description>
        <externalId>false</externalId>
        <formula>c2g__RevalGroup__r.c2g__DisplayName__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Group Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__HomeExchangeRate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Document Exchange Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>9</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__HomeValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Home Value</label>
        <summarizedField>c2g__codaCurrencyRevaluationSummary__c.c2g__HomeValue__c</summarizedField>
        <summaryForeignKey>c2g__codaCurrencyRevaluationSummary__c.c2g__CurrencyRevaluation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>c2g__Locks__c</fullName>
        <deprecated>false</deprecated>
        <description>The locking object ID used to protect updates to the owning currency revaluation field on Transaction lines.</description>
        <externalId>false</externalId>
        <label>Locks</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__OwnerCompany__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Company</label>
        <referenceTo>c2g__codaCompany__c</referenceTo>
        <relationshipLabel>Currency Revaluations</relationshipLabel>
        <relationshipName>CurrencyRevaluations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__PeriodFrom__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Period From</label>
        <referenceTo>c2g__codaPeriod__c</referenceTo>
        <relationshipLabel>Currency Revaluations</relationshipLabel>
        <relationshipName>CurrencyRevaluations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__PeriodTo__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Period To</label>
        <referenceTo>c2g__codaPeriod__c</referenceTo>
        <relationshipLabel>Currency Revaluations (Period To)</relationshipLabel>
        <relationshipName>CurrencyRevaluations1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__PostingPeriod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Posting Period</label>
        <referenceTo>c2g__codaPeriod__c</referenceTo>
        <relationshipLabel>Currency Revaluations (Posting Period)</relationshipLabel>
        <relationshipName>CurrencyRevaluations2</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__RevalGroup__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Our currency revaluation group.</description>
        <externalId>false</externalId>
        <label>Group</label>
        <referenceTo>c2g__codaCurrencyRevaluationGroup__c</referenceTo>
        <relationshipLabel>Group Members</relationshipLabel>
        <relationshipName>RevalGroupMembers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__RevaluationType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Revaluation Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Balance Sheet</fullName>
                    <default>false</default>
                    <label>Balance Sheet</label>
                </value>
                <value>
                    <fullName>Income Statement</fullName>
                    <default>false</default>
                    <label>Income Statement</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>c2g__ReversalPeriod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Reversal Period</label>
        <referenceTo>c2g__codaPeriod__c</referenceTo>
        <relationshipLabel>Currency Revaluations (Reversal Period)</relationshipLabel>
        <relationshipName>CurrencyRevaluations3</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>c2g__ScheduledTime__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>c2g__RevalGroup__r.c2g__ScheduledTime__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Scheduled Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>c2g__Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>true</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Generating</fullName>
                    <default>false</default>
                    <label>Generating</label>
                </value>
                <value>
                    <fullName>Generation Error</fullName>
                    <default>false</default>
                    <label>Generation Error</label>
                </value>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>Posting</fullName>
                    <default>false</default>
                    <label>Posting</label>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
                <value>
                    <fullName>Posting Error</fullName>
                    <default>false</default>
                    <label>Posting Error</label>
                </value>
                <value>
                    <fullName>Discarded</fullName>
                    <default>false</default>
                    <label>Discarded</label>
                </value>
                <value>
                    <fullName>Discarding</fullName>
                    <default>false</default>
                    <label>Discarding</label>
                </value>
                <value>
                    <fullName>Canceling</fullName>
                    <default>false</default>
                    <label>Canceling</label>
                </value>
                <value>
                    <fullName>Canceled</fullName>
                    <default>false</default>
                    <label>Canceled</label>
                </value>
                <value>
                    <fullName>Canceling Error</fullName>
                    <default>false</default>
                    <label>Canceling Error</label>
                </value>
                <value>
                    <fullName>Void</fullName>
                    <default>false</default>
                    <label>Void</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>c2g__UnitOfWork__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit of Work</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>c2g__UnrealizedGainsLossesGLA__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unrealized Gains/Losses GLA</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1 OR 2</booleanFilter>
            <errorMessage>The selected GLA does not exist or is not valid. It must be a corporate GLA.</errorMessage>
            <filterItems>
                <field>c2g__codaGeneralLedgerAccount__c.c2g__ChartOfAccountsStructure__r.Name</field>
                <operation>equals</operation>
                <value></value>
            </filterItems>
            <filterItems>
                <field>c2g__codaGeneralLedgerAccount__c.c2g__ChartOfAccountsStructure__r.c2g__IsCorporate__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>c2g__codaGeneralLedgerAccount__c</referenceTo>
        <relationshipLabel>Currency Revaluations</relationshipLabel>
        <relationshipName>CurrencyRevaluations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Currency Revaluation</label>
    <listViews>
        <fullName>FF_Amberport_Unlimited_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Amberport Unlimited</label>
        <queue>FF_Amberport_Unlimited</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_FF_Lendingpoint_2018_1_SPE_Trust_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint 2018-1 SPE Trust</label>
        <queue>FF_Lendingpoint_2018_1_SPE_Trust</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Finwise_LendingPoint_LLC_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Finwise- LendingPoint LLC</label>
        <queue>FF_Finwise_LendingPoint_LLC</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Lendingpoint_2018_SPE1_LLC_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint 2018 SPE1 LLC</label>
        <queue>FF_Lendingpoint_2018_SPE1_LLC</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Lendingpoint_Funding_I_2017_1_LLC_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint Funding I 2017-1 LLC</label>
        <queue>FF_Lendingpoint_Funding_I_2017_1_LLC</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Lendingpoint_Holdings_LLC_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint Holdings LLC</label>
        <queue>FF_Lendingpoint_Holdings_LLC</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Lendingpoint_LLC_Consolidated_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint LLC-Consolidated</label>
        <queue>FF_Lendingpoint_LLC_Consolidated</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Lendingpoint_LLC_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint LLC</label>
        <queue>FF_Lendingpoint_LLC</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Lendingpoint_SPE2_LLC_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint SPE2 LLC</label>
        <queue>FF_Lendingpoint_SPE2_LLC</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Lendingpoint_SPE3_LLC_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint SPE3 LLC</label>
        <queue>FF_Lendingpoint_SPE3_LLC</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Lendingpoint_SPE_LLC_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Lendingpoint SPE LLC</label>
        <queue>FF_Lendingpoint_SPE_LLC</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>FF_Loan_Hero_codaCurrencyRevaluation</fullName>
        <filterScope>Queue</filterScope>
        <label>FF Loan Hero</label>
        <queue>FF_Loan_Hero</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>c2g__All</fullName>
        <columns>NAME</columns>
        <columns>c2g__RevaluationType__c</columns>
        <columns>c2g__DocumentCurrency__c</columns>
        <columns>c2g__HomeExchangeRate__c</columns>
        <columns>c2g__DualExchangeRate__c</columns>
        <columns>c2g__PostingPeriod__c</columns>
        <columns>c2g__ReversalPeriod__c</columns>
        <columns>c2g__Status__c</columns>
        <columns>c2g__RevalGroup__c</columns>
        <columns>c2g__GroupDisplayName__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>CRV{000000}</displayFormat>
        <label>Currency Revaluation Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Currency Revaluations</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>c2g__RevaluationType__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__DocumentCurrency__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__HomeExchangeRate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__PostingPeriod__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__ReversalPeriod__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__RevalGroup__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__GroupDisplayName__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>c2g__ScheduledTime__c</customTabListAdditionalFields>
        <listViewButtons>c2g__Restart</listViewButtons>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>c2g__Cancel</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Cancel Revaluation</masterLabel>
        <openType>sidebar</openType>
        <page>c2g__codarevalcurrencycancel</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>c2g__ClearHistory</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Clear History</masterLabel>
        <openType>sidebar</openType>
        <page>c2g__currencyrevalcancelhistory</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>c2g__Discard</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Discard</masterLabel>
        <openType>sidebar</openType>
        <page>c2g__codacurrencyrevaldiscard</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>c2g__ErrorsListView</fullName>
        <availability>online</availability>
        <displayType>link</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Errors List View</masterLabel>
        <openType>sidebar</openType>
        <page>c2g__codarevalcurbatchlistview</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>c2g__Post</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Post</masterLabel>
        <openType>sidebar</openType>
        <page>c2g__currencyrevalpost</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>c2g__Restart</fullName>
        <availability>online</availability>
        <description>Restart an unfinished currency revaluation group.</description>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Restart Currency Revaluation Group</masterLabel>
        <openType>sidebar</openType>
        <page>c2g__codacurrencyrevalrestart</page>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
    </webLinks>
    <webLinks>
        <fullName>c2g__Unlock</fullName>
        <availability>online</availability>
        <description>Unlock any transaction lines held by this revaluation.</description>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Unlock</masterLabel>
        <openType>sidebar</openType>
        <page>c2g__codarevalcurrencyunlock</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
