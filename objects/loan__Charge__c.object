<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Charge is something that is applied.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>FFA_Integration_Complete__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>FFA Integration Complete</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Accrue_Interest_From__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This is the date from which the interest on the charge will be calculated. If this is not mentioned interest will be calculated from the date the charge was incurred.</inlineHelpText>
        <label>Accrue Interest From</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__Archived__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Selection indicates that this record can be archived. This is automatically updated by the system. Do no update it manually.</inlineHelpText>
        <label>Archive</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Balance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This field contains the principal balance on the loan after the charge has been applied.</inlineHelpText>
        <label>Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Calculated_Interest2__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>(loan__Original_Amount__c - loan__Paid_Amount__c) * ( loan__Interest_Rate__c ) *  ($User.loan__Current_Branch_s_System_Date__c -  loan__Accrue_Interest_From__c ) / (365)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Calculated Interest (Formula)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Calculated_Interest3__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>View only field which shows the calculated interest from last accrual date from SOD process.</description>
        <externalId>false</externalId>
        <label>Calculated Interest</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Capitalization_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Charge&apos;s Capitalization Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Charge&apos;s Capitalization Date</inlineHelpText>
        <label>Capitalization Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__Charge_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Charge Type_deprecated</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Late Fees</fullName>
                    <default>false</default>
                    <label>Late Fees</label>
                </value>
                <value>
                    <fullName>NSF Fees</fullName>
                    <default>false</default>
                    <label>NSF Fees</label>
                </value>
                <value>
                    <fullName>Wire Fees</fullName>
                    <default>false</default>
                    <label>Wire Fees</label>
                </value>
                <value>
                    <fullName>Pay-Off Fees</fullName>
                    <default>false</default>
                    <label>Pay-Off Fees</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>true</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>loan__Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This is the date the charge was incurred.</inlineHelpText>
        <label>Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__Dues_Details__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Dues Details</label>
        <referenceTo>loan__Loan_account_Due_Details__c</referenceTo>
        <relationshipLabel>Charges</relationshipLabel>
        <relationshipName>Charges</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>loan__Fee__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The associated Fee. The Fee object is used in case of automatic application of a charge. For example, the late fees is charged automatically on a loan.</inlineHelpText>
        <label>Fee</label>
        <referenceTo>loan__Fee__c</referenceTo>
        <relationshipLabel>Charges</relationshipLabel>
        <relationshipName>Charges</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>loan__Include_In_Schedule_Id__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field will reference to Amortization Schedule including fees.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field will reference to Amortization Schedule including fees.</inlineHelpText>
        <label>Include In Schedule Id</label>
        <referenceTo>loan__Repayment_Schedule__c</referenceTo>
        <relationshipLabel>Charges</relationshipLabel>
        <relationshipName>Charges</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>loan__Included_In_Bill_Id__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>When Is Included In Due is true, this refers to the bill in which this charge is being included.</description>
        <externalId>false</externalId>
        <inlineHelpText>When Is Included In Due is true, this refers to the bill in which this charge is being included.</inlineHelpText>
        <label>Included In Bill #</label>
        <referenceTo>loan__Loan_account_Due_Details__c</referenceTo>
        <relationshipLabel>Charges Included In Bill</relationshipLabel>
        <relationshipName>Charges_Included_In_Bill</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>loan__Interest_Bearing__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Check this option if the charge is interest bearing.</inlineHelpText>
        <label>Interest Bearing</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Interest_Due__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>Interest due after a payment(txn amount insufficient to clear complete interest) on interest bearing charges</description>
        <externalId>false</externalId>
        <label>Interest Due</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Interest_Rate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This is the interest rate to be applied on the charge (if applicable)</inlineHelpText>
        <label>Interest Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>loan__Internal_Accounting_Generated__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This field indicates if accounting has been passed for the accrual entry record.</inlineHelpText>
        <label>Internal Accounting Generated</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Internal_Accounting_Reversal_Generated__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Internal Accounting Reversal Generated</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Is_Charge_Capitalized__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Charge Capitalized</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Is_Charge_Capitalized_for_IO__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If this is checked means that the charge is capitalized for the Investment Orders in the Loan.</description>
        <externalId>false</externalId>
        <inlineHelpText>If this is checked means that the charge is capitalized for the Investment Orders in the Loan.</inlineHelpText>
        <label>Charge Capitalized for Investor</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Is_Included_In_Due__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Field should not be updated manually. System updates this field when charge is added to bill.</inlineHelpText>
        <label>Is Included In Due</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Last_Accrual_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Last accrual Date on charge (Usually the last payment transaction date)</description>
        <externalId>false</externalId>
        <inlineHelpText>The date when the last interest accrual was processed</inlineHelpText>
        <label>Last Accrual Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>loan__Loan_Account__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The associated loan.</inlineHelpText>
        <label>Loan Account</label>
        <referenceTo>loan__Loan_Account__c</referenceTo>
        <relationshipLabel>Charges</relationshipLabel>
        <relationshipName>Charges</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>loan__Master_Archive_Object__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Selection indicates that this is a master object record containing one or more child object records. Master object records may be archived but are not deleted by the system. This is automatically updated by the system. Do not update it manually.</inlineHelpText>
        <label>Master Archive Object</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Original_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The original amount of the charge.</inlineHelpText>
        <label>Original Amount</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Original_Tax__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Original Tax</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>loan__Paid_Amount__c</fullName>
        <defaultValue>0.0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The amount that has been paid.</inlineHelpText>
        <label>Paid Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Paid__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Paid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Payoff_Balance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This field contains the balance needed to payoff the loan after this charge has been applied.</inlineHelpText>
        <label>Payoff Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Principal_Due__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>Principal amount of charge that is yet to be paid.</description>
        <externalId>false</externalId>
        <label>Principal Due</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Product_Code__c</fullName>
        <deprecated>false</deprecated>
        <description>Field stores unique code of Lending Product</description>
        <externalId>false</externalId>
        <formula>loan__Loan_Account__r.loan__Loan_Product_Name__r.loan__Loan_Product_Code__c</formula>
        <inlineHelpText>Field stores unique code of Lending Product</inlineHelpText>
        <label>Product Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Reason_Waived__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Reason Waived</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Customer Satisfaction</fullName>
                    <default>false</default>
                    <label>Customer Satisfaction</label>
                </value>
                <value>
                    <fullName>Non-Recoverable</fullName>
                    <default>false</default>
                    <label>Non-Recoverable</label>
                </value>
                <value>
                    <fullName>Bankruptcy - Active</fullName>
                    <default>false</default>
                    <label>Bankruptcy - Active</label>
                </value>
                <value>
                    <fullName>Bankruptcy - Discharged</fullName>
                    <default>false</default>
                    <label>Bankruptcy - Discharged</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>loan__Reference__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>This text can be used to provide additional information about the charge.</inlineHelpText>
        <label>Reference</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>loan__Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Processed</fullName>
                    <default>true</default>
                    <label>Processed</label>
                </value>
                <value>
                    <fullName>Tax Calculated</fullName>
                    <default>false</default>
                    <label>Tax Calculated</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>loan__Summary_Record_Id__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The ID of the summary transaction record within which this record is summarized. This is automatically generated by the system. Do no update it manually.</inlineHelpText>
        <label>Summary Record Id</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Summary__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Selection indicates that the record is a summary transaction record for data archival purpose. This is automatically updated by the system. Do no update it manually.</inlineHelpText>
        <label>Summary</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Tax_Paid__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tax Paid</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>loan__Tax_Remaining__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tax Remaining</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>loan__Total_Amount_Due__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>Replacement for formula field</description>
        <externalId>false</externalId>
        <label>Total Amount Due</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>loan__Total_Due_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>loan__Principal_Due__c + loan__Interest_Due__c + loan__Calculated_Interest3__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Due Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Total_Waived_Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>loan__Waived_Principal__c +  loan__Waived_Interest__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Waived Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Waive__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Check this field if this charge must be waived off.</inlineHelpText>
        <label>Waived</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>loan__Waived_Interest__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Waived Interest</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>loan__Waived_Principal__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Waived Principal</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>peer__VAT_percent__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>VAT %age</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <label>Charge</label>
    <nameField>
        <displayFormat>CHG-{00000000}</displayFormat>
        <label>Charge Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Charges</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>loan__Accrue_Interest_From_Date_Check</fullName>
        <active>true</active>
        <errorConditionFormula>loan__Interest_Bearing__c = true &amp;&amp;
 ISBLANK(loan__Accrue_Interest_From__c)</errorConditionFormula>
        <errorMessage>For an interest bearing charge, please enter the accrue interest from date.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>loan__Check_Loan_Status</fullName>
        <active>false</active>
        <errorConditionFormula>NOT(ISPICKVAL( loan__Loan_Account__r.loan__Loan_Status__c , &apos;Active - Good Standing&apos;) ) &amp;&amp;
NOT(ISPICKVAL( loan__Loan_Account__r.loan__Loan_Status__c , &apos;Active - Bad Standing&apos;) ) &amp;&amp;
NOT(ISPICKVAL( loan__Loan_Account__r.loan__Loan_Status__c , &apos;Active - Matured&apos;) )</errorConditionFormula>
        <errorMessage>Charges can only be applied to active loans.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>loan__Edit_Paid_Charge_Check</fullName>
        <active>true</active>
        <description>Charges that have a payment against them cannot be waived</description>
        <errorConditionFormula>NOT(ISNEW()) &amp;&amp; loan__Paid__c  = true  &amp;&amp;
NOT(ISCHANGED(loan__Paid__c)) &amp;&amp;
(ISCHANGED(loan__Original_Amount__c) || ISCHANGED(loan__Paid_Amount__c) || ISCHANGED(loan__Reference__c) || ISCHANGED(loan__Interest_Bearing__c) || ISCHANGED(loan__Principal_Due__c ) || ISCHANGED(loan__Interest_Due__c ) || ISCHANGED(loan__Fee__c) || ISCHANGED(loan__Interest_Rate__c) || ISCHANGED( loan__Accrue_Interest_From__c ))</errorConditionFormula>
        <errorMessage>You cannot edit a paid charge.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>loan__Waive_Paid_Charges</fullName>
        <active>false</active>
        <description>Charges that have a payment against them cannot be waived</description>
        <errorConditionFormula>loan__Waive__c  = true  &amp;&amp; loan__Paid_Amount__c  &lt;&gt; 0</errorConditionFormula>
        <errorMessage>Charge cannot be waived as there is a payment made against it already</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>loan__Create_New_Charge</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Create New Charge</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>showURLInModalWindow(&apos;/apex/chargeNew?loanId=&quot;{!loan__Charge__c.loan__Loan_AccountId__c}&quot;&amp;modal=true&apos;)</url>
    </webLinks>
    <webLinks>
        <fullName>loan__Waive_Unwaive_Charge</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Waive/Unwaive Charge</masterLabel>
        <openType>sidebar</openType>
        <page>loan__waiveUnwaiveCharge</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
