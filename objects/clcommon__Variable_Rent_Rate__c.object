<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>1) Is unique for different dimensions for a single equipment.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>clcommon__Current_Variable_Rent_Rate_Reading__c</fullName>
        <deprecated>false</deprecated>
        <description>The latest  Variable Rent Rate reading</description>
        <externalId>false</externalId>
        <inlineHelpText>The latest Variable Rent Rate reading</inlineHelpText>
        <label>Current Variable Rent Rate Reading</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>clcommon__Equipment__c</fullName>
        <deprecated>false</deprecated>
        <description>The equipment to which this Variable Rent Rate is attached to</description>
        <externalId>false</externalId>
        <inlineHelpText>The equipment to which this Variable Rent Rate should be attached</inlineHelpText>
        <label>Equipment</label>
        <referenceTo>clcommon__Collateral__c</referenceTo>
        <relationshipLabel>Variable Rent Rates</relationshipLabel>
        <relationshipName>Variable_Rent_Rate</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>clcommon__Next_Usage_Generation_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Next usage generation date for this Variable Rent Rate</description>
        <externalId>false</externalId>
        <inlineHelpText>Next usage generation date for this Variable Rent Rate</inlineHelpText>
        <label>Next Usage Generation Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>clcommon__Status__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to signify whether charge has to be created against this Variable Rent Rate .</description>
        <externalId>false</externalId>
        <inlineHelpText>Active - Charge has to be created against this Variable Rent Rate .
Inactive - Charge will not be created against this Variable Rent Rate.</inlineHelpText>
        <label>Status</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>true</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>clcommon__Usage_Allowance__c</fullName>
        <deprecated>false</deprecated>
        <description>The unit upto which the usage is free</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the unit upto which the usage will be free</inlineHelpText>
        <label>Usage Allowance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>clcommon__Variable_Rent_Basis__c</fullName>
        <deprecated>false</deprecated>
        <description>This signifies on what basis the readings will be put.</description>
        <externalId>false</externalId>
        <inlineHelpText>Count – Every time the actual count of units used between the last charged period and current is entered.
Total – Every time the total meter reading is entered.</inlineHelpText>
        <label>Variable Rent Basis</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Count</fullName>
                    <default>false</default>
                    <label>Count</label>
                </value>
                <value>
                    <fullName>Total</fullName>
                    <default>true</default>
                    <label>Total</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>clcommon__Variable_Rent_Rate_Card__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This signifies which rate card is attached</description>
        <externalId>false</externalId>
        <inlineHelpText>This signifies which rate card is attached</inlineHelpText>
        <label>Variable Rent Rate Card</label>
        <referenceTo>clcommon__Variable_Rent_Rate_Card__c</referenceTo>
        <relationshipLabel>Variable Rent Rates</relationshipLabel>
        <relationshipName>Variable_Rent_Rate</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Variable Rent Rate</label>
    <listViews>
        <fullName>clcommon__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Variable Rent Rate</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Variable Rent Rates</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
