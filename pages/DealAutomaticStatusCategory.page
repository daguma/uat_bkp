<apex:page tabStyle="Lead_Automatic_Assignment__tab" showHeader="true" title="Status and Category" controller="DealAutomaticStatusCategoryCtrl" docType="html-5.0" sidebar="false" standardstylesheets="false">
    <!-- Css -->
    <apex:stylesheet value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <apex:stylesheet value="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" />
    <apex:stylesheet value="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css" />
    <apex:stylesheet value="{!$Resource.DealAutomaticAssigment}" />

    <!-- Javascript -->
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js" />
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.17/angular.min.js" />
    <apex:includeScript value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" />
    <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js" />
    <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js" />
    <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/json2/20150503/json2.min.js" />

    <script>
    /**
     * jquery no conflict initialization
     * @type {jQuery}
     */
    var $j = jQuery.noConflict();

    /**
     * jquery document ready annonymous function
     */
    $j(function() {
        $j('#ddlAssignedUsers').select2({
            closeOnSelect: false
        });
    });

    /**
     * angular initialization module
     * @type {string} module name
     */
    var categoryApp = angular.module('categoryApp', []);

    /**
     * angular controller = CategoryCtrl
     * @param  {Object} $scope  [Angular Scope object]
     */
    categoryApp.controller('CategoryCtrl', function($scope) {

        //--> locals var section
        var self = this;
        self.currentCategory;
        self.selectedUsers = [];
        self.oldSelectedUsers = [];

        /**
         * all the categories and statuses from the Deal Automatic Attributes
         * @type {Object}
         */
        $scope.categories = {!categories
        };

        /**
         * SelectedTab indicator
         * @type {Boolean}
         */
        $scope.isSelectedTab = false;

        /**
         * selected tab action click
         * @param  {Object} tab category object
         */
        $scope.onClickTab = function(tab) {
            $j('.popupBg, .processing').fadeIn();
            self.currentCategory = tab;

            delete self.currentCategory['attributes'];
            delete self.currentCategory['$$hashKey'];

            $scope.isSelectedTab = true;

            DealAutomaticStatusCategoryCtrl
                .getUsersGroupsByCategory(self.currentCategory, function(result, event) {
                    $j('#ddlAssignedUsers').select2("val", "");

                    self.selectedUsers = result;
                    self.oldSelectedUsers = result;

                    $scope.groups = _.groupBy(result, "groupName");

                    $scope.$apply();

                    $j('#ddlAssignedUsers').select2({
                        closeOnSelect: false
                    });
                    $j('.popupBg, .processing').fadeOut();
                });
        };

        /**
         * check if tab is active
         * @param  {Object} category object
         * @return {Boolean}  selected indicator
         */
        $scope.isActiveTab = function(tab) {
            var isActive = false;

            if (self.currentCategory != null) {
                isActive = tab.Display_Name__c == self.currentCategory.Display_Name__c;
            }

            return isActive;
        };

        /**
         * updates selected users object
         * @param {users} users list bind to ng-model
         */
        $scope.setUsers = function(users) {
            self.selectedUsers = users;
        };

        /**
         * save the selected users
         * @return {string} confirmation message
         */
        $scope.saveUsers = function() {
            $j('.popupBg, .processing').fadeIn();
            var newSelectedUsers = [];
            var oldSelectedUsers = [];

            for (var i = 0; i < self.selectedUsers.length; i++) {
                if (typeof self.selectedUsers[i] === 'string')
                    newSelectedUsers.push(JSON.parse(self.selectedUsers[i]));
                else if (typeof self.selectedUsers[i] === 'object' && self.selectedUsers[i].selected == true) {
                    delete self.selectedUsers[i]['attributes'];
                    delete self.selectedUsers[i]['$$hashKey'];
                    newSelectedUsers.push(self.selectedUsers[i]);
                }
            }

            for (var i = 0; i < self.oldSelectedUsers.length; i++) {
                if (typeof self.oldSelectedUsers[i] === 'string')
                    oldSelectedUsers.push(JSON.parse(self.oldSelectedUsers[i]));
                else if (typeof self.oldSelectedUsers[i] === 'object' && self.oldSelectedUsers[i].selected == true) {
                    delete self.oldSelectedUsers[i]['attributes'];
                    delete self.oldSelectedUsers[i]['$$hashKey'];
                    oldSelectedUsers.push(self.oldSelectedUsers[i]);
                }
            }

            DealAutomaticStatusCategoryCtrl.saveUsers(self.currentCategory,
                newSelectedUsers, oldSelectedUsers,
                function(result, event) {

                    self.oldSelectedUsers = self.selectedUsers;
                    $j('.popupBg, .processing').fadeOut();
                });
        };
    });
    </script>

    <section class="container-fluid" ng-app="categoryApp">

        <apex:tabPanel switchType="client" selectedTab="StatusCategory" id="DealAssigmentTabPanel" tabClass="activeTab" inactiveTabClass="inactiveTab">

            <apex:tab label="Time Frame & Location" name="TimeFrameLocation" id="tabTimeFrameLocation" ontabenter="window.parent.location.replace('/apex/DealAutomaticTimeFrameLocation')">
            </apex:tab>

            <apex:tab label="Sales Queue" name="SalesQueue" id="tabSalesQueue" ontabenter="window.parent.location.replace('/apex/DealAutomaticSalesQueue')">
            </apex:tab>

            <apex:tab label="Strategy Type" name="StrategyType" id="tabStrategyType" ontabenter="window.parent.location.replace('/apex/DealAutomaticStrategyType')">
            </apex:tab>

            <apex:tab label="Status" name="StatusCategory" id="tabStatusCategory" ontabenter="window.parent.location.replace('/apex/DealAutomaticStatusCategory')">

                <div class="panel panel-default" ng-controller="CategoryCtrl">
                    <div class="panel-heading">
                        <h3 class="panel-title">Statuses And Categories</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4" role="navigation">
                                <div class="list-group">
                                    <a ng-repeat="category in categories" ng-class="{active:isActiveTab(category)}" ng-click="onClickTab(category)" class="list-group-item">{{category.Display_Name__c}}</a>
                                </div>
                            </div>

                            <div class="col-md-8" ng-show="isSelectedTab">
                                <div class="form-group">
                                    <label class="title">Assigned Users</label>
                                    <select class="form-control" multiple="multiple" id="ddlAssignedUsers" ng-model="selectedUsers" ng-change="setUsers(selectedUsers)">
                                        <optgroup ng-repeat="(group, users) in groups | orderBy : group : reverse" label="{{group}}">
                                            <option ng-repeat="user in users" ng-selected="user.selected" value="{{ { 'groupId': user.groupId, 'groupName': user.groupName, 'userId': user.userId, 'userName': user.userName, 'selected': true} }}">{{user.userName}}</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <button type="button" class="btn btn-success" ng-click="saveUsers()">Save</button>
                            </div>
                        </div>
                    </div>
                </div>

            </apex:tab>

            <apex:tab label="Requested Amount" name="RequestedAmount" id="tabRequestedAmount" ontabenter="window.parent.location.replace('/apex/DealAutomaticRequestedAmount')">
            </apex:tab>

            <apex:tab label="FICO Ranges" name="FicoRange" id="tabFicoRange" ontabenter="window.parent.location.replace('/apex/DealAutomaticFicoRange')">
            </apex:tab>

            <apex:tab label="Grade" name="Grade" id="tabGrade" ontabenter="window.parent.location.replace('/apex/DealAutomaticGrade')">
            </apex:tab>

            <apex:tab label="Deal Source" name="DealSource" id="tabDealSource" ontabenter="window.parent.location.replace('/apex/DealAutomaticSource')">
            </apex:tab>

            <apex:tab label="Employment Type" name="EmploymentType" id="tabEmploymentTYpe" ontabenter="window.parent.location.replace('/apex/DealAutomaticEmploymentType')">
            </apex:tab>

            <apex:tab label="Use Of Funds" name="UseOfFund" id="tabUseOfFund" ontabenter="window.parent.location.replace('/apex/DealAutomaticUseOfFunds')">
            </apex:tab>

            <apex:tab label="Summary" name="SummaryType" id="tabSummary" ontabenter="window.parent.location.replace('/apex/DealAutomaticSummary')">
            </apex:tab>

            <apex:tab label="Exceptions" name="Exceptions" id="tabExceptions" ontabenter="window.parent.location.replace('/apex/DealAutomaticExceptions')">
            </apex:tab>

            <apex:tab label="DE Summary" name="CaseSummary" id="tabCaseSummary" ontabenter="window.parent.location.replace('/apex/CaseAutomaticSummary')">
            </apex:tab>

        </apex:tabPanel>

    </section>
    <div class="popupBg">
    </div>
    <div class="processing">
        <apex:image url="{!$Resource.loading}" />
        <p>Processing...</p>
    </div>

</apex:page>