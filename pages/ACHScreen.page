<apex:page standardController="genesis__Applications__c" extensions="ACHScreenCtrl,OpsWorkflowUtil" title="Automated Decisioning" action="{!NotifyStatementsPresent}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <apex:stylesheet value="{!$Resource.ApplicationTabs}" />
        <apex:stylesheet value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" />
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>    
    
    <style type="text/css">
        .colHeadBlank {
        border: none !important;
        }
        
        ._isNumber {
        text-align: right;
        width: 68px;
        }
        
        ._isLabel {
        text-align: right;
        width: 68px;
        }
        
        ._DatePicker {
        width: 68px;
        }
        
        ._isCurrency {
        width: 100px;
        }
        
        ._DatePickerDefault {
        font-size: 8pt;
        }
        
        .errorMsg {
        color: red;
        font-size: 15px;
        }
        
        .validation_Error {
        border: 2px solid red;
        }
        
        table[role=presentation] td {
        border-bottom: none !important;
        padding-left: 5px !important;
        }
        
        .hidden {
        display: none;
        }
        
        /*remove customPopup, disabledTextBox and closeButton*/
        
        .customPopup {
        background-color: white;
        border-style: solid;
        border-width: 2px;
        left: 20%;
        padding: 10px;
        position: absolute;
        z-index: 9999;
        
        /* These are the 3 css properties you will need to tweak so the pop
        up displays in the center of the screen. First set the width. Then set
        margin-left to negative half of what the width is. You can also add
        the height property for a fixed size pop up.*/
        
        width: 500px;
        top: 20%;
        
        }
        
        .disabledTextBox {
        
        background-color: white;
        
        border: 1px solid;
        
        color: black;
        
        cursor: default;
        
        width: 90px;
        
        display: table;
        
        padding: 2px 1px;
        
        text-align:right;
        
        }    
        
        .closeButton {
        
        float: right;
        
        }

    </style>
    
    
    <apex:composition template="genesis__wizardComposition">
        <!-- Define the page body, including form and button bar -->
        <apex:define name="pageBody">
            <!-- The first column -->
            <apex:panelGrid columnClasses="col1,col2" columns="2" width="100%" cellpadding="10" cellspacing="10" border="2" rules="all">
                
                <!-- left arrow button -->
                <apex:outputPanel layout="block" styleClass="outPanelLeft">
                    <div class="button bordered back">
                        <span class="pointer"></span>
                        <div class="content">
                            <span class="label">
                                <apex:commandLink action="{!backActionLogic}" value="{!manager.backButtonText}" />
                            </span>
                        </div>
                    </div>
                </apex:outputPanel>
                
                <!-- right arrow button -->
                <apex:outputPanel layout="block">
                    
                    <div class="button bordered next">
                        <span class="pointer"></span>
                        <div class="content">
                            <span class="label">
                                <apex:commandLink action="{!nextActionLogic}" value="{!manager.nextButtonText}"/>
                            </span>
                        </div>
                    </div>
                    
                </apex:outputPanel>
                
            </apex:panelGrid>
            <div class="{!isReadOnly}">
                <apex:pageBlock >
                    <apex:pageMessages escape="false" id="msg"/>
                    <apex:pageMessage summary="This customer has requested not to be contacted by phone or email. Please do not contact this customer in any way unless otherwise advised by a manager." severity="warning" strength="3" rendered="{!genesis__Applications__c.Do_Not_Contact__c}" />
                    <apex:pageMessage title="Please note this is a Refinance application. Please make sure the offer created date is the current date when pitching the offer to the customer. If the created date is different, please Re-grade the application." severity="warning" strength="3" rendered="{!IF(genesis__Applications__c.ApplicationType__c = 'Refinance', true, false)}"/>
                </apex:pageBlock>
                <apex:pageBlock title="ACH Information: " id="ACHscreenId">
                    <!-- Start of INC-1883 -->
                    <apex:inputHidden id="hiddenACHinfo" value="{!hiddenACHAcctNbr}" />
                    <apex:inputHidden id="hiddenConfirmACHinfo" value="{!hiddenConfACHAcctNbr}" />
                    <apex:inputHidden id="hiddenRoutingInfo" value="{!hiddenRoutingNbr}" />
                    <apex:inputHidden id="hiddenConfirmRoutinginfo" value="{!hiddenConfRoutingNbr}" />
                    <!-- Start of INC-1883 -->
                    <apex:inputHidden id="hiddenGetAvailability" value="{!getAvailability}" />
                    <apex:pageblockButtons >
                        <apex:commandButton value="Save" action="{!saveApp}" onclick="return verifyRoutingNumber(this);" />
                        <apex:commandButton value="Invitation E-Mail" onclick="return confirm('The Invitation E-mail will be sent, are you sure?');" action="{!sendInvitationEmail}"/>
                        <!-- #3043 - Ops Workflow Improvements - Customer Profile -->
                        <apex:include pageName="CustomerProfile"/>
                        <!-- #3043 - Ops Workflow Improvements - Customer Profile -->
                        <!-- #3016 - Ops Workflow Improvements - Document Checklist -->
                        <input id="showDocumentChecklist" type="button" class="btn" style="{!hideDocChkButton}" value="Document Checklist" />
                        <!-- #3016 - Ops Workflow Improvements - Document Checklist -->
                        <apex:commandButton value="Check Decision Logic Availability" id="dlCheck" action="{!checkDecisionLogicAvailability}" onclick="return validateFieldsForDecisionLogicAvailability(this);" rerender=""/> 
                        <!--3889 starts-->
                         <apex:commandButton value="Income validation" id="btnIncomeValidation" action="{!incomeValidation}" />                        
                          <!--3889 ends-->
                    </apex:pageblockButtons>
                    
                    <apex:pageBlockSection columns="2">
                        <apex:InputField html-noEnterSave="true" value="{!application.ACH_Account_Number__c}" StyleClass="ACHAccountNumber" />
                        <apex:InputField html-noEnterSave="true" value="{!application.Confirm_ACH_Account_Number__c}" StyleClass="ConfirmACHAccountNumber" />
                        <apex:InputField html-noEnterSave="true" value="{!application.ACH_Account_Type__c}" />
                        <apex:InputField html-noEnterSave="true" value="{!application.ACH_Member_Account_Number__c}" />
                        <apex:InputField html-noEnterSave="true" value="{!application.ACH_Routing_Number__c}" StyleClass="ACHRoutingNumber" />
                        <apex:InputField html-noEnterSave="true" value="{!application.Confirm_ACH_Routing_Number__c}" StyleClass="ConfirmACHRoutingNumber" />
                        <apex:InputField html-noEnterSave="true" value="{!application.ACH_Bank_Name__c}" id="ACHBankname" StyleClass="ACHBankname"/>
                        <apex:inputField value="{!application.Others__c}" />
                        <apex:inputField html-noEnterSave="true" value="{!application.Verified_Checklist__c}" />
                        <apex:inputField value="{!application.Bank_Info_Source__c}" />
                        <!-- <apex:InputField value="{!application.ACH_Drawer_Name__c}" /> -->
                    </apex:pageBlockSection>
                    
                    <apex:pageBlockSection title="Deal Assignment" columns="2">
                        <apex:InputField html-noEnterSave="true" value="{!application.DE_Assigned_To_ACH__c}" />
                    </apex:pageBlockSection>
                    
                </apex:pageBlock>
                
                <apex:pageBlock title="Bank Statements Information">
                    
                    <apex:pageBlockSection title="Request (Information Entered)" columns="2">
                        <apex:outputText label="Customer Id " value="{!dl_AccountStatementSF.customerIdentifier}" />
                        <apex:outputText label="Name Entered " value="{!dl_AccountStatementSF.nameEntered}" />
                        <apex:outputText label="Routing Number " value="{!dl_AccountStatementSF.routingNumberEnteredText}" />
                        <apex:outputText label="Institution Name " value="{!dl_AccountStatementSF.institutionName}" />
                        <apex:outputText label="Email Address " value="{!dl_AccountStatementSF.emailAddress}" />
                        <apex:outputText label="Account Number " value="{!dl_AccountStatementSF.accountNumberEntered}" />                               
                        <apex:outputText label="Request Code " value="{!dl_AccountStatementSF.requestCode}" />
                    </apex:pageBlockSection>               

                    <apex:pageBlockSection title="Response (Information Returned by Bank)" columns="2">
                        <apex:selectList value="{!application.DL_Account_Number_Found__c}" label="Account Number Found" size="1">
                            <apex:actionSupport event="onchange" action="{!useSelectedAccount}" />
                            <apex:selectOptions value="{!options}"/>
                        </apex:selectList>
                        <apex:outputText label="Account Name " value="{!dl_AccountStatementSF.accountName}" />
                        <apex:outputText label="Name Found " value="{!dl_AccountStatementSF.nameFound}" />
                        <apex:outputText label="Account Type: " value="{!dl_AccountStatementSF.accountType}" />
                        <apex:outputText label="Login: " value="{!dl_AccountStatementSF.loginText}" />
                        <apex:outputText label="Status: " value="{!dl_AccountStatementSF.statusText}" />
                    </apex:pageBlockSection>
                    
                    <apex:pageBlockSection title="Notes" columns="1">
                        <apex:outputText value="{!dl_AccountStatementSF.notes}" escape="false"/>
                    </apex:pageBlockSection>
                    
                    <apex:pageBlockSection title="Summary" columns="2">
                        <apex:InputField html-noEnterSave="true" value="{!application.DL_Available_Balance__c}" styleClass="_isNumber _isCurrency"/>
                        <apex:InputField html-noEnterSave="true" value="{!application.DL_As_of_Date__c}" />
                        <apex:InputField html-noEnterSave="true" value="{!application.DL_Current_Balance__c}" styleClass="_isNumber _isCurrency"/>
                        <apex:InputField html-noEnterSave="true" value="{!application.DL_Average_Balance__c}" styleClass="_isNumber _isCurrency"/>
                        <apex:InputField html-noEnterSave="true" value="{!application.DL_Deposits_Credits__c}" styleClass="_isNumber _isCurrency"/>
                        <apex:InputField html-noEnterSave="true" value="{!application.DL_Avg_Bal_Latest_Month__c}" styleClass="_isNumber _isCurrency"/>
                        <apex:InputField html-noEnterSave="true" value="{!application.DL_Withdrawals_Debits__c}" styleClass="_isNumber _isCurrency"/>
                        <apex:InputField label="Negative Days (Last Month)" html-noEnterSave="true" value="{!application.DL_Negative_Days__c}" styleClass="_isNumber"/>
                    </apex:pageBlockSection>
                    
                    <apex:pageblockSection columns="1">
                        <apex:pageBlockTable id="summaryTableWithout" value="{!dl_AccountStatementSF.transactionAnalysisSummaries}" var="txnAnalysis" columnsWidth="20%,10%,15%,15%,20%,20%">
                            <apex:column headerValue="Description" headerClass="colHeadBlank">
                                <apex:outputLink onclick="return openPopup('{!$Page.DL_TransactionsScreen}','{!txnAnalysis.popupLink}');">
                                    {!txnAnalysis.typeName}  
                                </apex:outputLink>             
                            </apex:column>
                            <apex:column headerValue="Code" headerClass="colHeadBlank">
                                <apex:outputText value="{!txnAnalysis.typeCode}"/>
                            </apex:column>
                            <apex:column headerValue="Transactions" headerClass="colHeadBlank">
                                <apex:outputText value="{!txnAnalysis.totalCount}"/>
                            </apex:column>
                            <apex:column headerValue="Total" headerClass="colHeadBlank" >
                                <apex:outputText value="{0, number, $###,###.##}">
                                    <apex:param value="{!txnAnalysis.totalAmount}" />      
                                </apex:outputText>
                            </apex:column>
                            <apex:column headerValue="Transactions Last Month *" headerClass="colHeadBlank">
                                <apex:outputText value="{!txnAnalysis.recentCount}"/>
                            </apex:column>
                            <apex:column headerValue="Total Last Month *">
                                <apex:outputText value="{0, number, $###,###.##}">
                                    <apex:param value="{!txnAnalysis.recentAmount}" />      
                                </apex:outputText>
                            </apex:column>
                        </apex:pageBlockTable>
                        
                        <apex:pageBlockSection columns="1">
                            <apex:outputLabel value="{!lastMonthDates}" styleClass="_isLabel"></apex:outputLabel>
                        </apex:pageBlockSection>
                        
                        <apex:pageBlockSection columns="1">
                            <apex:outputLink rendered="{!showFullTransactionsList}" onclick="return openPopup('{!$Page.DL_TransactionsScreen}','{!allTransactionsPopupLink}');" >
                                Click here to display the full transactions list
                            </apex:outputLink>
                        </apex:pageBlockSection>
                        
                    </apex:pageblockSection>
                    
                    <apex:pageblockSection title="Account expenses" columns="1">
                        <apex:pageBlockTable id="expensesTable" value="{!dl_AccountStatementSF.AccountExpenses}" var="expense" columnsWidth="33%,33%,44%">
                            <apex:column headerValue="Category" headerClass="colHeadBlank">
                                <apex:OutputText value="{!expense.category}" />
                            </apex:column>
                            <apex:column headerValue="Amount" headerClass="colHeadBlank">
                                <apex:outputText value="{0, number, $###,###.##}">
                                    <apex:param value="{!expense.amount}" />      
                                </apex:outputText>
                            </apex:column>
                            <apex:column headerValue="Percentage" headerClass="colHeadBlank">
                                <apex:outputText value="{0, number, ###.##}">
                                    <apex:param value="{!expense.percentage}" />      
                                </apex:outputText>
                            </apex:column>
                        </apex:pageBlockTable>
                    </apex:pageblockSection>
                    <!--3889 starts-->
                    <apex:pageblockSection title="Income Validation Summary" columns="1">
                   <apex:outputPanel id="IncomeValidationResult" >
                   <!-- <apex:actionregion id="IncomeValidationResult">-->
                     <table style="width:100%">
                     <tr>
                        <th style="width:25%" class="labelCol vfLabelColTextWrap  first ">
                        <apex:outputLabel value="Self-reported Income :" /></th>
                         <td style="width:25%" class="dataCol  first ">
                         <apex:outputText value="{!application.genesis__Contact__r.Annual_Income__c}" />
                         </td>
                         <th style="width:25%" class="labelCol vfLabelColTextWrap  first ">
                         <apex:outputLabel value="Estimated Gross Income :" style="font-weight:bold"/></th>
                        <td style="width:25%" class="dataCol  first ">
                         <apex:outputText value="{!application.Estimated_Gross_Income__c}" /></td></tr>
                         <tr><th style="width:25%" class="labelCol vfLabelColTextWrap  first ">
                         <apex:outputLabel value="Gross % to Declare :" style="font-weight:bold"/></th>
                         <td style="width:25%" class="dataCol  first ">
                         <apex:outputText value="{!application.Gross_Percentage_To_Declared__c}" /></td>
                         <th style="width:25%" class="labelCol vfLabelColTextWrap  first ">
                         <apex:outputLabel value="Result  :" style="font-weight:bold"/></th>
                         <td style="width:25%" class="dataCol  first ">
                         <apex:outputText value="{!application.Decision_Logic_Result__c}" /></td>
                         </tr>
                         </table>
                       </apex:outputPanel>
                    </apex:pageblockSection>
                    <!--<apex:actionFunction name="incomeValidation" action="{!incomeValidation}" oncomplete="hideProcessing();" />-->
                  <!--3889 ends-->
                    
                </apex:pageBlock>
                
                <apex:pageBlock title="Attach Note">
                    <apex:commandButton value="Attach Note" action="{!hidepanel}" rendered="{!hideattach}" />
                    <apex:pageBlockSection title="Attach Note" id="uploadPanel" columns="1" rendered="{!showpanel}">
                        <apex:inputText value="{!mynote.title}" label="Note Title" />
                        <apex:inputTextarea value="{!mynote.body}" label="Note body" />
                        <apex:commandButton value="Save Note" action="{!Savedoc}" />
                    </apex:pageBlockSection>
                </apex:pageBlock>
            </div>
        </apex:define>
       <apex:define name="pageNoForm"> 
            <apex:relatedList subject="{!genesis__Applications__c}" list="CombinedAttachments">
            <apex:facet name="header">
                <table>
                    <tr>
                        <td class="pbTitle">
                            <h3> Notes And Attachments</h3></td>
                    </tr>
                </table>
            </apex:facet>
        </apex:relatedList>
        </apex:define>        
    </apex:composition>
    
    
<script>
    $j = jQuery.noConflict();

    function openAdmin(iav) {
        var url = '';
        
        if (iav == true) {
            
            url = "https://iav.lendingpoint.com/verification/admin-iav";
        } else {
            
            url = "https://iav.lendingpoint.com/integration/admin-integration";
        }
        window.open(url);
    }
    
    function createUserYodlee(iav) {
        var appName = 'Bank Transaction';
        
        if (iav) {
            appName = 'Account Verification';
        }
        
        var html = '<div id="dialog" style="display: none" title="Create user"><p> Would you like to create this user on the Yodlee App? An Email will be sent to {!application.genesis__Contact__r.Email}.</p></div>';
        
        var $j = jQuery.noConflict();
        $j(function() {
            $j('head').append('<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" type="text/css"/>');
            $j('body').append(html);
            
            $j("#dialog").dialog({
                autoOpen: true,
                modal: true,
                buttons: {
                    "Create User": function() {
                        $j(this).dialog("close");
                        var url = '';
                        if (iav == true) {
                            //url = "https://192.169.197.187:8443/lendingpoint-iav/createUserSalesForceGet/" + 'WandersonBP' + "/" + 'wxesquevixos@bpanalytics.com' +"/iav";
                            
                            url = "https://iav.lendingpoint.com/verification/createUserSalesForceGet/" + '{!application.genesis__Contact__r.Name}' + "/" + '{!application.genesis__Contact__r.Email}' + "/iav";
                        } else {
                            //url = "https://192.169.197.187:8443/lendingpoint-iav/createUserSalesForceGet/" + 'WandersonYahoo' + "/" + 'wandersonxs@yahoo.com.br' +"/integration";
                            
                            url = "https://iav.lendingpoint.com/integration/createUserSalesForceGet/" + '{!application.genesis__Contact__r.Name}' + "/" + '{!application.genesis__Contact__r.Email}' + "/integration";
                        }
                        
                        $j.ajax({
                            url: url,
                            type: 'GET',
                            cache: false,
                            crossDomain: true
                        }).done(function() {
                            alert("User created and email sent!");
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            alert("Error: Was not able to create user. They may already exist in the system. " + jqXHR.responseText);
                        });
                        
                    },
                    "Cancel": function() {
                        $j(this).dialog("close");
                    }
                }
            });
        });
    }
    //Start of INC-1555
    
    function verifyRoutingNumber(t) {
        try {
            $j('.validation_Account').hide();
            $j('.validation_Routing').hide();
            $j('.ConfirmACHRoutingNumber').removeClass('validation_Error');
            $j('.ConfirmACHAccountNumber').removeClass('validation_Error');
            $j('.ACHRoutingNumber').removeClass('validation_Error');
            $j('.ACHAccountNumber').removeClass('validation_Error');
            
            //Start of INC-1883:
            // Replacing values with star (*) with the actual info for verification:
            var ACHAccountNumber = $j("[id$='hiddenACHinfo']");
            var ConfirmACHAccountNumber = $j("[id$='hiddenConfirmACHinfo']");
            var ACHRoutingNumber = $j("[id$='hiddenRoutingInfo']");
            var ConfirmACHRoutingNumber = $j("[id$='hiddenConfirmRoutinginfo']");
            // End of INC-1883.
            
            if (ACHRoutingNumber.val().trim() != null && ACHRoutingNumber.val().trim().length != 0 && ACHRoutingNumber.val().trim().length != 9) {
                alert('The ACH Routing Number must be exactly 9 digits');
                return false;
            } else if ((ACHAccountNumber.val().trim() != '' || ConfirmACHAccountNumber.val().trim() != '') && ACHAccountNumber.val().trim() != ConfirmACHAccountNumber.val().trim()) {
                $j('.ConfirmACHAccountNumber').addClass('validation_Error');
                $j('.ACHAccountNumber').addClass('validation_Error');
                $j('.ConfirmACHAccountNumber').parent().after("<div class='validation_Account' style='color:red;margin-bottom: 20px; font-size:13px;'>Mismatch</div>");
                return false;
            } else if ((ACHRoutingNumber.val().trim() != '' || ConfirmACHRoutingNumber.val().trim() != '') && ACHRoutingNumber.val().trim() != ConfirmACHRoutingNumber.val().trim()) {
                $j('.ConfirmACHRoutingNumber').parent().after("<div class='validation_Routing' style='color:red;margin-bottom: 20px; font-size:13px;'>Mismatch</div>");
                $j('.ConfirmACHRoutingNumber').addClass('validation_Error');
                $j('.ACHRoutingNumber').addClass('validation_Error');
                return false;
            } //End of INC-1555
            
        } catch (e) {
            alert(e);
        }
        return true;
    }

    <!-- DecLogic -->
    function openPopup(page,param){        
        window.open(page+'?'+param,'_blank', 'location=no, toolbar=no, scrollbars=yes, resizable=yes, top=150, left=100, width=900, height=400')
        return false;
    }

    function removeDelAndEditLinks() {
        $j(".actionLink").each( function(){ 
            if ($j(this).text() != 'View' ) { 
                $j(this).hide(); 
            } 
        });
    }
    //3889 starts
    //function hideProcessing(){
       // $j('#btnIncomeValidation').prop('disabled', false); 
       // $j('#img').hide();
       //  return true;
    //}   
    //3889 ends
    
    $j(document).ready(function() {
        
        //Do not allow saves with "Enter.key" 
        // html-noEnterSave="true"
        $j("[noEnterSave~='true']").keydown(function(e) {
            // (13) Enter
            if (e.keyCode == 13)
                e.preventDefault();
            else
                return
        });

        // Start of INC-1941 - HOT FIX:
        var ach = $j("[id$='hiddenACHinfo']").val();
        var confAch = $j("[id$='hiddenConfirmACHinfo']").val();
        var rout = $j("[id$='hiddenRoutingInfo']").val();
        var confRout = $j("[id$='hiddenConfirmRoutinginfo']").val();
        
        $j(".ACHAccountNumber").val(ach);
        $j(".ConfirmACHAccountNumber").val(confAch);
        $j(".ACHRoutingNumber").val(rout);
        $j(".ConfirmACHRoutingNumber").val(confRout);
        // End of INC-1941 - HOT FIX:
        
        loadInputsValidation();
        initDatePicker();
        
        // Start of INC-1941:
        $j(".ACHAccountNumber, .ConfirmACHAccountNumber, .ACHRoutingNumber, .ConfirmACHRoutingNumber").attr('autocomplete', 'off');
        
        $j(".ACHAccountNumber, .ConfirmACHAccountNumber, .ACHRoutingNumber, .ConfirmACHRoutingNumber").keydown(function(e) {
            // Allows:
            // (46) Backspace
            // (8) Delete
            // (9) Tab
            // (27) Escape
            // (37) Left arrow key
            // (39) Right arrow key
            if ($j.inArray(e.keyCode, [46, 8, 9, 27, 37, 39]) !== -1)
                return;
            // If key pressed is NOT a number, stop the event.
            else if (e.shiftKey || ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)))
                e.preventDefault();
        });
        // End of INC-1941:
        
        //Start of INC-1883:
        $j(".ACHAccountNumber, .ConfirmACHAccountNumber, .ACHRoutingNumber, .ConfirmACHRoutingNumber").bind("cut copy paste contextmenu", function(e) {
            e.preventDefault();
        });
        
        $j(".ACHAccountNumber").focusin(function(e) {
            $j(".ACHAccountNumber").val($j("[id$='hiddenACHinfo']").val());
            e.preventDefault();
        });
        
        $j(".ACHAccountNumber").focusout(function(e) {
            $j("[id$='hiddenACHinfo']").val($j(".ACHAccountNumber").val());
            $j(".ACHAccountNumber").val($j(".ACHAccountNumber").val().replace(/(.+?)/gi, '*'));
            e.preventDefault();
        });
        
        $j(".ConfirmACHAccountNumber").focusin(function(e) {
            $j(".ConfirmACHAccountNumber").val($j("[id$='hiddenConfirmACHinfo']").val());
            e.preventDefault();
        });
        
        $j(".ConfirmACHAccountNumber").focusout(function(e) {
            $j("[id$='hiddenConfirmACHinfo']").val($j(".ConfirmACHAccountNumber").val());
            $j(".ConfirmACHAccountNumber").val($j(".ConfirmACHAccountNumber").val().replace(/(.+?)/gi, '*'));
            e.preventDefault();
        });
        
        $j(".ACHRoutingNumber").focusin(function(e) {
            $j(".ACHRoutingNumber").val($j("[id$='hiddenRoutingInfo']").val());
            e.preventDefault();
        });
        
        $j(".ACHRoutingNumber").focusout(function(e) {
            $j("[id$='hiddenRoutingInfo']").val($j(".ACHRoutingNumber").val());
            $j(".ACHRoutingNumber").val($j(".ACHRoutingNumber").val().replace(/(.+?)/gi, '*'));
            e.preventDefault();
        });
        
        $j(".ConfirmACHRoutingNumber").focusin(function(e) {
            $j(".ConfirmACHRoutingNumber").val($j("[id$='hiddenConfirmRoutinginfo']").val());
            e.preventDefault();
        });
        
        $j(".ConfirmACHRoutingNumber").focusout(function(e) {
            $j("[id$='hiddenConfirmRoutinginfo']").val($j(".ConfirmACHRoutingNumber").val());
            $j(".ConfirmACHRoutingNumber").val($j(".ConfirmACHRoutingNumber").val().replace(/(.+?)/gi, '*'));
            e.preventDefault();
        });
        
        $j(".ACHAccountNumber, .ConfirmACHAccountNumber, .ACHRoutingNumber, .ConfirmACHRoutingNumber").blur();
        $j("[id$='hiddenConfirmRoutinginfo']").focus();
        
        //End of INC-1883:  

        removeDelAndEditLinks();    
    });
    
    var initDatePicker = function() {
        $j('._DatePicker').datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
        }).keyup(function(e) {
            if (e.keyCode == 8 || e.keyCode == 46) {
                $j.datepicker._clearDate(this);
            }
            if ((e.keyCode || e.which) == 9) {
                $j(this).next().focus();
            }
        }).keydown(false);
        $j("._DatePickerDefault").html(getCurrentDate());
        $j("._DatePickerDefault").off('click').on('click', function() {
            $j(this).parent().siblings('._DatePicker').val(getCurrentDate());
        });
    };
    
    var loadInputsValidation = function() {
        
        $j("[id$='bankStatementTable'], [id$='bankStatementsDepositsTable']").off('keyup.amount', 'input._isNumber[type=text]').on('keyup.amount', 'input._isNumber[type=text]', function(e) {
            var valid = /^-?\d{0,12}(\.\d{0,2})?$/.test(this.value),
                val = this.value;
            
            if (!valid) {
                this.value = val.replace(/[^0-9\.-]+/g, '').substring(0, val.length - 1);
            }
        });
        
        $j("[id$='bankStatementTable'] ,[id$='bankStatementsDepositsTable']").off('blur.amount', 'input._isCurrency[type=text]').on('blur.amount', 'input._isCurrency[type=text]', function(e) {
            var valid = /^-?\d{0,12}(\.\d{0,2})?$/.test(this.value);
            if (valid) {
                if (toNum(this.value) < 0) {
                    this.value = '-' + formatCurrency(toNum(this.value) * -1);
                } else {
                    this.value = formatCurrency(toNum(this.value));
                }
            } else {
                this.value = "";
            }
        });
        
        $j("[id$='bankStatementTable'],  [id$='bankStatementsDepositsTable']").off('focus.amount', 'input._isNumber[type=text]').on('focus.amount', 'input._isNumber[type=text]', function(e) {
            this.value = this.value.replace(/,/g, '');
        });
    }
    
    /*Convert str to Number*/
    var toNum = function(num) {
        return isNaN(num) || num === '' || num === null ? '' : parseFloat(num);
    }
    
    /*format Currency*/
    var formatCurrency = function(num) {
        return num.toFixed(2).replace(/./g, function(c, i, a) {
            return i && c !== '.' && !((a.length - i) % 3) ? ',' + c : c;
        });
    }
    
    var getCurrentDate = function() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        return mm + '/' + dd + '/' + yyyy;
    };

    function validateFieldsForDecisionLogicAvailability(field){
        try{
            $j('.validation_Routing').hide();
            $j('.ACHRoutingNumber').removeClass('validation_Error');
            $j('.ACHBankname').removeClass('validation_Error');

            var ACHRoutingNumber = $j("[id$='hiddenRoutingInfo']");
            var ACHBankname = $j("[id$='ACHBankname']");
            var ACHRoutingNumberIsNotEmpty = (ACHRoutingNumber.val().trim() != null && ACHRoutingNumber.val().trim().length != 0 && ACHRoutingNumber.val().trim() != '');
            var ACHRoutingNumberIsValid = (ACHRoutingNumber.val().trim() != null && ACHRoutingNumber.val().trim().length != 0 && ACHRoutingNumber.val().trim().length == 9);
            var ACHBanknameIsNotEmpty= (ACHBankname.val().trim() != null && ACHBankname.val().trim().length != 0 && ACHBankname.val().trim() != '');

            if(ACHRoutingNumberIsNotEmpty){

                if(!ACHRoutingNumberIsValid){
                    alert('The ACH Routing Number must be exactly 9 digits.');
                    $j('.ACHRoutingNumber').addClass('validation_Error');
                    return false;
                }else {

                    $j("[id$='hiddenGetAvailability']").val('1');
                    return true;
                }
            }else if(ACHBanknameIsNotEmpty){

                $j("[id$='hiddenGetAvailability']").val('2');

            }else{

                alert('Please fill the Routing Number or Bank Name field.');
                return false;
            } 

        }catch (e) {
            alert(e);
        }
        return true;
    };
</script>

    <!-- #3016 - Ops Workflow Improvements - Document Checklist -->
    <apex:include pageName="DocumentChecklist"/>
    <!-- #3016 - Ops Workflow Improvements - Document Checklist -->
    
</apex:page>