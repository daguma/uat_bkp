<apex:page controller="CreditReportDetailsCtrl" title="Credit Report Details" action="{!Initialize}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <style type="text/css">
    .colHeadLeft {
        text-align: left;
        background-color: white !important;
        color: #006699 !important;
        border: none !important;
        border-bottom: 1px solid #e3deb8 !important;
        height: 25px !important;
    }
    
    .colHeadAmount {
        text-align: left;
        background-color: white !important;
        color: #006699 !important;
        border: none !important;
        border-bottom: 1px solid #e3deb8 !important;
        height: 25px !important;
        padding-left: 90px !important;
    }
    
    .colHeadCenter {
        text-align: center;
        background-color: white !important;
        color: #006699 !important;
        border: none !important;
        border-bottom: 1px solid #e3deb8 !important;
        height: 25px !important;
        white-space: pre !important;
        font-weight: bold;
    }
    
    .colTitle {
        color: #006699 !important;
        height: 25px !important;
        display: block;
        vertical-align: middle !important;
        padding-top: 5px;
        font-weight: bold;
    }
    
    .sectionTable {
        border: none !important;
        border-bottom: 1px solid #e3deb8 !important;
    }
    
    table[role=presentation] td {
        border-bottom: none !important;
        padding-left: 5px !important;
    }
    
    table[name=amounts] input[type=text] {
        max-width: 120px;
        text-align: right;
        width: 100px;
    }
    
    .dataCell > fieldset {
        display: inline !important;
        max-height: 20px;
        padding-top: 0;
    }
    
    input[type="radio"] {
        display: none;
    }
    
    input[type=radio] + label:before {
        content: "";
        display: inline-block;
        width: 15px;
        height: 15px;
        vertical-align: middle;
        margin-right: 4px;
        background-color: #fafafa;
        border-radius: 0px;
        border: 1px solid #a9a9a9;
    }
    
    input[type=radio]:checked + label:before {
        content: "\2714";
        color: #99a1a7;
        background-color: #e9ecee;
        text-align: center;
        line-height: 15px;
    }
    
    ._DatePicker {
        width: 68px;
    }
    
    ._DatePickerDefault {
        font-size: 8pt;
    }
    
    textarea {
        border-color: #BFBFBF;
    }
    
    .poi {
        margin-right: 5%;
        margin-left: 16%;
        font-size: 8pt;
    }
    </style>

    <apex:form >

        <apex:pageBlock >
            <apex:pageMessages escape="false" />
            <apex:commandButton action="{!goBack}" Value="Back" id="btnBack" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </apex:pageBlock>

        <apex:pageBlock title="Credit Report">
            <apex:pageblockSection title="Credit Report Detail" columns="1">

                <apex:pageBlockTable value="{!crDetailsList}" var="crDetail" columnsWidth="30%,40%,30%" styleClass="sectionTable">

                    <apex:column headerValue="" style="text-align:right;" headerClass="colHeadCenter">
                        <apex:outputText value="{!crDetail.key}: " />
                    </apex:column>
                    <apex:column headerValue="" style="text-align:left;" headerClass="colHeadCenter">
                        <apex:outputText value="{!crDetail.displayText}" rendered="{!crDetail.isText}" />
                        <apex:outputLink value="{!crDetail.value}" rendered="{!crDetail.isAnchor}">{!crDetail.displayText}</apex:outputLink>
                    </apex:column>
                    <apex:column headerValue="" style="text-align:center;" headerClass="colHeadCenter">

                    </apex:column>

                </apex:pageBlockTable>

            </apex:pageblockSection>

            <apex:pageblockSection title="Criteria Values" columns="1">
                <apex:pageBlockTable value="{!crAttributesList}" var="attribute" columnsWidth="30%,40%,30%" styleClass="sectionTable">

                    <apex:column headerValue="" style="text-align:right;" headerClass="colHeadLeft">
                        <apex:outputText value="{!attribute.attributeType.conceptType}: " title="{!attribute.attributeType.description}" />
                    </apex:column>

                    <apex:column headerValue="" style="text-align:left;" headerClass="colHeadCenter">
                        <apex:outputText value="{!attribute.value}" />
                    </apex:column>
                    <apex:column headerValue="" style="text-align:center;" headerClass="colHeadCenter">

                    </apex:column>

                </apex:pageBlockTable>
            </apex:pageblockSection>

            <apex:pageBlockSection title="Other Values" columns="1">
                <apex:pageBlockTable value="{!crOtherAttributeList}" var="attribute" columnsWidth="30%,40%,30%" styleClass="sectionTable">
                    <apex:column headerValue="" style="text-align:right;" headerClass="colHeadLeft">
                        <apex:outputText value="{!attribute.attributeType.conceptType}: " title="{!attribute.attributeType.description}" />
                    </apex:column>

                    <apex:column headerValue="" style="text-align:left;" headerClass="colHeadCenter">
                        <apex:outputText value="{!attribute.value}" />
                    </apex:column>

                    <apex:column headerValue="" style="text-align:center;" headerClass="colHeadCenter">
                    </apex:column>
                </apex:pageBlockTable>
            </apex:pageBlockSection>


        </apex:pageBlock>

        <apex:pageBlock title="Credit Report Liability">
            <div style="position: relative; overflow-x: auto; max-width: 1024px">

                <table class="list">
                    <tr>
                        <td class="colHeadCenter">
                            <apex:outputText value="Segments Codes" />
                        </td>
                        <apex:repeat value="{!crLiabilities.headersList}" var="header">
                            <td class="colHeadCenter">
                                <apex:outputText value="{!header}" />
                            </td>
                        </apex:repeat>
                    </tr>
                    <apex:repeat value="{!crLiabilities.contentList}" var="content">
                        <tr class="dataRow">
                            <td class="dataCell" nowrap="nowrap">
                                <center>
                                    <apex:outputLink value="javascript:void(0);" html-data-content="{!content.subSegmentDetails}" style="text-decoration: none;">
                                        {!content.subSegmentsLink}
                                    </apex:outputLink>
                                </center>
                            </td>
                            <apex:repeat value="{!content.valuesList}" var="value">
                                <td class="dataCell">
                                    <center>
                                        <apex:outputText value="{!value}" />
                                    </center>
                                </td>
                            </apex:repeat>
                        </tr>
                    </apex:repeat>
                </table>
            </div>
        </apex:pageBlock>

        <apex:pageBlock title="Credit Report Score">
            <div style="position: relative; overflow-x: auto; max-width: 1024px">

                <table class="list">
                    <tr>
                        <td class="colHeadCenter">
                            <apex:outputText value="Segments Codes" />
                        </td>
                        <apex:repeat value="{!crScores.headersList}" var="header">
                            <td class="colHeadCenter">
                                <apex:outputText value="{!header}" />
                            </td>
                        </apex:repeat>
                    </tr>
                    <apex:repeat value="{!crScores.contentList}" var="content">
                        <tr class="dataRow">
                            <td class="dataCell" nowrap="nowrap">
                                <center>
                                    <apex:outputLink value="javascript:void(0);" html-data-content="{!content.subSegmentDetails}" style="text-decoration: none;">
                                        {!content.subSegmentsLink}
                                    </apex:outputLink>
                                </center>
                            </td>
                            <apex:repeat value="{!content.valuesList}" var="value">
                                <td class="dataCell">
                                    <center>
                                        <apex:outputText value="{!value}" />
                                    </center>
                                </td>
                            </apex:repeat>
                        </tr>
                    </apex:repeat>
                </table>
            </div>
        </apex:pageBlock>

        <apex:pageBlock title="Credit Report Inquiry">
            <div style="position: relative; overflow-x: auto; max-width: 1024px">

                <table class="list">
                    <tr>
                        <td class="colHeadCenter">
                            <apex:outputText value="Segments Codes" />
                        </td>
                        <apex:repeat value="{!crInquiries.headersList}" var="header">
                            <td class="colHeadCenter">
                                <apex:outputText value="{!header}" />
                            </td>
                        </apex:repeat>
                    </tr>
                    <apex:repeat value="{!crInquiries.contentList}" var="content">
                        <tr class="dataRow">
                            <td class="dataCell" nowrap="nowrap">
                                <center>
                                    <apex:outputLink value="javascript:void(0);" html-data-content="{!content.subSegmentDetails}" style="text-decoration: none;">
                                        {!content.subSegmentsLink}
                                    </apex:outputLink>
                                </center>
                            </td>
                            <apex:repeat value="{!content.valuesList}" var="value">
                                <td class="dataCell">
                                    <center>
                                        <apex:outputText value="{!value}" />
                                    </center>
                                </td>
                            </apex:repeat>
                        </tr>
                    </apex:repeat>
                </table>
            </div>
        </apex:pageBlock>

        <apex:pageBlock title="Credit Summary">

            <div style="position: relative; overflow-x: auto; max-width: 1024px">

                <table class="list">
                    <tr>
                        <td class="colHeadCenter">
                            <apex:outputText value="Segments Codes" />
                        </td>
                        <apex:repeat value="{!crSummaries.headersList}" var="header">
                            <td class="colHeadCenter">
                                <apex:outputText value="{!header}" />
                            </td>
                        </apex:repeat>
                    </tr>
                    <apex:repeat value="{!crSummaries.contentList}" var="content">
                        <tr class="dataRow">
                            <td class="dataCell" nowrap="nowrap">
                                <center>
                                    <apex:outputLink value="javascript:void(0);" html-data-content="{!content.subSegmentDetails}" style="text-decoration: none;">
                                        {!content.subSegmentsLink}
                                    </apex:outputLink>
                                </center>
                            </td>
                            <apex:repeat value="{!content.valuesList}" var="value">
                                <td class="dataCell">
                                    <center>
                                        <apex:outputText value="{!value}" />
                                    </center>
                                </td>
                            </apex:repeat>
                        </tr>
                    </apex:repeat>
                </table>
            </div>

        </apex:pageBlock>

    </apex:form>

    <div style="display:none">
        <div id="segmentDialog" title="Credit Report Details - Segment Codes">
            <div id="segmentsCodesBody" style="height: 90%; overflow: auto;">

            </div>
            <div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
                <div class="ui-dialog-buttonset">

                    <button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" id="btnCloseCrDialog">
                        <span class="ui-button-text">Close</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $j = jQuery.noConflict();

    var segmentDialog;

    // Create Base64 Object  - encode & decode
    var Base64 = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        encode: function(e) {
            var t = "";
            var n, r, i, s, o, u, a;
            var f = 0;
            e = Base64._utf8_encode(e);
            while (f < e.length) {
                n = e.charCodeAt(f++);
                r = e.charCodeAt(f++);
                i = e.charCodeAt(f++);
                s = n >> 2;
                o = (n & 3) << 4 | r >> 4;
                u = (r & 15) << 2 | i >> 6;
                a = i & 63;
                if (isNaN(r)) {
                    u = a = 64
                } else if (isNaN(i)) {
                    a = 64
                }
                t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
            }
            return t
        },
        decode: function(e) {
            var t = "";
            var n, r, i;
            var s, o, u, a;
            var f = 0;
            e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (f < e.length) {
                s = this._keyStr.indexOf(e.charAt(f++));
                o = this._keyStr.indexOf(e.charAt(f++));
                u = this._keyStr.indexOf(e.charAt(f++));
                a = this._keyStr.indexOf(e.charAt(f++));
                n = s << 2 | o >> 4;
                r = (o & 15) << 4 | u >> 2;
                i = (u & 3) << 6 | a;
                t = t + String.fromCharCode(n);
                if (u != 64) {
                    t = t + String.fromCharCode(r)
                }
                if (a != 64) {
                    t = t + String.fromCharCode(i)
                }
            }
            t = Base64._utf8_decode(t);
            return t
        },
        _utf8_encode: function(e) {
            e = e.replace(/\r\n/g, "\n");
            var t = "";
            for (var n = 0; n < e.length; n++) {
                var r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r)
                } else if (r > 127 && r < 2048) {
                    t += String.fromCharCode(r >> 6 | 192);
                    t += String.fromCharCode(r & 63 | 128)
                } else {
                    t += String.fromCharCode(r >> 12 | 224);
                    t += String.fromCharCode(r >> 6 & 63 | 128);
                    t += String.fromCharCode(r & 63 | 128)
                }
            }
            return t
        },
        _utf8_decode: function(e) {
            var t = "";
            var n = 0;
            var r = c1 = c2 = 0;
            while (n < e.length) {
                r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                    n++
                } else if (r > 191 && r < 224) {
                    c2 = e.charCodeAt(n + 1);
                    t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                    n += 2
                } else {
                    c2 = e.charCodeAt(n + 1);
                    c3 = e.charCodeAt(n + 2);
                    t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                    n += 3
                }
            }
            return t
        }
    }

    $j(function() {
        CustomInit();
        DialogInit();

        $j('div.bPageBlock').find('[title]').tooltip({
            content: function() {
                var element = $j(this);
                return element.attr('title')
            }
        });

    });
    var CustomInit = function() {};

    var DialogInit = function() {
        var width = $j(window).width() * 0.85;
        var height = $j(window).height() * 0.85;
        segmentDialog = $j("#segmentDialog").dialog({
            autoOpen: false,
            height: height,
            width: width,
            modal: true,
            resizable: true,
            draggable: true,
            show: {
                effect: "blind",
                duration: 500
            },
            hide: {
                effect: "explode",
                duration: 500
            },
        });
        $j("#segmentDialog").off('click.btnCloseCrDialog', '#btnCloseCrDialog').on('click.btnCloseCrDialog', '#btnCloseCrDialog', function(e) {
            segmentDialog.dialog("close");
        });

        $j('a[data-content]').off('click.showCrDetails').on('click.showCrDetails', function(e) {

            var htmlContent = Base64.decode($j(this).data('content'));
            segmentDialog.dialog('open');
            $j('#segmentsCodesBody').html(htmlContent);
        });
    };
    </script>
</apex:page>