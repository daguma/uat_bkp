<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Case On Lead</label>
    <protected>false</protected>
    <values>
        <field>Business_Hour_ID__c</field>
        <value xsi:type="xsd:string">01mU0000000L7Dn</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Delay in Merchant Onboarding Progress-</value>
    </values>
    <values>
        <field>Origin__c</field>
        <value xsi:type="xsd:string">Merchant Onboarding</value>
    </values>
    <values>
        <field>Priority__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">New</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Merchant Onboarding Long Form Not Submitted</value>
    </values>
    <values>
        <field>recordTypeId__c</field>
        <value xsi:type="xsd:string">0120B000000ap3p</value>
    </values>
</CustomMetadata>
