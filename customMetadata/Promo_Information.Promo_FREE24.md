<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Promo-FREE24</label>
    <protected>false</protected>
    <values>
        <field>Merchant_Discount__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:type="xsd:string">SAC_24MO_10.00MD</value>
    </values>
    <values>
        <field>Promo_Code__c</field>
        <value xsi:type="xsd:string">FREE24</value>
    </values>
    <values>
        <field>Term__c</field>
        <value xsi:type="xsd:double">24.0</value>
    </values>
</CustomMetadata>
