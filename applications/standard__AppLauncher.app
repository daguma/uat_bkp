<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-AppLauncher</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-AppLauncher</tabs>
    <tabs>loan__CR_Org_Definition__c</tabs>
    <tabs>Dashboard</tabs>
    <tabs>collect__Loan_Account__c</tabs>
    <tabs>Outbound_Emails_Temp__c</tabs>
    <tabs>et4ae5__Configuration__c</tabs>
    <tabs>SIC_Codes__c</tabs>
    <tabs>Loan_Line_Configuration__c</tabs>
    <tabs>Entity__c</tabs>
</CustomApplication>
