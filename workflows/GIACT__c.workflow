<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LMS_GIACT_Error</fullName>
        <description>LMS - GIACT Error</description>
        <protected>false</protected>
        <recipients>
            <recipient>msides@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_GIACT_Error</template>
    </alerts>
    <alerts>
        <fullName>LMS_GIACT_Insufficient_Funds</fullName>
        <description>LMS - GIACT Insufficient Funds</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_GIACT_Insufficient_Funds</template>
    </alerts>
</Workflow>
