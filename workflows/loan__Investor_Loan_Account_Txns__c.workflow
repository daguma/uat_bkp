<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>peer__Update_VAT_Amount</fullName>
        <field>peer__VAT_On_Service_Charge__c</field>
        <formula>IF( ISNULL( $Setup.loan__Fractionalization_Parameters__c.peer__Global_VAT_Percentage__c) , 0,  ( loan__Total_Service_Charge__c * ($Setup.loan__Fractionalization_Parameters__c.peer__Global_VAT_Percentage__c/100))/(1 + ($Setup.loan__Fractionalization_Parameters__c.peer__Global_VAT_Percentage__c/100)))</formula>
        <name>Update VAT Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>peer__Update VAT on Service Charge</fullName>
        <actions>
            <name>peer__Update_VAT_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
