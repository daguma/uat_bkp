<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>peer__Update_VAT_age</fullName>
        <field>peer__VAT_percent__c</field>
        <formula>IF(ISNEW(), loan__Loan_Account__r.peer__VAT_percent__c,  peer__VAT_percent__c )</formula>
        <name>Update VAT %age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>peer__Update VAT Amount</fullName>
        <actions>
            <name>peer__Update_VAT_age</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>loan__Loan_Account__r.peer__VAT__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
