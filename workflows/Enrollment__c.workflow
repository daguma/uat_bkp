<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LMS_Invisalign_Training_Complete_Email</fullName>
        <description>LMS - Invisalign Training Complete Email</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Invisalign_Training_Complete</template>
    </alerts>
</Workflow>
