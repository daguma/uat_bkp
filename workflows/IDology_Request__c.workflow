<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_KYC_Mode_on_Idology_Pass</fullName>
        <field>Kyc_Mode__c</field>
        <formula>&quot;IDology; w/ challenge questions&quot;</formula>
        <name>Set KYC Mode on Idology Pass</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Set KYC Mode by Idology</fullName>
        <actions>
            <name>Set_KYC_Mode_on_Idology_Pass</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IDology_Request__c.Verification_Result__c</field>
            <operation>equals</operation>
            <value>Pass</value>
        </criteriaItems>
        <criteriaItems>
            <field>IDology_Request__c.Verification_Result_Front__c</field>
            <operation>equals</operation>
            <value>Pass</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Kyc_Mode__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
