<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>loan__Savings_Txn_Update_To_Cleared</fullName>
        <field>loan__Cleared__c</field>
        <literalValue>1</literalValue>
        <name>Savings Txn Update To Cleared</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Savings_Txn_Update_To_Rejected</fullName>
        <field>loan__Rejected__c</field>
        <literalValue>1</literalValue>
        <name>Savings Txn Update To Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
