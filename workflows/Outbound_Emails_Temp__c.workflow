<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Outbound_Email_Send</fullName>
        <description>Outbound Email Send</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Outbound_Email_LH</template>
    </alerts>
    <alerts>
        <fullName>Outbound_Email_Send_ezCarePoint</fullName>
        <description>Outbound Email Send ezCarePoint</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>ezcarepoint@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Outbound_HTML_Email_EzCare</template>
    </alerts>
    <alerts>
        <fullName>Outbound_Email_to_Coordinator</fullName>
        <description>Outbound Email to Co-ordinator</description>
        <protected>false</protected>
        <recipients>
            <field>TO__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>ezcarepoint@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ffxp__ffdc5_emailTemplates/Outbound_Email_Header_EzCare</template>
    </alerts>
    <alerts>
        <fullName>Outbound_HTML_Email_Send</fullName>
        <description>Outbound HTML Email Send</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Outbound_HTML_Email_LH</template>
    </alerts>
    <rules>
        <fullName>Send outbound Email Out</fullName>
        <actions>
            <name>Outbound_Email_Send</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Outbound_Emails_Temp__c.Body__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send outbound Email ezCarePoint</fullName>
        <actions>
            <name>Outbound_Email_Send_ezCarePoint</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Outbound_Email_to_Coordinator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Outbound_Emails_Temp__c.htmlBody__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Emails_Temp__c.Product__c</field>
            <operation>equals</operation>
            <value>Point Of Need</value>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Emails_Temp__c.Product__c</field>
            <operation>equals</operation>
            <value>Self Pay</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send outbound HTML Email Out</fullName>
        <actions>
            <name>Outbound_HTML_Email_Send</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Outbound_Emails_Temp__c.htmlBody__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Outbound_Emails_Temp__c.Product__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
