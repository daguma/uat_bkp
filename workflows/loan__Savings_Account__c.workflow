<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>loan__Savings_Account_Status_Update</fullName>
        <field>loan__Status__c</field>
        <literalValue>Active</literalValue>
        <name>Savings Account Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Status_Change_Pend_Approval</fullName>
        <field>loan__Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Status Change Pend Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Update_Approval_Date</fullName>
        <field>loan__Approval_Date__c</field>
        <formula>loan__Branch__r.loan__Current_System_Date__c</formula>
        <name>Update Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
