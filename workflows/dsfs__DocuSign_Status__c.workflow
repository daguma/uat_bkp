<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Docusign_Status_Complete</fullName>
        <description>Docusign Status Complete</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahernandez@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nchavez@fuzionsoft.net</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nchristensen@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/ContractSignedNotification</template>
    </alerts>
</Workflow>
