<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Contract_Active_Payment_Plan_On</fullName>
        <description>When a payment plan record is inserted, set this flag to true.</description>
        <field>loan_Active_Payment_Plan__c</field>
        <literalValue>1</literalValue>
        <name>Set Contract Active Payment Plan On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Loan_Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Payment Plan update Flag</fullName>
        <actions>
            <name>Set_Contract_Active_Payment_Plan_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed - Obligations met</value>
        </criteriaItems>
        <description>Updates the flag on CL Contract when a payment plan is inserted</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
