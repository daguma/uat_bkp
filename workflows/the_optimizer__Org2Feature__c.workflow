<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>the_optimizer__Send_eMail_to_Business_Owner</fullName>
        <description>Send eMail to Business Owner</description>
        <protected>false</protected>
        <recipients>
            <field>the_optimizer__Business_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>the_optimizer__Standard_Feature_Manager/the_optimizer__Initial_Email_Task_Assignment</template>
    </alerts>
    <alerts>
        <fullName>the_optimizer__Send_email_to_IT_Owner</fullName>
        <description>Send email to IT Owner</description>
        <protected>false</protected>
        <recipients>
            <field>the_optimizer__IT_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>the_optimizer__Standard_Feature_Manager/the_optimizer__Initial_Email_Task_Assignment</template>
    </alerts>
    <rules>
        <fullName>the_optimizer__Business Owner Assignment</fullName>
        <actions>
            <name>the_optimizer__Send_eMail_to_Business_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Business Owner that they have a Org2Feature assignment to review.</description>
        <formula>AND(NOT(ISNULL(the_optimizer__Business_Owner__c)),   (ISPICKVAL(the_optimizer__Status__c, &quot;0. Not Reviewed&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>the_optimizer__IT Owner Assignment</fullName>
        <actions>
            <name>the_optimizer__Send_email_to_IT_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify IT Owner that they have a Org2Feature assignment to review.</description>
        <formula>AND(NOT(ISNULL(the_optimizer__IT_Owner__c)),   ISPICKVAL(the_optimizer__Status__c, &quot;0. Not Reviewed&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
