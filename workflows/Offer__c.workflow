<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Offer_Approved</fullName>
        <description>Offer Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Offer_Approval_Process/Offer_Approved</template>
    </alerts>
    <alerts>
        <fullName>Offer_Needs_Approval</fullName>
        <description>Offer Needs Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>ffatras@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hmotaharian@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jvaleo@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nchavez@fuzionsoft.net</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rtavares@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>skaudy@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Offer_Approval_Process/Offer_Approval</template>
    </alerts>
    <alerts>
        <fullName>Offer_Rejected</fullName>
        <description>Offer Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Offer_Approval_Process/Offer_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Offer_Approved</fullName>
        <field>NeedsApproval__c</field>
        <literalValue>0</literalValue>
        <name>Offer Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
