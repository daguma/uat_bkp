<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>loan__Trigger_Batch_Run</fullName>
        <field>loan__Trigger_Batch_Run__c</field>
        <literalValue>1</literalValue>
        <name>Trigger Batch Run</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>loan__Query Scheduler 1</fullName>
        <active>false</active>
        <criteriaItems>
            <field>loan__Archive_Run__c.loan__Scheduled_To_Run__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Archive_Run__c.loan__Completed__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Archive_Run__c.loan__Trigger_Scheduler__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Lets say for &apos;Archive_Run__c&apos; object..</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
