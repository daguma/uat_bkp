<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>loan__Loan_Disbursal_Txn_Update_to_Cleared</fullName>
        <field>loan__Cleared__c</field>
        <literalValue>1</literalValue>
        <name>Loan Disbursal Txn Update to Cleared</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Loan_Disbursal_Txn_Update_to_Rejected</fullName>
        <field>loan__Rejected__c</field>
        <literalValue>1</literalValue>
        <name>Loan Disbursal Txn Update to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Loan_Status_Change</fullName>
        <description>Loan Status changes from Approved to Good standing on loan disbursal approval.</description>
        <field>loan__Loan_Status__c</field>
        <literalValue>Active - Good Standing</literalValue>
        <name>Loan Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>loan__Loan_Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Loan_Status_to_A_GS</fullName>
        <description>Loan Status Update To Active Good Standing</description>
        <field>loan__Loan_Status__c</field>
        <literalValue>Active - Good Standing</literalValue>
        <name>Loan Status to A GS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>loan__Loan_Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Update_Loan_Disbursal_Date</fullName>
        <description>Update Loan Disbursal Date</description>
        <field>loan__Disbursal_Date__c</field>
        <formula>loan__Disbursal_Date__c</formula>
        <name>Update Loan Disbursal Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>loan__Loan_Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Loan Disbursal Creation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>loan__Loan_Disbursal_Transaction__c.loan__Financed_Amount__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Runs when the disbursal is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>loan__Disbursal Date Update</fullName>
        <actions>
            <name>loan__Update_Loan_Disbursal_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Loan Disbursal Date Update</description>
        <formula>OR( ISNULL(loan__Loan_Account__r.loan__Disbursal_Date__c) , loan__Loan_Account__r.loan__Disbursal_Date__c &lt;&gt;  loan__Disbursal_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>loan__Loan Status Update To Active Good Standing</fullName>
        <actions>
            <name>loan__Loan_Status_to_A_GS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Loan Status Update To Active Good Standing</description>
        <formula>(loan__Loan_Account__r.loan__Total_Amount_Overdue__c &lt;= 0) &amp;&amp; ((ISPICKVAL( loan__Loan_Account__r.loan__Loan_Status__c ,&apos;Active - Bad Standing&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>loan__Loan Status Update To GS</fullName>
        <actions>
            <name>loan__Loan_Status_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>loan__Loan_Disbursal_Transaction__c.loan__Cleared__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Disbursal_Transaction__c.loan__Rejected__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
