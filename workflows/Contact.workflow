<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AAN_Notification_for_Opportunity</fullName>
        <description>AAN Notification for Opportunity</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/LP_ANN_Notification_Opp</template>
    </alerts>
    <alerts>
        <fullName>Contact_Assignment_Notification</fullName>
        <description>Contact Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>mjacobson@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>ezcarepoint@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>cnotify__Notification_Templates/Contact_New_assignment_notification</template>
    </alerts>
    <alerts>
        <fullName>LMS_Account_Contact_s_Email_Request_For_Additional_Steps_2</fullName>
        <description>LMS - Account: Contact(s) Email Request For Additional Steps 2</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Contact_Complete_Step_2_of_Application</template>
    </alerts>
    <alerts>
        <fullName>LMS_Account_Contact_s_Email_Request_For_Additional_Steps_3</fullName>
        <description>LMS - Account: Contact(s) Email Request For Additional Steps 3</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Contacts_Step_3_Documents</template>
    </alerts>
    <alerts>
        <fullName>LMS_Account_Contact_s_Email_Request_For_Specific_Documents_Step_3</fullName>
        <description>LMS - Account: Contact(s) Email Request For Specific Documents Step 3</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Contacts_Step_3_Specific_Docs_Needed</template>
    </alerts>
    <alerts>
        <fullName>LMS_Account_Setup_Multi_Location_Alert</fullName>
        <description>LMS - Account Setup Multi-Location Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Welcome_Email_with_Invisalign_Training_Link_Multi_Location</template>
    </alerts>
    <alerts>
        <fullName>LMS_Account_Setup_Single_Location_Alert</fullName>
        <description>LMS - Account Setup Single-Location Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Welcome_Email_with_Invisalign_Training_Link</template>
    </alerts>
    <alerts>
        <fullName>LMS_Authorized_Signer</fullName>
        <description>LMS - Authorized Signer</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Authorized_Signer</template>
    </alerts>
    <alerts>
        <fullName>LMS_Awaiting_Contract_GIACT_Invisalign_Fail_Alert</fullName>
        <description>LMS - Awaiting Contract GIACT/Invisalign Fail Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Awaiting_Contract_GIACT_Invisalign_Fail_Alert</template>
    </alerts>
    <alerts>
        <fullName>LMS_Awaiting_Contract_GIACT_Invisalign_Pass_Alert</fullName>
        <description>LMS - Awaiting Contract GIACT/Invisalign Pass Alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Awaiting_Contract_if_GIACT_Invisalign_is_Passed</template>
    </alerts>
    <alerts>
        <fullName>LMS_Send_Approved_Email_Multi_Location</fullName>
        <description>LMS - Send Approved Email (Multi Location)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>salessupport@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS_Email_Templates/LMS_Welcome_Email_for_Users_with_Invisalign_Training_Link</template>
    </alerts>
    <alerts>
        <fullName>Reset_Password</fullName>
        <description>Reset Password</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Reset_Password</template>
    </alerts>
    <fieldUpdates>
        <fullName>Central_Time</fullName>
        <field>Time_Zone__c</field>
        <literalValue>Central TIme</literalValue>
        <name>Central Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Central_Time_Zone</fullName>
        <field>Time_Zone__c</field>
        <literalValue>Central TIme</literalValue>
        <name>Central Time Zone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DOB</fullName>
        <field>Birthdate</field>
        <formula>DOB__c</formula>
        <name>DOB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Eastern_Time_Zone</fullName>
        <field>Time_Zone__c</field>
        <literalValue>Eastern Time</literalValue>
        <name>Eastern Time Zone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Opt_Out</fullName>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <name>Email Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketo_Sync</fullName>
        <field>Marketo_Sync__c</field>
        <literalValue>1</literalValue>
        <name>Marketo Sync</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mountain_Time_Zone</fullName>
        <field>Time_Zone__c</field>
        <literalValue>Mountain Time</literalValue>
        <name>Mountain Time Zone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pacific_Time_Zone</fullName>
        <field>Time_Zone__c</field>
        <literalValue>Pacific Time</literalValue>
        <name>Pacific Time Zone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Flag_Off</fullName>
        <field>Reset_Portal_Password__c</field>
        <literalValue>0</literalValue>
        <name>Reset Flag Off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Military_Flag_To_True</fullName>
        <field>Military__c</field>
        <literalValue>1</literalValue>
        <name>Set Military Flag To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TwilioSF__Clear_Last_Message_Status</fullName>
        <field>TwilioSF__Last_Message_Status__c</field>
        <name>Clear Last Message Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TwilioSF__Clear_Last_Message_Status_Date</fullName>
        <field>TwilioSF__Last_Message_Status_Date__c</field>
        <name>Clear Last Message Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Employer_Name</fullName>
        <field>Employer_Name__c</field>
        <formula>&apos;N/A&apos;</formula>
        <name>Update Employer Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>mjacobson@lendingpoint.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Phone_Index</fullName>
        <field>Phone_Index__c</field>
        <formula>Phone_Clean__c</formula>
        <name>Update Phone Index</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LMS_Merchants</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contact Employer_Name%5F%5Fc is null set to N%2FA</fullName>
        <actions>
            <name>Update_Employer_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Employer_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DOB</fullName>
        <actions>
            <name>DOB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.DOB__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Do Not Contact %2F Email Opt Out</fullName>
        <actions>
            <name>Email_Opt_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Do_Not_Contact__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If the Do Not Contact check is true automatically update the Email Opt Out field to true.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EzCarePoint Contact Assignment</fullName>
        <actions>
            <name>Contact_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LeadSource</field>
            <operation>contains</operation>
            <value>EZVerify</value>
        </criteriaItems>
        <description>Reassign all EzCarePoint contacts to Victoria Coleman.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LMS Contact Record Type Update</fullName>
        <actions>
            <name>Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Lead_Record_Type_ID_Conversion__c</field>
            <operation>equals</operation>
            <value>0120B000000apiJ</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Maintain Phone Index</fullName>
        <actions>
            <name>Update_Phone_Index</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>BlankValue(Phone_Clean__c, &apos;&apos;) &lt;&gt;  BlankValue(Phone_Index__c, &apos;&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mountain Time Zone</fullName>
        <actions>
            <name>Mountain_Time_Zone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>UT,AZ,MT,NM,ID,WY,CO</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pacific Time Zone</fullName>
        <actions>
            <name>Pacific_Time_Zone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.MailingState</field>
            <operation>equals</operation>
            <value>WA,CA,OR,NV</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TwilioSF__Clear Last Message Status%2FDate</fullName>
        <actions>
            <name>TwilioSF__Clear_Last_Message_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TwilioSF__Clear_Last_Message_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1!=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Military Flag</fullName>
        <actions>
            <name>Set_Military_Flag_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Employment_Type__c</field>
            <operation>equals</operation>
            <value>Military</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Military__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates Military flag when Employment Flag is set to Military</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
