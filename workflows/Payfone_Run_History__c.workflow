<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>KYC_Mode_set_on_Payfone</fullName>
        <field>Kyc_Mode__c</field>
        <formula>&quot;Payfone; challenge questions not asked&quot;</formula>
        <name>KYC Mode set on Payfone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Set KYC on Payfone</fullName>
        <actions>
            <name>KYC_Mode_set_on_Payfone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Payfone_Run_History__c.Verification_Result__c</field>
            <operation>equals</operation>
            <value>Pass</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Kyc_Mode__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
