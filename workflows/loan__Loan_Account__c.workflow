<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ACH_Flag_Email_Alert</fullName>
        <description>ACH Flag Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Member_Services</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/ACH_Flag_Notice</template>
    </alerts>
    <alerts>
        <fullName>ACH_Payment_Reminder_DEFAULT</fullName>
        <description>ACH Payment Reminder - DEFAULT</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/ACH_Customer_Payment_Due_reminder_DEFAULT</template>
    </alerts>
    <alerts>
        <fullName>Active_Charge_Off_Email_Alert_to_Finance_team</fullName>
        <description>Active Charge Off Email Alert to Finance team</description>
        <protected>false</protected>
        <recipients>
            <recipient>Active_charge_Off_Email_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>cnotify__Notification_Templates/Active_Charge_Off_Template</template>
    </alerts>
    <alerts>
        <fullName>Approval_Submitted</fullName>
        <description>Approval Submitted</description>
        <protected>false</protected>
        <recipients>
            <recipient>jholder@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collection_Approvals/Approval_Requested</template>
    </alerts>
    <alerts>
        <fullName>Bankruptcy_Filed_Email_Alert_to_Finance_team</fullName>
        <description>Bankruptcy Filed Email Alert to Finance team</description>
        <protected>false</protected>
        <recipients>
            <recipient>Finance_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Bankruptcy_Filed_Email_Alert_to_Finance_team</template>
    </alerts>
    <alerts>
        <fullName>Changes_Approved</fullName>
        <description>Changes Approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>asibaja@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collection_Approvals/Changes_Approved</template>
    </alerts>
    <alerts>
        <fullName>Changes_Rejected</fullName>
        <description>Changes Rejected</description>
        <protected>false</protected>
        <recipients>
            <recipient>asibaja@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collection_Approvals/Changes_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Contract_Watch_Alert</fullName>
        <description>Contract Watch Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Contract_Watch</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ffxp__ffdc5_emailTemplates/Contract_Watch_Template</template>
    </alerts>
    <alerts>
        <fullName>EzCarePoint_Spanish_ACH_Payment_Reminder</fullName>
        <description>EzCarePoint - Spanish - ACH Payment Reminder - ARES</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/Spanish_EzVerify_ACH_Customer_Payment_Due_reminder_DEFAULT</template>
    </alerts>
    <alerts>
        <fullName>EzCarePoint_Spanish_ACH_Payment_Reminder_GUGG</fullName>
        <description>EzCarePoint - Spanish - ACH Payment Reminder - GUGG</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/Spanish_EzVerify_ACH_Customer_Payment_Due_reminder_GUGG</template>
    </alerts>
    <alerts>
        <fullName>EzCarePoint_Spanish_Pay_By_Check_Payment_Reminder_ARES</fullName>
        <description>EzCarePoint - Spanish - Pay By Check Payment Reminder - ARES</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/Spanish_EzVerify_Pay_By_Check_Payment_Reminder_DEFAULT</template>
    </alerts>
    <alerts>
        <fullName>EzCarePoint_Spanish_Pay_By_Check_Payment_Reminder_GUGG</fullName>
        <description>EzCarePoint - Spanish - Pay By Check Payment Reminder - GUGG</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/Spanish_EzVerify_Pay_By_Check_Payment_Reminder_GUGG</template>
    </alerts>
    <alerts>
        <fullName>Fraud_Contract_Email_Alert_to_Finance_team</fullName>
        <description>Fraud Contract Email Alert to Finance team</description>
        <protected>false</protected>
        <recipients>
            <recipient>Finance_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>cnotify__Notification_Templates/Fraud_Contract_Template</template>
    </alerts>
    <alerts>
        <fullName>Goodbye_Letter_for_Charge_Off_Accounts</fullName>
        <description>Goodbye Letter for Charge Off Accounts</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Collection_Communications/Goodbye_Letter_for_Charge_Off_Accounts</template>
    </alerts>
    <alerts>
        <fullName>LMS_Non_Promotional_Welcome_Email</fullName>
        <description>LMS Non-Promotional Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS/LMS_Welcome_Email_Customers</template>
    </alerts>
    <alerts>
        <fullName>LMS_Payment_Reminder_ACH_Non_Promotional</fullName>
        <description>LMS Payment Reminder ACH / Non-Promotional</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS/Payment_Reminder_ACH_Customer</template>
    </alerts>
    <alerts>
        <fullName>LMS_Payment_Reminder_ACH_Promotional</fullName>
        <description>LMS Payment Reminder ACH / Promotional</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS/Payment_Reminder_ACH_Customer_with_PP</template>
    </alerts>
    <alerts>
        <fullName>LMS_Payment_Reminder_Pay_By_Check_Non_Promotional</fullName>
        <description>LMS Payment Reminder Pay By Check  / Non-Promotional</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS/Payment_Reminder_Non_ACH</template>
    </alerts>
    <alerts>
        <fullName>LMS_Payment_Reminder_Pay_By_Check_Promotional</fullName>
        <description>LMS Payment Reminder Pay By Check / Promotional</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS/Payment_Reminder_Non_ACH_with_PP</template>
    </alerts>
    <alerts>
        <fullName>LMS_Welcome_Email</fullName>
        <description>LMS Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>LMS/Welcome_Email_for_Loan_Hero_Customers_PP</template>
    </alerts>
    <alerts>
        <fullName>Loan_Workout_Agreement_Received</fullName>
        <description>Loan Workout Agreement Received</description>
        <protected>false</protected>
        <recipients>
            <recipient>Member_Services</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>memberservices@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Signed_Loan_Workout_Agreement_Received</template>
    </alerts>
    <alerts>
        <fullName>New_Collection_Case2</fullName>
        <description>New Collection Case2</description>
        <protected>false</protected>
        <recipients>
            <recipient>jcampos@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collection_Communications/Collection_Case</template>
    </alerts>
    <alerts>
        <fullName>OT_ACH_Notice</fullName>
        <description>OT ACH Notice</description>
        <protected>false</protected>
        <recipients>
            <recipient>jcampos@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nharrison@lendingpoint.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/OT_ACH_Notice</template>
    </alerts>
    <alerts>
        <fullName>ObligationMet_14Days</fullName>
        <description>ObligationMet_14Days</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Obligation_Satisfied_OLD</template>
    </alerts>
    <alerts>
        <fullName>ObligationMet_7Days</fullName>
        <description>ObligationMet_7Days</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Obligation_Satisfied_OLD</template>
    </alerts>
    <alerts>
        <fullName>Pay_By_Check</fullName>
        <description>Pay By Check</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/Pay_By_Check_Payment_Reminder_ARES</template>
    </alerts>
    <alerts>
        <fullName>Pay_By_Check_DEFAULT</fullName>
        <description>Pay By Check - DEFAULT</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/Pay_By_Check_Payment_Reminder_DEFAULT</template>
    </alerts>
    <alerts>
        <fullName>Pay_By_Check_GUGG</fullName>
        <description>Pay By Check GUGG</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/Pay_By_Check_Payment_Reminder_GUGG</template>
    </alerts>
    <alerts>
        <fullName>Payment_Reminder</fullName>
        <description>Payment Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/ACH_Customer_Payment_Due_reminder_ARES</template>
    </alerts>
    <alerts>
        <fullName>Payment_Reminder_GUGG</fullName>
        <description>Payment Reminder GUGG</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>payments@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Payment_Reminders/ACH_Customer_Payment_Due_reminder_GUGG</template>
    </alerts>
    <alerts>
        <fullName>WelcomeEmail_Term_Update</fullName>
        <description>WelcomeEmail_Term_Update_NoPromo</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Welcome_to_New_Customers_LMS_FEB_PON_NOPP</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email</fullName>
        <description>Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Welcome_For_New_Customers</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email_EzCare</fullName>
        <description>Welcome Email EzCare</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>ezcarepoint@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EzCarePoint/ezCarePoint_Welcome</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email_Term_Update_Promo</fullName>
        <description>Welcome_Email_Term_Update_Promo</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Welcome_to_New_Customers_LMS_FEB_PON</template>
    </alerts>
    <alerts>
        <fullName>X1_Day_of_Interest_Remediation_Email</fullName>
        <description>1+ Day of Interest Remediation Email</description>
        <protected>false</protected>
        <recipients>
            <field>loan__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@lendingpoint.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/X1_Day_of_Interest_Remediation_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Accrual_Stop_Indicator</fullName>
        <field>loan__Accrual_Stop_Indicator__c</field>
        <literalValue>1</literalValue>
        <name>Accrual Stop Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_status_update</fullName>
        <description>Update the contract status to &quot;Closed settled off&quot; when the contract status is moved to &quot;Settled as agreed&quot;</description>
        <field>loan__Loan_Status__c</field>
        <literalValue>Closed - Settled Off</literalValue>
        <name>Contract status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMS_Email_Sent</fullName>
        <field>LMS_Welcome_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>LMS Email Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMS_Non_Promotional_Email_Sent</fullName>
        <field>LMS_Non_Promotional_Welcome_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>LMS Non-Promotional Email Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Obligations_Satisfied_Email_Sent</fullName>
        <field>Obligations_Satisfied_Email_Sent__c</field>
        <formula>TODAY()</formula>
        <name>Obligations Satisfied Email Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PayOff_AmortizedFee</fullName>
        <field>Amortized_Fee_Amount_PayOff__c</field>
        <formula>Amortized_Fee_Amount__c</formula>
        <name>PayOff AmortizedFee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PayOff_Remaining_Fee</fullName>
        <field>Remaining_Fee_Amount_PayOff__c</field>
        <formula>Remaining_Fee_Amount__c</formula>
        <name>PayOff Remaining Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MEtro2_Reporting_to_false</fullName>
        <field>loan__Include_In_Metro2_File__c</field>
        <literalValue>0</literalValue>
        <name>Set MEtro2 Reporting to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Metro2_Account_Status_Code</fullName>
        <field>loan__Metro2_Account_Status_Code__c</field>
        <formula>IF(PRIORVALUE(loan__Loan_Status__c)=&apos;7&apos; ,&apos;64&apos;,&apos;62&apos;)</formula>
        <name>Set Metro2 Account Status Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Welcome_Email_Sent_Date</fullName>
        <field>Welcome_Email_Sent_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Welcome Email Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_field_Contract_settled_off</fullName>
        <description>Update the field &quot;Contract settled-off&quot;</description>
        <field>Contract_settled_off_date__c</field>
        <formula>NOW()</formula>
        <name>Update the field &quot;Contract settled-off&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Welcome_Email_Sent_Date</fullName>
        <field>Welcome_Email_Sent_Date__c</field>
        <formula>NOW()</formula>
        <name>Welcome Email Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>contract_status_to_Closed_sold_off</fullName>
        <description>Updates the CL contract status to &quot;Closed-sold off&quot;</description>
        <field>loan__Loan_Status__c</field>
        <literalValue>Closed - Sold Off</literalValue>
        <name>contract status to &quot;Closed sold off&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Loan_Status_Update_To_Closed_OM</fullName>
        <description>Loan Status Update To Closed OM</description>
        <field>loan__Loan_Status__c</field>
        <literalValue>Closed - Obligations met</literalValue>
        <name>Loan Status Update To Closed OM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Post_approval_Loan_Status_Update_01</fullName>
        <description>Post-approval Loan Status Update 01</description>
        <field>loan__Loan_Status__c</field>
        <literalValue>Active - Good Standing</literalValue>
        <name>Post-approval Loan Status Update 01</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Status_Change_approved</fullName>
        <field>loan__Loan_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Change approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Status_Change_canceled</fullName>
        <field>loan__Loan_Status__c</field>
        <literalValue>Canceled</literalValue>
        <name>Status Change canceled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Status_Change_pending_app</fullName>
        <field>loan__Loan_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Status Change pending app</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Update_Approval_Date</fullName>
        <field>loan__Approval_Date__c</field>
        <formula>loan__Branch__r.loan__Current_System_Date__c</formula>
        <name>Update Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loan__Update_Payment_Application_Mode</fullName>
        <field>loan__Payment_Application_Mode__c</field>
        <literalValue>Current Dues</literalValue>
        <name>Update Payment Application Mode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>peer__Calculate_VAT_on_Origination</fullName>
        <field>peer__Origination_VAT_Amount__c</field>
        <formula>IF(OR(ISNEW()|| ISCHANGED(peer__Origination_Fee__c)), (IF(OR(ISNULL(peer__VAT_percent__c), NOT(peer__VAT__c)), 0, (peer__Origination_Fee__c * peer__VAT_percent__c)/(1 + peer__VAT_percent__c))),  peer__Origination_VAT_Amount__c)</formula>
        <name>Calculate VAT on Origination Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>peer__Update_VAT_percent</fullName>
        <description>Copies VAT %age from custom settings.</description>
        <field>peer__VAT_percent__c</field>
        <formula>IF(ISNEW(), $Setup.loan__Fractionalization_Parameters__c.peer__Global_VAT_Percentage__c / 100,  peer__VAT_percent__c)</formula>
        <name>Update VAT %age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CL Contract - Non-Promotional Welcome Email LMS</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>loan__Loan_Account__c.Original_Sub_Source__c</field>
            <operation>equals</operation>
            <value>LoanHero</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.LMS_Non_Promotional_Welcome_Email_Sent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Do_Not_Contact_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Application_Type__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>equals</operation>
            <value>Active - Good Standing</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Is_On_Discount_Promotion__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>2/20/2019</value>
        </criteriaItems>
        <description>LMS Non-Promotional Welcome Email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LMS_Non_Promotional_Welcome_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>LMS_Non_Promotional_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>loan__Loan_Account__c.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CL Contract - Promotional Welcome Email LMS</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>loan__Loan_Account__c.Original_Sub_Source__c</field>
            <operation>equals</operation>
            <value>LoanHero</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.LMS_Welcome_Email_Sent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Do_Not_Contact_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Application_Type__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>equals</operation>
            <value>Active - Good Standing</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Is_On_Discount_Promotion__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>2/20/2019</value>
        </criteriaItems>
        <description>LMS Promotional Welcome Email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LMS_Welcome_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>LMS_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>loan__Loan_Account__c.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Loan status update to %22Closed -settled off%22</fullName>
        <actions>
            <name>Contract_status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>equals</operation>
            <value>Settled as Agreed</value>
        </criteriaItems>
        <description>This workflow updates the loan status to &quot;Closed- settled Off&quot; when it&apos;s chosen as &quot;Settled As Agreed&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Loan status update to %22Closed- sold off%22</fullName>
        <actions>
            <name>contract_status_to_Closed_sold_off</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.Charge_Off_Sold_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Charge_Off_Sold_To__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To update the loan status to &quot;closed sold off&quot; when the fields on the contract, &quot;	
Charge Off Sold On&quot; and &quot;Charge Off Sold To&quot; were populated with data.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Metro2 Settled Account</fullName>
        <actions>
            <name>Set_Metro2_Account_Status_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>equals</operation>
            <value>Settled as Agreed</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Settled_Amount__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PayOff Fee</fullName>
        <actions>
            <name>PayOff_AmortizedFee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PayOff_Remaining_Fee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Pay_Off_Amount_As_Of_Today__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email After 14 days for Obligation Met</fullName>
        <active>false</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>equals</operation>
            <value>Closed - Obligations met</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Email_Opt_Out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Salesforce automatically send the Obligation Satisfied notice automatically 14 days after the loan has been fully paid.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ObligationMet_14Days</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Email After 14 days on Obligation Met</fullName>
        <active>false</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>equals</operation>
            <value>Closed - Obligations met</value>
        </criteriaItems>
        <description>Salesforce automatically send the Obligation Satisfied notice automatically 14 days after the loan has been fully paid.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ObligationMet_14Days</name>
                <type>Alert</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Email After 7 days for Obligation Met</fullName>
        <active>true</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>equals</operation>
            <value>Closed - Obligations met</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Email_Opt_Out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.New_Refinance_Contract__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Obligations_Satisfied_Email_Sent__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Salesforce automatically send the Obligation Satisfied notice automatically 7 days after the loan has been fully paid.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ObligationMet_7Days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Obligations_Satisfied_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Signed Loan Workout Agreement Received</fullName>
        <actions>
            <name>Loan_Workout_Agreement_Received</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.Signed_Agreement_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Turn off Metro2 Reporting on Certain Creteria</fullName>
        <actions>
            <name>Set_MEtro2_Reporting_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan_Active_Payment_Plan__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Include_In_Metro2_File__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Charge_Off_Sold_To__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Charged of sold or Active Payment</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update %22Contract settled-off date%22 on CL contracts</fullName>
        <actions>
            <name>Accrual_Stop_Indicator</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_the_field_Contract_settled_off</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update &quot;Contract settled-off date&quot; on CL contracts</description>
        <formula>OR(CONTAINS( text(loan__Loan_Status__c) , &apos;Settled as Agreed&apos;),
CONTAINS( text(loan__Loan_Status__c) , &apos;Closed - Settled Off&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome Email Sent</fullName>
        <actions>
            <name>Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Welcome_Email_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>loan__Loan_Account__c.Welcome_Email_Sent_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.loan__Loan_Status__c</field>
            <operation>equals</operation>
            <value>Active - Good Standing</value>
        </criteriaItems>
        <criteriaItems>
            <field>loan__Loan_Account__c.Email_Opt_Out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>loan__Copy Loan Product Fields</fullName>
        <actions>
            <name>loan__Update_Payment_Application_Mode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(loan__Loan_Product_Name__r.loan__Payment_Application_Mode__c, &apos;Current Dues&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>loan__Start Interest Accrual On Loan</fullName>
        <active>false</active>
        <formula>loan__Number_of_Days_Overdue__c &lt; $Setup.loan__Org_Parameters__c.loan__Interest_Accrual_Thresold_days__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>peer__Update VAT Amount</fullName>
        <actions>
            <name>peer__Calculate_VAT_on_Origination</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>peer__Update_VAT_percent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
