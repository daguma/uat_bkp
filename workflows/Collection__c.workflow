<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>X30_Days</fullName>
        <field>Delinquency_Bucket__c</field>
        <literalValue>30 Days</literalValue>
        <name>30 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X60_Days</fullName>
        <field>Delinquency_Bucket__c</field>
        <literalValue>60 Days</literalValue>
        <name>60 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X90_Days</fullName>
        <field>Delinquency_Bucket__c</field>
        <literalValue>90 Days</literalValue>
        <name>90 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X90_Dayss</fullName>
        <field>Delinquency_Bucket__c</field>
        <literalValue>+90 Days</literalValue>
        <name>+90 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>%2B90 Days</fullName>
        <actions>
            <name>X90_Dayss</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Collection__c.col_Number_of_Days_Overdue__c</field>
            <operation>greaterThan</operation>
            <value>90</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>30 Days</fullName>
        <actions>
            <name>X30_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Collection__c.col_Number_of_Days_Overdue__c</field>
            <operation>lessOrEqual</operation>
            <value>30</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>60 Days</fullName>
        <actions>
            <name>X60_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Collection__c.col_Number_of_Days_Overdue__c</field>
            <operation>greaterThan</operation>
            <value>30</value>
        </criteriaItems>
        <criteriaItems>
            <field>Collection__c.col_Number_of_Days_Overdue__c</field>
            <operation>lessThan</operation>
            <value>60</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>90 Days</fullName>
        <actions>
            <name>X90_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Collection__c.col_Number_of_Days_Overdue__c</field>
            <operation>greaterThan</operation>
            <value>60</value>
        </criteriaItems>
        <criteriaItems>
            <field>Collection__c.col_Number_of_Days_Overdue__c</field>
            <operation>lessThan</operation>
            <value>90</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
